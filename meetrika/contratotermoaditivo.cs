/*
               File: ContratoTermoAditivo
        Description: Contrato Termo Aditivo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:19:46.69
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratotermoaditivo : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxJX_Action18") == 0 )
         {
            A74Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            AV18SaldoContrato_UnidadeMedicao_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18SaldoContrato_UnidadeMedicao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18SaldoContrato_UnidadeMedicao_Codigo), 6, 0)));
            A316ContratoTermoAditivo_DataInicio = context.localUtil.ParseDateParm( GetNextPar( ));
            n316ContratoTermoAditivo_DataInicio = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A316ContratoTermoAditivo_DataInicio", context.localUtil.Format(A316ContratoTermoAditivo_DataInicio, "99/99/99"));
            A317ContratoTermoAditivo_DataFim = context.localUtil.ParseDateParm( GetNextPar( ));
            n317ContratoTermoAditivo_DataFim = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A317ContratoTermoAditivo_DataFim", context.localUtil.Format(A317ContratoTermoAditivo_DataFim, "99/99/99"));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            XC_18_1E53( A74Contrato_Codigo, AV18SaldoContrato_UnidadeMedicao_Codigo, A316ContratoTermoAditivo_DataInicio, A317ContratoTermoAditivo_DataFim) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxJX_Action19") == 0 )
         {
            A315ContratoTermoAditivo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A315ContratoTermoAditivo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A315ContratoTermoAditivo_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            XC_19_1E53( A315ContratoTermoAditivo_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vSALDOCONTRATO_UNIDADEMEDICAO_CODIGO") == 0 )
         {
            AV17Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Contrato_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDSVvSALDOCONTRATO_UNIDADEMEDICAO_CODIGO1E53( AV17Contrato_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_21") == 0 )
         {
            A74Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_21( A74Contrato_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_22") == 0 )
         {
            A39Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_22( A39Contratada_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_23") == 0 )
         {
            A40Contratada_PessoaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A40Contratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A40Contratada_PessoaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_23( A40Contratada_PessoaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7ContratoTermoAditivo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContratoTermoAditivo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoTermoAditivo_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATOTERMOADITIVO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContratoTermoAditivo_Codigo), "ZZZZZ9")));
               AV17Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Contrato_Codigo), 6, 0)));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynavSaldocontrato_unidademedicao_codigo.Name = "vSALDOCONTRATO_UNIDADEMEDICAO_CODIGO";
         dynavSaldocontrato_unidademedicao_codigo.WebTags = "";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Contrato Termo Aditivo", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtContratoTermoAditivo_DataInicio_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contratotermoaditivo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contratotermoaditivo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_ContratoTermoAditivo_Codigo ,
                           ref int aP2_Contrato_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7ContratoTermoAditivo_Codigo = aP1_ContratoTermoAditivo_Codigo;
         this.AV17Contrato_Codigo = aP2_Contrato_Codigo;
         executePrivate();
         aP2_Contrato_Codigo=this.AV17Contrato_Codigo;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavSaldocontrato_unidademedicao_codigo = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavSaldocontrato_unidademedicao_codigo.ItemCount > 0 )
         {
            AV18SaldoContrato_UnidadeMedicao_Codigo = (int)(NumberUtil.Val( dynavSaldocontrato_unidademedicao_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV18SaldoContrato_UnidadeMedicao_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18SaldoContrato_UnidadeMedicao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18SaldoContrato_UnidadeMedicao_Codigo), 6, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_1E53( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_1E53e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtavContrato_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17Contrato_Codigo), 6, 0, ",", "")), ((edtavContrato_codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV17Contrato_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV17Contrato_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavContrato_codigo_Visible, edtavContrato_codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoTermoAditivo.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_1E53( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_1E53( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_1E53e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_89_1E53( true) ;
         }
         return  ;
      }

      protected void wb_table3_89_1E53e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_1E53e( true) ;
         }
         else
         {
            wb_table1_2_1E53e( false) ;
         }
      }

      protected void wb_table3_89_1E53( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_89_1E53e( true) ;
         }
         else
         {
            wb_table3_89_1E53e( false) ;
         }
      }

      protected void wb_table2_5_1E53( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_1E53( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_1E53e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_1E53e( true) ;
         }
         else
         {
            wb_table2_5_1E53e( false) ;
         }
      }

      protected void wb_table4_13_1E53( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable_Internalname, tblTable_Internalname, "", "Table", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"4\" >") ;
            /* Control Group */
            GxWebStd.gx_group_start( context, grpUnnamedgroup1_Internalname, "do Contrato", 1, 0, "px", 0, "px", "Group", "", "HLP_ContratoTermoAditivo.htm");
            wb_table5_17_1E53( true) ;
         }
         return  ;
      }

      protected void wb_table5_17_1E53e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratotermoaditivo_datainicio_Internalname, "Data de inicio da Vig�ncia", "", "", lblTextblockcontratotermoaditivo_datainicio_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtContratoTermoAditivo_DataInicio_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContratoTermoAditivo_DataInicio_Internalname, context.localUtil.Format(A316ContratoTermoAditivo_DataInicio, "99/99/99"), context.localUtil.Format( A316ContratoTermoAditivo_DataInicio, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,50);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoTermoAditivo_DataInicio_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtContratoTermoAditivo_DataInicio_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_ContratoTermoAditivo.htm");
            GxWebStd.gx_bitmap( context, edtContratoTermoAditivo_DataInicio_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContratoTermoAditivo_DataInicio_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoTermoAditivo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratotermoaditivo_datafim_Internalname, "Data de termino da Vig�ncia", "", "", lblTextblockcontratotermoaditivo_datafim_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtContratoTermoAditivo_DataFim_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContratoTermoAditivo_DataFim_Internalname, context.localUtil.Format(A317ContratoTermoAditivo_DataFim, "99/99/99"), context.localUtil.Format( A317ContratoTermoAditivo_DataFim, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoTermoAditivo_DataFim_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtContratoTermoAditivo_DataFim_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_ContratoTermoAditivo.htm");
            GxWebStd.gx_bitmap( context, edtContratoTermoAditivo_DataFim_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContratoTermoAditivo_DataFim_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoTermoAditivo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratotermoaditivo_dataassinatura_Internalname, "Data de assinatura do Termo", "", "", lblTextblockcontratotermoaditivo_dataassinatura_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtContratoTermoAditivo_DataAssinatura_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContratoTermoAditivo_DataAssinatura_Internalname, context.localUtil.Format(A318ContratoTermoAditivo_DataAssinatura, "99/99/99"), context.localUtil.Format( A318ContratoTermoAditivo_DataAssinatura, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoTermoAditivo_DataAssinatura_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtContratoTermoAditivo_DataAssinatura_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_ContratoTermoAditivo.htm");
            GxWebStd.gx_bitmap( context, edtContratoTermoAditivo_DataAssinatura_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContratoTermoAditivo_DataAssinatura_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoTermoAditivo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratotermoaditivo_datapbldou_Internalname, "Data de Publica��o DOU", "", "", lblTextblockcontratotermoaditivo_datapbldou_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtContratoTermoAditivo_DataPblDOU_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContratoTermoAditivo_DataPblDOU_Internalname, context.localUtil.Format(A1363ContratoTermoAditivo_DataPblDOU, "99/99/99"), context.localUtil.Format( A1363ContratoTermoAditivo_DataPblDOU, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,63);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoTermoAditivo_DataPblDOU_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtContratoTermoAditivo_DataPblDOU_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoTermoAditivo.htm");
            GxWebStd.gx_bitmap( context, edtContratoTermoAditivo_DataPblDOU_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContratoTermoAditivo_DataPblDOU_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoTermoAditivo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratotermoaditivo_vlrttlprvto_Internalname, "Valor Contratado R$", "", "", lblTextblockcontratotermoaditivo_vlrttlprvto_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoTermoAditivo_VlrTtlPrvto_Internalname, StringUtil.LTrim( StringUtil.NToC( A1360ContratoTermoAditivo_VlrTtlPrvto, 18, 5, ",", "")), ((edtContratoTermoAditivo_VlrTtlPrvto_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1360ContratoTermoAditivo_VlrTtlPrvto, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( A1360ContratoTermoAditivo_VlrTtlPrvto, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,68);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoTermoAditivo_VlrTtlPrvto_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoTermoAditivo_VlrTtlPrvto_Enabled, 0, "text", "", 100, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_ContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratotermoaditivo_qtdcontratada_Internalname, "Quantidade Contratada", "", "", lblTextblockcontratotermoaditivo_qtdcontratada_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoTermoAditivo_QtdContratada_Internalname, StringUtil.LTrim( StringUtil.NToC( A1362ContratoTermoAditivo_QtdContratada, 14, 5, ",", "")), ((edtContratoTermoAditivo_QtdContratada_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1362ContratoTermoAditivo_QtdContratada, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( A1362ContratoTermoAditivo_QtdContratada, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,72);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoTermoAditivo_QtdContratada_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoTermoAditivo_QtdContratada_Enabled, 0, "text", "", 100, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_ContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksaldocontrato_unidademedicao_codigo_Internalname, "Unidade de Medi��o", "", "", lblTextblocksaldocontrato_unidademedicao_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavSaldocontrato_unidademedicao_codigo, dynavSaldocontrato_unidademedicao_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV18SaldoContrato_UnidadeMedicao_Codigo), 6, 0)), 1, dynavSaldocontrato_unidademedicao_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynavSaldocontrato_unidademedicao_codigo.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,77);\"", "", true, "HLP_ContratoTermoAditivo.htm");
            dynavSaldocontrato_unidademedicao_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV18SaldoContrato_UnidadeMedicao_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSaldocontrato_unidademedicao_codigo_Internalname, "Values", (String)(dynavSaldocontrato_unidademedicao_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratotermoaditivo_vlruntundcnt_Internalname, "Valor da Unidade R$", "", "", lblTextblockcontratotermoaditivo_vlruntundcnt_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoTermoAditivo_VlrUntUndCnt_Internalname, StringUtil.LTrim( StringUtil.NToC( A1361ContratoTermoAditivo_VlrUntUndCnt, 18, 5, ",", "")), ((edtContratoTermoAditivo_VlrUntUndCnt_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1361ContratoTermoAditivo_VlrUntUndCnt, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( A1361ContratoTermoAditivo_VlrUntUndCnt, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,81);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoTermoAditivo_VlrUntUndCnt_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoTermoAditivo_VlrUntUndCnt_Enabled, 0, "text", "", 80, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_ContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratotermoaditivo_observacao_Internalname, "Observa��o", "", "", lblTextblockcontratotermoaditivo_observacao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 86,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContratoTermoAditivo_Observacao_Internalname, A1364ContratoTermoAditivo_Observacao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,86);\"", 0, 1, edtContratoTermoAditivo_Observacao_Enabled, 0, 100, "%", 4, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "DescricaoLonga", "HLP_ContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_1E53e( true) ;
         }
         else
         {
            wb_table4_13_1E53e( false) ;
         }
      }

      protected void wb_table5_17_1E53( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblContrato_Internalname, tblContrato_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_numero_Internalname, "N� do Contrato", "", "", lblTextblockcontrato_numero_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table6_22_1E53( true) ;
         }
         return  ;
      }

      protected void wb_table6_22_1E53e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_pessoanom_Internalname, "Contratada", "", "", lblTextblockcontratada_pessoanom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table7_38_1E53( true) ;
         }
         return  ;
      }

      protected void wb_table7_38_1E53e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_17_1E53e( true) ;
         }
         else
         {
            wb_table5_17_1E53e( false) ;
         }
      }

      protected void wb_table7_38_1E53( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontratada_pessoanom_Internalname, tblTablemergedcontratada_pessoanom_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratada_PessoaNom_Internalname, StringUtil.RTrim( A41Contratada_PessoaNom), StringUtil.RTrim( context.localUtil.Format( A41Contratada_PessoaNom, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_PessoaNom_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratada_PessoaNom_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_ContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_pessoacnpj_Internalname, "CNPJ", "", "", lblTextblockcontratada_pessoacnpj_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratada_PessoaCNPJ_Internalname, A42Contratada_PessoaCNPJ, StringUtil.RTrim( context.localUtil.Format( A42Contratada_PessoaCNPJ, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_PessoaCNPJ_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratada_PessoaCNPJ_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Docto", "left", true, "HLP_ContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_38_1E53e( true) ;
         }
         else
         {
            wb_table7_38_1E53e( false) ;
         }
      }

      protected void wb_table6_22_1E53( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontrato_numero_Internalname, tblTablemergedcontrato_numero_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_Numero_Internalname, StringUtil.RTrim( A77Contrato_Numero), StringUtil.RTrim( context.localUtil.Format( A77Contrato_Numero, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Numero_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContrato_Numero_Enabled, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "NumeroContrato", "left", true, "HLP_ContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_numeroata_Internalname, "N� da Ata", "", "", lblTextblockcontrato_numeroata_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_NumeroAta_Internalname, StringUtil.RTrim( A78Contrato_NumeroAta), StringUtil.RTrim( context.localUtil.Format( A78Contrato_NumeroAta, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_NumeroAta_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContrato_NumeroAta_Enabled, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "NumeroAta", "left", true, "HLP_ContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_ano_Internalname, "Ano", "", "", lblTextblockcontrato_ano_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_Ano_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A79Contrato_Ano), 4, 0, ",", "")), ((edtContrato_Ano_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A79Contrato_Ano), "ZZZ9")) : context.localUtil.Format( (decimal)(A79Contrato_Ano), "ZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Ano_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContrato_Ano_Enabled, 0, "text", "", 50, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "Ano", "right", false, "HLP_ContratoTermoAditivo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_22_1E53e( true) ;
         }
         else
         {
            wb_table6_22_1E53e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E111E2 */
         E111E2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A77Contrato_Numero = cgiGet( edtContrato_Numero_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
               A78Contrato_NumeroAta = cgiGet( edtContrato_NumeroAta_Internalname);
               n78Contrato_NumeroAta = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
               A79Contrato_Ano = (short)(context.localUtil.CToN( cgiGet( edtContrato_Ano_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A79Contrato_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0)));
               A41Contratada_PessoaNom = StringUtil.Upper( cgiGet( edtContratada_PessoaNom_Internalname));
               n41Contratada_PessoaNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
               A42Contratada_PessoaCNPJ = cgiGet( edtContratada_PessoaCNPJ_Internalname);
               n42Contratada_PessoaCNPJ = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
               if ( context.localUtil.VCDate( cgiGet( edtContratoTermoAditivo_DataInicio_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data de inicio da Vig�ncia"}), 1, "CONTRATOTERMOADITIVO_DATAINICIO");
                  AnyError = 1;
                  GX_FocusControl = edtContratoTermoAditivo_DataInicio_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A316ContratoTermoAditivo_DataInicio = DateTime.MinValue;
                  n316ContratoTermoAditivo_DataInicio = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A316ContratoTermoAditivo_DataInicio", context.localUtil.Format(A316ContratoTermoAditivo_DataInicio, "99/99/99"));
               }
               else
               {
                  A316ContratoTermoAditivo_DataInicio = context.localUtil.CToD( cgiGet( edtContratoTermoAditivo_DataInicio_Internalname), 2);
                  n316ContratoTermoAditivo_DataInicio = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A316ContratoTermoAditivo_DataInicio", context.localUtil.Format(A316ContratoTermoAditivo_DataInicio, "99/99/99"));
               }
               n316ContratoTermoAditivo_DataInicio = ((DateTime.MinValue==A316ContratoTermoAditivo_DataInicio) ? true : false);
               if ( context.localUtil.VCDate( cgiGet( edtContratoTermoAditivo_DataFim_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data de termino da Vig�ncia"}), 1, "CONTRATOTERMOADITIVO_DATAFIM");
                  AnyError = 1;
                  GX_FocusControl = edtContratoTermoAditivo_DataFim_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A317ContratoTermoAditivo_DataFim = DateTime.MinValue;
                  n317ContratoTermoAditivo_DataFim = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A317ContratoTermoAditivo_DataFim", context.localUtil.Format(A317ContratoTermoAditivo_DataFim, "99/99/99"));
               }
               else
               {
                  A317ContratoTermoAditivo_DataFim = context.localUtil.CToD( cgiGet( edtContratoTermoAditivo_DataFim_Internalname), 2);
                  n317ContratoTermoAditivo_DataFim = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A317ContratoTermoAditivo_DataFim", context.localUtil.Format(A317ContratoTermoAditivo_DataFim, "99/99/99"));
               }
               n317ContratoTermoAditivo_DataFim = ((DateTime.MinValue==A317ContratoTermoAditivo_DataFim) ? true : false);
               if ( context.localUtil.VCDate( cgiGet( edtContratoTermoAditivo_DataAssinatura_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data de assinatura do Termo"}), 1, "CONTRATOTERMOADITIVO_DATAASSINATURA");
                  AnyError = 1;
                  GX_FocusControl = edtContratoTermoAditivo_DataAssinatura_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A318ContratoTermoAditivo_DataAssinatura = DateTime.MinValue;
                  n318ContratoTermoAditivo_DataAssinatura = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A318ContratoTermoAditivo_DataAssinatura", context.localUtil.Format(A318ContratoTermoAditivo_DataAssinatura, "99/99/99"));
               }
               else
               {
                  A318ContratoTermoAditivo_DataAssinatura = context.localUtil.CToD( cgiGet( edtContratoTermoAditivo_DataAssinatura_Internalname), 2);
                  n318ContratoTermoAditivo_DataAssinatura = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A318ContratoTermoAditivo_DataAssinatura", context.localUtil.Format(A318ContratoTermoAditivo_DataAssinatura, "99/99/99"));
               }
               n318ContratoTermoAditivo_DataAssinatura = ((DateTime.MinValue==A318ContratoTermoAditivo_DataAssinatura) ? true : false);
               if ( context.localUtil.VCDate( cgiGet( edtContratoTermoAditivo_DataPblDOU_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data de Publica��o DOU"}), 1, "CONTRATOTERMOADITIVO_DATAPBLDOU");
                  AnyError = 1;
                  GX_FocusControl = edtContratoTermoAditivo_DataPblDOU_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1363ContratoTermoAditivo_DataPblDOU = DateTime.MinValue;
                  n1363ContratoTermoAditivo_DataPblDOU = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1363ContratoTermoAditivo_DataPblDOU", context.localUtil.Format(A1363ContratoTermoAditivo_DataPblDOU, "99/99/99"));
               }
               else
               {
                  A1363ContratoTermoAditivo_DataPblDOU = context.localUtil.CToD( cgiGet( edtContratoTermoAditivo_DataPblDOU_Internalname), 2);
                  n1363ContratoTermoAditivo_DataPblDOU = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1363ContratoTermoAditivo_DataPblDOU", context.localUtil.Format(A1363ContratoTermoAditivo_DataPblDOU, "99/99/99"));
               }
               n1363ContratoTermoAditivo_DataPblDOU = ((DateTime.MinValue==A1363ContratoTermoAditivo_DataPblDOU) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoTermoAditivo_VlrTtlPrvto_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoTermoAditivo_VlrTtlPrvto_Internalname), ",", ".") > 999999999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOTERMOADITIVO_VLRTTLPRVTO");
                  AnyError = 1;
                  GX_FocusControl = edtContratoTermoAditivo_VlrTtlPrvto_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1360ContratoTermoAditivo_VlrTtlPrvto = 0;
                  n1360ContratoTermoAditivo_VlrTtlPrvto = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1360ContratoTermoAditivo_VlrTtlPrvto", StringUtil.LTrim( StringUtil.Str( A1360ContratoTermoAditivo_VlrTtlPrvto, 18, 5)));
               }
               else
               {
                  A1360ContratoTermoAditivo_VlrTtlPrvto = context.localUtil.CToN( cgiGet( edtContratoTermoAditivo_VlrTtlPrvto_Internalname), ",", ".");
                  n1360ContratoTermoAditivo_VlrTtlPrvto = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1360ContratoTermoAditivo_VlrTtlPrvto", StringUtil.LTrim( StringUtil.Str( A1360ContratoTermoAditivo_VlrTtlPrvto, 18, 5)));
               }
               n1360ContratoTermoAditivo_VlrTtlPrvto = ((Convert.ToDecimal(0)==A1360ContratoTermoAditivo_VlrTtlPrvto) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoTermoAditivo_QtdContratada_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoTermoAditivo_QtdContratada_Internalname), ",", ".") > 99999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOTERMOADITIVO_QTDCONTRATADA");
                  AnyError = 1;
                  GX_FocusControl = edtContratoTermoAditivo_QtdContratada_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1362ContratoTermoAditivo_QtdContratada = 0;
                  n1362ContratoTermoAditivo_QtdContratada = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1362ContratoTermoAditivo_QtdContratada", StringUtil.LTrim( StringUtil.Str( A1362ContratoTermoAditivo_QtdContratada, 14, 5)));
               }
               else
               {
                  A1362ContratoTermoAditivo_QtdContratada = context.localUtil.CToN( cgiGet( edtContratoTermoAditivo_QtdContratada_Internalname), ",", ".");
                  n1362ContratoTermoAditivo_QtdContratada = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1362ContratoTermoAditivo_QtdContratada", StringUtil.LTrim( StringUtil.Str( A1362ContratoTermoAditivo_QtdContratada, 14, 5)));
               }
               n1362ContratoTermoAditivo_QtdContratada = ((Convert.ToDecimal(0)==A1362ContratoTermoAditivo_QtdContratada) ? true : false);
               dynavSaldocontrato_unidademedicao_codigo.CurrentValue = cgiGet( dynavSaldocontrato_unidademedicao_codigo_Internalname);
               AV18SaldoContrato_UnidadeMedicao_Codigo = (int)(NumberUtil.Val( cgiGet( dynavSaldocontrato_unidademedicao_codigo_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18SaldoContrato_UnidadeMedicao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18SaldoContrato_UnidadeMedicao_Codigo), 6, 0)));
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoTermoAditivo_VlrUntUndCnt_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoTermoAditivo_VlrUntUndCnt_Internalname), ",", ".") > 999999999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOTERMOADITIVO_VLRUNTUNDCNT");
                  AnyError = 1;
                  GX_FocusControl = edtContratoTermoAditivo_VlrUntUndCnt_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1361ContratoTermoAditivo_VlrUntUndCnt = 0;
                  n1361ContratoTermoAditivo_VlrUntUndCnt = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1361ContratoTermoAditivo_VlrUntUndCnt", StringUtil.LTrim( StringUtil.Str( A1361ContratoTermoAditivo_VlrUntUndCnt, 18, 5)));
               }
               else
               {
                  A1361ContratoTermoAditivo_VlrUntUndCnt = context.localUtil.CToN( cgiGet( edtContratoTermoAditivo_VlrUntUndCnt_Internalname), ",", ".");
                  n1361ContratoTermoAditivo_VlrUntUndCnt = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1361ContratoTermoAditivo_VlrUntUndCnt", StringUtil.LTrim( StringUtil.Str( A1361ContratoTermoAditivo_VlrUntUndCnt, 18, 5)));
               }
               n1361ContratoTermoAditivo_VlrUntUndCnt = ((Convert.ToDecimal(0)==A1361ContratoTermoAditivo_VlrUntUndCnt) ? true : false);
               A1364ContratoTermoAditivo_Observacao = cgiGet( edtContratoTermoAditivo_Observacao_Internalname);
               n1364ContratoTermoAditivo_Observacao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1364ContratoTermoAditivo_Observacao", A1364ContratoTermoAditivo_Observacao);
               n1364ContratoTermoAditivo_Observacao = (String.IsNullOrEmpty(StringUtil.RTrim( A1364ContratoTermoAditivo_Observacao)) ? true : false);
               AV17Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavContrato_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Contrato_Codigo), 6, 0)));
               /* Read saved values. */
               Z315ContratoTermoAditivo_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z315ContratoTermoAditivo_Codigo"), ",", "."));
               Z1361ContratoTermoAditivo_VlrUntUndCnt = context.localUtil.CToN( cgiGet( "Z1361ContratoTermoAditivo_VlrUntUndCnt"), ",", ".");
               n1361ContratoTermoAditivo_VlrUntUndCnt = ((Convert.ToDecimal(0)==A1361ContratoTermoAditivo_VlrUntUndCnt) ? true : false);
               Z316ContratoTermoAditivo_DataInicio = context.localUtil.CToD( cgiGet( "Z316ContratoTermoAditivo_DataInicio"), 0);
               n316ContratoTermoAditivo_DataInicio = ((DateTime.MinValue==A316ContratoTermoAditivo_DataInicio) ? true : false);
               Z317ContratoTermoAditivo_DataFim = context.localUtil.CToD( cgiGet( "Z317ContratoTermoAditivo_DataFim"), 0);
               n317ContratoTermoAditivo_DataFim = ((DateTime.MinValue==A317ContratoTermoAditivo_DataFim) ? true : false);
               Z318ContratoTermoAditivo_DataAssinatura = context.localUtil.CToD( cgiGet( "Z318ContratoTermoAditivo_DataAssinatura"), 0);
               n318ContratoTermoAditivo_DataAssinatura = ((DateTime.MinValue==A318ContratoTermoAditivo_DataAssinatura) ? true : false);
               Z1360ContratoTermoAditivo_VlrTtlPrvto = context.localUtil.CToN( cgiGet( "Z1360ContratoTermoAditivo_VlrTtlPrvto"), ",", ".");
               n1360ContratoTermoAditivo_VlrTtlPrvto = ((Convert.ToDecimal(0)==A1360ContratoTermoAditivo_VlrTtlPrvto) ? true : false);
               Z1362ContratoTermoAditivo_QtdContratada = context.localUtil.CToN( cgiGet( "Z1362ContratoTermoAditivo_QtdContratada"), ",", ".");
               n1362ContratoTermoAditivo_QtdContratada = ((Convert.ToDecimal(0)==A1362ContratoTermoAditivo_QtdContratada) ? true : false);
               Z1363ContratoTermoAditivo_DataPblDOU = context.localUtil.CToD( cgiGet( "Z1363ContratoTermoAditivo_DataPblDOU"), 0);
               n1363ContratoTermoAditivo_DataPblDOU = ((DateTime.MinValue==A1363ContratoTermoAditivo_DataPblDOU) ? true : false);
               Z74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z74Contrato_Codigo"), ",", "."));
               A74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z74Contrato_Codigo"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "N74Contrato_Codigo"), ",", "."));
               AV7ContratoTermoAditivo_Codigo = (int)(context.localUtil.CToN( cgiGet( "vCONTRATOTERMOADITIVO_CODIGO"), ",", "."));
               A315ContratoTermoAditivo_Codigo = (int)(context.localUtil.CToN( cgiGet( "CONTRATOTERMOADITIVO_CODIGO"), ",", "."));
               AV11Insert_Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_CONTRATO_CODIGO"), ",", "."));
               A74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "CONTRATO_CODIGO"), ",", "."));
               AV13ContratoTermoAditivo_VlrTtlPrvto = context.localUtil.CToN( cgiGet( "vCONTRATOTERMOADITIVO_VLRTTLPRVTO"), ",", ".");
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               AV14ContratoTermoAditivo_QtdContratada = context.localUtil.CToN( cgiGet( "vCONTRATOTERMOADITIVO_QTDCONTRATADA"), ",", ".");
               AV16SaldoContrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "vSALDOCONTRATO_CODIGO"), ",", "."));
               A39Contratada_Codigo = (int)(context.localUtil.CToN( cgiGet( "CONTRATADA_CODIGO"), ",", "."));
               A40Contratada_PessoaCod = (int)(context.localUtil.CToN( cgiGet( "CONTRATADA_PESSOACOD"), ",", "."));
               AV20Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_table_Width = cgiGet( "DVPANEL_TABLE_Width");
               Dvpanel_table_Height = cgiGet( "DVPANEL_TABLE_Height");
               Dvpanel_table_Cls = cgiGet( "DVPANEL_TABLE_Cls");
               Dvpanel_table_Title = cgiGet( "DVPANEL_TABLE_Title");
               Dvpanel_table_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLE_Collapsible"));
               Dvpanel_table_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLE_Collapsed"));
               Dvpanel_table_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLE_Enabled"));
               Dvpanel_table_Class = cgiGet( "DVPANEL_TABLE_Class");
               Dvpanel_table_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLE_Autowidth"));
               Dvpanel_table_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLE_Autoheight"));
               Dvpanel_table_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLE_Showheader"));
               Dvpanel_table_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLE_Showcollapseicon"));
               Dvpanel_table_Iconposition = cgiGet( "DVPANEL_TABLE_Iconposition");
               Dvpanel_table_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLE_Autoscroll"));
               Dvpanel_table_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLE_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "ContratoTermoAditivo";
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A315ContratoTermoAditivo_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9");
               hsh = cgiGet( "hsh");
               if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("contratotermoaditivo:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("contratotermoaditivo:[SecurityCheckFailed value for]"+"ContratoTermoAditivo_Codigo:"+context.localUtil.Format( (decimal)(A315ContratoTermoAditivo_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("contratotermoaditivo:[SecurityCheckFailed value for]"+"Contrato_Codigo:"+context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A315ContratoTermoAditivo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A315ContratoTermoAditivo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A315ContratoTermoAditivo_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode53 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode53;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound53 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_1E0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E111E2 */
                           E111E2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E121E2 */
                           E121E2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E121E2 */
            E121E2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll1E53( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes1E53( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSaldocontrato_unidademedicao_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavSaldocontrato_unidademedicao_codigo.Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_codigo_Enabled), 5, 0)));
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_1E0( )
      {
         BeforeValidate1E53( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls1E53( ) ;
            }
            else
            {
               CheckExtendedTable1E53( ) ;
               CloseExtendedTableCursors1E53( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption1E0( )
      {
      }

      protected void E111E2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV20Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV21GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21GXV1), 8, 0)));
            while ( AV21GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV21GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Contrato_Codigo") == 0 )
               {
                  AV11Insert_Contrato_Codigo = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_Contrato_Codigo), 6, 0)));
               }
               AV21GXV1 = (int)(AV21GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21GXV1), 8, 0)));
            }
         }
         edtavContrato_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_codigo_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            new prc_valoresanterescontratados(context ).execute( ref  AV17Contrato_Codigo, ref  AV13ContratoTermoAditivo_VlrTtlPrvto, ref  AV14ContratoTermoAditivo_QtdContratada, ref  AV15ContratoTermoAditivo_VlrUntUndCnt) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Contrato_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13ContratoTermoAditivo_VlrTtlPrvto", StringUtil.LTrim( StringUtil.Str( AV13ContratoTermoAditivo_VlrTtlPrvto, 18, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ContratoTermoAditivo_QtdContratada", StringUtil.LTrim( StringUtil.Str( AV14ContratoTermoAditivo_QtdContratada, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ContratoTermoAditivo_VlrUntUndCnt", StringUtil.LTrim( StringUtil.Str( AV15ContratoTermoAditivo_VlrUntUndCnt, 18, 5)));
         }
      }

      protected void E121E2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwcontratotermoaditivo.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {(int)AV17Contrato_Codigo});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM1E53( short GX_JID )
      {
         if ( ( GX_JID == 20 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1361ContratoTermoAditivo_VlrUntUndCnt = T001E3_A1361ContratoTermoAditivo_VlrUntUndCnt[0];
               Z316ContratoTermoAditivo_DataInicio = T001E3_A316ContratoTermoAditivo_DataInicio[0];
               Z317ContratoTermoAditivo_DataFim = T001E3_A317ContratoTermoAditivo_DataFim[0];
               Z318ContratoTermoAditivo_DataAssinatura = T001E3_A318ContratoTermoAditivo_DataAssinatura[0];
               Z1360ContratoTermoAditivo_VlrTtlPrvto = T001E3_A1360ContratoTermoAditivo_VlrTtlPrvto[0];
               Z1362ContratoTermoAditivo_QtdContratada = T001E3_A1362ContratoTermoAditivo_QtdContratada[0];
               Z1363ContratoTermoAditivo_DataPblDOU = T001E3_A1363ContratoTermoAditivo_DataPblDOU[0];
               Z74Contrato_Codigo = T001E3_A74Contrato_Codigo[0];
            }
            else
            {
               Z1361ContratoTermoAditivo_VlrUntUndCnt = A1361ContratoTermoAditivo_VlrUntUndCnt;
               Z316ContratoTermoAditivo_DataInicio = A316ContratoTermoAditivo_DataInicio;
               Z317ContratoTermoAditivo_DataFim = A317ContratoTermoAditivo_DataFim;
               Z318ContratoTermoAditivo_DataAssinatura = A318ContratoTermoAditivo_DataAssinatura;
               Z1360ContratoTermoAditivo_VlrTtlPrvto = A1360ContratoTermoAditivo_VlrTtlPrvto;
               Z1362ContratoTermoAditivo_QtdContratada = A1362ContratoTermoAditivo_QtdContratada;
               Z1363ContratoTermoAditivo_DataPblDOU = A1363ContratoTermoAditivo_DataPblDOU;
               Z74Contrato_Codigo = A74Contrato_Codigo;
            }
         }
         if ( GX_JID == -20 )
         {
            Z315ContratoTermoAditivo_Codigo = A315ContratoTermoAditivo_Codigo;
            Z1361ContratoTermoAditivo_VlrUntUndCnt = A1361ContratoTermoAditivo_VlrUntUndCnt;
            Z316ContratoTermoAditivo_DataInicio = A316ContratoTermoAditivo_DataInicio;
            Z317ContratoTermoAditivo_DataFim = A317ContratoTermoAditivo_DataFim;
            Z318ContratoTermoAditivo_DataAssinatura = A318ContratoTermoAditivo_DataAssinatura;
            Z1360ContratoTermoAditivo_VlrTtlPrvto = A1360ContratoTermoAditivo_VlrTtlPrvto;
            Z1362ContratoTermoAditivo_QtdContratada = A1362ContratoTermoAditivo_QtdContratada;
            Z1363ContratoTermoAditivo_DataPblDOU = A1363ContratoTermoAditivo_DataPblDOU;
            Z1364ContratoTermoAditivo_Observacao = A1364ContratoTermoAditivo_Observacao;
            Z74Contrato_Codigo = A74Contrato_Codigo;
            Z39Contratada_Codigo = A39Contratada_Codigo;
            Z77Contrato_Numero = A77Contrato_Numero;
            Z78Contrato_NumeroAta = A78Contrato_NumeroAta;
            Z79Contrato_Ano = A79Contrato_Ano;
            Z40Contratada_PessoaCod = A40Contratada_PessoaCod;
            Z41Contratada_PessoaNom = A41Contratada_PessoaNom;
            Z42Contratada_PessoaCNPJ = A42Contratada_PessoaCNPJ;
         }
      }

      protected void standaloneNotModal( )
      {
         AV20Pgmname = "ContratoTermoAditivo";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Pgmname", AV20Pgmname);
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7ContratoTermoAditivo_Codigo) )
         {
            A315ContratoTermoAditivo_Codigo = AV7ContratoTermoAditivo_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A315ContratoTermoAditivo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A315ContratoTermoAditivo_Codigo), 6, 0)));
         }
         GXVvSALDOCONTRATO_UNIDADEMEDICAO_CODIGO_html1E53( AV17Contrato_Codigo) ;
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Contrato_Codigo) )
         {
            A74Contrato_Codigo = AV11Insert_Contrato_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (Convert.ToDecimal(0)==A1360ContratoTermoAditivo_VlrTtlPrvto) && ( Gx_BScreen == 0 ) )
         {
            A1360ContratoTermoAditivo_VlrTtlPrvto = AV13ContratoTermoAditivo_VlrTtlPrvto;
            n1360ContratoTermoAditivo_VlrTtlPrvto = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1360ContratoTermoAditivo_VlrTtlPrvto", StringUtil.LTrim( StringUtil.Str( A1360ContratoTermoAditivo_VlrTtlPrvto, 18, 5)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (Convert.ToDecimal(0)==A1362ContratoTermoAditivo_QtdContratada) && ( Gx_BScreen == 0 ) )
         {
            A1362ContratoTermoAditivo_QtdContratada = AV14ContratoTermoAditivo_QtdContratada;
            n1362ContratoTermoAditivo_QtdContratada = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1362ContratoTermoAditivo_QtdContratada", StringUtil.LTrim( StringUtil.Str( A1362ContratoTermoAditivo_QtdContratada, 14, 5)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T001E4 */
            pr_default.execute(2, new Object[] {A74Contrato_Codigo});
            A39Contratada_Codigo = T001E4_A39Contratada_Codigo[0];
            A77Contrato_Numero = T001E4_A77Contrato_Numero[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
            A78Contrato_NumeroAta = T001E4_A78Contrato_NumeroAta[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
            n78Contrato_NumeroAta = T001E4_n78Contrato_NumeroAta[0];
            A79Contrato_Ano = T001E4_A79Contrato_Ano[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A79Contrato_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0)));
            pr_default.close(2);
            /* Using cursor T001E5 */
            pr_default.execute(3, new Object[] {A39Contratada_Codigo});
            A40Contratada_PessoaCod = T001E5_A40Contratada_PessoaCod[0];
            pr_default.close(3);
            /* Using cursor T001E6 */
            pr_default.execute(4, new Object[] {A40Contratada_PessoaCod});
            A41Contratada_PessoaNom = T001E6_A41Contratada_PessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
            n41Contratada_PessoaNom = T001E6_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = T001E6_A42Contratada_PessoaCNPJ[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
            n42Contratada_PessoaCNPJ = T001E6_n42Contratada_PessoaCNPJ[0];
            pr_default.close(4);
            if ( ( A1360ContratoTermoAditivo_VlrTtlPrvto > Convert.ToDecimal( 0 )) && ( A1362ContratoTermoAditivo_QtdContratada > Convert.ToDecimal( 0 )) )
            {
               A1361ContratoTermoAditivo_VlrUntUndCnt = (decimal)(A1360ContratoTermoAditivo_VlrTtlPrvto/ (decimal)(A1362ContratoTermoAditivo_QtdContratada));
               n1361ContratoTermoAditivo_VlrUntUndCnt = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1361ContratoTermoAditivo_VlrUntUndCnt", StringUtil.LTrim( StringUtil.Str( A1361ContratoTermoAditivo_VlrUntUndCnt, 18, 5)));
            }
            else
            {
               if ( ( A1360ContratoTermoAditivo_VlrTtlPrvto == Convert.ToDecimal( 0 )) || ( A1362ContratoTermoAditivo_QtdContratada == Convert.ToDecimal( 0 )) )
               {
                  A1361ContratoTermoAditivo_VlrUntUndCnt = 0;
                  n1361ContratoTermoAditivo_VlrUntUndCnt = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1361ContratoTermoAditivo_VlrUntUndCnt", StringUtil.LTrim( StringUtil.Str( A1361ContratoTermoAditivo_VlrUntUndCnt, 18, 5)));
                  n1361ContratoTermoAditivo_VlrUntUndCnt = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1361ContratoTermoAditivo_VlrUntUndCnt", StringUtil.LTrim( StringUtil.Str( A1361ContratoTermoAditivo_VlrUntUndCnt, 18, 5)));
               }
            }
         }
      }

      protected void Load1E53( )
      {
         /* Using cursor T001E7 */
         pr_default.execute(5, new Object[] {A315ContratoTermoAditivo_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound53 = 1;
            A39Contratada_Codigo = T001E7_A39Contratada_Codigo[0];
            A1361ContratoTermoAditivo_VlrUntUndCnt = T001E7_A1361ContratoTermoAditivo_VlrUntUndCnt[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1361ContratoTermoAditivo_VlrUntUndCnt", StringUtil.LTrim( StringUtil.Str( A1361ContratoTermoAditivo_VlrUntUndCnt, 18, 5)));
            n1361ContratoTermoAditivo_VlrUntUndCnt = T001E7_n1361ContratoTermoAditivo_VlrUntUndCnt[0];
            A77Contrato_Numero = T001E7_A77Contrato_Numero[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
            A78Contrato_NumeroAta = T001E7_A78Contrato_NumeroAta[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
            n78Contrato_NumeroAta = T001E7_n78Contrato_NumeroAta[0];
            A79Contrato_Ano = T001E7_A79Contrato_Ano[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A79Contrato_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0)));
            A41Contratada_PessoaNom = T001E7_A41Contratada_PessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
            n41Contratada_PessoaNom = T001E7_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = T001E7_A42Contratada_PessoaCNPJ[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
            n42Contratada_PessoaCNPJ = T001E7_n42Contratada_PessoaCNPJ[0];
            A316ContratoTermoAditivo_DataInicio = T001E7_A316ContratoTermoAditivo_DataInicio[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A316ContratoTermoAditivo_DataInicio", context.localUtil.Format(A316ContratoTermoAditivo_DataInicio, "99/99/99"));
            n316ContratoTermoAditivo_DataInicio = T001E7_n316ContratoTermoAditivo_DataInicio[0];
            A317ContratoTermoAditivo_DataFim = T001E7_A317ContratoTermoAditivo_DataFim[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A317ContratoTermoAditivo_DataFim", context.localUtil.Format(A317ContratoTermoAditivo_DataFim, "99/99/99"));
            n317ContratoTermoAditivo_DataFim = T001E7_n317ContratoTermoAditivo_DataFim[0];
            A318ContratoTermoAditivo_DataAssinatura = T001E7_A318ContratoTermoAditivo_DataAssinatura[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A318ContratoTermoAditivo_DataAssinatura", context.localUtil.Format(A318ContratoTermoAditivo_DataAssinatura, "99/99/99"));
            n318ContratoTermoAditivo_DataAssinatura = T001E7_n318ContratoTermoAditivo_DataAssinatura[0];
            A1360ContratoTermoAditivo_VlrTtlPrvto = T001E7_A1360ContratoTermoAditivo_VlrTtlPrvto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1360ContratoTermoAditivo_VlrTtlPrvto", StringUtil.LTrim( StringUtil.Str( A1360ContratoTermoAditivo_VlrTtlPrvto, 18, 5)));
            n1360ContratoTermoAditivo_VlrTtlPrvto = T001E7_n1360ContratoTermoAditivo_VlrTtlPrvto[0];
            A1362ContratoTermoAditivo_QtdContratada = T001E7_A1362ContratoTermoAditivo_QtdContratada[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1362ContratoTermoAditivo_QtdContratada", StringUtil.LTrim( StringUtil.Str( A1362ContratoTermoAditivo_QtdContratada, 14, 5)));
            n1362ContratoTermoAditivo_QtdContratada = T001E7_n1362ContratoTermoAditivo_QtdContratada[0];
            A1363ContratoTermoAditivo_DataPblDOU = T001E7_A1363ContratoTermoAditivo_DataPblDOU[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1363ContratoTermoAditivo_DataPblDOU", context.localUtil.Format(A1363ContratoTermoAditivo_DataPblDOU, "99/99/99"));
            n1363ContratoTermoAditivo_DataPblDOU = T001E7_n1363ContratoTermoAditivo_DataPblDOU[0];
            A1364ContratoTermoAditivo_Observacao = T001E7_A1364ContratoTermoAditivo_Observacao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1364ContratoTermoAditivo_Observacao", A1364ContratoTermoAditivo_Observacao);
            n1364ContratoTermoAditivo_Observacao = T001E7_n1364ContratoTermoAditivo_Observacao[0];
            A74Contrato_Codigo = T001E7_A74Contrato_Codigo[0];
            A40Contratada_PessoaCod = T001E7_A40Contratada_PessoaCod[0];
            ZM1E53( -20) ;
         }
         pr_default.close(5);
         OnLoadActions1E53( ) ;
      }

      protected void OnLoadActions1E53( )
      {
         if ( ( A1360ContratoTermoAditivo_VlrTtlPrvto > Convert.ToDecimal( 0 )) && ( A1362ContratoTermoAditivo_QtdContratada > Convert.ToDecimal( 0 )) )
         {
            A1361ContratoTermoAditivo_VlrUntUndCnt = (decimal)(A1360ContratoTermoAditivo_VlrTtlPrvto/ (decimal)(A1362ContratoTermoAditivo_QtdContratada));
            n1361ContratoTermoAditivo_VlrUntUndCnt = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1361ContratoTermoAditivo_VlrUntUndCnt", StringUtil.LTrim( StringUtil.Str( A1361ContratoTermoAditivo_VlrUntUndCnt, 18, 5)));
         }
         else
         {
            if ( ( A1360ContratoTermoAditivo_VlrTtlPrvto == Convert.ToDecimal( 0 )) || ( A1362ContratoTermoAditivo_QtdContratada == Convert.ToDecimal( 0 )) )
            {
               A1361ContratoTermoAditivo_VlrUntUndCnt = 0;
               n1361ContratoTermoAditivo_VlrUntUndCnt = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1361ContratoTermoAditivo_VlrUntUndCnt", StringUtil.LTrim( StringUtil.Str( A1361ContratoTermoAditivo_VlrUntUndCnt, 18, 5)));
               n1361ContratoTermoAditivo_VlrUntUndCnt = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1361ContratoTermoAditivo_VlrUntUndCnt", StringUtil.LTrim( StringUtil.Str( A1361ContratoTermoAditivo_VlrUntUndCnt, 18, 5)));
            }
         }
      }

      protected void CheckExtendedTable1E53( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && ! new prc_saldocontrato_verifica(context).executeUdp( ref  AV17Contrato_Codigo, ref  A316ContratoTermoAditivo_DataInicio) )
         {
            GX_msglist.addItem("Data de inicio da vig�ncia do termo aditivo deve ser maior que a data de t�rmino da vig�ncia atual do contrato", 1, "CONTRATOTERMOADITIVO_DATAINICIO");
            AnyError = 1;
            GX_FocusControl = edtContratoTermoAditivo_DataInicio_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( (DateTime.MinValue==A316ContratoTermoAditivo_DataInicio) || ( A316ContratoTermoAditivo_DataInicio >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Data de inicio da Vig�ncia fora do intervalo", "OutOfRange", 1, "CONTRATOTERMOADITIVO_DATAINICIO");
            AnyError = 1;
            GX_FocusControl = edtContratoTermoAditivo_DataInicio_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( (DateTime.MinValue==A316ContratoTermoAditivo_DataInicio) )
         {
            GX_msglist.addItem("Data de inicio da Vig�ncia � obrigat�rio.", 1, "CONTRATOTERMOADITIVO_DATAINICIO");
            AnyError = 1;
            GX_FocusControl = edtContratoTermoAditivo_DataInicio_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( (DateTime.MinValue==A317ContratoTermoAditivo_DataFim) || ( A317ContratoTermoAditivo_DataFim >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Data de termino da Vig�ncia fora do intervalo", "OutOfRange", 1, "CONTRATOTERMOADITIVO_DATAFIM");
            AnyError = 1;
            GX_FocusControl = edtContratoTermoAditivo_DataFim_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( (DateTime.MinValue==A317ContratoTermoAditivo_DataFim) )
         {
            GX_msglist.addItem("Data de termino da Vig�ncia � obrigat�rio.", 1, "CONTRATOTERMOADITIVO_DATAFIM");
            AnyError = 1;
            GX_FocusControl = edtContratoTermoAditivo_DataFim_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( (DateTime.MinValue==A318ContratoTermoAditivo_DataAssinatura) || ( A318ContratoTermoAditivo_DataAssinatura >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Data de assinatura do Termo fora do intervalo", "OutOfRange", 1, "CONTRATOTERMOADITIVO_DATAASSINATURA");
            AnyError = 1;
            GX_FocusControl = edtContratoTermoAditivo_DataAssinatura_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ( A1360ContratoTermoAditivo_VlrTtlPrvto > Convert.ToDecimal( 0 )) && ( A1362ContratoTermoAditivo_QtdContratada > Convert.ToDecimal( 0 )) )
         {
            A1361ContratoTermoAditivo_VlrUntUndCnt = (decimal)(A1360ContratoTermoAditivo_VlrTtlPrvto/ (decimal)(A1362ContratoTermoAditivo_QtdContratada));
            n1361ContratoTermoAditivo_VlrUntUndCnt = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1361ContratoTermoAditivo_VlrUntUndCnt", StringUtil.LTrim( StringUtil.Str( A1361ContratoTermoAditivo_VlrUntUndCnt, 18, 5)));
         }
         else
         {
            if ( ( A1360ContratoTermoAditivo_VlrTtlPrvto == Convert.ToDecimal( 0 )) || ( A1362ContratoTermoAditivo_QtdContratada == Convert.ToDecimal( 0 )) )
            {
               A1361ContratoTermoAditivo_VlrUntUndCnt = 0;
               n1361ContratoTermoAditivo_VlrUntUndCnt = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1361ContratoTermoAditivo_VlrUntUndCnt", StringUtil.LTrim( StringUtil.Str( A1361ContratoTermoAditivo_VlrUntUndCnt, 18, 5)));
               n1361ContratoTermoAditivo_VlrUntUndCnt = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1361ContratoTermoAditivo_VlrUntUndCnt", StringUtil.LTrim( StringUtil.Str( A1361ContratoTermoAditivo_VlrUntUndCnt, 18, 5)));
            }
         }
         if ( ! ( (DateTime.MinValue==A1363ContratoTermoAditivo_DataPblDOU) || ( A1363ContratoTermoAditivo_DataPblDOU >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Data de Publica��o DOU fora do intervalo", "OutOfRange", 1, "CONTRATOTERMOADITIVO_DATAPBLDOU");
            AnyError = 1;
            GX_FocusControl = edtContratoTermoAditivo_DataPblDOU_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( (0==AV18SaldoContrato_UnidadeMedicao_Codigo) )
         {
            GX_msglist.addItem("Unidade de Medi��o � obrigat�rio.", 1, "vSALDOCONTRATO_UNIDADEMEDICAO_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynavSaldocontrato_unidademedicao_codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T001E4 */
         pr_default.execute(2, new Object[] {A74Contrato_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A39Contratada_Codigo = T001E4_A39Contratada_Codigo[0];
         A77Contrato_Numero = T001E4_A77Contrato_Numero[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
         A78Contrato_NumeroAta = T001E4_A78Contrato_NumeroAta[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
         n78Contrato_NumeroAta = T001E4_n78Contrato_NumeroAta[0];
         A79Contrato_Ano = T001E4_A79Contrato_Ano[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A79Contrato_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0)));
         pr_default.close(2);
         /* Using cursor T001E5 */
         pr_default.execute(3, new Object[] {A39Contratada_Codigo});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contratada'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A40Contratada_PessoaCod = T001E5_A40Contratada_PessoaCod[0];
         pr_default.close(3);
         /* Using cursor T001E6 */
         pr_default.execute(4, new Object[] {A40Contratada_PessoaCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contratada_Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A41Contratada_PessoaNom = T001E6_A41Contratada_PessoaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
         n41Contratada_PessoaNom = T001E6_n41Contratada_PessoaNom[0];
         A42Contratada_PessoaCNPJ = T001E6_A42Contratada_PessoaCNPJ[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
         n42Contratada_PessoaCNPJ = T001E6_n42Contratada_PessoaCNPJ[0];
         pr_default.close(4);
      }

      protected void CloseExtendedTableCursors1E53( )
      {
         pr_default.close(2);
         pr_default.close(3);
         pr_default.close(4);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_21( int A74Contrato_Codigo )
      {
         /* Using cursor T001E8 */
         pr_default.execute(6, new Object[] {A74Contrato_Codigo});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A39Contratada_Codigo = T001E8_A39Contratada_Codigo[0];
         A77Contrato_Numero = T001E8_A77Contrato_Numero[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
         A78Contrato_NumeroAta = T001E8_A78Contrato_NumeroAta[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
         n78Contrato_NumeroAta = T001E8_n78Contrato_NumeroAta[0];
         A79Contrato_Ano = T001E8_A79Contrato_Ano[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A79Contrato_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0)));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ".", "")))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A77Contrato_Numero))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A78Contrato_NumeroAta))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A79Contrato_Ano), 4, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void gxLoad_22( int A39Contratada_Codigo )
      {
         /* Using cursor T001E9 */
         pr_default.execute(7, new Object[] {A39Contratada_Codigo});
         if ( (pr_default.getStatus(7) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contratada'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A40Contratada_PessoaCod = T001E9_A40Contratada_PessoaCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A40Contratada_PessoaCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(7) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(7);
      }

      protected void gxLoad_23( int A40Contratada_PessoaCod )
      {
         /* Using cursor T001E10 */
         pr_default.execute(8, new Object[] {A40Contratada_PessoaCod});
         if ( (pr_default.getStatus(8) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contratada_Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A41Contratada_PessoaNom = T001E10_A41Contratada_PessoaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
         n41Contratada_PessoaNom = T001E10_n41Contratada_PessoaNom[0];
         A42Contratada_PessoaCNPJ = T001E10_A42Contratada_PessoaCNPJ[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
         n42Contratada_PessoaCNPJ = T001E10_n42Contratada_PessoaCNPJ[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A41Contratada_PessoaNom))+"\""+","+"\""+GXUtil.EncodeJSConstant( A42Contratada_PessoaCNPJ)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(8) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(8);
      }

      protected void GetKey1E53( )
      {
         /* Using cursor T001E11 */
         pr_default.execute(9, new Object[] {A315ContratoTermoAditivo_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            RcdFound53 = 1;
         }
         else
         {
            RcdFound53 = 0;
         }
         pr_default.close(9);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T001E3 */
         pr_default.execute(1, new Object[] {A315ContratoTermoAditivo_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM1E53( 20) ;
            RcdFound53 = 1;
            A315ContratoTermoAditivo_Codigo = T001E3_A315ContratoTermoAditivo_Codigo[0];
            A1361ContratoTermoAditivo_VlrUntUndCnt = T001E3_A1361ContratoTermoAditivo_VlrUntUndCnt[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1361ContratoTermoAditivo_VlrUntUndCnt", StringUtil.LTrim( StringUtil.Str( A1361ContratoTermoAditivo_VlrUntUndCnt, 18, 5)));
            n1361ContratoTermoAditivo_VlrUntUndCnt = T001E3_n1361ContratoTermoAditivo_VlrUntUndCnt[0];
            A316ContratoTermoAditivo_DataInicio = T001E3_A316ContratoTermoAditivo_DataInicio[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A316ContratoTermoAditivo_DataInicio", context.localUtil.Format(A316ContratoTermoAditivo_DataInicio, "99/99/99"));
            n316ContratoTermoAditivo_DataInicio = T001E3_n316ContratoTermoAditivo_DataInicio[0];
            A317ContratoTermoAditivo_DataFim = T001E3_A317ContratoTermoAditivo_DataFim[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A317ContratoTermoAditivo_DataFim", context.localUtil.Format(A317ContratoTermoAditivo_DataFim, "99/99/99"));
            n317ContratoTermoAditivo_DataFim = T001E3_n317ContratoTermoAditivo_DataFim[0];
            A318ContratoTermoAditivo_DataAssinatura = T001E3_A318ContratoTermoAditivo_DataAssinatura[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A318ContratoTermoAditivo_DataAssinatura", context.localUtil.Format(A318ContratoTermoAditivo_DataAssinatura, "99/99/99"));
            n318ContratoTermoAditivo_DataAssinatura = T001E3_n318ContratoTermoAditivo_DataAssinatura[0];
            A1360ContratoTermoAditivo_VlrTtlPrvto = T001E3_A1360ContratoTermoAditivo_VlrTtlPrvto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1360ContratoTermoAditivo_VlrTtlPrvto", StringUtil.LTrim( StringUtil.Str( A1360ContratoTermoAditivo_VlrTtlPrvto, 18, 5)));
            n1360ContratoTermoAditivo_VlrTtlPrvto = T001E3_n1360ContratoTermoAditivo_VlrTtlPrvto[0];
            A1362ContratoTermoAditivo_QtdContratada = T001E3_A1362ContratoTermoAditivo_QtdContratada[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1362ContratoTermoAditivo_QtdContratada", StringUtil.LTrim( StringUtil.Str( A1362ContratoTermoAditivo_QtdContratada, 14, 5)));
            n1362ContratoTermoAditivo_QtdContratada = T001E3_n1362ContratoTermoAditivo_QtdContratada[0];
            A1363ContratoTermoAditivo_DataPblDOU = T001E3_A1363ContratoTermoAditivo_DataPblDOU[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1363ContratoTermoAditivo_DataPblDOU", context.localUtil.Format(A1363ContratoTermoAditivo_DataPblDOU, "99/99/99"));
            n1363ContratoTermoAditivo_DataPblDOU = T001E3_n1363ContratoTermoAditivo_DataPblDOU[0];
            A1364ContratoTermoAditivo_Observacao = T001E3_A1364ContratoTermoAditivo_Observacao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1364ContratoTermoAditivo_Observacao", A1364ContratoTermoAditivo_Observacao);
            n1364ContratoTermoAditivo_Observacao = T001E3_n1364ContratoTermoAditivo_Observacao[0];
            A74Contrato_Codigo = T001E3_A74Contrato_Codigo[0];
            Z315ContratoTermoAditivo_Codigo = A315ContratoTermoAditivo_Codigo;
            sMode53 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load1E53( ) ;
            if ( AnyError == 1 )
            {
               RcdFound53 = 0;
               InitializeNonKey1E53( ) ;
            }
            Gx_mode = sMode53;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound53 = 0;
            InitializeNonKey1E53( ) ;
            sMode53 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode53;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey1E53( ) ;
         if ( RcdFound53 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound53 = 0;
         /* Using cursor T001E12 */
         pr_default.execute(10, new Object[] {A315ContratoTermoAditivo_Codigo});
         if ( (pr_default.getStatus(10) != 101) )
         {
            while ( (pr_default.getStatus(10) != 101) && ( ( T001E12_A315ContratoTermoAditivo_Codigo[0] < A315ContratoTermoAditivo_Codigo ) ) )
            {
               pr_default.readNext(10);
            }
            if ( (pr_default.getStatus(10) != 101) && ( ( T001E12_A315ContratoTermoAditivo_Codigo[0] > A315ContratoTermoAditivo_Codigo ) ) )
            {
               A315ContratoTermoAditivo_Codigo = T001E12_A315ContratoTermoAditivo_Codigo[0];
               RcdFound53 = 1;
            }
         }
         pr_default.close(10);
      }

      protected void move_previous( )
      {
         RcdFound53 = 0;
         /* Using cursor T001E13 */
         pr_default.execute(11, new Object[] {A315ContratoTermoAditivo_Codigo});
         if ( (pr_default.getStatus(11) != 101) )
         {
            while ( (pr_default.getStatus(11) != 101) && ( ( T001E13_A315ContratoTermoAditivo_Codigo[0] > A315ContratoTermoAditivo_Codigo ) ) )
            {
               pr_default.readNext(11);
            }
            if ( (pr_default.getStatus(11) != 101) && ( ( T001E13_A315ContratoTermoAditivo_Codigo[0] < A315ContratoTermoAditivo_Codigo ) ) )
            {
               A315ContratoTermoAditivo_Codigo = T001E13_A315ContratoTermoAditivo_Codigo[0];
               RcdFound53 = 1;
            }
         }
         pr_default.close(11);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey1E53( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtContratoTermoAditivo_DataInicio_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert1E53( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound53 == 1 )
            {
               if ( A315ContratoTermoAditivo_Codigo != Z315ContratoTermoAditivo_Codigo )
               {
                  A315ContratoTermoAditivo_Codigo = Z315ContratoTermoAditivo_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A315ContratoTermoAditivo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A315ContratoTermoAditivo_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtContratoTermoAditivo_DataInicio_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update1E53( ) ;
                  GX_FocusControl = edtContratoTermoAditivo_DataInicio_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A315ContratoTermoAditivo_Codigo != Z315ContratoTermoAditivo_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtContratoTermoAditivo_DataInicio_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert1E53( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                     AnyError = 1;
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtContratoTermoAditivo_DataInicio_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert1E53( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A315ContratoTermoAditivo_Codigo != Z315ContratoTermoAditivo_Codigo )
         {
            A315ContratoTermoAditivo_Codigo = Z315ContratoTermoAditivo_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A315ContratoTermoAditivo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A315ContratoTermoAditivo_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "");
            AnyError = 1;
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtContratoTermoAditivo_DataInicio_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency1E53( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T001E2 */
            pr_default.execute(0, new Object[] {A315ContratoTermoAditivo_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoTermoAditivo"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( Z1361ContratoTermoAditivo_VlrUntUndCnt != T001E2_A1361ContratoTermoAditivo_VlrUntUndCnt[0] ) || ( Z316ContratoTermoAditivo_DataInicio != T001E2_A316ContratoTermoAditivo_DataInicio[0] ) || ( Z317ContratoTermoAditivo_DataFim != T001E2_A317ContratoTermoAditivo_DataFim[0] ) || ( Z318ContratoTermoAditivo_DataAssinatura != T001E2_A318ContratoTermoAditivo_DataAssinatura[0] ) || ( Z1360ContratoTermoAditivo_VlrTtlPrvto != T001E2_A1360ContratoTermoAditivo_VlrTtlPrvto[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1362ContratoTermoAditivo_QtdContratada != T001E2_A1362ContratoTermoAditivo_QtdContratada[0] ) || ( Z1363ContratoTermoAditivo_DataPblDOU != T001E2_A1363ContratoTermoAditivo_DataPblDOU[0] ) || ( Z74Contrato_Codigo != T001E2_A74Contrato_Codigo[0] ) )
            {
               if ( Z1361ContratoTermoAditivo_VlrUntUndCnt != T001E2_A1361ContratoTermoAditivo_VlrUntUndCnt[0] )
               {
                  GXUtil.WriteLog("contratotermoaditivo:[seudo value changed for attri]"+"ContratoTermoAditivo_VlrUntUndCnt");
                  GXUtil.WriteLogRaw("Old: ",Z1361ContratoTermoAditivo_VlrUntUndCnt);
                  GXUtil.WriteLogRaw("Current: ",T001E2_A1361ContratoTermoAditivo_VlrUntUndCnt[0]);
               }
               if ( Z316ContratoTermoAditivo_DataInicio != T001E2_A316ContratoTermoAditivo_DataInicio[0] )
               {
                  GXUtil.WriteLog("contratotermoaditivo:[seudo value changed for attri]"+"ContratoTermoAditivo_DataInicio");
                  GXUtil.WriteLogRaw("Old: ",Z316ContratoTermoAditivo_DataInicio);
                  GXUtil.WriteLogRaw("Current: ",T001E2_A316ContratoTermoAditivo_DataInicio[0]);
               }
               if ( Z317ContratoTermoAditivo_DataFim != T001E2_A317ContratoTermoAditivo_DataFim[0] )
               {
                  GXUtil.WriteLog("contratotermoaditivo:[seudo value changed for attri]"+"ContratoTermoAditivo_DataFim");
                  GXUtil.WriteLogRaw("Old: ",Z317ContratoTermoAditivo_DataFim);
                  GXUtil.WriteLogRaw("Current: ",T001E2_A317ContratoTermoAditivo_DataFim[0]);
               }
               if ( Z318ContratoTermoAditivo_DataAssinatura != T001E2_A318ContratoTermoAditivo_DataAssinatura[0] )
               {
                  GXUtil.WriteLog("contratotermoaditivo:[seudo value changed for attri]"+"ContratoTermoAditivo_DataAssinatura");
                  GXUtil.WriteLogRaw("Old: ",Z318ContratoTermoAditivo_DataAssinatura);
                  GXUtil.WriteLogRaw("Current: ",T001E2_A318ContratoTermoAditivo_DataAssinatura[0]);
               }
               if ( Z1360ContratoTermoAditivo_VlrTtlPrvto != T001E2_A1360ContratoTermoAditivo_VlrTtlPrvto[0] )
               {
                  GXUtil.WriteLog("contratotermoaditivo:[seudo value changed for attri]"+"ContratoTermoAditivo_VlrTtlPrvto");
                  GXUtil.WriteLogRaw("Old: ",Z1360ContratoTermoAditivo_VlrTtlPrvto);
                  GXUtil.WriteLogRaw("Current: ",T001E2_A1360ContratoTermoAditivo_VlrTtlPrvto[0]);
               }
               if ( Z1362ContratoTermoAditivo_QtdContratada != T001E2_A1362ContratoTermoAditivo_QtdContratada[0] )
               {
                  GXUtil.WriteLog("contratotermoaditivo:[seudo value changed for attri]"+"ContratoTermoAditivo_QtdContratada");
                  GXUtil.WriteLogRaw("Old: ",Z1362ContratoTermoAditivo_QtdContratada);
                  GXUtil.WriteLogRaw("Current: ",T001E2_A1362ContratoTermoAditivo_QtdContratada[0]);
               }
               if ( Z1363ContratoTermoAditivo_DataPblDOU != T001E2_A1363ContratoTermoAditivo_DataPblDOU[0] )
               {
                  GXUtil.WriteLog("contratotermoaditivo:[seudo value changed for attri]"+"ContratoTermoAditivo_DataPblDOU");
                  GXUtil.WriteLogRaw("Old: ",Z1363ContratoTermoAditivo_DataPblDOU);
                  GXUtil.WriteLogRaw("Current: ",T001E2_A1363ContratoTermoAditivo_DataPblDOU[0]);
               }
               if ( Z74Contrato_Codigo != T001E2_A74Contrato_Codigo[0] )
               {
                  GXUtil.WriteLog("contratotermoaditivo:[seudo value changed for attri]"+"Contrato_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z74Contrato_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T001E2_A74Contrato_Codigo[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContratoTermoAditivo"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert1E53( )
      {
         BeforeValidate1E53( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1E53( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM1E53( 0) ;
            CheckOptimisticConcurrency1E53( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1E53( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert1E53( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001E14 */
                     pr_default.execute(12, new Object[] {n1361ContratoTermoAditivo_VlrUntUndCnt, A1361ContratoTermoAditivo_VlrUntUndCnt, n316ContratoTermoAditivo_DataInicio, A316ContratoTermoAditivo_DataInicio, n317ContratoTermoAditivo_DataFim, A317ContratoTermoAditivo_DataFim, n318ContratoTermoAditivo_DataAssinatura, A318ContratoTermoAditivo_DataAssinatura, n1360ContratoTermoAditivo_VlrTtlPrvto, A1360ContratoTermoAditivo_VlrTtlPrvto, n1362ContratoTermoAditivo_QtdContratada, A1362ContratoTermoAditivo_QtdContratada, n1363ContratoTermoAditivo_DataPblDOU, A1363ContratoTermoAditivo_DataPblDOU, n1364ContratoTermoAditivo_Observacao, A1364ContratoTermoAditivo_Observacao, A74Contrato_Codigo});
                     A315ContratoTermoAditivo_Codigo = T001E14_A315ContratoTermoAditivo_Codigo[0];
                     pr_default.close(12);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoTermoAditivo") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        new prc_saldocontratocriar(context ).execute(  A74Contrato_Codigo,  AV18SaldoContrato_UnidadeMedicao_Codigo,  A316ContratoTermoAditivo_DataInicio,  A317ContratoTermoAditivo_DataFim, out  AV16SaldoContrato_Codigo) ;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18SaldoContrato_UnidadeMedicao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18SaldoContrato_UnidadeMedicao_Codigo), 6, 0)));
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A316ContratoTermoAditivo_DataInicio", context.localUtil.Format(A316ContratoTermoAditivo_DataInicio, "99/99/99"));
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A317ContratoTermoAditivo_DataFim", context.localUtil.Format(A317ContratoTermoAditivo_DataFim, "99/99/99"));
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16SaldoContrato_Codigo), 6, 0)));
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption1E0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load1E53( ) ;
            }
            EndLevel1E53( ) ;
         }
         CloseExtendedTableCursors1E53( ) ;
      }

      protected void Update1E53( )
      {
         BeforeValidate1E53( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1E53( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1E53( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1E53( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate1E53( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001E15 */
                     pr_default.execute(13, new Object[] {n1361ContratoTermoAditivo_VlrUntUndCnt, A1361ContratoTermoAditivo_VlrUntUndCnt, n316ContratoTermoAditivo_DataInicio, A316ContratoTermoAditivo_DataInicio, n317ContratoTermoAditivo_DataFim, A317ContratoTermoAditivo_DataFim, n318ContratoTermoAditivo_DataAssinatura, A318ContratoTermoAditivo_DataAssinatura, n1360ContratoTermoAditivo_VlrTtlPrvto, A1360ContratoTermoAditivo_VlrTtlPrvto, n1362ContratoTermoAditivo_QtdContratada, A1362ContratoTermoAditivo_QtdContratada, n1363ContratoTermoAditivo_DataPblDOU, A1363ContratoTermoAditivo_DataPblDOU, n1364ContratoTermoAditivo_Observacao, A1364ContratoTermoAditivo_Observacao, A74Contrato_Codigo, A315ContratoTermoAditivo_Codigo});
                     pr_default.close(13);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoTermoAditivo") ;
                     if ( (pr_default.getStatus(13) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoTermoAditivo"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate1E53( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel1E53( ) ;
         }
         CloseExtendedTableCursors1E53( ) ;
      }

      protected void DeferredUpdate1E53( )
      {
      }

      protected void delete( )
      {
         BeforeValidate1E53( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1E53( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls1E53( ) ;
            AfterConfirm1E53( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete1E53( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T001E16 */
                  pr_default.execute(14, new Object[] {A315ContratoTermoAditivo_Codigo});
                  pr_default.close(14);
                  dsDefault.SmartCacheProvider.SetUpdated("ContratoTermoAditivo") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     new prc_saldocontratoexcluir(context ).execute(  A315ContratoTermoAditivo_Codigo) ;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A315ContratoTermoAditivo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A315ContratoTermoAditivo_Codigo), 6, 0)));
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode53 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel1E53( ) ;
         Gx_mode = sMode53;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls1E53( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && ! new prc_saldocontrato_verifica(context).executeUdp( ref  AV17Contrato_Codigo, ref  A316ContratoTermoAditivo_DataInicio) )
            {
               GX_msglist.addItem("Data de inicio da vig�ncia do termo aditivo deve ser maior que a data de t�rmino da vig�ncia atual do contrato", 1, "CONTRATOTERMOADITIVO_DATAINICIO");
               AnyError = 1;
               GX_FocusControl = edtContratoTermoAditivo_DataInicio_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            /* Using cursor T001E17 */
            pr_default.execute(15, new Object[] {A74Contrato_Codigo});
            A39Contratada_Codigo = T001E17_A39Contratada_Codigo[0];
            A77Contrato_Numero = T001E17_A77Contrato_Numero[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
            A78Contrato_NumeroAta = T001E17_A78Contrato_NumeroAta[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
            n78Contrato_NumeroAta = T001E17_n78Contrato_NumeroAta[0];
            A79Contrato_Ano = T001E17_A79Contrato_Ano[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A79Contrato_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0)));
            pr_default.close(15);
            /* Using cursor T001E18 */
            pr_default.execute(16, new Object[] {A39Contratada_Codigo});
            A40Contratada_PessoaCod = T001E18_A40Contratada_PessoaCod[0];
            pr_default.close(16);
            /* Using cursor T001E19 */
            pr_default.execute(17, new Object[] {A40Contratada_PessoaCod});
            A41Contratada_PessoaNom = T001E19_A41Contratada_PessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
            n41Contratada_PessoaNom = T001E19_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = T001E19_A42Contratada_PessoaCNPJ[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
            n42Contratada_PessoaCNPJ = T001E19_n42Contratada_PessoaCNPJ[0];
            pr_default.close(17);
         }
      }

      protected void EndLevel1E53( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete1E53( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(15);
            pr_default.close(16);
            pr_default.close(17);
            context.CommitDataStores( "ContratoTermoAditivo");
            if ( AnyError == 0 )
            {
               ConfirmValues1E0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(15);
            pr_default.close(16);
            pr_default.close(17);
            context.RollbackDataStores( "ContratoTermoAditivo");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart1E53( )
      {
         /* Scan By routine */
         /* Using cursor T001E20 */
         pr_default.execute(18);
         RcdFound53 = 0;
         if ( (pr_default.getStatus(18) != 101) )
         {
            RcdFound53 = 1;
            A315ContratoTermoAditivo_Codigo = T001E20_A315ContratoTermoAditivo_Codigo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext1E53( )
      {
         /* Scan next routine */
         pr_default.readNext(18);
         RcdFound53 = 0;
         if ( (pr_default.getStatus(18) != 101) )
         {
            RcdFound53 = 1;
            A315ContratoTermoAditivo_Codigo = T001E20_A315ContratoTermoAditivo_Codigo[0];
         }
      }

      protected void ScanEnd1E53( )
      {
         pr_default.close(18);
      }

      protected void AfterConfirm1E53( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert1E53( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate1E53( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete1E53( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete1E53( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate1E53( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes1E53( )
      {
         edtContrato_Numero_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Numero_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_Numero_Enabled), 5, 0)));
         edtContrato_NumeroAta_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_NumeroAta_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_NumeroAta_Enabled), 5, 0)));
         edtContrato_Ano_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Ano_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_Ano_Enabled), 5, 0)));
         edtContratada_PessoaNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_PessoaNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratada_PessoaNom_Enabled), 5, 0)));
         edtContratada_PessoaCNPJ_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_PessoaCNPJ_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratada_PessoaCNPJ_Enabled), 5, 0)));
         edtContratoTermoAditivo_DataInicio_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoTermoAditivo_DataInicio_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoTermoAditivo_DataInicio_Enabled), 5, 0)));
         edtContratoTermoAditivo_DataFim_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoTermoAditivo_DataFim_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoTermoAditivo_DataFim_Enabled), 5, 0)));
         edtContratoTermoAditivo_DataAssinatura_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoTermoAditivo_DataAssinatura_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoTermoAditivo_DataAssinatura_Enabled), 5, 0)));
         edtContratoTermoAditivo_DataPblDOU_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoTermoAditivo_DataPblDOU_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoTermoAditivo_DataPblDOU_Enabled), 5, 0)));
         edtContratoTermoAditivo_VlrTtlPrvto_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoTermoAditivo_VlrTtlPrvto_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoTermoAditivo_VlrTtlPrvto_Enabled), 5, 0)));
         edtContratoTermoAditivo_QtdContratada_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoTermoAditivo_QtdContratada_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoTermoAditivo_QtdContratada_Enabled), 5, 0)));
         dynavSaldocontrato_unidademedicao_codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSaldocontrato_unidademedicao_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavSaldocontrato_unidademedicao_codigo.Enabled), 5, 0)));
         edtContratoTermoAditivo_VlrUntUndCnt_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoTermoAditivo_VlrUntUndCnt_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoTermoAditivo_VlrUntUndCnt_Enabled), 5, 0)));
         edtContratoTermoAditivo_Observacao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoTermoAditivo_Observacao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoTermoAditivo_Observacao_Enabled), 5, 0)));
         edtavContrato_codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues1E0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117194968");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratotermoaditivo.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ContratoTermoAditivo_Codigo) + "," + UrlEncode("" +AV17Contrato_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z315ContratoTermoAditivo_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z315ContratoTermoAditivo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1361ContratoTermoAditivo_VlrUntUndCnt", StringUtil.LTrim( StringUtil.NToC( Z1361ContratoTermoAditivo_VlrUntUndCnt, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z316ContratoTermoAditivo_DataInicio", context.localUtil.DToC( Z316ContratoTermoAditivo_DataInicio, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z317ContratoTermoAditivo_DataFim", context.localUtil.DToC( Z317ContratoTermoAditivo_DataFim, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z318ContratoTermoAditivo_DataAssinatura", context.localUtil.DToC( Z318ContratoTermoAditivo_DataAssinatura, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z1360ContratoTermoAditivo_VlrTtlPrvto", StringUtil.LTrim( StringUtil.NToC( Z1360ContratoTermoAditivo_VlrTtlPrvto, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1362ContratoTermoAditivo_QtdContratada", StringUtil.LTrim( StringUtil.NToC( Z1362ContratoTermoAditivo_QtdContratada, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1363ContratoTermoAditivo_DataPblDOU", context.localUtil.DToC( Z1363ContratoTermoAditivo_DataPblDOU, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z74Contrato_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z74Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N74Contrato_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vCONTRATOTERMOADITIVO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContratoTermoAditivo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOTERMOADITIVO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A315ContratoTermoAditivo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATOTERMOADITIVO_VLRTTLPRVTO", StringUtil.LTrim( StringUtil.NToC( AV13ContratoTermoAditivo_VlrTtlPrvto, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATOTERMOADITIVO_QTDCONTRATADA", StringUtil.LTrim( StringUtil.NToC( AV14ContratoTermoAditivo_QtdContratada, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "vSALDOCONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16SaldoContrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_PESSOACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A40Contratada_PessoaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV20Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATOTERMOADITIVO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContratoTermoAditivo_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLE_Width", StringUtil.RTrim( Dvpanel_table_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLE_Cls", StringUtil.RTrim( Dvpanel_table_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLE_Title", StringUtil.RTrim( Dvpanel_table_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLE_Collapsible", StringUtil.BoolToStr( Dvpanel_table_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLE_Collapsed", StringUtil.BoolToStr( Dvpanel_table_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLE_Enabled", StringUtil.BoolToStr( Dvpanel_table_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLE_Autowidth", StringUtil.BoolToStr( Dvpanel_table_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLE_Autoheight", StringUtil.BoolToStr( Dvpanel_table_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLE_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_table_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLE_Iconposition", StringUtil.RTrim( Dvpanel_table_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLE_Autoscroll", StringUtil.BoolToStr( Dvpanel_table_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "ContratoTermoAditivo";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A315ContratoTermoAditivo_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contratotermoaditivo:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("contratotermoaditivo:[SendSecurityCheck value for]"+"ContratoTermoAditivo_Codigo:"+context.localUtil.Format( (decimal)(A315ContratoTermoAditivo_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("contratotermoaditivo:[SendSecurityCheck value for]"+"Contrato_Codigo:"+context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contratotermoaditivo.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ContratoTermoAditivo_Codigo) + "," + UrlEncode("" +AV17Contrato_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "ContratoTermoAditivo" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contrato Termo Aditivo" ;
      }

      protected void InitializeNonKey1E53( )
      {
         A39Contratada_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
         A74Contrato_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
         AV18SaldoContrato_UnidadeMedicao_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18SaldoContrato_UnidadeMedicao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18SaldoContrato_UnidadeMedicao_Codigo), 6, 0)));
         A1361ContratoTermoAditivo_VlrUntUndCnt = 0;
         n1361ContratoTermoAditivo_VlrUntUndCnt = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1361ContratoTermoAditivo_VlrUntUndCnt", StringUtil.LTrim( StringUtil.Str( A1361ContratoTermoAditivo_VlrUntUndCnt, 18, 5)));
         n1361ContratoTermoAditivo_VlrUntUndCnt = ((Convert.ToDecimal(0)==A1361ContratoTermoAditivo_VlrUntUndCnt) ? true : false);
         A77Contrato_Numero = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
         A78Contrato_NumeroAta = "";
         n78Contrato_NumeroAta = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
         A79Contrato_Ano = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A79Contrato_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0)));
         A40Contratada_PessoaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A40Contratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A40Contratada_PessoaCod), 6, 0)));
         A41Contratada_PessoaNom = "";
         n41Contratada_PessoaNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
         A42Contratada_PessoaCNPJ = "";
         n42Contratada_PessoaCNPJ = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
         A316ContratoTermoAditivo_DataInicio = DateTime.MinValue;
         n316ContratoTermoAditivo_DataInicio = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A316ContratoTermoAditivo_DataInicio", context.localUtil.Format(A316ContratoTermoAditivo_DataInicio, "99/99/99"));
         n316ContratoTermoAditivo_DataInicio = ((DateTime.MinValue==A316ContratoTermoAditivo_DataInicio) ? true : false);
         A317ContratoTermoAditivo_DataFim = DateTime.MinValue;
         n317ContratoTermoAditivo_DataFim = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A317ContratoTermoAditivo_DataFim", context.localUtil.Format(A317ContratoTermoAditivo_DataFim, "99/99/99"));
         n317ContratoTermoAditivo_DataFim = ((DateTime.MinValue==A317ContratoTermoAditivo_DataFim) ? true : false);
         A318ContratoTermoAditivo_DataAssinatura = DateTime.MinValue;
         n318ContratoTermoAditivo_DataAssinatura = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A318ContratoTermoAditivo_DataAssinatura", context.localUtil.Format(A318ContratoTermoAditivo_DataAssinatura, "99/99/99"));
         n318ContratoTermoAditivo_DataAssinatura = ((DateTime.MinValue==A318ContratoTermoAditivo_DataAssinatura) ? true : false);
         A1363ContratoTermoAditivo_DataPblDOU = DateTime.MinValue;
         n1363ContratoTermoAditivo_DataPblDOU = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1363ContratoTermoAditivo_DataPblDOU", context.localUtil.Format(A1363ContratoTermoAditivo_DataPblDOU, "99/99/99"));
         n1363ContratoTermoAditivo_DataPblDOU = ((DateTime.MinValue==A1363ContratoTermoAditivo_DataPblDOU) ? true : false);
         A1364ContratoTermoAditivo_Observacao = "";
         n1364ContratoTermoAditivo_Observacao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1364ContratoTermoAditivo_Observacao", A1364ContratoTermoAditivo_Observacao);
         n1364ContratoTermoAditivo_Observacao = (String.IsNullOrEmpty(StringUtil.RTrim( A1364ContratoTermoAditivo_Observacao)) ? true : false);
         AV16SaldoContrato_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16SaldoContrato_Codigo), 6, 0)));
         A1360ContratoTermoAditivo_VlrTtlPrvto = AV13ContratoTermoAditivo_VlrTtlPrvto;
         n1360ContratoTermoAditivo_VlrTtlPrvto = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1360ContratoTermoAditivo_VlrTtlPrvto", StringUtil.LTrim( StringUtil.Str( A1360ContratoTermoAditivo_VlrTtlPrvto, 18, 5)));
         A1362ContratoTermoAditivo_QtdContratada = AV14ContratoTermoAditivo_QtdContratada;
         n1362ContratoTermoAditivo_QtdContratada = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1362ContratoTermoAditivo_QtdContratada", StringUtil.LTrim( StringUtil.Str( A1362ContratoTermoAditivo_QtdContratada, 14, 5)));
         Z1361ContratoTermoAditivo_VlrUntUndCnt = 0;
         Z316ContratoTermoAditivo_DataInicio = DateTime.MinValue;
         Z317ContratoTermoAditivo_DataFim = DateTime.MinValue;
         Z318ContratoTermoAditivo_DataAssinatura = DateTime.MinValue;
         Z1360ContratoTermoAditivo_VlrTtlPrvto = 0;
         Z1362ContratoTermoAditivo_QtdContratada = 0;
         Z1363ContratoTermoAditivo_DataPblDOU = DateTime.MinValue;
         Z74Contrato_Codigo = 0;
      }

      protected void InitAll1E53( )
      {
         A315ContratoTermoAditivo_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A315ContratoTermoAditivo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A315ContratoTermoAditivo_Codigo), 6, 0)));
         InitializeNonKey1E53( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A1360ContratoTermoAditivo_VlrTtlPrvto = i1360ContratoTermoAditivo_VlrTtlPrvto;
         n1360ContratoTermoAditivo_VlrTtlPrvto = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1360ContratoTermoAditivo_VlrTtlPrvto", StringUtil.LTrim( StringUtil.Str( A1360ContratoTermoAditivo_VlrTtlPrvto, 18, 5)));
         A1362ContratoTermoAditivo_QtdContratada = i1362ContratoTermoAditivo_QtdContratada;
         n1362ContratoTermoAditivo_QtdContratada = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1362ContratoTermoAditivo_QtdContratada", StringUtil.LTrim( StringUtil.Str( A1362ContratoTermoAditivo_QtdContratada, 14, 5)));
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311719507");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contratotermoaditivo.js", "?2020311719507");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontrato_numero_Internalname = "TEXTBLOCKCONTRATO_NUMERO";
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO";
         lblTextblockcontrato_numeroata_Internalname = "TEXTBLOCKCONTRATO_NUMEROATA";
         edtContrato_NumeroAta_Internalname = "CONTRATO_NUMEROATA";
         lblTextblockcontrato_ano_Internalname = "TEXTBLOCKCONTRATO_ANO";
         edtContrato_Ano_Internalname = "CONTRATO_ANO";
         tblTablemergedcontrato_numero_Internalname = "TABLEMERGEDCONTRATO_NUMERO";
         lblTextblockcontratada_pessoanom_Internalname = "TEXTBLOCKCONTRATADA_PESSOANOM";
         edtContratada_PessoaNom_Internalname = "CONTRATADA_PESSOANOM";
         lblTextblockcontratada_pessoacnpj_Internalname = "TEXTBLOCKCONTRATADA_PESSOACNPJ";
         edtContratada_PessoaCNPJ_Internalname = "CONTRATADA_PESSOACNPJ";
         tblTablemergedcontratada_pessoanom_Internalname = "TABLEMERGEDCONTRATADA_PESSOANOM";
         tblContrato_Internalname = "CONTRATO";
         grpUnnamedgroup1_Internalname = "UNNAMEDGROUP1";
         lblTextblockcontratotermoaditivo_datainicio_Internalname = "TEXTBLOCKCONTRATOTERMOADITIVO_DATAINICIO";
         edtContratoTermoAditivo_DataInicio_Internalname = "CONTRATOTERMOADITIVO_DATAINICIO";
         lblTextblockcontratotermoaditivo_datafim_Internalname = "TEXTBLOCKCONTRATOTERMOADITIVO_DATAFIM";
         edtContratoTermoAditivo_DataFim_Internalname = "CONTRATOTERMOADITIVO_DATAFIM";
         lblTextblockcontratotermoaditivo_dataassinatura_Internalname = "TEXTBLOCKCONTRATOTERMOADITIVO_DATAASSINATURA";
         edtContratoTermoAditivo_DataAssinatura_Internalname = "CONTRATOTERMOADITIVO_DATAASSINATURA";
         lblTextblockcontratotermoaditivo_datapbldou_Internalname = "TEXTBLOCKCONTRATOTERMOADITIVO_DATAPBLDOU";
         edtContratoTermoAditivo_DataPblDOU_Internalname = "CONTRATOTERMOADITIVO_DATAPBLDOU";
         lblTextblockcontratotermoaditivo_vlrttlprvto_Internalname = "TEXTBLOCKCONTRATOTERMOADITIVO_VLRTTLPRVTO";
         edtContratoTermoAditivo_VlrTtlPrvto_Internalname = "CONTRATOTERMOADITIVO_VLRTTLPRVTO";
         lblTextblockcontratotermoaditivo_qtdcontratada_Internalname = "TEXTBLOCKCONTRATOTERMOADITIVO_QTDCONTRATADA";
         edtContratoTermoAditivo_QtdContratada_Internalname = "CONTRATOTERMOADITIVO_QTDCONTRATADA";
         lblTextblocksaldocontrato_unidademedicao_codigo_Internalname = "TEXTBLOCKSALDOCONTRATO_UNIDADEMEDICAO_CODIGO";
         dynavSaldocontrato_unidademedicao_codigo_Internalname = "vSALDOCONTRATO_UNIDADEMEDICAO_CODIGO";
         lblTextblockcontratotermoaditivo_vlruntundcnt_Internalname = "TEXTBLOCKCONTRATOTERMOADITIVO_VLRUNTUNDCNT";
         edtContratoTermoAditivo_VlrUntUndCnt_Internalname = "CONTRATOTERMOADITIVO_VLRUNTUNDCNT";
         lblTextblockcontratotermoaditivo_observacao_Internalname = "TEXTBLOCKCONTRATOTERMOADITIVO_OBSERVACAO";
         edtContratoTermoAditivo_Observacao_Internalname = "CONTRATOTERMOADITIVO_OBSERVACAO";
         tblTable_Internalname = "TABLE";
         Dvpanel_table_Internalname = "DVPANEL_TABLE";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtavContrato_codigo_Internalname = "vCONTRATO_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_table_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_table_Iconposition = "left";
         Dvpanel_table_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_table_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_table_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_table_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_table_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_table_Title = "Termo Aditivo";
         Dvpanel_table_Cls = "GXUI-DVelop-Panel";
         Dvpanel_table_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Contrato Termo Aditivo";
         edtContrato_Ano_Jsonclick = "";
         edtContrato_Ano_Enabled = 0;
         edtContrato_NumeroAta_Jsonclick = "";
         edtContrato_NumeroAta_Enabled = 0;
         edtContrato_Numero_Jsonclick = "";
         edtContrato_Numero_Enabled = 0;
         edtContratada_PessoaCNPJ_Jsonclick = "";
         edtContratada_PessoaCNPJ_Enabled = 0;
         edtContratada_PessoaNom_Jsonclick = "";
         edtContratada_PessoaNom_Enabled = 0;
         edtContratoTermoAditivo_Observacao_Enabled = 1;
         edtContratoTermoAditivo_VlrUntUndCnt_Jsonclick = "";
         edtContratoTermoAditivo_VlrUntUndCnt_Enabled = 1;
         dynavSaldocontrato_unidademedicao_codigo_Jsonclick = "";
         dynavSaldocontrato_unidademedicao_codigo.Enabled = 1;
         edtContratoTermoAditivo_QtdContratada_Jsonclick = "";
         edtContratoTermoAditivo_QtdContratada_Enabled = 1;
         edtContratoTermoAditivo_VlrTtlPrvto_Jsonclick = "";
         edtContratoTermoAditivo_VlrTtlPrvto_Enabled = 1;
         edtContratoTermoAditivo_DataPblDOU_Jsonclick = "";
         edtContratoTermoAditivo_DataPblDOU_Enabled = 1;
         edtContratoTermoAditivo_DataAssinatura_Jsonclick = "";
         edtContratoTermoAditivo_DataAssinatura_Enabled = 1;
         edtContratoTermoAditivo_DataFim_Jsonclick = "";
         edtContratoTermoAditivo_DataFim_Enabled = 1;
         edtContratoTermoAditivo_DataInicio_Jsonclick = "";
         edtContratoTermoAditivo_DataInicio_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtavContrato_codigo_Jsonclick = "";
         edtavContrato_codigo_Enabled = 0;
         edtavContrato_codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDSVvSALDOCONTRATO_UNIDADEMEDICAO_CODIGO1E53( int AV17Contrato_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDSVvSALDOCONTRATO_UNIDADEMEDICAO_CODIGO_data1E53( AV17Contrato_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvSALDOCONTRATO_UNIDADEMEDICAO_CODIGO_html1E53( int AV17Contrato_Codigo )
      {
         int gxdynajaxvalue ;
         GXDSVvSALDOCONTRATO_UNIDADEMEDICAO_CODIGO_data1E53( AV17Contrato_Codigo) ;
         gxdynajaxindex = 1;
         dynavSaldocontrato_unidademedicao_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavSaldocontrato_unidademedicao_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDSVvSALDOCONTRATO_UNIDADEMEDICAO_CODIGO_data1E53( int AV17Contrato_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhuma)");
         IGxCollection gxcolvSALDOCONTRATO_UNIDADEMEDICAO_CODIGO ;
         SdtSDT_ContratoServicoUnidadeMedicao gxcolitemvSALDOCONTRATO_UNIDADEMEDICAO_CODIGO ;
         new dp_contratoservicounidademedicao(context ).execute(  AV17Contrato_Codigo, out  gxcolvSALDOCONTRATO_UNIDADEMEDICAO_CODIGO) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Contrato_Codigo), 6, 0)));
         gxcolvSALDOCONTRATO_UNIDADEMEDICAO_CODIGO.Sort("Contratoservicos_unidadecontratadadescricao");
         int gxindex = 1 ;
         while ( gxindex <= gxcolvSALDOCONTRATO_UNIDADEMEDICAO_CODIGO.Count )
         {
            gxcolitemvSALDOCONTRATO_UNIDADEMEDICAO_CODIGO = ((SdtSDT_ContratoServicoUnidadeMedicao)gxcolvSALDOCONTRATO_UNIDADEMEDICAO_CODIGO.Item(gxindex));
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.Str( (decimal)(gxcolitemvSALDOCONTRATO_UNIDADEMEDICAO_CODIGO.gxTpr_Contratoservicos_unidadecontratada), 6, 0)));
            gxdynajaxctrldescr.Add(gxcolitemvSALDOCONTRATO_UNIDADEMEDICAO_CODIGO.gxTpr_Contratoservicos_unidadecontratadadescricao);
            gxindex = (int)(gxindex+1);
         }
      }

      protected void XC_18_1E53( int A74Contrato_Codigo ,
                                 int AV18SaldoContrato_UnidadeMedicao_Codigo ,
                                 DateTime A316ContratoTermoAditivo_DataInicio ,
                                 DateTime A317ContratoTermoAditivo_DataFim )
      {
         new prc_saldocontratocriar(context ).execute(  A74Contrato_Codigo,  AV18SaldoContrato_UnidadeMedicao_Codigo,  A316ContratoTermoAditivo_DataInicio,  A317ContratoTermoAditivo_DataFim, out  AV16SaldoContrato_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18SaldoContrato_UnidadeMedicao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18SaldoContrato_UnidadeMedicao_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A316ContratoTermoAditivo_DataInicio", context.localUtil.Format(A316ContratoTermoAditivo_DataInicio, "99/99/99"));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A317ContratoTermoAditivo_DataFim", context.localUtil.Format(A317ContratoTermoAditivo_DataFim, "99/99/99"));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16SaldoContrato_Codigo), 6, 0)));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16SaldoContrato_Codigo), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_19_1E53( int A315ContratoTermoAditivo_Codigo )
      {
         new prc_saldocontratoexcluir(context ).execute(  A315ContratoTermoAditivo_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A315ContratoTermoAditivo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A315ContratoTermoAditivo_Codigo), 6, 0)));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      public void Valid_Contratotermoaditivo_datainicio( String GX_Parm1 ,
                                                         DateTime GX_Parm2 ,
                                                         int GX_Parm3 )
      {
         Gx_mode = GX_Parm1;
         A316ContratoTermoAditivo_DataInicio = GX_Parm2;
         n316ContratoTermoAditivo_DataInicio = false;
         AV17Contrato_Codigo = GX_Parm3;
         if ( ! ( (DateTime.MinValue==A316ContratoTermoAditivo_DataInicio) || ( A316ContratoTermoAditivo_DataInicio >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Data de inicio da Vig�ncia fora do intervalo", "OutOfRange", 1, "CONTRATOTERMOADITIVO_DATAINICIO");
            AnyError = 1;
            GX_FocusControl = edtContratoTermoAditivo_DataInicio_Internalname;
         }
         if ( (DateTime.MinValue==A316ContratoTermoAditivo_DataInicio) )
         {
            GX_msglist.addItem("Data de inicio da Vig�ncia � obrigat�rio.", 1, "CONTRATOTERMOADITIVO_DATAINICIO");
            AnyError = 1;
            GX_FocusControl = edtContratoTermoAditivo_DataInicio_Internalname;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && ! new prc_saldocontrato_verifica(context).executeUdp( ref  AV17Contrato_Codigo, ref  A316ContratoTermoAditivo_DataInicio) )
         {
            GX_msglist.addItem("Data de inicio da vig�ncia do termo aditivo deve ser maior que a data de t�rmino da vig�ncia atual do contrato", 1, "CONTRATOTERMOADITIVO_DATAINICIO");
            AnyError = 1;
            GX_FocusControl = edtContratoTermoAditivo_DataInicio_Internalname;
         }
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Validv_Contrato_codigo( int GX_Parm1 ,
                                          GXCombobox dynGX_Parm2 )
      {
         AV17Contrato_Codigo = GX_Parm1;
         dynavSaldocontrato_unidademedicao_codigo = dynGX_Parm2;
         AV18SaldoContrato_UnidadeMedicao_Codigo = (int)(NumberUtil.Val( dynavSaldocontrato_unidademedicao_codigo.CurrentValue, "."));
         GXVvSALDOCONTRATO_UNIDADEMEDICAO_CODIGO_html1E53( AV17Contrato_Codigo) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         dynavSaldocontrato_unidademedicao_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV18SaldoContrato_UnidadeMedicao_Codigo), 6, 0));
         if ( dynavSaldocontrato_unidademedicao_codigo.ItemCount > 0 )
         {
            AV18SaldoContrato_UnidadeMedicao_Codigo = (int)(NumberUtil.Val( dynavSaldocontrato_unidademedicao_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV18SaldoContrato_UnidadeMedicao_Codigo), 6, 0))), "."));
         }
         dynavSaldocontrato_unidademedicao_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV18SaldoContrato_UnidadeMedicao_Codigo), 6, 0));
         isValidOutput.Add(dynavSaldocontrato_unidademedicao_codigo);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7ContratoTermoAditivo_Codigo',fld:'vCONTRATOTERMOADITIVO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV17Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E121E2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(15);
         pr_default.close(16);
         pr_default.close(17);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z316ContratoTermoAditivo_DataInicio = DateTime.MinValue;
         Z317ContratoTermoAditivo_DataFim = DateTime.MinValue;
         Z318ContratoTermoAditivo_DataAssinatura = DateTime.MinValue;
         Z1363ContratoTermoAditivo_DataPblDOU = DateTime.MinValue;
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         A316ContratoTermoAditivo_DataInicio = DateTime.MinValue;
         A317ContratoTermoAditivo_DataFim = DateTime.MinValue;
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockcontratotermoaditivo_datainicio_Jsonclick = "";
         lblTextblockcontratotermoaditivo_datafim_Jsonclick = "";
         lblTextblockcontratotermoaditivo_dataassinatura_Jsonclick = "";
         A318ContratoTermoAditivo_DataAssinatura = DateTime.MinValue;
         lblTextblockcontratotermoaditivo_datapbldou_Jsonclick = "";
         A1363ContratoTermoAditivo_DataPblDOU = DateTime.MinValue;
         lblTextblockcontratotermoaditivo_vlrttlprvto_Jsonclick = "";
         lblTextblockcontratotermoaditivo_qtdcontratada_Jsonclick = "";
         lblTextblocksaldocontrato_unidademedicao_codigo_Jsonclick = "";
         lblTextblockcontratotermoaditivo_vlruntundcnt_Jsonclick = "";
         lblTextblockcontratotermoaditivo_observacao_Jsonclick = "";
         A1364ContratoTermoAditivo_Observacao = "";
         lblTextblockcontrato_numero_Jsonclick = "";
         lblTextblockcontratada_pessoanom_Jsonclick = "";
         A41Contratada_PessoaNom = "";
         lblTextblockcontratada_pessoacnpj_Jsonclick = "";
         A42Contratada_PessoaCNPJ = "";
         A77Contrato_Numero = "";
         lblTextblockcontrato_numeroata_Jsonclick = "";
         A78Contrato_NumeroAta = "";
         lblTextblockcontrato_ano_Jsonclick = "";
         AV20Pgmname = "";
         Dvpanel_table_Height = "";
         Dvpanel_table_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode53 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z1364ContratoTermoAditivo_Observacao = "";
         Z77Contrato_Numero = "";
         Z78Contrato_NumeroAta = "";
         Z41Contratada_PessoaNom = "";
         Z42Contratada_PessoaCNPJ = "";
         T001E4_A39Contratada_Codigo = new int[1] ;
         T001E4_A77Contrato_Numero = new String[] {""} ;
         T001E4_A78Contrato_NumeroAta = new String[] {""} ;
         T001E4_n78Contrato_NumeroAta = new bool[] {false} ;
         T001E4_A79Contrato_Ano = new short[1] ;
         T001E5_A40Contratada_PessoaCod = new int[1] ;
         T001E6_A41Contratada_PessoaNom = new String[] {""} ;
         T001E6_n41Contratada_PessoaNom = new bool[] {false} ;
         T001E6_A42Contratada_PessoaCNPJ = new String[] {""} ;
         T001E6_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         T001E7_A39Contratada_Codigo = new int[1] ;
         T001E7_A315ContratoTermoAditivo_Codigo = new int[1] ;
         T001E7_A1361ContratoTermoAditivo_VlrUntUndCnt = new decimal[1] ;
         T001E7_n1361ContratoTermoAditivo_VlrUntUndCnt = new bool[] {false} ;
         T001E7_A77Contrato_Numero = new String[] {""} ;
         T001E7_A78Contrato_NumeroAta = new String[] {""} ;
         T001E7_n78Contrato_NumeroAta = new bool[] {false} ;
         T001E7_A79Contrato_Ano = new short[1] ;
         T001E7_A41Contratada_PessoaNom = new String[] {""} ;
         T001E7_n41Contratada_PessoaNom = new bool[] {false} ;
         T001E7_A42Contratada_PessoaCNPJ = new String[] {""} ;
         T001E7_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         T001E7_A316ContratoTermoAditivo_DataInicio = new DateTime[] {DateTime.MinValue} ;
         T001E7_n316ContratoTermoAditivo_DataInicio = new bool[] {false} ;
         T001E7_A317ContratoTermoAditivo_DataFim = new DateTime[] {DateTime.MinValue} ;
         T001E7_n317ContratoTermoAditivo_DataFim = new bool[] {false} ;
         T001E7_A318ContratoTermoAditivo_DataAssinatura = new DateTime[] {DateTime.MinValue} ;
         T001E7_n318ContratoTermoAditivo_DataAssinatura = new bool[] {false} ;
         T001E7_A1360ContratoTermoAditivo_VlrTtlPrvto = new decimal[1] ;
         T001E7_n1360ContratoTermoAditivo_VlrTtlPrvto = new bool[] {false} ;
         T001E7_A1362ContratoTermoAditivo_QtdContratada = new decimal[1] ;
         T001E7_n1362ContratoTermoAditivo_QtdContratada = new bool[] {false} ;
         T001E7_A1363ContratoTermoAditivo_DataPblDOU = new DateTime[] {DateTime.MinValue} ;
         T001E7_n1363ContratoTermoAditivo_DataPblDOU = new bool[] {false} ;
         T001E7_A1364ContratoTermoAditivo_Observacao = new String[] {""} ;
         T001E7_n1364ContratoTermoAditivo_Observacao = new bool[] {false} ;
         T001E7_A74Contrato_Codigo = new int[1] ;
         T001E7_A40Contratada_PessoaCod = new int[1] ;
         T001E8_A39Contratada_Codigo = new int[1] ;
         T001E8_A77Contrato_Numero = new String[] {""} ;
         T001E8_A78Contrato_NumeroAta = new String[] {""} ;
         T001E8_n78Contrato_NumeroAta = new bool[] {false} ;
         T001E8_A79Contrato_Ano = new short[1] ;
         T001E9_A40Contratada_PessoaCod = new int[1] ;
         T001E10_A41Contratada_PessoaNom = new String[] {""} ;
         T001E10_n41Contratada_PessoaNom = new bool[] {false} ;
         T001E10_A42Contratada_PessoaCNPJ = new String[] {""} ;
         T001E10_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         T001E11_A315ContratoTermoAditivo_Codigo = new int[1] ;
         T001E3_A315ContratoTermoAditivo_Codigo = new int[1] ;
         T001E3_A1361ContratoTermoAditivo_VlrUntUndCnt = new decimal[1] ;
         T001E3_n1361ContratoTermoAditivo_VlrUntUndCnt = new bool[] {false} ;
         T001E3_A316ContratoTermoAditivo_DataInicio = new DateTime[] {DateTime.MinValue} ;
         T001E3_n316ContratoTermoAditivo_DataInicio = new bool[] {false} ;
         T001E3_A317ContratoTermoAditivo_DataFim = new DateTime[] {DateTime.MinValue} ;
         T001E3_n317ContratoTermoAditivo_DataFim = new bool[] {false} ;
         T001E3_A318ContratoTermoAditivo_DataAssinatura = new DateTime[] {DateTime.MinValue} ;
         T001E3_n318ContratoTermoAditivo_DataAssinatura = new bool[] {false} ;
         T001E3_A1360ContratoTermoAditivo_VlrTtlPrvto = new decimal[1] ;
         T001E3_n1360ContratoTermoAditivo_VlrTtlPrvto = new bool[] {false} ;
         T001E3_A1362ContratoTermoAditivo_QtdContratada = new decimal[1] ;
         T001E3_n1362ContratoTermoAditivo_QtdContratada = new bool[] {false} ;
         T001E3_A1363ContratoTermoAditivo_DataPblDOU = new DateTime[] {DateTime.MinValue} ;
         T001E3_n1363ContratoTermoAditivo_DataPblDOU = new bool[] {false} ;
         T001E3_A1364ContratoTermoAditivo_Observacao = new String[] {""} ;
         T001E3_n1364ContratoTermoAditivo_Observacao = new bool[] {false} ;
         T001E3_A74Contrato_Codigo = new int[1] ;
         T001E12_A315ContratoTermoAditivo_Codigo = new int[1] ;
         T001E13_A315ContratoTermoAditivo_Codigo = new int[1] ;
         T001E2_A315ContratoTermoAditivo_Codigo = new int[1] ;
         T001E2_A1361ContratoTermoAditivo_VlrUntUndCnt = new decimal[1] ;
         T001E2_n1361ContratoTermoAditivo_VlrUntUndCnt = new bool[] {false} ;
         T001E2_A316ContratoTermoAditivo_DataInicio = new DateTime[] {DateTime.MinValue} ;
         T001E2_n316ContratoTermoAditivo_DataInicio = new bool[] {false} ;
         T001E2_A317ContratoTermoAditivo_DataFim = new DateTime[] {DateTime.MinValue} ;
         T001E2_n317ContratoTermoAditivo_DataFim = new bool[] {false} ;
         T001E2_A318ContratoTermoAditivo_DataAssinatura = new DateTime[] {DateTime.MinValue} ;
         T001E2_n318ContratoTermoAditivo_DataAssinatura = new bool[] {false} ;
         T001E2_A1360ContratoTermoAditivo_VlrTtlPrvto = new decimal[1] ;
         T001E2_n1360ContratoTermoAditivo_VlrTtlPrvto = new bool[] {false} ;
         T001E2_A1362ContratoTermoAditivo_QtdContratada = new decimal[1] ;
         T001E2_n1362ContratoTermoAditivo_QtdContratada = new bool[] {false} ;
         T001E2_A1363ContratoTermoAditivo_DataPblDOU = new DateTime[] {DateTime.MinValue} ;
         T001E2_n1363ContratoTermoAditivo_DataPblDOU = new bool[] {false} ;
         T001E2_A1364ContratoTermoAditivo_Observacao = new String[] {""} ;
         T001E2_n1364ContratoTermoAditivo_Observacao = new bool[] {false} ;
         T001E2_A74Contrato_Codigo = new int[1] ;
         T001E14_A315ContratoTermoAditivo_Codigo = new int[1] ;
         T001E17_A39Contratada_Codigo = new int[1] ;
         T001E17_A77Contrato_Numero = new String[] {""} ;
         T001E17_A78Contrato_NumeroAta = new String[] {""} ;
         T001E17_n78Contrato_NumeroAta = new bool[] {false} ;
         T001E17_A79Contrato_Ano = new short[1] ;
         T001E18_A40Contratada_PessoaCod = new int[1] ;
         T001E19_A41Contratada_PessoaNom = new String[] {""} ;
         T001E19_n41Contratada_PessoaNom = new bool[] {false} ;
         T001E19_A42Contratada_PessoaCNPJ = new String[] {""} ;
         T001E19_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         T001E20_A315ContratoTermoAditivo_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratotermoaditivo__default(),
            new Object[][] {
                new Object[] {
               T001E2_A315ContratoTermoAditivo_Codigo, T001E2_A1361ContratoTermoAditivo_VlrUntUndCnt, T001E2_n1361ContratoTermoAditivo_VlrUntUndCnt, T001E2_A316ContratoTermoAditivo_DataInicio, T001E2_n316ContratoTermoAditivo_DataInicio, T001E2_A317ContratoTermoAditivo_DataFim, T001E2_n317ContratoTermoAditivo_DataFim, T001E2_A318ContratoTermoAditivo_DataAssinatura, T001E2_n318ContratoTermoAditivo_DataAssinatura, T001E2_A1360ContratoTermoAditivo_VlrTtlPrvto,
               T001E2_n1360ContratoTermoAditivo_VlrTtlPrvto, T001E2_A1362ContratoTermoAditivo_QtdContratada, T001E2_n1362ContratoTermoAditivo_QtdContratada, T001E2_A1363ContratoTermoAditivo_DataPblDOU, T001E2_n1363ContratoTermoAditivo_DataPblDOU, T001E2_A1364ContratoTermoAditivo_Observacao, T001E2_n1364ContratoTermoAditivo_Observacao, T001E2_A74Contrato_Codigo
               }
               , new Object[] {
               T001E3_A315ContratoTermoAditivo_Codigo, T001E3_A1361ContratoTermoAditivo_VlrUntUndCnt, T001E3_n1361ContratoTermoAditivo_VlrUntUndCnt, T001E3_A316ContratoTermoAditivo_DataInicio, T001E3_n316ContratoTermoAditivo_DataInicio, T001E3_A317ContratoTermoAditivo_DataFim, T001E3_n317ContratoTermoAditivo_DataFim, T001E3_A318ContratoTermoAditivo_DataAssinatura, T001E3_n318ContratoTermoAditivo_DataAssinatura, T001E3_A1360ContratoTermoAditivo_VlrTtlPrvto,
               T001E3_n1360ContratoTermoAditivo_VlrTtlPrvto, T001E3_A1362ContratoTermoAditivo_QtdContratada, T001E3_n1362ContratoTermoAditivo_QtdContratada, T001E3_A1363ContratoTermoAditivo_DataPblDOU, T001E3_n1363ContratoTermoAditivo_DataPblDOU, T001E3_A1364ContratoTermoAditivo_Observacao, T001E3_n1364ContratoTermoAditivo_Observacao, T001E3_A74Contrato_Codigo
               }
               , new Object[] {
               T001E4_A39Contratada_Codigo, T001E4_A77Contrato_Numero, T001E4_A78Contrato_NumeroAta, T001E4_n78Contrato_NumeroAta, T001E4_A79Contrato_Ano
               }
               , new Object[] {
               T001E5_A40Contratada_PessoaCod
               }
               , new Object[] {
               T001E6_A41Contratada_PessoaNom, T001E6_n41Contratada_PessoaNom, T001E6_A42Contratada_PessoaCNPJ, T001E6_n42Contratada_PessoaCNPJ
               }
               , new Object[] {
               T001E7_A39Contratada_Codigo, T001E7_A315ContratoTermoAditivo_Codigo, T001E7_A1361ContratoTermoAditivo_VlrUntUndCnt, T001E7_n1361ContratoTermoAditivo_VlrUntUndCnt, T001E7_A77Contrato_Numero, T001E7_A78Contrato_NumeroAta, T001E7_n78Contrato_NumeroAta, T001E7_A79Contrato_Ano, T001E7_A41Contratada_PessoaNom, T001E7_n41Contratada_PessoaNom,
               T001E7_A42Contratada_PessoaCNPJ, T001E7_n42Contratada_PessoaCNPJ, T001E7_A316ContratoTermoAditivo_DataInicio, T001E7_n316ContratoTermoAditivo_DataInicio, T001E7_A317ContratoTermoAditivo_DataFim, T001E7_n317ContratoTermoAditivo_DataFim, T001E7_A318ContratoTermoAditivo_DataAssinatura, T001E7_n318ContratoTermoAditivo_DataAssinatura, T001E7_A1360ContratoTermoAditivo_VlrTtlPrvto, T001E7_n1360ContratoTermoAditivo_VlrTtlPrvto,
               T001E7_A1362ContratoTermoAditivo_QtdContratada, T001E7_n1362ContratoTermoAditivo_QtdContratada, T001E7_A1363ContratoTermoAditivo_DataPblDOU, T001E7_n1363ContratoTermoAditivo_DataPblDOU, T001E7_A1364ContratoTermoAditivo_Observacao, T001E7_n1364ContratoTermoAditivo_Observacao, T001E7_A74Contrato_Codigo, T001E7_A40Contratada_PessoaCod
               }
               , new Object[] {
               T001E8_A39Contratada_Codigo, T001E8_A77Contrato_Numero, T001E8_A78Contrato_NumeroAta, T001E8_n78Contrato_NumeroAta, T001E8_A79Contrato_Ano
               }
               , new Object[] {
               T001E9_A40Contratada_PessoaCod
               }
               , new Object[] {
               T001E10_A41Contratada_PessoaNom, T001E10_n41Contratada_PessoaNom, T001E10_A42Contratada_PessoaCNPJ, T001E10_n42Contratada_PessoaCNPJ
               }
               , new Object[] {
               T001E11_A315ContratoTermoAditivo_Codigo
               }
               , new Object[] {
               T001E12_A315ContratoTermoAditivo_Codigo
               }
               , new Object[] {
               T001E13_A315ContratoTermoAditivo_Codigo
               }
               , new Object[] {
               T001E14_A315ContratoTermoAditivo_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T001E17_A39Contratada_Codigo, T001E17_A77Contrato_Numero, T001E17_A78Contrato_NumeroAta, T001E17_n78Contrato_NumeroAta, T001E17_A79Contrato_Ano
               }
               , new Object[] {
               T001E18_A40Contratada_PessoaCod
               }
               , new Object[] {
               T001E19_A41Contratada_PessoaNom, T001E19_n41Contratada_PessoaNom, T001E19_A42Contratada_PessoaCNPJ, T001E19_n42Contratada_PessoaCNPJ
               }
               , new Object[] {
               T001E20_A315ContratoTermoAditivo_Codigo
               }
            }
         );
         AV20Pgmname = "ContratoTermoAditivo";
         Z1362ContratoTermoAditivo_QtdContratada = 0;
         n1362ContratoTermoAditivo_QtdContratada = false;
         A1362ContratoTermoAditivo_QtdContratada = 0;
         n1362ContratoTermoAditivo_QtdContratada = false;
         i1362ContratoTermoAditivo_QtdContratada = 0;
         n1362ContratoTermoAditivo_QtdContratada = false;
         Z1360ContratoTermoAditivo_VlrTtlPrvto = 0;
         n1360ContratoTermoAditivo_VlrTtlPrvto = false;
         A1360ContratoTermoAditivo_VlrTtlPrvto = 0;
         n1360ContratoTermoAditivo_VlrTtlPrvto = false;
         i1360ContratoTermoAditivo_VlrTtlPrvto = 0;
         n1360ContratoTermoAditivo_VlrTtlPrvto = false;
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A79Contrato_Ano ;
      private short Gx_BScreen ;
      private short RcdFound53 ;
      private short GX_JID ;
      private short Z79Contrato_Ano ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7ContratoTermoAditivo_Codigo ;
      private int wcpOAV17Contrato_Codigo ;
      private int Z315ContratoTermoAditivo_Codigo ;
      private int Z74Contrato_Codigo ;
      private int N74Contrato_Codigo ;
      private int A74Contrato_Codigo ;
      private int AV18SaldoContrato_UnidadeMedicao_Codigo ;
      private int A315ContratoTermoAditivo_Codigo ;
      private int AV17Contrato_Codigo ;
      private int A39Contratada_Codigo ;
      private int A40Contratada_PessoaCod ;
      private int AV7ContratoTermoAditivo_Codigo ;
      private int trnEnded ;
      private int edtavContrato_codigo_Enabled ;
      private int edtavContrato_codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtContratoTermoAditivo_DataInicio_Enabled ;
      private int edtContratoTermoAditivo_DataFim_Enabled ;
      private int edtContratoTermoAditivo_DataAssinatura_Enabled ;
      private int edtContratoTermoAditivo_DataPblDOU_Enabled ;
      private int edtContratoTermoAditivo_VlrTtlPrvto_Enabled ;
      private int edtContratoTermoAditivo_QtdContratada_Enabled ;
      private int edtContratoTermoAditivo_VlrUntUndCnt_Enabled ;
      private int edtContratoTermoAditivo_Observacao_Enabled ;
      private int edtContratada_PessoaNom_Enabled ;
      private int edtContratada_PessoaCNPJ_Enabled ;
      private int edtContrato_Numero_Enabled ;
      private int edtContrato_NumeroAta_Enabled ;
      private int edtContrato_Ano_Enabled ;
      private int AV11Insert_Contrato_Codigo ;
      private int AV16SaldoContrato_Codigo ;
      private int AV21GXV1 ;
      private int Z39Contratada_Codigo ;
      private int Z40Contratada_PessoaCod ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private decimal Z1361ContratoTermoAditivo_VlrUntUndCnt ;
      private decimal Z1360ContratoTermoAditivo_VlrTtlPrvto ;
      private decimal Z1362ContratoTermoAditivo_QtdContratada ;
      private decimal A1360ContratoTermoAditivo_VlrTtlPrvto ;
      private decimal A1362ContratoTermoAditivo_QtdContratada ;
      private decimal A1361ContratoTermoAditivo_VlrUntUndCnt ;
      private decimal AV13ContratoTermoAditivo_VlrTtlPrvto ;
      private decimal AV14ContratoTermoAditivo_QtdContratada ;
      private decimal AV15ContratoTermoAditivo_VlrUntUndCnt ;
      private decimal i1360ContratoTermoAditivo_VlrTtlPrvto ;
      private decimal i1362ContratoTermoAditivo_QtdContratada ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtContratoTermoAditivo_DataInicio_Internalname ;
      private String edtavContrato_codigo_Internalname ;
      private String edtavContrato_codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTable_Internalname ;
      private String grpUnnamedgroup1_Internalname ;
      private String lblTextblockcontratotermoaditivo_datainicio_Internalname ;
      private String lblTextblockcontratotermoaditivo_datainicio_Jsonclick ;
      private String edtContratoTermoAditivo_DataInicio_Jsonclick ;
      private String lblTextblockcontratotermoaditivo_datafim_Internalname ;
      private String lblTextblockcontratotermoaditivo_datafim_Jsonclick ;
      private String edtContratoTermoAditivo_DataFim_Internalname ;
      private String edtContratoTermoAditivo_DataFim_Jsonclick ;
      private String lblTextblockcontratotermoaditivo_dataassinatura_Internalname ;
      private String lblTextblockcontratotermoaditivo_dataassinatura_Jsonclick ;
      private String edtContratoTermoAditivo_DataAssinatura_Internalname ;
      private String edtContratoTermoAditivo_DataAssinatura_Jsonclick ;
      private String lblTextblockcontratotermoaditivo_datapbldou_Internalname ;
      private String lblTextblockcontratotermoaditivo_datapbldou_Jsonclick ;
      private String edtContratoTermoAditivo_DataPblDOU_Internalname ;
      private String edtContratoTermoAditivo_DataPblDOU_Jsonclick ;
      private String lblTextblockcontratotermoaditivo_vlrttlprvto_Internalname ;
      private String lblTextblockcontratotermoaditivo_vlrttlprvto_Jsonclick ;
      private String edtContratoTermoAditivo_VlrTtlPrvto_Internalname ;
      private String edtContratoTermoAditivo_VlrTtlPrvto_Jsonclick ;
      private String lblTextblockcontratotermoaditivo_qtdcontratada_Internalname ;
      private String lblTextblockcontratotermoaditivo_qtdcontratada_Jsonclick ;
      private String edtContratoTermoAditivo_QtdContratada_Internalname ;
      private String edtContratoTermoAditivo_QtdContratada_Jsonclick ;
      private String lblTextblocksaldocontrato_unidademedicao_codigo_Internalname ;
      private String lblTextblocksaldocontrato_unidademedicao_codigo_Jsonclick ;
      private String dynavSaldocontrato_unidademedicao_codigo_Internalname ;
      private String dynavSaldocontrato_unidademedicao_codigo_Jsonclick ;
      private String lblTextblockcontratotermoaditivo_vlruntundcnt_Internalname ;
      private String lblTextblockcontratotermoaditivo_vlruntundcnt_Jsonclick ;
      private String edtContratoTermoAditivo_VlrUntUndCnt_Internalname ;
      private String edtContratoTermoAditivo_VlrUntUndCnt_Jsonclick ;
      private String lblTextblockcontratotermoaditivo_observacao_Internalname ;
      private String lblTextblockcontratotermoaditivo_observacao_Jsonclick ;
      private String edtContratoTermoAditivo_Observacao_Internalname ;
      private String tblContrato_Internalname ;
      private String lblTextblockcontrato_numero_Internalname ;
      private String lblTextblockcontrato_numero_Jsonclick ;
      private String lblTextblockcontratada_pessoanom_Internalname ;
      private String lblTextblockcontratada_pessoanom_Jsonclick ;
      private String tblTablemergedcontratada_pessoanom_Internalname ;
      private String edtContratada_PessoaNom_Internalname ;
      private String A41Contratada_PessoaNom ;
      private String edtContratada_PessoaNom_Jsonclick ;
      private String lblTextblockcontratada_pessoacnpj_Internalname ;
      private String lblTextblockcontratada_pessoacnpj_Jsonclick ;
      private String edtContratada_PessoaCNPJ_Internalname ;
      private String edtContratada_PessoaCNPJ_Jsonclick ;
      private String tblTablemergedcontrato_numero_Internalname ;
      private String edtContrato_Numero_Internalname ;
      private String A77Contrato_Numero ;
      private String edtContrato_Numero_Jsonclick ;
      private String lblTextblockcontrato_numeroata_Internalname ;
      private String lblTextblockcontrato_numeroata_Jsonclick ;
      private String edtContrato_NumeroAta_Internalname ;
      private String A78Contrato_NumeroAta ;
      private String edtContrato_NumeroAta_Jsonclick ;
      private String lblTextblockcontrato_ano_Internalname ;
      private String lblTextblockcontrato_ano_Jsonclick ;
      private String edtContrato_Ano_Internalname ;
      private String edtContrato_Ano_Jsonclick ;
      private String AV20Pgmname ;
      private String Dvpanel_table_Width ;
      private String Dvpanel_table_Height ;
      private String Dvpanel_table_Cls ;
      private String Dvpanel_table_Title ;
      private String Dvpanel_table_Class ;
      private String Dvpanel_table_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode53 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z77Contrato_Numero ;
      private String Z78Contrato_NumeroAta ;
      private String Z41Contratada_PessoaNom ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_table_Internalname ;
      private String gxwrpcisep ;
      private DateTime Z316ContratoTermoAditivo_DataInicio ;
      private DateTime Z317ContratoTermoAditivo_DataFim ;
      private DateTime Z318ContratoTermoAditivo_DataAssinatura ;
      private DateTime Z1363ContratoTermoAditivo_DataPblDOU ;
      private DateTime A316ContratoTermoAditivo_DataInicio ;
      private DateTime A317ContratoTermoAditivo_DataFim ;
      private DateTime A318ContratoTermoAditivo_DataAssinatura ;
      private DateTime A1363ContratoTermoAditivo_DataPblDOU ;
      private bool entryPointCalled ;
      private bool n316ContratoTermoAditivo_DataInicio ;
      private bool n317ContratoTermoAditivo_DataFim ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n78Contrato_NumeroAta ;
      private bool n41Contratada_PessoaNom ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool n318ContratoTermoAditivo_DataAssinatura ;
      private bool n1363ContratoTermoAditivo_DataPblDOU ;
      private bool n1360ContratoTermoAditivo_VlrTtlPrvto ;
      private bool n1362ContratoTermoAditivo_QtdContratada ;
      private bool n1361ContratoTermoAditivo_VlrUntUndCnt ;
      private bool n1364ContratoTermoAditivo_Observacao ;
      private bool Dvpanel_table_Collapsible ;
      private bool Dvpanel_table_Collapsed ;
      private bool Dvpanel_table_Enabled ;
      private bool Dvpanel_table_Autowidth ;
      private bool Dvpanel_table_Autoheight ;
      private bool Dvpanel_table_Showheader ;
      private bool Dvpanel_table_Showcollapseicon ;
      private bool Dvpanel_table_Autoscroll ;
      private bool Dvpanel_table_Visible ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private String A1364ContratoTermoAditivo_Observacao ;
      private String Z1364ContratoTermoAditivo_Observacao ;
      private String A42Contratada_PessoaCNPJ ;
      private String Z42Contratada_PessoaCNPJ ;
      private IGxSession AV10WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP2_Contrato_Codigo ;
      private GXCombobox dynavSaldocontrato_unidademedicao_codigo ;
      private IDataStoreProvider pr_default ;
      private int[] T001E4_A39Contratada_Codigo ;
      private String[] T001E4_A77Contrato_Numero ;
      private String[] T001E4_A78Contrato_NumeroAta ;
      private bool[] T001E4_n78Contrato_NumeroAta ;
      private short[] T001E4_A79Contrato_Ano ;
      private int[] T001E5_A40Contratada_PessoaCod ;
      private String[] T001E6_A41Contratada_PessoaNom ;
      private bool[] T001E6_n41Contratada_PessoaNom ;
      private String[] T001E6_A42Contratada_PessoaCNPJ ;
      private bool[] T001E6_n42Contratada_PessoaCNPJ ;
      private int[] T001E7_A39Contratada_Codigo ;
      private int[] T001E7_A315ContratoTermoAditivo_Codigo ;
      private decimal[] T001E7_A1361ContratoTermoAditivo_VlrUntUndCnt ;
      private bool[] T001E7_n1361ContratoTermoAditivo_VlrUntUndCnt ;
      private String[] T001E7_A77Contrato_Numero ;
      private String[] T001E7_A78Contrato_NumeroAta ;
      private bool[] T001E7_n78Contrato_NumeroAta ;
      private short[] T001E7_A79Contrato_Ano ;
      private String[] T001E7_A41Contratada_PessoaNom ;
      private bool[] T001E7_n41Contratada_PessoaNom ;
      private String[] T001E7_A42Contratada_PessoaCNPJ ;
      private bool[] T001E7_n42Contratada_PessoaCNPJ ;
      private DateTime[] T001E7_A316ContratoTermoAditivo_DataInicio ;
      private bool[] T001E7_n316ContratoTermoAditivo_DataInicio ;
      private DateTime[] T001E7_A317ContratoTermoAditivo_DataFim ;
      private bool[] T001E7_n317ContratoTermoAditivo_DataFim ;
      private DateTime[] T001E7_A318ContratoTermoAditivo_DataAssinatura ;
      private bool[] T001E7_n318ContratoTermoAditivo_DataAssinatura ;
      private decimal[] T001E7_A1360ContratoTermoAditivo_VlrTtlPrvto ;
      private bool[] T001E7_n1360ContratoTermoAditivo_VlrTtlPrvto ;
      private decimal[] T001E7_A1362ContratoTermoAditivo_QtdContratada ;
      private bool[] T001E7_n1362ContratoTermoAditivo_QtdContratada ;
      private DateTime[] T001E7_A1363ContratoTermoAditivo_DataPblDOU ;
      private bool[] T001E7_n1363ContratoTermoAditivo_DataPblDOU ;
      private String[] T001E7_A1364ContratoTermoAditivo_Observacao ;
      private bool[] T001E7_n1364ContratoTermoAditivo_Observacao ;
      private int[] T001E7_A74Contrato_Codigo ;
      private int[] T001E7_A40Contratada_PessoaCod ;
      private int[] T001E8_A39Contratada_Codigo ;
      private String[] T001E8_A77Contrato_Numero ;
      private String[] T001E8_A78Contrato_NumeroAta ;
      private bool[] T001E8_n78Contrato_NumeroAta ;
      private short[] T001E8_A79Contrato_Ano ;
      private int[] T001E9_A40Contratada_PessoaCod ;
      private String[] T001E10_A41Contratada_PessoaNom ;
      private bool[] T001E10_n41Contratada_PessoaNom ;
      private String[] T001E10_A42Contratada_PessoaCNPJ ;
      private bool[] T001E10_n42Contratada_PessoaCNPJ ;
      private int[] T001E11_A315ContratoTermoAditivo_Codigo ;
      private int[] T001E3_A315ContratoTermoAditivo_Codigo ;
      private decimal[] T001E3_A1361ContratoTermoAditivo_VlrUntUndCnt ;
      private bool[] T001E3_n1361ContratoTermoAditivo_VlrUntUndCnt ;
      private DateTime[] T001E3_A316ContratoTermoAditivo_DataInicio ;
      private bool[] T001E3_n316ContratoTermoAditivo_DataInicio ;
      private DateTime[] T001E3_A317ContratoTermoAditivo_DataFim ;
      private bool[] T001E3_n317ContratoTermoAditivo_DataFim ;
      private DateTime[] T001E3_A318ContratoTermoAditivo_DataAssinatura ;
      private bool[] T001E3_n318ContratoTermoAditivo_DataAssinatura ;
      private decimal[] T001E3_A1360ContratoTermoAditivo_VlrTtlPrvto ;
      private bool[] T001E3_n1360ContratoTermoAditivo_VlrTtlPrvto ;
      private decimal[] T001E3_A1362ContratoTermoAditivo_QtdContratada ;
      private bool[] T001E3_n1362ContratoTermoAditivo_QtdContratada ;
      private DateTime[] T001E3_A1363ContratoTermoAditivo_DataPblDOU ;
      private bool[] T001E3_n1363ContratoTermoAditivo_DataPblDOU ;
      private String[] T001E3_A1364ContratoTermoAditivo_Observacao ;
      private bool[] T001E3_n1364ContratoTermoAditivo_Observacao ;
      private int[] T001E3_A74Contrato_Codigo ;
      private int[] T001E12_A315ContratoTermoAditivo_Codigo ;
      private int[] T001E13_A315ContratoTermoAditivo_Codigo ;
      private int[] T001E2_A315ContratoTermoAditivo_Codigo ;
      private decimal[] T001E2_A1361ContratoTermoAditivo_VlrUntUndCnt ;
      private bool[] T001E2_n1361ContratoTermoAditivo_VlrUntUndCnt ;
      private DateTime[] T001E2_A316ContratoTermoAditivo_DataInicio ;
      private bool[] T001E2_n316ContratoTermoAditivo_DataInicio ;
      private DateTime[] T001E2_A317ContratoTermoAditivo_DataFim ;
      private bool[] T001E2_n317ContratoTermoAditivo_DataFim ;
      private DateTime[] T001E2_A318ContratoTermoAditivo_DataAssinatura ;
      private bool[] T001E2_n318ContratoTermoAditivo_DataAssinatura ;
      private decimal[] T001E2_A1360ContratoTermoAditivo_VlrTtlPrvto ;
      private bool[] T001E2_n1360ContratoTermoAditivo_VlrTtlPrvto ;
      private decimal[] T001E2_A1362ContratoTermoAditivo_QtdContratada ;
      private bool[] T001E2_n1362ContratoTermoAditivo_QtdContratada ;
      private DateTime[] T001E2_A1363ContratoTermoAditivo_DataPblDOU ;
      private bool[] T001E2_n1363ContratoTermoAditivo_DataPblDOU ;
      private String[] T001E2_A1364ContratoTermoAditivo_Observacao ;
      private bool[] T001E2_n1364ContratoTermoAditivo_Observacao ;
      private int[] T001E2_A74Contrato_Codigo ;
      private int[] T001E14_A315ContratoTermoAditivo_Codigo ;
      private int[] T001E17_A39Contratada_Codigo ;
      private String[] T001E17_A77Contrato_Numero ;
      private String[] T001E17_A78Contrato_NumeroAta ;
      private bool[] T001E17_n78Contrato_NumeroAta ;
      private short[] T001E17_A79Contrato_Ano ;
      private int[] T001E18_A40Contratada_PessoaCod ;
      private String[] T001E19_A41Contratada_PessoaNom ;
      private bool[] T001E19_n41Contratada_PessoaNom ;
      private String[] T001E19_A42Contratada_PessoaCNPJ ;
      private bool[] T001E19_n42Contratada_PessoaCNPJ ;
      private int[] T001E20_A315ContratoTermoAditivo_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
   }

   public class contratotermoaditivo__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new UpdateCursor(def[13])
         ,new UpdateCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT001E7 ;
          prmT001E7 = new Object[] {
          new Object[] {"@ContratoTermoAditivo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001E4 ;
          prmT001E4 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001E5 ;
          prmT001E5 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001E6 ;
          prmT001E6 = new Object[] {
          new Object[] {"@Contratada_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001E8 ;
          prmT001E8 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001E9 ;
          prmT001E9 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001E10 ;
          prmT001E10 = new Object[] {
          new Object[] {"@Contratada_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001E11 ;
          prmT001E11 = new Object[] {
          new Object[] {"@ContratoTermoAditivo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001E3 ;
          prmT001E3 = new Object[] {
          new Object[] {"@ContratoTermoAditivo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001E12 ;
          prmT001E12 = new Object[] {
          new Object[] {"@ContratoTermoAditivo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001E13 ;
          prmT001E13 = new Object[] {
          new Object[] {"@ContratoTermoAditivo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001E2 ;
          prmT001E2 = new Object[] {
          new Object[] {"@ContratoTermoAditivo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001E14 ;
          prmT001E14 = new Object[] {
          new Object[] {"@ContratoTermoAditivo_VlrUntUndCnt",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContratoTermoAditivo_DataInicio",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContratoTermoAditivo_DataFim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContratoTermoAditivo_DataAssinatura",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContratoTermoAditivo_VlrTtlPrvto",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContratoTermoAditivo_QtdContratada",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContratoTermoAditivo_DataPblDOU",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContratoTermoAditivo_Observacao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001E15 ;
          prmT001E15 = new Object[] {
          new Object[] {"@ContratoTermoAditivo_VlrUntUndCnt",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContratoTermoAditivo_DataInicio",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContratoTermoAditivo_DataFim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContratoTermoAditivo_DataAssinatura",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContratoTermoAditivo_VlrTtlPrvto",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContratoTermoAditivo_QtdContratada",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContratoTermoAditivo_DataPblDOU",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContratoTermoAditivo_Observacao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoTermoAditivo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001E16 ;
          prmT001E16 = new Object[] {
          new Object[] {"@ContratoTermoAditivo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001E17 ;
          prmT001E17 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001E18 ;
          prmT001E18 = new Object[] {
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001E19 ;
          prmT001E19 = new Object[] {
          new Object[] {"@Contratada_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001E20 ;
          prmT001E20 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("T001E2", "SELECT [ContratoTermoAditivo_Codigo], [ContratoTermoAditivo_VlrUntUndCnt], [ContratoTermoAditivo_DataInicio], [ContratoTermoAditivo_DataFim], [ContratoTermoAditivo_DataAssinatura], [ContratoTermoAditivo_VlrTtlPrvto], [ContratoTermoAditivo_QtdContratada], [ContratoTermoAditivo_DataPblDOU], [ContratoTermoAditivo_Observacao], [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (UPDLOCK) WHERE [ContratoTermoAditivo_Codigo] = @ContratoTermoAditivo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001E2,1,0,true,false )
             ,new CursorDef("T001E3", "SELECT [ContratoTermoAditivo_Codigo], [ContratoTermoAditivo_VlrUntUndCnt], [ContratoTermoAditivo_DataInicio], [ContratoTermoAditivo_DataFim], [ContratoTermoAditivo_DataAssinatura], [ContratoTermoAditivo_VlrTtlPrvto], [ContratoTermoAditivo_QtdContratada], [ContratoTermoAditivo_DataPblDOU], [ContratoTermoAditivo_Observacao], [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) WHERE [ContratoTermoAditivo_Codigo] = @ContratoTermoAditivo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001E3,1,0,true,false )
             ,new CursorDef("T001E4", "SELECT [Contratada_Codigo], [Contrato_Numero], [Contrato_NumeroAta], [Contrato_Ano] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001E4,1,0,true,false )
             ,new CursorDef("T001E5", "SELECT [Contratada_PessoaCod] AS Contratada_PessoaCod FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001E5,1,0,true,false )
             ,new CursorDef("T001E6", "SELECT [Pessoa_Nome] AS Contratada_PessoaNom, [Pessoa_Docto] AS Contratada_PessoaCNPJ FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contratada_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001E6,1,0,true,false )
             ,new CursorDef("T001E7", "SELECT T2.[Contratada_Codigo], TM1.[ContratoTermoAditivo_Codigo], TM1.[ContratoTermoAditivo_VlrUntUndCnt], T2.[Contrato_Numero], T2.[Contrato_NumeroAta], T2.[Contrato_Ano], T4.[Pessoa_Nome] AS Contratada_PessoaNom, T4.[Pessoa_Docto] AS Contratada_PessoaCNPJ, TM1.[ContratoTermoAditivo_DataInicio], TM1.[ContratoTermoAditivo_DataFim], TM1.[ContratoTermoAditivo_DataAssinatura], TM1.[ContratoTermoAditivo_VlrTtlPrvto], TM1.[ContratoTermoAditivo_QtdContratada], TM1.[ContratoTermoAditivo_DataPblDOU], TM1.[ContratoTermoAditivo_Observacao], TM1.[Contrato_Codigo], T3.[Contratada_PessoaCod] AS Contratada_PessoaCod FROM ((([ContratoTermoAditivo] TM1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = TM1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod]) WHERE TM1.[ContratoTermoAditivo_Codigo] = @ContratoTermoAditivo_Codigo ORDER BY TM1.[ContratoTermoAditivo_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001E7,100,0,true,false )
             ,new CursorDef("T001E8", "SELECT [Contratada_Codigo], [Contrato_Numero], [Contrato_NumeroAta], [Contrato_Ano] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001E8,1,0,true,false )
             ,new CursorDef("T001E9", "SELECT [Contratada_PessoaCod] AS Contratada_PessoaCod FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001E9,1,0,true,false )
             ,new CursorDef("T001E10", "SELECT [Pessoa_Nome] AS Contratada_PessoaNom, [Pessoa_Docto] AS Contratada_PessoaCNPJ FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contratada_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001E10,1,0,true,false )
             ,new CursorDef("T001E11", "SELECT [ContratoTermoAditivo_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) WHERE [ContratoTermoAditivo_Codigo] = @ContratoTermoAditivo_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001E11,1,0,true,false )
             ,new CursorDef("T001E12", "SELECT TOP 1 [ContratoTermoAditivo_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) WHERE ( [ContratoTermoAditivo_Codigo] > @ContratoTermoAditivo_Codigo) ORDER BY [ContratoTermoAditivo_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001E12,1,0,true,true )
             ,new CursorDef("T001E13", "SELECT TOP 1 [ContratoTermoAditivo_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) WHERE ( [ContratoTermoAditivo_Codigo] < @ContratoTermoAditivo_Codigo) ORDER BY [ContratoTermoAditivo_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001E13,1,0,true,true )
             ,new CursorDef("T001E14", "INSERT INTO [ContratoTermoAditivo]([ContratoTermoAditivo_VlrUntUndCnt], [ContratoTermoAditivo_DataInicio], [ContratoTermoAditivo_DataFim], [ContratoTermoAditivo_DataAssinatura], [ContratoTermoAditivo_VlrTtlPrvto], [ContratoTermoAditivo_QtdContratada], [ContratoTermoAditivo_DataPblDOU], [ContratoTermoAditivo_Observacao], [Contrato_Codigo]) VALUES(@ContratoTermoAditivo_VlrUntUndCnt, @ContratoTermoAditivo_DataInicio, @ContratoTermoAditivo_DataFim, @ContratoTermoAditivo_DataAssinatura, @ContratoTermoAditivo_VlrTtlPrvto, @ContratoTermoAditivo_QtdContratada, @ContratoTermoAditivo_DataPblDOU, @ContratoTermoAditivo_Observacao, @Contrato_Codigo); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT001E14)
             ,new CursorDef("T001E15", "UPDATE [ContratoTermoAditivo] SET [ContratoTermoAditivo_VlrUntUndCnt]=@ContratoTermoAditivo_VlrUntUndCnt, [ContratoTermoAditivo_DataInicio]=@ContratoTermoAditivo_DataInicio, [ContratoTermoAditivo_DataFim]=@ContratoTermoAditivo_DataFim, [ContratoTermoAditivo_DataAssinatura]=@ContratoTermoAditivo_DataAssinatura, [ContratoTermoAditivo_VlrTtlPrvto]=@ContratoTermoAditivo_VlrTtlPrvto, [ContratoTermoAditivo_QtdContratada]=@ContratoTermoAditivo_QtdContratada, [ContratoTermoAditivo_DataPblDOU]=@ContratoTermoAditivo_DataPblDOU, [ContratoTermoAditivo_Observacao]=@ContratoTermoAditivo_Observacao, [Contrato_Codigo]=@Contrato_Codigo  WHERE [ContratoTermoAditivo_Codigo] = @ContratoTermoAditivo_Codigo", GxErrorMask.GX_NOMASK,prmT001E15)
             ,new CursorDef("T001E16", "DELETE FROM [ContratoTermoAditivo]  WHERE [ContratoTermoAditivo_Codigo] = @ContratoTermoAditivo_Codigo", GxErrorMask.GX_NOMASK,prmT001E16)
             ,new CursorDef("T001E17", "SELECT [Contratada_Codigo], [Contrato_Numero], [Contrato_NumeroAta], [Contrato_Ano] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001E17,1,0,true,false )
             ,new CursorDef("T001E18", "SELECT [Contratada_PessoaCod] AS Contratada_PessoaCod FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @Contratada_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001E18,1,0,true,false )
             ,new CursorDef("T001E19", "SELECT [Pessoa_Nome] AS Contratada_PessoaNom, [Pessoa_Docto] AS Contratada_PessoaCNPJ FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Contratada_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001E19,1,0,true,false )
             ,new CursorDef("T001E20", "SELECT [ContratoTermoAditivo_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) ORDER BY [ContratoTermoAditivo_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001E20,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((decimal[]) buf[9])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((decimal[]) buf[11])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((DateTime[]) buf[13])[0] = rslt.getGXDate(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getLongVarchar(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((int[]) buf[17])[0] = rslt.getInt(10) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((decimal[]) buf[9])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((decimal[]) buf[11])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((DateTime[]) buf[13])[0] = rslt.getGXDate(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getLongVarchar(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((int[]) buf[17])[0] = rslt.getInt(10) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 20) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 10) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((short[]) buf[7])[0] = rslt.getShort(6) ;
                ((String[]) buf[8])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((String[]) buf[10])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[12])[0] = rslt.getGXDate(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[14])[0] = rslt.getGXDate(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((DateTime[]) buf[16])[0] = rslt.getGXDate(11) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((decimal[]) buf[18])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                ((decimal[]) buf[20])[0] = rslt.getDecimal(13) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(13);
                ((DateTime[]) buf[22])[0] = rslt.getGXDate(14) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(14);
                ((String[]) buf[24])[0] = rslt.getLongVarchar(15) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(15);
                ((int[]) buf[26])[0] = rslt.getInt(16) ;
                ((int[]) buf[27])[0] = rslt.getInt(17) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 17 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(2, (DateTime)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(3, (DateTime)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(4, (DateTime)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 6 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(6, (decimal)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 7 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(7, (DateTime)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 8 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[15]);
                }
                stmt.SetParameter(9, (int)parms[16]);
                return;
             case 13 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(2, (DateTime)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(3, (DateTime)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(4, (DateTime)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 6 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(6, (decimal)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 7 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(7, (DateTime)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 8 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[15]);
                }
                stmt.SetParameter(9, (int)parms[16]);
                stmt.SetParameter(10, (int)parms[17]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
