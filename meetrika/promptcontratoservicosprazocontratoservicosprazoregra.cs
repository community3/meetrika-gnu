/*
               File: PromptContratoServicosPrazoContratoServicosPrazoRegra
        Description: Selecione Contrato Servicos Prazo Regra
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 18:54:4.99
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptcontratoservicosprazocontratoservicosprazoregra : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptcontratoservicosprazocontratoservicosprazoregra( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptcontratoservicosprazocontratoservicosprazoregra( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_InContratoServicosPrazo_CntSrvCod ,
                           ref short aP1_InOutContratoServicosPrazoRegra_Sequencial ,
                           ref decimal aP2_InOutContratoServicosPrazoRegra_Inicio )
      {
         this.AV7InContratoServicosPrazo_CntSrvCod = aP0_InContratoServicosPrazo_CntSrvCod;
         this.AV8InOutContratoServicosPrazoRegra_Sequencial = aP1_InOutContratoServicosPrazoRegra_Sequencial;
         this.AV9InOutContratoServicosPrazoRegra_Inicio = aP2_InOutContratoServicosPrazoRegra_Inicio;
         executePrivate();
         aP1_InOutContratoServicosPrazoRegra_Sequencial=this.AV8InOutContratoServicosPrazoRegra_Sequencial;
         aP2_InOutContratoServicosPrazoRegra_Inicio=this.AV9InOutContratoServicosPrazoRegra_Inicio;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_80 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_80_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_80_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV14OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
               AV15OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
               AV16DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
               AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV18ContratoServicosPrazoRegra_Inicio1 = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContratoServicosPrazoRegra_Inicio1", StringUtil.LTrim( StringUtil.Str( AV18ContratoServicosPrazoRegra_Inicio1, 14, 5)));
               AV20DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
               AV22ContratoServicosPrazoRegra_Inicio2 = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContratoServicosPrazoRegra_Inicio2", StringUtil.LTrim( StringUtil.Str( AV22ContratoServicosPrazoRegra_Inicio2, 14, 5)));
               AV24DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
               AV25DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)));
               AV26ContratoServicosPrazoRegra_Inicio3 = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ContratoServicosPrazoRegra_Inicio3", StringUtil.LTrim( StringUtil.Str( AV26ContratoServicosPrazoRegra_Inicio3, 14, 5)));
               AV19DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV23DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
               AV32TFContratoServicosPrazoRegra_Sequencial = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFContratoServicosPrazoRegra_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFContratoServicosPrazoRegra_Sequencial), 4, 0)));
               AV33TFContratoServicosPrazoRegra_Sequencial_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33TFContratoServicosPrazoRegra_Sequencial_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33TFContratoServicosPrazoRegra_Sequencial_To), 4, 0)));
               AV36TFContratoServicosPrazoRegra_Inicio = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFContratoServicosPrazoRegra_Inicio", StringUtil.LTrim( StringUtil.Str( AV36TFContratoServicosPrazoRegra_Inicio, 14, 5)));
               AV37TFContratoServicosPrazoRegra_Inicio_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFContratoServicosPrazoRegra_Inicio_To", StringUtil.LTrim( StringUtil.Str( AV37TFContratoServicosPrazoRegra_Inicio_To, 14, 5)));
               AV40TFContratoServicosPrazoRegra_Fim = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContratoServicosPrazoRegra_Fim", StringUtil.LTrim( StringUtil.Str( AV40TFContratoServicosPrazoRegra_Fim, 14, 5)));
               AV41TFContratoServicosPrazoRegra_Fim_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFContratoServicosPrazoRegra_Fim_To", StringUtil.LTrim( StringUtil.Str( AV41TFContratoServicosPrazoRegra_Fim_To, 14, 5)));
               AV44TFContratoServicosPrazoRegra_Dias = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContratoServicosPrazoRegra_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFContratoServicosPrazoRegra_Dias), 3, 0)));
               AV45TFContratoServicosPrazoRegra_Dias_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContratoServicosPrazoRegra_Dias_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFContratoServicosPrazoRegra_Dias_To), 3, 0)));
               AV7InContratoServicosPrazo_CntSrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InContratoServicosPrazo_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InContratoServicosPrazo_CntSrvCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vINCONTRATOSERVICOSPRAZO_CNTSRVCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7InContratoServicosPrazo_CntSrvCod), "ZZZZZ9")));
               AV34ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace", AV34ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace);
               AV38ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace", AV38ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace);
               AV42ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace", AV42ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace);
               AV46ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace", AV46ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContratoServicosPrazoRegra_Inicio1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22ContratoServicosPrazoRegra_Inicio2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26ContratoServicosPrazoRegra_Inicio3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV32TFContratoServicosPrazoRegra_Sequencial, AV33TFContratoServicosPrazoRegra_Sequencial_To, AV36TFContratoServicosPrazoRegra_Inicio, AV37TFContratoServicosPrazoRegra_Inicio_To, AV40TFContratoServicosPrazoRegra_Fim, AV41TFContratoServicosPrazoRegra_Fim_To, AV44TFContratoServicosPrazoRegra_Dias, AV45TFContratoServicosPrazoRegra_Dias_To, AV7InContratoServicosPrazo_CntSrvCod, AV34ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace, AV38ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace, AV42ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace, AV46ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InContratoServicosPrazo_CntSrvCod = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InContratoServicosPrazo_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InContratoServicosPrazo_CntSrvCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vINCONTRATOSERVICOSPRAZO_CNTSRVCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7InContratoServicosPrazo_CntSrvCod), "ZZZZZ9")));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV8InOutContratoServicosPrazoRegra_Sequencial = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContratoServicosPrazoRegra_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8InOutContratoServicosPrazoRegra_Sequencial), 4, 0)));
                  AV9InOutContratoServicosPrazoRegra_Inicio = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9InOutContratoServicosPrazoRegra_Inicio", StringUtil.LTrim( StringUtil.Str( AV9InOutContratoServicosPrazoRegra_Inicio, 14, 5)));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PAG22( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               WSG22( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEG22( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311854525");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptcontratoservicosprazocontratoservicosprazoregra.aspx") + "?" + UrlEncode("" +AV7InContratoServicosPrazo_CntSrvCod) + "," + UrlEncode("" +AV8InOutContratoServicosPrazoRegra_Sequencial) + "," + UrlEncode(StringUtil.Str(AV9InOutContratoServicosPrazoRegra_Inicio,14,5))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV15OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV16DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOSERVICOSPRAZOREGRA_INICIO1", StringUtil.LTrim( StringUtil.NToC( AV18ContratoServicosPrazoRegra_Inicio1, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV20DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOSERVICOSPRAZOREGRA_INICIO2", StringUtil.LTrim( StringUtil.NToC( AV22ContratoServicosPrazoRegra_Inicio2, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV24DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV25DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOSERVICOSPRAZOREGRA_INICIO3", StringUtil.LTrim( StringUtil.NToC( AV26ContratoServicosPrazoRegra_Inicio3, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV19DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV23DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32TFContratoServicosPrazoRegra_Sequencial), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV33TFContratoServicosPrazoRegra_Sequencial_To), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSPRAZOREGRA_INICIO", StringUtil.LTrim( StringUtil.NToC( AV36TFContratoServicosPrazoRegra_Inicio, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSPRAZOREGRA_INICIO_TO", StringUtil.LTrim( StringUtil.NToC( AV37TFContratoServicosPrazoRegra_Inicio_To, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSPRAZOREGRA_FIM", StringUtil.LTrim( StringUtil.NToC( AV40TFContratoServicosPrazoRegra_Fim, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSPRAZOREGRA_FIM_TO", StringUtil.LTrim( StringUtil.NToC( AV41TFContratoServicosPrazoRegra_Fim_To, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSPRAZOREGRA_DIAS", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV44TFContratoServicosPrazoRegra_Dias), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOSERVICOSPRAZOREGRA_DIAS_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV45TFContratoServicosPrazoRegra_Dias_To), 3, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_80", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_80), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV49GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV50GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV47DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV47DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSERVICOSPRAZOREGRA_SEQUENCIALTITLEFILTERDATA", AV31ContratoServicosPrazoRegra_SequencialTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSERVICOSPRAZOREGRA_SEQUENCIALTITLEFILTERDATA", AV31ContratoServicosPrazoRegra_SequencialTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSERVICOSPRAZOREGRA_INICIOTITLEFILTERDATA", AV35ContratoServicosPrazoRegra_InicioTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSERVICOSPRAZOREGRA_INICIOTITLEFILTERDATA", AV35ContratoServicosPrazoRegra_InicioTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSERVICOSPRAZOREGRA_FIMTITLEFILTERDATA", AV39ContratoServicosPrazoRegra_FimTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSERVICOSPRAZOREGRA_FIMTITLEFILTERDATA", AV39ContratoServicosPrazoRegra_FimTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSERVICOSPRAZOREGRA_DIASTITLEFILTERDATA", AV43ContratoServicosPrazoRegra_DiasTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSERVICOSPRAZOREGRA_DIASTITLEFILTERDATA", AV43ContratoServicosPrazoRegra_DiasTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vINCONTRATOSERVICOSPRAZO_CNTSRVCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InContratoServicosPrazo_CntSrvCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV11GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV11GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV28DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV27DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8InOutContratoServicosPrazoRegra_Sequencial), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTCONTRATOSERVICOSPRAZOREGRA_INICIO", StringUtil.LTrim( StringUtil.NToC( AV9InOutContratoServicosPrazoRegra_Inicio, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vINCONTRATOSERVICOSPRAZO_CNTSRVCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7InContratoServicosPrazo_CntSrvCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vINCONTRATOSERVICOSPRAZO_CNTSRVCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7InContratoServicosPrazo_CntSrvCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Caption", StringUtil.RTrim( Ddo_contratoservicosprazoregra_sequencial_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Tooltip", StringUtil.RTrim( Ddo_contratoservicosprazoregra_sequencial_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Cls", StringUtil.RTrim( Ddo_contratoservicosprazoregra_sequencial_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicosprazoregra_sequencial_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoservicosprazoregra_sequencial_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicosprazoregra_sequencial_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicosprazoregra_sequencial_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicosprazoregra_sequencial_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicosprazoregra_sequencial_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicosprazoregra_sequencial_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicosprazoregra_sequencial_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Filtertype", StringUtil.RTrim( Ddo_contratoservicosprazoregra_sequencial_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicosprazoregra_sequencial_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicosprazoregra_sequencial_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Sortasc", StringUtil.RTrim( Ddo_contratoservicosprazoregra_sequencial_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Sortdsc", StringUtil.RTrim( Ddo_contratoservicosprazoregra_sequencial_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicosprazoregra_sequencial_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoservicosprazoregra_sequencial_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Rangefilterto", StringUtil.RTrim( Ddo_contratoservicosprazoregra_sequencial_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicosprazoregra_sequencial_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Caption", StringUtil.RTrim( Ddo_contratoservicosprazoregra_inicio_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Tooltip", StringUtil.RTrim( Ddo_contratoservicosprazoregra_inicio_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Cls", StringUtil.RTrim( Ddo_contratoservicosprazoregra_inicio_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicosprazoregra_inicio_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoservicosprazoregra_inicio_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicosprazoregra_inicio_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicosprazoregra_inicio_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicosprazoregra_inicio_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicosprazoregra_inicio_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicosprazoregra_inicio_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicosprazoregra_inicio_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Filtertype", StringUtil.RTrim( Ddo_contratoservicosprazoregra_inicio_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicosprazoregra_inicio_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicosprazoregra_inicio_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Sortasc", StringUtil.RTrim( Ddo_contratoservicosprazoregra_inicio_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Sortdsc", StringUtil.RTrim( Ddo_contratoservicosprazoregra_inicio_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicosprazoregra_inicio_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoservicosprazoregra_inicio_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Rangefilterto", StringUtil.RTrim( Ddo_contratoservicosprazoregra_inicio_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicosprazoregra_inicio_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Caption", StringUtil.RTrim( Ddo_contratoservicosprazoregra_fim_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Tooltip", StringUtil.RTrim( Ddo_contratoservicosprazoregra_fim_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Cls", StringUtil.RTrim( Ddo_contratoservicosprazoregra_fim_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicosprazoregra_fim_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoservicosprazoregra_fim_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicosprazoregra_fim_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicosprazoregra_fim_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicosprazoregra_fim_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicosprazoregra_fim_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicosprazoregra_fim_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicosprazoregra_fim_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Filtertype", StringUtil.RTrim( Ddo_contratoservicosprazoregra_fim_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicosprazoregra_fim_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicosprazoregra_fim_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Sortasc", StringUtil.RTrim( Ddo_contratoservicosprazoregra_fim_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Sortdsc", StringUtil.RTrim( Ddo_contratoservicosprazoregra_fim_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicosprazoregra_fim_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoservicosprazoregra_fim_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Rangefilterto", StringUtil.RTrim( Ddo_contratoservicosprazoregra_fim_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicosprazoregra_fim_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Caption", StringUtil.RTrim( Ddo_contratoservicosprazoregra_dias_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Tooltip", StringUtil.RTrim( Ddo_contratoservicosprazoregra_dias_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Cls", StringUtil.RTrim( Ddo_contratoservicosprazoregra_dias_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicosprazoregra_dias_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoservicosprazoregra_dias_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicosprazoregra_dias_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicosprazoregra_dias_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicosprazoregra_dias_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicosprazoregra_dias_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicosprazoregra_dias_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicosprazoregra_dias_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Filtertype", StringUtil.RTrim( Ddo_contratoservicosprazoregra_dias_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicosprazoregra_dias_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicosprazoregra_dias_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Sortasc", StringUtil.RTrim( Ddo_contratoservicosprazoregra_dias_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Sortdsc", StringUtil.RTrim( Ddo_contratoservicosprazoregra_dias_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicosprazoregra_dias_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoservicosprazoregra_dias_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Rangefilterto", StringUtil.RTrim( Ddo_contratoservicosprazoregra_dias_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicosprazoregra_dias_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicosprazoregra_sequencial_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicosprazoregra_sequencial_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoservicosprazoregra_sequencial_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicosprazoregra_inicio_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicosprazoregra_inicio_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoservicosprazoregra_inicio_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicosprazoregra_fim_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicosprazoregra_fim_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoservicosprazoregra_fim_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicosprazoregra_dias_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicosprazoregra_dias_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoservicosprazoregra_dias_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormG22( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptContratoServicosPrazoContratoServicosPrazoRegra" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Contrato Servicos Prazo Regra" ;
      }

      protected void WBG20( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_G22( true) ;
         }
         else
         {
            wb_table1_2_G22( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_G22e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 90,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV19DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(90, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,90);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV23DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(91, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,91);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosprazoregra_sequencial_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32TFContratoServicosPrazoRegra_Sequencial), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV32TFContratoServicosPrazoRegra_Sequencial), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,92);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosprazoregra_sequencial_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosprazoregra_sequencial_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicosPrazoContratoServicosPrazoRegra.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 93,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosprazoregra_sequencial_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV33TFContratoServicosPrazoRegra_Sequencial_To), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV33TFContratoServicosPrazoRegra_Sequencial_To), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,93);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosprazoregra_sequencial_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosprazoregra_sequencial_to_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicosPrazoContratoServicosPrazoRegra.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosprazoregra_inicio_Internalname, StringUtil.LTrim( StringUtil.NToC( AV36TFContratoServicosPrazoRegra_Inicio, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV36TFContratoServicosPrazoRegra_Inicio, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,94);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosprazoregra_inicio_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosprazoregra_inicio_Visible, 1, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicosPrazoContratoServicosPrazoRegra.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosprazoregra_inicio_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV37TFContratoServicosPrazoRegra_Inicio_To, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV37TFContratoServicosPrazoRegra_Inicio_To, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,95);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosprazoregra_inicio_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosprazoregra_inicio_to_Visible, 1, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicosPrazoContratoServicosPrazoRegra.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosprazoregra_fim_Internalname, StringUtil.LTrim( StringUtil.NToC( AV40TFContratoServicosPrazoRegra_Fim, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV40TFContratoServicosPrazoRegra_Fim, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,96);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosprazoregra_fim_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosprazoregra_fim_Visible, 1, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicosPrazoContratoServicosPrazoRegra.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosprazoregra_fim_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV41TFContratoServicosPrazoRegra_Fim_To, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV41TFContratoServicosPrazoRegra_Fim_To, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,97);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosprazoregra_fim_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosprazoregra_fim_to_Visible, 1, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicosPrazoContratoServicosPrazoRegra.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosprazoregra_dias_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV44TFContratoServicosPrazoRegra_Dias), 3, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV44TFContratoServicosPrazoRegra_Dias), "ZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,98);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosprazoregra_dias_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosprazoregra_dias_Visible, 1, 0, "text", "", 30, "px", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicosPrazoContratoServicosPrazoRegra.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicosprazoregra_dias_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV45TFContratoServicosPrazoRegra_Dias_To), 3, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV45TFContratoServicosPrazoRegra_Dias_To), "ZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,99);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicosprazoregra_dias_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicosprazoregra_dias_to_Visible, 1, 0, "text", "", 30, "px", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicosPrazoContratoServicosPrazoRegra.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIALContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicosprazoregra_sequencialtitlecontrolidtoreplace_Internalname, AV34ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,101);\"", 0, edtavDdo_contratoservicosprazoregra_sequencialtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoServicosPrazoContratoServicosPrazoRegra.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSERVICOSPRAZOREGRA_INICIOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicosprazoregra_iniciotitlecontrolidtoreplace_Internalname, AV38ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,103);\"", 0, edtavDdo_contratoservicosprazoregra_iniciotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoServicosPrazoContratoServicosPrazoRegra.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSERVICOSPRAZOREGRA_FIMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicosprazoregra_fimtitlecontrolidtoreplace_Internalname, AV42ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,105);\"", 0, edtavDdo_contratoservicosprazoregra_fimtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoServicosPrazoContratoServicosPrazoRegra.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOSERVICOSPRAZOREGRA_DIASContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicosprazoregra_diastitlecontrolidtoreplace_Internalname, AV46ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,107);\"", 0, edtavDdo_contratoservicosprazoregra_diastitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoServicosPrazoContratoServicosPrazoRegra.htm");
         }
         wbLoad = true;
      }

      protected void STARTG22( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Contrato Servicos Prazo Regra", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPG20( ) ;
      }

      protected void WSG22( )
      {
         STARTG22( ) ;
         EVTG22( ) ;
      }

      protected void EVTG22( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11G22 */
                           E11G22 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12G22 */
                           E12G22 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13G22 */
                           E13G22 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14G22 */
                           E14G22 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15G22 */
                           E15G22 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16G22 */
                           E16G22 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17G22 */
                           E17G22 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18G22 */
                           E18G22 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19G22 */
                           E19G22 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E20G22 */
                           E20G22 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E21G22 */
                           E21G22 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E22G22 */
                           E22G22 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_80_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
                           SubsflControlProps_802( ) ;
                           AV29Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29Select)) ? AV53Select_GXI : context.convertURL( context.PathToRelativeUrl( AV29Select))));
                           A906ContratoServicosPrazoRegra_Sequencial = (short)(context.localUtil.CToN( cgiGet( edtContratoServicosPrazoRegra_Sequencial_Internalname), ",", "."));
                           A907ContratoServicosPrazoRegra_Inicio = context.localUtil.CToN( cgiGet( edtContratoServicosPrazoRegra_Inicio_Internalname), ",", ".");
                           A908ContratoServicosPrazoRegra_Fim = context.localUtil.CToN( cgiGet( edtContratoServicosPrazoRegra_Fim_Internalname), ",", ".");
                           A909ContratoServicosPrazoRegra_Dias = (short)(context.localUtil.CToN( cgiGet( edtContratoServicosPrazoRegra_Dias_Internalname), ",", "."));
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E23G22 */
                                 E23G22 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E24G22 */
                                 E24G22 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E25G22 */
                                 E25G22 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV15OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV17DynamicFiltersOperator1 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratoservicosprazoregra_inicio1 Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATOSERVICOSPRAZOREGRA_INICIO1"), ",", ".") != AV18ContratoServicosPrazoRegra_Inicio1 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV21DynamicFiltersOperator2 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratoservicosprazoregra_inicio2 Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATOSERVICOSPRAZOREGRA_INICIO2"), ",", ".") != AV22ContratoServicosPrazoRegra_Inicio2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV24DynamicFiltersSelector3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV25DynamicFiltersOperator3 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratoservicosprazoregra_inicio3 Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATOSERVICOSPRAZOREGRA_INICIO3"), ",", ".") != AV26ContratoServicosPrazoRegra_Inicio3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV23DynamicFiltersEnabled3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicosprazoregra_sequencial Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL"), ",", ".") != Convert.ToDecimal( AV32TFContratoServicosPrazoRegra_Sequencial )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicosprazoregra_sequencial_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_TO"), ",", ".") != Convert.ToDecimal( AV33TFContratoServicosPrazoRegra_Sequencial_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicosprazoregra_inicio Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSPRAZOREGRA_INICIO"), ",", ".") != AV36TFContratoServicosPrazoRegra_Inicio )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicosprazoregra_inicio_to Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSPRAZOREGRA_INICIO_TO"), ",", ".") != AV37TFContratoServicosPrazoRegra_Inicio_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicosprazoregra_fim Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSPRAZOREGRA_FIM"), ",", ".") != AV40TFContratoServicosPrazoRegra_Fim )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicosprazoregra_fim_to Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSPRAZOREGRA_FIM_TO"), ",", ".") != AV41TFContratoServicosPrazoRegra_Fim_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicosprazoregra_dias Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSPRAZOREGRA_DIAS"), ",", ".") != Convert.ToDecimal( AV44TFContratoServicosPrazoRegra_Dias )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoservicosprazoregra_dias_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSPRAZOREGRA_DIAS_TO"), ",", ".") != Convert.ToDecimal( AV45TFContratoServicosPrazoRegra_Dias_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E26G22 */
                                       E26G22 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEG22( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormG22( ) ;
            }
         }
      }

      protected void PAG22( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CONTRATOSERVICOSPRAZOREGRA_INICIO", "Inicio", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "<", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "=", 0);
            cmbavDynamicfiltersoperator1.addItem("2", ">", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CONTRATOSERVICOSPRAZOREGRA_INICIO", "Inicio", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "<", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "=", 0);
            cmbavDynamicfiltersoperator2.addItem("2", ">", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("CONTRATOSERVICOSPRAZOREGRA_INICIO", "Inicio", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV24DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV24DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "<", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "=", 0);
            cmbavDynamicfiltersoperator3.addItem("2", ">", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV25DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)));
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_802( ) ;
         while ( nGXsfl_80_idx <= nRC_GXsfl_80 )
         {
            sendrow_802( ) ;
            nGXsfl_80_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_80_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_80_idx+1));
            sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
            SubsflControlProps_802( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV14OrderedBy ,
                                       bool AV15OrderedDsc ,
                                       String AV16DynamicFiltersSelector1 ,
                                       short AV17DynamicFiltersOperator1 ,
                                       decimal AV18ContratoServicosPrazoRegra_Inicio1 ,
                                       String AV20DynamicFiltersSelector2 ,
                                       short AV21DynamicFiltersOperator2 ,
                                       decimal AV22ContratoServicosPrazoRegra_Inicio2 ,
                                       String AV24DynamicFiltersSelector3 ,
                                       short AV25DynamicFiltersOperator3 ,
                                       decimal AV26ContratoServicosPrazoRegra_Inicio3 ,
                                       bool AV19DynamicFiltersEnabled2 ,
                                       bool AV23DynamicFiltersEnabled3 ,
                                       short AV32TFContratoServicosPrazoRegra_Sequencial ,
                                       short AV33TFContratoServicosPrazoRegra_Sequencial_To ,
                                       decimal AV36TFContratoServicosPrazoRegra_Inicio ,
                                       decimal AV37TFContratoServicosPrazoRegra_Inicio_To ,
                                       decimal AV40TFContratoServicosPrazoRegra_Fim ,
                                       decimal AV41TFContratoServicosPrazoRegra_Fim_To ,
                                       short AV44TFContratoServicosPrazoRegra_Dias ,
                                       short AV45TFContratoServicosPrazoRegra_Dias_To ,
                                       int AV7InContratoServicosPrazo_CntSrvCod ,
                                       String AV34ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace ,
                                       String AV38ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace ,
                                       String AV42ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace ,
                                       String AV46ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFG22( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A906ContratoServicosPrazoRegra_Sequencial), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL", StringUtil.LTrim( StringUtil.NToC( (decimal)(A906ContratoServicosPrazoRegra_Sequencial), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSPRAZOREGRA_INICIO", GetSecureSignedToken( "", context.localUtil.Format( A907ContratoServicosPrazoRegra_Inicio, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSPRAZOREGRA_INICIO", StringUtil.LTrim( StringUtil.NToC( A907ContratoServicosPrazoRegra_Inicio, 14, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSPRAZOREGRA_FIM", GetSecureSignedToken( "", context.localUtil.Format( A908ContratoServicosPrazoRegra_Fim, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSPRAZOREGRA_FIM", StringUtil.LTrim( StringUtil.NToC( A908ContratoServicosPrazoRegra_Fim, 14, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSPRAZOREGRA_DIAS", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A909ContratoServicosPrazoRegra_Dias), "ZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSPRAZOREGRA_DIAS", StringUtil.LTrim( StringUtil.NToC( (decimal)(A909ContratoServicosPrazoRegra_Dias), 3, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV24DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV24DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV25DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFG22( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFG22( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 80;
         /* Execute user event: E24G22 */
         E24G22 ();
         nGXsfl_80_idx = 1;
         sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
         SubsflControlProps_802( ) ;
         nGXsfl_80_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_802( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV16DynamicFiltersSelector1 ,
                                                 AV17DynamicFiltersOperator1 ,
                                                 AV18ContratoServicosPrazoRegra_Inicio1 ,
                                                 AV19DynamicFiltersEnabled2 ,
                                                 AV20DynamicFiltersSelector2 ,
                                                 AV21DynamicFiltersOperator2 ,
                                                 AV22ContratoServicosPrazoRegra_Inicio2 ,
                                                 AV23DynamicFiltersEnabled3 ,
                                                 AV24DynamicFiltersSelector3 ,
                                                 AV25DynamicFiltersOperator3 ,
                                                 AV26ContratoServicosPrazoRegra_Inicio3 ,
                                                 AV32TFContratoServicosPrazoRegra_Sequencial ,
                                                 AV33TFContratoServicosPrazoRegra_Sequencial_To ,
                                                 AV36TFContratoServicosPrazoRegra_Inicio ,
                                                 AV37TFContratoServicosPrazoRegra_Inicio_To ,
                                                 AV40TFContratoServicosPrazoRegra_Fim ,
                                                 AV41TFContratoServicosPrazoRegra_Fim_To ,
                                                 AV44TFContratoServicosPrazoRegra_Dias ,
                                                 AV45TFContratoServicosPrazoRegra_Dias_To ,
                                                 A907ContratoServicosPrazoRegra_Inicio ,
                                                 A906ContratoServicosPrazoRegra_Sequencial ,
                                                 A908ContratoServicosPrazoRegra_Fim ,
                                                 A909ContratoServicosPrazoRegra_Dias ,
                                                 AV14OrderedBy ,
                                                 AV15OrderedDsc ,
                                                 A903ContratoServicosPrazo_CntSrvCod ,
                                                 AV7InContratoServicosPrazo_CntSrvCod },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                                 TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DECIMAL,
                                                 TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            /* Using cursor H00G22 */
            pr_default.execute(0, new Object[] {AV7InContratoServicosPrazo_CntSrvCod, AV18ContratoServicosPrazoRegra_Inicio1, AV18ContratoServicosPrazoRegra_Inicio1, AV18ContratoServicosPrazoRegra_Inicio1, AV22ContratoServicosPrazoRegra_Inicio2, AV22ContratoServicosPrazoRegra_Inicio2, AV22ContratoServicosPrazoRegra_Inicio2, AV26ContratoServicosPrazoRegra_Inicio3, AV26ContratoServicosPrazoRegra_Inicio3, AV26ContratoServicosPrazoRegra_Inicio3, AV32TFContratoServicosPrazoRegra_Sequencial, AV33TFContratoServicosPrazoRegra_Sequencial_To, AV36TFContratoServicosPrazoRegra_Inicio, AV37TFContratoServicosPrazoRegra_Inicio_To, AV40TFContratoServicosPrazoRegra_Fim, AV41TFContratoServicosPrazoRegra_Fim_To, AV44TFContratoServicosPrazoRegra_Dias, AV45TFContratoServicosPrazoRegra_Dias_To, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_80_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A903ContratoServicosPrazo_CntSrvCod = H00G22_A903ContratoServicosPrazo_CntSrvCod[0];
               A904ContratoServicosPrazo_Tipo = H00G22_A904ContratoServicosPrazo_Tipo[0];
               A909ContratoServicosPrazoRegra_Dias = H00G22_A909ContratoServicosPrazoRegra_Dias[0];
               A908ContratoServicosPrazoRegra_Fim = H00G22_A908ContratoServicosPrazoRegra_Fim[0];
               A907ContratoServicosPrazoRegra_Inicio = H00G22_A907ContratoServicosPrazoRegra_Inicio[0];
               A906ContratoServicosPrazoRegra_Sequencial = H00G22_A906ContratoServicosPrazoRegra_Sequencial[0];
               A904ContratoServicosPrazo_Tipo = H00G22_A904ContratoServicosPrazo_Tipo[0];
               /* Execute user event: E25G22 */
               E25G22 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 80;
            WBG20( ) ;
         }
         nGXsfl_80_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV16DynamicFiltersSelector1 ,
                                              AV17DynamicFiltersOperator1 ,
                                              AV18ContratoServicosPrazoRegra_Inicio1 ,
                                              AV19DynamicFiltersEnabled2 ,
                                              AV20DynamicFiltersSelector2 ,
                                              AV21DynamicFiltersOperator2 ,
                                              AV22ContratoServicosPrazoRegra_Inicio2 ,
                                              AV23DynamicFiltersEnabled3 ,
                                              AV24DynamicFiltersSelector3 ,
                                              AV25DynamicFiltersOperator3 ,
                                              AV26ContratoServicosPrazoRegra_Inicio3 ,
                                              AV32TFContratoServicosPrazoRegra_Sequencial ,
                                              AV33TFContratoServicosPrazoRegra_Sequencial_To ,
                                              AV36TFContratoServicosPrazoRegra_Inicio ,
                                              AV37TFContratoServicosPrazoRegra_Inicio_To ,
                                              AV40TFContratoServicosPrazoRegra_Fim ,
                                              AV41TFContratoServicosPrazoRegra_Fim_To ,
                                              AV44TFContratoServicosPrazoRegra_Dias ,
                                              AV45TFContratoServicosPrazoRegra_Dias_To ,
                                              A907ContratoServicosPrazoRegra_Inicio ,
                                              A906ContratoServicosPrazoRegra_Sequencial ,
                                              A908ContratoServicosPrazoRegra_Fim ,
                                              A909ContratoServicosPrazoRegra_Dias ,
                                              AV14OrderedBy ,
                                              AV15OrderedDsc ,
                                              A903ContratoServicosPrazo_CntSrvCod ,
                                              AV7InContratoServicosPrazo_CntSrvCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DECIMAL,
                                              TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         /* Using cursor H00G23 */
         pr_default.execute(1, new Object[] {AV7InContratoServicosPrazo_CntSrvCod, AV18ContratoServicosPrazoRegra_Inicio1, AV18ContratoServicosPrazoRegra_Inicio1, AV18ContratoServicosPrazoRegra_Inicio1, AV22ContratoServicosPrazoRegra_Inicio2, AV22ContratoServicosPrazoRegra_Inicio2, AV22ContratoServicosPrazoRegra_Inicio2, AV26ContratoServicosPrazoRegra_Inicio3, AV26ContratoServicosPrazoRegra_Inicio3, AV26ContratoServicosPrazoRegra_Inicio3, AV32TFContratoServicosPrazoRegra_Sequencial, AV33TFContratoServicosPrazoRegra_Sequencial_To, AV36TFContratoServicosPrazoRegra_Inicio, AV37TFContratoServicosPrazoRegra_Inicio_To, AV40TFContratoServicosPrazoRegra_Fim, AV41TFContratoServicosPrazoRegra_Fim_To, AV44TFContratoServicosPrazoRegra_Dias, AV45TFContratoServicosPrazoRegra_Dias_To});
         GRID_nRecordCount = H00G23_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContratoServicosPrazoRegra_Inicio1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22ContratoServicosPrazoRegra_Inicio2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26ContratoServicosPrazoRegra_Inicio3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV32TFContratoServicosPrazoRegra_Sequencial, AV33TFContratoServicosPrazoRegra_Sequencial_To, AV36TFContratoServicosPrazoRegra_Inicio, AV37TFContratoServicosPrazoRegra_Inicio_To, AV40TFContratoServicosPrazoRegra_Fim, AV41TFContratoServicosPrazoRegra_Fim_To, AV44TFContratoServicosPrazoRegra_Dias, AV45TFContratoServicosPrazoRegra_Dias_To, AV7InContratoServicosPrazo_CntSrvCod, AV34ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace, AV38ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace, AV42ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace, AV46ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContratoServicosPrazoRegra_Inicio1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22ContratoServicosPrazoRegra_Inicio2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26ContratoServicosPrazoRegra_Inicio3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV32TFContratoServicosPrazoRegra_Sequencial, AV33TFContratoServicosPrazoRegra_Sequencial_To, AV36TFContratoServicosPrazoRegra_Inicio, AV37TFContratoServicosPrazoRegra_Inicio_To, AV40TFContratoServicosPrazoRegra_Fim, AV41TFContratoServicosPrazoRegra_Fim_To, AV44TFContratoServicosPrazoRegra_Dias, AV45TFContratoServicosPrazoRegra_Dias_To, AV7InContratoServicosPrazo_CntSrvCod, AV34ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace, AV38ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace, AV42ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace, AV46ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContratoServicosPrazoRegra_Inicio1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22ContratoServicosPrazoRegra_Inicio2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26ContratoServicosPrazoRegra_Inicio3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV32TFContratoServicosPrazoRegra_Sequencial, AV33TFContratoServicosPrazoRegra_Sequencial_To, AV36TFContratoServicosPrazoRegra_Inicio, AV37TFContratoServicosPrazoRegra_Inicio_To, AV40TFContratoServicosPrazoRegra_Fim, AV41TFContratoServicosPrazoRegra_Fim_To, AV44TFContratoServicosPrazoRegra_Dias, AV45TFContratoServicosPrazoRegra_Dias_To, AV7InContratoServicosPrazo_CntSrvCod, AV34ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace, AV38ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace, AV42ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace, AV46ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContratoServicosPrazoRegra_Inicio1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22ContratoServicosPrazoRegra_Inicio2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26ContratoServicosPrazoRegra_Inicio3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV32TFContratoServicosPrazoRegra_Sequencial, AV33TFContratoServicosPrazoRegra_Sequencial_To, AV36TFContratoServicosPrazoRegra_Inicio, AV37TFContratoServicosPrazoRegra_Inicio_To, AV40TFContratoServicosPrazoRegra_Fim, AV41TFContratoServicosPrazoRegra_Fim_To, AV44TFContratoServicosPrazoRegra_Dias, AV45TFContratoServicosPrazoRegra_Dias_To, AV7InContratoServicosPrazo_CntSrvCod, AV34ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace, AV38ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace, AV42ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace, AV46ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContratoServicosPrazoRegra_Inicio1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22ContratoServicosPrazoRegra_Inicio2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26ContratoServicosPrazoRegra_Inicio3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV32TFContratoServicosPrazoRegra_Sequencial, AV33TFContratoServicosPrazoRegra_Sequencial_To, AV36TFContratoServicosPrazoRegra_Inicio, AV37TFContratoServicosPrazoRegra_Inicio_To, AV40TFContratoServicosPrazoRegra_Fim, AV41TFContratoServicosPrazoRegra_Fim_To, AV44TFContratoServicosPrazoRegra_Dias, AV45TFContratoServicosPrazoRegra_Dias_To, AV7InContratoServicosPrazo_CntSrvCod, AV34ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace, AV38ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace, AV42ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace, AV46ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace) ;
         }
         return (int)(0) ;
      }

      protected void STRUPG20( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E23G22 */
         E23G22 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV47DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSERVICOSPRAZOREGRA_SEQUENCIALTITLEFILTERDATA"), AV31ContratoServicosPrazoRegra_SequencialTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSERVICOSPRAZOREGRA_INICIOTITLEFILTERDATA"), AV35ContratoServicosPrazoRegra_InicioTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSERVICOSPRAZOREGRA_FIMTITLEFILTERDATA"), AV39ContratoServicosPrazoRegra_FimTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOSERVICOSPRAZOREGRA_DIASTITLEFILTERDATA"), AV43ContratoServicosPrazoRegra_DiasTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV14OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV16DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContratoservicosprazoregra_inicio1_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContratoservicosprazoregra_inicio1_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATOSERVICOSPRAZOREGRA_INICIO1");
               GX_FocusControl = edtavContratoservicosprazoregra_inicio1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV18ContratoServicosPrazoRegra_Inicio1 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContratoServicosPrazoRegra_Inicio1", StringUtil.LTrim( StringUtil.Str( AV18ContratoServicosPrazoRegra_Inicio1, 14, 5)));
            }
            else
            {
               AV18ContratoServicosPrazoRegra_Inicio1 = context.localUtil.CToN( cgiGet( edtavContratoservicosprazoregra_inicio1_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContratoServicosPrazoRegra_Inicio1", StringUtil.LTrim( StringUtil.Str( AV18ContratoServicosPrazoRegra_Inicio1, 14, 5)));
            }
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV20DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContratoservicosprazoregra_inicio2_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContratoservicosprazoregra_inicio2_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATOSERVICOSPRAZOREGRA_INICIO2");
               GX_FocusControl = edtavContratoservicosprazoregra_inicio2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV22ContratoServicosPrazoRegra_Inicio2 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContratoServicosPrazoRegra_Inicio2", StringUtil.LTrim( StringUtil.Str( AV22ContratoServicosPrazoRegra_Inicio2, 14, 5)));
            }
            else
            {
               AV22ContratoServicosPrazoRegra_Inicio2 = context.localUtil.CToN( cgiGet( edtavContratoservicosprazoregra_inicio2_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContratoServicosPrazoRegra_Inicio2", StringUtil.LTrim( StringUtil.Str( AV22ContratoServicosPrazoRegra_Inicio2, 14, 5)));
            }
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV24DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV25DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContratoservicosprazoregra_inicio3_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContratoservicosprazoregra_inicio3_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATOSERVICOSPRAZOREGRA_INICIO3");
               GX_FocusControl = edtavContratoservicosprazoregra_inicio3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV26ContratoServicosPrazoRegra_Inicio3 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ContratoServicosPrazoRegra_Inicio3", StringUtil.LTrim( StringUtil.Str( AV26ContratoServicosPrazoRegra_Inicio3, 14, 5)));
            }
            else
            {
               AV26ContratoServicosPrazoRegra_Inicio3 = context.localUtil.CToN( cgiGet( edtavContratoservicosprazoregra_inicio3_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ContratoServicosPrazoRegra_Inicio3", StringUtil.LTrim( StringUtil.Str( AV26ContratoServicosPrazoRegra_Inicio3, 14, 5)));
            }
            AV19DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
            AV23DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_sequencial_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_sequencial_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL");
               GX_FocusControl = edtavTfcontratoservicosprazoregra_sequencial_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV32TFContratoServicosPrazoRegra_Sequencial = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFContratoServicosPrazoRegra_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFContratoServicosPrazoRegra_Sequencial), 4, 0)));
            }
            else
            {
               AV32TFContratoServicosPrazoRegra_Sequencial = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_sequencial_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFContratoServicosPrazoRegra_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFContratoServicosPrazoRegra_Sequencial), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_sequencial_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_sequencial_to_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_TO");
               GX_FocusControl = edtavTfcontratoservicosprazoregra_sequencial_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV33TFContratoServicosPrazoRegra_Sequencial_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33TFContratoServicosPrazoRegra_Sequencial_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33TFContratoServicosPrazoRegra_Sequencial_To), 4, 0)));
            }
            else
            {
               AV33TFContratoServicosPrazoRegra_Sequencial_To = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_sequencial_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33TFContratoServicosPrazoRegra_Sequencial_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33TFContratoServicosPrazoRegra_Sequencial_To), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_inicio_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_inicio_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSPRAZOREGRA_INICIO");
               GX_FocusControl = edtavTfcontratoservicosprazoregra_inicio_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV36TFContratoServicosPrazoRegra_Inicio = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFContratoServicosPrazoRegra_Inicio", StringUtil.LTrim( StringUtil.Str( AV36TFContratoServicosPrazoRegra_Inicio, 14, 5)));
            }
            else
            {
               AV36TFContratoServicosPrazoRegra_Inicio = context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_inicio_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFContratoServicosPrazoRegra_Inicio", StringUtil.LTrim( StringUtil.Str( AV36TFContratoServicosPrazoRegra_Inicio, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_inicio_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_inicio_to_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSPRAZOREGRA_INICIO_TO");
               GX_FocusControl = edtavTfcontratoservicosprazoregra_inicio_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV37TFContratoServicosPrazoRegra_Inicio_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFContratoServicosPrazoRegra_Inicio_To", StringUtil.LTrim( StringUtil.Str( AV37TFContratoServicosPrazoRegra_Inicio_To, 14, 5)));
            }
            else
            {
               AV37TFContratoServicosPrazoRegra_Inicio_To = context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_inicio_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFContratoServicosPrazoRegra_Inicio_To", StringUtil.LTrim( StringUtil.Str( AV37TFContratoServicosPrazoRegra_Inicio_To, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_fim_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_fim_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSPRAZOREGRA_FIM");
               GX_FocusControl = edtavTfcontratoservicosprazoregra_fim_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV40TFContratoServicosPrazoRegra_Fim = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContratoServicosPrazoRegra_Fim", StringUtil.LTrim( StringUtil.Str( AV40TFContratoServicosPrazoRegra_Fim, 14, 5)));
            }
            else
            {
               AV40TFContratoServicosPrazoRegra_Fim = context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_fim_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContratoServicosPrazoRegra_Fim", StringUtil.LTrim( StringUtil.Str( AV40TFContratoServicosPrazoRegra_Fim, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_fim_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_fim_to_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSPRAZOREGRA_FIM_TO");
               GX_FocusControl = edtavTfcontratoservicosprazoregra_fim_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV41TFContratoServicosPrazoRegra_Fim_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFContratoServicosPrazoRegra_Fim_To", StringUtil.LTrim( StringUtil.Str( AV41TFContratoServicosPrazoRegra_Fim_To, 14, 5)));
            }
            else
            {
               AV41TFContratoServicosPrazoRegra_Fim_To = context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_fim_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFContratoServicosPrazoRegra_Fim_To", StringUtil.LTrim( StringUtil.Str( AV41TFContratoServicosPrazoRegra_Fim_To, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_dias_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_dias_Internalname), ",", ".") > Convert.ToDecimal( 999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSPRAZOREGRA_DIAS");
               GX_FocusControl = edtavTfcontratoservicosprazoregra_dias_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV44TFContratoServicosPrazoRegra_Dias = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContratoServicosPrazoRegra_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFContratoServicosPrazoRegra_Dias), 3, 0)));
            }
            else
            {
               AV44TFContratoServicosPrazoRegra_Dias = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_dias_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContratoServicosPrazoRegra_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFContratoServicosPrazoRegra_Dias), 3, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_dias_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_dias_to_Internalname), ",", ".") > Convert.ToDecimal( 999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSPRAZOREGRA_DIAS_TO");
               GX_FocusControl = edtavTfcontratoservicosprazoregra_dias_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV45TFContratoServicosPrazoRegra_Dias_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContratoServicosPrazoRegra_Dias_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFContratoServicosPrazoRegra_Dias_To), 3, 0)));
            }
            else
            {
               AV45TFContratoServicosPrazoRegra_Dias_To = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratoservicosprazoregra_dias_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContratoServicosPrazoRegra_Dias_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFContratoServicosPrazoRegra_Dias_To), 3, 0)));
            }
            AV34ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicosprazoregra_sequencialtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace", AV34ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace);
            AV38ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicosprazoregra_iniciotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace", AV38ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace);
            AV42ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicosprazoregra_fimtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace", AV42ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace);
            AV46ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicosprazoregra_diastitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace", AV46ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_80 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_80"), ",", "."));
            AV49GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV50GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_contratoservicosprazoregra_sequencial_Caption = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Caption");
            Ddo_contratoservicosprazoregra_sequencial_Tooltip = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Tooltip");
            Ddo_contratoservicosprazoregra_sequencial_Cls = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Cls");
            Ddo_contratoservicosprazoregra_sequencial_Filteredtext_set = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Filteredtext_set");
            Ddo_contratoservicosprazoregra_sequencial_Filteredtextto_set = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Filteredtextto_set");
            Ddo_contratoservicosprazoregra_sequencial_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Dropdownoptionstype");
            Ddo_contratoservicosprazoregra_sequencial_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Titlecontrolidtoreplace");
            Ddo_contratoservicosprazoregra_sequencial_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Includesortasc"));
            Ddo_contratoservicosprazoregra_sequencial_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Includesortdsc"));
            Ddo_contratoservicosprazoregra_sequencial_Sortedstatus = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Sortedstatus");
            Ddo_contratoservicosprazoregra_sequencial_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Includefilter"));
            Ddo_contratoservicosprazoregra_sequencial_Filtertype = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Filtertype");
            Ddo_contratoservicosprazoregra_sequencial_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Filterisrange"));
            Ddo_contratoservicosprazoregra_sequencial_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Includedatalist"));
            Ddo_contratoservicosprazoregra_sequencial_Sortasc = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Sortasc");
            Ddo_contratoservicosprazoregra_sequencial_Sortdsc = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Sortdsc");
            Ddo_contratoservicosprazoregra_sequencial_Cleanfilter = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Cleanfilter");
            Ddo_contratoservicosprazoregra_sequencial_Rangefilterfrom = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Rangefilterfrom");
            Ddo_contratoservicosprazoregra_sequencial_Rangefilterto = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Rangefilterto");
            Ddo_contratoservicosprazoregra_sequencial_Searchbuttontext = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Searchbuttontext");
            Ddo_contratoservicosprazoregra_inicio_Caption = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Caption");
            Ddo_contratoservicosprazoregra_inicio_Tooltip = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Tooltip");
            Ddo_contratoservicosprazoregra_inicio_Cls = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Cls");
            Ddo_contratoservicosprazoregra_inicio_Filteredtext_set = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Filteredtext_set");
            Ddo_contratoservicosprazoregra_inicio_Filteredtextto_set = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Filteredtextto_set");
            Ddo_contratoservicosprazoregra_inicio_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Dropdownoptionstype");
            Ddo_contratoservicosprazoregra_inicio_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Titlecontrolidtoreplace");
            Ddo_contratoservicosprazoregra_inicio_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Includesortasc"));
            Ddo_contratoservicosprazoregra_inicio_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Includesortdsc"));
            Ddo_contratoservicosprazoregra_inicio_Sortedstatus = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Sortedstatus");
            Ddo_contratoservicosprazoregra_inicio_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Includefilter"));
            Ddo_contratoservicosprazoregra_inicio_Filtertype = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Filtertype");
            Ddo_contratoservicosprazoregra_inicio_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Filterisrange"));
            Ddo_contratoservicosprazoregra_inicio_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Includedatalist"));
            Ddo_contratoservicosprazoregra_inicio_Sortasc = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Sortasc");
            Ddo_contratoservicosprazoregra_inicio_Sortdsc = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Sortdsc");
            Ddo_contratoservicosprazoregra_inicio_Cleanfilter = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Cleanfilter");
            Ddo_contratoservicosprazoregra_inicio_Rangefilterfrom = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Rangefilterfrom");
            Ddo_contratoservicosprazoregra_inicio_Rangefilterto = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Rangefilterto");
            Ddo_contratoservicosprazoregra_inicio_Searchbuttontext = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Searchbuttontext");
            Ddo_contratoservicosprazoregra_fim_Caption = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Caption");
            Ddo_contratoservicosprazoregra_fim_Tooltip = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Tooltip");
            Ddo_contratoservicosprazoregra_fim_Cls = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Cls");
            Ddo_contratoservicosprazoregra_fim_Filteredtext_set = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Filteredtext_set");
            Ddo_contratoservicosprazoregra_fim_Filteredtextto_set = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Filteredtextto_set");
            Ddo_contratoservicosprazoregra_fim_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Dropdownoptionstype");
            Ddo_contratoservicosprazoregra_fim_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Titlecontrolidtoreplace");
            Ddo_contratoservicosprazoregra_fim_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Includesortasc"));
            Ddo_contratoservicosprazoregra_fim_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Includesortdsc"));
            Ddo_contratoservicosprazoregra_fim_Sortedstatus = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Sortedstatus");
            Ddo_contratoservicosprazoregra_fim_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Includefilter"));
            Ddo_contratoservicosprazoregra_fim_Filtertype = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Filtertype");
            Ddo_contratoservicosprazoregra_fim_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Filterisrange"));
            Ddo_contratoservicosprazoregra_fim_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Includedatalist"));
            Ddo_contratoservicosprazoregra_fim_Sortasc = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Sortasc");
            Ddo_contratoservicosprazoregra_fim_Sortdsc = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Sortdsc");
            Ddo_contratoservicosprazoregra_fim_Cleanfilter = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Cleanfilter");
            Ddo_contratoservicosprazoregra_fim_Rangefilterfrom = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Rangefilterfrom");
            Ddo_contratoservicosprazoregra_fim_Rangefilterto = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Rangefilterto");
            Ddo_contratoservicosprazoregra_fim_Searchbuttontext = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Searchbuttontext");
            Ddo_contratoservicosprazoregra_dias_Caption = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Caption");
            Ddo_contratoservicosprazoregra_dias_Tooltip = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Tooltip");
            Ddo_contratoservicosprazoregra_dias_Cls = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Cls");
            Ddo_contratoservicosprazoregra_dias_Filteredtext_set = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Filteredtext_set");
            Ddo_contratoservicosprazoregra_dias_Filteredtextto_set = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Filteredtextto_set");
            Ddo_contratoservicosprazoregra_dias_Dropdownoptionstype = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Dropdownoptionstype");
            Ddo_contratoservicosprazoregra_dias_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Titlecontrolidtoreplace");
            Ddo_contratoservicosprazoregra_dias_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Includesortasc"));
            Ddo_contratoservicosprazoregra_dias_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Includesortdsc"));
            Ddo_contratoservicosprazoregra_dias_Sortedstatus = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Sortedstatus");
            Ddo_contratoservicosprazoregra_dias_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Includefilter"));
            Ddo_contratoservicosprazoregra_dias_Filtertype = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Filtertype");
            Ddo_contratoservicosprazoregra_dias_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Filterisrange"));
            Ddo_contratoservicosprazoregra_dias_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Includedatalist"));
            Ddo_contratoservicosprazoregra_dias_Sortasc = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Sortasc");
            Ddo_contratoservicosprazoregra_dias_Sortdsc = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Sortdsc");
            Ddo_contratoservicosprazoregra_dias_Cleanfilter = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Cleanfilter");
            Ddo_contratoservicosprazoregra_dias_Rangefilterfrom = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Rangefilterfrom");
            Ddo_contratoservicosprazoregra_dias_Rangefilterto = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Rangefilterto");
            Ddo_contratoservicosprazoregra_dias_Searchbuttontext = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contratoservicosprazoregra_sequencial_Activeeventkey = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Activeeventkey");
            Ddo_contratoservicosprazoregra_sequencial_Filteredtext_get = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Filteredtext_get");
            Ddo_contratoservicosprazoregra_sequencial_Filteredtextto_get = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_Filteredtextto_get");
            Ddo_contratoservicosprazoregra_inicio_Activeeventkey = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Activeeventkey");
            Ddo_contratoservicosprazoregra_inicio_Filteredtext_get = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Filteredtext_get");
            Ddo_contratoservicosprazoregra_inicio_Filteredtextto_get = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO_Filteredtextto_get");
            Ddo_contratoservicosprazoregra_fim_Activeeventkey = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Activeeventkey");
            Ddo_contratoservicosprazoregra_fim_Filteredtext_get = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Filteredtext_get");
            Ddo_contratoservicosprazoregra_fim_Filteredtextto_get = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM_Filteredtextto_get");
            Ddo_contratoservicosprazoregra_dias_Activeeventkey = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Activeeventkey");
            Ddo_contratoservicosprazoregra_dias_Filteredtext_get = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Filteredtext_get");
            Ddo_contratoservicosprazoregra_dias_Filteredtextto_get = cgiGet( "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV15OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV17DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATOSERVICOSPRAZOREGRA_INICIO1"), ",", ".") != AV18ContratoServicosPrazoRegra_Inicio1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV21DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATOSERVICOSPRAZOREGRA_INICIO2"), ",", ".") != AV22ContratoServicosPrazoRegra_Inicio2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV24DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV25DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATOSERVICOSPRAZOREGRA_INICIO3"), ",", ".") != AV26ContratoServicosPrazoRegra_Inicio3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV23DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL"), ",", ".") != Convert.ToDecimal( AV32TFContratoServicosPrazoRegra_Sequencial )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_TO"), ",", ".") != Convert.ToDecimal( AV33TFContratoServicosPrazoRegra_Sequencial_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSPRAZOREGRA_INICIO"), ",", ".") != AV36TFContratoServicosPrazoRegra_Inicio )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSPRAZOREGRA_INICIO_TO"), ",", ".") != AV37TFContratoServicosPrazoRegra_Inicio_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSPRAZOREGRA_FIM"), ",", ".") != AV40TFContratoServicosPrazoRegra_Fim )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSPRAZOREGRA_FIM_TO"), ",", ".") != AV41TFContratoServicosPrazoRegra_Fim_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSPRAZOREGRA_DIAS"), ",", ".") != Convert.ToDecimal( AV44TFContratoServicosPrazoRegra_Dias )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOSERVICOSPRAZOREGRA_DIAS_TO"), ",", ".") != Convert.ToDecimal( AV45TFContratoServicosPrazoRegra_Dias_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E23G22 */
         E23G22 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E23G22( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV16DynamicFiltersSelector1 = "CONTRATOSERVICOSPRAZOREGRA_INICIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV20DynamicFiltersSelector2 = "CONTRATOSERVICOSPRAZOREGRA_INICIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV24DynamicFiltersSelector3 = "CONTRATOSERVICOSPRAZOREGRA_INICIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfcontratoservicosprazoregra_sequencial_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosprazoregra_sequencial_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosprazoregra_sequencial_Visible), 5, 0)));
         edtavTfcontratoservicosprazoregra_sequencial_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosprazoregra_sequencial_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosprazoregra_sequencial_to_Visible), 5, 0)));
         edtavTfcontratoservicosprazoregra_inicio_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosprazoregra_inicio_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosprazoregra_inicio_Visible), 5, 0)));
         edtavTfcontratoservicosprazoregra_inicio_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosprazoregra_inicio_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosprazoregra_inicio_to_Visible), 5, 0)));
         edtavTfcontratoservicosprazoregra_fim_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosprazoregra_fim_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosprazoregra_fim_Visible), 5, 0)));
         edtavTfcontratoservicosprazoregra_fim_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosprazoregra_fim_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosprazoregra_fim_to_Visible), 5, 0)));
         edtavTfcontratoservicosprazoregra_dias_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosprazoregra_dias_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosprazoregra_dias_Visible), 5, 0)));
         edtavTfcontratoservicosprazoregra_dias_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoservicosprazoregra_dias_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicosprazoregra_dias_to_Visible), 5, 0)));
         Ddo_contratoservicosprazoregra_sequencial_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosPrazoRegra_Sequencial";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazoregra_sequencial_Internalname, "TitleControlIdToReplace", Ddo_contratoservicosprazoregra_sequencial_Titlecontrolidtoreplace);
         AV34ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace = Ddo_contratoservicosprazoregra_sequencial_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace", AV34ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace);
         edtavDdo_contratoservicosprazoregra_sequencialtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoservicosprazoregra_sequencialtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicosprazoregra_sequencialtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicosprazoregra_inicio_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosPrazoRegra_Inicio";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazoregra_inicio_Internalname, "TitleControlIdToReplace", Ddo_contratoservicosprazoregra_inicio_Titlecontrolidtoreplace);
         AV38ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace = Ddo_contratoservicosprazoregra_inicio_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace", AV38ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace);
         edtavDdo_contratoservicosprazoregra_iniciotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoservicosprazoregra_iniciotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicosprazoregra_iniciotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicosprazoregra_fim_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosPrazoRegra_Fim";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazoregra_fim_Internalname, "TitleControlIdToReplace", Ddo_contratoservicosprazoregra_fim_Titlecontrolidtoreplace);
         AV42ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace = Ddo_contratoservicosprazoregra_fim_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace", AV42ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace);
         edtavDdo_contratoservicosprazoregra_fimtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoservicosprazoregra_fimtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicosprazoregra_fimtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicosprazoregra_dias_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosPrazoRegra_Dias";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazoregra_dias_Internalname, "TitleControlIdToReplace", Ddo_contratoservicosprazoregra_dias_Titlecontrolidtoreplace);
         AV46ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace = Ddo_contratoservicosprazoregra_dias_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace", AV46ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace);
         edtavDdo_contratoservicosprazoregra_diastitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoservicosprazoregra_diastitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicosprazoregra_diastitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Selecione Contrato Servicos Prazo Regra";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Tipo", 0);
         cmbavOrderedby.addItem("2", "Sequencial", 0);
         cmbavOrderedby.addItem("3", "Inicio", 0);
         cmbavOrderedby.addItem("4", "Fim", 0);
         cmbavOrderedby.addItem("5", "Dias", 0);
         if ( AV14OrderedBy < 1 )
         {
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV47DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV47DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E24G22( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV31ContratoServicosPrazoRegra_SequencialTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV35ContratoServicosPrazoRegra_InicioTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV39ContratoServicosPrazoRegra_FimTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV43ContratoServicosPrazoRegra_DiasTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         edtContratoServicosPrazoRegra_Sequencial_Titleformat = 2;
         edtContratoServicosPrazoRegra_Sequencial_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Sequencial", AV34ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrazoRegra_Sequencial_Internalname, "Title", edtContratoServicosPrazoRegra_Sequencial_Title);
         edtContratoServicosPrazoRegra_Inicio_Titleformat = 2;
         edtContratoServicosPrazoRegra_Inicio_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Inicio", AV38ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrazoRegra_Inicio_Internalname, "Title", edtContratoServicosPrazoRegra_Inicio_Title);
         edtContratoServicosPrazoRegra_Fim_Titleformat = 2;
         edtContratoServicosPrazoRegra_Fim_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Fim", AV42ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrazoRegra_Fim_Internalname, "Title", edtContratoServicosPrazoRegra_Fim_Title);
         edtContratoServicosPrazoRegra_Dias_Titleformat = 2;
         edtContratoServicosPrazoRegra_Dias_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Dias", AV46ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrazoRegra_Dias_Internalname, "Title", edtContratoServicosPrazoRegra_Dias_Title);
         AV49GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49GridCurrentPage), 10, 0)));
         AV50GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV31ContratoServicosPrazoRegra_SequencialTitleFilterData", AV31ContratoServicosPrazoRegra_SequencialTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV35ContratoServicosPrazoRegra_InicioTitleFilterData", AV35ContratoServicosPrazoRegra_InicioTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV39ContratoServicosPrazoRegra_FimTitleFilterData", AV39ContratoServicosPrazoRegra_FimTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV43ContratoServicosPrazoRegra_DiasTitleFilterData", AV43ContratoServicosPrazoRegra_DiasTitleFilterData);
      }

      protected void E11G22( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV48PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV48PageToGo) ;
         }
      }

      protected void E12G22( )
      {
         /* Ddo_contratoservicosprazoregra_sequencial_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicosprazoregra_sequencial_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contratoservicosprazoregra_sequencial_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazoregra_sequencial_Internalname, "SortedStatus", Ddo_contratoservicosprazoregra_sequencial_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosprazoregra_sequencial_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contratoservicosprazoregra_sequencial_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazoregra_sequencial_Internalname, "SortedStatus", Ddo_contratoservicosprazoregra_sequencial_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosprazoregra_sequencial_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV32TFContratoServicosPrazoRegra_Sequencial = (short)(NumberUtil.Val( Ddo_contratoservicosprazoregra_sequencial_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFContratoServicosPrazoRegra_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFContratoServicosPrazoRegra_Sequencial), 4, 0)));
            AV33TFContratoServicosPrazoRegra_Sequencial_To = (short)(NumberUtil.Val( Ddo_contratoservicosprazoregra_sequencial_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33TFContratoServicosPrazoRegra_Sequencial_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33TFContratoServicosPrazoRegra_Sequencial_To), 4, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13G22( )
      {
         /* Ddo_contratoservicosprazoregra_inicio_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicosprazoregra_inicio_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contratoservicosprazoregra_inicio_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazoregra_inicio_Internalname, "SortedStatus", Ddo_contratoservicosprazoregra_inicio_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosprazoregra_inicio_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contratoservicosprazoregra_inicio_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazoregra_inicio_Internalname, "SortedStatus", Ddo_contratoservicosprazoregra_inicio_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosprazoregra_inicio_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV36TFContratoServicosPrazoRegra_Inicio = NumberUtil.Val( Ddo_contratoservicosprazoregra_inicio_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFContratoServicosPrazoRegra_Inicio", StringUtil.LTrim( StringUtil.Str( AV36TFContratoServicosPrazoRegra_Inicio, 14, 5)));
            AV37TFContratoServicosPrazoRegra_Inicio_To = NumberUtil.Val( Ddo_contratoservicosprazoregra_inicio_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFContratoServicosPrazoRegra_Inicio_To", StringUtil.LTrim( StringUtil.Str( AV37TFContratoServicosPrazoRegra_Inicio_To, 14, 5)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14G22( )
      {
         /* Ddo_contratoservicosprazoregra_fim_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicosprazoregra_fim_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contratoservicosprazoregra_fim_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazoregra_fim_Internalname, "SortedStatus", Ddo_contratoservicosprazoregra_fim_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosprazoregra_fim_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contratoservicosprazoregra_fim_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazoregra_fim_Internalname, "SortedStatus", Ddo_contratoservicosprazoregra_fim_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosprazoregra_fim_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV40TFContratoServicosPrazoRegra_Fim = NumberUtil.Val( Ddo_contratoservicosprazoregra_fim_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContratoServicosPrazoRegra_Fim", StringUtil.LTrim( StringUtil.Str( AV40TFContratoServicosPrazoRegra_Fim, 14, 5)));
            AV41TFContratoServicosPrazoRegra_Fim_To = NumberUtil.Val( Ddo_contratoservicosprazoregra_fim_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFContratoServicosPrazoRegra_Fim_To", StringUtil.LTrim( StringUtil.Str( AV41TFContratoServicosPrazoRegra_Fim_To, 14, 5)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E15G22( )
      {
         /* Ddo_contratoservicosprazoregra_dias_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicosprazoregra_dias_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contratoservicosprazoregra_dias_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazoregra_dias_Internalname, "SortedStatus", Ddo_contratoservicosprazoregra_dias_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosprazoregra_dias_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contratoservicosprazoregra_dias_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazoregra_dias_Internalname, "SortedStatus", Ddo_contratoservicosprazoregra_dias_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicosprazoregra_dias_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV44TFContratoServicosPrazoRegra_Dias = (short)(NumberUtil.Val( Ddo_contratoservicosprazoregra_dias_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContratoServicosPrazoRegra_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFContratoServicosPrazoRegra_Dias), 3, 0)));
            AV45TFContratoServicosPrazoRegra_Dias_To = (short)(NumberUtil.Val( Ddo_contratoservicosprazoregra_dias_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContratoServicosPrazoRegra_Dias_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFContratoServicosPrazoRegra_Dias_To), 3, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E25G22( )
      {
         /* Grid_Load Routine */
         AV29Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV29Select);
         AV53Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 80;
         }
         sendrow_802( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_80_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(80, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E26G22 */
         E26G22 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E26G22( )
      {
         /* Enter Routine */
         AV8InOutContratoServicosPrazoRegra_Sequencial = A906ContratoServicosPrazoRegra_Sequencial;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContratoServicosPrazoRegra_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8InOutContratoServicosPrazoRegra_Sequencial), 4, 0)));
         AV9InOutContratoServicosPrazoRegra_Inicio = A907ContratoServicosPrazoRegra_Inicio;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9InOutContratoServicosPrazoRegra_Inicio", StringUtil.LTrim( StringUtil.Str( AV9InOutContratoServicosPrazoRegra_Inicio, 14, 5)));
         context.setWebReturnParms(new Object[] {(short)AV8InOutContratoServicosPrazoRegra_Sequencial,(decimal)AV9InOutContratoServicosPrazoRegra_Inicio});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E16G22( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E21G22( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV19DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E17G22( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV27DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         AV28DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersIgnoreFirst", AV28DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV27DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         AV28DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersIgnoreFirst", AV28DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContratoServicosPrazoRegra_Inicio1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22ContratoServicosPrazoRegra_Inicio2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26ContratoServicosPrazoRegra_Inicio3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV32TFContratoServicosPrazoRegra_Sequencial, AV33TFContratoServicosPrazoRegra_Sequencial_To, AV36TFContratoServicosPrazoRegra_Inicio, AV37TFContratoServicosPrazoRegra_Inicio_To, AV40TFContratoServicosPrazoRegra_Fim, AV41TFContratoServicosPrazoRegra_Fim_To, AV44TFContratoServicosPrazoRegra_Dias, AV45TFContratoServicosPrazoRegra_Dias_To, AV7InContratoServicosPrazo_CntSrvCod, AV34ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace, AV38ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace, AV42ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace, AV46ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E22G22( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV23DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E18G22( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV27DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV27DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContratoServicosPrazoRegra_Inicio1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22ContratoServicosPrazoRegra_Inicio2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26ContratoServicosPrazoRegra_Inicio3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV32TFContratoServicosPrazoRegra_Sequencial, AV33TFContratoServicosPrazoRegra_Sequencial_To, AV36TFContratoServicosPrazoRegra_Inicio, AV37TFContratoServicosPrazoRegra_Inicio_To, AV40TFContratoServicosPrazoRegra_Fim, AV41TFContratoServicosPrazoRegra_Fim_To, AV44TFContratoServicosPrazoRegra_Dias, AV45TFContratoServicosPrazoRegra_Dias_To, AV7InContratoServicosPrazo_CntSrvCod, AV34ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace, AV38ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace, AV42ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace, AV46ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E19G22( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV27DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         AV23DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV27DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18ContratoServicosPrazoRegra_Inicio1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22ContratoServicosPrazoRegra_Inicio2, AV24DynamicFiltersSelector3, AV25DynamicFiltersOperator3, AV26ContratoServicosPrazoRegra_Inicio3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV32TFContratoServicosPrazoRegra_Sequencial, AV33TFContratoServicosPrazoRegra_Sequencial_To, AV36TFContratoServicosPrazoRegra_Inicio, AV37TFContratoServicosPrazoRegra_Inicio_To, AV40TFContratoServicosPrazoRegra_Fim, AV41TFContratoServicosPrazoRegra_Fim_To, AV44TFContratoServicosPrazoRegra_Dias, AV45TFContratoServicosPrazoRegra_Dias_To, AV7InContratoServicosPrazo_CntSrvCod, AV34ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace, AV38ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace, AV42ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace, AV46ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E20G22( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void S162( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contratoservicosprazoregra_sequencial_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazoregra_sequencial_Internalname, "SortedStatus", Ddo_contratoservicosprazoregra_sequencial_Sortedstatus);
         Ddo_contratoservicosprazoregra_inicio_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazoregra_inicio_Internalname, "SortedStatus", Ddo_contratoservicosprazoregra_inicio_Sortedstatus);
         Ddo_contratoservicosprazoregra_fim_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazoregra_fim_Internalname, "SortedStatus", Ddo_contratoservicosprazoregra_fim_Sortedstatus);
         Ddo_contratoservicosprazoregra_dias_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazoregra_dias_Internalname, "SortedStatus", Ddo_contratoservicosprazoregra_dias_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV14OrderedBy == 2 )
         {
            Ddo_contratoservicosprazoregra_sequencial_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazoregra_sequencial_Internalname, "SortedStatus", Ddo_contratoservicosprazoregra_sequencial_Sortedstatus);
         }
         else if ( AV14OrderedBy == 3 )
         {
            Ddo_contratoservicosprazoregra_inicio_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazoregra_inicio_Internalname, "SortedStatus", Ddo_contratoservicosprazoregra_inicio_Sortedstatus);
         }
         else if ( AV14OrderedBy == 4 )
         {
            Ddo_contratoservicosprazoregra_fim_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazoregra_fim_Internalname, "SortedStatus", Ddo_contratoservicosprazoregra_fim_Sortedstatus);
         }
         else if ( AV14OrderedBy == 5 )
         {
            Ddo_contratoservicosprazoregra_dias_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazoregra_dias_Internalname, "SortedStatus", Ddo_contratoservicosprazoregra_dias_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavContratoservicosprazoregra_inicio1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoservicosprazoregra_inicio1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicosprazoregra_inicio1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATOSERVICOSPRAZOREGRA_INICIO") == 0 )
         {
            edtavContratoservicosprazoregra_inicio1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoservicosprazoregra_inicio1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicosprazoregra_inicio1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavContratoservicosprazoregra_inicio2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoservicosprazoregra_inicio2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicosprazoregra_inicio2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATOSERVICOSPRAZOREGRA_INICIO") == 0 )
         {
            edtavContratoservicosprazoregra_inicio2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoservicosprazoregra_inicio2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicosprazoregra_inicio2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavContratoservicosprazoregra_inicio3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoservicosprazoregra_inicio3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicosprazoregra_inicio3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CONTRATOSERVICOSPRAZOREGRA_INICIO") == 0 )
         {
            edtavContratoservicosprazoregra_inicio3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoservicosprazoregra_inicio3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicosprazoregra_inicio3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S182( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         AV20DynamicFiltersSelector2 = "CONTRATOSERVICOSPRAZOREGRA_INICIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         AV21DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         AV22ContratoServicosPrazoRegra_Inicio2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContratoServicosPrazoRegra_Inicio2", StringUtil.LTrim( StringUtil.Str( AV22ContratoServicosPrazoRegra_Inicio2, 14, 5)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
         AV24DynamicFiltersSelector3 = "CONTRATOSERVICOSPRAZOREGRA_INICIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
         AV25DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)));
         AV26ContratoServicosPrazoRegra_Inicio3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ContratoServicosPrazoRegra_Inicio3", StringUtil.LTrim( StringUtil.Str( AV26ContratoServicosPrazoRegra_Inicio3, 14, 5)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S192( )
      {
         /* 'CLEANFILTERS' Routine */
         AV32TFContratoServicosPrazoRegra_Sequencial = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFContratoServicosPrazoRegra_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFContratoServicosPrazoRegra_Sequencial), 4, 0)));
         Ddo_contratoservicosprazoregra_sequencial_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazoregra_sequencial_Internalname, "FilteredText_set", Ddo_contratoservicosprazoregra_sequencial_Filteredtext_set);
         AV33TFContratoServicosPrazoRegra_Sequencial_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33TFContratoServicosPrazoRegra_Sequencial_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33TFContratoServicosPrazoRegra_Sequencial_To), 4, 0)));
         Ddo_contratoservicosprazoregra_sequencial_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazoregra_sequencial_Internalname, "FilteredTextTo_set", Ddo_contratoservicosprazoregra_sequencial_Filteredtextto_set);
         AV36TFContratoServicosPrazoRegra_Inicio = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFContratoServicosPrazoRegra_Inicio", StringUtil.LTrim( StringUtil.Str( AV36TFContratoServicosPrazoRegra_Inicio, 14, 5)));
         Ddo_contratoservicosprazoregra_inicio_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazoregra_inicio_Internalname, "FilteredText_set", Ddo_contratoservicosprazoregra_inicio_Filteredtext_set);
         AV37TFContratoServicosPrazoRegra_Inicio_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFContratoServicosPrazoRegra_Inicio_To", StringUtil.LTrim( StringUtil.Str( AV37TFContratoServicosPrazoRegra_Inicio_To, 14, 5)));
         Ddo_contratoservicosprazoregra_inicio_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazoregra_inicio_Internalname, "FilteredTextTo_set", Ddo_contratoservicosprazoregra_inicio_Filteredtextto_set);
         AV40TFContratoServicosPrazoRegra_Fim = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContratoServicosPrazoRegra_Fim", StringUtil.LTrim( StringUtil.Str( AV40TFContratoServicosPrazoRegra_Fim, 14, 5)));
         Ddo_contratoservicosprazoregra_fim_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazoregra_fim_Internalname, "FilteredText_set", Ddo_contratoservicosprazoregra_fim_Filteredtext_set);
         AV41TFContratoServicosPrazoRegra_Fim_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFContratoServicosPrazoRegra_Fim_To", StringUtil.LTrim( StringUtil.Str( AV41TFContratoServicosPrazoRegra_Fim_To, 14, 5)));
         Ddo_contratoservicosprazoregra_fim_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazoregra_fim_Internalname, "FilteredTextTo_set", Ddo_contratoservicosprazoregra_fim_Filteredtextto_set);
         AV44TFContratoServicosPrazoRegra_Dias = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContratoServicosPrazoRegra_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFContratoServicosPrazoRegra_Dias), 3, 0)));
         Ddo_contratoservicosprazoregra_dias_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazoregra_dias_Internalname, "FilteredText_set", Ddo_contratoservicosprazoregra_dias_Filteredtext_set);
         AV45TFContratoServicosPrazoRegra_Dias_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContratoServicosPrazoRegra_Dias_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFContratoServicosPrazoRegra_Dias_To), 3, 0)));
         Ddo_contratoservicosprazoregra_dias_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoservicosprazoregra_dias_Internalname, "FilteredTextTo_set", Ddo_contratoservicosprazoregra_dias_Filteredtextto_set);
         AV16DynamicFiltersSelector1 = "CONTRATOSERVICOSPRAZOREGRA_INICIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         AV17DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         AV18ContratoServicosPrazoRegra_Inicio1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContratoServicosPrazoRegra_Inicio1", StringUtil.LTrim( StringUtil.Str( AV18ContratoServicosPrazoRegra_Inicio1, 14, 5)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(1));
            AV16DynamicFiltersSelector1 = AV13GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATOSERVICOSPRAZOREGRA_INICIO") == 0 )
            {
               AV17DynamicFiltersOperator1 = AV13GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV18ContratoServicosPrazoRegra_Inicio1 = NumberUtil.Val( AV13GridStateDynamicFilter.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContratoServicosPrazoRegra_Inicio1", StringUtil.LTrim( StringUtil.Str( AV18ContratoServicosPrazoRegra_Inicio1, 14, 5)));
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV19DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(2));
               AV20DynamicFiltersSelector2 = AV13GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATOSERVICOSPRAZOREGRA_INICIO") == 0 )
               {
                  AV21DynamicFiltersOperator2 = AV13GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
                  AV22ContratoServicosPrazoRegra_Inicio2 = NumberUtil.Val( AV13GridStateDynamicFilter.gxTpr_Value, ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContratoServicosPrazoRegra_Inicio2", StringUtil.LTrim( StringUtil.Str( AV22ContratoServicosPrazoRegra_Inicio2, 14, 5)));
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV23DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
                  AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(3));
                  AV24DynamicFiltersSelector3 = AV13GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CONTRATOSERVICOSPRAZOREGRA_INICIO") == 0 )
                  {
                     AV25DynamicFiltersOperator3 = AV13GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)));
                     AV26ContratoServicosPrazoRegra_Inicio3 = NumberUtil.Val( AV13GridStateDynamicFilter.gxTpr_Value, ".");
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ContratoServicosPrazoRegra_Inicio3", StringUtil.LTrim( StringUtil.Str( AV26ContratoServicosPrazoRegra_Inicio3, 14, 5)));
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV27DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S172( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV28DynamicFiltersIgnoreFirst )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV16DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATOSERVICOSPRAZOREGRA_INICIO") == 0 ) && ! (Convert.ToDecimal(0)==AV18ContratoServicosPrazoRegra_Inicio1) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( AV18ContratoServicosPrazoRegra_Inicio1, 14, 5);
               AV13GridStateDynamicFilter.gxTpr_Operator = AV17DynamicFiltersOperator1;
            }
            if ( AV27DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV19DynamicFiltersEnabled2 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV20DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATOSERVICOSPRAZOREGRA_INICIO") == 0 ) && ! (Convert.ToDecimal(0)==AV22ContratoServicosPrazoRegra_Inicio2) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( AV22ContratoServicosPrazoRegra_Inicio2, 14, 5);
               AV13GridStateDynamicFilter.gxTpr_Operator = AV21DynamicFiltersOperator2;
            }
            if ( AV27DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV23DynamicFiltersEnabled3 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV24DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CONTRATOSERVICOSPRAZOREGRA_INICIO") == 0 ) && ! (Convert.ToDecimal(0)==AV26ContratoServicosPrazoRegra_Inicio3) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( AV26ContratoServicosPrazoRegra_Inicio3, 14, 5);
               AV13GridStateDynamicFilter.gxTpr_Operator = AV25DynamicFiltersOperator3;
            }
            if ( AV27DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_G22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_G22( true) ;
         }
         else
         {
            wb_table2_5_G22( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_G22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_74_G22( true) ;
         }
         else
         {
            wb_table3_74_G22( false) ;
         }
         return  ;
      }

      protected void wb_table3_74_G22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_G22e( true) ;
         }
         else
         {
            wb_table1_2_G22e( false) ;
         }
      }

      protected void wb_table3_74_G22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_77_G22( true) ;
         }
         else
         {
            wb_table4_77_G22( false) ;
         }
         return  ;
      }

      protected void wb_table4_77_G22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_74_G22e( true) ;
         }
         else
         {
            wb_table3_74_G22e( false) ;
         }
      }

      protected void wb_table4_77_G22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"80\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosPrazoRegra_Sequencial_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosPrazoRegra_Sequencial_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosPrazoRegra_Sequencial_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(94), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosPrazoRegra_Inicio_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosPrazoRegra_Inicio_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosPrazoRegra_Inicio_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(94), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosPrazoRegra_Fim_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosPrazoRegra_Fim_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosPrazoRegra_Fim_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(28), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosPrazoRegra_Dias_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosPrazoRegra_Dias_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosPrazoRegra_Dias_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV29Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A906ContratoServicosPrazoRegra_Sequencial), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosPrazoRegra_Sequencial_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrazoRegra_Sequencial_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A907ContratoServicosPrazoRegra_Inicio, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosPrazoRegra_Inicio_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrazoRegra_Inicio_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A908ContratoServicosPrazoRegra_Fim, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosPrazoRegra_Fim_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrazoRegra_Fim_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A909ContratoServicosPrazoRegra_Dias), 3, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosPrazoRegra_Dias_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrazoRegra_Dias_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 80 )
         {
            wbEnd = 0;
            nRC_GXsfl_80 = (short)(nGXsfl_80_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_77_G22e( true) ;
         }
         else
         {
            wb_table4_77_G22e( false) ;
         }
      }

      protected void wb_table2_5_G22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptContratoServicosPrazoContratoServicosPrazoRegra.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptContratoServicosPrazoContratoServicosPrazoRegra.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV15OrderedDsc), StringUtil.BoolToStr( AV15OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptContratoServicosPrazoContratoServicosPrazoRegra.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_14_G22( true) ;
         }
         else
         {
            wb_table5_14_G22( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_G22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_G22e( true) ;
         }
         else
         {
            wb_table2_5_G22e( false) ;
         }
      }

      protected void wb_table5_14_G22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoServicosPrazoContratoServicosPrazoRegra.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_19_G22( true) ;
         }
         else
         {
            wb_table6_19_G22( false) ;
         }
         return  ;
      }

      protected void wb_table6_19_G22e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptContratoServicosPrazoContratoServicosPrazoRegra.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_G22e( true) ;
         }
         else
         {
            wb_table5_14_G22e( false) ;
         }
      }

      protected void wb_table6_19_G22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoServicosPrazoContratoServicosPrazoRegra.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV16DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 7, "'"+""+"'"+",false,"+"'"+"e27g21_client"+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_PromptContratoServicosPrazoContratoServicosPrazoRegra.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoServicosPrazoContratoServicosPrazoRegra.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_G22( true) ;
         }
         else
         {
            wb_table7_28_G22( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_G22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoServicosPrazoContratoServicosPrazoRegra.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoServicosPrazoContratoServicosPrazoRegra.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoServicosPrazoContratoServicosPrazoRegra.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV20DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 7, "'"+""+"'"+",false,"+"'"+"e28g21_client"+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,41);\"", "", true, "HLP_PromptContratoServicosPrazoContratoServicosPrazoRegra.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoServicosPrazoContratoServicosPrazoRegra.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_45_G22( true) ;
         }
         else
         {
            wb_table8_45_G22( false) ;
         }
         return  ;
      }

      protected void wb_table8_45_G22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoServicosPrazoContratoServicosPrazoRegra.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoServicosPrazoContratoServicosPrazoRegra.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoServicosPrazoContratoServicosPrazoRegra.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV24DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 7, "'"+""+"'"+",false,"+"'"+"e29g21_client"+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,58);\"", "", true, "HLP_PromptContratoServicosPrazoContratoServicosPrazoRegra.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoServicosPrazoContratoServicosPrazoRegra.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_62_G22( true) ;
         }
         else
         {
            wb_table9_62_G22( false) ;
         }
         return  ;
      }

      protected void wb_table9_62_G22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoServicosPrazoContratoServicosPrazoRegra.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_19_G22e( true) ;
         }
         else
         {
            wb_table6_19_G22e( false) ;
         }
      }

      protected void wb_table9_62_G22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,65);\"", "", true, "HLP_PromptContratoServicosPrazoContratoServicosPrazoRegra.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratoservicosprazoregra_inicio3_Internalname, StringUtil.LTrim( StringUtil.NToC( AV26ContratoServicosPrazoRegra_Inicio3, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV26ContratoServicosPrazoRegra_Inicio3, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,67);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoservicosprazoregra_inicio3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratoservicosprazoregra_inicio3_Visible, 1, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicosPrazoContratoServicosPrazoRegra.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_62_G22e( true) ;
         }
         else
         {
            wb_table9_62_G22e( false) ;
         }
      }

      protected void wb_table8_45_G22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,48);\"", "", true, "HLP_PromptContratoServicosPrazoContratoServicosPrazoRegra.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratoservicosprazoregra_inicio2_Internalname, StringUtil.LTrim( StringUtil.NToC( AV22ContratoServicosPrazoRegra_Inicio2, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV22ContratoServicosPrazoRegra_Inicio2, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,50);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoservicosprazoregra_inicio2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratoservicosprazoregra_inicio2_Visible, 1, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicosPrazoContratoServicosPrazoRegra.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_45_G22e( true) ;
         }
         else
         {
            wb_table8_45_G22e( false) ;
         }
      }

      protected void wb_table7_28_G22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_PromptContratoServicosPrazoContratoServicosPrazoRegra.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratoservicosprazoregra_inicio1_Internalname, StringUtil.LTrim( StringUtil.NToC( AV18ContratoServicosPrazoRegra_Inicio1, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV18ContratoServicosPrazoRegra_Inicio1, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,33);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoservicosprazoregra_inicio1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratoservicosprazoregra_inicio1_Visible, 1, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoServicosPrazoContratoServicosPrazoRegra.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_G22e( true) ;
         }
         else
         {
            wb_table7_28_G22e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InContratoServicosPrazo_CntSrvCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InContratoServicosPrazo_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InContratoServicosPrazo_CntSrvCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vINCONTRATOSERVICOSPRAZO_CNTSRVCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7InContratoServicosPrazo_CntSrvCod), "ZZZZZ9")));
         AV8InOutContratoServicosPrazoRegra_Sequencial = Convert.ToInt16(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContratoServicosPrazoRegra_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8InOutContratoServicosPrazoRegra_Sequencial), 4, 0)));
         AV9InOutContratoServicosPrazoRegra_Inicio = (decimal)(Convert.ToDecimal((decimal)getParm(obj,2)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9InOutContratoServicosPrazoRegra_Inicio", StringUtil.LTrim( StringUtil.Str( AV9InOutContratoServicosPrazoRegra_Inicio, 14, 5)));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAG22( ) ;
         WSG22( ) ;
         WEG22( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203118541017");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptcontratoservicosprazocontratoservicosprazoregra.js", "?20203118541017");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_802( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_80_idx;
         edtContratoServicosPrazoRegra_Sequencial_Internalname = "CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_"+sGXsfl_80_idx;
         edtContratoServicosPrazoRegra_Inicio_Internalname = "CONTRATOSERVICOSPRAZOREGRA_INICIO_"+sGXsfl_80_idx;
         edtContratoServicosPrazoRegra_Fim_Internalname = "CONTRATOSERVICOSPRAZOREGRA_FIM_"+sGXsfl_80_idx;
         edtContratoServicosPrazoRegra_Dias_Internalname = "CONTRATOSERVICOSPRAZOREGRA_DIAS_"+sGXsfl_80_idx;
      }

      protected void SubsflControlProps_fel_802( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_80_fel_idx;
         edtContratoServicosPrazoRegra_Sequencial_Internalname = "CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_"+sGXsfl_80_fel_idx;
         edtContratoServicosPrazoRegra_Inicio_Internalname = "CONTRATOSERVICOSPRAZOREGRA_INICIO_"+sGXsfl_80_fel_idx;
         edtContratoServicosPrazoRegra_Fim_Internalname = "CONTRATOSERVICOSPRAZOREGRA_FIM_"+sGXsfl_80_fel_idx;
         edtContratoServicosPrazoRegra_Dias_Internalname = "CONTRATOSERVICOSPRAZOREGRA_DIAS_"+sGXsfl_80_fel_idx;
      }

      protected void sendrow_802( )
      {
         SubsflControlProps_802( ) ;
         WBG20( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_80_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_80_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_80_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 81,'',false,'',80)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV29Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV29Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV53Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV29Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV29Select)) ? AV53Select_GXI : context.PathToRelativeUrl( AV29Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_80_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV29Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosPrazoRegra_Sequencial_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A906ContratoServicosPrazoRegra_Sequencial), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A906ContratoServicosPrazoRegra_Sequencial), "ZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosPrazoRegra_Sequencial_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosPrazoRegra_Inicio_Internalname,StringUtil.LTrim( StringUtil.NToC( A907ContratoServicosPrazoRegra_Inicio, 14, 5, ",", "")),context.localUtil.Format( A907ContratoServicosPrazoRegra_Inicio, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosPrazoRegra_Inicio_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)94,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosPrazoRegra_Fim_Internalname,StringUtil.LTrim( StringUtil.NToC( A908ContratoServicosPrazoRegra_Fim, 14, 5, ",", "")),context.localUtil.Format( A908ContratoServicosPrazoRegra_Fim, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosPrazoRegra_Fim_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)94,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosPrazoRegra_Dias_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A909ContratoServicosPrazoRegra_Dias), 3, 0, ",", "")),context.localUtil.Format( (decimal)(A909ContratoServicosPrazoRegra_Dias), "ZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosPrazoRegra_Dias_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)28,(String)"px",(short)17,(String)"px",(short)3,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, context.localUtil.Format( (decimal)(A906ContratoServicosPrazoRegra_Sequencial), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSPRAZOREGRA_INICIO"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, context.localUtil.Format( A907ContratoServicosPrazoRegra_Inicio, "ZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSPRAZOREGRA_FIM"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, context.localUtil.Format( A908ContratoServicosPrazoRegra_Fim, "ZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSPRAZOREGRA_DIAS"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, context.localUtil.Format( (decimal)(A909ContratoServicosPrazoRegra_Dias), "ZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_80_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_80_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_80_idx+1));
            sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
            SubsflControlProps_802( ) ;
         }
         /* End function sendrow_802 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavContratoservicosprazoregra_inicio1_Internalname = "vCONTRATOSERVICOSPRAZOREGRA_INICIO1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavContratoservicosprazoregra_inicio2_Internalname = "vCONTRATOSERVICOSPRAZOREGRA_INICIO2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavContratoservicosprazoregra_inicio3_Internalname = "vCONTRATOSERVICOSPRAZOREGRA_INICIO3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtContratoServicosPrazoRegra_Sequencial_Internalname = "CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL";
         edtContratoServicosPrazoRegra_Inicio_Internalname = "CONTRATOSERVICOSPRAZOREGRA_INICIO";
         edtContratoServicosPrazoRegra_Fim_Internalname = "CONTRATOSERVICOSPRAZOREGRA_FIM";
         edtContratoServicosPrazoRegra_Dias_Internalname = "CONTRATOSERVICOSPRAZOREGRA_DIAS";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfcontratoservicosprazoregra_sequencial_Internalname = "vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL";
         edtavTfcontratoservicosprazoregra_sequencial_to_Internalname = "vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_TO";
         edtavTfcontratoservicosprazoregra_inicio_Internalname = "vTFCONTRATOSERVICOSPRAZOREGRA_INICIO";
         edtavTfcontratoservicosprazoregra_inicio_to_Internalname = "vTFCONTRATOSERVICOSPRAZOREGRA_INICIO_TO";
         edtavTfcontratoservicosprazoregra_fim_Internalname = "vTFCONTRATOSERVICOSPRAZOREGRA_FIM";
         edtavTfcontratoservicosprazoregra_fim_to_Internalname = "vTFCONTRATOSERVICOSPRAZOREGRA_FIM_TO";
         edtavTfcontratoservicosprazoregra_dias_Internalname = "vTFCONTRATOSERVICOSPRAZOREGRA_DIAS";
         edtavTfcontratoservicosprazoregra_dias_to_Internalname = "vTFCONTRATOSERVICOSPRAZOREGRA_DIAS_TO";
         Ddo_contratoservicosprazoregra_sequencial_Internalname = "DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL";
         edtavDdo_contratoservicosprazoregra_sequencialtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIALTITLECONTROLIDTOREPLACE";
         Ddo_contratoservicosprazoregra_inicio_Internalname = "DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO";
         edtavDdo_contratoservicosprazoregra_iniciotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSERVICOSPRAZOREGRA_INICIOTITLECONTROLIDTOREPLACE";
         Ddo_contratoservicosprazoregra_fim_Internalname = "DDO_CONTRATOSERVICOSPRAZOREGRA_FIM";
         edtavDdo_contratoservicosprazoregra_fimtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSERVICOSPRAZOREGRA_FIMTITLECONTROLIDTOREPLACE";
         Ddo_contratoservicosprazoregra_dias_Internalname = "DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS";
         edtavDdo_contratoservicosprazoregra_diastitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOSERVICOSPRAZOREGRA_DIASTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtContratoServicosPrazoRegra_Dias_Jsonclick = "";
         edtContratoServicosPrazoRegra_Fim_Jsonclick = "";
         edtContratoServicosPrazoRegra_Inicio_Jsonclick = "";
         edtContratoServicosPrazoRegra_Sequencial_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavContratoservicosprazoregra_inicio1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavContratoservicosprazoregra_inicio2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavContratoservicosprazoregra_inicio3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         edtContratoServicosPrazoRegra_Dias_Titleformat = 0;
         edtContratoServicosPrazoRegra_Fim_Titleformat = 0;
         edtContratoServicosPrazoRegra_Inicio_Titleformat = 0;
         edtContratoServicosPrazoRegra_Sequencial_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavContratoservicosprazoregra_inicio3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavContratoservicosprazoregra_inicio2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavContratoservicosprazoregra_inicio1_Visible = 1;
         edtContratoServicosPrazoRegra_Dias_Title = "Dias";
         edtContratoServicosPrazoRegra_Fim_Title = "Fim";
         edtContratoServicosPrazoRegra_Inicio_Title = "Inicio";
         edtContratoServicosPrazoRegra_Sequencial_Title = "Sequencial";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_contratoservicosprazoregra_diastitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicosprazoregra_fimtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicosprazoregra_iniciotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicosprazoregra_sequencialtitlecontrolidtoreplace_Visible = 1;
         edtavTfcontratoservicosprazoregra_dias_to_Jsonclick = "";
         edtavTfcontratoservicosprazoregra_dias_to_Visible = 1;
         edtavTfcontratoservicosprazoregra_dias_Jsonclick = "";
         edtavTfcontratoservicosprazoregra_dias_Visible = 1;
         edtavTfcontratoservicosprazoregra_fim_to_Jsonclick = "";
         edtavTfcontratoservicosprazoregra_fim_to_Visible = 1;
         edtavTfcontratoservicosprazoregra_fim_Jsonclick = "";
         edtavTfcontratoservicosprazoregra_fim_Visible = 1;
         edtavTfcontratoservicosprazoregra_inicio_to_Jsonclick = "";
         edtavTfcontratoservicosprazoregra_inicio_to_Visible = 1;
         edtavTfcontratoservicosprazoregra_inicio_Jsonclick = "";
         edtavTfcontratoservicosprazoregra_inicio_Visible = 1;
         edtavTfcontratoservicosprazoregra_sequencial_to_Jsonclick = "";
         edtavTfcontratoservicosprazoregra_sequencial_to_Visible = 1;
         edtavTfcontratoservicosprazoregra_sequencial_Jsonclick = "";
         edtavTfcontratoservicosprazoregra_sequencial_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_contratoservicosprazoregra_dias_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicosprazoregra_dias_Rangefilterto = "At�";
         Ddo_contratoservicosprazoregra_dias_Rangefilterfrom = "Desde";
         Ddo_contratoservicosprazoregra_dias_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicosprazoregra_dias_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicosprazoregra_dias_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicosprazoregra_dias_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoservicosprazoregra_dias_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoservicosprazoregra_dias_Filtertype = "Numeric";
         Ddo_contratoservicosprazoregra_dias_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicosprazoregra_dias_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicosprazoregra_dias_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicosprazoregra_dias_Titlecontrolidtoreplace = "";
         Ddo_contratoservicosprazoregra_dias_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicosprazoregra_dias_Cls = "ColumnSettings";
         Ddo_contratoservicosprazoregra_dias_Tooltip = "Op��es";
         Ddo_contratoservicosprazoregra_dias_Caption = "";
         Ddo_contratoservicosprazoregra_fim_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicosprazoregra_fim_Rangefilterto = "At�";
         Ddo_contratoservicosprazoregra_fim_Rangefilterfrom = "Desde";
         Ddo_contratoservicosprazoregra_fim_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicosprazoregra_fim_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicosprazoregra_fim_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicosprazoregra_fim_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoservicosprazoregra_fim_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoservicosprazoregra_fim_Filtertype = "Numeric";
         Ddo_contratoservicosprazoregra_fim_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicosprazoregra_fim_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicosprazoregra_fim_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicosprazoregra_fim_Titlecontrolidtoreplace = "";
         Ddo_contratoservicosprazoregra_fim_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicosprazoregra_fim_Cls = "ColumnSettings";
         Ddo_contratoservicosprazoregra_fim_Tooltip = "Op��es";
         Ddo_contratoservicosprazoregra_fim_Caption = "";
         Ddo_contratoservicosprazoregra_inicio_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicosprazoregra_inicio_Rangefilterto = "At�";
         Ddo_contratoservicosprazoregra_inicio_Rangefilterfrom = "Desde";
         Ddo_contratoservicosprazoregra_inicio_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicosprazoregra_inicio_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicosprazoregra_inicio_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicosprazoregra_inicio_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoservicosprazoregra_inicio_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoservicosprazoregra_inicio_Filtertype = "Numeric";
         Ddo_contratoservicosprazoregra_inicio_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicosprazoregra_inicio_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicosprazoregra_inicio_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicosprazoregra_inicio_Titlecontrolidtoreplace = "";
         Ddo_contratoservicosprazoregra_inicio_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicosprazoregra_inicio_Cls = "ColumnSettings";
         Ddo_contratoservicosprazoregra_inicio_Tooltip = "Op��es";
         Ddo_contratoservicosprazoregra_inicio_Caption = "";
         Ddo_contratoservicosprazoregra_sequencial_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicosprazoregra_sequencial_Rangefilterto = "At�";
         Ddo_contratoservicosprazoregra_sequencial_Rangefilterfrom = "Desde";
         Ddo_contratoservicosprazoregra_sequencial_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicosprazoregra_sequencial_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicosprazoregra_sequencial_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicosprazoregra_sequencial_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoservicosprazoregra_sequencial_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoservicosprazoregra_sequencial_Filtertype = "Numeric";
         Ddo_contratoservicosprazoregra_sequencial_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicosprazoregra_sequencial_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicosprazoregra_sequencial_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicosprazoregra_sequencial_Titlecontrolidtoreplace = "";
         Ddo_contratoservicosprazoregra_sequencial_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicosprazoregra_sequencial_Cls = "ColumnSettings";
         Ddo_contratoservicosprazoregra_sequencial_Tooltip = "Op��es";
         Ddo_contratoservicosprazoregra_sequencial_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Contrato Servicos Prazo Regra";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContratoServicosPrazoRegra_Inicio1',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIO1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ContratoServicosPrazoRegra_Inicio2',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIO2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26ContratoServicosPrazoRegra_Inicio3',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIO3',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFContratoServicosPrazoRegra_Sequencial',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL',pic:'ZZZ9',nv:0},{av:'AV33TFContratoServicosPrazoRegra_Sequencial_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_TO',pic:'ZZZ9',nv:0},{av:'AV36TFContratoServicosPrazoRegra_Inicio',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_INICIO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV37TFContratoServicosPrazoRegra_Inicio_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_INICIO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV40TFContratoServicosPrazoRegra_Fim',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_FIM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV41TFContratoServicosPrazoRegra_Fim_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_FIM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV44TFContratoServicosPrazoRegra_Dias',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_DIAS',pic:'ZZ9',nv:0},{av:'AV45TFContratoServicosPrazoRegra_Dias_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_DIAS_TO',pic:'ZZ9',nv:0},{av:'AV7InContratoServicosPrazo_CntSrvCod',fld:'vINCONTRATOSERVICOSPRAZO_CNTSRVCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV34ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''}],oparms:[{av:'AV31ContratoServicosPrazoRegra_SequencialTitleFilterData',fld:'vCONTRATOSERVICOSPRAZOREGRA_SEQUENCIALTITLEFILTERDATA',pic:'',nv:null},{av:'AV35ContratoServicosPrazoRegra_InicioTitleFilterData',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIOTITLEFILTERDATA',pic:'',nv:null},{av:'AV39ContratoServicosPrazoRegra_FimTitleFilterData',fld:'vCONTRATOSERVICOSPRAZOREGRA_FIMTITLEFILTERDATA',pic:'',nv:null},{av:'AV43ContratoServicosPrazoRegra_DiasTitleFilterData',fld:'vCONTRATOSERVICOSPRAZOREGRA_DIASTITLEFILTERDATA',pic:'',nv:null},{av:'edtContratoServicosPrazoRegra_Sequencial_Titleformat',ctrl:'CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL',prop:'Titleformat'},{av:'edtContratoServicosPrazoRegra_Sequencial_Title',ctrl:'CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL',prop:'Title'},{av:'edtContratoServicosPrazoRegra_Inicio_Titleformat',ctrl:'CONTRATOSERVICOSPRAZOREGRA_INICIO',prop:'Titleformat'},{av:'edtContratoServicosPrazoRegra_Inicio_Title',ctrl:'CONTRATOSERVICOSPRAZOREGRA_INICIO',prop:'Title'},{av:'edtContratoServicosPrazoRegra_Fim_Titleformat',ctrl:'CONTRATOSERVICOSPRAZOREGRA_FIM',prop:'Titleformat'},{av:'edtContratoServicosPrazoRegra_Fim_Title',ctrl:'CONTRATOSERVICOSPRAZOREGRA_FIM',prop:'Title'},{av:'edtContratoServicosPrazoRegra_Dias_Titleformat',ctrl:'CONTRATOSERVICOSPRAZOREGRA_DIAS',prop:'Titleformat'},{av:'edtContratoServicosPrazoRegra_Dias_Title',ctrl:'CONTRATOSERVICOSPRAZOREGRA_DIAS',prop:'Title'},{av:'AV49GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV50GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11G22',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContratoServicosPrazoRegra_Inicio1',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIO1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ContratoServicosPrazoRegra_Inicio2',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIO2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26ContratoServicosPrazoRegra_Inicio3',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIO3',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFContratoServicosPrazoRegra_Sequencial',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL',pic:'ZZZ9',nv:0},{av:'AV33TFContratoServicosPrazoRegra_Sequencial_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_TO',pic:'ZZZ9',nv:0},{av:'AV36TFContratoServicosPrazoRegra_Inicio',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_INICIO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV37TFContratoServicosPrazoRegra_Inicio_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_INICIO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV40TFContratoServicosPrazoRegra_Fim',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_FIM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV41TFContratoServicosPrazoRegra_Fim_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_FIM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV44TFContratoServicosPrazoRegra_Dias',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_DIAS',pic:'ZZ9',nv:0},{av:'AV45TFContratoServicosPrazoRegra_Dias_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_DIAS_TO',pic:'ZZ9',nv:0},{av:'AV7InContratoServicosPrazo_CntSrvCod',fld:'vINCONTRATOSERVICOSPRAZO_CNTSRVCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV34ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL.ONOPTIONCLICKED","{handler:'E12G22',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContratoServicosPrazoRegra_Inicio1',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIO1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ContratoServicosPrazoRegra_Inicio2',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIO2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26ContratoServicosPrazoRegra_Inicio3',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIO3',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFContratoServicosPrazoRegra_Sequencial',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL',pic:'ZZZ9',nv:0},{av:'AV33TFContratoServicosPrazoRegra_Sequencial_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_TO',pic:'ZZZ9',nv:0},{av:'AV36TFContratoServicosPrazoRegra_Inicio',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_INICIO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV37TFContratoServicosPrazoRegra_Inicio_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_INICIO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV40TFContratoServicosPrazoRegra_Fim',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_FIM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV41TFContratoServicosPrazoRegra_Fim_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_FIM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV44TFContratoServicosPrazoRegra_Dias',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_DIAS',pic:'ZZ9',nv:0},{av:'AV45TFContratoServicosPrazoRegra_Dias_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_DIAS_TO',pic:'ZZ9',nv:0},{av:'AV7InContratoServicosPrazo_CntSrvCod',fld:'vINCONTRATOSERVICOSPRAZO_CNTSRVCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV34ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'Ddo_contratoservicosprazoregra_sequencial_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL',prop:'ActiveEventKey'},{av:'Ddo_contratoservicosprazoregra_sequencial_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL',prop:'FilteredText_get'},{av:'Ddo_contratoservicosprazoregra_sequencial_Filteredtextto_get',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL',prop:'FilteredTextTo_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicosprazoregra_sequencial_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL',prop:'SortedStatus'},{av:'AV32TFContratoServicosPrazoRegra_Sequencial',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL',pic:'ZZZ9',nv:0},{av:'AV33TFContratoServicosPrazoRegra_Sequencial_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_TO',pic:'ZZZ9',nv:0},{av:'Ddo_contratoservicosprazoregra_inicio_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO',prop:'SortedStatus'},{av:'Ddo_contratoservicosprazoregra_fim_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_FIM',prop:'SortedStatus'},{av:'Ddo_contratoservicosprazoregra_dias_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO.ONOPTIONCLICKED","{handler:'E13G22',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContratoServicosPrazoRegra_Inicio1',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIO1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ContratoServicosPrazoRegra_Inicio2',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIO2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26ContratoServicosPrazoRegra_Inicio3',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIO3',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFContratoServicosPrazoRegra_Sequencial',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL',pic:'ZZZ9',nv:0},{av:'AV33TFContratoServicosPrazoRegra_Sequencial_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_TO',pic:'ZZZ9',nv:0},{av:'AV36TFContratoServicosPrazoRegra_Inicio',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_INICIO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV37TFContratoServicosPrazoRegra_Inicio_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_INICIO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV40TFContratoServicosPrazoRegra_Fim',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_FIM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV41TFContratoServicosPrazoRegra_Fim_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_FIM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV44TFContratoServicosPrazoRegra_Dias',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_DIAS',pic:'ZZ9',nv:0},{av:'AV45TFContratoServicosPrazoRegra_Dias_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_DIAS_TO',pic:'ZZ9',nv:0},{av:'AV7InContratoServicosPrazo_CntSrvCod',fld:'vINCONTRATOSERVICOSPRAZO_CNTSRVCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV34ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'Ddo_contratoservicosprazoregra_inicio_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO',prop:'ActiveEventKey'},{av:'Ddo_contratoservicosprazoregra_inicio_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO',prop:'FilteredText_get'},{av:'Ddo_contratoservicosprazoregra_inicio_Filteredtextto_get',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicosprazoregra_inicio_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO',prop:'SortedStatus'},{av:'AV36TFContratoServicosPrazoRegra_Inicio',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_INICIO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV37TFContratoServicosPrazoRegra_Inicio_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_INICIO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_contratoservicosprazoregra_sequencial_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL',prop:'SortedStatus'},{av:'Ddo_contratoservicosprazoregra_fim_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_FIM',prop:'SortedStatus'},{av:'Ddo_contratoservicosprazoregra_dias_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSPRAZOREGRA_FIM.ONOPTIONCLICKED","{handler:'E14G22',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContratoServicosPrazoRegra_Inicio1',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIO1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ContratoServicosPrazoRegra_Inicio2',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIO2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26ContratoServicosPrazoRegra_Inicio3',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIO3',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFContratoServicosPrazoRegra_Sequencial',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL',pic:'ZZZ9',nv:0},{av:'AV33TFContratoServicosPrazoRegra_Sequencial_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_TO',pic:'ZZZ9',nv:0},{av:'AV36TFContratoServicosPrazoRegra_Inicio',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_INICIO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV37TFContratoServicosPrazoRegra_Inicio_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_INICIO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV40TFContratoServicosPrazoRegra_Fim',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_FIM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV41TFContratoServicosPrazoRegra_Fim_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_FIM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV44TFContratoServicosPrazoRegra_Dias',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_DIAS',pic:'ZZ9',nv:0},{av:'AV45TFContratoServicosPrazoRegra_Dias_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_DIAS_TO',pic:'ZZ9',nv:0},{av:'AV7InContratoServicosPrazo_CntSrvCod',fld:'vINCONTRATOSERVICOSPRAZO_CNTSRVCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV34ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'Ddo_contratoservicosprazoregra_fim_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_FIM',prop:'ActiveEventKey'},{av:'Ddo_contratoservicosprazoregra_fim_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_FIM',prop:'FilteredText_get'},{av:'Ddo_contratoservicosprazoregra_fim_Filteredtextto_get',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_FIM',prop:'FilteredTextTo_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicosprazoregra_fim_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_FIM',prop:'SortedStatus'},{av:'AV40TFContratoServicosPrazoRegra_Fim',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_FIM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV41TFContratoServicosPrazoRegra_Fim_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_FIM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_contratoservicosprazoregra_sequencial_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL',prop:'SortedStatus'},{av:'Ddo_contratoservicosprazoregra_inicio_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO',prop:'SortedStatus'},{av:'Ddo_contratoservicosprazoregra_dias_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS.ONOPTIONCLICKED","{handler:'E15G22',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContratoServicosPrazoRegra_Inicio1',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIO1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ContratoServicosPrazoRegra_Inicio2',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIO2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26ContratoServicosPrazoRegra_Inicio3',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIO3',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFContratoServicosPrazoRegra_Sequencial',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL',pic:'ZZZ9',nv:0},{av:'AV33TFContratoServicosPrazoRegra_Sequencial_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_TO',pic:'ZZZ9',nv:0},{av:'AV36TFContratoServicosPrazoRegra_Inicio',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_INICIO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV37TFContratoServicosPrazoRegra_Inicio_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_INICIO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV40TFContratoServicosPrazoRegra_Fim',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_FIM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV41TFContratoServicosPrazoRegra_Fim_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_FIM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV44TFContratoServicosPrazoRegra_Dias',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_DIAS',pic:'ZZ9',nv:0},{av:'AV45TFContratoServicosPrazoRegra_Dias_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_DIAS_TO',pic:'ZZ9',nv:0},{av:'AV7InContratoServicosPrazo_CntSrvCod',fld:'vINCONTRATOSERVICOSPRAZO_CNTSRVCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV34ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'Ddo_contratoservicosprazoregra_dias_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS',prop:'ActiveEventKey'},{av:'Ddo_contratoservicosprazoregra_dias_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS',prop:'FilteredText_get'},{av:'Ddo_contratoservicosprazoregra_dias_Filteredtextto_get',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS',prop:'FilteredTextTo_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicosprazoregra_dias_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS',prop:'SortedStatus'},{av:'AV44TFContratoServicosPrazoRegra_Dias',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_DIAS',pic:'ZZ9',nv:0},{av:'AV45TFContratoServicosPrazoRegra_Dias_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_DIAS_TO',pic:'ZZ9',nv:0},{av:'Ddo_contratoservicosprazoregra_sequencial_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL',prop:'SortedStatus'},{av:'Ddo_contratoservicosprazoregra_inicio_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO',prop:'SortedStatus'},{av:'Ddo_contratoservicosprazoregra_fim_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_FIM',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E25G22',iparms:[],oparms:[{av:'AV29Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E26G22',iparms:[{av:'A906ContratoServicosPrazoRegra_Sequencial',fld:'CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL',pic:'ZZZ9',hsh:true,nv:0},{av:'A907ContratoServicosPrazoRegra_Inicio',fld:'CONTRATOSERVICOSPRAZOREGRA_INICIO',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0}],oparms:[{av:'AV8InOutContratoServicosPrazoRegra_Sequencial',fld:'vINOUTCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL',pic:'ZZZ9',nv:0},{av:'AV9InOutContratoServicosPrazoRegra_Inicio',fld:'vINOUTCONTRATOSERVICOSPRAZOREGRA_INICIO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E16G22',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContratoServicosPrazoRegra_Inicio1',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIO1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ContratoServicosPrazoRegra_Inicio2',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIO2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26ContratoServicosPrazoRegra_Inicio3',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIO3',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFContratoServicosPrazoRegra_Sequencial',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL',pic:'ZZZ9',nv:0},{av:'AV33TFContratoServicosPrazoRegra_Sequencial_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_TO',pic:'ZZZ9',nv:0},{av:'AV36TFContratoServicosPrazoRegra_Inicio',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_INICIO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV37TFContratoServicosPrazoRegra_Inicio_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_INICIO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV40TFContratoServicosPrazoRegra_Fim',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_FIM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV41TFContratoServicosPrazoRegra_Fim_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_FIM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV44TFContratoServicosPrazoRegra_Dias',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_DIAS',pic:'ZZ9',nv:0},{av:'AV45TFContratoServicosPrazoRegra_Dias_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_DIAS_TO',pic:'ZZ9',nv:0},{av:'AV7InContratoServicosPrazo_CntSrvCod',fld:'vINCONTRATOSERVICOSPRAZO_CNTSRVCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV34ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E21G22',iparms:[],oparms:[{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E17G22',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContratoServicosPrazoRegra_Inicio1',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIO1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ContratoServicosPrazoRegra_Inicio2',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIO2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26ContratoServicosPrazoRegra_Inicio3',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIO3',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFContratoServicosPrazoRegra_Sequencial',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL',pic:'ZZZ9',nv:0},{av:'AV33TFContratoServicosPrazoRegra_Sequencial_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_TO',pic:'ZZZ9',nv:0},{av:'AV36TFContratoServicosPrazoRegra_Inicio',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_INICIO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV37TFContratoServicosPrazoRegra_Inicio_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_INICIO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV40TFContratoServicosPrazoRegra_Fim',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_FIM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV41TFContratoServicosPrazoRegra_Fim_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_FIM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV44TFContratoServicosPrazoRegra_Dias',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_DIAS',pic:'ZZ9',nv:0},{av:'AV45TFContratoServicosPrazoRegra_Dias_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_DIAS_TO',pic:'ZZ9',nv:0},{av:'AV7InContratoServicosPrazo_CntSrvCod',fld:'vINCONTRATOSERVICOSPRAZO_CNTSRVCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV34ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ContratoServicosPrazoRegra_Inicio2',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIO2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26ContratoServicosPrazoRegra_Inicio3',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIO3',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContratoServicosPrazoRegra_Inicio1',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIO1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavContratoservicosprazoregra_inicio2_Visible',ctrl:'vCONTRATOSERVICOSPRAZOREGRA_INICIO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContratoservicosprazoregra_inicio3_Visible',ctrl:'vCONTRATOSERVICOSPRAZOREGRA_INICIO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContratoservicosprazoregra_inicio1_Visible',ctrl:'vCONTRATOSERVICOSPRAZOREGRA_INICIO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E27G21',iparms:[{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavContratoservicosprazoregra_inicio1_Visible',ctrl:'vCONTRATOSERVICOSPRAZOREGRA_INICIO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E22G22',iparms:[],oparms:[{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E18G22',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContratoServicosPrazoRegra_Inicio1',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIO1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ContratoServicosPrazoRegra_Inicio2',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIO2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26ContratoServicosPrazoRegra_Inicio3',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIO3',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFContratoServicosPrazoRegra_Sequencial',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL',pic:'ZZZ9',nv:0},{av:'AV33TFContratoServicosPrazoRegra_Sequencial_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_TO',pic:'ZZZ9',nv:0},{av:'AV36TFContratoServicosPrazoRegra_Inicio',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_INICIO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV37TFContratoServicosPrazoRegra_Inicio_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_INICIO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV40TFContratoServicosPrazoRegra_Fim',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_FIM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV41TFContratoServicosPrazoRegra_Fim_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_FIM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV44TFContratoServicosPrazoRegra_Dias',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_DIAS',pic:'ZZ9',nv:0},{av:'AV45TFContratoServicosPrazoRegra_Dias_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_DIAS_TO',pic:'ZZ9',nv:0},{av:'AV7InContratoServicosPrazo_CntSrvCod',fld:'vINCONTRATOSERVICOSPRAZO_CNTSRVCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV34ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ContratoServicosPrazoRegra_Inicio2',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIO2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26ContratoServicosPrazoRegra_Inicio3',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIO3',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContratoServicosPrazoRegra_Inicio1',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIO1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavContratoservicosprazoregra_inicio2_Visible',ctrl:'vCONTRATOSERVICOSPRAZOREGRA_INICIO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContratoservicosprazoregra_inicio3_Visible',ctrl:'vCONTRATOSERVICOSPRAZOREGRA_INICIO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContratoservicosprazoregra_inicio1_Visible',ctrl:'vCONTRATOSERVICOSPRAZOREGRA_INICIO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E28G21',iparms:[{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavContratoservicosprazoregra_inicio2_Visible',ctrl:'vCONTRATOSERVICOSPRAZOREGRA_INICIO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E19G22',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContratoServicosPrazoRegra_Inicio1',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIO1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ContratoServicosPrazoRegra_Inicio2',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIO2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26ContratoServicosPrazoRegra_Inicio3',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIO3',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFContratoServicosPrazoRegra_Sequencial',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL',pic:'ZZZ9',nv:0},{av:'AV33TFContratoServicosPrazoRegra_Sequencial_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_TO',pic:'ZZZ9',nv:0},{av:'AV36TFContratoServicosPrazoRegra_Inicio',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_INICIO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV37TFContratoServicosPrazoRegra_Inicio_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_INICIO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV40TFContratoServicosPrazoRegra_Fim',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_FIM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV41TFContratoServicosPrazoRegra_Fim_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_FIM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV44TFContratoServicosPrazoRegra_Dias',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_DIAS',pic:'ZZ9',nv:0},{av:'AV45TFContratoServicosPrazoRegra_Dias_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_DIAS_TO',pic:'ZZ9',nv:0},{av:'AV7InContratoServicosPrazo_CntSrvCod',fld:'vINCONTRATOSERVICOSPRAZO_CNTSRVCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV34ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ContratoServicosPrazoRegra_Inicio2',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIO2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26ContratoServicosPrazoRegra_Inicio3',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIO3',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContratoServicosPrazoRegra_Inicio1',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIO1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavContratoservicosprazoregra_inicio2_Visible',ctrl:'vCONTRATOSERVICOSPRAZOREGRA_INICIO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContratoservicosprazoregra_inicio3_Visible',ctrl:'vCONTRATOSERVICOSPRAZOREGRA_INICIO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContratoservicosprazoregra_inicio1_Visible',ctrl:'vCONTRATOSERVICOSPRAZOREGRA_INICIO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E29G21',iparms:[{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavContratoservicosprazoregra_inicio3_Visible',ctrl:'vCONTRATOSERVICOSPRAZOREGRA_INICIO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E20G22',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContratoServicosPrazoRegra_Inicio1',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIO1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ContratoServicosPrazoRegra_Inicio2',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIO2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26ContratoServicosPrazoRegra_Inicio3',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIO3',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV32TFContratoServicosPrazoRegra_Sequencial',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL',pic:'ZZZ9',nv:0},{av:'AV33TFContratoServicosPrazoRegra_Sequencial_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_TO',pic:'ZZZ9',nv:0},{av:'AV36TFContratoServicosPrazoRegra_Inicio',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_INICIO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV37TFContratoServicosPrazoRegra_Inicio_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_INICIO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV40TFContratoServicosPrazoRegra_Fim',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_FIM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV41TFContratoServicosPrazoRegra_Fim_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_FIM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV44TFContratoServicosPrazoRegra_Dias',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_DIAS',pic:'ZZ9',nv:0},{av:'AV45TFContratoServicosPrazoRegra_Dias_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_DIAS_TO',pic:'ZZ9',nv:0},{av:'AV7InContratoServicosPrazo_CntSrvCod',fld:'vINCONTRATOSERVICOSPRAZO_CNTSRVCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV34ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV38ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSPRAZOREGRA_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV32TFContratoServicosPrazoRegra_Sequencial',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL',pic:'ZZZ9',nv:0},{av:'Ddo_contratoservicosprazoregra_sequencial_Filteredtext_set',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL',prop:'FilteredText_set'},{av:'AV33TFContratoServicosPrazoRegra_Sequencial_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_TO',pic:'ZZZ9',nv:0},{av:'Ddo_contratoservicosprazoregra_sequencial_Filteredtextto_set',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL',prop:'FilteredTextTo_set'},{av:'AV36TFContratoServicosPrazoRegra_Inicio',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_INICIO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_contratoservicosprazoregra_inicio_Filteredtext_set',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO',prop:'FilteredText_set'},{av:'AV37TFContratoServicosPrazoRegra_Inicio_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_INICIO_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_contratoservicosprazoregra_inicio_Filteredtextto_set',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_INICIO',prop:'FilteredTextTo_set'},{av:'AV40TFContratoServicosPrazoRegra_Fim',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_FIM',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_contratoservicosprazoregra_fim_Filteredtext_set',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_FIM',prop:'FilteredText_set'},{av:'AV41TFContratoServicosPrazoRegra_Fim_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_FIM_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_contratoservicosprazoregra_fim_Filteredtextto_set',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_FIM',prop:'FilteredTextTo_set'},{av:'AV44TFContratoServicosPrazoRegra_Dias',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_DIAS',pic:'ZZ9',nv:0},{av:'Ddo_contratoservicosprazoregra_dias_Filteredtext_set',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS',prop:'FilteredText_set'},{av:'AV45TFContratoServicosPrazoRegra_Dias_To',fld:'vTFCONTRATOSERVICOSPRAZOREGRA_DIAS_TO',pic:'ZZ9',nv:0},{av:'Ddo_contratoservicosprazoregra_dias_Filteredtextto_set',ctrl:'DDO_CONTRATOSERVICOSPRAZOREGRA_DIAS',prop:'FilteredTextTo_set'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18ContratoServicosPrazoRegra_Inicio1',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIO1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavContratoservicosprazoregra_inicio1_Visible',ctrl:'vCONTRATOSERVICOSPRAZOREGRA_INICIO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ContratoServicosPrazoRegra_Inicio2',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIO2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV26ContratoServicosPrazoRegra_Inicio3',fld:'vCONTRATOSERVICOSPRAZOREGRA_INICIO3',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavContratoservicosprazoregra_inicio2_Visible',ctrl:'vCONTRATOSERVICOSPRAZOREGRA_INICIO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContratoservicosprazoregra_inicio3_Visible',ctrl:'vCONTRATOSERVICOSPRAZOREGRA_INICIO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_contratoservicosprazoregra_sequencial_Activeeventkey = "";
         Ddo_contratoservicosprazoregra_sequencial_Filteredtext_get = "";
         Ddo_contratoservicosprazoregra_sequencial_Filteredtextto_get = "";
         Ddo_contratoservicosprazoregra_inicio_Activeeventkey = "";
         Ddo_contratoservicosprazoregra_inicio_Filteredtext_get = "";
         Ddo_contratoservicosprazoregra_inicio_Filteredtextto_get = "";
         Ddo_contratoservicosprazoregra_fim_Activeeventkey = "";
         Ddo_contratoservicosprazoregra_fim_Filteredtext_get = "";
         Ddo_contratoservicosprazoregra_fim_Filteredtextto_get = "";
         Ddo_contratoservicosprazoregra_dias_Activeeventkey = "";
         Ddo_contratoservicosprazoregra_dias_Filteredtext_get = "";
         Ddo_contratoservicosprazoregra_dias_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV16DynamicFiltersSelector1 = "";
         AV20DynamicFiltersSelector2 = "";
         AV24DynamicFiltersSelector3 = "";
         AV34ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace = "";
         AV38ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace = "";
         AV42ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace = "";
         AV46ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV47DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV31ContratoServicosPrazoRegra_SequencialTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV35ContratoServicosPrazoRegra_InicioTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV39ContratoServicosPrazoRegra_FimTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV43ContratoServicosPrazoRegra_DiasTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         Ddo_contratoservicosprazoregra_sequencial_Filteredtext_set = "";
         Ddo_contratoservicosprazoregra_sequencial_Filteredtextto_set = "";
         Ddo_contratoservicosprazoregra_sequencial_Sortedstatus = "";
         Ddo_contratoservicosprazoregra_inicio_Filteredtext_set = "";
         Ddo_contratoservicosprazoregra_inicio_Filteredtextto_set = "";
         Ddo_contratoservicosprazoregra_inicio_Sortedstatus = "";
         Ddo_contratoservicosprazoregra_fim_Filteredtext_set = "";
         Ddo_contratoservicosprazoregra_fim_Filteredtextto_set = "";
         Ddo_contratoservicosprazoregra_fim_Sortedstatus = "";
         Ddo_contratoservicosprazoregra_dias_Filteredtext_set = "";
         Ddo_contratoservicosprazoregra_dias_Filteredtextto_set = "";
         Ddo_contratoservicosprazoregra_dias_Sortedstatus = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV29Select = "";
         AV53Select_GXI = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         H00G22_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         H00G22_A904ContratoServicosPrazo_Tipo = new String[] {""} ;
         H00G22_A909ContratoServicosPrazoRegra_Dias = new short[1] ;
         H00G22_A908ContratoServicosPrazoRegra_Fim = new decimal[1] ;
         H00G22_A907ContratoServicosPrazoRegra_Inicio = new decimal[1] ;
         H00G22_A906ContratoServicosPrazoRegra_Sequencial = new short[1] ;
         A904ContratoServicosPrazo_Tipo = "";
         H00G23_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptcontratoservicosprazocontratoservicosprazoregra__default(),
            new Object[][] {
                new Object[] {
               H00G22_A903ContratoServicosPrazo_CntSrvCod, H00G22_A904ContratoServicosPrazo_Tipo, H00G22_A909ContratoServicosPrazoRegra_Dias, H00G22_A908ContratoServicosPrazoRegra_Fim, H00G22_A907ContratoServicosPrazoRegra_Inicio, H00G22_A906ContratoServicosPrazoRegra_Sequencial
               }
               , new Object[] {
               H00G23_AGRID_nRecordCount
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV8InOutContratoServicosPrazoRegra_Sequencial ;
      private short wcpOAV8InOutContratoServicosPrazoRegra_Sequencial ;
      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_80 ;
      private short nGXsfl_80_idx=1 ;
      private short AV14OrderedBy ;
      private short AV17DynamicFiltersOperator1 ;
      private short AV21DynamicFiltersOperator2 ;
      private short AV25DynamicFiltersOperator3 ;
      private short AV32TFContratoServicosPrazoRegra_Sequencial ;
      private short AV33TFContratoServicosPrazoRegra_Sequencial_To ;
      private short AV44TFContratoServicosPrazoRegra_Dias ;
      private short AV45TFContratoServicosPrazoRegra_Dias_To ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short A906ContratoServicosPrazoRegra_Sequencial ;
      private short A909ContratoServicosPrazoRegra_Dias ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_80_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContratoServicosPrazoRegra_Sequencial_Titleformat ;
      private short edtContratoServicosPrazoRegra_Inicio_Titleformat ;
      private short edtContratoServicosPrazoRegra_Fim_Titleformat ;
      private short edtContratoServicosPrazoRegra_Dias_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InContratoServicosPrazo_CntSrvCod ;
      private int wcpOAV7InContratoServicosPrazo_CntSrvCod ;
      private int subGrid_Rows ;
      private int Gridpaginationbar_Pagestoshow ;
      private int edtavTfcontratoservicosprazoregra_sequencial_Visible ;
      private int edtavTfcontratoservicosprazoregra_sequencial_to_Visible ;
      private int edtavTfcontratoservicosprazoregra_inicio_Visible ;
      private int edtavTfcontratoservicosprazoregra_inicio_to_Visible ;
      private int edtavTfcontratoservicosprazoregra_fim_Visible ;
      private int edtavTfcontratoservicosprazoregra_fim_to_Visible ;
      private int edtavTfcontratoservicosprazoregra_dias_Visible ;
      private int edtavTfcontratoservicosprazoregra_dias_to_Visible ;
      private int edtavDdo_contratoservicosprazoregra_sequencialtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicosprazoregra_iniciotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicosprazoregra_fimtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicosprazoregra_diastitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int A903ContratoServicosPrazo_CntSrvCod ;
      private int edtavOrdereddsc_Visible ;
      private int AV48PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavContratoservicosprazoregra_inicio1_Visible ;
      private int edtavContratoservicosprazoregra_inicio2_Visible ;
      private int edtavContratoservicosprazoregra_inicio3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV49GridCurrentPage ;
      private long AV50GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV9InOutContratoServicosPrazoRegra_Inicio ;
      private decimal wcpOAV9InOutContratoServicosPrazoRegra_Inicio ;
      private decimal AV18ContratoServicosPrazoRegra_Inicio1 ;
      private decimal AV22ContratoServicosPrazoRegra_Inicio2 ;
      private decimal AV26ContratoServicosPrazoRegra_Inicio3 ;
      private decimal AV36TFContratoServicosPrazoRegra_Inicio ;
      private decimal AV37TFContratoServicosPrazoRegra_Inicio_To ;
      private decimal AV40TFContratoServicosPrazoRegra_Fim ;
      private decimal AV41TFContratoServicosPrazoRegra_Fim_To ;
      private decimal A907ContratoServicosPrazoRegra_Inicio ;
      private decimal A908ContratoServicosPrazoRegra_Fim ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contratoservicosprazoregra_sequencial_Activeeventkey ;
      private String Ddo_contratoservicosprazoregra_sequencial_Filteredtext_get ;
      private String Ddo_contratoservicosprazoregra_sequencial_Filteredtextto_get ;
      private String Ddo_contratoservicosprazoregra_inicio_Activeeventkey ;
      private String Ddo_contratoservicosprazoregra_inicio_Filteredtext_get ;
      private String Ddo_contratoservicosprazoregra_inicio_Filteredtextto_get ;
      private String Ddo_contratoservicosprazoregra_fim_Activeeventkey ;
      private String Ddo_contratoservicosprazoregra_fim_Filteredtext_get ;
      private String Ddo_contratoservicosprazoregra_fim_Filteredtextto_get ;
      private String Ddo_contratoservicosprazoregra_dias_Activeeventkey ;
      private String Ddo_contratoservicosprazoregra_dias_Filteredtext_get ;
      private String Ddo_contratoservicosprazoregra_dias_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_80_idx="0001" ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_contratoservicosprazoregra_sequencial_Caption ;
      private String Ddo_contratoservicosprazoregra_sequencial_Tooltip ;
      private String Ddo_contratoservicosprazoregra_sequencial_Cls ;
      private String Ddo_contratoservicosprazoregra_sequencial_Filteredtext_set ;
      private String Ddo_contratoservicosprazoregra_sequencial_Filteredtextto_set ;
      private String Ddo_contratoservicosprazoregra_sequencial_Dropdownoptionstype ;
      private String Ddo_contratoservicosprazoregra_sequencial_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicosprazoregra_sequencial_Sortedstatus ;
      private String Ddo_contratoservicosprazoregra_sequencial_Filtertype ;
      private String Ddo_contratoservicosprazoregra_sequencial_Sortasc ;
      private String Ddo_contratoservicosprazoregra_sequencial_Sortdsc ;
      private String Ddo_contratoservicosprazoregra_sequencial_Cleanfilter ;
      private String Ddo_contratoservicosprazoregra_sequencial_Rangefilterfrom ;
      private String Ddo_contratoservicosprazoregra_sequencial_Rangefilterto ;
      private String Ddo_contratoservicosprazoregra_sequencial_Searchbuttontext ;
      private String Ddo_contratoservicosprazoregra_inicio_Caption ;
      private String Ddo_contratoservicosprazoregra_inicio_Tooltip ;
      private String Ddo_contratoservicosprazoregra_inicio_Cls ;
      private String Ddo_contratoservicosprazoregra_inicio_Filteredtext_set ;
      private String Ddo_contratoservicosprazoregra_inicio_Filteredtextto_set ;
      private String Ddo_contratoservicosprazoregra_inicio_Dropdownoptionstype ;
      private String Ddo_contratoservicosprazoregra_inicio_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicosprazoregra_inicio_Sortedstatus ;
      private String Ddo_contratoservicosprazoregra_inicio_Filtertype ;
      private String Ddo_contratoservicosprazoregra_inicio_Sortasc ;
      private String Ddo_contratoservicosprazoregra_inicio_Sortdsc ;
      private String Ddo_contratoservicosprazoregra_inicio_Cleanfilter ;
      private String Ddo_contratoservicosprazoregra_inicio_Rangefilterfrom ;
      private String Ddo_contratoservicosprazoregra_inicio_Rangefilterto ;
      private String Ddo_contratoservicosprazoregra_inicio_Searchbuttontext ;
      private String Ddo_contratoservicosprazoregra_fim_Caption ;
      private String Ddo_contratoservicosprazoregra_fim_Tooltip ;
      private String Ddo_contratoservicosprazoregra_fim_Cls ;
      private String Ddo_contratoservicosprazoregra_fim_Filteredtext_set ;
      private String Ddo_contratoservicosprazoregra_fim_Filteredtextto_set ;
      private String Ddo_contratoservicosprazoregra_fim_Dropdownoptionstype ;
      private String Ddo_contratoservicosprazoregra_fim_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicosprazoregra_fim_Sortedstatus ;
      private String Ddo_contratoservicosprazoregra_fim_Filtertype ;
      private String Ddo_contratoservicosprazoregra_fim_Sortasc ;
      private String Ddo_contratoservicosprazoregra_fim_Sortdsc ;
      private String Ddo_contratoservicosprazoregra_fim_Cleanfilter ;
      private String Ddo_contratoservicosprazoregra_fim_Rangefilterfrom ;
      private String Ddo_contratoservicosprazoregra_fim_Rangefilterto ;
      private String Ddo_contratoservicosprazoregra_fim_Searchbuttontext ;
      private String Ddo_contratoservicosprazoregra_dias_Caption ;
      private String Ddo_contratoservicosprazoregra_dias_Tooltip ;
      private String Ddo_contratoservicosprazoregra_dias_Cls ;
      private String Ddo_contratoservicosprazoregra_dias_Filteredtext_set ;
      private String Ddo_contratoservicosprazoregra_dias_Filteredtextto_set ;
      private String Ddo_contratoservicosprazoregra_dias_Dropdownoptionstype ;
      private String Ddo_contratoservicosprazoregra_dias_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicosprazoregra_dias_Sortedstatus ;
      private String Ddo_contratoservicosprazoregra_dias_Filtertype ;
      private String Ddo_contratoservicosprazoregra_dias_Sortasc ;
      private String Ddo_contratoservicosprazoregra_dias_Sortdsc ;
      private String Ddo_contratoservicosprazoregra_dias_Cleanfilter ;
      private String Ddo_contratoservicosprazoregra_dias_Rangefilterfrom ;
      private String Ddo_contratoservicosprazoregra_dias_Rangefilterto ;
      private String Ddo_contratoservicosprazoregra_dias_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfcontratoservicosprazoregra_sequencial_Internalname ;
      private String edtavTfcontratoservicosprazoregra_sequencial_Jsonclick ;
      private String edtavTfcontratoservicosprazoregra_sequencial_to_Internalname ;
      private String edtavTfcontratoservicosprazoregra_sequencial_to_Jsonclick ;
      private String edtavTfcontratoservicosprazoregra_inicio_Internalname ;
      private String edtavTfcontratoservicosprazoregra_inicio_Jsonclick ;
      private String edtavTfcontratoservicosprazoregra_inicio_to_Internalname ;
      private String edtavTfcontratoservicosprazoregra_inicio_to_Jsonclick ;
      private String edtavTfcontratoservicosprazoregra_fim_Internalname ;
      private String edtavTfcontratoservicosprazoregra_fim_Jsonclick ;
      private String edtavTfcontratoservicosprazoregra_fim_to_Internalname ;
      private String edtavTfcontratoservicosprazoregra_fim_to_Jsonclick ;
      private String edtavTfcontratoservicosprazoregra_dias_Internalname ;
      private String edtavTfcontratoservicosprazoregra_dias_Jsonclick ;
      private String edtavTfcontratoservicosprazoregra_dias_to_Internalname ;
      private String edtavTfcontratoservicosprazoregra_dias_to_Jsonclick ;
      private String edtavDdo_contratoservicosprazoregra_sequencialtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicosprazoregra_iniciotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicosprazoregra_fimtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicosprazoregra_diastitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtContratoServicosPrazoRegra_Sequencial_Internalname ;
      private String edtContratoServicosPrazoRegra_Inicio_Internalname ;
      private String edtContratoServicosPrazoRegra_Fim_Internalname ;
      private String edtContratoServicosPrazoRegra_Dias_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String A904ContratoServicosPrazo_Tipo ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavContratoservicosprazoregra_inicio1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavContratoservicosprazoregra_inicio2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavContratoservicosprazoregra_inicio3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_contratoservicosprazoregra_sequencial_Internalname ;
      private String Ddo_contratoservicosprazoregra_inicio_Internalname ;
      private String Ddo_contratoservicosprazoregra_fim_Internalname ;
      private String Ddo_contratoservicosprazoregra_dias_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtContratoServicosPrazoRegra_Sequencial_Title ;
      private String edtContratoServicosPrazoRegra_Inicio_Title ;
      private String edtContratoServicosPrazoRegra_Fim_Title ;
      private String edtContratoServicosPrazoRegra_Dias_Title ;
      private String edtavSelect_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavContratoservicosprazoregra_inicio3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavContratoservicosprazoregra_inicio2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavContratoservicosprazoregra_inicio1_Jsonclick ;
      private String sGXsfl_80_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtContratoServicosPrazoRegra_Sequencial_Jsonclick ;
      private String edtContratoServicosPrazoRegra_Inicio_Jsonclick ;
      private String edtContratoServicosPrazoRegra_Fim_Jsonclick ;
      private String edtContratoServicosPrazoRegra_Dias_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV15OrderedDsc ;
      private bool AV19DynamicFiltersEnabled2 ;
      private bool AV23DynamicFiltersEnabled3 ;
      private bool toggleJsOutput ;
      private bool AV28DynamicFiltersIgnoreFirst ;
      private bool AV27DynamicFiltersRemoving ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contratoservicosprazoregra_sequencial_Includesortasc ;
      private bool Ddo_contratoservicosprazoregra_sequencial_Includesortdsc ;
      private bool Ddo_contratoservicosprazoregra_sequencial_Includefilter ;
      private bool Ddo_contratoservicosprazoregra_sequencial_Filterisrange ;
      private bool Ddo_contratoservicosprazoregra_sequencial_Includedatalist ;
      private bool Ddo_contratoservicosprazoregra_inicio_Includesortasc ;
      private bool Ddo_contratoservicosprazoregra_inicio_Includesortdsc ;
      private bool Ddo_contratoservicosprazoregra_inicio_Includefilter ;
      private bool Ddo_contratoservicosprazoregra_inicio_Filterisrange ;
      private bool Ddo_contratoservicosprazoregra_inicio_Includedatalist ;
      private bool Ddo_contratoservicosprazoregra_fim_Includesortasc ;
      private bool Ddo_contratoservicosprazoregra_fim_Includesortdsc ;
      private bool Ddo_contratoservicosprazoregra_fim_Includefilter ;
      private bool Ddo_contratoservicosprazoregra_fim_Filterisrange ;
      private bool Ddo_contratoservicosprazoregra_fim_Includedatalist ;
      private bool Ddo_contratoservicosprazoregra_dias_Includesortasc ;
      private bool Ddo_contratoservicosprazoregra_dias_Includesortdsc ;
      private bool Ddo_contratoservicosprazoregra_dias_Includefilter ;
      private bool Ddo_contratoservicosprazoregra_dias_Filterisrange ;
      private bool Ddo_contratoservicosprazoregra_dias_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV29Select_IsBlob ;
      private String AV16DynamicFiltersSelector1 ;
      private String AV20DynamicFiltersSelector2 ;
      private String AV24DynamicFiltersSelector3 ;
      private String AV34ddo_ContratoServicosPrazoRegra_SequencialTitleControlIdToReplace ;
      private String AV38ddo_ContratoServicosPrazoRegra_InicioTitleControlIdToReplace ;
      private String AV42ddo_ContratoServicosPrazoRegra_FimTitleControlIdToReplace ;
      private String AV46ddo_ContratoServicosPrazoRegra_DiasTitleControlIdToReplace ;
      private String AV53Select_GXI ;
      private String AV29Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private short aP1_InOutContratoServicosPrazoRegra_Sequencial ;
      private decimal aP2_InOutContratoServicosPrazoRegra_Inicio ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H00G22_A903ContratoServicosPrazo_CntSrvCod ;
      private String[] H00G22_A904ContratoServicosPrazo_Tipo ;
      private short[] H00G22_A909ContratoServicosPrazoRegra_Dias ;
      private decimal[] H00G22_A908ContratoServicosPrazoRegra_Fim ;
      private decimal[] H00G22_A907ContratoServicosPrazoRegra_Inicio ;
      private short[] H00G22_A906ContratoServicosPrazoRegra_Sequencial ;
      private long[] H00G23_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV31ContratoServicosPrazoRegra_SequencialTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV35ContratoServicosPrazoRegra_InicioTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV39ContratoServicosPrazoRegra_FimTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV43ContratoServicosPrazoRegra_DiasTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV13GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV47DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class promptcontratoservicosprazocontratoservicosprazoregra__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00G22( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             short AV17DynamicFiltersOperator1 ,
                                             decimal AV18ContratoServicosPrazoRegra_Inicio1 ,
                                             bool AV19DynamicFiltersEnabled2 ,
                                             String AV20DynamicFiltersSelector2 ,
                                             short AV21DynamicFiltersOperator2 ,
                                             decimal AV22ContratoServicosPrazoRegra_Inicio2 ,
                                             bool AV23DynamicFiltersEnabled3 ,
                                             String AV24DynamicFiltersSelector3 ,
                                             short AV25DynamicFiltersOperator3 ,
                                             decimal AV26ContratoServicosPrazoRegra_Inicio3 ,
                                             short AV32TFContratoServicosPrazoRegra_Sequencial ,
                                             short AV33TFContratoServicosPrazoRegra_Sequencial_To ,
                                             decimal AV36TFContratoServicosPrazoRegra_Inicio ,
                                             decimal AV37TFContratoServicosPrazoRegra_Inicio_To ,
                                             decimal AV40TFContratoServicosPrazoRegra_Fim ,
                                             decimal AV41TFContratoServicosPrazoRegra_Fim_To ,
                                             short AV44TFContratoServicosPrazoRegra_Dias ,
                                             short AV45TFContratoServicosPrazoRegra_Dias_To ,
                                             decimal A907ContratoServicosPrazoRegra_Inicio ,
                                             short A906ContratoServicosPrazoRegra_Sequencial ,
                                             decimal A908ContratoServicosPrazoRegra_Fim ,
                                             short A909ContratoServicosPrazoRegra_Dias ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A903ContratoServicosPrazo_CntSrvCod ,
                                             int AV7InContratoServicosPrazo_CntSrvCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [23] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[ContratoServicosPrazo_CntSrvCod], T2.[ContratoServicosPrazo_Tipo], T1.[ContratoServicosPrazoRegra_Dias], T1.[ContratoServicosPrazoRegra_Fim], T1.[ContratoServicosPrazoRegra_Inicio], T1.[ContratoServicosPrazoRegra_Sequencial]";
         sFromString = " FROM ([ContratoServicosPrazoContratoServicosPrazoRegra] T1 WITH (NOLOCK) INNER JOIN [ContratoServicosPrazo] T2 WITH (NOLOCK) ON T2.[ContratoServicosPrazo_CntSrvCod] = T1.[ContratoServicosPrazo_CntSrvCod])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[ContratoServicosPrazo_CntSrvCod] = @AV7InContratoServicosPrazo_CntSrvCod)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATOSERVICOSPRAZOREGRA_INICIO") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! (Convert.ToDecimal(0)==AV18ContratoServicosPrazoRegra_Inicio1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosPrazoRegra_Inicio] < @AV18ContratoServicosPrazoRegra_Inicio1)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATOSERVICOSPRAZOREGRA_INICIO") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! (Convert.ToDecimal(0)==AV18ContratoServicosPrazoRegra_Inicio1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosPrazoRegra_Inicio] = @AV18ContratoServicosPrazoRegra_Inicio1)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATOSERVICOSPRAZOREGRA_INICIO") == 0 ) && ( AV17DynamicFiltersOperator1 == 2 ) && ( ! (Convert.ToDecimal(0)==AV18ContratoServicosPrazoRegra_Inicio1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosPrazoRegra_Inicio] > @AV18ContratoServicosPrazoRegra_Inicio1)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATOSERVICOSPRAZOREGRA_INICIO") == 0 ) && ( AV21DynamicFiltersOperator2 == 0 ) && ( ! (Convert.ToDecimal(0)==AV22ContratoServicosPrazoRegra_Inicio2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosPrazoRegra_Inicio] < @AV22ContratoServicosPrazoRegra_Inicio2)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATOSERVICOSPRAZOREGRA_INICIO") == 0 ) && ( AV21DynamicFiltersOperator2 == 1 ) && ( ! (Convert.ToDecimal(0)==AV22ContratoServicosPrazoRegra_Inicio2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosPrazoRegra_Inicio] = @AV22ContratoServicosPrazoRegra_Inicio2)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATOSERVICOSPRAZOREGRA_INICIO") == 0 ) && ( AV21DynamicFiltersOperator2 == 2 ) && ( ! (Convert.ToDecimal(0)==AV22ContratoServicosPrazoRegra_Inicio2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosPrazoRegra_Inicio] > @AV22ContratoServicosPrazoRegra_Inicio2)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CONTRATOSERVICOSPRAZOREGRA_INICIO") == 0 ) && ( AV25DynamicFiltersOperator3 == 0 ) && ( ! (Convert.ToDecimal(0)==AV26ContratoServicosPrazoRegra_Inicio3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosPrazoRegra_Inicio] < @AV26ContratoServicosPrazoRegra_Inicio3)";
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CONTRATOSERVICOSPRAZOREGRA_INICIO") == 0 ) && ( AV25DynamicFiltersOperator3 == 1 ) && ( ! (Convert.ToDecimal(0)==AV26ContratoServicosPrazoRegra_Inicio3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosPrazoRegra_Inicio] = @AV26ContratoServicosPrazoRegra_Inicio3)";
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CONTRATOSERVICOSPRAZOREGRA_INICIO") == 0 ) && ( AV25DynamicFiltersOperator3 == 2 ) && ( ! (Convert.ToDecimal(0)==AV26ContratoServicosPrazoRegra_Inicio3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosPrazoRegra_Inicio] > @AV26ContratoServicosPrazoRegra_Inicio3)";
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ! (0==AV32TFContratoServicosPrazoRegra_Sequencial) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosPrazoRegra_Sequencial] >= @AV32TFContratoServicosPrazoRegra_Sequencial)";
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ! (0==AV33TFContratoServicosPrazoRegra_Sequencial_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosPrazoRegra_Sequencial] <= @AV33TFContratoServicosPrazoRegra_Sequencial_To)";
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV36TFContratoServicosPrazoRegra_Inicio) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosPrazoRegra_Inicio] >= @AV36TFContratoServicosPrazoRegra_Inicio)";
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV37TFContratoServicosPrazoRegra_Inicio_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosPrazoRegra_Inicio] <= @AV37TFContratoServicosPrazoRegra_Inicio_To)";
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV40TFContratoServicosPrazoRegra_Fim) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosPrazoRegra_Fim] >= @AV40TFContratoServicosPrazoRegra_Fim)";
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV41TFContratoServicosPrazoRegra_Fim_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosPrazoRegra_Fim] <= @AV41TFContratoServicosPrazoRegra_Fim_To)";
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( ! (0==AV44TFContratoServicosPrazoRegra_Dias) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosPrazoRegra_Dias] >= @AV44TFContratoServicosPrazoRegra_Dias)";
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( ! (0==AV45TFContratoServicosPrazoRegra_Dias_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosPrazoRegra_Dias] <= @AV45TFContratoServicosPrazoRegra_Dias_To)";
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( AV14OrderedBy == 1 )
         {
            sOrderString = sOrderString + " ORDER BY T2.[ContratoServicosPrazo_Tipo]";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosPrazoRegra_Sequencial]";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosPrazoRegra_Sequencial] DESC";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosPrazoRegra_Inicio]";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosPrazoRegra_Inicio] DESC";
         }
         else if ( ( AV14OrderedBy == 4 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosPrazoRegra_Fim]";
         }
         else if ( ( AV14OrderedBy == 4 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosPrazoRegra_Fim] DESC";
         }
         else if ( ( AV14OrderedBy == 5 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosPrazoRegra_Dias]";
         }
         else if ( ( AV14OrderedBy == 5 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosPrazoRegra_Dias] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosPrazo_CntSrvCod], T1.[ContratoServicosPrazoRegra_Sequencial]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00G23( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             short AV17DynamicFiltersOperator1 ,
                                             decimal AV18ContratoServicosPrazoRegra_Inicio1 ,
                                             bool AV19DynamicFiltersEnabled2 ,
                                             String AV20DynamicFiltersSelector2 ,
                                             short AV21DynamicFiltersOperator2 ,
                                             decimal AV22ContratoServicosPrazoRegra_Inicio2 ,
                                             bool AV23DynamicFiltersEnabled3 ,
                                             String AV24DynamicFiltersSelector3 ,
                                             short AV25DynamicFiltersOperator3 ,
                                             decimal AV26ContratoServicosPrazoRegra_Inicio3 ,
                                             short AV32TFContratoServicosPrazoRegra_Sequencial ,
                                             short AV33TFContratoServicosPrazoRegra_Sequencial_To ,
                                             decimal AV36TFContratoServicosPrazoRegra_Inicio ,
                                             decimal AV37TFContratoServicosPrazoRegra_Inicio_To ,
                                             decimal AV40TFContratoServicosPrazoRegra_Fim ,
                                             decimal AV41TFContratoServicosPrazoRegra_Fim_To ,
                                             short AV44TFContratoServicosPrazoRegra_Dias ,
                                             short AV45TFContratoServicosPrazoRegra_Dias_To ,
                                             decimal A907ContratoServicosPrazoRegra_Inicio ,
                                             short A906ContratoServicosPrazoRegra_Sequencial ,
                                             decimal A908ContratoServicosPrazoRegra_Fim ,
                                             short A909ContratoServicosPrazoRegra_Dias ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A903ContratoServicosPrazo_CntSrvCod ,
                                             int AV7InContratoServicosPrazo_CntSrvCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [18] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([ContratoServicosPrazoContratoServicosPrazoRegra] T1 WITH (NOLOCK) INNER JOIN [ContratoServicosPrazo] T2 WITH (NOLOCK) ON T2.[ContratoServicosPrazo_CntSrvCod] = T1.[ContratoServicosPrazo_CntSrvCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratoServicosPrazo_CntSrvCod] = @AV7InContratoServicosPrazo_CntSrvCod)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATOSERVICOSPRAZOREGRA_INICIO") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! (Convert.ToDecimal(0)==AV18ContratoServicosPrazoRegra_Inicio1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosPrazoRegra_Inicio] < @AV18ContratoServicosPrazoRegra_Inicio1)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATOSERVICOSPRAZOREGRA_INICIO") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! (Convert.ToDecimal(0)==AV18ContratoServicosPrazoRegra_Inicio1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosPrazoRegra_Inicio] = @AV18ContratoServicosPrazoRegra_Inicio1)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATOSERVICOSPRAZOREGRA_INICIO") == 0 ) && ( AV17DynamicFiltersOperator1 == 2 ) && ( ! (Convert.ToDecimal(0)==AV18ContratoServicosPrazoRegra_Inicio1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosPrazoRegra_Inicio] > @AV18ContratoServicosPrazoRegra_Inicio1)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATOSERVICOSPRAZOREGRA_INICIO") == 0 ) && ( AV21DynamicFiltersOperator2 == 0 ) && ( ! (Convert.ToDecimal(0)==AV22ContratoServicosPrazoRegra_Inicio2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosPrazoRegra_Inicio] < @AV22ContratoServicosPrazoRegra_Inicio2)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATOSERVICOSPRAZOREGRA_INICIO") == 0 ) && ( AV21DynamicFiltersOperator2 == 1 ) && ( ! (Convert.ToDecimal(0)==AV22ContratoServicosPrazoRegra_Inicio2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosPrazoRegra_Inicio] = @AV22ContratoServicosPrazoRegra_Inicio2)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATOSERVICOSPRAZOREGRA_INICIO") == 0 ) && ( AV21DynamicFiltersOperator2 == 2 ) && ( ! (Convert.ToDecimal(0)==AV22ContratoServicosPrazoRegra_Inicio2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosPrazoRegra_Inicio] > @AV22ContratoServicosPrazoRegra_Inicio2)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CONTRATOSERVICOSPRAZOREGRA_INICIO") == 0 ) && ( AV25DynamicFiltersOperator3 == 0 ) && ( ! (Convert.ToDecimal(0)==AV26ContratoServicosPrazoRegra_Inicio3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosPrazoRegra_Inicio] < @AV26ContratoServicosPrazoRegra_Inicio3)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CONTRATOSERVICOSPRAZOREGRA_INICIO") == 0 ) && ( AV25DynamicFiltersOperator3 == 1 ) && ( ! (Convert.ToDecimal(0)==AV26ContratoServicosPrazoRegra_Inicio3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosPrazoRegra_Inicio] = @AV26ContratoServicosPrazoRegra_Inicio3)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CONTRATOSERVICOSPRAZOREGRA_INICIO") == 0 ) && ( AV25DynamicFiltersOperator3 == 2 ) && ( ! (Convert.ToDecimal(0)==AV26ContratoServicosPrazoRegra_Inicio3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosPrazoRegra_Inicio] > @AV26ContratoServicosPrazoRegra_Inicio3)";
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ! (0==AV32TFContratoServicosPrazoRegra_Sequencial) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosPrazoRegra_Sequencial] >= @AV32TFContratoServicosPrazoRegra_Sequencial)";
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! (0==AV33TFContratoServicosPrazoRegra_Sequencial_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosPrazoRegra_Sequencial] <= @AV33TFContratoServicosPrazoRegra_Sequencial_To)";
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV36TFContratoServicosPrazoRegra_Inicio) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosPrazoRegra_Inicio] >= @AV36TFContratoServicosPrazoRegra_Inicio)";
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV37TFContratoServicosPrazoRegra_Inicio_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosPrazoRegra_Inicio] <= @AV37TFContratoServicosPrazoRegra_Inicio_To)";
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV40TFContratoServicosPrazoRegra_Fim) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosPrazoRegra_Fim] >= @AV40TFContratoServicosPrazoRegra_Fim)";
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV41TFContratoServicosPrazoRegra_Fim_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosPrazoRegra_Fim] <= @AV41TFContratoServicosPrazoRegra_Fim_To)";
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( ! (0==AV44TFContratoServicosPrazoRegra_Dias) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosPrazoRegra_Dias] >= @AV44TFContratoServicosPrazoRegra_Dias)";
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( ! (0==AV45TFContratoServicosPrazoRegra_Dias_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosPrazoRegra_Dias] <= @AV45TFContratoServicosPrazoRegra_Dias_To)";
         }
         else
         {
            GXv_int4[17] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( AV14OrderedBy == 1 )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 4 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 4 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 5 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 5 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00G22(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (decimal)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (decimal)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (decimal)dynConstraints[10] , (short)dynConstraints[11] , (short)dynConstraints[12] , (decimal)dynConstraints[13] , (decimal)dynConstraints[14] , (decimal)dynConstraints[15] , (decimal)dynConstraints[16] , (short)dynConstraints[17] , (short)dynConstraints[18] , (decimal)dynConstraints[19] , (short)dynConstraints[20] , (decimal)dynConstraints[21] , (short)dynConstraints[22] , (short)dynConstraints[23] , (bool)dynConstraints[24] , (int)dynConstraints[25] , (int)dynConstraints[26] );
               case 1 :
                     return conditional_H00G23(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (decimal)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (decimal)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (decimal)dynConstraints[10] , (short)dynConstraints[11] , (short)dynConstraints[12] , (decimal)dynConstraints[13] , (decimal)dynConstraints[14] , (decimal)dynConstraints[15] , (decimal)dynConstraints[16] , (short)dynConstraints[17] , (short)dynConstraints[18] , (decimal)dynConstraints[19] , (short)dynConstraints[20] , (decimal)dynConstraints[21] , (short)dynConstraints[22] , (short)dynConstraints[23] , (bool)dynConstraints[24] , (int)dynConstraints[25] , (int)dynConstraints[26] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00G22 ;
          prmH00G22 = new Object[] {
          new Object[] {"@AV7InContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18ContratoServicosPrazoRegra_Inicio1",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV18ContratoServicosPrazoRegra_Inicio1",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV18ContratoServicosPrazoRegra_Inicio1",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV22ContratoServicosPrazoRegra_Inicio2",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV22ContratoServicosPrazoRegra_Inicio2",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV22ContratoServicosPrazoRegra_Inicio2",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV26ContratoServicosPrazoRegra_Inicio3",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV26ContratoServicosPrazoRegra_Inicio3",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV26ContratoServicosPrazoRegra_Inicio3",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV32TFContratoServicosPrazoRegra_Sequencial",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV33TFContratoServicosPrazoRegra_Sequencial_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV36TFContratoServicosPrazoRegra_Inicio",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV37TFContratoServicosPrazoRegra_Inicio_To",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV40TFContratoServicosPrazoRegra_Fim",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV41TFContratoServicosPrazoRegra_Fim_To",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV44TFContratoServicosPrazoRegra_Dias",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV45TFContratoServicosPrazoRegra_Dias_To",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00G23 ;
          prmH00G23 = new Object[] {
          new Object[] {"@AV7InContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18ContratoServicosPrazoRegra_Inicio1",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV18ContratoServicosPrazoRegra_Inicio1",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV18ContratoServicosPrazoRegra_Inicio1",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV22ContratoServicosPrazoRegra_Inicio2",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV22ContratoServicosPrazoRegra_Inicio2",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV22ContratoServicosPrazoRegra_Inicio2",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV26ContratoServicosPrazoRegra_Inicio3",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV26ContratoServicosPrazoRegra_Inicio3",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV26ContratoServicosPrazoRegra_Inicio3",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV32TFContratoServicosPrazoRegra_Sequencial",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV33TFContratoServicosPrazoRegra_Sequencial_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV36TFContratoServicosPrazoRegra_Inicio",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV37TFContratoServicosPrazoRegra_Inicio_To",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV40TFContratoServicosPrazoRegra_Fim",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV41TFContratoServicosPrazoRegra_Fim_To",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV44TFContratoServicosPrazoRegra_Dias",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV45TFContratoServicosPrazoRegra_Dias_To",SqlDbType.SmallInt,3,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00G22", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00G22,11,0,true,false )
             ,new CursorDef("H00G23", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00G23,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                ((short[]) buf[5])[0] = rslt.getShort(6) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[24]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[25]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[26]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[27]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[28]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[29]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[30]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[31]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[32]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[33]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[34]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[35]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[36]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[37]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[38]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[39]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[40]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[42]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[18]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[19]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[20]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[21]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[22]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[23]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[24]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[25]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[26]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[27]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[28]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[29]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[30]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[31]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[32]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[33]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[34]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[35]);
                }
                return;
       }
    }

 }

}
