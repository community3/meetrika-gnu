/*
               File: SistemaVersaoWC
        Description: Sistema Versao WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/26/2020 9:12:1.68
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class sistemaversaowc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public sistemaversaowc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public sistemaversaowc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_SistemaVersao_SistemaCod )
      {
         this.AV34SistemaVersao_SistemaCod = aP0_SistemaVersao_SistemaCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV34SistemaVersao_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34SistemaVersao_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34SistemaVersao_SistemaCod), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV34SistemaVersao_SistemaCod});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_8 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_8_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_8_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
                  AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
                  AV17TFSistemaVersao_Data = context.localUtil.ParseDTimeParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17TFSistemaVersao_Data", context.localUtil.TToC( AV17TFSistemaVersao_Data, 8, 5, 0, 3, "/", ":", " "));
                  AV18TFSistemaVersao_Data_To = context.localUtil.ParseDTimeParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18TFSistemaVersao_Data_To", context.localUtil.TToC( AV18TFSistemaVersao_Data_To, 8, 5, 0, 3, "/", ":", " "));
                  AV23TFSistemaVersao_Id = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFSistemaVersao_Id", AV23TFSistemaVersao_Id);
                  AV24TFSistemaVersao_Id_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24TFSistemaVersao_Id_Sel", AV24TFSistemaVersao_Id_Sel);
                  AV27TFSistemaVersao_Descricao = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27TFSistemaVersao_Descricao", AV27TFSistemaVersao_Descricao);
                  AV28TFSistemaVersao_Descricao_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28TFSistemaVersao_Descricao_Sel", AV28TFSistemaVersao_Descricao_Sel);
                  AV21ddo_SistemaVersao_DataTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21ddo_SistemaVersao_DataTitleControlIdToReplace", AV21ddo_SistemaVersao_DataTitleControlIdToReplace);
                  AV25ddo_SistemaVersao_IdTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25ddo_SistemaVersao_IdTitleControlIdToReplace", AV25ddo_SistemaVersao_IdTitleControlIdToReplace);
                  AV29ddo_SistemaVersao_DescricaoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29ddo_SistemaVersao_DescricaoTitleControlIdToReplace", AV29ddo_SistemaVersao_DescricaoTitleControlIdToReplace);
                  AV37Pgmname = GetNextPar( );
                  AV34SistemaVersao_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34SistemaVersao_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34SistemaVersao_SistemaCod), 6, 0)));
                  A1859SistemaVersao_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1859SistemaVersao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1859SistemaVersao_Codigo), 6, 0)));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV17TFSistemaVersao_Data, AV18TFSistemaVersao_Data_To, AV23TFSistemaVersao_Id, AV24TFSistemaVersao_Id_Sel, AV27TFSistemaVersao_Descricao, AV28TFSistemaVersao_Descricao_Sel, AV21ddo_SistemaVersao_DataTitleControlIdToReplace, AV25ddo_SistemaVersao_IdTitleControlIdToReplace, AV29ddo_SistemaVersao_DescricaoTitleControlIdToReplace, AV37Pgmname, AV34SistemaVersao_SistemaCod, A1859SistemaVersao_Codigo, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAOC2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV37Pgmname = "SistemaVersaoWC";
               context.Gx_err = 0;
               WSOC2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Sistema Versao WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020326912188");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("sistemaversaowc.aspx") + "?" + UrlEncode("" +AV34SistemaVersao_SistemaCod)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSISTEMAVERSAO_DATA", context.localUtil.TToC( AV17TFSistemaVersao_Data, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSISTEMAVERSAO_DATA_TO", context.localUtil.TToC( AV18TFSistemaVersao_Data_To, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSISTEMAVERSAO_ID", StringUtil.RTrim( AV23TFSistemaVersao_Id));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSISTEMAVERSAO_ID_SEL", StringUtil.RTrim( AV24TFSistemaVersao_Id_Sel));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSISTEMAVERSAO_DESCRICAO", AV27TFSistemaVersao_Descricao);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFSISTEMAVERSAO_DESCRICAO_SEL", AV28TFSistemaVersao_Descricao_Sel);
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_8", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_8), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV33GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV30DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV30DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vSISTEMAVERSAO_DATATITLEFILTERDATA", AV16SistemaVersao_DataTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vSISTEMAVERSAO_DATATITLEFILTERDATA", AV16SistemaVersao_DataTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vSISTEMAVERSAO_IDTITLEFILTERDATA", AV22SistemaVersao_IdTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vSISTEMAVERSAO_IDTITLEFILTERDATA", AV22SistemaVersao_IdTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vSISTEMAVERSAO_DESCRICAOTITLEFILTERDATA", AV26SistemaVersao_DescricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vSISTEMAVERSAO_DESCRICAOTITLEFILTERDATA", AV26SistemaVersao_DescricaoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV34SistemaVersao_SistemaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV34SistemaVersao_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV37Pgmname));
         GxWebStd.gx_hidden_field( context, sPrefix+"vSISTEMAVERSAO_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV34SistemaVersao_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"SISTEMAVERSAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1859SistemaVersao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DATA_Caption", StringUtil.RTrim( Ddo_sistemaversao_data_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DATA_Tooltip", StringUtil.RTrim( Ddo_sistemaversao_data_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DATA_Cls", StringUtil.RTrim( Ddo_sistemaversao_data_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DATA_Filteredtext_set", StringUtil.RTrim( Ddo_sistemaversao_data_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DATA_Filteredtextto_set", StringUtil.RTrim( Ddo_sistemaversao_data_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DATA_Dropdownoptionstype", StringUtil.RTrim( Ddo_sistemaversao_data_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DATA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_sistemaversao_data_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DATA_Includesortasc", StringUtil.BoolToStr( Ddo_sistemaversao_data_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DATA_Includesortdsc", StringUtil.BoolToStr( Ddo_sistemaversao_data_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DATA_Sortedstatus", StringUtil.RTrim( Ddo_sistemaversao_data_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DATA_Includefilter", StringUtil.BoolToStr( Ddo_sistemaversao_data_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DATA_Filtertype", StringUtil.RTrim( Ddo_sistemaversao_data_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DATA_Filterisrange", StringUtil.BoolToStr( Ddo_sistemaversao_data_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DATA_Includedatalist", StringUtil.BoolToStr( Ddo_sistemaversao_data_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DATA_Sortasc", StringUtil.RTrim( Ddo_sistemaversao_data_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DATA_Sortdsc", StringUtil.RTrim( Ddo_sistemaversao_data_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DATA_Cleanfilter", StringUtil.RTrim( Ddo_sistemaversao_data_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DATA_Rangefilterfrom", StringUtil.RTrim( Ddo_sistemaversao_data_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DATA_Rangefilterto", StringUtil.RTrim( Ddo_sistemaversao_data_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DATA_Searchbuttontext", StringUtil.RTrim( Ddo_sistemaversao_data_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_ID_Caption", StringUtil.RTrim( Ddo_sistemaversao_id_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_ID_Tooltip", StringUtil.RTrim( Ddo_sistemaversao_id_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_ID_Cls", StringUtil.RTrim( Ddo_sistemaversao_id_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_ID_Filteredtext_set", StringUtil.RTrim( Ddo_sistemaversao_id_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_ID_Selectedvalue_set", StringUtil.RTrim( Ddo_sistemaversao_id_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_ID_Dropdownoptionstype", StringUtil.RTrim( Ddo_sistemaversao_id_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_ID_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_sistemaversao_id_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_ID_Includesortasc", StringUtil.BoolToStr( Ddo_sistemaversao_id_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_ID_Includesortdsc", StringUtil.BoolToStr( Ddo_sistemaversao_id_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_ID_Sortedstatus", StringUtil.RTrim( Ddo_sistemaversao_id_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_ID_Includefilter", StringUtil.BoolToStr( Ddo_sistemaversao_id_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_ID_Filtertype", StringUtil.RTrim( Ddo_sistemaversao_id_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_ID_Filterisrange", StringUtil.BoolToStr( Ddo_sistemaversao_id_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_ID_Includedatalist", StringUtil.BoolToStr( Ddo_sistemaversao_id_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_ID_Datalisttype", StringUtil.RTrim( Ddo_sistemaversao_id_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_ID_Datalistproc", StringUtil.RTrim( Ddo_sistemaversao_id_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_ID_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_sistemaversao_id_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_ID_Sortasc", StringUtil.RTrim( Ddo_sistemaversao_id_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_ID_Sortdsc", StringUtil.RTrim( Ddo_sistemaversao_id_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_ID_Loadingdata", StringUtil.RTrim( Ddo_sistemaversao_id_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_ID_Cleanfilter", StringUtil.RTrim( Ddo_sistemaversao_id_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_ID_Noresultsfound", StringUtil.RTrim( Ddo_sistemaversao_id_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_ID_Searchbuttontext", StringUtil.RTrim( Ddo_sistemaversao_id_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Caption", StringUtil.RTrim( Ddo_sistemaversao_descricao_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_sistemaversao_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Cls", StringUtil.RTrim( Ddo_sistemaversao_descricao_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_sistemaversao_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_sistemaversao_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_sistemaversao_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_sistemaversao_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_sistemaversao_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_sistemaversao_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_sistemaversao_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_sistemaversao_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_sistemaversao_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_sistemaversao_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_sistemaversao_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_sistemaversao_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_sistemaversao_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_sistemaversao_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_sistemaversao_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_sistemaversao_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_sistemaversao_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_sistemaversao_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_sistemaversao_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_sistemaversao_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DATA_Activeeventkey", StringUtil.RTrim( Ddo_sistemaversao_data_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DATA_Filteredtext_get", StringUtil.RTrim( Ddo_sistemaversao_data_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DATA_Filteredtextto_get", StringUtil.RTrim( Ddo_sistemaversao_data_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_ID_Activeeventkey", StringUtil.RTrim( Ddo_sistemaversao_id_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_ID_Filteredtext_get", StringUtil.RTrim( Ddo_sistemaversao_id_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_ID_Selectedvalue_get", StringUtil.RTrim( Ddo_sistemaversao_id_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_sistemaversao_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_sistemaversao_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_sistemaversao_descricao_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormOC2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("sistemaversaowc.js", "?2020326912296");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "SistemaVersaoWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Sistema Versao WC" ;
      }

      protected void WBOC0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "sistemaversaowc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            wb_table1_2_OC2( true) ;
         }
         else
         {
            wb_table1_2_OC2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_OC2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrderedby_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV13OrderedBy), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,18);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrderedby_Jsonclick, 0, "Attribute", "", "", "", edtavOrderedby_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaVersaoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_SistemaVersaoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfsistemaversao_data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfsistemaversao_data_Internalname, context.localUtil.TToC( AV17TFSistemaVersao_Data, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV17TFSistemaVersao_Data, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,20);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsistemaversao_data_Jsonclick, 0, "Attribute", "", "", "", edtavTfsistemaversao_data_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaVersaoWC.htm");
            GxWebStd.gx_bitmap( context, edtavTfsistemaversao_data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfsistemaversao_data_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_SistemaVersaoWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfsistemaversao_data_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfsistemaversao_data_to_Internalname, context.localUtil.TToC( AV18TFSistemaVersao_Data_To, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV18TFSistemaVersao_Data_To, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,21);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsistemaversao_data_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfsistemaversao_data_to_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaVersaoWC.htm");
            GxWebStd.gx_bitmap( context, edtavTfsistemaversao_data_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfsistemaversao_data_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_SistemaVersaoWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_sistemaversao_dataauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_sistemaversao_dataauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_sistemaversao_dataauxdate_Internalname, context.localUtil.Format(AV19DDO_SistemaVersao_DataAuxDate, "99/99/99"), context.localUtil.Format( AV19DDO_SistemaVersao_DataAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,23);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_sistemaversao_dataauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaVersaoWC.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_sistemaversao_dataauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_SistemaVersaoWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_sistemaversao_dataauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_sistemaversao_dataauxdateto_Internalname, context.localUtil.Format(AV20DDO_SistemaVersao_DataAuxDateTo, "99/99/99"), context.localUtil.Format( AV20DDO_SistemaVersao_DataAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,24);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_sistemaversao_dataauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaVersaoWC.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_sistemaversao_dataauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_SistemaVersaoWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsistemaversao_id_Internalname, StringUtil.RTrim( AV23TFSistemaVersao_Id), StringUtil.RTrim( context.localUtil.Format( AV23TFSistemaVersao_Id, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,25);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsistemaversao_id_Jsonclick, 0, "Attribute", "", "", "", edtavTfsistemaversao_id_Visible, 1, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_SistemaVersaoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsistemaversao_id_sel_Internalname, StringUtil.RTrim( AV24TFSistemaVersao_Id_Sel), StringUtil.RTrim( context.localUtil.Format( AV24TFSistemaVersao_Id_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,26);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsistemaversao_id_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfsistemaversao_id_sel_Visible, 1, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_SistemaVersaoWC.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfsistemaversao_descricao_Internalname, AV27TFSistemaVersao_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,27);\"", 0, edtavTfsistemaversao_descricao_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_SistemaVersaoWC.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfsistemaversao_descricao_sel_Internalname, AV28TFSistemaVersao_Descricao_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", 0, edtavTfsistemaversao_descricao_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_SistemaVersaoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_SISTEMAVERSAO_DATAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_sistemaversao_datatitlecontrolidtoreplace_Internalname, AV21ddo_SistemaVersao_DataTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,30);\"", 0, edtavDdo_sistemaversao_datatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_SistemaVersaoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_SISTEMAVERSAO_IDContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_sistemaversao_idtitlecontrolidtoreplace_Internalname, AV25ddo_SistemaVersao_IdTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,32);\"", 0, edtavDdo_sistemaversao_idtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_SistemaVersaoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_SISTEMAVERSAO_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_sistemaversao_descricaotitlecontrolidtoreplace_Internalname, AV29ddo_SistemaVersao_DescricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,34);\"", 0, edtavDdo_sistemaversao_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_SistemaVersaoWC.htm");
         }
         wbLoad = true;
      }

      protected void STARTOC2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Sistema Versao WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPOC0( ) ;
            }
         }
      }

      protected void WSOC2( )
      {
         STARTOC2( ) ;
         EVTOC2( ) ;
      }

      protected void EVTOC2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPOC0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPOC0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11OC2 */
                                    E11OC2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SISTEMAVERSAO_DATA.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPOC0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12OC2 */
                                    E12OC2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SISTEMAVERSAO_ID.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPOC0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13OC2 */
                                    E13OC2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SISTEMAVERSAO_DESCRICAO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPOC0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14OC2 */
                                    E14OC2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPOC0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15OC2 */
                                    E15OC2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPOC0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavOrderedby_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPOC0( ) ;
                              }
                              nGXsfl_8_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
                              SubsflControlProps_82( ) ;
                              A1865SistemaVersao_Data = context.localUtil.CToT( cgiGet( edtSistemaVersao_Data_Internalname), 0);
                              A1860SistemaVersao_Id = cgiGet( edtSistemaVersao_Id_Internalname);
                              A1861SistemaVersao_Descricao = cgiGet( edtSistemaVersao_Descricao_Internalname);
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E16OC2 */
                                          E16OC2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E17OC2 */
                                          E17OC2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E18OC2 */
                                          E18OC2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfsistemaversao_data Changed */
                                             if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFSISTEMAVERSAO_DATA"), 0) != AV17TFSistemaVersao_Data )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfsistemaversao_data_to Changed */
                                             if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFSISTEMAVERSAO_DATA_TO"), 0) != AV18TFSistemaVersao_Data_To )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfsistemaversao_id Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSISTEMAVERSAO_ID"), AV23TFSistemaVersao_Id) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfsistemaversao_id_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSISTEMAVERSAO_ID_SEL"), AV24TFSistemaVersao_Id_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfsistemaversao_descricao Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSISTEMAVERSAO_DESCRICAO"), AV27TFSistemaVersao_Descricao) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfsistemaversao_descricao_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSISTEMAVERSAO_DESCRICAO_SEL"), AV28TFSistemaVersao_Descricao_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPOC0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEOC2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormOC2( ) ;
            }
         }
      }

      protected void PAOC2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_82( ) ;
         while ( nGXsfl_8_idx <= nRC_GXsfl_8 )
         {
            sendrow_82( ) ;
            nGXsfl_8_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_8_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_8_idx+1));
            sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
            SubsflControlProps_82( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       DateTime AV17TFSistemaVersao_Data ,
                                       DateTime AV18TFSistemaVersao_Data_To ,
                                       String AV23TFSistemaVersao_Id ,
                                       String AV24TFSistemaVersao_Id_Sel ,
                                       String AV27TFSistemaVersao_Descricao ,
                                       String AV28TFSistemaVersao_Descricao_Sel ,
                                       String AV21ddo_SistemaVersao_DataTitleControlIdToReplace ,
                                       String AV25ddo_SistemaVersao_IdTitleControlIdToReplace ,
                                       String AV29ddo_SistemaVersao_DescricaoTitleControlIdToReplace ,
                                       String AV37Pgmname ,
                                       int AV34SistemaVersao_SistemaCod ,
                                       int A1859SistemaVersao_Codigo ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFOC2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SISTEMAVERSAO_DATA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1865SistemaVersao_Data, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"SISTEMAVERSAO_DATA", context.localUtil.TToC( A1865SistemaVersao_Data, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SISTEMAVERSAO_ID", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1860SistemaVersao_Id, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"SISTEMAVERSAO_ID", StringUtil.RTrim( A1860SistemaVersao_Id));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SISTEMAVERSAO_DESCRICAO", GetSecureSignedToken( sPrefix, A1861SistemaVersao_Descricao));
         GxWebStd.gx_hidden_field( context, sPrefix+"SISTEMAVERSAO_DESCRICAO", A1861SistemaVersao_Descricao);
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFOC2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV37Pgmname = "SistemaVersaoWC";
         context.Gx_err = 0;
      }

      protected void RFOC2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 8;
         /* Execute user event: E17OC2 */
         E17OC2 ();
         nGXsfl_8_idx = 1;
         sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
         SubsflControlProps_82( ) ;
         nGXsfl_8_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_82( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV17TFSistemaVersao_Data ,
                                                 AV18TFSistemaVersao_Data_To ,
                                                 AV24TFSistemaVersao_Id_Sel ,
                                                 AV23TFSistemaVersao_Id ,
                                                 AV28TFSistemaVersao_Descricao_Sel ,
                                                 AV27TFSistemaVersao_Descricao ,
                                                 A1865SistemaVersao_Data ,
                                                 A1860SistemaVersao_Id ,
                                                 A1861SistemaVersao_Descricao ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT,
                                                 TypeConstants.BOOLEAN
                                                 }
            });
            lV23TFSistemaVersao_Id = StringUtil.PadR( StringUtil.RTrim( AV23TFSistemaVersao_Id), 20, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFSistemaVersao_Id", AV23TFSistemaVersao_Id);
            lV27TFSistemaVersao_Descricao = StringUtil.Concat( StringUtil.RTrim( AV27TFSistemaVersao_Descricao), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27TFSistemaVersao_Descricao", AV27TFSistemaVersao_Descricao);
            /* Using cursor H00OC2 */
            pr_default.execute(0, new Object[] {AV17TFSistemaVersao_Data, AV18TFSistemaVersao_Data_To, lV23TFSistemaVersao_Id, AV24TFSistemaVersao_Id_Sel, lV27TFSistemaVersao_Descricao, AV28TFSistemaVersao_Descricao_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_8_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1859SistemaVersao_Codigo = H00OC2_A1859SistemaVersao_Codigo[0];
               A1861SistemaVersao_Descricao = H00OC2_A1861SistemaVersao_Descricao[0];
               A1860SistemaVersao_Id = H00OC2_A1860SistemaVersao_Id[0];
               A1865SistemaVersao_Data = H00OC2_A1865SistemaVersao_Data[0];
               /* Execute user event: E18OC2 */
               E18OC2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 8;
            WBOC0( ) ;
         }
         nGXsfl_8_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV17TFSistemaVersao_Data ,
                                              AV18TFSistemaVersao_Data_To ,
                                              AV24TFSistemaVersao_Id_Sel ,
                                              AV23TFSistemaVersao_Id ,
                                              AV28TFSistemaVersao_Descricao_Sel ,
                                              AV27TFSistemaVersao_Descricao ,
                                              A1865SistemaVersao_Data ,
                                              A1860SistemaVersao_Id ,
                                              A1861SistemaVersao_Descricao ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.BOOLEAN
                                              }
         });
         lV23TFSistemaVersao_Id = StringUtil.PadR( StringUtil.RTrim( AV23TFSistemaVersao_Id), 20, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFSistemaVersao_Id", AV23TFSistemaVersao_Id);
         lV27TFSistemaVersao_Descricao = StringUtil.Concat( StringUtil.RTrim( AV27TFSistemaVersao_Descricao), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27TFSistemaVersao_Descricao", AV27TFSistemaVersao_Descricao);
         /* Using cursor H00OC3 */
         pr_default.execute(1, new Object[] {AV17TFSistemaVersao_Data, AV18TFSistemaVersao_Data_To, lV23TFSistemaVersao_Id, AV24TFSistemaVersao_Id_Sel, lV27TFSistemaVersao_Descricao, AV28TFSistemaVersao_Descricao_Sel});
         GRID_nRecordCount = H00OC3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV17TFSistemaVersao_Data, AV18TFSistemaVersao_Data_To, AV23TFSistemaVersao_Id, AV24TFSistemaVersao_Id_Sel, AV27TFSistemaVersao_Descricao, AV28TFSistemaVersao_Descricao_Sel, AV21ddo_SistemaVersao_DataTitleControlIdToReplace, AV25ddo_SistemaVersao_IdTitleControlIdToReplace, AV29ddo_SistemaVersao_DescricaoTitleControlIdToReplace, AV37Pgmname, AV34SistemaVersao_SistemaCod, A1859SistemaVersao_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV17TFSistemaVersao_Data, AV18TFSistemaVersao_Data_To, AV23TFSistemaVersao_Id, AV24TFSistemaVersao_Id_Sel, AV27TFSistemaVersao_Descricao, AV28TFSistemaVersao_Descricao_Sel, AV21ddo_SistemaVersao_DataTitleControlIdToReplace, AV25ddo_SistemaVersao_IdTitleControlIdToReplace, AV29ddo_SistemaVersao_DescricaoTitleControlIdToReplace, AV37Pgmname, AV34SistemaVersao_SistemaCod, A1859SistemaVersao_Codigo, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV17TFSistemaVersao_Data, AV18TFSistemaVersao_Data_To, AV23TFSistemaVersao_Id, AV24TFSistemaVersao_Id_Sel, AV27TFSistemaVersao_Descricao, AV28TFSistemaVersao_Descricao_Sel, AV21ddo_SistemaVersao_DataTitleControlIdToReplace, AV25ddo_SistemaVersao_IdTitleControlIdToReplace, AV29ddo_SistemaVersao_DescricaoTitleControlIdToReplace, AV37Pgmname, AV34SistemaVersao_SistemaCod, A1859SistemaVersao_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV17TFSistemaVersao_Data, AV18TFSistemaVersao_Data_To, AV23TFSistemaVersao_Id, AV24TFSistemaVersao_Id_Sel, AV27TFSistemaVersao_Descricao, AV28TFSistemaVersao_Descricao_Sel, AV21ddo_SistemaVersao_DataTitleControlIdToReplace, AV25ddo_SistemaVersao_IdTitleControlIdToReplace, AV29ddo_SistemaVersao_DescricaoTitleControlIdToReplace, AV37Pgmname, AV34SistemaVersao_SistemaCod, A1859SistemaVersao_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV17TFSistemaVersao_Data, AV18TFSistemaVersao_Data_To, AV23TFSistemaVersao_Id, AV24TFSistemaVersao_Id_Sel, AV27TFSistemaVersao_Descricao, AV28TFSistemaVersao_Descricao_Sel, AV21ddo_SistemaVersao_DataTitleControlIdToReplace, AV25ddo_SistemaVersao_IdTitleControlIdToReplace, AV29ddo_SistemaVersao_DescricaoTitleControlIdToReplace, AV37Pgmname, AV34SistemaVersao_SistemaCod, A1859SistemaVersao_Codigo, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPOC0( )
      {
         /* Before Start, stand alone formulas. */
         AV37Pgmname = "SistemaVersaoWC";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E16OC2 */
         E16OC2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV30DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vSISTEMAVERSAO_DATATITLEFILTERDATA"), AV16SistemaVersao_DataTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vSISTEMAVERSAO_IDTITLEFILTERDATA"), AV22SistemaVersao_IdTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vSISTEMAVERSAO_DESCRICAOTITLEFILTERDATA"), AV26SistemaVersao_DescricaoTitleFilterData);
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vORDEREDBY");
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV13OrderedBy = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            else
            {
               AV13OrderedBy = (short)(context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfsistemaversao_data_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFSistema Versao_Data"}), 1, "vTFSISTEMAVERSAO_DATA");
               GX_FocusControl = edtavTfsistemaversao_data_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV17TFSistemaVersao_Data = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17TFSistemaVersao_Data", context.localUtil.TToC( AV17TFSistemaVersao_Data, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV17TFSistemaVersao_Data = context.localUtil.CToT( cgiGet( edtavTfsistemaversao_data_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17TFSistemaVersao_Data", context.localUtil.TToC( AV17TFSistemaVersao_Data, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfsistemaversao_data_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFSistema Versao_Data_To"}), 1, "vTFSISTEMAVERSAO_DATA_TO");
               GX_FocusControl = edtavTfsistemaversao_data_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV18TFSistemaVersao_Data_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18TFSistemaVersao_Data_To", context.localUtil.TToC( AV18TFSistemaVersao_Data_To, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV18TFSistemaVersao_Data_To = context.localUtil.CToT( cgiGet( edtavTfsistemaversao_data_to_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18TFSistemaVersao_Data_To", context.localUtil.TToC( AV18TFSistemaVersao_Data_To, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_sistemaversao_dataauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Sistema Versao_Data Aux Date"}), 1, "vDDO_SISTEMAVERSAO_DATAAUXDATE");
               GX_FocusControl = edtavDdo_sistemaversao_dataauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV19DDO_SistemaVersao_DataAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DDO_SistemaVersao_DataAuxDate", context.localUtil.Format(AV19DDO_SistemaVersao_DataAuxDate, "99/99/99"));
            }
            else
            {
               AV19DDO_SistemaVersao_DataAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_sistemaversao_dataauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DDO_SistemaVersao_DataAuxDate", context.localUtil.Format(AV19DDO_SistemaVersao_DataAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_sistemaversao_dataauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Sistema Versao_Data Aux Date To"}), 1, "vDDO_SISTEMAVERSAO_DATAAUXDATETO");
               GX_FocusControl = edtavDdo_sistemaversao_dataauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV20DDO_SistemaVersao_DataAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DDO_SistemaVersao_DataAuxDateTo", context.localUtil.Format(AV20DDO_SistemaVersao_DataAuxDateTo, "99/99/99"));
            }
            else
            {
               AV20DDO_SistemaVersao_DataAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_sistemaversao_dataauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DDO_SistemaVersao_DataAuxDateTo", context.localUtil.Format(AV20DDO_SistemaVersao_DataAuxDateTo, "99/99/99"));
            }
            AV23TFSistemaVersao_Id = cgiGet( edtavTfsistemaversao_id_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFSistemaVersao_Id", AV23TFSistemaVersao_Id);
            AV24TFSistemaVersao_Id_Sel = cgiGet( edtavTfsistemaversao_id_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24TFSistemaVersao_Id_Sel", AV24TFSistemaVersao_Id_Sel);
            AV27TFSistemaVersao_Descricao = cgiGet( edtavTfsistemaversao_descricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27TFSistemaVersao_Descricao", AV27TFSistemaVersao_Descricao);
            AV28TFSistemaVersao_Descricao_Sel = cgiGet( edtavTfsistemaversao_descricao_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28TFSistemaVersao_Descricao_Sel", AV28TFSistemaVersao_Descricao_Sel);
            AV21ddo_SistemaVersao_DataTitleControlIdToReplace = cgiGet( edtavDdo_sistemaversao_datatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21ddo_SistemaVersao_DataTitleControlIdToReplace", AV21ddo_SistemaVersao_DataTitleControlIdToReplace);
            AV25ddo_SistemaVersao_IdTitleControlIdToReplace = cgiGet( edtavDdo_sistemaversao_idtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25ddo_SistemaVersao_IdTitleControlIdToReplace", AV25ddo_SistemaVersao_IdTitleControlIdToReplace);
            AV29ddo_SistemaVersao_DescricaoTitleControlIdToReplace = cgiGet( edtavDdo_sistemaversao_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29ddo_SistemaVersao_DescricaoTitleControlIdToReplace", AV29ddo_SistemaVersao_DescricaoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_8 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_8"), ",", "."));
            AV32GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV33GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV34SistemaVersao_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV34SistemaVersao_SistemaCod"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_sistemaversao_data_Caption = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DATA_Caption");
            Ddo_sistemaversao_data_Tooltip = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DATA_Tooltip");
            Ddo_sistemaversao_data_Cls = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DATA_Cls");
            Ddo_sistemaversao_data_Filteredtext_set = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DATA_Filteredtext_set");
            Ddo_sistemaversao_data_Filteredtextto_set = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DATA_Filteredtextto_set");
            Ddo_sistemaversao_data_Dropdownoptionstype = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DATA_Dropdownoptionstype");
            Ddo_sistemaversao_data_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DATA_Titlecontrolidtoreplace");
            Ddo_sistemaversao_data_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DATA_Includesortasc"));
            Ddo_sistemaversao_data_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DATA_Includesortdsc"));
            Ddo_sistemaversao_data_Sortedstatus = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DATA_Sortedstatus");
            Ddo_sistemaversao_data_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DATA_Includefilter"));
            Ddo_sistemaversao_data_Filtertype = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DATA_Filtertype");
            Ddo_sistemaversao_data_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DATA_Filterisrange"));
            Ddo_sistemaversao_data_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DATA_Includedatalist"));
            Ddo_sistemaversao_data_Sortasc = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DATA_Sortasc");
            Ddo_sistemaversao_data_Sortdsc = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DATA_Sortdsc");
            Ddo_sistemaversao_data_Cleanfilter = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DATA_Cleanfilter");
            Ddo_sistemaversao_data_Rangefilterfrom = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DATA_Rangefilterfrom");
            Ddo_sistemaversao_data_Rangefilterto = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DATA_Rangefilterto");
            Ddo_sistemaversao_data_Searchbuttontext = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DATA_Searchbuttontext");
            Ddo_sistemaversao_id_Caption = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_ID_Caption");
            Ddo_sistemaversao_id_Tooltip = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_ID_Tooltip");
            Ddo_sistemaversao_id_Cls = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_ID_Cls");
            Ddo_sistemaversao_id_Filteredtext_set = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_ID_Filteredtext_set");
            Ddo_sistemaversao_id_Selectedvalue_set = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_ID_Selectedvalue_set");
            Ddo_sistemaversao_id_Dropdownoptionstype = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_ID_Dropdownoptionstype");
            Ddo_sistemaversao_id_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_ID_Titlecontrolidtoreplace");
            Ddo_sistemaversao_id_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SISTEMAVERSAO_ID_Includesortasc"));
            Ddo_sistemaversao_id_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SISTEMAVERSAO_ID_Includesortdsc"));
            Ddo_sistemaversao_id_Sortedstatus = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_ID_Sortedstatus");
            Ddo_sistemaversao_id_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SISTEMAVERSAO_ID_Includefilter"));
            Ddo_sistemaversao_id_Filtertype = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_ID_Filtertype");
            Ddo_sistemaversao_id_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SISTEMAVERSAO_ID_Filterisrange"));
            Ddo_sistemaversao_id_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SISTEMAVERSAO_ID_Includedatalist"));
            Ddo_sistemaversao_id_Datalisttype = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_ID_Datalisttype");
            Ddo_sistemaversao_id_Datalistproc = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_ID_Datalistproc");
            Ddo_sistemaversao_id_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_SISTEMAVERSAO_ID_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_sistemaversao_id_Sortasc = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_ID_Sortasc");
            Ddo_sistemaversao_id_Sortdsc = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_ID_Sortdsc");
            Ddo_sistemaversao_id_Loadingdata = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_ID_Loadingdata");
            Ddo_sistemaversao_id_Cleanfilter = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_ID_Cleanfilter");
            Ddo_sistemaversao_id_Noresultsfound = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_ID_Noresultsfound");
            Ddo_sistemaversao_id_Searchbuttontext = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_ID_Searchbuttontext");
            Ddo_sistemaversao_descricao_Caption = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Caption");
            Ddo_sistemaversao_descricao_Tooltip = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Tooltip");
            Ddo_sistemaversao_descricao_Cls = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Cls");
            Ddo_sistemaversao_descricao_Filteredtext_set = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Filteredtext_set");
            Ddo_sistemaversao_descricao_Selectedvalue_set = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Selectedvalue_set");
            Ddo_sistemaversao_descricao_Dropdownoptionstype = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Dropdownoptionstype");
            Ddo_sistemaversao_descricao_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_sistemaversao_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Includesortasc"));
            Ddo_sistemaversao_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Includesortdsc"));
            Ddo_sistemaversao_descricao_Sortedstatus = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Sortedstatus");
            Ddo_sistemaversao_descricao_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Includefilter"));
            Ddo_sistemaversao_descricao_Filtertype = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Filtertype");
            Ddo_sistemaversao_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Filterisrange"));
            Ddo_sistemaversao_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Includedatalist"));
            Ddo_sistemaversao_descricao_Datalisttype = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Datalisttype");
            Ddo_sistemaversao_descricao_Datalistproc = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Datalistproc");
            Ddo_sistemaversao_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_sistemaversao_descricao_Sortasc = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Sortasc");
            Ddo_sistemaversao_descricao_Sortdsc = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Sortdsc");
            Ddo_sistemaversao_descricao_Loadingdata = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Loadingdata");
            Ddo_sistemaversao_descricao_Cleanfilter = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Cleanfilter");
            Ddo_sistemaversao_descricao_Noresultsfound = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Noresultsfound");
            Ddo_sistemaversao_descricao_Searchbuttontext = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            Ddo_sistemaversao_data_Activeeventkey = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DATA_Activeeventkey");
            Ddo_sistemaversao_data_Filteredtext_get = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DATA_Filteredtext_get");
            Ddo_sistemaversao_data_Filteredtextto_get = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DATA_Filteredtextto_get");
            Ddo_sistemaversao_id_Activeeventkey = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_ID_Activeeventkey");
            Ddo_sistemaversao_id_Filteredtext_get = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_ID_Filteredtext_get");
            Ddo_sistemaversao_id_Selectedvalue_get = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_ID_Selectedvalue_get");
            Ddo_sistemaversao_descricao_Activeeventkey = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Activeeventkey");
            Ddo_sistemaversao_descricao_Filteredtext_get = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Filteredtext_get");
            Ddo_sistemaversao_descricao_Selectedvalue_get = cgiGet( sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFSISTEMAVERSAO_DATA"), 0) != AV17TFSistemaVersao_Data )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFSISTEMAVERSAO_DATA_TO"), 0) != AV18TFSistemaVersao_Data_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSISTEMAVERSAO_ID"), AV23TFSistemaVersao_Id) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSISTEMAVERSAO_ID_SEL"), AV24TFSistemaVersao_Id_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSISTEMAVERSAO_DESCRICAO"), AV27TFSistemaVersao_Descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFSISTEMAVERSAO_DESCRICAO_SEL"), AV28TFSistemaVersao_Descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E16OC2 */
         E16OC2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E16OC2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtavTfsistemaversao_data_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfsistemaversao_data_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsistemaversao_data_Visible), 5, 0)));
         edtavTfsistemaversao_data_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfsistemaversao_data_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsistemaversao_data_to_Visible), 5, 0)));
         edtavTfsistemaversao_id_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfsistemaversao_id_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsistemaversao_id_Visible), 5, 0)));
         edtavTfsistemaversao_id_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfsistemaversao_id_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsistemaversao_id_sel_Visible), 5, 0)));
         edtavTfsistemaversao_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfsistemaversao_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsistemaversao_descricao_Visible), 5, 0)));
         edtavTfsistemaversao_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfsistemaversao_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsistemaversao_descricao_sel_Visible), 5, 0)));
         Ddo_sistemaversao_data_Titlecontrolidtoreplace = subGrid_Internalname+"_SistemaVersao_Data";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_sistemaversao_data_Internalname, "TitleControlIdToReplace", Ddo_sistemaversao_data_Titlecontrolidtoreplace);
         AV21ddo_SistemaVersao_DataTitleControlIdToReplace = Ddo_sistemaversao_data_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21ddo_SistemaVersao_DataTitleControlIdToReplace", AV21ddo_SistemaVersao_DataTitleControlIdToReplace);
         edtavDdo_sistemaversao_datatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_sistemaversao_datatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_sistemaversao_datatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_sistemaversao_id_Titlecontrolidtoreplace = subGrid_Internalname+"_SistemaVersao_Id";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_sistemaversao_id_Internalname, "TitleControlIdToReplace", Ddo_sistemaversao_id_Titlecontrolidtoreplace);
         AV25ddo_SistemaVersao_IdTitleControlIdToReplace = Ddo_sistemaversao_id_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25ddo_SistemaVersao_IdTitleControlIdToReplace", AV25ddo_SistemaVersao_IdTitleControlIdToReplace);
         edtavDdo_sistemaversao_idtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_sistemaversao_idtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_sistemaversao_idtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_sistemaversao_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_SistemaVersao_Descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_sistemaversao_descricao_Internalname, "TitleControlIdToReplace", Ddo_sistemaversao_descricao_Titlecontrolidtoreplace);
         AV29ddo_SistemaVersao_DescricaoTitleControlIdToReplace = Ddo_sistemaversao_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29ddo_SistemaVersao_DescricaoTitleControlIdToReplace", AV29ddo_SistemaVersao_DescricaoTitleControlIdToReplace);
         edtavDdo_sistemaversao_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_sistemaversao_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_sistemaversao_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavOrderedby_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrderedby_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrderedby_Visible), 5, 0)));
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV30DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV30DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E17OC2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV16SistemaVersao_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV22SistemaVersao_IdTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV26SistemaVersao_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtSistemaVersao_Data_Titleformat = 2;
         edtSistemaVersao_Data_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Data", AV21ddo_SistemaVersao_DataTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtSistemaVersao_Data_Internalname, "Title", edtSistemaVersao_Data_Title);
         edtSistemaVersao_Id_Titleformat = 2;
         edtSistemaVersao_Id_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Vers�o", AV25ddo_SistemaVersao_IdTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtSistemaVersao_Id_Internalname, "Title", edtSistemaVersao_Id_Title);
         edtSistemaVersao_Descricao_Titleformat = 2;
         edtSistemaVersao_Descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Descri��o", AV29ddo_SistemaVersao_DescricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtSistemaVersao_Descricao_Internalname, "Title", edtSistemaVersao_Descricao_Title);
         AV32GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32GridCurrentPage), 10, 0)));
         AV33GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33GridPageCount), 10, 0)));
         imgInsert_Visible = (AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgInsert_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Visible), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV16SistemaVersao_DataTitleFilterData", AV16SistemaVersao_DataTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV22SistemaVersao_IdTitleFilterData", AV22SistemaVersao_IdTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV26SistemaVersao_DescricaoTitleFilterData", AV26SistemaVersao_DescricaoTitleFilterData);
      }

      protected void E11OC2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV31PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV31PageToGo) ;
         }
      }

      protected void E12OC2( )
      {
         /* Ddo_sistemaversao_data_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_sistemaversao_data_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_sistemaversao_data_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_sistemaversao_data_Internalname, "SortedStatus", Ddo_sistemaversao_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_sistemaversao_data_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_sistemaversao_data_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_sistemaversao_data_Internalname, "SortedStatus", Ddo_sistemaversao_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_sistemaversao_data_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV17TFSistemaVersao_Data = context.localUtil.CToT( Ddo_sistemaversao_data_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17TFSistemaVersao_Data", context.localUtil.TToC( AV17TFSistemaVersao_Data, 8, 5, 0, 3, "/", ":", " "));
            AV18TFSistemaVersao_Data_To = context.localUtil.CToT( Ddo_sistemaversao_data_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18TFSistemaVersao_Data_To", context.localUtil.TToC( AV18TFSistemaVersao_Data_To, 8, 5, 0, 3, "/", ":", " "));
            if ( ! (DateTime.MinValue==AV18TFSistemaVersao_Data_To) )
            {
               AV18TFSistemaVersao_Data_To = context.localUtil.YMDHMSToT( (short)(DateTimeUtil.Year( AV18TFSistemaVersao_Data_To)), (short)(DateTimeUtil.Month( AV18TFSistemaVersao_Data_To)), (short)(DateTimeUtil.Day( AV18TFSistemaVersao_Data_To)), 23, 59, 59);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18TFSistemaVersao_Data_To", context.localUtil.TToC( AV18TFSistemaVersao_Data_To, 8, 5, 0, 3, "/", ":", " "));
            }
            subgrid_firstpage( ) ;
         }
      }

      protected void E13OC2( )
      {
         /* Ddo_sistemaversao_id_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_sistemaversao_id_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_sistemaversao_id_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_sistemaversao_id_Internalname, "SortedStatus", Ddo_sistemaversao_id_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_sistemaversao_id_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_sistemaversao_id_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_sistemaversao_id_Internalname, "SortedStatus", Ddo_sistemaversao_id_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_sistemaversao_id_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV23TFSistemaVersao_Id = Ddo_sistemaversao_id_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFSistemaVersao_Id", AV23TFSistemaVersao_Id);
            AV24TFSistemaVersao_Id_Sel = Ddo_sistemaversao_id_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24TFSistemaVersao_Id_Sel", AV24TFSistemaVersao_Id_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E14OC2( )
      {
         /* Ddo_sistemaversao_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_sistemaversao_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_sistemaversao_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_sistemaversao_descricao_Internalname, "SortedStatus", Ddo_sistemaversao_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_sistemaversao_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_sistemaversao_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_sistemaversao_descricao_Internalname, "SortedStatus", Ddo_sistemaversao_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_sistemaversao_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV27TFSistemaVersao_Descricao = Ddo_sistemaversao_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27TFSistemaVersao_Descricao", AV27TFSistemaVersao_Descricao);
            AV28TFSistemaVersao_Descricao_Sel = Ddo_sistemaversao_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28TFSistemaVersao_Descricao_Sel", AV28TFSistemaVersao_Descricao_Sel);
            subgrid_firstpage( ) ;
         }
      }

      private void E18OC2( )
      {
         /* Grid_Load Routine */
         edtSistemaVersao_Id_Link = formatLink("viewsistemaversao.aspx") + "?" + UrlEncode("" +A1859SistemaVersao_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 8;
         }
         sendrow_82( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_8_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(8, GridRow);
         }
      }

      protected void E15OC2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("sistemaversao.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode("" +AV34SistemaVersao_SistemaCod);
         context.wjLocDisableFrm = 1;
      }

      protected void S152( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_sistemaversao_data_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_sistemaversao_data_Internalname, "SortedStatus", Ddo_sistemaversao_data_Sortedstatus);
         Ddo_sistemaversao_id_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_sistemaversao_id_Internalname, "SortedStatus", Ddo_sistemaversao_id_Sortedstatus);
         Ddo_sistemaversao_descricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_sistemaversao_descricao_Internalname, "SortedStatus", Ddo_sistemaversao_descricao_Sortedstatus);
      }

      protected void S132( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_sistemaversao_data_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_sistemaversao_data_Internalname, "SortedStatus", Ddo_sistemaversao_data_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_sistemaversao_id_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_sistemaversao_id_Internalname, "SortedStatus", Ddo_sistemaversao_id_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_sistemaversao_descricao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_sistemaversao_descricao_Internalname, "SortedStatus", Ddo_sistemaversao_descricao_Sortedstatus);
         }
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV15Session.Get(AV37Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV37Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV15Session.Get(AV37Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV38GXV1 = 1;
         while ( AV38GXV1 <= AV11GridState.gxTpr_Filtervalues.Count )
         {
            AV12GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV11GridState.gxTpr_Filtervalues.Item(AV38GXV1));
            if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFSISTEMAVERSAO_DATA") == 0 )
            {
               AV17TFSistemaVersao_Data = context.localUtil.CToT( AV12GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17TFSistemaVersao_Data", context.localUtil.TToC( AV17TFSistemaVersao_Data, 8, 5, 0, 3, "/", ":", " "));
               AV18TFSistemaVersao_Data_To = context.localUtil.CToT( AV12GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18TFSistemaVersao_Data_To", context.localUtil.TToC( AV18TFSistemaVersao_Data_To, 8, 5, 0, 3, "/", ":", " "));
               if ( ! (DateTime.MinValue==AV17TFSistemaVersao_Data) )
               {
                  AV19DDO_SistemaVersao_DataAuxDate = DateTimeUtil.ResetTime(AV17TFSistemaVersao_Data);
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19DDO_SistemaVersao_DataAuxDate", context.localUtil.Format(AV19DDO_SistemaVersao_DataAuxDate, "99/99/99"));
                  Ddo_sistemaversao_data_Filteredtext_set = context.localUtil.DToC( AV19DDO_SistemaVersao_DataAuxDate, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_sistemaversao_data_Internalname, "FilteredText_set", Ddo_sistemaversao_data_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV18TFSistemaVersao_Data_To) )
               {
                  AV20DDO_SistemaVersao_DataAuxDateTo = DateTimeUtil.ResetTime(AV18TFSistemaVersao_Data_To);
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DDO_SistemaVersao_DataAuxDateTo", context.localUtil.Format(AV20DDO_SistemaVersao_DataAuxDateTo, "99/99/99"));
                  Ddo_sistemaversao_data_Filteredtextto_set = context.localUtil.DToC( AV20DDO_SistemaVersao_DataAuxDateTo, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_sistemaversao_data_Internalname, "FilteredTextTo_set", Ddo_sistemaversao_data_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFSISTEMAVERSAO_ID") == 0 )
            {
               AV23TFSistemaVersao_Id = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFSistemaVersao_Id", AV23TFSistemaVersao_Id);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFSistemaVersao_Id)) )
               {
                  Ddo_sistemaversao_id_Filteredtext_set = AV23TFSistemaVersao_Id;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_sistemaversao_id_Internalname, "FilteredText_set", Ddo_sistemaversao_id_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFSISTEMAVERSAO_ID_SEL") == 0 )
            {
               AV24TFSistemaVersao_Id_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24TFSistemaVersao_Id_Sel", AV24TFSistemaVersao_Id_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24TFSistemaVersao_Id_Sel)) )
               {
                  Ddo_sistemaversao_id_Selectedvalue_set = AV24TFSistemaVersao_Id_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_sistemaversao_id_Internalname, "SelectedValue_set", Ddo_sistemaversao_id_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFSISTEMAVERSAO_DESCRICAO") == 0 )
            {
               AV27TFSistemaVersao_Descricao = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27TFSistemaVersao_Descricao", AV27TFSistemaVersao_Descricao);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27TFSistemaVersao_Descricao)) )
               {
                  Ddo_sistemaversao_descricao_Filteredtext_set = AV27TFSistemaVersao_Descricao;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_sistemaversao_descricao_Internalname, "FilteredText_set", Ddo_sistemaversao_descricao_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFSISTEMAVERSAO_DESCRICAO_SEL") == 0 )
            {
               AV28TFSistemaVersao_Descricao_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28TFSistemaVersao_Descricao_Sel", AV28TFSistemaVersao_Descricao_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28TFSistemaVersao_Descricao_Sel)) )
               {
                  Ddo_sistemaversao_descricao_Selectedvalue_set = AV28TFSistemaVersao_Descricao_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_sistemaversao_descricao_Internalname, "SelectedValue_set", Ddo_sistemaversao_descricao_Selectedvalue_set);
               }
            }
            AV38GXV1 = (int)(AV38GXV1+1);
         }
      }

      protected void S142( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV15Session.Get(AV37Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (DateTime.MinValue==AV17TFSistemaVersao_Data) && (DateTime.MinValue==AV18TFSistemaVersao_Data_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFSISTEMAVERSAO_DATA";
            AV12GridStateFilterValue.gxTpr_Value = context.localUtil.TToC( AV17TFSistemaVersao_Data, 8, 5, 0, 3, "/", ":", " ");
            AV12GridStateFilterValue.gxTpr_Valueto = context.localUtil.TToC( AV18TFSistemaVersao_Data_To, 8, 5, 0, 3, "/", ":", " ");
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFSistemaVersao_Id)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFSISTEMAVERSAO_ID";
            AV12GridStateFilterValue.gxTpr_Value = AV23TFSistemaVersao_Id;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24TFSistemaVersao_Id_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFSISTEMAVERSAO_ID_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV24TFSistemaVersao_Id_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27TFSistemaVersao_Descricao)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFSISTEMAVERSAO_DESCRICAO";
            AV12GridStateFilterValue.gxTpr_Value = AV27TFSistemaVersao_Descricao;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28TFSistemaVersao_Descricao_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFSISTEMAVERSAO_DESCRICAO_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV28TFSistemaVersao_Descricao_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV34SistemaVersao_SistemaCod) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "PARM_&SISTEMAVERSAO_SISTEMACOD";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV34SistemaVersao_SistemaCod), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV37Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV37Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "SistemaVersao";
         AV15Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_OC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 2, 5, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table2_5_OC2( true) ;
         }
         else
         {
            wb_table2_5_OC2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_OC2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgInsert_Visible, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SistemaVersaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_OC2e( true) ;
         }
         else
         {
            wb_table1_2_OC2e( false) ;
         }
      }

      protected void wb_table2_5_OC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"8\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSistemaVersao_Data_Titleformat == 0 )
               {
                  context.SendWebValue( edtSistemaVersao_Data_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSistemaVersao_Data_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSistemaVersao_Id_Titleformat == 0 )
               {
                  context.SendWebValue( edtSistemaVersao_Id_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSistemaVersao_Id_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSistemaVersao_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtSistemaVersao_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSistemaVersao_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( A1865SistemaVersao_Data, 10, 8, 0, 3, "/", ":", " "));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSistemaVersao_Data_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSistemaVersao_Data_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1860SistemaVersao_Id));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSistemaVersao_Id_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSistemaVersao_Id_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtSistemaVersao_Id_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A1861SistemaVersao_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSistemaVersao_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSistemaVersao_Descricao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 8 )
         {
            wbEnd = 0;
            nRC_GXsfl_8 = (short)(nGXsfl_8_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_OC2e( true) ;
         }
         else
         {
            wb_table2_5_OC2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV34SistemaVersao_SistemaCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34SistemaVersao_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34SistemaVersao_SistemaCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAOC2( ) ;
         WSOC2( ) ;
         WEOC2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV34SistemaVersao_SistemaCod = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAOC2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "sistemaversaowc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAOC2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV34SistemaVersao_SistemaCod = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34SistemaVersao_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34SistemaVersao_SistemaCod), 6, 0)));
         }
         wcpOAV34SistemaVersao_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV34SistemaVersao_SistemaCod"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV34SistemaVersao_SistemaCod != wcpOAV34SistemaVersao_SistemaCod ) ) )
         {
            setjustcreated();
         }
         wcpOAV34SistemaVersao_SistemaCod = AV34SistemaVersao_SistemaCod;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV34SistemaVersao_SistemaCod = cgiGet( sPrefix+"AV34SistemaVersao_SistemaCod_CTRL");
         if ( StringUtil.Len( sCtrlAV34SistemaVersao_SistemaCod) > 0 )
         {
            AV34SistemaVersao_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sCtrlAV34SistemaVersao_SistemaCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34SistemaVersao_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34SistemaVersao_SistemaCod), 6, 0)));
         }
         else
         {
            AV34SistemaVersao_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV34SistemaVersao_SistemaCod_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAOC2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSOC2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSOC2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV34SistemaVersao_SistemaCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV34SistemaVersao_SistemaCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV34SistemaVersao_SistemaCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV34SistemaVersao_SistemaCod_CTRL", StringUtil.RTrim( sCtrlAV34SistemaVersao_SistemaCod));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEOC2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020326912651");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("sistemaversaowc.js", "?2020326912652");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_82( )
      {
         edtSistemaVersao_Data_Internalname = sPrefix+"SISTEMAVERSAO_DATA_"+sGXsfl_8_idx;
         edtSistemaVersao_Id_Internalname = sPrefix+"SISTEMAVERSAO_ID_"+sGXsfl_8_idx;
         edtSistemaVersao_Descricao_Internalname = sPrefix+"SISTEMAVERSAO_DESCRICAO_"+sGXsfl_8_idx;
      }

      protected void SubsflControlProps_fel_82( )
      {
         edtSistemaVersao_Data_Internalname = sPrefix+"SISTEMAVERSAO_DATA_"+sGXsfl_8_fel_idx;
         edtSistemaVersao_Id_Internalname = sPrefix+"SISTEMAVERSAO_ID_"+sGXsfl_8_fel_idx;
         edtSistemaVersao_Descricao_Internalname = sPrefix+"SISTEMAVERSAO_DESCRICAO_"+sGXsfl_8_fel_idx;
      }

      protected void sendrow_82( )
      {
         SubsflControlProps_82( ) ;
         WBOC0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_8_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_8_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_8_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSistemaVersao_Data_Internalname,context.localUtil.TToC( A1865SistemaVersao_Data, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A1865SistemaVersao_Data, "99/99/99 99:99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSistemaVersao_Data_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSistemaVersao_Id_Internalname,StringUtil.RTrim( A1860SistemaVersao_Id),(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)edtSistemaVersao_Id_Link,(String)"",(String)"",(String)"",(String)edtSistemaVersao_Id_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)20,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSistemaVersao_Descricao_Internalname,(String)A1861SistemaVersao_Descricao,(String)A1861SistemaVersao_Descricao,(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSistemaVersao_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)500,(short)0,(short)0,(short)8,(short)1,(short)0,(short)-1,(bool)true,(String)"DescricaoLonga",(String)"left",(bool)false});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SISTEMAVERSAO_DATA"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, context.localUtil.Format( A1865SistemaVersao_Data, "99/99/99 99:99")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SISTEMAVERSAO_ID"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, StringUtil.RTrim( context.localUtil.Format( A1860SistemaVersao_Id, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SISTEMAVERSAO_DESCRICAO"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, A1861SistemaVersao_Descricao));
            GridContainer.AddRow(GridRow);
            nGXsfl_8_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_8_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_8_idx+1));
            sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
            SubsflControlProps_82( ) ;
         }
         /* End function sendrow_82 */
      }

      protected void init_default_properties( )
      {
         edtSistemaVersao_Data_Internalname = sPrefix+"SISTEMAVERSAO_DATA";
         edtSistemaVersao_Id_Internalname = sPrefix+"SISTEMAVERSAO_ID";
         edtSistemaVersao_Descricao_Internalname = sPrefix+"SISTEMAVERSAO_DESCRICAO";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         imgInsert_Internalname = sPrefix+"INSERT";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         edtavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         edtavTfsistemaversao_data_Internalname = sPrefix+"vTFSISTEMAVERSAO_DATA";
         edtavTfsistemaversao_data_to_Internalname = sPrefix+"vTFSISTEMAVERSAO_DATA_TO";
         edtavDdo_sistemaversao_dataauxdate_Internalname = sPrefix+"vDDO_SISTEMAVERSAO_DATAAUXDATE";
         edtavDdo_sistemaversao_dataauxdateto_Internalname = sPrefix+"vDDO_SISTEMAVERSAO_DATAAUXDATETO";
         divDdo_sistemaversao_dataauxdates_Internalname = sPrefix+"DDO_SISTEMAVERSAO_DATAAUXDATES";
         edtavTfsistemaversao_id_Internalname = sPrefix+"vTFSISTEMAVERSAO_ID";
         edtavTfsistemaversao_id_sel_Internalname = sPrefix+"vTFSISTEMAVERSAO_ID_SEL";
         edtavTfsistemaversao_descricao_Internalname = sPrefix+"vTFSISTEMAVERSAO_DESCRICAO";
         edtavTfsistemaversao_descricao_sel_Internalname = sPrefix+"vTFSISTEMAVERSAO_DESCRICAO_SEL";
         Ddo_sistemaversao_data_Internalname = sPrefix+"DDO_SISTEMAVERSAO_DATA";
         edtavDdo_sistemaversao_datatitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_SISTEMAVERSAO_DATATITLECONTROLIDTOREPLACE";
         Ddo_sistemaversao_id_Internalname = sPrefix+"DDO_SISTEMAVERSAO_ID";
         edtavDdo_sistemaversao_idtitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_SISTEMAVERSAO_IDTITLECONTROLIDTOREPLACE";
         Ddo_sistemaversao_descricao_Internalname = sPrefix+"DDO_SISTEMAVERSAO_DESCRICAO";
         edtavDdo_sistemaversao_descricaotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_SISTEMAVERSAO_DESCRICAOTITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtSistemaVersao_Descricao_Jsonclick = "";
         edtSistemaVersao_Id_Jsonclick = "";
         edtSistemaVersao_Data_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtSistemaVersao_Id_Link = "";
         edtSistemaVersao_Descricao_Titleformat = 0;
         edtSistemaVersao_Id_Titleformat = 0;
         edtSistemaVersao_Data_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         imgInsert_Visible = 1;
         edtSistemaVersao_Descricao_Title = "Descri��o";
         edtSistemaVersao_Id_Title = "Vers�o";
         edtSistemaVersao_Data_Title = "Data";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_sistemaversao_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_sistemaversao_idtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_sistemaversao_datatitlecontrolidtoreplace_Visible = 1;
         edtavTfsistemaversao_descricao_sel_Visible = 1;
         edtavTfsistemaversao_descricao_Visible = 1;
         edtavTfsistemaversao_id_sel_Jsonclick = "";
         edtavTfsistemaversao_id_sel_Visible = 1;
         edtavTfsistemaversao_id_Jsonclick = "";
         edtavTfsistemaversao_id_Visible = 1;
         edtavDdo_sistemaversao_dataauxdateto_Jsonclick = "";
         edtavDdo_sistemaversao_dataauxdate_Jsonclick = "";
         edtavTfsistemaversao_data_to_Jsonclick = "";
         edtavTfsistemaversao_data_to_Visible = 1;
         edtavTfsistemaversao_data_Jsonclick = "";
         edtavTfsistemaversao_data_Visible = 1;
         edtavOrdereddsc_Jsonclick = "";
         edtavOrdereddsc_Visible = 1;
         edtavOrderedby_Jsonclick = "";
         edtavOrderedby_Visible = 1;
         Ddo_sistemaversao_descricao_Searchbuttontext = "Pesquisar";
         Ddo_sistemaversao_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_sistemaversao_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_sistemaversao_descricao_Loadingdata = "Carregando dados...";
         Ddo_sistemaversao_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_sistemaversao_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_sistemaversao_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_sistemaversao_descricao_Datalistproc = "GetSistemaVersaoWCFilterData";
         Ddo_sistemaversao_descricao_Datalisttype = "Dynamic";
         Ddo_sistemaversao_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_sistemaversao_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_sistemaversao_descricao_Filtertype = "Character";
         Ddo_sistemaversao_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_sistemaversao_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_sistemaversao_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_sistemaversao_descricao_Titlecontrolidtoreplace = "";
         Ddo_sistemaversao_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_sistemaversao_descricao_Cls = "ColumnSettings";
         Ddo_sistemaversao_descricao_Tooltip = "Op��es";
         Ddo_sistemaversao_descricao_Caption = "";
         Ddo_sistemaversao_id_Searchbuttontext = "Pesquisar";
         Ddo_sistemaversao_id_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_sistemaversao_id_Cleanfilter = "Limpar pesquisa";
         Ddo_sistemaversao_id_Loadingdata = "Carregando dados...";
         Ddo_sistemaversao_id_Sortdsc = "Ordenar de Z � A";
         Ddo_sistemaversao_id_Sortasc = "Ordenar de A � Z";
         Ddo_sistemaversao_id_Datalistupdateminimumcharacters = 0;
         Ddo_sistemaversao_id_Datalistproc = "GetSistemaVersaoWCFilterData";
         Ddo_sistemaversao_id_Datalisttype = "Dynamic";
         Ddo_sistemaversao_id_Includedatalist = Convert.ToBoolean( -1);
         Ddo_sistemaversao_id_Filterisrange = Convert.ToBoolean( 0);
         Ddo_sistemaversao_id_Filtertype = "Character";
         Ddo_sistemaversao_id_Includefilter = Convert.ToBoolean( -1);
         Ddo_sistemaversao_id_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_sistemaversao_id_Includesortasc = Convert.ToBoolean( -1);
         Ddo_sistemaversao_id_Titlecontrolidtoreplace = "";
         Ddo_sistemaversao_id_Dropdownoptionstype = "GridTitleSettings";
         Ddo_sistemaversao_id_Cls = "ColumnSettings";
         Ddo_sistemaversao_id_Tooltip = "Op��es";
         Ddo_sistemaversao_id_Caption = "";
         Ddo_sistemaversao_data_Searchbuttontext = "Pesquisar";
         Ddo_sistemaversao_data_Rangefilterto = "At�";
         Ddo_sistemaversao_data_Rangefilterfrom = "Desde";
         Ddo_sistemaversao_data_Cleanfilter = "Limpar pesquisa";
         Ddo_sistemaversao_data_Sortdsc = "Ordenar de Z � A";
         Ddo_sistemaversao_data_Sortasc = "Ordenar de A � Z";
         Ddo_sistemaversao_data_Includedatalist = Convert.ToBoolean( 0);
         Ddo_sistemaversao_data_Filterisrange = Convert.ToBoolean( -1);
         Ddo_sistemaversao_data_Filtertype = "Date";
         Ddo_sistemaversao_data_Includefilter = Convert.ToBoolean( -1);
         Ddo_sistemaversao_data_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_sistemaversao_data_Includesortasc = Convert.ToBoolean( -1);
         Ddo_sistemaversao_data_Titlecontrolidtoreplace = "";
         Ddo_sistemaversao_data_Dropdownoptionstype = "GridTitleSettings";
         Ddo_sistemaversao_data_Cls = "ColumnSettings";
         Ddo_sistemaversao_data_Tooltip = "Op��es";
         Ddo_sistemaversao_data_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A1859SistemaVersao_Codigo',fld:'SISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'AV21ddo_SistemaVersao_DataTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV25ddo_SistemaVersao_IdTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_IDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV29ddo_SistemaVersao_DescricaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17TFSistemaVersao_Data',fld:'vTFSISTEMAVERSAO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV18TFSistemaVersao_Data_To',fld:'vTFSISTEMAVERSAO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV23TFSistemaVersao_Id',fld:'vTFSISTEMAVERSAO_ID',pic:'',nv:''},{av:'AV24TFSistemaVersao_Id_Sel',fld:'vTFSISTEMAVERSAO_ID_SEL',pic:'',nv:''},{av:'AV27TFSistemaVersao_Descricao',fld:'vTFSISTEMAVERSAO_DESCRICAO',pic:'',nv:''},{av:'AV28TFSistemaVersao_Descricao_Sel',fld:'vTFSISTEMAVERSAO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV34SistemaVersao_SistemaCod',fld:'vSISTEMAVERSAO_SISTEMACOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV16SistemaVersao_DataTitleFilterData',fld:'vSISTEMAVERSAO_DATATITLEFILTERDATA',pic:'',nv:null},{av:'AV22SistemaVersao_IdTitleFilterData',fld:'vSISTEMAVERSAO_IDTITLEFILTERDATA',pic:'',nv:null},{av:'AV26SistemaVersao_DescricaoTitleFilterData',fld:'vSISTEMAVERSAO_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'edtSistemaVersao_Data_Titleformat',ctrl:'SISTEMAVERSAO_DATA',prop:'Titleformat'},{av:'edtSistemaVersao_Data_Title',ctrl:'SISTEMAVERSAO_DATA',prop:'Title'},{av:'edtSistemaVersao_Id_Titleformat',ctrl:'SISTEMAVERSAO_ID',prop:'Titleformat'},{av:'edtSistemaVersao_Id_Title',ctrl:'SISTEMAVERSAO_ID',prop:'Title'},{av:'edtSistemaVersao_Descricao_Titleformat',ctrl:'SISTEMAVERSAO_DESCRICAO',prop:'Titleformat'},{av:'edtSistemaVersao_Descricao_Title',ctrl:'SISTEMAVERSAO_DESCRICAO',prop:'Title'},{av:'AV32GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV33GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'imgInsert_Visible',ctrl:'INSERT',prop:'Visible'}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11OC2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17TFSistemaVersao_Data',fld:'vTFSISTEMAVERSAO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV18TFSistemaVersao_Data_To',fld:'vTFSISTEMAVERSAO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV23TFSistemaVersao_Id',fld:'vTFSISTEMAVERSAO_ID',pic:'',nv:''},{av:'AV24TFSistemaVersao_Id_Sel',fld:'vTFSISTEMAVERSAO_ID_SEL',pic:'',nv:''},{av:'AV27TFSistemaVersao_Descricao',fld:'vTFSISTEMAVERSAO_DESCRICAO',pic:'',nv:''},{av:'AV28TFSistemaVersao_Descricao_Sel',fld:'vTFSISTEMAVERSAO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV21ddo_SistemaVersao_DataTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV25ddo_SistemaVersao_IdTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_IDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV29ddo_SistemaVersao_DescricaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV34SistemaVersao_SistemaCod',fld:'vSISTEMAVERSAO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A1859SistemaVersao_Codigo',fld:'SISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_SISTEMAVERSAO_DATA.ONOPTIONCLICKED","{handler:'E12OC2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17TFSistemaVersao_Data',fld:'vTFSISTEMAVERSAO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV18TFSistemaVersao_Data_To',fld:'vTFSISTEMAVERSAO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV23TFSistemaVersao_Id',fld:'vTFSISTEMAVERSAO_ID',pic:'',nv:''},{av:'AV24TFSistemaVersao_Id_Sel',fld:'vTFSISTEMAVERSAO_ID_SEL',pic:'',nv:''},{av:'AV27TFSistemaVersao_Descricao',fld:'vTFSISTEMAVERSAO_DESCRICAO',pic:'',nv:''},{av:'AV28TFSistemaVersao_Descricao_Sel',fld:'vTFSISTEMAVERSAO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV21ddo_SistemaVersao_DataTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV25ddo_SistemaVersao_IdTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_IDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV29ddo_SistemaVersao_DescricaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV34SistemaVersao_SistemaCod',fld:'vSISTEMAVERSAO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A1859SistemaVersao_Codigo',fld:'SISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Ddo_sistemaversao_data_Activeeventkey',ctrl:'DDO_SISTEMAVERSAO_DATA',prop:'ActiveEventKey'},{av:'Ddo_sistemaversao_data_Filteredtext_get',ctrl:'DDO_SISTEMAVERSAO_DATA',prop:'FilteredText_get'},{av:'Ddo_sistemaversao_data_Filteredtextto_get',ctrl:'DDO_SISTEMAVERSAO_DATA',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_sistemaversao_data_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_DATA',prop:'SortedStatus'},{av:'AV17TFSistemaVersao_Data',fld:'vTFSISTEMAVERSAO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV18TFSistemaVersao_Data_To',fld:'vTFSISTEMAVERSAO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_sistemaversao_id_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_ID',prop:'SortedStatus'},{av:'Ddo_sistemaversao_descricao_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_DESCRICAO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SISTEMAVERSAO_ID.ONOPTIONCLICKED","{handler:'E13OC2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17TFSistemaVersao_Data',fld:'vTFSISTEMAVERSAO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV18TFSistemaVersao_Data_To',fld:'vTFSISTEMAVERSAO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV23TFSistemaVersao_Id',fld:'vTFSISTEMAVERSAO_ID',pic:'',nv:''},{av:'AV24TFSistemaVersao_Id_Sel',fld:'vTFSISTEMAVERSAO_ID_SEL',pic:'',nv:''},{av:'AV27TFSistemaVersao_Descricao',fld:'vTFSISTEMAVERSAO_DESCRICAO',pic:'',nv:''},{av:'AV28TFSistemaVersao_Descricao_Sel',fld:'vTFSISTEMAVERSAO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV21ddo_SistemaVersao_DataTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV25ddo_SistemaVersao_IdTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_IDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV29ddo_SistemaVersao_DescricaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV34SistemaVersao_SistemaCod',fld:'vSISTEMAVERSAO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A1859SistemaVersao_Codigo',fld:'SISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Ddo_sistemaversao_id_Activeeventkey',ctrl:'DDO_SISTEMAVERSAO_ID',prop:'ActiveEventKey'},{av:'Ddo_sistemaversao_id_Filteredtext_get',ctrl:'DDO_SISTEMAVERSAO_ID',prop:'FilteredText_get'},{av:'Ddo_sistemaversao_id_Selectedvalue_get',ctrl:'DDO_SISTEMAVERSAO_ID',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_sistemaversao_id_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_ID',prop:'SortedStatus'},{av:'AV23TFSistemaVersao_Id',fld:'vTFSISTEMAVERSAO_ID',pic:'',nv:''},{av:'AV24TFSistemaVersao_Id_Sel',fld:'vTFSISTEMAVERSAO_ID_SEL',pic:'',nv:''},{av:'Ddo_sistemaversao_data_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_DATA',prop:'SortedStatus'},{av:'Ddo_sistemaversao_descricao_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_DESCRICAO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SISTEMAVERSAO_DESCRICAO.ONOPTIONCLICKED","{handler:'E14OC2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17TFSistemaVersao_Data',fld:'vTFSISTEMAVERSAO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV18TFSistemaVersao_Data_To',fld:'vTFSISTEMAVERSAO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV23TFSistemaVersao_Id',fld:'vTFSISTEMAVERSAO_ID',pic:'',nv:''},{av:'AV24TFSistemaVersao_Id_Sel',fld:'vTFSISTEMAVERSAO_ID_SEL',pic:'',nv:''},{av:'AV27TFSistemaVersao_Descricao',fld:'vTFSISTEMAVERSAO_DESCRICAO',pic:'',nv:''},{av:'AV28TFSistemaVersao_Descricao_Sel',fld:'vTFSISTEMAVERSAO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV21ddo_SistemaVersao_DataTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV25ddo_SistemaVersao_IdTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_IDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV29ddo_SistemaVersao_DescricaoTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV34SistemaVersao_SistemaCod',fld:'vSISTEMAVERSAO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A1859SistemaVersao_Codigo',fld:'SISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Ddo_sistemaversao_descricao_Activeeventkey',ctrl:'DDO_SISTEMAVERSAO_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_sistemaversao_descricao_Filteredtext_get',ctrl:'DDO_SISTEMAVERSAO_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_sistemaversao_descricao_Selectedvalue_get',ctrl:'DDO_SISTEMAVERSAO_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_sistemaversao_descricao_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_DESCRICAO',prop:'SortedStatus'},{av:'AV27TFSistemaVersao_Descricao',fld:'vTFSISTEMAVERSAO_DESCRICAO',pic:'',nv:''},{av:'AV28TFSistemaVersao_Descricao_Sel',fld:'vTFSISTEMAVERSAO_DESCRICAO_SEL',pic:'',nv:''},{av:'Ddo_sistemaversao_data_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_DATA',prop:'SortedStatus'},{av:'Ddo_sistemaversao_id_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_ID',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E18OC2',iparms:[{av:'A1859SistemaVersao_Codigo',fld:'SISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'edtSistemaVersao_Id_Link',ctrl:'SISTEMAVERSAO_ID',prop:'Link'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E15OC2',iparms:[{av:'AV34SistemaVersao_SistemaCod',fld:'vSISTEMAVERSAO_SISTEMACOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV34SistemaVersao_SistemaCod',fld:'vSISTEMAVERSAO_SISTEMACOD',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_sistemaversao_data_Activeeventkey = "";
         Ddo_sistemaversao_data_Filteredtext_get = "";
         Ddo_sistemaversao_data_Filteredtextto_get = "";
         Ddo_sistemaversao_id_Activeeventkey = "";
         Ddo_sistemaversao_id_Filteredtext_get = "";
         Ddo_sistemaversao_id_Selectedvalue_get = "";
         Ddo_sistemaversao_descricao_Activeeventkey = "";
         Ddo_sistemaversao_descricao_Filteredtext_get = "";
         Ddo_sistemaversao_descricao_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV17TFSistemaVersao_Data = (DateTime)(DateTime.MinValue);
         AV18TFSistemaVersao_Data_To = (DateTime)(DateTime.MinValue);
         AV23TFSistemaVersao_Id = "";
         AV24TFSistemaVersao_Id_Sel = "";
         AV27TFSistemaVersao_Descricao = "";
         AV28TFSistemaVersao_Descricao_Sel = "";
         AV21ddo_SistemaVersao_DataTitleControlIdToReplace = "";
         AV25ddo_SistemaVersao_IdTitleControlIdToReplace = "";
         AV29ddo_SistemaVersao_DescricaoTitleControlIdToReplace = "";
         AV37Pgmname = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV30DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV16SistemaVersao_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV22SistemaVersao_IdTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV26SistemaVersao_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_sistemaversao_data_Filteredtext_set = "";
         Ddo_sistemaversao_data_Filteredtextto_set = "";
         Ddo_sistemaversao_data_Sortedstatus = "";
         Ddo_sistemaversao_id_Filteredtext_set = "";
         Ddo_sistemaversao_id_Selectedvalue_set = "";
         Ddo_sistemaversao_id_Sortedstatus = "";
         Ddo_sistemaversao_descricao_Filteredtext_set = "";
         Ddo_sistemaversao_descricao_Selectedvalue_set = "";
         Ddo_sistemaversao_descricao_Sortedstatus = "";
         GX_FocusControl = "";
         TempTags = "";
         AV19DDO_SistemaVersao_DataAuxDate = DateTime.MinValue;
         AV20DDO_SistemaVersao_DataAuxDateTo = DateTime.MinValue;
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         A1865SistemaVersao_Data = (DateTime)(DateTime.MinValue);
         A1860SistemaVersao_Id = "";
         A1861SistemaVersao_Descricao = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV23TFSistemaVersao_Id = "";
         lV27TFSistemaVersao_Descricao = "";
         H00OC2_A1859SistemaVersao_Codigo = new int[1] ;
         H00OC2_A1861SistemaVersao_Descricao = new String[] {""} ;
         H00OC2_A1860SistemaVersao_Id = new String[] {""} ;
         H00OC2_A1865SistemaVersao_Data = new DateTime[] {DateTime.MinValue} ;
         H00OC3_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV15Session = context.GetSession();
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         imgInsert_Jsonclick = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV34SistemaVersao_SistemaCod = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.sistemaversaowc__default(),
            new Object[][] {
                new Object[] {
               H00OC2_A1859SistemaVersao_Codigo, H00OC2_A1861SistemaVersao_Descricao, H00OC2_A1860SistemaVersao_Id, H00OC2_A1865SistemaVersao_Data
               }
               , new Object[] {
               H00OC3_AGRID_nRecordCount
               }
            }
         );
         AV37Pgmname = "SistemaVersaoWC";
         /* GeneXus formulas. */
         AV37Pgmname = "SistemaVersaoWC";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_8 ;
      private short nGXsfl_8_idx=1 ;
      private short AV13OrderedBy ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_8_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtSistemaVersao_Data_Titleformat ;
      private short edtSistemaVersao_Id_Titleformat ;
      private short edtSistemaVersao_Descricao_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV34SistemaVersao_SistemaCod ;
      private int wcpOAV34SistemaVersao_SistemaCod ;
      private int subGrid_Rows ;
      private int A1859SistemaVersao_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_sistemaversao_id_Datalistupdateminimumcharacters ;
      private int Ddo_sistemaversao_descricao_Datalistupdateminimumcharacters ;
      private int edtavOrderedby_Visible ;
      private int edtavOrdereddsc_Visible ;
      private int edtavTfsistemaversao_data_Visible ;
      private int edtavTfsistemaversao_data_to_Visible ;
      private int edtavTfsistemaversao_id_Visible ;
      private int edtavTfsistemaversao_id_sel_Visible ;
      private int edtavTfsistemaversao_descricao_Visible ;
      private int edtavTfsistemaversao_descricao_sel_Visible ;
      private int edtavDdo_sistemaversao_datatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_sistemaversao_idtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_sistemaversao_descricaotitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int imgInsert_Visible ;
      private int AV31PageToGo ;
      private int AV38GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV32GridCurrentPage ;
      private long AV33GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_sistemaversao_data_Activeeventkey ;
      private String Ddo_sistemaversao_data_Filteredtext_get ;
      private String Ddo_sistemaversao_data_Filteredtextto_get ;
      private String Ddo_sistemaversao_id_Activeeventkey ;
      private String Ddo_sistemaversao_id_Filteredtext_get ;
      private String Ddo_sistemaversao_id_Selectedvalue_get ;
      private String Ddo_sistemaversao_descricao_Activeeventkey ;
      private String Ddo_sistemaversao_descricao_Filteredtext_get ;
      private String Ddo_sistemaversao_descricao_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_8_idx="0001" ;
      private String AV23TFSistemaVersao_Id ;
      private String AV24TFSistemaVersao_Id_Sel ;
      private String AV37Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_sistemaversao_data_Caption ;
      private String Ddo_sistemaversao_data_Tooltip ;
      private String Ddo_sistemaversao_data_Cls ;
      private String Ddo_sistemaversao_data_Filteredtext_set ;
      private String Ddo_sistemaversao_data_Filteredtextto_set ;
      private String Ddo_sistemaversao_data_Dropdownoptionstype ;
      private String Ddo_sistemaversao_data_Titlecontrolidtoreplace ;
      private String Ddo_sistemaversao_data_Sortedstatus ;
      private String Ddo_sistemaversao_data_Filtertype ;
      private String Ddo_sistemaversao_data_Sortasc ;
      private String Ddo_sistemaversao_data_Sortdsc ;
      private String Ddo_sistemaversao_data_Cleanfilter ;
      private String Ddo_sistemaversao_data_Rangefilterfrom ;
      private String Ddo_sistemaversao_data_Rangefilterto ;
      private String Ddo_sistemaversao_data_Searchbuttontext ;
      private String Ddo_sistemaversao_id_Caption ;
      private String Ddo_sistemaversao_id_Tooltip ;
      private String Ddo_sistemaversao_id_Cls ;
      private String Ddo_sistemaversao_id_Filteredtext_set ;
      private String Ddo_sistemaversao_id_Selectedvalue_set ;
      private String Ddo_sistemaversao_id_Dropdownoptionstype ;
      private String Ddo_sistemaversao_id_Titlecontrolidtoreplace ;
      private String Ddo_sistemaversao_id_Sortedstatus ;
      private String Ddo_sistemaversao_id_Filtertype ;
      private String Ddo_sistemaversao_id_Datalisttype ;
      private String Ddo_sistemaversao_id_Datalistproc ;
      private String Ddo_sistemaversao_id_Sortasc ;
      private String Ddo_sistemaversao_id_Sortdsc ;
      private String Ddo_sistemaversao_id_Loadingdata ;
      private String Ddo_sistemaversao_id_Cleanfilter ;
      private String Ddo_sistemaversao_id_Noresultsfound ;
      private String Ddo_sistemaversao_id_Searchbuttontext ;
      private String Ddo_sistemaversao_descricao_Caption ;
      private String Ddo_sistemaversao_descricao_Tooltip ;
      private String Ddo_sistemaversao_descricao_Cls ;
      private String Ddo_sistemaversao_descricao_Filteredtext_set ;
      private String Ddo_sistemaversao_descricao_Selectedvalue_set ;
      private String Ddo_sistemaversao_descricao_Dropdownoptionstype ;
      private String Ddo_sistemaversao_descricao_Titlecontrolidtoreplace ;
      private String Ddo_sistemaversao_descricao_Sortedstatus ;
      private String Ddo_sistemaversao_descricao_Filtertype ;
      private String Ddo_sistemaversao_descricao_Datalisttype ;
      private String Ddo_sistemaversao_descricao_Datalistproc ;
      private String Ddo_sistemaversao_descricao_Sortasc ;
      private String Ddo_sistemaversao_descricao_Sortdsc ;
      private String Ddo_sistemaversao_descricao_Loadingdata ;
      private String Ddo_sistemaversao_descricao_Cleanfilter ;
      private String Ddo_sistemaversao_descricao_Noresultsfound ;
      private String Ddo_sistemaversao_descricao_Searchbuttontext ;
      private String GX_FocusControl ;
      private String TempTags ;
      private String edtavOrderedby_Internalname ;
      private String edtavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavOrdereddsc_Jsonclick ;
      private String edtavTfsistemaversao_data_Internalname ;
      private String edtavTfsistemaversao_data_Jsonclick ;
      private String edtavTfsistemaversao_data_to_Internalname ;
      private String edtavTfsistemaversao_data_to_Jsonclick ;
      private String divDdo_sistemaversao_dataauxdates_Internalname ;
      private String edtavDdo_sistemaversao_dataauxdate_Internalname ;
      private String edtavDdo_sistemaversao_dataauxdate_Jsonclick ;
      private String edtavDdo_sistemaversao_dataauxdateto_Internalname ;
      private String edtavDdo_sistemaversao_dataauxdateto_Jsonclick ;
      private String edtavTfsistemaversao_id_Internalname ;
      private String edtavTfsistemaversao_id_Jsonclick ;
      private String edtavTfsistemaversao_id_sel_Internalname ;
      private String edtavTfsistemaversao_id_sel_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavTfsistemaversao_descricao_Internalname ;
      private String edtavTfsistemaversao_descricao_sel_Internalname ;
      private String edtavDdo_sistemaversao_datatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_sistemaversao_idtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_sistemaversao_descricaotitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtSistemaVersao_Data_Internalname ;
      private String A1860SistemaVersao_Id ;
      private String edtSistemaVersao_Id_Internalname ;
      private String edtSistemaVersao_Descricao_Internalname ;
      private String scmdbuf ;
      private String lV23TFSistemaVersao_Id ;
      private String subGrid_Internalname ;
      private String Ddo_sistemaversao_data_Internalname ;
      private String Ddo_sistemaversao_id_Internalname ;
      private String Ddo_sistemaversao_descricao_Internalname ;
      private String edtSistemaVersao_Data_Title ;
      private String edtSistemaVersao_Id_Title ;
      private String edtSistemaVersao_Descricao_Title ;
      private String imgInsert_Internalname ;
      private String edtSistemaVersao_Id_Link ;
      private String sStyleString ;
      private String tblUnnamedtable1_Internalname ;
      private String imgInsert_Jsonclick ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String sCtrlAV34SistemaVersao_SistemaCod ;
      private String sGXsfl_8_fel_idx="0001" ;
      private String ROClassString ;
      private String edtSistemaVersao_Data_Jsonclick ;
      private String edtSistemaVersao_Id_Jsonclick ;
      private String edtSistemaVersao_Descricao_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV17TFSistemaVersao_Data ;
      private DateTime AV18TFSistemaVersao_Data_To ;
      private DateTime A1865SistemaVersao_Data ;
      private DateTime AV19DDO_SistemaVersao_DataAuxDate ;
      private DateTime AV20DDO_SistemaVersao_DataAuxDateTo ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_sistemaversao_data_Includesortasc ;
      private bool Ddo_sistemaversao_data_Includesortdsc ;
      private bool Ddo_sistemaversao_data_Includefilter ;
      private bool Ddo_sistemaversao_data_Filterisrange ;
      private bool Ddo_sistemaversao_data_Includedatalist ;
      private bool Ddo_sistemaversao_id_Includesortasc ;
      private bool Ddo_sistemaversao_id_Includesortdsc ;
      private bool Ddo_sistemaversao_id_Includefilter ;
      private bool Ddo_sistemaversao_id_Filterisrange ;
      private bool Ddo_sistemaversao_id_Includedatalist ;
      private bool Ddo_sistemaversao_descricao_Includesortasc ;
      private bool Ddo_sistemaversao_descricao_Includesortdsc ;
      private bool Ddo_sistemaversao_descricao_Includefilter ;
      private bool Ddo_sistemaversao_descricao_Filterisrange ;
      private bool Ddo_sistemaversao_descricao_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private String A1861SistemaVersao_Descricao ;
      private String AV27TFSistemaVersao_Descricao ;
      private String AV28TFSistemaVersao_Descricao_Sel ;
      private String AV21ddo_SistemaVersao_DataTitleControlIdToReplace ;
      private String AV25ddo_SistemaVersao_IdTitleControlIdToReplace ;
      private String AV29ddo_SistemaVersao_DescricaoTitleControlIdToReplace ;
      private String lV27TFSistemaVersao_Descricao ;
      private IGxSession AV15Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00OC2_A1859SistemaVersao_Codigo ;
      private String[] H00OC2_A1861SistemaVersao_Descricao ;
      private String[] H00OC2_A1860SistemaVersao_Id ;
      private DateTime[] H00OC2_A1865SistemaVersao_Data ;
      private long[] H00OC3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV16SistemaVersao_DataTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV22SistemaVersao_IdTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV26SistemaVersao_DescricaoTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV30DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class sistemaversaowc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00OC2( IGxContext context ,
                                             DateTime AV17TFSistemaVersao_Data ,
                                             DateTime AV18TFSistemaVersao_Data_To ,
                                             String AV24TFSistemaVersao_Id_Sel ,
                                             String AV23TFSistemaVersao_Id ,
                                             String AV28TFSistemaVersao_Descricao_Sel ,
                                             String AV27TFSistemaVersao_Descricao ,
                                             DateTime A1865SistemaVersao_Data ,
                                             String A1860SistemaVersao_Id ,
                                             String A1861SistemaVersao_Descricao ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [11] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [SistemaVersao_Codigo], [SistemaVersao_Descricao], [SistemaVersao_Id], [SistemaVersao_Data]";
         sFromString = " FROM [SistemaVersao] WITH (NOLOCK)";
         sOrderString = "";
         if ( ! (DateTime.MinValue==AV17TFSistemaVersao_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Data] >= @AV17TFSistemaVersao_Data)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Data] >= @AV17TFSistemaVersao_Data)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ! (DateTime.MinValue==AV18TFSistemaVersao_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Data] <= @AV18TFSistemaVersao_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Data] <= @AV18TFSistemaVersao_Data_To)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV24TFSistemaVersao_Id_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFSistemaVersao_Id)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Id] like @lV23TFSistemaVersao_Id)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Id] like @lV23TFSistemaVersao_Id)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24TFSistemaVersao_Id_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Id] = @AV24TFSistemaVersao_Id_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Id] = @AV24TFSistemaVersao_Id_Sel)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV28TFSistemaVersao_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27TFSistemaVersao_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Descricao] like @lV27TFSistemaVersao_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Descricao] like @lV27TFSistemaVersao_Descricao)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28TFSistemaVersao_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Descricao] = @AV28TFSistemaVersao_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Descricao] = @AV28TFSistemaVersao_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [SistemaVersao_Data]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [SistemaVersao_Data] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [SistemaVersao_Id]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [SistemaVersao_Id] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [SistemaVersao_Descricao]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [SistemaVersao_Descricao] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [SistemaVersao_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00OC3( IGxContext context ,
                                             DateTime AV17TFSistemaVersao_Data ,
                                             DateTime AV18TFSistemaVersao_Data_To ,
                                             String AV24TFSistemaVersao_Id_Sel ,
                                             String AV23TFSistemaVersao_Id ,
                                             String AV28TFSistemaVersao_Descricao_Sel ,
                                             String AV27TFSistemaVersao_Descricao ,
                                             DateTime A1865SistemaVersao_Data ,
                                             String A1860SistemaVersao_Id ,
                                             String A1861SistemaVersao_Descricao ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [6] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [SistemaVersao] WITH (NOLOCK)";
         if ( ! (DateTime.MinValue==AV17TFSistemaVersao_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Data] >= @AV17TFSistemaVersao_Data)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Data] >= @AV17TFSistemaVersao_Data)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ! (DateTime.MinValue==AV18TFSistemaVersao_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Data] <= @AV18TFSistemaVersao_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Data] <= @AV18TFSistemaVersao_Data_To)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV24TFSistemaVersao_Id_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFSistemaVersao_Id)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Id] like @lV23TFSistemaVersao_Id)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Id] like @lV23TFSistemaVersao_Id)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24TFSistemaVersao_Id_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Id] = @AV24TFSistemaVersao_Id_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Id] = @AV24TFSistemaVersao_Id_Sel)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV28TFSistemaVersao_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27TFSistemaVersao_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Descricao] like @lV27TFSistemaVersao_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Descricao] like @lV27TFSistemaVersao_Descricao)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28TFSistemaVersao_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SistemaVersao_Descricao] = @AV28TFSistemaVersao_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([SistemaVersao_Descricao] = @AV28TFSistemaVersao_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00OC2(context, (DateTime)dynConstraints[0] , (DateTime)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (DateTime)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (bool)dynConstraints[10] );
               case 1 :
                     return conditional_H00OC3(context, (DateTime)dynConstraints[0] , (DateTime)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (DateTime)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (bool)dynConstraints[10] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00OC2 ;
          prmH00OC2 = new Object[] {
          new Object[] {"@AV17TFSistemaVersao_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV18TFSistemaVersao_Data_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV23TFSistemaVersao_Id",SqlDbType.Char,20,0} ,
          new Object[] {"@AV24TFSistemaVersao_Id_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV27TFSistemaVersao_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV28TFSistemaVersao_Descricao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00OC3 ;
          prmH00OC3 = new Object[] {
          new Object[] {"@AV17TFSistemaVersao_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV18TFSistemaVersao_Data_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV23TFSistemaVersao_Id",SqlDbType.Char,20,0} ,
          new Object[] {"@AV24TFSistemaVersao_Id_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV27TFSistemaVersao_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV28TFSistemaVersao_Descricao_Sel",SqlDbType.VarChar,200,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00OC2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00OC2,11,0,true,false )
             ,new CursorDef("H00OC3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00OC3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDateTime(4) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[11]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[12]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[18]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[6]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[7]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                return;
       }
    }

 }

}
