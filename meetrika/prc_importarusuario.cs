/*
               File: PRC_ImportarUsuario
        Description: Importar Usuario
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:7:44.4
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_importarusuario : GXProcedure
   {
      public prc_importarusuario( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_importarusuario( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratante_Codigo ,
                           int aP1_Contratada_Codigo ,
                           int aP2_Usuario_Codigo )
      {
         this.AV9Contratante_Codigo = aP0_Contratante_Codigo;
         this.AV8Contratada_Codigo = aP1_Contratada_Codigo;
         this.AV10Usuario_Codigo = aP2_Usuario_Codigo;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_Contratante_Codigo ,
                                 int aP1_Contratada_Codigo ,
                                 int aP2_Usuario_Codigo )
      {
         prc_importarusuario objprc_importarusuario;
         objprc_importarusuario = new prc_importarusuario();
         objprc_importarusuario.AV9Contratante_Codigo = aP0_Contratante_Codigo;
         objprc_importarusuario.AV8Contratada_Codigo = aP1_Contratada_Codigo;
         objprc_importarusuario.AV10Usuario_Codigo = aP2_Usuario_Codigo;
         objprc_importarusuario.context.SetSubmitInitialConfig(context);
         objprc_importarusuario.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_importarusuario);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_importarusuario)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( ! (0==AV10Usuario_Codigo) )
         {
            if ( (0==AV8Contratada_Codigo) )
            {
               /*
                  INSERT RECORD ON TABLE ContratanteUsuario

               */
               A63ContratanteUsuario_ContratanteCod = AV9Contratante_Codigo;
               A60ContratanteUsuario_UsuarioCod = AV10Usuario_Codigo;
               /* Using cursor P002V2 */
               pr_default.execute(0, new Object[] {A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod});
               pr_default.close(0);
               dsDefault.SmartCacheProvider.SetUpdated("ContratanteUsuario") ;
               if ( (pr_default.getStatus(0) == 1) )
               {
                  context.Gx_err = 1;
                  Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
               }
               else
               {
                  context.Gx_err = 0;
                  Gx_emsg = "";
               }
               /* End Insert */
            }
            else
            {
               /* Using cursor P002V3 */
               pr_default.execute(1, new Object[] {AV8Contratada_Codigo});
               while ( (pr_default.getStatus(1) != 101) )
               {
                  A40Contratada_PessoaCod = P002V3_A40Contratada_PessoaCod[0];
                  A39Contratada_Codigo = P002V3_A39Contratada_Codigo[0];
                  A42Contratada_PessoaCNPJ = P002V3_A42Contratada_PessoaCNPJ[0];
                  n42Contratada_PessoaCNPJ = P002V3_n42Contratada_PessoaCNPJ[0];
                  A42Contratada_PessoaCNPJ = P002V3_A42Contratada_PessoaCNPJ[0];
                  n42Contratada_PessoaCNPJ = P002V3_n42Contratada_PessoaCNPJ[0];
                  AV13CNPJ = A42Contratada_PessoaCNPJ;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(1);
               /* Using cursor P002V4 */
               pr_default.execute(2, new Object[] {AV10Usuario_Codigo, AV13CNPJ});
               while ( (pr_default.getStatus(2) != 101) )
               {
                  A66ContratadaUsuario_ContratadaCod = P002V4_A66ContratadaUsuario_ContratadaCod[0];
                  A67ContratadaUsuario_ContratadaPessoaCod = P002V4_A67ContratadaUsuario_ContratadaPessoaCod[0];
                  n67ContratadaUsuario_ContratadaPessoaCod = P002V4_n67ContratadaUsuario_ContratadaPessoaCod[0];
                  A577ContratadaUsuario_CstUntPrdExt = P002V4_A577ContratadaUsuario_CstUntPrdExt[0];
                  n577ContratadaUsuario_CstUntPrdExt = P002V4_n577ContratadaUsuario_CstUntPrdExt[0];
                  A576ContratadaUsuario_CstUntPrdNrm = P002V4_A576ContratadaUsuario_CstUntPrdNrm[0];
                  n576ContratadaUsuario_CstUntPrdNrm = P002V4_n576ContratadaUsuario_CstUntPrdNrm[0];
                  A348ContratadaUsuario_ContratadaPessoaCNPJ = P002V4_A348ContratadaUsuario_ContratadaPessoaCNPJ[0];
                  n348ContratadaUsuario_ContratadaPessoaCNPJ = P002V4_n348ContratadaUsuario_ContratadaPessoaCNPJ[0];
                  A69ContratadaUsuario_UsuarioCod = P002V4_A69ContratadaUsuario_UsuarioCod[0];
                  A67ContratadaUsuario_ContratadaPessoaCod = P002V4_A67ContratadaUsuario_ContratadaPessoaCod[0];
                  n67ContratadaUsuario_ContratadaPessoaCod = P002V4_n67ContratadaUsuario_ContratadaPessoaCod[0];
                  A348ContratadaUsuario_ContratadaPessoaCNPJ = P002V4_A348ContratadaUsuario_ContratadaPessoaCNPJ[0];
                  n348ContratadaUsuario_ContratadaPessoaCNPJ = P002V4_n348ContratadaUsuario_ContratadaPessoaCNPJ[0];
                  AV11ContratadaUsuario_CstUntPrdNrm = A576ContratadaUsuario_CstUntPrdNrm;
                  AV12ContratadaUsuario_CstUntPrdExt = A577ContratadaUsuario_CstUntPrdExt;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  pr_default.readNext(2);
               }
               pr_default.close(2);
               /*
                  INSERT RECORD ON TABLE ContratadaUsuario

               */
               A66ContratadaUsuario_ContratadaCod = AV8Contratada_Codigo;
               A69ContratadaUsuario_UsuarioCod = AV10Usuario_Codigo;
               A576ContratadaUsuario_CstUntPrdNrm = AV11ContratadaUsuario_CstUntPrdNrm;
               n576ContratadaUsuario_CstUntPrdNrm = false;
               A577ContratadaUsuario_CstUntPrdExt = AV12ContratadaUsuario_CstUntPrdExt;
               n577ContratadaUsuario_CstUntPrdExt = false;
               /* Using cursor P002V5 */
               pr_default.execute(3, new Object[] {A66ContratadaUsuario_ContratadaCod, A69ContratadaUsuario_UsuarioCod, n576ContratadaUsuario_CstUntPrdNrm, A576ContratadaUsuario_CstUntPrdNrm, n577ContratadaUsuario_CstUntPrdExt, A577ContratadaUsuario_CstUntPrdExt});
               pr_default.close(3);
               dsDefault.SmartCacheProvider.SetUpdated("ContratadaUsuario") ;
               if ( (pr_default.getStatus(3) == 1) )
               {
                  context.Gx_err = 1;
                  Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
               }
               else
               {
                  context.Gx_err = 0;
                  Gx_emsg = "";
               }
               /* End Insert */
            }
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_ImportarUsuario");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gx_emsg = "";
         scmdbuf = "";
         P002V3_A40Contratada_PessoaCod = new int[1] ;
         P002V3_A39Contratada_Codigo = new int[1] ;
         P002V3_A42Contratada_PessoaCNPJ = new String[] {""} ;
         P002V3_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         A42Contratada_PessoaCNPJ = "";
         AV13CNPJ = "";
         P002V4_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         P002V4_A67ContratadaUsuario_ContratadaPessoaCod = new int[1] ;
         P002V4_n67ContratadaUsuario_ContratadaPessoaCod = new bool[] {false} ;
         P002V4_A577ContratadaUsuario_CstUntPrdExt = new decimal[1] ;
         P002V4_n577ContratadaUsuario_CstUntPrdExt = new bool[] {false} ;
         P002V4_A576ContratadaUsuario_CstUntPrdNrm = new decimal[1] ;
         P002V4_n576ContratadaUsuario_CstUntPrdNrm = new bool[] {false} ;
         P002V4_A348ContratadaUsuario_ContratadaPessoaCNPJ = new String[] {""} ;
         P002V4_n348ContratadaUsuario_ContratadaPessoaCNPJ = new bool[] {false} ;
         P002V4_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         A348ContratadaUsuario_ContratadaPessoaCNPJ = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_importarusuario__default(),
            new Object[][] {
                new Object[] {
               }
               , new Object[] {
               P002V3_A40Contratada_PessoaCod, P002V3_A39Contratada_Codigo, P002V3_A42Contratada_PessoaCNPJ, P002V3_n42Contratada_PessoaCNPJ
               }
               , new Object[] {
               P002V4_A66ContratadaUsuario_ContratadaCod, P002V4_A67ContratadaUsuario_ContratadaPessoaCod, P002V4_n67ContratadaUsuario_ContratadaPessoaCod, P002V4_A577ContratadaUsuario_CstUntPrdExt, P002V4_n577ContratadaUsuario_CstUntPrdExt, P002V4_A576ContratadaUsuario_CstUntPrdNrm, P002V4_n576ContratadaUsuario_CstUntPrdNrm, P002V4_A348ContratadaUsuario_ContratadaPessoaCNPJ, P002V4_n348ContratadaUsuario_ContratadaPessoaCNPJ, P002V4_A69ContratadaUsuario_UsuarioCod
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV9Contratante_Codigo ;
      private int AV8Contratada_Codigo ;
      private int AV10Usuario_Codigo ;
      private int GX_INS15 ;
      private int A63ContratanteUsuario_ContratanteCod ;
      private int A60ContratanteUsuario_UsuarioCod ;
      private int A40Contratada_PessoaCod ;
      private int A39Contratada_Codigo ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A67ContratadaUsuario_ContratadaPessoaCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int GX_INS16 ;
      private decimal A577ContratadaUsuario_CstUntPrdExt ;
      private decimal A576ContratadaUsuario_CstUntPrdNrm ;
      private decimal AV11ContratadaUsuario_CstUntPrdNrm ;
      private decimal AV12ContratadaUsuario_CstUntPrdExt ;
      private String Gx_emsg ;
      private String scmdbuf ;
      private String AV13CNPJ ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool n67ContratadaUsuario_ContratadaPessoaCod ;
      private bool n577ContratadaUsuario_CstUntPrdExt ;
      private bool n576ContratadaUsuario_CstUntPrdNrm ;
      private bool n348ContratadaUsuario_ContratadaPessoaCNPJ ;
      private String A42Contratada_PessoaCNPJ ;
      private String A348ContratadaUsuario_ContratadaPessoaCNPJ ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P002V3_A40Contratada_PessoaCod ;
      private int[] P002V3_A39Contratada_Codigo ;
      private String[] P002V3_A42Contratada_PessoaCNPJ ;
      private bool[] P002V3_n42Contratada_PessoaCNPJ ;
      private int[] P002V4_A66ContratadaUsuario_ContratadaCod ;
      private int[] P002V4_A67ContratadaUsuario_ContratadaPessoaCod ;
      private bool[] P002V4_n67ContratadaUsuario_ContratadaPessoaCod ;
      private decimal[] P002V4_A577ContratadaUsuario_CstUntPrdExt ;
      private bool[] P002V4_n577ContratadaUsuario_CstUntPrdExt ;
      private decimal[] P002V4_A576ContratadaUsuario_CstUntPrdNrm ;
      private bool[] P002V4_n576ContratadaUsuario_CstUntPrdNrm ;
      private String[] P002V4_A348ContratadaUsuario_ContratadaPessoaCNPJ ;
      private bool[] P002V4_n348ContratadaUsuario_ContratadaPessoaCNPJ ;
      private int[] P002V4_A69ContratadaUsuario_UsuarioCod ;
   }

   public class prc_importarusuario__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new UpdateCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new UpdateCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP002V2 ;
          prmP002V2 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP002V3 ;
          prmP002V3 = new Object[] {
          new Object[] {"@AV8Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP002V4 ;
          prmP002V4 = new Object[] {
          new Object[] {"@AV10Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13CNPJ",SqlDbType.Char,14,0}
          } ;
          Object[] prmP002V5 ;
          prmP002V5 = new Object[] {
          new Object[] {"@ContratadaUsuario_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratadaUsuario_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratadaUsuario_CstUntPrdNrm",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContratadaUsuario_CstUntPrdExt",SqlDbType.Decimal,18,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("P002V2", "INSERT INTO [ContratanteUsuario]([ContratanteUsuario_ContratanteCod], [ContratanteUsuario_UsuarioCod], [ContratanteUsuario_EhFiscal]) VALUES(@ContratanteUsuario_ContratanteCod, @ContratanteUsuario_UsuarioCod, convert(bit, 0))", GxErrorMask.GX_NOMASK,prmP002V2)
             ,new CursorDef("P002V3", "SELECT TOP 1 T1.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[Contratada_Codigo], T2.[Pessoa_Docto] AS Contratada_PessoaCNPJ FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) WHERE T1.[Contratada_Codigo] = @AV8Contratada_Codigo ORDER BY T1.[Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP002V3,1,0,false,true )
             ,new CursorDef("P002V4", "SELECT TOP 1 T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T2.[Contratada_PessoaCod] AS ContratadaUsuario_ContratadaPessoaCod, T1.[ContratadaUsuario_CstUntPrdExt], T1.[ContratadaUsuario_CstUntPrdNrm], T3.[Pessoa_Docto] AS ContratadaUsuario_ContratadaPe, T1.[ContratadaUsuario_UsuarioCod] FROM (([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratada_PessoaCod]) WHERE (T1.[ContratadaUsuario_UsuarioCod] = @AV10Usuario_Codigo) AND (T1.[ContratadaUsuario_CstUntPrdNrm] + T1.[ContratadaUsuario_CstUntPrdExt] > 0) AND (T3.[Pessoa_Docto] = @AV13CNPJ) ORDER BY T1.[ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP002V4,1,0,false,true )
             ,new CursorDef("P002V5", "INSERT INTO [ContratadaUsuario]([ContratadaUsuario_ContratadaCod], [ContratadaUsuario_UsuarioCod], [ContratadaUsuario_CstUntPrdNrm], [ContratadaUsuario_CstUntPrdExt], [ContratadaUsuario_TmpEstExc], [ContratadaUsuario_TmpEstCrr], [ContratadaUsuario_TmpEstAnl]) VALUES(@ContratadaUsuario_ContratadaCod, @ContratadaUsuario_UsuarioCod, @ContratadaUsuario_CstUntPrdNrm, @ContratadaUsuario_CstUntPrdExt, convert(int, 0), convert(int, 0), convert(int, 0))", GxErrorMask.GX_NOMASK,prmP002V5)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((decimal[]) buf[5])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(3, (decimal)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[5]);
                }
                return;
       }
    }

 }

}
