/*
               File: GetPromptFuncaoDadosFilterData
        Description: Get Prompt Funcao Dados Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:10:58.12
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptfuncaodadosfilterdata : GXProcedure
   {
      public getpromptfuncaodadosfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptfuncaodadosfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV26DDOName = aP0_DDOName;
         this.AV24SearchTxt = aP1_SearchTxt;
         this.AV25SearchTxtTo = aP2_SearchTxtTo;
         this.AV30OptionsJson = "" ;
         this.AV33OptionsDescJson = "" ;
         this.AV35OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV30OptionsJson;
         aP4_OptionsDescJson=this.AV33OptionsDescJson;
         aP5_OptionIndexesJson=this.AV35OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV26DDOName = aP0_DDOName;
         this.AV24SearchTxt = aP1_SearchTxt;
         this.AV25SearchTxtTo = aP2_SearchTxtTo;
         this.AV30OptionsJson = "" ;
         this.AV33OptionsDescJson = "" ;
         this.AV35OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV30OptionsJson;
         aP4_OptionsDescJson=this.AV33OptionsDescJson;
         aP5_OptionIndexesJson=this.AV35OptionIndexesJson;
         return AV35OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptfuncaodadosfilterdata objgetpromptfuncaodadosfilterdata;
         objgetpromptfuncaodadosfilterdata = new getpromptfuncaodadosfilterdata();
         objgetpromptfuncaodadosfilterdata.AV26DDOName = aP0_DDOName;
         objgetpromptfuncaodadosfilterdata.AV24SearchTxt = aP1_SearchTxt;
         objgetpromptfuncaodadosfilterdata.AV25SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptfuncaodadosfilterdata.AV30OptionsJson = "" ;
         objgetpromptfuncaodadosfilterdata.AV33OptionsDescJson = "" ;
         objgetpromptfuncaodadosfilterdata.AV35OptionIndexesJson = "" ;
         objgetpromptfuncaodadosfilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptfuncaodadosfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptfuncaodadosfilterdata);
         aP3_OptionsJson=this.AV30OptionsJson;
         aP4_OptionsDescJson=this.AV33OptionsDescJson;
         aP5_OptionIndexesJson=this.AV35OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptfuncaodadosfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV29Options = (IGxCollection)(new GxSimpleCollection());
         AV32OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV34OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV26DDOName), "DDO_FUNCAODADOS_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADFUNCAODADOS_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV30OptionsJson = AV29Options.ToJSonString(false);
         AV33OptionsDescJson = AV32OptionsDesc.ToJSonString(false);
         AV35OptionIndexesJson = AV34OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV37Session.Get("PromptFuncaoDadosGridState"), "") == 0 )
         {
            AV39GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptFuncaoDadosGridState"), "");
         }
         else
         {
            AV39GridState.FromXml(AV37Session.Get("PromptFuncaoDadosGridState"), "");
         }
         AV46GXV1 = 1;
         while ( AV46GXV1 <= AV39GridState.gxTpr_Filtervalues.Count )
         {
            AV40GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV39GridState.gxTpr_Filtervalues.Item(AV46GXV1));
            if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "FUNCAODADOS_NOME") == 0 )
            {
               AV42FuncaoDados_Nome = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_NOME") == 0 )
            {
               AV10TFFuncaoDados_Nome = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_NOME_SEL") == 0 )
            {
               AV11TFFuncaoDados_Nome_Sel = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_TIPO_SEL") == 0 )
            {
               AV12TFFuncaoDados_Tipo_SelsJson = AV40GridStateFilterValue.gxTpr_Value;
               AV13TFFuncaoDados_Tipo_Sels.FromJSonString(AV12TFFuncaoDados_Tipo_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_COMPLEXIDADE_SEL") == 0 )
            {
               AV14TFFuncaoDados_Complexidade_SelsJson = AV40GridStateFilterValue.gxTpr_Value;
               AV15TFFuncaoDados_Complexidade_Sels.FromJSonString(AV14TFFuncaoDados_Complexidade_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_DER") == 0 )
            {
               AV16TFFuncaoDados_DER = (short)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Value, "."));
               AV17TFFuncaoDados_DER_To = (short)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_RLR") == 0 )
            {
               AV18TFFuncaoDados_RLR = (short)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Value, "."));
               AV19TFFuncaoDados_RLR_To = (short)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_PF") == 0 )
            {
               AV20TFFuncaoDados_PF = NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Value, ".");
               AV21TFFuncaoDados_PF_To = NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_ATIVO_SEL") == 0 )
            {
               AV22TFFuncaoDados_Ativo_SelsJson = AV40GridStateFilterValue.gxTpr_Value;
               AV23TFFuncaoDados_Ativo_Sels.FromJSonString(AV22TFFuncaoDados_Ativo_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "PARM_&FUNCAODADOS_SISTEMACOD") == 0 )
            {
               AV43FuncaoDados_SistemaCod = (int)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Value, "."));
            }
            AV46GXV1 = (int)(AV46GXV1+1);
         }
      }

      protected void S121( )
      {
         /* 'LOADFUNCAODADOS_NOMEOPTIONS' Routine */
         AV10TFFuncaoDados_Nome = AV24SearchTxt;
         AV11TFFuncaoDados_Nome_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A376FuncaoDados_Complexidade ,
                                              AV15TFFuncaoDados_Complexidade_Sels ,
                                              A373FuncaoDados_Tipo ,
                                              AV13TFFuncaoDados_Tipo_Sels ,
                                              A394FuncaoDados_Ativo ,
                                              AV23TFFuncaoDados_Ativo_Sels ,
                                              AV42FuncaoDados_Nome ,
                                              AV11TFFuncaoDados_Nome_Sel ,
                                              AV10TFFuncaoDados_Nome ,
                                              AV13TFFuncaoDados_Tipo_Sels.Count ,
                                              AV23TFFuncaoDados_Ativo_Sels.Count ,
                                              AV43FuncaoDados_SistemaCod ,
                                              A369FuncaoDados_Nome ,
                                              A370FuncaoDados_SistemaCod ,
                                              AV15TFFuncaoDados_Complexidade_Sels.Count ,
                                              AV16TFFuncaoDados_DER ,
                                              A374FuncaoDados_DER ,
                                              AV17TFFuncaoDados_DER_To ,
                                              AV18TFFuncaoDados_RLR ,
                                              A375FuncaoDados_RLR ,
                                              AV19TFFuncaoDados_RLR_To ,
                                              AV20TFFuncaoDados_PF ,
                                              A377FuncaoDados_PF ,
                                              AV21TFFuncaoDados_PF_To },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL
                                              }
         });
         lV42FuncaoDados_Nome = StringUtil.Concat( StringUtil.RTrim( AV42FuncaoDados_Nome), "%", "");
         lV10TFFuncaoDados_Nome = StringUtil.Concat( StringUtil.RTrim( AV10TFFuncaoDados_Nome), "%", "");
         /* Using cursor P00LK2 */
         pr_default.execute(0, new Object[] {AV15TFFuncaoDados_Complexidade_Sels.Count, lV42FuncaoDados_Nome, lV10TFFuncaoDados_Nome, AV11TFFuncaoDados_Nome_Sel, AV43FuncaoDados_SistemaCod});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKLK2 = false;
            A369FuncaoDados_Nome = P00LK2_A369FuncaoDados_Nome[0];
            A370FuncaoDados_SistemaCod = P00LK2_A370FuncaoDados_SistemaCod[0];
            A394FuncaoDados_Ativo = P00LK2_A394FuncaoDados_Ativo[0];
            A373FuncaoDados_Tipo = P00LK2_A373FuncaoDados_Tipo[0];
            A755FuncaoDados_Contar = P00LK2_A755FuncaoDados_Contar[0];
            n755FuncaoDados_Contar = P00LK2_n755FuncaoDados_Contar[0];
            A368FuncaoDados_Codigo = P00LK2_A368FuncaoDados_Codigo[0];
            GXt_char1 = A376FuncaoDados_Complexidade;
            new prc_fdcomplexidade(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_char1) ;
            A376FuncaoDados_Complexidade = GXt_char1;
            if ( ( AV15TFFuncaoDados_Complexidade_Sels.Count <= 0 ) || ( (AV15TFFuncaoDados_Complexidade_Sels.IndexOf(StringUtil.RTrim( A376FuncaoDados_Complexidade))>0) ) )
            {
               GXt_int2 = A374FuncaoDados_DER;
               new prc_funcaodados_der(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int2) ;
               A374FuncaoDados_DER = GXt_int2;
               if ( (0==AV16TFFuncaoDados_DER) || ( ( A374FuncaoDados_DER >= AV16TFFuncaoDados_DER ) ) )
               {
                  if ( (0==AV17TFFuncaoDados_DER_To) || ( ( A374FuncaoDados_DER <= AV17TFFuncaoDados_DER_To ) ) )
                  {
                     GXt_int2 = A375FuncaoDados_RLR;
                     new prc_funcaodados_rlr(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int2) ;
                     A375FuncaoDados_RLR = GXt_int2;
                     if ( (0==AV18TFFuncaoDados_RLR) || ( ( A375FuncaoDados_RLR >= AV18TFFuncaoDados_RLR ) ) )
                     {
                        if ( (0==AV19TFFuncaoDados_RLR_To) || ( ( A375FuncaoDados_RLR <= AV19TFFuncaoDados_RLR_To ) ) )
                        {
                           if ( A755FuncaoDados_Contar )
                           {
                              GXt_int2 = (short)(A377FuncaoDados_PF);
                              new prc_fdpf(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_int2) ;
                              A377FuncaoDados_PF = (decimal)(GXt_int2);
                           }
                           else
                           {
                              A377FuncaoDados_PF = 0;
                           }
                           if ( (Convert.ToDecimal(0)==AV20TFFuncaoDados_PF) || ( ( A377FuncaoDados_PF >= AV20TFFuncaoDados_PF ) ) )
                           {
                              if ( (Convert.ToDecimal(0)==AV21TFFuncaoDados_PF_To) || ( ( A377FuncaoDados_PF <= AV21TFFuncaoDados_PF_To ) ) )
                              {
                                 AV36count = 0;
                                 while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00LK2_A369FuncaoDados_Nome[0], A369FuncaoDados_Nome) == 0 ) )
                                 {
                                    BRKLK2 = false;
                                    A368FuncaoDados_Codigo = P00LK2_A368FuncaoDados_Codigo[0];
                                    AV36count = (long)(AV36count+1);
                                    BRKLK2 = true;
                                    pr_default.readNext(0);
                                 }
                                 if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A369FuncaoDados_Nome)) )
                                 {
                                    AV28Option = A369FuncaoDados_Nome;
                                    AV29Options.Add(AV28Option, 0);
                                    AV34OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV36count), "Z,ZZZ,ZZZ,ZZ9")), 0);
                                 }
                                 if ( AV29Options.Count == 50 )
                                 {
                                    /* Exit For each command. Update data (if necessary), close cursors & exit. */
                                    if (true) break;
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
            if ( ! BRKLK2 )
            {
               BRKLK2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV29Options = new GxSimpleCollection();
         AV32OptionsDesc = new GxSimpleCollection();
         AV34OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV37Session = context.GetSession();
         AV39GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV40GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV42FuncaoDados_Nome = "";
         AV10TFFuncaoDados_Nome = "";
         AV11TFFuncaoDados_Nome_Sel = "";
         AV12TFFuncaoDados_Tipo_SelsJson = "";
         AV13TFFuncaoDados_Tipo_Sels = new GxSimpleCollection();
         AV14TFFuncaoDados_Complexidade_SelsJson = "";
         AV15TFFuncaoDados_Complexidade_Sels = new GxSimpleCollection();
         AV22TFFuncaoDados_Ativo_SelsJson = "";
         AV23TFFuncaoDados_Ativo_Sels = new GxSimpleCollection();
         scmdbuf = "";
         lV42FuncaoDados_Nome = "";
         lV10TFFuncaoDados_Nome = "";
         A376FuncaoDados_Complexidade = "";
         A373FuncaoDados_Tipo = "";
         A394FuncaoDados_Ativo = "";
         A369FuncaoDados_Nome = "";
         P00LK2_A369FuncaoDados_Nome = new String[] {""} ;
         P00LK2_A370FuncaoDados_SistemaCod = new int[1] ;
         P00LK2_A394FuncaoDados_Ativo = new String[] {""} ;
         P00LK2_A373FuncaoDados_Tipo = new String[] {""} ;
         P00LK2_A755FuncaoDados_Contar = new bool[] {false} ;
         P00LK2_n755FuncaoDados_Contar = new bool[] {false} ;
         P00LK2_A368FuncaoDados_Codigo = new int[1] ;
         GXt_char1 = "";
         AV28Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptfuncaodadosfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00LK2_A369FuncaoDados_Nome, P00LK2_A370FuncaoDados_SistemaCod, P00LK2_A394FuncaoDados_Ativo, P00LK2_A373FuncaoDados_Tipo, P00LK2_A755FuncaoDados_Contar, P00LK2_n755FuncaoDados_Contar, P00LK2_A368FuncaoDados_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV16TFFuncaoDados_DER ;
      private short AV17TFFuncaoDados_DER_To ;
      private short AV18TFFuncaoDados_RLR ;
      private short AV19TFFuncaoDados_RLR_To ;
      private short A374FuncaoDados_DER ;
      private short A375FuncaoDados_RLR ;
      private short GXt_int2 ;
      private int AV46GXV1 ;
      private int AV43FuncaoDados_SistemaCod ;
      private int AV13TFFuncaoDados_Tipo_Sels_Count ;
      private int AV23TFFuncaoDados_Ativo_Sels_Count ;
      private int AV15TFFuncaoDados_Complexidade_Sels_Count ;
      private int A370FuncaoDados_SistemaCod ;
      private int A368FuncaoDados_Codigo ;
      private long AV36count ;
      private decimal AV20TFFuncaoDados_PF ;
      private decimal AV21TFFuncaoDados_PF_To ;
      private decimal A377FuncaoDados_PF ;
      private String scmdbuf ;
      private String A376FuncaoDados_Complexidade ;
      private String A373FuncaoDados_Tipo ;
      private String A394FuncaoDados_Ativo ;
      private String GXt_char1 ;
      private bool returnInSub ;
      private bool BRKLK2 ;
      private bool A755FuncaoDados_Contar ;
      private bool n755FuncaoDados_Contar ;
      private String AV35OptionIndexesJson ;
      private String AV30OptionsJson ;
      private String AV33OptionsDescJson ;
      private String AV12TFFuncaoDados_Tipo_SelsJson ;
      private String AV14TFFuncaoDados_Complexidade_SelsJson ;
      private String AV22TFFuncaoDados_Ativo_SelsJson ;
      private String AV26DDOName ;
      private String AV24SearchTxt ;
      private String AV25SearchTxtTo ;
      private String AV42FuncaoDados_Nome ;
      private String AV10TFFuncaoDados_Nome ;
      private String AV11TFFuncaoDados_Nome_Sel ;
      private String lV42FuncaoDados_Nome ;
      private String lV10TFFuncaoDados_Nome ;
      private String A369FuncaoDados_Nome ;
      private String AV28Option ;
      private IGxSession AV37Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00LK2_A369FuncaoDados_Nome ;
      private int[] P00LK2_A370FuncaoDados_SistemaCod ;
      private String[] P00LK2_A394FuncaoDados_Ativo ;
      private String[] P00LK2_A373FuncaoDados_Tipo ;
      private bool[] P00LK2_A755FuncaoDados_Contar ;
      private bool[] P00LK2_n755FuncaoDados_Contar ;
      private int[] P00LK2_A368FuncaoDados_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV13TFFuncaoDados_Tipo_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV15TFFuncaoDados_Complexidade_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV23TFFuncaoDados_Ativo_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV29Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV32OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV34OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV39GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV40GridStateFilterValue ;
   }

   public class getpromptfuncaodadosfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00LK2( IGxContext context ,
                                             String A376FuncaoDados_Complexidade ,
                                             IGxCollection AV15TFFuncaoDados_Complexidade_Sels ,
                                             String A373FuncaoDados_Tipo ,
                                             IGxCollection AV13TFFuncaoDados_Tipo_Sels ,
                                             String A394FuncaoDados_Ativo ,
                                             IGxCollection AV23TFFuncaoDados_Ativo_Sels ,
                                             String AV42FuncaoDados_Nome ,
                                             String AV11TFFuncaoDados_Nome_Sel ,
                                             String AV10TFFuncaoDados_Nome ,
                                             int AV13TFFuncaoDados_Tipo_Sels_Count ,
                                             int AV23TFFuncaoDados_Ativo_Sels_Count ,
                                             int AV43FuncaoDados_SistemaCod ,
                                             String A369FuncaoDados_Nome ,
                                             int A370FuncaoDados_SistemaCod ,
                                             int AV15TFFuncaoDados_Complexidade_Sels_Count ,
                                             short AV16TFFuncaoDados_DER ,
                                             short A374FuncaoDados_DER ,
                                             short AV17TFFuncaoDados_DER_To ,
                                             short AV18TFFuncaoDados_RLR ,
                                             short A375FuncaoDados_RLR ,
                                             short AV19TFFuncaoDados_RLR_To ,
                                             decimal AV20TFFuncaoDados_PF ,
                                             decimal A377FuncaoDados_PF ,
                                             decimal AV21TFFuncaoDados_PF_To )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [5] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT [FuncaoDados_Nome], [FuncaoDados_SistemaCod], [FuncaoDados_Ativo], [FuncaoDados_Tipo], [FuncaoDados_Contar], [FuncaoDados_Codigo] FROM [FuncaoDados] WITH (NOLOCK)";
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42FuncaoDados_Nome)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoDados_Nome] like '%' + @lV42FuncaoDados_Nome + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoDados_Nome] like '%' + @lV42FuncaoDados_Nome + '%')";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFFuncaoDados_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFFuncaoDados_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoDados_Nome] like @lV10TFFuncaoDados_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoDados_Nome] like @lV10TFFuncaoDados_Nome)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFFuncaoDados_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoDados_Nome] = @AV11TFFuncaoDados_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoDados_Nome] = @AV11TFFuncaoDados_Nome_Sel)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV13TFFuncaoDados_Tipo_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV13TFFuncaoDados_Tipo_Sels, "[FuncaoDados_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV13TFFuncaoDados_Tipo_Sels, "[FuncaoDados_Tipo] IN (", ")") + ")";
            }
         }
         if ( AV23TFFuncaoDados_Ativo_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV23TFFuncaoDados_Ativo_Sels, "[FuncaoDados_Ativo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV23TFFuncaoDados_Ativo_Sels, "[FuncaoDados_Ativo] IN (", ")") + ")";
            }
         }
         if ( ! (0==AV43FuncaoDados_SistemaCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoDados_SistemaCod] = @AV43FuncaoDados_SistemaCod)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoDados_SistemaCod] = @AV43FuncaoDados_SistemaCod)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [FuncaoDados_Nome]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00LK2(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (String)dynConstraints[4] , (IGxCollection)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (short)dynConstraints[15] , (short)dynConstraints[16] , (short)dynConstraints[17] , (short)dynConstraints[18] , (short)dynConstraints[19] , (short)dynConstraints[20] , (decimal)dynConstraints[21] , (decimal)dynConstraints[22] , (decimal)dynConstraints[23] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00LK2 ;
          prmP00LK2 = new Object[] {
          new Object[] {"@AV15TFFuCount",SqlDbType.Int,9,0} ,
          new Object[] {"@lV42FuncaoDados_Nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV10TFFuncaoDados_Nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV11TFFuncaoDados_Nome_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV43FuncaoDados_SistemaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00LK2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00LK2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 3) ;
                ((bool[]) buf[4])[0] = rslt.getBool(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[6]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptfuncaodadosfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptfuncaodadosfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptfuncaodadosfilterdata") )
          {
             return  ;
          }
          getpromptfuncaodadosfilterdata worker = new getpromptfuncaodadosfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
