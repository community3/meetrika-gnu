/*
               File: PRC_EcluirDoLote
        Description: Ecluir Do Lote
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:9:38.45
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_ecluirdolote : GXProcedure
   {
      public prc_ecluirdolote( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_ecluirdolote( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultado_Codigo ,
                           int aP1_Lote_Codigo ,
                           String aP2_Observacao )
      {
         this.AV11ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV10Lote_Codigo = aP1_Lote_Codigo;
         this.AV8Observacao = aP2_Observacao;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.AV11ContagemResultado_Codigo;
      }

      public void executeSubmit( ref int aP0_ContagemResultado_Codigo ,
                                 int aP1_Lote_Codigo ,
                                 String aP2_Observacao )
      {
         prc_ecluirdolote objprc_ecluirdolote;
         objprc_ecluirdolote = new prc_ecluirdolote();
         objprc_ecluirdolote.AV11ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_ecluirdolote.AV10Lote_Codigo = aP1_Lote_Codigo;
         objprc_ecluirdolote.AV8Observacao = aP2_Observacao;
         objprc_ecluirdolote.context.SetSubmitInitialConfig(context);
         objprc_ecluirdolote.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_ecluirdolote);
         aP0_ContagemResultado_Codigo=this.AV11ContagemResultado_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_ecluirdolote)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV11ContagemResultado_Codigo ,
                                              AV10Lote_Codigo ,
                                              A456ContagemResultado_Codigo ,
                                              A597ContagemResultado_LoteAceiteCod },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor P00B92 */
         pr_default.execute(0, new Object[] {AV11ContagemResultado_Codigo, AV10Lote_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A597ContagemResultado_LoteAceiteCod = P00B92_A597ContagemResultado_LoteAceiteCod[0];
            n597ContagemResultado_LoteAceiteCod = P00B92_n597ContagemResultado_LoteAceiteCod[0];
            A456ContagemResultado_Codigo = P00B92_A456ContagemResultado_Codigo[0];
            A484ContagemResultado_StatusDmn = P00B92_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00B92_n484ContagemResultado_StatusDmn[0];
            A472ContagemResultado_DataEntrega = P00B92_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = P00B92_n472ContagemResultado_DataEntrega[0];
            A912ContagemResultado_HoraEntrega = P00B92_A912ContagemResultado_HoraEntrega[0];
            n912ContagemResultado_HoraEntrega = P00B92_n912ContagemResultado_HoraEntrega[0];
            A890ContagemResultado_Responsavel = P00B92_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = P00B92_n890ContagemResultado_Responsavel[0];
            A525ContagemResultado_AceiteUserCod = P00B92_A525ContagemResultado_AceiteUserCod[0];
            n525ContagemResultado_AceiteUserCod = P00B92_n525ContagemResultado_AceiteUserCod[0];
            A525ContagemResultado_AceiteUserCod = P00B92_A525ContagemResultado_AceiteUserCod[0];
            n525ContagemResultado_AceiteUserCod = P00B92_n525ContagemResultado_AceiteUserCod[0];
            A484ContagemResultado_StatusDmn = "H";
            n484ContagemResultado_StatusDmn = false;
            A597ContagemResultado_LoteAceiteCod = 0;
            n597ContagemResultado_LoteAceiteCod = false;
            n597ContagemResultado_LoteAceiteCod = true;
            AV9Prazo = DateTimeUtil.ResetTime( A472ContagemResultado_DataEntrega ) ;
            AV9Prazo = DateTimeUtil.TAdd( AV9Prazo, 3600*(DateTimeUtil.Hour( A912ContagemResultado_HoraEntrega)));
            AV9Prazo = DateTimeUtil.TAdd( AV9Prazo, 60*(DateTimeUtil.Minute( A912ContagemResultado_HoraEntrega)));
            new prc_inslogresponsavel(context ).execute( ref  A456ContagemResultado_Codigo,  0,  "BK",  "D",  A525ContagemResultado_AceiteUserCod,  0,  "O",  "H",  AV8Observacao,  AV9Prazo,  false) ;
            /* Using cursor P00B93 */
            pr_default.execute(1, new Object[] {n597ContagemResultado_LoteAceiteCod, A597ContagemResultado_LoteAceiteCod, n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, A456ContagemResultado_Codigo});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         if ( AV10Lote_Codigo > 0 )
         {
            /* Using cursor P00B95 */
            pr_default.execute(2, new Object[] {AV10Lote_Codigo});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A595Lote_AreaTrabalhoCod = P00B95_A595Lote_AreaTrabalhoCod[0];
               A596Lote_Codigo = P00B95_A596Lote_Codigo[0];
               A1231Lote_ContratadaCod = P00B95_A1231Lote_ContratadaCod[0];
               n1231Lote_ContratadaCod = P00B95_n1231Lote_ContratadaCod[0];
               A1231Lote_ContratadaCod = P00B95_A1231Lote_ContratadaCod[0];
               n1231Lote_ContratadaCod = P00B95_n1231Lote_ContratadaCod[0];
               GXt_boolean1 = AV13OSAutomatica;
               new prc_osautomatica(context ).execute( ref  A595Lote_AreaTrabalhoCod, out  GXt_boolean1) ;
               AV13OSAutomatica = GXt_boolean1;
               pr_default.dynParam(3, new Object[]{ new Object[]{
                                                    AV13OSAutomatica ,
                                                    A597ContagemResultado_LoteAceiteCod ,
                                                    A596Lote_Codigo },
                                                    new int[] {
                                                    TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                                    }
               });
               /* Using cursor P00B96 */
               pr_default.execute(3, new Object[] {A596Lote_Codigo});
               while ( (pr_default.getStatus(3) != 101) )
               {
                  A597ContagemResultado_LoteAceiteCod = P00B96_A597ContagemResultado_LoteAceiteCod[0];
                  n597ContagemResultado_LoteAceiteCod = P00B96_n597ContagemResultado_LoteAceiteCod[0];
                  A493ContagemResultado_DemandaFM = P00B96_A493ContagemResultado_DemandaFM[0];
                  n493ContagemResultado_DemandaFM = P00B96_n493ContagemResultado_DemandaFM[0];
                  A1452ContagemResultado_SS = P00B96_A1452ContagemResultado_SS[0];
                  n1452ContagemResultado_SS = P00B96_n1452ContagemResultado_SS[0];
                  A456ContagemResultado_Codigo = P00B96_A456ContagemResultado_Codigo[0];
                  if ( AV13OSAutomatica )
                  {
                     AV12Contratada_OS = (int)(A1452ContagemResultado_SS-1);
                  }
                  else
                  {
                     AV12Contratada_OS = (int)(NumberUtil.Val( A493ContagemResultado_DemandaFM, ".")-1);
                  }
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  pr_default.readNext(3);
               }
               pr_default.close(3);
               /* Using cursor P00B97 */
               pr_default.execute(4, new Object[] {n1231Lote_ContratadaCod, A1231Lote_ContratadaCod});
               while ( (pr_default.getStatus(4) != 101) )
               {
                  A39Contratada_Codigo = P00B97_A39Contratada_Codigo[0];
                  A524Contratada_OS = P00B97_A524Contratada_OS[0];
                  n524Contratada_OS = P00B97_n524Contratada_OS[0];
                  A530Contratada_Lote = P00B97_A530Contratada_Lote[0];
                  n530Contratada_Lote = P00B97_n530Contratada_Lote[0];
                  A524Contratada_OS = AV12Contratada_OS;
                  n524Contratada_OS = false;
                  A530Contratada_Lote = (short)(A530Contratada_Lote-1);
                  n530Contratada_Lote = false;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  /* Using cursor P00B98 */
                  pr_default.execute(5, new Object[] {n524Contratada_OS, A524Contratada_OS, n530Contratada_Lote, A530Contratada_Lote, A39Contratada_Codigo});
                  pr_default.close(5);
                  dsDefault.SmartCacheProvider.SetUpdated("Contratada") ;
                  if (true) break;
                  /* Using cursor P00B99 */
                  pr_default.execute(6, new Object[] {n524Contratada_OS, A524Contratada_OS, n530Contratada_Lote, A530Contratada_Lote, A39Contratada_Codigo});
                  pr_default.close(6);
                  dsDefault.SmartCacheProvider.SetUpdated("Contratada") ;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(4);
               /* Using cursor P00B910 */
               pr_default.execute(7, new Object[] {A596Lote_Codigo});
               pr_default.close(7);
               dsDefault.SmartCacheProvider.SetUpdated("Lote") ;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(2);
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_EcluirDoLote");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00B92_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P00B92_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P00B92_A456ContagemResultado_Codigo = new int[1] ;
         P00B92_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00B92_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00B92_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P00B92_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P00B92_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         P00B92_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         P00B92_A890ContagemResultado_Responsavel = new int[1] ;
         P00B92_n890ContagemResultado_Responsavel = new bool[] {false} ;
         P00B92_A525ContagemResultado_AceiteUserCod = new int[1] ;
         P00B92_n525ContagemResultado_AceiteUserCod = new bool[] {false} ;
         A484ContagemResultado_StatusDmn = "";
         A472ContagemResultado_DataEntrega = DateTime.MinValue;
         A912ContagemResultado_HoraEntrega = (DateTime)(DateTime.MinValue);
         AV9Prazo = (DateTime)(DateTime.MinValue);
         P00B95_A595Lote_AreaTrabalhoCod = new int[1] ;
         P00B95_A596Lote_Codigo = new int[1] ;
         P00B95_A1231Lote_ContratadaCod = new int[1] ;
         P00B95_n1231Lote_ContratadaCod = new bool[] {false} ;
         P00B96_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P00B96_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P00B96_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00B96_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00B96_A1452ContagemResultado_SS = new int[1] ;
         P00B96_n1452ContagemResultado_SS = new bool[] {false} ;
         P00B96_A456ContagemResultado_Codigo = new int[1] ;
         A493ContagemResultado_DemandaFM = "";
         P00B97_A39Contratada_Codigo = new int[1] ;
         P00B97_A524Contratada_OS = new int[1] ;
         P00B97_n524Contratada_OS = new bool[] {false} ;
         P00B97_A530Contratada_Lote = new short[1] ;
         P00B97_n530Contratada_Lote = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_ecluirdolote__default(),
            new Object[][] {
                new Object[] {
               P00B92_A597ContagemResultado_LoteAceiteCod, P00B92_n597ContagemResultado_LoteAceiteCod, P00B92_A456ContagemResultado_Codigo, P00B92_A484ContagemResultado_StatusDmn, P00B92_n484ContagemResultado_StatusDmn, P00B92_A472ContagemResultado_DataEntrega, P00B92_n472ContagemResultado_DataEntrega, P00B92_A912ContagemResultado_HoraEntrega, P00B92_n912ContagemResultado_HoraEntrega, P00B92_A890ContagemResultado_Responsavel,
               P00B92_n890ContagemResultado_Responsavel, P00B92_A525ContagemResultado_AceiteUserCod, P00B92_n525ContagemResultado_AceiteUserCod
               }
               , new Object[] {
               }
               , new Object[] {
               P00B95_A595Lote_AreaTrabalhoCod, P00B95_A596Lote_Codigo, P00B95_A1231Lote_ContratadaCod, P00B95_n1231Lote_ContratadaCod
               }
               , new Object[] {
               P00B96_A597ContagemResultado_LoteAceiteCod, P00B96_n597ContagemResultado_LoteAceiteCod, P00B96_A493ContagemResultado_DemandaFM, P00B96_n493ContagemResultado_DemandaFM, P00B96_A1452ContagemResultado_SS, P00B96_n1452ContagemResultado_SS, P00B96_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P00B97_A39Contratada_Codigo, P00B97_A524Contratada_OS, P00B97_n524Contratada_OS, P00B97_A530Contratada_Lote, P00B97_n530Contratada_Lote
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A530Contratada_Lote ;
      private int AV11ContagemResultado_Codigo ;
      private int AV10Lote_Codigo ;
      private int A456ContagemResultado_Codigo ;
      private int A597ContagemResultado_LoteAceiteCod ;
      private int A890ContagemResultado_Responsavel ;
      private int A525ContagemResultado_AceiteUserCod ;
      private int A595Lote_AreaTrabalhoCod ;
      private int A596Lote_Codigo ;
      private int A1231Lote_ContratadaCod ;
      private int A1452ContagemResultado_SS ;
      private int AV12Contratada_OS ;
      private int A39Contratada_Codigo ;
      private int A524Contratada_OS ;
      private String scmdbuf ;
      private String A484ContagemResultado_StatusDmn ;
      private DateTime A912ContagemResultado_HoraEntrega ;
      private DateTime AV9Prazo ;
      private DateTime A472ContagemResultado_DataEntrega ;
      private bool n597ContagemResultado_LoteAceiteCod ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n472ContagemResultado_DataEntrega ;
      private bool n912ContagemResultado_HoraEntrega ;
      private bool n890ContagemResultado_Responsavel ;
      private bool n525ContagemResultado_AceiteUserCod ;
      private bool n1231Lote_ContratadaCod ;
      private bool AV13OSAutomatica ;
      private bool GXt_boolean1 ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n1452ContagemResultado_SS ;
      private bool n524Contratada_OS ;
      private bool n530Contratada_Lote ;
      private String AV8Observacao ;
      private String A493ContagemResultado_DemandaFM ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultado_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00B92_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P00B92_n597ContagemResultado_LoteAceiteCod ;
      private int[] P00B92_A456ContagemResultado_Codigo ;
      private String[] P00B92_A484ContagemResultado_StatusDmn ;
      private bool[] P00B92_n484ContagemResultado_StatusDmn ;
      private DateTime[] P00B92_A472ContagemResultado_DataEntrega ;
      private bool[] P00B92_n472ContagemResultado_DataEntrega ;
      private DateTime[] P00B92_A912ContagemResultado_HoraEntrega ;
      private bool[] P00B92_n912ContagemResultado_HoraEntrega ;
      private int[] P00B92_A890ContagemResultado_Responsavel ;
      private bool[] P00B92_n890ContagemResultado_Responsavel ;
      private int[] P00B92_A525ContagemResultado_AceiteUserCod ;
      private bool[] P00B92_n525ContagemResultado_AceiteUserCod ;
      private int[] P00B95_A595Lote_AreaTrabalhoCod ;
      private int[] P00B95_A596Lote_Codigo ;
      private int[] P00B95_A1231Lote_ContratadaCod ;
      private bool[] P00B95_n1231Lote_ContratadaCod ;
      private int[] P00B96_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P00B96_n597ContagemResultado_LoteAceiteCod ;
      private String[] P00B96_A493ContagemResultado_DemandaFM ;
      private bool[] P00B96_n493ContagemResultado_DemandaFM ;
      private int[] P00B96_A1452ContagemResultado_SS ;
      private bool[] P00B96_n1452ContagemResultado_SS ;
      private int[] P00B96_A456ContagemResultado_Codigo ;
      private int[] P00B97_A39Contratada_Codigo ;
      private int[] P00B97_A524Contratada_OS ;
      private bool[] P00B97_n524Contratada_OS ;
      private short[] P00B97_A530Contratada_Lote ;
      private bool[] P00B97_n530Contratada_Lote ;
   }

   public class prc_ecluirdolote__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00B92( IGxContext context ,
                                             int AV11ContagemResultado_Codigo ,
                                             int AV10Lote_Codigo ,
                                             int A456ContagemResultado_Codigo ,
                                             int A597ContagemResultado_LoteAceiteCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [2] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_LoteAceiteCod] AS ContagemResultado_LoteAceiteCod, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_DataEntrega], T1.[ContagemResultado_HoraEntrega], T1.[ContagemResultado_Responsavel], T2.[Lote_UserCod] AS ContagemResultado_AceiteUserCod FROM ([ContagemResultado] T1 WITH (UPDLOCK) LEFT JOIN [Lote] T2 WITH (NOLOCK) ON T2.[Lote_Codigo] = T1.[ContagemResultado_LoteAceiteCod])";
         if ( ! (0==AV11ContagemResultado_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_Codigo] = @AV11ContagemResultado_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_Codigo] = @AV11ContagemResultado_Codigo)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ! (0==AV10Lote_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContagemResultado_LoteAceiteCod] = @AV10Lote_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContagemResultado_LoteAceiteCod] = @AV10Lote_Codigo)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Codigo]";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_P00B96( IGxContext context ,
                                             bool AV13OSAutomatica ,
                                             int A597ContagemResultado_LoteAceiteCod ,
                                             int A596Lote_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [1] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT TOP 1 [ContagemResultado_LoteAceiteCod] AS ContagemResultado_LoteAceiteCod, [ContagemResultado_DemandaFM], [ContagemResultado_SS], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([ContagemResultado_LoteAceiteCod] = @Lote_Codigo)";
         if ( AV13OSAutomatica )
         {
            scmdbuf = scmdbuf + " ORDER BY [ContagemResultado_LoteAceiteCod], [ContagemResultado_SS]";
         }
         else if ( AV13OSAutomatica )
         {
            scmdbuf = scmdbuf + " ORDER BY [ContagemResultado_LoteAceiteCod], [ContagemResultado_DemandaFM]";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00B92(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] );
               case 3 :
                     return conditional_P00B96(context, (bool)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new UpdateCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new UpdateCursor(def[7])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00B93 ;
          prmP00B93 = new Object[] {
          new Object[] {"@ContagemResultado_LoteAceiteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00B95 ;
          prmP00B95 = new Object[] {
          new Object[] {"@AV10Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00B97 ;
          prmP00B97 = new Object[] {
          new Object[] {"@Lote_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00B98 ;
          prmP00B98 = new Object[] {
          new Object[] {"@Contratada_OS",SqlDbType.Int,8,0} ,
          new Object[] {"@Contratada_Lote",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00B99 ;
          prmP00B99 = new Object[] {
          new Object[] {"@Contratada_OS",SqlDbType.Int,8,0} ,
          new Object[] {"@Contratada_Lote",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00B910 ;
          prmP00B910 = new Object[] {
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00B92 ;
          prmP00B92 = new Object[] {
          new Object[] {"@AV11ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV10Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00B96 ;
          prmP00B96 = new Object[] {
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00B92", "scmdbuf",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00B92,1,0,true,false )
             ,new CursorDef("P00B93", "UPDATE [ContagemResultado] SET [ContagemResultado_LoteAceiteCod]=@ContagemResultado_LoteAceiteCod, [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00B93)
             ,new CursorDef("P00B95", "SELECT TOP 1 T1.[Lote_AreaTrabalhoCod], T1.[Lote_Codigo], COALESCE( T2.[Lote_ContratadaCod], 0) AS Lote_ContratadaCod FROM ([Lote] T1 WITH (UPDLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_ContratadaCod]) AS Lote_ContratadaCod, [ContagemResultado_LoteAceiteCod] AS ContagemResultado_LoteAceiteCod FROM [ContagemResultado] WITH (NOLOCK) GROUP BY [ContagemResultado_LoteAceiteCod] ) T2 ON T2.[ContagemResultado_LoteAceiteCod] = T1.[Lote_Codigo]) WHERE T1.[Lote_Codigo] = @AV10Lote_Codigo ORDER BY T1.[Lote_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00B95,1,0,true,true )
             ,new CursorDef("P00B96", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00B96,1,0,false,true )
             ,new CursorDef("P00B97", "SELECT TOP 1 [Contratada_Codigo], [Contratada_OS], [Contratada_Lote] FROM [Contratada] WITH (UPDLOCK) WHERE [Contratada_Codigo] = @Lote_ContratadaCod ORDER BY [Contratada_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00B97,1,0,true,true )
             ,new CursorDef("P00B98", "UPDATE [Contratada] SET [Contratada_OS]=@Contratada_OS, [Contratada_Lote]=@Contratada_Lote  WHERE [Contratada_Codigo] = @Contratada_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00B98)
             ,new CursorDef("P00B99", "UPDATE [Contratada] SET [Contratada_OS]=@Contratada_OS, [Contratada_Lote]=@Contratada_Lote  WHERE [Contratada_Codigo] = @Contratada_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00B99)
             ,new CursorDef("P00B910", "DELETE FROM [Lote]  WHERE [Lote_Codigo] = @Lote_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00B910)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[7])[0] = rslt.getGXDateTime(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[2]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[1]);
                }
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(2, (short)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(2, (short)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
