/*
               File: LogResponsavel
        Description: Log de a��o dos Respons�veis
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:9:53.60
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class logresponsavel : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"LOGRESPONSAVEL_USUARIOCOD") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLALOGRESPONSAVEL_USUARIOCOD2T198( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel5"+"_"+"LOGRESPONSAVEL_USUARIOEHCONTRATANTE") == 0 )
         {
            A892LogResponsavel_DemandaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n892LogResponsavel_DemandaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A892LogResponsavel_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A892LogResponsavel_DemandaCod), 6, 0)));
            A891LogResponsavel_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n891LogResponsavel_UsuarioCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A891LogResponsavel_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A891LogResponsavel_UsuarioCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX5ASALOGRESPONSAVEL_USUARIOEHCONTRATANTE2T198( A892LogResponsavel_DemandaCod, A891LogResponsavel_UsuarioCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel6"+"_"+"LOGRESPONSAVEL_OWNEREHCONTRATANTE") == 0 )
         {
            A892LogResponsavel_DemandaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n892LogResponsavel_DemandaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A892LogResponsavel_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A892LogResponsavel_DemandaCod), 6, 0)));
            A896LogResponsavel_Owner = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A896LogResponsavel_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A896LogResponsavel_Owner), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX6ASALOGRESPONSAVEL_OWNEREHCONTRATANTE2T198( A892LogResponsavel_DemandaCod, A896LogResponsavel_Owner) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_9") == 0 )
         {
            A891LogResponsavel_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n891LogResponsavel_UsuarioCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A891LogResponsavel_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A891LogResponsavel_UsuarioCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_9( A891LogResponsavel_UsuarioCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_11") == 0 )
         {
            A892LogResponsavel_DemandaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n892LogResponsavel_DemandaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A892LogResponsavel_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A892LogResponsavel_DemandaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_11( A892LogResponsavel_DemandaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_13") == 0 )
         {
            A490ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n490ContagemResultado_ContratadaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A490ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_13( A490ContagemResultado_ContratadaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_10") == 0 )
         {
            A896LogResponsavel_Owner = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A896LogResponsavel_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A896LogResponsavel_Owner), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_10( A896LogResponsavel_Owner) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_12") == 0 )
         {
            A1219LogResponsavel_OwnerPessoaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1219LogResponsavel_OwnerPessoaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1219LogResponsavel_OwnerPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1219LogResponsavel_OwnerPessoaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_12( A1219LogResponsavel_OwnerPessoaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         cmbLogResponsavel_Acao.Name = "LOGRESPONSAVEL_ACAO";
         cmbLogResponsavel_Acao.WebTags = "";
         cmbLogResponsavel_Acao.addItem("A", "Atribui", 0);
         cmbLogResponsavel_Acao.addItem("B", "StandBy", 0);
         cmbLogResponsavel_Acao.addItem("C", "Captura", 0);
         cmbLogResponsavel_Acao.addItem("R", "Rejeita", 0);
         cmbLogResponsavel_Acao.addItem("L", "Libera", 0);
         cmbLogResponsavel_Acao.addItem("E", "Encaminha", 0);
         cmbLogResponsavel_Acao.addItem("I", "Importa", 0);
         cmbLogResponsavel_Acao.addItem("S", "Solicita", 0);
         cmbLogResponsavel_Acao.addItem("D", "Diverg�ncia", 0);
         cmbLogResponsavel_Acao.addItem("V", "Resolvida", 0);
         cmbLogResponsavel_Acao.addItem("H", "Homologa", 0);
         cmbLogResponsavel_Acao.addItem("Q", "Liquida", 0);
         cmbLogResponsavel_Acao.addItem("P", "Fatura", 0);
         cmbLogResponsavel_Acao.addItem("O", "Aceite", 0);
         cmbLogResponsavel_Acao.addItem("N", "N�o Acata", 0);
         cmbLogResponsavel_Acao.addItem("M", "Autom�tica", 0);
         cmbLogResponsavel_Acao.addItem("F", "Cumprido", 0);
         cmbLogResponsavel_Acao.addItem("T", "Acata", 0);
         cmbLogResponsavel_Acao.addItem("X", "Cancela", 0);
         cmbLogResponsavel_Acao.addItem("NO", "Nota", 0);
         cmbLogResponsavel_Acao.addItem("U", "Altera", 0);
         cmbLogResponsavel_Acao.addItem("UN", "Rascunho", 0);
         cmbLogResponsavel_Acao.addItem("EA", "Em An�lise", 0);
         cmbLogResponsavel_Acao.addItem("RN", "Reuni�o", 0);
         cmbLogResponsavel_Acao.addItem("BK", "Desfaz", 0);
         cmbLogResponsavel_Acao.addItem("RE", "Reinicio", 0);
         cmbLogResponsavel_Acao.addItem("PR", "Prioridade", 0);
         if ( cmbLogResponsavel_Acao.ItemCount > 0 )
         {
            A894LogResponsavel_Acao = cmbLogResponsavel_Acao.getValidValue(A894LogResponsavel_Acao);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A894LogResponsavel_Acao", A894LogResponsavel_Acao);
         }
         dynLogResponsavel_UsuarioCod.Name = "LOGRESPONSAVEL_USUARIOCOD";
         dynLogResponsavel_UsuarioCod.WebTags = "";
         chkLogResponsavel_UsuarioEhContratante.Name = "LOGRESPONSAVEL_USUARIOEHCONTRATANTE";
         chkLogResponsavel_UsuarioEhContratante.WebTags = "";
         chkLogResponsavel_UsuarioEhContratante.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkLogResponsavel_UsuarioEhContratante_Internalname, "TitleCaption", chkLogResponsavel_UsuarioEhContratante.Caption);
         chkLogResponsavel_UsuarioEhContratante.CheckedValue = "false";
         cmbLogResponsavel_ObjetoTipo.Name = "LOGRESPONSAVEL_OBJETOTIPO";
         cmbLogResponsavel_ObjetoTipo.WebTags = "";
         cmbLogResponsavel_ObjetoTipo.addItem("D", "Demanda", 0);
         if ( cmbLogResponsavel_ObjetoTipo.ItemCount > 0 )
         {
            A895LogResponsavel_ObjetoTipo = cmbLogResponsavel_ObjetoTipo.getValidValue(A895LogResponsavel_ObjetoTipo);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A895LogResponsavel_ObjetoTipo", A895LogResponsavel_ObjetoTipo);
         }
         chkLogResponsavel_OwnerEhContratante.Name = "LOGRESPONSAVEL_OWNEREHCONTRATANTE";
         chkLogResponsavel_OwnerEhContratante.WebTags = "";
         chkLogResponsavel_OwnerEhContratante.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkLogResponsavel_OwnerEhContratante_Internalname, "TitleCaption", chkLogResponsavel_OwnerEhContratante.Caption);
         chkLogResponsavel_OwnerEhContratante.CheckedValue = "false";
         cmbLogResponsavel_Status.Name = "LOGRESPONSAVEL_STATUS";
         cmbLogResponsavel_Status.WebTags = "";
         cmbLogResponsavel_Status.addItem("B", "Stand by", 0);
         cmbLogResponsavel_Status.addItem("S", "Solicitada", 0);
         cmbLogResponsavel_Status.addItem("E", "Em An�lise", 0);
         cmbLogResponsavel_Status.addItem("A", "Em execu��o", 0);
         cmbLogResponsavel_Status.addItem("R", "Resolvida", 0);
         cmbLogResponsavel_Status.addItem("C", "Conferida", 0);
         cmbLogResponsavel_Status.addItem("D", "Rejeitada", 0);
         cmbLogResponsavel_Status.addItem("H", "Homologada", 0);
         cmbLogResponsavel_Status.addItem("O", "Aceite", 0);
         cmbLogResponsavel_Status.addItem("P", "A Pagar", 0);
         cmbLogResponsavel_Status.addItem("L", "Liquidada", 0);
         cmbLogResponsavel_Status.addItem("X", "Cancelada", 0);
         cmbLogResponsavel_Status.addItem("N", "N�o Faturada", 0);
         cmbLogResponsavel_Status.addItem("J", "Planejamento", 0);
         cmbLogResponsavel_Status.addItem("I", "An�lise Planejamento", 0);
         cmbLogResponsavel_Status.addItem("T", "Validacao T�cnica", 0);
         cmbLogResponsavel_Status.addItem("Q", "Validacao Qualidade", 0);
         cmbLogResponsavel_Status.addItem("G", "Em Homologa��o", 0);
         cmbLogResponsavel_Status.addItem("M", "Valida��o Mensura��o", 0);
         cmbLogResponsavel_Status.addItem("U", "Rascunho", 0);
         if ( cmbLogResponsavel_Status.ItemCount > 0 )
         {
            A1130LogResponsavel_Status = cmbLogResponsavel_Status.getValidValue(A1130LogResponsavel_Status);
            n1130LogResponsavel_Status = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1130LogResponsavel_Status", A1130LogResponsavel_Status);
         }
         cmbLogResponsavel_NovoStatus.Name = "LOGRESPONSAVEL_NOVOSTATUS";
         cmbLogResponsavel_NovoStatus.WebTags = "";
         cmbLogResponsavel_NovoStatus.addItem("B", "Stand by", 0);
         cmbLogResponsavel_NovoStatus.addItem("S", "Solicitada", 0);
         cmbLogResponsavel_NovoStatus.addItem("E", "Em An�lise", 0);
         cmbLogResponsavel_NovoStatus.addItem("A", "Em execu��o", 0);
         cmbLogResponsavel_NovoStatus.addItem("R", "Resolvida", 0);
         cmbLogResponsavel_NovoStatus.addItem("C", "Conferida", 0);
         cmbLogResponsavel_NovoStatus.addItem("D", "Rejeitada", 0);
         cmbLogResponsavel_NovoStatus.addItem("H", "Homologada", 0);
         cmbLogResponsavel_NovoStatus.addItem("O", "Aceite", 0);
         cmbLogResponsavel_NovoStatus.addItem("P", "A Pagar", 0);
         cmbLogResponsavel_NovoStatus.addItem("L", "Liquidada", 0);
         cmbLogResponsavel_NovoStatus.addItem("X", "Cancelada", 0);
         cmbLogResponsavel_NovoStatus.addItem("N", "N�o Faturada", 0);
         cmbLogResponsavel_NovoStatus.addItem("J", "Planejamento", 0);
         cmbLogResponsavel_NovoStatus.addItem("I", "An�lise Planejamento", 0);
         cmbLogResponsavel_NovoStatus.addItem("T", "Validacao T�cnica", 0);
         cmbLogResponsavel_NovoStatus.addItem("Q", "Validacao Qualidade", 0);
         cmbLogResponsavel_NovoStatus.addItem("G", "Em Homologa��o", 0);
         cmbLogResponsavel_NovoStatus.addItem("M", "Valida��o Mensura��o", 0);
         cmbLogResponsavel_NovoStatus.addItem("U", "Rascunho", 0);
         if ( cmbLogResponsavel_NovoStatus.ItemCount > 0 )
         {
            A1234LogResponsavel_NovoStatus = cmbLogResponsavel_NovoStatus.getValidValue(A1234LogResponsavel_NovoStatus);
            n1234LogResponsavel_NovoStatus = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1234LogResponsavel_NovoStatus", A1234LogResponsavel_NovoStatus);
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Log de a��o dos Respons�veis", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtLogResponsavel_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public logresponsavel( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public logresponsavel( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbLogResponsavel_Acao = new GXCombobox();
         dynLogResponsavel_UsuarioCod = new GXCombobox();
         chkLogResponsavel_UsuarioEhContratante = new GXCheckbox();
         cmbLogResponsavel_ObjetoTipo = new GXCombobox();
         chkLogResponsavel_OwnerEhContratante = new GXCheckbox();
         cmbLogResponsavel_Status = new GXCombobox();
         cmbLogResponsavel_NovoStatus = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbLogResponsavel_Acao.ItemCount > 0 )
         {
            A894LogResponsavel_Acao = cmbLogResponsavel_Acao.getValidValue(A894LogResponsavel_Acao);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A894LogResponsavel_Acao", A894LogResponsavel_Acao);
         }
         if ( dynLogResponsavel_UsuarioCod.ItemCount > 0 )
         {
            A891LogResponsavel_UsuarioCod = (int)(NumberUtil.Val( dynLogResponsavel_UsuarioCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A891LogResponsavel_UsuarioCod), 6, 0))), "."));
            n891LogResponsavel_UsuarioCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A891LogResponsavel_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A891LogResponsavel_UsuarioCod), 6, 0)));
         }
         if ( cmbLogResponsavel_ObjetoTipo.ItemCount > 0 )
         {
            A895LogResponsavel_ObjetoTipo = cmbLogResponsavel_ObjetoTipo.getValidValue(A895LogResponsavel_ObjetoTipo);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A895LogResponsavel_ObjetoTipo", A895LogResponsavel_ObjetoTipo);
         }
         if ( cmbLogResponsavel_Status.ItemCount > 0 )
         {
            A1130LogResponsavel_Status = cmbLogResponsavel_Status.getValidValue(A1130LogResponsavel_Status);
            n1130LogResponsavel_Status = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1130LogResponsavel_Status", A1130LogResponsavel_Status);
         }
         if ( cmbLogResponsavel_NovoStatus.ItemCount > 0 )
         {
            A1234LogResponsavel_NovoStatus = cmbLogResponsavel_NovoStatus.getValidValue(A1234LogResponsavel_NovoStatus);
            n1234LogResponsavel_NovoStatus = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1234LogResponsavel_NovoStatus", A1234LogResponsavel_NovoStatus);
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_2T198( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_2T198e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_2T198( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_2T198( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_2T198e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroupdata_Internalname, "Log de a��o dos Respons�veis", 1, 0, "px", 0, "px", "Group", " "+"style=\"-moz-border-radius: 3pt;;\""+" ", "HLP_LogResponsavel.htm");
            wb_table3_28_2T198( true) ;
         }
         return  ;
      }

      protected void wb_table3_28_2T198e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_2T198e( true) ;
         }
         else
         {
            wb_table1_2_2T198e( false) ;
         }
      }

      protected void wb_table3_28_2T198( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_34_2T198( true) ;
         }
         return  ;
      }

      protected void wb_table4_34_2T198e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_LogResponsavel.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_LogResponsavel.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_LogResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_2T198e( true) ;
         }
         else
         {
            wb_table3_28_2T198e( false) ;
         }
      }

      protected void wb_table4_34_2T198( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Container", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklogresponsavel_codigo_Internalname, "Responsavel_Codigo", "", "", lblTextblocklogresponsavel_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_LogResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtLogResponsavel_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1797LogResponsavel_Codigo), 10, 0, ",", "")), ((edtLogResponsavel_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1797LogResponsavel_Codigo), "ZZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A1797LogResponsavel_Codigo), "ZZZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLogResponsavel_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtLogResponsavel_Codigo_Enabled, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, 0, true, "Codigo10", "right", false, "HLP_LogResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklogresponsavel_datahora_Internalname, "Data", "", "", lblTextblocklogresponsavel_datahora_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_LogResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtLogResponsavel_DataHora_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtLogResponsavel_DataHora_Internalname, context.localUtil.TToC( A893LogResponsavel_DataHora, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A893LogResponsavel_DataHora, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLogResponsavel_DataHora_Jsonclick, 0, "Attribute", "", "", "", 1, edtLogResponsavel_DataHora_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, 0, 0, true, "DataHora", "right", false, "HLP_LogResponsavel.htm");
            GxWebStd.gx_bitmap( context, edtLogResponsavel_DataHora_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtLogResponsavel_DataHora_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_LogResponsavel.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklogresponsavel_prazo_Internalname, "Prazo", "", "", lblTextblocklogresponsavel_prazo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_LogResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtLogResponsavel_Prazo_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtLogResponsavel_Prazo_Internalname, context.localUtil.TToC( A1177LogResponsavel_Prazo, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A1177LogResponsavel_Prazo, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLogResponsavel_Prazo_Jsonclick, 0, "Attribute", "", "", "", 1, edtLogResponsavel_Prazo_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "DataHora", "right", false, "HLP_LogResponsavel.htm");
            GxWebStd.gx_bitmap( context, edtLogResponsavel_Prazo_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtLogResponsavel_Prazo_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_LogResponsavel.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklogresponsavel_acao_Internalname, "A��o", "", "", lblTextblocklogresponsavel_acao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_LogResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbLogResponsavel_Acao, cmbLogResponsavel_Acao_Internalname, StringUtil.RTrim( A894LogResponsavel_Acao), 1, cmbLogResponsavel_Acao_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbLogResponsavel_Acao.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,54);\"", "", true, "HLP_LogResponsavel.htm");
            cmbLogResponsavel_Acao.CurrentValue = StringUtil.RTrim( A894LogResponsavel_Acao);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbLogResponsavel_Acao_Internalname, "Values", (String)(cmbLogResponsavel_Acao.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklogresponsavel_usuariocod_Internalname, "Pessoa", "", "", lblTextblocklogresponsavel_usuariocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_LogResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynLogResponsavel_UsuarioCod, dynLogResponsavel_UsuarioCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A891LogResponsavel_UsuarioCod), 6, 0)), 1, dynLogResponsavel_UsuarioCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynLogResponsavel_UsuarioCod.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", "", true, "HLP_LogResponsavel.htm");
            dynLogResponsavel_UsuarioCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A891LogResponsavel_UsuarioCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynLogResponsavel_UsuarioCod_Internalname, "Values", (String)(dynLogResponsavel_UsuarioCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklogresponsavel_usuarioehcontratante_Internalname, "Contratante", "", "", lblTextblocklogresponsavel_usuarioehcontratante_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_LogResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Check box */
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkLogResponsavel_UsuarioEhContratante_Internalname, StringUtil.BoolToStr( A1148LogResponsavel_UsuarioEhContratante), "", "", 1, chkLogResponsavel_UsuarioEhContratante.Enabled, "true", "", StyleString, ClassString, "", "");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklogresponsavel_objetotipo_Internalname, "Objeto", "", "", lblTextblocklogresponsavel_objetotipo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_LogResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbLogResponsavel_ObjetoTipo, cmbLogResponsavel_ObjetoTipo_Internalname, StringUtil.RTrim( A895LogResponsavel_ObjetoTipo), 1, cmbLogResponsavel_ObjetoTipo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbLogResponsavel_ObjetoTipo.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,69);\"", "", true, "HLP_LogResponsavel.htm");
            cmbLogResponsavel_ObjetoTipo.CurrentValue = StringUtil.RTrim( A895LogResponsavel_ObjetoTipo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbLogResponsavel_ObjetoTipo_Internalname, "Values", (String)(cmbLogResponsavel_ObjetoTipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklogresponsavel_demandacod_Internalname, "Demanda", "", "", lblTextblocklogresponsavel_demandacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_LogResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtLogResponsavel_DemandaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A892LogResponsavel_DemandaCod), 6, 0, ",", "")), ((edtLogResponsavel_DemandaCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A892LogResponsavel_DemandaCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A892LogResponsavel_DemandaCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,74);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLogResponsavel_DemandaCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtLogResponsavel_DemandaCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_LogResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_areatrabalhocod_Internalname, "�rea de Trabalho", "", "", lblTextblockcontratada_areatrabalhocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_LogResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratada_AreaTrabalhoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0, ",", "")), ((edtContratada_AreaTrabalhoCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A52Contratada_AreaTrabalhoCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A52Contratada_AreaTrabalhoCod), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_AreaTrabalhoCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtContratada_AreaTrabalhoCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_LogResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklogresponsavel_owner_Internalname, "Owner", "", "", lblTextblocklogresponsavel_owner_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_LogResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtLogResponsavel_Owner_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A896LogResponsavel_Owner), 6, 0, ",", "")), ((edtLogResponsavel_Owner_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A896LogResponsavel_Owner), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A896LogResponsavel_Owner), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,84);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLogResponsavel_Owner_Jsonclick, 0, "Attribute", "", "", "", 1, edtLogResponsavel_Owner_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_LogResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklogresponsavel_ownerpessoacod_Internalname, "Pessoa", "", "", lblTextblocklogresponsavel_ownerpessoacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_LogResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtLogResponsavel_OwnerPessoaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1219LogResponsavel_OwnerPessoaCod), 6, 0, ",", "")), ((edtLogResponsavel_OwnerPessoaCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1219LogResponsavel_OwnerPessoaCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1219LogResponsavel_OwnerPessoaCod), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLogResponsavel_OwnerPessoaCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtLogResponsavel_OwnerPessoaCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_LogResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklogresponsavel_ownerpessoanom_Internalname, "Pessoa Nom", "", "", lblTextblocklogresponsavel_ownerpessoanom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_LogResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtLogResponsavel_OwnerPessoaNom_Internalname, StringUtil.RTrim( A1222LogResponsavel_OwnerPessoaNom), StringUtil.RTrim( context.localUtil.Format( A1222LogResponsavel_OwnerPessoaNom, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLogResponsavel_OwnerPessoaNom_Jsonclick, 0, "BootstrapAttribute100", "", "", "", 1, edtLogResponsavel_OwnerPessoaNom_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_LogResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklogresponsavel_ownerehcontratante_Internalname, "contratante", "", "", lblTextblocklogresponsavel_ownerehcontratante_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_LogResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Check box */
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkLogResponsavel_OwnerEhContratante_Internalname, StringUtil.BoolToStr( A1149LogResponsavel_OwnerEhContratante), "", "", 1, chkLogResponsavel_OwnerEhContratante.Enabled, "true", "", StyleString, ClassString, "", "");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklogresponsavel_status_Internalname, "anterior", "", "", lblTextblocklogresponsavel_status_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_LogResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbLogResponsavel_Status, cmbLogResponsavel_Status_Internalname, StringUtil.RTrim( A1130LogResponsavel_Status), 1, cmbLogResponsavel_Status_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbLogResponsavel_Status.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,104);\"", "", true, "HLP_LogResponsavel.htm");
            cmbLogResponsavel_Status.CurrentValue = StringUtil.RTrim( A1130LogResponsavel_Status);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbLogResponsavel_Status_Internalname, "Values", (String)(cmbLogResponsavel_Status.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklogresponsavel_novostatus_Internalname, "status", "", "", lblTextblocklogresponsavel_novostatus_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_LogResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbLogResponsavel_NovoStatus, cmbLogResponsavel_NovoStatus_Internalname, StringUtil.RTrim( A1234LogResponsavel_NovoStatus), 1, cmbLogResponsavel_NovoStatus_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbLogResponsavel_NovoStatus.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,109);\"", "", true, "HLP_LogResponsavel.htm");
            cmbLogResponsavel_NovoStatus.CurrentValue = StringUtil.RTrim( A1234LogResponsavel_NovoStatus);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbLogResponsavel_NovoStatus_Internalname, "Values", (String)(cmbLogResponsavel_NovoStatus.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklogresponsavel_observacao_Internalname, "Observa��o", "", "", lblTextblocklogresponsavel_observacao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_LogResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtLogResponsavel_Observacao_Internalname, A1131LogResponsavel_Observacao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,114);\"", 0, 1, edtLogResponsavel_Observacao_Enabled, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_LogResponsavel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_2T198e( true) ;
         }
         else
         {
            wb_table4_34_2T198e( false) ;
         }
      }

      protected void wb_table2_5_2T198( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSectiontoolbar_Internalname, 1, 0, "px", 0, "px", "ToolbarMain", "left", "top", "", "WHITE-SPACE: nowrap;", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_Internalname, context.GetImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_Visible, 1, "", "Primeiro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_LogResponsavel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_LogResponsavel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_Internalname, context.GetImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_LogResponsavel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_LogResponsavel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_Internalname, context.GetImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_Visible, 1, "", "Pr�ximo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_LogResponsavel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_LogResponsavel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_Internalname, context.GetImagePath( "29211874-e613-48e5-9011-8017d984217e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_Visible, 1, "", "�ltimo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_LogResponsavel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_LogResponsavel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_Internalname, context.GetImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_Visible, 1, "", "Selecionar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_LogResponsavel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_LogResponsavel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_Internalname, context.GetImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_LogResponsavel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_LogResponsavel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_Internalname, context.GetImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_Visible, 1, "", "Fechar", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_LogResponsavel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_LogResponsavel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_Internalname, context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_LogResponsavel.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_LogResponsavel.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_2T198e( true) ;
         }
         else
         {
            wb_table2_5_2T198e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtLogResponsavel_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtLogResponsavel_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 9999999999L )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "LOGRESPONSAVEL_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtLogResponsavel_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1797LogResponsavel_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1797LogResponsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1797LogResponsavel_Codigo), 10, 0)));
               }
               else
               {
                  A1797LogResponsavel_Codigo = (long)(context.localUtil.CToN( cgiGet( edtLogResponsavel_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1797LogResponsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1797LogResponsavel_Codigo), 10, 0)));
               }
               if ( context.localUtil.VCDateTime( cgiGet( edtLogResponsavel_DataHora_Internalname), 2, 0) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Data"}), 1, "LOGRESPONSAVEL_DATAHORA");
                  AnyError = 1;
                  GX_FocusControl = edtLogResponsavel_DataHora_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A893LogResponsavel_DataHora = (DateTime)(DateTime.MinValue);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A893LogResponsavel_DataHora", context.localUtil.TToC( A893LogResponsavel_DataHora, 8, 5, 0, 3, "/", ":", " "));
               }
               else
               {
                  A893LogResponsavel_DataHora = context.localUtil.CToT( cgiGet( edtLogResponsavel_DataHora_Internalname));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A893LogResponsavel_DataHora", context.localUtil.TToC( A893LogResponsavel_DataHora, 8, 5, 0, 3, "/", ":", " "));
               }
               if ( context.localUtil.VCDateTime( cgiGet( edtLogResponsavel_Prazo_Internalname), 2, 0) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Prazo"}), 1, "LOGRESPONSAVEL_PRAZO");
                  AnyError = 1;
                  GX_FocusControl = edtLogResponsavel_Prazo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1177LogResponsavel_Prazo = (DateTime)(DateTime.MinValue);
                  n1177LogResponsavel_Prazo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1177LogResponsavel_Prazo", context.localUtil.TToC( A1177LogResponsavel_Prazo, 8, 5, 0, 3, "/", ":", " "));
               }
               else
               {
                  A1177LogResponsavel_Prazo = context.localUtil.CToT( cgiGet( edtLogResponsavel_Prazo_Internalname));
                  n1177LogResponsavel_Prazo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1177LogResponsavel_Prazo", context.localUtil.TToC( A1177LogResponsavel_Prazo, 8, 5, 0, 3, "/", ":", " "));
               }
               n1177LogResponsavel_Prazo = ((DateTime.MinValue==A1177LogResponsavel_Prazo) ? true : false);
               cmbLogResponsavel_Acao.CurrentValue = cgiGet( cmbLogResponsavel_Acao_Internalname);
               A894LogResponsavel_Acao = cgiGet( cmbLogResponsavel_Acao_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A894LogResponsavel_Acao", A894LogResponsavel_Acao);
               dynLogResponsavel_UsuarioCod.CurrentValue = cgiGet( dynLogResponsavel_UsuarioCod_Internalname);
               A891LogResponsavel_UsuarioCod = (int)(NumberUtil.Val( cgiGet( dynLogResponsavel_UsuarioCod_Internalname), "."));
               n891LogResponsavel_UsuarioCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A891LogResponsavel_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A891LogResponsavel_UsuarioCod), 6, 0)));
               n891LogResponsavel_UsuarioCod = ((0==A891LogResponsavel_UsuarioCod) ? true : false);
               A1148LogResponsavel_UsuarioEhContratante = StringUtil.StrToBool( cgiGet( chkLogResponsavel_UsuarioEhContratante_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1148LogResponsavel_UsuarioEhContratante", A1148LogResponsavel_UsuarioEhContratante);
               cmbLogResponsavel_ObjetoTipo.CurrentValue = cgiGet( cmbLogResponsavel_ObjetoTipo_Internalname);
               A895LogResponsavel_ObjetoTipo = cgiGet( cmbLogResponsavel_ObjetoTipo_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A895LogResponsavel_ObjetoTipo", A895LogResponsavel_ObjetoTipo);
               if ( ( ( context.localUtil.CToN( cgiGet( edtLogResponsavel_DemandaCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtLogResponsavel_DemandaCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "LOGRESPONSAVEL_DEMANDACOD");
                  AnyError = 1;
                  GX_FocusControl = edtLogResponsavel_DemandaCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A892LogResponsavel_DemandaCod = 0;
                  n892LogResponsavel_DemandaCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A892LogResponsavel_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A892LogResponsavel_DemandaCod), 6, 0)));
               }
               else
               {
                  A892LogResponsavel_DemandaCod = (int)(context.localUtil.CToN( cgiGet( edtLogResponsavel_DemandaCod_Internalname), ",", "."));
                  n892LogResponsavel_DemandaCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A892LogResponsavel_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A892LogResponsavel_DemandaCod), 6, 0)));
               }
               n892LogResponsavel_DemandaCod = ((0==A892LogResponsavel_DemandaCod) ? true : false);
               A52Contratada_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtContratada_AreaTrabalhoCod_Internalname), ",", "."));
               n52Contratada_AreaTrabalhoCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A52Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0)));
               if ( ( ( context.localUtil.CToN( cgiGet( edtLogResponsavel_Owner_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtLogResponsavel_Owner_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "LOGRESPONSAVEL_OWNER");
                  AnyError = 1;
                  GX_FocusControl = edtLogResponsavel_Owner_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A896LogResponsavel_Owner = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A896LogResponsavel_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A896LogResponsavel_Owner), 6, 0)));
               }
               else
               {
                  A896LogResponsavel_Owner = (int)(context.localUtil.CToN( cgiGet( edtLogResponsavel_Owner_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A896LogResponsavel_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A896LogResponsavel_Owner), 6, 0)));
               }
               A1219LogResponsavel_OwnerPessoaCod = (int)(context.localUtil.CToN( cgiGet( edtLogResponsavel_OwnerPessoaCod_Internalname), ",", "."));
               n1219LogResponsavel_OwnerPessoaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1219LogResponsavel_OwnerPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1219LogResponsavel_OwnerPessoaCod), 6, 0)));
               A1222LogResponsavel_OwnerPessoaNom = StringUtil.Upper( cgiGet( edtLogResponsavel_OwnerPessoaNom_Internalname));
               n1222LogResponsavel_OwnerPessoaNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1222LogResponsavel_OwnerPessoaNom", A1222LogResponsavel_OwnerPessoaNom);
               A1149LogResponsavel_OwnerEhContratante = StringUtil.StrToBool( cgiGet( chkLogResponsavel_OwnerEhContratante_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1149LogResponsavel_OwnerEhContratante", A1149LogResponsavel_OwnerEhContratante);
               cmbLogResponsavel_Status.CurrentValue = cgiGet( cmbLogResponsavel_Status_Internalname);
               A1130LogResponsavel_Status = cgiGet( cmbLogResponsavel_Status_Internalname);
               n1130LogResponsavel_Status = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1130LogResponsavel_Status", A1130LogResponsavel_Status);
               n1130LogResponsavel_Status = (String.IsNullOrEmpty(StringUtil.RTrim( A1130LogResponsavel_Status)) ? true : false);
               cmbLogResponsavel_NovoStatus.CurrentValue = cgiGet( cmbLogResponsavel_NovoStatus_Internalname);
               A1234LogResponsavel_NovoStatus = cgiGet( cmbLogResponsavel_NovoStatus_Internalname);
               n1234LogResponsavel_NovoStatus = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1234LogResponsavel_NovoStatus", A1234LogResponsavel_NovoStatus);
               n1234LogResponsavel_NovoStatus = (String.IsNullOrEmpty(StringUtil.RTrim( A1234LogResponsavel_NovoStatus)) ? true : false);
               A1131LogResponsavel_Observacao = cgiGet( edtLogResponsavel_Observacao_Internalname);
               n1131LogResponsavel_Observacao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1131LogResponsavel_Observacao", A1131LogResponsavel_Observacao);
               n1131LogResponsavel_Observacao = (String.IsNullOrEmpty(StringUtil.RTrim( A1131LogResponsavel_Observacao)) ? true : false);
               /* Read saved values. */
               Z1797LogResponsavel_Codigo = (long)(context.localUtil.CToN( cgiGet( "Z1797LogResponsavel_Codigo"), ",", "."));
               Z893LogResponsavel_DataHora = context.localUtil.CToT( cgiGet( "Z893LogResponsavel_DataHora"), 0);
               Z1177LogResponsavel_Prazo = context.localUtil.CToT( cgiGet( "Z1177LogResponsavel_Prazo"), 0);
               n1177LogResponsavel_Prazo = ((DateTime.MinValue==A1177LogResponsavel_Prazo) ? true : false);
               Z894LogResponsavel_Acao = cgiGet( "Z894LogResponsavel_Acao");
               Z895LogResponsavel_ObjetoTipo = cgiGet( "Z895LogResponsavel_ObjetoTipo");
               Z1130LogResponsavel_Status = cgiGet( "Z1130LogResponsavel_Status");
               n1130LogResponsavel_Status = (String.IsNullOrEmpty(StringUtil.RTrim( A1130LogResponsavel_Status)) ? true : false);
               Z1234LogResponsavel_NovoStatus = cgiGet( "Z1234LogResponsavel_NovoStatus");
               n1234LogResponsavel_NovoStatus = (String.IsNullOrEmpty(StringUtil.RTrim( A1234LogResponsavel_NovoStatus)) ? true : false);
               Z891LogResponsavel_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( "Z891LogResponsavel_UsuarioCod"), ",", "."));
               n891LogResponsavel_UsuarioCod = ((0==A891LogResponsavel_UsuarioCod) ? true : false);
               Z896LogResponsavel_Owner = (int)(context.localUtil.CToN( cgiGet( "Z896LogResponsavel_Owner"), ",", "."));
               Z892LogResponsavel_DemandaCod = (int)(context.localUtil.CToN( cgiGet( "Z892LogResponsavel_DemandaCod"), ",", "."));
               n892LogResponsavel_DemandaCod = ((0==A892LogResponsavel_DemandaCod) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               A490ContagemResultado_ContratadaCod = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADO_CONTRATADACOD"), ",", "."));
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A1797LogResponsavel_Codigo = (long)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1797LogResponsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1797LogResponsavel_Codigo), 10, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll2T198( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         imgBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_Visible), 5, 0)));
         imgBtn_first_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_separator_Visible), 5, 0)));
         imgBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_Visible), 5, 0)));
         imgBtn_previous_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_separator_Visible), 5, 0)));
         imgBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_Visible), 5, 0)));
         imgBtn_next_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_separator_Visible), 5, 0)));
         imgBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_Visible), 5, 0)));
         imgBtn_last_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_separator_Visible), 5, 0)));
         imgBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_Visible), 5, 0)));
         imgBtn_select_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_separator_Visible), 5, 0)));
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Visible), 5, 0)));
            imgBtn_enter2_separator_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_separator_Visible), 5, 0)));
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)));
         }
         DisableAttributes2T198( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption2T0( )
      {
      }

      protected void ZM2T198( short GX_JID )
      {
         if ( ( GX_JID == 8 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z893LogResponsavel_DataHora = T002T3_A893LogResponsavel_DataHora[0];
               Z1177LogResponsavel_Prazo = T002T3_A1177LogResponsavel_Prazo[0];
               Z894LogResponsavel_Acao = T002T3_A894LogResponsavel_Acao[0];
               Z895LogResponsavel_ObjetoTipo = T002T3_A895LogResponsavel_ObjetoTipo[0];
               Z1130LogResponsavel_Status = T002T3_A1130LogResponsavel_Status[0];
               Z1234LogResponsavel_NovoStatus = T002T3_A1234LogResponsavel_NovoStatus[0];
               Z891LogResponsavel_UsuarioCod = T002T3_A891LogResponsavel_UsuarioCod[0];
               Z896LogResponsavel_Owner = T002T3_A896LogResponsavel_Owner[0];
               Z892LogResponsavel_DemandaCod = T002T3_A892LogResponsavel_DemandaCod[0];
            }
            else
            {
               Z893LogResponsavel_DataHora = A893LogResponsavel_DataHora;
               Z1177LogResponsavel_Prazo = A1177LogResponsavel_Prazo;
               Z894LogResponsavel_Acao = A894LogResponsavel_Acao;
               Z895LogResponsavel_ObjetoTipo = A895LogResponsavel_ObjetoTipo;
               Z1130LogResponsavel_Status = A1130LogResponsavel_Status;
               Z1234LogResponsavel_NovoStatus = A1234LogResponsavel_NovoStatus;
               Z891LogResponsavel_UsuarioCod = A891LogResponsavel_UsuarioCod;
               Z896LogResponsavel_Owner = A896LogResponsavel_Owner;
               Z892LogResponsavel_DemandaCod = A892LogResponsavel_DemandaCod;
            }
         }
         if ( GX_JID == -8 )
         {
            Z1797LogResponsavel_Codigo = A1797LogResponsavel_Codigo;
            Z893LogResponsavel_DataHora = A893LogResponsavel_DataHora;
            Z1177LogResponsavel_Prazo = A1177LogResponsavel_Prazo;
            Z894LogResponsavel_Acao = A894LogResponsavel_Acao;
            Z895LogResponsavel_ObjetoTipo = A895LogResponsavel_ObjetoTipo;
            Z1130LogResponsavel_Status = A1130LogResponsavel_Status;
            Z1234LogResponsavel_NovoStatus = A1234LogResponsavel_NovoStatus;
            Z1131LogResponsavel_Observacao = A1131LogResponsavel_Observacao;
            Z891LogResponsavel_UsuarioCod = A891LogResponsavel_UsuarioCod;
            Z896LogResponsavel_Owner = A896LogResponsavel_Owner;
            Z892LogResponsavel_DemandaCod = A892LogResponsavel_DemandaCod;
            Z490ContagemResultado_ContratadaCod = A490ContagemResultado_ContratadaCod;
            Z52Contratada_AreaTrabalhoCod = A52Contratada_AreaTrabalhoCod;
            Z1219LogResponsavel_OwnerPessoaCod = A1219LogResponsavel_OwnerPessoaCod;
            Z1222LogResponsavel_OwnerPessoaNom = A1222LogResponsavel_OwnerPessoaNom;
         }
      }

      protected void standaloneNotModal( )
      {
         GXALOGRESPONSAVEL_USUARIOCOD_html2T198( ) ;
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_delete2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_enter2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
      }

      protected void Load2T198( )
      {
         /* Using cursor T002T9 */
         pr_default.execute(7, new Object[] {A1797LogResponsavel_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound198 = 1;
            A490ContagemResultado_ContratadaCod = T002T9_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = T002T9_n490ContagemResultado_ContratadaCod[0];
            A893LogResponsavel_DataHora = T002T9_A893LogResponsavel_DataHora[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A893LogResponsavel_DataHora", context.localUtil.TToC( A893LogResponsavel_DataHora, 8, 5, 0, 3, "/", ":", " "));
            A1177LogResponsavel_Prazo = T002T9_A1177LogResponsavel_Prazo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1177LogResponsavel_Prazo", context.localUtil.TToC( A1177LogResponsavel_Prazo, 8, 5, 0, 3, "/", ":", " "));
            n1177LogResponsavel_Prazo = T002T9_n1177LogResponsavel_Prazo[0];
            A894LogResponsavel_Acao = T002T9_A894LogResponsavel_Acao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A894LogResponsavel_Acao", A894LogResponsavel_Acao);
            A895LogResponsavel_ObjetoTipo = T002T9_A895LogResponsavel_ObjetoTipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A895LogResponsavel_ObjetoTipo", A895LogResponsavel_ObjetoTipo);
            A1222LogResponsavel_OwnerPessoaNom = T002T9_A1222LogResponsavel_OwnerPessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1222LogResponsavel_OwnerPessoaNom", A1222LogResponsavel_OwnerPessoaNom);
            n1222LogResponsavel_OwnerPessoaNom = T002T9_n1222LogResponsavel_OwnerPessoaNom[0];
            A1130LogResponsavel_Status = T002T9_A1130LogResponsavel_Status[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1130LogResponsavel_Status", A1130LogResponsavel_Status);
            n1130LogResponsavel_Status = T002T9_n1130LogResponsavel_Status[0];
            A1234LogResponsavel_NovoStatus = T002T9_A1234LogResponsavel_NovoStatus[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1234LogResponsavel_NovoStatus", A1234LogResponsavel_NovoStatus);
            n1234LogResponsavel_NovoStatus = T002T9_n1234LogResponsavel_NovoStatus[0];
            A1131LogResponsavel_Observacao = T002T9_A1131LogResponsavel_Observacao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1131LogResponsavel_Observacao", A1131LogResponsavel_Observacao);
            n1131LogResponsavel_Observacao = T002T9_n1131LogResponsavel_Observacao[0];
            A891LogResponsavel_UsuarioCod = T002T9_A891LogResponsavel_UsuarioCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A891LogResponsavel_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A891LogResponsavel_UsuarioCod), 6, 0)));
            n891LogResponsavel_UsuarioCod = T002T9_n891LogResponsavel_UsuarioCod[0];
            A896LogResponsavel_Owner = T002T9_A896LogResponsavel_Owner[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A896LogResponsavel_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A896LogResponsavel_Owner), 6, 0)));
            A892LogResponsavel_DemandaCod = T002T9_A892LogResponsavel_DemandaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A892LogResponsavel_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A892LogResponsavel_DemandaCod), 6, 0)));
            n892LogResponsavel_DemandaCod = T002T9_n892LogResponsavel_DemandaCod[0];
            A1219LogResponsavel_OwnerPessoaCod = T002T9_A1219LogResponsavel_OwnerPessoaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1219LogResponsavel_OwnerPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1219LogResponsavel_OwnerPessoaCod), 6, 0)));
            n1219LogResponsavel_OwnerPessoaCod = T002T9_n1219LogResponsavel_OwnerPessoaCod[0];
            A52Contratada_AreaTrabalhoCod = T002T9_A52Contratada_AreaTrabalhoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A52Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0)));
            n52Contratada_AreaTrabalhoCod = T002T9_n52Contratada_AreaTrabalhoCod[0];
            ZM2T198( -8) ;
         }
         pr_default.close(7);
         OnLoadActions2T198( ) ;
      }

      protected void OnLoadActions2T198( )
      {
         GXt_boolean1 = A1148LogResponsavel_UsuarioEhContratante;
         new prc_usuarioehcontratante(context ).execute( ref  A892LogResponsavel_DemandaCod,  A891LogResponsavel_UsuarioCod, out  GXt_boolean1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A892LogResponsavel_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A892LogResponsavel_DemandaCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A891LogResponsavel_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A891LogResponsavel_UsuarioCod), 6, 0)));
         A1148LogResponsavel_UsuarioEhContratante = GXt_boolean1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1148LogResponsavel_UsuarioEhContratante", A1148LogResponsavel_UsuarioEhContratante);
         GXt_boolean1 = A1149LogResponsavel_OwnerEhContratante;
         new prc_usuarioehcontratante(context ).execute( ref  A892LogResponsavel_DemandaCod,  A896LogResponsavel_Owner, out  GXt_boolean1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A892LogResponsavel_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A892LogResponsavel_DemandaCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A896LogResponsavel_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A896LogResponsavel_Owner), 6, 0)));
         A1149LogResponsavel_OwnerEhContratante = GXt_boolean1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1149LogResponsavel_OwnerEhContratante", A1149LogResponsavel_OwnerEhContratante);
      }

      protected void CheckExtendedTable2T198( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         if ( ! ( (DateTime.MinValue==A893LogResponsavel_DataHora) || ( A893LogResponsavel_DataHora >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Data fora do intervalo", "OutOfRange", 1, "LOGRESPONSAVEL_DATAHORA");
            AnyError = 1;
            GX_FocusControl = edtLogResponsavel_DataHora_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( (DateTime.MinValue==A1177LogResponsavel_Prazo) || ( A1177LogResponsavel_Prazo >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Prazo fora do intervalo", "OutOfRange", 1, "LOGRESPONSAVEL_PRAZO");
            AnyError = 1;
            GX_FocusControl = edtLogResponsavel_Prazo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T002T4 */
         pr_default.execute(2, new Object[] {n891LogResponsavel_UsuarioCod, A891LogResponsavel_UsuarioCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            if ( ! ( (0==A891LogResponsavel_UsuarioCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Log Responsavel_Usuarios'.", "ForeignKeyNotFound", 1, "LOGRESPONSAVEL_USUARIOCOD");
               AnyError = 1;
               GX_FocusControl = dynLogResponsavel_UsuarioCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         pr_default.close(2);
         GXt_boolean1 = A1148LogResponsavel_UsuarioEhContratante;
         new prc_usuarioehcontratante(context ).execute( ref  A892LogResponsavel_DemandaCod,  A891LogResponsavel_UsuarioCod, out  GXt_boolean1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A892LogResponsavel_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A892LogResponsavel_DemandaCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A891LogResponsavel_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A891LogResponsavel_UsuarioCod), 6, 0)));
         A1148LogResponsavel_UsuarioEhContratante = GXt_boolean1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1148LogResponsavel_UsuarioEhContratante", A1148LogResponsavel_UsuarioEhContratante);
         /* Using cursor T002T6 */
         pr_default.execute(4, new Object[] {n892LogResponsavel_DemandaCod, A892LogResponsavel_DemandaCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (0==A892LogResponsavel_DemandaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Log Responsavel_Contagem Resultado'.", "ForeignKeyNotFound", 1, "LOGRESPONSAVEL_DEMANDACOD");
               AnyError = 1;
               GX_FocusControl = edtLogResponsavel_DemandaCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A490ContagemResultado_ContratadaCod = T002T6_A490ContagemResultado_ContratadaCod[0];
         n490ContagemResultado_ContratadaCod = T002T6_n490ContagemResultado_ContratadaCod[0];
         pr_default.close(4);
         /* Using cursor T002T8 */
         pr_default.execute(6, new Object[] {n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            if ( ! ( (0==A490ContagemResultado_ContratadaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Contratada'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A52Contratada_AreaTrabalhoCod = T002T8_A52Contratada_AreaTrabalhoCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A52Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0)));
         n52Contratada_AreaTrabalhoCod = T002T8_n52Contratada_AreaTrabalhoCod[0];
         pr_default.close(6);
         GXt_boolean1 = A1149LogResponsavel_OwnerEhContratante;
         new prc_usuarioehcontratante(context ).execute( ref  A892LogResponsavel_DemandaCod,  A896LogResponsavel_Owner, out  GXt_boolean1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A892LogResponsavel_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A892LogResponsavel_DemandaCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A896LogResponsavel_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A896LogResponsavel_Owner), 6, 0)));
         A1149LogResponsavel_OwnerEhContratante = GXt_boolean1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1149LogResponsavel_OwnerEhContratante", A1149LogResponsavel_OwnerEhContratante);
         /* Using cursor T002T5 */
         pr_default.execute(3, new Object[] {A896LogResponsavel_Owner});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Log Responsavel_Usuario Owner'.", "ForeignKeyNotFound", 1, "LOGRESPONSAVEL_OWNER");
            AnyError = 1;
            GX_FocusControl = edtLogResponsavel_Owner_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1219LogResponsavel_OwnerPessoaCod = T002T5_A1219LogResponsavel_OwnerPessoaCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1219LogResponsavel_OwnerPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1219LogResponsavel_OwnerPessoaCod), 6, 0)));
         n1219LogResponsavel_OwnerPessoaCod = T002T5_n1219LogResponsavel_OwnerPessoaCod[0];
         pr_default.close(3);
         /* Using cursor T002T7 */
         pr_default.execute(5, new Object[] {n1219LogResponsavel_OwnerPessoaCod, A1219LogResponsavel_OwnerPessoaCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1222LogResponsavel_OwnerPessoaNom = T002T7_A1222LogResponsavel_OwnerPessoaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1222LogResponsavel_OwnerPessoaNom", A1222LogResponsavel_OwnerPessoaNom);
         n1222LogResponsavel_OwnerPessoaNom = T002T7_n1222LogResponsavel_OwnerPessoaNom[0];
         pr_default.close(5);
         if ( ! ( ( StringUtil.StrCmp(A1130LogResponsavel_Status, "B") == 0 ) || ( StringUtil.StrCmp(A1130LogResponsavel_Status, "S") == 0 ) || ( StringUtil.StrCmp(A1130LogResponsavel_Status, "E") == 0 ) || ( StringUtil.StrCmp(A1130LogResponsavel_Status, "A") == 0 ) || ( StringUtil.StrCmp(A1130LogResponsavel_Status, "R") == 0 ) || ( StringUtil.StrCmp(A1130LogResponsavel_Status, "C") == 0 ) || ( StringUtil.StrCmp(A1130LogResponsavel_Status, "D") == 0 ) || ( StringUtil.StrCmp(A1130LogResponsavel_Status, "H") == 0 ) || ( StringUtil.StrCmp(A1130LogResponsavel_Status, "O") == 0 ) || ( StringUtil.StrCmp(A1130LogResponsavel_Status, "P") == 0 ) || ( StringUtil.StrCmp(A1130LogResponsavel_Status, "L") == 0 ) || ( StringUtil.StrCmp(A1130LogResponsavel_Status, "X") == 0 ) || ( StringUtil.StrCmp(A1130LogResponsavel_Status, "N") == 0 ) || ( StringUtil.StrCmp(A1130LogResponsavel_Status, "J") == 0 ) || ( StringUtil.StrCmp(A1130LogResponsavel_Status, "I") == 0 ) || ( StringUtil.StrCmp(A1130LogResponsavel_Status, "T") == 0 ) || ( StringUtil.StrCmp(A1130LogResponsavel_Status, "Q") == 0 ) || ( StringUtil.StrCmp(A1130LogResponsavel_Status, "G") == 0 ) || ( StringUtil.StrCmp(A1130LogResponsavel_Status, "M") == 0 ) || ( StringUtil.StrCmp(A1130LogResponsavel_Status, "U") == 0 ) || String.IsNullOrEmpty(StringUtil.RTrim( A1130LogResponsavel_Status)) ) )
         {
            GX_msglist.addItem("Campo Status anterior fora do intervalo", "OutOfRange", 1, "LOGRESPONSAVEL_STATUS");
            AnyError = 1;
            GX_FocusControl = cmbLogResponsavel_Status_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( ( StringUtil.StrCmp(A1234LogResponsavel_NovoStatus, "B") == 0 ) || ( StringUtil.StrCmp(A1234LogResponsavel_NovoStatus, "S") == 0 ) || ( StringUtil.StrCmp(A1234LogResponsavel_NovoStatus, "E") == 0 ) || ( StringUtil.StrCmp(A1234LogResponsavel_NovoStatus, "A") == 0 ) || ( StringUtil.StrCmp(A1234LogResponsavel_NovoStatus, "R") == 0 ) || ( StringUtil.StrCmp(A1234LogResponsavel_NovoStatus, "C") == 0 ) || ( StringUtil.StrCmp(A1234LogResponsavel_NovoStatus, "D") == 0 ) || ( StringUtil.StrCmp(A1234LogResponsavel_NovoStatus, "H") == 0 ) || ( StringUtil.StrCmp(A1234LogResponsavel_NovoStatus, "O") == 0 ) || ( StringUtil.StrCmp(A1234LogResponsavel_NovoStatus, "P") == 0 ) || ( StringUtil.StrCmp(A1234LogResponsavel_NovoStatus, "L") == 0 ) || ( StringUtil.StrCmp(A1234LogResponsavel_NovoStatus, "X") == 0 ) || ( StringUtil.StrCmp(A1234LogResponsavel_NovoStatus, "N") == 0 ) || ( StringUtil.StrCmp(A1234LogResponsavel_NovoStatus, "J") == 0 ) || ( StringUtil.StrCmp(A1234LogResponsavel_NovoStatus, "I") == 0 ) || ( StringUtil.StrCmp(A1234LogResponsavel_NovoStatus, "T") == 0 ) || ( StringUtil.StrCmp(A1234LogResponsavel_NovoStatus, "Q") == 0 ) || ( StringUtil.StrCmp(A1234LogResponsavel_NovoStatus, "G") == 0 ) || ( StringUtil.StrCmp(A1234LogResponsavel_NovoStatus, "M") == 0 ) || ( StringUtil.StrCmp(A1234LogResponsavel_NovoStatus, "U") == 0 ) || String.IsNullOrEmpty(StringUtil.RTrim( A1234LogResponsavel_NovoStatus)) ) )
         {
            GX_msglist.addItem("Campo Novo status fora do intervalo", "OutOfRange", 1, "LOGRESPONSAVEL_NOVOSTATUS");
            AnyError = 1;
            GX_FocusControl = cmbLogResponsavel_NovoStatus_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors2T198( )
      {
         pr_default.close(2);
         pr_default.close(4);
         pr_default.close(6);
         pr_default.close(3);
         pr_default.close(5);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_9( int A891LogResponsavel_UsuarioCod )
      {
         /* Using cursor T002T10 */
         pr_default.execute(8, new Object[] {n891LogResponsavel_UsuarioCod, A891LogResponsavel_UsuarioCod});
         if ( (pr_default.getStatus(8) == 101) )
         {
            if ( ! ( (0==A891LogResponsavel_UsuarioCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Log Responsavel_Usuarios'.", "ForeignKeyNotFound", 1, "LOGRESPONSAVEL_USUARIOCOD");
               AnyError = 1;
               GX_FocusControl = dynLogResponsavel_UsuarioCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(8) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(8);
      }

      protected void gxLoad_11( int A892LogResponsavel_DemandaCod )
      {
         /* Using cursor T002T11 */
         pr_default.execute(9, new Object[] {n892LogResponsavel_DemandaCod, A892LogResponsavel_DemandaCod});
         if ( (pr_default.getStatus(9) == 101) )
         {
            if ( ! ( (0==A892LogResponsavel_DemandaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Log Responsavel_Contagem Resultado'.", "ForeignKeyNotFound", 1, "LOGRESPONSAVEL_DEMANDACOD");
               AnyError = 1;
               GX_FocusControl = edtLogResponsavel_DemandaCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A490ContagemResultado_ContratadaCod = T002T11_A490ContagemResultado_ContratadaCod[0];
         n490ContagemResultado_ContratadaCod = T002T11_n490ContagemResultado_ContratadaCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(9) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(9);
      }

      protected void gxLoad_13( int A490ContagemResultado_ContratadaCod )
      {
         /* Using cursor T002T12 */
         pr_default.execute(10, new Object[] {n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod});
         if ( (pr_default.getStatus(10) == 101) )
         {
            if ( ! ( (0==A490ContagemResultado_ContratadaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Contratada'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A52Contratada_AreaTrabalhoCod = T002T12_A52Contratada_AreaTrabalhoCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A52Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0)));
         n52Contratada_AreaTrabalhoCod = T002T12_n52Contratada_AreaTrabalhoCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(10) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(10);
      }

      protected void gxLoad_10( int A896LogResponsavel_Owner )
      {
         /* Using cursor T002T13 */
         pr_default.execute(11, new Object[] {A896LogResponsavel_Owner});
         if ( (pr_default.getStatus(11) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Log Responsavel_Usuario Owner'.", "ForeignKeyNotFound", 1, "LOGRESPONSAVEL_OWNER");
            AnyError = 1;
            GX_FocusControl = edtLogResponsavel_Owner_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1219LogResponsavel_OwnerPessoaCod = T002T13_A1219LogResponsavel_OwnerPessoaCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1219LogResponsavel_OwnerPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1219LogResponsavel_OwnerPessoaCod), 6, 0)));
         n1219LogResponsavel_OwnerPessoaCod = T002T13_n1219LogResponsavel_OwnerPessoaCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1219LogResponsavel_OwnerPessoaCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(11) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(11);
      }

      protected void gxLoad_12( int A1219LogResponsavel_OwnerPessoaCod )
      {
         /* Using cursor T002T14 */
         pr_default.execute(12, new Object[] {n1219LogResponsavel_OwnerPessoaCod, A1219LogResponsavel_OwnerPessoaCod});
         if ( (pr_default.getStatus(12) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1222LogResponsavel_OwnerPessoaNom = T002T14_A1222LogResponsavel_OwnerPessoaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1222LogResponsavel_OwnerPessoaNom", A1222LogResponsavel_OwnerPessoaNom);
         n1222LogResponsavel_OwnerPessoaNom = T002T14_n1222LogResponsavel_OwnerPessoaNom[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1222LogResponsavel_OwnerPessoaNom))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(12) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(12);
      }

      protected void GetKey2T198( )
      {
         /* Using cursor T002T15 */
         pr_default.execute(13, new Object[] {A1797LogResponsavel_Codigo});
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound198 = 1;
         }
         else
         {
            RcdFound198 = 0;
         }
         pr_default.close(13);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T002T3 */
         pr_default.execute(1, new Object[] {A1797LogResponsavel_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM2T198( 8) ;
            RcdFound198 = 1;
            A1797LogResponsavel_Codigo = T002T3_A1797LogResponsavel_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1797LogResponsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1797LogResponsavel_Codigo), 10, 0)));
            A893LogResponsavel_DataHora = T002T3_A893LogResponsavel_DataHora[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A893LogResponsavel_DataHora", context.localUtil.TToC( A893LogResponsavel_DataHora, 8, 5, 0, 3, "/", ":", " "));
            A1177LogResponsavel_Prazo = T002T3_A1177LogResponsavel_Prazo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1177LogResponsavel_Prazo", context.localUtil.TToC( A1177LogResponsavel_Prazo, 8, 5, 0, 3, "/", ":", " "));
            n1177LogResponsavel_Prazo = T002T3_n1177LogResponsavel_Prazo[0];
            A894LogResponsavel_Acao = T002T3_A894LogResponsavel_Acao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A894LogResponsavel_Acao", A894LogResponsavel_Acao);
            A895LogResponsavel_ObjetoTipo = T002T3_A895LogResponsavel_ObjetoTipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A895LogResponsavel_ObjetoTipo", A895LogResponsavel_ObjetoTipo);
            A1130LogResponsavel_Status = T002T3_A1130LogResponsavel_Status[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1130LogResponsavel_Status", A1130LogResponsavel_Status);
            n1130LogResponsavel_Status = T002T3_n1130LogResponsavel_Status[0];
            A1234LogResponsavel_NovoStatus = T002T3_A1234LogResponsavel_NovoStatus[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1234LogResponsavel_NovoStatus", A1234LogResponsavel_NovoStatus);
            n1234LogResponsavel_NovoStatus = T002T3_n1234LogResponsavel_NovoStatus[0];
            A1131LogResponsavel_Observacao = T002T3_A1131LogResponsavel_Observacao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1131LogResponsavel_Observacao", A1131LogResponsavel_Observacao);
            n1131LogResponsavel_Observacao = T002T3_n1131LogResponsavel_Observacao[0];
            A891LogResponsavel_UsuarioCod = T002T3_A891LogResponsavel_UsuarioCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A891LogResponsavel_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A891LogResponsavel_UsuarioCod), 6, 0)));
            n891LogResponsavel_UsuarioCod = T002T3_n891LogResponsavel_UsuarioCod[0];
            A896LogResponsavel_Owner = T002T3_A896LogResponsavel_Owner[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A896LogResponsavel_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A896LogResponsavel_Owner), 6, 0)));
            A892LogResponsavel_DemandaCod = T002T3_A892LogResponsavel_DemandaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A892LogResponsavel_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A892LogResponsavel_DemandaCod), 6, 0)));
            n892LogResponsavel_DemandaCod = T002T3_n892LogResponsavel_DemandaCod[0];
            Z1797LogResponsavel_Codigo = A1797LogResponsavel_Codigo;
            sMode198 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load2T198( ) ;
            if ( AnyError == 1 )
            {
               RcdFound198 = 0;
               InitializeNonKey2T198( ) ;
            }
            Gx_mode = sMode198;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound198 = 0;
            InitializeNonKey2T198( ) ;
            sMode198 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode198;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey2T198( ) ;
         if ( RcdFound198 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound198 = 0;
         /* Using cursor T002T16 */
         pr_default.execute(14, new Object[] {A1797LogResponsavel_Codigo});
         if ( (pr_default.getStatus(14) != 101) )
         {
            while ( (pr_default.getStatus(14) != 101) && ( ( T002T16_A1797LogResponsavel_Codigo[0] < A1797LogResponsavel_Codigo ) ) )
            {
               pr_default.readNext(14);
            }
            if ( (pr_default.getStatus(14) != 101) && ( ( T002T16_A1797LogResponsavel_Codigo[0] > A1797LogResponsavel_Codigo ) ) )
            {
               A1797LogResponsavel_Codigo = T002T16_A1797LogResponsavel_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1797LogResponsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1797LogResponsavel_Codigo), 10, 0)));
               RcdFound198 = 1;
            }
         }
         pr_default.close(14);
      }

      protected void move_previous( )
      {
         RcdFound198 = 0;
         /* Using cursor T002T17 */
         pr_default.execute(15, new Object[] {A1797LogResponsavel_Codigo});
         if ( (pr_default.getStatus(15) != 101) )
         {
            while ( (pr_default.getStatus(15) != 101) && ( ( T002T17_A1797LogResponsavel_Codigo[0] > A1797LogResponsavel_Codigo ) ) )
            {
               pr_default.readNext(15);
            }
            if ( (pr_default.getStatus(15) != 101) && ( ( T002T17_A1797LogResponsavel_Codigo[0] < A1797LogResponsavel_Codigo ) ) )
            {
               A1797LogResponsavel_Codigo = T002T17_A1797LogResponsavel_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1797LogResponsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1797LogResponsavel_Codigo), 10, 0)));
               RcdFound198 = 1;
            }
         }
         pr_default.close(15);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey2T198( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtLogResponsavel_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert2T198( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound198 == 1 )
            {
               if ( A1797LogResponsavel_Codigo != Z1797LogResponsavel_Codigo )
               {
                  A1797LogResponsavel_Codigo = Z1797LogResponsavel_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1797LogResponsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1797LogResponsavel_Codigo), 10, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "LOGRESPONSAVEL_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtLogResponsavel_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtLogResponsavel_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update2T198( ) ;
                  GX_FocusControl = edtLogResponsavel_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A1797LogResponsavel_Codigo != Z1797LogResponsavel_Codigo )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtLogResponsavel_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert2T198( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "LOGRESPONSAVEL_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtLogResponsavel_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtLogResponsavel_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert2T198( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( A1797LogResponsavel_Codigo != Z1797LogResponsavel_Codigo )
         {
            A1797LogResponsavel_Codigo = Z1797LogResponsavel_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1797LogResponsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1797LogResponsavel_Codigo), 10, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "LOGRESPONSAVEL_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtLogResponsavel_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtLogResponsavel_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound198 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "LOGRESPONSAVEL_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtLogResponsavel_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtLogResponsavel_DataHora_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart2T198( ) ;
         if ( RcdFound198 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtLogResponsavel_DataHora_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd2T198( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound198 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtLogResponsavel_DataHora_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound198 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtLogResponsavel_DataHora_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart2T198( ) ;
         if ( RcdFound198 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound198 != 0 )
            {
               ScanNext2T198( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtLogResponsavel_DataHora_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd2T198( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency2T198( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T002T2 */
            pr_default.execute(0, new Object[] {A1797LogResponsavel_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"LogResponsavel"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( Z893LogResponsavel_DataHora != T002T2_A893LogResponsavel_DataHora[0] ) || ( Z1177LogResponsavel_Prazo != T002T2_A1177LogResponsavel_Prazo[0] ) || ( StringUtil.StrCmp(Z894LogResponsavel_Acao, T002T2_A894LogResponsavel_Acao[0]) != 0 ) || ( StringUtil.StrCmp(Z895LogResponsavel_ObjetoTipo, T002T2_A895LogResponsavel_ObjetoTipo[0]) != 0 ) || ( StringUtil.StrCmp(Z1130LogResponsavel_Status, T002T2_A1130LogResponsavel_Status[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( StringUtil.StrCmp(Z1234LogResponsavel_NovoStatus, T002T2_A1234LogResponsavel_NovoStatus[0]) != 0 ) || ( Z891LogResponsavel_UsuarioCod != T002T2_A891LogResponsavel_UsuarioCod[0] ) || ( Z896LogResponsavel_Owner != T002T2_A896LogResponsavel_Owner[0] ) || ( Z892LogResponsavel_DemandaCod != T002T2_A892LogResponsavel_DemandaCod[0] ) )
            {
               if ( Z893LogResponsavel_DataHora != T002T2_A893LogResponsavel_DataHora[0] )
               {
                  GXUtil.WriteLog("logresponsavel:[seudo value changed for attri]"+"LogResponsavel_DataHora");
                  GXUtil.WriteLogRaw("Old: ",Z893LogResponsavel_DataHora);
                  GXUtil.WriteLogRaw("Current: ",T002T2_A893LogResponsavel_DataHora[0]);
               }
               if ( Z1177LogResponsavel_Prazo != T002T2_A1177LogResponsavel_Prazo[0] )
               {
                  GXUtil.WriteLog("logresponsavel:[seudo value changed for attri]"+"LogResponsavel_Prazo");
                  GXUtil.WriteLogRaw("Old: ",Z1177LogResponsavel_Prazo);
                  GXUtil.WriteLogRaw("Current: ",T002T2_A1177LogResponsavel_Prazo[0]);
               }
               if ( StringUtil.StrCmp(Z894LogResponsavel_Acao, T002T2_A894LogResponsavel_Acao[0]) != 0 )
               {
                  GXUtil.WriteLog("logresponsavel:[seudo value changed for attri]"+"LogResponsavel_Acao");
                  GXUtil.WriteLogRaw("Old: ",Z894LogResponsavel_Acao);
                  GXUtil.WriteLogRaw("Current: ",T002T2_A894LogResponsavel_Acao[0]);
               }
               if ( StringUtil.StrCmp(Z895LogResponsavel_ObjetoTipo, T002T2_A895LogResponsavel_ObjetoTipo[0]) != 0 )
               {
                  GXUtil.WriteLog("logresponsavel:[seudo value changed for attri]"+"LogResponsavel_ObjetoTipo");
                  GXUtil.WriteLogRaw("Old: ",Z895LogResponsavel_ObjetoTipo);
                  GXUtil.WriteLogRaw("Current: ",T002T2_A895LogResponsavel_ObjetoTipo[0]);
               }
               if ( StringUtil.StrCmp(Z1130LogResponsavel_Status, T002T2_A1130LogResponsavel_Status[0]) != 0 )
               {
                  GXUtil.WriteLog("logresponsavel:[seudo value changed for attri]"+"LogResponsavel_Status");
                  GXUtil.WriteLogRaw("Old: ",Z1130LogResponsavel_Status);
                  GXUtil.WriteLogRaw("Current: ",T002T2_A1130LogResponsavel_Status[0]);
               }
               if ( StringUtil.StrCmp(Z1234LogResponsavel_NovoStatus, T002T2_A1234LogResponsavel_NovoStatus[0]) != 0 )
               {
                  GXUtil.WriteLog("logresponsavel:[seudo value changed for attri]"+"LogResponsavel_NovoStatus");
                  GXUtil.WriteLogRaw("Old: ",Z1234LogResponsavel_NovoStatus);
                  GXUtil.WriteLogRaw("Current: ",T002T2_A1234LogResponsavel_NovoStatus[0]);
               }
               if ( Z891LogResponsavel_UsuarioCod != T002T2_A891LogResponsavel_UsuarioCod[0] )
               {
                  GXUtil.WriteLog("logresponsavel:[seudo value changed for attri]"+"LogResponsavel_UsuarioCod");
                  GXUtil.WriteLogRaw("Old: ",Z891LogResponsavel_UsuarioCod);
                  GXUtil.WriteLogRaw("Current: ",T002T2_A891LogResponsavel_UsuarioCod[0]);
               }
               if ( Z896LogResponsavel_Owner != T002T2_A896LogResponsavel_Owner[0] )
               {
                  GXUtil.WriteLog("logresponsavel:[seudo value changed for attri]"+"LogResponsavel_Owner");
                  GXUtil.WriteLogRaw("Old: ",Z896LogResponsavel_Owner);
                  GXUtil.WriteLogRaw("Current: ",T002T2_A896LogResponsavel_Owner[0]);
               }
               if ( Z892LogResponsavel_DemandaCod != T002T2_A892LogResponsavel_DemandaCod[0] )
               {
                  GXUtil.WriteLog("logresponsavel:[seudo value changed for attri]"+"LogResponsavel_DemandaCod");
                  GXUtil.WriteLogRaw("Old: ",Z892LogResponsavel_DemandaCod);
                  GXUtil.WriteLogRaw("Current: ",T002T2_A892LogResponsavel_DemandaCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"LogResponsavel"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert2T198( )
      {
         BeforeValidate2T198( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2T198( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM2T198( 0) ;
            CheckOptimisticConcurrency2T198( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2T198( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert2T198( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002T18 */
                     pr_default.execute(16, new Object[] {A893LogResponsavel_DataHora, n1177LogResponsavel_Prazo, A1177LogResponsavel_Prazo, A894LogResponsavel_Acao, A895LogResponsavel_ObjetoTipo, n1130LogResponsavel_Status, A1130LogResponsavel_Status, n1234LogResponsavel_NovoStatus, A1234LogResponsavel_NovoStatus, n1131LogResponsavel_Observacao, A1131LogResponsavel_Observacao, n891LogResponsavel_UsuarioCod, A891LogResponsavel_UsuarioCod, A896LogResponsavel_Owner, n892LogResponsavel_DemandaCod, A892LogResponsavel_DemandaCod});
                     A1797LogResponsavel_Codigo = T002T18_A1797LogResponsavel_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1797LogResponsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1797LogResponsavel_Codigo), 10, 0)));
                     pr_default.close(16);
                     dsDefault.SmartCacheProvider.SetUpdated("LogResponsavel") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption2T0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load2T198( ) ;
            }
            EndLevel2T198( ) ;
         }
         CloseExtendedTableCursors2T198( ) ;
      }

      protected void Update2T198( )
      {
         BeforeValidate2T198( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2T198( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2T198( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2T198( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate2T198( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002T19 */
                     pr_default.execute(17, new Object[] {A893LogResponsavel_DataHora, n1177LogResponsavel_Prazo, A1177LogResponsavel_Prazo, A894LogResponsavel_Acao, A895LogResponsavel_ObjetoTipo, n1130LogResponsavel_Status, A1130LogResponsavel_Status, n1234LogResponsavel_NovoStatus, A1234LogResponsavel_NovoStatus, n1131LogResponsavel_Observacao, A1131LogResponsavel_Observacao, n891LogResponsavel_UsuarioCod, A891LogResponsavel_UsuarioCod, A896LogResponsavel_Owner, n892LogResponsavel_DemandaCod, A892LogResponsavel_DemandaCod, A1797LogResponsavel_Codigo});
                     pr_default.close(17);
                     dsDefault.SmartCacheProvider.SetUpdated("LogResponsavel") ;
                     if ( (pr_default.getStatus(17) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"LogResponsavel"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate2T198( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption2T0( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel2T198( ) ;
         }
         CloseExtendedTableCursors2T198( ) ;
      }

      protected void DeferredUpdate2T198( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate2T198( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2T198( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls2T198( ) ;
            AfterConfirm2T198( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete2T198( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T002T20 */
                  pr_default.execute(18, new Object[] {A1797LogResponsavel_Codigo});
                  pr_default.close(18);
                  dsDefault.SmartCacheProvider.SetUpdated("LogResponsavel") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound198 == 0 )
                        {
                           InitAll2T198( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption2T0( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode198 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel2T198( ) ;
         Gx_mode = sMode198;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls2T198( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T002T21 */
            pr_default.execute(19, new Object[] {n892LogResponsavel_DemandaCod, A892LogResponsavel_DemandaCod});
            A490ContagemResultado_ContratadaCod = T002T21_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = T002T21_n490ContagemResultado_ContratadaCod[0];
            pr_default.close(19);
            /* Using cursor T002T22 */
            pr_default.execute(20, new Object[] {n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod});
            A52Contratada_AreaTrabalhoCod = T002T22_A52Contratada_AreaTrabalhoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A52Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0)));
            n52Contratada_AreaTrabalhoCod = T002T22_n52Contratada_AreaTrabalhoCod[0];
            pr_default.close(20);
            GXt_boolean1 = A1148LogResponsavel_UsuarioEhContratante;
            new prc_usuarioehcontratante(context ).execute( ref  A892LogResponsavel_DemandaCod,  A891LogResponsavel_UsuarioCod, out  GXt_boolean1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A892LogResponsavel_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A892LogResponsavel_DemandaCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A891LogResponsavel_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A891LogResponsavel_UsuarioCod), 6, 0)));
            A1148LogResponsavel_UsuarioEhContratante = GXt_boolean1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1148LogResponsavel_UsuarioEhContratante", A1148LogResponsavel_UsuarioEhContratante);
            /* Using cursor T002T23 */
            pr_default.execute(21, new Object[] {A896LogResponsavel_Owner});
            A1219LogResponsavel_OwnerPessoaCod = T002T23_A1219LogResponsavel_OwnerPessoaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1219LogResponsavel_OwnerPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1219LogResponsavel_OwnerPessoaCod), 6, 0)));
            n1219LogResponsavel_OwnerPessoaCod = T002T23_n1219LogResponsavel_OwnerPessoaCod[0];
            pr_default.close(21);
            /* Using cursor T002T24 */
            pr_default.execute(22, new Object[] {n1219LogResponsavel_OwnerPessoaCod, A1219LogResponsavel_OwnerPessoaCod});
            A1222LogResponsavel_OwnerPessoaNom = T002T24_A1222LogResponsavel_OwnerPessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1222LogResponsavel_OwnerPessoaNom", A1222LogResponsavel_OwnerPessoaNom);
            n1222LogResponsavel_OwnerPessoaNom = T002T24_n1222LogResponsavel_OwnerPessoaNom[0];
            pr_default.close(22);
            GXt_boolean1 = A1149LogResponsavel_OwnerEhContratante;
            new prc_usuarioehcontratante(context ).execute( ref  A892LogResponsavel_DemandaCod,  A896LogResponsavel_Owner, out  GXt_boolean1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A892LogResponsavel_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A892LogResponsavel_DemandaCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A896LogResponsavel_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A896LogResponsavel_Owner), 6, 0)));
            A1149LogResponsavel_OwnerEhContratante = GXt_boolean1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1149LogResponsavel_OwnerEhContratante", A1149LogResponsavel_OwnerEhContratante);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T002T25 */
            pr_default.execute(23, new Object[] {A1797LogResponsavel_Codigo});
            if ( (pr_default.getStatus(23) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Resultado Nao Cnf"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(23);
         }
      }

      protected void EndLevel2T198( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete2T198( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(21);
            pr_default.close(19);
            pr_default.close(22);
            pr_default.close(20);
            context.CommitDataStores( "LogResponsavel");
            if ( AnyError == 0 )
            {
               ConfirmValues2T0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(21);
            pr_default.close(19);
            pr_default.close(22);
            pr_default.close(20);
            context.RollbackDataStores( "LogResponsavel");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart2T198( )
      {
         /* Using cursor T002T26 */
         pr_default.execute(24);
         RcdFound198 = 0;
         if ( (pr_default.getStatus(24) != 101) )
         {
            RcdFound198 = 1;
            A1797LogResponsavel_Codigo = T002T26_A1797LogResponsavel_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1797LogResponsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1797LogResponsavel_Codigo), 10, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext2T198( )
      {
         /* Scan next routine */
         pr_default.readNext(24);
         RcdFound198 = 0;
         if ( (pr_default.getStatus(24) != 101) )
         {
            RcdFound198 = 1;
            A1797LogResponsavel_Codigo = T002T26_A1797LogResponsavel_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1797LogResponsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1797LogResponsavel_Codigo), 10, 0)));
         }
      }

      protected void ScanEnd2T198( )
      {
         pr_default.close(24);
      }

      protected void AfterConfirm2T198( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert2T198( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate2T198( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete2T198( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete2T198( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate2T198( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes2T198( )
      {
         edtLogResponsavel_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLogResponsavel_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLogResponsavel_Codigo_Enabled), 5, 0)));
         edtLogResponsavel_DataHora_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLogResponsavel_DataHora_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLogResponsavel_DataHora_Enabled), 5, 0)));
         edtLogResponsavel_Prazo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLogResponsavel_Prazo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLogResponsavel_Prazo_Enabled), 5, 0)));
         cmbLogResponsavel_Acao.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbLogResponsavel_Acao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbLogResponsavel_Acao.Enabled), 5, 0)));
         dynLogResponsavel_UsuarioCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynLogResponsavel_UsuarioCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynLogResponsavel_UsuarioCod.Enabled), 5, 0)));
         chkLogResponsavel_UsuarioEhContratante.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkLogResponsavel_UsuarioEhContratante_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkLogResponsavel_UsuarioEhContratante.Enabled), 5, 0)));
         cmbLogResponsavel_ObjetoTipo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbLogResponsavel_ObjetoTipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbLogResponsavel_ObjetoTipo.Enabled), 5, 0)));
         edtLogResponsavel_DemandaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLogResponsavel_DemandaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLogResponsavel_DemandaCod_Enabled), 5, 0)));
         edtContratada_AreaTrabalhoCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_AreaTrabalhoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratada_AreaTrabalhoCod_Enabled), 5, 0)));
         edtLogResponsavel_Owner_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLogResponsavel_Owner_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLogResponsavel_Owner_Enabled), 5, 0)));
         edtLogResponsavel_OwnerPessoaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLogResponsavel_OwnerPessoaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLogResponsavel_OwnerPessoaCod_Enabled), 5, 0)));
         edtLogResponsavel_OwnerPessoaNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLogResponsavel_OwnerPessoaNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLogResponsavel_OwnerPessoaNom_Enabled), 5, 0)));
         chkLogResponsavel_OwnerEhContratante.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkLogResponsavel_OwnerEhContratante_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkLogResponsavel_OwnerEhContratante.Enabled), 5, 0)));
         cmbLogResponsavel_Status.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbLogResponsavel_Status_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbLogResponsavel_Status.Enabled), 5, 0)));
         cmbLogResponsavel_NovoStatus.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbLogResponsavel_NovoStatus_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbLogResponsavel_NovoStatus.Enabled), 5, 0)));
         edtLogResponsavel_Observacao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLogResponsavel_Observacao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLogResponsavel_Observacao_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues2T0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203122195591");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("logresponsavel.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1797LogResponsavel_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1797LogResponsavel_Codigo), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z893LogResponsavel_DataHora", context.localUtil.TToC( Z893LogResponsavel_DataHora, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "Z1177LogResponsavel_Prazo", context.localUtil.TToC( Z1177LogResponsavel_Prazo, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "Z894LogResponsavel_Acao", StringUtil.RTrim( Z894LogResponsavel_Acao));
         GxWebStd.gx_hidden_field( context, "Z895LogResponsavel_ObjetoTipo", StringUtil.RTrim( Z895LogResponsavel_ObjetoTipo));
         GxWebStd.gx_hidden_field( context, "Z1130LogResponsavel_Status", StringUtil.RTrim( Z1130LogResponsavel_Status));
         GxWebStd.gx_hidden_field( context, "Z1234LogResponsavel_NovoStatus", StringUtil.RTrim( Z1234LogResponsavel_NovoStatus));
         GxWebStd.gx_hidden_field( context, "Z891LogResponsavel_UsuarioCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z891LogResponsavel_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z896LogResponsavel_Owner", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z896LogResponsavel_Owner), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z892LogResponsavel_DemandaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z892LogResponsavel_DemandaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("logresponsavel.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "LogResponsavel" ;
      }

      public override String GetPgmdesc( )
      {
         return "Log de a��o dos Respons�veis" ;
      }

      protected void InitializeNonKey2T198( )
      {
         A490ContagemResultado_ContratadaCod = 0;
         n490ContagemResultado_ContratadaCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A490ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0)));
         A1149LogResponsavel_OwnerEhContratante = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1149LogResponsavel_OwnerEhContratante", A1149LogResponsavel_OwnerEhContratante);
         A1148LogResponsavel_UsuarioEhContratante = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1148LogResponsavel_UsuarioEhContratante", A1148LogResponsavel_UsuarioEhContratante);
         A893LogResponsavel_DataHora = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A893LogResponsavel_DataHora", context.localUtil.TToC( A893LogResponsavel_DataHora, 8, 5, 0, 3, "/", ":", " "));
         A1177LogResponsavel_Prazo = (DateTime)(DateTime.MinValue);
         n1177LogResponsavel_Prazo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1177LogResponsavel_Prazo", context.localUtil.TToC( A1177LogResponsavel_Prazo, 8, 5, 0, 3, "/", ":", " "));
         n1177LogResponsavel_Prazo = ((DateTime.MinValue==A1177LogResponsavel_Prazo) ? true : false);
         A894LogResponsavel_Acao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A894LogResponsavel_Acao", A894LogResponsavel_Acao);
         A891LogResponsavel_UsuarioCod = 0;
         n891LogResponsavel_UsuarioCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A891LogResponsavel_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A891LogResponsavel_UsuarioCod), 6, 0)));
         n891LogResponsavel_UsuarioCod = ((0==A891LogResponsavel_UsuarioCod) ? true : false);
         A895LogResponsavel_ObjetoTipo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A895LogResponsavel_ObjetoTipo", A895LogResponsavel_ObjetoTipo);
         A892LogResponsavel_DemandaCod = 0;
         n892LogResponsavel_DemandaCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A892LogResponsavel_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A892LogResponsavel_DemandaCod), 6, 0)));
         n892LogResponsavel_DemandaCod = ((0==A892LogResponsavel_DemandaCod) ? true : false);
         A52Contratada_AreaTrabalhoCod = 0;
         n52Contratada_AreaTrabalhoCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A52Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0)));
         A896LogResponsavel_Owner = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A896LogResponsavel_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A896LogResponsavel_Owner), 6, 0)));
         A1219LogResponsavel_OwnerPessoaCod = 0;
         n1219LogResponsavel_OwnerPessoaCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1219LogResponsavel_OwnerPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1219LogResponsavel_OwnerPessoaCod), 6, 0)));
         A1222LogResponsavel_OwnerPessoaNom = "";
         n1222LogResponsavel_OwnerPessoaNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1222LogResponsavel_OwnerPessoaNom", A1222LogResponsavel_OwnerPessoaNom);
         A1130LogResponsavel_Status = "";
         n1130LogResponsavel_Status = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1130LogResponsavel_Status", A1130LogResponsavel_Status);
         n1130LogResponsavel_Status = (String.IsNullOrEmpty(StringUtil.RTrim( A1130LogResponsavel_Status)) ? true : false);
         A1234LogResponsavel_NovoStatus = "";
         n1234LogResponsavel_NovoStatus = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1234LogResponsavel_NovoStatus", A1234LogResponsavel_NovoStatus);
         n1234LogResponsavel_NovoStatus = (String.IsNullOrEmpty(StringUtil.RTrim( A1234LogResponsavel_NovoStatus)) ? true : false);
         A1131LogResponsavel_Observacao = "";
         n1131LogResponsavel_Observacao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1131LogResponsavel_Observacao", A1131LogResponsavel_Observacao);
         n1131LogResponsavel_Observacao = (String.IsNullOrEmpty(StringUtil.RTrim( A1131LogResponsavel_Observacao)) ? true : false);
         Z893LogResponsavel_DataHora = (DateTime)(DateTime.MinValue);
         Z1177LogResponsavel_Prazo = (DateTime)(DateTime.MinValue);
         Z894LogResponsavel_Acao = "";
         Z895LogResponsavel_ObjetoTipo = "";
         Z1130LogResponsavel_Status = "";
         Z1234LogResponsavel_NovoStatus = "";
         Z891LogResponsavel_UsuarioCod = 0;
         Z896LogResponsavel_Owner = 0;
         Z892LogResponsavel_DemandaCod = 0;
      }

      protected void InitAll2T198( )
      {
         A1797LogResponsavel_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1797LogResponsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1797LogResponsavel_Codigo), 10, 0)));
         InitializeNonKey2T198( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020312219563");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("logresponsavel.js", "?2020312219564");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         imgBtn_first_Internalname = "BTN_FIRST";
         imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR";
         imgBtn_previous_Internalname = "BTN_PREVIOUS";
         imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR";
         imgBtn_next_Internalname = "BTN_NEXT";
         imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR";
         imgBtn_last_Internalname = "BTN_LAST";
         imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR";
         imgBtn_select_Internalname = "BTN_SELECT";
         imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR";
         imgBtn_enter2_Internalname = "BTN_ENTER2";
         imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR";
         imgBtn_cancel2_Internalname = "BTN_CANCEL2";
         imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR";
         imgBtn_delete2_Internalname = "BTN_DELETE2";
         imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR";
         divSectiontoolbar_Internalname = "SECTIONTOOLBAR";
         tblTabletoolbar_Internalname = "TABLETOOLBAR";
         lblTextblocklogresponsavel_codigo_Internalname = "TEXTBLOCKLOGRESPONSAVEL_CODIGO";
         edtLogResponsavel_Codigo_Internalname = "LOGRESPONSAVEL_CODIGO";
         lblTextblocklogresponsavel_datahora_Internalname = "TEXTBLOCKLOGRESPONSAVEL_DATAHORA";
         edtLogResponsavel_DataHora_Internalname = "LOGRESPONSAVEL_DATAHORA";
         lblTextblocklogresponsavel_prazo_Internalname = "TEXTBLOCKLOGRESPONSAVEL_PRAZO";
         edtLogResponsavel_Prazo_Internalname = "LOGRESPONSAVEL_PRAZO";
         lblTextblocklogresponsavel_acao_Internalname = "TEXTBLOCKLOGRESPONSAVEL_ACAO";
         cmbLogResponsavel_Acao_Internalname = "LOGRESPONSAVEL_ACAO";
         lblTextblocklogresponsavel_usuariocod_Internalname = "TEXTBLOCKLOGRESPONSAVEL_USUARIOCOD";
         dynLogResponsavel_UsuarioCod_Internalname = "LOGRESPONSAVEL_USUARIOCOD";
         lblTextblocklogresponsavel_usuarioehcontratante_Internalname = "TEXTBLOCKLOGRESPONSAVEL_USUARIOEHCONTRATANTE";
         chkLogResponsavel_UsuarioEhContratante_Internalname = "LOGRESPONSAVEL_USUARIOEHCONTRATANTE";
         lblTextblocklogresponsavel_objetotipo_Internalname = "TEXTBLOCKLOGRESPONSAVEL_OBJETOTIPO";
         cmbLogResponsavel_ObjetoTipo_Internalname = "LOGRESPONSAVEL_OBJETOTIPO";
         lblTextblocklogresponsavel_demandacod_Internalname = "TEXTBLOCKLOGRESPONSAVEL_DEMANDACOD";
         edtLogResponsavel_DemandaCod_Internalname = "LOGRESPONSAVEL_DEMANDACOD";
         lblTextblockcontratada_areatrabalhocod_Internalname = "TEXTBLOCKCONTRATADA_AREATRABALHOCOD";
         edtContratada_AreaTrabalhoCod_Internalname = "CONTRATADA_AREATRABALHOCOD";
         lblTextblocklogresponsavel_owner_Internalname = "TEXTBLOCKLOGRESPONSAVEL_OWNER";
         edtLogResponsavel_Owner_Internalname = "LOGRESPONSAVEL_OWNER";
         lblTextblocklogresponsavel_ownerpessoacod_Internalname = "TEXTBLOCKLOGRESPONSAVEL_OWNERPESSOACOD";
         edtLogResponsavel_OwnerPessoaCod_Internalname = "LOGRESPONSAVEL_OWNERPESSOACOD";
         lblTextblocklogresponsavel_ownerpessoanom_Internalname = "TEXTBLOCKLOGRESPONSAVEL_OWNERPESSOANOM";
         edtLogResponsavel_OwnerPessoaNom_Internalname = "LOGRESPONSAVEL_OWNERPESSOANOM";
         lblTextblocklogresponsavel_ownerehcontratante_Internalname = "TEXTBLOCKLOGRESPONSAVEL_OWNEREHCONTRATANTE";
         chkLogResponsavel_OwnerEhContratante_Internalname = "LOGRESPONSAVEL_OWNEREHCONTRATANTE";
         lblTextblocklogresponsavel_status_Internalname = "TEXTBLOCKLOGRESPONSAVEL_STATUS";
         cmbLogResponsavel_Status_Internalname = "LOGRESPONSAVEL_STATUS";
         lblTextblocklogresponsavel_novostatus_Internalname = "TEXTBLOCKLOGRESPONSAVEL_NOVOSTATUS";
         cmbLogResponsavel_NovoStatus_Internalname = "LOGRESPONSAVEL_NOVOSTATUS";
         lblTextblocklogresponsavel_observacao_Internalname = "TEXTBLOCKLOGRESPONSAVEL_OBSERVACAO";
         edtLogResponsavel_Observacao_Internalname = "LOGRESPONSAVEL_OBSERVACAO";
         tblTable2_Internalname = "TABLE2";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         tblTable1_Internalname = "TABLE1";
         grpGroupdata_Internalname = "GROUPDATA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Log de a��o dos Respons�veis";
         imgBtn_delete2_separator_Visible = 1;
         imgBtn_delete2_Enabled = 1;
         imgBtn_delete2_Visible = 1;
         imgBtn_cancel2_separator_Visible = 1;
         imgBtn_cancel2_Visible = 1;
         imgBtn_enter2_separator_Visible = 1;
         imgBtn_enter2_Enabled = 1;
         imgBtn_enter2_Visible = 1;
         imgBtn_select_separator_Visible = 1;
         imgBtn_select_Visible = 1;
         imgBtn_last_separator_Visible = 1;
         imgBtn_last_Visible = 1;
         imgBtn_next_separator_Visible = 1;
         imgBtn_next_Visible = 1;
         imgBtn_previous_separator_Visible = 1;
         imgBtn_previous_Visible = 1;
         imgBtn_first_separator_Visible = 1;
         imgBtn_first_Visible = 1;
         edtLogResponsavel_Observacao_Enabled = 1;
         cmbLogResponsavel_NovoStatus_Jsonclick = "";
         cmbLogResponsavel_NovoStatus.Enabled = 1;
         cmbLogResponsavel_Status_Jsonclick = "";
         cmbLogResponsavel_Status.Enabled = 1;
         chkLogResponsavel_OwnerEhContratante.Enabled = 0;
         edtLogResponsavel_OwnerPessoaNom_Jsonclick = "";
         edtLogResponsavel_OwnerPessoaNom_Enabled = 0;
         edtLogResponsavel_OwnerPessoaCod_Jsonclick = "";
         edtLogResponsavel_OwnerPessoaCod_Enabled = 0;
         edtLogResponsavel_Owner_Jsonclick = "";
         edtLogResponsavel_Owner_Enabled = 1;
         edtContratada_AreaTrabalhoCod_Jsonclick = "";
         edtContratada_AreaTrabalhoCod_Enabled = 0;
         edtLogResponsavel_DemandaCod_Jsonclick = "";
         edtLogResponsavel_DemandaCod_Enabled = 1;
         cmbLogResponsavel_ObjetoTipo_Jsonclick = "";
         cmbLogResponsavel_ObjetoTipo.Enabled = 1;
         chkLogResponsavel_UsuarioEhContratante.Enabled = 0;
         dynLogResponsavel_UsuarioCod_Jsonclick = "";
         dynLogResponsavel_UsuarioCod.Enabled = 1;
         cmbLogResponsavel_Acao_Jsonclick = "";
         cmbLogResponsavel_Acao.Enabled = 1;
         edtLogResponsavel_Prazo_Jsonclick = "";
         edtLogResponsavel_Prazo_Enabled = 1;
         edtLogResponsavel_DataHora_Jsonclick = "";
         edtLogResponsavel_DataHora_Enabled = 1;
         edtLogResponsavel_Codigo_Jsonclick = "";
         edtLogResponsavel_Codigo_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Visible = 1;
         chkLogResponsavel_OwnerEhContratante.Caption = "";
         chkLogResponsavel_UsuarioEhContratante.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLALOGRESPONSAVEL_USUARIOCOD2T198( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLALOGRESPONSAVEL_USUARIOCOD_data2T198( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXALOGRESPONSAVEL_USUARIOCOD_html2T198( )
      {
         int gxdynajaxvalue ;
         GXDLALOGRESPONSAVEL_USUARIOCOD_data2T198( ) ;
         gxdynajaxindex = 1;
         dynLogResponsavel_UsuarioCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynLogResponsavel_UsuarioCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLALOGRESPONSAVEL_USUARIOCOD_data2T198( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor T002T27 */
         pr_default.execute(25);
         while ( (pr_default.getStatus(25) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T002T27_A1Usuario_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T002T27_A58Usuario_PessoaNom[0]));
            pr_default.readNext(25);
         }
         pr_default.close(25);
      }

      protected void GX5ASALOGRESPONSAVEL_USUARIOEHCONTRATANTE2T198( int A892LogResponsavel_DemandaCod ,
                                                                     int A891LogResponsavel_UsuarioCod )
      {
         GXt_boolean1 = A1148LogResponsavel_UsuarioEhContratante;
         new prc_usuarioehcontratante(context ).execute( ref  A892LogResponsavel_DemandaCod,  A891LogResponsavel_UsuarioCod, out  GXt_boolean1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A892LogResponsavel_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A892LogResponsavel_DemandaCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A891LogResponsavel_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A891LogResponsavel_UsuarioCod), 6, 0)));
         A1148LogResponsavel_UsuarioEhContratante = GXt_boolean1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1148LogResponsavel_UsuarioEhContratante", A1148LogResponsavel_UsuarioEhContratante);
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.BoolToStr( A1148LogResponsavel_UsuarioEhContratante))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void GX6ASALOGRESPONSAVEL_OWNEREHCONTRATANTE2T198( int A892LogResponsavel_DemandaCod ,
                                                                   int A896LogResponsavel_Owner )
      {
         GXt_boolean1 = A1149LogResponsavel_OwnerEhContratante;
         new prc_usuarioehcontratante(context ).execute( ref  A892LogResponsavel_DemandaCod,  A896LogResponsavel_Owner, out  GXt_boolean1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A892LogResponsavel_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A892LogResponsavel_DemandaCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A896LogResponsavel_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A896LogResponsavel_Owner), 6, 0)));
         A1149LogResponsavel_OwnerEhContratante = GXt_boolean1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1149LogResponsavel_OwnerEhContratante", A1149LogResponsavel_OwnerEhContratante);
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.BoolToStr( A1149LogResponsavel_OwnerEhContratante))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         GX_FocusControl = edtLogResponsavel_DataHora_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Logresponsavel_codigo( long GX_Parm1 ,
                                               DateTime GX_Parm2 ,
                                               DateTime GX_Parm3 ,
                                               GXCombobox cmbGX_Parm4 ,
                                               GXCombobox cmbGX_Parm5 ,
                                               GXCombobox cmbGX_Parm6 ,
                                               GXCombobox cmbGX_Parm7 ,
                                               String GX_Parm8 ,
                                               GXCombobox dynGX_Parm9 ,
                                               int GX_Parm10 ,
                                               int GX_Parm11 )
      {
         A1797LogResponsavel_Codigo = GX_Parm1;
         A893LogResponsavel_DataHora = GX_Parm2;
         A1177LogResponsavel_Prazo = GX_Parm3;
         n1177LogResponsavel_Prazo = false;
         cmbLogResponsavel_Acao = cmbGX_Parm4;
         A894LogResponsavel_Acao = cmbLogResponsavel_Acao.CurrentValue;
         cmbLogResponsavel_Acao.CurrentValue = A894LogResponsavel_Acao;
         cmbLogResponsavel_ObjetoTipo = cmbGX_Parm5;
         A895LogResponsavel_ObjetoTipo = cmbLogResponsavel_ObjetoTipo.CurrentValue;
         cmbLogResponsavel_ObjetoTipo.CurrentValue = A895LogResponsavel_ObjetoTipo;
         cmbLogResponsavel_Status = cmbGX_Parm6;
         A1130LogResponsavel_Status = cmbLogResponsavel_Status.CurrentValue;
         n1130LogResponsavel_Status = false;
         cmbLogResponsavel_Status.CurrentValue = A1130LogResponsavel_Status;
         cmbLogResponsavel_NovoStatus = cmbGX_Parm7;
         A1234LogResponsavel_NovoStatus = cmbLogResponsavel_NovoStatus.CurrentValue;
         n1234LogResponsavel_NovoStatus = false;
         cmbLogResponsavel_NovoStatus.CurrentValue = A1234LogResponsavel_NovoStatus;
         A1131LogResponsavel_Observacao = GX_Parm8;
         n1131LogResponsavel_Observacao = false;
         dynLogResponsavel_UsuarioCod = dynGX_Parm9;
         A891LogResponsavel_UsuarioCod = (int)(NumberUtil.Val( dynLogResponsavel_UsuarioCod.CurrentValue, "."));
         n891LogResponsavel_UsuarioCod = false;
         A896LogResponsavel_Owner = GX_Parm10;
         A892LogResponsavel_DemandaCod = GX_Parm11;
         n892LogResponsavel_DemandaCod = false;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A490ContagemResultado_ContratadaCod = 0;
            n490ContagemResultado_ContratadaCod = false;
            A52Contratada_AreaTrabalhoCod = 0;
            n52Contratada_AreaTrabalhoCod = false;
            A1219LogResponsavel_OwnerPessoaCod = 0;
            n1219LogResponsavel_OwnerPessoaCod = false;
            A1222LogResponsavel_OwnerPessoaNom = "";
            n1222LogResponsavel_OwnerPessoaNom = false;
         }
         GXALOGRESPONSAVEL_USUARIOCOD_html2T198( ) ;
         dynLogResponsavel_UsuarioCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A891LogResponsavel_UsuarioCod), 6, 0));
         isValidOutput.Add(context.localUtil.TToC( A893LogResponsavel_DataHora, 10, 8, 0, 3, "/", ":", " "));
         isValidOutput.Add(context.localUtil.TToC( A1177LogResponsavel_Prazo, 10, 8, 0, 3, "/", ":", " "));
         cmbLogResponsavel_Acao.CurrentValue = A894LogResponsavel_Acao;
         isValidOutput.Add(cmbLogResponsavel_Acao);
         if ( dynLogResponsavel_UsuarioCod.ItemCount > 0 )
         {
            A891LogResponsavel_UsuarioCod = (int)(NumberUtil.Val( dynLogResponsavel_UsuarioCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A891LogResponsavel_UsuarioCod), 6, 0))), "."));
            n891LogResponsavel_UsuarioCod = false;
         }
         dynLogResponsavel_UsuarioCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A891LogResponsavel_UsuarioCod), 6, 0));
         isValidOutput.Add(dynLogResponsavel_UsuarioCod);
         cmbLogResponsavel_ObjetoTipo.CurrentValue = A895LogResponsavel_ObjetoTipo;
         isValidOutput.Add(cmbLogResponsavel_ObjetoTipo);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A892LogResponsavel_DemandaCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A896LogResponsavel_Owner), 6, 0, ".", "")));
         cmbLogResponsavel_Status.CurrentValue = A1130LogResponsavel_Status;
         isValidOutput.Add(cmbLogResponsavel_Status);
         cmbLogResponsavel_NovoStatus.CurrentValue = A1234LogResponsavel_NovoStatus;
         isValidOutput.Add(cmbLogResponsavel_NovoStatus);
         isValidOutput.Add(A1131LogResponsavel_Observacao);
         isValidOutput.Add(A1148LogResponsavel_UsuarioEhContratante);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0, ".", "")));
         isValidOutput.Add(A1149LogResponsavel_OwnerEhContratante);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1219LogResponsavel_OwnerPessoaCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A1222LogResponsavel_OwnerPessoaNom));
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1797LogResponsavel_Codigo), 10, 0, ",", "")));
         isValidOutput.Add(context.localUtil.TToC( Z893LogResponsavel_DataHora, 10, 8, 0, 0, "/", ":", " "));
         isValidOutput.Add(context.localUtil.TToC( Z1177LogResponsavel_Prazo, 10, 8, 0, 0, "/", ":", " "));
         isValidOutput.Add(StringUtil.RTrim( Z894LogResponsavel_Acao));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z891LogResponsavel_UsuarioCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.RTrim( Z895LogResponsavel_ObjetoTipo));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z892LogResponsavel_DemandaCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z896LogResponsavel_Owner), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.RTrim( Z1130LogResponsavel_Status));
         isValidOutput.Add(StringUtil.RTrim( Z1234LogResponsavel_NovoStatus));
         isValidOutput.Add(Z1131LogResponsavel_Observacao);
         isValidOutput.Add(Z1148LogResponsavel_UsuarioEhContratante);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z490ContagemResultado_ContratadaCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z52Contratada_AreaTrabalhoCod), 6, 0, ".", "")));
         isValidOutput.Add(Z1149LogResponsavel_OwnerEhContratante);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1219LogResponsavel_OwnerPessoaCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( Z1222LogResponsavel_OwnerPessoaNom));
         isValidOutput.Add(imgBtn_delete2_Enabled);
         isValidOutput.Add(imgBtn_enter2_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Logresponsavel_usuariocod( GXCombobox dynGX_Parm1 )
      {
         dynLogResponsavel_UsuarioCod = dynGX_Parm1;
         A891LogResponsavel_UsuarioCod = (int)(NumberUtil.Val( dynLogResponsavel_UsuarioCod.CurrentValue, "."));
         n891LogResponsavel_UsuarioCod = false;
         /* Using cursor T002T28 */
         pr_default.execute(26, new Object[] {n891LogResponsavel_UsuarioCod, A891LogResponsavel_UsuarioCod});
         if ( (pr_default.getStatus(26) == 101) )
         {
            if ( ! ( (0==A891LogResponsavel_UsuarioCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Log Responsavel_Usuarios'.", "ForeignKeyNotFound", 1, "LOGRESPONSAVEL_USUARIOCOD");
               AnyError = 1;
               GX_FocusControl = dynLogResponsavel_UsuarioCod_Internalname;
            }
         }
         pr_default.close(26);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Logresponsavel_demandacod( int GX_Parm1 ,
                                                   int GX_Parm2 ,
                                                   GXCombobox dynGX_Parm3 ,
                                                   int GX_Parm4 ,
                                                   bool GX_Parm5 )
      {
         A892LogResponsavel_DemandaCod = GX_Parm1;
         n892LogResponsavel_DemandaCod = false;
         A490ContagemResultado_ContratadaCod = GX_Parm2;
         n490ContagemResultado_ContratadaCod = false;
         dynLogResponsavel_UsuarioCod = dynGX_Parm3;
         A891LogResponsavel_UsuarioCod = (int)(NumberUtil.Val( dynLogResponsavel_UsuarioCod.CurrentValue, "."));
         n891LogResponsavel_UsuarioCod = false;
         A52Contratada_AreaTrabalhoCod = GX_Parm4;
         n52Contratada_AreaTrabalhoCod = false;
         A1148LogResponsavel_UsuarioEhContratante = GX_Parm5;
         /* Using cursor T002T29 */
         pr_default.execute(27, new Object[] {n892LogResponsavel_DemandaCod, A892LogResponsavel_DemandaCod});
         if ( (pr_default.getStatus(27) == 101) )
         {
            if ( ! ( (0==A892LogResponsavel_DemandaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Log Responsavel_Contagem Resultado'.", "ForeignKeyNotFound", 1, "LOGRESPONSAVEL_DEMANDACOD");
               AnyError = 1;
               GX_FocusControl = edtLogResponsavel_DemandaCod_Internalname;
            }
         }
         A490ContagemResultado_ContratadaCod = T002T29_A490ContagemResultado_ContratadaCod[0];
         n490ContagemResultado_ContratadaCod = T002T29_n490ContagemResultado_ContratadaCod[0];
         pr_default.close(27);
         /* Using cursor T002T30 */
         pr_default.execute(28, new Object[] {n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod});
         if ( (pr_default.getStatus(28) == 101) )
         {
            if ( ! ( (0==A490ContagemResultado_ContratadaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Contratada'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A52Contratada_AreaTrabalhoCod = T002T30_A52Contratada_AreaTrabalhoCod[0];
         n52Contratada_AreaTrabalhoCod = T002T30_n52Contratada_AreaTrabalhoCod[0];
         pr_default.close(28);
         GXt_boolean1 = A1148LogResponsavel_UsuarioEhContratante;
         new prc_usuarioehcontratante(context ).execute( ref  A892LogResponsavel_DemandaCod,  A891LogResponsavel_UsuarioCod, out  GXt_boolean1) ;
         A1148LogResponsavel_UsuarioEhContratante = GXt_boolean1;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A490ContagemResultado_ContratadaCod = 0;
            n490ContagemResultado_ContratadaCod = false;
            A52Contratada_AreaTrabalhoCod = 0;
            n52Contratada_AreaTrabalhoCod = false;
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0, ".", "")));
         isValidOutput.Add(A1148LogResponsavel_UsuarioEhContratante);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Logresponsavel_owner( int GX_Parm1 ,
                                              int GX_Parm2 ,
                                              int GX_Parm3 ,
                                              String GX_Parm4 ,
                                              bool GX_Parm5 )
      {
         A896LogResponsavel_Owner = GX_Parm1;
         A1219LogResponsavel_OwnerPessoaCod = GX_Parm2;
         n1219LogResponsavel_OwnerPessoaCod = false;
         A892LogResponsavel_DemandaCod = GX_Parm3;
         n892LogResponsavel_DemandaCod = false;
         A1222LogResponsavel_OwnerPessoaNom = GX_Parm4;
         n1222LogResponsavel_OwnerPessoaNom = false;
         A1149LogResponsavel_OwnerEhContratante = GX_Parm5;
         /* Using cursor T002T31 */
         pr_default.execute(29, new Object[] {A896LogResponsavel_Owner});
         if ( (pr_default.getStatus(29) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Log Responsavel_Usuario Owner'.", "ForeignKeyNotFound", 1, "LOGRESPONSAVEL_OWNER");
            AnyError = 1;
            GX_FocusControl = edtLogResponsavel_Owner_Internalname;
         }
         A1219LogResponsavel_OwnerPessoaCod = T002T31_A1219LogResponsavel_OwnerPessoaCod[0];
         n1219LogResponsavel_OwnerPessoaCod = T002T31_n1219LogResponsavel_OwnerPessoaCod[0];
         pr_default.close(29);
         /* Using cursor T002T32 */
         pr_default.execute(30, new Object[] {n1219LogResponsavel_OwnerPessoaCod, A1219LogResponsavel_OwnerPessoaCod});
         if ( (pr_default.getStatus(30) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1222LogResponsavel_OwnerPessoaNom = T002T32_A1222LogResponsavel_OwnerPessoaNom[0];
         n1222LogResponsavel_OwnerPessoaNom = T002T32_n1222LogResponsavel_OwnerPessoaNom[0];
         pr_default.close(30);
         GXt_boolean1 = A1149LogResponsavel_OwnerEhContratante;
         new prc_usuarioehcontratante(context ).execute( ref  A892LogResponsavel_DemandaCod,  A896LogResponsavel_Owner, out  GXt_boolean1) ;
         A1149LogResponsavel_OwnerEhContratante = GXt_boolean1;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1219LogResponsavel_OwnerPessoaCod = 0;
            n1219LogResponsavel_OwnerPessoaCod = false;
            A1222LogResponsavel_OwnerPessoaNom = "";
            n1222LogResponsavel_OwnerPessoaNom = false;
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1219LogResponsavel_OwnerPessoaCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A1222LogResponsavel_OwnerPessoaNom));
         isValidOutput.Add(A1149LogResponsavel_OwnerEhContratante);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(26);
         pr_default.close(29);
         pr_default.close(21);
         pr_default.close(27);
         pr_default.close(19);
         pr_default.close(30);
         pr_default.close(22);
         pr_default.close(28);
         pr_default.close(20);
      }

      public override void initialize( )
      {
         sPrefix = "";
         Z893LogResponsavel_DataHora = (DateTime)(DateTime.MinValue);
         Z1177LogResponsavel_Prazo = (DateTime)(DateTime.MinValue);
         Z894LogResponsavel_Acao = "";
         Z895LogResponsavel_ObjetoTipo = "";
         Z1130LogResponsavel_Status = "";
         Z1234LogResponsavel_NovoStatus = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         A894LogResponsavel_Acao = "";
         A895LogResponsavel_ObjetoTipo = "";
         A1130LogResponsavel_Status = "";
         A1234LogResponsavel_NovoStatus = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         lblTextblocklogresponsavel_codigo_Jsonclick = "";
         lblTextblocklogresponsavel_datahora_Jsonclick = "";
         A893LogResponsavel_DataHora = (DateTime)(DateTime.MinValue);
         lblTextblocklogresponsavel_prazo_Jsonclick = "";
         A1177LogResponsavel_Prazo = (DateTime)(DateTime.MinValue);
         lblTextblocklogresponsavel_acao_Jsonclick = "";
         lblTextblocklogresponsavel_usuariocod_Jsonclick = "";
         lblTextblocklogresponsavel_usuarioehcontratante_Jsonclick = "";
         lblTextblocklogresponsavel_objetotipo_Jsonclick = "";
         lblTextblocklogresponsavel_demandacod_Jsonclick = "";
         lblTextblockcontratada_areatrabalhocod_Jsonclick = "";
         lblTextblocklogresponsavel_owner_Jsonclick = "";
         lblTextblocklogresponsavel_ownerpessoacod_Jsonclick = "";
         lblTextblocklogresponsavel_ownerpessoanom_Jsonclick = "";
         A1222LogResponsavel_OwnerPessoaNom = "";
         lblTextblocklogresponsavel_ownerehcontratante_Jsonclick = "";
         lblTextblocklogresponsavel_status_Jsonclick = "";
         lblTextblocklogresponsavel_novostatus_Jsonclick = "";
         lblTextblocklogresponsavel_observacao_Jsonclick = "";
         A1131LogResponsavel_Observacao = "";
         imgBtn_first_Jsonclick = "";
         imgBtn_first_separator_Jsonclick = "";
         imgBtn_previous_Jsonclick = "";
         imgBtn_previous_separator_Jsonclick = "";
         imgBtn_next_Jsonclick = "";
         imgBtn_next_separator_Jsonclick = "";
         imgBtn_last_Jsonclick = "";
         imgBtn_last_separator_Jsonclick = "";
         imgBtn_select_Jsonclick = "";
         imgBtn_select_separator_Jsonclick = "";
         imgBtn_enter2_Jsonclick = "";
         imgBtn_enter2_separator_Jsonclick = "";
         imgBtn_cancel2_Jsonclick = "";
         imgBtn_cancel2_separator_Jsonclick = "";
         imgBtn_delete2_Jsonclick = "";
         imgBtn_delete2_separator_Jsonclick = "";
         Gx_mode = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         Z1131LogResponsavel_Observacao = "";
         Z1222LogResponsavel_OwnerPessoaNom = "";
         T002T9_A490ContagemResultado_ContratadaCod = new int[1] ;
         T002T9_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         T002T9_A1797LogResponsavel_Codigo = new long[1] ;
         T002T9_A893LogResponsavel_DataHora = new DateTime[] {DateTime.MinValue} ;
         T002T9_A1177LogResponsavel_Prazo = new DateTime[] {DateTime.MinValue} ;
         T002T9_n1177LogResponsavel_Prazo = new bool[] {false} ;
         T002T9_A894LogResponsavel_Acao = new String[] {""} ;
         T002T9_A895LogResponsavel_ObjetoTipo = new String[] {""} ;
         T002T9_A1222LogResponsavel_OwnerPessoaNom = new String[] {""} ;
         T002T9_n1222LogResponsavel_OwnerPessoaNom = new bool[] {false} ;
         T002T9_A1130LogResponsavel_Status = new String[] {""} ;
         T002T9_n1130LogResponsavel_Status = new bool[] {false} ;
         T002T9_A1234LogResponsavel_NovoStatus = new String[] {""} ;
         T002T9_n1234LogResponsavel_NovoStatus = new bool[] {false} ;
         T002T9_A1131LogResponsavel_Observacao = new String[] {""} ;
         T002T9_n1131LogResponsavel_Observacao = new bool[] {false} ;
         T002T9_A891LogResponsavel_UsuarioCod = new int[1] ;
         T002T9_n891LogResponsavel_UsuarioCod = new bool[] {false} ;
         T002T9_A896LogResponsavel_Owner = new int[1] ;
         T002T9_A892LogResponsavel_DemandaCod = new int[1] ;
         T002T9_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         T002T9_A1219LogResponsavel_OwnerPessoaCod = new int[1] ;
         T002T9_n1219LogResponsavel_OwnerPessoaCod = new bool[] {false} ;
         T002T9_A52Contratada_AreaTrabalhoCod = new int[1] ;
         T002T9_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         T002T4_A891LogResponsavel_UsuarioCod = new int[1] ;
         T002T4_n891LogResponsavel_UsuarioCod = new bool[] {false} ;
         T002T6_A490ContagemResultado_ContratadaCod = new int[1] ;
         T002T6_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         T002T8_A52Contratada_AreaTrabalhoCod = new int[1] ;
         T002T8_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         T002T5_A1219LogResponsavel_OwnerPessoaCod = new int[1] ;
         T002T5_n1219LogResponsavel_OwnerPessoaCod = new bool[] {false} ;
         T002T7_A1222LogResponsavel_OwnerPessoaNom = new String[] {""} ;
         T002T7_n1222LogResponsavel_OwnerPessoaNom = new bool[] {false} ;
         T002T10_A891LogResponsavel_UsuarioCod = new int[1] ;
         T002T10_n891LogResponsavel_UsuarioCod = new bool[] {false} ;
         T002T11_A490ContagemResultado_ContratadaCod = new int[1] ;
         T002T11_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         T002T12_A52Contratada_AreaTrabalhoCod = new int[1] ;
         T002T12_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         T002T13_A1219LogResponsavel_OwnerPessoaCod = new int[1] ;
         T002T13_n1219LogResponsavel_OwnerPessoaCod = new bool[] {false} ;
         T002T14_A1222LogResponsavel_OwnerPessoaNom = new String[] {""} ;
         T002T14_n1222LogResponsavel_OwnerPessoaNom = new bool[] {false} ;
         T002T15_A1797LogResponsavel_Codigo = new long[1] ;
         T002T3_A1797LogResponsavel_Codigo = new long[1] ;
         T002T3_A893LogResponsavel_DataHora = new DateTime[] {DateTime.MinValue} ;
         T002T3_A1177LogResponsavel_Prazo = new DateTime[] {DateTime.MinValue} ;
         T002T3_n1177LogResponsavel_Prazo = new bool[] {false} ;
         T002T3_A894LogResponsavel_Acao = new String[] {""} ;
         T002T3_A895LogResponsavel_ObjetoTipo = new String[] {""} ;
         T002T3_A1130LogResponsavel_Status = new String[] {""} ;
         T002T3_n1130LogResponsavel_Status = new bool[] {false} ;
         T002T3_A1234LogResponsavel_NovoStatus = new String[] {""} ;
         T002T3_n1234LogResponsavel_NovoStatus = new bool[] {false} ;
         T002T3_A1131LogResponsavel_Observacao = new String[] {""} ;
         T002T3_n1131LogResponsavel_Observacao = new bool[] {false} ;
         T002T3_A891LogResponsavel_UsuarioCod = new int[1] ;
         T002T3_n891LogResponsavel_UsuarioCod = new bool[] {false} ;
         T002T3_A896LogResponsavel_Owner = new int[1] ;
         T002T3_A892LogResponsavel_DemandaCod = new int[1] ;
         T002T3_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         sMode198 = "";
         T002T16_A1797LogResponsavel_Codigo = new long[1] ;
         T002T17_A1797LogResponsavel_Codigo = new long[1] ;
         T002T2_A1797LogResponsavel_Codigo = new long[1] ;
         T002T2_A893LogResponsavel_DataHora = new DateTime[] {DateTime.MinValue} ;
         T002T2_A1177LogResponsavel_Prazo = new DateTime[] {DateTime.MinValue} ;
         T002T2_n1177LogResponsavel_Prazo = new bool[] {false} ;
         T002T2_A894LogResponsavel_Acao = new String[] {""} ;
         T002T2_A895LogResponsavel_ObjetoTipo = new String[] {""} ;
         T002T2_A1130LogResponsavel_Status = new String[] {""} ;
         T002T2_n1130LogResponsavel_Status = new bool[] {false} ;
         T002T2_A1234LogResponsavel_NovoStatus = new String[] {""} ;
         T002T2_n1234LogResponsavel_NovoStatus = new bool[] {false} ;
         T002T2_A1131LogResponsavel_Observacao = new String[] {""} ;
         T002T2_n1131LogResponsavel_Observacao = new bool[] {false} ;
         T002T2_A891LogResponsavel_UsuarioCod = new int[1] ;
         T002T2_n891LogResponsavel_UsuarioCod = new bool[] {false} ;
         T002T2_A896LogResponsavel_Owner = new int[1] ;
         T002T2_A892LogResponsavel_DemandaCod = new int[1] ;
         T002T2_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         T002T18_A1797LogResponsavel_Codigo = new long[1] ;
         T002T21_A490ContagemResultado_ContratadaCod = new int[1] ;
         T002T21_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         T002T22_A52Contratada_AreaTrabalhoCod = new int[1] ;
         T002T22_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         T002T23_A1219LogResponsavel_OwnerPessoaCod = new int[1] ;
         T002T23_n1219LogResponsavel_OwnerPessoaCod = new bool[] {false} ;
         T002T24_A1222LogResponsavel_OwnerPessoaNom = new String[] {""} ;
         T002T24_n1222LogResponsavel_OwnerPessoaNom = new bool[] {false} ;
         T002T25_A2024ContagemResultadoNaoCnf_Codigo = new int[1] ;
         T002T26_A1797LogResponsavel_Codigo = new long[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T002T27_A57Usuario_PessoaCod = new int[1] ;
         T002T27_A1Usuario_Codigo = new int[1] ;
         T002T27_A58Usuario_PessoaNom = new String[] {""} ;
         T002T27_n58Usuario_PessoaNom = new bool[] {false} ;
         isValidOutput = new GxUnknownObjectCollection();
         T002T28_A891LogResponsavel_UsuarioCod = new int[1] ;
         T002T28_n891LogResponsavel_UsuarioCod = new bool[] {false} ;
         T002T29_A490ContagemResultado_ContratadaCod = new int[1] ;
         T002T29_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         T002T30_A52Contratada_AreaTrabalhoCod = new int[1] ;
         T002T30_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         T002T31_A1219LogResponsavel_OwnerPessoaCod = new int[1] ;
         T002T31_n1219LogResponsavel_OwnerPessoaCod = new bool[] {false} ;
         T002T32_A1222LogResponsavel_OwnerPessoaNom = new String[] {""} ;
         T002T32_n1222LogResponsavel_OwnerPessoaNom = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.logresponsavel__default(),
            new Object[][] {
                new Object[] {
               T002T2_A1797LogResponsavel_Codigo, T002T2_A893LogResponsavel_DataHora, T002T2_A1177LogResponsavel_Prazo, T002T2_n1177LogResponsavel_Prazo, T002T2_A894LogResponsavel_Acao, T002T2_A895LogResponsavel_ObjetoTipo, T002T2_A1130LogResponsavel_Status, T002T2_n1130LogResponsavel_Status, T002T2_A1234LogResponsavel_NovoStatus, T002T2_n1234LogResponsavel_NovoStatus,
               T002T2_A1131LogResponsavel_Observacao, T002T2_n1131LogResponsavel_Observacao, T002T2_A891LogResponsavel_UsuarioCod, T002T2_n891LogResponsavel_UsuarioCod, T002T2_A896LogResponsavel_Owner, T002T2_A892LogResponsavel_DemandaCod, T002T2_n892LogResponsavel_DemandaCod
               }
               , new Object[] {
               T002T3_A1797LogResponsavel_Codigo, T002T3_A893LogResponsavel_DataHora, T002T3_A1177LogResponsavel_Prazo, T002T3_n1177LogResponsavel_Prazo, T002T3_A894LogResponsavel_Acao, T002T3_A895LogResponsavel_ObjetoTipo, T002T3_A1130LogResponsavel_Status, T002T3_n1130LogResponsavel_Status, T002T3_A1234LogResponsavel_NovoStatus, T002T3_n1234LogResponsavel_NovoStatus,
               T002T3_A1131LogResponsavel_Observacao, T002T3_n1131LogResponsavel_Observacao, T002T3_A891LogResponsavel_UsuarioCod, T002T3_n891LogResponsavel_UsuarioCod, T002T3_A896LogResponsavel_Owner, T002T3_A892LogResponsavel_DemandaCod, T002T3_n892LogResponsavel_DemandaCod
               }
               , new Object[] {
               T002T4_A891LogResponsavel_UsuarioCod
               }
               , new Object[] {
               T002T5_A1219LogResponsavel_OwnerPessoaCod, T002T5_n1219LogResponsavel_OwnerPessoaCod
               }
               , new Object[] {
               T002T6_A490ContagemResultado_ContratadaCod, T002T6_n490ContagemResultado_ContratadaCod
               }
               , new Object[] {
               T002T7_A1222LogResponsavel_OwnerPessoaNom, T002T7_n1222LogResponsavel_OwnerPessoaNom
               }
               , new Object[] {
               T002T8_A52Contratada_AreaTrabalhoCod, T002T8_n52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               T002T9_A490ContagemResultado_ContratadaCod, T002T9_n490ContagemResultado_ContratadaCod, T002T9_A1797LogResponsavel_Codigo, T002T9_A893LogResponsavel_DataHora, T002T9_A1177LogResponsavel_Prazo, T002T9_n1177LogResponsavel_Prazo, T002T9_A894LogResponsavel_Acao, T002T9_A895LogResponsavel_ObjetoTipo, T002T9_A1222LogResponsavel_OwnerPessoaNom, T002T9_n1222LogResponsavel_OwnerPessoaNom,
               T002T9_A1130LogResponsavel_Status, T002T9_n1130LogResponsavel_Status, T002T9_A1234LogResponsavel_NovoStatus, T002T9_n1234LogResponsavel_NovoStatus, T002T9_A1131LogResponsavel_Observacao, T002T9_n1131LogResponsavel_Observacao, T002T9_A891LogResponsavel_UsuarioCod, T002T9_n891LogResponsavel_UsuarioCod, T002T9_A896LogResponsavel_Owner, T002T9_A892LogResponsavel_DemandaCod,
               T002T9_n892LogResponsavel_DemandaCod, T002T9_A1219LogResponsavel_OwnerPessoaCod, T002T9_n1219LogResponsavel_OwnerPessoaCod, T002T9_A52Contratada_AreaTrabalhoCod, T002T9_n52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               T002T10_A891LogResponsavel_UsuarioCod
               }
               , new Object[] {
               T002T11_A490ContagemResultado_ContratadaCod, T002T11_n490ContagemResultado_ContratadaCod
               }
               , new Object[] {
               T002T12_A52Contratada_AreaTrabalhoCod, T002T12_n52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               T002T13_A1219LogResponsavel_OwnerPessoaCod, T002T13_n1219LogResponsavel_OwnerPessoaCod
               }
               , new Object[] {
               T002T14_A1222LogResponsavel_OwnerPessoaNom, T002T14_n1222LogResponsavel_OwnerPessoaNom
               }
               , new Object[] {
               T002T15_A1797LogResponsavel_Codigo
               }
               , new Object[] {
               T002T16_A1797LogResponsavel_Codigo
               }
               , new Object[] {
               T002T17_A1797LogResponsavel_Codigo
               }
               , new Object[] {
               T002T18_A1797LogResponsavel_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T002T21_A490ContagemResultado_ContratadaCod, T002T21_n490ContagemResultado_ContratadaCod
               }
               , new Object[] {
               T002T22_A52Contratada_AreaTrabalhoCod, T002T22_n52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               T002T23_A1219LogResponsavel_OwnerPessoaCod, T002T23_n1219LogResponsavel_OwnerPessoaCod
               }
               , new Object[] {
               T002T24_A1222LogResponsavel_OwnerPessoaNom, T002T24_n1222LogResponsavel_OwnerPessoaNom
               }
               , new Object[] {
               T002T25_A2024ContagemResultadoNaoCnf_Codigo
               }
               , new Object[] {
               T002T26_A1797LogResponsavel_Codigo
               }
               , new Object[] {
               T002T27_A57Usuario_PessoaCod, T002T27_A1Usuario_Codigo, T002T27_A58Usuario_PessoaNom, T002T27_n58Usuario_PessoaNom
               }
               , new Object[] {
               T002T28_A891LogResponsavel_UsuarioCod
               }
               , new Object[] {
               T002T29_A490ContagemResultado_ContratadaCod, T002T29_n490ContagemResultado_ContratadaCod
               }
               , new Object[] {
               T002T30_A52Contratada_AreaTrabalhoCod, T002T30_n52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               T002T31_A1219LogResponsavel_OwnerPessoaCod, T002T31_n1219LogResponsavel_OwnerPessoaCod
               }
               , new Object[] {
               T002T32_A1222LogResponsavel_OwnerPessoaNom, T002T32_n1222LogResponsavel_OwnerPessoaNom
               }
            }
         );
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short GX_JID ;
      private short RcdFound198 ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z891LogResponsavel_UsuarioCod ;
      private int Z896LogResponsavel_Owner ;
      private int Z892LogResponsavel_DemandaCod ;
      private int A892LogResponsavel_DemandaCod ;
      private int A891LogResponsavel_UsuarioCod ;
      private int A896LogResponsavel_Owner ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A1219LogResponsavel_OwnerPessoaCod ;
      private int trnEnded ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int edtLogResponsavel_Codigo_Enabled ;
      private int edtLogResponsavel_DataHora_Enabled ;
      private int edtLogResponsavel_Prazo_Enabled ;
      private int edtLogResponsavel_DemandaCod_Enabled ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int edtContratada_AreaTrabalhoCod_Enabled ;
      private int edtLogResponsavel_Owner_Enabled ;
      private int edtLogResponsavel_OwnerPessoaCod_Enabled ;
      private int edtLogResponsavel_OwnerPessoaNom_Enabled ;
      private int edtLogResponsavel_Observacao_Enabled ;
      private int imgBtn_first_Visible ;
      private int imgBtn_first_separator_Visible ;
      private int imgBtn_previous_Visible ;
      private int imgBtn_previous_separator_Visible ;
      private int imgBtn_next_Visible ;
      private int imgBtn_next_separator_Visible ;
      private int imgBtn_last_Visible ;
      private int imgBtn_last_separator_Visible ;
      private int imgBtn_select_Visible ;
      private int imgBtn_select_separator_Visible ;
      private int imgBtn_enter2_Visible ;
      private int imgBtn_enter2_Enabled ;
      private int imgBtn_enter2_separator_Visible ;
      private int imgBtn_cancel2_Visible ;
      private int imgBtn_cancel2_separator_Visible ;
      private int imgBtn_delete2_Visible ;
      private int imgBtn_delete2_Enabled ;
      private int imgBtn_delete2_separator_Visible ;
      private int Z490ContagemResultado_ContratadaCod ;
      private int Z52Contratada_AreaTrabalhoCod ;
      private int Z1219LogResponsavel_OwnerPessoaCod ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private long Z1797LogResponsavel_Codigo ;
      private long A1797LogResponsavel_Codigo ;
      private String sPrefix ;
      private String Z894LogResponsavel_Acao ;
      private String Z895LogResponsavel_ObjetoTipo ;
      private String Z1130LogResponsavel_Status ;
      private String Z1234LogResponsavel_NovoStatus ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String A894LogResponsavel_Acao ;
      private String chkLogResponsavel_UsuarioEhContratante_Internalname ;
      private String A895LogResponsavel_ObjetoTipo ;
      private String chkLogResponsavel_OwnerEhContratante_Internalname ;
      private String A1130LogResponsavel_Status ;
      private String A1234LogResponsavel_NovoStatus ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtLogResponsavel_Codigo_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String grpGroupdata_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblocklogresponsavel_codigo_Internalname ;
      private String lblTextblocklogresponsavel_codigo_Jsonclick ;
      private String edtLogResponsavel_Codigo_Jsonclick ;
      private String lblTextblocklogresponsavel_datahora_Internalname ;
      private String lblTextblocklogresponsavel_datahora_Jsonclick ;
      private String edtLogResponsavel_DataHora_Internalname ;
      private String edtLogResponsavel_DataHora_Jsonclick ;
      private String lblTextblocklogresponsavel_prazo_Internalname ;
      private String lblTextblocklogresponsavel_prazo_Jsonclick ;
      private String edtLogResponsavel_Prazo_Internalname ;
      private String edtLogResponsavel_Prazo_Jsonclick ;
      private String lblTextblocklogresponsavel_acao_Internalname ;
      private String lblTextblocklogresponsavel_acao_Jsonclick ;
      private String cmbLogResponsavel_Acao_Internalname ;
      private String cmbLogResponsavel_Acao_Jsonclick ;
      private String lblTextblocklogresponsavel_usuariocod_Internalname ;
      private String lblTextblocklogresponsavel_usuariocod_Jsonclick ;
      private String dynLogResponsavel_UsuarioCod_Internalname ;
      private String dynLogResponsavel_UsuarioCod_Jsonclick ;
      private String lblTextblocklogresponsavel_usuarioehcontratante_Internalname ;
      private String lblTextblocklogresponsavel_usuarioehcontratante_Jsonclick ;
      private String lblTextblocklogresponsavel_objetotipo_Internalname ;
      private String lblTextblocklogresponsavel_objetotipo_Jsonclick ;
      private String cmbLogResponsavel_ObjetoTipo_Internalname ;
      private String cmbLogResponsavel_ObjetoTipo_Jsonclick ;
      private String lblTextblocklogresponsavel_demandacod_Internalname ;
      private String lblTextblocklogresponsavel_demandacod_Jsonclick ;
      private String edtLogResponsavel_DemandaCod_Internalname ;
      private String edtLogResponsavel_DemandaCod_Jsonclick ;
      private String lblTextblockcontratada_areatrabalhocod_Internalname ;
      private String lblTextblockcontratada_areatrabalhocod_Jsonclick ;
      private String edtContratada_AreaTrabalhoCod_Internalname ;
      private String edtContratada_AreaTrabalhoCod_Jsonclick ;
      private String lblTextblocklogresponsavel_owner_Internalname ;
      private String lblTextblocklogresponsavel_owner_Jsonclick ;
      private String edtLogResponsavel_Owner_Internalname ;
      private String edtLogResponsavel_Owner_Jsonclick ;
      private String lblTextblocklogresponsavel_ownerpessoacod_Internalname ;
      private String lblTextblocklogresponsavel_ownerpessoacod_Jsonclick ;
      private String edtLogResponsavel_OwnerPessoaCod_Internalname ;
      private String edtLogResponsavel_OwnerPessoaCod_Jsonclick ;
      private String lblTextblocklogresponsavel_ownerpessoanom_Internalname ;
      private String lblTextblocklogresponsavel_ownerpessoanom_Jsonclick ;
      private String edtLogResponsavel_OwnerPessoaNom_Internalname ;
      private String A1222LogResponsavel_OwnerPessoaNom ;
      private String edtLogResponsavel_OwnerPessoaNom_Jsonclick ;
      private String lblTextblocklogresponsavel_ownerehcontratante_Internalname ;
      private String lblTextblocklogresponsavel_ownerehcontratante_Jsonclick ;
      private String lblTextblocklogresponsavel_status_Internalname ;
      private String lblTextblocklogresponsavel_status_Jsonclick ;
      private String cmbLogResponsavel_Status_Internalname ;
      private String cmbLogResponsavel_Status_Jsonclick ;
      private String lblTextblocklogresponsavel_novostatus_Internalname ;
      private String lblTextblocklogresponsavel_novostatus_Jsonclick ;
      private String cmbLogResponsavel_NovoStatus_Internalname ;
      private String cmbLogResponsavel_NovoStatus_Jsonclick ;
      private String lblTextblocklogresponsavel_observacao_Internalname ;
      private String lblTextblocklogresponsavel_observacao_Jsonclick ;
      private String edtLogResponsavel_Observacao_Internalname ;
      private String tblTabletoolbar_Internalname ;
      private String divSectiontoolbar_Internalname ;
      private String imgBtn_first_Internalname ;
      private String imgBtn_first_Jsonclick ;
      private String imgBtn_first_separator_Internalname ;
      private String imgBtn_first_separator_Jsonclick ;
      private String imgBtn_previous_Internalname ;
      private String imgBtn_previous_Jsonclick ;
      private String imgBtn_previous_separator_Internalname ;
      private String imgBtn_previous_separator_Jsonclick ;
      private String imgBtn_next_Internalname ;
      private String imgBtn_next_Jsonclick ;
      private String imgBtn_next_separator_Internalname ;
      private String imgBtn_next_separator_Jsonclick ;
      private String imgBtn_last_Internalname ;
      private String imgBtn_last_Jsonclick ;
      private String imgBtn_last_separator_Internalname ;
      private String imgBtn_last_separator_Jsonclick ;
      private String imgBtn_select_Internalname ;
      private String imgBtn_select_Jsonclick ;
      private String imgBtn_select_separator_Internalname ;
      private String imgBtn_select_separator_Jsonclick ;
      private String imgBtn_enter2_Internalname ;
      private String imgBtn_enter2_Jsonclick ;
      private String imgBtn_enter2_separator_Internalname ;
      private String imgBtn_enter2_separator_Jsonclick ;
      private String imgBtn_cancel2_Internalname ;
      private String imgBtn_cancel2_Jsonclick ;
      private String imgBtn_cancel2_separator_Internalname ;
      private String imgBtn_cancel2_separator_Jsonclick ;
      private String imgBtn_delete2_Internalname ;
      private String imgBtn_delete2_Jsonclick ;
      private String imgBtn_delete2_separator_Internalname ;
      private String imgBtn_delete2_separator_Jsonclick ;
      private String Gx_mode ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z1222LogResponsavel_OwnerPessoaNom ;
      private String sMode198 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String gxwrpcisep ;
      private DateTime Z893LogResponsavel_DataHora ;
      private DateTime Z1177LogResponsavel_Prazo ;
      private DateTime A893LogResponsavel_DataHora ;
      private DateTime A1177LogResponsavel_Prazo ;
      private bool entryPointCalled ;
      private bool n892LogResponsavel_DemandaCod ;
      private bool n891LogResponsavel_UsuarioCod ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n1219LogResponsavel_OwnerPessoaCod ;
      private bool toggleJsOutput ;
      private bool n1130LogResponsavel_Status ;
      private bool n1234LogResponsavel_NovoStatus ;
      private bool wbErr ;
      private bool A1148LogResponsavel_UsuarioEhContratante ;
      private bool A1149LogResponsavel_OwnerEhContratante ;
      private bool n1177LogResponsavel_Prazo ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n1222LogResponsavel_OwnerPessoaNom ;
      private bool n1131LogResponsavel_Observacao ;
      private bool Gx_longc ;
      private bool Z1148LogResponsavel_UsuarioEhContratante ;
      private bool Z1149LogResponsavel_OwnerEhContratante ;
      private bool GXt_boolean1 ;
      private String A1131LogResponsavel_Observacao ;
      private String Z1131LogResponsavel_Observacao ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbLogResponsavel_Acao ;
      private GXCombobox dynLogResponsavel_UsuarioCod ;
      private GXCheckbox chkLogResponsavel_UsuarioEhContratante ;
      private GXCombobox cmbLogResponsavel_ObjetoTipo ;
      private GXCheckbox chkLogResponsavel_OwnerEhContratante ;
      private GXCombobox cmbLogResponsavel_Status ;
      private GXCombobox cmbLogResponsavel_NovoStatus ;
      private IDataStoreProvider pr_default ;
      private int[] T002T9_A490ContagemResultado_ContratadaCod ;
      private bool[] T002T9_n490ContagemResultado_ContratadaCod ;
      private long[] T002T9_A1797LogResponsavel_Codigo ;
      private DateTime[] T002T9_A893LogResponsavel_DataHora ;
      private DateTime[] T002T9_A1177LogResponsavel_Prazo ;
      private bool[] T002T9_n1177LogResponsavel_Prazo ;
      private String[] T002T9_A894LogResponsavel_Acao ;
      private String[] T002T9_A895LogResponsavel_ObjetoTipo ;
      private String[] T002T9_A1222LogResponsavel_OwnerPessoaNom ;
      private bool[] T002T9_n1222LogResponsavel_OwnerPessoaNom ;
      private String[] T002T9_A1130LogResponsavel_Status ;
      private bool[] T002T9_n1130LogResponsavel_Status ;
      private String[] T002T9_A1234LogResponsavel_NovoStatus ;
      private bool[] T002T9_n1234LogResponsavel_NovoStatus ;
      private String[] T002T9_A1131LogResponsavel_Observacao ;
      private bool[] T002T9_n1131LogResponsavel_Observacao ;
      private int[] T002T9_A891LogResponsavel_UsuarioCod ;
      private bool[] T002T9_n891LogResponsavel_UsuarioCod ;
      private int[] T002T9_A896LogResponsavel_Owner ;
      private int[] T002T9_A892LogResponsavel_DemandaCod ;
      private bool[] T002T9_n892LogResponsavel_DemandaCod ;
      private int[] T002T9_A1219LogResponsavel_OwnerPessoaCod ;
      private bool[] T002T9_n1219LogResponsavel_OwnerPessoaCod ;
      private int[] T002T9_A52Contratada_AreaTrabalhoCod ;
      private bool[] T002T9_n52Contratada_AreaTrabalhoCod ;
      private int[] T002T4_A891LogResponsavel_UsuarioCod ;
      private bool[] T002T4_n891LogResponsavel_UsuarioCod ;
      private int[] T002T6_A490ContagemResultado_ContratadaCod ;
      private bool[] T002T6_n490ContagemResultado_ContratadaCod ;
      private int[] T002T8_A52Contratada_AreaTrabalhoCod ;
      private bool[] T002T8_n52Contratada_AreaTrabalhoCod ;
      private int[] T002T5_A1219LogResponsavel_OwnerPessoaCod ;
      private bool[] T002T5_n1219LogResponsavel_OwnerPessoaCod ;
      private String[] T002T7_A1222LogResponsavel_OwnerPessoaNom ;
      private bool[] T002T7_n1222LogResponsavel_OwnerPessoaNom ;
      private int[] T002T10_A891LogResponsavel_UsuarioCod ;
      private bool[] T002T10_n891LogResponsavel_UsuarioCod ;
      private int[] T002T11_A490ContagemResultado_ContratadaCod ;
      private bool[] T002T11_n490ContagemResultado_ContratadaCod ;
      private int[] T002T12_A52Contratada_AreaTrabalhoCod ;
      private bool[] T002T12_n52Contratada_AreaTrabalhoCod ;
      private int[] T002T13_A1219LogResponsavel_OwnerPessoaCod ;
      private bool[] T002T13_n1219LogResponsavel_OwnerPessoaCod ;
      private String[] T002T14_A1222LogResponsavel_OwnerPessoaNom ;
      private bool[] T002T14_n1222LogResponsavel_OwnerPessoaNom ;
      private long[] T002T15_A1797LogResponsavel_Codigo ;
      private long[] T002T3_A1797LogResponsavel_Codigo ;
      private DateTime[] T002T3_A893LogResponsavel_DataHora ;
      private DateTime[] T002T3_A1177LogResponsavel_Prazo ;
      private bool[] T002T3_n1177LogResponsavel_Prazo ;
      private String[] T002T3_A894LogResponsavel_Acao ;
      private String[] T002T3_A895LogResponsavel_ObjetoTipo ;
      private String[] T002T3_A1130LogResponsavel_Status ;
      private bool[] T002T3_n1130LogResponsavel_Status ;
      private String[] T002T3_A1234LogResponsavel_NovoStatus ;
      private bool[] T002T3_n1234LogResponsavel_NovoStatus ;
      private String[] T002T3_A1131LogResponsavel_Observacao ;
      private bool[] T002T3_n1131LogResponsavel_Observacao ;
      private int[] T002T3_A891LogResponsavel_UsuarioCod ;
      private bool[] T002T3_n891LogResponsavel_UsuarioCod ;
      private int[] T002T3_A896LogResponsavel_Owner ;
      private int[] T002T3_A892LogResponsavel_DemandaCod ;
      private bool[] T002T3_n892LogResponsavel_DemandaCod ;
      private long[] T002T16_A1797LogResponsavel_Codigo ;
      private long[] T002T17_A1797LogResponsavel_Codigo ;
      private long[] T002T2_A1797LogResponsavel_Codigo ;
      private DateTime[] T002T2_A893LogResponsavel_DataHora ;
      private DateTime[] T002T2_A1177LogResponsavel_Prazo ;
      private bool[] T002T2_n1177LogResponsavel_Prazo ;
      private String[] T002T2_A894LogResponsavel_Acao ;
      private String[] T002T2_A895LogResponsavel_ObjetoTipo ;
      private String[] T002T2_A1130LogResponsavel_Status ;
      private bool[] T002T2_n1130LogResponsavel_Status ;
      private String[] T002T2_A1234LogResponsavel_NovoStatus ;
      private bool[] T002T2_n1234LogResponsavel_NovoStatus ;
      private String[] T002T2_A1131LogResponsavel_Observacao ;
      private bool[] T002T2_n1131LogResponsavel_Observacao ;
      private int[] T002T2_A891LogResponsavel_UsuarioCod ;
      private bool[] T002T2_n891LogResponsavel_UsuarioCod ;
      private int[] T002T2_A896LogResponsavel_Owner ;
      private int[] T002T2_A892LogResponsavel_DemandaCod ;
      private bool[] T002T2_n892LogResponsavel_DemandaCod ;
      private long[] T002T18_A1797LogResponsavel_Codigo ;
      private int[] T002T21_A490ContagemResultado_ContratadaCod ;
      private bool[] T002T21_n490ContagemResultado_ContratadaCod ;
      private int[] T002T22_A52Contratada_AreaTrabalhoCod ;
      private bool[] T002T22_n52Contratada_AreaTrabalhoCod ;
      private int[] T002T23_A1219LogResponsavel_OwnerPessoaCod ;
      private bool[] T002T23_n1219LogResponsavel_OwnerPessoaCod ;
      private String[] T002T24_A1222LogResponsavel_OwnerPessoaNom ;
      private bool[] T002T24_n1222LogResponsavel_OwnerPessoaNom ;
      private int[] T002T25_A2024ContagemResultadoNaoCnf_Codigo ;
      private long[] T002T26_A1797LogResponsavel_Codigo ;
      private int[] T002T27_A57Usuario_PessoaCod ;
      private int[] T002T27_A1Usuario_Codigo ;
      private String[] T002T27_A58Usuario_PessoaNom ;
      private bool[] T002T27_n58Usuario_PessoaNom ;
      private int[] T002T28_A891LogResponsavel_UsuarioCod ;
      private bool[] T002T28_n891LogResponsavel_UsuarioCod ;
      private int[] T002T29_A490ContagemResultado_ContratadaCod ;
      private bool[] T002T29_n490ContagemResultado_ContratadaCod ;
      private int[] T002T30_A52Contratada_AreaTrabalhoCod ;
      private bool[] T002T30_n52Contratada_AreaTrabalhoCod ;
      private int[] T002T31_A1219LogResponsavel_OwnerPessoaCod ;
      private bool[] T002T31_n1219LogResponsavel_OwnerPessoaCod ;
      private String[] T002T32_A1222LogResponsavel_OwnerPessoaNom ;
      private bool[] T002T32_n1222LogResponsavel_OwnerPessoaNom ;
      private GXWebForm Form ;
   }

   public class logresponsavel__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new UpdateCursor(def[17])
         ,new UpdateCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
         ,new ForEachCursor(def[25])
         ,new ForEachCursor(def[26])
         ,new ForEachCursor(def[27])
         ,new ForEachCursor(def[28])
         ,new ForEachCursor(def[29])
         ,new ForEachCursor(def[30])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT002T9 ;
          prmT002T9 = new Object[] {
          new Object[] {"@LogResponsavel_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmT002T4 ;
          prmT002T4 = new Object[] {
          new Object[] {"@LogResponsavel_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002T6 ;
          prmT002T6 = new Object[] {
          new Object[] {"@LogResponsavel_DemandaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002T8 ;
          prmT002T8 = new Object[] {
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002T5 ;
          prmT002T5 = new Object[] {
          new Object[] {"@LogResponsavel_Owner",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002T7 ;
          prmT002T7 = new Object[] {
          new Object[] {"@LogResponsavel_OwnerPessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002T10 ;
          prmT002T10 = new Object[] {
          new Object[] {"@LogResponsavel_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002T11 ;
          prmT002T11 = new Object[] {
          new Object[] {"@LogResponsavel_DemandaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002T12 ;
          prmT002T12 = new Object[] {
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002T13 ;
          prmT002T13 = new Object[] {
          new Object[] {"@LogResponsavel_Owner",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002T14 ;
          prmT002T14 = new Object[] {
          new Object[] {"@LogResponsavel_OwnerPessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002T15 ;
          prmT002T15 = new Object[] {
          new Object[] {"@LogResponsavel_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmT002T3 ;
          prmT002T3 = new Object[] {
          new Object[] {"@LogResponsavel_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmT002T16 ;
          prmT002T16 = new Object[] {
          new Object[] {"@LogResponsavel_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmT002T17 ;
          prmT002T17 = new Object[] {
          new Object[] {"@LogResponsavel_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmT002T2 ;
          prmT002T2 = new Object[] {
          new Object[] {"@LogResponsavel_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmT002T18 ;
          prmT002T18 = new Object[] {
          new Object[] {"@LogResponsavel_DataHora",SqlDbType.DateTime,8,5} ,
          new Object[] {"@LogResponsavel_Prazo",SqlDbType.DateTime,8,5} ,
          new Object[] {"@LogResponsavel_Acao",SqlDbType.Char,20,0} ,
          new Object[] {"@LogResponsavel_ObjetoTipo",SqlDbType.Char,1,0} ,
          new Object[] {"@LogResponsavel_Status",SqlDbType.Char,1,0} ,
          new Object[] {"@LogResponsavel_NovoStatus",SqlDbType.Char,1,0} ,
          new Object[] {"@LogResponsavel_Observacao",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@LogResponsavel_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@LogResponsavel_Owner",SqlDbType.Int,6,0} ,
          new Object[] {"@LogResponsavel_DemandaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002T19 ;
          prmT002T19 = new Object[] {
          new Object[] {"@LogResponsavel_DataHora",SqlDbType.DateTime,8,5} ,
          new Object[] {"@LogResponsavel_Prazo",SqlDbType.DateTime,8,5} ,
          new Object[] {"@LogResponsavel_Acao",SqlDbType.Char,20,0} ,
          new Object[] {"@LogResponsavel_ObjetoTipo",SqlDbType.Char,1,0} ,
          new Object[] {"@LogResponsavel_Status",SqlDbType.Char,1,0} ,
          new Object[] {"@LogResponsavel_NovoStatus",SqlDbType.Char,1,0} ,
          new Object[] {"@LogResponsavel_Observacao",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@LogResponsavel_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@LogResponsavel_Owner",SqlDbType.Int,6,0} ,
          new Object[] {"@LogResponsavel_DemandaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@LogResponsavel_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmT002T20 ;
          prmT002T20 = new Object[] {
          new Object[] {"@LogResponsavel_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmT002T21 ;
          prmT002T21 = new Object[] {
          new Object[] {"@LogResponsavel_DemandaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002T22 ;
          prmT002T22 = new Object[] {
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002T23 ;
          prmT002T23 = new Object[] {
          new Object[] {"@LogResponsavel_Owner",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002T24 ;
          prmT002T24 = new Object[] {
          new Object[] {"@LogResponsavel_OwnerPessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002T25 ;
          prmT002T25 = new Object[] {
          new Object[] {"@LogResponsavel_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmT002T26 ;
          prmT002T26 = new Object[] {
          } ;
          Object[] prmT002T27 ;
          prmT002T27 = new Object[] {
          } ;
          Object[] prmT002T28 ;
          prmT002T28 = new Object[] {
          new Object[] {"@LogResponsavel_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002T29 ;
          prmT002T29 = new Object[] {
          new Object[] {"@LogResponsavel_DemandaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002T30 ;
          prmT002T30 = new Object[] {
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002T31 ;
          prmT002T31 = new Object[] {
          new Object[] {"@LogResponsavel_Owner",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002T32 ;
          prmT002T32 = new Object[] {
          new Object[] {"@LogResponsavel_OwnerPessoaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T002T2", "SELECT [LogResponsavel_Codigo], [LogResponsavel_DataHora], [LogResponsavel_Prazo], [LogResponsavel_Acao], [LogResponsavel_ObjetoTipo], [LogResponsavel_Status], [LogResponsavel_NovoStatus], [LogResponsavel_Observacao], [LogResponsavel_UsuarioCod] AS LogResponsavel_UsuarioCod, [LogResponsavel_Owner] AS LogResponsavel_Owner, [LogResponsavel_DemandaCod] AS LogResponsavel_DemandaCod FROM [LogResponsavel] WITH (UPDLOCK) WHERE [LogResponsavel_Codigo] = @LogResponsavel_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002T2,1,0,true,false )
             ,new CursorDef("T002T3", "SELECT [LogResponsavel_Codigo], [LogResponsavel_DataHora], [LogResponsavel_Prazo], [LogResponsavel_Acao], [LogResponsavel_ObjetoTipo], [LogResponsavel_Status], [LogResponsavel_NovoStatus], [LogResponsavel_Observacao], [LogResponsavel_UsuarioCod] AS LogResponsavel_UsuarioCod, [LogResponsavel_Owner] AS LogResponsavel_Owner, [LogResponsavel_DemandaCod] AS LogResponsavel_DemandaCod FROM [LogResponsavel] WITH (NOLOCK) WHERE [LogResponsavel_Codigo] = @LogResponsavel_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002T3,1,0,true,false )
             ,new CursorDef("T002T4", "SELECT [Usuario_Codigo] AS LogResponsavel_UsuarioCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @LogResponsavel_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002T4,1,0,true,false )
             ,new CursorDef("T002T5", "SELECT [Usuario_PessoaCod] AS LogResponsavel_OwnerPessoaCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @LogResponsavel_Owner ",true, GxErrorMask.GX_NOMASK, false, this,prmT002T5,1,0,true,false )
             ,new CursorDef("T002T6", "SELECT [ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @LogResponsavel_DemandaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002T6,1,0,true,false )
             ,new CursorDef("T002T7", "SELECT [Pessoa_Nome] AS LogResponsavel_OwnerPessoaNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @LogResponsavel_OwnerPessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002T7,1,0,true,false )
             ,new CursorDef("T002T8", "SELECT [Contratada_AreaTrabalhoCod] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContagemResultado_ContratadaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002T8,1,0,true,false )
             ,new CursorDef("T002T9", "SELECT T2.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, TM1.[LogResponsavel_Codigo], TM1.[LogResponsavel_DataHora], TM1.[LogResponsavel_Prazo], TM1.[LogResponsavel_Acao], TM1.[LogResponsavel_ObjetoTipo], T5.[Pessoa_Nome] AS LogResponsavel_OwnerPessoaNom, TM1.[LogResponsavel_Status], TM1.[LogResponsavel_NovoStatus], TM1.[LogResponsavel_Observacao], TM1.[LogResponsavel_UsuarioCod] AS LogResponsavel_UsuarioCod, TM1.[LogResponsavel_Owner] AS LogResponsavel_Owner, TM1.[LogResponsavel_DemandaCod] AS LogResponsavel_DemandaCod, T4.[Usuario_PessoaCod] AS LogResponsavel_OwnerPessoaCod, T3.[Contratada_AreaTrabalhoCod] FROM (((([LogResponsavel] TM1 WITH (NOLOCK) LEFT JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = TM1.[LogResponsavel_DemandaCod]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[ContagemResultado_ContratadaCod]) INNER JOIN [Usuario] T4 WITH (NOLOCK) ON T4.[Usuario_Codigo] = TM1.[LogResponsavel_Owner]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Usuario_PessoaCod]) WHERE TM1.[LogResponsavel_Codigo] = @LogResponsavel_Codigo ORDER BY TM1.[LogResponsavel_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002T9,100,0,true,false )
             ,new CursorDef("T002T10", "SELECT [Usuario_Codigo] AS LogResponsavel_UsuarioCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @LogResponsavel_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002T10,1,0,true,false )
             ,new CursorDef("T002T11", "SELECT [ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @LogResponsavel_DemandaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002T11,1,0,true,false )
             ,new CursorDef("T002T12", "SELECT [Contratada_AreaTrabalhoCod] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContagemResultado_ContratadaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002T12,1,0,true,false )
             ,new CursorDef("T002T13", "SELECT [Usuario_PessoaCod] AS LogResponsavel_OwnerPessoaCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @LogResponsavel_Owner ",true, GxErrorMask.GX_NOMASK, false, this,prmT002T13,1,0,true,false )
             ,new CursorDef("T002T14", "SELECT [Pessoa_Nome] AS LogResponsavel_OwnerPessoaNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @LogResponsavel_OwnerPessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002T14,1,0,true,false )
             ,new CursorDef("T002T15", "SELECT [LogResponsavel_Codigo] FROM [LogResponsavel] WITH (NOLOCK) WHERE [LogResponsavel_Codigo] = @LogResponsavel_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002T15,1,0,true,false )
             ,new CursorDef("T002T16", "SELECT TOP 1 [LogResponsavel_Codigo] FROM [LogResponsavel] WITH (NOLOCK) WHERE ( [LogResponsavel_Codigo] > @LogResponsavel_Codigo) ORDER BY [LogResponsavel_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002T16,1,0,true,true )
             ,new CursorDef("T002T17", "SELECT TOP 1 [LogResponsavel_Codigo] FROM [LogResponsavel] WITH (NOLOCK) WHERE ( [LogResponsavel_Codigo] < @LogResponsavel_Codigo) ORDER BY [LogResponsavel_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002T17,1,0,true,true )
             ,new CursorDef("T002T18", "INSERT INTO [LogResponsavel]([LogResponsavel_DataHora], [LogResponsavel_Prazo], [LogResponsavel_Acao], [LogResponsavel_ObjetoTipo], [LogResponsavel_Status], [LogResponsavel_NovoStatus], [LogResponsavel_Observacao], [LogResponsavel_UsuarioCod], [LogResponsavel_Owner], [LogResponsavel_DemandaCod]) VALUES(@LogResponsavel_DataHora, @LogResponsavel_Prazo, @LogResponsavel_Acao, @LogResponsavel_ObjetoTipo, @LogResponsavel_Status, @LogResponsavel_NovoStatus, @LogResponsavel_Observacao, @LogResponsavel_UsuarioCod, @LogResponsavel_Owner, @LogResponsavel_DemandaCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT002T18)
             ,new CursorDef("T002T19", "UPDATE [LogResponsavel] SET [LogResponsavel_DataHora]=@LogResponsavel_DataHora, [LogResponsavel_Prazo]=@LogResponsavel_Prazo, [LogResponsavel_Acao]=@LogResponsavel_Acao, [LogResponsavel_ObjetoTipo]=@LogResponsavel_ObjetoTipo, [LogResponsavel_Status]=@LogResponsavel_Status, [LogResponsavel_NovoStatus]=@LogResponsavel_NovoStatus, [LogResponsavel_Observacao]=@LogResponsavel_Observacao, [LogResponsavel_UsuarioCod]=@LogResponsavel_UsuarioCod, [LogResponsavel_Owner]=@LogResponsavel_Owner, [LogResponsavel_DemandaCod]=@LogResponsavel_DemandaCod  WHERE [LogResponsavel_Codigo] = @LogResponsavel_Codigo", GxErrorMask.GX_NOMASK,prmT002T19)
             ,new CursorDef("T002T20", "DELETE FROM [LogResponsavel]  WHERE [LogResponsavel_Codigo] = @LogResponsavel_Codigo", GxErrorMask.GX_NOMASK,prmT002T20)
             ,new CursorDef("T002T21", "SELECT [ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @LogResponsavel_DemandaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002T21,1,0,true,false )
             ,new CursorDef("T002T22", "SELECT [Contratada_AreaTrabalhoCod] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContagemResultado_ContratadaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002T22,1,0,true,false )
             ,new CursorDef("T002T23", "SELECT [Usuario_PessoaCod] AS LogResponsavel_OwnerPessoaCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @LogResponsavel_Owner ",true, GxErrorMask.GX_NOMASK, false, this,prmT002T23,1,0,true,false )
             ,new CursorDef("T002T24", "SELECT [Pessoa_Nome] AS LogResponsavel_OwnerPessoaNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @LogResponsavel_OwnerPessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002T24,1,0,true,false )
             ,new CursorDef("T002T25", "SELECT TOP 1 [ContagemResultadoNaoCnf_Codigo] FROM [ContagemResultadoNaoCnf] WITH (NOLOCK) WHERE [ContagemResultadoNaoCnf_LogRspCod] = @LogResponsavel_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002T25,1,0,true,true )
             ,new CursorDef("T002T26", "SELECT [LogResponsavel_Codigo] FROM [LogResponsavel] WITH (NOLOCK) ORDER BY [LogResponsavel_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002T26,100,0,true,false )
             ,new CursorDef("T002T27", "SELECT T2.[Pessoa_Codigo] AS Usuario_PessoaCod, T1.[Usuario_Codigo], T2.[Pessoa_Nome] AS Usuario_PessoaNom FROM ([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) ORDER BY T2.[Pessoa_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT002T27,0,0,true,false )
             ,new CursorDef("T002T28", "SELECT [Usuario_Codigo] AS LogResponsavel_UsuarioCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @LogResponsavel_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002T28,1,0,true,false )
             ,new CursorDef("T002T29", "SELECT [ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @LogResponsavel_DemandaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002T29,1,0,true,false )
             ,new CursorDef("T002T30", "SELECT [Contratada_AreaTrabalhoCod] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContagemResultado_ContratadaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002T30,1,0,true,false )
             ,new CursorDef("T002T31", "SELECT [Usuario_PessoaCod] AS LogResponsavel_OwnerPessoaCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @LogResponsavel_Owner ",true, GxErrorMask.GX_NOMASK, false, this,prmT002T31,1,0,true,false )
             ,new CursorDef("T002T32", "SELECT [Pessoa_Nome] AS LogResponsavel_OwnerPessoaNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @LogResponsavel_OwnerPessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002T32,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 20) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 1) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getString(7, 1) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((String[]) buf[10])[0] = rslt.getLongVarchar(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((int[]) buf[12])[0] = rslt.getInt(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((int[]) buf[14])[0] = rslt.getInt(10) ;
                ((int[]) buf[15])[0] = rslt.getInt(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 20) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 1) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getString(7, 1) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((String[]) buf[10])[0] = rslt.getLongVarchar(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((int[]) buf[12])[0] = rslt.getInt(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((int[]) buf[14])[0] = rslt.getInt(10) ;
                ((int[]) buf[15])[0] = rslt.getInt(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((long[]) buf[2])[0] = rslt.getLong(2) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDateTime(3) ;
                ((DateTime[]) buf[4])[0] = rslt.getGXDateTime(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 20) ;
                ((String[]) buf[7])[0] = rslt.getString(6, 1) ;
                ((String[]) buf[8])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((String[]) buf[10])[0] = rslt.getString(8, 1) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((String[]) buf[12])[0] = rslt.getString(9, 1) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((String[]) buf[14])[0] = rslt.getLongVarchar(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((int[]) buf[16])[0] = rslt.getInt(11) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((int[]) buf[18])[0] = rslt.getInt(12) ;
                ((int[]) buf[19])[0] = rslt.getInt(13) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(13);
                ((int[]) buf[21])[0] = rslt.getInt(14) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(14);
                ((int[]) buf[23])[0] = rslt.getInt(15) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(15);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 12 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 13 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 14 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 15 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 16 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 22 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 23 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 24 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 25 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 26 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 27 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 28 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 29 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
       }
       getresults30( cursor, rslt, buf) ;
    }

    public void getresults30( int cursor ,
                              IFieldGetter rslt ,
                              Object[] buf )
    {
       switch ( cursor )
       {
             case 30 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 13 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 16 :
                stmt.SetParameterDatetime(1, (DateTime)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(2, (DateTime)parms[2]);
                }
                stmt.SetParameter(3, (String)parms[3]);
                stmt.SetParameter(4, (String)parms[4]);
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 8 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(8, (int)parms[12]);
                }
                stmt.SetParameter(9, (int)parms[13]);
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 10 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(10, (int)parms[15]);
                }
                return;
             case 17 :
                stmt.SetParameterDatetime(1, (DateTime)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(2, (DateTime)parms[2]);
                }
                stmt.SetParameter(3, (String)parms[3]);
                stmt.SetParameter(4, (String)parms[4]);
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 8 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(8, (int)parms[12]);
                }
                stmt.SetParameter(9, (int)parms[13]);
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 10 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(10, (int)parms[15]);
                }
                stmt.SetParameter(11, (long)parms[16]);
                return;
             case 18 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 19 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 20 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 21 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 22 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 23 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 26 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 27 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 28 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 29 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
       setparameters30( cursor, stmt, parms) ;
    }

    public void setparameters30( int cursor ,
                                 IFieldSetter stmt ,
                                 Object[] parms )
    {
       switch ( cursor )
       {
             case 30 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
