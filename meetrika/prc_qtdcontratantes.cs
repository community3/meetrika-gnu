/*
               File: PRC_QtdContratantes
        Description: Qtd de Contratantes
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:7:40.51
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_qtdcontratantes : GXProcedure
   {
      public prc_qtdcontratantes( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_qtdcontratantes( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( out int aP0_Qtd )
      {
         this.AV8Qtd = 0 ;
         initialize();
         executePrivate();
         aP0_Qtd=this.AV8Qtd;
      }

      public int executeUdp( )
      {
         this.AV8Qtd = 0 ;
         initialize();
         executePrivate();
         aP0_Qtd=this.AV8Qtd;
         return AV8Qtd ;
      }

      public void executeSubmit( out int aP0_Qtd )
      {
         prc_qtdcontratantes objprc_qtdcontratantes;
         objprc_qtdcontratantes = new prc_qtdcontratantes();
         objprc_qtdcontratantes.AV8Qtd = 0 ;
         objprc_qtdcontratantes.context.SetSubmitInitialConfig(context);
         objprc_qtdcontratantes.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_qtdcontratantes);
         aP0_Qtd=this.AV8Qtd;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_qtdcontratantes)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Optimized group. */
         /* Using cursor P00152 */
         pr_default.execute(0);
         cV8Qtd = P00152_AV8Qtd[0];
         pr_default.close(0);
         AV8Qtd = (int)(AV8Qtd+cV8Qtd*1);
         /* End optimized group. */
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00152_AV8Qtd = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_qtdcontratantes__default(),
            new Object[][] {
                new Object[] {
               P00152_AV8Qtd
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV8Qtd ;
      private int cV8Qtd ;
      private String scmdbuf ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00152_AV8Qtd ;
      private int aP0_Qtd ;
   }

   public class prc_qtdcontratantes__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00152 ;
          prmP00152 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("P00152", "SELECT COUNT(*) FROM [Contratante] WITH (NOLOCK) WHERE [Contratante_Ativo] = 1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00152,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

 }

}
