/*
               File: GxOfflineEventReplicator
        Description: Offline Event Replicator
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:7:49.99
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gxofflineeventreplicator : GXProcedure
   {
      public gxofflineeventreplicator( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public gxofflineeventreplicator( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( IGxCollection aP0_GxPendingEvents ,
                           SdtGxSynchroInfoSDT aP1_GxSyncroInfo ,
                           out IGxCollection aP2_EventResults )
      {
         this.GxPendingEvents = aP0_GxPendingEvents;
         this.GxSyncroInfo = aP1_GxSyncroInfo;
         this.AV28EventResults = new GxObjectCollection( context, "GxSynchroEventResultSDT.GxSynchroEventResultSDTItem", "GxEv3Up14_Meetrika", "SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP2_EventResults=this.AV28EventResults;
      }

      public IGxCollection executeUdp( IGxCollection aP0_GxPendingEvents ,
                                       SdtGxSynchroInfoSDT aP1_GxSyncroInfo )
      {
         this.GxPendingEvents = aP0_GxPendingEvents;
         this.GxSyncroInfo = aP1_GxSyncroInfo;
         this.AV28EventResults = new GxObjectCollection( context, "GxSynchroEventResultSDT.GxSynchroEventResultSDTItem", "GxEv3Up14_Meetrika", "SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP2_EventResults=this.AV28EventResults;
         return AV28EventResults ;
      }

      public void executeSubmit( IGxCollection aP0_GxPendingEvents ,
                                 SdtGxSynchroInfoSDT aP1_GxSyncroInfo ,
                                 out IGxCollection aP2_EventResults )
      {
         gxofflineeventreplicator objgxofflineeventreplicator;
         objgxofflineeventreplicator = new gxofflineeventreplicator();
         objgxofflineeventreplicator.GxPendingEvents = aP0_GxPendingEvents;
         objgxofflineeventreplicator.GxSyncroInfo = aP1_GxSyncroInfo;
         objgxofflineeventreplicator.AV28EventResults = new GxObjectCollection( context, "GxSynchroEventResultSDT.GxSynchroEventResultSDTItem", "GxEv3Up14_Meetrika", "SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem", "GeneXus.Programs") ;
         objgxofflineeventreplicator.context.SetSubmitInitialConfig(context);
         objgxofflineeventreplicator.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgxofflineeventreplicator);
         aP2_EventResults=this.AV28EventResults;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((gxofflineeventreplicator)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV21Continue = true;
         AV59GXV1 = 1;
         while ( AV59GXV1 <= GxPendingEvents.Count )
         {
            AV47PendingEvent = ((SdtGxSynchroEventSDT_GxSynchroEventSDTItem)GxPendingEvents.Item(AV59GXV1));
            if ( AV47PendingEvent.gxTpr_Eventstatus == 1 )
            {
               AV20BC = (GxSilentTrnSdt)(GeneXus.Utils.GxSilentTrnSdt.Create( AV47PendingEvent.gxTpr_Eventbc, "GeneXus.Programs", context));
               if ( AV20BC.Success() )
               {
                  AV44Metadata = AV20BC.GetMetadata();
                  /* Execute user subroutine: 'PROCESSBC' */
                  S111 ();
                  if ( returnInSub )
                  {
                     this.cleanup();
                     if (true) return;
                  }
               }
               else
               {
                  AV43Messages = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
                  AV42Message = new SdtMessages_Message(context);
                  AV42Message.gxTpr_Id = "BC not found";
                  AV42Message.gxTpr_Description = StringUtil.Format( "BusinessComponent not found: %1", AV47PendingEvent.gxTpr_Eventbc, "", "", "", "", "", "", "", "");
                  AV42Message.gxTpr_Type = 1;
                  AV43Messages.Add(AV42Message, 0);
                  AV47PendingEvent.gxTpr_Eventstatus = 7;
                  AV25EventResult = new SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem(context);
                  AV25EventResult.gxTpr_Eventid = (Guid)(AV47PendingEvent.gxTpr_Eventid);
                  AV25EventResult.gxTpr_Eventtimestamp = DateTimeUtil.Now( context);
                  AV25EventResult.gxTpr_Eventstatus = 7;
                  AV25EventResult.gxTpr_Eventerrors = AV43Messages.ToJSonString(false);
                  AV28EventResults.Add(AV25EventResult, 0);
                  GXt_boolean1 = AV21Continue;
                  new gxonpendingeventfailed(context ).execute(  AV47PendingEvent,  AV47PendingEvent.gxTpr_Eventbc,  AV24EventData,  AV25EventResult,  GxSyncroInfo, out  GXt_boolean1) ;
                  AV21Continue = GXt_boolean1;
               }
            }
            if ( ! AV21Continue )
            {
               if (true) break;
            }
            AV59GXV1 = (int)(AV59GXV1+1);
         }
         new gxaftereventreplicator(context ).execute(  AV28EventResults,  GxSyncroInfo) ;
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'PROCESSBC' Routine */
         AV19Authorized = true;
         AV54CurrentMappingInfo.Clear();
         AV53PKMappingPK = (IGxCollection)(new GxSimpleCollection());
         AV25EventResult = new SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem(context);
         AV25EventResult.gxTpr_Eventid = (Guid)(AV47PendingEvent.gxTpr_Eventid);
         AV25EventResult.gxTpr_Eventtimestamp = DateTimeUtil.Now( context);
         AV24EventData = AV47PendingEvent.gxTpr_Eventdata;
         AV56Props.Clear();
         if ( AV47PendingEvent.gxTpr_Eventaction == 1 )
         {
            if ( StringUtil.StrCmp("True", AV44Metadata.Get("AllowInsert")) == 0 )
            {
               AV20BC.FromJSonString(AV47PendingEvent.gxTpr_Eventdata);
               /* Execute user subroutine: 'APPLYMAPPINGS' */
               S121 ();
               if (returnInSub) return;
               if ( AV25EventResult.gxTpr_Eventstatus != 6 )
               {
                  AV24EventData = AV20BC.ToJSonString(true);
                  AV20BC.Save();
                  /* Execute user subroutine: 'GETNEWMAPPINGS' */
                  S131 ();
                  if (returnInSub) return;
               }
            }
            else
            {
               AV19Authorized = false;
            }
         }
         if ( AV47PendingEvent.gxTpr_Eventaction == 2 )
         {
            if ( StringUtil.StrCmp("True", AV44Metadata.Get("AllowUpdate")) == 0 )
            {
               AV51UpdEventData = AV47PendingEvent.gxTpr_Eventdata;
               AV20BC.FromJSonString(AV51UpdEventData);
               /* Execute user subroutine: 'APPLYPKMAPPINGS' */
               S141 ();
               if (returnInSub) return;
               AV20BC.Load();
               if ( AV20BC.Success() )
               {
                  AV20BC.FromJSonString(AV51UpdEventData);
                  /* Execute user subroutine: 'APPLYMAPPINGS' */
                  S121 ();
                  if (returnInSub) return;
                  if ( AV25EventResult.gxTpr_Eventstatus != 6 )
                  {
                     AV24EventData = AV20BC.ToJSonString(true);
                     AV20BC.Save();
                  }
               }
            }
            else
            {
               AV19Authorized = false;
            }
         }
         if ( AV47PendingEvent.gxTpr_Eventaction == 3 )
         {
            if ( StringUtil.StrCmp("True", AV44Metadata.Get("AllowDelete")) == 0 )
            {
               AV20BC.FromJSonString(AV47PendingEvent.gxTpr_Eventdata);
               AV20BC.Load();
               if ( AV20BC.Success() )
               {
                  AV20BC.Delete();
               }
            }
            else
            {
               AV19Authorized = false;
            }
         }
         if ( AV19Authorized )
         {
            if ( AV25EventResult.gxTpr_Eventstatus != 6 )
            {
               if ( AV20BC.Success() )
               {
                  AV47PendingEvent.gxTpr_Eventstatus = 3;
                  AV25EventResult.gxTpr_Eventstatus = 3;
               }
               else
               {
                  AV47PendingEvent.gxTpr_Eventstatus = 4;
                  AV25EventResult.gxTpr_Eventstatus = 4;
                  AV25EventResult.gxTpr_Eventerrors = AV20BC.GetMessages().ToJSonString(false);
                  GXt_boolean1 = AV21Continue;
                  new gxonpendingeventfailed(context ).execute(  AV47PendingEvent,  AV47PendingEvent.gxTpr_Eventbc,  AV24EventData,  AV25EventResult,  GxSyncroInfo, out  GXt_boolean1) ;
                  AV21Continue = GXt_boolean1;
               }
            }
            else
            {
               GXt_boolean1 = AV21Continue;
               new gxonpendingeventfailed(context ).execute(  AV47PendingEvent,  AV47PendingEvent.gxTpr_Eventbc,  AV24EventData,  AV25EventResult,  GxSyncroInfo, out  GXt_boolean1) ;
               AV21Continue = GXt_boolean1;
            }
         }
         else
         {
            AV43Messages = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
            AV42Message = new SdtMessages_Message(context);
            AV42Message.gxTpr_Id = "NotAuthorized";
            AV42Message.gxTpr_Description = "Not Authorized";
            AV42Message.gxTpr_Type = 1;
            AV43Messages.Add(AV42Message, 0);
            AV47PendingEvent.gxTpr_Eventstatus = 4;
            AV25EventResult.gxTpr_Eventstatus = 4;
            AV25EventResult.gxTpr_Eventerrors = AV43Messages.ToJSonString(false);
            GXt_boolean1 = AV21Continue;
            new gxonpendingeventfailed(context ).execute(  AV47PendingEvent,  AV47PendingEvent.gxTpr_Eventbc,  AV24EventData,  AV25EventResult,  GxSyncroInfo, out  GXt_boolean1) ;
            AV21Continue = GXt_boolean1;
         }
         AV28EventResults.Add(AV25EventResult, 0);
         context.CommitDataStores( "GxOfflineEventReplicator");
      }

      protected void S141( )
      {
         /* 'APPLYPKMAPPINGS' Routine */
         AV39MappingConditions = new GxObjectCollection( context, "GxKeyValue", "GxEv3Up14_Meetrika", "SdtGxKeyValue", "GeneXus.Programs");
         new gxofflineeventreplicatormap(context ).execute(  AV44Metadata,  "",  0,  AV8AllAttributeMappings,  AV54CurrentMappingInfo,  AV25EventResult,  AV20BC,  AV39MappingConditions,  "PK",  AV53PKMappingPK,  AV56Props) ;
         AV51UpdEventData = AV20BC.ToJSonString(false);
      }

      protected void S121( )
      {
         /* 'APPLYMAPPINGS' Routine */
         AV39MappingConditions = new GxObjectCollection( context, "GxKeyValue", "GxEv3Up14_Meetrika", "SdtGxKeyValue", "GeneXus.Programs");
         new gxofflineeventreplicatormap(context ).execute(  AV44Metadata,  "",  0,  AV8AllAttributeMappings,  AV54CurrentMappingInfo,  AV25EventResult,  AV20BC,  AV39MappingConditions,  "FK",  AV53PKMappingPK,  AV56Props) ;
      }

      protected void S131( )
      {
         /* 'GETNEWMAPPINGS' Routine */
         if ( AV20BC.Success() )
         {
            AV56Props.Set("Result", "Success");
         }
         else
         {
            AV56Props.Set("Result", "Error");
         }
         AV39MappingConditions = new GxObjectCollection( context, "GxKeyValue", "GxEv3Up14_Meetrika", "SdtGxKeyValue", "GeneXus.Programs");
         new gxofflineeventreplicatormap(context ).execute(  AV44Metadata,  "",  0,  AV8AllAttributeMappings,  AV54CurrentMappingInfo,  AV25EventResult,  AV20BC,  AV39MappingConditions,  "TblUpd",  AV53PKMappingPK,  AV56Props) ;
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV47PendingEvent = new SdtGxSynchroEventSDT_GxSynchroEventSDTItem(context);
         AV20BC = new GxSilentTrnSdt(context);
         AV44Metadata = new GXProperties();
         AV43Messages = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
         AV42Message = new SdtMessages_Message(context);
         AV25EventResult = new SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem(context);
         AV24EventData = "";
         AV54CurrentMappingInfo = new GXProperties();
         AV53PKMappingPK = new GxSimpleCollection();
         AV56Props = new GXProperties();
         AV51UpdEventData = "";
         AV39MappingConditions = new GxObjectCollection( context, "GxKeyValue", "GxEv3Up14_Meetrika", "SdtGxKeyValue", "GeneXus.Programs");
         AV8AllAttributeMappings = new GXProperties();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.gxofflineeventreplicator__default(),
            new Object[][] {
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV59GXV1 ;
      private bool AV21Continue ;
      private bool returnInSub ;
      private bool AV19Authorized ;
      private bool GXt_boolean1 ;
      private String AV24EventData ;
      private String AV51UpdEventData ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IGxCollection aP2_EventResults ;
      private IDataStoreProvider pr_default ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV53PKMappingPK ;
      [ObjectCollection(ItemType=typeof( SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem ))]
      private IGxCollection AV28EventResults ;
      [ObjectCollection(ItemType=typeof( SdtGxSynchroEventSDT_GxSynchroEventSDTItem ))]
      private IGxCollection GxPendingEvents ;
      [ObjectCollection(ItemType=typeof( SdtGxKeyValue ))]
      private IGxCollection AV39MappingConditions ;
      [ObjectCollection(ItemType=typeof( SdtMessages_Message ))]
      private IGxCollection AV43Messages ;
      private GXProperties AV44Metadata ;
      private GXProperties AV54CurrentMappingInfo ;
      private GXProperties AV56Props ;
      private GXProperties AV8AllAttributeMappings ;
      private SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem AV25EventResult ;
      private SdtGxSynchroEventSDT_GxSynchroEventSDTItem AV47PendingEvent ;
      private SdtGxSynchroInfoSDT GxSyncroInfo ;
      private SdtMessages_Message AV42Message ;
      private GxSilentTrnSdt AV20BC ;
   }

   public class gxofflineeventreplicator__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          def= new CursorDef[] {
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.gxofflineeventreplicator_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class gxofflineeventreplicator_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( GxGenericCollection<SdtGxSynchroEventSDT_GxSynchroEventSDTItem_RESTInterface> GxPendingEvents ,
                         SdtGxSynchroInfoSDT_RESTInterface GxSyncroInfo ,
                         out GxGenericCollection<SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_RESTInterface> EventResults )
    {
       EventResults = new GxGenericCollection<SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_RESTInterface>() ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("gxofflineeventreplicator") )
          {
             return  ;
          }
          gxofflineeventreplicator worker = new gxofflineeventreplicator(context) ;
          worker.IsMain = RunAsMain ;
          IGxCollection gxrGxPendingEvents = new GxObjectCollection() ;
          GxPendingEvents.LoadCollection(gxrGxPendingEvents);
          SdtGxSynchroInfoSDT gxrGxSyncroInfo = GxSyncroInfo.sdt ;
          IGxCollection gxrEventResults = new GxObjectCollection() ;
          worker.execute(gxrGxPendingEvents,gxrGxSyncroInfo,out gxrEventResults );
          worker.cleanup( );
          EventResults = new GxGenericCollection<SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_RESTInterface>(gxrEventResults) ;
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
