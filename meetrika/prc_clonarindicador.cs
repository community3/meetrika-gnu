/*
               File: PRC_ClonarIndicador
        Description: Clonar Indicador
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:9:12.78
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_clonarindicador : GXProcedure
   {
      public prc_clonarindicador( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_clonarindicador( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratoServicos_Codigo ,
                           int aP1_Indicador ,
                           bool aP2_SomenteFaixas ,
                           int aP3_ContratoServicosIndicador_Codigo )
      {
         this.AV8ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         this.AV9Indicador = aP1_Indicador;
         this.AV11SomenteFaixas = aP2_SomenteFaixas;
         this.AV10ContratoServicosIndicador_Codigo = aP3_ContratoServicosIndicador_Codigo;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_ContratoServicos_Codigo ,
                                 int aP1_Indicador ,
                                 bool aP2_SomenteFaixas ,
                                 int aP3_ContratoServicosIndicador_Codigo )
      {
         prc_clonarindicador objprc_clonarindicador;
         objprc_clonarindicador = new prc_clonarindicador();
         objprc_clonarindicador.AV8ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         objprc_clonarindicador.AV9Indicador = aP1_Indicador;
         objprc_clonarindicador.AV11SomenteFaixas = aP2_SomenteFaixas;
         objprc_clonarindicador.AV10ContratoServicosIndicador_Codigo = aP3_ContratoServicosIndicador_Codigo;
         objprc_clonarindicador.context.SetSubmitInitialConfig(context);
         objprc_clonarindicador.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_clonarindicador);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_clonarindicador)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( AV11SomenteFaixas )
         {
            /* Using cursor P009K2 */
            pr_default.execute(0, new Object[] {AV9Indicador});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A1269ContratoServicosIndicador_Codigo = P009K2_A1269ContratoServicosIndicador_Codigo[0];
               OV16ContratoServicosIndicadorFaixa_Desde = AV16ContratoServicosIndicadorFaixa_Desde;
               OV17ContratoServicosIndicadorFaixa_Ate = AV17ContratoServicosIndicadorFaixa_Ate;
               OV18ContratoServicosIndicadorFaixa_Reduz = AV18ContratoServicosIndicadorFaixa_Reduz;
               OV19ContratoServicosIndicadorFaixa_Und = AV19ContratoServicosIndicadorFaixa_Und;
               /* Using cursor P009K3 */
               pr_default.execute(1, new Object[] {A1269ContratoServicosIndicador_Codigo});
               while ( (pr_default.getStatus(1) != 101) )
               {
                  A1301ContratoServicosIndicadorFaixa_Desde = P009K3_A1301ContratoServicosIndicadorFaixa_Desde[0];
                  A1302ContratoServicosIndicadorFaixa_Ate = P009K3_A1302ContratoServicosIndicadorFaixa_Ate[0];
                  A1303ContratoServicosIndicadorFaixa_Reduz = P009K3_A1303ContratoServicosIndicadorFaixa_Reduz[0];
                  A1304ContratoServicosIndicadorFaixa_Und = P009K3_A1304ContratoServicosIndicadorFaixa_Und[0];
                  A1299ContratoServicosIndicadorFaixa_Codigo = P009K3_A1299ContratoServicosIndicadorFaixa_Codigo[0];
                  AV13DoIndicador_Codigo = AV10ContratoServicosIndicador_Codigo;
                  AV14ContratoServicosIndicadorFaixa_Codigo = A1299ContratoServicosIndicadorFaixa_Codigo;
                  AV16ContratoServicosIndicadorFaixa_Desde = A1301ContratoServicosIndicadorFaixa_Desde;
                  AV17ContratoServicosIndicadorFaixa_Ate = A1302ContratoServicosIndicadorFaixa_Ate;
                  AV18ContratoServicosIndicadorFaixa_Reduz = A1303ContratoServicosIndicadorFaixa_Reduz;
                  AV19ContratoServicosIndicadorFaixa_Und = A1304ContratoServicosIndicadorFaixa_Und;
                  /* Execute user subroutine: 'NEWFAIXA' */
                  S111 ();
                  if ( returnInSub )
                  {
                     pr_default.close(1);
                     this.cleanup();
                     if (true) return;
                  }
                  pr_default.readNext(1);
               }
               pr_default.close(1);
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
         }
         else
         {
            /* Using cursor P009K4 */
            pr_default.execute(2, new Object[] {AV9Indicador});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A1270ContratoServicosIndicador_CntSrvCod = P009K4_A1270ContratoServicosIndicador_CntSrvCod[0];
               A2052ContratoServicosIndicador_Formato = P009K4_A2052ContratoServicosIndicador_Formato[0];
               A2051ContratoServicosIndicador_Sigla = P009K4_A2051ContratoServicosIndicador_Sigla[0];
               n2051ContratoServicosIndicador_Sigla = P009K4_n2051ContratoServicosIndicador_Sigla[0];
               A1345ContratoServicosIndicador_CalculoSob = P009K4_A1345ContratoServicosIndicador_CalculoSob[0];
               n1345ContratoServicosIndicador_CalculoSob = P009K4_n1345ContratoServicosIndicador_CalculoSob[0];
               A1310ContratoServicosIndicador_Vigencia = P009K4_A1310ContratoServicosIndicador_Vigencia[0];
               n1310ContratoServicosIndicador_Vigencia = P009K4_n1310ContratoServicosIndicador_Vigencia[0];
               A1309ContratoServicosIndicador_Periodicidade = P009K4_A1309ContratoServicosIndicador_Periodicidade[0];
               n1309ContratoServicosIndicador_Periodicidade = P009K4_n1309ContratoServicosIndicador_Periodicidade[0];
               A1308ContratoServicosIndicador_Tipo = P009K4_A1308ContratoServicosIndicador_Tipo[0];
               n1308ContratoServicosIndicador_Tipo = P009K4_n1308ContratoServicosIndicador_Tipo[0];
               A1307ContratoServicosIndicador_InstrumentoMedicao = P009K4_A1307ContratoServicosIndicador_InstrumentoMedicao[0];
               n1307ContratoServicosIndicador_InstrumentoMedicao = P009K4_n1307ContratoServicosIndicador_InstrumentoMedicao[0];
               A1306ContratoServicosIndicador_Meta = P009K4_A1306ContratoServicosIndicador_Meta[0];
               n1306ContratoServicosIndicador_Meta = P009K4_n1306ContratoServicosIndicador_Meta[0];
               A1305ContratoServicosIndicador_Finalidade = P009K4_A1305ContratoServicosIndicador_Finalidade[0];
               n1305ContratoServicosIndicador_Finalidade = P009K4_n1305ContratoServicosIndicador_Finalidade[0];
               A1274ContratoServicosIndicador_Indicador = P009K4_A1274ContratoServicosIndicador_Indicador[0];
               A1271ContratoServicosIndicador_Numero = P009K4_A1271ContratoServicosIndicador_Numero[0];
               A1269ContratoServicosIndicador_Codigo = P009K4_A1269ContratoServicosIndicador_Codigo[0];
               /*
                  INSERT RECORD ON TABLE ContratoServicosIndicador

               */
               W1270ContratoServicosIndicador_CntSrvCod = A1270ContratoServicosIndicador_CntSrvCod;
               A1270ContratoServicosIndicador_CntSrvCod = AV8ContratoServicos_Codigo;
               /* Using cursor P009K5 */
               pr_default.execute(3, new Object[] {A1270ContratoServicosIndicador_CntSrvCod, A1271ContratoServicosIndicador_Numero, A1274ContratoServicosIndicador_Indicador, n1305ContratoServicosIndicador_Finalidade, A1305ContratoServicosIndicador_Finalidade, n1306ContratoServicosIndicador_Meta, A1306ContratoServicosIndicador_Meta, n1307ContratoServicosIndicador_InstrumentoMedicao, A1307ContratoServicosIndicador_InstrumentoMedicao, n1308ContratoServicosIndicador_Tipo, A1308ContratoServicosIndicador_Tipo, n1309ContratoServicosIndicador_Periodicidade, A1309ContratoServicosIndicador_Periodicidade, n1310ContratoServicosIndicador_Vigencia, A1310ContratoServicosIndicador_Vigencia, n1345ContratoServicosIndicador_CalculoSob, A1345ContratoServicosIndicador_CalculoSob, n2051ContratoServicosIndicador_Sigla, A2051ContratoServicosIndicador_Sigla, A2052ContratoServicosIndicador_Formato});
               A1269ContratoServicosIndicador_Codigo = P009K5_A1269ContratoServicosIndicador_Codigo[0];
               pr_default.close(3);
               dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosIndicador") ;
               if ( (pr_default.getStatus(3) == 1) )
               {
                  context.Gx_err = 1;
                  Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
               }
               else
               {
                  context.Gx_err = 0;
                  Gx_emsg = "";
               }
               A1270ContratoServicosIndicador_CntSrvCod = W1270ContratoServicosIndicador_CntSrvCod;
               /* End Insert */
               AV13DoIndicador_Codigo = A1269ContratoServicosIndicador_Codigo;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(2);
            /* Using cursor P009K6 */
            pr_default.execute(4, new Object[] {AV9Indicador});
            while ( (pr_default.getStatus(4) != 101) )
            {
               A1269ContratoServicosIndicador_Codigo = P009K6_A1269ContratoServicosIndicador_Codigo[0];
               OV16ContratoServicosIndicadorFaixa_Desde = AV16ContratoServicosIndicadorFaixa_Desde;
               OV17ContratoServicosIndicadorFaixa_Ate = AV17ContratoServicosIndicadorFaixa_Ate;
               OV18ContratoServicosIndicadorFaixa_Reduz = AV18ContratoServicosIndicadorFaixa_Reduz;
               OV19ContratoServicosIndicadorFaixa_Und = AV19ContratoServicosIndicadorFaixa_Und;
               /* Using cursor P009K7 */
               pr_default.execute(5, new Object[] {A1269ContratoServicosIndicador_Codigo});
               while ( (pr_default.getStatus(5) != 101) )
               {
                  A1301ContratoServicosIndicadorFaixa_Desde = P009K7_A1301ContratoServicosIndicadorFaixa_Desde[0];
                  A1302ContratoServicosIndicadorFaixa_Ate = P009K7_A1302ContratoServicosIndicadorFaixa_Ate[0];
                  A1303ContratoServicosIndicadorFaixa_Reduz = P009K7_A1303ContratoServicosIndicadorFaixa_Reduz[0];
                  A1304ContratoServicosIndicadorFaixa_Und = P009K7_A1304ContratoServicosIndicadorFaixa_Und[0];
                  A1299ContratoServicosIndicadorFaixa_Codigo = P009K7_A1299ContratoServicosIndicadorFaixa_Codigo[0];
                  AV14ContratoServicosIndicadorFaixa_Codigo = A1299ContratoServicosIndicadorFaixa_Codigo;
                  AV16ContratoServicosIndicadorFaixa_Desde = A1301ContratoServicosIndicadorFaixa_Desde;
                  AV17ContratoServicosIndicadorFaixa_Ate = A1302ContratoServicosIndicadorFaixa_Ate;
                  AV18ContratoServicosIndicadorFaixa_Reduz = A1303ContratoServicosIndicadorFaixa_Reduz;
                  AV19ContratoServicosIndicadorFaixa_Und = A1304ContratoServicosIndicadorFaixa_Und;
                  /* Execute user subroutine: 'NEWFAIXA' */
                  S111 ();
                  if ( returnInSub )
                  {
                     pr_default.close(5);
                     this.cleanup();
                     if (true) return;
                  }
                  pr_default.readNext(5);
               }
               pr_default.close(5);
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(4);
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'NEWFAIXA' Routine */
         /*
            INSERT RECORD ON TABLE ContratoServicosIndicadorFaixas

         */
         A1269ContratoServicosIndicador_Codigo = AV13DoIndicador_Codigo;
         A1299ContratoServicosIndicadorFaixa_Codigo = AV14ContratoServicosIndicadorFaixa_Codigo;
         A1301ContratoServicosIndicadorFaixa_Desde = AV16ContratoServicosIndicadorFaixa_Desde;
         A1302ContratoServicosIndicadorFaixa_Ate = AV17ContratoServicosIndicadorFaixa_Ate;
         A1303ContratoServicosIndicadorFaixa_Reduz = AV18ContratoServicosIndicadorFaixa_Reduz;
         A1304ContratoServicosIndicadorFaixa_Und = AV19ContratoServicosIndicadorFaixa_Und;
         A1300ContratoServicosIndicadorFaixa_Numero = 0;
         /* Using cursor P009K8 */
         pr_default.execute(6, new Object[] {A1269ContratoServicosIndicador_Codigo, A1299ContratoServicosIndicadorFaixa_Codigo, A1300ContratoServicosIndicadorFaixa_Numero, A1301ContratoServicosIndicadorFaixa_Desde, A1302ContratoServicosIndicadorFaixa_Ate, A1303ContratoServicosIndicadorFaixa_Reduz, A1304ContratoServicosIndicadorFaixa_Und});
         pr_default.close(6);
         dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosIndicadorFaixas") ;
         if ( (pr_default.getStatus(6) == 1) )
         {
            context.Gx_err = 1;
            Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
         }
         else
         {
            context.Gx_err = 0;
            Gx_emsg = "";
         }
         /* End Insert */
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_ClonarIndicador");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P009K2_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         AV16ContratoServicosIndicadorFaixa_Desde = 0;
         AV17ContratoServicosIndicadorFaixa_Ate = 0;
         AV18ContratoServicosIndicadorFaixa_Reduz = 0;
         AV19ContratoServicosIndicadorFaixa_Und = 0;
         P009K3_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         P009K3_A1301ContratoServicosIndicadorFaixa_Desde = new decimal[1] ;
         P009K3_A1302ContratoServicosIndicadorFaixa_Ate = new decimal[1] ;
         P009K3_A1303ContratoServicosIndicadorFaixa_Reduz = new decimal[1] ;
         P009K3_A1304ContratoServicosIndicadorFaixa_Und = new short[1] ;
         P009K3_A1299ContratoServicosIndicadorFaixa_Codigo = new int[1] ;
         P009K4_A1270ContratoServicosIndicador_CntSrvCod = new int[1] ;
         P009K4_A2052ContratoServicosIndicador_Formato = new short[1] ;
         P009K4_A2051ContratoServicosIndicador_Sigla = new String[] {""} ;
         P009K4_n2051ContratoServicosIndicador_Sigla = new bool[] {false} ;
         P009K4_A1345ContratoServicosIndicador_CalculoSob = new String[] {""} ;
         P009K4_n1345ContratoServicosIndicador_CalculoSob = new bool[] {false} ;
         P009K4_A1310ContratoServicosIndicador_Vigencia = new String[] {""} ;
         P009K4_n1310ContratoServicosIndicador_Vigencia = new bool[] {false} ;
         P009K4_A1309ContratoServicosIndicador_Periodicidade = new String[] {""} ;
         P009K4_n1309ContratoServicosIndicador_Periodicidade = new bool[] {false} ;
         P009K4_A1308ContratoServicosIndicador_Tipo = new String[] {""} ;
         P009K4_n1308ContratoServicosIndicador_Tipo = new bool[] {false} ;
         P009K4_A1307ContratoServicosIndicador_InstrumentoMedicao = new String[] {""} ;
         P009K4_n1307ContratoServicosIndicador_InstrumentoMedicao = new bool[] {false} ;
         P009K4_A1306ContratoServicosIndicador_Meta = new String[] {""} ;
         P009K4_n1306ContratoServicosIndicador_Meta = new bool[] {false} ;
         P009K4_A1305ContratoServicosIndicador_Finalidade = new String[] {""} ;
         P009K4_n1305ContratoServicosIndicador_Finalidade = new bool[] {false} ;
         P009K4_A1274ContratoServicosIndicador_Indicador = new String[] {""} ;
         P009K4_A1271ContratoServicosIndicador_Numero = new short[1] ;
         P009K4_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         A2051ContratoServicosIndicador_Sigla = "";
         A1345ContratoServicosIndicador_CalculoSob = "";
         A1310ContratoServicosIndicador_Vigencia = "";
         A1309ContratoServicosIndicador_Periodicidade = "";
         A1308ContratoServicosIndicador_Tipo = "";
         A1307ContratoServicosIndicador_InstrumentoMedicao = "";
         A1306ContratoServicosIndicador_Meta = "";
         A1305ContratoServicosIndicador_Finalidade = "";
         A1274ContratoServicosIndicador_Indicador = "";
         P009K5_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         Gx_emsg = "";
         P009K6_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         P009K7_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         P009K7_A1301ContratoServicosIndicadorFaixa_Desde = new decimal[1] ;
         P009K7_A1302ContratoServicosIndicadorFaixa_Ate = new decimal[1] ;
         P009K7_A1303ContratoServicosIndicadorFaixa_Reduz = new decimal[1] ;
         P009K7_A1304ContratoServicosIndicadorFaixa_Und = new short[1] ;
         P009K7_A1299ContratoServicosIndicadorFaixa_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_clonarindicador__default(),
            new Object[][] {
                new Object[] {
               P009K2_A1269ContratoServicosIndicador_Codigo
               }
               , new Object[] {
               P009K3_A1269ContratoServicosIndicador_Codigo, P009K3_A1301ContratoServicosIndicadorFaixa_Desde, P009K3_A1302ContratoServicosIndicadorFaixa_Ate, P009K3_A1303ContratoServicosIndicadorFaixa_Reduz, P009K3_A1304ContratoServicosIndicadorFaixa_Und, P009K3_A1299ContratoServicosIndicadorFaixa_Codigo
               }
               , new Object[] {
               P009K4_A1270ContratoServicosIndicador_CntSrvCod, P009K4_A2052ContratoServicosIndicador_Formato, P009K4_A2051ContratoServicosIndicador_Sigla, P009K4_n2051ContratoServicosIndicador_Sigla, P009K4_A1345ContratoServicosIndicador_CalculoSob, P009K4_n1345ContratoServicosIndicador_CalculoSob, P009K4_A1310ContratoServicosIndicador_Vigencia, P009K4_n1310ContratoServicosIndicador_Vigencia, P009K4_A1309ContratoServicosIndicador_Periodicidade, P009K4_n1309ContratoServicosIndicador_Periodicidade,
               P009K4_A1308ContratoServicosIndicador_Tipo, P009K4_n1308ContratoServicosIndicador_Tipo, P009K4_A1307ContratoServicosIndicador_InstrumentoMedicao, P009K4_n1307ContratoServicosIndicador_InstrumentoMedicao, P009K4_A1306ContratoServicosIndicador_Meta, P009K4_n1306ContratoServicosIndicador_Meta, P009K4_A1305ContratoServicosIndicador_Finalidade, P009K4_n1305ContratoServicosIndicador_Finalidade, P009K4_A1274ContratoServicosIndicador_Indicador, P009K4_A1271ContratoServicosIndicador_Numero,
               P009K4_A1269ContratoServicosIndicador_Codigo
               }
               , new Object[] {
               P009K5_A1269ContratoServicosIndicador_Codigo
               }
               , new Object[] {
               P009K6_A1269ContratoServicosIndicador_Codigo
               }
               , new Object[] {
               P009K7_A1269ContratoServicosIndicador_Codigo, P009K7_A1301ContratoServicosIndicadorFaixa_Desde, P009K7_A1302ContratoServicosIndicadorFaixa_Ate, P009K7_A1303ContratoServicosIndicadorFaixa_Reduz, P009K7_A1304ContratoServicosIndicadorFaixa_Und, P009K7_A1299ContratoServicosIndicadorFaixa_Codigo
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short OV19ContratoServicosIndicadorFaixa_Und ;
      private short AV19ContratoServicosIndicadorFaixa_Und ;
      private short A1304ContratoServicosIndicadorFaixa_Und ;
      private short A2052ContratoServicosIndicador_Formato ;
      private short A1271ContratoServicosIndicador_Numero ;
      private short A1300ContratoServicosIndicadorFaixa_Numero ;
      private int AV8ContratoServicos_Codigo ;
      private int AV9Indicador ;
      private int AV10ContratoServicosIndicador_Codigo ;
      private int A1269ContratoServicosIndicador_Codigo ;
      private int A1299ContratoServicosIndicadorFaixa_Codigo ;
      private int AV13DoIndicador_Codigo ;
      private int AV14ContratoServicosIndicadorFaixa_Codigo ;
      private int A1270ContratoServicosIndicador_CntSrvCod ;
      private int GX_INS154 ;
      private int W1270ContratoServicosIndicador_CntSrvCod ;
      private int GX_INS159 ;
      private decimal OV16ContratoServicosIndicadorFaixa_Desde ;
      private decimal AV16ContratoServicosIndicadorFaixa_Desde ;
      private decimal OV17ContratoServicosIndicadorFaixa_Ate ;
      private decimal AV17ContratoServicosIndicadorFaixa_Ate ;
      private decimal OV18ContratoServicosIndicadorFaixa_Reduz ;
      private decimal AV18ContratoServicosIndicadorFaixa_Reduz ;
      private decimal A1301ContratoServicosIndicadorFaixa_Desde ;
      private decimal A1302ContratoServicosIndicadorFaixa_Ate ;
      private decimal A1303ContratoServicosIndicadorFaixa_Reduz ;
      private String scmdbuf ;
      private String A2051ContratoServicosIndicador_Sigla ;
      private String A1345ContratoServicosIndicador_CalculoSob ;
      private String A1309ContratoServicosIndicador_Periodicidade ;
      private String A1308ContratoServicosIndicador_Tipo ;
      private String Gx_emsg ;
      private bool AV11SomenteFaixas ;
      private bool returnInSub ;
      private bool n2051ContratoServicosIndicador_Sigla ;
      private bool n1345ContratoServicosIndicador_CalculoSob ;
      private bool n1310ContratoServicosIndicador_Vigencia ;
      private bool n1309ContratoServicosIndicador_Periodicidade ;
      private bool n1308ContratoServicosIndicador_Tipo ;
      private bool n1307ContratoServicosIndicador_InstrumentoMedicao ;
      private bool n1306ContratoServicosIndicador_Meta ;
      private bool n1305ContratoServicosIndicador_Finalidade ;
      private String A1307ContratoServicosIndicador_InstrumentoMedicao ;
      private String A1306ContratoServicosIndicador_Meta ;
      private String A1305ContratoServicosIndicador_Finalidade ;
      private String A1274ContratoServicosIndicador_Indicador ;
      private String A1310ContratoServicosIndicador_Vigencia ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P009K2_A1269ContratoServicosIndicador_Codigo ;
      private int[] P009K3_A1269ContratoServicosIndicador_Codigo ;
      private decimal[] P009K3_A1301ContratoServicosIndicadorFaixa_Desde ;
      private decimal[] P009K3_A1302ContratoServicosIndicadorFaixa_Ate ;
      private decimal[] P009K3_A1303ContratoServicosIndicadorFaixa_Reduz ;
      private short[] P009K3_A1304ContratoServicosIndicadorFaixa_Und ;
      private int[] P009K3_A1299ContratoServicosIndicadorFaixa_Codigo ;
      private int[] P009K4_A1270ContratoServicosIndicador_CntSrvCod ;
      private short[] P009K4_A2052ContratoServicosIndicador_Formato ;
      private String[] P009K4_A2051ContratoServicosIndicador_Sigla ;
      private bool[] P009K4_n2051ContratoServicosIndicador_Sigla ;
      private String[] P009K4_A1345ContratoServicosIndicador_CalculoSob ;
      private bool[] P009K4_n1345ContratoServicosIndicador_CalculoSob ;
      private String[] P009K4_A1310ContratoServicosIndicador_Vigencia ;
      private bool[] P009K4_n1310ContratoServicosIndicador_Vigencia ;
      private String[] P009K4_A1309ContratoServicosIndicador_Periodicidade ;
      private bool[] P009K4_n1309ContratoServicosIndicador_Periodicidade ;
      private String[] P009K4_A1308ContratoServicosIndicador_Tipo ;
      private bool[] P009K4_n1308ContratoServicosIndicador_Tipo ;
      private String[] P009K4_A1307ContratoServicosIndicador_InstrumentoMedicao ;
      private bool[] P009K4_n1307ContratoServicosIndicador_InstrumentoMedicao ;
      private String[] P009K4_A1306ContratoServicosIndicador_Meta ;
      private bool[] P009K4_n1306ContratoServicosIndicador_Meta ;
      private String[] P009K4_A1305ContratoServicosIndicador_Finalidade ;
      private bool[] P009K4_n1305ContratoServicosIndicador_Finalidade ;
      private String[] P009K4_A1274ContratoServicosIndicador_Indicador ;
      private short[] P009K4_A1271ContratoServicosIndicador_Numero ;
      private int[] P009K4_A1269ContratoServicosIndicador_Codigo ;
      private int[] P009K5_A1269ContratoServicosIndicador_Codigo ;
      private int[] P009K6_A1269ContratoServicosIndicador_Codigo ;
      private int[] P009K7_A1269ContratoServicosIndicador_Codigo ;
      private decimal[] P009K7_A1301ContratoServicosIndicadorFaixa_Desde ;
      private decimal[] P009K7_A1302ContratoServicosIndicadorFaixa_Ate ;
      private decimal[] P009K7_A1303ContratoServicosIndicadorFaixa_Reduz ;
      private short[] P009K7_A1304ContratoServicosIndicadorFaixa_Und ;
      private int[] P009K7_A1299ContratoServicosIndicadorFaixa_Codigo ;
   }

   public class prc_clonarindicador__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new UpdateCursor(def[6])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP009K2 ;
          prmP009K2 = new Object[] {
          new Object[] {"@AV9Indicador",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009K3 ;
          prmP009K3 = new Object[] {
          new Object[] {"@ContratoServicosIndicador_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009K4 ;
          prmP009K4 = new Object[] {
          new Object[] {"@AV9Indicador",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009K5 ;
          prmP009K5 = new Object[] {
          new Object[] {"@ContratoServicosIndicador_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosIndicador_Numero",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicosIndicador_Indicador",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContratoServicosIndicador_Finalidade",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContratoServicosIndicador_Meta",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContratoServicosIndicador_InstrumentoMedicao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContratoServicosIndicador_Tipo",SqlDbType.Char,2,0} ,
          new Object[] {"@ContratoServicosIndicador_Periodicidade",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicosIndicador_Vigencia",SqlDbType.VarChar,50,0} ,
          new Object[] {"@ContratoServicosIndicador_CalculoSob",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicosIndicador_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@ContratoServicosIndicador_Formato",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmP009K6 ;
          prmP009K6 = new Object[] {
          new Object[] {"@AV9Indicador",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009K7 ;
          prmP009K7 = new Object[] {
          new Object[] {"@ContratoServicosIndicador_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009K8 ;
          prmP009K8 = new Object[] {
          new Object[] {"@ContratoServicosIndicador_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosIndicadorFaixa_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosIndicadorFaixa_Numero",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicosIndicadorFaixa_Desde",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContratoServicosIndicadorFaixa_Ate",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContratoServicosIndicadorFaixa_Reduz",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContratoServicosIndicadorFaixa_Und",SqlDbType.SmallInt,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P009K2", "SELECT TOP 1 [ContratoServicosIndicador_Codigo] FROM [ContratoServicosIndicador] WITH (NOLOCK) WHERE [ContratoServicosIndicador_Codigo] = @AV9Indicador ORDER BY [ContratoServicosIndicador_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009K2,1,0,true,true )
             ,new CursorDef("P009K3", "SELECT [ContratoServicosIndicador_Codigo], [ContratoServicosIndicadorFaixa_Desde], [ContratoServicosIndicadorFaixa_Ate], [ContratoServicosIndicadorFaixa_Reduz], [ContratoServicosIndicadorFaixa_Und], [ContratoServicosIndicadorFaixa_Codigo] FROM [ContratoServicosIndicadorFaixas] WITH (NOLOCK) WHERE [ContratoServicosIndicador_Codigo] = @ContratoServicosIndicador_Codigo ORDER BY [ContratoServicosIndicador_Codigo], [ContratoServicosIndicadorFaixa_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009K3,100,0,true,false )
             ,new CursorDef("P009K4", "SELECT TOP 1 [ContratoServicosIndicador_CntSrvCod], [ContratoServicosIndicador_Formato], [ContratoServicosIndicador_Sigla], [ContratoServicosIndicador_CalculoSob], [ContratoServicosIndicador_Vigencia], [ContratoServicosIndicador_Periodicidade], [ContratoServicosIndicador_Tipo], [ContratoServicosIndicador_InstrumentoMedicao], [ContratoServicosIndicador_Meta], [ContratoServicosIndicador_Finalidade], [ContratoServicosIndicador_Indicador], [ContratoServicosIndicador_Numero], [ContratoServicosIndicador_Codigo] FROM [ContratoServicosIndicador] WITH (NOLOCK) WHERE [ContratoServicosIndicador_Codigo] = @AV9Indicador ORDER BY [ContratoServicosIndicador_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009K4,1,0,true,true )
             ,new CursorDef("P009K5", "INSERT INTO [ContratoServicosIndicador]([ContratoServicosIndicador_CntSrvCod], [ContratoServicosIndicador_Numero], [ContratoServicosIndicador_Indicador], [ContratoServicosIndicador_Finalidade], [ContratoServicosIndicador_Meta], [ContratoServicosIndicador_InstrumentoMedicao], [ContratoServicosIndicador_Tipo], [ContratoServicosIndicador_Periodicidade], [ContratoServicosIndicador_Vigencia], [ContratoServicosIndicador_CalculoSob], [ContratoServicosIndicador_Sigla], [ContratoServicosIndicador_Formato]) VALUES(@ContratoServicosIndicador_CntSrvCod, @ContratoServicosIndicador_Numero, @ContratoServicosIndicador_Indicador, @ContratoServicosIndicador_Finalidade, @ContratoServicosIndicador_Meta, @ContratoServicosIndicador_InstrumentoMedicao, @ContratoServicosIndicador_Tipo, @ContratoServicosIndicador_Periodicidade, @ContratoServicosIndicador_Vigencia, @ContratoServicosIndicador_CalculoSob, @ContratoServicosIndicador_Sigla, @ContratoServicosIndicador_Formato); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmP009K5)
             ,new CursorDef("P009K6", "SELECT TOP 1 [ContratoServicosIndicador_Codigo] FROM [ContratoServicosIndicador] WITH (NOLOCK) WHERE [ContratoServicosIndicador_Codigo] = @AV9Indicador ORDER BY [ContratoServicosIndicador_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009K6,1,0,true,true )
             ,new CursorDef("P009K7", "SELECT [ContratoServicosIndicador_Codigo], [ContratoServicosIndicadorFaixa_Desde], [ContratoServicosIndicadorFaixa_Ate], [ContratoServicosIndicadorFaixa_Reduz], [ContratoServicosIndicadorFaixa_Und], [ContratoServicosIndicadorFaixa_Codigo] FROM [ContratoServicosIndicadorFaixas] WITH (NOLOCK) WHERE [ContratoServicosIndicador_Codigo] = @ContratoServicosIndicador_Codigo ORDER BY [ContratoServicosIndicador_Codigo], [ContratoServicosIndicadorFaixa_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009K7,100,0,true,false )
             ,new CursorDef("P009K8", "INSERT INTO [ContratoServicosIndicadorFaixas]([ContratoServicosIndicador_Codigo], [ContratoServicosIndicadorFaixa_Codigo], [ContratoServicosIndicadorFaixa_Numero], [ContratoServicosIndicadorFaixa_Desde], [ContratoServicosIndicadorFaixa_Ate], [ContratoServicosIndicadorFaixa_Reduz], [ContratoServicosIndicadorFaixa_Und]) VALUES(@ContratoServicosIndicador_Codigo, @ContratoServicosIndicadorFaixa_Codigo, @ContratoServicosIndicadorFaixa_Numero, @ContratoServicosIndicadorFaixa_Desde, @ContratoServicosIndicadorFaixa_Ate, @ContratoServicosIndicadorFaixa_Reduz, @ContratoServicosIndicadorFaixa_Und)", GxErrorMask.GX_NOMASK,prmP009K8)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 2) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getLongVarchar(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getLongVarchar(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getLongVarchar(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getLongVarchar(11) ;
                ((short[]) buf[19])[0] = rslt.getShort(12) ;
                ((int[]) buf[20])[0] = rslt.getInt(13) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 8 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 9 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(9, (String)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 10 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(10, (String)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 11 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(11, (String)parms[18]);
                }
                stmt.SetParameter(12, (short)parms[19]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                stmt.SetParameter(4, (decimal)parms[3]);
                stmt.SetParameter(5, (decimal)parms[4]);
                stmt.SetParameter(6, (decimal)parms[5]);
                stmt.SetParameter(7, (short)parms[6]);
                return;
       }
    }

 }

}
