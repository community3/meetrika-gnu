/*
               File: PRC_PendenciasDaOS
        Description: Pendencias da OS e vinculadas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:8:30.25
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_pendenciasdaos : GXProcedure
   {
      public prc_pendenciasdaos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_pendenciasdaos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo ,
                           out short aP1_Retorno )
      {
         this.AV11ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV10Retorno = 0 ;
         initialize();
         executePrivate();
         aP1_Retorno=this.AV10Retorno;
      }

      public short executeUdp( int aP0_ContagemResultado_Codigo )
      {
         this.AV11ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV10Retorno = 0 ;
         initialize();
         executePrivate();
         aP1_Retorno=this.AV10Retorno;
         return AV10Retorno ;
      }

      public void executeSubmit( int aP0_ContagemResultado_Codigo ,
                                 out short aP1_Retorno )
      {
         prc_pendenciasdaos objprc_pendenciasdaos;
         objprc_pendenciasdaos = new prc_pendenciasdaos();
         objprc_pendenciasdaos.AV11ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_pendenciasdaos.AV10Retorno = 0 ;
         objprc_pendenciasdaos.context.SetSubmitInitialConfig(context);
         objprc_pendenciasdaos.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_pendenciasdaos);
         aP1_Retorno=this.AV10Retorno;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_pendenciasdaos)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P006C2 */
         pr_default.execute(0, new Object[] {AV11ContagemResultado_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A456ContagemResultado_Codigo = P006C2_A456ContagemResultado_Codigo[0];
            A602ContagemResultado_OSVinculada = P006C2_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P006C2_n602ContagemResultado_OSVinculada[0];
            AV12ContagemResultado_OSVinculada = A602ContagemResultado_OSVinculada;
            /* Optimized group. */
            /* Using cursor P006C3 */
            pr_default.execute(1, new Object[] {A456ContagemResultado_Codigo});
            cV10Retorno = P006C3_AV10Retorno[0];
            pr_default.close(1);
            AV10Retorno = (short)(AV10Retorno+cV10Retorno*1);
            /* End optimized group. */
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         if ( ! (0==AV12ContagemResultado_OSVinculada) )
         {
            /* Using cursor P006C4 */
            pr_default.execute(2, new Object[] {AV12ContagemResultado_OSVinculada});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A484ContagemResultado_StatusDmn = P006C4_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = P006C4_n484ContagemResultado_StatusDmn[0];
               A456ContagemResultado_Codigo = P006C4_A456ContagemResultado_Codigo[0];
               AV10Retorno = (short)(AV10Retorno+1);
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(2);
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P006C2_A456ContagemResultado_Codigo = new int[1] ;
         P006C2_A602ContagemResultado_OSVinculada = new int[1] ;
         P006C2_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P006C3_AV10Retorno = new short[1] ;
         P006C4_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P006C4_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P006C4_A456ContagemResultado_Codigo = new int[1] ;
         A484ContagemResultado_StatusDmn = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_pendenciasdaos__default(),
            new Object[][] {
                new Object[] {
               P006C2_A456ContagemResultado_Codigo, P006C2_A602ContagemResultado_OSVinculada, P006C2_n602ContagemResultado_OSVinculada
               }
               , new Object[] {
               P006C3_AV10Retorno
               }
               , new Object[] {
               P006C4_A484ContagemResultado_StatusDmn, P006C4_n484ContagemResultado_StatusDmn, P006C4_A456ContagemResultado_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV10Retorno ;
      private short cV10Retorno ;
      private int AV11ContagemResultado_Codigo ;
      private int A456ContagemResultado_Codigo ;
      private int A602ContagemResultado_OSVinculada ;
      private int AV12ContagemResultado_OSVinculada ;
      private String scmdbuf ;
      private String A484ContagemResultado_StatusDmn ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n484ContagemResultado_StatusDmn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P006C2_A456ContagemResultado_Codigo ;
      private int[] P006C2_A602ContagemResultado_OSVinculada ;
      private bool[] P006C2_n602ContagemResultado_OSVinculada ;
      private short[] P006C3_AV10Retorno ;
      private String[] P006C4_A484ContagemResultado_StatusDmn ;
      private bool[] P006C4_n484ContagemResultado_StatusDmn ;
      private int[] P006C4_A456ContagemResultado_Codigo ;
      private short aP1_Retorno ;
   }

   public class prc_pendenciasdaos__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP006C2 ;
          prmP006C2 = new Object[] {
          new Object[] {"@AV11ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006C3 ;
          prmP006C3 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006C4 ;
          prmP006C4 = new Object[] {
          new Object[] {"@AV12ContagemResultado_OSVinculada",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P006C2", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_OSVinculada] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @AV11ContagemResultado_Codigo ORDER BY [ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006C2,1,0,true,true )
             ,new CursorDef("P006C3", "SELECT COUNT(*) FROM [ContagemResultadoErro] WITH (NOLOCK) WHERE ([ContagemResultado_Codigo] = @ContagemResultado_Codigo) AND ([ContagemResultadoErro_Status] = 'P' or [ContagemResultadoErro_Status] = 'E') ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006C3,1,0,true,false )
             ,new CursorDef("P006C4", "SELECT TOP 1 [ContagemResultado_StatusDmn], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE ([ContagemResultado_Codigo] = @AV12ContagemResultado_OSVinculada) AND ([ContagemResultado_StatusDmn] = 'A' or [ContagemResultado_StatusDmn] = 'D') ORDER BY [ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006C4,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 1 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
