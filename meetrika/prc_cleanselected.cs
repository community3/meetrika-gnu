/*
               File: PRC_CleanSelected
        Description: Clean Selected do Usu�rio
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:7:43.95
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_cleanselected : GXProcedure
   {
      public prc_cleanselected( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_cleanselected( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Usuario_Codigo )
      {
         this.A1Usuario_Codigo = aP0_Usuario_Codigo;
         initialize();
         executePrivate();
         aP0_Usuario_Codigo=this.A1Usuario_Codigo;
      }

      public int executeUdp( )
      {
         this.A1Usuario_Codigo = aP0_Usuario_Codigo;
         initialize();
         executePrivate();
         aP0_Usuario_Codigo=this.A1Usuario_Codigo;
         return A1Usuario_Codigo ;
      }

      public void executeSubmit( ref int aP0_Usuario_Codigo )
      {
         prc_cleanselected objprc_cleanselected;
         objprc_cleanselected = new prc_cleanselected();
         objprc_cleanselected.A1Usuario_Codigo = aP0_Usuario_Codigo;
         objprc_cleanselected.context.SetSubmitInitialConfig(context);
         objprc_cleanselected.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_cleanselected);
         aP0_Usuario_Codigo=this.A1Usuario_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_cleanselected)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P002W2 */
         pr_default.execute(0, new Object[] {A1Usuario_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A487Selected_Codigo = P002W2_A487Selected_Codigo[0];
            A488Selected_Flag = P002W2_A488Selected_Flag[0];
            BatchSize = 100;
            pr_default.initializeBatch( 1, BatchSize, this, "Executebatchp002w3");
            /* Using cursor P002W3 */
            pr_default.addRecord(1, new Object[] {A1Usuario_Codigo, A487Selected_Codigo, A488Selected_Flag});
            if ( pr_default.recordCount(1) == pr_default.getBatchSize(1) )
            {
               Executebatchp002w3( ) ;
            }
            dsDefault.SmartCacheProvider.SetUpdated("Selected") ;
            pr_default.readNext(0);
         }
         if ( pr_default.getBatchSize(1) > 0 )
         {
            Executebatchp002w3( ) ;
         }
         pr_default.close(0);
         this.cleanup();
      }

      protected void Executebatchp002w3( )
      {
         /* Using cursor P002W3 */
         pr_default.executeBatch(1);
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_CleanSelected");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P002W2_A1Usuario_Codigo = new int[1] ;
         P002W2_A487Selected_Codigo = new int[1] ;
         P002W2_A488Selected_Flag = new bool[] {false} ;
         P002W3_A1Usuario_Codigo = new int[1] ;
         P002W3_A487Selected_Codigo = new int[1] ;
         P002W3_A488Selected_Flag = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_cleanselected__default(),
            new Object[][] {
                new Object[] {
               P002W2_A1Usuario_Codigo, P002W2_A487Selected_Codigo, P002W2_A488Selected_Flag
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A1Usuario_Codigo ;
      private int A487Selected_Codigo ;
      private int BatchSize ;
      private String scmdbuf ;
      private bool A488Selected_Flag ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Usuario_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P002W2_A1Usuario_Codigo ;
      private int[] P002W2_A487Selected_Codigo ;
      private bool[] P002W2_A488Selected_Flag ;
      private int[] P002W3_A1Usuario_Codigo ;
      private int[] P002W3_A487Selected_Codigo ;
      private bool[] P002W3_A488Selected_Flag ;
   }

   public class prc_cleanselected__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new BatchUpdateCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP002W2 ;
          prmP002W2 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP002W3 ;
          prmP002W3 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Selected_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Selected_Flag",SqlDbType.Bit,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P002W2", "SELECT [Usuario_Codigo], [Selected_Codigo], [Selected_Flag] FROM [Selected] WITH (UPDLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo ORDER BY [Usuario_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP002W2,1,0,true,false )
             ,new CursorDef("P002W3", "DELETE FROM [Selected]  WHERE [Usuario_Codigo] = @Usuario_Codigo AND [Selected_Codigo] = @Selected_Codigo AND [Selected_Flag] = @Selected_Flag", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP002W3)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (bool)parms[2]);
                return;
       }
    }

 }

}
