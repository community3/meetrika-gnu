/*
               File: GetExtraWWContagemResultadoEmGarantiaFilterData
        Description: Get Extra WWContagem Resultado Em Garantia Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/24/2020 23:7:51.98
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getextrawwcontagemresultadoemgarantiafilterdata : GXProcedure
   {
      public getextrawwcontagemresultadoemgarantiafilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getextrawwcontagemresultadoemgarantiafilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV37DDOName = aP0_DDOName;
         this.AV35SearchTxt = aP1_SearchTxt;
         this.AV36SearchTxtTo = aP2_SearchTxtTo;
         this.AV41OptionsJson = "" ;
         this.AV44OptionsDescJson = "" ;
         this.AV46OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV41OptionsJson;
         aP4_OptionsDescJson=this.AV44OptionsDescJson;
         aP5_OptionIndexesJson=this.AV46OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV37DDOName = aP0_DDOName;
         this.AV35SearchTxt = aP1_SearchTxt;
         this.AV36SearchTxtTo = aP2_SearchTxtTo;
         this.AV41OptionsJson = "" ;
         this.AV44OptionsDescJson = "" ;
         this.AV46OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV41OptionsJson;
         aP4_OptionsDescJson=this.AV44OptionsDescJson;
         aP5_OptionIndexesJson=this.AV46OptionIndexesJson;
         return AV46OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getextrawwcontagemresultadoemgarantiafilterdata objgetextrawwcontagemresultadoemgarantiafilterdata;
         objgetextrawwcontagemresultadoemgarantiafilterdata = new getextrawwcontagemresultadoemgarantiafilterdata();
         objgetextrawwcontagemresultadoemgarantiafilterdata.AV37DDOName = aP0_DDOName;
         objgetextrawwcontagemresultadoemgarantiafilterdata.AV35SearchTxt = aP1_SearchTxt;
         objgetextrawwcontagemresultadoemgarantiafilterdata.AV36SearchTxtTo = aP2_SearchTxtTo;
         objgetextrawwcontagemresultadoemgarantiafilterdata.AV41OptionsJson = "" ;
         objgetextrawwcontagemresultadoemgarantiafilterdata.AV44OptionsDescJson = "" ;
         objgetextrawwcontagemresultadoemgarantiafilterdata.AV46OptionIndexesJson = "" ;
         objgetextrawwcontagemresultadoemgarantiafilterdata.context.SetSubmitInitialConfig(context);
         objgetextrawwcontagemresultadoemgarantiafilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetextrawwcontagemresultadoemgarantiafilterdata);
         aP3_OptionsJson=this.AV41OptionsJson;
         aP4_OptionsDescJson=this.AV44OptionsDescJson;
         aP5_OptionIndexesJson=this.AV46OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getextrawwcontagemresultadoemgarantiafilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV40Options = (IGxCollection)(new GxSimpleCollection());
         AV43OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV45OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV37DDOName), "DDO_CONTAGEMRESULTADO_DEMANDAFM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMRESULTADO_DEMANDAFMOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV37DDOName), "DDO_CONTAGEMRESULTADO_DEMANDA") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMRESULTADO_DEMANDAOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV37DDOName), "DDO_CONTAGEMRESULTADO_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMRESULTADO_DESCRICAOOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV37DDOName), "DDO_CONTAGEMRESULTADO_CONTRATADASIGLA") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMRESULTADO_CONTRATADASIGLAOPTIONS' */
            S151 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV37DDOName), "DDO_CONTAGEMRESULTADO_SERVICOSIGLA") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMRESULTADO_SERVICOSIGLAOPTIONS' */
            S161 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV41OptionsJson = AV40Options.ToJSonString(false);
         AV44OptionsDescJson = AV43OptionsDesc.ToJSonString(false);
         AV46OptionIndexesJson = AV45OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV48Session.Get("ExtraWWContagemResultadoEmGarantiaGridState"), "") == 0 )
         {
            AV50GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "ExtraWWContagemResultadoEmGarantiaGridState"), "");
         }
         else
         {
            AV50GridState.FromXml(AV48Session.Get("ExtraWWContagemResultadoEmGarantiaGridState"), "");
         }
         AV176GXV1 = 1;
         while ( AV176GXV1 <= AV50GridState.gxTpr_Filtervalues.Count )
         {
            AV51GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV50GridState.gxTpr_Filtervalues.Item(AV176GXV1));
            if ( StringUtil.StrCmp(AV51GridStateFilterValue.gxTpr_Name, "CONTRATADA_AREATRABALHOCOD") == 0 )
            {
               AV53Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( AV51GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV51GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DEMANDAFM") == 0 )
            {
               AV12TFContagemResultado_DemandaFM = AV51GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV51GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DEMANDAFM_SEL") == 0 )
            {
               AV13TFContagemResultado_DemandaFM_Sel = AV51GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV51GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DEMANDA") == 0 )
            {
               AV14TFContagemResultado_Demanda = AV51GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV51GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DEMANDA_SEL") == 0 )
            {
               AV15TFContagemResultado_Demanda_Sel = AV51GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV51GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DESCRICAO") == 0 )
            {
               AV16TFContagemResultado_Descricao = AV51GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV51GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DESCRICAO_SEL") == 0 )
            {
               AV17TFContagemResultado_Descricao_Sel = AV51GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV51GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DATADMN") == 0 )
            {
               AV18TFContagemResultado_DataDmn = context.localUtil.CToD( AV51GridStateFilterValue.gxTpr_Value, 2);
               AV19TFContagemResultado_DataDmn_To = context.localUtil.CToD( AV51GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV51GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DATAULTCNT") == 0 )
            {
               AV20TFContagemResultado_DataUltCnt = context.localUtil.CToD( AV51GridStateFilterValue.gxTpr_Value, 2);
               AV21TFContagemResultado_DataUltCnt_To = context.localUtil.CToD( AV51GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV51GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_CONTRATADASIGLA") == 0 )
            {
               AV22TFContagemResultado_ContratadaSigla = AV51GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV51GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_CONTRATADASIGLA_SEL") == 0 )
            {
               AV23TFContagemResultado_ContratadaSigla_Sel = AV51GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV51GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_STATUSDMN_SEL") == 0 )
            {
               AV26TFContagemResultado_StatusDmn_SelsJson = AV51GridStateFilterValue.gxTpr_Value;
               AV27TFContagemResultado_StatusDmn_Sels.FromJSonString(AV26TFContagemResultado_StatusDmn_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV51GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_SERVICOSIGLA") == 0 )
            {
               AV29TFContagemResultado_ServicoSigla = AV51GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV51GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_SERVICOSIGLA_SEL") == 0 )
            {
               AV30TFContagemResultado_ServicoSigla_Sel = AV51GridStateFilterValue.gxTpr_Value;
            }
            AV176GXV1 = (int)(AV176GXV1+1);
         }
         if ( AV50GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV52GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV50GridState.gxTpr_Dynamicfilters.Item(1));
            AV55DynamicFiltersSelector1 = AV52GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV55DynamicFiltersSelector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
            {
               AV56DynamicFiltersOperator1 = AV52GridStateDynamicFilter.gxTpr_Operator;
               AV60ContagemResultado_OsFsOsFm1 = AV52GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV55DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 )
            {
               AV61ContagemResultado_DataDmn1 = context.localUtil.CToD( AV52GridStateDynamicFilter.gxTpr_Value, 2);
               AV62ContagemResultado_DataDmn_To1 = context.localUtil.CToD( AV52GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV55DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATACNT") == 0 )
            {
               AV57ContagemResultado_DataCnt1 = context.localUtil.CToD( AV52GridStateDynamicFilter.gxTpr_Value, 2);
               AV58ContagemResultado_DataCnt_To1 = context.localUtil.CToD( AV52GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV55DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
            {
               AV63ContagemResultado_DataPrevista1 = context.localUtil.CToT( AV52GridStateDynamicFilter.gxTpr_Value, 2);
               AV64ContagemResultado_DataPrevista_To1 = context.localUtil.CToT( AV52GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV55DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
            {
               AV59ContagemResultado_StatusDmn1 = AV52GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV55DynamicFiltersSelector1, "CONTAGEMRESULTADO_SERVICO") == 0 )
            {
               AV65ContagemResultado_Servico1 = (int)(NumberUtil.Val( AV52GridStateDynamicFilter.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV55DynamicFiltersSelector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
            {
               AV67ContagemResultado_SistemaCod1 = (int)(NumberUtil.Val( AV52GridStateDynamicFilter.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV55DynamicFiltersSelector1, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
            {
               AV68ContagemResultado_ContratadaCod1 = (int)(NumberUtil.Val( AV52GridStateDynamicFilter.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV55DynamicFiltersSelector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
            {
               AV75ContagemResultado_Descricao1 = AV52GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV55DynamicFiltersSelector1, "CONTAGEMRESULTADO_OWNER") == 0 )
            {
               AV76ContagemResultado_Owner1 = (int)(NumberUtil.Val( AV52GridStateDynamicFilter.gxTpr_Value, "."));
            }
            if ( AV50GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV78DynamicFiltersEnabled2 = true;
               AV52GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV50GridState.gxTpr_Dynamicfilters.Item(2));
               AV79DynamicFiltersSelector2 = AV52GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV79DynamicFiltersSelector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
               {
                  AV80DynamicFiltersOperator2 = AV52GridStateDynamicFilter.gxTpr_Operator;
                  AV84ContagemResultado_OsFsOsFm2 = AV52GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV79DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 )
               {
                  AV85ContagemResultado_DataDmn2 = context.localUtil.CToD( AV52GridStateDynamicFilter.gxTpr_Value, 2);
                  AV86ContagemResultado_DataDmn_To2 = context.localUtil.CToD( AV52GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               else if ( StringUtil.StrCmp(AV79DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATACNT") == 0 )
               {
                  AV81ContagemResultado_DataCnt2 = context.localUtil.CToD( AV52GridStateDynamicFilter.gxTpr_Value, 2);
                  AV82ContagemResultado_DataCnt_To2 = context.localUtil.CToD( AV52GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               else if ( StringUtil.StrCmp(AV79DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
               {
                  AV87ContagemResultado_DataPrevista2 = context.localUtil.CToT( AV52GridStateDynamicFilter.gxTpr_Value, 2);
                  AV88ContagemResultado_DataPrevista_To2 = context.localUtil.CToT( AV52GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               else if ( StringUtil.StrCmp(AV79DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
               {
                  AV83ContagemResultado_StatusDmn2 = AV52GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV79DynamicFiltersSelector2, "CONTAGEMRESULTADO_SERVICO") == 0 )
               {
                  AV89ContagemResultado_Servico2 = (int)(NumberUtil.Val( AV52GridStateDynamicFilter.gxTpr_Value, "."));
               }
               else if ( StringUtil.StrCmp(AV79DynamicFiltersSelector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
               {
                  AV91ContagemResultado_SistemaCod2 = (int)(NumberUtil.Val( AV52GridStateDynamicFilter.gxTpr_Value, "."));
               }
               else if ( StringUtil.StrCmp(AV79DynamicFiltersSelector2, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
               {
                  AV92ContagemResultado_ContratadaCod2 = (int)(NumberUtil.Val( AV52GridStateDynamicFilter.gxTpr_Value, "."));
               }
               else if ( StringUtil.StrCmp(AV79DynamicFiltersSelector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
               {
                  AV99ContagemResultado_Descricao2 = AV52GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV79DynamicFiltersSelector2, "CONTAGEMRESULTADO_OWNER") == 0 )
               {
                  AV100ContagemResultado_Owner2 = (int)(NumberUtil.Val( AV52GridStateDynamicFilter.gxTpr_Value, "."));
               }
               if ( AV50GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV102DynamicFiltersEnabled3 = true;
                  AV52GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV50GridState.gxTpr_Dynamicfilters.Item(3));
                  AV103DynamicFiltersSelector3 = AV52GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV103DynamicFiltersSelector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
                  {
                     AV104DynamicFiltersOperator3 = AV52GridStateDynamicFilter.gxTpr_Operator;
                     AV108ContagemResultado_OsFsOsFm3 = AV52GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV103DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 )
                  {
                     AV109ContagemResultado_DataDmn3 = context.localUtil.CToD( AV52GridStateDynamicFilter.gxTpr_Value, 2);
                     AV110ContagemResultado_DataDmn_To3 = context.localUtil.CToD( AV52GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
                  else if ( StringUtil.StrCmp(AV103DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATACNT") == 0 )
                  {
                     AV105ContagemResultado_DataCnt3 = context.localUtil.CToD( AV52GridStateDynamicFilter.gxTpr_Value, 2);
                     AV106ContagemResultado_DataCnt_To3 = context.localUtil.CToD( AV52GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
                  else if ( StringUtil.StrCmp(AV103DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
                  {
                     AV111ContagemResultado_DataPrevista3 = context.localUtil.CToT( AV52GridStateDynamicFilter.gxTpr_Value, 2);
                     AV112ContagemResultado_DataPrevista_To3 = context.localUtil.CToT( AV52GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
                  else if ( StringUtil.StrCmp(AV103DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
                  {
                     AV107ContagemResultado_StatusDmn3 = AV52GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV103DynamicFiltersSelector3, "CONTAGEMRESULTADO_SERVICO") == 0 )
                  {
                     AV113ContagemResultado_Servico3 = (int)(NumberUtil.Val( AV52GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  else if ( StringUtil.StrCmp(AV103DynamicFiltersSelector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
                  {
                     AV115ContagemResultado_SistemaCod3 = (int)(NumberUtil.Val( AV52GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  else if ( StringUtil.StrCmp(AV103DynamicFiltersSelector3, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
                  {
                     AV116ContagemResultado_ContratadaCod3 = (int)(NumberUtil.Val( AV52GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  else if ( StringUtil.StrCmp(AV103DynamicFiltersSelector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
                  {
                     AV123ContagemResultado_Descricao3 = AV52GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV103DynamicFiltersSelector3, "CONTAGEMRESULTADO_OWNER") == 0 )
                  {
                     AV124ContagemResultado_Owner3 = (int)(NumberUtil.Val( AV52GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  if ( AV50GridState.gxTpr_Dynamicfilters.Count >= 4 )
                  {
                     AV126DynamicFiltersEnabled4 = true;
                     AV52GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV50GridState.gxTpr_Dynamicfilters.Item(4));
                     AV127DynamicFiltersSelector4 = AV52GridStateDynamicFilter.gxTpr_Selected;
                     if ( StringUtil.StrCmp(AV127DynamicFiltersSelector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
                     {
                        AV128DynamicFiltersOperator4 = AV52GridStateDynamicFilter.gxTpr_Operator;
                        AV132ContagemResultado_OsFsOsFm4 = AV52GridStateDynamicFilter.gxTpr_Value;
                     }
                     else if ( StringUtil.StrCmp(AV127DynamicFiltersSelector4, "CONTAGEMRESULTADO_DATADMN") == 0 )
                     {
                        AV133ContagemResultado_DataDmn4 = context.localUtil.CToD( AV52GridStateDynamicFilter.gxTpr_Value, 2);
                        AV134ContagemResultado_DataDmn_To4 = context.localUtil.CToD( AV52GridStateDynamicFilter.gxTpr_Valueto, 2);
                     }
                     else if ( StringUtil.StrCmp(AV127DynamicFiltersSelector4, "CONTAGEMRESULTADO_DATACNT") == 0 )
                     {
                        AV129ContagemResultado_DataCnt4 = context.localUtil.CToD( AV52GridStateDynamicFilter.gxTpr_Value, 2);
                        AV130ContagemResultado_DataCnt_To4 = context.localUtil.CToD( AV52GridStateDynamicFilter.gxTpr_Valueto, 2);
                     }
                     else if ( StringUtil.StrCmp(AV127DynamicFiltersSelector4, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
                     {
                        AV135ContagemResultado_DataPrevista4 = context.localUtil.CToT( AV52GridStateDynamicFilter.gxTpr_Value, 2);
                        AV136ContagemResultado_DataPrevista_To4 = context.localUtil.CToT( AV52GridStateDynamicFilter.gxTpr_Valueto, 2);
                     }
                     else if ( StringUtil.StrCmp(AV127DynamicFiltersSelector4, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
                     {
                        AV131ContagemResultado_StatusDmn4 = AV52GridStateDynamicFilter.gxTpr_Value;
                     }
                     else if ( StringUtil.StrCmp(AV127DynamicFiltersSelector4, "CONTAGEMRESULTADO_SERVICO") == 0 )
                     {
                        AV137ContagemResultado_Servico4 = (int)(NumberUtil.Val( AV52GridStateDynamicFilter.gxTpr_Value, "."));
                     }
                     else if ( StringUtil.StrCmp(AV127DynamicFiltersSelector4, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
                     {
                        AV139ContagemResultado_SistemaCod4 = (int)(NumberUtil.Val( AV52GridStateDynamicFilter.gxTpr_Value, "."));
                     }
                     else if ( StringUtil.StrCmp(AV127DynamicFiltersSelector4, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
                     {
                        AV140ContagemResultado_ContratadaCod4 = (int)(NumberUtil.Val( AV52GridStateDynamicFilter.gxTpr_Value, "."));
                     }
                     else if ( StringUtil.StrCmp(AV127DynamicFiltersSelector4, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
                     {
                        AV147ContagemResultado_Descricao4 = AV52GridStateDynamicFilter.gxTpr_Value;
                     }
                     else if ( StringUtil.StrCmp(AV127DynamicFiltersSelector4, "CONTAGEMRESULTADO_OWNER") == 0 )
                     {
                        AV148ContagemResultado_Owner4 = (int)(NumberUtil.Val( AV52GridStateDynamicFilter.gxTpr_Value, "."));
                     }
                     if ( AV50GridState.gxTpr_Dynamicfilters.Count >= 5 )
                     {
                        AV150DynamicFiltersEnabled5 = true;
                        AV52GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV50GridState.gxTpr_Dynamicfilters.Item(5));
                        AV151DynamicFiltersSelector5 = AV52GridStateDynamicFilter.gxTpr_Selected;
                        if ( StringUtil.StrCmp(AV151DynamicFiltersSelector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
                        {
                           AV152DynamicFiltersOperator5 = AV52GridStateDynamicFilter.gxTpr_Operator;
                           AV156ContagemResultado_OsFsOsFm5 = AV52GridStateDynamicFilter.gxTpr_Value;
                        }
                        else if ( StringUtil.StrCmp(AV151DynamicFiltersSelector5, "CONTAGEMRESULTADO_DATADMN") == 0 )
                        {
                           AV157ContagemResultado_DataDmn5 = context.localUtil.CToD( AV52GridStateDynamicFilter.gxTpr_Value, 2);
                           AV158ContagemResultado_DataDmn_To5 = context.localUtil.CToD( AV52GridStateDynamicFilter.gxTpr_Valueto, 2);
                        }
                        else if ( StringUtil.StrCmp(AV151DynamicFiltersSelector5, "CONTAGEMRESULTADO_DATACNT") == 0 )
                        {
                           AV153ContagemResultado_DataCnt5 = context.localUtil.CToD( AV52GridStateDynamicFilter.gxTpr_Value, 2);
                           AV154ContagemResultado_DataCnt_To5 = context.localUtil.CToD( AV52GridStateDynamicFilter.gxTpr_Valueto, 2);
                        }
                        else if ( StringUtil.StrCmp(AV151DynamicFiltersSelector5, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
                        {
                           AV159ContagemResultado_DataPrevista5 = context.localUtil.CToT( AV52GridStateDynamicFilter.gxTpr_Value, 2);
                           AV160ContagemResultado_DataPrevista_To5 = context.localUtil.CToT( AV52GridStateDynamicFilter.gxTpr_Valueto, 2);
                        }
                        else if ( StringUtil.StrCmp(AV151DynamicFiltersSelector5, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
                        {
                           AV155ContagemResultado_StatusDmn5 = AV52GridStateDynamicFilter.gxTpr_Value;
                        }
                        else if ( StringUtil.StrCmp(AV151DynamicFiltersSelector5, "CONTAGEMRESULTADO_SERVICO") == 0 )
                        {
                           AV161ContagemResultado_Servico5 = (int)(NumberUtil.Val( AV52GridStateDynamicFilter.gxTpr_Value, "."));
                        }
                        else if ( StringUtil.StrCmp(AV151DynamicFiltersSelector5, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
                        {
                           AV163ContagemResultado_SistemaCod5 = (int)(NumberUtil.Val( AV52GridStateDynamicFilter.gxTpr_Value, "."));
                        }
                        else if ( StringUtil.StrCmp(AV151DynamicFiltersSelector5, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
                        {
                           AV164ContagemResultado_ContratadaCod5 = (int)(NumberUtil.Val( AV52GridStateDynamicFilter.gxTpr_Value, "."));
                        }
                        else if ( StringUtil.StrCmp(AV151DynamicFiltersSelector5, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
                        {
                           AV171ContagemResultado_Descricao5 = AV52GridStateDynamicFilter.gxTpr_Value;
                        }
                        else if ( StringUtil.StrCmp(AV151DynamicFiltersSelector5, "CONTAGEMRESULTADO_OWNER") == 0 )
                        {
                           AV172ContagemResultado_Owner5 = (int)(NumberUtil.Val( AV52GridStateDynamicFilter.gxTpr_Value, "."));
                        }
                     }
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTAGEMRESULTADO_DEMANDAFMOPTIONS' Routine */
         AV12TFContagemResultado_DemandaFM = AV35SearchTxt;
         AV13TFContagemResultado_DemandaFM_Sel = "";
         AV178ExtraWWContagemResultadoEmGarantiaDS_1_Contratada_areatrabalhocod = AV53Contratada_AreaTrabalhoCod;
         AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1 = AV55DynamicFiltersSelector1;
         AV180ExtraWWContagemResultadoEmGarantiaDS_3_Dynamicfiltersoperator1 = AV56DynamicFiltersOperator1;
         AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 = AV60ContagemResultado_OsFsOsFm1;
         AV182ExtraWWContagemResultadoEmGarantiaDS_5_Contagemresultado_datadmn1 = AV61ContagemResultado_DataDmn1;
         AV183ExtraWWContagemResultadoEmGarantiaDS_6_Contagemresultado_datadmn_to1 = AV62ContagemResultado_DataDmn_To1;
         AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1 = AV57ContagemResultado_DataCnt1;
         AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1 = AV58ContagemResultado_DataCnt_To1;
         AV186ExtraWWContagemResultadoEmGarantiaDS_9_Contagemresultado_dataprevista1 = AV63ContagemResultado_DataPrevista1;
         AV187ExtraWWContagemResultadoEmGarantiaDS_10_Contagemresultado_dataprevista_to1 = AV64ContagemResultado_DataPrevista_To1;
         AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1 = AV59ContagemResultado_StatusDmn1;
         AV189ExtraWWContagemResultadoEmGarantiaDS_12_Contagemresultado_servico1 = AV65ContagemResultado_Servico1;
         AV190ExtraWWContagemResultadoEmGarantiaDS_13_Contagemresultado_sistemacod1 = AV67ContagemResultado_SistemaCod1;
         AV191ExtraWWContagemResultadoEmGarantiaDS_14_Contagemresultado_contratadacod1 = AV68ContagemResultado_ContratadaCod1;
         AV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1 = AV75ContagemResultado_Descricao1;
         AV193ExtraWWContagemResultadoEmGarantiaDS_16_Contagemresultado_owner1 = AV76ContagemResultado_Owner1;
         AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 = AV78DynamicFiltersEnabled2;
         AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2 = AV79DynamicFiltersSelector2;
         AV196ExtraWWContagemResultadoEmGarantiaDS_19_Dynamicfiltersoperator2 = AV80DynamicFiltersOperator2;
         AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 = AV84ContagemResultado_OsFsOsFm2;
         AV198ExtraWWContagemResultadoEmGarantiaDS_21_Contagemresultado_datadmn2 = AV85ContagemResultado_DataDmn2;
         AV199ExtraWWContagemResultadoEmGarantiaDS_22_Contagemresultado_datadmn_to2 = AV86ContagemResultado_DataDmn_To2;
         AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2 = AV81ContagemResultado_DataCnt2;
         AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2 = AV82ContagemResultado_DataCnt_To2;
         AV202ExtraWWContagemResultadoEmGarantiaDS_25_Contagemresultado_dataprevista2 = AV87ContagemResultado_DataPrevista2;
         AV203ExtraWWContagemResultadoEmGarantiaDS_26_Contagemresultado_dataprevista_to2 = AV88ContagemResultado_DataPrevista_To2;
         AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2 = AV83ContagemResultado_StatusDmn2;
         AV205ExtraWWContagemResultadoEmGarantiaDS_28_Contagemresultado_servico2 = AV89ContagemResultado_Servico2;
         AV206ExtraWWContagemResultadoEmGarantiaDS_29_Contagemresultado_sistemacod2 = AV91ContagemResultado_SistemaCod2;
         AV207ExtraWWContagemResultadoEmGarantiaDS_30_Contagemresultado_contratadacod2 = AV92ContagemResultado_ContratadaCod2;
         AV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2 = AV99ContagemResultado_Descricao2;
         AV209ExtraWWContagemResultadoEmGarantiaDS_32_Contagemresultado_owner2 = AV100ContagemResultado_Owner2;
         AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 = AV102DynamicFiltersEnabled3;
         AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3 = AV103DynamicFiltersSelector3;
         AV212ExtraWWContagemResultadoEmGarantiaDS_35_Dynamicfiltersoperator3 = AV104DynamicFiltersOperator3;
         AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 = AV108ContagemResultado_OsFsOsFm3;
         AV214ExtraWWContagemResultadoEmGarantiaDS_37_Contagemresultado_datadmn3 = AV109ContagemResultado_DataDmn3;
         AV215ExtraWWContagemResultadoEmGarantiaDS_38_Contagemresultado_datadmn_to3 = AV110ContagemResultado_DataDmn_To3;
         AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3 = AV105ContagemResultado_DataCnt3;
         AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3 = AV106ContagemResultado_DataCnt_To3;
         AV218ExtraWWContagemResultadoEmGarantiaDS_41_Contagemresultado_dataprevista3 = AV111ContagemResultado_DataPrevista3;
         AV219ExtraWWContagemResultadoEmGarantiaDS_42_Contagemresultado_dataprevista_to3 = AV112ContagemResultado_DataPrevista_To3;
         AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3 = AV107ContagemResultado_StatusDmn3;
         AV221ExtraWWContagemResultadoEmGarantiaDS_44_Contagemresultado_servico3 = AV113ContagemResultado_Servico3;
         AV222ExtraWWContagemResultadoEmGarantiaDS_45_Contagemresultado_sistemacod3 = AV115ContagemResultado_SistemaCod3;
         AV223ExtraWWContagemResultadoEmGarantiaDS_46_Contagemresultado_contratadacod3 = AV116ContagemResultado_ContratadaCod3;
         AV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3 = AV123ContagemResultado_Descricao3;
         AV225ExtraWWContagemResultadoEmGarantiaDS_48_Contagemresultado_owner3 = AV124ContagemResultado_Owner3;
         AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 = AV126DynamicFiltersEnabled4;
         AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4 = AV127DynamicFiltersSelector4;
         AV228ExtraWWContagemResultadoEmGarantiaDS_51_Dynamicfiltersoperator4 = AV128DynamicFiltersOperator4;
         AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 = AV132ContagemResultado_OsFsOsFm4;
         AV230ExtraWWContagemResultadoEmGarantiaDS_53_Contagemresultado_datadmn4 = AV133ContagemResultado_DataDmn4;
         AV231ExtraWWContagemResultadoEmGarantiaDS_54_Contagemresultado_datadmn_to4 = AV134ContagemResultado_DataDmn_To4;
         AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4 = AV129ContagemResultado_DataCnt4;
         AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4 = AV130ContagemResultado_DataCnt_To4;
         AV234ExtraWWContagemResultadoEmGarantiaDS_57_Contagemresultado_dataprevista4 = AV135ContagemResultado_DataPrevista4;
         AV235ExtraWWContagemResultadoEmGarantiaDS_58_Contagemresultado_dataprevista_to4 = AV136ContagemResultado_DataPrevista_To4;
         AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4 = AV131ContagemResultado_StatusDmn4;
         AV237ExtraWWContagemResultadoEmGarantiaDS_60_Contagemresultado_servico4 = AV137ContagemResultado_Servico4;
         AV238ExtraWWContagemResultadoEmGarantiaDS_61_Contagemresultado_sistemacod4 = AV139ContagemResultado_SistemaCod4;
         AV239ExtraWWContagemResultadoEmGarantiaDS_62_Contagemresultado_contratadacod4 = AV140ContagemResultado_ContratadaCod4;
         AV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4 = AV147ContagemResultado_Descricao4;
         AV241ExtraWWContagemResultadoEmGarantiaDS_64_Contagemresultado_owner4 = AV148ContagemResultado_Owner4;
         AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 = AV150DynamicFiltersEnabled5;
         AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5 = AV151DynamicFiltersSelector5;
         AV244ExtraWWContagemResultadoEmGarantiaDS_67_Dynamicfiltersoperator5 = AV152DynamicFiltersOperator5;
         AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 = AV156ContagemResultado_OsFsOsFm5;
         AV246ExtraWWContagemResultadoEmGarantiaDS_69_Contagemresultado_datadmn5 = AV157ContagemResultado_DataDmn5;
         AV247ExtraWWContagemResultadoEmGarantiaDS_70_Contagemresultado_datadmn_to5 = AV158ContagemResultado_DataDmn_To5;
         AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5 = AV153ContagemResultado_DataCnt5;
         AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5 = AV154ContagemResultado_DataCnt_To5;
         AV250ExtraWWContagemResultadoEmGarantiaDS_73_Contagemresultado_dataprevista5 = AV159ContagemResultado_DataPrevista5;
         AV251ExtraWWContagemResultadoEmGarantiaDS_74_Contagemresultado_dataprevista_to5 = AV160ContagemResultado_DataPrevista_To5;
         AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5 = AV155ContagemResultado_StatusDmn5;
         AV253ExtraWWContagemResultadoEmGarantiaDS_76_Contagemresultado_servico5 = AV161ContagemResultado_Servico5;
         AV254ExtraWWContagemResultadoEmGarantiaDS_77_Contagemresultado_sistemacod5 = AV163ContagemResultado_SistemaCod5;
         AV255ExtraWWContagemResultadoEmGarantiaDS_78_Contagemresultado_contratadacod5 = AV164ContagemResultado_ContratadaCod5;
         AV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5 = AV171ContagemResultado_Descricao5;
         AV257ExtraWWContagemResultadoEmGarantiaDS_80_Contagemresultado_owner5 = AV172ContagemResultado_Owner5;
         AV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm = AV12TFContagemResultado_DemandaFM;
         AV259ExtraWWContagemResultadoEmGarantiaDS_82_Tfcontagemresultado_demandafm_sel = AV13TFContagemResultado_DemandaFM_Sel;
         AV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda = AV14TFContagemResultado_Demanda;
         AV261ExtraWWContagemResultadoEmGarantiaDS_84_Tfcontagemresultado_demanda_sel = AV15TFContagemResultado_Demanda_Sel;
         AV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao = AV16TFContagemResultado_Descricao;
         AV263ExtraWWContagemResultadoEmGarantiaDS_86_Tfcontagemresultado_descricao_sel = AV17TFContagemResultado_Descricao_Sel;
         AV264ExtraWWContagemResultadoEmGarantiaDS_87_Tfcontagemresultado_datadmn = AV18TFContagemResultado_DataDmn;
         AV265ExtraWWContagemResultadoEmGarantiaDS_88_Tfcontagemresultado_datadmn_to = AV19TFContagemResultado_DataDmn_To;
         AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt = AV20TFContagemResultado_DataUltCnt;
         AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to = AV21TFContagemResultado_DataUltCnt_To;
         AV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla = AV22TFContagemResultado_ContratadaSigla;
         AV269ExtraWWContagemResultadoEmGarantiaDS_92_Tfcontagemresultado_contratadasigla_sel = AV23TFContagemResultado_ContratadaSigla_Sel;
         AV270ExtraWWContagemResultadoEmGarantiaDS_93_Tfcontagemresultado_statusdmn_sels = AV27TFContagemResultado_StatusDmn_Sels;
         AV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla = AV29TFContagemResultado_ServicoSigla;
         AV272ExtraWWContagemResultadoEmGarantiaDS_95_Tfcontagemresultado_servicosigla_sel = AV30TFContagemResultado_ServicoSigla_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A484ContagemResultado_StatusDmn ,
                                              AV270ExtraWWContagemResultadoEmGarantiaDS_93_Tfcontagemresultado_statusdmn_sels ,
                                              AV9WWPContext.gxTpr_Contratada_codigo ,
                                              AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1 ,
                                              AV180ExtraWWContagemResultadoEmGarantiaDS_3_Dynamicfiltersoperator1 ,
                                              AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 ,
                                              AV182ExtraWWContagemResultadoEmGarantiaDS_5_Contagemresultado_datadmn1 ,
                                              AV183ExtraWWContagemResultadoEmGarantiaDS_6_Contagemresultado_datadmn_to1 ,
                                              AV186ExtraWWContagemResultadoEmGarantiaDS_9_Contagemresultado_dataprevista1 ,
                                              AV187ExtraWWContagemResultadoEmGarantiaDS_10_Contagemresultado_dataprevista_to1 ,
                                              AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1 ,
                                              AV189ExtraWWContagemResultadoEmGarantiaDS_12_Contagemresultado_servico1 ,
                                              AV190ExtraWWContagemResultadoEmGarantiaDS_13_Contagemresultado_sistemacod1 ,
                                              AV191ExtraWWContagemResultadoEmGarantiaDS_14_Contagemresultado_contratadacod1 ,
                                              AV178ExtraWWContagemResultadoEmGarantiaDS_1_Contratada_areatrabalhocod ,
                                              AV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1 ,
                                              AV193ExtraWWContagemResultadoEmGarantiaDS_16_Contagemresultado_owner1 ,
                                              AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 ,
                                              AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2 ,
                                              AV196ExtraWWContagemResultadoEmGarantiaDS_19_Dynamicfiltersoperator2 ,
                                              AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 ,
                                              AV198ExtraWWContagemResultadoEmGarantiaDS_21_Contagemresultado_datadmn2 ,
                                              AV199ExtraWWContagemResultadoEmGarantiaDS_22_Contagemresultado_datadmn_to2 ,
                                              AV202ExtraWWContagemResultadoEmGarantiaDS_25_Contagemresultado_dataprevista2 ,
                                              AV203ExtraWWContagemResultadoEmGarantiaDS_26_Contagemresultado_dataprevista_to2 ,
                                              AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2 ,
                                              AV205ExtraWWContagemResultadoEmGarantiaDS_28_Contagemresultado_servico2 ,
                                              AV206ExtraWWContagemResultadoEmGarantiaDS_29_Contagemresultado_sistemacod2 ,
                                              AV207ExtraWWContagemResultadoEmGarantiaDS_30_Contagemresultado_contratadacod2 ,
                                              AV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2 ,
                                              AV209ExtraWWContagemResultadoEmGarantiaDS_32_Contagemresultado_owner2 ,
                                              AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 ,
                                              AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3 ,
                                              AV212ExtraWWContagemResultadoEmGarantiaDS_35_Dynamicfiltersoperator3 ,
                                              AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 ,
                                              AV214ExtraWWContagemResultadoEmGarantiaDS_37_Contagemresultado_datadmn3 ,
                                              AV215ExtraWWContagemResultadoEmGarantiaDS_38_Contagemresultado_datadmn_to3 ,
                                              AV218ExtraWWContagemResultadoEmGarantiaDS_41_Contagemresultado_dataprevista3 ,
                                              AV219ExtraWWContagemResultadoEmGarantiaDS_42_Contagemresultado_dataprevista_to3 ,
                                              AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3 ,
                                              AV221ExtraWWContagemResultadoEmGarantiaDS_44_Contagemresultado_servico3 ,
                                              AV222ExtraWWContagemResultadoEmGarantiaDS_45_Contagemresultado_sistemacod3 ,
                                              AV223ExtraWWContagemResultadoEmGarantiaDS_46_Contagemresultado_contratadacod3 ,
                                              AV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3 ,
                                              AV225ExtraWWContagemResultadoEmGarantiaDS_48_Contagemresultado_owner3 ,
                                              AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 ,
                                              AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4 ,
                                              AV228ExtraWWContagemResultadoEmGarantiaDS_51_Dynamicfiltersoperator4 ,
                                              AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 ,
                                              AV230ExtraWWContagemResultadoEmGarantiaDS_53_Contagemresultado_datadmn4 ,
                                              AV231ExtraWWContagemResultadoEmGarantiaDS_54_Contagemresultado_datadmn_to4 ,
                                              AV234ExtraWWContagemResultadoEmGarantiaDS_57_Contagemresultado_dataprevista4 ,
                                              AV235ExtraWWContagemResultadoEmGarantiaDS_58_Contagemresultado_dataprevista_to4 ,
                                              AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4 ,
                                              AV237ExtraWWContagemResultadoEmGarantiaDS_60_Contagemresultado_servico4 ,
                                              AV238ExtraWWContagemResultadoEmGarantiaDS_61_Contagemresultado_sistemacod4 ,
                                              AV239ExtraWWContagemResultadoEmGarantiaDS_62_Contagemresultado_contratadacod4 ,
                                              AV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4 ,
                                              AV241ExtraWWContagemResultadoEmGarantiaDS_64_Contagemresultado_owner4 ,
                                              AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 ,
                                              AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5 ,
                                              AV244ExtraWWContagemResultadoEmGarantiaDS_67_Dynamicfiltersoperator5 ,
                                              AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 ,
                                              AV246ExtraWWContagemResultadoEmGarantiaDS_69_Contagemresultado_datadmn5 ,
                                              AV247ExtraWWContagemResultadoEmGarantiaDS_70_Contagemresultado_datadmn_to5 ,
                                              AV250ExtraWWContagemResultadoEmGarantiaDS_73_Contagemresultado_dataprevista5 ,
                                              AV251ExtraWWContagemResultadoEmGarantiaDS_74_Contagemresultado_dataprevista_to5 ,
                                              AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5 ,
                                              AV253ExtraWWContagemResultadoEmGarantiaDS_76_Contagemresultado_servico5 ,
                                              AV254ExtraWWContagemResultadoEmGarantiaDS_77_Contagemresultado_sistemacod5 ,
                                              AV255ExtraWWContagemResultadoEmGarantiaDS_78_Contagemresultado_contratadacod5 ,
                                              AV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5 ,
                                              AV257ExtraWWContagemResultadoEmGarantiaDS_80_Contagemresultado_owner5 ,
                                              AV259ExtraWWContagemResultadoEmGarantiaDS_82_Tfcontagemresultado_demandafm_sel ,
                                              AV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm ,
                                              AV261ExtraWWContagemResultadoEmGarantiaDS_84_Tfcontagemresultado_demanda_sel ,
                                              AV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda ,
                                              AV263ExtraWWContagemResultadoEmGarantiaDS_86_Tfcontagemresultado_descricao_sel ,
                                              AV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao ,
                                              AV264ExtraWWContagemResultadoEmGarantiaDS_87_Tfcontagemresultado_datadmn ,
                                              AV265ExtraWWContagemResultadoEmGarantiaDS_88_Tfcontagemresultado_datadmn_to ,
                                              AV269ExtraWWContagemResultadoEmGarantiaDS_92_Tfcontagemresultado_contratadasigla_sel ,
                                              AV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla ,
                                              AV270ExtraWWContagemResultadoEmGarantiaDS_93_Tfcontagemresultado_statusdmn_sels.Count ,
                                              AV272ExtraWWContagemResultadoEmGarantiaDS_95_Tfcontagemresultado_servicosigla_sel ,
                                              AV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla ,
                                              A490ContagemResultado_ContratadaCod ,
                                              A457ContagemResultado_Demanda ,
                                              A493ContagemResultado_DemandaFM ,
                                              A471ContagemResultado_DataDmn ,
                                              A1351ContagemResultado_DataPrevista ,
                                              A601ContagemResultado_Servico ,
                                              A489ContagemResultado_SistemaCod ,
                                              A494ContagemResultado_Descricao ,
                                              A508ContagemResultado_Owner ,
                                              A803ContagemResultado_ContratadaSigla ,
                                              A801ContagemResultado_ServicoSigla ,
                                              AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1 ,
                                              A566ContagemResultado_DataUltCnt ,
                                              AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1 ,
                                              AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2 ,
                                              AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2 ,
                                              AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3 ,
                                              AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3 ,
                                              AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4 ,
                                              AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4 ,
                                              AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5 ,
                                              AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5 ,
                                              AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt ,
                                              AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1), "%", "");
         lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1), "%", "");
         lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1), "%", "");
         lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1), "%", "");
         lV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1), "%", "");
         lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2), "%", "");
         lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2), "%", "");
         lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2), "%", "");
         lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2), "%", "");
         lV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2), "%", "");
         lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3), "%", "");
         lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3), "%", "");
         lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3), "%", "");
         lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3), "%", "");
         lV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3), "%", "");
         lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4), "%", "");
         lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4), "%", "");
         lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4), "%", "");
         lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4), "%", "");
         lV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4 = StringUtil.Concat( StringUtil.RTrim( AV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4), "%", "");
         lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5), "%", "");
         lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5), "%", "");
         lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5), "%", "");
         lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5), "%", "");
         lV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5 = StringUtil.Concat( StringUtil.RTrim( AV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5), "%", "");
         lV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm = StringUtil.Concat( StringUtil.RTrim( AV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm), "%", "");
         lV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda = StringUtil.Concat( StringUtil.RTrim( AV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda), "%", "");
         lV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao = StringUtil.Concat( StringUtil.RTrim( AV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao), "%", "");
         lV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla = StringUtil.PadR( StringUtil.RTrim( AV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla), 15, "%");
         lV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla = StringUtil.PadR( StringUtil.RTrim( AV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla), 15, "%");
         /* Using cursor P00U13 */
         pr_default.execute(0, new Object[] {AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1, AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1, AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1, AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1, AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2, AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2, AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2, AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2, AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2, AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2, AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3, AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3, AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3, AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3, AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3, AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3, AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4, AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4, AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4, AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4, AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4, AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4, AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5, AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5, AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5, AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5, AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5, AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5, AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt, AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt, AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to, AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to, AV9WWPContext.gxTpr_Areatrabalho_codigo, AV9WWPContext.gxTpr_Contratada_codigo, AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1, AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1, lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1, lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1, lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1, lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1, AV182ExtraWWContagemResultadoEmGarantiaDS_5_Contagemresultado_datadmn1, AV183ExtraWWContagemResultadoEmGarantiaDS_6_Contagemresultado_datadmn_to1, AV186ExtraWWContagemResultadoEmGarantiaDS_9_Contagemresultado_dataprevista1, AV187ExtraWWContagemResultadoEmGarantiaDS_10_Contagemresultado_dataprevista_to1, AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1, AV189ExtraWWContagemResultadoEmGarantiaDS_12_Contagemresultado_servico1, AV190ExtraWWContagemResultadoEmGarantiaDS_13_Contagemresultado_sistemacod1, AV191ExtraWWContagemResultadoEmGarantiaDS_14_Contagemresultado_contratadacod1, lV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1, AV193ExtraWWContagemResultadoEmGarantiaDS_16_Contagemresultado_owner1, AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2, AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2, lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2, lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2, lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2, lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2, AV198ExtraWWContagemResultadoEmGarantiaDS_21_Contagemresultado_datadmn2, AV199ExtraWWContagemResultadoEmGarantiaDS_22_Contagemresultado_datadmn_to2, AV202ExtraWWContagemResultadoEmGarantiaDS_25_Contagemresultado_dataprevista2, AV203ExtraWWContagemResultadoEmGarantiaDS_26_Contagemresultado_dataprevista_to2, AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2, AV205ExtraWWContagemResultadoEmGarantiaDS_28_Contagemresultado_servico2, AV206ExtraWWContagemResultadoEmGarantiaDS_29_Contagemresultado_sistemacod2, AV207ExtraWWContagemResultadoEmGarantiaDS_30_Contagemresultado_contratadacod2, lV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2, AV209ExtraWWContagemResultadoEmGarantiaDS_32_Contagemresultado_owner2, AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3, AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3, lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3, lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3, lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3, lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3, AV214ExtraWWContagemResultadoEmGarantiaDS_37_Contagemresultado_datadmn3, AV215ExtraWWContagemResultadoEmGarantiaDS_38_Contagemresultado_datadmn_to3, AV218ExtraWWContagemResultadoEmGarantiaDS_41_Contagemresultado_dataprevista3, AV219ExtraWWContagemResultadoEmGarantiaDS_42_Contagemresultado_dataprevista_to3, AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3, AV221ExtraWWContagemResultadoEmGarantiaDS_44_Contagemresultado_servico3, AV222ExtraWWContagemResultadoEmGarantiaDS_45_Contagemresultado_sistemacod3, AV223ExtraWWContagemResultadoEmGarantiaDS_46_Contagemresultado_contratadacod3, lV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3, AV225ExtraWWContagemResultadoEmGarantiaDS_48_Contagemresultado_owner3, AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4, AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4, lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4, lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4, lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4, lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4, AV230ExtraWWContagemResultadoEmGarantiaDS_53_Contagemresultado_datadmn4, AV231ExtraWWContagemResultadoEmGarantiaDS_54_Contagemresultado_datadmn_to4, AV234ExtraWWContagemResultadoEmGarantiaDS_57_Contagemresultado_dataprevista4, AV235ExtraWWContagemResultadoEmGarantiaDS_58_Contagemresultado_dataprevista_to4, AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4, AV237ExtraWWContagemResultadoEmGarantiaDS_60_Contagemresultado_servico4, AV238ExtraWWContagemResultadoEmGarantiaDS_61_Contagemresultado_sistemacod4, AV239ExtraWWContagemResultadoEmGarantiaDS_62_Contagemresultado_contratadacod4, lV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4, AV241ExtraWWContagemResultadoEmGarantiaDS_64_Contagemresultado_owner4, AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5, AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5, lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5, lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5, lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5, lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5, AV246ExtraWWContagemResultadoEmGarantiaDS_69_Contagemresultado_datadmn5, AV247ExtraWWContagemResultadoEmGarantiaDS_70_Contagemresultado_datadmn_to5, AV250ExtraWWContagemResultadoEmGarantiaDS_73_Contagemresultado_dataprevista5, AV251ExtraWWContagemResultadoEmGarantiaDS_74_Contagemresultado_dataprevista_to5, AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5, AV253ExtraWWContagemResultadoEmGarantiaDS_76_Contagemresultado_servico5, AV254ExtraWWContagemResultadoEmGarantiaDS_77_Contagemresultado_sistemacod5, AV255ExtraWWContagemResultadoEmGarantiaDS_78_Contagemresultado_contratadacod5,
         lV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5, AV257ExtraWWContagemResultadoEmGarantiaDS_80_Contagemresultado_owner5, lV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm, AV259ExtraWWContagemResultadoEmGarantiaDS_82_Tfcontagemresultado_demandafm_sel, lV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda, AV261ExtraWWContagemResultadoEmGarantiaDS_84_Tfcontagemresultado_demanda_sel, lV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao, AV263ExtraWWContagemResultadoEmGarantiaDS_86_Tfcontagemresultado_descricao_sel, AV264ExtraWWContagemResultadoEmGarantiaDS_87_Tfcontagemresultado_datadmn, AV265ExtraWWContagemResultadoEmGarantiaDS_88_Tfcontagemresultado_datadmn_to, lV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla, AV269ExtraWWContagemResultadoEmGarantiaDS_92_Tfcontagemresultado_contratadasigla_sel, lV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla, AV272ExtraWWContagemResultadoEmGarantiaDS_95_Tfcontagemresultado_servicosigla_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKU12 = false;
            A1553ContagemResultado_CntSrvCod = P00U13_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00U13_n1553ContagemResultado_CntSrvCod[0];
            A456ContagemResultado_Codigo = P00U13_A456ContagemResultado_Codigo[0];
            A52Contratada_AreaTrabalhoCod = P00U13_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00U13_n52Contratada_AreaTrabalhoCod[0];
            A493ContagemResultado_DemandaFM = P00U13_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00U13_n493ContagemResultado_DemandaFM[0];
            A801ContagemResultado_ServicoSigla = P00U13_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00U13_n801ContagemResultado_ServicoSigla[0];
            A803ContagemResultado_ContratadaSigla = P00U13_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P00U13_n803ContagemResultado_ContratadaSigla[0];
            A508ContagemResultado_Owner = P00U13_A508ContagemResultado_Owner[0];
            A494ContagemResultado_Descricao = P00U13_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = P00U13_n494ContagemResultado_Descricao[0];
            A489ContagemResultado_SistemaCod = P00U13_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00U13_n489ContagemResultado_SistemaCod[0];
            A601ContagemResultado_Servico = P00U13_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00U13_n601ContagemResultado_Servico[0];
            A484ContagemResultado_StatusDmn = P00U13_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00U13_n484ContagemResultado_StatusDmn[0];
            A1351ContagemResultado_DataPrevista = P00U13_A1351ContagemResultado_DataPrevista[0];
            n1351ContagemResultado_DataPrevista = P00U13_n1351ContagemResultado_DataPrevista[0];
            A471ContagemResultado_DataDmn = P00U13_A471ContagemResultado_DataDmn[0];
            A457ContagemResultado_Demanda = P00U13_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00U13_n457ContagemResultado_Demanda[0];
            A490ContagemResultado_ContratadaCod = P00U13_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00U13_n490ContagemResultado_ContratadaCod[0];
            A566ContagemResultado_DataUltCnt = P00U13_A566ContagemResultado_DataUltCnt[0];
            n566ContagemResultado_DataUltCnt = P00U13_n566ContagemResultado_DataUltCnt[0];
            A601ContagemResultado_Servico = P00U13_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00U13_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = P00U13_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00U13_n801ContagemResultado_ServicoSigla[0];
            A566ContagemResultado_DataUltCnt = P00U13_A566ContagemResultado_DataUltCnt[0];
            n566ContagemResultado_DataUltCnt = P00U13_n566ContagemResultado_DataUltCnt[0];
            A52Contratada_AreaTrabalhoCod = P00U13_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00U13_n52Contratada_AreaTrabalhoCod[0];
            A803ContagemResultado_ContratadaSigla = P00U13_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P00U13_n803ContagemResultado_ContratadaSigla[0];
            AV47count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00U13_A493ContagemResultado_DemandaFM[0], A493ContagemResultado_DemandaFM) == 0 ) )
            {
               BRKU12 = false;
               A456ContagemResultado_Codigo = P00U13_A456ContagemResultado_Codigo[0];
               AV47count = (long)(AV47count+1);
               BRKU12 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) )
            {
               AV39Option = A493ContagemResultado_DemandaFM;
               AV40Options.Add(AV39Option, 0);
               AV45OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV47count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV40Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKU12 )
            {
               BRKU12 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTAGEMRESULTADO_DEMANDAOPTIONS' Routine */
         AV14TFContagemResultado_Demanda = AV35SearchTxt;
         AV15TFContagemResultado_Demanda_Sel = "";
         AV178ExtraWWContagemResultadoEmGarantiaDS_1_Contratada_areatrabalhocod = AV53Contratada_AreaTrabalhoCod;
         AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1 = AV55DynamicFiltersSelector1;
         AV180ExtraWWContagemResultadoEmGarantiaDS_3_Dynamicfiltersoperator1 = AV56DynamicFiltersOperator1;
         AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 = AV60ContagemResultado_OsFsOsFm1;
         AV182ExtraWWContagemResultadoEmGarantiaDS_5_Contagemresultado_datadmn1 = AV61ContagemResultado_DataDmn1;
         AV183ExtraWWContagemResultadoEmGarantiaDS_6_Contagemresultado_datadmn_to1 = AV62ContagemResultado_DataDmn_To1;
         AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1 = AV57ContagemResultado_DataCnt1;
         AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1 = AV58ContagemResultado_DataCnt_To1;
         AV186ExtraWWContagemResultadoEmGarantiaDS_9_Contagemresultado_dataprevista1 = AV63ContagemResultado_DataPrevista1;
         AV187ExtraWWContagemResultadoEmGarantiaDS_10_Contagemresultado_dataprevista_to1 = AV64ContagemResultado_DataPrevista_To1;
         AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1 = AV59ContagemResultado_StatusDmn1;
         AV189ExtraWWContagemResultadoEmGarantiaDS_12_Contagemresultado_servico1 = AV65ContagemResultado_Servico1;
         AV190ExtraWWContagemResultadoEmGarantiaDS_13_Contagemresultado_sistemacod1 = AV67ContagemResultado_SistemaCod1;
         AV191ExtraWWContagemResultadoEmGarantiaDS_14_Contagemresultado_contratadacod1 = AV68ContagemResultado_ContratadaCod1;
         AV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1 = AV75ContagemResultado_Descricao1;
         AV193ExtraWWContagemResultadoEmGarantiaDS_16_Contagemresultado_owner1 = AV76ContagemResultado_Owner1;
         AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 = AV78DynamicFiltersEnabled2;
         AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2 = AV79DynamicFiltersSelector2;
         AV196ExtraWWContagemResultadoEmGarantiaDS_19_Dynamicfiltersoperator2 = AV80DynamicFiltersOperator2;
         AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 = AV84ContagemResultado_OsFsOsFm2;
         AV198ExtraWWContagemResultadoEmGarantiaDS_21_Contagemresultado_datadmn2 = AV85ContagemResultado_DataDmn2;
         AV199ExtraWWContagemResultadoEmGarantiaDS_22_Contagemresultado_datadmn_to2 = AV86ContagemResultado_DataDmn_To2;
         AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2 = AV81ContagemResultado_DataCnt2;
         AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2 = AV82ContagemResultado_DataCnt_To2;
         AV202ExtraWWContagemResultadoEmGarantiaDS_25_Contagemresultado_dataprevista2 = AV87ContagemResultado_DataPrevista2;
         AV203ExtraWWContagemResultadoEmGarantiaDS_26_Contagemresultado_dataprevista_to2 = AV88ContagemResultado_DataPrevista_To2;
         AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2 = AV83ContagemResultado_StatusDmn2;
         AV205ExtraWWContagemResultadoEmGarantiaDS_28_Contagemresultado_servico2 = AV89ContagemResultado_Servico2;
         AV206ExtraWWContagemResultadoEmGarantiaDS_29_Contagemresultado_sistemacod2 = AV91ContagemResultado_SistemaCod2;
         AV207ExtraWWContagemResultadoEmGarantiaDS_30_Contagemresultado_contratadacod2 = AV92ContagemResultado_ContratadaCod2;
         AV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2 = AV99ContagemResultado_Descricao2;
         AV209ExtraWWContagemResultadoEmGarantiaDS_32_Contagemresultado_owner2 = AV100ContagemResultado_Owner2;
         AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 = AV102DynamicFiltersEnabled3;
         AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3 = AV103DynamicFiltersSelector3;
         AV212ExtraWWContagemResultadoEmGarantiaDS_35_Dynamicfiltersoperator3 = AV104DynamicFiltersOperator3;
         AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 = AV108ContagemResultado_OsFsOsFm3;
         AV214ExtraWWContagemResultadoEmGarantiaDS_37_Contagemresultado_datadmn3 = AV109ContagemResultado_DataDmn3;
         AV215ExtraWWContagemResultadoEmGarantiaDS_38_Contagemresultado_datadmn_to3 = AV110ContagemResultado_DataDmn_To3;
         AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3 = AV105ContagemResultado_DataCnt3;
         AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3 = AV106ContagemResultado_DataCnt_To3;
         AV218ExtraWWContagemResultadoEmGarantiaDS_41_Contagemresultado_dataprevista3 = AV111ContagemResultado_DataPrevista3;
         AV219ExtraWWContagemResultadoEmGarantiaDS_42_Contagemresultado_dataprevista_to3 = AV112ContagemResultado_DataPrevista_To3;
         AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3 = AV107ContagemResultado_StatusDmn3;
         AV221ExtraWWContagemResultadoEmGarantiaDS_44_Contagemresultado_servico3 = AV113ContagemResultado_Servico3;
         AV222ExtraWWContagemResultadoEmGarantiaDS_45_Contagemresultado_sistemacod3 = AV115ContagemResultado_SistemaCod3;
         AV223ExtraWWContagemResultadoEmGarantiaDS_46_Contagemresultado_contratadacod3 = AV116ContagemResultado_ContratadaCod3;
         AV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3 = AV123ContagemResultado_Descricao3;
         AV225ExtraWWContagemResultadoEmGarantiaDS_48_Contagemresultado_owner3 = AV124ContagemResultado_Owner3;
         AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 = AV126DynamicFiltersEnabled4;
         AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4 = AV127DynamicFiltersSelector4;
         AV228ExtraWWContagemResultadoEmGarantiaDS_51_Dynamicfiltersoperator4 = AV128DynamicFiltersOperator4;
         AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 = AV132ContagemResultado_OsFsOsFm4;
         AV230ExtraWWContagemResultadoEmGarantiaDS_53_Contagemresultado_datadmn4 = AV133ContagemResultado_DataDmn4;
         AV231ExtraWWContagemResultadoEmGarantiaDS_54_Contagemresultado_datadmn_to4 = AV134ContagemResultado_DataDmn_To4;
         AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4 = AV129ContagemResultado_DataCnt4;
         AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4 = AV130ContagemResultado_DataCnt_To4;
         AV234ExtraWWContagemResultadoEmGarantiaDS_57_Contagemresultado_dataprevista4 = AV135ContagemResultado_DataPrevista4;
         AV235ExtraWWContagemResultadoEmGarantiaDS_58_Contagemresultado_dataprevista_to4 = AV136ContagemResultado_DataPrevista_To4;
         AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4 = AV131ContagemResultado_StatusDmn4;
         AV237ExtraWWContagemResultadoEmGarantiaDS_60_Contagemresultado_servico4 = AV137ContagemResultado_Servico4;
         AV238ExtraWWContagemResultadoEmGarantiaDS_61_Contagemresultado_sistemacod4 = AV139ContagemResultado_SistemaCod4;
         AV239ExtraWWContagemResultadoEmGarantiaDS_62_Contagemresultado_contratadacod4 = AV140ContagemResultado_ContratadaCod4;
         AV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4 = AV147ContagemResultado_Descricao4;
         AV241ExtraWWContagemResultadoEmGarantiaDS_64_Contagemresultado_owner4 = AV148ContagemResultado_Owner4;
         AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 = AV150DynamicFiltersEnabled5;
         AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5 = AV151DynamicFiltersSelector5;
         AV244ExtraWWContagemResultadoEmGarantiaDS_67_Dynamicfiltersoperator5 = AV152DynamicFiltersOperator5;
         AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 = AV156ContagemResultado_OsFsOsFm5;
         AV246ExtraWWContagemResultadoEmGarantiaDS_69_Contagemresultado_datadmn5 = AV157ContagemResultado_DataDmn5;
         AV247ExtraWWContagemResultadoEmGarantiaDS_70_Contagemresultado_datadmn_to5 = AV158ContagemResultado_DataDmn_To5;
         AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5 = AV153ContagemResultado_DataCnt5;
         AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5 = AV154ContagemResultado_DataCnt_To5;
         AV250ExtraWWContagemResultadoEmGarantiaDS_73_Contagemresultado_dataprevista5 = AV159ContagemResultado_DataPrevista5;
         AV251ExtraWWContagemResultadoEmGarantiaDS_74_Contagemresultado_dataprevista_to5 = AV160ContagemResultado_DataPrevista_To5;
         AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5 = AV155ContagemResultado_StatusDmn5;
         AV253ExtraWWContagemResultadoEmGarantiaDS_76_Contagemresultado_servico5 = AV161ContagemResultado_Servico5;
         AV254ExtraWWContagemResultadoEmGarantiaDS_77_Contagemresultado_sistemacod5 = AV163ContagemResultado_SistemaCod5;
         AV255ExtraWWContagemResultadoEmGarantiaDS_78_Contagemresultado_contratadacod5 = AV164ContagemResultado_ContratadaCod5;
         AV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5 = AV171ContagemResultado_Descricao5;
         AV257ExtraWWContagemResultadoEmGarantiaDS_80_Contagemresultado_owner5 = AV172ContagemResultado_Owner5;
         AV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm = AV12TFContagemResultado_DemandaFM;
         AV259ExtraWWContagemResultadoEmGarantiaDS_82_Tfcontagemresultado_demandafm_sel = AV13TFContagemResultado_DemandaFM_Sel;
         AV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda = AV14TFContagemResultado_Demanda;
         AV261ExtraWWContagemResultadoEmGarantiaDS_84_Tfcontagemresultado_demanda_sel = AV15TFContagemResultado_Demanda_Sel;
         AV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao = AV16TFContagemResultado_Descricao;
         AV263ExtraWWContagemResultadoEmGarantiaDS_86_Tfcontagemresultado_descricao_sel = AV17TFContagemResultado_Descricao_Sel;
         AV264ExtraWWContagemResultadoEmGarantiaDS_87_Tfcontagemresultado_datadmn = AV18TFContagemResultado_DataDmn;
         AV265ExtraWWContagemResultadoEmGarantiaDS_88_Tfcontagemresultado_datadmn_to = AV19TFContagemResultado_DataDmn_To;
         AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt = AV20TFContagemResultado_DataUltCnt;
         AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to = AV21TFContagemResultado_DataUltCnt_To;
         AV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla = AV22TFContagemResultado_ContratadaSigla;
         AV269ExtraWWContagemResultadoEmGarantiaDS_92_Tfcontagemresultado_contratadasigla_sel = AV23TFContagemResultado_ContratadaSigla_Sel;
         AV270ExtraWWContagemResultadoEmGarantiaDS_93_Tfcontagemresultado_statusdmn_sels = AV27TFContagemResultado_StatusDmn_Sels;
         AV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla = AV29TFContagemResultado_ServicoSigla;
         AV272ExtraWWContagemResultadoEmGarantiaDS_95_Tfcontagemresultado_servicosigla_sel = AV30TFContagemResultado_ServicoSigla_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A484ContagemResultado_StatusDmn ,
                                              AV270ExtraWWContagemResultadoEmGarantiaDS_93_Tfcontagemresultado_statusdmn_sels ,
                                              AV9WWPContext.gxTpr_Contratada_codigo ,
                                              AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1 ,
                                              AV180ExtraWWContagemResultadoEmGarantiaDS_3_Dynamicfiltersoperator1 ,
                                              AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 ,
                                              AV182ExtraWWContagemResultadoEmGarantiaDS_5_Contagemresultado_datadmn1 ,
                                              AV183ExtraWWContagemResultadoEmGarantiaDS_6_Contagemresultado_datadmn_to1 ,
                                              AV186ExtraWWContagemResultadoEmGarantiaDS_9_Contagemresultado_dataprevista1 ,
                                              AV187ExtraWWContagemResultadoEmGarantiaDS_10_Contagemresultado_dataprevista_to1 ,
                                              AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1 ,
                                              AV189ExtraWWContagemResultadoEmGarantiaDS_12_Contagemresultado_servico1 ,
                                              AV190ExtraWWContagemResultadoEmGarantiaDS_13_Contagemresultado_sistemacod1 ,
                                              AV191ExtraWWContagemResultadoEmGarantiaDS_14_Contagemresultado_contratadacod1 ,
                                              AV178ExtraWWContagemResultadoEmGarantiaDS_1_Contratada_areatrabalhocod ,
                                              AV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1 ,
                                              AV193ExtraWWContagemResultadoEmGarantiaDS_16_Contagemresultado_owner1 ,
                                              AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 ,
                                              AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2 ,
                                              AV196ExtraWWContagemResultadoEmGarantiaDS_19_Dynamicfiltersoperator2 ,
                                              AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 ,
                                              AV198ExtraWWContagemResultadoEmGarantiaDS_21_Contagemresultado_datadmn2 ,
                                              AV199ExtraWWContagemResultadoEmGarantiaDS_22_Contagemresultado_datadmn_to2 ,
                                              AV202ExtraWWContagemResultadoEmGarantiaDS_25_Contagemresultado_dataprevista2 ,
                                              AV203ExtraWWContagemResultadoEmGarantiaDS_26_Contagemresultado_dataprevista_to2 ,
                                              AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2 ,
                                              AV205ExtraWWContagemResultadoEmGarantiaDS_28_Contagemresultado_servico2 ,
                                              AV206ExtraWWContagemResultadoEmGarantiaDS_29_Contagemresultado_sistemacod2 ,
                                              AV207ExtraWWContagemResultadoEmGarantiaDS_30_Contagemresultado_contratadacod2 ,
                                              AV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2 ,
                                              AV209ExtraWWContagemResultadoEmGarantiaDS_32_Contagemresultado_owner2 ,
                                              AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 ,
                                              AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3 ,
                                              AV212ExtraWWContagemResultadoEmGarantiaDS_35_Dynamicfiltersoperator3 ,
                                              AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 ,
                                              AV214ExtraWWContagemResultadoEmGarantiaDS_37_Contagemresultado_datadmn3 ,
                                              AV215ExtraWWContagemResultadoEmGarantiaDS_38_Contagemresultado_datadmn_to3 ,
                                              AV218ExtraWWContagemResultadoEmGarantiaDS_41_Contagemresultado_dataprevista3 ,
                                              AV219ExtraWWContagemResultadoEmGarantiaDS_42_Contagemresultado_dataprevista_to3 ,
                                              AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3 ,
                                              AV221ExtraWWContagemResultadoEmGarantiaDS_44_Contagemresultado_servico3 ,
                                              AV222ExtraWWContagemResultadoEmGarantiaDS_45_Contagemresultado_sistemacod3 ,
                                              AV223ExtraWWContagemResultadoEmGarantiaDS_46_Contagemresultado_contratadacod3 ,
                                              AV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3 ,
                                              AV225ExtraWWContagemResultadoEmGarantiaDS_48_Contagemresultado_owner3 ,
                                              AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 ,
                                              AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4 ,
                                              AV228ExtraWWContagemResultadoEmGarantiaDS_51_Dynamicfiltersoperator4 ,
                                              AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 ,
                                              AV230ExtraWWContagemResultadoEmGarantiaDS_53_Contagemresultado_datadmn4 ,
                                              AV231ExtraWWContagemResultadoEmGarantiaDS_54_Contagemresultado_datadmn_to4 ,
                                              AV234ExtraWWContagemResultadoEmGarantiaDS_57_Contagemresultado_dataprevista4 ,
                                              AV235ExtraWWContagemResultadoEmGarantiaDS_58_Contagemresultado_dataprevista_to4 ,
                                              AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4 ,
                                              AV237ExtraWWContagemResultadoEmGarantiaDS_60_Contagemresultado_servico4 ,
                                              AV238ExtraWWContagemResultadoEmGarantiaDS_61_Contagemresultado_sistemacod4 ,
                                              AV239ExtraWWContagemResultadoEmGarantiaDS_62_Contagemresultado_contratadacod4 ,
                                              AV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4 ,
                                              AV241ExtraWWContagemResultadoEmGarantiaDS_64_Contagemresultado_owner4 ,
                                              AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 ,
                                              AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5 ,
                                              AV244ExtraWWContagemResultadoEmGarantiaDS_67_Dynamicfiltersoperator5 ,
                                              AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 ,
                                              AV246ExtraWWContagemResultadoEmGarantiaDS_69_Contagemresultado_datadmn5 ,
                                              AV247ExtraWWContagemResultadoEmGarantiaDS_70_Contagemresultado_datadmn_to5 ,
                                              AV250ExtraWWContagemResultadoEmGarantiaDS_73_Contagemresultado_dataprevista5 ,
                                              AV251ExtraWWContagemResultadoEmGarantiaDS_74_Contagemresultado_dataprevista_to5 ,
                                              AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5 ,
                                              AV253ExtraWWContagemResultadoEmGarantiaDS_76_Contagemresultado_servico5 ,
                                              AV254ExtraWWContagemResultadoEmGarantiaDS_77_Contagemresultado_sistemacod5 ,
                                              AV255ExtraWWContagemResultadoEmGarantiaDS_78_Contagemresultado_contratadacod5 ,
                                              AV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5 ,
                                              AV257ExtraWWContagemResultadoEmGarantiaDS_80_Contagemresultado_owner5 ,
                                              AV259ExtraWWContagemResultadoEmGarantiaDS_82_Tfcontagemresultado_demandafm_sel ,
                                              AV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm ,
                                              AV261ExtraWWContagemResultadoEmGarantiaDS_84_Tfcontagemresultado_demanda_sel ,
                                              AV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda ,
                                              AV263ExtraWWContagemResultadoEmGarantiaDS_86_Tfcontagemresultado_descricao_sel ,
                                              AV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao ,
                                              AV264ExtraWWContagemResultadoEmGarantiaDS_87_Tfcontagemresultado_datadmn ,
                                              AV265ExtraWWContagemResultadoEmGarantiaDS_88_Tfcontagemresultado_datadmn_to ,
                                              AV269ExtraWWContagemResultadoEmGarantiaDS_92_Tfcontagemresultado_contratadasigla_sel ,
                                              AV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla ,
                                              AV270ExtraWWContagemResultadoEmGarantiaDS_93_Tfcontagemresultado_statusdmn_sels.Count ,
                                              AV272ExtraWWContagemResultadoEmGarantiaDS_95_Tfcontagemresultado_servicosigla_sel ,
                                              AV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla ,
                                              A490ContagemResultado_ContratadaCod ,
                                              A457ContagemResultado_Demanda ,
                                              A493ContagemResultado_DemandaFM ,
                                              A471ContagemResultado_DataDmn ,
                                              A1351ContagemResultado_DataPrevista ,
                                              A601ContagemResultado_Servico ,
                                              A489ContagemResultado_SistemaCod ,
                                              A494ContagemResultado_Descricao ,
                                              A508ContagemResultado_Owner ,
                                              A803ContagemResultado_ContratadaSigla ,
                                              A801ContagemResultado_ServicoSigla ,
                                              AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1 ,
                                              A566ContagemResultado_DataUltCnt ,
                                              AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1 ,
                                              AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2 ,
                                              AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2 ,
                                              AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3 ,
                                              AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3 ,
                                              AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4 ,
                                              AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4 ,
                                              AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5 ,
                                              AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5 ,
                                              AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt ,
                                              AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1), "%", "");
         lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1), "%", "");
         lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1), "%", "");
         lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1), "%", "");
         lV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1), "%", "");
         lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2), "%", "");
         lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2), "%", "");
         lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2), "%", "");
         lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2), "%", "");
         lV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2), "%", "");
         lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3), "%", "");
         lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3), "%", "");
         lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3), "%", "");
         lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3), "%", "");
         lV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3), "%", "");
         lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4), "%", "");
         lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4), "%", "");
         lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4), "%", "");
         lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4), "%", "");
         lV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4 = StringUtil.Concat( StringUtil.RTrim( AV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4), "%", "");
         lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5), "%", "");
         lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5), "%", "");
         lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5), "%", "");
         lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5), "%", "");
         lV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5 = StringUtil.Concat( StringUtil.RTrim( AV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5), "%", "");
         lV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm = StringUtil.Concat( StringUtil.RTrim( AV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm), "%", "");
         lV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda = StringUtil.Concat( StringUtil.RTrim( AV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda), "%", "");
         lV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao = StringUtil.Concat( StringUtil.RTrim( AV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao), "%", "");
         lV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla = StringUtil.PadR( StringUtil.RTrim( AV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla), 15, "%");
         lV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla = StringUtil.PadR( StringUtil.RTrim( AV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla), 15, "%");
         /* Using cursor P00U15 */
         pr_default.execute(1, new Object[] {AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1, AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1, AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1, AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1, AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2, AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2, AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2, AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2, AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2, AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2, AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3, AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3, AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3, AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3, AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3, AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3, AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4, AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4, AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4, AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4, AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4, AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4, AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5, AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5, AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5, AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5, AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5, AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5, AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt, AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt, AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to, AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to, AV9WWPContext.gxTpr_Areatrabalho_codigo, AV9WWPContext.gxTpr_Contratada_codigo, AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1, AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1, lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1, lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1, lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1, lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1, AV182ExtraWWContagemResultadoEmGarantiaDS_5_Contagemresultado_datadmn1, AV183ExtraWWContagemResultadoEmGarantiaDS_6_Contagemresultado_datadmn_to1, AV186ExtraWWContagemResultadoEmGarantiaDS_9_Contagemresultado_dataprevista1, AV187ExtraWWContagemResultadoEmGarantiaDS_10_Contagemresultado_dataprevista_to1, AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1, AV189ExtraWWContagemResultadoEmGarantiaDS_12_Contagemresultado_servico1, AV190ExtraWWContagemResultadoEmGarantiaDS_13_Contagemresultado_sistemacod1, AV191ExtraWWContagemResultadoEmGarantiaDS_14_Contagemresultado_contratadacod1, lV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1, AV193ExtraWWContagemResultadoEmGarantiaDS_16_Contagemresultado_owner1, AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2, AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2, lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2, lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2, lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2, lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2, AV198ExtraWWContagemResultadoEmGarantiaDS_21_Contagemresultado_datadmn2, AV199ExtraWWContagemResultadoEmGarantiaDS_22_Contagemresultado_datadmn_to2, AV202ExtraWWContagemResultadoEmGarantiaDS_25_Contagemresultado_dataprevista2, AV203ExtraWWContagemResultadoEmGarantiaDS_26_Contagemresultado_dataprevista_to2, AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2, AV205ExtraWWContagemResultadoEmGarantiaDS_28_Contagemresultado_servico2, AV206ExtraWWContagemResultadoEmGarantiaDS_29_Contagemresultado_sistemacod2, AV207ExtraWWContagemResultadoEmGarantiaDS_30_Contagemresultado_contratadacod2, lV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2, AV209ExtraWWContagemResultadoEmGarantiaDS_32_Contagemresultado_owner2, AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3, AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3, lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3, lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3, lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3, lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3, AV214ExtraWWContagemResultadoEmGarantiaDS_37_Contagemresultado_datadmn3, AV215ExtraWWContagemResultadoEmGarantiaDS_38_Contagemresultado_datadmn_to3, AV218ExtraWWContagemResultadoEmGarantiaDS_41_Contagemresultado_dataprevista3, AV219ExtraWWContagemResultadoEmGarantiaDS_42_Contagemresultado_dataprevista_to3, AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3, AV221ExtraWWContagemResultadoEmGarantiaDS_44_Contagemresultado_servico3, AV222ExtraWWContagemResultadoEmGarantiaDS_45_Contagemresultado_sistemacod3, AV223ExtraWWContagemResultadoEmGarantiaDS_46_Contagemresultado_contratadacod3, lV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3, AV225ExtraWWContagemResultadoEmGarantiaDS_48_Contagemresultado_owner3, AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4, AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4, lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4, lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4, lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4, lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4, AV230ExtraWWContagemResultadoEmGarantiaDS_53_Contagemresultado_datadmn4, AV231ExtraWWContagemResultadoEmGarantiaDS_54_Contagemresultado_datadmn_to4, AV234ExtraWWContagemResultadoEmGarantiaDS_57_Contagemresultado_dataprevista4, AV235ExtraWWContagemResultadoEmGarantiaDS_58_Contagemresultado_dataprevista_to4, AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4, AV237ExtraWWContagemResultadoEmGarantiaDS_60_Contagemresultado_servico4, AV238ExtraWWContagemResultadoEmGarantiaDS_61_Contagemresultado_sistemacod4, AV239ExtraWWContagemResultadoEmGarantiaDS_62_Contagemresultado_contratadacod4, lV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4, AV241ExtraWWContagemResultadoEmGarantiaDS_64_Contagemresultado_owner4, AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5, AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5, lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5, lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5, lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5, lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5, AV246ExtraWWContagemResultadoEmGarantiaDS_69_Contagemresultado_datadmn5, AV247ExtraWWContagemResultadoEmGarantiaDS_70_Contagemresultado_datadmn_to5, AV250ExtraWWContagemResultadoEmGarantiaDS_73_Contagemresultado_dataprevista5, AV251ExtraWWContagemResultadoEmGarantiaDS_74_Contagemresultado_dataprevista_to5, AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5, AV253ExtraWWContagemResultadoEmGarantiaDS_76_Contagemresultado_servico5, AV254ExtraWWContagemResultadoEmGarantiaDS_77_Contagemresultado_sistemacod5, AV255ExtraWWContagemResultadoEmGarantiaDS_78_Contagemresultado_contratadacod5,
         lV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5, AV257ExtraWWContagemResultadoEmGarantiaDS_80_Contagemresultado_owner5, lV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm, AV259ExtraWWContagemResultadoEmGarantiaDS_82_Tfcontagemresultado_demandafm_sel, lV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda, AV261ExtraWWContagemResultadoEmGarantiaDS_84_Tfcontagemresultado_demanda_sel, lV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao, AV263ExtraWWContagemResultadoEmGarantiaDS_86_Tfcontagemresultado_descricao_sel, AV264ExtraWWContagemResultadoEmGarantiaDS_87_Tfcontagemresultado_datadmn, AV265ExtraWWContagemResultadoEmGarantiaDS_88_Tfcontagemresultado_datadmn_to, lV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla, AV269ExtraWWContagemResultadoEmGarantiaDS_92_Tfcontagemresultado_contratadasigla_sel, lV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla, AV272ExtraWWContagemResultadoEmGarantiaDS_95_Tfcontagemresultado_servicosigla_sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKU14 = false;
            A1553ContagemResultado_CntSrvCod = P00U15_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00U15_n1553ContagemResultado_CntSrvCod[0];
            A456ContagemResultado_Codigo = P00U15_A456ContagemResultado_Codigo[0];
            A457ContagemResultado_Demanda = P00U15_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00U15_n457ContagemResultado_Demanda[0];
            A801ContagemResultado_ServicoSigla = P00U15_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00U15_n801ContagemResultado_ServicoSigla[0];
            A803ContagemResultado_ContratadaSigla = P00U15_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P00U15_n803ContagemResultado_ContratadaSigla[0];
            A508ContagemResultado_Owner = P00U15_A508ContagemResultado_Owner[0];
            A494ContagemResultado_Descricao = P00U15_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = P00U15_n494ContagemResultado_Descricao[0];
            A489ContagemResultado_SistemaCod = P00U15_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00U15_n489ContagemResultado_SistemaCod[0];
            A601ContagemResultado_Servico = P00U15_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00U15_n601ContagemResultado_Servico[0];
            A484ContagemResultado_StatusDmn = P00U15_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00U15_n484ContagemResultado_StatusDmn[0];
            A1351ContagemResultado_DataPrevista = P00U15_A1351ContagemResultado_DataPrevista[0];
            n1351ContagemResultado_DataPrevista = P00U15_n1351ContagemResultado_DataPrevista[0];
            A471ContagemResultado_DataDmn = P00U15_A471ContagemResultado_DataDmn[0];
            A493ContagemResultado_DemandaFM = P00U15_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00U15_n493ContagemResultado_DemandaFM[0];
            A490ContagemResultado_ContratadaCod = P00U15_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00U15_n490ContagemResultado_ContratadaCod[0];
            A52Contratada_AreaTrabalhoCod = P00U15_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00U15_n52Contratada_AreaTrabalhoCod[0];
            A566ContagemResultado_DataUltCnt = P00U15_A566ContagemResultado_DataUltCnt[0];
            n566ContagemResultado_DataUltCnt = P00U15_n566ContagemResultado_DataUltCnt[0];
            A601ContagemResultado_Servico = P00U15_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00U15_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = P00U15_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00U15_n801ContagemResultado_ServicoSigla[0];
            A566ContagemResultado_DataUltCnt = P00U15_A566ContagemResultado_DataUltCnt[0];
            n566ContagemResultado_DataUltCnt = P00U15_n566ContagemResultado_DataUltCnt[0];
            A803ContagemResultado_ContratadaSigla = P00U15_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P00U15_n803ContagemResultado_ContratadaSigla[0];
            A52Contratada_AreaTrabalhoCod = P00U15_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00U15_n52Contratada_AreaTrabalhoCod[0];
            AV47count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00U15_A457ContagemResultado_Demanda[0], A457ContagemResultado_Demanda) == 0 ) )
            {
               BRKU14 = false;
               A456ContagemResultado_Codigo = P00U15_A456ContagemResultado_Codigo[0];
               AV47count = (long)(AV47count+1);
               BRKU14 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A457ContagemResultado_Demanda)) )
            {
               AV39Option = A457ContagemResultado_Demanda;
               AV42OptionDesc = StringUtil.Trim( StringUtil.RTrim( context.localUtil.Format( A457ContagemResultado_Demanda, "@!")));
               AV40Options.Add(AV39Option, 0);
               AV43OptionsDesc.Add(AV42OptionDesc, 0);
               AV45OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV47count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV40Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKU14 )
            {
               BRKU14 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADCONTAGEMRESULTADO_DESCRICAOOPTIONS' Routine */
         AV16TFContagemResultado_Descricao = AV35SearchTxt;
         AV17TFContagemResultado_Descricao_Sel = "";
         AV178ExtraWWContagemResultadoEmGarantiaDS_1_Contratada_areatrabalhocod = AV53Contratada_AreaTrabalhoCod;
         AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1 = AV55DynamicFiltersSelector1;
         AV180ExtraWWContagemResultadoEmGarantiaDS_3_Dynamicfiltersoperator1 = AV56DynamicFiltersOperator1;
         AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 = AV60ContagemResultado_OsFsOsFm1;
         AV182ExtraWWContagemResultadoEmGarantiaDS_5_Contagemresultado_datadmn1 = AV61ContagemResultado_DataDmn1;
         AV183ExtraWWContagemResultadoEmGarantiaDS_6_Contagemresultado_datadmn_to1 = AV62ContagemResultado_DataDmn_To1;
         AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1 = AV57ContagemResultado_DataCnt1;
         AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1 = AV58ContagemResultado_DataCnt_To1;
         AV186ExtraWWContagemResultadoEmGarantiaDS_9_Contagemresultado_dataprevista1 = AV63ContagemResultado_DataPrevista1;
         AV187ExtraWWContagemResultadoEmGarantiaDS_10_Contagemresultado_dataprevista_to1 = AV64ContagemResultado_DataPrevista_To1;
         AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1 = AV59ContagemResultado_StatusDmn1;
         AV189ExtraWWContagemResultadoEmGarantiaDS_12_Contagemresultado_servico1 = AV65ContagemResultado_Servico1;
         AV190ExtraWWContagemResultadoEmGarantiaDS_13_Contagemresultado_sistemacod1 = AV67ContagemResultado_SistemaCod1;
         AV191ExtraWWContagemResultadoEmGarantiaDS_14_Contagemresultado_contratadacod1 = AV68ContagemResultado_ContratadaCod1;
         AV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1 = AV75ContagemResultado_Descricao1;
         AV193ExtraWWContagemResultadoEmGarantiaDS_16_Contagemresultado_owner1 = AV76ContagemResultado_Owner1;
         AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 = AV78DynamicFiltersEnabled2;
         AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2 = AV79DynamicFiltersSelector2;
         AV196ExtraWWContagemResultadoEmGarantiaDS_19_Dynamicfiltersoperator2 = AV80DynamicFiltersOperator2;
         AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 = AV84ContagemResultado_OsFsOsFm2;
         AV198ExtraWWContagemResultadoEmGarantiaDS_21_Contagemresultado_datadmn2 = AV85ContagemResultado_DataDmn2;
         AV199ExtraWWContagemResultadoEmGarantiaDS_22_Contagemresultado_datadmn_to2 = AV86ContagemResultado_DataDmn_To2;
         AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2 = AV81ContagemResultado_DataCnt2;
         AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2 = AV82ContagemResultado_DataCnt_To2;
         AV202ExtraWWContagemResultadoEmGarantiaDS_25_Contagemresultado_dataprevista2 = AV87ContagemResultado_DataPrevista2;
         AV203ExtraWWContagemResultadoEmGarantiaDS_26_Contagemresultado_dataprevista_to2 = AV88ContagemResultado_DataPrevista_To2;
         AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2 = AV83ContagemResultado_StatusDmn2;
         AV205ExtraWWContagemResultadoEmGarantiaDS_28_Contagemresultado_servico2 = AV89ContagemResultado_Servico2;
         AV206ExtraWWContagemResultadoEmGarantiaDS_29_Contagemresultado_sistemacod2 = AV91ContagemResultado_SistemaCod2;
         AV207ExtraWWContagemResultadoEmGarantiaDS_30_Contagemresultado_contratadacod2 = AV92ContagemResultado_ContratadaCod2;
         AV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2 = AV99ContagemResultado_Descricao2;
         AV209ExtraWWContagemResultadoEmGarantiaDS_32_Contagemresultado_owner2 = AV100ContagemResultado_Owner2;
         AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 = AV102DynamicFiltersEnabled3;
         AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3 = AV103DynamicFiltersSelector3;
         AV212ExtraWWContagemResultadoEmGarantiaDS_35_Dynamicfiltersoperator3 = AV104DynamicFiltersOperator3;
         AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 = AV108ContagemResultado_OsFsOsFm3;
         AV214ExtraWWContagemResultadoEmGarantiaDS_37_Contagemresultado_datadmn3 = AV109ContagemResultado_DataDmn3;
         AV215ExtraWWContagemResultadoEmGarantiaDS_38_Contagemresultado_datadmn_to3 = AV110ContagemResultado_DataDmn_To3;
         AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3 = AV105ContagemResultado_DataCnt3;
         AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3 = AV106ContagemResultado_DataCnt_To3;
         AV218ExtraWWContagemResultadoEmGarantiaDS_41_Contagemresultado_dataprevista3 = AV111ContagemResultado_DataPrevista3;
         AV219ExtraWWContagemResultadoEmGarantiaDS_42_Contagemresultado_dataprevista_to3 = AV112ContagemResultado_DataPrevista_To3;
         AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3 = AV107ContagemResultado_StatusDmn3;
         AV221ExtraWWContagemResultadoEmGarantiaDS_44_Contagemresultado_servico3 = AV113ContagemResultado_Servico3;
         AV222ExtraWWContagemResultadoEmGarantiaDS_45_Contagemresultado_sistemacod3 = AV115ContagemResultado_SistemaCod3;
         AV223ExtraWWContagemResultadoEmGarantiaDS_46_Contagemresultado_contratadacod3 = AV116ContagemResultado_ContratadaCod3;
         AV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3 = AV123ContagemResultado_Descricao3;
         AV225ExtraWWContagemResultadoEmGarantiaDS_48_Contagemresultado_owner3 = AV124ContagemResultado_Owner3;
         AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 = AV126DynamicFiltersEnabled4;
         AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4 = AV127DynamicFiltersSelector4;
         AV228ExtraWWContagemResultadoEmGarantiaDS_51_Dynamicfiltersoperator4 = AV128DynamicFiltersOperator4;
         AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 = AV132ContagemResultado_OsFsOsFm4;
         AV230ExtraWWContagemResultadoEmGarantiaDS_53_Contagemresultado_datadmn4 = AV133ContagemResultado_DataDmn4;
         AV231ExtraWWContagemResultadoEmGarantiaDS_54_Contagemresultado_datadmn_to4 = AV134ContagemResultado_DataDmn_To4;
         AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4 = AV129ContagemResultado_DataCnt4;
         AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4 = AV130ContagemResultado_DataCnt_To4;
         AV234ExtraWWContagemResultadoEmGarantiaDS_57_Contagemresultado_dataprevista4 = AV135ContagemResultado_DataPrevista4;
         AV235ExtraWWContagemResultadoEmGarantiaDS_58_Contagemresultado_dataprevista_to4 = AV136ContagemResultado_DataPrevista_To4;
         AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4 = AV131ContagemResultado_StatusDmn4;
         AV237ExtraWWContagemResultadoEmGarantiaDS_60_Contagemresultado_servico4 = AV137ContagemResultado_Servico4;
         AV238ExtraWWContagemResultadoEmGarantiaDS_61_Contagemresultado_sistemacod4 = AV139ContagemResultado_SistemaCod4;
         AV239ExtraWWContagemResultadoEmGarantiaDS_62_Contagemresultado_contratadacod4 = AV140ContagemResultado_ContratadaCod4;
         AV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4 = AV147ContagemResultado_Descricao4;
         AV241ExtraWWContagemResultadoEmGarantiaDS_64_Contagemresultado_owner4 = AV148ContagemResultado_Owner4;
         AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 = AV150DynamicFiltersEnabled5;
         AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5 = AV151DynamicFiltersSelector5;
         AV244ExtraWWContagemResultadoEmGarantiaDS_67_Dynamicfiltersoperator5 = AV152DynamicFiltersOperator5;
         AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 = AV156ContagemResultado_OsFsOsFm5;
         AV246ExtraWWContagemResultadoEmGarantiaDS_69_Contagemresultado_datadmn5 = AV157ContagemResultado_DataDmn5;
         AV247ExtraWWContagemResultadoEmGarantiaDS_70_Contagemresultado_datadmn_to5 = AV158ContagemResultado_DataDmn_To5;
         AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5 = AV153ContagemResultado_DataCnt5;
         AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5 = AV154ContagemResultado_DataCnt_To5;
         AV250ExtraWWContagemResultadoEmGarantiaDS_73_Contagemresultado_dataprevista5 = AV159ContagemResultado_DataPrevista5;
         AV251ExtraWWContagemResultadoEmGarantiaDS_74_Contagemresultado_dataprevista_to5 = AV160ContagemResultado_DataPrevista_To5;
         AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5 = AV155ContagemResultado_StatusDmn5;
         AV253ExtraWWContagemResultadoEmGarantiaDS_76_Contagemresultado_servico5 = AV161ContagemResultado_Servico5;
         AV254ExtraWWContagemResultadoEmGarantiaDS_77_Contagemresultado_sistemacod5 = AV163ContagemResultado_SistemaCod5;
         AV255ExtraWWContagemResultadoEmGarantiaDS_78_Contagemresultado_contratadacod5 = AV164ContagemResultado_ContratadaCod5;
         AV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5 = AV171ContagemResultado_Descricao5;
         AV257ExtraWWContagemResultadoEmGarantiaDS_80_Contagemresultado_owner5 = AV172ContagemResultado_Owner5;
         AV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm = AV12TFContagemResultado_DemandaFM;
         AV259ExtraWWContagemResultadoEmGarantiaDS_82_Tfcontagemresultado_demandafm_sel = AV13TFContagemResultado_DemandaFM_Sel;
         AV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda = AV14TFContagemResultado_Demanda;
         AV261ExtraWWContagemResultadoEmGarantiaDS_84_Tfcontagemresultado_demanda_sel = AV15TFContagemResultado_Demanda_Sel;
         AV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao = AV16TFContagemResultado_Descricao;
         AV263ExtraWWContagemResultadoEmGarantiaDS_86_Tfcontagemresultado_descricao_sel = AV17TFContagemResultado_Descricao_Sel;
         AV264ExtraWWContagemResultadoEmGarantiaDS_87_Tfcontagemresultado_datadmn = AV18TFContagemResultado_DataDmn;
         AV265ExtraWWContagemResultadoEmGarantiaDS_88_Tfcontagemresultado_datadmn_to = AV19TFContagemResultado_DataDmn_To;
         AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt = AV20TFContagemResultado_DataUltCnt;
         AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to = AV21TFContagemResultado_DataUltCnt_To;
         AV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla = AV22TFContagemResultado_ContratadaSigla;
         AV269ExtraWWContagemResultadoEmGarantiaDS_92_Tfcontagemresultado_contratadasigla_sel = AV23TFContagemResultado_ContratadaSigla_Sel;
         AV270ExtraWWContagemResultadoEmGarantiaDS_93_Tfcontagemresultado_statusdmn_sels = AV27TFContagemResultado_StatusDmn_Sels;
         AV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla = AV29TFContagemResultado_ServicoSigla;
         AV272ExtraWWContagemResultadoEmGarantiaDS_95_Tfcontagemresultado_servicosigla_sel = AV30TFContagemResultado_ServicoSigla_Sel;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A484ContagemResultado_StatusDmn ,
                                              AV270ExtraWWContagemResultadoEmGarantiaDS_93_Tfcontagemresultado_statusdmn_sels ,
                                              AV9WWPContext.gxTpr_Contratada_codigo ,
                                              AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1 ,
                                              AV180ExtraWWContagemResultadoEmGarantiaDS_3_Dynamicfiltersoperator1 ,
                                              AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 ,
                                              AV182ExtraWWContagemResultadoEmGarantiaDS_5_Contagemresultado_datadmn1 ,
                                              AV183ExtraWWContagemResultadoEmGarantiaDS_6_Contagemresultado_datadmn_to1 ,
                                              AV186ExtraWWContagemResultadoEmGarantiaDS_9_Contagemresultado_dataprevista1 ,
                                              AV187ExtraWWContagemResultadoEmGarantiaDS_10_Contagemresultado_dataprevista_to1 ,
                                              AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1 ,
                                              AV189ExtraWWContagemResultadoEmGarantiaDS_12_Contagemresultado_servico1 ,
                                              AV190ExtraWWContagemResultadoEmGarantiaDS_13_Contagemresultado_sistemacod1 ,
                                              AV191ExtraWWContagemResultadoEmGarantiaDS_14_Contagemresultado_contratadacod1 ,
                                              AV178ExtraWWContagemResultadoEmGarantiaDS_1_Contratada_areatrabalhocod ,
                                              AV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1 ,
                                              AV193ExtraWWContagemResultadoEmGarantiaDS_16_Contagemresultado_owner1 ,
                                              AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 ,
                                              AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2 ,
                                              AV196ExtraWWContagemResultadoEmGarantiaDS_19_Dynamicfiltersoperator2 ,
                                              AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 ,
                                              AV198ExtraWWContagemResultadoEmGarantiaDS_21_Contagemresultado_datadmn2 ,
                                              AV199ExtraWWContagemResultadoEmGarantiaDS_22_Contagemresultado_datadmn_to2 ,
                                              AV202ExtraWWContagemResultadoEmGarantiaDS_25_Contagemresultado_dataprevista2 ,
                                              AV203ExtraWWContagemResultadoEmGarantiaDS_26_Contagemresultado_dataprevista_to2 ,
                                              AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2 ,
                                              AV205ExtraWWContagemResultadoEmGarantiaDS_28_Contagemresultado_servico2 ,
                                              AV206ExtraWWContagemResultadoEmGarantiaDS_29_Contagemresultado_sistemacod2 ,
                                              AV207ExtraWWContagemResultadoEmGarantiaDS_30_Contagemresultado_contratadacod2 ,
                                              AV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2 ,
                                              AV209ExtraWWContagemResultadoEmGarantiaDS_32_Contagemresultado_owner2 ,
                                              AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 ,
                                              AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3 ,
                                              AV212ExtraWWContagemResultadoEmGarantiaDS_35_Dynamicfiltersoperator3 ,
                                              AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 ,
                                              AV214ExtraWWContagemResultadoEmGarantiaDS_37_Contagemresultado_datadmn3 ,
                                              AV215ExtraWWContagemResultadoEmGarantiaDS_38_Contagemresultado_datadmn_to3 ,
                                              AV218ExtraWWContagemResultadoEmGarantiaDS_41_Contagemresultado_dataprevista3 ,
                                              AV219ExtraWWContagemResultadoEmGarantiaDS_42_Contagemresultado_dataprevista_to3 ,
                                              AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3 ,
                                              AV221ExtraWWContagemResultadoEmGarantiaDS_44_Contagemresultado_servico3 ,
                                              AV222ExtraWWContagemResultadoEmGarantiaDS_45_Contagemresultado_sistemacod3 ,
                                              AV223ExtraWWContagemResultadoEmGarantiaDS_46_Contagemresultado_contratadacod3 ,
                                              AV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3 ,
                                              AV225ExtraWWContagemResultadoEmGarantiaDS_48_Contagemresultado_owner3 ,
                                              AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 ,
                                              AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4 ,
                                              AV228ExtraWWContagemResultadoEmGarantiaDS_51_Dynamicfiltersoperator4 ,
                                              AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 ,
                                              AV230ExtraWWContagemResultadoEmGarantiaDS_53_Contagemresultado_datadmn4 ,
                                              AV231ExtraWWContagemResultadoEmGarantiaDS_54_Contagemresultado_datadmn_to4 ,
                                              AV234ExtraWWContagemResultadoEmGarantiaDS_57_Contagemresultado_dataprevista4 ,
                                              AV235ExtraWWContagemResultadoEmGarantiaDS_58_Contagemresultado_dataprevista_to4 ,
                                              AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4 ,
                                              AV237ExtraWWContagemResultadoEmGarantiaDS_60_Contagemresultado_servico4 ,
                                              AV238ExtraWWContagemResultadoEmGarantiaDS_61_Contagemresultado_sistemacod4 ,
                                              AV239ExtraWWContagemResultadoEmGarantiaDS_62_Contagemresultado_contratadacod4 ,
                                              AV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4 ,
                                              AV241ExtraWWContagemResultadoEmGarantiaDS_64_Contagemresultado_owner4 ,
                                              AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 ,
                                              AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5 ,
                                              AV244ExtraWWContagemResultadoEmGarantiaDS_67_Dynamicfiltersoperator5 ,
                                              AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 ,
                                              AV246ExtraWWContagemResultadoEmGarantiaDS_69_Contagemresultado_datadmn5 ,
                                              AV247ExtraWWContagemResultadoEmGarantiaDS_70_Contagemresultado_datadmn_to5 ,
                                              AV250ExtraWWContagemResultadoEmGarantiaDS_73_Contagemresultado_dataprevista5 ,
                                              AV251ExtraWWContagemResultadoEmGarantiaDS_74_Contagemresultado_dataprevista_to5 ,
                                              AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5 ,
                                              AV253ExtraWWContagemResultadoEmGarantiaDS_76_Contagemresultado_servico5 ,
                                              AV254ExtraWWContagemResultadoEmGarantiaDS_77_Contagemresultado_sistemacod5 ,
                                              AV255ExtraWWContagemResultadoEmGarantiaDS_78_Contagemresultado_contratadacod5 ,
                                              AV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5 ,
                                              AV257ExtraWWContagemResultadoEmGarantiaDS_80_Contagemresultado_owner5 ,
                                              AV259ExtraWWContagemResultadoEmGarantiaDS_82_Tfcontagemresultado_demandafm_sel ,
                                              AV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm ,
                                              AV261ExtraWWContagemResultadoEmGarantiaDS_84_Tfcontagemresultado_demanda_sel ,
                                              AV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda ,
                                              AV263ExtraWWContagemResultadoEmGarantiaDS_86_Tfcontagemresultado_descricao_sel ,
                                              AV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao ,
                                              AV264ExtraWWContagemResultadoEmGarantiaDS_87_Tfcontagemresultado_datadmn ,
                                              AV265ExtraWWContagemResultadoEmGarantiaDS_88_Tfcontagemresultado_datadmn_to ,
                                              AV269ExtraWWContagemResultadoEmGarantiaDS_92_Tfcontagemresultado_contratadasigla_sel ,
                                              AV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla ,
                                              AV270ExtraWWContagemResultadoEmGarantiaDS_93_Tfcontagemresultado_statusdmn_sels.Count ,
                                              AV272ExtraWWContagemResultadoEmGarantiaDS_95_Tfcontagemresultado_servicosigla_sel ,
                                              AV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla ,
                                              A490ContagemResultado_ContratadaCod ,
                                              A457ContagemResultado_Demanda ,
                                              A493ContagemResultado_DemandaFM ,
                                              A471ContagemResultado_DataDmn ,
                                              A1351ContagemResultado_DataPrevista ,
                                              A601ContagemResultado_Servico ,
                                              A489ContagemResultado_SistemaCod ,
                                              A494ContagemResultado_Descricao ,
                                              A508ContagemResultado_Owner ,
                                              A803ContagemResultado_ContratadaSigla ,
                                              A801ContagemResultado_ServicoSigla ,
                                              AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1 ,
                                              A566ContagemResultado_DataUltCnt ,
                                              AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1 ,
                                              AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2 ,
                                              AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2 ,
                                              AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3 ,
                                              AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3 ,
                                              AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4 ,
                                              AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4 ,
                                              AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5 ,
                                              AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5 ,
                                              AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt ,
                                              AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1), "%", "");
         lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1), "%", "");
         lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1), "%", "");
         lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1), "%", "");
         lV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1), "%", "");
         lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2), "%", "");
         lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2), "%", "");
         lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2), "%", "");
         lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2), "%", "");
         lV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2), "%", "");
         lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3), "%", "");
         lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3), "%", "");
         lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3), "%", "");
         lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3), "%", "");
         lV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3), "%", "");
         lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4), "%", "");
         lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4), "%", "");
         lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4), "%", "");
         lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4), "%", "");
         lV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4 = StringUtil.Concat( StringUtil.RTrim( AV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4), "%", "");
         lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5), "%", "");
         lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5), "%", "");
         lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5), "%", "");
         lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5), "%", "");
         lV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5 = StringUtil.Concat( StringUtil.RTrim( AV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5), "%", "");
         lV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm = StringUtil.Concat( StringUtil.RTrim( AV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm), "%", "");
         lV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda = StringUtil.Concat( StringUtil.RTrim( AV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda), "%", "");
         lV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao = StringUtil.Concat( StringUtil.RTrim( AV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao), "%", "");
         lV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla = StringUtil.PadR( StringUtil.RTrim( AV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla), 15, "%");
         lV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla = StringUtil.PadR( StringUtil.RTrim( AV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla), 15, "%");
         /* Using cursor P00U17 */
         pr_default.execute(2, new Object[] {AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1, AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1, AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1, AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1, AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2, AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2, AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2, AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2, AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2, AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2, AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3, AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3, AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3, AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3, AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3, AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3, AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4, AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4, AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4, AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4, AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4, AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4, AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5, AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5, AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5, AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5, AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5, AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5, AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt, AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt, AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to, AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to, AV9WWPContext.gxTpr_Areatrabalho_codigo, AV9WWPContext.gxTpr_Contratada_codigo, AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1, AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1, lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1, lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1, lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1, lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1, AV182ExtraWWContagemResultadoEmGarantiaDS_5_Contagemresultado_datadmn1, AV183ExtraWWContagemResultadoEmGarantiaDS_6_Contagemresultado_datadmn_to1, AV186ExtraWWContagemResultadoEmGarantiaDS_9_Contagemresultado_dataprevista1, AV187ExtraWWContagemResultadoEmGarantiaDS_10_Contagemresultado_dataprevista_to1, AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1, AV189ExtraWWContagemResultadoEmGarantiaDS_12_Contagemresultado_servico1, AV190ExtraWWContagemResultadoEmGarantiaDS_13_Contagemresultado_sistemacod1, AV191ExtraWWContagemResultadoEmGarantiaDS_14_Contagemresultado_contratadacod1, lV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1, AV193ExtraWWContagemResultadoEmGarantiaDS_16_Contagemresultado_owner1, AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2, AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2, lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2, lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2, lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2, lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2, AV198ExtraWWContagemResultadoEmGarantiaDS_21_Contagemresultado_datadmn2, AV199ExtraWWContagemResultadoEmGarantiaDS_22_Contagemresultado_datadmn_to2, AV202ExtraWWContagemResultadoEmGarantiaDS_25_Contagemresultado_dataprevista2, AV203ExtraWWContagemResultadoEmGarantiaDS_26_Contagemresultado_dataprevista_to2, AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2, AV205ExtraWWContagemResultadoEmGarantiaDS_28_Contagemresultado_servico2, AV206ExtraWWContagemResultadoEmGarantiaDS_29_Contagemresultado_sistemacod2, AV207ExtraWWContagemResultadoEmGarantiaDS_30_Contagemresultado_contratadacod2, lV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2, AV209ExtraWWContagemResultadoEmGarantiaDS_32_Contagemresultado_owner2, AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3, AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3, lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3, lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3, lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3, lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3, AV214ExtraWWContagemResultadoEmGarantiaDS_37_Contagemresultado_datadmn3, AV215ExtraWWContagemResultadoEmGarantiaDS_38_Contagemresultado_datadmn_to3, AV218ExtraWWContagemResultadoEmGarantiaDS_41_Contagemresultado_dataprevista3, AV219ExtraWWContagemResultadoEmGarantiaDS_42_Contagemresultado_dataprevista_to3, AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3, AV221ExtraWWContagemResultadoEmGarantiaDS_44_Contagemresultado_servico3, AV222ExtraWWContagemResultadoEmGarantiaDS_45_Contagemresultado_sistemacod3, AV223ExtraWWContagemResultadoEmGarantiaDS_46_Contagemresultado_contratadacod3, lV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3, AV225ExtraWWContagemResultadoEmGarantiaDS_48_Contagemresultado_owner3, AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4, AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4, lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4, lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4, lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4, lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4, AV230ExtraWWContagemResultadoEmGarantiaDS_53_Contagemresultado_datadmn4, AV231ExtraWWContagemResultadoEmGarantiaDS_54_Contagemresultado_datadmn_to4, AV234ExtraWWContagemResultadoEmGarantiaDS_57_Contagemresultado_dataprevista4, AV235ExtraWWContagemResultadoEmGarantiaDS_58_Contagemresultado_dataprevista_to4, AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4, AV237ExtraWWContagemResultadoEmGarantiaDS_60_Contagemresultado_servico4, AV238ExtraWWContagemResultadoEmGarantiaDS_61_Contagemresultado_sistemacod4, AV239ExtraWWContagemResultadoEmGarantiaDS_62_Contagemresultado_contratadacod4, lV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4, AV241ExtraWWContagemResultadoEmGarantiaDS_64_Contagemresultado_owner4, AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5, AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5, lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5, lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5, lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5, lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5, AV246ExtraWWContagemResultadoEmGarantiaDS_69_Contagemresultado_datadmn5, AV247ExtraWWContagemResultadoEmGarantiaDS_70_Contagemresultado_datadmn_to5, AV250ExtraWWContagemResultadoEmGarantiaDS_73_Contagemresultado_dataprevista5, AV251ExtraWWContagemResultadoEmGarantiaDS_74_Contagemresultado_dataprevista_to5, AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5, AV253ExtraWWContagemResultadoEmGarantiaDS_76_Contagemresultado_servico5, AV254ExtraWWContagemResultadoEmGarantiaDS_77_Contagemresultado_sistemacod5, AV255ExtraWWContagemResultadoEmGarantiaDS_78_Contagemresultado_contratadacod5,
         lV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5, AV257ExtraWWContagemResultadoEmGarantiaDS_80_Contagemresultado_owner5, lV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm, AV259ExtraWWContagemResultadoEmGarantiaDS_82_Tfcontagemresultado_demandafm_sel, lV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda, AV261ExtraWWContagemResultadoEmGarantiaDS_84_Tfcontagemresultado_demanda_sel, lV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao, AV263ExtraWWContagemResultadoEmGarantiaDS_86_Tfcontagemresultado_descricao_sel, AV264ExtraWWContagemResultadoEmGarantiaDS_87_Tfcontagemresultado_datadmn, AV265ExtraWWContagemResultadoEmGarantiaDS_88_Tfcontagemresultado_datadmn_to, lV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla, AV269ExtraWWContagemResultadoEmGarantiaDS_92_Tfcontagemresultado_contratadasigla_sel, lV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla, AV272ExtraWWContagemResultadoEmGarantiaDS_95_Tfcontagemresultado_servicosigla_sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKU16 = false;
            A1553ContagemResultado_CntSrvCod = P00U17_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00U17_n1553ContagemResultado_CntSrvCod[0];
            A456ContagemResultado_Codigo = P00U17_A456ContagemResultado_Codigo[0];
            A52Contratada_AreaTrabalhoCod = P00U17_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00U17_n52Contratada_AreaTrabalhoCod[0];
            A494ContagemResultado_Descricao = P00U17_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = P00U17_n494ContagemResultado_Descricao[0];
            A801ContagemResultado_ServicoSigla = P00U17_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00U17_n801ContagemResultado_ServicoSigla[0];
            A803ContagemResultado_ContratadaSigla = P00U17_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P00U17_n803ContagemResultado_ContratadaSigla[0];
            A508ContagemResultado_Owner = P00U17_A508ContagemResultado_Owner[0];
            A489ContagemResultado_SistemaCod = P00U17_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00U17_n489ContagemResultado_SistemaCod[0];
            A601ContagemResultado_Servico = P00U17_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00U17_n601ContagemResultado_Servico[0];
            A484ContagemResultado_StatusDmn = P00U17_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00U17_n484ContagemResultado_StatusDmn[0];
            A1351ContagemResultado_DataPrevista = P00U17_A1351ContagemResultado_DataPrevista[0];
            n1351ContagemResultado_DataPrevista = P00U17_n1351ContagemResultado_DataPrevista[0];
            A471ContagemResultado_DataDmn = P00U17_A471ContagemResultado_DataDmn[0];
            A493ContagemResultado_DemandaFM = P00U17_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00U17_n493ContagemResultado_DemandaFM[0];
            A457ContagemResultado_Demanda = P00U17_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00U17_n457ContagemResultado_Demanda[0];
            A490ContagemResultado_ContratadaCod = P00U17_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00U17_n490ContagemResultado_ContratadaCod[0];
            A566ContagemResultado_DataUltCnt = P00U17_A566ContagemResultado_DataUltCnt[0];
            n566ContagemResultado_DataUltCnt = P00U17_n566ContagemResultado_DataUltCnt[0];
            A601ContagemResultado_Servico = P00U17_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00U17_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = P00U17_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00U17_n801ContagemResultado_ServicoSigla[0];
            A566ContagemResultado_DataUltCnt = P00U17_A566ContagemResultado_DataUltCnt[0];
            n566ContagemResultado_DataUltCnt = P00U17_n566ContagemResultado_DataUltCnt[0];
            A52Contratada_AreaTrabalhoCod = P00U17_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00U17_n52Contratada_AreaTrabalhoCod[0];
            A803ContagemResultado_ContratadaSigla = P00U17_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P00U17_n803ContagemResultado_ContratadaSigla[0];
            AV47count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00U17_A494ContagemResultado_Descricao[0], A494ContagemResultado_Descricao) == 0 ) )
            {
               BRKU16 = false;
               A456ContagemResultado_Codigo = P00U17_A456ContagemResultado_Codigo[0];
               AV47count = (long)(AV47count+1);
               BRKU16 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A494ContagemResultado_Descricao)) )
            {
               AV39Option = A494ContagemResultado_Descricao;
               AV40Options.Add(AV39Option, 0);
               AV45OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV47count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV40Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKU16 )
            {
               BRKU16 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      protected void S151( )
      {
         /* 'LOADCONTAGEMRESULTADO_CONTRATADASIGLAOPTIONS' Routine */
         AV22TFContagemResultado_ContratadaSigla = AV35SearchTxt;
         AV23TFContagemResultado_ContratadaSigla_Sel = "";
         AV178ExtraWWContagemResultadoEmGarantiaDS_1_Contratada_areatrabalhocod = AV53Contratada_AreaTrabalhoCod;
         AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1 = AV55DynamicFiltersSelector1;
         AV180ExtraWWContagemResultadoEmGarantiaDS_3_Dynamicfiltersoperator1 = AV56DynamicFiltersOperator1;
         AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 = AV60ContagemResultado_OsFsOsFm1;
         AV182ExtraWWContagemResultadoEmGarantiaDS_5_Contagemresultado_datadmn1 = AV61ContagemResultado_DataDmn1;
         AV183ExtraWWContagemResultadoEmGarantiaDS_6_Contagemresultado_datadmn_to1 = AV62ContagemResultado_DataDmn_To1;
         AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1 = AV57ContagemResultado_DataCnt1;
         AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1 = AV58ContagemResultado_DataCnt_To1;
         AV186ExtraWWContagemResultadoEmGarantiaDS_9_Contagemresultado_dataprevista1 = AV63ContagemResultado_DataPrevista1;
         AV187ExtraWWContagemResultadoEmGarantiaDS_10_Contagemresultado_dataprevista_to1 = AV64ContagemResultado_DataPrevista_To1;
         AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1 = AV59ContagemResultado_StatusDmn1;
         AV189ExtraWWContagemResultadoEmGarantiaDS_12_Contagemresultado_servico1 = AV65ContagemResultado_Servico1;
         AV190ExtraWWContagemResultadoEmGarantiaDS_13_Contagemresultado_sistemacod1 = AV67ContagemResultado_SistemaCod1;
         AV191ExtraWWContagemResultadoEmGarantiaDS_14_Contagemresultado_contratadacod1 = AV68ContagemResultado_ContratadaCod1;
         AV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1 = AV75ContagemResultado_Descricao1;
         AV193ExtraWWContagemResultadoEmGarantiaDS_16_Contagemresultado_owner1 = AV76ContagemResultado_Owner1;
         AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 = AV78DynamicFiltersEnabled2;
         AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2 = AV79DynamicFiltersSelector2;
         AV196ExtraWWContagemResultadoEmGarantiaDS_19_Dynamicfiltersoperator2 = AV80DynamicFiltersOperator2;
         AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 = AV84ContagemResultado_OsFsOsFm2;
         AV198ExtraWWContagemResultadoEmGarantiaDS_21_Contagemresultado_datadmn2 = AV85ContagemResultado_DataDmn2;
         AV199ExtraWWContagemResultadoEmGarantiaDS_22_Contagemresultado_datadmn_to2 = AV86ContagemResultado_DataDmn_To2;
         AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2 = AV81ContagemResultado_DataCnt2;
         AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2 = AV82ContagemResultado_DataCnt_To2;
         AV202ExtraWWContagemResultadoEmGarantiaDS_25_Contagemresultado_dataprevista2 = AV87ContagemResultado_DataPrevista2;
         AV203ExtraWWContagemResultadoEmGarantiaDS_26_Contagemresultado_dataprevista_to2 = AV88ContagemResultado_DataPrevista_To2;
         AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2 = AV83ContagemResultado_StatusDmn2;
         AV205ExtraWWContagemResultadoEmGarantiaDS_28_Contagemresultado_servico2 = AV89ContagemResultado_Servico2;
         AV206ExtraWWContagemResultadoEmGarantiaDS_29_Contagemresultado_sistemacod2 = AV91ContagemResultado_SistemaCod2;
         AV207ExtraWWContagemResultadoEmGarantiaDS_30_Contagemresultado_contratadacod2 = AV92ContagemResultado_ContratadaCod2;
         AV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2 = AV99ContagemResultado_Descricao2;
         AV209ExtraWWContagemResultadoEmGarantiaDS_32_Contagemresultado_owner2 = AV100ContagemResultado_Owner2;
         AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 = AV102DynamicFiltersEnabled3;
         AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3 = AV103DynamicFiltersSelector3;
         AV212ExtraWWContagemResultadoEmGarantiaDS_35_Dynamicfiltersoperator3 = AV104DynamicFiltersOperator3;
         AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 = AV108ContagemResultado_OsFsOsFm3;
         AV214ExtraWWContagemResultadoEmGarantiaDS_37_Contagemresultado_datadmn3 = AV109ContagemResultado_DataDmn3;
         AV215ExtraWWContagemResultadoEmGarantiaDS_38_Contagemresultado_datadmn_to3 = AV110ContagemResultado_DataDmn_To3;
         AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3 = AV105ContagemResultado_DataCnt3;
         AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3 = AV106ContagemResultado_DataCnt_To3;
         AV218ExtraWWContagemResultadoEmGarantiaDS_41_Contagemresultado_dataprevista3 = AV111ContagemResultado_DataPrevista3;
         AV219ExtraWWContagemResultadoEmGarantiaDS_42_Contagemresultado_dataprevista_to3 = AV112ContagemResultado_DataPrevista_To3;
         AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3 = AV107ContagemResultado_StatusDmn3;
         AV221ExtraWWContagemResultadoEmGarantiaDS_44_Contagemresultado_servico3 = AV113ContagemResultado_Servico3;
         AV222ExtraWWContagemResultadoEmGarantiaDS_45_Contagemresultado_sistemacod3 = AV115ContagemResultado_SistemaCod3;
         AV223ExtraWWContagemResultadoEmGarantiaDS_46_Contagemresultado_contratadacod3 = AV116ContagemResultado_ContratadaCod3;
         AV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3 = AV123ContagemResultado_Descricao3;
         AV225ExtraWWContagemResultadoEmGarantiaDS_48_Contagemresultado_owner3 = AV124ContagemResultado_Owner3;
         AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 = AV126DynamicFiltersEnabled4;
         AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4 = AV127DynamicFiltersSelector4;
         AV228ExtraWWContagemResultadoEmGarantiaDS_51_Dynamicfiltersoperator4 = AV128DynamicFiltersOperator4;
         AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 = AV132ContagemResultado_OsFsOsFm4;
         AV230ExtraWWContagemResultadoEmGarantiaDS_53_Contagemresultado_datadmn4 = AV133ContagemResultado_DataDmn4;
         AV231ExtraWWContagemResultadoEmGarantiaDS_54_Contagemresultado_datadmn_to4 = AV134ContagemResultado_DataDmn_To4;
         AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4 = AV129ContagemResultado_DataCnt4;
         AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4 = AV130ContagemResultado_DataCnt_To4;
         AV234ExtraWWContagemResultadoEmGarantiaDS_57_Contagemresultado_dataprevista4 = AV135ContagemResultado_DataPrevista4;
         AV235ExtraWWContagemResultadoEmGarantiaDS_58_Contagemresultado_dataprevista_to4 = AV136ContagemResultado_DataPrevista_To4;
         AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4 = AV131ContagemResultado_StatusDmn4;
         AV237ExtraWWContagemResultadoEmGarantiaDS_60_Contagemresultado_servico4 = AV137ContagemResultado_Servico4;
         AV238ExtraWWContagemResultadoEmGarantiaDS_61_Contagemresultado_sistemacod4 = AV139ContagemResultado_SistemaCod4;
         AV239ExtraWWContagemResultadoEmGarantiaDS_62_Contagemresultado_contratadacod4 = AV140ContagemResultado_ContratadaCod4;
         AV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4 = AV147ContagemResultado_Descricao4;
         AV241ExtraWWContagemResultadoEmGarantiaDS_64_Contagemresultado_owner4 = AV148ContagemResultado_Owner4;
         AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 = AV150DynamicFiltersEnabled5;
         AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5 = AV151DynamicFiltersSelector5;
         AV244ExtraWWContagemResultadoEmGarantiaDS_67_Dynamicfiltersoperator5 = AV152DynamicFiltersOperator5;
         AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 = AV156ContagemResultado_OsFsOsFm5;
         AV246ExtraWWContagemResultadoEmGarantiaDS_69_Contagemresultado_datadmn5 = AV157ContagemResultado_DataDmn5;
         AV247ExtraWWContagemResultadoEmGarantiaDS_70_Contagemresultado_datadmn_to5 = AV158ContagemResultado_DataDmn_To5;
         AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5 = AV153ContagemResultado_DataCnt5;
         AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5 = AV154ContagemResultado_DataCnt_To5;
         AV250ExtraWWContagemResultadoEmGarantiaDS_73_Contagemresultado_dataprevista5 = AV159ContagemResultado_DataPrevista5;
         AV251ExtraWWContagemResultadoEmGarantiaDS_74_Contagemresultado_dataprevista_to5 = AV160ContagemResultado_DataPrevista_To5;
         AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5 = AV155ContagemResultado_StatusDmn5;
         AV253ExtraWWContagemResultadoEmGarantiaDS_76_Contagemresultado_servico5 = AV161ContagemResultado_Servico5;
         AV254ExtraWWContagemResultadoEmGarantiaDS_77_Contagemresultado_sistemacod5 = AV163ContagemResultado_SistemaCod5;
         AV255ExtraWWContagemResultadoEmGarantiaDS_78_Contagemresultado_contratadacod5 = AV164ContagemResultado_ContratadaCod5;
         AV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5 = AV171ContagemResultado_Descricao5;
         AV257ExtraWWContagemResultadoEmGarantiaDS_80_Contagemresultado_owner5 = AV172ContagemResultado_Owner5;
         AV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm = AV12TFContagemResultado_DemandaFM;
         AV259ExtraWWContagemResultadoEmGarantiaDS_82_Tfcontagemresultado_demandafm_sel = AV13TFContagemResultado_DemandaFM_Sel;
         AV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda = AV14TFContagemResultado_Demanda;
         AV261ExtraWWContagemResultadoEmGarantiaDS_84_Tfcontagemresultado_demanda_sel = AV15TFContagemResultado_Demanda_Sel;
         AV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao = AV16TFContagemResultado_Descricao;
         AV263ExtraWWContagemResultadoEmGarantiaDS_86_Tfcontagemresultado_descricao_sel = AV17TFContagemResultado_Descricao_Sel;
         AV264ExtraWWContagemResultadoEmGarantiaDS_87_Tfcontagemresultado_datadmn = AV18TFContagemResultado_DataDmn;
         AV265ExtraWWContagemResultadoEmGarantiaDS_88_Tfcontagemresultado_datadmn_to = AV19TFContagemResultado_DataDmn_To;
         AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt = AV20TFContagemResultado_DataUltCnt;
         AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to = AV21TFContagemResultado_DataUltCnt_To;
         AV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla = AV22TFContagemResultado_ContratadaSigla;
         AV269ExtraWWContagemResultadoEmGarantiaDS_92_Tfcontagemresultado_contratadasigla_sel = AV23TFContagemResultado_ContratadaSigla_Sel;
         AV270ExtraWWContagemResultadoEmGarantiaDS_93_Tfcontagemresultado_statusdmn_sels = AV27TFContagemResultado_StatusDmn_Sels;
         AV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla = AV29TFContagemResultado_ServicoSigla;
         AV272ExtraWWContagemResultadoEmGarantiaDS_95_Tfcontagemresultado_servicosigla_sel = AV30TFContagemResultado_ServicoSigla_Sel;
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              A484ContagemResultado_StatusDmn ,
                                              AV270ExtraWWContagemResultadoEmGarantiaDS_93_Tfcontagemresultado_statusdmn_sels ,
                                              AV9WWPContext.gxTpr_Contratada_codigo ,
                                              AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1 ,
                                              AV180ExtraWWContagemResultadoEmGarantiaDS_3_Dynamicfiltersoperator1 ,
                                              AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 ,
                                              AV182ExtraWWContagemResultadoEmGarantiaDS_5_Contagemresultado_datadmn1 ,
                                              AV183ExtraWWContagemResultadoEmGarantiaDS_6_Contagemresultado_datadmn_to1 ,
                                              AV186ExtraWWContagemResultadoEmGarantiaDS_9_Contagemresultado_dataprevista1 ,
                                              AV187ExtraWWContagemResultadoEmGarantiaDS_10_Contagemresultado_dataprevista_to1 ,
                                              AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1 ,
                                              AV189ExtraWWContagemResultadoEmGarantiaDS_12_Contagemresultado_servico1 ,
                                              AV190ExtraWWContagemResultadoEmGarantiaDS_13_Contagemresultado_sistemacod1 ,
                                              AV191ExtraWWContagemResultadoEmGarantiaDS_14_Contagemresultado_contratadacod1 ,
                                              AV178ExtraWWContagemResultadoEmGarantiaDS_1_Contratada_areatrabalhocod ,
                                              AV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1 ,
                                              AV193ExtraWWContagemResultadoEmGarantiaDS_16_Contagemresultado_owner1 ,
                                              AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 ,
                                              AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2 ,
                                              AV196ExtraWWContagemResultadoEmGarantiaDS_19_Dynamicfiltersoperator2 ,
                                              AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 ,
                                              AV198ExtraWWContagemResultadoEmGarantiaDS_21_Contagemresultado_datadmn2 ,
                                              AV199ExtraWWContagemResultadoEmGarantiaDS_22_Contagemresultado_datadmn_to2 ,
                                              AV202ExtraWWContagemResultadoEmGarantiaDS_25_Contagemresultado_dataprevista2 ,
                                              AV203ExtraWWContagemResultadoEmGarantiaDS_26_Contagemresultado_dataprevista_to2 ,
                                              AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2 ,
                                              AV205ExtraWWContagemResultadoEmGarantiaDS_28_Contagemresultado_servico2 ,
                                              AV206ExtraWWContagemResultadoEmGarantiaDS_29_Contagemresultado_sistemacod2 ,
                                              AV207ExtraWWContagemResultadoEmGarantiaDS_30_Contagemresultado_contratadacod2 ,
                                              AV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2 ,
                                              AV209ExtraWWContagemResultadoEmGarantiaDS_32_Contagemresultado_owner2 ,
                                              AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 ,
                                              AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3 ,
                                              AV212ExtraWWContagemResultadoEmGarantiaDS_35_Dynamicfiltersoperator3 ,
                                              AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 ,
                                              AV214ExtraWWContagemResultadoEmGarantiaDS_37_Contagemresultado_datadmn3 ,
                                              AV215ExtraWWContagemResultadoEmGarantiaDS_38_Contagemresultado_datadmn_to3 ,
                                              AV218ExtraWWContagemResultadoEmGarantiaDS_41_Contagemresultado_dataprevista3 ,
                                              AV219ExtraWWContagemResultadoEmGarantiaDS_42_Contagemresultado_dataprevista_to3 ,
                                              AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3 ,
                                              AV221ExtraWWContagemResultadoEmGarantiaDS_44_Contagemresultado_servico3 ,
                                              AV222ExtraWWContagemResultadoEmGarantiaDS_45_Contagemresultado_sistemacod3 ,
                                              AV223ExtraWWContagemResultadoEmGarantiaDS_46_Contagemresultado_contratadacod3 ,
                                              AV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3 ,
                                              AV225ExtraWWContagemResultadoEmGarantiaDS_48_Contagemresultado_owner3 ,
                                              AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 ,
                                              AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4 ,
                                              AV228ExtraWWContagemResultadoEmGarantiaDS_51_Dynamicfiltersoperator4 ,
                                              AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 ,
                                              AV230ExtraWWContagemResultadoEmGarantiaDS_53_Contagemresultado_datadmn4 ,
                                              AV231ExtraWWContagemResultadoEmGarantiaDS_54_Contagemresultado_datadmn_to4 ,
                                              AV234ExtraWWContagemResultadoEmGarantiaDS_57_Contagemresultado_dataprevista4 ,
                                              AV235ExtraWWContagemResultadoEmGarantiaDS_58_Contagemresultado_dataprevista_to4 ,
                                              AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4 ,
                                              AV237ExtraWWContagemResultadoEmGarantiaDS_60_Contagemresultado_servico4 ,
                                              AV238ExtraWWContagemResultadoEmGarantiaDS_61_Contagemresultado_sistemacod4 ,
                                              AV239ExtraWWContagemResultadoEmGarantiaDS_62_Contagemresultado_contratadacod4 ,
                                              AV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4 ,
                                              AV241ExtraWWContagemResultadoEmGarantiaDS_64_Contagemresultado_owner4 ,
                                              AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 ,
                                              AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5 ,
                                              AV244ExtraWWContagemResultadoEmGarantiaDS_67_Dynamicfiltersoperator5 ,
                                              AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 ,
                                              AV246ExtraWWContagemResultadoEmGarantiaDS_69_Contagemresultado_datadmn5 ,
                                              AV247ExtraWWContagemResultadoEmGarantiaDS_70_Contagemresultado_datadmn_to5 ,
                                              AV250ExtraWWContagemResultadoEmGarantiaDS_73_Contagemresultado_dataprevista5 ,
                                              AV251ExtraWWContagemResultadoEmGarantiaDS_74_Contagemresultado_dataprevista_to5 ,
                                              AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5 ,
                                              AV253ExtraWWContagemResultadoEmGarantiaDS_76_Contagemresultado_servico5 ,
                                              AV254ExtraWWContagemResultadoEmGarantiaDS_77_Contagemresultado_sistemacod5 ,
                                              AV255ExtraWWContagemResultadoEmGarantiaDS_78_Contagemresultado_contratadacod5 ,
                                              AV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5 ,
                                              AV257ExtraWWContagemResultadoEmGarantiaDS_80_Contagemresultado_owner5 ,
                                              AV259ExtraWWContagemResultadoEmGarantiaDS_82_Tfcontagemresultado_demandafm_sel ,
                                              AV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm ,
                                              AV261ExtraWWContagemResultadoEmGarantiaDS_84_Tfcontagemresultado_demanda_sel ,
                                              AV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda ,
                                              AV263ExtraWWContagemResultadoEmGarantiaDS_86_Tfcontagemresultado_descricao_sel ,
                                              AV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao ,
                                              AV264ExtraWWContagemResultadoEmGarantiaDS_87_Tfcontagemresultado_datadmn ,
                                              AV265ExtraWWContagemResultadoEmGarantiaDS_88_Tfcontagemresultado_datadmn_to ,
                                              AV269ExtraWWContagemResultadoEmGarantiaDS_92_Tfcontagemresultado_contratadasigla_sel ,
                                              AV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla ,
                                              AV270ExtraWWContagemResultadoEmGarantiaDS_93_Tfcontagemresultado_statusdmn_sels.Count ,
                                              AV272ExtraWWContagemResultadoEmGarantiaDS_95_Tfcontagemresultado_servicosigla_sel ,
                                              AV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla ,
                                              A490ContagemResultado_ContratadaCod ,
                                              A457ContagemResultado_Demanda ,
                                              A493ContagemResultado_DemandaFM ,
                                              A471ContagemResultado_DataDmn ,
                                              A1351ContagemResultado_DataPrevista ,
                                              A601ContagemResultado_Servico ,
                                              A489ContagemResultado_SistemaCod ,
                                              A494ContagemResultado_Descricao ,
                                              A508ContagemResultado_Owner ,
                                              A803ContagemResultado_ContratadaSigla ,
                                              A801ContagemResultado_ServicoSigla ,
                                              AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1 ,
                                              A566ContagemResultado_DataUltCnt ,
                                              AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1 ,
                                              AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2 ,
                                              AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2 ,
                                              AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3 ,
                                              AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3 ,
                                              AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4 ,
                                              AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4 ,
                                              AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5 ,
                                              AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5 ,
                                              AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt ,
                                              AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1), "%", "");
         lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1), "%", "");
         lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1), "%", "");
         lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1), "%", "");
         lV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1), "%", "");
         lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2), "%", "");
         lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2), "%", "");
         lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2), "%", "");
         lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2), "%", "");
         lV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2), "%", "");
         lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3), "%", "");
         lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3), "%", "");
         lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3), "%", "");
         lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3), "%", "");
         lV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3), "%", "");
         lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4), "%", "");
         lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4), "%", "");
         lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4), "%", "");
         lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4), "%", "");
         lV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4 = StringUtil.Concat( StringUtil.RTrim( AV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4), "%", "");
         lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5), "%", "");
         lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5), "%", "");
         lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5), "%", "");
         lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5), "%", "");
         lV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5 = StringUtil.Concat( StringUtil.RTrim( AV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5), "%", "");
         lV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm = StringUtil.Concat( StringUtil.RTrim( AV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm), "%", "");
         lV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda = StringUtil.Concat( StringUtil.RTrim( AV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda), "%", "");
         lV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao = StringUtil.Concat( StringUtil.RTrim( AV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao), "%", "");
         lV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla = StringUtil.PadR( StringUtil.RTrim( AV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla), 15, "%");
         lV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla = StringUtil.PadR( StringUtil.RTrim( AV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla), 15, "%");
         /* Using cursor P00U19 */
         pr_default.execute(3, new Object[] {AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1, AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1, AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1, AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1, AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2, AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2, AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2, AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2, AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2, AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2, AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3, AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3, AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3, AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3, AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3, AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3, AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4, AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4, AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4, AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4, AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4, AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4, AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5, AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5, AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5, AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5, AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5, AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5, AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt, AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt, AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to, AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to, AV9WWPContext.gxTpr_Areatrabalho_codigo, AV9WWPContext.gxTpr_Contratada_codigo, AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1, AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1, lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1, lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1, lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1, lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1, AV182ExtraWWContagemResultadoEmGarantiaDS_5_Contagemresultado_datadmn1, AV183ExtraWWContagemResultadoEmGarantiaDS_6_Contagemresultado_datadmn_to1, AV186ExtraWWContagemResultadoEmGarantiaDS_9_Contagemresultado_dataprevista1, AV187ExtraWWContagemResultadoEmGarantiaDS_10_Contagemresultado_dataprevista_to1, AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1, AV189ExtraWWContagemResultadoEmGarantiaDS_12_Contagemresultado_servico1, AV190ExtraWWContagemResultadoEmGarantiaDS_13_Contagemresultado_sistemacod1, AV191ExtraWWContagemResultadoEmGarantiaDS_14_Contagemresultado_contratadacod1, lV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1, AV193ExtraWWContagemResultadoEmGarantiaDS_16_Contagemresultado_owner1, AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2, AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2, lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2, lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2, lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2, lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2, AV198ExtraWWContagemResultadoEmGarantiaDS_21_Contagemresultado_datadmn2, AV199ExtraWWContagemResultadoEmGarantiaDS_22_Contagemresultado_datadmn_to2, AV202ExtraWWContagemResultadoEmGarantiaDS_25_Contagemresultado_dataprevista2, AV203ExtraWWContagemResultadoEmGarantiaDS_26_Contagemresultado_dataprevista_to2, AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2, AV205ExtraWWContagemResultadoEmGarantiaDS_28_Contagemresultado_servico2, AV206ExtraWWContagemResultadoEmGarantiaDS_29_Contagemresultado_sistemacod2, AV207ExtraWWContagemResultadoEmGarantiaDS_30_Contagemresultado_contratadacod2, lV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2, AV209ExtraWWContagemResultadoEmGarantiaDS_32_Contagemresultado_owner2, AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3, AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3, lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3, lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3, lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3, lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3, AV214ExtraWWContagemResultadoEmGarantiaDS_37_Contagemresultado_datadmn3, AV215ExtraWWContagemResultadoEmGarantiaDS_38_Contagemresultado_datadmn_to3, AV218ExtraWWContagemResultadoEmGarantiaDS_41_Contagemresultado_dataprevista3, AV219ExtraWWContagemResultadoEmGarantiaDS_42_Contagemresultado_dataprevista_to3, AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3, AV221ExtraWWContagemResultadoEmGarantiaDS_44_Contagemresultado_servico3, AV222ExtraWWContagemResultadoEmGarantiaDS_45_Contagemresultado_sistemacod3, AV223ExtraWWContagemResultadoEmGarantiaDS_46_Contagemresultado_contratadacod3, lV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3, AV225ExtraWWContagemResultadoEmGarantiaDS_48_Contagemresultado_owner3, AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4, AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4, lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4, lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4, lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4, lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4, AV230ExtraWWContagemResultadoEmGarantiaDS_53_Contagemresultado_datadmn4, AV231ExtraWWContagemResultadoEmGarantiaDS_54_Contagemresultado_datadmn_to4, AV234ExtraWWContagemResultadoEmGarantiaDS_57_Contagemresultado_dataprevista4, AV235ExtraWWContagemResultadoEmGarantiaDS_58_Contagemresultado_dataprevista_to4, AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4, AV237ExtraWWContagemResultadoEmGarantiaDS_60_Contagemresultado_servico4, AV238ExtraWWContagemResultadoEmGarantiaDS_61_Contagemresultado_sistemacod4, AV239ExtraWWContagemResultadoEmGarantiaDS_62_Contagemresultado_contratadacod4, lV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4, AV241ExtraWWContagemResultadoEmGarantiaDS_64_Contagemresultado_owner4, AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5, AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5, lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5, lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5, lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5, lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5, AV246ExtraWWContagemResultadoEmGarantiaDS_69_Contagemresultado_datadmn5, AV247ExtraWWContagemResultadoEmGarantiaDS_70_Contagemresultado_datadmn_to5, AV250ExtraWWContagemResultadoEmGarantiaDS_73_Contagemresultado_dataprevista5, AV251ExtraWWContagemResultadoEmGarantiaDS_74_Contagemresultado_dataprevista_to5, AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5, AV253ExtraWWContagemResultadoEmGarantiaDS_76_Contagemresultado_servico5, AV254ExtraWWContagemResultadoEmGarantiaDS_77_Contagemresultado_sistemacod5, AV255ExtraWWContagemResultadoEmGarantiaDS_78_Contagemresultado_contratadacod5,
         lV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5, AV257ExtraWWContagemResultadoEmGarantiaDS_80_Contagemresultado_owner5, lV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm, AV259ExtraWWContagemResultadoEmGarantiaDS_82_Tfcontagemresultado_demandafm_sel, lV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda, AV261ExtraWWContagemResultadoEmGarantiaDS_84_Tfcontagemresultado_demanda_sel, lV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao, AV263ExtraWWContagemResultadoEmGarantiaDS_86_Tfcontagemresultado_descricao_sel, AV264ExtraWWContagemResultadoEmGarantiaDS_87_Tfcontagemresultado_datadmn, AV265ExtraWWContagemResultadoEmGarantiaDS_88_Tfcontagemresultado_datadmn_to, lV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla, AV269ExtraWWContagemResultadoEmGarantiaDS_92_Tfcontagemresultado_contratadasigla_sel, lV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla, AV272ExtraWWContagemResultadoEmGarantiaDS_95_Tfcontagemresultado_servicosigla_sel});
         while ( (pr_default.getStatus(3) != 101) )
         {
            BRKU18 = false;
            A1553ContagemResultado_CntSrvCod = P00U19_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00U19_n1553ContagemResultado_CntSrvCod[0];
            A456ContagemResultado_Codigo = P00U19_A456ContagemResultado_Codigo[0];
            A52Contratada_AreaTrabalhoCod = P00U19_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00U19_n52Contratada_AreaTrabalhoCod[0];
            A803ContagemResultado_ContratadaSigla = P00U19_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P00U19_n803ContagemResultado_ContratadaSigla[0];
            A801ContagemResultado_ServicoSigla = P00U19_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00U19_n801ContagemResultado_ServicoSigla[0];
            A508ContagemResultado_Owner = P00U19_A508ContagemResultado_Owner[0];
            A494ContagemResultado_Descricao = P00U19_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = P00U19_n494ContagemResultado_Descricao[0];
            A489ContagemResultado_SistemaCod = P00U19_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00U19_n489ContagemResultado_SistemaCod[0];
            A601ContagemResultado_Servico = P00U19_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00U19_n601ContagemResultado_Servico[0];
            A484ContagemResultado_StatusDmn = P00U19_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00U19_n484ContagemResultado_StatusDmn[0];
            A1351ContagemResultado_DataPrevista = P00U19_A1351ContagemResultado_DataPrevista[0];
            n1351ContagemResultado_DataPrevista = P00U19_n1351ContagemResultado_DataPrevista[0];
            A471ContagemResultado_DataDmn = P00U19_A471ContagemResultado_DataDmn[0];
            A493ContagemResultado_DemandaFM = P00U19_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00U19_n493ContagemResultado_DemandaFM[0];
            A457ContagemResultado_Demanda = P00U19_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00U19_n457ContagemResultado_Demanda[0];
            A490ContagemResultado_ContratadaCod = P00U19_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00U19_n490ContagemResultado_ContratadaCod[0];
            A566ContagemResultado_DataUltCnt = P00U19_A566ContagemResultado_DataUltCnt[0];
            n566ContagemResultado_DataUltCnt = P00U19_n566ContagemResultado_DataUltCnt[0];
            A601ContagemResultado_Servico = P00U19_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00U19_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = P00U19_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00U19_n801ContagemResultado_ServicoSigla[0];
            A566ContagemResultado_DataUltCnt = P00U19_A566ContagemResultado_DataUltCnt[0];
            n566ContagemResultado_DataUltCnt = P00U19_n566ContagemResultado_DataUltCnt[0];
            A52Contratada_AreaTrabalhoCod = P00U19_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00U19_n52Contratada_AreaTrabalhoCod[0];
            A803ContagemResultado_ContratadaSigla = P00U19_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P00U19_n803ContagemResultado_ContratadaSigla[0];
            AV47count = 0;
            while ( (pr_default.getStatus(3) != 101) && ( StringUtil.StrCmp(P00U19_A803ContagemResultado_ContratadaSigla[0], A803ContagemResultado_ContratadaSigla) == 0 ) )
            {
               BRKU18 = false;
               A456ContagemResultado_Codigo = P00U19_A456ContagemResultado_Codigo[0];
               A490ContagemResultado_ContratadaCod = P00U19_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = P00U19_n490ContagemResultado_ContratadaCod[0];
               AV47count = (long)(AV47count+1);
               BRKU18 = true;
               pr_default.readNext(3);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A803ContagemResultado_ContratadaSigla)) )
            {
               AV39Option = A803ContagemResultado_ContratadaSigla;
               AV40Options.Add(AV39Option, 0);
               AV45OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV47count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV40Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKU18 )
            {
               BRKU18 = true;
               pr_default.readNext(3);
            }
         }
         pr_default.close(3);
      }

      protected void S161( )
      {
         /* 'LOADCONTAGEMRESULTADO_SERVICOSIGLAOPTIONS' Routine */
         AV29TFContagemResultado_ServicoSigla = AV35SearchTxt;
         AV30TFContagemResultado_ServicoSigla_Sel = "";
         AV178ExtraWWContagemResultadoEmGarantiaDS_1_Contratada_areatrabalhocod = AV53Contratada_AreaTrabalhoCod;
         AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1 = AV55DynamicFiltersSelector1;
         AV180ExtraWWContagemResultadoEmGarantiaDS_3_Dynamicfiltersoperator1 = AV56DynamicFiltersOperator1;
         AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 = AV60ContagemResultado_OsFsOsFm1;
         AV182ExtraWWContagemResultadoEmGarantiaDS_5_Contagemresultado_datadmn1 = AV61ContagemResultado_DataDmn1;
         AV183ExtraWWContagemResultadoEmGarantiaDS_6_Contagemresultado_datadmn_to1 = AV62ContagemResultado_DataDmn_To1;
         AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1 = AV57ContagemResultado_DataCnt1;
         AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1 = AV58ContagemResultado_DataCnt_To1;
         AV186ExtraWWContagemResultadoEmGarantiaDS_9_Contagemresultado_dataprevista1 = AV63ContagemResultado_DataPrevista1;
         AV187ExtraWWContagemResultadoEmGarantiaDS_10_Contagemresultado_dataprevista_to1 = AV64ContagemResultado_DataPrevista_To1;
         AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1 = AV59ContagemResultado_StatusDmn1;
         AV189ExtraWWContagemResultadoEmGarantiaDS_12_Contagemresultado_servico1 = AV65ContagemResultado_Servico1;
         AV190ExtraWWContagemResultadoEmGarantiaDS_13_Contagemresultado_sistemacod1 = AV67ContagemResultado_SistemaCod1;
         AV191ExtraWWContagemResultadoEmGarantiaDS_14_Contagemresultado_contratadacod1 = AV68ContagemResultado_ContratadaCod1;
         AV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1 = AV75ContagemResultado_Descricao1;
         AV193ExtraWWContagemResultadoEmGarantiaDS_16_Contagemresultado_owner1 = AV76ContagemResultado_Owner1;
         AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 = AV78DynamicFiltersEnabled2;
         AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2 = AV79DynamicFiltersSelector2;
         AV196ExtraWWContagemResultadoEmGarantiaDS_19_Dynamicfiltersoperator2 = AV80DynamicFiltersOperator2;
         AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 = AV84ContagemResultado_OsFsOsFm2;
         AV198ExtraWWContagemResultadoEmGarantiaDS_21_Contagemresultado_datadmn2 = AV85ContagemResultado_DataDmn2;
         AV199ExtraWWContagemResultadoEmGarantiaDS_22_Contagemresultado_datadmn_to2 = AV86ContagemResultado_DataDmn_To2;
         AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2 = AV81ContagemResultado_DataCnt2;
         AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2 = AV82ContagemResultado_DataCnt_To2;
         AV202ExtraWWContagemResultadoEmGarantiaDS_25_Contagemresultado_dataprevista2 = AV87ContagemResultado_DataPrevista2;
         AV203ExtraWWContagemResultadoEmGarantiaDS_26_Contagemresultado_dataprevista_to2 = AV88ContagemResultado_DataPrevista_To2;
         AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2 = AV83ContagemResultado_StatusDmn2;
         AV205ExtraWWContagemResultadoEmGarantiaDS_28_Contagemresultado_servico2 = AV89ContagemResultado_Servico2;
         AV206ExtraWWContagemResultadoEmGarantiaDS_29_Contagemresultado_sistemacod2 = AV91ContagemResultado_SistemaCod2;
         AV207ExtraWWContagemResultadoEmGarantiaDS_30_Contagemresultado_contratadacod2 = AV92ContagemResultado_ContratadaCod2;
         AV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2 = AV99ContagemResultado_Descricao2;
         AV209ExtraWWContagemResultadoEmGarantiaDS_32_Contagemresultado_owner2 = AV100ContagemResultado_Owner2;
         AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 = AV102DynamicFiltersEnabled3;
         AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3 = AV103DynamicFiltersSelector3;
         AV212ExtraWWContagemResultadoEmGarantiaDS_35_Dynamicfiltersoperator3 = AV104DynamicFiltersOperator3;
         AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 = AV108ContagemResultado_OsFsOsFm3;
         AV214ExtraWWContagemResultadoEmGarantiaDS_37_Contagemresultado_datadmn3 = AV109ContagemResultado_DataDmn3;
         AV215ExtraWWContagemResultadoEmGarantiaDS_38_Contagemresultado_datadmn_to3 = AV110ContagemResultado_DataDmn_To3;
         AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3 = AV105ContagemResultado_DataCnt3;
         AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3 = AV106ContagemResultado_DataCnt_To3;
         AV218ExtraWWContagemResultadoEmGarantiaDS_41_Contagemresultado_dataprevista3 = AV111ContagemResultado_DataPrevista3;
         AV219ExtraWWContagemResultadoEmGarantiaDS_42_Contagemresultado_dataprevista_to3 = AV112ContagemResultado_DataPrevista_To3;
         AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3 = AV107ContagemResultado_StatusDmn3;
         AV221ExtraWWContagemResultadoEmGarantiaDS_44_Contagemresultado_servico3 = AV113ContagemResultado_Servico3;
         AV222ExtraWWContagemResultadoEmGarantiaDS_45_Contagemresultado_sistemacod3 = AV115ContagemResultado_SistemaCod3;
         AV223ExtraWWContagemResultadoEmGarantiaDS_46_Contagemresultado_contratadacod3 = AV116ContagemResultado_ContratadaCod3;
         AV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3 = AV123ContagemResultado_Descricao3;
         AV225ExtraWWContagemResultadoEmGarantiaDS_48_Contagemresultado_owner3 = AV124ContagemResultado_Owner3;
         AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 = AV126DynamicFiltersEnabled4;
         AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4 = AV127DynamicFiltersSelector4;
         AV228ExtraWWContagemResultadoEmGarantiaDS_51_Dynamicfiltersoperator4 = AV128DynamicFiltersOperator4;
         AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 = AV132ContagemResultado_OsFsOsFm4;
         AV230ExtraWWContagemResultadoEmGarantiaDS_53_Contagemresultado_datadmn4 = AV133ContagemResultado_DataDmn4;
         AV231ExtraWWContagemResultadoEmGarantiaDS_54_Contagemresultado_datadmn_to4 = AV134ContagemResultado_DataDmn_To4;
         AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4 = AV129ContagemResultado_DataCnt4;
         AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4 = AV130ContagemResultado_DataCnt_To4;
         AV234ExtraWWContagemResultadoEmGarantiaDS_57_Contagemresultado_dataprevista4 = AV135ContagemResultado_DataPrevista4;
         AV235ExtraWWContagemResultadoEmGarantiaDS_58_Contagemresultado_dataprevista_to4 = AV136ContagemResultado_DataPrevista_To4;
         AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4 = AV131ContagemResultado_StatusDmn4;
         AV237ExtraWWContagemResultadoEmGarantiaDS_60_Contagemresultado_servico4 = AV137ContagemResultado_Servico4;
         AV238ExtraWWContagemResultadoEmGarantiaDS_61_Contagemresultado_sistemacod4 = AV139ContagemResultado_SistemaCod4;
         AV239ExtraWWContagemResultadoEmGarantiaDS_62_Contagemresultado_contratadacod4 = AV140ContagemResultado_ContratadaCod4;
         AV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4 = AV147ContagemResultado_Descricao4;
         AV241ExtraWWContagemResultadoEmGarantiaDS_64_Contagemresultado_owner4 = AV148ContagemResultado_Owner4;
         AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 = AV150DynamicFiltersEnabled5;
         AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5 = AV151DynamicFiltersSelector5;
         AV244ExtraWWContagemResultadoEmGarantiaDS_67_Dynamicfiltersoperator5 = AV152DynamicFiltersOperator5;
         AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 = AV156ContagemResultado_OsFsOsFm5;
         AV246ExtraWWContagemResultadoEmGarantiaDS_69_Contagemresultado_datadmn5 = AV157ContagemResultado_DataDmn5;
         AV247ExtraWWContagemResultadoEmGarantiaDS_70_Contagemresultado_datadmn_to5 = AV158ContagemResultado_DataDmn_To5;
         AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5 = AV153ContagemResultado_DataCnt5;
         AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5 = AV154ContagemResultado_DataCnt_To5;
         AV250ExtraWWContagemResultadoEmGarantiaDS_73_Contagemresultado_dataprevista5 = AV159ContagemResultado_DataPrevista5;
         AV251ExtraWWContagemResultadoEmGarantiaDS_74_Contagemresultado_dataprevista_to5 = AV160ContagemResultado_DataPrevista_To5;
         AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5 = AV155ContagemResultado_StatusDmn5;
         AV253ExtraWWContagemResultadoEmGarantiaDS_76_Contagemresultado_servico5 = AV161ContagemResultado_Servico5;
         AV254ExtraWWContagemResultadoEmGarantiaDS_77_Contagemresultado_sistemacod5 = AV163ContagemResultado_SistemaCod5;
         AV255ExtraWWContagemResultadoEmGarantiaDS_78_Contagemresultado_contratadacod5 = AV164ContagemResultado_ContratadaCod5;
         AV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5 = AV171ContagemResultado_Descricao5;
         AV257ExtraWWContagemResultadoEmGarantiaDS_80_Contagemresultado_owner5 = AV172ContagemResultado_Owner5;
         AV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm = AV12TFContagemResultado_DemandaFM;
         AV259ExtraWWContagemResultadoEmGarantiaDS_82_Tfcontagemresultado_demandafm_sel = AV13TFContagemResultado_DemandaFM_Sel;
         AV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda = AV14TFContagemResultado_Demanda;
         AV261ExtraWWContagemResultadoEmGarantiaDS_84_Tfcontagemresultado_demanda_sel = AV15TFContagemResultado_Demanda_Sel;
         AV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao = AV16TFContagemResultado_Descricao;
         AV263ExtraWWContagemResultadoEmGarantiaDS_86_Tfcontagemresultado_descricao_sel = AV17TFContagemResultado_Descricao_Sel;
         AV264ExtraWWContagemResultadoEmGarantiaDS_87_Tfcontagemresultado_datadmn = AV18TFContagemResultado_DataDmn;
         AV265ExtraWWContagemResultadoEmGarantiaDS_88_Tfcontagemresultado_datadmn_to = AV19TFContagemResultado_DataDmn_To;
         AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt = AV20TFContagemResultado_DataUltCnt;
         AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to = AV21TFContagemResultado_DataUltCnt_To;
         AV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla = AV22TFContagemResultado_ContratadaSigla;
         AV269ExtraWWContagemResultadoEmGarantiaDS_92_Tfcontagemresultado_contratadasigla_sel = AV23TFContagemResultado_ContratadaSigla_Sel;
         AV270ExtraWWContagemResultadoEmGarantiaDS_93_Tfcontagemresultado_statusdmn_sels = AV27TFContagemResultado_StatusDmn_Sels;
         AV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla = AV29TFContagemResultado_ServicoSigla;
         AV272ExtraWWContagemResultadoEmGarantiaDS_95_Tfcontagemresultado_servicosigla_sel = AV30TFContagemResultado_ServicoSigla_Sel;
         pr_default.dynParam(4, new Object[]{ new Object[]{
                                              A484ContagemResultado_StatusDmn ,
                                              AV270ExtraWWContagemResultadoEmGarantiaDS_93_Tfcontagemresultado_statusdmn_sels ,
                                              AV9WWPContext.gxTpr_Contratada_codigo ,
                                              AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1 ,
                                              AV180ExtraWWContagemResultadoEmGarantiaDS_3_Dynamicfiltersoperator1 ,
                                              AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 ,
                                              AV182ExtraWWContagemResultadoEmGarantiaDS_5_Contagemresultado_datadmn1 ,
                                              AV183ExtraWWContagemResultadoEmGarantiaDS_6_Contagemresultado_datadmn_to1 ,
                                              AV186ExtraWWContagemResultadoEmGarantiaDS_9_Contagemresultado_dataprevista1 ,
                                              AV187ExtraWWContagemResultadoEmGarantiaDS_10_Contagemresultado_dataprevista_to1 ,
                                              AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1 ,
                                              AV189ExtraWWContagemResultadoEmGarantiaDS_12_Contagemresultado_servico1 ,
                                              AV190ExtraWWContagemResultadoEmGarantiaDS_13_Contagemresultado_sistemacod1 ,
                                              AV191ExtraWWContagemResultadoEmGarantiaDS_14_Contagemresultado_contratadacod1 ,
                                              AV178ExtraWWContagemResultadoEmGarantiaDS_1_Contratada_areatrabalhocod ,
                                              AV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1 ,
                                              AV193ExtraWWContagemResultadoEmGarantiaDS_16_Contagemresultado_owner1 ,
                                              AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 ,
                                              AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2 ,
                                              AV196ExtraWWContagemResultadoEmGarantiaDS_19_Dynamicfiltersoperator2 ,
                                              AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 ,
                                              AV198ExtraWWContagemResultadoEmGarantiaDS_21_Contagemresultado_datadmn2 ,
                                              AV199ExtraWWContagemResultadoEmGarantiaDS_22_Contagemresultado_datadmn_to2 ,
                                              AV202ExtraWWContagemResultadoEmGarantiaDS_25_Contagemresultado_dataprevista2 ,
                                              AV203ExtraWWContagemResultadoEmGarantiaDS_26_Contagemresultado_dataprevista_to2 ,
                                              AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2 ,
                                              AV205ExtraWWContagemResultadoEmGarantiaDS_28_Contagemresultado_servico2 ,
                                              AV206ExtraWWContagemResultadoEmGarantiaDS_29_Contagemresultado_sistemacod2 ,
                                              AV207ExtraWWContagemResultadoEmGarantiaDS_30_Contagemresultado_contratadacod2 ,
                                              AV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2 ,
                                              AV209ExtraWWContagemResultadoEmGarantiaDS_32_Contagemresultado_owner2 ,
                                              AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 ,
                                              AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3 ,
                                              AV212ExtraWWContagemResultadoEmGarantiaDS_35_Dynamicfiltersoperator3 ,
                                              AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 ,
                                              AV214ExtraWWContagemResultadoEmGarantiaDS_37_Contagemresultado_datadmn3 ,
                                              AV215ExtraWWContagemResultadoEmGarantiaDS_38_Contagemresultado_datadmn_to3 ,
                                              AV218ExtraWWContagemResultadoEmGarantiaDS_41_Contagemresultado_dataprevista3 ,
                                              AV219ExtraWWContagemResultadoEmGarantiaDS_42_Contagemresultado_dataprevista_to3 ,
                                              AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3 ,
                                              AV221ExtraWWContagemResultadoEmGarantiaDS_44_Contagemresultado_servico3 ,
                                              AV222ExtraWWContagemResultadoEmGarantiaDS_45_Contagemresultado_sistemacod3 ,
                                              AV223ExtraWWContagemResultadoEmGarantiaDS_46_Contagemresultado_contratadacod3 ,
                                              AV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3 ,
                                              AV225ExtraWWContagemResultadoEmGarantiaDS_48_Contagemresultado_owner3 ,
                                              AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 ,
                                              AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4 ,
                                              AV228ExtraWWContagemResultadoEmGarantiaDS_51_Dynamicfiltersoperator4 ,
                                              AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 ,
                                              AV230ExtraWWContagemResultadoEmGarantiaDS_53_Contagemresultado_datadmn4 ,
                                              AV231ExtraWWContagemResultadoEmGarantiaDS_54_Contagemresultado_datadmn_to4 ,
                                              AV234ExtraWWContagemResultadoEmGarantiaDS_57_Contagemresultado_dataprevista4 ,
                                              AV235ExtraWWContagemResultadoEmGarantiaDS_58_Contagemresultado_dataprevista_to4 ,
                                              AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4 ,
                                              AV237ExtraWWContagemResultadoEmGarantiaDS_60_Contagemresultado_servico4 ,
                                              AV238ExtraWWContagemResultadoEmGarantiaDS_61_Contagemresultado_sistemacod4 ,
                                              AV239ExtraWWContagemResultadoEmGarantiaDS_62_Contagemresultado_contratadacod4 ,
                                              AV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4 ,
                                              AV241ExtraWWContagemResultadoEmGarantiaDS_64_Contagemresultado_owner4 ,
                                              AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 ,
                                              AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5 ,
                                              AV244ExtraWWContagemResultadoEmGarantiaDS_67_Dynamicfiltersoperator5 ,
                                              AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 ,
                                              AV246ExtraWWContagemResultadoEmGarantiaDS_69_Contagemresultado_datadmn5 ,
                                              AV247ExtraWWContagemResultadoEmGarantiaDS_70_Contagemresultado_datadmn_to5 ,
                                              AV250ExtraWWContagemResultadoEmGarantiaDS_73_Contagemresultado_dataprevista5 ,
                                              AV251ExtraWWContagemResultadoEmGarantiaDS_74_Contagemresultado_dataprevista_to5 ,
                                              AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5 ,
                                              AV253ExtraWWContagemResultadoEmGarantiaDS_76_Contagemresultado_servico5 ,
                                              AV254ExtraWWContagemResultadoEmGarantiaDS_77_Contagemresultado_sistemacod5 ,
                                              AV255ExtraWWContagemResultadoEmGarantiaDS_78_Contagemresultado_contratadacod5 ,
                                              AV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5 ,
                                              AV257ExtraWWContagemResultadoEmGarantiaDS_80_Contagemresultado_owner5 ,
                                              AV259ExtraWWContagemResultadoEmGarantiaDS_82_Tfcontagemresultado_demandafm_sel ,
                                              AV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm ,
                                              AV261ExtraWWContagemResultadoEmGarantiaDS_84_Tfcontagemresultado_demanda_sel ,
                                              AV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda ,
                                              AV263ExtraWWContagemResultadoEmGarantiaDS_86_Tfcontagemresultado_descricao_sel ,
                                              AV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao ,
                                              AV264ExtraWWContagemResultadoEmGarantiaDS_87_Tfcontagemresultado_datadmn ,
                                              AV265ExtraWWContagemResultadoEmGarantiaDS_88_Tfcontagemresultado_datadmn_to ,
                                              AV269ExtraWWContagemResultadoEmGarantiaDS_92_Tfcontagemresultado_contratadasigla_sel ,
                                              AV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla ,
                                              AV270ExtraWWContagemResultadoEmGarantiaDS_93_Tfcontagemresultado_statusdmn_sels.Count ,
                                              AV272ExtraWWContagemResultadoEmGarantiaDS_95_Tfcontagemresultado_servicosigla_sel ,
                                              AV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla ,
                                              A490ContagemResultado_ContratadaCod ,
                                              A457ContagemResultado_Demanda ,
                                              A493ContagemResultado_DemandaFM ,
                                              A471ContagemResultado_DataDmn ,
                                              A1351ContagemResultado_DataPrevista ,
                                              A601ContagemResultado_Servico ,
                                              A489ContagemResultado_SistemaCod ,
                                              A494ContagemResultado_Descricao ,
                                              A508ContagemResultado_Owner ,
                                              A803ContagemResultado_ContratadaSigla ,
                                              A801ContagemResultado_ServicoSigla ,
                                              AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1 ,
                                              A566ContagemResultado_DataUltCnt ,
                                              AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1 ,
                                              AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2 ,
                                              AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2 ,
                                              AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3 ,
                                              AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3 ,
                                              AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4 ,
                                              AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4 ,
                                              AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5 ,
                                              AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5 ,
                                              AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt ,
                                              AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1), "%", "");
         lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1), "%", "");
         lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1), "%", "");
         lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1), "%", "");
         lV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1), "%", "");
         lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2), "%", "");
         lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2), "%", "");
         lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2), "%", "");
         lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2), "%", "");
         lV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2), "%", "");
         lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3), "%", "");
         lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3), "%", "");
         lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3), "%", "");
         lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3), "%", "");
         lV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3), "%", "");
         lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4), "%", "");
         lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4), "%", "");
         lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4), "%", "");
         lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4), "%", "");
         lV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4 = StringUtil.Concat( StringUtil.RTrim( AV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4), "%", "");
         lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5), "%", "");
         lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5), "%", "");
         lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5), "%", "");
         lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5), "%", "");
         lV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5 = StringUtil.Concat( StringUtil.RTrim( AV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5), "%", "");
         lV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm = StringUtil.Concat( StringUtil.RTrim( AV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm), "%", "");
         lV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda = StringUtil.Concat( StringUtil.RTrim( AV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda), "%", "");
         lV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao = StringUtil.Concat( StringUtil.RTrim( AV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao), "%", "");
         lV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla = StringUtil.PadR( StringUtil.RTrim( AV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla), 15, "%");
         lV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla = StringUtil.PadR( StringUtil.RTrim( AV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla), 15, "%");
         /* Using cursor P00U111 */
         pr_default.execute(4, new Object[] {AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1, AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1, AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1, AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1, AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2, AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2, AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2, AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2, AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2, AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2, AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3, AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3, AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3, AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3, AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3, AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3, AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4, AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4, AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4, AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4, AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4, AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4, AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5, AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5, AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5, AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5, AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5, AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5, AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt, AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt, AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to, AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to, AV9WWPContext.gxTpr_Areatrabalho_codigo, AV9WWPContext.gxTpr_Contratada_codigo, AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1, AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1, lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1, lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1, lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1, lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1, AV182ExtraWWContagemResultadoEmGarantiaDS_5_Contagemresultado_datadmn1, AV183ExtraWWContagemResultadoEmGarantiaDS_6_Contagemresultado_datadmn_to1, AV186ExtraWWContagemResultadoEmGarantiaDS_9_Contagemresultado_dataprevista1, AV187ExtraWWContagemResultadoEmGarantiaDS_10_Contagemresultado_dataprevista_to1, AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1, AV189ExtraWWContagemResultadoEmGarantiaDS_12_Contagemresultado_servico1, AV190ExtraWWContagemResultadoEmGarantiaDS_13_Contagemresultado_sistemacod1, AV191ExtraWWContagemResultadoEmGarantiaDS_14_Contagemresultado_contratadacod1, lV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1, AV193ExtraWWContagemResultadoEmGarantiaDS_16_Contagemresultado_owner1, AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2, AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2, lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2, lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2, lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2, lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2, AV198ExtraWWContagemResultadoEmGarantiaDS_21_Contagemresultado_datadmn2, AV199ExtraWWContagemResultadoEmGarantiaDS_22_Contagemresultado_datadmn_to2, AV202ExtraWWContagemResultadoEmGarantiaDS_25_Contagemresultado_dataprevista2, AV203ExtraWWContagemResultadoEmGarantiaDS_26_Contagemresultado_dataprevista_to2, AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2, AV205ExtraWWContagemResultadoEmGarantiaDS_28_Contagemresultado_servico2, AV206ExtraWWContagemResultadoEmGarantiaDS_29_Contagemresultado_sistemacod2, AV207ExtraWWContagemResultadoEmGarantiaDS_30_Contagemresultado_contratadacod2, lV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2, AV209ExtraWWContagemResultadoEmGarantiaDS_32_Contagemresultado_owner2, AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3, AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3, lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3, lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3, lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3, lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3, AV214ExtraWWContagemResultadoEmGarantiaDS_37_Contagemresultado_datadmn3, AV215ExtraWWContagemResultadoEmGarantiaDS_38_Contagemresultado_datadmn_to3, AV218ExtraWWContagemResultadoEmGarantiaDS_41_Contagemresultado_dataprevista3, AV219ExtraWWContagemResultadoEmGarantiaDS_42_Contagemresultado_dataprevista_to3, AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3, AV221ExtraWWContagemResultadoEmGarantiaDS_44_Contagemresultado_servico3, AV222ExtraWWContagemResultadoEmGarantiaDS_45_Contagemresultado_sistemacod3, AV223ExtraWWContagemResultadoEmGarantiaDS_46_Contagemresultado_contratadacod3, lV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3, AV225ExtraWWContagemResultadoEmGarantiaDS_48_Contagemresultado_owner3, AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4, AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4, lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4, lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4, lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4, lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4, AV230ExtraWWContagemResultadoEmGarantiaDS_53_Contagemresultado_datadmn4, AV231ExtraWWContagemResultadoEmGarantiaDS_54_Contagemresultado_datadmn_to4, AV234ExtraWWContagemResultadoEmGarantiaDS_57_Contagemresultado_dataprevista4, AV235ExtraWWContagemResultadoEmGarantiaDS_58_Contagemresultado_dataprevista_to4, AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4, AV237ExtraWWContagemResultadoEmGarantiaDS_60_Contagemresultado_servico4, AV238ExtraWWContagemResultadoEmGarantiaDS_61_Contagemresultado_sistemacod4, AV239ExtraWWContagemResultadoEmGarantiaDS_62_Contagemresultado_contratadacod4, lV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4, AV241ExtraWWContagemResultadoEmGarantiaDS_64_Contagemresultado_owner4, AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5, AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5, lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5, lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5, lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5, lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5, AV246ExtraWWContagemResultadoEmGarantiaDS_69_Contagemresultado_datadmn5, AV247ExtraWWContagemResultadoEmGarantiaDS_70_Contagemresultado_datadmn_to5, AV250ExtraWWContagemResultadoEmGarantiaDS_73_Contagemresultado_dataprevista5, AV251ExtraWWContagemResultadoEmGarantiaDS_74_Contagemresultado_dataprevista_to5, AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5, AV253ExtraWWContagemResultadoEmGarantiaDS_76_Contagemresultado_servico5, AV254ExtraWWContagemResultadoEmGarantiaDS_77_Contagemresultado_sistemacod5, AV255ExtraWWContagemResultadoEmGarantiaDS_78_Contagemresultado_contratadacod5,
         lV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5, AV257ExtraWWContagemResultadoEmGarantiaDS_80_Contagemresultado_owner5, lV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm, AV259ExtraWWContagemResultadoEmGarantiaDS_82_Tfcontagemresultado_demandafm_sel, lV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda, AV261ExtraWWContagemResultadoEmGarantiaDS_84_Tfcontagemresultado_demanda_sel, lV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao, AV263ExtraWWContagemResultadoEmGarantiaDS_86_Tfcontagemresultado_descricao_sel, AV264ExtraWWContagemResultadoEmGarantiaDS_87_Tfcontagemresultado_datadmn, AV265ExtraWWContagemResultadoEmGarantiaDS_88_Tfcontagemresultado_datadmn_to, lV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla, AV269ExtraWWContagemResultadoEmGarantiaDS_92_Tfcontagemresultado_contratadasigla_sel, lV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla, AV272ExtraWWContagemResultadoEmGarantiaDS_95_Tfcontagemresultado_servicosigla_sel});
         while ( (pr_default.getStatus(4) != 101) )
         {
            BRKU110 = false;
            A1553ContagemResultado_CntSrvCod = P00U111_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00U111_n1553ContagemResultado_CntSrvCod[0];
            A456ContagemResultado_Codigo = P00U111_A456ContagemResultado_Codigo[0];
            A52Contratada_AreaTrabalhoCod = P00U111_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00U111_n52Contratada_AreaTrabalhoCod[0];
            A801ContagemResultado_ServicoSigla = P00U111_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00U111_n801ContagemResultado_ServicoSigla[0];
            A803ContagemResultado_ContratadaSigla = P00U111_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P00U111_n803ContagemResultado_ContratadaSigla[0];
            A508ContagemResultado_Owner = P00U111_A508ContagemResultado_Owner[0];
            A494ContagemResultado_Descricao = P00U111_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = P00U111_n494ContagemResultado_Descricao[0];
            A489ContagemResultado_SistemaCod = P00U111_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00U111_n489ContagemResultado_SistemaCod[0];
            A601ContagemResultado_Servico = P00U111_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00U111_n601ContagemResultado_Servico[0];
            A484ContagemResultado_StatusDmn = P00U111_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00U111_n484ContagemResultado_StatusDmn[0];
            A1351ContagemResultado_DataPrevista = P00U111_A1351ContagemResultado_DataPrevista[0];
            n1351ContagemResultado_DataPrevista = P00U111_n1351ContagemResultado_DataPrevista[0];
            A471ContagemResultado_DataDmn = P00U111_A471ContagemResultado_DataDmn[0];
            A493ContagemResultado_DemandaFM = P00U111_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00U111_n493ContagemResultado_DemandaFM[0];
            A457ContagemResultado_Demanda = P00U111_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00U111_n457ContagemResultado_Demanda[0];
            A490ContagemResultado_ContratadaCod = P00U111_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00U111_n490ContagemResultado_ContratadaCod[0];
            A566ContagemResultado_DataUltCnt = P00U111_A566ContagemResultado_DataUltCnt[0];
            n566ContagemResultado_DataUltCnt = P00U111_n566ContagemResultado_DataUltCnt[0];
            A601ContagemResultado_Servico = P00U111_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00U111_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = P00U111_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00U111_n801ContagemResultado_ServicoSigla[0];
            A566ContagemResultado_DataUltCnt = P00U111_A566ContagemResultado_DataUltCnt[0];
            n566ContagemResultado_DataUltCnt = P00U111_n566ContagemResultado_DataUltCnt[0];
            A52Contratada_AreaTrabalhoCod = P00U111_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00U111_n52Contratada_AreaTrabalhoCod[0];
            A803ContagemResultado_ContratadaSigla = P00U111_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P00U111_n803ContagemResultado_ContratadaSigla[0];
            AV47count = 0;
            while ( (pr_default.getStatus(4) != 101) && ( StringUtil.StrCmp(P00U111_A801ContagemResultado_ServicoSigla[0], A801ContagemResultado_ServicoSigla) == 0 ) )
            {
               BRKU110 = false;
               A1553ContagemResultado_CntSrvCod = P00U111_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P00U111_n1553ContagemResultado_CntSrvCod[0];
               A456ContagemResultado_Codigo = P00U111_A456ContagemResultado_Codigo[0];
               A601ContagemResultado_Servico = P00U111_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = P00U111_n601ContagemResultado_Servico[0];
               A601ContagemResultado_Servico = P00U111_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = P00U111_n601ContagemResultado_Servico[0];
               AV47count = (long)(AV47count+1);
               BRKU110 = true;
               pr_default.readNext(4);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A801ContagemResultado_ServicoSigla)) )
            {
               AV39Option = A801ContagemResultado_ServicoSigla;
               AV40Options.Add(AV39Option, 0);
               AV45OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV47count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV40Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKU110 )
            {
               BRKU110 = true;
               pr_default.readNext(4);
            }
         }
         pr_default.close(4);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV40Options = new GxSimpleCollection();
         AV43OptionsDesc = new GxSimpleCollection();
         AV45OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV48Session = context.GetSession();
         AV50GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV51GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12TFContagemResultado_DemandaFM = "";
         AV13TFContagemResultado_DemandaFM_Sel = "";
         AV14TFContagemResultado_Demanda = "";
         AV15TFContagemResultado_Demanda_Sel = "";
         AV16TFContagemResultado_Descricao = "";
         AV17TFContagemResultado_Descricao_Sel = "";
         AV18TFContagemResultado_DataDmn = DateTime.MinValue;
         AV19TFContagemResultado_DataDmn_To = DateTime.MinValue;
         AV20TFContagemResultado_DataUltCnt = DateTime.MinValue;
         AV21TFContagemResultado_DataUltCnt_To = DateTime.MinValue;
         AV22TFContagemResultado_ContratadaSigla = "";
         AV23TFContagemResultado_ContratadaSigla_Sel = "";
         AV26TFContagemResultado_StatusDmn_SelsJson = "";
         AV27TFContagemResultado_StatusDmn_Sels = new GxSimpleCollection();
         AV29TFContagemResultado_ServicoSigla = "";
         AV30TFContagemResultado_ServicoSigla_Sel = "";
         AV52GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV55DynamicFiltersSelector1 = "";
         AV60ContagemResultado_OsFsOsFm1 = "";
         AV61ContagemResultado_DataDmn1 = DateTime.MinValue;
         AV62ContagemResultado_DataDmn_To1 = DateTime.MinValue;
         AV57ContagemResultado_DataCnt1 = DateTime.MinValue;
         AV58ContagemResultado_DataCnt_To1 = DateTime.MinValue;
         AV63ContagemResultado_DataPrevista1 = (DateTime)(DateTime.MinValue);
         AV64ContagemResultado_DataPrevista_To1 = (DateTime)(DateTime.MinValue);
         AV59ContagemResultado_StatusDmn1 = "";
         AV75ContagemResultado_Descricao1 = "";
         AV79DynamicFiltersSelector2 = "";
         AV84ContagemResultado_OsFsOsFm2 = "";
         AV85ContagemResultado_DataDmn2 = DateTime.MinValue;
         AV86ContagemResultado_DataDmn_To2 = DateTime.MinValue;
         AV81ContagemResultado_DataCnt2 = DateTime.MinValue;
         AV82ContagemResultado_DataCnt_To2 = DateTime.MinValue;
         AV87ContagemResultado_DataPrevista2 = (DateTime)(DateTime.MinValue);
         AV88ContagemResultado_DataPrevista_To2 = (DateTime)(DateTime.MinValue);
         AV83ContagemResultado_StatusDmn2 = "";
         AV99ContagemResultado_Descricao2 = "";
         AV103DynamicFiltersSelector3 = "";
         AV108ContagemResultado_OsFsOsFm3 = "";
         AV109ContagemResultado_DataDmn3 = DateTime.MinValue;
         AV110ContagemResultado_DataDmn_To3 = DateTime.MinValue;
         AV105ContagemResultado_DataCnt3 = DateTime.MinValue;
         AV106ContagemResultado_DataCnt_To3 = DateTime.MinValue;
         AV111ContagemResultado_DataPrevista3 = (DateTime)(DateTime.MinValue);
         AV112ContagemResultado_DataPrevista_To3 = (DateTime)(DateTime.MinValue);
         AV107ContagemResultado_StatusDmn3 = "";
         AV123ContagemResultado_Descricao3 = "";
         AV127DynamicFiltersSelector4 = "";
         AV132ContagemResultado_OsFsOsFm4 = "";
         AV133ContagemResultado_DataDmn4 = DateTime.MinValue;
         AV134ContagemResultado_DataDmn_To4 = DateTime.MinValue;
         AV129ContagemResultado_DataCnt4 = DateTime.MinValue;
         AV130ContagemResultado_DataCnt_To4 = DateTime.MinValue;
         AV135ContagemResultado_DataPrevista4 = (DateTime)(DateTime.MinValue);
         AV136ContagemResultado_DataPrevista_To4 = (DateTime)(DateTime.MinValue);
         AV131ContagemResultado_StatusDmn4 = "";
         AV147ContagemResultado_Descricao4 = "";
         AV151DynamicFiltersSelector5 = "";
         AV156ContagemResultado_OsFsOsFm5 = "";
         AV157ContagemResultado_DataDmn5 = DateTime.MinValue;
         AV158ContagemResultado_DataDmn_To5 = DateTime.MinValue;
         AV153ContagemResultado_DataCnt5 = DateTime.MinValue;
         AV154ContagemResultado_DataCnt_To5 = DateTime.MinValue;
         AV159ContagemResultado_DataPrevista5 = (DateTime)(DateTime.MinValue);
         AV160ContagemResultado_DataPrevista_To5 = (DateTime)(DateTime.MinValue);
         AV155ContagemResultado_StatusDmn5 = "";
         AV171ContagemResultado_Descricao5 = "";
         AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1 = "";
         AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 = "";
         AV182ExtraWWContagemResultadoEmGarantiaDS_5_Contagemresultado_datadmn1 = DateTime.MinValue;
         AV183ExtraWWContagemResultadoEmGarantiaDS_6_Contagemresultado_datadmn_to1 = DateTime.MinValue;
         AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1 = DateTime.MinValue;
         AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1 = DateTime.MinValue;
         AV186ExtraWWContagemResultadoEmGarantiaDS_9_Contagemresultado_dataprevista1 = (DateTime)(DateTime.MinValue);
         AV187ExtraWWContagemResultadoEmGarantiaDS_10_Contagemresultado_dataprevista_to1 = (DateTime)(DateTime.MinValue);
         AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1 = "";
         AV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1 = "";
         AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2 = "";
         AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 = "";
         AV198ExtraWWContagemResultadoEmGarantiaDS_21_Contagemresultado_datadmn2 = DateTime.MinValue;
         AV199ExtraWWContagemResultadoEmGarantiaDS_22_Contagemresultado_datadmn_to2 = DateTime.MinValue;
         AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2 = DateTime.MinValue;
         AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2 = DateTime.MinValue;
         AV202ExtraWWContagemResultadoEmGarantiaDS_25_Contagemresultado_dataprevista2 = (DateTime)(DateTime.MinValue);
         AV203ExtraWWContagemResultadoEmGarantiaDS_26_Contagemresultado_dataprevista_to2 = (DateTime)(DateTime.MinValue);
         AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2 = "";
         AV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2 = "";
         AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3 = "";
         AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 = "";
         AV214ExtraWWContagemResultadoEmGarantiaDS_37_Contagemresultado_datadmn3 = DateTime.MinValue;
         AV215ExtraWWContagemResultadoEmGarantiaDS_38_Contagemresultado_datadmn_to3 = DateTime.MinValue;
         AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3 = DateTime.MinValue;
         AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3 = DateTime.MinValue;
         AV218ExtraWWContagemResultadoEmGarantiaDS_41_Contagemresultado_dataprevista3 = (DateTime)(DateTime.MinValue);
         AV219ExtraWWContagemResultadoEmGarantiaDS_42_Contagemresultado_dataprevista_to3 = (DateTime)(DateTime.MinValue);
         AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3 = "";
         AV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3 = "";
         AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4 = "";
         AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 = "";
         AV230ExtraWWContagemResultadoEmGarantiaDS_53_Contagemresultado_datadmn4 = DateTime.MinValue;
         AV231ExtraWWContagemResultadoEmGarantiaDS_54_Contagemresultado_datadmn_to4 = DateTime.MinValue;
         AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4 = DateTime.MinValue;
         AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4 = DateTime.MinValue;
         AV234ExtraWWContagemResultadoEmGarantiaDS_57_Contagemresultado_dataprevista4 = (DateTime)(DateTime.MinValue);
         AV235ExtraWWContagemResultadoEmGarantiaDS_58_Contagemresultado_dataprevista_to4 = (DateTime)(DateTime.MinValue);
         AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4 = "";
         AV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4 = "";
         AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5 = "";
         AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 = "";
         AV246ExtraWWContagemResultadoEmGarantiaDS_69_Contagemresultado_datadmn5 = DateTime.MinValue;
         AV247ExtraWWContagemResultadoEmGarantiaDS_70_Contagemresultado_datadmn_to5 = DateTime.MinValue;
         AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5 = DateTime.MinValue;
         AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5 = DateTime.MinValue;
         AV250ExtraWWContagemResultadoEmGarantiaDS_73_Contagemresultado_dataprevista5 = (DateTime)(DateTime.MinValue);
         AV251ExtraWWContagemResultadoEmGarantiaDS_74_Contagemresultado_dataprevista_to5 = (DateTime)(DateTime.MinValue);
         AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5 = "";
         AV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5 = "";
         AV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm = "";
         AV259ExtraWWContagemResultadoEmGarantiaDS_82_Tfcontagemresultado_demandafm_sel = "";
         AV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda = "";
         AV261ExtraWWContagemResultadoEmGarantiaDS_84_Tfcontagemresultado_demanda_sel = "";
         AV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao = "";
         AV263ExtraWWContagemResultadoEmGarantiaDS_86_Tfcontagemresultado_descricao_sel = "";
         AV264ExtraWWContagemResultadoEmGarantiaDS_87_Tfcontagemresultado_datadmn = DateTime.MinValue;
         AV265ExtraWWContagemResultadoEmGarantiaDS_88_Tfcontagemresultado_datadmn_to = DateTime.MinValue;
         AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt = DateTime.MinValue;
         AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to = DateTime.MinValue;
         AV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla = "";
         AV269ExtraWWContagemResultadoEmGarantiaDS_92_Tfcontagemresultado_contratadasigla_sel = "";
         AV270ExtraWWContagemResultadoEmGarantiaDS_93_Tfcontagemresultado_statusdmn_sels = new GxSimpleCollection();
         AV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla = "";
         AV272ExtraWWContagemResultadoEmGarantiaDS_95_Tfcontagemresultado_servicosigla_sel = "";
         scmdbuf = "";
         lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 = "";
         lV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1 = "";
         lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 = "";
         lV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2 = "";
         lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 = "";
         lV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3 = "";
         lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 = "";
         lV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4 = "";
         lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 = "";
         lV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5 = "";
         lV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm = "";
         lV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda = "";
         lV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao = "";
         lV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla = "";
         lV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla = "";
         A484ContagemResultado_StatusDmn = "";
         A457ContagemResultado_Demanda = "";
         A493ContagemResultado_DemandaFM = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A1351ContagemResultado_DataPrevista = (DateTime)(DateTime.MinValue);
         A494ContagemResultado_Descricao = "";
         A803ContagemResultado_ContratadaSigla = "";
         A801ContagemResultado_ServicoSigla = "";
         A566ContagemResultado_DataUltCnt = DateTime.MinValue;
         P00U13_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00U13_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00U13_A456ContagemResultado_Codigo = new int[1] ;
         P00U13_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00U13_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00U13_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00U13_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00U13_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P00U13_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P00U13_A803ContagemResultado_ContratadaSigla = new String[] {""} ;
         P00U13_n803ContagemResultado_ContratadaSigla = new bool[] {false} ;
         P00U13_A508ContagemResultado_Owner = new int[1] ;
         P00U13_A494ContagemResultado_Descricao = new String[] {""} ;
         P00U13_n494ContagemResultado_Descricao = new bool[] {false} ;
         P00U13_A489ContagemResultado_SistemaCod = new int[1] ;
         P00U13_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00U13_A601ContagemResultado_Servico = new int[1] ;
         P00U13_n601ContagemResultado_Servico = new bool[] {false} ;
         P00U13_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00U13_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00U13_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P00U13_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         P00U13_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00U13_A457ContagemResultado_Demanda = new String[] {""} ;
         P00U13_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00U13_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00U13_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00U13_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         P00U13_n566ContagemResultado_DataUltCnt = new bool[] {false} ;
         AV39Option = "";
         P00U15_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00U15_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00U15_A456ContagemResultado_Codigo = new int[1] ;
         P00U15_A457ContagemResultado_Demanda = new String[] {""} ;
         P00U15_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00U15_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P00U15_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P00U15_A803ContagemResultado_ContratadaSigla = new String[] {""} ;
         P00U15_n803ContagemResultado_ContratadaSigla = new bool[] {false} ;
         P00U15_A508ContagemResultado_Owner = new int[1] ;
         P00U15_A494ContagemResultado_Descricao = new String[] {""} ;
         P00U15_n494ContagemResultado_Descricao = new bool[] {false} ;
         P00U15_A489ContagemResultado_SistemaCod = new int[1] ;
         P00U15_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00U15_A601ContagemResultado_Servico = new int[1] ;
         P00U15_n601ContagemResultado_Servico = new bool[] {false} ;
         P00U15_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00U15_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00U15_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P00U15_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         P00U15_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00U15_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00U15_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00U15_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00U15_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00U15_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00U15_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00U15_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         P00U15_n566ContagemResultado_DataUltCnt = new bool[] {false} ;
         AV42OptionDesc = "";
         P00U17_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00U17_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00U17_A456ContagemResultado_Codigo = new int[1] ;
         P00U17_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00U17_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00U17_A494ContagemResultado_Descricao = new String[] {""} ;
         P00U17_n494ContagemResultado_Descricao = new bool[] {false} ;
         P00U17_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P00U17_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P00U17_A803ContagemResultado_ContratadaSigla = new String[] {""} ;
         P00U17_n803ContagemResultado_ContratadaSigla = new bool[] {false} ;
         P00U17_A508ContagemResultado_Owner = new int[1] ;
         P00U17_A489ContagemResultado_SistemaCod = new int[1] ;
         P00U17_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00U17_A601ContagemResultado_Servico = new int[1] ;
         P00U17_n601ContagemResultado_Servico = new bool[] {false} ;
         P00U17_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00U17_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00U17_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P00U17_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         P00U17_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00U17_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00U17_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00U17_A457ContagemResultado_Demanda = new String[] {""} ;
         P00U17_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00U17_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00U17_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00U17_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         P00U17_n566ContagemResultado_DataUltCnt = new bool[] {false} ;
         P00U19_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00U19_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00U19_A456ContagemResultado_Codigo = new int[1] ;
         P00U19_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00U19_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00U19_A803ContagemResultado_ContratadaSigla = new String[] {""} ;
         P00U19_n803ContagemResultado_ContratadaSigla = new bool[] {false} ;
         P00U19_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P00U19_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P00U19_A508ContagemResultado_Owner = new int[1] ;
         P00U19_A494ContagemResultado_Descricao = new String[] {""} ;
         P00U19_n494ContagemResultado_Descricao = new bool[] {false} ;
         P00U19_A489ContagemResultado_SistemaCod = new int[1] ;
         P00U19_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00U19_A601ContagemResultado_Servico = new int[1] ;
         P00U19_n601ContagemResultado_Servico = new bool[] {false} ;
         P00U19_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00U19_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00U19_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P00U19_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         P00U19_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00U19_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00U19_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00U19_A457ContagemResultado_Demanda = new String[] {""} ;
         P00U19_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00U19_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00U19_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00U19_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         P00U19_n566ContagemResultado_DataUltCnt = new bool[] {false} ;
         P00U111_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00U111_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00U111_A456ContagemResultado_Codigo = new int[1] ;
         P00U111_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00U111_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00U111_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P00U111_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P00U111_A803ContagemResultado_ContratadaSigla = new String[] {""} ;
         P00U111_n803ContagemResultado_ContratadaSigla = new bool[] {false} ;
         P00U111_A508ContagemResultado_Owner = new int[1] ;
         P00U111_A494ContagemResultado_Descricao = new String[] {""} ;
         P00U111_n494ContagemResultado_Descricao = new bool[] {false} ;
         P00U111_A489ContagemResultado_SistemaCod = new int[1] ;
         P00U111_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00U111_A601ContagemResultado_Servico = new int[1] ;
         P00U111_n601ContagemResultado_Servico = new bool[] {false} ;
         P00U111_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00U111_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00U111_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P00U111_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         P00U111_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00U111_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00U111_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00U111_A457ContagemResultado_Demanda = new String[] {""} ;
         P00U111_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00U111_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00U111_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00U111_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         P00U111_n566ContagemResultado_DataUltCnt = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getextrawwcontagemresultadoemgarantiafilterdata__default(),
            new Object[][] {
                new Object[] {
               P00U13_A1553ContagemResultado_CntSrvCod, P00U13_n1553ContagemResultado_CntSrvCod, P00U13_A456ContagemResultado_Codigo, P00U13_A52Contratada_AreaTrabalhoCod, P00U13_n52Contratada_AreaTrabalhoCod, P00U13_A493ContagemResultado_DemandaFM, P00U13_n493ContagemResultado_DemandaFM, P00U13_A801ContagemResultado_ServicoSigla, P00U13_n801ContagemResultado_ServicoSigla, P00U13_A803ContagemResultado_ContratadaSigla,
               P00U13_n803ContagemResultado_ContratadaSigla, P00U13_A508ContagemResultado_Owner, P00U13_A494ContagemResultado_Descricao, P00U13_n494ContagemResultado_Descricao, P00U13_A489ContagemResultado_SistemaCod, P00U13_n489ContagemResultado_SistemaCod, P00U13_A601ContagemResultado_Servico, P00U13_n601ContagemResultado_Servico, P00U13_A484ContagemResultado_StatusDmn, P00U13_n484ContagemResultado_StatusDmn,
               P00U13_A1351ContagemResultado_DataPrevista, P00U13_n1351ContagemResultado_DataPrevista, P00U13_A471ContagemResultado_DataDmn, P00U13_A457ContagemResultado_Demanda, P00U13_n457ContagemResultado_Demanda, P00U13_A490ContagemResultado_ContratadaCod, P00U13_n490ContagemResultado_ContratadaCod, P00U13_A566ContagemResultado_DataUltCnt, P00U13_n566ContagemResultado_DataUltCnt
               }
               , new Object[] {
               P00U15_A1553ContagemResultado_CntSrvCod, P00U15_n1553ContagemResultado_CntSrvCod, P00U15_A456ContagemResultado_Codigo, P00U15_A457ContagemResultado_Demanda, P00U15_n457ContagemResultado_Demanda, P00U15_A801ContagemResultado_ServicoSigla, P00U15_n801ContagemResultado_ServicoSigla, P00U15_A803ContagemResultado_ContratadaSigla, P00U15_n803ContagemResultado_ContratadaSigla, P00U15_A508ContagemResultado_Owner,
               P00U15_A494ContagemResultado_Descricao, P00U15_n494ContagemResultado_Descricao, P00U15_A489ContagemResultado_SistemaCod, P00U15_n489ContagemResultado_SistemaCod, P00U15_A601ContagemResultado_Servico, P00U15_n601ContagemResultado_Servico, P00U15_A484ContagemResultado_StatusDmn, P00U15_n484ContagemResultado_StatusDmn, P00U15_A1351ContagemResultado_DataPrevista, P00U15_n1351ContagemResultado_DataPrevista,
               P00U15_A471ContagemResultado_DataDmn, P00U15_A493ContagemResultado_DemandaFM, P00U15_n493ContagemResultado_DemandaFM, P00U15_A490ContagemResultado_ContratadaCod, P00U15_n490ContagemResultado_ContratadaCod, P00U15_A52Contratada_AreaTrabalhoCod, P00U15_n52Contratada_AreaTrabalhoCod, P00U15_A566ContagemResultado_DataUltCnt, P00U15_n566ContagemResultado_DataUltCnt
               }
               , new Object[] {
               P00U17_A1553ContagemResultado_CntSrvCod, P00U17_n1553ContagemResultado_CntSrvCod, P00U17_A456ContagemResultado_Codigo, P00U17_A52Contratada_AreaTrabalhoCod, P00U17_n52Contratada_AreaTrabalhoCod, P00U17_A494ContagemResultado_Descricao, P00U17_n494ContagemResultado_Descricao, P00U17_A801ContagemResultado_ServicoSigla, P00U17_n801ContagemResultado_ServicoSigla, P00U17_A803ContagemResultado_ContratadaSigla,
               P00U17_n803ContagemResultado_ContratadaSigla, P00U17_A508ContagemResultado_Owner, P00U17_A489ContagemResultado_SistemaCod, P00U17_n489ContagemResultado_SistemaCod, P00U17_A601ContagemResultado_Servico, P00U17_n601ContagemResultado_Servico, P00U17_A484ContagemResultado_StatusDmn, P00U17_n484ContagemResultado_StatusDmn, P00U17_A1351ContagemResultado_DataPrevista, P00U17_n1351ContagemResultado_DataPrevista,
               P00U17_A471ContagemResultado_DataDmn, P00U17_A493ContagemResultado_DemandaFM, P00U17_n493ContagemResultado_DemandaFM, P00U17_A457ContagemResultado_Demanda, P00U17_n457ContagemResultado_Demanda, P00U17_A490ContagemResultado_ContratadaCod, P00U17_n490ContagemResultado_ContratadaCod, P00U17_A566ContagemResultado_DataUltCnt, P00U17_n566ContagemResultado_DataUltCnt
               }
               , new Object[] {
               P00U19_A1553ContagemResultado_CntSrvCod, P00U19_n1553ContagemResultado_CntSrvCod, P00U19_A456ContagemResultado_Codigo, P00U19_A52Contratada_AreaTrabalhoCod, P00U19_n52Contratada_AreaTrabalhoCod, P00U19_A803ContagemResultado_ContratadaSigla, P00U19_n803ContagemResultado_ContratadaSigla, P00U19_A801ContagemResultado_ServicoSigla, P00U19_n801ContagemResultado_ServicoSigla, P00U19_A508ContagemResultado_Owner,
               P00U19_A494ContagemResultado_Descricao, P00U19_n494ContagemResultado_Descricao, P00U19_A489ContagemResultado_SistemaCod, P00U19_n489ContagemResultado_SistemaCod, P00U19_A601ContagemResultado_Servico, P00U19_n601ContagemResultado_Servico, P00U19_A484ContagemResultado_StatusDmn, P00U19_n484ContagemResultado_StatusDmn, P00U19_A1351ContagemResultado_DataPrevista, P00U19_n1351ContagemResultado_DataPrevista,
               P00U19_A471ContagemResultado_DataDmn, P00U19_A493ContagemResultado_DemandaFM, P00U19_n493ContagemResultado_DemandaFM, P00U19_A457ContagemResultado_Demanda, P00U19_n457ContagemResultado_Demanda, P00U19_A490ContagemResultado_ContratadaCod, P00U19_n490ContagemResultado_ContratadaCod, P00U19_A566ContagemResultado_DataUltCnt, P00U19_n566ContagemResultado_DataUltCnt
               }
               , new Object[] {
               P00U111_A1553ContagemResultado_CntSrvCod, P00U111_n1553ContagemResultado_CntSrvCod, P00U111_A456ContagemResultado_Codigo, P00U111_A52Contratada_AreaTrabalhoCod, P00U111_n52Contratada_AreaTrabalhoCod, P00U111_A801ContagemResultado_ServicoSigla, P00U111_n801ContagemResultado_ServicoSigla, P00U111_A803ContagemResultado_ContratadaSigla, P00U111_n803ContagemResultado_ContratadaSigla, P00U111_A508ContagemResultado_Owner,
               P00U111_A494ContagemResultado_Descricao, P00U111_n494ContagemResultado_Descricao, P00U111_A489ContagemResultado_SistemaCod, P00U111_n489ContagemResultado_SistemaCod, P00U111_A601ContagemResultado_Servico, P00U111_n601ContagemResultado_Servico, P00U111_A484ContagemResultado_StatusDmn, P00U111_n484ContagemResultado_StatusDmn, P00U111_A1351ContagemResultado_DataPrevista, P00U111_n1351ContagemResultado_DataPrevista,
               P00U111_A471ContagemResultado_DataDmn, P00U111_A493ContagemResultado_DemandaFM, P00U111_n493ContagemResultado_DemandaFM, P00U111_A457ContagemResultado_Demanda, P00U111_n457ContagemResultado_Demanda, P00U111_A490ContagemResultado_ContratadaCod, P00U111_n490ContagemResultado_ContratadaCod, P00U111_A566ContagemResultado_DataUltCnt, P00U111_n566ContagemResultado_DataUltCnt
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV56DynamicFiltersOperator1 ;
      private short AV80DynamicFiltersOperator2 ;
      private short AV104DynamicFiltersOperator3 ;
      private short AV128DynamicFiltersOperator4 ;
      private short AV152DynamicFiltersOperator5 ;
      private short AV180ExtraWWContagemResultadoEmGarantiaDS_3_Dynamicfiltersoperator1 ;
      private short AV196ExtraWWContagemResultadoEmGarantiaDS_19_Dynamicfiltersoperator2 ;
      private short AV212ExtraWWContagemResultadoEmGarantiaDS_35_Dynamicfiltersoperator3 ;
      private short AV228ExtraWWContagemResultadoEmGarantiaDS_51_Dynamicfiltersoperator4 ;
      private short AV244ExtraWWContagemResultadoEmGarantiaDS_67_Dynamicfiltersoperator5 ;
      private int AV176GXV1 ;
      private int AV53Contratada_AreaTrabalhoCod ;
      private int AV65ContagemResultado_Servico1 ;
      private int AV67ContagemResultado_SistemaCod1 ;
      private int AV68ContagemResultado_ContratadaCod1 ;
      private int AV76ContagemResultado_Owner1 ;
      private int AV89ContagemResultado_Servico2 ;
      private int AV91ContagemResultado_SistemaCod2 ;
      private int AV92ContagemResultado_ContratadaCod2 ;
      private int AV100ContagemResultado_Owner2 ;
      private int AV113ContagemResultado_Servico3 ;
      private int AV115ContagemResultado_SistemaCod3 ;
      private int AV116ContagemResultado_ContratadaCod3 ;
      private int AV124ContagemResultado_Owner3 ;
      private int AV137ContagemResultado_Servico4 ;
      private int AV139ContagemResultado_SistemaCod4 ;
      private int AV140ContagemResultado_ContratadaCod4 ;
      private int AV148ContagemResultado_Owner4 ;
      private int AV161ContagemResultado_Servico5 ;
      private int AV163ContagemResultado_SistemaCod5 ;
      private int AV164ContagemResultado_ContratadaCod5 ;
      private int AV172ContagemResultado_Owner5 ;
      private int AV178ExtraWWContagemResultadoEmGarantiaDS_1_Contratada_areatrabalhocod ;
      private int AV189ExtraWWContagemResultadoEmGarantiaDS_12_Contagemresultado_servico1 ;
      private int AV190ExtraWWContagemResultadoEmGarantiaDS_13_Contagemresultado_sistemacod1 ;
      private int AV191ExtraWWContagemResultadoEmGarantiaDS_14_Contagemresultado_contratadacod1 ;
      private int AV193ExtraWWContagemResultadoEmGarantiaDS_16_Contagemresultado_owner1 ;
      private int AV205ExtraWWContagemResultadoEmGarantiaDS_28_Contagemresultado_servico2 ;
      private int AV206ExtraWWContagemResultadoEmGarantiaDS_29_Contagemresultado_sistemacod2 ;
      private int AV207ExtraWWContagemResultadoEmGarantiaDS_30_Contagemresultado_contratadacod2 ;
      private int AV209ExtraWWContagemResultadoEmGarantiaDS_32_Contagemresultado_owner2 ;
      private int AV221ExtraWWContagemResultadoEmGarantiaDS_44_Contagemresultado_servico3 ;
      private int AV222ExtraWWContagemResultadoEmGarantiaDS_45_Contagemresultado_sistemacod3 ;
      private int AV223ExtraWWContagemResultadoEmGarantiaDS_46_Contagemresultado_contratadacod3 ;
      private int AV225ExtraWWContagemResultadoEmGarantiaDS_48_Contagemresultado_owner3 ;
      private int AV237ExtraWWContagemResultadoEmGarantiaDS_60_Contagemresultado_servico4 ;
      private int AV238ExtraWWContagemResultadoEmGarantiaDS_61_Contagemresultado_sistemacod4 ;
      private int AV239ExtraWWContagemResultadoEmGarantiaDS_62_Contagemresultado_contratadacod4 ;
      private int AV241ExtraWWContagemResultadoEmGarantiaDS_64_Contagemresultado_owner4 ;
      private int AV253ExtraWWContagemResultadoEmGarantiaDS_76_Contagemresultado_servico5 ;
      private int AV254ExtraWWContagemResultadoEmGarantiaDS_77_Contagemresultado_sistemacod5 ;
      private int AV255ExtraWWContagemResultadoEmGarantiaDS_78_Contagemresultado_contratadacod5 ;
      private int AV257ExtraWWContagemResultadoEmGarantiaDS_80_Contagemresultado_owner5 ;
      private int AV9WWPContext_gxTpr_Contratada_codigo ;
      private int AV270ExtraWWContagemResultadoEmGarantiaDS_93_Tfcontagemresultado_statusdmn_sels_Count ;
      private int AV9WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A601ContagemResultado_Servico ;
      private int A489ContagemResultado_SistemaCod ;
      private int A508ContagemResultado_Owner ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A456ContagemResultado_Codigo ;
      private long AV47count ;
      private String AV22TFContagemResultado_ContratadaSigla ;
      private String AV23TFContagemResultado_ContratadaSigla_Sel ;
      private String AV29TFContagemResultado_ServicoSigla ;
      private String AV30TFContagemResultado_ServicoSigla_Sel ;
      private String AV59ContagemResultado_StatusDmn1 ;
      private String AV83ContagemResultado_StatusDmn2 ;
      private String AV107ContagemResultado_StatusDmn3 ;
      private String AV131ContagemResultado_StatusDmn4 ;
      private String AV155ContagemResultado_StatusDmn5 ;
      private String AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1 ;
      private String AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2 ;
      private String AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3 ;
      private String AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4 ;
      private String AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5 ;
      private String AV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla ;
      private String AV269ExtraWWContagemResultadoEmGarantiaDS_92_Tfcontagemresultado_contratadasigla_sel ;
      private String AV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla ;
      private String AV272ExtraWWContagemResultadoEmGarantiaDS_95_Tfcontagemresultado_servicosigla_sel ;
      private String scmdbuf ;
      private String lV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla ;
      private String lV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla ;
      private String A484ContagemResultado_StatusDmn ;
      private String A803ContagemResultado_ContratadaSigla ;
      private String A801ContagemResultado_ServicoSigla ;
      private DateTime AV63ContagemResultado_DataPrevista1 ;
      private DateTime AV64ContagemResultado_DataPrevista_To1 ;
      private DateTime AV87ContagemResultado_DataPrevista2 ;
      private DateTime AV88ContagemResultado_DataPrevista_To2 ;
      private DateTime AV111ContagemResultado_DataPrevista3 ;
      private DateTime AV112ContagemResultado_DataPrevista_To3 ;
      private DateTime AV135ContagemResultado_DataPrevista4 ;
      private DateTime AV136ContagemResultado_DataPrevista_To4 ;
      private DateTime AV159ContagemResultado_DataPrevista5 ;
      private DateTime AV160ContagemResultado_DataPrevista_To5 ;
      private DateTime AV186ExtraWWContagemResultadoEmGarantiaDS_9_Contagemresultado_dataprevista1 ;
      private DateTime AV187ExtraWWContagemResultadoEmGarantiaDS_10_Contagemresultado_dataprevista_to1 ;
      private DateTime AV202ExtraWWContagemResultadoEmGarantiaDS_25_Contagemresultado_dataprevista2 ;
      private DateTime AV203ExtraWWContagemResultadoEmGarantiaDS_26_Contagemresultado_dataprevista_to2 ;
      private DateTime AV218ExtraWWContagemResultadoEmGarantiaDS_41_Contagemresultado_dataprevista3 ;
      private DateTime AV219ExtraWWContagemResultadoEmGarantiaDS_42_Contagemresultado_dataprevista_to3 ;
      private DateTime AV234ExtraWWContagemResultadoEmGarantiaDS_57_Contagemresultado_dataprevista4 ;
      private DateTime AV235ExtraWWContagemResultadoEmGarantiaDS_58_Contagemresultado_dataprevista_to4 ;
      private DateTime AV250ExtraWWContagemResultadoEmGarantiaDS_73_Contagemresultado_dataprevista5 ;
      private DateTime AV251ExtraWWContagemResultadoEmGarantiaDS_74_Contagemresultado_dataprevista_to5 ;
      private DateTime A1351ContagemResultado_DataPrevista ;
      private DateTime AV18TFContagemResultado_DataDmn ;
      private DateTime AV19TFContagemResultado_DataDmn_To ;
      private DateTime AV20TFContagemResultado_DataUltCnt ;
      private DateTime AV21TFContagemResultado_DataUltCnt_To ;
      private DateTime AV61ContagemResultado_DataDmn1 ;
      private DateTime AV62ContagemResultado_DataDmn_To1 ;
      private DateTime AV57ContagemResultado_DataCnt1 ;
      private DateTime AV58ContagemResultado_DataCnt_To1 ;
      private DateTime AV85ContagemResultado_DataDmn2 ;
      private DateTime AV86ContagemResultado_DataDmn_To2 ;
      private DateTime AV81ContagemResultado_DataCnt2 ;
      private DateTime AV82ContagemResultado_DataCnt_To2 ;
      private DateTime AV109ContagemResultado_DataDmn3 ;
      private DateTime AV110ContagemResultado_DataDmn_To3 ;
      private DateTime AV105ContagemResultado_DataCnt3 ;
      private DateTime AV106ContagemResultado_DataCnt_To3 ;
      private DateTime AV133ContagemResultado_DataDmn4 ;
      private DateTime AV134ContagemResultado_DataDmn_To4 ;
      private DateTime AV129ContagemResultado_DataCnt4 ;
      private DateTime AV130ContagemResultado_DataCnt_To4 ;
      private DateTime AV157ContagemResultado_DataDmn5 ;
      private DateTime AV158ContagemResultado_DataDmn_To5 ;
      private DateTime AV153ContagemResultado_DataCnt5 ;
      private DateTime AV154ContagemResultado_DataCnt_To5 ;
      private DateTime AV182ExtraWWContagemResultadoEmGarantiaDS_5_Contagemresultado_datadmn1 ;
      private DateTime AV183ExtraWWContagemResultadoEmGarantiaDS_6_Contagemresultado_datadmn_to1 ;
      private DateTime AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1 ;
      private DateTime AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1 ;
      private DateTime AV198ExtraWWContagemResultadoEmGarantiaDS_21_Contagemresultado_datadmn2 ;
      private DateTime AV199ExtraWWContagemResultadoEmGarantiaDS_22_Contagemresultado_datadmn_to2 ;
      private DateTime AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2 ;
      private DateTime AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2 ;
      private DateTime AV214ExtraWWContagemResultadoEmGarantiaDS_37_Contagemresultado_datadmn3 ;
      private DateTime AV215ExtraWWContagemResultadoEmGarantiaDS_38_Contagemresultado_datadmn_to3 ;
      private DateTime AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3 ;
      private DateTime AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3 ;
      private DateTime AV230ExtraWWContagemResultadoEmGarantiaDS_53_Contagemresultado_datadmn4 ;
      private DateTime AV231ExtraWWContagemResultadoEmGarantiaDS_54_Contagemresultado_datadmn_to4 ;
      private DateTime AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4 ;
      private DateTime AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4 ;
      private DateTime AV246ExtraWWContagemResultadoEmGarantiaDS_69_Contagemresultado_datadmn5 ;
      private DateTime AV247ExtraWWContagemResultadoEmGarantiaDS_70_Contagemresultado_datadmn_to5 ;
      private DateTime AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5 ;
      private DateTime AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5 ;
      private DateTime AV264ExtraWWContagemResultadoEmGarantiaDS_87_Tfcontagemresultado_datadmn ;
      private DateTime AV265ExtraWWContagemResultadoEmGarantiaDS_88_Tfcontagemresultado_datadmn_to ;
      private DateTime AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt ;
      private DateTime AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime A566ContagemResultado_DataUltCnt ;
      private bool returnInSub ;
      private bool AV78DynamicFiltersEnabled2 ;
      private bool AV102DynamicFiltersEnabled3 ;
      private bool AV126DynamicFiltersEnabled4 ;
      private bool AV150DynamicFiltersEnabled5 ;
      private bool AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 ;
      private bool AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 ;
      private bool AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 ;
      private bool AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 ;
      private bool BRKU12 ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n801ContagemResultado_ServicoSigla ;
      private bool n803ContagemResultado_ContratadaSigla ;
      private bool n494ContagemResultado_Descricao ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n1351ContagemResultado_DataPrevista ;
      private bool n457ContagemResultado_Demanda ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n566ContagemResultado_DataUltCnt ;
      private bool BRKU14 ;
      private bool BRKU16 ;
      private bool BRKU18 ;
      private bool BRKU110 ;
      private String AV46OptionIndexesJson ;
      private String AV41OptionsJson ;
      private String AV44OptionsDescJson ;
      private String AV26TFContagemResultado_StatusDmn_SelsJson ;
      private String AV37DDOName ;
      private String AV35SearchTxt ;
      private String AV36SearchTxtTo ;
      private String AV12TFContagemResultado_DemandaFM ;
      private String AV13TFContagemResultado_DemandaFM_Sel ;
      private String AV14TFContagemResultado_Demanda ;
      private String AV15TFContagemResultado_Demanda_Sel ;
      private String AV16TFContagemResultado_Descricao ;
      private String AV17TFContagemResultado_Descricao_Sel ;
      private String AV55DynamicFiltersSelector1 ;
      private String AV60ContagemResultado_OsFsOsFm1 ;
      private String AV75ContagemResultado_Descricao1 ;
      private String AV79DynamicFiltersSelector2 ;
      private String AV84ContagemResultado_OsFsOsFm2 ;
      private String AV99ContagemResultado_Descricao2 ;
      private String AV103DynamicFiltersSelector3 ;
      private String AV108ContagemResultado_OsFsOsFm3 ;
      private String AV123ContagemResultado_Descricao3 ;
      private String AV127DynamicFiltersSelector4 ;
      private String AV132ContagemResultado_OsFsOsFm4 ;
      private String AV147ContagemResultado_Descricao4 ;
      private String AV151DynamicFiltersSelector5 ;
      private String AV156ContagemResultado_OsFsOsFm5 ;
      private String AV171ContagemResultado_Descricao5 ;
      private String AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1 ;
      private String AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 ;
      private String AV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1 ;
      private String AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2 ;
      private String AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 ;
      private String AV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2 ;
      private String AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3 ;
      private String AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 ;
      private String AV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3 ;
      private String AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4 ;
      private String AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 ;
      private String AV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4 ;
      private String AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5 ;
      private String AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 ;
      private String AV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5 ;
      private String AV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm ;
      private String AV259ExtraWWContagemResultadoEmGarantiaDS_82_Tfcontagemresultado_demandafm_sel ;
      private String AV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda ;
      private String AV261ExtraWWContagemResultadoEmGarantiaDS_84_Tfcontagemresultado_demanda_sel ;
      private String AV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao ;
      private String AV263ExtraWWContagemResultadoEmGarantiaDS_86_Tfcontagemresultado_descricao_sel ;
      private String lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 ;
      private String lV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1 ;
      private String lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 ;
      private String lV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2 ;
      private String lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 ;
      private String lV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3 ;
      private String lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 ;
      private String lV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4 ;
      private String lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 ;
      private String lV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5 ;
      private String lV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm ;
      private String lV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda ;
      private String lV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao ;
      private String A457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String A494ContagemResultado_Descricao ;
      private String AV39Option ;
      private String AV42OptionDesc ;
      private IGxSession AV48Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00U13_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00U13_n1553ContagemResultado_CntSrvCod ;
      private int[] P00U13_A456ContagemResultado_Codigo ;
      private int[] P00U13_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00U13_n52Contratada_AreaTrabalhoCod ;
      private String[] P00U13_A493ContagemResultado_DemandaFM ;
      private bool[] P00U13_n493ContagemResultado_DemandaFM ;
      private String[] P00U13_A801ContagemResultado_ServicoSigla ;
      private bool[] P00U13_n801ContagemResultado_ServicoSigla ;
      private String[] P00U13_A803ContagemResultado_ContratadaSigla ;
      private bool[] P00U13_n803ContagemResultado_ContratadaSigla ;
      private int[] P00U13_A508ContagemResultado_Owner ;
      private String[] P00U13_A494ContagemResultado_Descricao ;
      private bool[] P00U13_n494ContagemResultado_Descricao ;
      private int[] P00U13_A489ContagemResultado_SistemaCod ;
      private bool[] P00U13_n489ContagemResultado_SistemaCod ;
      private int[] P00U13_A601ContagemResultado_Servico ;
      private bool[] P00U13_n601ContagemResultado_Servico ;
      private String[] P00U13_A484ContagemResultado_StatusDmn ;
      private bool[] P00U13_n484ContagemResultado_StatusDmn ;
      private DateTime[] P00U13_A1351ContagemResultado_DataPrevista ;
      private bool[] P00U13_n1351ContagemResultado_DataPrevista ;
      private DateTime[] P00U13_A471ContagemResultado_DataDmn ;
      private String[] P00U13_A457ContagemResultado_Demanda ;
      private bool[] P00U13_n457ContagemResultado_Demanda ;
      private int[] P00U13_A490ContagemResultado_ContratadaCod ;
      private bool[] P00U13_n490ContagemResultado_ContratadaCod ;
      private DateTime[] P00U13_A566ContagemResultado_DataUltCnt ;
      private bool[] P00U13_n566ContagemResultado_DataUltCnt ;
      private int[] P00U15_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00U15_n1553ContagemResultado_CntSrvCod ;
      private int[] P00U15_A456ContagemResultado_Codigo ;
      private String[] P00U15_A457ContagemResultado_Demanda ;
      private bool[] P00U15_n457ContagemResultado_Demanda ;
      private String[] P00U15_A801ContagemResultado_ServicoSigla ;
      private bool[] P00U15_n801ContagemResultado_ServicoSigla ;
      private String[] P00U15_A803ContagemResultado_ContratadaSigla ;
      private bool[] P00U15_n803ContagemResultado_ContratadaSigla ;
      private int[] P00U15_A508ContagemResultado_Owner ;
      private String[] P00U15_A494ContagemResultado_Descricao ;
      private bool[] P00U15_n494ContagemResultado_Descricao ;
      private int[] P00U15_A489ContagemResultado_SistemaCod ;
      private bool[] P00U15_n489ContagemResultado_SistemaCod ;
      private int[] P00U15_A601ContagemResultado_Servico ;
      private bool[] P00U15_n601ContagemResultado_Servico ;
      private String[] P00U15_A484ContagemResultado_StatusDmn ;
      private bool[] P00U15_n484ContagemResultado_StatusDmn ;
      private DateTime[] P00U15_A1351ContagemResultado_DataPrevista ;
      private bool[] P00U15_n1351ContagemResultado_DataPrevista ;
      private DateTime[] P00U15_A471ContagemResultado_DataDmn ;
      private String[] P00U15_A493ContagemResultado_DemandaFM ;
      private bool[] P00U15_n493ContagemResultado_DemandaFM ;
      private int[] P00U15_A490ContagemResultado_ContratadaCod ;
      private bool[] P00U15_n490ContagemResultado_ContratadaCod ;
      private int[] P00U15_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00U15_n52Contratada_AreaTrabalhoCod ;
      private DateTime[] P00U15_A566ContagemResultado_DataUltCnt ;
      private bool[] P00U15_n566ContagemResultado_DataUltCnt ;
      private int[] P00U17_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00U17_n1553ContagemResultado_CntSrvCod ;
      private int[] P00U17_A456ContagemResultado_Codigo ;
      private int[] P00U17_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00U17_n52Contratada_AreaTrabalhoCod ;
      private String[] P00U17_A494ContagemResultado_Descricao ;
      private bool[] P00U17_n494ContagemResultado_Descricao ;
      private String[] P00U17_A801ContagemResultado_ServicoSigla ;
      private bool[] P00U17_n801ContagemResultado_ServicoSigla ;
      private String[] P00U17_A803ContagemResultado_ContratadaSigla ;
      private bool[] P00U17_n803ContagemResultado_ContratadaSigla ;
      private int[] P00U17_A508ContagemResultado_Owner ;
      private int[] P00U17_A489ContagemResultado_SistemaCod ;
      private bool[] P00U17_n489ContagemResultado_SistemaCod ;
      private int[] P00U17_A601ContagemResultado_Servico ;
      private bool[] P00U17_n601ContagemResultado_Servico ;
      private String[] P00U17_A484ContagemResultado_StatusDmn ;
      private bool[] P00U17_n484ContagemResultado_StatusDmn ;
      private DateTime[] P00U17_A1351ContagemResultado_DataPrevista ;
      private bool[] P00U17_n1351ContagemResultado_DataPrevista ;
      private DateTime[] P00U17_A471ContagemResultado_DataDmn ;
      private String[] P00U17_A493ContagemResultado_DemandaFM ;
      private bool[] P00U17_n493ContagemResultado_DemandaFM ;
      private String[] P00U17_A457ContagemResultado_Demanda ;
      private bool[] P00U17_n457ContagemResultado_Demanda ;
      private int[] P00U17_A490ContagemResultado_ContratadaCod ;
      private bool[] P00U17_n490ContagemResultado_ContratadaCod ;
      private DateTime[] P00U17_A566ContagemResultado_DataUltCnt ;
      private bool[] P00U17_n566ContagemResultado_DataUltCnt ;
      private int[] P00U19_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00U19_n1553ContagemResultado_CntSrvCod ;
      private int[] P00U19_A456ContagemResultado_Codigo ;
      private int[] P00U19_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00U19_n52Contratada_AreaTrabalhoCod ;
      private String[] P00U19_A803ContagemResultado_ContratadaSigla ;
      private bool[] P00U19_n803ContagemResultado_ContratadaSigla ;
      private String[] P00U19_A801ContagemResultado_ServicoSigla ;
      private bool[] P00U19_n801ContagemResultado_ServicoSigla ;
      private int[] P00U19_A508ContagemResultado_Owner ;
      private String[] P00U19_A494ContagemResultado_Descricao ;
      private bool[] P00U19_n494ContagemResultado_Descricao ;
      private int[] P00U19_A489ContagemResultado_SistemaCod ;
      private bool[] P00U19_n489ContagemResultado_SistemaCod ;
      private int[] P00U19_A601ContagemResultado_Servico ;
      private bool[] P00U19_n601ContagemResultado_Servico ;
      private String[] P00U19_A484ContagemResultado_StatusDmn ;
      private bool[] P00U19_n484ContagemResultado_StatusDmn ;
      private DateTime[] P00U19_A1351ContagemResultado_DataPrevista ;
      private bool[] P00U19_n1351ContagemResultado_DataPrevista ;
      private DateTime[] P00U19_A471ContagemResultado_DataDmn ;
      private String[] P00U19_A493ContagemResultado_DemandaFM ;
      private bool[] P00U19_n493ContagemResultado_DemandaFM ;
      private String[] P00U19_A457ContagemResultado_Demanda ;
      private bool[] P00U19_n457ContagemResultado_Demanda ;
      private int[] P00U19_A490ContagemResultado_ContratadaCod ;
      private bool[] P00U19_n490ContagemResultado_ContratadaCod ;
      private DateTime[] P00U19_A566ContagemResultado_DataUltCnt ;
      private bool[] P00U19_n566ContagemResultado_DataUltCnt ;
      private int[] P00U111_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00U111_n1553ContagemResultado_CntSrvCod ;
      private int[] P00U111_A456ContagemResultado_Codigo ;
      private int[] P00U111_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00U111_n52Contratada_AreaTrabalhoCod ;
      private String[] P00U111_A801ContagemResultado_ServicoSigla ;
      private bool[] P00U111_n801ContagemResultado_ServicoSigla ;
      private String[] P00U111_A803ContagemResultado_ContratadaSigla ;
      private bool[] P00U111_n803ContagemResultado_ContratadaSigla ;
      private int[] P00U111_A508ContagemResultado_Owner ;
      private String[] P00U111_A494ContagemResultado_Descricao ;
      private bool[] P00U111_n494ContagemResultado_Descricao ;
      private int[] P00U111_A489ContagemResultado_SistemaCod ;
      private bool[] P00U111_n489ContagemResultado_SistemaCod ;
      private int[] P00U111_A601ContagemResultado_Servico ;
      private bool[] P00U111_n601ContagemResultado_Servico ;
      private String[] P00U111_A484ContagemResultado_StatusDmn ;
      private bool[] P00U111_n484ContagemResultado_StatusDmn ;
      private DateTime[] P00U111_A1351ContagemResultado_DataPrevista ;
      private bool[] P00U111_n1351ContagemResultado_DataPrevista ;
      private DateTime[] P00U111_A471ContagemResultado_DataDmn ;
      private String[] P00U111_A493ContagemResultado_DemandaFM ;
      private bool[] P00U111_n493ContagemResultado_DemandaFM ;
      private String[] P00U111_A457ContagemResultado_Demanda ;
      private bool[] P00U111_n457ContagemResultado_Demanda ;
      private int[] P00U111_A490ContagemResultado_ContratadaCod ;
      private bool[] P00U111_n490ContagemResultado_ContratadaCod ;
      private DateTime[] P00U111_A566ContagemResultado_DataUltCnt ;
      private bool[] P00U111_n566ContagemResultado_DataUltCnt ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV27TFContagemResultado_StatusDmn_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV270ExtraWWContagemResultadoEmGarantiaDS_93_Tfcontagemresultado_statusdmn_sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV40Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV43OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV45OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV50GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV51GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV52GridStateDynamicFilter ;
   }

   public class getextrawwcontagemresultadoemgarantiafilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00U13( IGxContext context ,
                                             String A484ContagemResultado_StatusDmn ,
                                             IGxCollection AV270ExtraWWContagemResultadoEmGarantiaDS_93_Tfcontagemresultado_statusdmn_sels ,
                                             int AV9WWPContext_gxTpr_Contratada_codigo ,
                                             String AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1 ,
                                             short AV180ExtraWWContagemResultadoEmGarantiaDS_3_Dynamicfiltersoperator1 ,
                                             String AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 ,
                                             DateTime AV182ExtraWWContagemResultadoEmGarantiaDS_5_Contagemresultado_datadmn1 ,
                                             DateTime AV183ExtraWWContagemResultadoEmGarantiaDS_6_Contagemresultado_datadmn_to1 ,
                                             DateTime AV186ExtraWWContagemResultadoEmGarantiaDS_9_Contagemresultado_dataprevista1 ,
                                             DateTime AV187ExtraWWContagemResultadoEmGarantiaDS_10_Contagemresultado_dataprevista_to1 ,
                                             String AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1 ,
                                             int AV189ExtraWWContagemResultadoEmGarantiaDS_12_Contagemresultado_servico1 ,
                                             int AV190ExtraWWContagemResultadoEmGarantiaDS_13_Contagemresultado_sistemacod1 ,
                                             int AV191ExtraWWContagemResultadoEmGarantiaDS_14_Contagemresultado_contratadacod1 ,
                                             int AV178ExtraWWContagemResultadoEmGarantiaDS_1_Contratada_areatrabalhocod ,
                                             String AV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1 ,
                                             int AV193ExtraWWContagemResultadoEmGarantiaDS_16_Contagemresultado_owner1 ,
                                             bool AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 ,
                                             String AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2 ,
                                             short AV196ExtraWWContagemResultadoEmGarantiaDS_19_Dynamicfiltersoperator2 ,
                                             String AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 ,
                                             DateTime AV198ExtraWWContagemResultadoEmGarantiaDS_21_Contagemresultado_datadmn2 ,
                                             DateTime AV199ExtraWWContagemResultadoEmGarantiaDS_22_Contagemresultado_datadmn_to2 ,
                                             DateTime AV202ExtraWWContagemResultadoEmGarantiaDS_25_Contagemresultado_dataprevista2 ,
                                             DateTime AV203ExtraWWContagemResultadoEmGarantiaDS_26_Contagemresultado_dataprevista_to2 ,
                                             String AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2 ,
                                             int AV205ExtraWWContagemResultadoEmGarantiaDS_28_Contagemresultado_servico2 ,
                                             int AV206ExtraWWContagemResultadoEmGarantiaDS_29_Contagemresultado_sistemacod2 ,
                                             int AV207ExtraWWContagemResultadoEmGarantiaDS_30_Contagemresultado_contratadacod2 ,
                                             String AV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2 ,
                                             int AV209ExtraWWContagemResultadoEmGarantiaDS_32_Contagemresultado_owner2 ,
                                             bool AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 ,
                                             String AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3 ,
                                             short AV212ExtraWWContagemResultadoEmGarantiaDS_35_Dynamicfiltersoperator3 ,
                                             String AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 ,
                                             DateTime AV214ExtraWWContagemResultadoEmGarantiaDS_37_Contagemresultado_datadmn3 ,
                                             DateTime AV215ExtraWWContagemResultadoEmGarantiaDS_38_Contagemresultado_datadmn_to3 ,
                                             DateTime AV218ExtraWWContagemResultadoEmGarantiaDS_41_Contagemresultado_dataprevista3 ,
                                             DateTime AV219ExtraWWContagemResultadoEmGarantiaDS_42_Contagemresultado_dataprevista_to3 ,
                                             String AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3 ,
                                             int AV221ExtraWWContagemResultadoEmGarantiaDS_44_Contagemresultado_servico3 ,
                                             int AV222ExtraWWContagemResultadoEmGarantiaDS_45_Contagemresultado_sistemacod3 ,
                                             int AV223ExtraWWContagemResultadoEmGarantiaDS_46_Contagemresultado_contratadacod3 ,
                                             String AV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3 ,
                                             int AV225ExtraWWContagemResultadoEmGarantiaDS_48_Contagemresultado_owner3 ,
                                             bool AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 ,
                                             String AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4 ,
                                             short AV228ExtraWWContagemResultadoEmGarantiaDS_51_Dynamicfiltersoperator4 ,
                                             String AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 ,
                                             DateTime AV230ExtraWWContagemResultadoEmGarantiaDS_53_Contagemresultado_datadmn4 ,
                                             DateTime AV231ExtraWWContagemResultadoEmGarantiaDS_54_Contagemresultado_datadmn_to4 ,
                                             DateTime AV234ExtraWWContagemResultadoEmGarantiaDS_57_Contagemresultado_dataprevista4 ,
                                             DateTime AV235ExtraWWContagemResultadoEmGarantiaDS_58_Contagemresultado_dataprevista_to4 ,
                                             String AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4 ,
                                             int AV237ExtraWWContagemResultadoEmGarantiaDS_60_Contagemresultado_servico4 ,
                                             int AV238ExtraWWContagemResultadoEmGarantiaDS_61_Contagemresultado_sistemacod4 ,
                                             int AV239ExtraWWContagemResultadoEmGarantiaDS_62_Contagemresultado_contratadacod4 ,
                                             String AV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4 ,
                                             int AV241ExtraWWContagemResultadoEmGarantiaDS_64_Contagemresultado_owner4 ,
                                             bool AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 ,
                                             String AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5 ,
                                             short AV244ExtraWWContagemResultadoEmGarantiaDS_67_Dynamicfiltersoperator5 ,
                                             String AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 ,
                                             DateTime AV246ExtraWWContagemResultadoEmGarantiaDS_69_Contagemresultado_datadmn5 ,
                                             DateTime AV247ExtraWWContagemResultadoEmGarantiaDS_70_Contagemresultado_datadmn_to5 ,
                                             DateTime AV250ExtraWWContagemResultadoEmGarantiaDS_73_Contagemresultado_dataprevista5 ,
                                             DateTime AV251ExtraWWContagemResultadoEmGarantiaDS_74_Contagemresultado_dataprevista_to5 ,
                                             String AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5 ,
                                             int AV253ExtraWWContagemResultadoEmGarantiaDS_76_Contagemresultado_servico5 ,
                                             int AV254ExtraWWContagemResultadoEmGarantiaDS_77_Contagemresultado_sistemacod5 ,
                                             int AV255ExtraWWContagemResultadoEmGarantiaDS_78_Contagemresultado_contratadacod5 ,
                                             String AV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5 ,
                                             int AV257ExtraWWContagemResultadoEmGarantiaDS_80_Contagemresultado_owner5 ,
                                             String AV259ExtraWWContagemResultadoEmGarantiaDS_82_Tfcontagemresultado_demandafm_sel ,
                                             String AV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm ,
                                             String AV261ExtraWWContagemResultadoEmGarantiaDS_84_Tfcontagemresultado_demanda_sel ,
                                             String AV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda ,
                                             String AV263ExtraWWContagemResultadoEmGarantiaDS_86_Tfcontagemresultado_descricao_sel ,
                                             String AV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao ,
                                             DateTime AV264ExtraWWContagemResultadoEmGarantiaDS_87_Tfcontagemresultado_datadmn ,
                                             DateTime AV265ExtraWWContagemResultadoEmGarantiaDS_88_Tfcontagemresultado_datadmn_to ,
                                             String AV269ExtraWWContagemResultadoEmGarantiaDS_92_Tfcontagemresultado_contratadasigla_sel ,
                                             String AV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla ,
                                             int AV270ExtraWWContagemResultadoEmGarantiaDS_93_Tfcontagemresultado_statusdmn_sels_Count ,
                                             String AV272ExtraWWContagemResultadoEmGarantiaDS_95_Tfcontagemresultado_servicosigla_sel ,
                                             String AV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             String A457ContagemResultado_Demanda ,
                                             String A493ContagemResultado_DemandaFM ,
                                             DateTime A471ContagemResultado_DataDmn ,
                                             DateTime A1351ContagemResultado_DataPrevista ,
                                             int A601ContagemResultado_Servico ,
                                             int A489ContagemResultado_SistemaCod ,
                                             String A494ContagemResultado_Descricao ,
                                             int A508ContagemResultado_Owner ,
                                             String A803ContagemResultado_ContratadaSigla ,
                                             String A801ContagemResultado_ServicoSigla ,
                                             DateTime AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1 ,
                                             DateTime A566ContagemResultado_DataUltCnt ,
                                             DateTime AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1 ,
                                             DateTime AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2 ,
                                             DateTime AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2 ,
                                             DateTime AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3 ,
                                             DateTime AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3 ,
                                             DateTime AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4 ,
                                             DateTime AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4 ,
                                             DateTime AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5 ,
                                             DateTime AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5 ,
                                             DateTime AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt ,
                                             DateTime AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [136] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_Codigo], T5.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_DemandaFM], T3.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T5.[Contratada_Sigla] AS ContagemResultado_ContratadaSigla, T1.[ContagemResultado_Owner], T1.[ContagemResultado_Descricao], T1.[ContagemResultado_SistemaCod], T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_DataPrevista], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt FROM (((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T4 ON T4.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Contratada] T5 WITH (NOLOCK) ON T5.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod])";
         scmdbuf = scmdbuf + " WHERE (Not ( @AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1))";
         scmdbuf = scmdbuf + " and (Not ( @AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1))";
         scmdbuf = scmdbuf + " and (Not ( @AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 = 1 and @AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2))";
         scmdbuf = scmdbuf + " and (Not ( @AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 = 1 and @AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2))";
         scmdbuf = scmdbuf + " and (Not ( @AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 = 1 and @AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3))";
         scmdbuf = scmdbuf + " and (Not ( @AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 = 1 and @AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3))";
         scmdbuf = scmdbuf + " and (Not ( @AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 = 1 and @AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4))";
         scmdbuf = scmdbuf + " and (Not ( @AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 = 1 and @AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4))";
         scmdbuf = scmdbuf + " and (Not ( @AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 = 1 and @AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5))";
         scmdbuf = scmdbuf + " and (Not ( @AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 = 1 and @AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5))";
         scmdbuf = scmdbuf + " and ((@AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt))";
         scmdbuf = scmdbuf + " and ((@AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to))";
         scmdbuf = scmdbuf + " and (T5.[Contratada_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( AV9WWPContext_gxTpr_Contratada_codigo > 0 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV9WWPCo_2Contratada_codigo)";
         }
         else
         {
            GXv_int1[43] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV180ExtraWWContagemResultadoEmGarantiaDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] = @AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int1[44] = 1;
            GXv_int1[45] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV180ExtraWWContagemResultadoEmGarantiaDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] like @lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int1[46] = 1;
            GXv_int1[47] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV180ExtraWWContagemResultadoEmGarantiaDS_3_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] like '%' + @lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int1[48] = 1;
            GXv_int1[49] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV182ExtraWWContagemResultadoEmGarantiaDS_5_Contagemresultado_datadmn1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV182ExtraWWContagemResultadoEmGarantiaDS_5_Contagemresultado_datadmn1)";
         }
         else
         {
            GXv_int1[50] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV183ExtraWWContagemResultadoEmGarantiaDS_6_Contagemresultado_datadmn_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV183ExtraWWContagemResultadoEmGarantiaDS_6_Contagemresultado_datadmn_to1)";
         }
         else
         {
            GXv_int1[51] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV186ExtraWWContagemResultadoEmGarantiaDS_9_Contagemresultado_dataprevista1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV186ExtraWWContagemResultadoEmGarantiaDS_9_Contagemresultado_dataprevista1)";
         }
         else
         {
            GXv_int1[52] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV187ExtraWWContagemResultadoEmGarantiaDS_10_Contagemresultado_dataprevista_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] <= @AV187ExtraWWContagemResultadoEmGarantiaDS_10_Contagemresultado_dataprevista_to1)";
         }
         else
         {
            GXv_int1[53] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1)) && ! ( StringUtil.StrCmp(AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1)";
         }
         else
         {
            GXv_int1[54] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L' or T1.[ContagemResultado_StatusDmn] = 'O'))";
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV189ExtraWWContagemResultadoEmGarantiaDS_12_Contagemresultado_servico1) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV189ExtraWWContagemResultadoEmGarantiaDS_12_Contagemresultado_servico1)";
         }
         else
         {
            GXv_int1[55] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV190ExtraWWContagemResultadoEmGarantiaDS_13_Contagemresultado_sistemacod1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV190ExtraWWContagemResultadoEmGarantiaDS_13_Contagemresultado_sistemacod1)";
         }
         else
         {
            GXv_int1[56] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV191ExtraWWContagemResultadoEmGarantiaDS_14_Contagemresultado_contratadacod1 > 0 ) && ( AV178ExtraWWContagemResultadoEmGarantiaDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV191ExtraWWContagemResultadoEmGarantiaDS_14_Contagemresultado_contratadacod1)";
         }
         else
         {
            GXv_int1[57] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1 + '%')";
         }
         else
         {
            GXv_int1[58] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV193ExtraWWContagemResultadoEmGarantiaDS_16_Contagemresultado_owner1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Owner] = @AV193ExtraWWContagemResultadoEmGarantiaDS_16_Contagemresultado_owner1)";
         }
         else
         {
            GXv_int1[59] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV196ExtraWWContagemResultadoEmGarantiaDS_19_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] = @AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int1[60] = 1;
            GXv_int1[61] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV196ExtraWWContagemResultadoEmGarantiaDS_19_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] like @lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int1[62] = 1;
            GXv_int1[63] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV196ExtraWWContagemResultadoEmGarantiaDS_19_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] like '%' + @lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int1[64] = 1;
            GXv_int1[65] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV198ExtraWWContagemResultadoEmGarantiaDS_21_Contagemresultado_datadmn2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV198ExtraWWContagemResultadoEmGarantiaDS_21_Contagemresultado_datadmn2)";
         }
         else
         {
            GXv_int1[66] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV199ExtraWWContagemResultadoEmGarantiaDS_22_Contagemresultado_datadmn_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV199ExtraWWContagemResultadoEmGarantiaDS_22_Contagemresultado_datadmn_to2)";
         }
         else
         {
            GXv_int1[67] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV202ExtraWWContagemResultadoEmGarantiaDS_25_Contagemresultado_dataprevista2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV202ExtraWWContagemResultadoEmGarantiaDS_25_Contagemresultado_dataprevista2)";
         }
         else
         {
            GXv_int1[68] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV203ExtraWWContagemResultadoEmGarantiaDS_26_Contagemresultado_dataprevista_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] <= @AV203ExtraWWContagemResultadoEmGarantiaDS_26_Contagemresultado_dataprevista_to2)";
         }
         else
         {
            GXv_int1[69] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2)) && ! ( StringUtil.StrCmp(AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2)";
         }
         else
         {
            GXv_int1[70] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L' or T1.[ContagemResultado_StatusDmn] = 'O'))";
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV205ExtraWWContagemResultadoEmGarantiaDS_28_Contagemresultado_servico2) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV205ExtraWWContagemResultadoEmGarantiaDS_28_Contagemresultado_servico2)";
         }
         else
         {
            GXv_int1[71] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV206ExtraWWContagemResultadoEmGarantiaDS_29_Contagemresultado_sistemacod2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV206ExtraWWContagemResultadoEmGarantiaDS_29_Contagemresultado_sistemacod2)";
         }
         else
         {
            GXv_int1[72] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV207ExtraWWContagemResultadoEmGarantiaDS_30_Contagemresultado_contratadacod2 > 0 ) && ( AV178ExtraWWContagemResultadoEmGarantiaDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV207ExtraWWContagemResultadoEmGarantiaDS_30_Contagemresultado_contratadacod2)";
         }
         else
         {
            GXv_int1[73] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2 + '%')";
         }
         else
         {
            GXv_int1[74] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV209ExtraWWContagemResultadoEmGarantiaDS_32_Contagemresultado_owner2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Owner] = @AV209ExtraWWContagemResultadoEmGarantiaDS_32_Contagemresultado_owner2)";
         }
         else
         {
            GXv_int1[75] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV212ExtraWWContagemResultadoEmGarantiaDS_35_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] = @AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int1[76] = 1;
            GXv_int1[77] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV212ExtraWWContagemResultadoEmGarantiaDS_35_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] like @lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int1[78] = 1;
            GXv_int1[79] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV212ExtraWWContagemResultadoEmGarantiaDS_35_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] like '%' + @lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int1[80] = 1;
            GXv_int1[81] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV214ExtraWWContagemResultadoEmGarantiaDS_37_Contagemresultado_datadmn3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV214ExtraWWContagemResultadoEmGarantiaDS_37_Contagemresultado_datadmn3)";
         }
         else
         {
            GXv_int1[82] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV215ExtraWWContagemResultadoEmGarantiaDS_38_Contagemresultado_datadmn_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV215ExtraWWContagemResultadoEmGarantiaDS_38_Contagemresultado_datadmn_to3)";
         }
         else
         {
            GXv_int1[83] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV218ExtraWWContagemResultadoEmGarantiaDS_41_Contagemresultado_dataprevista3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV218ExtraWWContagemResultadoEmGarantiaDS_41_Contagemresultado_dataprevista3)";
         }
         else
         {
            GXv_int1[84] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV219ExtraWWContagemResultadoEmGarantiaDS_42_Contagemresultado_dataprevista_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] <= @AV219ExtraWWContagemResultadoEmGarantiaDS_42_Contagemresultado_dataprevista_to3)";
         }
         else
         {
            GXv_int1[85] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3)) && ! ( StringUtil.StrCmp(AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3)";
         }
         else
         {
            GXv_int1[86] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L' or T1.[ContagemResultado_StatusDmn] = 'O'))";
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV221ExtraWWContagemResultadoEmGarantiaDS_44_Contagemresultado_servico3) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV221ExtraWWContagemResultadoEmGarantiaDS_44_Contagemresultado_servico3)";
         }
         else
         {
            GXv_int1[87] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV222ExtraWWContagemResultadoEmGarantiaDS_45_Contagemresultado_sistemacod3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV222ExtraWWContagemResultadoEmGarantiaDS_45_Contagemresultado_sistemacod3)";
         }
         else
         {
            GXv_int1[88] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV223ExtraWWContagemResultadoEmGarantiaDS_46_Contagemresultado_contratadacod3 > 0 ) && ( AV178ExtraWWContagemResultadoEmGarantiaDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV223ExtraWWContagemResultadoEmGarantiaDS_46_Contagemresultado_contratadacod3)";
         }
         else
         {
            GXv_int1[89] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3 + '%')";
         }
         else
         {
            GXv_int1[90] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV225ExtraWWContagemResultadoEmGarantiaDS_48_Contagemresultado_owner3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Owner] = @AV225ExtraWWContagemResultadoEmGarantiaDS_48_Contagemresultado_owner3)";
         }
         else
         {
            GXv_int1[91] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV228ExtraWWContagemResultadoEmGarantiaDS_51_Dynamicfiltersoperator4 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 or T1.[ContagemResultado_DemandaFM] = @AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4)";
         }
         else
         {
            GXv_int1[92] = 1;
            GXv_int1[93] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV228ExtraWWContagemResultadoEmGarantiaDS_51_Dynamicfiltersoperator4 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 or T1.[ContagemResultado_DemandaFM] like @lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4)";
         }
         else
         {
            GXv_int1[94] = 1;
            GXv_int1[95] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV228ExtraWWContagemResultadoEmGarantiaDS_51_Dynamicfiltersoperator4 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 or T1.[ContagemResultado_DemandaFM] like '%' + @lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4)";
         }
         else
         {
            GXv_int1[96] = 1;
            GXv_int1[97] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV230ExtraWWContagemResultadoEmGarantiaDS_53_Contagemresultado_datadmn4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV230ExtraWWContagemResultadoEmGarantiaDS_53_Contagemresultado_datadmn4)";
         }
         else
         {
            GXv_int1[98] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV231ExtraWWContagemResultadoEmGarantiaDS_54_Contagemresultado_datadmn_to4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV231ExtraWWContagemResultadoEmGarantiaDS_54_Contagemresultado_datadmn_to4)";
         }
         else
         {
            GXv_int1[99] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV234ExtraWWContagemResultadoEmGarantiaDS_57_Contagemresultado_dataprevista4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV234ExtraWWContagemResultadoEmGarantiaDS_57_Contagemresultado_dataprevista4)";
         }
         else
         {
            GXv_int1[100] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV235ExtraWWContagemResultadoEmGarantiaDS_58_Contagemresultado_dataprevista_to4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] <= @AV235ExtraWWContagemResultadoEmGarantiaDS_58_Contagemresultado_dataprevista_to4)";
         }
         else
         {
            GXv_int1[101] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4)) && ! ( StringUtil.StrCmp(AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4)";
         }
         else
         {
            GXv_int1[102] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L' or T1.[ContagemResultado_StatusDmn] = 'O'))";
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV237ExtraWWContagemResultadoEmGarantiaDS_60_Contagemresultado_servico4) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV237ExtraWWContagemResultadoEmGarantiaDS_60_Contagemresultado_servico4)";
         }
         else
         {
            GXv_int1[103] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV238ExtraWWContagemResultadoEmGarantiaDS_61_Contagemresultado_sistemacod4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV238ExtraWWContagemResultadoEmGarantiaDS_61_Contagemresultado_sistemacod4)";
         }
         else
         {
            GXv_int1[104] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV239ExtraWWContagemResultadoEmGarantiaDS_62_Contagemresultado_contratadacod4 > 0 ) && ( AV178ExtraWWContagemResultadoEmGarantiaDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV239ExtraWWContagemResultadoEmGarantiaDS_62_Contagemresultado_contratadacod4)";
         }
         else
         {
            GXv_int1[105] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4 + '%')";
         }
         else
         {
            GXv_int1[106] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV241ExtraWWContagemResultadoEmGarantiaDS_64_Contagemresultado_owner4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Owner] = @AV241ExtraWWContagemResultadoEmGarantiaDS_64_Contagemresultado_owner4)";
         }
         else
         {
            GXv_int1[107] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV244ExtraWWContagemResultadoEmGarantiaDS_67_Dynamicfiltersoperator5 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 or T1.[ContagemResultado_DemandaFM] = @AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5)";
         }
         else
         {
            GXv_int1[108] = 1;
            GXv_int1[109] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV244ExtraWWContagemResultadoEmGarantiaDS_67_Dynamicfiltersoperator5 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 or T1.[ContagemResultado_DemandaFM] like @lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5)";
         }
         else
         {
            GXv_int1[110] = 1;
            GXv_int1[111] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV244ExtraWWContagemResultadoEmGarantiaDS_67_Dynamicfiltersoperator5 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 or T1.[ContagemResultado_DemandaFM] like '%' + @lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5)";
         }
         else
         {
            GXv_int1[112] = 1;
            GXv_int1[113] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV246ExtraWWContagemResultadoEmGarantiaDS_69_Contagemresultado_datadmn5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV246ExtraWWContagemResultadoEmGarantiaDS_69_Contagemresultado_datadmn5)";
         }
         else
         {
            GXv_int1[114] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV247ExtraWWContagemResultadoEmGarantiaDS_70_Contagemresultado_datadmn_to5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV247ExtraWWContagemResultadoEmGarantiaDS_70_Contagemresultado_datadmn_to5)";
         }
         else
         {
            GXv_int1[115] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV250ExtraWWContagemResultadoEmGarantiaDS_73_Contagemresultado_dataprevista5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV250ExtraWWContagemResultadoEmGarantiaDS_73_Contagemresultado_dataprevista5)";
         }
         else
         {
            GXv_int1[116] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV251ExtraWWContagemResultadoEmGarantiaDS_74_Contagemresultado_dataprevista_to5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] <= @AV251ExtraWWContagemResultadoEmGarantiaDS_74_Contagemresultado_dataprevista_to5)";
         }
         else
         {
            GXv_int1[117] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5)) && ! ( StringUtil.StrCmp(AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5)";
         }
         else
         {
            GXv_int1[118] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L' or T1.[ContagemResultado_StatusDmn] = 'O'))";
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV253ExtraWWContagemResultadoEmGarantiaDS_76_Contagemresultado_servico5) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV253ExtraWWContagemResultadoEmGarantiaDS_76_Contagemresultado_servico5)";
         }
         else
         {
            GXv_int1[119] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV254ExtraWWContagemResultadoEmGarantiaDS_77_Contagemresultado_sistemacod5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV254ExtraWWContagemResultadoEmGarantiaDS_77_Contagemresultado_sistemacod5)";
         }
         else
         {
            GXv_int1[120] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV255ExtraWWContagemResultadoEmGarantiaDS_78_Contagemresultado_contratadacod5 > 0 ) && ( AV178ExtraWWContagemResultadoEmGarantiaDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV255ExtraWWContagemResultadoEmGarantiaDS_78_Contagemresultado_contratadacod5)";
         }
         else
         {
            GXv_int1[121] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5 + '%')";
         }
         else
         {
            GXv_int1[122] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV257ExtraWWContagemResultadoEmGarantiaDS_80_Contagemresultado_owner5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Owner] = @AV257ExtraWWContagemResultadoEmGarantiaDS_80_Contagemresultado_owner5)";
         }
         else
         {
            GXv_int1[123] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV259ExtraWWContagemResultadoEmGarantiaDS_82_Tfcontagemresultado_demandafm_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] like @lV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm)";
         }
         else
         {
            GXv_int1[124] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV259ExtraWWContagemResultadoEmGarantiaDS_82_Tfcontagemresultado_demandafm_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] = @AV259ExtraWWContagemResultadoEmGarantiaDS_82_Tfcontagemresultado_demandafm_sel)";
         }
         else
         {
            GXv_int1[125] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV261ExtraWWContagemResultadoEmGarantiaDS_84_Tfcontagemresultado_demanda_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda)";
         }
         else
         {
            GXv_int1[126] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV261ExtraWWContagemResultadoEmGarantiaDS_84_Tfcontagemresultado_demanda_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV261ExtraWWContagemResultadoEmGarantiaDS_84_Tfcontagemresultado_demanda_sel)";
         }
         else
         {
            GXv_int1[127] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV263ExtraWWContagemResultadoEmGarantiaDS_86_Tfcontagemresultado_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like @lV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao)";
         }
         else
         {
            GXv_int1[128] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV263ExtraWWContagemResultadoEmGarantiaDS_86_Tfcontagemresultado_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] = @AV263ExtraWWContagemResultadoEmGarantiaDS_86_Tfcontagemresultado_descricao_sel)";
         }
         else
         {
            GXv_int1[129] = 1;
         }
         if ( ! (DateTime.MinValue==AV264ExtraWWContagemResultadoEmGarantiaDS_87_Tfcontagemresultado_datadmn) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV264ExtraWWContagemResultadoEmGarantiaDS_87_Tfcontagemresultado_datadmn)";
         }
         else
         {
            GXv_int1[130] = 1;
         }
         if ( ! (DateTime.MinValue==AV265ExtraWWContagemResultadoEmGarantiaDS_88_Tfcontagemresultado_datadmn_to) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV265ExtraWWContagemResultadoEmGarantiaDS_88_Tfcontagemresultado_datadmn_to)";
         }
         else
         {
            GXv_int1[131] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV269ExtraWWContagemResultadoEmGarantiaDS_92_Tfcontagemresultado_contratadasigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Contratada_Sigla] like @lV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla)";
         }
         else
         {
            GXv_int1[132] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV269ExtraWWContagemResultadoEmGarantiaDS_92_Tfcontagemresultado_contratadasigla_sel)) )
         {
            sWhereString = sWhereString + " and (T5.[Contratada_Sigla] = @AV269ExtraWWContagemResultadoEmGarantiaDS_92_Tfcontagemresultado_contratadasigla_sel)";
         }
         else
         {
            GXv_int1[133] = 1;
         }
         if ( AV270ExtraWWContagemResultadoEmGarantiaDS_93_Tfcontagemresultado_statusdmn_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV270ExtraWWContagemResultadoEmGarantiaDS_93_Tfcontagemresultado_statusdmn_sels, "T1.[ContagemResultado_StatusDmn] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV272ExtraWWContagemResultadoEmGarantiaDS_95_Tfcontagemresultado_servicosigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Sigla] like @lV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla)";
         }
         else
         {
            GXv_int1[134] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV272ExtraWWContagemResultadoEmGarantiaDS_95_Tfcontagemresultado_servicosigla_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Sigla] = @AV272ExtraWWContagemResultadoEmGarantiaDS_95_Tfcontagemresultado_servicosigla_sel)";
         }
         else
         {
            GXv_int1[135] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_DemandaFM]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00U15( IGxContext context ,
                                             String A484ContagemResultado_StatusDmn ,
                                             IGxCollection AV270ExtraWWContagemResultadoEmGarantiaDS_93_Tfcontagemresultado_statusdmn_sels ,
                                             int AV9WWPContext_gxTpr_Contratada_codigo ,
                                             String AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1 ,
                                             short AV180ExtraWWContagemResultadoEmGarantiaDS_3_Dynamicfiltersoperator1 ,
                                             String AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 ,
                                             DateTime AV182ExtraWWContagemResultadoEmGarantiaDS_5_Contagemresultado_datadmn1 ,
                                             DateTime AV183ExtraWWContagemResultadoEmGarantiaDS_6_Contagemresultado_datadmn_to1 ,
                                             DateTime AV186ExtraWWContagemResultadoEmGarantiaDS_9_Contagemresultado_dataprevista1 ,
                                             DateTime AV187ExtraWWContagemResultadoEmGarantiaDS_10_Contagemresultado_dataprevista_to1 ,
                                             String AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1 ,
                                             int AV189ExtraWWContagemResultadoEmGarantiaDS_12_Contagemresultado_servico1 ,
                                             int AV190ExtraWWContagemResultadoEmGarantiaDS_13_Contagemresultado_sistemacod1 ,
                                             int AV191ExtraWWContagemResultadoEmGarantiaDS_14_Contagemresultado_contratadacod1 ,
                                             int AV178ExtraWWContagemResultadoEmGarantiaDS_1_Contratada_areatrabalhocod ,
                                             String AV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1 ,
                                             int AV193ExtraWWContagemResultadoEmGarantiaDS_16_Contagemresultado_owner1 ,
                                             bool AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 ,
                                             String AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2 ,
                                             short AV196ExtraWWContagemResultadoEmGarantiaDS_19_Dynamicfiltersoperator2 ,
                                             String AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 ,
                                             DateTime AV198ExtraWWContagemResultadoEmGarantiaDS_21_Contagemresultado_datadmn2 ,
                                             DateTime AV199ExtraWWContagemResultadoEmGarantiaDS_22_Contagemresultado_datadmn_to2 ,
                                             DateTime AV202ExtraWWContagemResultadoEmGarantiaDS_25_Contagemresultado_dataprevista2 ,
                                             DateTime AV203ExtraWWContagemResultadoEmGarantiaDS_26_Contagemresultado_dataprevista_to2 ,
                                             String AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2 ,
                                             int AV205ExtraWWContagemResultadoEmGarantiaDS_28_Contagemresultado_servico2 ,
                                             int AV206ExtraWWContagemResultadoEmGarantiaDS_29_Contagemresultado_sistemacod2 ,
                                             int AV207ExtraWWContagemResultadoEmGarantiaDS_30_Contagemresultado_contratadacod2 ,
                                             String AV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2 ,
                                             int AV209ExtraWWContagemResultadoEmGarantiaDS_32_Contagemresultado_owner2 ,
                                             bool AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 ,
                                             String AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3 ,
                                             short AV212ExtraWWContagemResultadoEmGarantiaDS_35_Dynamicfiltersoperator3 ,
                                             String AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 ,
                                             DateTime AV214ExtraWWContagemResultadoEmGarantiaDS_37_Contagemresultado_datadmn3 ,
                                             DateTime AV215ExtraWWContagemResultadoEmGarantiaDS_38_Contagemresultado_datadmn_to3 ,
                                             DateTime AV218ExtraWWContagemResultadoEmGarantiaDS_41_Contagemresultado_dataprevista3 ,
                                             DateTime AV219ExtraWWContagemResultadoEmGarantiaDS_42_Contagemresultado_dataprevista_to3 ,
                                             String AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3 ,
                                             int AV221ExtraWWContagemResultadoEmGarantiaDS_44_Contagemresultado_servico3 ,
                                             int AV222ExtraWWContagemResultadoEmGarantiaDS_45_Contagemresultado_sistemacod3 ,
                                             int AV223ExtraWWContagemResultadoEmGarantiaDS_46_Contagemresultado_contratadacod3 ,
                                             String AV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3 ,
                                             int AV225ExtraWWContagemResultadoEmGarantiaDS_48_Contagemresultado_owner3 ,
                                             bool AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 ,
                                             String AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4 ,
                                             short AV228ExtraWWContagemResultadoEmGarantiaDS_51_Dynamicfiltersoperator4 ,
                                             String AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 ,
                                             DateTime AV230ExtraWWContagemResultadoEmGarantiaDS_53_Contagemresultado_datadmn4 ,
                                             DateTime AV231ExtraWWContagemResultadoEmGarantiaDS_54_Contagemresultado_datadmn_to4 ,
                                             DateTime AV234ExtraWWContagemResultadoEmGarantiaDS_57_Contagemresultado_dataprevista4 ,
                                             DateTime AV235ExtraWWContagemResultadoEmGarantiaDS_58_Contagemresultado_dataprevista_to4 ,
                                             String AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4 ,
                                             int AV237ExtraWWContagemResultadoEmGarantiaDS_60_Contagemresultado_servico4 ,
                                             int AV238ExtraWWContagemResultadoEmGarantiaDS_61_Contagemresultado_sistemacod4 ,
                                             int AV239ExtraWWContagemResultadoEmGarantiaDS_62_Contagemresultado_contratadacod4 ,
                                             String AV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4 ,
                                             int AV241ExtraWWContagemResultadoEmGarantiaDS_64_Contagemresultado_owner4 ,
                                             bool AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 ,
                                             String AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5 ,
                                             short AV244ExtraWWContagemResultadoEmGarantiaDS_67_Dynamicfiltersoperator5 ,
                                             String AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 ,
                                             DateTime AV246ExtraWWContagemResultadoEmGarantiaDS_69_Contagemresultado_datadmn5 ,
                                             DateTime AV247ExtraWWContagemResultadoEmGarantiaDS_70_Contagemresultado_datadmn_to5 ,
                                             DateTime AV250ExtraWWContagemResultadoEmGarantiaDS_73_Contagemresultado_dataprevista5 ,
                                             DateTime AV251ExtraWWContagemResultadoEmGarantiaDS_74_Contagemresultado_dataprevista_to5 ,
                                             String AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5 ,
                                             int AV253ExtraWWContagemResultadoEmGarantiaDS_76_Contagemresultado_servico5 ,
                                             int AV254ExtraWWContagemResultadoEmGarantiaDS_77_Contagemresultado_sistemacod5 ,
                                             int AV255ExtraWWContagemResultadoEmGarantiaDS_78_Contagemresultado_contratadacod5 ,
                                             String AV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5 ,
                                             int AV257ExtraWWContagemResultadoEmGarantiaDS_80_Contagemresultado_owner5 ,
                                             String AV259ExtraWWContagemResultadoEmGarantiaDS_82_Tfcontagemresultado_demandafm_sel ,
                                             String AV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm ,
                                             String AV261ExtraWWContagemResultadoEmGarantiaDS_84_Tfcontagemresultado_demanda_sel ,
                                             String AV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda ,
                                             String AV263ExtraWWContagemResultadoEmGarantiaDS_86_Tfcontagemresultado_descricao_sel ,
                                             String AV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao ,
                                             DateTime AV264ExtraWWContagemResultadoEmGarantiaDS_87_Tfcontagemresultado_datadmn ,
                                             DateTime AV265ExtraWWContagemResultadoEmGarantiaDS_88_Tfcontagemresultado_datadmn_to ,
                                             String AV269ExtraWWContagemResultadoEmGarantiaDS_92_Tfcontagemresultado_contratadasigla_sel ,
                                             String AV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla ,
                                             int AV270ExtraWWContagemResultadoEmGarantiaDS_93_Tfcontagemresultado_statusdmn_sels_Count ,
                                             String AV272ExtraWWContagemResultadoEmGarantiaDS_95_Tfcontagemresultado_servicosigla_sel ,
                                             String AV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             String A457ContagemResultado_Demanda ,
                                             String A493ContagemResultado_DemandaFM ,
                                             DateTime A471ContagemResultado_DataDmn ,
                                             DateTime A1351ContagemResultado_DataPrevista ,
                                             int A601ContagemResultado_Servico ,
                                             int A489ContagemResultado_SistemaCod ,
                                             String A494ContagemResultado_Descricao ,
                                             int A508ContagemResultado_Owner ,
                                             String A803ContagemResultado_ContratadaSigla ,
                                             String A801ContagemResultado_ServicoSigla ,
                                             DateTime AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1 ,
                                             DateTime A566ContagemResultado_DataUltCnt ,
                                             DateTime AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1 ,
                                             DateTime AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2 ,
                                             DateTime AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2 ,
                                             DateTime AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3 ,
                                             DateTime AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3 ,
                                             DateTime AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4 ,
                                             DateTime AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4 ,
                                             DateTime AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5 ,
                                             DateTime AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5 ,
                                             DateTime AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt ,
                                             DateTime AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [136] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_Demanda], T3.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T5.[Contratada_Sigla] AS ContagemResultado_ContratadaSigla, T1.[ContagemResultado_Owner], T1.[ContagemResultado_Descricao], T1.[ContagemResultado_SistemaCod], T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_DataPrevista], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T5.[Contratada_AreaTrabalhoCod], COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt FROM (((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T4 ON T4.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Contratada] T5 WITH (NOLOCK) ON T5.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod])";
         scmdbuf = scmdbuf + " WHERE (Not ( @AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1))";
         scmdbuf = scmdbuf + " and (Not ( @AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1))";
         scmdbuf = scmdbuf + " and (Not ( @AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 = 1 and @AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2))";
         scmdbuf = scmdbuf + " and (Not ( @AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 = 1 and @AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2))";
         scmdbuf = scmdbuf + " and (Not ( @AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 = 1 and @AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3))";
         scmdbuf = scmdbuf + " and (Not ( @AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 = 1 and @AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3))";
         scmdbuf = scmdbuf + " and (Not ( @AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 = 1 and @AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4))";
         scmdbuf = scmdbuf + " and (Not ( @AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 = 1 and @AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4))";
         scmdbuf = scmdbuf + " and (Not ( @AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 = 1 and @AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5))";
         scmdbuf = scmdbuf + " and (Not ( @AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 = 1 and @AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5))";
         scmdbuf = scmdbuf + " and ((@AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt))";
         scmdbuf = scmdbuf + " and ((@AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to))";
         scmdbuf = scmdbuf + " and (T5.[Contratada_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( AV9WWPContext_gxTpr_Contratada_codigo > 0 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV9WWPCo_2Contratada_codigo)";
         }
         else
         {
            GXv_int3[43] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV180ExtraWWContagemResultadoEmGarantiaDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] = @AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int3[44] = 1;
            GXv_int3[45] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV180ExtraWWContagemResultadoEmGarantiaDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] like @lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int3[46] = 1;
            GXv_int3[47] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV180ExtraWWContagemResultadoEmGarantiaDS_3_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] like '%' + @lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int3[48] = 1;
            GXv_int3[49] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV182ExtraWWContagemResultadoEmGarantiaDS_5_Contagemresultado_datadmn1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV182ExtraWWContagemResultadoEmGarantiaDS_5_Contagemresultado_datadmn1)";
         }
         else
         {
            GXv_int3[50] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV183ExtraWWContagemResultadoEmGarantiaDS_6_Contagemresultado_datadmn_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV183ExtraWWContagemResultadoEmGarantiaDS_6_Contagemresultado_datadmn_to1)";
         }
         else
         {
            GXv_int3[51] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV186ExtraWWContagemResultadoEmGarantiaDS_9_Contagemresultado_dataprevista1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV186ExtraWWContagemResultadoEmGarantiaDS_9_Contagemresultado_dataprevista1)";
         }
         else
         {
            GXv_int3[52] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV187ExtraWWContagemResultadoEmGarantiaDS_10_Contagemresultado_dataprevista_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] <= @AV187ExtraWWContagemResultadoEmGarantiaDS_10_Contagemresultado_dataprevista_to1)";
         }
         else
         {
            GXv_int3[53] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1)) && ! ( StringUtil.StrCmp(AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1)";
         }
         else
         {
            GXv_int3[54] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L' or T1.[ContagemResultado_StatusDmn] = 'O'))";
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV189ExtraWWContagemResultadoEmGarantiaDS_12_Contagemresultado_servico1) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV189ExtraWWContagemResultadoEmGarantiaDS_12_Contagemresultado_servico1)";
         }
         else
         {
            GXv_int3[55] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV190ExtraWWContagemResultadoEmGarantiaDS_13_Contagemresultado_sistemacod1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV190ExtraWWContagemResultadoEmGarantiaDS_13_Contagemresultado_sistemacod1)";
         }
         else
         {
            GXv_int3[56] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV191ExtraWWContagemResultadoEmGarantiaDS_14_Contagemresultado_contratadacod1 > 0 ) && ( AV178ExtraWWContagemResultadoEmGarantiaDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV191ExtraWWContagemResultadoEmGarantiaDS_14_Contagemresultado_contratadacod1)";
         }
         else
         {
            GXv_int3[57] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1 + '%')";
         }
         else
         {
            GXv_int3[58] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV193ExtraWWContagemResultadoEmGarantiaDS_16_Contagemresultado_owner1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Owner] = @AV193ExtraWWContagemResultadoEmGarantiaDS_16_Contagemresultado_owner1)";
         }
         else
         {
            GXv_int3[59] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV196ExtraWWContagemResultadoEmGarantiaDS_19_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] = @AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int3[60] = 1;
            GXv_int3[61] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV196ExtraWWContagemResultadoEmGarantiaDS_19_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] like @lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int3[62] = 1;
            GXv_int3[63] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV196ExtraWWContagemResultadoEmGarantiaDS_19_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] like '%' + @lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int3[64] = 1;
            GXv_int3[65] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV198ExtraWWContagemResultadoEmGarantiaDS_21_Contagemresultado_datadmn2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV198ExtraWWContagemResultadoEmGarantiaDS_21_Contagemresultado_datadmn2)";
         }
         else
         {
            GXv_int3[66] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV199ExtraWWContagemResultadoEmGarantiaDS_22_Contagemresultado_datadmn_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV199ExtraWWContagemResultadoEmGarantiaDS_22_Contagemresultado_datadmn_to2)";
         }
         else
         {
            GXv_int3[67] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV202ExtraWWContagemResultadoEmGarantiaDS_25_Contagemresultado_dataprevista2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV202ExtraWWContagemResultadoEmGarantiaDS_25_Contagemresultado_dataprevista2)";
         }
         else
         {
            GXv_int3[68] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV203ExtraWWContagemResultadoEmGarantiaDS_26_Contagemresultado_dataprevista_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] <= @AV203ExtraWWContagemResultadoEmGarantiaDS_26_Contagemresultado_dataprevista_to2)";
         }
         else
         {
            GXv_int3[69] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2)) && ! ( StringUtil.StrCmp(AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2)";
         }
         else
         {
            GXv_int3[70] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L' or T1.[ContagemResultado_StatusDmn] = 'O'))";
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV205ExtraWWContagemResultadoEmGarantiaDS_28_Contagemresultado_servico2) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV205ExtraWWContagemResultadoEmGarantiaDS_28_Contagemresultado_servico2)";
         }
         else
         {
            GXv_int3[71] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV206ExtraWWContagemResultadoEmGarantiaDS_29_Contagemresultado_sistemacod2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV206ExtraWWContagemResultadoEmGarantiaDS_29_Contagemresultado_sistemacod2)";
         }
         else
         {
            GXv_int3[72] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV207ExtraWWContagemResultadoEmGarantiaDS_30_Contagemresultado_contratadacod2 > 0 ) && ( AV178ExtraWWContagemResultadoEmGarantiaDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV207ExtraWWContagemResultadoEmGarantiaDS_30_Contagemresultado_contratadacod2)";
         }
         else
         {
            GXv_int3[73] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2 + '%')";
         }
         else
         {
            GXv_int3[74] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV209ExtraWWContagemResultadoEmGarantiaDS_32_Contagemresultado_owner2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Owner] = @AV209ExtraWWContagemResultadoEmGarantiaDS_32_Contagemresultado_owner2)";
         }
         else
         {
            GXv_int3[75] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV212ExtraWWContagemResultadoEmGarantiaDS_35_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] = @AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int3[76] = 1;
            GXv_int3[77] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV212ExtraWWContagemResultadoEmGarantiaDS_35_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] like @lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int3[78] = 1;
            GXv_int3[79] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV212ExtraWWContagemResultadoEmGarantiaDS_35_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] like '%' + @lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int3[80] = 1;
            GXv_int3[81] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV214ExtraWWContagemResultadoEmGarantiaDS_37_Contagemresultado_datadmn3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV214ExtraWWContagemResultadoEmGarantiaDS_37_Contagemresultado_datadmn3)";
         }
         else
         {
            GXv_int3[82] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV215ExtraWWContagemResultadoEmGarantiaDS_38_Contagemresultado_datadmn_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV215ExtraWWContagemResultadoEmGarantiaDS_38_Contagemresultado_datadmn_to3)";
         }
         else
         {
            GXv_int3[83] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV218ExtraWWContagemResultadoEmGarantiaDS_41_Contagemresultado_dataprevista3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV218ExtraWWContagemResultadoEmGarantiaDS_41_Contagemresultado_dataprevista3)";
         }
         else
         {
            GXv_int3[84] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV219ExtraWWContagemResultadoEmGarantiaDS_42_Contagemresultado_dataprevista_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] <= @AV219ExtraWWContagemResultadoEmGarantiaDS_42_Contagemresultado_dataprevista_to3)";
         }
         else
         {
            GXv_int3[85] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3)) && ! ( StringUtil.StrCmp(AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3)";
         }
         else
         {
            GXv_int3[86] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L' or T1.[ContagemResultado_StatusDmn] = 'O'))";
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV221ExtraWWContagemResultadoEmGarantiaDS_44_Contagemresultado_servico3) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV221ExtraWWContagemResultadoEmGarantiaDS_44_Contagemresultado_servico3)";
         }
         else
         {
            GXv_int3[87] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV222ExtraWWContagemResultadoEmGarantiaDS_45_Contagemresultado_sistemacod3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV222ExtraWWContagemResultadoEmGarantiaDS_45_Contagemresultado_sistemacod3)";
         }
         else
         {
            GXv_int3[88] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV223ExtraWWContagemResultadoEmGarantiaDS_46_Contagemresultado_contratadacod3 > 0 ) && ( AV178ExtraWWContagemResultadoEmGarantiaDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV223ExtraWWContagemResultadoEmGarantiaDS_46_Contagemresultado_contratadacod3)";
         }
         else
         {
            GXv_int3[89] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3 + '%')";
         }
         else
         {
            GXv_int3[90] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV225ExtraWWContagemResultadoEmGarantiaDS_48_Contagemresultado_owner3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Owner] = @AV225ExtraWWContagemResultadoEmGarantiaDS_48_Contagemresultado_owner3)";
         }
         else
         {
            GXv_int3[91] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV228ExtraWWContagemResultadoEmGarantiaDS_51_Dynamicfiltersoperator4 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 or T1.[ContagemResultado_DemandaFM] = @AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4)";
         }
         else
         {
            GXv_int3[92] = 1;
            GXv_int3[93] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV228ExtraWWContagemResultadoEmGarantiaDS_51_Dynamicfiltersoperator4 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 or T1.[ContagemResultado_DemandaFM] like @lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4)";
         }
         else
         {
            GXv_int3[94] = 1;
            GXv_int3[95] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV228ExtraWWContagemResultadoEmGarantiaDS_51_Dynamicfiltersoperator4 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 or T1.[ContagemResultado_DemandaFM] like '%' + @lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4)";
         }
         else
         {
            GXv_int3[96] = 1;
            GXv_int3[97] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV230ExtraWWContagemResultadoEmGarantiaDS_53_Contagemresultado_datadmn4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV230ExtraWWContagemResultadoEmGarantiaDS_53_Contagemresultado_datadmn4)";
         }
         else
         {
            GXv_int3[98] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV231ExtraWWContagemResultadoEmGarantiaDS_54_Contagemresultado_datadmn_to4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV231ExtraWWContagemResultadoEmGarantiaDS_54_Contagemresultado_datadmn_to4)";
         }
         else
         {
            GXv_int3[99] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV234ExtraWWContagemResultadoEmGarantiaDS_57_Contagemresultado_dataprevista4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV234ExtraWWContagemResultadoEmGarantiaDS_57_Contagemresultado_dataprevista4)";
         }
         else
         {
            GXv_int3[100] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV235ExtraWWContagemResultadoEmGarantiaDS_58_Contagemresultado_dataprevista_to4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] <= @AV235ExtraWWContagemResultadoEmGarantiaDS_58_Contagemresultado_dataprevista_to4)";
         }
         else
         {
            GXv_int3[101] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4)) && ! ( StringUtil.StrCmp(AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4)";
         }
         else
         {
            GXv_int3[102] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L' or T1.[ContagemResultado_StatusDmn] = 'O'))";
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV237ExtraWWContagemResultadoEmGarantiaDS_60_Contagemresultado_servico4) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV237ExtraWWContagemResultadoEmGarantiaDS_60_Contagemresultado_servico4)";
         }
         else
         {
            GXv_int3[103] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV238ExtraWWContagemResultadoEmGarantiaDS_61_Contagemresultado_sistemacod4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV238ExtraWWContagemResultadoEmGarantiaDS_61_Contagemresultado_sistemacod4)";
         }
         else
         {
            GXv_int3[104] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV239ExtraWWContagemResultadoEmGarantiaDS_62_Contagemresultado_contratadacod4 > 0 ) && ( AV178ExtraWWContagemResultadoEmGarantiaDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV239ExtraWWContagemResultadoEmGarantiaDS_62_Contagemresultado_contratadacod4)";
         }
         else
         {
            GXv_int3[105] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4 + '%')";
         }
         else
         {
            GXv_int3[106] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV241ExtraWWContagemResultadoEmGarantiaDS_64_Contagemresultado_owner4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Owner] = @AV241ExtraWWContagemResultadoEmGarantiaDS_64_Contagemresultado_owner4)";
         }
         else
         {
            GXv_int3[107] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV244ExtraWWContagemResultadoEmGarantiaDS_67_Dynamicfiltersoperator5 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 or T1.[ContagemResultado_DemandaFM] = @AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5)";
         }
         else
         {
            GXv_int3[108] = 1;
            GXv_int3[109] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV244ExtraWWContagemResultadoEmGarantiaDS_67_Dynamicfiltersoperator5 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 or T1.[ContagemResultado_DemandaFM] like @lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5)";
         }
         else
         {
            GXv_int3[110] = 1;
            GXv_int3[111] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV244ExtraWWContagemResultadoEmGarantiaDS_67_Dynamicfiltersoperator5 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 or T1.[ContagemResultado_DemandaFM] like '%' + @lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5)";
         }
         else
         {
            GXv_int3[112] = 1;
            GXv_int3[113] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV246ExtraWWContagemResultadoEmGarantiaDS_69_Contagemresultado_datadmn5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV246ExtraWWContagemResultadoEmGarantiaDS_69_Contagemresultado_datadmn5)";
         }
         else
         {
            GXv_int3[114] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV247ExtraWWContagemResultadoEmGarantiaDS_70_Contagemresultado_datadmn_to5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV247ExtraWWContagemResultadoEmGarantiaDS_70_Contagemresultado_datadmn_to5)";
         }
         else
         {
            GXv_int3[115] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV250ExtraWWContagemResultadoEmGarantiaDS_73_Contagemresultado_dataprevista5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV250ExtraWWContagemResultadoEmGarantiaDS_73_Contagemresultado_dataprevista5)";
         }
         else
         {
            GXv_int3[116] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV251ExtraWWContagemResultadoEmGarantiaDS_74_Contagemresultado_dataprevista_to5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] <= @AV251ExtraWWContagemResultadoEmGarantiaDS_74_Contagemresultado_dataprevista_to5)";
         }
         else
         {
            GXv_int3[117] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5)) && ! ( StringUtil.StrCmp(AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5)";
         }
         else
         {
            GXv_int3[118] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L' or T1.[ContagemResultado_StatusDmn] = 'O'))";
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV253ExtraWWContagemResultadoEmGarantiaDS_76_Contagemresultado_servico5) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV253ExtraWWContagemResultadoEmGarantiaDS_76_Contagemresultado_servico5)";
         }
         else
         {
            GXv_int3[119] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV254ExtraWWContagemResultadoEmGarantiaDS_77_Contagemresultado_sistemacod5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV254ExtraWWContagemResultadoEmGarantiaDS_77_Contagemresultado_sistemacod5)";
         }
         else
         {
            GXv_int3[120] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV255ExtraWWContagemResultadoEmGarantiaDS_78_Contagemresultado_contratadacod5 > 0 ) && ( AV178ExtraWWContagemResultadoEmGarantiaDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV255ExtraWWContagemResultadoEmGarantiaDS_78_Contagemresultado_contratadacod5)";
         }
         else
         {
            GXv_int3[121] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5 + '%')";
         }
         else
         {
            GXv_int3[122] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV257ExtraWWContagemResultadoEmGarantiaDS_80_Contagemresultado_owner5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Owner] = @AV257ExtraWWContagemResultadoEmGarantiaDS_80_Contagemresultado_owner5)";
         }
         else
         {
            GXv_int3[123] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV259ExtraWWContagemResultadoEmGarantiaDS_82_Tfcontagemresultado_demandafm_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] like @lV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm)";
         }
         else
         {
            GXv_int3[124] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV259ExtraWWContagemResultadoEmGarantiaDS_82_Tfcontagemresultado_demandafm_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] = @AV259ExtraWWContagemResultadoEmGarantiaDS_82_Tfcontagemresultado_demandafm_sel)";
         }
         else
         {
            GXv_int3[125] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV261ExtraWWContagemResultadoEmGarantiaDS_84_Tfcontagemresultado_demanda_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda)";
         }
         else
         {
            GXv_int3[126] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV261ExtraWWContagemResultadoEmGarantiaDS_84_Tfcontagemresultado_demanda_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV261ExtraWWContagemResultadoEmGarantiaDS_84_Tfcontagemresultado_demanda_sel)";
         }
         else
         {
            GXv_int3[127] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV263ExtraWWContagemResultadoEmGarantiaDS_86_Tfcontagemresultado_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like @lV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao)";
         }
         else
         {
            GXv_int3[128] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV263ExtraWWContagemResultadoEmGarantiaDS_86_Tfcontagemresultado_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] = @AV263ExtraWWContagemResultadoEmGarantiaDS_86_Tfcontagemresultado_descricao_sel)";
         }
         else
         {
            GXv_int3[129] = 1;
         }
         if ( ! (DateTime.MinValue==AV264ExtraWWContagemResultadoEmGarantiaDS_87_Tfcontagemresultado_datadmn) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV264ExtraWWContagemResultadoEmGarantiaDS_87_Tfcontagemresultado_datadmn)";
         }
         else
         {
            GXv_int3[130] = 1;
         }
         if ( ! (DateTime.MinValue==AV265ExtraWWContagemResultadoEmGarantiaDS_88_Tfcontagemresultado_datadmn_to) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV265ExtraWWContagemResultadoEmGarantiaDS_88_Tfcontagemresultado_datadmn_to)";
         }
         else
         {
            GXv_int3[131] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV269ExtraWWContagemResultadoEmGarantiaDS_92_Tfcontagemresultado_contratadasigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Contratada_Sigla] like @lV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla)";
         }
         else
         {
            GXv_int3[132] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV269ExtraWWContagemResultadoEmGarantiaDS_92_Tfcontagemresultado_contratadasigla_sel)) )
         {
            sWhereString = sWhereString + " and (T5.[Contratada_Sigla] = @AV269ExtraWWContagemResultadoEmGarantiaDS_92_Tfcontagemresultado_contratadasigla_sel)";
         }
         else
         {
            GXv_int3[133] = 1;
         }
         if ( AV270ExtraWWContagemResultadoEmGarantiaDS_93_Tfcontagemresultado_statusdmn_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV270ExtraWWContagemResultadoEmGarantiaDS_93_Tfcontagemresultado_statusdmn_sels, "T1.[ContagemResultado_StatusDmn] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV272ExtraWWContagemResultadoEmGarantiaDS_95_Tfcontagemresultado_servicosigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Sigla] like @lV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla)";
         }
         else
         {
            GXv_int3[134] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV272ExtraWWContagemResultadoEmGarantiaDS_95_Tfcontagemresultado_servicosigla_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Sigla] = @AV272ExtraWWContagemResultadoEmGarantiaDS_95_Tfcontagemresultado_servicosigla_sel)";
         }
         else
         {
            GXv_int3[135] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Demanda]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00U17( IGxContext context ,
                                             String A484ContagemResultado_StatusDmn ,
                                             IGxCollection AV270ExtraWWContagemResultadoEmGarantiaDS_93_Tfcontagemresultado_statusdmn_sels ,
                                             int AV9WWPContext_gxTpr_Contratada_codigo ,
                                             String AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1 ,
                                             short AV180ExtraWWContagemResultadoEmGarantiaDS_3_Dynamicfiltersoperator1 ,
                                             String AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 ,
                                             DateTime AV182ExtraWWContagemResultadoEmGarantiaDS_5_Contagemresultado_datadmn1 ,
                                             DateTime AV183ExtraWWContagemResultadoEmGarantiaDS_6_Contagemresultado_datadmn_to1 ,
                                             DateTime AV186ExtraWWContagemResultadoEmGarantiaDS_9_Contagemresultado_dataprevista1 ,
                                             DateTime AV187ExtraWWContagemResultadoEmGarantiaDS_10_Contagemresultado_dataprevista_to1 ,
                                             String AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1 ,
                                             int AV189ExtraWWContagemResultadoEmGarantiaDS_12_Contagemresultado_servico1 ,
                                             int AV190ExtraWWContagemResultadoEmGarantiaDS_13_Contagemresultado_sistemacod1 ,
                                             int AV191ExtraWWContagemResultadoEmGarantiaDS_14_Contagemresultado_contratadacod1 ,
                                             int AV178ExtraWWContagemResultadoEmGarantiaDS_1_Contratada_areatrabalhocod ,
                                             String AV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1 ,
                                             int AV193ExtraWWContagemResultadoEmGarantiaDS_16_Contagemresultado_owner1 ,
                                             bool AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 ,
                                             String AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2 ,
                                             short AV196ExtraWWContagemResultadoEmGarantiaDS_19_Dynamicfiltersoperator2 ,
                                             String AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 ,
                                             DateTime AV198ExtraWWContagemResultadoEmGarantiaDS_21_Contagemresultado_datadmn2 ,
                                             DateTime AV199ExtraWWContagemResultadoEmGarantiaDS_22_Contagemresultado_datadmn_to2 ,
                                             DateTime AV202ExtraWWContagemResultadoEmGarantiaDS_25_Contagemresultado_dataprevista2 ,
                                             DateTime AV203ExtraWWContagemResultadoEmGarantiaDS_26_Contagemresultado_dataprevista_to2 ,
                                             String AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2 ,
                                             int AV205ExtraWWContagemResultadoEmGarantiaDS_28_Contagemresultado_servico2 ,
                                             int AV206ExtraWWContagemResultadoEmGarantiaDS_29_Contagemresultado_sistemacod2 ,
                                             int AV207ExtraWWContagemResultadoEmGarantiaDS_30_Contagemresultado_contratadacod2 ,
                                             String AV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2 ,
                                             int AV209ExtraWWContagemResultadoEmGarantiaDS_32_Contagemresultado_owner2 ,
                                             bool AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 ,
                                             String AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3 ,
                                             short AV212ExtraWWContagemResultadoEmGarantiaDS_35_Dynamicfiltersoperator3 ,
                                             String AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 ,
                                             DateTime AV214ExtraWWContagemResultadoEmGarantiaDS_37_Contagemresultado_datadmn3 ,
                                             DateTime AV215ExtraWWContagemResultadoEmGarantiaDS_38_Contagemresultado_datadmn_to3 ,
                                             DateTime AV218ExtraWWContagemResultadoEmGarantiaDS_41_Contagemresultado_dataprevista3 ,
                                             DateTime AV219ExtraWWContagemResultadoEmGarantiaDS_42_Contagemresultado_dataprevista_to3 ,
                                             String AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3 ,
                                             int AV221ExtraWWContagemResultadoEmGarantiaDS_44_Contagemresultado_servico3 ,
                                             int AV222ExtraWWContagemResultadoEmGarantiaDS_45_Contagemresultado_sistemacod3 ,
                                             int AV223ExtraWWContagemResultadoEmGarantiaDS_46_Contagemresultado_contratadacod3 ,
                                             String AV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3 ,
                                             int AV225ExtraWWContagemResultadoEmGarantiaDS_48_Contagemresultado_owner3 ,
                                             bool AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 ,
                                             String AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4 ,
                                             short AV228ExtraWWContagemResultadoEmGarantiaDS_51_Dynamicfiltersoperator4 ,
                                             String AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 ,
                                             DateTime AV230ExtraWWContagemResultadoEmGarantiaDS_53_Contagemresultado_datadmn4 ,
                                             DateTime AV231ExtraWWContagemResultadoEmGarantiaDS_54_Contagemresultado_datadmn_to4 ,
                                             DateTime AV234ExtraWWContagemResultadoEmGarantiaDS_57_Contagemresultado_dataprevista4 ,
                                             DateTime AV235ExtraWWContagemResultadoEmGarantiaDS_58_Contagemresultado_dataprevista_to4 ,
                                             String AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4 ,
                                             int AV237ExtraWWContagemResultadoEmGarantiaDS_60_Contagemresultado_servico4 ,
                                             int AV238ExtraWWContagemResultadoEmGarantiaDS_61_Contagemresultado_sistemacod4 ,
                                             int AV239ExtraWWContagemResultadoEmGarantiaDS_62_Contagemresultado_contratadacod4 ,
                                             String AV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4 ,
                                             int AV241ExtraWWContagemResultadoEmGarantiaDS_64_Contagemresultado_owner4 ,
                                             bool AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 ,
                                             String AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5 ,
                                             short AV244ExtraWWContagemResultadoEmGarantiaDS_67_Dynamicfiltersoperator5 ,
                                             String AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 ,
                                             DateTime AV246ExtraWWContagemResultadoEmGarantiaDS_69_Contagemresultado_datadmn5 ,
                                             DateTime AV247ExtraWWContagemResultadoEmGarantiaDS_70_Contagemresultado_datadmn_to5 ,
                                             DateTime AV250ExtraWWContagemResultadoEmGarantiaDS_73_Contagemresultado_dataprevista5 ,
                                             DateTime AV251ExtraWWContagemResultadoEmGarantiaDS_74_Contagemresultado_dataprevista_to5 ,
                                             String AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5 ,
                                             int AV253ExtraWWContagemResultadoEmGarantiaDS_76_Contagemresultado_servico5 ,
                                             int AV254ExtraWWContagemResultadoEmGarantiaDS_77_Contagemresultado_sistemacod5 ,
                                             int AV255ExtraWWContagemResultadoEmGarantiaDS_78_Contagemresultado_contratadacod5 ,
                                             String AV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5 ,
                                             int AV257ExtraWWContagemResultadoEmGarantiaDS_80_Contagemresultado_owner5 ,
                                             String AV259ExtraWWContagemResultadoEmGarantiaDS_82_Tfcontagemresultado_demandafm_sel ,
                                             String AV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm ,
                                             String AV261ExtraWWContagemResultadoEmGarantiaDS_84_Tfcontagemresultado_demanda_sel ,
                                             String AV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda ,
                                             String AV263ExtraWWContagemResultadoEmGarantiaDS_86_Tfcontagemresultado_descricao_sel ,
                                             String AV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao ,
                                             DateTime AV264ExtraWWContagemResultadoEmGarantiaDS_87_Tfcontagemresultado_datadmn ,
                                             DateTime AV265ExtraWWContagemResultadoEmGarantiaDS_88_Tfcontagemresultado_datadmn_to ,
                                             String AV269ExtraWWContagemResultadoEmGarantiaDS_92_Tfcontagemresultado_contratadasigla_sel ,
                                             String AV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla ,
                                             int AV270ExtraWWContagemResultadoEmGarantiaDS_93_Tfcontagemresultado_statusdmn_sels_Count ,
                                             String AV272ExtraWWContagemResultadoEmGarantiaDS_95_Tfcontagemresultado_servicosigla_sel ,
                                             String AV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             String A457ContagemResultado_Demanda ,
                                             String A493ContagemResultado_DemandaFM ,
                                             DateTime A471ContagemResultado_DataDmn ,
                                             DateTime A1351ContagemResultado_DataPrevista ,
                                             int A601ContagemResultado_Servico ,
                                             int A489ContagemResultado_SistemaCod ,
                                             String A494ContagemResultado_Descricao ,
                                             int A508ContagemResultado_Owner ,
                                             String A803ContagemResultado_ContratadaSigla ,
                                             String A801ContagemResultado_ServicoSigla ,
                                             DateTime AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1 ,
                                             DateTime A566ContagemResultado_DataUltCnt ,
                                             DateTime AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1 ,
                                             DateTime AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2 ,
                                             DateTime AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2 ,
                                             DateTime AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3 ,
                                             DateTime AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3 ,
                                             DateTime AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4 ,
                                             DateTime AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4 ,
                                             DateTime AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5 ,
                                             DateTime AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5 ,
                                             DateTime AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt ,
                                             DateTime AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [136] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_Codigo], T5.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_Descricao], T3.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T5.[Contratada_Sigla] AS ContagemResultado_ContratadaSigla, T1.[ContagemResultado_Owner], T1.[ContagemResultado_SistemaCod], T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_DataPrevista], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt FROM (((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T4 ON T4.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Contratada] T5 WITH (NOLOCK) ON T5.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod])";
         scmdbuf = scmdbuf + " WHERE (Not ( @AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1))";
         scmdbuf = scmdbuf + " and (Not ( @AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1))";
         scmdbuf = scmdbuf + " and (Not ( @AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 = 1 and @AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2))";
         scmdbuf = scmdbuf + " and (Not ( @AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 = 1 and @AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2))";
         scmdbuf = scmdbuf + " and (Not ( @AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 = 1 and @AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3))";
         scmdbuf = scmdbuf + " and (Not ( @AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 = 1 and @AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3))";
         scmdbuf = scmdbuf + " and (Not ( @AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 = 1 and @AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4))";
         scmdbuf = scmdbuf + " and (Not ( @AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 = 1 and @AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4))";
         scmdbuf = scmdbuf + " and (Not ( @AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 = 1 and @AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5))";
         scmdbuf = scmdbuf + " and (Not ( @AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 = 1 and @AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5))";
         scmdbuf = scmdbuf + " and ((@AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt))";
         scmdbuf = scmdbuf + " and ((@AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to))";
         scmdbuf = scmdbuf + " and (T5.[Contratada_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( AV9WWPContext_gxTpr_Contratada_codigo > 0 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV9WWPCo_2Contratada_codigo)";
         }
         else
         {
            GXv_int5[43] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV180ExtraWWContagemResultadoEmGarantiaDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] = @AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int5[44] = 1;
            GXv_int5[45] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV180ExtraWWContagemResultadoEmGarantiaDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] like @lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int5[46] = 1;
            GXv_int5[47] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV180ExtraWWContagemResultadoEmGarantiaDS_3_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] like '%' + @lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int5[48] = 1;
            GXv_int5[49] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV182ExtraWWContagemResultadoEmGarantiaDS_5_Contagemresultado_datadmn1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV182ExtraWWContagemResultadoEmGarantiaDS_5_Contagemresultado_datadmn1)";
         }
         else
         {
            GXv_int5[50] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV183ExtraWWContagemResultadoEmGarantiaDS_6_Contagemresultado_datadmn_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV183ExtraWWContagemResultadoEmGarantiaDS_6_Contagemresultado_datadmn_to1)";
         }
         else
         {
            GXv_int5[51] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV186ExtraWWContagemResultadoEmGarantiaDS_9_Contagemresultado_dataprevista1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV186ExtraWWContagemResultadoEmGarantiaDS_9_Contagemresultado_dataprevista1)";
         }
         else
         {
            GXv_int5[52] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV187ExtraWWContagemResultadoEmGarantiaDS_10_Contagemresultado_dataprevista_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] <= @AV187ExtraWWContagemResultadoEmGarantiaDS_10_Contagemresultado_dataprevista_to1)";
         }
         else
         {
            GXv_int5[53] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1)) && ! ( StringUtil.StrCmp(AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1)";
         }
         else
         {
            GXv_int5[54] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L' or T1.[ContagemResultado_StatusDmn] = 'O'))";
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV189ExtraWWContagemResultadoEmGarantiaDS_12_Contagemresultado_servico1) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV189ExtraWWContagemResultadoEmGarantiaDS_12_Contagemresultado_servico1)";
         }
         else
         {
            GXv_int5[55] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV190ExtraWWContagemResultadoEmGarantiaDS_13_Contagemresultado_sistemacod1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV190ExtraWWContagemResultadoEmGarantiaDS_13_Contagemresultado_sistemacod1)";
         }
         else
         {
            GXv_int5[56] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV191ExtraWWContagemResultadoEmGarantiaDS_14_Contagemresultado_contratadacod1 > 0 ) && ( AV178ExtraWWContagemResultadoEmGarantiaDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV191ExtraWWContagemResultadoEmGarantiaDS_14_Contagemresultado_contratadacod1)";
         }
         else
         {
            GXv_int5[57] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1 + '%')";
         }
         else
         {
            GXv_int5[58] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV193ExtraWWContagemResultadoEmGarantiaDS_16_Contagemresultado_owner1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Owner] = @AV193ExtraWWContagemResultadoEmGarantiaDS_16_Contagemresultado_owner1)";
         }
         else
         {
            GXv_int5[59] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV196ExtraWWContagemResultadoEmGarantiaDS_19_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] = @AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int5[60] = 1;
            GXv_int5[61] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV196ExtraWWContagemResultadoEmGarantiaDS_19_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] like @lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int5[62] = 1;
            GXv_int5[63] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV196ExtraWWContagemResultadoEmGarantiaDS_19_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] like '%' + @lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int5[64] = 1;
            GXv_int5[65] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV198ExtraWWContagemResultadoEmGarantiaDS_21_Contagemresultado_datadmn2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV198ExtraWWContagemResultadoEmGarantiaDS_21_Contagemresultado_datadmn2)";
         }
         else
         {
            GXv_int5[66] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV199ExtraWWContagemResultadoEmGarantiaDS_22_Contagemresultado_datadmn_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV199ExtraWWContagemResultadoEmGarantiaDS_22_Contagemresultado_datadmn_to2)";
         }
         else
         {
            GXv_int5[67] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV202ExtraWWContagemResultadoEmGarantiaDS_25_Contagemresultado_dataprevista2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV202ExtraWWContagemResultadoEmGarantiaDS_25_Contagemresultado_dataprevista2)";
         }
         else
         {
            GXv_int5[68] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV203ExtraWWContagemResultadoEmGarantiaDS_26_Contagemresultado_dataprevista_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] <= @AV203ExtraWWContagemResultadoEmGarantiaDS_26_Contagemresultado_dataprevista_to2)";
         }
         else
         {
            GXv_int5[69] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2)) && ! ( StringUtil.StrCmp(AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2)";
         }
         else
         {
            GXv_int5[70] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L' or T1.[ContagemResultado_StatusDmn] = 'O'))";
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV205ExtraWWContagemResultadoEmGarantiaDS_28_Contagemresultado_servico2) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV205ExtraWWContagemResultadoEmGarantiaDS_28_Contagemresultado_servico2)";
         }
         else
         {
            GXv_int5[71] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV206ExtraWWContagemResultadoEmGarantiaDS_29_Contagemresultado_sistemacod2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV206ExtraWWContagemResultadoEmGarantiaDS_29_Contagemresultado_sistemacod2)";
         }
         else
         {
            GXv_int5[72] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV207ExtraWWContagemResultadoEmGarantiaDS_30_Contagemresultado_contratadacod2 > 0 ) && ( AV178ExtraWWContagemResultadoEmGarantiaDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV207ExtraWWContagemResultadoEmGarantiaDS_30_Contagemresultado_contratadacod2)";
         }
         else
         {
            GXv_int5[73] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2 + '%')";
         }
         else
         {
            GXv_int5[74] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV209ExtraWWContagemResultadoEmGarantiaDS_32_Contagemresultado_owner2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Owner] = @AV209ExtraWWContagemResultadoEmGarantiaDS_32_Contagemresultado_owner2)";
         }
         else
         {
            GXv_int5[75] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV212ExtraWWContagemResultadoEmGarantiaDS_35_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] = @AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int5[76] = 1;
            GXv_int5[77] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV212ExtraWWContagemResultadoEmGarantiaDS_35_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] like @lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int5[78] = 1;
            GXv_int5[79] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV212ExtraWWContagemResultadoEmGarantiaDS_35_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] like '%' + @lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int5[80] = 1;
            GXv_int5[81] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV214ExtraWWContagemResultadoEmGarantiaDS_37_Contagemresultado_datadmn3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV214ExtraWWContagemResultadoEmGarantiaDS_37_Contagemresultado_datadmn3)";
         }
         else
         {
            GXv_int5[82] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV215ExtraWWContagemResultadoEmGarantiaDS_38_Contagemresultado_datadmn_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV215ExtraWWContagemResultadoEmGarantiaDS_38_Contagemresultado_datadmn_to3)";
         }
         else
         {
            GXv_int5[83] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV218ExtraWWContagemResultadoEmGarantiaDS_41_Contagemresultado_dataprevista3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV218ExtraWWContagemResultadoEmGarantiaDS_41_Contagemresultado_dataprevista3)";
         }
         else
         {
            GXv_int5[84] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV219ExtraWWContagemResultadoEmGarantiaDS_42_Contagemresultado_dataprevista_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] <= @AV219ExtraWWContagemResultadoEmGarantiaDS_42_Contagemresultado_dataprevista_to3)";
         }
         else
         {
            GXv_int5[85] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3)) && ! ( StringUtil.StrCmp(AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3)";
         }
         else
         {
            GXv_int5[86] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L' or T1.[ContagemResultado_StatusDmn] = 'O'))";
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV221ExtraWWContagemResultadoEmGarantiaDS_44_Contagemresultado_servico3) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV221ExtraWWContagemResultadoEmGarantiaDS_44_Contagemresultado_servico3)";
         }
         else
         {
            GXv_int5[87] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV222ExtraWWContagemResultadoEmGarantiaDS_45_Contagemresultado_sistemacod3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV222ExtraWWContagemResultadoEmGarantiaDS_45_Contagemresultado_sistemacod3)";
         }
         else
         {
            GXv_int5[88] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV223ExtraWWContagemResultadoEmGarantiaDS_46_Contagemresultado_contratadacod3 > 0 ) && ( AV178ExtraWWContagemResultadoEmGarantiaDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV223ExtraWWContagemResultadoEmGarantiaDS_46_Contagemresultado_contratadacod3)";
         }
         else
         {
            GXv_int5[89] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3 + '%')";
         }
         else
         {
            GXv_int5[90] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV225ExtraWWContagemResultadoEmGarantiaDS_48_Contagemresultado_owner3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Owner] = @AV225ExtraWWContagemResultadoEmGarantiaDS_48_Contagemresultado_owner3)";
         }
         else
         {
            GXv_int5[91] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV228ExtraWWContagemResultadoEmGarantiaDS_51_Dynamicfiltersoperator4 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 or T1.[ContagemResultado_DemandaFM] = @AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4)";
         }
         else
         {
            GXv_int5[92] = 1;
            GXv_int5[93] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV228ExtraWWContagemResultadoEmGarantiaDS_51_Dynamicfiltersoperator4 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 or T1.[ContagemResultado_DemandaFM] like @lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4)";
         }
         else
         {
            GXv_int5[94] = 1;
            GXv_int5[95] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV228ExtraWWContagemResultadoEmGarantiaDS_51_Dynamicfiltersoperator4 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 or T1.[ContagemResultado_DemandaFM] like '%' + @lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4)";
         }
         else
         {
            GXv_int5[96] = 1;
            GXv_int5[97] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV230ExtraWWContagemResultadoEmGarantiaDS_53_Contagemresultado_datadmn4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV230ExtraWWContagemResultadoEmGarantiaDS_53_Contagemresultado_datadmn4)";
         }
         else
         {
            GXv_int5[98] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV231ExtraWWContagemResultadoEmGarantiaDS_54_Contagemresultado_datadmn_to4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV231ExtraWWContagemResultadoEmGarantiaDS_54_Contagemresultado_datadmn_to4)";
         }
         else
         {
            GXv_int5[99] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV234ExtraWWContagemResultadoEmGarantiaDS_57_Contagemresultado_dataprevista4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV234ExtraWWContagemResultadoEmGarantiaDS_57_Contagemresultado_dataprevista4)";
         }
         else
         {
            GXv_int5[100] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV235ExtraWWContagemResultadoEmGarantiaDS_58_Contagemresultado_dataprevista_to4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] <= @AV235ExtraWWContagemResultadoEmGarantiaDS_58_Contagemresultado_dataprevista_to4)";
         }
         else
         {
            GXv_int5[101] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4)) && ! ( StringUtil.StrCmp(AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4)";
         }
         else
         {
            GXv_int5[102] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L' or T1.[ContagemResultado_StatusDmn] = 'O'))";
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV237ExtraWWContagemResultadoEmGarantiaDS_60_Contagemresultado_servico4) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV237ExtraWWContagemResultadoEmGarantiaDS_60_Contagemresultado_servico4)";
         }
         else
         {
            GXv_int5[103] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV238ExtraWWContagemResultadoEmGarantiaDS_61_Contagemresultado_sistemacod4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV238ExtraWWContagemResultadoEmGarantiaDS_61_Contagemresultado_sistemacod4)";
         }
         else
         {
            GXv_int5[104] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV239ExtraWWContagemResultadoEmGarantiaDS_62_Contagemresultado_contratadacod4 > 0 ) && ( AV178ExtraWWContagemResultadoEmGarantiaDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV239ExtraWWContagemResultadoEmGarantiaDS_62_Contagemresultado_contratadacod4)";
         }
         else
         {
            GXv_int5[105] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4 + '%')";
         }
         else
         {
            GXv_int5[106] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV241ExtraWWContagemResultadoEmGarantiaDS_64_Contagemresultado_owner4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Owner] = @AV241ExtraWWContagemResultadoEmGarantiaDS_64_Contagemresultado_owner4)";
         }
         else
         {
            GXv_int5[107] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV244ExtraWWContagemResultadoEmGarantiaDS_67_Dynamicfiltersoperator5 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 or T1.[ContagemResultado_DemandaFM] = @AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5)";
         }
         else
         {
            GXv_int5[108] = 1;
            GXv_int5[109] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV244ExtraWWContagemResultadoEmGarantiaDS_67_Dynamicfiltersoperator5 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 or T1.[ContagemResultado_DemandaFM] like @lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5)";
         }
         else
         {
            GXv_int5[110] = 1;
            GXv_int5[111] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV244ExtraWWContagemResultadoEmGarantiaDS_67_Dynamicfiltersoperator5 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 or T1.[ContagemResultado_DemandaFM] like '%' + @lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5)";
         }
         else
         {
            GXv_int5[112] = 1;
            GXv_int5[113] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV246ExtraWWContagemResultadoEmGarantiaDS_69_Contagemresultado_datadmn5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV246ExtraWWContagemResultadoEmGarantiaDS_69_Contagemresultado_datadmn5)";
         }
         else
         {
            GXv_int5[114] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV247ExtraWWContagemResultadoEmGarantiaDS_70_Contagemresultado_datadmn_to5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV247ExtraWWContagemResultadoEmGarantiaDS_70_Contagemresultado_datadmn_to5)";
         }
         else
         {
            GXv_int5[115] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV250ExtraWWContagemResultadoEmGarantiaDS_73_Contagemresultado_dataprevista5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV250ExtraWWContagemResultadoEmGarantiaDS_73_Contagemresultado_dataprevista5)";
         }
         else
         {
            GXv_int5[116] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV251ExtraWWContagemResultadoEmGarantiaDS_74_Contagemresultado_dataprevista_to5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] <= @AV251ExtraWWContagemResultadoEmGarantiaDS_74_Contagemresultado_dataprevista_to5)";
         }
         else
         {
            GXv_int5[117] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5)) && ! ( StringUtil.StrCmp(AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5)";
         }
         else
         {
            GXv_int5[118] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L' or T1.[ContagemResultado_StatusDmn] = 'O'))";
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV253ExtraWWContagemResultadoEmGarantiaDS_76_Contagemresultado_servico5) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV253ExtraWWContagemResultadoEmGarantiaDS_76_Contagemresultado_servico5)";
         }
         else
         {
            GXv_int5[119] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV254ExtraWWContagemResultadoEmGarantiaDS_77_Contagemresultado_sistemacod5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV254ExtraWWContagemResultadoEmGarantiaDS_77_Contagemresultado_sistemacod5)";
         }
         else
         {
            GXv_int5[120] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV255ExtraWWContagemResultadoEmGarantiaDS_78_Contagemresultado_contratadacod5 > 0 ) && ( AV178ExtraWWContagemResultadoEmGarantiaDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV255ExtraWWContagemResultadoEmGarantiaDS_78_Contagemresultado_contratadacod5)";
         }
         else
         {
            GXv_int5[121] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5 + '%')";
         }
         else
         {
            GXv_int5[122] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV257ExtraWWContagemResultadoEmGarantiaDS_80_Contagemresultado_owner5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Owner] = @AV257ExtraWWContagemResultadoEmGarantiaDS_80_Contagemresultado_owner5)";
         }
         else
         {
            GXv_int5[123] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV259ExtraWWContagemResultadoEmGarantiaDS_82_Tfcontagemresultado_demandafm_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] like @lV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm)";
         }
         else
         {
            GXv_int5[124] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV259ExtraWWContagemResultadoEmGarantiaDS_82_Tfcontagemresultado_demandafm_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] = @AV259ExtraWWContagemResultadoEmGarantiaDS_82_Tfcontagemresultado_demandafm_sel)";
         }
         else
         {
            GXv_int5[125] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV261ExtraWWContagemResultadoEmGarantiaDS_84_Tfcontagemresultado_demanda_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda)";
         }
         else
         {
            GXv_int5[126] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV261ExtraWWContagemResultadoEmGarantiaDS_84_Tfcontagemresultado_demanda_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV261ExtraWWContagemResultadoEmGarantiaDS_84_Tfcontagemresultado_demanda_sel)";
         }
         else
         {
            GXv_int5[127] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV263ExtraWWContagemResultadoEmGarantiaDS_86_Tfcontagemresultado_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like @lV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao)";
         }
         else
         {
            GXv_int5[128] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV263ExtraWWContagemResultadoEmGarantiaDS_86_Tfcontagemresultado_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] = @AV263ExtraWWContagemResultadoEmGarantiaDS_86_Tfcontagemresultado_descricao_sel)";
         }
         else
         {
            GXv_int5[129] = 1;
         }
         if ( ! (DateTime.MinValue==AV264ExtraWWContagemResultadoEmGarantiaDS_87_Tfcontagemresultado_datadmn) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV264ExtraWWContagemResultadoEmGarantiaDS_87_Tfcontagemresultado_datadmn)";
         }
         else
         {
            GXv_int5[130] = 1;
         }
         if ( ! (DateTime.MinValue==AV265ExtraWWContagemResultadoEmGarantiaDS_88_Tfcontagemresultado_datadmn_to) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV265ExtraWWContagemResultadoEmGarantiaDS_88_Tfcontagemresultado_datadmn_to)";
         }
         else
         {
            GXv_int5[131] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV269ExtraWWContagemResultadoEmGarantiaDS_92_Tfcontagemresultado_contratadasigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Contratada_Sigla] like @lV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla)";
         }
         else
         {
            GXv_int5[132] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV269ExtraWWContagemResultadoEmGarantiaDS_92_Tfcontagemresultado_contratadasigla_sel)) )
         {
            sWhereString = sWhereString + " and (T5.[Contratada_Sigla] = @AV269ExtraWWContagemResultadoEmGarantiaDS_92_Tfcontagemresultado_contratadasigla_sel)";
         }
         else
         {
            GXv_int5[133] = 1;
         }
         if ( AV270ExtraWWContagemResultadoEmGarantiaDS_93_Tfcontagemresultado_statusdmn_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV270ExtraWWContagemResultadoEmGarantiaDS_93_Tfcontagemresultado_statusdmn_sels, "T1.[ContagemResultado_StatusDmn] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV272ExtraWWContagemResultadoEmGarantiaDS_95_Tfcontagemresultado_servicosigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Sigla] like @lV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla)";
         }
         else
         {
            GXv_int5[134] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV272ExtraWWContagemResultadoEmGarantiaDS_95_Tfcontagemresultado_servicosigla_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Sigla] = @AV272ExtraWWContagemResultadoEmGarantiaDS_95_Tfcontagemresultado_servicosigla_sel)";
         }
         else
         {
            GXv_int5[135] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Descricao]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      protected Object[] conditional_P00U19( IGxContext context ,
                                             String A484ContagemResultado_StatusDmn ,
                                             IGxCollection AV270ExtraWWContagemResultadoEmGarantiaDS_93_Tfcontagemresultado_statusdmn_sels ,
                                             int AV9WWPContext_gxTpr_Contratada_codigo ,
                                             String AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1 ,
                                             short AV180ExtraWWContagemResultadoEmGarantiaDS_3_Dynamicfiltersoperator1 ,
                                             String AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 ,
                                             DateTime AV182ExtraWWContagemResultadoEmGarantiaDS_5_Contagemresultado_datadmn1 ,
                                             DateTime AV183ExtraWWContagemResultadoEmGarantiaDS_6_Contagemresultado_datadmn_to1 ,
                                             DateTime AV186ExtraWWContagemResultadoEmGarantiaDS_9_Contagemresultado_dataprevista1 ,
                                             DateTime AV187ExtraWWContagemResultadoEmGarantiaDS_10_Contagemresultado_dataprevista_to1 ,
                                             String AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1 ,
                                             int AV189ExtraWWContagemResultadoEmGarantiaDS_12_Contagemresultado_servico1 ,
                                             int AV190ExtraWWContagemResultadoEmGarantiaDS_13_Contagemresultado_sistemacod1 ,
                                             int AV191ExtraWWContagemResultadoEmGarantiaDS_14_Contagemresultado_contratadacod1 ,
                                             int AV178ExtraWWContagemResultadoEmGarantiaDS_1_Contratada_areatrabalhocod ,
                                             String AV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1 ,
                                             int AV193ExtraWWContagemResultadoEmGarantiaDS_16_Contagemresultado_owner1 ,
                                             bool AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 ,
                                             String AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2 ,
                                             short AV196ExtraWWContagemResultadoEmGarantiaDS_19_Dynamicfiltersoperator2 ,
                                             String AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 ,
                                             DateTime AV198ExtraWWContagemResultadoEmGarantiaDS_21_Contagemresultado_datadmn2 ,
                                             DateTime AV199ExtraWWContagemResultadoEmGarantiaDS_22_Contagemresultado_datadmn_to2 ,
                                             DateTime AV202ExtraWWContagemResultadoEmGarantiaDS_25_Contagemresultado_dataprevista2 ,
                                             DateTime AV203ExtraWWContagemResultadoEmGarantiaDS_26_Contagemresultado_dataprevista_to2 ,
                                             String AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2 ,
                                             int AV205ExtraWWContagemResultadoEmGarantiaDS_28_Contagemresultado_servico2 ,
                                             int AV206ExtraWWContagemResultadoEmGarantiaDS_29_Contagemresultado_sistemacod2 ,
                                             int AV207ExtraWWContagemResultadoEmGarantiaDS_30_Contagemresultado_contratadacod2 ,
                                             String AV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2 ,
                                             int AV209ExtraWWContagemResultadoEmGarantiaDS_32_Contagemresultado_owner2 ,
                                             bool AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 ,
                                             String AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3 ,
                                             short AV212ExtraWWContagemResultadoEmGarantiaDS_35_Dynamicfiltersoperator3 ,
                                             String AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 ,
                                             DateTime AV214ExtraWWContagemResultadoEmGarantiaDS_37_Contagemresultado_datadmn3 ,
                                             DateTime AV215ExtraWWContagemResultadoEmGarantiaDS_38_Contagemresultado_datadmn_to3 ,
                                             DateTime AV218ExtraWWContagemResultadoEmGarantiaDS_41_Contagemresultado_dataprevista3 ,
                                             DateTime AV219ExtraWWContagemResultadoEmGarantiaDS_42_Contagemresultado_dataprevista_to3 ,
                                             String AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3 ,
                                             int AV221ExtraWWContagemResultadoEmGarantiaDS_44_Contagemresultado_servico3 ,
                                             int AV222ExtraWWContagemResultadoEmGarantiaDS_45_Contagemresultado_sistemacod3 ,
                                             int AV223ExtraWWContagemResultadoEmGarantiaDS_46_Contagemresultado_contratadacod3 ,
                                             String AV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3 ,
                                             int AV225ExtraWWContagemResultadoEmGarantiaDS_48_Contagemresultado_owner3 ,
                                             bool AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 ,
                                             String AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4 ,
                                             short AV228ExtraWWContagemResultadoEmGarantiaDS_51_Dynamicfiltersoperator4 ,
                                             String AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 ,
                                             DateTime AV230ExtraWWContagemResultadoEmGarantiaDS_53_Contagemresultado_datadmn4 ,
                                             DateTime AV231ExtraWWContagemResultadoEmGarantiaDS_54_Contagemresultado_datadmn_to4 ,
                                             DateTime AV234ExtraWWContagemResultadoEmGarantiaDS_57_Contagemresultado_dataprevista4 ,
                                             DateTime AV235ExtraWWContagemResultadoEmGarantiaDS_58_Contagemresultado_dataprevista_to4 ,
                                             String AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4 ,
                                             int AV237ExtraWWContagemResultadoEmGarantiaDS_60_Contagemresultado_servico4 ,
                                             int AV238ExtraWWContagemResultadoEmGarantiaDS_61_Contagemresultado_sistemacod4 ,
                                             int AV239ExtraWWContagemResultadoEmGarantiaDS_62_Contagemresultado_contratadacod4 ,
                                             String AV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4 ,
                                             int AV241ExtraWWContagemResultadoEmGarantiaDS_64_Contagemresultado_owner4 ,
                                             bool AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 ,
                                             String AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5 ,
                                             short AV244ExtraWWContagemResultadoEmGarantiaDS_67_Dynamicfiltersoperator5 ,
                                             String AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 ,
                                             DateTime AV246ExtraWWContagemResultadoEmGarantiaDS_69_Contagemresultado_datadmn5 ,
                                             DateTime AV247ExtraWWContagemResultadoEmGarantiaDS_70_Contagemresultado_datadmn_to5 ,
                                             DateTime AV250ExtraWWContagemResultadoEmGarantiaDS_73_Contagemresultado_dataprevista5 ,
                                             DateTime AV251ExtraWWContagemResultadoEmGarantiaDS_74_Contagemresultado_dataprevista_to5 ,
                                             String AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5 ,
                                             int AV253ExtraWWContagemResultadoEmGarantiaDS_76_Contagemresultado_servico5 ,
                                             int AV254ExtraWWContagemResultadoEmGarantiaDS_77_Contagemresultado_sistemacod5 ,
                                             int AV255ExtraWWContagemResultadoEmGarantiaDS_78_Contagemresultado_contratadacod5 ,
                                             String AV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5 ,
                                             int AV257ExtraWWContagemResultadoEmGarantiaDS_80_Contagemresultado_owner5 ,
                                             String AV259ExtraWWContagemResultadoEmGarantiaDS_82_Tfcontagemresultado_demandafm_sel ,
                                             String AV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm ,
                                             String AV261ExtraWWContagemResultadoEmGarantiaDS_84_Tfcontagemresultado_demanda_sel ,
                                             String AV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda ,
                                             String AV263ExtraWWContagemResultadoEmGarantiaDS_86_Tfcontagemresultado_descricao_sel ,
                                             String AV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao ,
                                             DateTime AV264ExtraWWContagemResultadoEmGarantiaDS_87_Tfcontagemresultado_datadmn ,
                                             DateTime AV265ExtraWWContagemResultadoEmGarantiaDS_88_Tfcontagemresultado_datadmn_to ,
                                             String AV269ExtraWWContagemResultadoEmGarantiaDS_92_Tfcontagemresultado_contratadasigla_sel ,
                                             String AV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla ,
                                             int AV270ExtraWWContagemResultadoEmGarantiaDS_93_Tfcontagemresultado_statusdmn_sels_Count ,
                                             String AV272ExtraWWContagemResultadoEmGarantiaDS_95_Tfcontagemresultado_servicosigla_sel ,
                                             String AV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             String A457ContagemResultado_Demanda ,
                                             String A493ContagemResultado_DemandaFM ,
                                             DateTime A471ContagemResultado_DataDmn ,
                                             DateTime A1351ContagemResultado_DataPrevista ,
                                             int A601ContagemResultado_Servico ,
                                             int A489ContagemResultado_SistemaCod ,
                                             String A494ContagemResultado_Descricao ,
                                             int A508ContagemResultado_Owner ,
                                             String A803ContagemResultado_ContratadaSigla ,
                                             String A801ContagemResultado_ServicoSigla ,
                                             DateTime AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1 ,
                                             DateTime A566ContagemResultado_DataUltCnt ,
                                             DateTime AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1 ,
                                             DateTime AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2 ,
                                             DateTime AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2 ,
                                             DateTime AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3 ,
                                             DateTime AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3 ,
                                             DateTime AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4 ,
                                             DateTime AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4 ,
                                             DateTime AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5 ,
                                             DateTime AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5 ,
                                             DateTime AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt ,
                                             DateTime AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int7 ;
         GXv_int7 = new short [136] ;
         Object[] GXv_Object8 ;
         GXv_Object8 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_Codigo], T5.[Contratada_AreaTrabalhoCod], T5.[Contratada_Sigla] AS ContagemResultado_ContratadaSigla, T3.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T1.[ContagemResultado_Owner], T1.[ContagemResultado_Descricao], T1.[ContagemResultado_SistemaCod], T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_DataPrevista], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt FROM (((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T4 ON T4.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Contratada] T5 WITH (NOLOCK) ON T5.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod])";
         scmdbuf = scmdbuf + " WHERE (Not ( @AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1))";
         scmdbuf = scmdbuf + " and (Not ( @AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1))";
         scmdbuf = scmdbuf + " and (Not ( @AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 = 1 and @AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2))";
         scmdbuf = scmdbuf + " and (Not ( @AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 = 1 and @AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2))";
         scmdbuf = scmdbuf + " and (Not ( @AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 = 1 and @AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3))";
         scmdbuf = scmdbuf + " and (Not ( @AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 = 1 and @AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3))";
         scmdbuf = scmdbuf + " and (Not ( @AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 = 1 and @AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4))";
         scmdbuf = scmdbuf + " and (Not ( @AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 = 1 and @AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4))";
         scmdbuf = scmdbuf + " and (Not ( @AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 = 1 and @AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5))";
         scmdbuf = scmdbuf + " and (Not ( @AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 = 1 and @AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5))";
         scmdbuf = scmdbuf + " and ((@AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt))";
         scmdbuf = scmdbuf + " and ((@AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to))";
         scmdbuf = scmdbuf + " and (T5.[Contratada_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( AV9WWPContext_gxTpr_Contratada_codigo > 0 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV9WWPCo_2Contratada_codigo)";
         }
         else
         {
            GXv_int7[43] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV180ExtraWWContagemResultadoEmGarantiaDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] = @AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int7[44] = 1;
            GXv_int7[45] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV180ExtraWWContagemResultadoEmGarantiaDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] like @lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int7[46] = 1;
            GXv_int7[47] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV180ExtraWWContagemResultadoEmGarantiaDS_3_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] like '%' + @lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int7[48] = 1;
            GXv_int7[49] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV182ExtraWWContagemResultadoEmGarantiaDS_5_Contagemresultado_datadmn1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV182ExtraWWContagemResultadoEmGarantiaDS_5_Contagemresultado_datadmn1)";
         }
         else
         {
            GXv_int7[50] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV183ExtraWWContagemResultadoEmGarantiaDS_6_Contagemresultado_datadmn_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV183ExtraWWContagemResultadoEmGarantiaDS_6_Contagemresultado_datadmn_to1)";
         }
         else
         {
            GXv_int7[51] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV186ExtraWWContagemResultadoEmGarantiaDS_9_Contagemresultado_dataprevista1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV186ExtraWWContagemResultadoEmGarantiaDS_9_Contagemresultado_dataprevista1)";
         }
         else
         {
            GXv_int7[52] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV187ExtraWWContagemResultadoEmGarantiaDS_10_Contagemresultado_dataprevista_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] <= @AV187ExtraWWContagemResultadoEmGarantiaDS_10_Contagemresultado_dataprevista_to1)";
         }
         else
         {
            GXv_int7[53] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1)) && ! ( StringUtil.StrCmp(AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1)";
         }
         else
         {
            GXv_int7[54] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L' or T1.[ContagemResultado_StatusDmn] = 'O'))";
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV189ExtraWWContagemResultadoEmGarantiaDS_12_Contagemresultado_servico1) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV189ExtraWWContagemResultadoEmGarantiaDS_12_Contagemresultado_servico1)";
         }
         else
         {
            GXv_int7[55] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV190ExtraWWContagemResultadoEmGarantiaDS_13_Contagemresultado_sistemacod1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV190ExtraWWContagemResultadoEmGarantiaDS_13_Contagemresultado_sistemacod1)";
         }
         else
         {
            GXv_int7[56] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV191ExtraWWContagemResultadoEmGarantiaDS_14_Contagemresultado_contratadacod1 > 0 ) && ( AV178ExtraWWContagemResultadoEmGarantiaDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV191ExtraWWContagemResultadoEmGarantiaDS_14_Contagemresultado_contratadacod1)";
         }
         else
         {
            GXv_int7[57] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1 + '%')";
         }
         else
         {
            GXv_int7[58] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV193ExtraWWContagemResultadoEmGarantiaDS_16_Contagemresultado_owner1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Owner] = @AV193ExtraWWContagemResultadoEmGarantiaDS_16_Contagemresultado_owner1)";
         }
         else
         {
            GXv_int7[59] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV196ExtraWWContagemResultadoEmGarantiaDS_19_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] = @AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int7[60] = 1;
            GXv_int7[61] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV196ExtraWWContagemResultadoEmGarantiaDS_19_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] like @lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int7[62] = 1;
            GXv_int7[63] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV196ExtraWWContagemResultadoEmGarantiaDS_19_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] like '%' + @lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int7[64] = 1;
            GXv_int7[65] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV198ExtraWWContagemResultadoEmGarantiaDS_21_Contagemresultado_datadmn2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV198ExtraWWContagemResultadoEmGarantiaDS_21_Contagemresultado_datadmn2)";
         }
         else
         {
            GXv_int7[66] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV199ExtraWWContagemResultadoEmGarantiaDS_22_Contagemresultado_datadmn_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV199ExtraWWContagemResultadoEmGarantiaDS_22_Contagemresultado_datadmn_to2)";
         }
         else
         {
            GXv_int7[67] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV202ExtraWWContagemResultadoEmGarantiaDS_25_Contagemresultado_dataprevista2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV202ExtraWWContagemResultadoEmGarantiaDS_25_Contagemresultado_dataprevista2)";
         }
         else
         {
            GXv_int7[68] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV203ExtraWWContagemResultadoEmGarantiaDS_26_Contagemresultado_dataprevista_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] <= @AV203ExtraWWContagemResultadoEmGarantiaDS_26_Contagemresultado_dataprevista_to2)";
         }
         else
         {
            GXv_int7[69] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2)) && ! ( StringUtil.StrCmp(AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2)";
         }
         else
         {
            GXv_int7[70] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L' or T1.[ContagemResultado_StatusDmn] = 'O'))";
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV205ExtraWWContagemResultadoEmGarantiaDS_28_Contagemresultado_servico2) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV205ExtraWWContagemResultadoEmGarantiaDS_28_Contagemresultado_servico2)";
         }
         else
         {
            GXv_int7[71] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV206ExtraWWContagemResultadoEmGarantiaDS_29_Contagemresultado_sistemacod2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV206ExtraWWContagemResultadoEmGarantiaDS_29_Contagemresultado_sistemacod2)";
         }
         else
         {
            GXv_int7[72] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV207ExtraWWContagemResultadoEmGarantiaDS_30_Contagemresultado_contratadacod2 > 0 ) && ( AV178ExtraWWContagemResultadoEmGarantiaDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV207ExtraWWContagemResultadoEmGarantiaDS_30_Contagemresultado_contratadacod2)";
         }
         else
         {
            GXv_int7[73] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2 + '%')";
         }
         else
         {
            GXv_int7[74] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV209ExtraWWContagemResultadoEmGarantiaDS_32_Contagemresultado_owner2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Owner] = @AV209ExtraWWContagemResultadoEmGarantiaDS_32_Contagemresultado_owner2)";
         }
         else
         {
            GXv_int7[75] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV212ExtraWWContagemResultadoEmGarantiaDS_35_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] = @AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int7[76] = 1;
            GXv_int7[77] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV212ExtraWWContagemResultadoEmGarantiaDS_35_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] like @lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int7[78] = 1;
            GXv_int7[79] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV212ExtraWWContagemResultadoEmGarantiaDS_35_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] like '%' + @lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int7[80] = 1;
            GXv_int7[81] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV214ExtraWWContagemResultadoEmGarantiaDS_37_Contagemresultado_datadmn3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV214ExtraWWContagemResultadoEmGarantiaDS_37_Contagemresultado_datadmn3)";
         }
         else
         {
            GXv_int7[82] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV215ExtraWWContagemResultadoEmGarantiaDS_38_Contagemresultado_datadmn_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV215ExtraWWContagemResultadoEmGarantiaDS_38_Contagemresultado_datadmn_to3)";
         }
         else
         {
            GXv_int7[83] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV218ExtraWWContagemResultadoEmGarantiaDS_41_Contagemresultado_dataprevista3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV218ExtraWWContagemResultadoEmGarantiaDS_41_Contagemresultado_dataprevista3)";
         }
         else
         {
            GXv_int7[84] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV219ExtraWWContagemResultadoEmGarantiaDS_42_Contagemresultado_dataprevista_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] <= @AV219ExtraWWContagemResultadoEmGarantiaDS_42_Contagemresultado_dataprevista_to3)";
         }
         else
         {
            GXv_int7[85] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3)) && ! ( StringUtil.StrCmp(AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3)";
         }
         else
         {
            GXv_int7[86] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L' or T1.[ContagemResultado_StatusDmn] = 'O'))";
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV221ExtraWWContagemResultadoEmGarantiaDS_44_Contagemresultado_servico3) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV221ExtraWWContagemResultadoEmGarantiaDS_44_Contagemresultado_servico3)";
         }
         else
         {
            GXv_int7[87] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV222ExtraWWContagemResultadoEmGarantiaDS_45_Contagemresultado_sistemacod3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV222ExtraWWContagemResultadoEmGarantiaDS_45_Contagemresultado_sistemacod3)";
         }
         else
         {
            GXv_int7[88] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV223ExtraWWContagemResultadoEmGarantiaDS_46_Contagemresultado_contratadacod3 > 0 ) && ( AV178ExtraWWContagemResultadoEmGarantiaDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV223ExtraWWContagemResultadoEmGarantiaDS_46_Contagemresultado_contratadacod3)";
         }
         else
         {
            GXv_int7[89] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3 + '%')";
         }
         else
         {
            GXv_int7[90] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV225ExtraWWContagemResultadoEmGarantiaDS_48_Contagemresultado_owner3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Owner] = @AV225ExtraWWContagemResultadoEmGarantiaDS_48_Contagemresultado_owner3)";
         }
         else
         {
            GXv_int7[91] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV228ExtraWWContagemResultadoEmGarantiaDS_51_Dynamicfiltersoperator4 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 or T1.[ContagemResultado_DemandaFM] = @AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4)";
         }
         else
         {
            GXv_int7[92] = 1;
            GXv_int7[93] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV228ExtraWWContagemResultadoEmGarantiaDS_51_Dynamicfiltersoperator4 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 or T1.[ContagemResultado_DemandaFM] like @lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4)";
         }
         else
         {
            GXv_int7[94] = 1;
            GXv_int7[95] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV228ExtraWWContagemResultadoEmGarantiaDS_51_Dynamicfiltersoperator4 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 or T1.[ContagemResultado_DemandaFM] like '%' + @lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4)";
         }
         else
         {
            GXv_int7[96] = 1;
            GXv_int7[97] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV230ExtraWWContagemResultadoEmGarantiaDS_53_Contagemresultado_datadmn4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV230ExtraWWContagemResultadoEmGarantiaDS_53_Contagemresultado_datadmn4)";
         }
         else
         {
            GXv_int7[98] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV231ExtraWWContagemResultadoEmGarantiaDS_54_Contagemresultado_datadmn_to4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV231ExtraWWContagemResultadoEmGarantiaDS_54_Contagemresultado_datadmn_to4)";
         }
         else
         {
            GXv_int7[99] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV234ExtraWWContagemResultadoEmGarantiaDS_57_Contagemresultado_dataprevista4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV234ExtraWWContagemResultadoEmGarantiaDS_57_Contagemresultado_dataprevista4)";
         }
         else
         {
            GXv_int7[100] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV235ExtraWWContagemResultadoEmGarantiaDS_58_Contagemresultado_dataprevista_to4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] <= @AV235ExtraWWContagemResultadoEmGarantiaDS_58_Contagemresultado_dataprevista_to4)";
         }
         else
         {
            GXv_int7[101] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4)) && ! ( StringUtil.StrCmp(AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4)";
         }
         else
         {
            GXv_int7[102] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L' or T1.[ContagemResultado_StatusDmn] = 'O'))";
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV237ExtraWWContagemResultadoEmGarantiaDS_60_Contagemresultado_servico4) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV237ExtraWWContagemResultadoEmGarantiaDS_60_Contagemresultado_servico4)";
         }
         else
         {
            GXv_int7[103] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV238ExtraWWContagemResultadoEmGarantiaDS_61_Contagemresultado_sistemacod4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV238ExtraWWContagemResultadoEmGarantiaDS_61_Contagemresultado_sistemacod4)";
         }
         else
         {
            GXv_int7[104] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV239ExtraWWContagemResultadoEmGarantiaDS_62_Contagemresultado_contratadacod4 > 0 ) && ( AV178ExtraWWContagemResultadoEmGarantiaDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV239ExtraWWContagemResultadoEmGarantiaDS_62_Contagemresultado_contratadacod4)";
         }
         else
         {
            GXv_int7[105] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4 + '%')";
         }
         else
         {
            GXv_int7[106] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV241ExtraWWContagemResultadoEmGarantiaDS_64_Contagemresultado_owner4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Owner] = @AV241ExtraWWContagemResultadoEmGarantiaDS_64_Contagemresultado_owner4)";
         }
         else
         {
            GXv_int7[107] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV244ExtraWWContagemResultadoEmGarantiaDS_67_Dynamicfiltersoperator5 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 or T1.[ContagemResultado_DemandaFM] = @AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5)";
         }
         else
         {
            GXv_int7[108] = 1;
            GXv_int7[109] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV244ExtraWWContagemResultadoEmGarantiaDS_67_Dynamicfiltersoperator5 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 or T1.[ContagemResultado_DemandaFM] like @lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5)";
         }
         else
         {
            GXv_int7[110] = 1;
            GXv_int7[111] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV244ExtraWWContagemResultadoEmGarantiaDS_67_Dynamicfiltersoperator5 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 or T1.[ContagemResultado_DemandaFM] like '%' + @lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5)";
         }
         else
         {
            GXv_int7[112] = 1;
            GXv_int7[113] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV246ExtraWWContagemResultadoEmGarantiaDS_69_Contagemresultado_datadmn5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV246ExtraWWContagemResultadoEmGarantiaDS_69_Contagemresultado_datadmn5)";
         }
         else
         {
            GXv_int7[114] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV247ExtraWWContagemResultadoEmGarantiaDS_70_Contagemresultado_datadmn_to5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV247ExtraWWContagemResultadoEmGarantiaDS_70_Contagemresultado_datadmn_to5)";
         }
         else
         {
            GXv_int7[115] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV250ExtraWWContagemResultadoEmGarantiaDS_73_Contagemresultado_dataprevista5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV250ExtraWWContagemResultadoEmGarantiaDS_73_Contagemresultado_dataprevista5)";
         }
         else
         {
            GXv_int7[116] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV251ExtraWWContagemResultadoEmGarantiaDS_74_Contagemresultado_dataprevista_to5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] <= @AV251ExtraWWContagemResultadoEmGarantiaDS_74_Contagemresultado_dataprevista_to5)";
         }
         else
         {
            GXv_int7[117] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5)) && ! ( StringUtil.StrCmp(AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5)";
         }
         else
         {
            GXv_int7[118] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L' or T1.[ContagemResultado_StatusDmn] = 'O'))";
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV253ExtraWWContagemResultadoEmGarantiaDS_76_Contagemresultado_servico5) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV253ExtraWWContagemResultadoEmGarantiaDS_76_Contagemresultado_servico5)";
         }
         else
         {
            GXv_int7[119] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV254ExtraWWContagemResultadoEmGarantiaDS_77_Contagemresultado_sistemacod5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV254ExtraWWContagemResultadoEmGarantiaDS_77_Contagemresultado_sistemacod5)";
         }
         else
         {
            GXv_int7[120] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV255ExtraWWContagemResultadoEmGarantiaDS_78_Contagemresultado_contratadacod5 > 0 ) && ( AV178ExtraWWContagemResultadoEmGarantiaDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV255ExtraWWContagemResultadoEmGarantiaDS_78_Contagemresultado_contratadacod5)";
         }
         else
         {
            GXv_int7[121] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5 + '%')";
         }
         else
         {
            GXv_int7[122] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV257ExtraWWContagemResultadoEmGarantiaDS_80_Contagemresultado_owner5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Owner] = @AV257ExtraWWContagemResultadoEmGarantiaDS_80_Contagemresultado_owner5)";
         }
         else
         {
            GXv_int7[123] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV259ExtraWWContagemResultadoEmGarantiaDS_82_Tfcontagemresultado_demandafm_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] like @lV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm)";
         }
         else
         {
            GXv_int7[124] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV259ExtraWWContagemResultadoEmGarantiaDS_82_Tfcontagemresultado_demandafm_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] = @AV259ExtraWWContagemResultadoEmGarantiaDS_82_Tfcontagemresultado_demandafm_sel)";
         }
         else
         {
            GXv_int7[125] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV261ExtraWWContagemResultadoEmGarantiaDS_84_Tfcontagemresultado_demanda_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda)";
         }
         else
         {
            GXv_int7[126] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV261ExtraWWContagemResultadoEmGarantiaDS_84_Tfcontagemresultado_demanda_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV261ExtraWWContagemResultadoEmGarantiaDS_84_Tfcontagemresultado_demanda_sel)";
         }
         else
         {
            GXv_int7[127] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV263ExtraWWContagemResultadoEmGarantiaDS_86_Tfcontagemresultado_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like @lV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao)";
         }
         else
         {
            GXv_int7[128] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV263ExtraWWContagemResultadoEmGarantiaDS_86_Tfcontagemresultado_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] = @AV263ExtraWWContagemResultadoEmGarantiaDS_86_Tfcontagemresultado_descricao_sel)";
         }
         else
         {
            GXv_int7[129] = 1;
         }
         if ( ! (DateTime.MinValue==AV264ExtraWWContagemResultadoEmGarantiaDS_87_Tfcontagemresultado_datadmn) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV264ExtraWWContagemResultadoEmGarantiaDS_87_Tfcontagemresultado_datadmn)";
         }
         else
         {
            GXv_int7[130] = 1;
         }
         if ( ! (DateTime.MinValue==AV265ExtraWWContagemResultadoEmGarantiaDS_88_Tfcontagemresultado_datadmn_to) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV265ExtraWWContagemResultadoEmGarantiaDS_88_Tfcontagemresultado_datadmn_to)";
         }
         else
         {
            GXv_int7[131] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV269ExtraWWContagemResultadoEmGarantiaDS_92_Tfcontagemresultado_contratadasigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Contratada_Sigla] like @lV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla)";
         }
         else
         {
            GXv_int7[132] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV269ExtraWWContagemResultadoEmGarantiaDS_92_Tfcontagemresultado_contratadasigla_sel)) )
         {
            sWhereString = sWhereString + " and (T5.[Contratada_Sigla] = @AV269ExtraWWContagemResultadoEmGarantiaDS_92_Tfcontagemresultado_contratadasigla_sel)";
         }
         else
         {
            GXv_int7[133] = 1;
         }
         if ( AV270ExtraWWContagemResultadoEmGarantiaDS_93_Tfcontagemresultado_statusdmn_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV270ExtraWWContagemResultadoEmGarantiaDS_93_Tfcontagemresultado_statusdmn_sels, "T1.[ContagemResultado_StatusDmn] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV272ExtraWWContagemResultadoEmGarantiaDS_95_Tfcontagemresultado_servicosigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Sigla] like @lV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla)";
         }
         else
         {
            GXv_int7[134] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV272ExtraWWContagemResultadoEmGarantiaDS_95_Tfcontagemresultado_servicosigla_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Sigla] = @AV272ExtraWWContagemResultadoEmGarantiaDS_95_Tfcontagemresultado_servicosigla_sel)";
         }
         else
         {
            GXv_int7[135] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T5.[Contratada_Sigla]";
         GXv_Object8[0] = scmdbuf;
         GXv_Object8[1] = GXv_int7;
         return GXv_Object8 ;
      }

      protected Object[] conditional_P00U111( IGxContext context ,
                                              String A484ContagemResultado_StatusDmn ,
                                              IGxCollection AV270ExtraWWContagemResultadoEmGarantiaDS_93_Tfcontagemresultado_statusdmn_sels ,
                                              int AV9WWPContext_gxTpr_Contratada_codigo ,
                                              String AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1 ,
                                              short AV180ExtraWWContagemResultadoEmGarantiaDS_3_Dynamicfiltersoperator1 ,
                                              String AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 ,
                                              DateTime AV182ExtraWWContagemResultadoEmGarantiaDS_5_Contagemresultado_datadmn1 ,
                                              DateTime AV183ExtraWWContagemResultadoEmGarantiaDS_6_Contagemresultado_datadmn_to1 ,
                                              DateTime AV186ExtraWWContagemResultadoEmGarantiaDS_9_Contagemresultado_dataprevista1 ,
                                              DateTime AV187ExtraWWContagemResultadoEmGarantiaDS_10_Contagemresultado_dataprevista_to1 ,
                                              String AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1 ,
                                              int AV189ExtraWWContagemResultadoEmGarantiaDS_12_Contagemresultado_servico1 ,
                                              int AV190ExtraWWContagemResultadoEmGarantiaDS_13_Contagemresultado_sistemacod1 ,
                                              int AV191ExtraWWContagemResultadoEmGarantiaDS_14_Contagemresultado_contratadacod1 ,
                                              int AV178ExtraWWContagemResultadoEmGarantiaDS_1_Contratada_areatrabalhocod ,
                                              String AV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1 ,
                                              int AV193ExtraWWContagemResultadoEmGarantiaDS_16_Contagemresultado_owner1 ,
                                              bool AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 ,
                                              String AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2 ,
                                              short AV196ExtraWWContagemResultadoEmGarantiaDS_19_Dynamicfiltersoperator2 ,
                                              String AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 ,
                                              DateTime AV198ExtraWWContagemResultadoEmGarantiaDS_21_Contagemresultado_datadmn2 ,
                                              DateTime AV199ExtraWWContagemResultadoEmGarantiaDS_22_Contagemresultado_datadmn_to2 ,
                                              DateTime AV202ExtraWWContagemResultadoEmGarantiaDS_25_Contagemresultado_dataprevista2 ,
                                              DateTime AV203ExtraWWContagemResultadoEmGarantiaDS_26_Contagemresultado_dataprevista_to2 ,
                                              String AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2 ,
                                              int AV205ExtraWWContagemResultadoEmGarantiaDS_28_Contagemresultado_servico2 ,
                                              int AV206ExtraWWContagemResultadoEmGarantiaDS_29_Contagemresultado_sistemacod2 ,
                                              int AV207ExtraWWContagemResultadoEmGarantiaDS_30_Contagemresultado_contratadacod2 ,
                                              String AV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2 ,
                                              int AV209ExtraWWContagemResultadoEmGarantiaDS_32_Contagemresultado_owner2 ,
                                              bool AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 ,
                                              String AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3 ,
                                              short AV212ExtraWWContagemResultadoEmGarantiaDS_35_Dynamicfiltersoperator3 ,
                                              String AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 ,
                                              DateTime AV214ExtraWWContagemResultadoEmGarantiaDS_37_Contagemresultado_datadmn3 ,
                                              DateTime AV215ExtraWWContagemResultadoEmGarantiaDS_38_Contagemresultado_datadmn_to3 ,
                                              DateTime AV218ExtraWWContagemResultadoEmGarantiaDS_41_Contagemresultado_dataprevista3 ,
                                              DateTime AV219ExtraWWContagemResultadoEmGarantiaDS_42_Contagemresultado_dataprevista_to3 ,
                                              String AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3 ,
                                              int AV221ExtraWWContagemResultadoEmGarantiaDS_44_Contagemresultado_servico3 ,
                                              int AV222ExtraWWContagemResultadoEmGarantiaDS_45_Contagemresultado_sistemacod3 ,
                                              int AV223ExtraWWContagemResultadoEmGarantiaDS_46_Contagemresultado_contratadacod3 ,
                                              String AV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3 ,
                                              int AV225ExtraWWContagemResultadoEmGarantiaDS_48_Contagemresultado_owner3 ,
                                              bool AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 ,
                                              String AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4 ,
                                              short AV228ExtraWWContagemResultadoEmGarantiaDS_51_Dynamicfiltersoperator4 ,
                                              String AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 ,
                                              DateTime AV230ExtraWWContagemResultadoEmGarantiaDS_53_Contagemresultado_datadmn4 ,
                                              DateTime AV231ExtraWWContagemResultadoEmGarantiaDS_54_Contagemresultado_datadmn_to4 ,
                                              DateTime AV234ExtraWWContagemResultadoEmGarantiaDS_57_Contagemresultado_dataprevista4 ,
                                              DateTime AV235ExtraWWContagemResultadoEmGarantiaDS_58_Contagemresultado_dataprevista_to4 ,
                                              String AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4 ,
                                              int AV237ExtraWWContagemResultadoEmGarantiaDS_60_Contagemresultado_servico4 ,
                                              int AV238ExtraWWContagemResultadoEmGarantiaDS_61_Contagemresultado_sistemacod4 ,
                                              int AV239ExtraWWContagemResultadoEmGarantiaDS_62_Contagemresultado_contratadacod4 ,
                                              String AV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4 ,
                                              int AV241ExtraWWContagemResultadoEmGarantiaDS_64_Contagemresultado_owner4 ,
                                              bool AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 ,
                                              String AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5 ,
                                              short AV244ExtraWWContagemResultadoEmGarantiaDS_67_Dynamicfiltersoperator5 ,
                                              String AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 ,
                                              DateTime AV246ExtraWWContagemResultadoEmGarantiaDS_69_Contagemresultado_datadmn5 ,
                                              DateTime AV247ExtraWWContagemResultadoEmGarantiaDS_70_Contagemresultado_datadmn_to5 ,
                                              DateTime AV250ExtraWWContagemResultadoEmGarantiaDS_73_Contagemresultado_dataprevista5 ,
                                              DateTime AV251ExtraWWContagemResultadoEmGarantiaDS_74_Contagemresultado_dataprevista_to5 ,
                                              String AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5 ,
                                              int AV253ExtraWWContagemResultadoEmGarantiaDS_76_Contagemresultado_servico5 ,
                                              int AV254ExtraWWContagemResultadoEmGarantiaDS_77_Contagemresultado_sistemacod5 ,
                                              int AV255ExtraWWContagemResultadoEmGarantiaDS_78_Contagemresultado_contratadacod5 ,
                                              String AV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5 ,
                                              int AV257ExtraWWContagemResultadoEmGarantiaDS_80_Contagemresultado_owner5 ,
                                              String AV259ExtraWWContagemResultadoEmGarantiaDS_82_Tfcontagemresultado_demandafm_sel ,
                                              String AV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm ,
                                              String AV261ExtraWWContagemResultadoEmGarantiaDS_84_Tfcontagemresultado_demanda_sel ,
                                              String AV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda ,
                                              String AV263ExtraWWContagemResultadoEmGarantiaDS_86_Tfcontagemresultado_descricao_sel ,
                                              String AV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao ,
                                              DateTime AV264ExtraWWContagemResultadoEmGarantiaDS_87_Tfcontagemresultado_datadmn ,
                                              DateTime AV265ExtraWWContagemResultadoEmGarantiaDS_88_Tfcontagemresultado_datadmn_to ,
                                              String AV269ExtraWWContagemResultadoEmGarantiaDS_92_Tfcontagemresultado_contratadasigla_sel ,
                                              String AV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla ,
                                              int AV270ExtraWWContagemResultadoEmGarantiaDS_93_Tfcontagemresultado_statusdmn_sels_Count ,
                                              String AV272ExtraWWContagemResultadoEmGarantiaDS_95_Tfcontagemresultado_servicosigla_sel ,
                                              String AV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla ,
                                              int A490ContagemResultado_ContratadaCod ,
                                              String A457ContagemResultado_Demanda ,
                                              String A493ContagemResultado_DemandaFM ,
                                              DateTime A471ContagemResultado_DataDmn ,
                                              DateTime A1351ContagemResultado_DataPrevista ,
                                              int A601ContagemResultado_Servico ,
                                              int A489ContagemResultado_SistemaCod ,
                                              String A494ContagemResultado_Descricao ,
                                              int A508ContagemResultado_Owner ,
                                              String A803ContagemResultado_ContratadaSigla ,
                                              String A801ContagemResultado_ServicoSigla ,
                                              DateTime AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1 ,
                                              DateTime A566ContagemResultado_DataUltCnt ,
                                              DateTime AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1 ,
                                              DateTime AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2 ,
                                              DateTime AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2 ,
                                              DateTime AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3 ,
                                              DateTime AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3 ,
                                              DateTime AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4 ,
                                              DateTime AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4 ,
                                              DateTime AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5 ,
                                              DateTime AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5 ,
                                              DateTime AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt ,
                                              DateTime AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to ,
                                              int A52Contratada_AreaTrabalhoCod ,
                                              int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int9 ;
         GXv_int9 = new short [136] ;
         Object[] GXv_Object10 ;
         GXv_Object10 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_Codigo], T5.[Contratada_AreaTrabalhoCod], T3.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T5.[Contratada_Sigla] AS ContagemResultado_ContratadaSigla, T1.[ContagemResultado_Owner], T1.[ContagemResultado_Descricao], T1.[ContagemResultado_SistemaCod], T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_DataPrevista], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt FROM (((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T4 ON T4.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Contratada] T5 WITH (NOLOCK) ON T5.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod])";
         scmdbuf = scmdbuf + " WHERE (Not ( @AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1))";
         scmdbuf = scmdbuf + " and (Not ( @AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1))";
         scmdbuf = scmdbuf + " and (Not ( @AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 = 1 and @AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2))";
         scmdbuf = scmdbuf + " and (Not ( @AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 = 1 and @AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2))";
         scmdbuf = scmdbuf + " and (Not ( @AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 = 1 and @AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3))";
         scmdbuf = scmdbuf + " and (Not ( @AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 = 1 and @AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3))";
         scmdbuf = scmdbuf + " and (Not ( @AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 = 1 and @AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4))";
         scmdbuf = scmdbuf + " and (Not ( @AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 = 1 and @AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4))";
         scmdbuf = scmdbuf + " and (Not ( @AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 = 1 and @AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5))";
         scmdbuf = scmdbuf + " and (Not ( @AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 = 1 and @AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5))";
         scmdbuf = scmdbuf + " and ((@AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt))";
         scmdbuf = scmdbuf + " and ((@AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T4.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to))";
         scmdbuf = scmdbuf + " and (T5.[Contratada_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( AV9WWPContext_gxTpr_Contratada_codigo > 0 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV9WWPCo_2Contratada_codigo)";
         }
         else
         {
            GXv_int9[43] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV180ExtraWWContagemResultadoEmGarantiaDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] = @AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int9[44] = 1;
            GXv_int9[45] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV180ExtraWWContagemResultadoEmGarantiaDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] like @lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int9[46] = 1;
            GXv_int9[47] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV180ExtraWWContagemResultadoEmGarantiaDS_3_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] like '%' + @lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int9[48] = 1;
            GXv_int9[49] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV182ExtraWWContagemResultadoEmGarantiaDS_5_Contagemresultado_datadmn1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV182ExtraWWContagemResultadoEmGarantiaDS_5_Contagemresultado_datadmn1)";
         }
         else
         {
            GXv_int9[50] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV183ExtraWWContagemResultadoEmGarantiaDS_6_Contagemresultado_datadmn_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV183ExtraWWContagemResultadoEmGarantiaDS_6_Contagemresultado_datadmn_to1)";
         }
         else
         {
            GXv_int9[51] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV186ExtraWWContagemResultadoEmGarantiaDS_9_Contagemresultado_dataprevista1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV186ExtraWWContagemResultadoEmGarantiaDS_9_Contagemresultado_dataprevista1)";
         }
         else
         {
            GXv_int9[52] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV187ExtraWWContagemResultadoEmGarantiaDS_10_Contagemresultado_dataprevista_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] <= @AV187ExtraWWContagemResultadoEmGarantiaDS_10_Contagemresultado_dataprevista_to1)";
         }
         else
         {
            GXv_int9[53] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1)) && ! ( StringUtil.StrCmp(AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1)";
         }
         else
         {
            GXv_int9[54] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L' or T1.[ContagemResultado_StatusDmn] = 'O'))";
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV189ExtraWWContagemResultadoEmGarantiaDS_12_Contagemresultado_servico1) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV189ExtraWWContagemResultadoEmGarantiaDS_12_Contagemresultado_servico1)";
         }
         else
         {
            GXv_int9[55] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV190ExtraWWContagemResultadoEmGarantiaDS_13_Contagemresultado_sistemacod1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV190ExtraWWContagemResultadoEmGarantiaDS_13_Contagemresultado_sistemacod1)";
         }
         else
         {
            GXv_int9[56] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV191ExtraWWContagemResultadoEmGarantiaDS_14_Contagemresultado_contratadacod1 > 0 ) && ( AV178ExtraWWContagemResultadoEmGarantiaDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV191ExtraWWContagemResultadoEmGarantiaDS_14_Contagemresultado_contratadacod1)";
         }
         else
         {
            GXv_int9[57] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1 + '%')";
         }
         else
         {
            GXv_int9[58] = 1;
         }
         if ( ( StringUtil.StrCmp(AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV193ExtraWWContagemResultadoEmGarantiaDS_16_Contagemresultado_owner1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Owner] = @AV193ExtraWWContagemResultadoEmGarantiaDS_16_Contagemresultado_owner1)";
         }
         else
         {
            GXv_int9[59] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV196ExtraWWContagemResultadoEmGarantiaDS_19_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] = @AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int9[60] = 1;
            GXv_int9[61] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV196ExtraWWContagemResultadoEmGarantiaDS_19_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] like @lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int9[62] = 1;
            GXv_int9[63] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV196ExtraWWContagemResultadoEmGarantiaDS_19_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] like '%' + @lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int9[64] = 1;
            GXv_int9[65] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV198ExtraWWContagemResultadoEmGarantiaDS_21_Contagemresultado_datadmn2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV198ExtraWWContagemResultadoEmGarantiaDS_21_Contagemresultado_datadmn2)";
         }
         else
         {
            GXv_int9[66] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV199ExtraWWContagemResultadoEmGarantiaDS_22_Contagemresultado_datadmn_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV199ExtraWWContagemResultadoEmGarantiaDS_22_Contagemresultado_datadmn_to2)";
         }
         else
         {
            GXv_int9[67] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV202ExtraWWContagemResultadoEmGarantiaDS_25_Contagemresultado_dataprevista2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV202ExtraWWContagemResultadoEmGarantiaDS_25_Contagemresultado_dataprevista2)";
         }
         else
         {
            GXv_int9[68] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV203ExtraWWContagemResultadoEmGarantiaDS_26_Contagemresultado_dataprevista_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] <= @AV203ExtraWWContagemResultadoEmGarantiaDS_26_Contagemresultado_dataprevista_to2)";
         }
         else
         {
            GXv_int9[69] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2)) && ! ( StringUtil.StrCmp(AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2)";
         }
         else
         {
            GXv_int9[70] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L' or T1.[ContagemResultado_StatusDmn] = 'O'))";
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV205ExtraWWContagemResultadoEmGarantiaDS_28_Contagemresultado_servico2) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV205ExtraWWContagemResultadoEmGarantiaDS_28_Contagemresultado_servico2)";
         }
         else
         {
            GXv_int9[71] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV206ExtraWWContagemResultadoEmGarantiaDS_29_Contagemresultado_sistemacod2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV206ExtraWWContagemResultadoEmGarantiaDS_29_Contagemresultado_sistemacod2)";
         }
         else
         {
            GXv_int9[72] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV207ExtraWWContagemResultadoEmGarantiaDS_30_Contagemresultado_contratadacod2 > 0 ) && ( AV178ExtraWWContagemResultadoEmGarantiaDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV207ExtraWWContagemResultadoEmGarantiaDS_30_Contagemresultado_contratadacod2)";
         }
         else
         {
            GXv_int9[73] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2 + '%')";
         }
         else
         {
            GXv_int9[74] = 1;
         }
         if ( AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV209ExtraWWContagemResultadoEmGarantiaDS_32_Contagemresultado_owner2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Owner] = @AV209ExtraWWContagemResultadoEmGarantiaDS_32_Contagemresultado_owner2)";
         }
         else
         {
            GXv_int9[75] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV212ExtraWWContagemResultadoEmGarantiaDS_35_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] = @AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int9[76] = 1;
            GXv_int9[77] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV212ExtraWWContagemResultadoEmGarantiaDS_35_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] like @lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int9[78] = 1;
            GXv_int9[79] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV212ExtraWWContagemResultadoEmGarantiaDS_35_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] like '%' + @lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int9[80] = 1;
            GXv_int9[81] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV214ExtraWWContagemResultadoEmGarantiaDS_37_Contagemresultado_datadmn3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV214ExtraWWContagemResultadoEmGarantiaDS_37_Contagemresultado_datadmn3)";
         }
         else
         {
            GXv_int9[82] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV215ExtraWWContagemResultadoEmGarantiaDS_38_Contagemresultado_datadmn_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV215ExtraWWContagemResultadoEmGarantiaDS_38_Contagemresultado_datadmn_to3)";
         }
         else
         {
            GXv_int9[83] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV218ExtraWWContagemResultadoEmGarantiaDS_41_Contagemresultado_dataprevista3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV218ExtraWWContagemResultadoEmGarantiaDS_41_Contagemresultado_dataprevista3)";
         }
         else
         {
            GXv_int9[84] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV219ExtraWWContagemResultadoEmGarantiaDS_42_Contagemresultado_dataprevista_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] <= @AV219ExtraWWContagemResultadoEmGarantiaDS_42_Contagemresultado_dataprevista_to3)";
         }
         else
         {
            GXv_int9[85] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3)) && ! ( StringUtil.StrCmp(AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3)";
         }
         else
         {
            GXv_int9[86] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L' or T1.[ContagemResultado_StatusDmn] = 'O'))";
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV221ExtraWWContagemResultadoEmGarantiaDS_44_Contagemresultado_servico3) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV221ExtraWWContagemResultadoEmGarantiaDS_44_Contagemresultado_servico3)";
         }
         else
         {
            GXv_int9[87] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV222ExtraWWContagemResultadoEmGarantiaDS_45_Contagemresultado_sistemacod3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV222ExtraWWContagemResultadoEmGarantiaDS_45_Contagemresultado_sistemacod3)";
         }
         else
         {
            GXv_int9[88] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV223ExtraWWContagemResultadoEmGarantiaDS_46_Contagemresultado_contratadacod3 > 0 ) && ( AV178ExtraWWContagemResultadoEmGarantiaDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV223ExtraWWContagemResultadoEmGarantiaDS_46_Contagemresultado_contratadacod3)";
         }
         else
         {
            GXv_int9[89] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3 + '%')";
         }
         else
         {
            GXv_int9[90] = 1;
         }
         if ( AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV225ExtraWWContagemResultadoEmGarantiaDS_48_Contagemresultado_owner3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Owner] = @AV225ExtraWWContagemResultadoEmGarantiaDS_48_Contagemresultado_owner3)";
         }
         else
         {
            GXv_int9[91] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV228ExtraWWContagemResultadoEmGarantiaDS_51_Dynamicfiltersoperator4 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 or T1.[ContagemResultado_DemandaFM] = @AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4)";
         }
         else
         {
            GXv_int9[92] = 1;
            GXv_int9[93] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV228ExtraWWContagemResultadoEmGarantiaDS_51_Dynamicfiltersoperator4 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 or T1.[ContagemResultado_DemandaFM] like @lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4)";
         }
         else
         {
            GXv_int9[94] = 1;
            GXv_int9[95] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV228ExtraWWContagemResultadoEmGarantiaDS_51_Dynamicfiltersoperator4 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4 or T1.[ContagemResultado_DemandaFM] like '%' + @lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4)";
         }
         else
         {
            GXv_int9[96] = 1;
            GXv_int9[97] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV230ExtraWWContagemResultadoEmGarantiaDS_53_Contagemresultado_datadmn4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV230ExtraWWContagemResultadoEmGarantiaDS_53_Contagemresultado_datadmn4)";
         }
         else
         {
            GXv_int9[98] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV231ExtraWWContagemResultadoEmGarantiaDS_54_Contagemresultado_datadmn_to4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV231ExtraWWContagemResultadoEmGarantiaDS_54_Contagemresultado_datadmn_to4)";
         }
         else
         {
            GXv_int9[99] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV234ExtraWWContagemResultadoEmGarantiaDS_57_Contagemresultado_dataprevista4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV234ExtraWWContagemResultadoEmGarantiaDS_57_Contagemresultado_dataprevista4)";
         }
         else
         {
            GXv_int9[100] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV235ExtraWWContagemResultadoEmGarantiaDS_58_Contagemresultado_dataprevista_to4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] <= @AV235ExtraWWContagemResultadoEmGarantiaDS_58_Contagemresultado_dataprevista_to4)";
         }
         else
         {
            GXv_int9[101] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4)) && ! ( StringUtil.StrCmp(AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4)";
         }
         else
         {
            GXv_int9[102] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L' or T1.[ContagemResultado_StatusDmn] = 'O'))";
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV237ExtraWWContagemResultadoEmGarantiaDS_60_Contagemresultado_servico4) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV237ExtraWWContagemResultadoEmGarantiaDS_60_Contagemresultado_servico4)";
         }
         else
         {
            GXv_int9[103] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV238ExtraWWContagemResultadoEmGarantiaDS_61_Contagemresultado_sistemacod4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV238ExtraWWContagemResultadoEmGarantiaDS_61_Contagemresultado_sistemacod4)";
         }
         else
         {
            GXv_int9[104] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV239ExtraWWContagemResultadoEmGarantiaDS_62_Contagemresultado_contratadacod4 > 0 ) && ( AV178ExtraWWContagemResultadoEmGarantiaDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV239ExtraWWContagemResultadoEmGarantiaDS_62_Contagemresultado_contratadacod4)";
         }
         else
         {
            GXv_int9[105] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4 + '%')";
         }
         else
         {
            GXv_int9[106] = 1;
         }
         if ( AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV241ExtraWWContagemResultadoEmGarantiaDS_64_Contagemresultado_owner4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Owner] = @AV241ExtraWWContagemResultadoEmGarantiaDS_64_Contagemresultado_owner4)";
         }
         else
         {
            GXv_int9[107] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV244ExtraWWContagemResultadoEmGarantiaDS_67_Dynamicfiltersoperator5 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 or T1.[ContagemResultado_DemandaFM] = @AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5)";
         }
         else
         {
            GXv_int9[108] = 1;
            GXv_int9[109] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV244ExtraWWContagemResultadoEmGarantiaDS_67_Dynamicfiltersoperator5 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 or T1.[ContagemResultado_DemandaFM] like @lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5)";
         }
         else
         {
            GXv_int9[110] = 1;
            GXv_int9[111] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV244ExtraWWContagemResultadoEmGarantiaDS_67_Dynamicfiltersoperator5 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5 or T1.[ContagemResultado_DemandaFM] like '%' + @lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5)";
         }
         else
         {
            GXv_int9[112] = 1;
            GXv_int9[113] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV246ExtraWWContagemResultadoEmGarantiaDS_69_Contagemresultado_datadmn5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV246ExtraWWContagemResultadoEmGarantiaDS_69_Contagemresultado_datadmn5)";
         }
         else
         {
            GXv_int9[114] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV247ExtraWWContagemResultadoEmGarantiaDS_70_Contagemresultado_datadmn_to5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV247ExtraWWContagemResultadoEmGarantiaDS_70_Contagemresultado_datadmn_to5)";
         }
         else
         {
            GXv_int9[115] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV250ExtraWWContagemResultadoEmGarantiaDS_73_Contagemresultado_dataprevista5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV250ExtraWWContagemResultadoEmGarantiaDS_73_Contagemresultado_dataprevista5)";
         }
         else
         {
            GXv_int9[116] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV251ExtraWWContagemResultadoEmGarantiaDS_74_Contagemresultado_dataprevista_to5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] <= @AV251ExtraWWContagemResultadoEmGarantiaDS_74_Contagemresultado_dataprevista_to5)";
         }
         else
         {
            GXv_int9[117] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5)) && ! ( StringUtil.StrCmp(AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5)";
         }
         else
         {
            GXv_int9[118] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L' or T1.[ContagemResultado_StatusDmn] = 'O'))";
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV253ExtraWWContagemResultadoEmGarantiaDS_76_Contagemresultado_servico5) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV253ExtraWWContagemResultadoEmGarantiaDS_76_Contagemresultado_servico5)";
         }
         else
         {
            GXv_int9[119] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV254ExtraWWContagemResultadoEmGarantiaDS_77_Contagemresultado_sistemacod5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV254ExtraWWContagemResultadoEmGarantiaDS_77_Contagemresultado_sistemacod5)";
         }
         else
         {
            GXv_int9[120] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV255ExtraWWContagemResultadoEmGarantiaDS_78_Contagemresultado_contratadacod5 > 0 ) && ( AV178ExtraWWContagemResultadoEmGarantiaDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV255ExtraWWContagemResultadoEmGarantiaDS_78_Contagemresultado_contratadacod5)";
         }
         else
         {
            GXv_int9[121] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5 + '%')";
         }
         else
         {
            GXv_int9[122] = 1;
         }
         if ( AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV257ExtraWWContagemResultadoEmGarantiaDS_80_Contagemresultado_owner5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Owner] = @AV257ExtraWWContagemResultadoEmGarantiaDS_80_Contagemresultado_owner5)";
         }
         else
         {
            GXv_int9[123] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV259ExtraWWContagemResultadoEmGarantiaDS_82_Tfcontagemresultado_demandafm_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] like @lV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm)";
         }
         else
         {
            GXv_int9[124] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV259ExtraWWContagemResultadoEmGarantiaDS_82_Tfcontagemresultado_demandafm_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] = @AV259ExtraWWContagemResultadoEmGarantiaDS_82_Tfcontagemresultado_demandafm_sel)";
         }
         else
         {
            GXv_int9[125] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV261ExtraWWContagemResultadoEmGarantiaDS_84_Tfcontagemresultado_demanda_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda)";
         }
         else
         {
            GXv_int9[126] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV261ExtraWWContagemResultadoEmGarantiaDS_84_Tfcontagemresultado_demanda_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV261ExtraWWContagemResultadoEmGarantiaDS_84_Tfcontagemresultado_demanda_sel)";
         }
         else
         {
            GXv_int9[127] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV263ExtraWWContagemResultadoEmGarantiaDS_86_Tfcontagemresultado_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like @lV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao)";
         }
         else
         {
            GXv_int9[128] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV263ExtraWWContagemResultadoEmGarantiaDS_86_Tfcontagemresultado_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] = @AV263ExtraWWContagemResultadoEmGarantiaDS_86_Tfcontagemresultado_descricao_sel)";
         }
         else
         {
            GXv_int9[129] = 1;
         }
         if ( ! (DateTime.MinValue==AV264ExtraWWContagemResultadoEmGarantiaDS_87_Tfcontagemresultado_datadmn) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV264ExtraWWContagemResultadoEmGarantiaDS_87_Tfcontagemresultado_datadmn)";
         }
         else
         {
            GXv_int9[130] = 1;
         }
         if ( ! (DateTime.MinValue==AV265ExtraWWContagemResultadoEmGarantiaDS_88_Tfcontagemresultado_datadmn_to) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV265ExtraWWContagemResultadoEmGarantiaDS_88_Tfcontagemresultado_datadmn_to)";
         }
         else
         {
            GXv_int9[131] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV269ExtraWWContagemResultadoEmGarantiaDS_92_Tfcontagemresultado_contratadasigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Contratada_Sigla] like @lV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla)";
         }
         else
         {
            GXv_int9[132] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV269ExtraWWContagemResultadoEmGarantiaDS_92_Tfcontagemresultado_contratadasigla_sel)) )
         {
            sWhereString = sWhereString + " and (T5.[Contratada_Sigla] = @AV269ExtraWWContagemResultadoEmGarantiaDS_92_Tfcontagemresultado_contratadasigla_sel)";
         }
         else
         {
            GXv_int9[133] = 1;
         }
         if ( AV270ExtraWWContagemResultadoEmGarantiaDS_93_Tfcontagemresultado_statusdmn_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV270ExtraWWContagemResultadoEmGarantiaDS_93_Tfcontagemresultado_statusdmn_sels, "T1.[ContagemResultado_StatusDmn] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV272ExtraWWContagemResultadoEmGarantiaDS_95_Tfcontagemresultado_servicosigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Sigla] like @lV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla)";
         }
         else
         {
            GXv_int9[134] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV272ExtraWWContagemResultadoEmGarantiaDS_95_Tfcontagemresultado_servicosigla_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Sigla] = @AV272ExtraWWContagemResultadoEmGarantiaDS_95_Tfcontagemresultado_servicosigla_sel)";
         }
         else
         {
            GXv_int9[135] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T3.[Servico_Sigla]";
         GXv_Object10[0] = scmdbuf;
         GXv_Object10[1] = GXv_int9;
         return GXv_Object10 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00U13(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (String)dynConstraints[3] , (short)dynConstraints[4] , (String)dynConstraints[5] , (DateTime)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (DateTime)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (bool)dynConstraints[17] , (String)dynConstraints[18] , (short)dynConstraints[19] , (String)dynConstraints[20] , (DateTime)dynConstraints[21] , (DateTime)dynConstraints[22] , (DateTime)dynConstraints[23] , (DateTime)dynConstraints[24] , (String)dynConstraints[25] , (int)dynConstraints[26] , (int)dynConstraints[27] , (int)dynConstraints[28] , (String)dynConstraints[29] , (int)dynConstraints[30] , (bool)dynConstraints[31] , (String)dynConstraints[32] , (short)dynConstraints[33] , (String)dynConstraints[34] , (DateTime)dynConstraints[35] , (DateTime)dynConstraints[36] , (DateTime)dynConstraints[37] , (DateTime)dynConstraints[38] , (String)dynConstraints[39] , (int)dynConstraints[40] , (int)dynConstraints[41] , (int)dynConstraints[42] , (String)dynConstraints[43] , (int)dynConstraints[44] , (bool)dynConstraints[45] , (String)dynConstraints[46] , (short)dynConstraints[47] , (String)dynConstraints[48] , (DateTime)dynConstraints[49] , (DateTime)dynConstraints[50] , (DateTime)dynConstraints[51] , (DateTime)dynConstraints[52] , (String)dynConstraints[53] , (int)dynConstraints[54] , (int)dynConstraints[55] , (int)dynConstraints[56] , (String)dynConstraints[57] , (int)dynConstraints[58] , (bool)dynConstraints[59] , (String)dynConstraints[60] , (short)dynConstraints[61] , (String)dynConstraints[62] , (DateTime)dynConstraints[63] , (DateTime)dynConstraints[64] , (DateTime)dynConstraints[65] , (DateTime)dynConstraints[66] , (String)dynConstraints[67] , (int)dynConstraints[68] , (int)dynConstraints[69] , (int)dynConstraints[70] , (String)dynConstraints[71] , (int)dynConstraints[72] , (String)dynConstraints[73] , (String)dynConstraints[74] , (String)dynConstraints[75] , (String)dynConstraints[76] , (String)dynConstraints[77] , (String)dynConstraints[78] , (DateTime)dynConstraints[79] , (DateTime)dynConstraints[80] , (String)dynConstraints[81] , (String)dynConstraints[82] , (int)dynConstraints[83] , (String)dynConstraints[84] , (String)dynConstraints[85] , (int)dynConstraints[86] , (String)dynConstraints[87] , (String)dynConstraints[88] , (DateTime)dynConstraints[89] , (DateTime)dynConstraints[90] , (int)dynConstraints[91] , (int)dynConstraints[92] , (String)dynConstraints[93] , (int)dynConstraints[94] , (String)dynConstraints[95] , (String)dynConstraints[96] , (DateTime)dynConstraints[97] , (DateTime)dynConstraints[98] , (DateTime)dynConstraints[99] , (DateTime)dynConstraints[100] , (DateTime)dynConstraints[101] , (DateTime)dynConstraints[102] , (DateTime)dynConstraints[103] , (DateTime)dynConstraints[104] , (DateTime)dynConstraints[105] , (DateTime)dynConstraints[106] , (DateTime)dynConstraints[107] , (DateTime)dynConstraints[108] , (DateTime)dynConstraints[109] , (int)dynConstraints[110] , (int)dynConstraints[111] );
               case 1 :
                     return conditional_P00U15(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (String)dynConstraints[3] , (short)dynConstraints[4] , (String)dynConstraints[5] , (DateTime)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (DateTime)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (bool)dynConstraints[17] , (String)dynConstraints[18] , (short)dynConstraints[19] , (String)dynConstraints[20] , (DateTime)dynConstraints[21] , (DateTime)dynConstraints[22] , (DateTime)dynConstraints[23] , (DateTime)dynConstraints[24] , (String)dynConstraints[25] , (int)dynConstraints[26] , (int)dynConstraints[27] , (int)dynConstraints[28] , (String)dynConstraints[29] , (int)dynConstraints[30] , (bool)dynConstraints[31] , (String)dynConstraints[32] , (short)dynConstraints[33] , (String)dynConstraints[34] , (DateTime)dynConstraints[35] , (DateTime)dynConstraints[36] , (DateTime)dynConstraints[37] , (DateTime)dynConstraints[38] , (String)dynConstraints[39] , (int)dynConstraints[40] , (int)dynConstraints[41] , (int)dynConstraints[42] , (String)dynConstraints[43] , (int)dynConstraints[44] , (bool)dynConstraints[45] , (String)dynConstraints[46] , (short)dynConstraints[47] , (String)dynConstraints[48] , (DateTime)dynConstraints[49] , (DateTime)dynConstraints[50] , (DateTime)dynConstraints[51] , (DateTime)dynConstraints[52] , (String)dynConstraints[53] , (int)dynConstraints[54] , (int)dynConstraints[55] , (int)dynConstraints[56] , (String)dynConstraints[57] , (int)dynConstraints[58] , (bool)dynConstraints[59] , (String)dynConstraints[60] , (short)dynConstraints[61] , (String)dynConstraints[62] , (DateTime)dynConstraints[63] , (DateTime)dynConstraints[64] , (DateTime)dynConstraints[65] , (DateTime)dynConstraints[66] , (String)dynConstraints[67] , (int)dynConstraints[68] , (int)dynConstraints[69] , (int)dynConstraints[70] , (String)dynConstraints[71] , (int)dynConstraints[72] , (String)dynConstraints[73] , (String)dynConstraints[74] , (String)dynConstraints[75] , (String)dynConstraints[76] , (String)dynConstraints[77] , (String)dynConstraints[78] , (DateTime)dynConstraints[79] , (DateTime)dynConstraints[80] , (String)dynConstraints[81] , (String)dynConstraints[82] , (int)dynConstraints[83] , (String)dynConstraints[84] , (String)dynConstraints[85] , (int)dynConstraints[86] , (String)dynConstraints[87] , (String)dynConstraints[88] , (DateTime)dynConstraints[89] , (DateTime)dynConstraints[90] , (int)dynConstraints[91] , (int)dynConstraints[92] , (String)dynConstraints[93] , (int)dynConstraints[94] , (String)dynConstraints[95] , (String)dynConstraints[96] , (DateTime)dynConstraints[97] , (DateTime)dynConstraints[98] , (DateTime)dynConstraints[99] , (DateTime)dynConstraints[100] , (DateTime)dynConstraints[101] , (DateTime)dynConstraints[102] , (DateTime)dynConstraints[103] , (DateTime)dynConstraints[104] , (DateTime)dynConstraints[105] , (DateTime)dynConstraints[106] , (DateTime)dynConstraints[107] , (DateTime)dynConstraints[108] , (DateTime)dynConstraints[109] , (int)dynConstraints[110] , (int)dynConstraints[111] );
               case 2 :
                     return conditional_P00U17(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (String)dynConstraints[3] , (short)dynConstraints[4] , (String)dynConstraints[5] , (DateTime)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (DateTime)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (bool)dynConstraints[17] , (String)dynConstraints[18] , (short)dynConstraints[19] , (String)dynConstraints[20] , (DateTime)dynConstraints[21] , (DateTime)dynConstraints[22] , (DateTime)dynConstraints[23] , (DateTime)dynConstraints[24] , (String)dynConstraints[25] , (int)dynConstraints[26] , (int)dynConstraints[27] , (int)dynConstraints[28] , (String)dynConstraints[29] , (int)dynConstraints[30] , (bool)dynConstraints[31] , (String)dynConstraints[32] , (short)dynConstraints[33] , (String)dynConstraints[34] , (DateTime)dynConstraints[35] , (DateTime)dynConstraints[36] , (DateTime)dynConstraints[37] , (DateTime)dynConstraints[38] , (String)dynConstraints[39] , (int)dynConstraints[40] , (int)dynConstraints[41] , (int)dynConstraints[42] , (String)dynConstraints[43] , (int)dynConstraints[44] , (bool)dynConstraints[45] , (String)dynConstraints[46] , (short)dynConstraints[47] , (String)dynConstraints[48] , (DateTime)dynConstraints[49] , (DateTime)dynConstraints[50] , (DateTime)dynConstraints[51] , (DateTime)dynConstraints[52] , (String)dynConstraints[53] , (int)dynConstraints[54] , (int)dynConstraints[55] , (int)dynConstraints[56] , (String)dynConstraints[57] , (int)dynConstraints[58] , (bool)dynConstraints[59] , (String)dynConstraints[60] , (short)dynConstraints[61] , (String)dynConstraints[62] , (DateTime)dynConstraints[63] , (DateTime)dynConstraints[64] , (DateTime)dynConstraints[65] , (DateTime)dynConstraints[66] , (String)dynConstraints[67] , (int)dynConstraints[68] , (int)dynConstraints[69] , (int)dynConstraints[70] , (String)dynConstraints[71] , (int)dynConstraints[72] , (String)dynConstraints[73] , (String)dynConstraints[74] , (String)dynConstraints[75] , (String)dynConstraints[76] , (String)dynConstraints[77] , (String)dynConstraints[78] , (DateTime)dynConstraints[79] , (DateTime)dynConstraints[80] , (String)dynConstraints[81] , (String)dynConstraints[82] , (int)dynConstraints[83] , (String)dynConstraints[84] , (String)dynConstraints[85] , (int)dynConstraints[86] , (String)dynConstraints[87] , (String)dynConstraints[88] , (DateTime)dynConstraints[89] , (DateTime)dynConstraints[90] , (int)dynConstraints[91] , (int)dynConstraints[92] , (String)dynConstraints[93] , (int)dynConstraints[94] , (String)dynConstraints[95] , (String)dynConstraints[96] , (DateTime)dynConstraints[97] , (DateTime)dynConstraints[98] , (DateTime)dynConstraints[99] , (DateTime)dynConstraints[100] , (DateTime)dynConstraints[101] , (DateTime)dynConstraints[102] , (DateTime)dynConstraints[103] , (DateTime)dynConstraints[104] , (DateTime)dynConstraints[105] , (DateTime)dynConstraints[106] , (DateTime)dynConstraints[107] , (DateTime)dynConstraints[108] , (DateTime)dynConstraints[109] , (int)dynConstraints[110] , (int)dynConstraints[111] );
               case 3 :
                     return conditional_P00U19(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (String)dynConstraints[3] , (short)dynConstraints[4] , (String)dynConstraints[5] , (DateTime)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (DateTime)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (bool)dynConstraints[17] , (String)dynConstraints[18] , (short)dynConstraints[19] , (String)dynConstraints[20] , (DateTime)dynConstraints[21] , (DateTime)dynConstraints[22] , (DateTime)dynConstraints[23] , (DateTime)dynConstraints[24] , (String)dynConstraints[25] , (int)dynConstraints[26] , (int)dynConstraints[27] , (int)dynConstraints[28] , (String)dynConstraints[29] , (int)dynConstraints[30] , (bool)dynConstraints[31] , (String)dynConstraints[32] , (short)dynConstraints[33] , (String)dynConstraints[34] , (DateTime)dynConstraints[35] , (DateTime)dynConstraints[36] , (DateTime)dynConstraints[37] , (DateTime)dynConstraints[38] , (String)dynConstraints[39] , (int)dynConstraints[40] , (int)dynConstraints[41] , (int)dynConstraints[42] , (String)dynConstraints[43] , (int)dynConstraints[44] , (bool)dynConstraints[45] , (String)dynConstraints[46] , (short)dynConstraints[47] , (String)dynConstraints[48] , (DateTime)dynConstraints[49] , (DateTime)dynConstraints[50] , (DateTime)dynConstraints[51] , (DateTime)dynConstraints[52] , (String)dynConstraints[53] , (int)dynConstraints[54] , (int)dynConstraints[55] , (int)dynConstraints[56] , (String)dynConstraints[57] , (int)dynConstraints[58] , (bool)dynConstraints[59] , (String)dynConstraints[60] , (short)dynConstraints[61] , (String)dynConstraints[62] , (DateTime)dynConstraints[63] , (DateTime)dynConstraints[64] , (DateTime)dynConstraints[65] , (DateTime)dynConstraints[66] , (String)dynConstraints[67] , (int)dynConstraints[68] , (int)dynConstraints[69] , (int)dynConstraints[70] , (String)dynConstraints[71] , (int)dynConstraints[72] , (String)dynConstraints[73] , (String)dynConstraints[74] , (String)dynConstraints[75] , (String)dynConstraints[76] , (String)dynConstraints[77] , (String)dynConstraints[78] , (DateTime)dynConstraints[79] , (DateTime)dynConstraints[80] , (String)dynConstraints[81] , (String)dynConstraints[82] , (int)dynConstraints[83] , (String)dynConstraints[84] , (String)dynConstraints[85] , (int)dynConstraints[86] , (String)dynConstraints[87] , (String)dynConstraints[88] , (DateTime)dynConstraints[89] , (DateTime)dynConstraints[90] , (int)dynConstraints[91] , (int)dynConstraints[92] , (String)dynConstraints[93] , (int)dynConstraints[94] , (String)dynConstraints[95] , (String)dynConstraints[96] , (DateTime)dynConstraints[97] , (DateTime)dynConstraints[98] , (DateTime)dynConstraints[99] , (DateTime)dynConstraints[100] , (DateTime)dynConstraints[101] , (DateTime)dynConstraints[102] , (DateTime)dynConstraints[103] , (DateTime)dynConstraints[104] , (DateTime)dynConstraints[105] , (DateTime)dynConstraints[106] , (DateTime)dynConstraints[107] , (DateTime)dynConstraints[108] , (DateTime)dynConstraints[109] , (int)dynConstraints[110] , (int)dynConstraints[111] );
               case 4 :
                     return conditional_P00U111(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (String)dynConstraints[3] , (short)dynConstraints[4] , (String)dynConstraints[5] , (DateTime)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (DateTime)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (bool)dynConstraints[17] , (String)dynConstraints[18] , (short)dynConstraints[19] , (String)dynConstraints[20] , (DateTime)dynConstraints[21] , (DateTime)dynConstraints[22] , (DateTime)dynConstraints[23] , (DateTime)dynConstraints[24] , (String)dynConstraints[25] , (int)dynConstraints[26] , (int)dynConstraints[27] , (int)dynConstraints[28] , (String)dynConstraints[29] , (int)dynConstraints[30] , (bool)dynConstraints[31] , (String)dynConstraints[32] , (short)dynConstraints[33] , (String)dynConstraints[34] , (DateTime)dynConstraints[35] , (DateTime)dynConstraints[36] , (DateTime)dynConstraints[37] , (DateTime)dynConstraints[38] , (String)dynConstraints[39] , (int)dynConstraints[40] , (int)dynConstraints[41] , (int)dynConstraints[42] , (String)dynConstraints[43] , (int)dynConstraints[44] , (bool)dynConstraints[45] , (String)dynConstraints[46] , (short)dynConstraints[47] , (String)dynConstraints[48] , (DateTime)dynConstraints[49] , (DateTime)dynConstraints[50] , (DateTime)dynConstraints[51] , (DateTime)dynConstraints[52] , (String)dynConstraints[53] , (int)dynConstraints[54] , (int)dynConstraints[55] , (int)dynConstraints[56] , (String)dynConstraints[57] , (int)dynConstraints[58] , (bool)dynConstraints[59] , (String)dynConstraints[60] , (short)dynConstraints[61] , (String)dynConstraints[62] , (DateTime)dynConstraints[63] , (DateTime)dynConstraints[64] , (DateTime)dynConstraints[65] , (DateTime)dynConstraints[66] , (String)dynConstraints[67] , (int)dynConstraints[68] , (int)dynConstraints[69] , (int)dynConstraints[70] , (String)dynConstraints[71] , (int)dynConstraints[72] , (String)dynConstraints[73] , (String)dynConstraints[74] , (String)dynConstraints[75] , (String)dynConstraints[76] , (String)dynConstraints[77] , (String)dynConstraints[78] , (DateTime)dynConstraints[79] , (DateTime)dynConstraints[80] , (String)dynConstraints[81] , (String)dynConstraints[82] , (int)dynConstraints[83] , (String)dynConstraints[84] , (String)dynConstraints[85] , (int)dynConstraints[86] , (String)dynConstraints[87] , (String)dynConstraints[88] , (DateTime)dynConstraints[89] , (DateTime)dynConstraints[90] , (int)dynConstraints[91] , (int)dynConstraints[92] , (String)dynConstraints[93] , (int)dynConstraints[94] , (String)dynConstraints[95] , (String)dynConstraints[96] , (DateTime)dynConstraints[97] , (DateTime)dynConstraints[98] , (DateTime)dynConstraints[99] , (DateTime)dynConstraints[100] , (DateTime)dynConstraints[101] , (DateTime)dynConstraints[102] , (DateTime)dynConstraints[103] , (DateTime)dynConstraints[104] , (DateTime)dynConstraints[105] , (DateTime)dynConstraints[106] , (DateTime)dynConstraints[107] , (DateTime)dynConstraints[108] , (DateTime)dynConstraints[109] , (int)dynConstraints[110] , (int)dynConstraints[111] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00U13 ;
          prmP00U13 = new Object[] {
          new Object[] {"@AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9WWPCo_2Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV182ExtraWWContagemResultadoEmGarantiaDS_5_Contagemresultado_datadmn1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV183ExtraWWContagemResultadoEmGarantiaDS_6_Contagemresultado_datadmn_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV186ExtraWWContagemResultadoEmGarantiaDS_9_Contagemresultado_dataprevista1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV187ExtraWWContagemResultadoEmGarantiaDS_10_Contagemresultado_dataprevista_to1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV189ExtraWWContagemResultadoEmGarantiaDS_12_Contagemresultado_servico1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV190ExtraWWContagemResultadoEmGarantiaDS_13_Contagemresultado_sistemacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV191ExtraWWContagemResultadoEmGarantiaDS_14_Contagemresultado_contratadacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV193ExtraWWContagemResultadoEmGarantiaDS_16_Contagemresultado_owner1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV198ExtraWWContagemResultadoEmGarantiaDS_21_Contagemresultado_datadmn2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV199ExtraWWContagemResultadoEmGarantiaDS_22_Contagemresultado_datadmn_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV202ExtraWWContagemResultadoEmGarantiaDS_25_Contagemresultado_dataprevista2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV203ExtraWWContagemResultadoEmGarantiaDS_26_Contagemresultado_dataprevista_to2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV205ExtraWWContagemResultadoEmGarantiaDS_28_Contagemresultado_servico2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV206ExtraWWContagemResultadoEmGarantiaDS_29_Contagemresultado_sistemacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV207ExtraWWContagemResultadoEmGarantiaDS_30_Contagemresultado_contratadacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV209ExtraWWContagemResultadoEmGarantiaDS_32_Contagemresultado_owner2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV214ExtraWWContagemResultadoEmGarantiaDS_37_Contagemresultado_datadmn3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV215ExtraWWContagemResultadoEmGarantiaDS_38_Contagemresultado_datadmn_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV218ExtraWWContagemResultadoEmGarantiaDS_41_Contagemresultado_dataprevista3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV219ExtraWWContagemResultadoEmGarantiaDS_42_Contagemresultado_dataprevista_to3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV221ExtraWWContagemResultadoEmGarantiaDS_44_Contagemresultado_servico3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV222ExtraWWContagemResultadoEmGarantiaDS_45_Contagemresultado_sistemacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV223ExtraWWContagemResultadoEmGarantiaDS_46_Contagemresultado_contratadacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV225ExtraWWContagemResultadoEmGarantiaDS_48_Contagemresultado_owner3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV230ExtraWWContagemResultadoEmGarantiaDS_53_Contagemresultado_datadmn4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV231ExtraWWContagemResultadoEmGarantiaDS_54_Contagemresultado_datadmn_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV234ExtraWWContagemResultadoEmGarantiaDS_57_Contagemresultado_dataprevista4",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV235ExtraWWContagemResultadoEmGarantiaDS_58_Contagemresultado_dataprevista_to4",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4",SqlDbType.Char,1,0} ,
          new Object[] {"@AV237ExtraWWContagemResultadoEmGarantiaDS_60_Contagemresultado_servico4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV238ExtraWWContagemResultadoEmGarantiaDS_61_Contagemresultado_sistemacod4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV239ExtraWWContagemResultadoEmGarantiaDS_62_Contagemresultado_contratadacod4",SqlDbType.Int,6,0} ,
          new Object[] {"@lV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV241ExtraWWContagemResultadoEmGarantiaDS_64_Contagemresultado_owner4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV246ExtraWWContagemResultadoEmGarantiaDS_69_Contagemresultado_datadmn5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV247ExtraWWContagemResultadoEmGarantiaDS_70_Contagemresultado_datadmn_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV250ExtraWWContagemResultadoEmGarantiaDS_73_Contagemresultado_dataprevista5",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV251ExtraWWContagemResultadoEmGarantiaDS_74_Contagemresultado_dataprevista_to5",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5",SqlDbType.Char,1,0} ,
          new Object[] {"@AV253ExtraWWContagemResultadoEmGarantiaDS_76_Contagemresultado_servico5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV254ExtraWWContagemResultadoEmGarantiaDS_77_Contagemresultado_sistemacod5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV255ExtraWWContagemResultadoEmGarantiaDS_78_Contagemresultado_contratadacod5",SqlDbType.Int,6,0} ,
          new Object[] {"@lV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV257ExtraWWContagemResultadoEmGarantiaDS_80_Contagemresultado_owner5",SqlDbType.Int,6,0} ,
          new Object[] {"@lV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV259ExtraWWContagemResultadoEmGarantiaDS_82_Tfcontagemresultado_demandafm_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV261ExtraWWContagemResultadoEmGarantiaDS_84_Tfcontagemresultado_demanda_sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV263ExtraWWContagemResultadoEmGarantiaDS_86_Tfcontagemresultado_descricao_sel",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV264ExtraWWContagemResultadoEmGarantiaDS_87_Tfcontagemresultado_datadmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV265ExtraWWContagemResultadoEmGarantiaDS_88_Tfcontagemresultado_datadmn_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV269ExtraWWContagemResultadoEmGarantiaDS_92_Tfcontagemresultado_contratadasigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV272ExtraWWContagemResultadoEmGarantiaDS_95_Tfcontagemresultado_servicosigla_sel",SqlDbType.Char,15,0}
          } ;
          Object[] prmP00U15 ;
          prmP00U15 = new Object[] {
          new Object[] {"@AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9WWPCo_2Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV182ExtraWWContagemResultadoEmGarantiaDS_5_Contagemresultado_datadmn1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV183ExtraWWContagemResultadoEmGarantiaDS_6_Contagemresultado_datadmn_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV186ExtraWWContagemResultadoEmGarantiaDS_9_Contagemresultado_dataprevista1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV187ExtraWWContagemResultadoEmGarantiaDS_10_Contagemresultado_dataprevista_to1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV189ExtraWWContagemResultadoEmGarantiaDS_12_Contagemresultado_servico1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV190ExtraWWContagemResultadoEmGarantiaDS_13_Contagemresultado_sistemacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV191ExtraWWContagemResultadoEmGarantiaDS_14_Contagemresultado_contratadacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV193ExtraWWContagemResultadoEmGarantiaDS_16_Contagemresultado_owner1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV198ExtraWWContagemResultadoEmGarantiaDS_21_Contagemresultado_datadmn2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV199ExtraWWContagemResultadoEmGarantiaDS_22_Contagemresultado_datadmn_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV202ExtraWWContagemResultadoEmGarantiaDS_25_Contagemresultado_dataprevista2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV203ExtraWWContagemResultadoEmGarantiaDS_26_Contagemresultado_dataprevista_to2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV205ExtraWWContagemResultadoEmGarantiaDS_28_Contagemresultado_servico2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV206ExtraWWContagemResultadoEmGarantiaDS_29_Contagemresultado_sistemacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV207ExtraWWContagemResultadoEmGarantiaDS_30_Contagemresultado_contratadacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV209ExtraWWContagemResultadoEmGarantiaDS_32_Contagemresultado_owner2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV214ExtraWWContagemResultadoEmGarantiaDS_37_Contagemresultado_datadmn3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV215ExtraWWContagemResultadoEmGarantiaDS_38_Contagemresultado_datadmn_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV218ExtraWWContagemResultadoEmGarantiaDS_41_Contagemresultado_dataprevista3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV219ExtraWWContagemResultadoEmGarantiaDS_42_Contagemresultado_dataprevista_to3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV221ExtraWWContagemResultadoEmGarantiaDS_44_Contagemresultado_servico3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV222ExtraWWContagemResultadoEmGarantiaDS_45_Contagemresultado_sistemacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV223ExtraWWContagemResultadoEmGarantiaDS_46_Contagemresultado_contratadacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV225ExtraWWContagemResultadoEmGarantiaDS_48_Contagemresultado_owner3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV230ExtraWWContagemResultadoEmGarantiaDS_53_Contagemresultado_datadmn4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV231ExtraWWContagemResultadoEmGarantiaDS_54_Contagemresultado_datadmn_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV234ExtraWWContagemResultadoEmGarantiaDS_57_Contagemresultado_dataprevista4",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV235ExtraWWContagemResultadoEmGarantiaDS_58_Contagemresultado_dataprevista_to4",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4",SqlDbType.Char,1,0} ,
          new Object[] {"@AV237ExtraWWContagemResultadoEmGarantiaDS_60_Contagemresultado_servico4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV238ExtraWWContagemResultadoEmGarantiaDS_61_Contagemresultado_sistemacod4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV239ExtraWWContagemResultadoEmGarantiaDS_62_Contagemresultado_contratadacod4",SqlDbType.Int,6,0} ,
          new Object[] {"@lV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV241ExtraWWContagemResultadoEmGarantiaDS_64_Contagemresultado_owner4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV246ExtraWWContagemResultadoEmGarantiaDS_69_Contagemresultado_datadmn5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV247ExtraWWContagemResultadoEmGarantiaDS_70_Contagemresultado_datadmn_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV250ExtraWWContagemResultadoEmGarantiaDS_73_Contagemresultado_dataprevista5",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV251ExtraWWContagemResultadoEmGarantiaDS_74_Contagemresultado_dataprevista_to5",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5",SqlDbType.Char,1,0} ,
          new Object[] {"@AV253ExtraWWContagemResultadoEmGarantiaDS_76_Contagemresultado_servico5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV254ExtraWWContagemResultadoEmGarantiaDS_77_Contagemresultado_sistemacod5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV255ExtraWWContagemResultadoEmGarantiaDS_78_Contagemresultado_contratadacod5",SqlDbType.Int,6,0} ,
          new Object[] {"@lV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV257ExtraWWContagemResultadoEmGarantiaDS_80_Contagemresultado_owner5",SqlDbType.Int,6,0} ,
          new Object[] {"@lV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV259ExtraWWContagemResultadoEmGarantiaDS_82_Tfcontagemresultado_demandafm_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV261ExtraWWContagemResultadoEmGarantiaDS_84_Tfcontagemresultado_demanda_sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV263ExtraWWContagemResultadoEmGarantiaDS_86_Tfcontagemresultado_descricao_sel",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV264ExtraWWContagemResultadoEmGarantiaDS_87_Tfcontagemresultado_datadmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV265ExtraWWContagemResultadoEmGarantiaDS_88_Tfcontagemresultado_datadmn_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV269ExtraWWContagemResultadoEmGarantiaDS_92_Tfcontagemresultado_contratadasigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV272ExtraWWContagemResultadoEmGarantiaDS_95_Tfcontagemresultado_servicosigla_sel",SqlDbType.Char,15,0}
          } ;
          Object[] prmP00U17 ;
          prmP00U17 = new Object[] {
          new Object[] {"@AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9WWPCo_2Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV182ExtraWWContagemResultadoEmGarantiaDS_5_Contagemresultado_datadmn1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV183ExtraWWContagemResultadoEmGarantiaDS_6_Contagemresultado_datadmn_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV186ExtraWWContagemResultadoEmGarantiaDS_9_Contagemresultado_dataprevista1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV187ExtraWWContagemResultadoEmGarantiaDS_10_Contagemresultado_dataprevista_to1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV189ExtraWWContagemResultadoEmGarantiaDS_12_Contagemresultado_servico1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV190ExtraWWContagemResultadoEmGarantiaDS_13_Contagemresultado_sistemacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV191ExtraWWContagemResultadoEmGarantiaDS_14_Contagemresultado_contratadacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV193ExtraWWContagemResultadoEmGarantiaDS_16_Contagemresultado_owner1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV198ExtraWWContagemResultadoEmGarantiaDS_21_Contagemresultado_datadmn2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV199ExtraWWContagemResultadoEmGarantiaDS_22_Contagemresultado_datadmn_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV202ExtraWWContagemResultadoEmGarantiaDS_25_Contagemresultado_dataprevista2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV203ExtraWWContagemResultadoEmGarantiaDS_26_Contagemresultado_dataprevista_to2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV205ExtraWWContagemResultadoEmGarantiaDS_28_Contagemresultado_servico2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV206ExtraWWContagemResultadoEmGarantiaDS_29_Contagemresultado_sistemacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV207ExtraWWContagemResultadoEmGarantiaDS_30_Contagemresultado_contratadacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV209ExtraWWContagemResultadoEmGarantiaDS_32_Contagemresultado_owner2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV214ExtraWWContagemResultadoEmGarantiaDS_37_Contagemresultado_datadmn3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV215ExtraWWContagemResultadoEmGarantiaDS_38_Contagemresultado_datadmn_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV218ExtraWWContagemResultadoEmGarantiaDS_41_Contagemresultado_dataprevista3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV219ExtraWWContagemResultadoEmGarantiaDS_42_Contagemresultado_dataprevista_to3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV221ExtraWWContagemResultadoEmGarantiaDS_44_Contagemresultado_servico3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV222ExtraWWContagemResultadoEmGarantiaDS_45_Contagemresultado_sistemacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV223ExtraWWContagemResultadoEmGarantiaDS_46_Contagemresultado_contratadacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV225ExtraWWContagemResultadoEmGarantiaDS_48_Contagemresultado_owner3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV230ExtraWWContagemResultadoEmGarantiaDS_53_Contagemresultado_datadmn4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV231ExtraWWContagemResultadoEmGarantiaDS_54_Contagemresultado_datadmn_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV234ExtraWWContagemResultadoEmGarantiaDS_57_Contagemresultado_dataprevista4",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV235ExtraWWContagemResultadoEmGarantiaDS_58_Contagemresultado_dataprevista_to4",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4",SqlDbType.Char,1,0} ,
          new Object[] {"@AV237ExtraWWContagemResultadoEmGarantiaDS_60_Contagemresultado_servico4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV238ExtraWWContagemResultadoEmGarantiaDS_61_Contagemresultado_sistemacod4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV239ExtraWWContagemResultadoEmGarantiaDS_62_Contagemresultado_contratadacod4",SqlDbType.Int,6,0} ,
          new Object[] {"@lV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV241ExtraWWContagemResultadoEmGarantiaDS_64_Contagemresultado_owner4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV246ExtraWWContagemResultadoEmGarantiaDS_69_Contagemresultado_datadmn5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV247ExtraWWContagemResultadoEmGarantiaDS_70_Contagemresultado_datadmn_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV250ExtraWWContagemResultadoEmGarantiaDS_73_Contagemresultado_dataprevista5",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV251ExtraWWContagemResultadoEmGarantiaDS_74_Contagemresultado_dataprevista_to5",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5",SqlDbType.Char,1,0} ,
          new Object[] {"@AV253ExtraWWContagemResultadoEmGarantiaDS_76_Contagemresultado_servico5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV254ExtraWWContagemResultadoEmGarantiaDS_77_Contagemresultado_sistemacod5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV255ExtraWWContagemResultadoEmGarantiaDS_78_Contagemresultado_contratadacod5",SqlDbType.Int,6,0} ,
          new Object[] {"@lV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV257ExtraWWContagemResultadoEmGarantiaDS_80_Contagemresultado_owner5",SqlDbType.Int,6,0} ,
          new Object[] {"@lV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV259ExtraWWContagemResultadoEmGarantiaDS_82_Tfcontagemresultado_demandafm_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV261ExtraWWContagemResultadoEmGarantiaDS_84_Tfcontagemresultado_demanda_sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV263ExtraWWContagemResultadoEmGarantiaDS_86_Tfcontagemresultado_descricao_sel",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV264ExtraWWContagemResultadoEmGarantiaDS_87_Tfcontagemresultado_datadmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV265ExtraWWContagemResultadoEmGarantiaDS_88_Tfcontagemresultado_datadmn_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV269ExtraWWContagemResultadoEmGarantiaDS_92_Tfcontagemresultado_contratadasigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV272ExtraWWContagemResultadoEmGarantiaDS_95_Tfcontagemresultado_servicosigla_sel",SqlDbType.Char,15,0}
          } ;
          Object[] prmP00U19 ;
          prmP00U19 = new Object[] {
          new Object[] {"@AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9WWPCo_2Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV182ExtraWWContagemResultadoEmGarantiaDS_5_Contagemresultado_datadmn1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV183ExtraWWContagemResultadoEmGarantiaDS_6_Contagemresultado_datadmn_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV186ExtraWWContagemResultadoEmGarantiaDS_9_Contagemresultado_dataprevista1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV187ExtraWWContagemResultadoEmGarantiaDS_10_Contagemresultado_dataprevista_to1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV189ExtraWWContagemResultadoEmGarantiaDS_12_Contagemresultado_servico1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV190ExtraWWContagemResultadoEmGarantiaDS_13_Contagemresultado_sistemacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV191ExtraWWContagemResultadoEmGarantiaDS_14_Contagemresultado_contratadacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV193ExtraWWContagemResultadoEmGarantiaDS_16_Contagemresultado_owner1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV198ExtraWWContagemResultadoEmGarantiaDS_21_Contagemresultado_datadmn2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV199ExtraWWContagemResultadoEmGarantiaDS_22_Contagemresultado_datadmn_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV202ExtraWWContagemResultadoEmGarantiaDS_25_Contagemresultado_dataprevista2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV203ExtraWWContagemResultadoEmGarantiaDS_26_Contagemresultado_dataprevista_to2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV205ExtraWWContagemResultadoEmGarantiaDS_28_Contagemresultado_servico2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV206ExtraWWContagemResultadoEmGarantiaDS_29_Contagemresultado_sistemacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV207ExtraWWContagemResultadoEmGarantiaDS_30_Contagemresultado_contratadacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV209ExtraWWContagemResultadoEmGarantiaDS_32_Contagemresultado_owner2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV214ExtraWWContagemResultadoEmGarantiaDS_37_Contagemresultado_datadmn3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV215ExtraWWContagemResultadoEmGarantiaDS_38_Contagemresultado_datadmn_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV218ExtraWWContagemResultadoEmGarantiaDS_41_Contagemresultado_dataprevista3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV219ExtraWWContagemResultadoEmGarantiaDS_42_Contagemresultado_dataprevista_to3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV221ExtraWWContagemResultadoEmGarantiaDS_44_Contagemresultado_servico3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV222ExtraWWContagemResultadoEmGarantiaDS_45_Contagemresultado_sistemacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV223ExtraWWContagemResultadoEmGarantiaDS_46_Contagemresultado_contratadacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV225ExtraWWContagemResultadoEmGarantiaDS_48_Contagemresultado_owner3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV230ExtraWWContagemResultadoEmGarantiaDS_53_Contagemresultado_datadmn4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV231ExtraWWContagemResultadoEmGarantiaDS_54_Contagemresultado_datadmn_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV234ExtraWWContagemResultadoEmGarantiaDS_57_Contagemresultado_dataprevista4",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV235ExtraWWContagemResultadoEmGarantiaDS_58_Contagemresultado_dataprevista_to4",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4",SqlDbType.Char,1,0} ,
          new Object[] {"@AV237ExtraWWContagemResultadoEmGarantiaDS_60_Contagemresultado_servico4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV238ExtraWWContagemResultadoEmGarantiaDS_61_Contagemresultado_sistemacod4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV239ExtraWWContagemResultadoEmGarantiaDS_62_Contagemresultado_contratadacod4",SqlDbType.Int,6,0} ,
          new Object[] {"@lV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV241ExtraWWContagemResultadoEmGarantiaDS_64_Contagemresultado_owner4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV246ExtraWWContagemResultadoEmGarantiaDS_69_Contagemresultado_datadmn5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV247ExtraWWContagemResultadoEmGarantiaDS_70_Contagemresultado_datadmn_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV250ExtraWWContagemResultadoEmGarantiaDS_73_Contagemresultado_dataprevista5",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV251ExtraWWContagemResultadoEmGarantiaDS_74_Contagemresultado_dataprevista_to5",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5",SqlDbType.Char,1,0} ,
          new Object[] {"@AV253ExtraWWContagemResultadoEmGarantiaDS_76_Contagemresultado_servico5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV254ExtraWWContagemResultadoEmGarantiaDS_77_Contagemresultado_sistemacod5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV255ExtraWWContagemResultadoEmGarantiaDS_78_Contagemresultado_contratadacod5",SqlDbType.Int,6,0} ,
          new Object[] {"@lV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV257ExtraWWContagemResultadoEmGarantiaDS_80_Contagemresultado_owner5",SqlDbType.Int,6,0} ,
          new Object[] {"@lV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV259ExtraWWContagemResultadoEmGarantiaDS_82_Tfcontagemresultado_demandafm_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV261ExtraWWContagemResultadoEmGarantiaDS_84_Tfcontagemresultado_demanda_sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV263ExtraWWContagemResultadoEmGarantiaDS_86_Tfcontagemresultado_descricao_sel",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV264ExtraWWContagemResultadoEmGarantiaDS_87_Tfcontagemresultado_datadmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV265ExtraWWContagemResultadoEmGarantiaDS_88_Tfcontagemresultado_datadmn_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV269ExtraWWContagemResultadoEmGarantiaDS_92_Tfcontagemresultado_contratadasigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV272ExtraWWContagemResultadoEmGarantiaDS_95_Tfcontagemresultado_servicosigla_sel",SqlDbType.Char,15,0}
          } ;
          Object[] prmP00U111 ;
          prmP00U111 = new Object[] {
          new Object[] {"@AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV184ExtraWWContagemResultadoEmGarantiaDS_7_Contagemresultado_datacnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV179ExtraWWContagemResultadoEmGarantiaDS_2_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV185ExtraWWContagemResultadoEmGarantiaDS_8_Contagemresultado_datacnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV200ExtraWWContagemResultadoEmGarantiaDS_23_Contagemresultado_datacnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV194ExtraWWContagemResultadoEmGarantiaDS_17_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV195ExtraWWContagemResultadoEmGarantiaDS_18_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV201ExtraWWContagemResultadoEmGarantiaDS_24_Contagemresultado_datacnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV216ExtraWWContagemResultadoEmGarantiaDS_39_Contagemresultado_datacnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV210ExtraWWContagemResultadoEmGarantiaDS_33_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV211ExtraWWContagemResultadoEmGarantiaDS_34_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV217ExtraWWContagemResultadoEmGarantiaDS_40_Contagemresultado_datacnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV232ExtraWWContagemResultadoEmGarantiaDS_55_Contagemresultado_datacnt4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV226ExtraWWContagemResultadoEmGarantiaDS_49_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV227ExtraWWContagemResultadoEmGarantiaDS_50_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV233ExtraWWContagemResultadoEmGarantiaDS_56_Contagemresultado_datacnt_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV248ExtraWWContagemResultadoEmGarantiaDS_71_Contagemresultado_datacnt5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV242ExtraWWContagemResultadoEmGarantiaDS_65_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV243ExtraWWContagemResultadoEmGarantiaDS_66_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV249ExtraWWContagemResultadoEmGarantiaDS_72_Contagemresultado_datacnt_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV266ExtraWWContagemResultadoEmGarantiaDS_89_Tfcontagemresultado_dataultcnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV267ExtraWWContagemResultadoEmGarantiaDS_90_Tfcontagemresultado_dataultcnt_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9WWPCo_2Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV181ExtraWWContagemResultadoEmGarantiaDS_4_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV182ExtraWWContagemResultadoEmGarantiaDS_5_Contagemresultado_datadmn1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV183ExtraWWContagemResultadoEmGarantiaDS_6_Contagemresultado_datadmn_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV186ExtraWWContagemResultadoEmGarantiaDS_9_Contagemresultado_dataprevista1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV187ExtraWWContagemResultadoEmGarantiaDS_10_Contagemresultado_dataprevista_to1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV188ExtraWWContagemResultadoEmGarantiaDS_11_Contagemresultado_statusdmn1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV189ExtraWWContagemResultadoEmGarantiaDS_12_Contagemresultado_servico1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV190ExtraWWContagemResultadoEmGarantiaDS_13_Contagemresultado_sistemacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV191ExtraWWContagemResultadoEmGarantiaDS_14_Contagemresultado_contratadacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV192ExtraWWContagemResultadoEmGarantiaDS_15_Contagemresultado_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV193ExtraWWContagemResultadoEmGarantiaDS_16_Contagemresultado_owner1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV197ExtraWWContagemResultadoEmGarantiaDS_20_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV198ExtraWWContagemResultadoEmGarantiaDS_21_Contagemresultado_datadmn2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV199ExtraWWContagemResultadoEmGarantiaDS_22_Contagemresultado_datadmn_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV202ExtraWWContagemResultadoEmGarantiaDS_25_Contagemresultado_dataprevista2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV203ExtraWWContagemResultadoEmGarantiaDS_26_Contagemresultado_dataprevista_to2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV204ExtraWWContagemResultadoEmGarantiaDS_27_Contagemresultado_statusdmn2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV205ExtraWWContagemResultadoEmGarantiaDS_28_Contagemresultado_servico2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV206ExtraWWContagemResultadoEmGarantiaDS_29_Contagemresultado_sistemacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV207ExtraWWContagemResultadoEmGarantiaDS_30_Contagemresultado_contratadacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV208ExtraWWContagemResultadoEmGarantiaDS_31_Contagemresultado_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV209ExtraWWContagemResultadoEmGarantiaDS_32_Contagemresultado_owner2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV213ExtraWWContagemResultadoEmGarantiaDS_36_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV214ExtraWWContagemResultadoEmGarantiaDS_37_Contagemresultado_datadmn3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV215ExtraWWContagemResultadoEmGarantiaDS_38_Contagemresultado_datadmn_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV218ExtraWWContagemResultadoEmGarantiaDS_41_Contagemresultado_dataprevista3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV219ExtraWWContagemResultadoEmGarantiaDS_42_Contagemresultado_dataprevista_to3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV220ExtraWWContagemResultadoEmGarantiaDS_43_Contagemresultado_statusdmn3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV221ExtraWWContagemResultadoEmGarantiaDS_44_Contagemresultado_servico3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV222ExtraWWContagemResultadoEmGarantiaDS_45_Contagemresultado_sistemacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV223ExtraWWContagemResultadoEmGarantiaDS_46_Contagemresultado_contratadacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV224ExtraWWContagemResultadoEmGarantiaDS_47_Contagemresultado_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV225ExtraWWContagemResultadoEmGarantiaDS_48_Contagemresultado_owner3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV229ExtraWWContagemResultadoEmGarantiaDS_52_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV230ExtraWWContagemResultadoEmGarantiaDS_53_Contagemresultado_datadmn4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV231ExtraWWContagemResultadoEmGarantiaDS_54_Contagemresultado_datadmn_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV234ExtraWWContagemResultadoEmGarantiaDS_57_Contagemresultado_dataprevista4",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV235ExtraWWContagemResultadoEmGarantiaDS_58_Contagemresultado_dataprevista_to4",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV236ExtraWWContagemResultadoEmGarantiaDS_59_Contagemresultado_statusdmn4",SqlDbType.Char,1,0} ,
          new Object[] {"@AV237ExtraWWContagemResultadoEmGarantiaDS_60_Contagemresultado_servico4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV238ExtraWWContagemResultadoEmGarantiaDS_61_Contagemresultado_sistemacod4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV239ExtraWWContagemResultadoEmGarantiaDS_62_Contagemresultado_contratadacod4",SqlDbType.Int,6,0} ,
          new Object[] {"@lV240ExtraWWContagemResultadoEmGarantiaDS_63_Contagemresultado_descricao4",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV241ExtraWWContagemResultadoEmGarantiaDS_64_Contagemresultado_owner4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV245ExtraWWContagemResultadoEmGarantiaDS_68_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV246ExtraWWContagemResultadoEmGarantiaDS_69_Contagemresultado_datadmn5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV247ExtraWWContagemResultadoEmGarantiaDS_70_Contagemresultado_datadmn_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV250ExtraWWContagemResultadoEmGarantiaDS_73_Contagemresultado_dataprevista5",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV251ExtraWWContagemResultadoEmGarantiaDS_74_Contagemresultado_dataprevista_to5",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV252ExtraWWContagemResultadoEmGarantiaDS_75_Contagemresultado_statusdmn5",SqlDbType.Char,1,0} ,
          new Object[] {"@AV253ExtraWWContagemResultadoEmGarantiaDS_76_Contagemresultado_servico5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV254ExtraWWContagemResultadoEmGarantiaDS_77_Contagemresultado_sistemacod5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV255ExtraWWContagemResultadoEmGarantiaDS_78_Contagemresultado_contratadacod5",SqlDbType.Int,6,0} ,
          new Object[] {"@lV256ExtraWWContagemResultadoEmGarantiaDS_79_Contagemresultado_descricao5",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV257ExtraWWContagemResultadoEmGarantiaDS_80_Contagemresultado_owner5",SqlDbType.Int,6,0} ,
          new Object[] {"@lV258ExtraWWContagemResultadoEmGarantiaDS_81_Tfcontagemresultado_demandafm",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV259ExtraWWContagemResultadoEmGarantiaDS_82_Tfcontagemresultado_demandafm_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV260ExtraWWContagemResultadoEmGarantiaDS_83_Tfcontagemresultado_demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV261ExtraWWContagemResultadoEmGarantiaDS_84_Tfcontagemresultado_demanda_sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV262ExtraWWContagemResultadoEmGarantiaDS_85_Tfcontagemresultado_descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV263ExtraWWContagemResultadoEmGarantiaDS_86_Tfcontagemresultado_descricao_sel",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV264ExtraWWContagemResultadoEmGarantiaDS_87_Tfcontagemresultado_datadmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV265ExtraWWContagemResultadoEmGarantiaDS_88_Tfcontagemresultado_datadmn_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV268ExtraWWContagemResultadoEmGarantiaDS_91_Tfcontagemresultado_contratadasigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV269ExtraWWContagemResultadoEmGarantiaDS_92_Tfcontagemresultado_contratadasigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV271ExtraWWContagemResultadoEmGarantiaDS_94_Tfcontagemresultado_servicosigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV272ExtraWWContagemResultadoEmGarantiaDS_95_Tfcontagemresultado_servicosigla_sel",SqlDbType.Char,15,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00U13", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00U13,100,0,true,false )
             ,new CursorDef("P00U15", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00U15,100,0,true,false )
             ,new CursorDef("P00U17", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00U17,100,0,true,false )
             ,new CursorDef("P00U19", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00U19,100,0,true,false )
             ,new CursorDef("P00U111", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00U111,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((String[]) buf[12])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[20])[0] = rslt.getGXDateTime(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((DateTime[]) buf[22])[0] = rslt.getGXDate(13) ;
                ((String[]) buf[23])[0] = rslt.getVarchar(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((int[]) buf[25])[0] = rslt.getInt(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((DateTime[]) buf[27])[0] = rslt.getGXDate(16) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((String[]) buf[10])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getString(10, 1) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((DateTime[]) buf[18])[0] = rslt.getGXDateTime(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[20])[0] = rslt.getGXDate(12) ;
                ((String[]) buf[21])[0] = rslt.getVarchar(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((int[]) buf[23])[0] = rslt.getInt(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((int[]) buf[25])[0] = rslt.getInt(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((DateTime[]) buf[27])[0] = rslt.getGXDate(16) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getString(10, 1) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((DateTime[]) buf[18])[0] = rslt.getGXDateTime(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[20])[0] = rslt.getGXDate(12) ;
                ((String[]) buf[21])[0] = rslt.getVarchar(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((String[]) buf[23])[0] = rslt.getVarchar(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((int[]) buf[25])[0] = rslt.getInt(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((DateTime[]) buf[27])[0] = rslt.getGXDate(16) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((String[]) buf[10])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getString(10, 1) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((DateTime[]) buf[18])[0] = rslt.getGXDateTime(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[20])[0] = rslt.getGXDate(12) ;
                ((String[]) buf[21])[0] = rslt.getVarchar(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((String[]) buf[23])[0] = rslt.getVarchar(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((int[]) buf[25])[0] = rslt.getInt(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((DateTime[]) buf[27])[0] = rslt.getGXDate(16) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((String[]) buf[10])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getString(10, 1) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((DateTime[]) buf[18])[0] = rslt.getGXDateTime(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[20])[0] = rslt.getGXDate(12) ;
                ((String[]) buf[21])[0] = rslt.getVarchar(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((String[]) buf[23])[0] = rslt.getVarchar(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((int[]) buf[25])[0] = rslt.getInt(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((DateTime[]) buf[27])[0] = rslt.getGXDate(16) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[136]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[137]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[138]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[139]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[140]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[141]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[142]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[143]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[144]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[145]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[146]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[147]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[148]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[149]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[150]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[151]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[152]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[153]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[154]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[155]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[156]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[157]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[158]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[159]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[160]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[161]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[162]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[163]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[164]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[165]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[166]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[167]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[168]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[169]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[170]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[171]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[172]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[173]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[174]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[175]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[176]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[177]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[178]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[179]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[180]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[181]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[182]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[183]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[184]);
                }
                if ( (short)parms[49] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[185]);
                }
                if ( (short)parms[50] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[186]);
                }
                if ( (short)parms[51] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[187]);
                }
                if ( (short)parms[52] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[188]);
                }
                if ( (short)parms[53] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[189]);
                }
                if ( (short)parms[54] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[190]);
                }
                if ( (short)parms[55] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[191]);
                }
                if ( (short)parms[56] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[192]);
                }
                if ( (short)parms[57] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[193]);
                }
                if ( (short)parms[58] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[194]);
                }
                if ( (short)parms[59] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[195]);
                }
                if ( (short)parms[60] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[196]);
                }
                if ( (short)parms[61] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[197]);
                }
                if ( (short)parms[62] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[198]);
                }
                if ( (short)parms[63] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[199]);
                }
                if ( (short)parms[64] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[200]);
                }
                if ( (short)parms[65] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[201]);
                }
                if ( (short)parms[66] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[202]);
                }
                if ( (short)parms[67] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[203]);
                }
                if ( (short)parms[68] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[204]);
                }
                if ( (short)parms[69] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[205]);
                }
                if ( (short)parms[70] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[206]);
                }
                if ( (short)parms[71] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[207]);
                }
                if ( (short)parms[72] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[208]);
                }
                if ( (short)parms[73] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[209]);
                }
                if ( (short)parms[74] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[210]);
                }
                if ( (short)parms[75] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[211]);
                }
                if ( (short)parms[76] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[212]);
                }
                if ( (short)parms[77] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[213]);
                }
                if ( (short)parms[78] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[214]);
                }
                if ( (short)parms[79] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[215]);
                }
                if ( (short)parms[80] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[216]);
                }
                if ( (short)parms[81] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[217]);
                }
                if ( (short)parms[82] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[218]);
                }
                if ( (short)parms[83] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[219]);
                }
                if ( (short)parms[84] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[220]);
                }
                if ( (short)parms[85] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[221]);
                }
                if ( (short)parms[86] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[222]);
                }
                if ( (short)parms[87] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[223]);
                }
                if ( (short)parms[88] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[224]);
                }
                if ( (short)parms[89] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[225]);
                }
                if ( (short)parms[90] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[226]);
                }
                if ( (short)parms[91] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[227]);
                }
                if ( (short)parms[92] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[228]);
                }
                if ( (short)parms[93] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[229]);
                }
                if ( (short)parms[94] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[230]);
                }
                if ( (short)parms[95] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[231]);
                }
                if ( (short)parms[96] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[232]);
                }
                if ( (short)parms[97] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[233]);
                }
                if ( (short)parms[98] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[234]);
                }
                if ( (short)parms[99] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[235]);
                }
                if ( (short)parms[100] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[236]);
                }
                if ( (short)parms[101] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[237]);
                }
                if ( (short)parms[102] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[238]);
                }
                if ( (short)parms[103] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[239]);
                }
                if ( (short)parms[104] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[240]);
                }
                if ( (short)parms[105] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[241]);
                }
                if ( (short)parms[106] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[242]);
                }
                if ( (short)parms[107] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[243]);
                }
                if ( (short)parms[108] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[244]);
                }
                if ( (short)parms[109] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[245]);
                }
                if ( (short)parms[110] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[246]);
                }
                if ( (short)parms[111] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[247]);
                }
                if ( (short)parms[112] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[248]);
                }
                if ( (short)parms[113] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[249]);
                }
                if ( (short)parms[114] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[250]);
                }
                if ( (short)parms[115] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[251]);
                }
                if ( (short)parms[116] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[252]);
                }
                if ( (short)parms[117] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[253]);
                }
                if ( (short)parms[118] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[254]);
                }
                if ( (short)parms[119] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[255]);
                }
                if ( (short)parms[120] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[256]);
                }
                if ( (short)parms[121] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[257]);
                }
                if ( (short)parms[122] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[258]);
                }
                if ( (short)parms[123] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[259]);
                }
                if ( (short)parms[124] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[260]);
                }
                if ( (short)parms[125] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[261]);
                }
                if ( (short)parms[126] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[262]);
                }
                if ( (short)parms[127] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[263]);
                }
                if ( (short)parms[128] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[264]);
                }
                if ( (short)parms[129] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[265]);
                }
                if ( (short)parms[130] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[266]);
                }
                if ( (short)parms[131] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[267]);
                }
                if ( (short)parms[132] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[268]);
                }
                if ( (short)parms[133] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[269]);
                }
                if ( (short)parms[134] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[270]);
                }
                if ( (short)parms[135] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[271]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[136]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[137]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[138]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[139]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[140]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[141]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[142]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[143]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[144]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[145]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[146]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[147]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[148]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[149]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[150]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[151]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[152]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[153]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[154]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[155]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[156]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[157]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[158]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[159]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[160]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[161]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[162]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[163]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[164]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[165]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[166]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[167]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[168]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[169]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[170]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[171]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[172]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[173]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[174]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[175]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[176]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[177]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[178]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[179]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[180]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[181]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[182]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[183]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[184]);
                }
                if ( (short)parms[49] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[185]);
                }
                if ( (short)parms[50] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[186]);
                }
                if ( (short)parms[51] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[187]);
                }
                if ( (short)parms[52] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[188]);
                }
                if ( (short)parms[53] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[189]);
                }
                if ( (short)parms[54] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[190]);
                }
                if ( (short)parms[55] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[191]);
                }
                if ( (short)parms[56] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[192]);
                }
                if ( (short)parms[57] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[193]);
                }
                if ( (short)parms[58] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[194]);
                }
                if ( (short)parms[59] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[195]);
                }
                if ( (short)parms[60] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[196]);
                }
                if ( (short)parms[61] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[197]);
                }
                if ( (short)parms[62] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[198]);
                }
                if ( (short)parms[63] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[199]);
                }
                if ( (short)parms[64] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[200]);
                }
                if ( (short)parms[65] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[201]);
                }
                if ( (short)parms[66] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[202]);
                }
                if ( (short)parms[67] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[203]);
                }
                if ( (short)parms[68] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[204]);
                }
                if ( (short)parms[69] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[205]);
                }
                if ( (short)parms[70] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[206]);
                }
                if ( (short)parms[71] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[207]);
                }
                if ( (short)parms[72] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[208]);
                }
                if ( (short)parms[73] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[209]);
                }
                if ( (short)parms[74] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[210]);
                }
                if ( (short)parms[75] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[211]);
                }
                if ( (short)parms[76] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[212]);
                }
                if ( (short)parms[77] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[213]);
                }
                if ( (short)parms[78] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[214]);
                }
                if ( (short)parms[79] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[215]);
                }
                if ( (short)parms[80] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[216]);
                }
                if ( (short)parms[81] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[217]);
                }
                if ( (short)parms[82] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[218]);
                }
                if ( (short)parms[83] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[219]);
                }
                if ( (short)parms[84] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[220]);
                }
                if ( (short)parms[85] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[221]);
                }
                if ( (short)parms[86] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[222]);
                }
                if ( (short)parms[87] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[223]);
                }
                if ( (short)parms[88] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[224]);
                }
                if ( (short)parms[89] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[225]);
                }
                if ( (short)parms[90] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[226]);
                }
                if ( (short)parms[91] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[227]);
                }
                if ( (short)parms[92] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[228]);
                }
                if ( (short)parms[93] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[229]);
                }
                if ( (short)parms[94] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[230]);
                }
                if ( (short)parms[95] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[231]);
                }
                if ( (short)parms[96] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[232]);
                }
                if ( (short)parms[97] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[233]);
                }
                if ( (short)parms[98] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[234]);
                }
                if ( (short)parms[99] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[235]);
                }
                if ( (short)parms[100] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[236]);
                }
                if ( (short)parms[101] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[237]);
                }
                if ( (short)parms[102] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[238]);
                }
                if ( (short)parms[103] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[239]);
                }
                if ( (short)parms[104] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[240]);
                }
                if ( (short)parms[105] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[241]);
                }
                if ( (short)parms[106] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[242]);
                }
                if ( (short)parms[107] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[243]);
                }
                if ( (short)parms[108] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[244]);
                }
                if ( (short)parms[109] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[245]);
                }
                if ( (short)parms[110] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[246]);
                }
                if ( (short)parms[111] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[247]);
                }
                if ( (short)parms[112] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[248]);
                }
                if ( (short)parms[113] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[249]);
                }
                if ( (short)parms[114] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[250]);
                }
                if ( (short)parms[115] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[251]);
                }
                if ( (short)parms[116] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[252]);
                }
                if ( (short)parms[117] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[253]);
                }
                if ( (short)parms[118] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[254]);
                }
                if ( (short)parms[119] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[255]);
                }
                if ( (short)parms[120] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[256]);
                }
                if ( (short)parms[121] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[257]);
                }
                if ( (short)parms[122] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[258]);
                }
                if ( (short)parms[123] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[259]);
                }
                if ( (short)parms[124] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[260]);
                }
                if ( (short)parms[125] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[261]);
                }
                if ( (short)parms[126] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[262]);
                }
                if ( (short)parms[127] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[263]);
                }
                if ( (short)parms[128] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[264]);
                }
                if ( (short)parms[129] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[265]);
                }
                if ( (short)parms[130] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[266]);
                }
                if ( (short)parms[131] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[267]);
                }
                if ( (short)parms[132] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[268]);
                }
                if ( (short)parms[133] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[269]);
                }
                if ( (short)parms[134] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[270]);
                }
                if ( (short)parms[135] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[271]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[136]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[137]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[138]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[139]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[140]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[141]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[142]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[143]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[144]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[145]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[146]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[147]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[148]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[149]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[150]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[151]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[152]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[153]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[154]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[155]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[156]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[157]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[158]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[159]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[160]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[161]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[162]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[163]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[164]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[165]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[166]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[167]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[168]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[169]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[170]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[171]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[172]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[173]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[174]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[175]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[176]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[177]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[178]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[179]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[180]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[181]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[182]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[183]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[184]);
                }
                if ( (short)parms[49] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[185]);
                }
                if ( (short)parms[50] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[186]);
                }
                if ( (short)parms[51] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[187]);
                }
                if ( (short)parms[52] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[188]);
                }
                if ( (short)parms[53] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[189]);
                }
                if ( (short)parms[54] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[190]);
                }
                if ( (short)parms[55] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[191]);
                }
                if ( (short)parms[56] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[192]);
                }
                if ( (short)parms[57] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[193]);
                }
                if ( (short)parms[58] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[194]);
                }
                if ( (short)parms[59] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[195]);
                }
                if ( (short)parms[60] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[196]);
                }
                if ( (short)parms[61] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[197]);
                }
                if ( (short)parms[62] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[198]);
                }
                if ( (short)parms[63] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[199]);
                }
                if ( (short)parms[64] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[200]);
                }
                if ( (short)parms[65] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[201]);
                }
                if ( (short)parms[66] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[202]);
                }
                if ( (short)parms[67] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[203]);
                }
                if ( (short)parms[68] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[204]);
                }
                if ( (short)parms[69] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[205]);
                }
                if ( (short)parms[70] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[206]);
                }
                if ( (short)parms[71] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[207]);
                }
                if ( (short)parms[72] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[208]);
                }
                if ( (short)parms[73] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[209]);
                }
                if ( (short)parms[74] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[210]);
                }
                if ( (short)parms[75] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[211]);
                }
                if ( (short)parms[76] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[212]);
                }
                if ( (short)parms[77] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[213]);
                }
                if ( (short)parms[78] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[214]);
                }
                if ( (short)parms[79] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[215]);
                }
                if ( (short)parms[80] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[216]);
                }
                if ( (short)parms[81] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[217]);
                }
                if ( (short)parms[82] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[218]);
                }
                if ( (short)parms[83] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[219]);
                }
                if ( (short)parms[84] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[220]);
                }
                if ( (short)parms[85] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[221]);
                }
                if ( (short)parms[86] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[222]);
                }
                if ( (short)parms[87] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[223]);
                }
                if ( (short)parms[88] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[224]);
                }
                if ( (short)parms[89] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[225]);
                }
                if ( (short)parms[90] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[226]);
                }
                if ( (short)parms[91] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[227]);
                }
                if ( (short)parms[92] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[228]);
                }
                if ( (short)parms[93] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[229]);
                }
                if ( (short)parms[94] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[230]);
                }
                if ( (short)parms[95] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[231]);
                }
                if ( (short)parms[96] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[232]);
                }
                if ( (short)parms[97] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[233]);
                }
                if ( (short)parms[98] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[234]);
                }
                if ( (short)parms[99] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[235]);
                }
                if ( (short)parms[100] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[236]);
                }
                if ( (short)parms[101] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[237]);
                }
                if ( (short)parms[102] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[238]);
                }
                if ( (short)parms[103] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[239]);
                }
                if ( (short)parms[104] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[240]);
                }
                if ( (short)parms[105] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[241]);
                }
                if ( (short)parms[106] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[242]);
                }
                if ( (short)parms[107] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[243]);
                }
                if ( (short)parms[108] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[244]);
                }
                if ( (short)parms[109] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[245]);
                }
                if ( (short)parms[110] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[246]);
                }
                if ( (short)parms[111] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[247]);
                }
                if ( (short)parms[112] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[248]);
                }
                if ( (short)parms[113] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[249]);
                }
                if ( (short)parms[114] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[250]);
                }
                if ( (short)parms[115] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[251]);
                }
                if ( (short)parms[116] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[252]);
                }
                if ( (short)parms[117] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[253]);
                }
                if ( (short)parms[118] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[254]);
                }
                if ( (short)parms[119] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[255]);
                }
                if ( (short)parms[120] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[256]);
                }
                if ( (short)parms[121] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[257]);
                }
                if ( (short)parms[122] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[258]);
                }
                if ( (short)parms[123] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[259]);
                }
                if ( (short)parms[124] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[260]);
                }
                if ( (short)parms[125] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[261]);
                }
                if ( (short)parms[126] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[262]);
                }
                if ( (short)parms[127] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[263]);
                }
                if ( (short)parms[128] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[264]);
                }
                if ( (short)parms[129] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[265]);
                }
                if ( (short)parms[130] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[266]);
                }
                if ( (short)parms[131] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[267]);
                }
                if ( (short)parms[132] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[268]);
                }
                if ( (short)parms[133] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[269]);
                }
                if ( (short)parms[134] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[270]);
                }
                if ( (short)parms[135] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[271]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[136]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[137]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[138]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[139]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[140]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[141]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[142]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[143]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[144]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[145]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[146]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[147]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[148]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[149]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[150]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[151]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[152]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[153]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[154]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[155]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[156]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[157]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[158]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[159]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[160]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[161]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[162]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[163]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[164]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[165]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[166]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[167]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[168]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[169]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[170]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[171]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[172]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[173]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[174]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[175]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[176]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[177]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[178]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[179]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[180]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[181]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[182]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[183]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[184]);
                }
                if ( (short)parms[49] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[185]);
                }
                if ( (short)parms[50] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[186]);
                }
                if ( (short)parms[51] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[187]);
                }
                if ( (short)parms[52] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[188]);
                }
                if ( (short)parms[53] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[189]);
                }
                if ( (short)parms[54] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[190]);
                }
                if ( (short)parms[55] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[191]);
                }
                if ( (short)parms[56] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[192]);
                }
                if ( (short)parms[57] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[193]);
                }
                if ( (short)parms[58] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[194]);
                }
                if ( (short)parms[59] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[195]);
                }
                if ( (short)parms[60] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[196]);
                }
                if ( (short)parms[61] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[197]);
                }
                if ( (short)parms[62] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[198]);
                }
                if ( (short)parms[63] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[199]);
                }
                if ( (short)parms[64] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[200]);
                }
                if ( (short)parms[65] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[201]);
                }
                if ( (short)parms[66] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[202]);
                }
                if ( (short)parms[67] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[203]);
                }
                if ( (short)parms[68] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[204]);
                }
                if ( (short)parms[69] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[205]);
                }
                if ( (short)parms[70] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[206]);
                }
                if ( (short)parms[71] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[207]);
                }
                if ( (short)parms[72] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[208]);
                }
                if ( (short)parms[73] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[209]);
                }
                if ( (short)parms[74] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[210]);
                }
                if ( (short)parms[75] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[211]);
                }
                if ( (short)parms[76] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[212]);
                }
                if ( (short)parms[77] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[213]);
                }
                if ( (short)parms[78] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[214]);
                }
                if ( (short)parms[79] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[215]);
                }
                if ( (short)parms[80] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[216]);
                }
                if ( (short)parms[81] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[217]);
                }
                if ( (short)parms[82] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[218]);
                }
                if ( (short)parms[83] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[219]);
                }
                if ( (short)parms[84] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[220]);
                }
                if ( (short)parms[85] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[221]);
                }
                if ( (short)parms[86] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[222]);
                }
                if ( (short)parms[87] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[223]);
                }
                if ( (short)parms[88] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[224]);
                }
                if ( (short)parms[89] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[225]);
                }
                if ( (short)parms[90] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[226]);
                }
                if ( (short)parms[91] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[227]);
                }
                if ( (short)parms[92] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[228]);
                }
                if ( (short)parms[93] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[229]);
                }
                if ( (short)parms[94] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[230]);
                }
                if ( (short)parms[95] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[231]);
                }
                if ( (short)parms[96] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[232]);
                }
                if ( (short)parms[97] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[233]);
                }
                if ( (short)parms[98] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[234]);
                }
                if ( (short)parms[99] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[235]);
                }
                if ( (short)parms[100] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[236]);
                }
                if ( (short)parms[101] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[237]);
                }
                if ( (short)parms[102] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[238]);
                }
                if ( (short)parms[103] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[239]);
                }
                if ( (short)parms[104] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[240]);
                }
                if ( (short)parms[105] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[241]);
                }
                if ( (short)parms[106] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[242]);
                }
                if ( (short)parms[107] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[243]);
                }
                if ( (short)parms[108] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[244]);
                }
                if ( (short)parms[109] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[245]);
                }
                if ( (short)parms[110] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[246]);
                }
                if ( (short)parms[111] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[247]);
                }
                if ( (short)parms[112] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[248]);
                }
                if ( (short)parms[113] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[249]);
                }
                if ( (short)parms[114] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[250]);
                }
                if ( (short)parms[115] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[251]);
                }
                if ( (short)parms[116] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[252]);
                }
                if ( (short)parms[117] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[253]);
                }
                if ( (short)parms[118] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[254]);
                }
                if ( (short)parms[119] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[255]);
                }
                if ( (short)parms[120] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[256]);
                }
                if ( (short)parms[121] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[257]);
                }
                if ( (short)parms[122] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[258]);
                }
                if ( (short)parms[123] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[259]);
                }
                if ( (short)parms[124] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[260]);
                }
                if ( (short)parms[125] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[261]);
                }
                if ( (short)parms[126] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[262]);
                }
                if ( (short)parms[127] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[263]);
                }
                if ( (short)parms[128] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[264]);
                }
                if ( (short)parms[129] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[265]);
                }
                if ( (short)parms[130] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[266]);
                }
                if ( (short)parms[131] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[267]);
                }
                if ( (short)parms[132] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[268]);
                }
                if ( (short)parms[133] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[269]);
                }
                if ( (short)parms[134] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[270]);
                }
                if ( (short)parms[135] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[271]);
                }
                return;
             case 4 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[136]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[137]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[138]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[139]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[140]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[141]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[142]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[143]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[144]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[145]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[146]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[147]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[148]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[149]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[150]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[151]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[152]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[153]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[154]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[155]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[156]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[157]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[158]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[159]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[160]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[161]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[162]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[163]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[164]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[165]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[166]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[167]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[168]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[169]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[170]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[171]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[172]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[173]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[174]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[175]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[176]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[177]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[178]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[179]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[180]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[181]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[182]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[183]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[184]);
                }
                if ( (short)parms[49] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[185]);
                }
                if ( (short)parms[50] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[186]);
                }
                if ( (short)parms[51] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[187]);
                }
                if ( (short)parms[52] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[188]);
                }
                if ( (short)parms[53] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[189]);
                }
                if ( (short)parms[54] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[190]);
                }
                if ( (short)parms[55] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[191]);
                }
                if ( (short)parms[56] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[192]);
                }
                if ( (short)parms[57] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[193]);
                }
                if ( (short)parms[58] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[194]);
                }
                if ( (short)parms[59] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[195]);
                }
                if ( (short)parms[60] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[196]);
                }
                if ( (short)parms[61] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[197]);
                }
                if ( (short)parms[62] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[198]);
                }
                if ( (short)parms[63] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[199]);
                }
                if ( (short)parms[64] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[200]);
                }
                if ( (short)parms[65] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[201]);
                }
                if ( (short)parms[66] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[202]);
                }
                if ( (short)parms[67] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[203]);
                }
                if ( (short)parms[68] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[204]);
                }
                if ( (short)parms[69] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[205]);
                }
                if ( (short)parms[70] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[206]);
                }
                if ( (short)parms[71] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[207]);
                }
                if ( (short)parms[72] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[208]);
                }
                if ( (short)parms[73] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[209]);
                }
                if ( (short)parms[74] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[210]);
                }
                if ( (short)parms[75] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[211]);
                }
                if ( (short)parms[76] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[212]);
                }
                if ( (short)parms[77] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[213]);
                }
                if ( (short)parms[78] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[214]);
                }
                if ( (short)parms[79] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[215]);
                }
                if ( (short)parms[80] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[216]);
                }
                if ( (short)parms[81] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[217]);
                }
                if ( (short)parms[82] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[218]);
                }
                if ( (short)parms[83] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[219]);
                }
                if ( (short)parms[84] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[220]);
                }
                if ( (short)parms[85] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[221]);
                }
                if ( (short)parms[86] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[222]);
                }
                if ( (short)parms[87] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[223]);
                }
                if ( (short)parms[88] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[224]);
                }
                if ( (short)parms[89] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[225]);
                }
                if ( (short)parms[90] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[226]);
                }
                if ( (short)parms[91] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[227]);
                }
                if ( (short)parms[92] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[228]);
                }
                if ( (short)parms[93] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[229]);
                }
                if ( (short)parms[94] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[230]);
                }
                if ( (short)parms[95] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[231]);
                }
                if ( (short)parms[96] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[232]);
                }
                if ( (short)parms[97] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[233]);
                }
                if ( (short)parms[98] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[234]);
                }
                if ( (short)parms[99] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[235]);
                }
                if ( (short)parms[100] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[236]);
                }
                if ( (short)parms[101] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[237]);
                }
                if ( (short)parms[102] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[238]);
                }
                if ( (short)parms[103] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[239]);
                }
                if ( (short)parms[104] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[240]);
                }
                if ( (short)parms[105] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[241]);
                }
                if ( (short)parms[106] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[242]);
                }
                if ( (short)parms[107] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[243]);
                }
                if ( (short)parms[108] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[244]);
                }
                if ( (short)parms[109] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[245]);
                }
                if ( (short)parms[110] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[246]);
                }
                if ( (short)parms[111] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[247]);
                }
                if ( (short)parms[112] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[248]);
                }
                if ( (short)parms[113] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[249]);
                }
                if ( (short)parms[114] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[250]);
                }
                if ( (short)parms[115] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[251]);
                }
                if ( (short)parms[116] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[252]);
                }
                if ( (short)parms[117] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[253]);
                }
                if ( (short)parms[118] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[254]);
                }
                if ( (short)parms[119] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[255]);
                }
                if ( (short)parms[120] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[256]);
                }
                if ( (short)parms[121] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[257]);
                }
                if ( (short)parms[122] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[258]);
                }
                if ( (short)parms[123] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[259]);
                }
                if ( (short)parms[124] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[260]);
                }
                if ( (short)parms[125] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[261]);
                }
                if ( (short)parms[126] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[262]);
                }
                if ( (short)parms[127] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[263]);
                }
                if ( (short)parms[128] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[264]);
                }
                if ( (short)parms[129] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[265]);
                }
                if ( (short)parms[130] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[266]);
                }
                if ( (short)parms[131] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[267]);
                }
                if ( (short)parms[132] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[268]);
                }
                if ( (short)parms[133] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[269]);
                }
                if ( (short)parms[134] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[270]);
                }
                if ( (short)parms[135] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[271]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getextrawwcontagemresultadoemgarantiafilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getextrawwcontagemresultadoemgarantiafilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getextrawwcontagemresultadoemgarantiafilterdata") )
          {
             return  ;
          }
          getextrawwcontagemresultadoemgarantiafilterdata worker = new getextrawwcontagemresultadoemgarantiafilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
