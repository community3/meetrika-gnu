/*
               File: REL_PlanoDeContagem
        Description: Plano de Contagem
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:8:31.26
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.Printer;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class arel_planodecontagem : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            if ( ! entryPointCalled )
            {
               A192Contagem_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
            }
         }
         if ( GxWebError == 0 )
         {
            executePrivate();
         }
         cleanup();
      }

      public arel_planodecontagem( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public arel_planodecontagem( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Contagem_Codigo )
      {
         this.A192Contagem_Codigo = aP0_Contagem_Codigo;
         initialize();
         executePrivate();
         aP0_Contagem_Codigo=this.A192Contagem_Codigo;
      }

      public int executeUdp( )
      {
         this.A192Contagem_Codigo = aP0_Contagem_Codigo;
         initialize();
         executePrivate();
         aP0_Contagem_Codigo=this.A192Contagem_Codigo;
         return A192Contagem_Codigo ;
      }

      public void executeSubmit( ref int aP0_Contagem_Codigo )
      {
         arel_planodecontagem objarel_planodecontagem;
         objarel_planodecontagem = new arel_planodecontagem();
         objarel_planodecontagem.A192Contagem_Codigo = aP0_Contagem_Codigo;
         objarel_planodecontagem.context.SetSubmitInitialConfig(context);
         objarel_planodecontagem.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objarel_planodecontagem);
         aP0_Contagem_Codigo=this.A192Contagem_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((arel_planodecontagem)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         M_top = 0;
         M_bot = 3;
         P_lines = (int)(66-M_bot);
         getPrinter().GxClearAttris() ;
         add_metrics( ) ;
         lineHeight = 15;
         PrtOffset = 0;
         gxXPage = 100;
         gxYPage = 100;
         getPrinter().GxSetDocName("") ;
         try
         {
            Gx_out = "FIL" ;
            if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 1, 9, 16834, 11909, 0, 1, 1, 0, 1, 1) )
            {
               cleanup();
               return;
            }
            getPrinter().setModal(false) ;
            P_lines = (int)(gxYPage-(lineHeight*3));
            Gx_line = (int)(P_lines+1);
            getPrinter().setPageLines(P_lines);
            getPrinter().setLineHeight(lineHeight);
            getPrinter().setM_top(M_top);
            getPrinter().setM_bot(M_bot);
            /* Using cursor P006H2 */
            pr_default.execute(0, new Object[] {A192Contagem_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A940Contagem_SistemaCod = P006H2_A940Contagem_SistemaCod[0];
               n940Contagem_SistemaCod = P006H2_n940Contagem_SistemaCod[0];
               A197Contagem_DataCriacao = P006H2_A197Contagem_DataCriacao[0];
               A196Contagem_Tipo = P006H2_A196Contagem_Tipo[0];
               n196Contagem_Tipo = P006H2_n196Contagem_Tipo[0];
               A941Contagem_SistemaSigla = P006H2_A941Contagem_SistemaSigla[0];
               n941Contagem_SistemaSigla = P006H2_n941Contagem_SistemaSigla[0];
               A949Contagem_SistemaCoord = P006H2_A949Contagem_SistemaCoord[0];
               n949Contagem_SistemaCoord = P006H2_n949Contagem_SistemaCoord[0];
               A947Contagem_Fator = P006H2_A947Contagem_Fator[0];
               n947Contagem_Fator = P006H2_n947Contagem_Fator[0];
               A943Contagem_PFB = P006H2_A943Contagem_PFB[0];
               n943Contagem_PFB = P006H2_n943Contagem_PFB[0];
               A944Contagem_PFL = P006H2_A944Contagem_PFL[0];
               n944Contagem_PFL = P006H2_n944Contagem_PFL[0];
               A195Contagem_Tecnica = P006H2_A195Contagem_Tecnica[0];
               n195Contagem_Tecnica = P006H2_n195Contagem_Tecnica[0];
               A1059Contagem_Notas = P006H2_A1059Contagem_Notas[0];
               n1059Contagem_Notas = P006H2_n1059Contagem_Notas[0];
               A941Contagem_SistemaSigla = P006H2_A941Contagem_SistemaSigla[0];
               n941Contagem_SistemaSigla = P006H2_n941Contagem_SistemaSigla[0];
               A949Contagem_SistemaCoord = P006H2_A949Contagem_SistemaCoord[0];
               n949Contagem_SistemaCoord = P006H2_n949Contagem_SistemaCoord[0];
               AV19Contagem_DataCriacao = A197Contagem_DataCriacao;
               AV18Contagem_Tipo = gxdomaintipocontagem.getDescription(context,A196Contagem_Tipo);
               AV17Contagem_SistemaSigla = A941Contagem_SistemaSigla;
               AV16Contagem_SistemaCoord = A949Contagem_SistemaCoord;
               AV20Contagem_Fator = A947Contagem_Fator;
               AV21Contagem_PFB = A943Contagem_PFB;
               AV22Contagem_PFL = A944Contagem_PFL;
               AV23Contagem_Tecnica = gxdomaintecnicacontagem.getDescription(context,A195Contagem_Tecnica);
               AV24Contagem_Notas = A1059Contagem_Notas;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            AV10tPFB = 0;
            AV11tPFL = 0;
            AV12tDER = 0;
            AV13tRA = 0;
            AV14tQtd = 0;
            AV15tINM = 0;
            /* Using cursor P006H3 */
            pr_default.execute(1, new Object[] {A192Contagem_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A954ContagemItem_QtdINM = P006H3_A954ContagemItem_QtdINM[0];
               n954ContagemItem_QtdINM = P006H3_n954ContagemItem_QtdINM[0];
               A951ContagemItem_PFL = P006H3_A951ContagemItem_PFL[0];
               n951ContagemItem_PFL = P006H3_n951ContagemItem_PFL[0];
               A950ContagemItem_PFB = P006H3_A950ContagemItem_PFB[0];
               n950ContagemItem_PFB = P006H3_n950ContagemItem_PFB[0];
               A955ContagemItem_CP = P006H3_A955ContagemItem_CP[0];
               n955ContagemItem_CP = P006H3_n955ContagemItem_CP[0];
               A956ContagemItem_RA = P006H3_A956ContagemItem_RA[0];
               n956ContagemItem_RA = P006H3_n956ContagemItem_RA[0];
               A957ContagemItem_DER = P006H3_A957ContagemItem_DER[0];
               n957ContagemItem_DER = P006H3_n957ContagemItem_DER[0];
               A952ContagemItem_TipoUnidade = P006H3_A952ContagemItem_TipoUnidade[0];
               A958ContagemItem_Funcao = P006H3_A958ContagemItem_Funcao[0];
               n958ContagemItem_Funcao = P006H3_n958ContagemItem_Funcao[0];
               A224ContagemItem_Lancamento = P006H3_A224ContagemItem_Lancamento[0];
               H6H0( false, 15) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A958ContagemItem_Funcao, "")), 0, Gx_line+0, 261, Gx_line+15, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A952ContagemItem_TipoUnidade, "")), 265, Gx_line+0, 300, Gx_line+15, 1+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(A957ContagemItem_DER), "ZZ9")), 306, Gx_line+0, 326, Gx_line+15, 1+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(A956ContagemItem_RA), "ZZ9")), 341, Gx_line+0, 358, Gx_line+15, 1, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A955ContagemItem_CP, "")), 402, Gx_line+0, 414, Gx_line+15, 1+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A950ContagemItem_PFB, "ZZ,ZZZ,ZZ9.999")), 410, Gx_line+0, 499, Gx_line+15, 2+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( A951ContagemItem_PFL, "ZZ,ZZZ,ZZ9.999")), 497, Gx_line+0, 586, Gx_line+15, 2+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(A954ContagemItem_QtdINM), "ZZZ9")), 371, Gx_line+0, 388, Gx_line+15, 1, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+15);
               AV10tPFB = (decimal)(AV10tPFB+A950ContagemItem_PFB);
               AV11tPFL = (decimal)(AV11tPFL+A951ContagemItem_PFL);
               AV12tDER = (short)(AV12tDER+A957ContagemItem_DER);
               AV13tRA = (short)(AV13tRA+A956ContagemItem_RA);
               AV15tINM = (short)(AV15tINM+A954ContagemItem_QtdINM);
               AV14tQtd = (short)(AV14tQtd+1);
               pr_default.readNext(1);
            }
            pr_default.close(1);
            H6H0( false, 26) ;
            getPrinter().GxDrawLine(0, Gx_line+4, 838, Gx_line+4, 1, 0, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV14tQtd), "ZZZ9")), 83, Gx_line+5, 109, Gx_line+20, 2+256, 0, 0, 1) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV10tPFB, "ZZ,ZZZ,ZZ9.999")), 396, Gx_line+7, 499, Gx_line+26, 2, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV11tPFL, "ZZ,ZZZ,ZZ9.999")), 486, Gx_line+7, 586, Gx_line+26, 2, 0, 0, 0) ;
            getPrinter().GxDrawText("Totais", 225, Gx_line+5, 260, Gx_line+23, 0, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV12tDER), "ZZZ9")), 301, Gx_line+5, 331, Gx_line+24, 1, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV13tRA), "ZZZ9")), 333, Gx_line+5, 363, Gx_line+24, 1, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV15tINM), "ZZZ9")), 365, Gx_line+5, 395, Gx_line+24, 1, 0, 0, 0) ;
            getPrinter().GxDrawText("fun��es", 117, Gx_line+5, 164, Gx_line+23, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+26);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24Contagem_Notas)) )
            {
               /* Eject command */
               Gx_OldLine = Gx_line;
               Gx_line = (int)(P_lines+1);
               H6H0( false, 1125) ;
               getPrinter().GxAttris("Calibri", 12, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Anota��es:", 17, Gx_line+7, 109, Gx_line+27, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(AV24Contagem_Notas, 17, Gx_line+33, 809, Gx_line+1116, 0, 1, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+1125);
            }
            /* Print footer for last page */
            ToSkip = (int)(P_lines+1);
            H6H0( true, 0) ;
         }
         catch ( GeneXus.Printer.ProcessInterruptedException e )
         {
         }
         finally
         {
            /* Close printer file */
            try
            {
               getPrinter().GxEndPage() ;
               getPrinter().GxEndDocument() ;
            }
            catch ( GeneXus.Printer.ProcessInterruptedException e )
            {
            }
            endPrinter();
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      protected void H6H0( bool bFoot ,
                           int Inc )
      {
         /* Skip the required number of lines */
         while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
         {
            if ( Gx_line + Inc >= P_lines )
            {
               if ( Gx_page > 0 )
               {
                  /* Print footers */
                  Gx_line = P_lines;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(Gx_page), "ZZZZZ9")), 733, Gx_line+0, 772, Gx_line+15, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("{{Pages}}", 800, Gx_line+0, 849, Gx_line+14, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("de", 783, Gx_line+0, 797, Gx_line+14, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("P�gina", 700, Gx_line+0, 735, Gx_line+14, 0+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+17);
                  getPrinter().GxEndPage() ;
                  if ( bFoot )
                  {
                     return  ;
                  }
               }
               ToSkip = 0;
               Gx_line = 0;
               Gx_page = (int)(Gx_page+1);
               /* Skip Margin Top Lines */
               Gx_line = (int)(Gx_line+(M_top*lineHeight));
               /* Print headers */
               getPrinter().GxStartPage() ;
               getPrinter().GxDrawBitMap(context.GetImagePath( "7ad3f77f-79a3-4554-aaa4-e84910e0d464", "", context.GetTheme( )), 0, Gx_line+0, 120, Gx_line+100) ;
               getPrinter().GxDrawLine(0, Gx_line+167, 838, Gx_line+167, 1, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 16, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Plano de Contagem", 317, Gx_line+17, 542, Gx_line+45, 1, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(A192Contagem_Codigo), "ZZZZZ9")), 292, Gx_line+68, 331, Gx_line+83, 0+256, 0, 0, 2) ;
               getPrinter().GxDrawText(context.localUtil.Format( AV19Contagem_DataCriacao, "99/99/99"), 375, Gx_line+68, 424, Gx_line+83, 0+256, 0, 0, 2) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV18Contagem_Tipo, "@!")), 475, Gx_line+68, 736, Gx_line+83, 0+256, 0, 0, 2) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV17Contagem_SistemaSigla, "@!")), 292, Gx_line+84, 423, Gx_line+99, 0+256, 0, 0, 2) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV16Contagem_SistemaCoord, "@!")), 525, Gx_line+84, 786, Gx_line+99, 0+256, 0, 0, 2) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( Gx_time, "")), 783, Gx_line+17, 849, Gx_line+32, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(context.localUtil.Format( Gx_date, "99/99/99"), 725, Gx_line+17, 774, Gx_line+32, 2+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV20Contagem_Fator, "9.99")), 333, Gx_line+100, 359, Gx_line+115, 0+256, 0, 0, 2) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV21Contagem_PFB, "ZZZZZZ9.99999")), 425, Gx_line+100, 507, Gx_line+115, 0+256, 0, 0, 2) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV22Contagem_PFL, "ZZZZZZ9.99999")), 550, Gx_line+100, 632, Gx_line+115, 0+256, 0, 0, 2) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV23Contagem_Tecnica, "")), 285, Gx_line+117, 390, Gx_line+132, 0+256, 0, 0, 2) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Processo Elementar ou Grupo de Dados", 0, Gx_line+150, 235, Gx_line+164, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText("Tipo", 267, Gx_line+150, 292, Gx_line+164, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText("DER", 300, Gx_line+150, 327, Gx_line+164, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText("R/A", 342, Gx_line+150, 364, Gx_line+164, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText("QTD", 367, Gx_line+150, 393, Gx_line+164, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText("CP", 400, Gx_line+150, 418, Gx_line+164, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText("PFB", 458, Gx_line+150, 483, Gx_line+164, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText("PFL", 550, Gx_line+150, 574, Gx_line+164, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText("C�digo:", 242, Gx_line+67, 288, Gx_line+85, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Data:", 342, Gx_line+67, 375, Gx_line+85, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Tipo:", 442, Gx_line+67, 471, Gx_line+85, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Area gestora:", 442, Gx_line+83, 522, Gx_line+101, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Sistema:", 242, Gx_line+83, 293, Gx_line+101, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Fator de Ajuste:", 242, Gx_line+100, 337, Gx_line+118, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("PFB:", 392, Gx_line+100, 421, Gx_line+118, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("PFL:", 517, Gx_line+100, 545, Gx_line+118, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("T�nica:", 242, Gx_line+117, 285, Gx_line+135, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+169);
               if (true) break;
            }
            else
            {
               PrtOffset = 0;
               Gx_line = (int)(Gx_line+1);
            }
            ToSkip = (int)(ToSkip-1);
         }
         getPrinter().setPage(Gx_page);
      }

      protected void add_metrics( )
      {
         add_metrics0( ) ;
         add_metrics1( ) ;
         add_metrics2( ) ;
      }

      protected void add_metrics0( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      protected void add_metrics1( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      protected void add_metrics2( )
      {
         getPrinter().setMetrics("Calibri", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if (IsMain)	waitPrinterEnd();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXKey = "";
         gxfirstwebparm = "";
         scmdbuf = "";
         P006H2_A940Contagem_SistemaCod = new int[1] ;
         P006H2_n940Contagem_SistemaCod = new bool[] {false} ;
         P006H2_A192Contagem_Codigo = new int[1] ;
         P006H2_A197Contagem_DataCriacao = new DateTime[] {DateTime.MinValue} ;
         P006H2_A196Contagem_Tipo = new String[] {""} ;
         P006H2_n196Contagem_Tipo = new bool[] {false} ;
         P006H2_A941Contagem_SistemaSigla = new String[] {""} ;
         P006H2_n941Contagem_SistemaSigla = new bool[] {false} ;
         P006H2_A949Contagem_SistemaCoord = new String[] {""} ;
         P006H2_n949Contagem_SistemaCoord = new bool[] {false} ;
         P006H2_A947Contagem_Fator = new decimal[1] ;
         P006H2_n947Contagem_Fator = new bool[] {false} ;
         P006H2_A943Contagem_PFB = new decimal[1] ;
         P006H2_n943Contagem_PFB = new bool[] {false} ;
         P006H2_A944Contagem_PFL = new decimal[1] ;
         P006H2_n944Contagem_PFL = new bool[] {false} ;
         P006H2_A195Contagem_Tecnica = new String[] {""} ;
         P006H2_n195Contagem_Tecnica = new bool[] {false} ;
         P006H2_A1059Contagem_Notas = new String[] {""} ;
         P006H2_n1059Contagem_Notas = new bool[] {false} ;
         A197Contagem_DataCriacao = DateTime.MinValue;
         A196Contagem_Tipo = "";
         A941Contagem_SistemaSigla = "";
         A949Contagem_SistemaCoord = "";
         A195Contagem_Tecnica = "";
         A1059Contagem_Notas = "";
         AV19Contagem_DataCriacao = DateTime.MinValue;
         AV18Contagem_Tipo = "";
         AV17Contagem_SistemaSigla = "";
         AV16Contagem_SistemaCoord = "";
         AV23Contagem_Tecnica = "";
         AV24Contagem_Notas = "";
         P006H3_A192Contagem_Codigo = new int[1] ;
         P006H3_A954ContagemItem_QtdINM = new short[1] ;
         P006H3_n954ContagemItem_QtdINM = new bool[] {false} ;
         P006H3_A951ContagemItem_PFL = new decimal[1] ;
         P006H3_n951ContagemItem_PFL = new bool[] {false} ;
         P006H3_A950ContagemItem_PFB = new decimal[1] ;
         P006H3_n950ContagemItem_PFB = new bool[] {false} ;
         P006H3_A955ContagemItem_CP = new String[] {""} ;
         P006H3_n955ContagemItem_CP = new bool[] {false} ;
         P006H3_A956ContagemItem_RA = new short[1] ;
         P006H3_n956ContagemItem_RA = new bool[] {false} ;
         P006H3_A957ContagemItem_DER = new short[1] ;
         P006H3_n957ContagemItem_DER = new bool[] {false} ;
         P006H3_A952ContagemItem_TipoUnidade = new String[] {""} ;
         P006H3_A958ContagemItem_Funcao = new String[] {""} ;
         P006H3_n958ContagemItem_Funcao = new bool[] {false} ;
         P006H3_A224ContagemItem_Lancamento = new int[1] ;
         A955ContagemItem_CP = "";
         A952ContagemItem_TipoUnidade = "";
         A958ContagemItem_Funcao = "";
         Gx_time = "";
         Gx_date = DateTime.MinValue;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.arel_planodecontagem__default(),
            new Object[][] {
                new Object[] {
               P006H2_A940Contagem_SistemaCod, P006H2_n940Contagem_SistemaCod, P006H2_A192Contagem_Codigo, P006H2_A197Contagem_DataCriacao, P006H2_A196Contagem_Tipo, P006H2_n196Contagem_Tipo, P006H2_A941Contagem_SistemaSigla, P006H2_n941Contagem_SistemaSigla, P006H2_A949Contagem_SistemaCoord, P006H2_n949Contagem_SistemaCoord,
               P006H2_A947Contagem_Fator, P006H2_n947Contagem_Fator, P006H2_A943Contagem_PFB, P006H2_n943Contagem_PFB, P006H2_A944Contagem_PFL, P006H2_n944Contagem_PFL, P006H2_A195Contagem_Tecnica, P006H2_n195Contagem_Tecnica, P006H2_A1059Contagem_Notas, P006H2_n1059Contagem_Notas
               }
               , new Object[] {
               P006H3_A192Contagem_Codigo, P006H3_A954ContagemItem_QtdINM, P006H3_n954ContagemItem_QtdINM, P006H3_A951ContagemItem_PFL, P006H3_n951ContagemItem_PFL, P006H3_A950ContagemItem_PFB, P006H3_n950ContagemItem_PFB, P006H3_A955ContagemItem_CP, P006H3_n955ContagemItem_CP, P006H3_A956ContagemItem_RA,
               P006H3_n956ContagemItem_RA, P006H3_A957ContagemItem_DER, P006H3_n957ContagemItem_DER, P006H3_A952ContagemItem_TipoUnidade, P006H3_A958ContagemItem_Funcao, P006H3_n958ContagemItem_Funcao, P006H3_A224ContagemItem_Lancamento
               }
            }
         );
         Gx_date = DateTimeUtil.Today( context);
         Gx_time = context.localUtil.Time( );
         /* GeneXus formulas. */
         Gx_line = 0;
         Gx_date = DateTimeUtil.Today( context);
         Gx_time = context.localUtil.Time( );
         context.Gx_err = 0;
      }

      private short gxcookieaux ;
      private short nGotPars ;
      private short GxWebError ;
      private short AV12tDER ;
      private short AV13tRA ;
      private short AV14tQtd ;
      private short AV15tINM ;
      private short A954ContagemItem_QtdINM ;
      private short A956ContagemItem_RA ;
      private short A957ContagemItem_DER ;
      private int A192Contagem_Codigo ;
      private int M_top ;
      private int M_bot ;
      private int Line ;
      private int ToSkip ;
      private int PrtOffset ;
      private int A940Contagem_SistemaCod ;
      private int A224ContagemItem_Lancamento ;
      private int Gx_OldLine ;
      private decimal A947Contagem_Fator ;
      private decimal A943Contagem_PFB ;
      private decimal A944Contagem_PFL ;
      private decimal AV20Contagem_Fator ;
      private decimal AV21Contagem_PFB ;
      private decimal AV22Contagem_PFL ;
      private decimal AV10tPFB ;
      private decimal AV11tPFL ;
      private decimal A951ContagemItem_PFL ;
      private decimal A950ContagemItem_PFB ;
      private String GXKey ;
      private String gxfirstwebparm ;
      private String scmdbuf ;
      private String A196Contagem_Tipo ;
      private String A941Contagem_SistemaSigla ;
      private String A195Contagem_Tecnica ;
      private String AV18Contagem_Tipo ;
      private String AV17Contagem_SistemaSigla ;
      private String AV23Contagem_Tecnica ;
      private String A955ContagemItem_CP ;
      private String Gx_time ;
      private DateTime A197Contagem_DataCriacao ;
      private DateTime AV19Contagem_DataCriacao ;
      private DateTime Gx_date ;
      private bool entryPointCalled ;
      private bool n940Contagem_SistemaCod ;
      private bool n196Contagem_Tipo ;
      private bool n941Contagem_SistemaSigla ;
      private bool n949Contagem_SistemaCoord ;
      private bool n947Contagem_Fator ;
      private bool n943Contagem_PFB ;
      private bool n944Contagem_PFL ;
      private bool n195Contagem_Tecnica ;
      private bool n1059Contagem_Notas ;
      private bool n954ContagemItem_QtdINM ;
      private bool n951ContagemItem_PFL ;
      private bool n950ContagemItem_PFB ;
      private bool n955ContagemItem_CP ;
      private bool n956ContagemItem_RA ;
      private bool n957ContagemItem_DER ;
      private bool n958ContagemItem_Funcao ;
      private String A1059Contagem_Notas ;
      private String AV24Contagem_Notas ;
      private String A949Contagem_SistemaCoord ;
      private String AV16Contagem_SistemaCoord ;
      private String A952ContagemItem_TipoUnidade ;
      private String A958ContagemItem_Funcao ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Contagem_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P006H2_A940Contagem_SistemaCod ;
      private bool[] P006H2_n940Contagem_SistemaCod ;
      private int[] P006H2_A192Contagem_Codigo ;
      private DateTime[] P006H2_A197Contagem_DataCriacao ;
      private String[] P006H2_A196Contagem_Tipo ;
      private bool[] P006H2_n196Contagem_Tipo ;
      private String[] P006H2_A941Contagem_SistemaSigla ;
      private bool[] P006H2_n941Contagem_SistemaSigla ;
      private String[] P006H2_A949Contagem_SistemaCoord ;
      private bool[] P006H2_n949Contagem_SistemaCoord ;
      private decimal[] P006H2_A947Contagem_Fator ;
      private bool[] P006H2_n947Contagem_Fator ;
      private decimal[] P006H2_A943Contagem_PFB ;
      private bool[] P006H2_n943Contagem_PFB ;
      private decimal[] P006H2_A944Contagem_PFL ;
      private bool[] P006H2_n944Contagem_PFL ;
      private String[] P006H2_A195Contagem_Tecnica ;
      private bool[] P006H2_n195Contagem_Tecnica ;
      private String[] P006H2_A1059Contagem_Notas ;
      private bool[] P006H2_n1059Contagem_Notas ;
      private int[] P006H3_A192Contagem_Codigo ;
      private short[] P006H3_A954ContagemItem_QtdINM ;
      private bool[] P006H3_n954ContagemItem_QtdINM ;
      private decimal[] P006H3_A951ContagemItem_PFL ;
      private bool[] P006H3_n951ContagemItem_PFL ;
      private decimal[] P006H3_A950ContagemItem_PFB ;
      private bool[] P006H3_n950ContagemItem_PFB ;
      private String[] P006H3_A955ContagemItem_CP ;
      private bool[] P006H3_n955ContagemItem_CP ;
      private short[] P006H3_A956ContagemItem_RA ;
      private bool[] P006H3_n956ContagemItem_RA ;
      private short[] P006H3_A957ContagemItem_DER ;
      private bool[] P006H3_n957ContagemItem_DER ;
      private String[] P006H3_A952ContagemItem_TipoUnidade ;
      private String[] P006H3_A958ContagemItem_Funcao ;
      private bool[] P006H3_n958ContagemItem_Funcao ;
      private int[] P006H3_A224ContagemItem_Lancamento ;
   }

   public class arel_planodecontagem__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP006H2 ;
          prmP006H2 = new Object[] {
          new Object[] {"@Contagem_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006H3 ;
          prmP006H3 = new Object[] {
          new Object[] {"@Contagem_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P006H2", "SELECT T1.[Contagem_SistemaCod] AS Contagem_SistemaCod, T1.[Contagem_Codigo], T1.[Contagem_DataCriacao], T1.[Contagem_Tipo], T2.[Sistema_Sigla] AS Contagem_SistemaSigla, T2.[Sistema_Coordenacao] AS Contagem_SistemaCoord, T1.[Contagem_Fator], T1.[Contagem_PFB], T1.[Contagem_PFL], T1.[Contagem_Tecnica], T1.[Contagem_Notas] FROM ([Contagem] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[Contagem_SistemaCod]) WHERE T1.[Contagem_Codigo] = @Contagem_Codigo ORDER BY T1.[Contagem_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006H2,1,0,false,true )
             ,new CursorDef("P006H3", "SELECT [Contagem_Codigo], [ContagemItem_QtdINM], [ContagemItem_PFL], [ContagemItem_PFB], [ContagemItem_CP], [ContagemItem_RA], [ContagemItem_DER], [ContagemItem_TipoUnidade], [ContagemItem_Funcao], [ContagemItem_Lancamento] FROM [ContagemItem] WITH (NOLOCK) WHERE [Contagem_Codigo] = @Contagem_Codigo ORDER BY [Contagem_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006H3,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 25) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((decimal[]) buf[10])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((decimal[]) buf[14])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getString(10, 1) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getLongVarchar(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((decimal[]) buf[5])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((short[]) buf[9])[0] = rslt.getShort(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((short[]) buf[11])[0] = rslt.getShort(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getVarchar(8) ;
                ((String[]) buf[14])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
