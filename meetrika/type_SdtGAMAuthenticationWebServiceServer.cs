/*
               File: type_SdtGAMAuthenticationWebServiceServer
        Description: GAMAuthenticationWebServiceServer
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:36:44.7
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtGAMAuthenticationWebServiceServer : GxUserType, IGxExternalObject
   {
      public SdtGAMAuthenticationWebServiceServer( )
      {
         initialize();
      }

      public SdtGAMAuthenticationWebServiceServer( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public String tostring( )
      {
         String returntostring ;
         if ( GAMAuthenticationWebServiceServer_externalReference == null )
         {
            GAMAuthenticationWebServiceServer_externalReference = new Artech.Security.GAMAuthenticationWebServiceServer(context);
         }
         returntostring = "";
         returntostring = (String)(GAMAuthenticationWebServiceServer_externalReference.ToString());
         return returntostring ;
      }

      public String gxTpr_Name
      {
         get {
            if ( GAMAuthenticationWebServiceServer_externalReference == null )
            {
               GAMAuthenticationWebServiceServer_externalReference = new Artech.Security.GAMAuthenticationWebServiceServer(context);
            }
            return GAMAuthenticationWebServiceServer_externalReference.Name ;
         }

         set {
            if ( GAMAuthenticationWebServiceServer_externalReference == null )
            {
               GAMAuthenticationWebServiceServer_externalReference = new Artech.Security.GAMAuthenticationWebServiceServer(context);
            }
            GAMAuthenticationWebServiceServer_externalReference.Name = value;
         }

      }

      public int gxTpr_Port
      {
         get {
            if ( GAMAuthenticationWebServiceServer_externalReference == null )
            {
               GAMAuthenticationWebServiceServer_externalReference = new Artech.Security.GAMAuthenticationWebServiceServer(context);
            }
            return GAMAuthenticationWebServiceServer_externalReference.Port ;
         }

         set {
            if ( GAMAuthenticationWebServiceServer_externalReference == null )
            {
               GAMAuthenticationWebServiceServer_externalReference = new Artech.Security.GAMAuthenticationWebServiceServer(context);
            }
            GAMAuthenticationWebServiceServer_externalReference.Port = value;
         }

      }

      public String gxTpr_Baseurl
      {
         get {
            if ( GAMAuthenticationWebServiceServer_externalReference == null )
            {
               GAMAuthenticationWebServiceServer_externalReference = new Artech.Security.GAMAuthenticationWebServiceServer(context);
            }
            return GAMAuthenticationWebServiceServer_externalReference.BaseURL ;
         }

         set {
            if ( GAMAuthenticationWebServiceServer_externalReference == null )
            {
               GAMAuthenticationWebServiceServer_externalReference = new Artech.Security.GAMAuthenticationWebServiceServer(context);
            }
            GAMAuthenticationWebServiceServer_externalReference.BaseURL = value;
         }

      }

      public short gxTpr_Secureprotocol
      {
         get {
            if ( GAMAuthenticationWebServiceServer_externalReference == null )
            {
               GAMAuthenticationWebServiceServer_externalReference = new Artech.Security.GAMAuthenticationWebServiceServer(context);
            }
            return GAMAuthenticationWebServiceServer_externalReference.SecureProtocol ;
         }

         set {
            if ( GAMAuthenticationWebServiceServer_externalReference == null )
            {
               GAMAuthenticationWebServiceServer_externalReference = new Artech.Security.GAMAuthenticationWebServiceServer(context);
            }
            GAMAuthenticationWebServiceServer_externalReference.SecureProtocol = value;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( GAMAuthenticationWebServiceServer_externalReference == null )
            {
               GAMAuthenticationWebServiceServer_externalReference = new Artech.Security.GAMAuthenticationWebServiceServer(context);
            }
            return GAMAuthenticationWebServiceServer_externalReference ;
         }

         set {
            GAMAuthenticationWebServiceServer_externalReference = (Artech.Security.GAMAuthenticationWebServiceServer)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected Artech.Security.GAMAuthenticationWebServiceServer GAMAuthenticationWebServiceServer_externalReference=null ;
   }

}
