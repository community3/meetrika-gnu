/*
               File: REL_ContagemTA
        Description: Stub for REL_ContagemTA
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:14:19.61
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class rel_contagemta : GXProcedure
   {
      public rel_contagemta( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public rel_contagemta( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratada_AreaTrabalhoCod ,
                           short aP1_ContagemResultado_StatusCnt ,
                           String aP2_GridStateXML ,
                           int aP3_ContagemResultado_LoteAceite ,
                           int aP4_ContagemResultado_SS )
      {
         this.AV2Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         this.AV3ContagemResultado_StatusCnt = aP1_ContagemResultado_StatusCnt;
         this.AV4GridStateXML = aP2_GridStateXML;
         this.AV5ContagemResultado_LoteAceite = aP3_ContagemResultado_LoteAceite;
         this.AV6ContagemResultado_SS = aP4_ContagemResultado_SS;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_Contratada_AreaTrabalhoCod ,
                                 short aP1_ContagemResultado_StatusCnt ,
                                 String aP2_GridStateXML ,
                                 int aP3_ContagemResultado_LoteAceite ,
                                 int aP4_ContagemResultado_SS )
      {
         rel_contagemta objrel_contagemta;
         objrel_contagemta = new rel_contagemta();
         objrel_contagemta.AV2Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         objrel_contagemta.AV3ContagemResultado_StatusCnt = aP1_ContagemResultado_StatusCnt;
         objrel_contagemta.AV4GridStateXML = aP2_GridStateXML;
         objrel_contagemta.AV5ContagemResultado_LoteAceite = aP3_ContagemResultado_LoteAceite;
         objrel_contagemta.AV6ContagemResultado_SS = aP4_ContagemResultado_SS;
         objrel_contagemta.context.SetSubmitInitialConfig(context);
         objrel_contagemta.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objrel_contagemta);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((rel_contagemta)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         args = new Object[] {(int)AV2Contratada_AreaTrabalhoCod,(short)AV3ContagemResultado_StatusCnt,(String)AV4GridStateXML,(int)AV5ContagemResultado_LoteAceite,(int)AV6ContagemResultado_SS} ;
         ClassLoader.Execute("arel_contagemta","GeneXus.Programs.arel_contagemta", new Object[] {context }, "execute", args);
         if ( ( args != null ) && ( args.Length == 5 ) )
         {
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV3ContagemResultado_StatusCnt ;
      private int AV2Contratada_AreaTrabalhoCod ;
      private int AV5ContagemResultado_LoteAceite ;
      private int AV6ContagemResultado_SS ;
      private String AV4GridStateXML ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private Object[] args ;
   }

}
