/*
               File: PRC_GamUserDataAtualizacao
        Description: PRC_Gam User Data Atualizacao
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:10:6.78
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_gamuserdataatualizacao : GXProcedure
   {
      public prc_gamuserdataatualizacao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_gamuserdataatualizacao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Usuario_Codigo ,
                           out DateTime aP1_Usuario_DataAlteracao )
      {
         this.AV8Usuario_Codigo = aP0_Usuario_Codigo;
         this.AV12Usuario_DataAlteracao = DateTime.MinValue ;
         initialize();
         executePrivate();
         aP1_Usuario_DataAlteracao=this.AV12Usuario_DataAlteracao;
      }

      public DateTime executeUdp( int aP0_Usuario_Codigo )
      {
         this.AV8Usuario_Codigo = aP0_Usuario_Codigo;
         this.AV12Usuario_DataAlteracao = DateTime.MinValue ;
         initialize();
         executePrivate();
         aP1_Usuario_DataAlteracao=this.AV12Usuario_DataAlteracao;
         return AV12Usuario_DataAlteracao ;
      }

      public void executeSubmit( int aP0_Usuario_Codigo ,
                                 out DateTime aP1_Usuario_DataAlteracao )
      {
         prc_gamuserdataatualizacao objprc_gamuserdataatualizacao;
         objprc_gamuserdataatualizacao = new prc_gamuserdataatualizacao();
         objprc_gamuserdataatualizacao.AV8Usuario_Codigo = aP0_Usuario_Codigo;
         objprc_gamuserdataatualizacao.AV12Usuario_DataAlteracao = DateTime.MinValue ;
         objprc_gamuserdataatualizacao.context.SetSubmitInitialConfig(context);
         objprc_gamuserdataatualizacao.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_gamuserdataatualizacao);
         aP1_Usuario_DataAlteracao=this.AV12Usuario_DataAlteracao;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_gamuserdataatualizacao)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00DB2 */
         pr_default.execute(0, new Object[] {AV8Usuario_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1Usuario_Codigo = P00DB2_A1Usuario_Codigo[0];
            A341Usuario_UserGamGuid = P00DB2_A341Usuario_UserGamGuid[0];
            AV9GamUser.load( A341Usuario_UserGamGuid);
            AV12Usuario_DataAlteracao = AV9GamUser.gxTpr_Dateupdated;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00DB2_A1Usuario_Codigo = new int[1] ;
         P00DB2_A341Usuario_UserGamGuid = new String[] {""} ;
         A341Usuario_UserGamGuid = "";
         AV9GamUser = new SdtGAMUser(context);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_gamuserdataatualizacao__default(),
            new Object[][] {
                new Object[] {
               P00DB2_A1Usuario_Codigo, P00DB2_A341Usuario_UserGamGuid
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV8Usuario_Codigo ;
      private int A1Usuario_Codigo ;
      private String scmdbuf ;
      private String A341Usuario_UserGamGuid ;
      private DateTime AV12Usuario_DataAlteracao ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00DB2_A1Usuario_Codigo ;
      private String[] P00DB2_A341Usuario_UserGamGuid ;
      private DateTime aP1_Usuario_DataAlteracao ;
      private SdtGAMUser AV9GamUser ;
   }

   public class prc_gamuserdataatualizacao__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00DB2 ;
          prmP00DB2 = new Object[] {
          new Object[] {"@AV8Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00DB2", "SELECT [Usuario_Codigo], [Usuario_UserGamGuid] FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @AV8Usuario_Codigo ORDER BY [Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00DB2,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 40) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
