/*
               File: type_SdtContratoAuxiliar
        Description: Auxiliar
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:30:0.56
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "ContratoAuxiliar" )]
   [XmlType(TypeName =  "ContratoAuxiliar" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtContratoAuxiliar : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContratoAuxiliar( )
      {
         /* Constructor for serialization */
         gxTv_SdtContratoAuxiliar_Mode = "";
      }

      public SdtContratoAuxiliar( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV1824ContratoAuxiliar_ContratoCod ,
                        int AV1825ContratoAuxiliar_UsuarioCod )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV1824ContratoAuxiliar_ContratoCod,(int)AV1825ContratoAuxiliar_UsuarioCod});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"ContratoAuxiliar_ContratoCod", typeof(int)}, new Object[]{"ContratoAuxiliar_UsuarioCod", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "ContratoAuxiliar");
         metadata.Set("BT", "ContratoAuxiliar");
         metadata.Set("PK", "[ \"ContratoAuxiliar_ContratoCod\",\"ContratoAuxiliar_UsuarioCod\" ]");
         metadata.Set("PKAssigned", "[ \"ContratoAuxiliar_ContratoCod\",\"ContratoAuxiliar_UsuarioCod\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"Contrato_Codigo\" ],\"FKMap\":[ \"ContratoAuxiliar_ContratoCod-Contrato_Codigo\" ] },{ \"FK\":[ \"Usuario_Codigo\" ],\"FKMap\":[ \"ContratoAuxiliar_UsuarioCod-Usuario_Codigo\" ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoauxiliar_contratocod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratoauxiliar_usuariocod_Z" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtContratoAuxiliar deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtContratoAuxiliar)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtContratoAuxiliar obj ;
         obj = this;
         obj.gxTpr_Contratoauxiliar_contratocod = deserialized.gxTpr_Contratoauxiliar_contratocod;
         obj.gxTpr_Contratoauxiliar_usuariocod = deserialized.gxTpr_Contratoauxiliar_usuariocod;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Contratoauxiliar_contratocod_Z = deserialized.gxTpr_Contratoauxiliar_contratocod_Z;
         obj.gxTpr_Contratoauxiliar_usuariocod_Z = deserialized.gxTpr_Contratoauxiliar_usuariocod_Z;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoAuxiliar_ContratoCod") )
               {
                  gxTv_SdtContratoAuxiliar_Contratoauxiliar_contratocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoAuxiliar_UsuarioCod") )
               {
                  gxTv_SdtContratoAuxiliar_Contratoauxiliar_usuariocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtContratoAuxiliar_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtContratoAuxiliar_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoAuxiliar_ContratoCod_Z") )
               {
                  gxTv_SdtContratoAuxiliar_Contratoauxiliar_contratocod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratoAuxiliar_UsuarioCod_Z") )
               {
                  gxTv_SdtContratoAuxiliar_Contratoauxiliar_usuariocod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "ContratoAuxiliar";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("ContratoAuxiliar_ContratoCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoAuxiliar_Contratoauxiliar_contratocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContratoAuxiliar_UsuarioCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoAuxiliar_Contratoauxiliar_usuariocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtContratoAuxiliar_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoAuxiliar_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratoAuxiliar_ContratoCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoAuxiliar_Contratoauxiliar_contratocod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContratoAuxiliar_UsuarioCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratoAuxiliar_Contratoauxiliar_usuariocod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("ContratoAuxiliar_ContratoCod", gxTv_SdtContratoAuxiliar_Contratoauxiliar_contratocod, false);
         AddObjectProperty("ContratoAuxiliar_UsuarioCod", gxTv_SdtContratoAuxiliar_Contratoauxiliar_usuariocod, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtContratoAuxiliar_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtContratoAuxiliar_Initialized, false);
            AddObjectProperty("ContratoAuxiliar_ContratoCod_Z", gxTv_SdtContratoAuxiliar_Contratoauxiliar_contratocod_Z, false);
            AddObjectProperty("ContratoAuxiliar_UsuarioCod_Z", gxTv_SdtContratoAuxiliar_Contratoauxiliar_usuariocod_Z, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "ContratoAuxiliar_ContratoCod" )]
      [  XmlElement( ElementName = "ContratoAuxiliar_ContratoCod"   )]
      public int gxTpr_Contratoauxiliar_contratocod
      {
         get {
            return gxTv_SdtContratoAuxiliar_Contratoauxiliar_contratocod ;
         }

         set {
            if ( gxTv_SdtContratoAuxiliar_Contratoauxiliar_contratocod != value )
            {
               gxTv_SdtContratoAuxiliar_Mode = "INS";
               this.gxTv_SdtContratoAuxiliar_Contratoauxiliar_contratocod_Z_SetNull( );
               this.gxTv_SdtContratoAuxiliar_Contratoauxiliar_usuariocod_Z_SetNull( );
            }
            gxTv_SdtContratoAuxiliar_Contratoauxiliar_contratocod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContratoAuxiliar_UsuarioCod" )]
      [  XmlElement( ElementName = "ContratoAuxiliar_UsuarioCod"   )]
      public int gxTpr_Contratoauxiliar_usuariocod
      {
         get {
            return gxTv_SdtContratoAuxiliar_Contratoauxiliar_usuariocod ;
         }

         set {
            if ( gxTv_SdtContratoAuxiliar_Contratoauxiliar_usuariocod != value )
            {
               gxTv_SdtContratoAuxiliar_Mode = "INS";
               this.gxTv_SdtContratoAuxiliar_Contratoauxiliar_contratocod_Z_SetNull( );
               this.gxTv_SdtContratoAuxiliar_Contratoauxiliar_usuariocod_Z_SetNull( );
            }
            gxTv_SdtContratoAuxiliar_Contratoauxiliar_usuariocod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtContratoAuxiliar_Mode ;
         }

         set {
            gxTv_SdtContratoAuxiliar_Mode = (String)(value);
         }

      }

      public void gxTv_SdtContratoAuxiliar_Mode_SetNull( )
      {
         gxTv_SdtContratoAuxiliar_Mode = "";
         return  ;
      }

      public bool gxTv_SdtContratoAuxiliar_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtContratoAuxiliar_Initialized ;
         }

         set {
            gxTv_SdtContratoAuxiliar_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtContratoAuxiliar_Initialized_SetNull( )
      {
         gxTv_SdtContratoAuxiliar_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtContratoAuxiliar_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoAuxiliar_ContratoCod_Z" )]
      [  XmlElement( ElementName = "ContratoAuxiliar_ContratoCod_Z"   )]
      public int gxTpr_Contratoauxiliar_contratocod_Z
      {
         get {
            return gxTv_SdtContratoAuxiliar_Contratoauxiliar_contratocod_Z ;
         }

         set {
            gxTv_SdtContratoAuxiliar_Contratoauxiliar_contratocod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratoAuxiliar_Contratoauxiliar_contratocod_Z_SetNull( )
      {
         gxTv_SdtContratoAuxiliar_Contratoauxiliar_contratocod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoAuxiliar_Contratoauxiliar_contratocod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratoAuxiliar_UsuarioCod_Z" )]
      [  XmlElement( ElementName = "ContratoAuxiliar_UsuarioCod_Z"   )]
      public int gxTpr_Contratoauxiliar_usuariocod_Z
      {
         get {
            return gxTv_SdtContratoAuxiliar_Contratoauxiliar_usuariocod_Z ;
         }

         set {
            gxTv_SdtContratoAuxiliar_Contratoauxiliar_usuariocod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratoAuxiliar_Contratoauxiliar_usuariocod_Z_SetNull( )
      {
         gxTv_SdtContratoAuxiliar_Contratoauxiliar_usuariocod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratoAuxiliar_Contratoauxiliar_usuariocod_Z_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtContratoAuxiliar_Mode = "";
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "contratoauxiliar", "GeneXus.Programs.contratoauxiliar_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtContratoAuxiliar_Initialized ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtContratoAuxiliar_Contratoauxiliar_contratocod ;
      private int gxTv_SdtContratoAuxiliar_Contratoauxiliar_usuariocod ;
      private int gxTv_SdtContratoAuxiliar_Contratoauxiliar_contratocod_Z ;
      private int gxTv_SdtContratoAuxiliar_Contratoauxiliar_usuariocod_Z ;
      private String gxTv_SdtContratoAuxiliar_Mode ;
      private String sTagName ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"ContratoAuxiliar", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtContratoAuxiliar_RESTInterface : GxGenericCollectionItem<SdtContratoAuxiliar>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContratoAuxiliar_RESTInterface( ) : base()
      {
      }

      public SdtContratoAuxiliar_RESTInterface( SdtContratoAuxiliar psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "ContratoAuxiliar_ContratoCod" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratoauxiliar_contratocod
      {
         get {
            return sdt.gxTpr_Contratoauxiliar_contratocod ;
         }

         set {
            sdt.gxTpr_Contratoauxiliar_contratocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratoAuxiliar_UsuarioCod" , Order = 1 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratoauxiliar_usuariocod
      {
         get {
            return sdt.gxTpr_Contratoauxiliar_usuariocod ;
         }

         set {
            sdt.gxTpr_Contratoauxiliar_usuariocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      public SdtContratoAuxiliar sdt
      {
         get {
            return (SdtContratoAuxiliar)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtContratoAuxiliar() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 6 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
