/*
               File: PRC_DltServico
        Description: Dlt Servico
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:9:37.91
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_dltservico : GXProcedure
   {
      public prc_dltservico( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_dltservico( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Servico_Codigo )
      {
         this.A155Servico_Codigo = aP0_Servico_Codigo;
         initialize();
         executePrivate();
         aP0_Servico_Codigo=this.A155Servico_Codigo;
      }

      public int executeUdp( )
      {
         this.A155Servico_Codigo = aP0_Servico_Codigo;
         initialize();
         executePrivate();
         aP0_Servico_Codigo=this.A155Servico_Codigo;
         return A155Servico_Codigo ;
      }

      public void executeSubmit( ref int aP0_Servico_Codigo )
      {
         prc_dltservico objprc_dltservico;
         objprc_dltservico = new prc_dltservico();
         objprc_dltservico.A155Servico_Codigo = aP0_Servico_Codigo;
         objprc_dltservico.context.SetSubmitInitialConfig(context);
         objprc_dltservico.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_dltservico);
         aP0_Servico_Codigo=this.A155Servico_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_dltservico)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00BD2 */
         pr_default.execute(0, new Object[] {A155Servico_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            /* Optimized DELETE. */
            /* Using cursor P00BD3 */
            pr_default.execute(1, new Object[] {A155Servico_Codigo});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("UsuarioServicos") ;
            /* End optimized DELETE. */
            /* Optimized DELETE. */
            /* Using cursor P00BD4 */
            pr_default.execute(2, new Object[] {A155Servico_Codigo});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("ServicoResponsavel") ;
            /* End optimized DELETE. */
            /* Using cursor P00BD5 */
            pr_default.execute(3, new Object[] {A155Servico_Codigo});
            pr_default.close(3);
            dsDefault.SmartCacheProvider.SetUpdated("Servico") ;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_DltServico");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00BD2_A155Servico_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_dltservico__default(),
            new Object[][] {
                new Object[] {
               P00BD2_A155Servico_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A155Servico_Codigo ;
      private String scmdbuf ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Servico_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00BD2_A155Servico_Codigo ;
   }

   public class prc_dltservico__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
         ,new UpdateCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00BD2 ;
          prmP00BD2 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00BD3 ;
          prmP00BD3 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00BD4 ;
          prmP00BD4 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00BD5 ;
          prmP00BD5 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00BD2", "SELECT TOP 1 [Servico_Codigo] FROM [Servico] WITH (UPDLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ORDER BY [Servico_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00BD2,1,0,true,true )
             ,new CursorDef("P00BD3", "DELETE FROM [UsuarioServicos]  WHERE [UsuarioServicos_ServicoCod] = @Servico_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00BD3)
             ,new CursorDef("P00BD4", "DELETE FROM [ServicoResponsavel]  WHERE [ServicoResponsavel_SrvCod] = @Servico_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00BD4)
             ,new CursorDef("P00BD5", "DELETE FROM [Servico]  WHERE [Servico_Codigo] = @Servico_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00BD5)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
