/*
               File: ContagemItemParecer
        Description: Contagem Item Parecer
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:19:22.49
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagemitemparecer : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_15") == 0 )
         {
            A246ContagemItemParecer_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A246ContagemItemParecer_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A246ContagemItemParecer_UsuarioCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_15( A246ContagemItemParecer_UsuarioCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_16") == 0 )
         {
            A247ContagemItemParecer_UsuarioPessoaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n247ContagemItemParecer_UsuarioPessoaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A247ContagemItemParecer_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A247ContagemItemParecer_UsuarioPessoaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_16( A247ContagemItemParecer_UsuarioPessoaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7ContagemItemParecer_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContagemItemParecer_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemItemParecer_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMITEMPARECER_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContagemItemParecer_Codigo), "ZZZZZ9")));
               A224ContagemItem_Lancamento = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Contagem Item Parecer", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtContagemItemParecer_Data_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contagemitemparecer( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contagemitemparecer( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_ContagemItemParecer_Codigo ,
                           ref int aP2_ContagemItem_Lancamento )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7ContagemItemParecer_Codigo = aP1_ContagemItemParecer_Codigo;
         this.A224ContagemItem_Lancamento = aP2_ContagemItem_Lancamento;
         executePrivate();
         aP2_ContagemItem_Lancamento=this.A224ContagemItem_Lancamento;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_1946( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_1946e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItemParecer_UsuarioPessoaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A247ContagemItemParecer_UsuarioPessoaCod), 6, 0, ",", "")), ((edtContagemItemParecer_UsuarioPessoaCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A247ContagemItemParecer_UsuarioPessoaCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A247ContagemItemParecer_UsuarioPessoaCod), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItemParecer_UsuarioPessoaCod_Jsonclick, 0, "Attribute", "", "", "", edtContagemItemParecer_UsuarioPessoaCod_Visible, edtContagemItemParecer_UsuarioPessoaCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemItemParecer.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_1946( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_1946( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_1946e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_46_1946( true) ;
         }
         return  ;
      }

      protected void wb_table3_46_1946e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_1946e( true) ;
         }
         else
         {
            wb_table1_2_1946e( false) ;
         }
      }

      protected void wb_table3_46_1946( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemItemParecer.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemItemParecer.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemItemParecer.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_46_1946e( true) ;
         }
         else
         {
            wb_table3_46_1946e( false) ;
         }
      }

      protected void wb_table2_5_1946( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_1946( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_1946e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_1946e( true) ;
         }
         else
         {
            wb_table2_5_1946e( false) ;
         }
      }

      protected void wb_table4_13_1946( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitemparecer_codigo_Internalname, "Sequencial", "", "", lblTextblockcontagemitemparecer_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemParecer.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItemParecer_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A243ContagemItemParecer_Codigo), 6, 0, ",", "")), ((edtContagemItemParecer_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A243ContagemItemParecer_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A243ContagemItemParecer_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItemParecer_Codigo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemItemParecer_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemItemParecer.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_lancamento_Internalname, "Lan�amento", "", "", lblTextblockcontagemitem_lancamento_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemParecer.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemItem_Lancamento_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A224ContagemItem_Lancamento), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A224ContagemItem_Lancamento), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,23);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_Lancamento_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemItem_Lancamento_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemItemParecer.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitemparecer_data_Internalname, "Data", "", "", lblTextblockcontagemitemparecer_data_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemParecer.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtContagemItemParecer_Data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContagemItemParecer_Data_Internalname, context.localUtil.TToC( A245ContagemItemParecer_Data, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A245ContagemItemParecer_Data, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,28);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItemParecer_Data_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, edtContagemItemParecer_Data_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "DataHora", "right", false, "HLP_ContagemItemParecer.htm");
            GxWebStd.gx_bitmap( context, edtContagemItemParecer_Data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContagemItemParecer_Data_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContagemItemParecer.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitemparecer_comentario_Internalname, "Coment�rio", "", "", lblTextblockcontagemitemparecer_comentario_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemParecer.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContagemItemParecer_Comentario_Internalname, A244ContagemItemParecer_Comentario, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", 0, 1, edtContagemItemParecer_Comentario_Enabled, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "5000", -1, "", "", -1, true, "Comentario", "HLP_ContagemItemParecer.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitemparecer_usuariocod_Internalname, "Usu�rio", "", "", lblTextblockcontagemitemparecer_usuariocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemParecer.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table5_38_1946( true) ;
         }
         return  ;
      }

      protected void wb_table5_38_1946e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_1946e( true) ;
         }
         else
         {
            wb_table4_13_1946e( false) ;
         }
      }

      protected void wb_table5_38_1946( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontagemitemparecer_usuariocod_Internalname, tblTablemergedcontagemitemparecer_usuariocod_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemItemParecer_UsuarioCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A246ContagemItemParecer_UsuarioCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A246ContagemItemParecer_UsuarioCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,41);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItemParecer_UsuarioCod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemItemParecer_UsuarioCod_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemItemParecer.htm");
            /* Static images/pictures */
            ClassString = "gx-prompt Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgprompt_246_Internalname, context.GetImagePath( "f5b04895-0024-488b-8e3b-b687ca4598ee", "", context.GetTheme( )), imgprompt_246_Link, "", "", context.GetTheme( ), imgprompt_246_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, false, false, "HLP_ContagemItemParecer.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItemParecer_UsuarioPessoaNom_Internalname, StringUtil.RTrim( A248ContagemItemParecer_UsuarioPessoaNom), StringUtil.RTrim( context.localUtil.Format( A248ContagemItemParecer_UsuarioPessoaNom, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItemParecer_UsuarioPessoaNom_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemItemParecer_UsuarioPessoaNom_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_ContagemItemParecer.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_38_1946e( true) ;
         }
         else
         {
            wb_table5_38_1946e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11192 */
         E11192 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A243ContagemItemParecer_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagemItemParecer_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A243ContagemItemParecer_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A243ContagemItemParecer_Codigo), 6, 0)));
               A224ContagemItem_Lancamento = (int)(context.localUtil.CToN( cgiGet( edtContagemItem_Lancamento_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
               if ( context.localUtil.VCDateTime( cgiGet( edtContagemItemParecer_Data_Internalname), 2, 0) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"D/Hora do Parecer"}), 1, "CONTAGEMITEMPARECER_DATA");
                  AnyError = 1;
                  GX_FocusControl = edtContagemItemParecer_Data_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A245ContagemItemParecer_Data = (DateTime)(DateTime.MinValue);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A245ContagemItemParecer_Data", context.localUtil.TToC( A245ContagemItemParecer_Data, 8, 5, 0, 3, "/", ":", " "));
               }
               else
               {
                  A245ContagemItemParecer_Data = context.localUtil.CToT( cgiGet( edtContagemItemParecer_Data_Internalname));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A245ContagemItemParecer_Data", context.localUtil.TToC( A245ContagemItemParecer_Data, 8, 5, 0, 3, "/", ":", " "));
               }
               A244ContagemItemParecer_Comentario = cgiGet( edtContagemItemParecer_Comentario_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A244ContagemItemParecer_Comentario", A244ContagemItemParecer_Comentario);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemItemParecer_UsuarioCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemItemParecer_UsuarioCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMITEMPARECER_USUARIOCOD");
                  AnyError = 1;
                  GX_FocusControl = edtContagemItemParecer_UsuarioCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A246ContagemItemParecer_UsuarioCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A246ContagemItemParecer_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A246ContagemItemParecer_UsuarioCod), 6, 0)));
               }
               else
               {
                  A246ContagemItemParecer_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( edtContagemItemParecer_UsuarioCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A246ContagemItemParecer_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A246ContagemItemParecer_UsuarioCod), 6, 0)));
               }
               A248ContagemItemParecer_UsuarioPessoaNom = StringUtil.Upper( cgiGet( edtContagemItemParecer_UsuarioPessoaNom_Internalname));
               n248ContagemItemParecer_UsuarioPessoaNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A248ContagemItemParecer_UsuarioPessoaNom", A248ContagemItemParecer_UsuarioPessoaNom);
               A247ContagemItemParecer_UsuarioPessoaCod = (int)(context.localUtil.CToN( cgiGet( edtContagemItemParecer_UsuarioPessoaCod_Internalname), ",", "."));
               n247ContagemItemParecer_UsuarioPessoaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A247ContagemItemParecer_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A247ContagemItemParecer_UsuarioPessoaCod), 6, 0)));
               /* Read saved values. */
               Z243ContagemItemParecer_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z243ContagemItemParecer_Codigo"), ",", "."));
               Z245ContagemItemParecer_Data = context.localUtil.CToT( cgiGet( "Z245ContagemItemParecer_Data"), 0);
               Z246ContagemItemParecer_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( "Z246ContagemItemParecer_UsuarioCod"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N224ContagemItem_Lancamento = (int)(context.localUtil.CToN( cgiGet( "N224ContagemItem_Lancamento"), ",", "."));
               N246ContagemItemParecer_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( "N246ContagemItemParecer_UsuarioCod"), ",", "."));
               AV7ContagemItemParecer_Codigo = (int)(context.localUtil.CToN( cgiGet( "vCONTAGEMITEMPARECER_CODIGO"), ",", "."));
               AV11Insert_ContagemItem_Lancamento = (int)(context.localUtil.CToN( cgiGet( "vINSERT_CONTAGEMITEM_LANCAMENTO"), ",", "."));
               AV12Insert_ContagemItemParecer_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_CONTAGEMITEMPARECER_USUARIOCOD"), ",", "."));
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               AV15Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "ContagemItemParecer";
               A243ContagemItemParecer_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagemItemParecer_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A243ContagemItemParecer_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A243ContagemItemParecer_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A243ContagemItemParecer_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A243ContagemItemParecer_Codigo != Z243ContagemItemParecer_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("contagemitemparecer:[SecurityCheckFailed value for]"+"ContagemItemParecer_Codigo:"+context.localUtil.Format( (decimal)(A243ContagemItemParecer_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("contagemitemparecer:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A243ContagemItemParecer_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A243ContagemItemParecer_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A243ContagemItemParecer_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode46 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode46;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound46 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_190( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "CONTAGEMITEMPARECER_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtContagemItemParecer_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11192 */
                           E11192 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12192 */
                           E12192 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E12192 */
            E12192 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll1946( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes1946( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_190( )
      {
         BeforeValidate1946( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls1946( ) ;
            }
            else
            {
               CheckExtendedTable1946( ) ;
               CloseExtendedTableCursors1946( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption190( )
      {
      }

      protected void E11192( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV15Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV16GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16GXV1), 8, 0)));
            while ( AV16GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV13TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV16GXV1));
               if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "ContagemItem_Lancamento") == 0 )
               {
                  AV11Insert_ContagemItem_Lancamento = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_ContagemItem_Lancamento), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "ContagemItemParecer_UsuarioCod") == 0 )
               {
                  AV12Insert_ContagemItemParecer_UsuarioCod = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Insert_ContagemItemParecer_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Insert_ContagemItemParecer_UsuarioCod), 6, 0)));
               }
               AV16GXV1 = (int)(AV16GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16GXV1), 8, 0)));
            }
         }
         edtContagemItemParecer_UsuarioPessoaCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItemParecer_UsuarioPessoaCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItemParecer_UsuarioPessoaCod_Visible), 5, 0)));
      }

      protected void E12192( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwcontagemitemparecer.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {(int)A224ContagemItem_Lancamento});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM1946( short GX_JID )
      {
         if ( ( GX_JID == 13 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z245ContagemItemParecer_Data = T00193_A245ContagemItemParecer_Data[0];
               Z246ContagemItemParecer_UsuarioCod = T00193_A246ContagemItemParecer_UsuarioCod[0];
            }
            else
            {
               Z245ContagemItemParecer_Data = A245ContagemItemParecer_Data;
               Z246ContagemItemParecer_UsuarioCod = A246ContagemItemParecer_UsuarioCod;
            }
         }
         if ( GX_JID == -13 )
         {
            Z224ContagemItem_Lancamento = A224ContagemItem_Lancamento;
            Z243ContagemItemParecer_Codigo = A243ContagemItemParecer_Codigo;
            Z244ContagemItemParecer_Comentario = A244ContagemItemParecer_Comentario;
            Z245ContagemItemParecer_Data = A245ContagemItemParecer_Data;
            Z246ContagemItemParecer_UsuarioCod = A246ContagemItemParecer_UsuarioCod;
            Z247ContagemItemParecer_UsuarioPessoaCod = A247ContagemItemParecer_UsuarioPessoaCod;
            Z248ContagemItemParecer_UsuarioPessoaNom = A248ContagemItemParecer_UsuarioPessoaNom;
         }
      }

      protected void standaloneNotModal( )
      {
         edtContagemItemParecer_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItemParecer_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItemParecer_Codigo_Enabled), 5, 0)));
         AV15Pgmname = "ContagemItemParecer";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Pgmname", AV15Pgmname);
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         imgprompt_246_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"promptusuario.aspx"+"',["+"{Ctrl:gx.dom.el('"+"CONTAGEMITEMPARECER_USUARIOCOD"+"'), id:'"+"CONTAGEMITEMPARECER_USUARIOCOD"+"'"+",IOType:'inout'}"+","+"{Ctrl:gx.dom.el('"+"CONTAGEMITEMPARECER_USUARIOPESSOANOM"+"'), id:'"+"CONTAGEMITEMPARECER_USUARIOPESSOANOM"+"'"+",IOType:'inout'}"+"],"+"null"+","+"'', false"+","+"false"+");");
         edtContagemItemParecer_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItemParecer_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItemParecer_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7ContagemItemParecer_Codigo) )
         {
            A243ContagemItemParecer_Codigo = AV7ContagemItemParecer_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A243ContagemItemParecer_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A243ContagemItemParecer_Codigo), 6, 0)));
         }
         /* Using cursor T00194 */
         pr_default.execute(2, new Object[] {A224ContagemItem_Lancamento});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Item'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         pr_default.close(2);
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_ContagemItem_Lancamento) )
         {
            edtContagemItem_Lancamento_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_Lancamento_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_Lancamento_Enabled), 5, 0)));
         }
         else
         {
            edtContagemItem_Lancamento_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_Lancamento_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_Lancamento_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_ContagemItemParecer_UsuarioCod) )
         {
            edtContagemItemParecer_UsuarioCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItemParecer_UsuarioCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItemParecer_UsuarioCod_Enabled), 5, 0)));
         }
         else
         {
            edtContagemItemParecer_UsuarioCod_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItemParecer_UsuarioCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItemParecer_UsuarioCod_Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_ContagemItemParecer_UsuarioCod) )
         {
            A246ContagemItemParecer_UsuarioCod = AV12Insert_ContagemItemParecer_UsuarioCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A246ContagemItemParecer_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A246ContagemItemParecer_UsuarioCod), 6, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A246ContagemItemParecer_UsuarioCod) && ( Gx_BScreen == 0 ) )
            {
               A246ContagemItemParecer_UsuarioCod = AV8WWPContext.gxTpr_Userid;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A246ContagemItemParecer_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A246ContagemItemParecer_UsuarioCod), 6, 0)));
            }
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (DateTime.MinValue==A245ContagemItemParecer_Data) && ( Gx_BScreen == 0 ) )
         {
            A245ContagemItemParecer_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A245ContagemItemParecer_Data", context.localUtil.TToC( A245ContagemItemParecer_Data, 8, 5, 0, 3, "/", ":", " "));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T00195 */
            pr_default.execute(3, new Object[] {A246ContagemItemParecer_UsuarioCod});
            A247ContagemItemParecer_UsuarioPessoaCod = T00195_A247ContagemItemParecer_UsuarioPessoaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A247ContagemItemParecer_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A247ContagemItemParecer_UsuarioPessoaCod), 6, 0)));
            n247ContagemItemParecer_UsuarioPessoaCod = T00195_n247ContagemItemParecer_UsuarioPessoaCod[0];
            pr_default.close(3);
            /* Using cursor T00196 */
            pr_default.execute(4, new Object[] {n247ContagemItemParecer_UsuarioPessoaCod, A247ContagemItemParecer_UsuarioPessoaCod});
            A248ContagemItemParecer_UsuarioPessoaNom = T00196_A248ContagemItemParecer_UsuarioPessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A248ContagemItemParecer_UsuarioPessoaNom", A248ContagemItemParecer_UsuarioPessoaNom);
            n248ContagemItemParecer_UsuarioPessoaNom = T00196_n248ContagemItemParecer_UsuarioPessoaNom[0];
            pr_default.close(4);
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_ContagemItem_Lancamento) )
            {
               A224ContagemItem_Lancamento = AV11Insert_ContagemItem_Lancamento;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
            }
         }
      }

      protected void Load1946( )
      {
         /* Using cursor T00197 */
         pr_default.execute(5, new Object[] {A243ContagemItemParecer_Codigo, A224ContagemItem_Lancamento});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound46 = 1;
            A244ContagemItemParecer_Comentario = T00197_A244ContagemItemParecer_Comentario[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A244ContagemItemParecer_Comentario", A244ContagemItemParecer_Comentario);
            A245ContagemItemParecer_Data = T00197_A245ContagemItemParecer_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A245ContagemItemParecer_Data", context.localUtil.TToC( A245ContagemItemParecer_Data, 8, 5, 0, 3, "/", ":", " "));
            A248ContagemItemParecer_UsuarioPessoaNom = T00197_A248ContagemItemParecer_UsuarioPessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A248ContagemItemParecer_UsuarioPessoaNom", A248ContagemItemParecer_UsuarioPessoaNom);
            n248ContagemItemParecer_UsuarioPessoaNom = T00197_n248ContagemItemParecer_UsuarioPessoaNom[0];
            A246ContagemItemParecer_UsuarioCod = T00197_A246ContagemItemParecer_UsuarioCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A246ContagemItemParecer_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A246ContagemItemParecer_UsuarioCod), 6, 0)));
            A247ContagemItemParecer_UsuarioPessoaCod = T00197_A247ContagemItemParecer_UsuarioPessoaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A247ContagemItemParecer_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A247ContagemItemParecer_UsuarioPessoaCod), 6, 0)));
            n247ContagemItemParecer_UsuarioPessoaCod = T00197_n247ContagemItemParecer_UsuarioPessoaCod[0];
            ZM1946( -13) ;
         }
         pr_default.close(5);
         OnLoadActions1946( ) ;
      }

      protected void OnLoadActions1946( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_ContagemItem_Lancamento) )
         {
            A224ContagemItem_Lancamento = AV11Insert_ContagemItem_Lancamento;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
         }
      }

      protected void CheckExtendedTable1946( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_ContagemItem_Lancamento) )
         {
            A224ContagemItem_Lancamento = AV11Insert_ContagemItem_Lancamento;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
         }
         if ( ! ( (DateTime.MinValue==A245ContagemItemParecer_Data) || ( A245ContagemItemParecer_Data >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo D/Hora do Parecer fora do intervalo", "OutOfRange", 1, "CONTAGEMITEMPARECER_DATA");
            AnyError = 1;
            GX_FocusControl = edtContagemItemParecer_Data_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T00195 */
         pr_default.execute(3, new Object[] {A246ContagemItemParecer_UsuarioCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contagem Item Parecer_Usuario'.", "ForeignKeyNotFound", 1, "CONTAGEMITEMPARECER_USUARIOCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemItemParecer_UsuarioCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A247ContagemItemParecer_UsuarioPessoaCod = T00195_A247ContagemItemParecer_UsuarioPessoaCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A247ContagemItemParecer_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A247ContagemItemParecer_UsuarioPessoaCod), 6, 0)));
         n247ContagemItemParecer_UsuarioPessoaCod = T00195_n247ContagemItemParecer_UsuarioPessoaCod[0];
         pr_default.close(3);
         /* Using cursor T00196 */
         pr_default.execute(4, new Object[] {n247ContagemItemParecer_UsuarioPessoaCod, A247ContagemItemParecer_UsuarioPessoaCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A248ContagemItemParecer_UsuarioPessoaNom = T00196_A248ContagemItemParecer_UsuarioPessoaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A248ContagemItemParecer_UsuarioPessoaNom", A248ContagemItemParecer_UsuarioPessoaNom);
         n248ContagemItemParecer_UsuarioPessoaNom = T00196_n248ContagemItemParecer_UsuarioPessoaNom[0];
         pr_default.close(4);
      }

      protected void CloseExtendedTableCursors1946( )
      {
         pr_default.close(3);
         pr_default.close(4);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_15( int A246ContagemItemParecer_UsuarioCod )
      {
         /* Using cursor T00198 */
         pr_default.execute(6, new Object[] {A246ContagemItemParecer_UsuarioCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contagem Item Parecer_Usuario'.", "ForeignKeyNotFound", 1, "CONTAGEMITEMPARECER_USUARIOCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemItemParecer_UsuarioCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A247ContagemItemParecer_UsuarioPessoaCod = T00198_A247ContagemItemParecer_UsuarioPessoaCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A247ContagemItemParecer_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A247ContagemItemParecer_UsuarioPessoaCod), 6, 0)));
         n247ContagemItemParecer_UsuarioPessoaCod = T00198_n247ContagemItemParecer_UsuarioPessoaCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A247ContagemItemParecer_UsuarioPessoaCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void gxLoad_16( int A247ContagemItemParecer_UsuarioPessoaCod )
      {
         /* Using cursor T00199 */
         pr_default.execute(7, new Object[] {n247ContagemItemParecer_UsuarioPessoaCod, A247ContagemItemParecer_UsuarioPessoaCod});
         if ( (pr_default.getStatus(7) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A248ContagemItemParecer_UsuarioPessoaNom = T00199_A248ContagemItemParecer_UsuarioPessoaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A248ContagemItemParecer_UsuarioPessoaNom", A248ContagemItemParecer_UsuarioPessoaNom);
         n248ContagemItemParecer_UsuarioPessoaNom = T00199_n248ContagemItemParecer_UsuarioPessoaNom[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A248ContagemItemParecer_UsuarioPessoaNom))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(7) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(7);
      }

      protected void GetKey1946( )
      {
         /* Using cursor T001910 */
         pr_default.execute(8, new Object[] {A243ContagemItemParecer_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            RcdFound46 = 1;
         }
         else
         {
            RcdFound46 = 0;
         }
         pr_default.close(8);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00193 */
         pr_default.execute(1, new Object[] {A243ContagemItemParecer_Codigo});
         if ( (pr_default.getStatus(1) != 101) && ( T00193_A224ContagemItem_Lancamento[0] == A224ContagemItem_Lancamento ) )
         {
            ZM1946( 13) ;
            RcdFound46 = 1;
            A243ContagemItemParecer_Codigo = T00193_A243ContagemItemParecer_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A243ContagemItemParecer_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A243ContagemItemParecer_Codigo), 6, 0)));
            A244ContagemItemParecer_Comentario = T00193_A244ContagemItemParecer_Comentario[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A244ContagemItemParecer_Comentario", A244ContagemItemParecer_Comentario);
            A245ContagemItemParecer_Data = T00193_A245ContagemItemParecer_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A245ContagemItemParecer_Data", context.localUtil.TToC( A245ContagemItemParecer_Data, 8, 5, 0, 3, "/", ":", " "));
            A246ContagemItemParecer_UsuarioCod = T00193_A246ContagemItemParecer_UsuarioCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A246ContagemItemParecer_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A246ContagemItemParecer_UsuarioCod), 6, 0)));
            Z243ContagemItemParecer_Codigo = A243ContagemItemParecer_Codigo;
            sMode46 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load1946( ) ;
            if ( AnyError == 1 )
            {
               RcdFound46 = 0;
               InitializeNonKey1946( ) ;
            }
            Gx_mode = sMode46;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound46 = 0;
            InitializeNonKey1946( ) ;
            sMode46 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode46;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey1946( ) ;
         if ( RcdFound46 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound46 = 0;
         /* Using cursor T001911 */
         pr_default.execute(9, new Object[] {A243ContagemItemParecer_Codigo, A224ContagemItem_Lancamento});
         if ( (pr_default.getStatus(9) != 101) )
         {
            while ( (pr_default.getStatus(9) != 101) && ( ( T001911_A243ContagemItemParecer_Codigo[0] < A243ContagemItemParecer_Codigo ) ) && ( T001911_A224ContagemItem_Lancamento[0] == A224ContagemItem_Lancamento ) )
            {
               pr_default.readNext(9);
            }
            if ( (pr_default.getStatus(9) != 101) && ( ( T001911_A243ContagemItemParecer_Codigo[0] > A243ContagemItemParecer_Codigo ) ) && ( T001911_A224ContagemItem_Lancamento[0] == A224ContagemItem_Lancamento ) )
            {
               A243ContagemItemParecer_Codigo = T001911_A243ContagemItemParecer_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A243ContagemItemParecer_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A243ContagemItemParecer_Codigo), 6, 0)));
               RcdFound46 = 1;
            }
         }
         pr_default.close(9);
      }

      protected void move_previous( )
      {
         RcdFound46 = 0;
         /* Using cursor T001912 */
         pr_default.execute(10, new Object[] {A243ContagemItemParecer_Codigo, A224ContagemItem_Lancamento});
         if ( (pr_default.getStatus(10) != 101) )
         {
            while ( (pr_default.getStatus(10) != 101) && ( ( T001912_A243ContagemItemParecer_Codigo[0] > A243ContagemItemParecer_Codigo ) ) && ( T001912_A224ContagemItem_Lancamento[0] == A224ContagemItem_Lancamento ) )
            {
               pr_default.readNext(10);
            }
            if ( (pr_default.getStatus(10) != 101) && ( ( T001912_A243ContagemItemParecer_Codigo[0] < A243ContagemItemParecer_Codigo ) ) && ( T001912_A224ContagemItem_Lancamento[0] == A224ContagemItem_Lancamento ) )
            {
               A243ContagemItemParecer_Codigo = T001912_A243ContagemItemParecer_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A243ContagemItemParecer_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A243ContagemItemParecer_Codigo), 6, 0)));
               RcdFound46 = 1;
            }
         }
         pr_default.close(10);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey1946( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtContagemItemParecer_Data_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert1946( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound46 == 1 )
            {
               if ( A243ContagemItemParecer_Codigo != Z243ContagemItemParecer_Codigo )
               {
                  A243ContagemItemParecer_Codigo = Z243ContagemItemParecer_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A243ContagemItemParecer_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A243ContagemItemParecer_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CONTAGEMITEMPARECER_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContagemItemParecer_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtContagemItemParecer_Data_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update1946( ) ;
                  GX_FocusControl = edtContagemItemParecer_Data_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A243ContagemItemParecer_Codigo != Z243ContagemItemParecer_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtContagemItemParecer_Data_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert1946( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CONTAGEMITEMPARECER_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtContagemItemParecer_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtContagemItemParecer_Data_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert1946( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A243ContagemItemParecer_Codigo != Z243ContagemItemParecer_Codigo )
         {
            A243ContagemItemParecer_Codigo = Z243ContagemItemParecer_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A243ContagemItemParecer_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A243ContagemItemParecer_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CONTAGEMITEMPARECER_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemItemParecer_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtContagemItemParecer_Data_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency1946( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00192 */
            pr_default.execute(0, new Object[] {A243ContagemItemParecer_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemItemParecer"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z245ContagemItemParecer_Data != T00192_A245ContagemItemParecer_Data[0] ) || ( Z246ContagemItemParecer_UsuarioCod != T00192_A246ContagemItemParecer_UsuarioCod[0] ) )
            {
               if ( Z245ContagemItemParecer_Data != T00192_A245ContagemItemParecer_Data[0] )
               {
                  GXUtil.WriteLog("contagemitemparecer:[seudo value changed for attri]"+"ContagemItemParecer_Data");
                  GXUtil.WriteLogRaw("Old: ",Z245ContagemItemParecer_Data);
                  GXUtil.WriteLogRaw("Current: ",T00192_A245ContagemItemParecer_Data[0]);
               }
               if ( Z246ContagemItemParecer_UsuarioCod != T00192_A246ContagemItemParecer_UsuarioCod[0] )
               {
                  GXUtil.WriteLog("contagemitemparecer:[seudo value changed for attri]"+"ContagemItemParecer_UsuarioCod");
                  GXUtil.WriteLogRaw("Old: ",Z246ContagemItemParecer_UsuarioCod);
                  GXUtil.WriteLogRaw("Current: ",T00192_A246ContagemItemParecer_UsuarioCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContagemItemParecer"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert1946( )
      {
         BeforeValidate1946( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1946( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM1946( 0) ;
            CheckOptimisticConcurrency1946( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1946( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert1946( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001913 */
                     pr_default.execute(11, new Object[] {A224ContagemItem_Lancamento, A244ContagemItemParecer_Comentario, A245ContagemItemParecer_Data, A246ContagemItemParecer_UsuarioCod});
                     A243ContagemItemParecer_Codigo = T001913_A243ContagemItemParecer_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A243ContagemItemParecer_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A243ContagemItemParecer_Codigo), 6, 0)));
                     pr_default.close(11);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemItemParecer") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption190( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load1946( ) ;
            }
            EndLevel1946( ) ;
         }
         CloseExtendedTableCursors1946( ) ;
      }

      protected void Update1946( )
      {
         BeforeValidate1946( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1946( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1946( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1946( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate1946( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001914 */
                     pr_default.execute(12, new Object[] {A224ContagemItem_Lancamento, A244ContagemItemParecer_Comentario, A245ContagemItemParecer_Data, A246ContagemItemParecer_UsuarioCod, A243ContagemItemParecer_Codigo});
                     pr_default.close(12);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemItemParecer") ;
                     if ( (pr_default.getStatus(12) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemItemParecer"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate1946( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel1946( ) ;
         }
         CloseExtendedTableCursors1946( ) ;
      }

      protected void DeferredUpdate1946( )
      {
      }

      protected void delete( )
      {
         BeforeValidate1946( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1946( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls1946( ) ;
            AfterConfirm1946( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete1946( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T001915 */
                  pr_default.execute(13, new Object[] {A243ContagemItemParecer_Codigo});
                  pr_default.close(13);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemItemParecer") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode46 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel1946( ) ;
         Gx_mode = sMode46;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls1946( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T001916 */
            pr_default.execute(14, new Object[] {A246ContagemItemParecer_UsuarioCod});
            A247ContagemItemParecer_UsuarioPessoaCod = T001916_A247ContagemItemParecer_UsuarioPessoaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A247ContagemItemParecer_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A247ContagemItemParecer_UsuarioPessoaCod), 6, 0)));
            n247ContagemItemParecer_UsuarioPessoaCod = T001916_n247ContagemItemParecer_UsuarioPessoaCod[0];
            pr_default.close(14);
            /* Using cursor T001917 */
            pr_default.execute(15, new Object[] {n247ContagemItemParecer_UsuarioPessoaCod, A247ContagemItemParecer_UsuarioPessoaCod});
            A248ContagemItemParecer_UsuarioPessoaNom = T001917_A248ContagemItemParecer_UsuarioPessoaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A248ContagemItemParecer_UsuarioPessoaNom", A248ContagemItemParecer_UsuarioPessoaNom);
            n248ContagemItemParecer_UsuarioPessoaNom = T001917_n248ContagemItemParecer_UsuarioPessoaNom[0];
            pr_default.close(15);
         }
      }

      protected void EndLevel1946( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete1946( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(14);
            pr_default.close(15);
            context.CommitDataStores( "ContagemItemParecer");
            if ( AnyError == 0 )
            {
               ConfirmValues190( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(14);
            pr_default.close(15);
            context.RollbackDataStores( "ContagemItemParecer");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart1946( )
      {
         /* Scan By routine */
         /* Using cursor T001918 */
         pr_default.execute(16, new Object[] {A224ContagemItem_Lancamento});
         RcdFound46 = 0;
         if ( (pr_default.getStatus(16) != 101) )
         {
            RcdFound46 = 1;
            A243ContagemItemParecer_Codigo = T001918_A243ContagemItemParecer_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A243ContagemItemParecer_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A243ContagemItemParecer_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext1946( )
      {
         /* Scan next routine */
         pr_default.readNext(16);
         RcdFound46 = 0;
         if ( (pr_default.getStatus(16) != 101) )
         {
            RcdFound46 = 1;
            A243ContagemItemParecer_Codigo = T001918_A243ContagemItemParecer_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A243ContagemItemParecer_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A243ContagemItemParecer_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd1946( )
      {
         pr_default.close(16);
      }

      protected void AfterConfirm1946( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert1946( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate1946( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete1946( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete1946( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate1946( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes1946( )
      {
         edtContagemItemParecer_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItemParecer_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItemParecer_Codigo_Enabled), 5, 0)));
         edtContagemItem_Lancamento_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_Lancamento_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_Lancamento_Enabled), 5, 0)));
         edtContagemItemParecer_Data_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItemParecer_Data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItemParecer_Data_Enabled), 5, 0)));
         edtContagemItemParecer_Comentario_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItemParecer_Comentario_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItemParecer_Comentario_Enabled), 5, 0)));
         edtContagemItemParecer_UsuarioCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItemParecer_UsuarioCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItemParecer_UsuarioCod_Enabled), 5, 0)));
         edtContagemItemParecer_UsuarioPessoaNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItemParecer_UsuarioPessoaNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItemParecer_UsuarioPessoaNom_Enabled), 5, 0)));
         edtContagemItemParecer_UsuarioPessoaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItemParecer_UsuarioPessoaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItemParecer_UsuarioPessoaCod_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues190( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117192412");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contagemitemparecer.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ContagemItemParecer_Codigo) + "," + UrlEncode("" +A224ContagemItem_Lancamento)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z243ContagemItemParecer_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z243ContagemItemParecer_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z245ContagemItemParecer_Data", context.localUtil.TToC( Z245ContagemItemParecer_Data, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "Z246ContagemItemParecer_UsuarioCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z246ContagemItemParecer_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.NToC( (decimal)(A224ContagemItem_Lancamento), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N246ContagemItemParecer_UsuarioCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A246ContagemItemParecer_UsuarioCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vCONTAGEMITEMPARECER_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContagemItemParecer_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CONTAGEMITEM_LANCAMENTO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_ContagemItem_Lancamento), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_CONTAGEMITEMPARECER_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12Insert_ContagemItemParecer_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV15Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMITEMPARECER_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContagemItemParecer_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "ContagemItemParecer";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A243ContagemItemParecer_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contagemitemparecer:[SendSecurityCheck value for]"+"ContagemItemParecer_Codigo:"+context.localUtil.Format( (decimal)(A243ContagemItemParecer_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("contagemitemparecer:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contagemitemparecer.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ContagemItemParecer_Codigo) + "," + UrlEncode("" +A224ContagemItem_Lancamento) ;
      }

      public override String GetPgmname( )
      {
         return "ContagemItemParecer" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contagem Item Parecer" ;
      }

      protected void InitializeNonKey1946( )
      {
         A244ContagemItemParecer_Comentario = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A244ContagemItemParecer_Comentario", A244ContagemItemParecer_Comentario);
         A247ContagemItemParecer_UsuarioPessoaCod = 0;
         n247ContagemItemParecer_UsuarioPessoaCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A247ContagemItemParecer_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A247ContagemItemParecer_UsuarioPessoaCod), 6, 0)));
         A248ContagemItemParecer_UsuarioPessoaNom = "";
         n248ContagemItemParecer_UsuarioPessoaNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A248ContagemItemParecer_UsuarioPessoaNom", A248ContagemItemParecer_UsuarioPessoaNom);
         A246ContagemItemParecer_UsuarioCod = AV8WWPContext.gxTpr_Userid;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A246ContagemItemParecer_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A246ContagemItemParecer_UsuarioCod), 6, 0)));
         A245ContagemItemParecer_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A245ContagemItemParecer_Data", context.localUtil.TToC( A245ContagemItemParecer_Data, 8, 5, 0, 3, "/", ":", " "));
         Z245ContagemItemParecer_Data = (DateTime)(DateTime.MinValue);
         Z246ContagemItemParecer_UsuarioCod = 0;
      }

      protected void InitAll1946( )
      {
         A243ContagemItemParecer_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A243ContagemItemParecer_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A243ContagemItemParecer_Codigo), 6, 0)));
         InitializeNonKey1946( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A246ContagemItemParecer_UsuarioCod = i246ContagemItemParecer_UsuarioCod;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A246ContagemItemParecer_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A246ContagemItemParecer_UsuarioCod), 6, 0)));
         A245ContagemItemParecer_Data = i245ContagemItemParecer_Data;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A245ContagemItemParecer_Data", context.localUtil.TToC( A245ContagemItemParecer_Data, 8, 5, 0, 3, "/", ":", " "));
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117192441");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contagemitemparecer.js", "?20203117192441");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontagemitemparecer_codigo_Internalname = "TEXTBLOCKCONTAGEMITEMPARECER_CODIGO";
         edtContagemItemParecer_Codigo_Internalname = "CONTAGEMITEMPARECER_CODIGO";
         lblTextblockcontagemitem_lancamento_Internalname = "TEXTBLOCKCONTAGEMITEM_LANCAMENTO";
         edtContagemItem_Lancamento_Internalname = "CONTAGEMITEM_LANCAMENTO";
         lblTextblockcontagemitemparecer_data_Internalname = "TEXTBLOCKCONTAGEMITEMPARECER_DATA";
         edtContagemItemParecer_Data_Internalname = "CONTAGEMITEMPARECER_DATA";
         lblTextblockcontagemitemparecer_comentario_Internalname = "TEXTBLOCKCONTAGEMITEMPARECER_COMENTARIO";
         edtContagemItemParecer_Comentario_Internalname = "CONTAGEMITEMPARECER_COMENTARIO";
         lblTextblockcontagemitemparecer_usuariocod_Internalname = "TEXTBLOCKCONTAGEMITEMPARECER_USUARIOCOD";
         edtContagemItemParecer_UsuarioCod_Internalname = "CONTAGEMITEMPARECER_USUARIOCOD";
         edtContagemItemParecer_UsuarioPessoaNom_Internalname = "CONTAGEMITEMPARECER_USUARIOPESSOANOM";
         tblTablemergedcontagemitemparecer_usuariocod_Internalname = "TABLEMERGEDCONTAGEMITEMPARECER_USUARIOCOD";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtContagemItemParecer_UsuarioPessoaCod_Internalname = "CONTAGEMITEMPARECER_USUARIOPESSOACOD";
         Form.Internalname = "FORM";
         imgprompt_246_Internalname = "PROMPT_246";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Hist�rico - Parecer do Item(Lan�amento)";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Contagem Item Parecer";
         edtContagemItemParecer_UsuarioPessoaNom_Jsonclick = "";
         edtContagemItemParecer_UsuarioPessoaNom_Enabled = 0;
         imgprompt_246_Visible = 1;
         imgprompt_246_Link = "";
         edtContagemItemParecer_UsuarioCod_Jsonclick = "";
         edtContagemItemParecer_UsuarioCod_Enabled = 1;
         edtContagemItemParecer_Comentario_Enabled = 1;
         edtContagemItemParecer_Data_Jsonclick = "";
         edtContagemItemParecer_Data_Enabled = 1;
         edtContagemItem_Lancamento_Jsonclick = "";
         edtContagemItem_Lancamento_Enabled = 0;
         edtContagemItemParecer_Codigo_Jsonclick = "";
         edtContagemItemParecer_Codigo_Enabled = 0;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtContagemItemParecer_UsuarioPessoaCod_Jsonclick = "";
         edtContagemItemParecer_UsuarioPessoaCod_Enabled = 0;
         edtContagemItemParecer_UsuarioPessoaCod_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public void Valid_Contagemitemparecer_usuariocod( int GX_Parm1 ,
                                                        int GX_Parm2 ,
                                                        String GX_Parm3 )
      {
         A246ContagemItemParecer_UsuarioCod = GX_Parm1;
         A247ContagemItemParecer_UsuarioPessoaCod = GX_Parm2;
         n247ContagemItemParecer_UsuarioPessoaCod = false;
         A248ContagemItemParecer_UsuarioPessoaNom = GX_Parm3;
         n248ContagemItemParecer_UsuarioPessoaNom = false;
         /* Using cursor T001916 */
         pr_default.execute(14, new Object[] {A246ContagemItemParecer_UsuarioCod});
         if ( (pr_default.getStatus(14) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contagem Item Parecer_Usuario'.", "ForeignKeyNotFound", 1, "CONTAGEMITEMPARECER_USUARIOCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemItemParecer_UsuarioCod_Internalname;
         }
         A247ContagemItemParecer_UsuarioPessoaCod = T001916_A247ContagemItemParecer_UsuarioPessoaCod[0];
         n247ContagemItemParecer_UsuarioPessoaCod = T001916_n247ContagemItemParecer_UsuarioPessoaCod[0];
         pr_default.close(14);
         /* Using cursor T001917 */
         pr_default.execute(15, new Object[] {n247ContagemItemParecer_UsuarioPessoaCod, A247ContagemItemParecer_UsuarioPessoaCod});
         if ( (pr_default.getStatus(15) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A248ContagemItemParecer_UsuarioPessoaNom = T001917_A248ContagemItemParecer_UsuarioPessoaNom[0];
         n248ContagemItemParecer_UsuarioPessoaNom = T001917_n248ContagemItemParecer_UsuarioPessoaNom[0];
         pr_default.close(15);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A247ContagemItemParecer_UsuarioPessoaCod = 0;
            n247ContagemItemParecer_UsuarioPessoaCod = false;
            A248ContagemItemParecer_UsuarioPessoaNom = "";
            n248ContagemItemParecer_UsuarioPessoaNom = false;
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A247ContagemItemParecer_UsuarioPessoaCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A248ContagemItemParecer_UsuarioPessoaNom));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7ContagemItemParecer_Codigo',fld:'vCONTAGEMITEMPARECER_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E12192',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(14);
         pr_default.close(15);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z245ContagemItemParecer_Data = (DateTime)(DateTime.MinValue);
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockcontagemitemparecer_codigo_Jsonclick = "";
         lblTextblockcontagemitem_lancamento_Jsonclick = "";
         lblTextblockcontagemitemparecer_data_Jsonclick = "";
         A245ContagemItemParecer_Data = (DateTime)(DateTime.MinValue);
         lblTextblockcontagemitemparecer_comentario_Jsonclick = "";
         A244ContagemItemParecer_Comentario = "";
         lblTextblockcontagemitemparecer_usuariocod_Jsonclick = "";
         A248ContagemItemParecer_UsuarioPessoaNom = "";
         AV15Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode46 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV13TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z244ContagemItemParecer_Comentario = "";
         Z248ContagemItemParecer_UsuarioPessoaNom = "";
         T00194_A224ContagemItem_Lancamento = new int[1] ;
         T00195_A247ContagemItemParecer_UsuarioPessoaCod = new int[1] ;
         T00195_n247ContagemItemParecer_UsuarioPessoaCod = new bool[] {false} ;
         T00196_A248ContagemItemParecer_UsuarioPessoaNom = new String[] {""} ;
         T00196_n248ContagemItemParecer_UsuarioPessoaNom = new bool[] {false} ;
         T00197_A224ContagemItem_Lancamento = new int[1] ;
         T00197_A243ContagemItemParecer_Codigo = new int[1] ;
         T00197_A244ContagemItemParecer_Comentario = new String[] {""} ;
         T00197_A245ContagemItemParecer_Data = new DateTime[] {DateTime.MinValue} ;
         T00197_A248ContagemItemParecer_UsuarioPessoaNom = new String[] {""} ;
         T00197_n248ContagemItemParecer_UsuarioPessoaNom = new bool[] {false} ;
         T00197_A246ContagemItemParecer_UsuarioCod = new int[1] ;
         T00197_A247ContagemItemParecer_UsuarioPessoaCod = new int[1] ;
         T00197_n247ContagemItemParecer_UsuarioPessoaCod = new bool[] {false} ;
         T00198_A247ContagemItemParecer_UsuarioPessoaCod = new int[1] ;
         T00198_n247ContagemItemParecer_UsuarioPessoaCod = new bool[] {false} ;
         T00199_A248ContagemItemParecer_UsuarioPessoaNom = new String[] {""} ;
         T00199_n248ContagemItemParecer_UsuarioPessoaNom = new bool[] {false} ;
         T001910_A243ContagemItemParecer_Codigo = new int[1] ;
         T00193_A224ContagemItem_Lancamento = new int[1] ;
         T00193_A243ContagemItemParecer_Codigo = new int[1] ;
         T00193_A244ContagemItemParecer_Comentario = new String[] {""} ;
         T00193_A245ContagemItemParecer_Data = new DateTime[] {DateTime.MinValue} ;
         T00193_A246ContagemItemParecer_UsuarioCod = new int[1] ;
         T001911_A243ContagemItemParecer_Codigo = new int[1] ;
         T001911_A224ContagemItem_Lancamento = new int[1] ;
         T001912_A243ContagemItemParecer_Codigo = new int[1] ;
         T001912_A224ContagemItem_Lancamento = new int[1] ;
         T00192_A224ContagemItem_Lancamento = new int[1] ;
         T00192_A243ContagemItemParecer_Codigo = new int[1] ;
         T00192_A244ContagemItemParecer_Comentario = new String[] {""} ;
         T00192_A245ContagemItemParecer_Data = new DateTime[] {DateTime.MinValue} ;
         T00192_A246ContagemItemParecer_UsuarioCod = new int[1] ;
         T001913_A243ContagemItemParecer_Codigo = new int[1] ;
         T001916_A247ContagemItemParecer_UsuarioPessoaCod = new int[1] ;
         T001916_n247ContagemItemParecer_UsuarioPessoaCod = new bool[] {false} ;
         T001917_A248ContagemItemParecer_UsuarioPessoaNom = new String[] {""} ;
         T001917_n248ContagemItemParecer_UsuarioPessoaNom = new bool[] {false} ;
         T001918_A243ContagemItemParecer_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         i245ContagemItemParecer_Data = (DateTime)(DateTime.MinValue);
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemitemparecer__default(),
            new Object[][] {
                new Object[] {
               T00192_A224ContagemItem_Lancamento, T00192_A243ContagemItemParecer_Codigo, T00192_A244ContagemItemParecer_Comentario, T00192_A245ContagemItemParecer_Data, T00192_A246ContagemItemParecer_UsuarioCod
               }
               , new Object[] {
               T00193_A224ContagemItem_Lancamento, T00193_A243ContagemItemParecer_Codigo, T00193_A244ContagemItemParecer_Comentario, T00193_A245ContagemItemParecer_Data, T00193_A246ContagemItemParecer_UsuarioCod
               }
               , new Object[] {
               T00194_A224ContagemItem_Lancamento
               }
               , new Object[] {
               T00195_A247ContagemItemParecer_UsuarioPessoaCod, T00195_n247ContagemItemParecer_UsuarioPessoaCod
               }
               , new Object[] {
               T00196_A248ContagemItemParecer_UsuarioPessoaNom, T00196_n248ContagemItemParecer_UsuarioPessoaNom
               }
               , new Object[] {
               T00197_A224ContagemItem_Lancamento, T00197_A243ContagemItemParecer_Codigo, T00197_A244ContagemItemParecer_Comentario, T00197_A245ContagemItemParecer_Data, T00197_A248ContagemItemParecer_UsuarioPessoaNom, T00197_n248ContagemItemParecer_UsuarioPessoaNom, T00197_A246ContagemItemParecer_UsuarioCod, T00197_A247ContagemItemParecer_UsuarioPessoaCod, T00197_n247ContagemItemParecer_UsuarioPessoaCod
               }
               , new Object[] {
               T00198_A247ContagemItemParecer_UsuarioPessoaCod, T00198_n247ContagemItemParecer_UsuarioPessoaCod
               }
               , new Object[] {
               T00199_A248ContagemItemParecer_UsuarioPessoaNom, T00199_n248ContagemItemParecer_UsuarioPessoaNom
               }
               , new Object[] {
               T001910_A243ContagemItemParecer_Codigo
               }
               , new Object[] {
               T001911_A243ContagemItemParecer_Codigo, T001911_A224ContagemItem_Lancamento
               }
               , new Object[] {
               T001912_A243ContagemItemParecer_Codigo, T001912_A224ContagemItem_Lancamento
               }
               , new Object[] {
               T001913_A243ContagemItemParecer_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T001916_A247ContagemItemParecer_UsuarioPessoaCod, T001916_n247ContagemItemParecer_UsuarioPessoaCod
               }
               , new Object[] {
               T001917_A248ContagemItemParecer_UsuarioPessoaNom, T001917_n248ContagemItemParecer_UsuarioPessoaNom
               }
               , new Object[] {
               T001918_A243ContagemItemParecer_Codigo
               }
            }
         );
         N224ContagemItem_Lancamento = 0;
         Z224ContagemItem_Lancamento = 0;
         A224ContagemItem_Lancamento = 0;
         AV15Pgmname = "ContagemItemParecer";
         Z246ContagemItemParecer_UsuarioCod = AV8WWPContext.gxTpr_Userid;
         N246ContagemItemParecer_UsuarioCod = AV8WWPContext.gxTpr_Userid;
         i246ContagemItemParecer_UsuarioCod = AV8WWPContext.gxTpr_Userid;
         A246ContagemItemParecer_UsuarioCod = AV8WWPContext.gxTpr_Userid;
         Z245ContagemItemParecer_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         A245ContagemItemParecer_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         i245ContagemItemParecer_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short Gx_BScreen ;
      private short RcdFound46 ;
      private short GX_JID ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7ContagemItemParecer_Codigo ;
      private int wcpOA224ContagemItem_Lancamento ;
      private int Z243ContagemItemParecer_Codigo ;
      private int Z246ContagemItemParecer_UsuarioCod ;
      private int N224ContagemItem_Lancamento ;
      private int N246ContagemItemParecer_UsuarioCod ;
      private int A246ContagemItemParecer_UsuarioCod ;
      private int A247ContagemItemParecer_UsuarioPessoaCod ;
      private int AV7ContagemItemParecer_Codigo ;
      private int A224ContagemItem_Lancamento ;
      private int trnEnded ;
      private int edtContagemItemParecer_UsuarioPessoaCod_Enabled ;
      private int edtContagemItemParecer_UsuarioPessoaCod_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int A243ContagemItemParecer_Codigo ;
      private int edtContagemItemParecer_Codigo_Enabled ;
      private int edtContagemItem_Lancamento_Enabled ;
      private int edtContagemItemParecer_Data_Enabled ;
      private int edtContagemItemParecer_Comentario_Enabled ;
      private int edtContagemItemParecer_UsuarioCod_Enabled ;
      private int imgprompt_246_Visible ;
      private int edtContagemItemParecer_UsuarioPessoaNom_Enabled ;
      private int AV11Insert_ContagemItem_Lancamento ;
      private int AV12Insert_ContagemItemParecer_UsuarioCod ;
      private int AV16GXV1 ;
      private int Z224ContagemItem_Lancamento ;
      private int Z247ContagemItemParecer_UsuarioPessoaCod ;
      private int i246ContagemItemParecer_UsuarioCod ;
      private int idxLst ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtContagemItemParecer_Data_Internalname ;
      private String edtContagemItemParecer_UsuarioPessoaCod_Internalname ;
      private String edtContagemItemParecer_UsuarioPessoaCod_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockcontagemitemparecer_codigo_Internalname ;
      private String lblTextblockcontagemitemparecer_codigo_Jsonclick ;
      private String edtContagemItemParecer_Codigo_Internalname ;
      private String edtContagemItemParecer_Codigo_Jsonclick ;
      private String lblTextblockcontagemitem_lancamento_Internalname ;
      private String lblTextblockcontagemitem_lancamento_Jsonclick ;
      private String edtContagemItem_Lancamento_Internalname ;
      private String edtContagemItem_Lancamento_Jsonclick ;
      private String lblTextblockcontagemitemparecer_data_Internalname ;
      private String lblTextblockcontagemitemparecer_data_Jsonclick ;
      private String edtContagemItemParecer_Data_Jsonclick ;
      private String lblTextblockcontagemitemparecer_comentario_Internalname ;
      private String lblTextblockcontagemitemparecer_comentario_Jsonclick ;
      private String edtContagemItemParecer_Comentario_Internalname ;
      private String lblTextblockcontagemitemparecer_usuariocod_Internalname ;
      private String lblTextblockcontagemitemparecer_usuariocod_Jsonclick ;
      private String tblTablemergedcontagemitemparecer_usuariocod_Internalname ;
      private String edtContagemItemParecer_UsuarioCod_Internalname ;
      private String edtContagemItemParecer_UsuarioCod_Jsonclick ;
      private String imgprompt_246_Internalname ;
      private String imgprompt_246_Link ;
      private String edtContagemItemParecer_UsuarioPessoaNom_Internalname ;
      private String A248ContagemItemParecer_UsuarioPessoaNom ;
      private String edtContagemItemParecer_UsuarioPessoaNom_Jsonclick ;
      private String AV15Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode46 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z248ContagemItemParecer_UsuarioPessoaNom ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private DateTime Z245ContagemItemParecer_Data ;
      private DateTime A245ContagemItemParecer_Data ;
      private DateTime i245ContagemItemParecer_Data ;
      private bool entryPointCalled ;
      private bool n247ContagemItemParecer_UsuarioPessoaCod ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n248ContagemItemParecer_UsuarioPessoaNom ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private String A244ContagemItemParecer_Comentario ;
      private String Z244ContagemItemParecer_Comentario ;
      private IGxSession AV10WebSession ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP2_ContagemItem_Lancamento ;
      private IDataStoreProvider pr_default ;
      private int[] T00194_A224ContagemItem_Lancamento ;
      private int[] T00195_A247ContagemItemParecer_UsuarioPessoaCod ;
      private bool[] T00195_n247ContagemItemParecer_UsuarioPessoaCod ;
      private String[] T00196_A248ContagemItemParecer_UsuarioPessoaNom ;
      private bool[] T00196_n248ContagemItemParecer_UsuarioPessoaNom ;
      private int[] T00197_A224ContagemItem_Lancamento ;
      private int[] T00197_A243ContagemItemParecer_Codigo ;
      private String[] T00197_A244ContagemItemParecer_Comentario ;
      private DateTime[] T00197_A245ContagemItemParecer_Data ;
      private String[] T00197_A248ContagemItemParecer_UsuarioPessoaNom ;
      private bool[] T00197_n248ContagemItemParecer_UsuarioPessoaNom ;
      private int[] T00197_A246ContagemItemParecer_UsuarioCod ;
      private int[] T00197_A247ContagemItemParecer_UsuarioPessoaCod ;
      private bool[] T00197_n247ContagemItemParecer_UsuarioPessoaCod ;
      private int[] T00198_A247ContagemItemParecer_UsuarioPessoaCod ;
      private bool[] T00198_n247ContagemItemParecer_UsuarioPessoaCod ;
      private String[] T00199_A248ContagemItemParecer_UsuarioPessoaNom ;
      private bool[] T00199_n248ContagemItemParecer_UsuarioPessoaNom ;
      private int[] T001910_A243ContagemItemParecer_Codigo ;
      private int[] T00193_A224ContagemItem_Lancamento ;
      private int[] T00193_A243ContagemItemParecer_Codigo ;
      private String[] T00193_A244ContagemItemParecer_Comentario ;
      private DateTime[] T00193_A245ContagemItemParecer_Data ;
      private int[] T00193_A246ContagemItemParecer_UsuarioCod ;
      private int[] T001911_A243ContagemItemParecer_Codigo ;
      private int[] T001911_A224ContagemItem_Lancamento ;
      private int[] T001912_A243ContagemItemParecer_Codigo ;
      private int[] T001912_A224ContagemItem_Lancamento ;
      private int[] T00192_A224ContagemItem_Lancamento ;
      private int[] T00192_A243ContagemItemParecer_Codigo ;
      private String[] T00192_A244ContagemItemParecer_Comentario ;
      private DateTime[] T00192_A245ContagemItemParecer_Data ;
      private int[] T00192_A246ContagemItemParecer_UsuarioCod ;
      private int[] T001913_A243ContagemItemParecer_Codigo ;
      private int[] T001916_A247ContagemItemParecer_UsuarioPessoaCod ;
      private bool[] T001916_n247ContagemItemParecer_UsuarioPessoaCod ;
      private String[] T001917_A248ContagemItemParecer_UsuarioPessoaNom ;
      private bool[] T001917_n248ContagemItemParecer_UsuarioPessoaNom ;
      private int[] T001918_A243ContagemItemParecer_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV13TrnContextAtt ;
   }

   public class contagemitemparecer__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new UpdateCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00194 ;
          prmT00194 = new Object[] {
          new Object[] {"@ContagemItem_Lancamento",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00197 ;
          prmT00197 = new Object[] {
          new Object[] {"@ContagemItemParecer_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemItem_Lancamento",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00195 ;
          prmT00195 = new Object[] {
          new Object[] {"@ContagemItemParecer_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00196 ;
          prmT00196 = new Object[] {
          new Object[] {"@ContagemItemParecer_UsuarioPessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00198 ;
          prmT00198 = new Object[] {
          new Object[] {"@ContagemItemParecer_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00199 ;
          prmT00199 = new Object[] {
          new Object[] {"@ContagemItemParecer_UsuarioPessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001910 ;
          prmT001910 = new Object[] {
          new Object[] {"@ContagemItemParecer_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00193 ;
          prmT00193 = new Object[] {
          new Object[] {"@ContagemItemParecer_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001911 ;
          prmT001911 = new Object[] {
          new Object[] {"@ContagemItemParecer_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemItem_Lancamento",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001912 ;
          prmT001912 = new Object[] {
          new Object[] {"@ContagemItemParecer_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemItem_Lancamento",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00192 ;
          prmT00192 = new Object[] {
          new Object[] {"@ContagemItemParecer_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001913 ;
          prmT001913 = new Object[] {
          new Object[] {"@ContagemItem_Lancamento",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemItemParecer_Comentario",SqlDbType.VarChar,5000,0} ,
          new Object[] {"@ContagemItemParecer_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemItemParecer_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001914 ;
          prmT001914 = new Object[] {
          new Object[] {"@ContagemItem_Lancamento",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemItemParecer_Comentario",SqlDbType.VarChar,5000,0} ,
          new Object[] {"@ContagemItemParecer_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemItemParecer_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemItemParecer_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001915 ;
          prmT001915 = new Object[] {
          new Object[] {"@ContagemItemParecer_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001918 ;
          prmT001918 = new Object[] {
          new Object[] {"@ContagemItem_Lancamento",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001916 ;
          prmT001916 = new Object[] {
          new Object[] {"@ContagemItemParecer_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001917 ;
          prmT001917 = new Object[] {
          new Object[] {"@ContagemItemParecer_UsuarioPessoaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00192", "SELECT [ContagemItem_Lancamento], [ContagemItemParecer_Codigo], [ContagemItemParecer_Comentario], [ContagemItemParecer_Data], [ContagemItemParecer_UsuarioCod] AS ContagemItemParecer_UsuarioCod FROM [ContagemItemParecer] WITH (UPDLOCK) WHERE [ContagemItemParecer_Codigo] = @ContagemItemParecer_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00192,1,0,true,false )
             ,new CursorDef("T00193", "SELECT [ContagemItem_Lancamento], [ContagemItemParecer_Codigo], [ContagemItemParecer_Comentario], [ContagemItemParecer_Data], [ContagemItemParecer_UsuarioCod] AS ContagemItemParecer_UsuarioCod FROM [ContagemItemParecer] WITH (NOLOCK) WHERE [ContagemItemParecer_Codigo] = @ContagemItemParecer_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00193,1,0,true,false )
             ,new CursorDef("T00194", "SELECT [ContagemItem_Lancamento] FROM [ContagemItem] WITH (NOLOCK) WHERE [ContagemItem_Lancamento] = @ContagemItem_Lancamento ",true, GxErrorMask.GX_NOMASK, false, this,prmT00194,1,0,true,false )
             ,new CursorDef("T00195", "SELECT [Usuario_PessoaCod] AS ContagemItemParecer_UsuarioPessoaCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemItemParecer_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00195,1,0,true,false )
             ,new CursorDef("T00196", "SELECT [Pessoa_Nome] AS ContagemItemParecer_UsuarioPessoaNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemItemParecer_UsuarioPessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00196,1,0,true,false )
             ,new CursorDef("T00197", "SELECT TM1.[ContagemItem_Lancamento], TM1.[ContagemItemParecer_Codigo], TM1.[ContagemItemParecer_Comentario], TM1.[ContagemItemParecer_Data], T3.[Pessoa_Nome] AS ContagemItemParecer_UsuarioPessoaNom, TM1.[ContagemItemParecer_UsuarioCod] AS ContagemItemParecer_UsuarioCod, T2.[Usuario_PessoaCod] AS ContagemItemParecer_UsuarioPessoaCod FROM (([ContagemItemParecer] TM1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = TM1.[ContagemItemParecer_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) WHERE TM1.[ContagemItemParecer_Codigo] = @ContagemItemParecer_Codigo and TM1.[ContagemItem_Lancamento] = @ContagemItem_Lancamento ORDER BY TM1.[ContagemItemParecer_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00197,100,0,true,false )
             ,new CursorDef("T00198", "SELECT [Usuario_PessoaCod] AS ContagemItemParecer_UsuarioPessoaCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemItemParecer_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00198,1,0,true,false )
             ,new CursorDef("T00199", "SELECT [Pessoa_Nome] AS ContagemItemParecer_UsuarioPessoaNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemItemParecer_UsuarioPessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00199,1,0,true,false )
             ,new CursorDef("T001910", "SELECT [ContagemItemParecer_Codigo] FROM [ContagemItemParecer] WITH (NOLOCK) WHERE [ContagemItemParecer_Codigo] = @ContagemItemParecer_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001910,1,0,true,false )
             ,new CursorDef("T001911", "SELECT TOP 1 [ContagemItemParecer_Codigo], [ContagemItem_Lancamento] FROM [ContagemItemParecer] WITH (NOLOCK) WHERE ( [ContagemItemParecer_Codigo] > @ContagemItemParecer_Codigo) and [ContagemItem_Lancamento] = @ContagemItem_Lancamento ORDER BY [ContagemItemParecer_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001911,1,0,true,true )
             ,new CursorDef("T001912", "SELECT TOP 1 [ContagemItemParecer_Codigo], [ContagemItem_Lancamento] FROM [ContagemItemParecer] WITH (NOLOCK) WHERE ( [ContagemItemParecer_Codigo] < @ContagemItemParecer_Codigo) and [ContagemItem_Lancamento] = @ContagemItem_Lancamento ORDER BY [ContagemItemParecer_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001912,1,0,true,true )
             ,new CursorDef("T001913", "INSERT INTO [ContagemItemParecer]([ContagemItem_Lancamento], [ContagemItemParecer_Comentario], [ContagemItemParecer_Data], [ContagemItemParecer_UsuarioCod]) VALUES(@ContagemItem_Lancamento, @ContagemItemParecer_Comentario, @ContagemItemParecer_Data, @ContagemItemParecer_UsuarioCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT001913)
             ,new CursorDef("T001914", "UPDATE [ContagemItemParecer] SET [ContagemItem_Lancamento]=@ContagemItem_Lancamento, [ContagemItemParecer_Comentario]=@ContagemItemParecer_Comentario, [ContagemItemParecer_Data]=@ContagemItemParecer_Data, [ContagemItemParecer_UsuarioCod]=@ContagemItemParecer_UsuarioCod  WHERE [ContagemItemParecer_Codigo] = @ContagemItemParecer_Codigo", GxErrorMask.GX_NOMASK,prmT001914)
             ,new CursorDef("T001915", "DELETE FROM [ContagemItemParecer]  WHERE [ContagemItemParecer_Codigo] = @ContagemItemParecer_Codigo", GxErrorMask.GX_NOMASK,prmT001915)
             ,new CursorDef("T001916", "SELECT [Usuario_PessoaCod] AS ContagemItemParecer_UsuarioPessoaCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemItemParecer_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001916,1,0,true,false )
             ,new CursorDef("T001917", "SELECT [Pessoa_Nome] AS ContagemItemParecer_UsuarioPessoaNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemItemParecer_UsuarioPessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001917,1,0,true,false )
             ,new CursorDef("T001918", "SELECT [ContagemItemParecer_Codigo] FROM [ContagemItemParecer] WITH (NOLOCK) WHERE [ContagemItem_Lancamento] = @ContagemItem_Lancamento ORDER BY [ContagemItemParecer_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001918,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDateTime(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDateTime(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDateTime(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 15 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameterDatetime(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameterDatetime(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                stmt.SetParameter(5, (int)parms[4]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
