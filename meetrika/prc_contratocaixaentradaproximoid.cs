/*
               File: PRC_ContratoCaixaEntradaProximoId
        Description: Busca o Pr�x�mo ID
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:13:51.18
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_contratocaixaentradaproximoid : GXProcedure
   {
      public prc_contratocaixaentradaproximoid( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_contratocaixaentradaproximoid( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contrato_Codigo ,
                           out int aP1_ContratoCaixaEntrada_Codigo )
      {
         this.A74Contrato_Codigo = aP0_Contrato_Codigo;
         this.AV8ContratoCaixaEntrada_Codigo = 0 ;
         initialize();
         executePrivate();
         aP1_ContratoCaixaEntrada_Codigo=this.AV8ContratoCaixaEntrada_Codigo;
      }

      public int executeUdp( int aP0_Contrato_Codigo )
      {
         this.A74Contrato_Codigo = aP0_Contrato_Codigo;
         this.AV8ContratoCaixaEntrada_Codigo = 0 ;
         initialize();
         executePrivate();
         aP1_ContratoCaixaEntrada_Codigo=this.AV8ContratoCaixaEntrada_Codigo;
         return AV8ContratoCaixaEntrada_Codigo ;
      }

      public void executeSubmit( int aP0_Contrato_Codigo ,
                                 out int aP1_ContratoCaixaEntrada_Codigo )
      {
         prc_contratocaixaentradaproximoid objprc_contratocaixaentradaproximoid;
         objprc_contratocaixaentradaproximoid = new prc_contratocaixaentradaproximoid();
         objprc_contratocaixaentradaproximoid.A74Contrato_Codigo = aP0_Contrato_Codigo;
         objprc_contratocaixaentradaproximoid.AV8ContratoCaixaEntrada_Codigo = 0 ;
         objprc_contratocaixaentradaproximoid.context.SetSubmitInitialConfig(context);
         objprc_contratocaixaentradaproximoid.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_contratocaixaentradaproximoid);
         aP1_ContratoCaixaEntrada_Codigo=this.AV8ContratoCaixaEntrada_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_contratocaixaentradaproximoid)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV8ContratoCaixaEntrada_Codigo = 0;
         /* Using cursor P00Y02 */
         pr_default.execute(0, new Object[] {A74Contrato_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A2098ContratoCaixaEntrada_Codigo = P00Y02_A2098ContratoCaixaEntrada_Codigo[0];
            AV8ContratoCaixaEntrada_Codigo = A2098ContratoCaixaEntrada_Codigo;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         AV8ContratoCaixaEntrada_Codigo = (int)(AV8ContratoCaixaEntrada_Codigo+1);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00Y02_A74Contrato_Codigo = new int[1] ;
         P00Y02_A2098ContratoCaixaEntrada_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_contratocaixaentradaproximoid__default(),
            new Object[][] {
                new Object[] {
               P00Y02_A74Contrato_Codigo, P00Y02_A2098ContratoCaixaEntrada_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A74Contrato_Codigo ;
      private int AV8ContratoCaixaEntrada_Codigo ;
      private int A2098ContratoCaixaEntrada_Codigo ;
      private String scmdbuf ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00Y02_A74Contrato_Codigo ;
      private int[] P00Y02_A2098ContratoCaixaEntrada_Codigo ;
      private int aP1_ContratoCaixaEntrada_Codigo ;
   }

   public class prc_contratocaixaentradaproximoid__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00Y02 ;
          prmP00Y02 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00Y02", "SELECT TOP 1 [Contrato_Codigo], [ContratoCaixaEntrada_Codigo] FROM [ContratoCaixaEntrada] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ORDER BY [ContratoCaixaEntrada_Codigo] DESC ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00Y02,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
