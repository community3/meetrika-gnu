/*
               File: PRC_DpnParaHomologar
        Description: Dependencia para homologar
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:9:28.63
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_dpnparahomologar : GXProcedure
   {
      public prc_dpnparahomologar( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_dpnparahomologar( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultado_OSVinculada ,
                           out bool aP1_TemPndHmlg ,
                           out String aP2_TooltipText )
      {
         this.A602ContagemResultado_OSVinculada = aP0_ContagemResultado_OSVinculada;
         this.AV10TemPndHmlg = false ;
         this.AV8TooltipText = "" ;
         initialize();
         executePrivate();
         aP0_ContagemResultado_OSVinculada=this.A602ContagemResultado_OSVinculada;
         aP1_TemPndHmlg=this.AV10TemPndHmlg;
         aP2_TooltipText=this.AV8TooltipText;
      }

      public String executeUdp( ref int aP0_ContagemResultado_OSVinculada ,
                                out bool aP1_TemPndHmlg )
      {
         this.A602ContagemResultado_OSVinculada = aP0_ContagemResultado_OSVinculada;
         this.AV10TemPndHmlg = false ;
         this.AV8TooltipText = "" ;
         initialize();
         executePrivate();
         aP0_ContagemResultado_OSVinculada=this.A602ContagemResultado_OSVinculada;
         aP1_TemPndHmlg=this.AV10TemPndHmlg;
         aP2_TooltipText=this.AV8TooltipText;
         return AV8TooltipText ;
      }

      public void executeSubmit( ref int aP0_ContagemResultado_OSVinculada ,
                                 out bool aP1_TemPndHmlg ,
                                 out String aP2_TooltipText )
      {
         prc_dpnparahomologar objprc_dpnparahomologar;
         objprc_dpnparahomologar = new prc_dpnparahomologar();
         objprc_dpnparahomologar.A602ContagemResultado_OSVinculada = aP0_ContagemResultado_OSVinculada;
         objprc_dpnparahomologar.AV10TemPndHmlg = false ;
         objprc_dpnparahomologar.AV8TooltipText = "" ;
         objprc_dpnparahomologar.context.SetSubmitInitialConfig(context);
         objprc_dpnparahomologar.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_dpnparahomologar);
         aP0_ContagemResultado_OSVinculada=this.A602ContagemResultado_OSVinculada;
         aP1_TemPndHmlg=this.AV10TemPndHmlg;
         aP2_TooltipText=this.AV8TooltipText;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_dpnparahomologar)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00AJ2 */
         pr_default.execute(0, new Object[] {n602ContagemResultado_OSVinculada, A602ContagemResultado_OSVinculada});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = P00AJ2_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00AJ2_n1553ContagemResultado_CntSrvCod[0];
            A484ContagemResultado_StatusDmn = P00AJ2_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00AJ2_n484ContagemResultado_StatusDmn[0];
            A456ContagemResultado_Codigo = P00AJ2_A456ContagemResultado_Codigo[0];
            /* Using cursor P00AJ3 */
            pr_default.execute(1, new Object[] {n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod});
            A601ContagemResultado_Servico = P00AJ3_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00AJ3_n601ContagemResultado_Servico[0];
            A1593ContagemResultado_CntSrvTpVnc = P00AJ3_A1593ContagemResultado_CntSrvTpVnc[0];
            n1593ContagemResultado_CntSrvTpVnc = P00AJ3_n1593ContagemResultado_CntSrvTpVnc[0];
            pr_default.close(1);
            if ( ( StringUtil.StrCmp(A1593ContagemResultado_CntSrvTpVnc, "C") == 0 ) || ( StringUtil.StrCmp(A1593ContagemResultado_CntSrvTpVnc, "A") == 0 ) )
            {
               /* Using cursor P00AJ4 */
               pr_default.execute(2, new Object[] {n601ContagemResultado_Servico, A601ContagemResultado_Servico});
               A801ContagemResultado_ServicoSigla = P00AJ4_A801ContagemResultado_ServicoSigla[0];
               n801ContagemResultado_ServicoSigla = P00AJ4_n801ContagemResultado_ServicoSigla[0];
               pr_default.close(2);
               AV10TemPndHmlg = true;
               AV8TooltipText = AV8TooltipText + " " + A801ContagemResultado_ServicoSigla;
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
         pr_default.close(1);
         pr_default.close(2);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00AJ2_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00AJ2_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00AJ2_A602ContagemResultado_OSVinculada = new int[1] ;
         P00AJ2_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P00AJ2_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00AJ2_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00AJ2_A456ContagemResultado_Codigo = new int[1] ;
         A484ContagemResultado_StatusDmn = "";
         P00AJ3_A601ContagemResultado_Servico = new int[1] ;
         P00AJ3_n601ContagemResultado_Servico = new bool[] {false} ;
         P00AJ3_A1593ContagemResultado_CntSrvTpVnc = new String[] {""} ;
         P00AJ3_n1593ContagemResultado_CntSrvTpVnc = new bool[] {false} ;
         A1593ContagemResultado_CntSrvTpVnc = "";
         P00AJ4_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P00AJ4_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         A801ContagemResultado_ServicoSigla = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_dpnparahomologar__default(),
            new Object[][] {
                new Object[] {
               P00AJ2_A1553ContagemResultado_CntSrvCod, P00AJ2_n1553ContagemResultado_CntSrvCod, P00AJ2_A602ContagemResultado_OSVinculada, P00AJ2_n602ContagemResultado_OSVinculada, P00AJ2_A484ContagemResultado_StatusDmn, P00AJ2_n484ContagemResultado_StatusDmn, P00AJ2_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P00AJ3_A601ContagemResultado_Servico, P00AJ3_n601ContagemResultado_Servico, P00AJ3_A1593ContagemResultado_CntSrvTpVnc, P00AJ3_n1593ContagemResultado_CntSrvTpVnc
               }
               , new Object[] {
               P00AJ4_A801ContagemResultado_ServicoSigla, P00AJ4_n801ContagemResultado_ServicoSigla
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A602ContagemResultado_OSVinculada ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A456ContagemResultado_Codigo ;
      private int A601ContagemResultado_Servico ;
      private String AV8TooltipText ;
      private String scmdbuf ;
      private String A484ContagemResultado_StatusDmn ;
      private String A1593ContagemResultado_CntSrvTpVnc ;
      private String A801ContagemResultado_ServicoSigla ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n601ContagemResultado_Servico ;
      private bool n1593ContagemResultado_CntSrvTpVnc ;
      private bool n801ContagemResultado_ServicoSigla ;
      private bool AV10TemPndHmlg ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultado_OSVinculada ;
      private IDataStoreProvider pr_default ;
      private int[] P00AJ2_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00AJ2_n1553ContagemResultado_CntSrvCod ;
      private int[] P00AJ2_A602ContagemResultado_OSVinculada ;
      private bool[] P00AJ2_n602ContagemResultado_OSVinculada ;
      private String[] P00AJ2_A484ContagemResultado_StatusDmn ;
      private bool[] P00AJ2_n484ContagemResultado_StatusDmn ;
      private int[] P00AJ2_A456ContagemResultado_Codigo ;
      private int[] P00AJ3_A601ContagemResultado_Servico ;
      private bool[] P00AJ3_n601ContagemResultado_Servico ;
      private String[] P00AJ3_A1593ContagemResultado_CntSrvTpVnc ;
      private bool[] P00AJ3_n1593ContagemResultado_CntSrvTpVnc ;
      private String[] P00AJ4_A801ContagemResultado_ServicoSigla ;
      private bool[] P00AJ4_n801ContagemResultado_ServicoSigla ;
      private bool aP1_TemPndHmlg ;
      private String aP2_TooltipText ;
   }

   public class prc_dpnparahomologar__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00AJ2 ;
          prmP00AJ2 = new Object[] {
          new Object[] {"@ContagemResultado_OSVinculada",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00AJ3 ;
          prmP00AJ3 = new Object[] {
          new Object[] {"@ContagemResultado_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00AJ4 ;
          prmP00AJ4 = new Object[] {
          new Object[] {"@ContagemResultado_Servico",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00AJ2", "SELECT [ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, [ContagemResultado_OSVinculada], [ContagemResultado_StatusDmn], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE ([ContagemResultado_OSVinculada] = @ContagemResultado_OSVinculada) AND (Not ( [ContagemResultado_StatusDmn] = 'X' or [ContagemResultado_StatusDmn] = 'R' or [ContagemResultado_StatusDmn] = 'C' or [ContagemResultado_StatusDmn] = 'H' or [ContagemResultado_StatusDmn] = 'O' or [ContagemResultado_StatusDmn] = 'P' or [ContagemResultado_StatusDmn] = 'L')) ORDER BY [ContagemResultado_OSVinculada] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00AJ2,100,0,true,false )
             ,new CursorDef("P00AJ3", "SELECT [Servico_Codigo] AS ContagemResultado_Servico, [ContratoServicos_TipoVnc] AS ContagemResultado_CntSrvTpVnc FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContagemResultado_CntSrvCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00AJ3,1,0,true,false )
             ,new CursorDef("P00AJ4", "SELECT [Servico_Sigla] AS ContagemResultado_ServicoSigla FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @ContagemResultado_Servico ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00AJ4,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
