/*
               File: type_SdtContagemResultadoEvidencia
        Description: Anexar
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/24/2020 23:54:4.80
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "ContagemResultadoEvidencia" )]
   [XmlType(TypeName =  "ContagemResultadoEvidencia" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtContagemResultadoEvidencia : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContagemResultadoEvidencia( )
      {
         /* Constructor for serialization */
         gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demanda = "";
         gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demandafm = "";
         gxTv_SdtContagemResultadoEvidencia_Contagemresultado_osfsosfm = "";
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_link = "";
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_arquivo = "";
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_nomearq = "";
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_tipoarq = "";
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data = (DateTime)(DateTime.MinValue);
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_descricao = "";
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated = (DateTime)(DateTime.MinValue);
         gxTv_SdtContagemResultadoEvidencia_Tipodocumento_nome = "";
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_entidade = "";
         gxTv_SdtContagemResultadoEvidencia_Mode = "";
         gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demanda_Z = "";
         gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demandafm_Z = "";
         gxTv_SdtContagemResultadoEvidencia_Contagemresultado_osfsosfm_Z = "";
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_nomearq_Z = "";
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_tipoarq_Z = "";
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data_Z = (DateTime)(DateTime.MinValue);
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated_Z = (DateTime)(DateTime.MinValue);
         gxTv_SdtContagemResultadoEvidencia_Tipodocumento_nome_Z = "";
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_entidade_Z = "";
      }

      public SdtContagemResultadoEvidencia( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV586ContagemResultadoEvidencia_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV586ContagemResultadoEvidencia_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"ContagemResultadoEvidencia_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "ContagemResultadoEvidencia");
         metadata.Set("BT", "ContagemResultadoEvidencia");
         metadata.Set("PK", "[ \"ContagemResultadoEvidencia_Codigo\" ]");
         metadata.Set("PKAssigned", "[ \"ContagemResultadoEvidencia_Codigo\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"ContagemResultado_Codigo\" ],\"FKMap\":[  ] },{ \"FK\":[ \"TipoDocumento_Codigo\" ],\"FKMap\":[  ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadoevidencia_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_demanda_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_demandafm_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_areatrabalhocod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_osfsosfm_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadoevidencia_nomearq_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadoevidencia_tipoarq_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadoevidencia_data_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadoevidencia_rdmncreated_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Tipodocumento_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Tipodocumento_nome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadoevidencia_owner_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadoevidencia_entidade_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_demanda_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_demandafm_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_areatrabalhocod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadoevidencia_link_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadoevidencia_arquivo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadoevidencia_nomearq_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadoevidencia_tipoarq_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadoevidencia_data_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadoevidencia_descricao_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadoevidencia_rdmncreated_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Tipodocumento_codigo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadoevidencia_owner_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtContagemResultadoEvidencia deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtContagemResultadoEvidencia)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtContagemResultadoEvidencia obj ;
         obj = this;
         obj.gxTpr_Contagemresultadoevidencia_codigo = deserialized.gxTpr_Contagemresultadoevidencia_codigo;
         obj.gxTpr_Contagemresultado_codigo = deserialized.gxTpr_Contagemresultado_codigo;
         obj.gxTpr_Contagemresultado_demanda = deserialized.gxTpr_Contagemresultado_demanda;
         obj.gxTpr_Contagemresultado_demandafm = deserialized.gxTpr_Contagemresultado_demandafm;
         obj.gxTpr_Contratada_areatrabalhocod = deserialized.gxTpr_Contratada_areatrabalhocod;
         obj.gxTpr_Contagemresultado_osfsosfm = deserialized.gxTpr_Contagemresultado_osfsosfm;
         obj.gxTpr_Contagemresultadoevidencia_link = deserialized.gxTpr_Contagemresultadoevidencia_link;
         obj.gxTpr_Contagemresultadoevidencia_arquivo = deserialized.gxTpr_Contagemresultadoevidencia_arquivo;
         obj.gxTpr_Contagemresultadoevidencia_nomearq = deserialized.gxTpr_Contagemresultadoevidencia_nomearq;
         obj.gxTpr_Contagemresultadoevidencia_tipoarq = deserialized.gxTpr_Contagemresultadoevidencia_tipoarq;
         obj.gxTpr_Contagemresultadoevidencia_data = deserialized.gxTpr_Contagemresultadoevidencia_data;
         obj.gxTpr_Contagemresultadoevidencia_descricao = deserialized.gxTpr_Contagemresultadoevidencia_descricao;
         obj.gxTpr_Contagemresultadoevidencia_rdmncreated = deserialized.gxTpr_Contagemresultadoevidencia_rdmncreated;
         obj.gxTpr_Tipodocumento_codigo = deserialized.gxTpr_Tipodocumento_codigo;
         obj.gxTpr_Tipodocumento_nome = deserialized.gxTpr_Tipodocumento_nome;
         obj.gxTpr_Contagemresultadoevidencia_owner = deserialized.gxTpr_Contagemresultadoevidencia_owner;
         obj.gxTpr_Contagemresultadoevidencia_entidade = deserialized.gxTpr_Contagemresultadoevidencia_entidade;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Contagemresultadoevidencia_codigo_Z = deserialized.gxTpr_Contagemresultadoevidencia_codigo_Z;
         obj.gxTpr_Contagemresultado_codigo_Z = deserialized.gxTpr_Contagemresultado_codigo_Z;
         obj.gxTpr_Contagemresultado_demanda_Z = deserialized.gxTpr_Contagemresultado_demanda_Z;
         obj.gxTpr_Contagemresultado_demandafm_Z = deserialized.gxTpr_Contagemresultado_demandafm_Z;
         obj.gxTpr_Contratada_areatrabalhocod_Z = deserialized.gxTpr_Contratada_areatrabalhocod_Z;
         obj.gxTpr_Contagemresultado_osfsosfm_Z = deserialized.gxTpr_Contagemresultado_osfsosfm_Z;
         obj.gxTpr_Contagemresultadoevidencia_nomearq_Z = deserialized.gxTpr_Contagemresultadoevidencia_nomearq_Z;
         obj.gxTpr_Contagemresultadoevidencia_tipoarq_Z = deserialized.gxTpr_Contagemresultadoevidencia_tipoarq_Z;
         obj.gxTpr_Contagemresultadoevidencia_data_Z = deserialized.gxTpr_Contagemresultadoevidencia_data_Z;
         obj.gxTpr_Contagemresultadoevidencia_rdmncreated_Z = deserialized.gxTpr_Contagemresultadoevidencia_rdmncreated_Z;
         obj.gxTpr_Tipodocumento_codigo_Z = deserialized.gxTpr_Tipodocumento_codigo_Z;
         obj.gxTpr_Tipodocumento_nome_Z = deserialized.gxTpr_Tipodocumento_nome_Z;
         obj.gxTpr_Contagemresultadoevidencia_owner_Z = deserialized.gxTpr_Contagemresultadoevidencia_owner_Z;
         obj.gxTpr_Contagemresultadoevidencia_entidade_Z = deserialized.gxTpr_Contagemresultadoevidencia_entidade_Z;
         obj.gxTpr_Contagemresultado_demanda_N = deserialized.gxTpr_Contagemresultado_demanda_N;
         obj.gxTpr_Contagemresultado_demandafm_N = deserialized.gxTpr_Contagemresultado_demandafm_N;
         obj.gxTpr_Contratada_areatrabalhocod_N = deserialized.gxTpr_Contratada_areatrabalhocod_N;
         obj.gxTpr_Contagemresultadoevidencia_link_N = deserialized.gxTpr_Contagemresultadoevidencia_link_N;
         obj.gxTpr_Contagemresultadoevidencia_arquivo_N = deserialized.gxTpr_Contagemresultadoevidencia_arquivo_N;
         obj.gxTpr_Contagemresultadoevidencia_nomearq_N = deserialized.gxTpr_Contagemresultadoevidencia_nomearq_N;
         obj.gxTpr_Contagemresultadoevidencia_tipoarq_N = deserialized.gxTpr_Contagemresultadoevidencia_tipoarq_N;
         obj.gxTpr_Contagemresultadoevidencia_data_N = deserialized.gxTpr_Contagemresultadoevidencia_data_N;
         obj.gxTpr_Contagemresultadoevidencia_descricao_N = deserialized.gxTpr_Contagemresultadoevidencia_descricao_N;
         obj.gxTpr_Contagemresultadoevidencia_rdmncreated_N = deserialized.gxTpr_Contagemresultadoevidencia_rdmncreated_N;
         obj.gxTpr_Tipodocumento_codigo_N = deserialized.gxTpr_Tipodocumento_codigo_N;
         obj.gxTpr_Contagemresultadoevidencia_owner_N = deserialized.gxTpr_Contagemresultadoevidencia_owner_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoEvidencia_Codigo") )
               {
                  gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Codigo") )
               {
                  gxTv_SdtContagemResultadoEvidencia_Contagemresultado_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Demanda") )
               {
                  gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demanda = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DemandaFM") )
               {
                  gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demandafm = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_AreaTrabalhoCod") )
               {
                  gxTv_SdtContagemResultadoEvidencia_Contratada_areatrabalhocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_OsFsOsFm") )
               {
                  gxTv_SdtContagemResultadoEvidencia_Contagemresultado_osfsosfm = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoEvidencia_Link") )
               {
                  gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_link = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoEvidencia_Arquivo") )
               {
                  gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_arquivo=context.FileFromBase64( oReader.Value) ;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoEvidencia_NomeArq") )
               {
                  gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_nomearq = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoEvidencia_TipoArq") )
               {
                  gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_tipoarq = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoEvidencia_Data") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoEvidencia_Descricao") )
               {
                  gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_descricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoEvidencia_RdmnCreated") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "TipoDocumento_Codigo") )
               {
                  gxTv_SdtContagemResultadoEvidencia_Tipodocumento_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "TipoDocumento_Nome") )
               {
                  gxTv_SdtContagemResultadoEvidencia_Tipodocumento_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoEvidencia_Owner") )
               {
                  gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_owner = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoEvidencia_Entidade") )
               {
                  gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_entidade = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtContagemResultadoEvidencia_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtContagemResultadoEvidencia_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoEvidencia_Codigo_Z") )
               {
                  gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Codigo_Z") )
               {
                  gxTv_SdtContagemResultadoEvidencia_Contagemresultado_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Demanda_Z") )
               {
                  gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demanda_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DemandaFM_Z") )
               {
                  gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demandafm_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_AreaTrabalhoCod_Z") )
               {
                  gxTv_SdtContagemResultadoEvidencia_Contratada_areatrabalhocod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_OsFsOsFm_Z") )
               {
                  gxTv_SdtContagemResultadoEvidencia_Contagemresultado_osfsosfm_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoEvidencia_NomeArq_Z") )
               {
                  gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_nomearq_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoEvidencia_TipoArq_Z") )
               {
                  gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_tipoarq_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoEvidencia_Data_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data_Z = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data_Z = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoEvidencia_RdmnCreated_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated_Z = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated_Z = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "TipoDocumento_Codigo_Z") )
               {
                  gxTv_SdtContagemResultadoEvidencia_Tipodocumento_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "TipoDocumento_Nome_Z") )
               {
                  gxTv_SdtContagemResultadoEvidencia_Tipodocumento_nome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoEvidencia_Owner_Z") )
               {
                  gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_owner_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoEvidencia_Entidade_Z") )
               {
                  gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_entidade_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Demanda_N") )
               {
                  gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demanda_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DemandaFM_N") )
               {
                  gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demandafm_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_AreaTrabalhoCod_N") )
               {
                  gxTv_SdtContagemResultadoEvidencia_Contratada_areatrabalhocod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoEvidencia_Link_N") )
               {
                  gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_link_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoEvidencia_Arquivo_N") )
               {
                  gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_arquivo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoEvidencia_NomeArq_N") )
               {
                  gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_nomearq_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoEvidencia_TipoArq_N") )
               {
                  gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_tipoarq_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoEvidencia_Data_N") )
               {
                  gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoEvidencia_Descricao_N") )
               {
                  gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_descricao_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoEvidencia_RdmnCreated_N") )
               {
                  gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "TipoDocumento_Codigo_N") )
               {
                  gxTv_SdtContagemResultadoEvidencia_Tipodocumento_codigo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoEvidencia_Owner_N") )
               {
                  gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_owner_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "ContagemResultadoEvidencia";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("ContagemResultadoEvidencia_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoEvidencia_Contagemresultado_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_Demanda", StringUtil.RTrim( gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demanda));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_DemandaFM", StringUtil.RTrim( gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demandafm));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratada_AreaTrabalhoCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoEvidencia_Contratada_areatrabalhocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_OsFsOsFm", StringUtil.RTrim( gxTv_SdtContagemResultadoEvidencia_Contagemresultado_osfsosfm));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultadoEvidencia_Link", StringUtil.RTrim( gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_link));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultadoEvidencia_Arquivo", context.FileToBase64( gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_arquivo));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultadoEvidencia_NomeArq", StringUtil.RTrim( gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_nomearq));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultadoEvidencia_TipoArq", StringUtil.RTrim( gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_tipoarq));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( (DateTime.MinValue==gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data) )
         {
            oWriter.WriteStartElement("ContagemResultadoEvidencia_Data");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("ContagemResultadoEvidencia_Data", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteElement("ContagemResultadoEvidencia_Descricao", StringUtil.RTrim( gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_descricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( (DateTime.MinValue==gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated) )
         {
            oWriter.WriteStartElement("ContagemResultadoEvidencia_RdmnCreated");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("ContagemResultadoEvidencia_RdmnCreated", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteElement("TipoDocumento_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoEvidencia_Tipodocumento_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("TipoDocumento_Nome", StringUtil.RTrim( gxTv_SdtContagemResultadoEvidencia_Tipodocumento_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultadoEvidencia_Owner", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_owner), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultadoEvidencia_Entidade", StringUtil.RTrim( gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_entidade));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtContagemResultadoEvidencia_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoEvidencia_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoEvidencia_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultado_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoEvidencia_Contagemresultado_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultado_Demanda_Z", StringUtil.RTrim( gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demanda_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultado_DemandaFM_Z", StringUtil.RTrim( gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demandafm_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_AreaTrabalhoCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoEvidencia_Contratada_areatrabalhocod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultado_OsFsOsFm_Z", StringUtil.RTrim( gxTv_SdtContagemResultadoEvidencia_Contagemresultado_osfsosfm_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoEvidencia_NomeArq_Z", StringUtil.RTrim( gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_nomearq_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoEvidencia_TipoArq_Z", StringUtil.RTrim( gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_tipoarq_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            if ( (DateTime.MinValue==gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data_Z) )
            {
               oWriter.WriteStartElement("ContagemResultadoEvidencia_Data_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "T";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("ContagemResultadoEvidencia_Data_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
               }
            }
            if ( (DateTime.MinValue==gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated_Z) )
            {
               oWriter.WriteStartElement("ContagemResultadoEvidencia_RdmnCreated_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "T";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("ContagemResultadoEvidencia_RdmnCreated_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
               }
            }
            oWriter.WriteElement("TipoDocumento_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoEvidencia_Tipodocumento_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("TipoDocumento_Nome_Z", StringUtil.RTrim( gxTv_SdtContagemResultadoEvidencia_Tipodocumento_nome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoEvidencia_Owner_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_owner_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoEvidencia_Entidade_Z", StringUtil.RTrim( gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_entidade_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultado_Demanda_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demanda_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultado_DemandaFM_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demandafm_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratada_AreaTrabalhoCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoEvidencia_Contratada_areatrabalhocod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoEvidencia_Link_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_link_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoEvidencia_Arquivo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_arquivo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoEvidencia_NomeArq_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_nomearq_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoEvidencia_TipoArq_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_tipoarq_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoEvidencia_Data_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoEvidencia_Descricao_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_descricao_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoEvidencia_RdmnCreated_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("TipoDocumento_Codigo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoEvidencia_Tipodocumento_codigo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoEvidencia_Owner_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_owner_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("ContagemResultadoEvidencia_Codigo", gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_codigo, false);
         AddObjectProperty("ContagemResultado_Codigo", gxTv_SdtContagemResultadoEvidencia_Contagemresultado_codigo, false);
         AddObjectProperty("ContagemResultado_Demanda", gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demanda, false);
         AddObjectProperty("ContagemResultado_DemandaFM", gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demandafm, false);
         AddObjectProperty("Contratada_AreaTrabalhoCod", gxTv_SdtContagemResultadoEvidencia_Contratada_areatrabalhocod, false);
         AddObjectProperty("ContagemResultado_OsFsOsFm", gxTv_SdtContagemResultadoEvidencia_Contagemresultado_osfsosfm, false);
         AddObjectProperty("ContagemResultadoEvidencia_Link", gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_link, false);
         AddObjectProperty("ContagemResultadoEvidencia_Arquivo", gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_arquivo, false);
         AddObjectProperty("ContagemResultadoEvidencia_NomeArq", gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_nomearq, false);
         AddObjectProperty("ContagemResultadoEvidencia_TipoArq", gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_tipoarq, false);
         datetime_STZ = gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data;
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("ContagemResultadoEvidencia_Data", sDateCnv, false);
         AddObjectProperty("ContagemResultadoEvidencia_Descricao", gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_descricao, false);
         datetime_STZ = gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated;
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("ContagemResultadoEvidencia_RdmnCreated", sDateCnv, false);
         AddObjectProperty("TipoDocumento_Codigo", gxTv_SdtContagemResultadoEvidencia_Tipodocumento_codigo, false);
         AddObjectProperty("TipoDocumento_Nome", gxTv_SdtContagemResultadoEvidencia_Tipodocumento_nome, false);
         AddObjectProperty("ContagemResultadoEvidencia_Owner", gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_owner, false);
         AddObjectProperty("ContagemResultadoEvidencia_Entidade", gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_entidade, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtContagemResultadoEvidencia_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtContagemResultadoEvidencia_Initialized, false);
            AddObjectProperty("ContagemResultadoEvidencia_Codigo_Z", gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_codigo_Z, false);
            AddObjectProperty("ContagemResultado_Codigo_Z", gxTv_SdtContagemResultadoEvidencia_Contagemresultado_codigo_Z, false);
            AddObjectProperty("ContagemResultado_Demanda_Z", gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demanda_Z, false);
            AddObjectProperty("ContagemResultado_DemandaFM_Z", gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demandafm_Z, false);
            AddObjectProperty("Contratada_AreaTrabalhoCod_Z", gxTv_SdtContagemResultadoEvidencia_Contratada_areatrabalhocod_Z, false);
            AddObjectProperty("ContagemResultado_OsFsOsFm_Z", gxTv_SdtContagemResultadoEvidencia_Contagemresultado_osfsosfm_Z, false);
            AddObjectProperty("ContagemResultadoEvidencia_NomeArq_Z", gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_nomearq_Z, false);
            AddObjectProperty("ContagemResultadoEvidencia_TipoArq_Z", gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_tipoarq_Z, false);
            datetime_STZ = gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data_Z;
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("ContagemResultadoEvidencia_Data_Z", sDateCnv, false);
            datetime_STZ = gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated_Z;
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("ContagemResultadoEvidencia_RdmnCreated_Z", sDateCnv, false);
            AddObjectProperty("TipoDocumento_Codigo_Z", gxTv_SdtContagemResultadoEvidencia_Tipodocumento_codigo_Z, false);
            AddObjectProperty("TipoDocumento_Nome_Z", gxTv_SdtContagemResultadoEvidencia_Tipodocumento_nome_Z, false);
            AddObjectProperty("ContagemResultadoEvidencia_Owner_Z", gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_owner_Z, false);
            AddObjectProperty("ContagemResultadoEvidencia_Entidade_Z", gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_entidade_Z, false);
            AddObjectProperty("ContagemResultado_Demanda_N", gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demanda_N, false);
            AddObjectProperty("ContagemResultado_DemandaFM_N", gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demandafm_N, false);
            AddObjectProperty("Contratada_AreaTrabalhoCod_N", gxTv_SdtContagemResultadoEvidencia_Contratada_areatrabalhocod_N, false);
            AddObjectProperty("ContagemResultadoEvidencia_Link_N", gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_link_N, false);
            AddObjectProperty("ContagemResultadoEvidencia_Arquivo_N", gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_arquivo_N, false);
            AddObjectProperty("ContagemResultadoEvidencia_NomeArq_N", gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_nomearq_N, false);
            AddObjectProperty("ContagemResultadoEvidencia_TipoArq_N", gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_tipoarq_N, false);
            AddObjectProperty("ContagemResultadoEvidencia_Data_N", gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data_N, false);
            AddObjectProperty("ContagemResultadoEvidencia_Descricao_N", gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_descricao_N, false);
            AddObjectProperty("ContagemResultadoEvidencia_RdmnCreated_N", gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated_N, false);
            AddObjectProperty("TipoDocumento_Codigo_N", gxTv_SdtContagemResultadoEvidencia_Tipodocumento_codigo_N, false);
            AddObjectProperty("ContagemResultadoEvidencia_Owner_N", gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_owner_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "ContagemResultadoEvidencia_Codigo" )]
      [  XmlElement( ElementName = "ContagemResultadoEvidencia_Codigo"   )]
      public int gxTpr_Contagemresultadoevidencia_codigo
      {
         get {
            return gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_codigo ;
         }

         set {
            if ( gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_codigo != value )
            {
               gxTv_SdtContagemResultadoEvidencia_Mode = "INS";
               this.gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_codigo_Z_SetNull( );
               this.gxTv_SdtContagemResultadoEvidencia_Contagemresultado_codigo_Z_SetNull( );
               this.gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demanda_Z_SetNull( );
               this.gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demandafm_Z_SetNull( );
               this.gxTv_SdtContagemResultadoEvidencia_Contratada_areatrabalhocod_Z_SetNull( );
               this.gxTv_SdtContagemResultadoEvidencia_Contagemresultado_osfsosfm_Z_SetNull( );
               this.gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_nomearq_Z_SetNull( );
               this.gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_tipoarq_Z_SetNull( );
               this.gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data_Z_SetNull( );
               this.gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated_Z_SetNull( );
               this.gxTv_SdtContagemResultadoEvidencia_Tipodocumento_codigo_Z_SetNull( );
               this.gxTv_SdtContagemResultadoEvidencia_Tipodocumento_nome_Z_SetNull( );
               this.gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_owner_Z_SetNull( );
               this.gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_entidade_Z_SetNull( );
            }
            gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_Codigo" )]
      [  XmlElement( ElementName = "ContagemResultado_Codigo"   )]
      public int gxTpr_Contagemresultado_codigo
      {
         get {
            return gxTv_SdtContagemResultadoEvidencia_Contagemresultado_codigo ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Contagemresultado_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_Demanda" )]
      [  XmlElement( ElementName = "ContagemResultado_Demanda"   )]
      public String gxTpr_Contagemresultado_demanda
      {
         get {
            return gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demanda ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demanda_N = 0;
            gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demanda = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demanda_SetNull( )
      {
         gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demanda_N = 1;
         gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demanda = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demanda_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_DemandaFM" )]
      [  XmlElement( ElementName = "ContagemResultado_DemandaFM"   )]
      public String gxTpr_Contagemresultado_demandafm
      {
         get {
            return gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demandafm ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demandafm_N = 0;
            gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demandafm = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demandafm_SetNull( )
      {
         gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demandafm_N = 1;
         gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demandafm = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demandafm_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_AreaTrabalhoCod" )]
      [  XmlElement( ElementName = "Contratada_AreaTrabalhoCod"   )]
      public int gxTpr_Contratada_areatrabalhocod
      {
         get {
            return gxTv_SdtContagemResultadoEvidencia_Contratada_areatrabalhocod ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Contratada_areatrabalhocod_N = 0;
            gxTv_SdtContagemResultadoEvidencia_Contratada_areatrabalhocod = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoEvidencia_Contratada_areatrabalhocod_SetNull( )
      {
         gxTv_SdtContagemResultadoEvidencia_Contratada_areatrabalhocod_N = 1;
         gxTv_SdtContagemResultadoEvidencia_Contratada_areatrabalhocod = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoEvidencia_Contratada_areatrabalhocod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_OsFsOsFm" )]
      [  XmlElement( ElementName = "ContagemResultado_OsFsOsFm"   )]
      public String gxTpr_Contagemresultado_osfsosfm
      {
         get {
            return gxTv_SdtContagemResultadoEvidencia_Contagemresultado_osfsosfm ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Contagemresultado_osfsosfm = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoEvidencia_Contagemresultado_osfsosfm_SetNull( )
      {
         gxTv_SdtContagemResultadoEvidencia_Contagemresultado_osfsosfm = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoEvidencia_Contagemresultado_osfsosfm_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoEvidencia_Link" )]
      [  XmlElement( ElementName = "ContagemResultadoEvidencia_Link"   )]
      public String gxTpr_Contagemresultadoevidencia_link
      {
         get {
            return gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_link ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_link_N = 0;
            gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_link = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_link_SetNull( )
      {
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_link_N = 1;
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_link = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_link_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoEvidencia_Arquivo" )]
      [  XmlElement( ElementName = "ContagemResultadoEvidencia_Arquivo"   )]
      [GxUpload()]
      public byte[] gxTpr_Contagemresultadoevidencia_arquivo_Blob
      {
         get {
            IGxContext context = this.context == null ? new GxContext() : this.context;
            return context.FileToByteArray( gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_arquivo) ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_arquivo_N = 0;
            IGxContext context = this.context == null ? new GxContext() : this.context;
            gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_arquivo=context.FileFromByteArray( value) ;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      [GxUpload()]
      public String gxTpr_Contagemresultadoevidencia_arquivo
      {
         get {
            return gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_arquivo ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_arquivo_N = 0;
            gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_arquivo = value;
         }

      }

      public void gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_arquivo_SetBlob( String blob ,
                                                                                                 String fileName ,
                                                                                                 String fileType )
      {
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_arquivo = blob;
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_nomearq = fileName;
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_tipoarq = fileType;
         return  ;
      }

      public void gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_arquivo_SetNull( )
      {
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_arquivo_N = 1;
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_arquivo = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_arquivo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoEvidencia_NomeArq" )]
      [  XmlElement( ElementName = "ContagemResultadoEvidencia_NomeArq"   )]
      public String gxTpr_Contagemresultadoevidencia_nomearq
      {
         get {
            return gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_nomearq ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_nomearq_N = 0;
            gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_nomearq = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_nomearq_SetNull( )
      {
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_nomearq_N = 1;
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_nomearq = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_nomearq_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoEvidencia_TipoArq" )]
      [  XmlElement( ElementName = "ContagemResultadoEvidencia_TipoArq"   )]
      public String gxTpr_Contagemresultadoevidencia_tipoarq
      {
         get {
            return gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_tipoarq ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_tipoarq_N = 0;
            gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_tipoarq = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_tipoarq_SetNull( )
      {
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_tipoarq_N = 1;
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_tipoarq = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_tipoarq_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoEvidencia_Data" )]
      [  XmlElement( ElementName = "ContagemResultadoEvidencia_Data"  , IsNullable=true )]
      public string gxTpr_Contagemresultadoevidencia_data_Nullable
      {
         get {
            if ( gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data).value ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data_N = 0;
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data = DateTime.MinValue;
            else
               gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultadoevidencia_data
      {
         get {
            return gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data_N = 0;
            gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data = (DateTime)(value);
         }

      }

      public void gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data_SetNull( )
      {
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data_N = 1;
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoEvidencia_Descricao" )]
      [  XmlElement( ElementName = "ContagemResultadoEvidencia_Descricao"   )]
      public String gxTpr_Contagemresultadoevidencia_descricao
      {
         get {
            return gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_descricao ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_descricao_N = 0;
            gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_descricao = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_descricao_SetNull( )
      {
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_descricao_N = 1;
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_descricao = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_descricao_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoEvidencia_RdmnCreated" )]
      [  XmlElement( ElementName = "ContagemResultadoEvidencia_RdmnCreated"  , IsNullable=true )]
      public string gxTpr_Contagemresultadoevidencia_rdmncreated_Nullable
      {
         get {
            if ( gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated).value ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated_N = 0;
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated = DateTime.MinValue;
            else
               gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultadoevidencia_rdmncreated
      {
         get {
            return gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated_N = 0;
            gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated = (DateTime)(value);
         }

      }

      public void gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated_SetNull( )
      {
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated_N = 1;
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "TipoDocumento_Codigo" )]
      [  XmlElement( ElementName = "TipoDocumento_Codigo"   )]
      public int gxTpr_Tipodocumento_codigo
      {
         get {
            return gxTv_SdtContagemResultadoEvidencia_Tipodocumento_codigo ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Tipodocumento_codigo_N = 0;
            gxTv_SdtContagemResultadoEvidencia_Tipodocumento_codigo = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoEvidencia_Tipodocumento_codigo_SetNull( )
      {
         gxTv_SdtContagemResultadoEvidencia_Tipodocumento_codigo_N = 1;
         gxTv_SdtContagemResultadoEvidencia_Tipodocumento_codigo = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoEvidencia_Tipodocumento_codigo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "TipoDocumento_Nome" )]
      [  XmlElement( ElementName = "TipoDocumento_Nome"   )]
      public String gxTpr_Tipodocumento_nome
      {
         get {
            return gxTv_SdtContagemResultadoEvidencia_Tipodocumento_nome ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Tipodocumento_nome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultadoEvidencia_Owner" )]
      [  XmlElement( ElementName = "ContagemResultadoEvidencia_Owner"   )]
      public int gxTpr_Contagemresultadoevidencia_owner
      {
         get {
            return gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_owner ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_owner_N = 0;
            gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_owner = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_owner_SetNull( )
      {
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_owner_N = 1;
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_owner = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_owner_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoEvidencia_Entidade" )]
      [  XmlElement( ElementName = "ContagemResultadoEvidencia_Entidade"   )]
      public String gxTpr_Contagemresultadoevidencia_entidade
      {
         get {
            return gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_entidade ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_entidade = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_entidade_SetNull( )
      {
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_entidade = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_entidade_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtContagemResultadoEvidencia_Mode ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Mode = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoEvidencia_Mode_SetNull( )
      {
         gxTv_SdtContagemResultadoEvidencia_Mode = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoEvidencia_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtContagemResultadoEvidencia_Initialized ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoEvidencia_Initialized_SetNull( )
      {
         gxTv_SdtContagemResultadoEvidencia_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoEvidencia_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoEvidencia_Codigo_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoEvidencia_Codigo_Z"   )]
      public int gxTpr_Contagemresultadoevidencia_codigo_Z
      {
         get {
            return gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_codigo_Z ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_codigo_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Codigo_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_Codigo_Z"   )]
      public int gxTpr_Contagemresultado_codigo_Z
      {
         get {
            return gxTv_SdtContagemResultadoEvidencia_Contagemresultado_codigo_Z ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Contagemresultado_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoEvidencia_Contagemresultado_codigo_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoEvidencia_Contagemresultado_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoEvidencia_Contagemresultado_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Demanda_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_Demanda_Z"   )]
      public String gxTpr_Contagemresultado_demanda_Z
      {
         get {
            return gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demanda_Z ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demanda_Z = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demanda_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demanda_Z = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demanda_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_DemandaFM_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_DemandaFM_Z"   )]
      public String gxTpr_Contagemresultado_demandafm_Z
      {
         get {
            return gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demandafm_Z ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demandafm_Z = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demandafm_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demandafm_Z = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demandafm_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_AreaTrabalhoCod_Z" )]
      [  XmlElement( ElementName = "Contratada_AreaTrabalhoCod_Z"   )]
      public int gxTpr_Contratada_areatrabalhocod_Z
      {
         get {
            return gxTv_SdtContagemResultadoEvidencia_Contratada_areatrabalhocod_Z ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Contratada_areatrabalhocod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoEvidencia_Contratada_areatrabalhocod_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoEvidencia_Contratada_areatrabalhocod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoEvidencia_Contratada_areatrabalhocod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_OsFsOsFm_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_OsFsOsFm_Z"   )]
      public String gxTpr_Contagemresultado_osfsosfm_Z
      {
         get {
            return gxTv_SdtContagemResultadoEvidencia_Contagemresultado_osfsosfm_Z ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Contagemresultado_osfsosfm_Z = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoEvidencia_Contagemresultado_osfsosfm_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoEvidencia_Contagemresultado_osfsosfm_Z = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoEvidencia_Contagemresultado_osfsosfm_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoEvidencia_NomeArq_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoEvidencia_NomeArq_Z"   )]
      public String gxTpr_Contagemresultadoevidencia_nomearq_Z
      {
         get {
            return gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_nomearq_Z ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_nomearq_Z = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_nomearq_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_nomearq_Z = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_nomearq_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoEvidencia_TipoArq_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoEvidencia_TipoArq_Z"   )]
      public String gxTpr_Contagemresultadoevidencia_tipoarq_Z
      {
         get {
            return gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_tipoarq_Z ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_tipoarq_Z = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_tipoarq_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_tipoarq_Z = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_tipoarq_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoEvidencia_Data_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoEvidencia_Data_Z"  , IsNullable=true )]
      public string gxTpr_Contagemresultadoevidencia_data_Z_Nullable
      {
         get {
            if ( gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data_Z == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data_Z).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data_Z = DateTime.MinValue;
            else
               gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultadoevidencia_data_Z
      {
         get {
            return gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data_Z ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data_Z = (DateTime)(value);
         }

      }

      public void gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoEvidencia_RdmnCreated_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoEvidencia_RdmnCreated_Z"  , IsNullable=true )]
      public string gxTpr_Contagemresultadoevidencia_rdmncreated_Z_Nullable
      {
         get {
            if ( gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated_Z == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated_Z).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated_Z = DateTime.MinValue;
            else
               gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultadoevidencia_rdmncreated_Z
      {
         get {
            return gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated_Z ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated_Z = (DateTime)(value);
         }

      }

      public void gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "TipoDocumento_Codigo_Z" )]
      [  XmlElement( ElementName = "TipoDocumento_Codigo_Z"   )]
      public int gxTpr_Tipodocumento_codigo_Z
      {
         get {
            return gxTv_SdtContagemResultadoEvidencia_Tipodocumento_codigo_Z ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Tipodocumento_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoEvidencia_Tipodocumento_codigo_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoEvidencia_Tipodocumento_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoEvidencia_Tipodocumento_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "TipoDocumento_Nome_Z" )]
      [  XmlElement( ElementName = "TipoDocumento_Nome_Z"   )]
      public String gxTpr_Tipodocumento_nome_Z
      {
         get {
            return gxTv_SdtContagemResultadoEvidencia_Tipodocumento_nome_Z ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Tipodocumento_nome_Z = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoEvidencia_Tipodocumento_nome_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoEvidencia_Tipodocumento_nome_Z = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoEvidencia_Tipodocumento_nome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoEvidencia_Owner_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoEvidencia_Owner_Z"   )]
      public int gxTpr_Contagemresultadoevidencia_owner_Z
      {
         get {
            return gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_owner_Z ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_owner_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_owner_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_owner_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_owner_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoEvidencia_Entidade_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoEvidencia_Entidade_Z"   )]
      public String gxTpr_Contagemresultadoevidencia_entidade_Z
      {
         get {
            return gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_entidade_Z ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_entidade_Z = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_entidade_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_entidade_Z = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_entidade_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Demanda_N" )]
      [  XmlElement( ElementName = "ContagemResultado_Demanda_N"   )]
      public short gxTpr_Contagemresultado_demanda_N
      {
         get {
            return gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demanda_N ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demanda_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demanda_N_SetNull( )
      {
         gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demanda_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demanda_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_DemandaFM_N" )]
      [  XmlElement( ElementName = "ContagemResultado_DemandaFM_N"   )]
      public short gxTpr_Contagemresultado_demandafm_N
      {
         get {
            return gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demandafm_N ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demandafm_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demandafm_N_SetNull( )
      {
         gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demandafm_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demandafm_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_AreaTrabalhoCod_N" )]
      [  XmlElement( ElementName = "Contratada_AreaTrabalhoCod_N"   )]
      public short gxTpr_Contratada_areatrabalhocod_N
      {
         get {
            return gxTv_SdtContagemResultadoEvidencia_Contratada_areatrabalhocod_N ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Contratada_areatrabalhocod_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoEvidencia_Contratada_areatrabalhocod_N_SetNull( )
      {
         gxTv_SdtContagemResultadoEvidencia_Contratada_areatrabalhocod_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoEvidencia_Contratada_areatrabalhocod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoEvidencia_Link_N" )]
      [  XmlElement( ElementName = "ContagemResultadoEvidencia_Link_N"   )]
      public short gxTpr_Contagemresultadoevidencia_link_N
      {
         get {
            return gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_link_N ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_link_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_link_N_SetNull( )
      {
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_link_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_link_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoEvidencia_Arquivo_N" )]
      [  XmlElement( ElementName = "ContagemResultadoEvidencia_Arquivo_N"   )]
      public short gxTpr_Contagemresultadoevidencia_arquivo_N
      {
         get {
            return gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_arquivo_N ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_arquivo_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_arquivo_N_SetNull( )
      {
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_arquivo_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_arquivo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoEvidencia_NomeArq_N" )]
      [  XmlElement( ElementName = "ContagemResultadoEvidencia_NomeArq_N"   )]
      public short gxTpr_Contagemresultadoevidencia_nomearq_N
      {
         get {
            return gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_nomearq_N ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_nomearq_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_nomearq_N_SetNull( )
      {
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_nomearq_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_nomearq_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoEvidencia_TipoArq_N" )]
      [  XmlElement( ElementName = "ContagemResultadoEvidencia_TipoArq_N"   )]
      public short gxTpr_Contagemresultadoevidencia_tipoarq_N
      {
         get {
            return gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_tipoarq_N ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_tipoarq_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_tipoarq_N_SetNull( )
      {
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_tipoarq_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_tipoarq_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoEvidencia_Data_N" )]
      [  XmlElement( ElementName = "ContagemResultadoEvidencia_Data_N"   )]
      public short gxTpr_Contagemresultadoevidencia_data_N
      {
         get {
            return gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data_N ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data_N_SetNull( )
      {
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoEvidencia_Descricao_N" )]
      [  XmlElement( ElementName = "ContagemResultadoEvidencia_Descricao_N"   )]
      public short gxTpr_Contagemresultadoevidencia_descricao_N
      {
         get {
            return gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_descricao_N ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_descricao_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_descricao_N_SetNull( )
      {
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_descricao_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_descricao_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoEvidencia_RdmnCreated_N" )]
      [  XmlElement( ElementName = "ContagemResultadoEvidencia_RdmnCreated_N"   )]
      public short gxTpr_Contagemresultadoevidencia_rdmncreated_N
      {
         get {
            return gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated_N ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated_N_SetNull( )
      {
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "TipoDocumento_Codigo_N" )]
      [  XmlElement( ElementName = "TipoDocumento_Codigo_N"   )]
      public short gxTpr_Tipodocumento_codigo_N
      {
         get {
            return gxTv_SdtContagemResultadoEvidencia_Tipodocumento_codigo_N ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Tipodocumento_codigo_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoEvidencia_Tipodocumento_codigo_N_SetNull( )
      {
         gxTv_SdtContagemResultadoEvidencia_Tipodocumento_codigo_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoEvidencia_Tipodocumento_codigo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoEvidencia_Owner_N" )]
      [  XmlElement( ElementName = "ContagemResultadoEvidencia_Owner_N"   )]
      public short gxTpr_Contagemresultadoevidencia_owner_N
      {
         get {
            return gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_owner_N ;
         }

         set {
            gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_owner_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_owner_N_SetNull( )
      {
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_owner_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_owner_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demanda = "";
         gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demandafm = "";
         gxTv_SdtContagemResultadoEvidencia_Contagemresultado_osfsosfm = "";
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_link = "";
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_arquivo = "";
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_nomearq = "";
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_tipoarq = "";
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data = DateTimeUtil.ServerNow( (IGxContext)(context), "DEFAULT");
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_descricao = "";
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated = (DateTime)(DateTime.MinValue);
         gxTv_SdtContagemResultadoEvidencia_Tipodocumento_nome = "";
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_entidade = "";
         gxTv_SdtContagemResultadoEvidencia_Mode = "";
         gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demanda_Z = "";
         gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demandafm_Z = "";
         gxTv_SdtContagemResultadoEvidencia_Contagemresultado_osfsosfm_Z = "";
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_nomearq_Z = "";
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_tipoarq_Z = "";
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data_Z = (DateTime)(DateTime.MinValue);
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated_Z = (DateTime)(DateTime.MinValue);
         gxTv_SdtContagemResultadoEvidencia_Tipodocumento_nome_Z = "";
         gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_entidade_Z = "";
         sTagName = "";
         sDateCnv = "";
         sNumToPad = "";
         datetime_STZ = (DateTime)(DateTime.MinValue);
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "contagemresultadoevidencia", "GeneXus.Programs.contagemresultadoevidencia_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtContagemResultadoEvidencia_Initialized ;
      private short gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demanda_N ;
      private short gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demandafm_N ;
      private short gxTv_SdtContagemResultadoEvidencia_Contratada_areatrabalhocod_N ;
      private short gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_link_N ;
      private short gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_arquivo_N ;
      private short gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_nomearq_N ;
      private short gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_tipoarq_N ;
      private short gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data_N ;
      private short gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_descricao_N ;
      private short gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated_N ;
      private short gxTv_SdtContagemResultadoEvidencia_Tipodocumento_codigo_N ;
      private short gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_owner_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_codigo ;
      private int gxTv_SdtContagemResultadoEvidencia_Contagemresultado_codigo ;
      private int gxTv_SdtContagemResultadoEvidencia_Contratada_areatrabalhocod ;
      private int gxTv_SdtContagemResultadoEvidencia_Tipodocumento_codigo ;
      private int gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_owner ;
      private int gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_codigo_Z ;
      private int gxTv_SdtContagemResultadoEvidencia_Contagemresultado_codigo_Z ;
      private int gxTv_SdtContagemResultadoEvidencia_Contratada_areatrabalhocod_Z ;
      private int gxTv_SdtContagemResultadoEvidencia_Tipodocumento_codigo_Z ;
      private int gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_owner_Z ;
      private String gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_nomearq ;
      private String gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_tipoarq ;
      private String gxTv_SdtContagemResultadoEvidencia_Tipodocumento_nome ;
      private String gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_entidade ;
      private String gxTv_SdtContagemResultadoEvidencia_Mode ;
      private String gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_nomearq_Z ;
      private String gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_tipoarq_Z ;
      private String gxTv_SdtContagemResultadoEvidencia_Tipodocumento_nome_Z ;
      private String gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_entidade_Z ;
      private String sTagName ;
      private String sDateCnv ;
      private String sNumToPad ;
      private DateTime gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data ;
      private DateTime gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated ;
      private DateTime gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_data_Z ;
      private DateTime gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_rdmncreated_Z ;
      private DateTime datetime_STZ ;
      private String gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_link ;
      private String gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_descricao ;
      private String gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demanda ;
      private String gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demandafm ;
      private String gxTv_SdtContagemResultadoEvidencia_Contagemresultado_osfsosfm ;
      private String gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demanda_Z ;
      private String gxTv_SdtContagemResultadoEvidencia_Contagemresultado_demandafm_Z ;
      private String gxTv_SdtContagemResultadoEvidencia_Contagemresultado_osfsosfm_Z ;
      private String gxTv_SdtContagemResultadoEvidencia_Contagemresultadoevidencia_arquivo ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"ContagemResultadoEvidencia", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtContagemResultadoEvidencia_RESTInterface : GxGenericCollectionItem<SdtContagemResultadoEvidencia>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContagemResultadoEvidencia_RESTInterface( ) : base()
      {
      }

      public SdtContagemResultadoEvidencia_RESTInterface( SdtContagemResultadoEvidencia psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "ContagemResultadoEvidencia_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultadoevidencia_codigo
      {
         get {
            return sdt.gxTpr_Contagemresultadoevidencia_codigo ;
         }

         set {
            sdt.gxTpr_Contagemresultadoevidencia_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_Codigo" , Order = 1 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultado_codigo
      {
         get {
            return sdt.gxTpr_Contagemresultado_codigo ;
         }

         set {
            sdt.gxTpr_Contagemresultado_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_Demanda" , Order = 2 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultado_demanda
      {
         get {
            return sdt.gxTpr_Contagemresultado_demanda ;
         }

         set {
            sdt.gxTpr_Contagemresultado_demanda = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_DemandaFM" , Order = 3 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultado_demandafm
      {
         get {
            return sdt.gxTpr_Contagemresultado_demandafm ;
         }

         set {
            sdt.gxTpr_Contagemresultado_demandafm = (String)(value);
         }

      }

      [DataMember( Name = "Contratada_AreaTrabalhoCod" , Order = 4 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratada_areatrabalhocod
      {
         get {
            return sdt.gxTpr_Contratada_areatrabalhocod ;
         }

         set {
            sdt.gxTpr_Contratada_areatrabalhocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_OsFsOsFm" , Order = 5 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultado_osfsosfm
      {
         get {
            return sdt.gxTpr_Contagemresultado_osfsosfm ;
         }

         set {
            sdt.gxTpr_Contagemresultado_osfsosfm = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultadoEvidencia_Link" , Order = 6 )]
      public String gxTpr_Contagemresultadoevidencia_link
      {
         get {
            return sdt.gxTpr_Contagemresultadoevidencia_link ;
         }

         set {
            sdt.gxTpr_Contagemresultadoevidencia_link = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultadoEvidencia_Arquivo" , Order = 7 )]
      [GxUpload()]
      public String gxTpr_Contagemresultadoevidencia_arquivo
      {
         get {
            return PathUtil.RelativePath( sdt.gxTpr_Contagemresultadoevidencia_arquivo) ;
         }

         set {
            sdt.gxTpr_Contagemresultadoevidencia_arquivo = value;
         }

      }

      [DataMember( Name = "ContagemResultadoEvidencia_NomeArq" , Order = 8 )]
      public String gxTpr_Contagemresultadoevidencia_nomearq
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultadoevidencia_nomearq) ;
         }

         set {
            sdt.gxTpr_Contagemresultadoevidencia_nomearq = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultadoEvidencia_TipoArq" , Order = 9 )]
      public String gxTpr_Contagemresultadoevidencia_tipoarq
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultadoevidencia_tipoarq) ;
         }

         set {
            sdt.gxTpr_Contagemresultadoevidencia_tipoarq = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultadoEvidencia_Data" , Order = 10 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultadoevidencia_data
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Contagemresultadoevidencia_data) ;
         }

         set {
            sdt.gxTpr_Contagemresultadoevidencia_data = DateTimeUtil.CToT2( (String)(value));
         }

      }

      [DataMember( Name = "ContagemResultadoEvidencia_Descricao" , Order = 11 )]
      public String gxTpr_Contagemresultadoevidencia_descricao
      {
         get {
            return sdt.gxTpr_Contagemresultadoevidencia_descricao ;
         }

         set {
            sdt.gxTpr_Contagemresultadoevidencia_descricao = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultadoEvidencia_RdmnCreated" , Order = 12 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultadoevidencia_rdmncreated
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Contagemresultadoevidencia_rdmncreated) ;
         }

         set {
            sdt.gxTpr_Contagemresultadoevidencia_rdmncreated = DateTimeUtil.CToT2( (String)(value));
         }

      }

      [DataMember( Name = "TipoDocumento_Codigo" , Order = 13 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Tipodocumento_codigo
      {
         get {
            return sdt.gxTpr_Tipodocumento_codigo ;
         }

         set {
            sdt.gxTpr_Tipodocumento_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "TipoDocumento_Nome" , Order = 14 )]
      [GxSeudo()]
      public String gxTpr_Tipodocumento_nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Tipodocumento_nome) ;
         }

         set {
            sdt.gxTpr_Tipodocumento_nome = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultadoEvidencia_Owner" , Order = 15 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultadoevidencia_owner
      {
         get {
            return sdt.gxTpr_Contagemresultadoevidencia_owner ;
         }

         set {
            sdt.gxTpr_Contagemresultadoevidencia_owner = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultadoEvidencia_Entidade" , Order = 16 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultadoevidencia_entidade
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultadoevidencia_entidade) ;
         }

         set {
            sdt.gxTpr_Contagemresultadoevidencia_entidade = (String)(value);
         }

      }

      public SdtContagemResultadoEvidencia sdt
      {
         get {
            return (SdtContagemResultadoEvidencia)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtContagemResultadoEvidencia() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 45 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
