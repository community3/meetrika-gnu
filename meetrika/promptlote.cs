/*
               File: PromptLote
        Description: Selecione Lote
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/24/2020 23:42:36.23
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptlote : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptlote( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptlote( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutLote_Codigo ,
                           ref String aP1_InOutLote_Nome )
      {
         this.AV35InOutLote_Codigo = aP0_InOutLote_Codigo;
         this.AV8InOutLote_Nome = aP1_InOutLote_Nome;
         executePrivate();
         aP0_InOutLote_Codigo=this.AV35InOutLote_Codigo;
         aP1_InOutLote_Nome=this.AV8InOutLote_Nome;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_94 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_94_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_94_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17Lote_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Lote_Nome1", AV17Lote_Nome1);
               AV18Lote_UserNom1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Lote_UserNom1", AV18Lote_UserNom1);
               AV20DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
               AV22Lote_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Lote_Nome2", AV22Lote_Nome2);
               AV23Lote_UserNom2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Lote_UserNom2", AV23Lote_UserNom2);
               AV25DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
               AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
               AV27Lote_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Lote_Nome3", AV27Lote_Nome3);
               AV28Lote_UserNom3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Lote_UserNom3", AV28Lote_UserNom3);
               AV19DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV24DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
               AV38TFLote_Numero = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFLote_Numero", AV38TFLote_Numero);
               AV39TFLote_Numero_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFLote_Numero_Sel", AV39TFLote_Numero_Sel);
               AV42TFLote_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFLote_Nome", AV42TFLote_Nome);
               AV43TFLote_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFLote_Nome_Sel", AV43TFLote_Nome_Sel);
               AV46TFLote_Data = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFLote_Data", context.localUtil.TToC( AV46TFLote_Data, 8, 5, 0, 3, "/", ":", " "));
               AV47TFLote_Data_To = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFLote_Data_To", context.localUtil.TToC( AV47TFLote_Data_To, 8, 5, 0, 3, "/", ":", " "));
               AV52TFLote_UserCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFLote_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52TFLote_UserCod), 6, 0)));
               AV53TFLote_UserCod_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFLote_UserCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53TFLote_UserCod_To), 6, 0)));
               AV56TFLote_PessoaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFLote_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56TFLote_PessoaCod), 6, 0)));
               AV57TFLote_PessoaCod_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFLote_PessoaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV57TFLote_PessoaCod_To), 6, 0)));
               AV60TFLote_UserNom = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFLote_UserNom", AV60TFLote_UserNom);
               AV61TFLote_UserNom_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFLote_UserNom_Sel", AV61TFLote_UserNom_Sel);
               AV64TFLote_ValorPF = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFLote_ValorPF", StringUtil.LTrim( StringUtil.Str( AV64TFLote_ValorPF, 18, 5)));
               AV65TFLote_ValorPF_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFLote_ValorPF_To", StringUtil.LTrim( StringUtil.Str( AV65TFLote_ValorPF_To, 18, 5)));
               AV68TFLote_DataIni = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFLote_DataIni", context.localUtil.Format(AV68TFLote_DataIni, "99/99/99"));
               AV69TFLote_DataIni_To = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFLote_DataIni_To", context.localUtil.Format(AV69TFLote_DataIni_To, "99/99/99"));
               AV74TFLote_DataFim = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFLote_DataFim", context.localUtil.Format(AV74TFLote_DataFim, "99/99/99"));
               AV75TFLote_DataFim_To = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFLote_DataFim_To", context.localUtil.Format(AV75TFLote_DataFim_To, "99/99/99"));
               AV80TFLote_QtdeDmn = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFLote_QtdeDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV80TFLote_QtdeDmn), 4, 0)));
               AV81TFLote_QtdeDmn_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFLote_QtdeDmn_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV81TFLote_QtdeDmn_To), 4, 0)));
               AV84TFLote_Valor = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFLote_Valor", StringUtil.LTrim( StringUtil.Str( AV84TFLote_Valor, 18, 5)));
               AV85TFLote_Valor_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85TFLote_Valor_To", StringUtil.LTrim( StringUtil.Str( AV85TFLote_Valor_To, 18, 5)));
               AV40ddo_Lote_NumeroTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ddo_Lote_NumeroTitleControlIdToReplace", AV40ddo_Lote_NumeroTitleControlIdToReplace);
               AV44ddo_Lote_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ddo_Lote_NomeTitleControlIdToReplace", AV44ddo_Lote_NomeTitleControlIdToReplace);
               AV50ddo_Lote_DataTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ddo_Lote_DataTitleControlIdToReplace", AV50ddo_Lote_DataTitleControlIdToReplace);
               AV54ddo_Lote_UserCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54ddo_Lote_UserCodTitleControlIdToReplace", AV54ddo_Lote_UserCodTitleControlIdToReplace);
               AV58ddo_Lote_PessoaCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58ddo_Lote_PessoaCodTitleControlIdToReplace", AV58ddo_Lote_PessoaCodTitleControlIdToReplace);
               AV62ddo_Lote_UserNomTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62ddo_Lote_UserNomTitleControlIdToReplace", AV62ddo_Lote_UserNomTitleControlIdToReplace);
               AV66ddo_Lote_ValorPFTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66ddo_Lote_ValorPFTitleControlIdToReplace", AV66ddo_Lote_ValorPFTitleControlIdToReplace);
               AV72ddo_Lote_DataIniTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72ddo_Lote_DataIniTitleControlIdToReplace", AV72ddo_Lote_DataIniTitleControlIdToReplace);
               AV78ddo_Lote_DataFimTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78ddo_Lote_DataFimTitleControlIdToReplace", AV78ddo_Lote_DataFimTitleControlIdToReplace);
               AV82ddo_Lote_QtdeDmnTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82ddo_Lote_QtdeDmnTitleControlIdToReplace", AV82ddo_Lote_QtdeDmnTitleControlIdToReplace);
               AV86ddo_Lote_ValorTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86ddo_Lote_ValorTitleControlIdToReplace", AV86ddo_Lote_ValorTitleControlIdToReplace);
               AV34Lote_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Lote_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Lote_AreaTrabalhoCod), 6, 0)));
               AV36Lote_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Lote_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Lote_ContratadaCod), 6, 0)));
               AV94Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV30DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
               AV29DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Lote_Nome1, AV18Lote_UserNom1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Lote_Nome2, AV23Lote_UserNom2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27Lote_Nome3, AV28Lote_UserNom3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV38TFLote_Numero, AV39TFLote_Numero_Sel, AV42TFLote_Nome, AV43TFLote_Nome_Sel, AV46TFLote_Data, AV47TFLote_Data_To, AV52TFLote_UserCod, AV53TFLote_UserCod_To, AV56TFLote_PessoaCod, AV57TFLote_PessoaCod_To, AV60TFLote_UserNom, AV61TFLote_UserNom_Sel, AV64TFLote_ValorPF, AV65TFLote_ValorPF_To, AV68TFLote_DataIni, AV69TFLote_DataIni_To, AV74TFLote_DataFim, AV75TFLote_DataFim_To, AV80TFLote_QtdeDmn, AV81TFLote_QtdeDmn_To, AV84TFLote_Valor, AV85TFLote_Valor_To, AV40ddo_Lote_NumeroTitleControlIdToReplace, AV44ddo_Lote_NomeTitleControlIdToReplace, AV50ddo_Lote_DataTitleControlIdToReplace, AV54ddo_Lote_UserCodTitleControlIdToReplace, AV58ddo_Lote_PessoaCodTitleControlIdToReplace, AV62ddo_Lote_UserNomTitleControlIdToReplace, AV66ddo_Lote_ValorPFTitleControlIdToReplace, AV72ddo_Lote_DataIniTitleControlIdToReplace, AV78ddo_Lote_DataFimTitleControlIdToReplace, AV82ddo_Lote_QtdeDmnTitleControlIdToReplace, AV86ddo_Lote_ValorTitleControlIdToReplace, AV34Lote_AreaTrabalhoCod, AV36Lote_ContratadaCod, AV94Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "PromptLote";
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A596Lote_Codigo), "ZZZZZ9");
               GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
               GXUtil.WriteLog("promptlote:[SendSecurityCheck value for]"+"Lote_Codigo:"+context.localUtil.Format( (decimal)(A596Lote_Codigo), "ZZZZZ9"));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV35InOutLote_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35InOutLote_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35InOutLote_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV8InOutLote_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutLote_Nome", AV8InOutLote_Nome);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PABR2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV94Pgmname = "PromptLote";
               context.Gx_err = 0;
               WSBR2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEBR2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void GetLote_ValorOSs( int A596Lote_Codigo )
      {
         /* Create private dbobjects */
         /* Navigation */
         A1058Lote_ValorOSs = 0;
         /* Using cursor H00BR2 */
         pr_default.execute(0, new Object[] {A596Lote_Codigo});
         while ( (pr_default.getStatus(0) != 101) && ( H00BR2_A597ContagemResultado_LoteAceiteCod[0] == A596Lote_Codigo ) )
         {
            GXt_decimal1 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  H00BR2_A456ContagemResultado_Codigo[0], out  GXt_decimal1) ;
            A574ContagemResultado_PFFinal = GXt_decimal1;
            A606ContagemResultado_ValorFinal = (decimal)(A574ContagemResultado_PFFinal*H00BR2_A512ContagemResultado_ValorPF[0]);
            A1058Lote_ValorOSs = (decimal)(A1058Lote_ValorOSs+A606ContagemResultado_ValorFinal);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1058Lote_ValorOSs", StringUtil.LTrim( StringUtil.Str( A1058Lote_ValorOSs, 18, 5)));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203242342374");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptlote.aspx") + "?" + UrlEncode("" +AV35InOutLote_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV8InOutLote_Nome))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_NOME1", StringUtil.RTrim( AV17Lote_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_USERNOM1", StringUtil.RTrim( AV18Lote_UserNom1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV20DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_NOME2", StringUtil.RTrim( AV22Lote_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_USERNOM2", StringUtil.RTrim( AV23Lote_UserNom2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV25DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV26DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_NOME3", StringUtil.RTrim( AV27Lote_Nome3));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTE_USERNOM3", StringUtil.RTrim( AV28Lote_UserNom3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV19DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV24DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFLOTE_NUMERO", StringUtil.RTrim( AV38TFLote_Numero));
         GxWebStd.gx_hidden_field( context, "GXH_vTFLOTE_NUMERO_SEL", StringUtil.RTrim( AV39TFLote_Numero_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFLOTE_NOME", StringUtil.RTrim( AV42TFLote_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFLOTE_NOME_SEL", StringUtil.RTrim( AV43TFLote_Nome_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFLOTE_DATA", context.localUtil.TToC( AV46TFLote_Data, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vTFLOTE_DATA_TO", context.localUtil.TToC( AV47TFLote_Data_To, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vTFLOTE_USERCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV52TFLote_UserCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFLOTE_USERCOD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV53TFLote_UserCod_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFLOTE_PESSOACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV56TFLote_PessoaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFLOTE_PESSOACOD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV57TFLote_PessoaCod_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFLOTE_USERNOM", StringUtil.RTrim( AV60TFLote_UserNom));
         GxWebStd.gx_hidden_field( context, "GXH_vTFLOTE_USERNOM_SEL", StringUtil.RTrim( AV61TFLote_UserNom_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFLOTE_VALORPF", StringUtil.LTrim( StringUtil.NToC( AV64TFLote_ValorPF, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFLOTE_VALORPF_TO", StringUtil.LTrim( StringUtil.NToC( AV65TFLote_ValorPF_To, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFLOTE_DATAINI", context.localUtil.Format(AV68TFLote_DataIni, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFLOTE_DATAINI_TO", context.localUtil.Format(AV69TFLote_DataIni_To, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFLOTE_DATAFIM", context.localUtil.Format(AV74TFLote_DataFim, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFLOTE_DATAFIM_TO", context.localUtil.Format(AV75TFLote_DataFim_To, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFLOTE_QTDEDMN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV80TFLote_QtdeDmn), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFLOTE_QTDEDMN_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV81TFLote_QtdeDmn_To), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFLOTE_VALOR", StringUtil.LTrim( StringUtil.NToC( AV84TFLote_Valor, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFLOTE_VALOR_TO", StringUtil.LTrim( StringUtil.NToC( AV85TFLote_Valor_To, 18, 5, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_94", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_94), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV89GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV90GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV87DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV87DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vLOTE_NUMEROTITLEFILTERDATA", AV37Lote_NumeroTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vLOTE_NUMEROTITLEFILTERDATA", AV37Lote_NumeroTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vLOTE_NOMETITLEFILTERDATA", AV41Lote_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vLOTE_NOMETITLEFILTERDATA", AV41Lote_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vLOTE_DATATITLEFILTERDATA", AV45Lote_DataTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vLOTE_DATATITLEFILTERDATA", AV45Lote_DataTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vLOTE_USERCODTITLEFILTERDATA", AV51Lote_UserCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vLOTE_USERCODTITLEFILTERDATA", AV51Lote_UserCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vLOTE_PESSOACODTITLEFILTERDATA", AV55Lote_PessoaCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vLOTE_PESSOACODTITLEFILTERDATA", AV55Lote_PessoaCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vLOTE_USERNOMTITLEFILTERDATA", AV59Lote_UserNomTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vLOTE_USERNOMTITLEFILTERDATA", AV59Lote_UserNomTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vLOTE_VALORPFTITLEFILTERDATA", AV63Lote_ValorPFTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vLOTE_VALORPFTITLEFILTERDATA", AV63Lote_ValorPFTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vLOTE_DATAINITITLEFILTERDATA", AV67Lote_DataIniTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vLOTE_DATAINITITLEFILTERDATA", AV67Lote_DataIniTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vLOTE_DATAFIMTITLEFILTERDATA", AV73Lote_DataFimTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vLOTE_DATAFIMTITLEFILTERDATA", AV73Lote_DataFimTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vLOTE_QTDEDMNTITLEFILTERDATA", AV79Lote_QtdeDmnTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vLOTE_QTDEDMNTITLEFILTERDATA", AV79Lote_QtdeDmnTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vLOTE_VALORTITLEFILTERDATA", AV83Lote_ValorTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vLOTE_VALORTITLEFILTERDATA", AV83Lote_ValorTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV94Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV30DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV29DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTLOTE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35InOutLote_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTLOTE_NOME", StringUtil.RTrim( AV8InOutLote_Nome));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "LOTE_VALOROSS", StringUtil.LTrim( StringUtil.NToC( A1058Lote_ValorOSs, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "LOTE_VALORGLOSAS", StringUtil.LTrim( StringUtil.NToC( A1057Lote_ValorGlosas, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NUMERO_Caption", StringUtil.RTrim( Ddo_lote_numero_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NUMERO_Tooltip", StringUtil.RTrim( Ddo_lote_numero_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NUMERO_Cls", StringUtil.RTrim( Ddo_lote_numero_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NUMERO_Filteredtext_set", StringUtil.RTrim( Ddo_lote_numero_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NUMERO_Selectedvalue_set", StringUtil.RTrim( Ddo_lote_numero_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NUMERO_Dropdownoptionstype", StringUtil.RTrim( Ddo_lote_numero_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NUMERO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_lote_numero_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NUMERO_Includesortasc", StringUtil.BoolToStr( Ddo_lote_numero_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NUMERO_Includesortdsc", StringUtil.BoolToStr( Ddo_lote_numero_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NUMERO_Sortedstatus", StringUtil.RTrim( Ddo_lote_numero_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NUMERO_Includefilter", StringUtil.BoolToStr( Ddo_lote_numero_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NUMERO_Filtertype", StringUtil.RTrim( Ddo_lote_numero_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NUMERO_Filterisrange", StringUtil.BoolToStr( Ddo_lote_numero_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NUMERO_Includedatalist", StringUtil.BoolToStr( Ddo_lote_numero_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NUMERO_Datalisttype", StringUtil.RTrim( Ddo_lote_numero_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NUMERO_Datalistproc", StringUtil.RTrim( Ddo_lote_numero_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NUMERO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_lote_numero_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NUMERO_Sortasc", StringUtil.RTrim( Ddo_lote_numero_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NUMERO_Sortdsc", StringUtil.RTrim( Ddo_lote_numero_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NUMERO_Loadingdata", StringUtil.RTrim( Ddo_lote_numero_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NUMERO_Cleanfilter", StringUtil.RTrim( Ddo_lote_numero_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NUMERO_Noresultsfound", StringUtil.RTrim( Ddo_lote_numero_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NUMERO_Searchbuttontext", StringUtil.RTrim( Ddo_lote_numero_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NOME_Caption", StringUtil.RTrim( Ddo_lote_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NOME_Tooltip", StringUtil.RTrim( Ddo_lote_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NOME_Cls", StringUtil.RTrim( Ddo_lote_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_lote_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_lote_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_lote_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_lote_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_lote_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_lote_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NOME_Sortedstatus", StringUtil.RTrim( Ddo_lote_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NOME_Includefilter", StringUtil.BoolToStr( Ddo_lote_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NOME_Filtertype", StringUtil.RTrim( Ddo_lote_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_lote_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_lote_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NOME_Datalisttype", StringUtil.RTrim( Ddo_lote_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NOME_Datalistproc", StringUtil.RTrim( Ddo_lote_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_lote_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NOME_Sortasc", StringUtil.RTrim( Ddo_lote_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NOME_Sortdsc", StringUtil.RTrim( Ddo_lote_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NOME_Loadingdata", StringUtil.RTrim( Ddo_lote_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NOME_Cleanfilter", StringUtil.RTrim( Ddo_lote_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NOME_Noresultsfound", StringUtil.RTrim( Ddo_lote_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_lote_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATA_Caption", StringUtil.RTrim( Ddo_lote_data_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATA_Tooltip", StringUtil.RTrim( Ddo_lote_data_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATA_Cls", StringUtil.RTrim( Ddo_lote_data_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATA_Filteredtext_set", StringUtil.RTrim( Ddo_lote_data_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATA_Filteredtextto_set", StringUtil.RTrim( Ddo_lote_data_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATA_Dropdownoptionstype", StringUtil.RTrim( Ddo_lote_data_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_lote_data_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATA_Includesortasc", StringUtil.BoolToStr( Ddo_lote_data_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATA_Includesortdsc", StringUtil.BoolToStr( Ddo_lote_data_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATA_Sortedstatus", StringUtil.RTrim( Ddo_lote_data_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATA_Includefilter", StringUtil.BoolToStr( Ddo_lote_data_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATA_Filtertype", StringUtil.RTrim( Ddo_lote_data_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATA_Filterisrange", StringUtil.BoolToStr( Ddo_lote_data_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATA_Includedatalist", StringUtil.BoolToStr( Ddo_lote_data_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATA_Sortasc", StringUtil.RTrim( Ddo_lote_data_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATA_Sortdsc", StringUtil.RTrim( Ddo_lote_data_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATA_Cleanfilter", StringUtil.RTrim( Ddo_lote_data_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATA_Rangefilterfrom", StringUtil.RTrim( Ddo_lote_data_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATA_Rangefilterto", StringUtil.RTrim( Ddo_lote_data_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATA_Searchbuttontext", StringUtil.RTrim( Ddo_lote_data_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERCOD_Caption", StringUtil.RTrim( Ddo_lote_usercod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERCOD_Tooltip", StringUtil.RTrim( Ddo_lote_usercod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERCOD_Cls", StringUtil.RTrim( Ddo_lote_usercod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERCOD_Filteredtext_set", StringUtil.RTrim( Ddo_lote_usercod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERCOD_Filteredtextto_set", StringUtil.RTrim( Ddo_lote_usercod_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERCOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_lote_usercod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERCOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_lote_usercod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERCOD_Includesortasc", StringUtil.BoolToStr( Ddo_lote_usercod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERCOD_Includesortdsc", StringUtil.BoolToStr( Ddo_lote_usercod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERCOD_Sortedstatus", StringUtil.RTrim( Ddo_lote_usercod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERCOD_Includefilter", StringUtil.BoolToStr( Ddo_lote_usercod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERCOD_Filtertype", StringUtil.RTrim( Ddo_lote_usercod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERCOD_Filterisrange", StringUtil.BoolToStr( Ddo_lote_usercod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERCOD_Includedatalist", StringUtil.BoolToStr( Ddo_lote_usercod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERCOD_Sortasc", StringUtil.RTrim( Ddo_lote_usercod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERCOD_Sortdsc", StringUtil.RTrim( Ddo_lote_usercod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERCOD_Cleanfilter", StringUtil.RTrim( Ddo_lote_usercod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERCOD_Rangefilterfrom", StringUtil.RTrim( Ddo_lote_usercod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERCOD_Rangefilterto", StringUtil.RTrim( Ddo_lote_usercod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERCOD_Searchbuttontext", StringUtil.RTrim( Ddo_lote_usercod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_PESSOACOD_Caption", StringUtil.RTrim( Ddo_lote_pessoacod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_PESSOACOD_Tooltip", StringUtil.RTrim( Ddo_lote_pessoacod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_PESSOACOD_Cls", StringUtil.RTrim( Ddo_lote_pessoacod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_PESSOACOD_Filteredtext_set", StringUtil.RTrim( Ddo_lote_pessoacod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_PESSOACOD_Filteredtextto_set", StringUtil.RTrim( Ddo_lote_pessoacod_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_PESSOACOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_lote_pessoacod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_PESSOACOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_lote_pessoacod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_PESSOACOD_Includesortasc", StringUtil.BoolToStr( Ddo_lote_pessoacod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_PESSOACOD_Includesortdsc", StringUtil.BoolToStr( Ddo_lote_pessoacod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_PESSOACOD_Sortedstatus", StringUtil.RTrim( Ddo_lote_pessoacod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_PESSOACOD_Includefilter", StringUtil.BoolToStr( Ddo_lote_pessoacod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_PESSOACOD_Filtertype", StringUtil.RTrim( Ddo_lote_pessoacod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_PESSOACOD_Filterisrange", StringUtil.BoolToStr( Ddo_lote_pessoacod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_PESSOACOD_Includedatalist", StringUtil.BoolToStr( Ddo_lote_pessoacod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_PESSOACOD_Sortasc", StringUtil.RTrim( Ddo_lote_pessoacod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_PESSOACOD_Sortdsc", StringUtil.RTrim( Ddo_lote_pessoacod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_PESSOACOD_Cleanfilter", StringUtil.RTrim( Ddo_lote_pessoacod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_PESSOACOD_Rangefilterfrom", StringUtil.RTrim( Ddo_lote_pessoacod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_PESSOACOD_Rangefilterto", StringUtil.RTrim( Ddo_lote_pessoacod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_PESSOACOD_Searchbuttontext", StringUtil.RTrim( Ddo_lote_pessoacod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERNOM_Caption", StringUtil.RTrim( Ddo_lote_usernom_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERNOM_Tooltip", StringUtil.RTrim( Ddo_lote_usernom_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERNOM_Cls", StringUtil.RTrim( Ddo_lote_usernom_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERNOM_Filteredtext_set", StringUtil.RTrim( Ddo_lote_usernom_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERNOM_Selectedvalue_set", StringUtil.RTrim( Ddo_lote_usernom_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERNOM_Dropdownoptionstype", StringUtil.RTrim( Ddo_lote_usernom_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERNOM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_lote_usernom_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERNOM_Includesortasc", StringUtil.BoolToStr( Ddo_lote_usernom_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERNOM_Includesortdsc", StringUtil.BoolToStr( Ddo_lote_usernom_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERNOM_Sortedstatus", StringUtil.RTrim( Ddo_lote_usernom_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERNOM_Includefilter", StringUtil.BoolToStr( Ddo_lote_usernom_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERNOM_Filtertype", StringUtil.RTrim( Ddo_lote_usernom_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERNOM_Filterisrange", StringUtil.BoolToStr( Ddo_lote_usernom_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERNOM_Includedatalist", StringUtil.BoolToStr( Ddo_lote_usernom_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERNOM_Datalisttype", StringUtil.RTrim( Ddo_lote_usernom_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERNOM_Datalistproc", StringUtil.RTrim( Ddo_lote_usernom_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERNOM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_lote_usernom_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERNOM_Sortasc", StringUtil.RTrim( Ddo_lote_usernom_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERNOM_Sortdsc", StringUtil.RTrim( Ddo_lote_usernom_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERNOM_Loadingdata", StringUtil.RTrim( Ddo_lote_usernom_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERNOM_Cleanfilter", StringUtil.RTrim( Ddo_lote_usernom_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERNOM_Noresultsfound", StringUtil.RTrim( Ddo_lote_usernom_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERNOM_Searchbuttontext", StringUtil.RTrim( Ddo_lote_usernom_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_VALORPF_Caption", StringUtil.RTrim( Ddo_lote_valorpf_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_VALORPF_Tooltip", StringUtil.RTrim( Ddo_lote_valorpf_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_VALORPF_Cls", StringUtil.RTrim( Ddo_lote_valorpf_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_VALORPF_Filteredtext_set", StringUtil.RTrim( Ddo_lote_valorpf_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_VALORPF_Filteredtextto_set", StringUtil.RTrim( Ddo_lote_valorpf_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_VALORPF_Dropdownoptionstype", StringUtil.RTrim( Ddo_lote_valorpf_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_VALORPF_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_lote_valorpf_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_VALORPF_Includesortasc", StringUtil.BoolToStr( Ddo_lote_valorpf_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_VALORPF_Includesortdsc", StringUtil.BoolToStr( Ddo_lote_valorpf_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_VALORPF_Sortedstatus", StringUtil.RTrim( Ddo_lote_valorpf_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_VALORPF_Includefilter", StringUtil.BoolToStr( Ddo_lote_valorpf_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_VALORPF_Filtertype", StringUtil.RTrim( Ddo_lote_valorpf_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_VALORPF_Filterisrange", StringUtil.BoolToStr( Ddo_lote_valorpf_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_VALORPF_Includedatalist", StringUtil.BoolToStr( Ddo_lote_valorpf_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_VALORPF_Sortasc", StringUtil.RTrim( Ddo_lote_valorpf_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_VALORPF_Sortdsc", StringUtil.RTrim( Ddo_lote_valorpf_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_VALORPF_Cleanfilter", StringUtil.RTrim( Ddo_lote_valorpf_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_VALORPF_Rangefilterfrom", StringUtil.RTrim( Ddo_lote_valorpf_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_VALORPF_Rangefilterto", StringUtil.RTrim( Ddo_lote_valorpf_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_VALORPF_Searchbuttontext", StringUtil.RTrim( Ddo_lote_valorpf_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATAINI_Caption", StringUtil.RTrim( Ddo_lote_dataini_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATAINI_Tooltip", StringUtil.RTrim( Ddo_lote_dataini_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATAINI_Cls", StringUtil.RTrim( Ddo_lote_dataini_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATAINI_Filteredtext_set", StringUtil.RTrim( Ddo_lote_dataini_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATAINI_Filteredtextto_set", StringUtil.RTrim( Ddo_lote_dataini_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATAINI_Dropdownoptionstype", StringUtil.RTrim( Ddo_lote_dataini_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATAINI_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_lote_dataini_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATAINI_Includesortasc", StringUtil.BoolToStr( Ddo_lote_dataini_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATAINI_Includesortdsc", StringUtil.BoolToStr( Ddo_lote_dataini_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATAINI_Includefilter", StringUtil.BoolToStr( Ddo_lote_dataini_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATAINI_Filtertype", StringUtil.RTrim( Ddo_lote_dataini_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATAINI_Filterisrange", StringUtil.BoolToStr( Ddo_lote_dataini_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATAINI_Includedatalist", StringUtil.BoolToStr( Ddo_lote_dataini_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATAINI_Cleanfilter", StringUtil.RTrim( Ddo_lote_dataini_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATAINI_Rangefilterfrom", StringUtil.RTrim( Ddo_lote_dataini_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATAINI_Rangefilterto", StringUtil.RTrim( Ddo_lote_dataini_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATAINI_Searchbuttontext", StringUtil.RTrim( Ddo_lote_dataini_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATAFIM_Caption", StringUtil.RTrim( Ddo_lote_datafim_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATAFIM_Tooltip", StringUtil.RTrim( Ddo_lote_datafim_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATAFIM_Cls", StringUtil.RTrim( Ddo_lote_datafim_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATAFIM_Filteredtext_set", StringUtil.RTrim( Ddo_lote_datafim_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATAFIM_Filteredtextto_set", StringUtil.RTrim( Ddo_lote_datafim_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATAFIM_Dropdownoptionstype", StringUtil.RTrim( Ddo_lote_datafim_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATAFIM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_lote_datafim_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATAFIM_Includesortasc", StringUtil.BoolToStr( Ddo_lote_datafim_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATAFIM_Includesortdsc", StringUtil.BoolToStr( Ddo_lote_datafim_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATAFIM_Includefilter", StringUtil.BoolToStr( Ddo_lote_datafim_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATAFIM_Filtertype", StringUtil.RTrim( Ddo_lote_datafim_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATAFIM_Filterisrange", StringUtil.BoolToStr( Ddo_lote_datafim_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATAFIM_Includedatalist", StringUtil.BoolToStr( Ddo_lote_datafim_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATAFIM_Cleanfilter", StringUtil.RTrim( Ddo_lote_datafim_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATAFIM_Rangefilterfrom", StringUtil.RTrim( Ddo_lote_datafim_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATAFIM_Rangefilterto", StringUtil.RTrim( Ddo_lote_datafim_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATAFIM_Searchbuttontext", StringUtil.RTrim( Ddo_lote_datafim_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_QTDEDMN_Caption", StringUtil.RTrim( Ddo_lote_qtdedmn_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_QTDEDMN_Tooltip", StringUtil.RTrim( Ddo_lote_qtdedmn_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_QTDEDMN_Cls", StringUtil.RTrim( Ddo_lote_qtdedmn_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_QTDEDMN_Filteredtext_set", StringUtil.RTrim( Ddo_lote_qtdedmn_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_QTDEDMN_Filteredtextto_set", StringUtil.RTrim( Ddo_lote_qtdedmn_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_QTDEDMN_Dropdownoptionstype", StringUtil.RTrim( Ddo_lote_qtdedmn_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_QTDEDMN_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_lote_qtdedmn_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_QTDEDMN_Includesortasc", StringUtil.BoolToStr( Ddo_lote_qtdedmn_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_QTDEDMN_Includesortdsc", StringUtil.BoolToStr( Ddo_lote_qtdedmn_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_QTDEDMN_Includefilter", StringUtil.BoolToStr( Ddo_lote_qtdedmn_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_QTDEDMN_Filtertype", StringUtil.RTrim( Ddo_lote_qtdedmn_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_QTDEDMN_Filterisrange", StringUtil.BoolToStr( Ddo_lote_qtdedmn_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_QTDEDMN_Includedatalist", StringUtil.BoolToStr( Ddo_lote_qtdedmn_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_QTDEDMN_Cleanfilter", StringUtil.RTrim( Ddo_lote_qtdedmn_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_QTDEDMN_Rangefilterfrom", StringUtil.RTrim( Ddo_lote_qtdedmn_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_QTDEDMN_Rangefilterto", StringUtil.RTrim( Ddo_lote_qtdedmn_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_QTDEDMN_Searchbuttontext", StringUtil.RTrim( Ddo_lote_qtdedmn_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_VALOR_Caption", StringUtil.RTrim( Ddo_lote_valor_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_VALOR_Tooltip", StringUtil.RTrim( Ddo_lote_valor_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_VALOR_Cls", StringUtil.RTrim( Ddo_lote_valor_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_VALOR_Filteredtext_set", StringUtil.RTrim( Ddo_lote_valor_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_VALOR_Filteredtextto_set", StringUtil.RTrim( Ddo_lote_valor_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_VALOR_Dropdownoptionstype", StringUtil.RTrim( Ddo_lote_valor_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_VALOR_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_lote_valor_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_VALOR_Includesortasc", StringUtil.BoolToStr( Ddo_lote_valor_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_VALOR_Includesortdsc", StringUtil.BoolToStr( Ddo_lote_valor_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_VALOR_Includefilter", StringUtil.BoolToStr( Ddo_lote_valor_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_VALOR_Filtertype", StringUtil.RTrim( Ddo_lote_valor_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_VALOR_Filterisrange", StringUtil.BoolToStr( Ddo_lote_valor_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_VALOR_Includedatalist", StringUtil.BoolToStr( Ddo_lote_valor_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_VALOR_Cleanfilter", StringUtil.RTrim( Ddo_lote_valor_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_VALOR_Rangefilterfrom", StringUtil.RTrim( Ddo_lote_valor_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_VALOR_Rangefilterto", StringUtil.RTrim( Ddo_lote_valor_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_VALOR_Searchbuttontext", StringUtil.RTrim( Ddo_lote_valor_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NUMERO_Activeeventkey", StringUtil.RTrim( Ddo_lote_numero_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NUMERO_Filteredtext_get", StringUtil.RTrim( Ddo_lote_numero_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NUMERO_Selectedvalue_get", StringUtil.RTrim( Ddo_lote_numero_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NOME_Activeeventkey", StringUtil.RTrim( Ddo_lote_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_lote_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_lote_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATA_Activeeventkey", StringUtil.RTrim( Ddo_lote_data_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATA_Filteredtext_get", StringUtil.RTrim( Ddo_lote_data_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATA_Filteredtextto_get", StringUtil.RTrim( Ddo_lote_data_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERCOD_Activeeventkey", StringUtil.RTrim( Ddo_lote_usercod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERCOD_Filteredtext_get", StringUtil.RTrim( Ddo_lote_usercod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERCOD_Filteredtextto_get", StringUtil.RTrim( Ddo_lote_usercod_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_PESSOACOD_Activeeventkey", StringUtil.RTrim( Ddo_lote_pessoacod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_PESSOACOD_Filteredtext_get", StringUtil.RTrim( Ddo_lote_pessoacod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_PESSOACOD_Filteredtextto_get", StringUtil.RTrim( Ddo_lote_pessoacod_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERNOM_Activeeventkey", StringUtil.RTrim( Ddo_lote_usernom_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERNOM_Filteredtext_get", StringUtil.RTrim( Ddo_lote_usernom_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_USERNOM_Selectedvalue_get", StringUtil.RTrim( Ddo_lote_usernom_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_VALORPF_Activeeventkey", StringUtil.RTrim( Ddo_lote_valorpf_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_VALORPF_Filteredtext_get", StringUtil.RTrim( Ddo_lote_valorpf_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_VALORPF_Filteredtextto_get", StringUtil.RTrim( Ddo_lote_valorpf_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATAINI_Activeeventkey", StringUtil.RTrim( Ddo_lote_dataini_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATAINI_Filteredtext_get", StringUtil.RTrim( Ddo_lote_dataini_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATAINI_Filteredtextto_get", StringUtil.RTrim( Ddo_lote_dataini_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATAFIM_Activeeventkey", StringUtil.RTrim( Ddo_lote_datafim_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATAFIM_Filteredtext_get", StringUtil.RTrim( Ddo_lote_datafim_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_DATAFIM_Filteredtextto_get", StringUtil.RTrim( Ddo_lote_datafim_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_QTDEDMN_Activeeventkey", StringUtil.RTrim( Ddo_lote_qtdedmn_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_QTDEDMN_Filteredtext_get", StringUtil.RTrim( Ddo_lote_qtdedmn_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_QTDEDMN_Filteredtextto_get", StringUtil.RTrim( Ddo_lote_qtdedmn_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_VALOR_Activeeventkey", StringUtil.RTrim( Ddo_lote_valor_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_VALOR_Filteredtext_get", StringUtil.RTrim( Ddo_lote_valor_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_LOTE_VALOR_Filteredtextto_get", StringUtil.RTrim( Ddo_lote_valor_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "PromptLote";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A596Lote_Codigo), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("promptlote:[SendSecurityCheck value for]"+"Lote_Codigo:"+context.localUtil.Format( (decimal)(A596Lote_Codigo), "ZZZZZ9"));
      }

      protected void RenderHtmlCloseFormBR2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptLote" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Lote" ;
      }

      protected void WBBR0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_BR2( true) ;
         }
         else
         {
            wb_table1_2_BR2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_BR2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtLote_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A596Lote_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A596Lote_Codigo), "ZZZZZ9"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLote_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtLote_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_PromptLote.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV19DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(112, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,112);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV24DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(113, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,113);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTflote_numero_Internalname, StringUtil.RTrim( AV38TFLote_Numero), StringUtil.RTrim( context.localUtil.Format( AV38TFLote_Numero, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,114);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTflote_numero_Jsonclick, 0, "Attribute", "", "", "", edtavTflote_numero_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptLote.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTflote_numero_sel_Internalname, StringUtil.RTrim( AV39TFLote_Numero_Sel), StringUtil.RTrim( context.localUtil.Format( AV39TFLote_Numero_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,115);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTflote_numero_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTflote_numero_sel_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptLote.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTflote_nome_Internalname, StringUtil.RTrim( AV42TFLote_Nome), StringUtil.RTrim( context.localUtil.Format( AV42TFLote_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,116);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTflote_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTflote_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptLote.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTflote_nome_sel_Internalname, StringUtil.RTrim( AV43TFLote_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV43TFLote_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,117);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTflote_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTflote_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptLote.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'',false,'" + sGXsfl_94_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTflote_data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTflote_data_Internalname, context.localUtil.TToC( AV46TFLote_Data, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV46TFLote_Data, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,118);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTflote_data_Jsonclick, 0, "Attribute", "", "", "", edtavTflote_data_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptLote.htm");
            GxWebStd.gx_bitmap( context, edtavTflote_data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTflote_data_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptLote.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'',false,'" + sGXsfl_94_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTflote_data_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTflote_data_to_Internalname, context.localUtil.TToC( AV47TFLote_Data_To, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV47TFLote_Data_To, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,119);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTflote_data_to_Jsonclick, 0, "Attribute", "", "", "", edtavTflote_data_to_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptLote.htm");
            GxWebStd.gx_bitmap( context, edtavTflote_data_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTflote_data_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptLote.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_lote_dataauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 121,'',false,'" + sGXsfl_94_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_lote_dataauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_lote_dataauxdate_Internalname, context.localUtil.Format(AV48DDO_Lote_DataAuxDate, "99/99/99"), context.localUtil.Format( AV48DDO_Lote_DataAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,121);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_lote_dataauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptLote.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_lote_dataauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptLote.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 122,'',false,'" + sGXsfl_94_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_lote_dataauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_lote_dataauxdateto_Internalname, context.localUtil.Format(AV49DDO_Lote_DataAuxDateTo, "99/99/99"), context.localUtil.Format( AV49DDO_Lote_DataAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,122);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_lote_dataauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptLote.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_lote_dataauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptLote.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 123,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTflote_usercod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV52TFLote_UserCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV52TFLote_UserCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,123);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTflote_usercod_Jsonclick, 0, "Attribute", "", "", "", edtavTflote_usercod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptLote.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 124,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTflote_usercod_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV53TFLote_UserCod_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV53TFLote_UserCod_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,124);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTflote_usercod_to_Jsonclick, 0, "Attribute", "", "", "", edtavTflote_usercod_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptLote.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 125,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTflote_pessoacod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV56TFLote_PessoaCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV56TFLote_PessoaCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,125);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTflote_pessoacod_Jsonclick, 0, "Attribute", "", "", "", edtavTflote_pessoacod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptLote.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 126,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTflote_pessoacod_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV57TFLote_PessoaCod_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV57TFLote_PessoaCod_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,126);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTflote_pessoacod_to_Jsonclick, 0, "Attribute", "", "", "", edtavTflote_pessoacod_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptLote.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 127,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTflote_usernom_Internalname, StringUtil.RTrim( AV60TFLote_UserNom), StringUtil.RTrim( context.localUtil.Format( AV60TFLote_UserNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,127);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTflote_usernom_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTflote_usernom_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptLote.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 128,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTflote_usernom_sel_Internalname, StringUtil.RTrim( AV61TFLote_UserNom_Sel), StringUtil.RTrim( context.localUtil.Format( AV61TFLote_UserNom_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,128);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTflote_usernom_sel_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTflote_usernom_sel_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptLote.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 129,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTflote_valorpf_Internalname, StringUtil.LTrim( StringUtil.NToC( AV64TFLote_ValorPF, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV64TFLote_ValorPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,129);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTflote_valorpf_Jsonclick, 0, "Attribute", "", "", "", edtavTflote_valorpf_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptLote.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 130,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTflote_valorpf_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV65TFLote_ValorPF_To, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV65TFLote_ValorPF_To, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,130);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTflote_valorpf_to_Jsonclick, 0, "Attribute", "", "", "", edtavTflote_valorpf_to_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptLote.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 131,'',false,'" + sGXsfl_94_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTflote_dataini_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTflote_dataini_Internalname, context.localUtil.Format(AV68TFLote_DataIni, "99/99/99"), context.localUtil.Format( AV68TFLote_DataIni, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,131);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTflote_dataini_Jsonclick, 0, "Attribute", "", "", "", edtavTflote_dataini_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptLote.htm");
            GxWebStd.gx_bitmap( context, edtavTflote_dataini_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTflote_dataini_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptLote.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 132,'',false,'" + sGXsfl_94_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTflote_dataini_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTflote_dataini_to_Internalname, context.localUtil.Format(AV69TFLote_DataIni_To, "99/99/99"), context.localUtil.Format( AV69TFLote_DataIni_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,132);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTflote_dataini_to_Jsonclick, 0, "Attribute", "", "", "", edtavTflote_dataini_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptLote.htm");
            GxWebStd.gx_bitmap( context, edtavTflote_dataini_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTflote_dataini_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptLote.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_lote_datainiauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 134,'',false,'" + sGXsfl_94_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_lote_datainiauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_lote_datainiauxdate_Internalname, context.localUtil.Format(AV70DDO_Lote_DataIniAuxDate, "99/99/99"), context.localUtil.Format( AV70DDO_Lote_DataIniAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,134);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_lote_datainiauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptLote.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_lote_datainiauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptLote.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 135,'',false,'" + sGXsfl_94_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_lote_datainiauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_lote_datainiauxdateto_Internalname, context.localUtil.Format(AV71DDO_Lote_DataIniAuxDateTo, "99/99/99"), context.localUtil.Format( AV71DDO_Lote_DataIniAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,135);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_lote_datainiauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptLote.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_lote_datainiauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptLote.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 136,'',false,'" + sGXsfl_94_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTflote_datafim_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTflote_datafim_Internalname, context.localUtil.Format(AV74TFLote_DataFim, "99/99/99"), context.localUtil.Format( AV74TFLote_DataFim, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,136);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTflote_datafim_Jsonclick, 0, "Attribute", "", "", "", edtavTflote_datafim_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptLote.htm");
            GxWebStd.gx_bitmap( context, edtavTflote_datafim_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTflote_datafim_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptLote.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 137,'',false,'" + sGXsfl_94_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTflote_datafim_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTflote_datafim_to_Internalname, context.localUtil.Format(AV75TFLote_DataFim_To, "99/99/99"), context.localUtil.Format( AV75TFLote_DataFim_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,137);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTflote_datafim_to_Jsonclick, 0, "Attribute", "", "", "", edtavTflote_datafim_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptLote.htm");
            GxWebStd.gx_bitmap( context, edtavTflote_datafim_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTflote_datafim_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptLote.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_lote_datafimauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 139,'',false,'" + sGXsfl_94_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_lote_datafimauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_lote_datafimauxdate_Internalname, context.localUtil.Format(AV76DDO_Lote_DataFimAuxDate, "99/99/99"), context.localUtil.Format( AV76DDO_Lote_DataFimAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,139);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_lote_datafimauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptLote.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_lote_datafimauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptLote.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 140,'',false,'" + sGXsfl_94_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_lote_datafimauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_lote_datafimauxdateto_Internalname, context.localUtil.Format(AV77DDO_Lote_DataFimAuxDateTo, "99/99/99"), context.localUtil.Format( AV77DDO_Lote_DataFimAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,140);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_lote_datafimauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptLote.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_lote_datafimauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptLote.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 141,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTflote_qtdedmn_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV80TFLote_QtdeDmn), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV80TFLote_QtdeDmn), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,141);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTflote_qtdedmn_Jsonclick, 0, "Attribute", "", "", "", edtavTflote_qtdedmn_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptLote.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 142,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTflote_qtdedmn_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV81TFLote_QtdeDmn_To), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV81TFLote_QtdeDmn_To), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,142);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTflote_qtdedmn_to_Jsonclick, 0, "Attribute", "", "", "", edtavTflote_qtdedmn_to_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptLote.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 143,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTflote_valor_Internalname, StringUtil.LTrim( StringUtil.NToC( AV84TFLote_Valor, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV84TFLote_Valor, "ZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,143);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTflote_valor_Jsonclick, 0, "Attribute", "", "", "", edtavTflote_valor_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptLote.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 144,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTflote_valor_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV85TFLote_Valor_To, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV85TFLote_Valor_To, "ZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,144);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTflote_valor_to_Jsonclick, 0, "Attribute", "", "", "", edtavTflote_valor_to_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptLote.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_LOTE_NUMEROContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 146,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_lote_numerotitlecontrolidtoreplace_Internalname, AV40ddo_Lote_NumeroTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,146);\"", 0, edtavDdo_lote_numerotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptLote.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_LOTE_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 148,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_lote_nometitlecontrolidtoreplace_Internalname, AV44ddo_Lote_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,148);\"", 0, edtavDdo_lote_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptLote.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_LOTE_DATAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 150,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_lote_datatitlecontrolidtoreplace_Internalname, AV50ddo_Lote_DataTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,150);\"", 0, edtavDdo_lote_datatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptLote.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_LOTE_USERCODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 152,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_lote_usercodtitlecontrolidtoreplace_Internalname, AV54ddo_Lote_UserCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,152);\"", 0, edtavDdo_lote_usercodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptLote.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_LOTE_PESSOACODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 154,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_lote_pessoacodtitlecontrolidtoreplace_Internalname, AV58ddo_Lote_PessoaCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,154);\"", 0, edtavDdo_lote_pessoacodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptLote.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_LOTE_USERNOMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 156,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_lote_usernomtitlecontrolidtoreplace_Internalname, AV62ddo_Lote_UserNomTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,156);\"", 0, edtavDdo_lote_usernomtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptLote.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_LOTE_VALORPFContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 158,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_lote_valorpftitlecontrolidtoreplace_Internalname, AV66ddo_Lote_ValorPFTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,158);\"", 0, edtavDdo_lote_valorpftitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptLote.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_LOTE_DATAINIContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 160,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_lote_datainititlecontrolidtoreplace_Internalname, AV72ddo_Lote_DataIniTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,160);\"", 0, edtavDdo_lote_datainititlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptLote.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_LOTE_DATAFIMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 162,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_lote_datafimtitlecontrolidtoreplace_Internalname, AV78ddo_Lote_DataFimTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,162);\"", 0, edtavDdo_lote_datafimtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptLote.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_LOTE_QTDEDMNContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 164,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_lote_qtdedmntitlecontrolidtoreplace_Internalname, AV82ddo_Lote_QtdeDmnTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,164);\"", 0, edtavDdo_lote_qtdedmntitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptLote.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_LOTE_VALORContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 166,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_lote_valortitlecontrolidtoreplace_Internalname, AV86ddo_Lote_ValorTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,166);\"", 0, edtavDdo_lote_valortitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptLote.htm");
         }
         wbLoad = true;
      }

      protected void STARTBR2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Lote", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPBR0( ) ;
      }

      protected void WSBR2( )
      {
         STARTBR2( ) ;
         EVTBR2( ) ;
      }

      protected void EVTBR2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11BR2 */
                           E11BR2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_LOTE_NUMERO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12BR2 */
                           E12BR2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_LOTE_NOME.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13BR2 */
                           E13BR2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_LOTE_DATA.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14BR2 */
                           E14BR2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_LOTE_USERCOD.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15BR2 */
                           E15BR2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_LOTE_PESSOACOD.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16BR2 */
                           E16BR2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_LOTE_USERNOM.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17BR2 */
                           E17BR2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_LOTE_VALORPF.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18BR2 */
                           E18BR2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_LOTE_DATAINI.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19BR2 */
                           E19BR2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_LOTE_DATAFIM.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E20BR2 */
                           E20BR2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_LOTE_QTDEDMN.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E21BR2 */
                           E21BR2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_LOTE_VALOR.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E22BR2 */
                           E22BR2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E23BR2 */
                           E23BR2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E24BR2 */
                           E24BR2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E25BR2 */
                           E25BR2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E26BR2 */
                           E26BR2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E27BR2 */
                           E27BR2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E28BR2 */
                           E28BR2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E29BR2 */
                           E29BR2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E30BR2 */
                           E30BR2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E31BR2 */
                           E31BR2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E32BR2 */
                           E32BR2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_94_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_94_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_94_idx), 4, 0)), 4, "0");
                           SubsflControlProps_942( ) ;
                           AV31Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV31Select)) ? AV93Select_GXI : context.convertURL( context.PathToRelativeUrl( AV31Select))));
                           A562Lote_Numero = cgiGet( edtLote_Numero_Internalname);
                           A563Lote_Nome = StringUtil.Upper( cgiGet( edtLote_Nome_Internalname));
                           A564Lote_Data = context.localUtil.CToT( cgiGet( edtLote_Data_Internalname), 0);
                           A559Lote_UserCod = (int)(context.localUtil.CToN( cgiGet( edtLote_UserCod_Internalname), ",", "."));
                           A560Lote_PessoaCod = (int)(context.localUtil.CToN( cgiGet( edtLote_PessoaCod_Internalname), ",", "."));
                           n560Lote_PessoaCod = false;
                           A561Lote_UserNom = StringUtil.Upper( cgiGet( edtLote_UserNom_Internalname));
                           n561Lote_UserNom = false;
                           A565Lote_ValorPF = context.localUtil.CToN( cgiGet( edtLote_ValorPF_Internalname), ",", ".");
                           A567Lote_DataIni = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtLote_DataIni_Internalname), 0));
                           A568Lote_DataFim = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtLote_DataFim_Internalname), 0));
                           A569Lote_QtdeDmn = (short)(context.localUtil.CToN( cgiGet( edtLote_QtdeDmn_Internalname), ",", "."));
                           A572Lote_Valor = context.localUtil.CToN( cgiGet( edtLote_Valor_Internalname), ",", ".");
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E33BR2 */
                                 E33BR2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E34BR2 */
                                 E34BR2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E35BR2 */
                                 E35BR2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Lote_nome1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vLOTE_NOME1"), AV17Lote_Nome1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Lote_usernom1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vLOTE_USERNOM1"), AV18Lote_UserNom1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV21DynamicFiltersOperator2 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Lote_nome2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vLOTE_NOME2"), AV22Lote_Nome2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Lote_usernom2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vLOTE_USERNOM2"), AV23Lote_UserNom2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV25DynamicFiltersSelector3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV26DynamicFiltersOperator3 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Lote_nome3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vLOTE_NOME3"), AV27Lote_Nome3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Lote_usernom3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vLOTE_USERNOM3"), AV28Lote_UserNom3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV24DynamicFiltersEnabled3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tflote_numero Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFLOTE_NUMERO"), AV38TFLote_Numero) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tflote_numero_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFLOTE_NUMERO_SEL"), AV39TFLote_Numero_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tflote_nome Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFLOTE_NOME"), AV42TFLote_Nome) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tflote_nome_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFLOTE_NOME_SEL"), AV43TFLote_Nome_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tflote_data Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFLOTE_DATA"), 0) != AV46TFLote_Data )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tflote_data_to Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFLOTE_DATA_TO"), 0) != AV47TFLote_Data_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tflote_usercod Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFLOTE_USERCOD"), ",", ".") != Convert.ToDecimal( AV52TFLote_UserCod )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tflote_usercod_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFLOTE_USERCOD_TO"), ",", ".") != Convert.ToDecimal( AV53TFLote_UserCod_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tflote_pessoacod Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFLOTE_PESSOACOD"), ",", ".") != Convert.ToDecimal( AV56TFLote_PessoaCod )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tflote_pessoacod_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFLOTE_PESSOACOD_TO"), ",", ".") != Convert.ToDecimal( AV57TFLote_PessoaCod_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tflote_usernom Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFLOTE_USERNOM"), AV60TFLote_UserNom) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tflote_usernom_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFLOTE_USERNOM_SEL"), AV61TFLote_UserNom_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tflote_valorpf Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFLOTE_VALORPF"), ",", ".") != AV64TFLote_ValorPF )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tflote_valorpf_to Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFLOTE_VALORPF_TO"), ",", ".") != AV65TFLote_ValorPF_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tflote_dataini Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFLOTE_DATAINI"), 0) != AV68TFLote_DataIni )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tflote_dataini_to Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFLOTE_DATAINI_TO"), 0) != AV69TFLote_DataIni_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tflote_datafim Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFLOTE_DATAFIM"), 0) != AV74TFLote_DataFim )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tflote_datafim_to Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFLOTE_DATAFIM_TO"), 0) != AV75TFLote_DataFim_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tflote_qtdedmn Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFLOTE_QTDEDMN"), ",", ".") != Convert.ToDecimal( AV80TFLote_QtdeDmn )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tflote_qtdedmn_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFLOTE_QTDEDMN_TO"), ",", ".") != Convert.ToDecimal( AV81TFLote_QtdeDmn_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tflote_valor Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFLOTE_VALOR"), ",", ".") != AV84TFLote_Valor )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tflote_valor_to Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFLOTE_VALOR_TO"), ",", ".") != AV85TFLote_Valor_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E36BR2 */
                                       E36BR2 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEBR2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormBR2( ) ;
            }
         }
      }

      protected void PABR2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("LOTE_NOME", "Nome", 0);
            cmbavDynamicfiltersselector1.addItem("LOTE_USERNOM", "Usu�rio", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("LOTE_NOME", "Nome", 0);
            cmbavDynamicfiltersselector2.addItem("LOTE_USERNOM", "Usu�rio", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("LOTE_NOME", "Nome", 0);
            cmbavDynamicfiltersselector3.addItem("LOTE_USERNOM", "Usu�rio", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV25DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV25DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_942( ) ;
         while ( nGXsfl_94_idx <= nRC_GXsfl_94 )
         {
            sendrow_942( ) ;
            nGXsfl_94_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_94_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_94_idx+1));
            sGXsfl_94_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_94_idx), 4, 0)), 4, "0");
            SubsflControlProps_942( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       String AV17Lote_Nome1 ,
                                       String AV18Lote_UserNom1 ,
                                       String AV20DynamicFiltersSelector2 ,
                                       short AV21DynamicFiltersOperator2 ,
                                       String AV22Lote_Nome2 ,
                                       String AV23Lote_UserNom2 ,
                                       String AV25DynamicFiltersSelector3 ,
                                       short AV26DynamicFiltersOperator3 ,
                                       String AV27Lote_Nome3 ,
                                       String AV28Lote_UserNom3 ,
                                       bool AV19DynamicFiltersEnabled2 ,
                                       bool AV24DynamicFiltersEnabled3 ,
                                       String AV38TFLote_Numero ,
                                       String AV39TFLote_Numero_Sel ,
                                       String AV42TFLote_Nome ,
                                       String AV43TFLote_Nome_Sel ,
                                       DateTime AV46TFLote_Data ,
                                       DateTime AV47TFLote_Data_To ,
                                       int AV52TFLote_UserCod ,
                                       int AV53TFLote_UserCod_To ,
                                       int AV56TFLote_PessoaCod ,
                                       int AV57TFLote_PessoaCod_To ,
                                       String AV60TFLote_UserNom ,
                                       String AV61TFLote_UserNom_Sel ,
                                       decimal AV64TFLote_ValorPF ,
                                       decimal AV65TFLote_ValorPF_To ,
                                       DateTime AV68TFLote_DataIni ,
                                       DateTime AV69TFLote_DataIni_To ,
                                       DateTime AV74TFLote_DataFim ,
                                       DateTime AV75TFLote_DataFim_To ,
                                       short AV80TFLote_QtdeDmn ,
                                       short AV81TFLote_QtdeDmn_To ,
                                       decimal AV84TFLote_Valor ,
                                       decimal AV85TFLote_Valor_To ,
                                       String AV40ddo_Lote_NumeroTitleControlIdToReplace ,
                                       String AV44ddo_Lote_NomeTitleControlIdToReplace ,
                                       String AV50ddo_Lote_DataTitleControlIdToReplace ,
                                       String AV54ddo_Lote_UserCodTitleControlIdToReplace ,
                                       String AV58ddo_Lote_PessoaCodTitleControlIdToReplace ,
                                       String AV62ddo_Lote_UserNomTitleControlIdToReplace ,
                                       String AV66ddo_Lote_ValorPFTitleControlIdToReplace ,
                                       String AV72ddo_Lote_DataIniTitleControlIdToReplace ,
                                       String AV78ddo_Lote_DataFimTitleControlIdToReplace ,
                                       String AV82ddo_Lote_QtdeDmnTitleControlIdToReplace ,
                                       String AV86ddo_Lote_ValorTitleControlIdToReplace ,
                                       int AV34Lote_AreaTrabalhoCod ,
                                       int AV36Lote_ContratadaCod ,
                                       String AV94Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV30DynamicFiltersIgnoreFirst ,
                                       bool AV29DynamicFiltersRemoving )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFBR2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_LOTE_NUMERO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A562Lote_Numero, ""))));
         GxWebStd.gx_hidden_field( context, "LOTE_NUMERO", StringUtil.RTrim( A562Lote_Numero));
         GxWebStd.gx_hidden_field( context, "gxhash_LOTE_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A563Lote_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "LOTE_NOME", StringUtil.RTrim( A563Lote_Nome));
         GxWebStd.gx_hidden_field( context, "gxhash_LOTE_DATA", GetSecureSignedToken( "", context.localUtil.Format( A564Lote_Data, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, "LOTE_DATA", context.localUtil.TToC( A564Lote_Data, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "gxhash_LOTE_USERCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A559Lote_UserCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "LOTE_USERCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A559Lote_UserCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_LOTE_VALORPF", GetSecureSignedToken( "", context.localUtil.Format( A565Lote_ValorPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "LOTE_VALORPF", StringUtil.LTrim( StringUtil.NToC( A565Lote_ValorPF, 18, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_LOTE_VALOR", GetSecureSignedToken( "", context.localUtil.Format( A572Lote_Valor, "ZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "LOTE_VALOR", StringUtil.LTrim( StringUtil.NToC( A572Lote_Valor, 18, 5, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV25DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV25DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFBR2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV94Pgmname = "PromptLote";
         context.Gx_err = 0;
      }

      protected void RFBR2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 94;
         /* Execute user event: E34BR2 */
         E34BR2 ();
         nGXsfl_94_idx = 1;
         sGXsfl_94_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_94_idx), 4, 0)), 4, "0");
         SubsflControlProps_942( ) ;
         nGXsfl_94_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_942( ) ;
            pr_default.dynParam(1, new Object[]{ new Object[]{
                                                 AV15DynamicFiltersSelector1 ,
                                                 AV16DynamicFiltersOperator1 ,
                                                 AV17Lote_Nome1 ,
                                                 AV18Lote_UserNom1 ,
                                                 AV19DynamicFiltersEnabled2 ,
                                                 AV20DynamicFiltersSelector2 ,
                                                 AV21DynamicFiltersOperator2 ,
                                                 AV22Lote_Nome2 ,
                                                 AV23Lote_UserNom2 ,
                                                 AV24DynamicFiltersEnabled3 ,
                                                 AV25DynamicFiltersSelector3 ,
                                                 AV26DynamicFiltersOperator3 ,
                                                 AV27Lote_Nome3 ,
                                                 AV28Lote_UserNom3 ,
                                                 AV39TFLote_Numero_Sel ,
                                                 AV38TFLote_Numero ,
                                                 AV43TFLote_Nome_Sel ,
                                                 AV42TFLote_Nome ,
                                                 AV46TFLote_Data ,
                                                 AV47TFLote_Data_To ,
                                                 AV52TFLote_UserCod ,
                                                 AV53TFLote_UserCod_To ,
                                                 AV56TFLote_PessoaCod ,
                                                 AV57TFLote_PessoaCod_To ,
                                                 AV61TFLote_UserNom_Sel ,
                                                 AV60TFLote_UserNom ,
                                                 AV64TFLote_ValorPF ,
                                                 AV65TFLote_ValorPF_To ,
                                                 AV80TFLote_QtdeDmn ,
                                                 AV81TFLote_QtdeDmn_To ,
                                                 A563Lote_Nome ,
                                                 A561Lote_UserNom ,
                                                 A562Lote_Numero ,
                                                 A564Lote_Data ,
                                                 A559Lote_UserCod ,
                                                 A560Lote_PessoaCod ,
                                                 A565Lote_ValorPF ,
                                                 A569Lote_QtdeDmn ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 A595Lote_AreaTrabalhoCod ,
                                                 AV6WWPContext.gxTpr_Areatrabalho_codigo ,
                                                 AV6WWPContext.gxTpr_Userehcontratante ,
                                                 A1231Lote_ContratadaCod ,
                                                 AV6WWPContext.gxTpr_Contratada_codigo ,
                                                 AV68TFLote_DataIni ,
                                                 A567Lote_DataIni ,
                                                 AV69TFLote_DataIni_To ,
                                                 AV74TFLote_DataFim ,
                                                 A568Lote_DataFim ,
                                                 AV75TFLote_DataFim_To ,
                                                 AV84TFLote_Valor ,
                                                 A572Lote_Valor ,
                                                 AV85TFLote_Valor_To },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE,
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.SHORT,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.SHORT,
                                                 TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                                 TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL
                                                 }
            });
            lV17Lote_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV17Lote_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Lote_Nome1", AV17Lote_Nome1);
            lV17Lote_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV17Lote_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Lote_Nome1", AV17Lote_Nome1);
            lV18Lote_UserNom1 = StringUtil.PadR( StringUtil.RTrim( AV18Lote_UserNom1), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Lote_UserNom1", AV18Lote_UserNom1);
            lV18Lote_UserNom1 = StringUtil.PadR( StringUtil.RTrim( AV18Lote_UserNom1), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Lote_UserNom1", AV18Lote_UserNom1);
            lV22Lote_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV22Lote_Nome2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Lote_Nome2", AV22Lote_Nome2);
            lV22Lote_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV22Lote_Nome2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Lote_Nome2", AV22Lote_Nome2);
            lV23Lote_UserNom2 = StringUtil.PadR( StringUtil.RTrim( AV23Lote_UserNom2), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Lote_UserNom2", AV23Lote_UserNom2);
            lV23Lote_UserNom2 = StringUtil.PadR( StringUtil.RTrim( AV23Lote_UserNom2), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Lote_UserNom2", AV23Lote_UserNom2);
            lV27Lote_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV27Lote_Nome3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Lote_Nome3", AV27Lote_Nome3);
            lV27Lote_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV27Lote_Nome3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Lote_Nome3", AV27Lote_Nome3);
            lV28Lote_UserNom3 = StringUtil.PadR( StringUtil.RTrim( AV28Lote_UserNom3), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Lote_UserNom3", AV28Lote_UserNom3);
            lV28Lote_UserNom3 = StringUtil.PadR( StringUtil.RTrim( AV28Lote_UserNom3), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Lote_UserNom3", AV28Lote_UserNom3);
            lV38TFLote_Numero = StringUtil.PadR( StringUtil.RTrim( AV38TFLote_Numero), 10, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFLote_Numero", AV38TFLote_Numero);
            lV42TFLote_Nome = StringUtil.PadR( StringUtil.RTrim( AV42TFLote_Nome), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFLote_Nome", AV42TFLote_Nome);
            lV60TFLote_UserNom = StringUtil.PadR( StringUtil.RTrim( AV60TFLote_UserNom), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFLote_UserNom", AV60TFLote_UserNom);
            /* Using cursor H00BR9 */
            pr_default.execute(1, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, AV6WWPContext.gxTpr_Userehcontratante, AV6WWPContext.gxTpr_Contratada_codigo, AV68TFLote_DataIni, AV68TFLote_DataIni, AV69TFLote_DataIni_To, AV69TFLote_DataIni_To, AV74TFLote_DataFim, AV74TFLote_DataFim, AV75TFLote_DataFim_To, AV75TFLote_DataFim_To, lV17Lote_Nome1, lV17Lote_Nome1, lV18Lote_UserNom1, lV18Lote_UserNom1, lV22Lote_Nome2, lV22Lote_Nome2, lV23Lote_UserNom2, lV23Lote_UserNom2, lV27Lote_Nome3, lV27Lote_Nome3, lV28Lote_UserNom3, lV28Lote_UserNom3, lV38TFLote_Numero, AV39TFLote_Numero_Sel, lV42TFLote_Nome, AV43TFLote_Nome_Sel, AV46TFLote_Data, AV47TFLote_Data_To, AV52TFLote_UserCod, AV53TFLote_UserCod_To, AV56TFLote_PessoaCod, AV57TFLote_PessoaCod_To, lV60TFLote_UserNom, AV61TFLote_UserNom_Sel, AV64TFLote_ValorPF, AV65TFLote_ValorPF_To, AV80TFLote_QtdeDmn, AV81TFLote_QtdeDmn_To});
            nGXsfl_94_idx = 1;
            GRID_nEOF = 0;
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            while ( ( (pr_default.getStatus(1) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) ) )
            {
               A595Lote_AreaTrabalhoCod = H00BR9_A595Lote_AreaTrabalhoCod[0];
               A565Lote_ValorPF = H00BR9_A565Lote_ValorPF[0];
               A561Lote_UserNom = H00BR9_A561Lote_UserNom[0];
               n561Lote_UserNom = H00BR9_n561Lote_UserNom[0];
               A560Lote_PessoaCod = H00BR9_A560Lote_PessoaCod[0];
               n560Lote_PessoaCod = H00BR9_n560Lote_PessoaCod[0];
               A559Lote_UserCod = H00BR9_A559Lote_UserCod[0];
               A564Lote_Data = H00BR9_A564Lote_Data[0];
               A563Lote_Nome = H00BR9_A563Lote_Nome[0];
               A562Lote_Numero = H00BR9_A562Lote_Numero[0];
               A596Lote_Codigo = H00BR9_A596Lote_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A596Lote_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A596Lote_Codigo), 6, 0)));
               A1231Lote_ContratadaCod = H00BR9_A1231Lote_ContratadaCod[0];
               A569Lote_QtdeDmn = H00BR9_A569Lote_QtdeDmn[0];
               A568Lote_DataFim = H00BR9_A568Lote_DataFim[0];
               A567Lote_DataIni = H00BR9_A567Lote_DataIni[0];
               A1057Lote_ValorGlosas = H00BR9_A1057Lote_ValorGlosas[0];
               n1057Lote_ValorGlosas = H00BR9_n1057Lote_ValorGlosas[0];
               A560Lote_PessoaCod = H00BR9_A560Lote_PessoaCod[0];
               n560Lote_PessoaCod = H00BR9_n560Lote_PessoaCod[0];
               A561Lote_UserNom = H00BR9_A561Lote_UserNom[0];
               n561Lote_UserNom = H00BR9_n561Lote_UserNom[0];
               A1231Lote_ContratadaCod = H00BR9_A1231Lote_ContratadaCod[0];
               A569Lote_QtdeDmn = H00BR9_A569Lote_QtdeDmn[0];
               A1057Lote_ValorGlosas = H00BR9_A1057Lote_ValorGlosas[0];
               n1057Lote_ValorGlosas = H00BR9_n1057Lote_ValorGlosas[0];
               A568Lote_DataFim = H00BR9_A568Lote_DataFim[0];
               A567Lote_DataIni = H00BR9_A567Lote_DataIni[0];
               GetLote_ValorOSs( A596Lote_Codigo) ;
               A572Lote_Valor = (decimal)(A1058Lote_ValorOSs-A1057Lote_ValorGlosas);
               if ( (Convert.ToDecimal(0)==AV84TFLote_Valor) || ( ( A572Lote_Valor >= AV84TFLote_Valor ) ) )
               {
                  if ( (Convert.ToDecimal(0)==AV85TFLote_Valor_To) || ( ( A572Lote_Valor <= AV85TFLote_Valor_To ) ) )
                  {
                     /* Execute user event: E35BR2 */
                     E35BR2 ();
                  }
               }
               pr_default.readNext(1);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(1) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(1);
            wbEnd = 94;
            WBBR0( ) ;
         }
         nGXsfl_94_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Lote_Nome1, AV18Lote_UserNom1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Lote_Nome2, AV23Lote_UserNom2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27Lote_Nome3, AV28Lote_UserNom3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV38TFLote_Numero, AV39TFLote_Numero_Sel, AV42TFLote_Nome, AV43TFLote_Nome_Sel, AV46TFLote_Data, AV47TFLote_Data_To, AV52TFLote_UserCod, AV53TFLote_UserCod_To, AV56TFLote_PessoaCod, AV57TFLote_PessoaCod_To, AV60TFLote_UserNom, AV61TFLote_UserNom_Sel, AV64TFLote_ValorPF, AV65TFLote_ValorPF_To, AV68TFLote_DataIni, AV69TFLote_DataIni_To, AV74TFLote_DataFim, AV75TFLote_DataFim_To, AV80TFLote_QtdeDmn, AV81TFLote_QtdeDmn_To, AV84TFLote_Valor, AV85TFLote_Valor_To, AV40ddo_Lote_NumeroTitleControlIdToReplace, AV44ddo_Lote_NomeTitleControlIdToReplace, AV50ddo_Lote_DataTitleControlIdToReplace, AV54ddo_Lote_UserCodTitleControlIdToReplace, AV58ddo_Lote_PessoaCodTitleControlIdToReplace, AV62ddo_Lote_UserNomTitleControlIdToReplace, AV66ddo_Lote_ValorPFTitleControlIdToReplace, AV72ddo_Lote_DataIniTitleControlIdToReplace, AV78ddo_Lote_DataFimTitleControlIdToReplace, AV82ddo_Lote_QtdeDmnTitleControlIdToReplace, AV86ddo_Lote_ValorTitleControlIdToReplace, AV34Lote_AreaTrabalhoCod, AV36Lote_ContratadaCod, AV94Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         if ( GRID_nEOF == 0 )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Lote_Nome1, AV18Lote_UserNom1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Lote_Nome2, AV23Lote_UserNom2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27Lote_Nome3, AV28Lote_UserNom3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV38TFLote_Numero, AV39TFLote_Numero_Sel, AV42TFLote_Nome, AV43TFLote_Nome_Sel, AV46TFLote_Data, AV47TFLote_Data_To, AV52TFLote_UserCod, AV53TFLote_UserCod_To, AV56TFLote_PessoaCod, AV57TFLote_PessoaCod_To, AV60TFLote_UserNom, AV61TFLote_UserNom_Sel, AV64TFLote_ValorPF, AV65TFLote_ValorPF_To, AV68TFLote_DataIni, AV69TFLote_DataIni_To, AV74TFLote_DataFim, AV75TFLote_DataFim_To, AV80TFLote_QtdeDmn, AV81TFLote_QtdeDmn_To, AV84TFLote_Valor, AV85TFLote_Valor_To, AV40ddo_Lote_NumeroTitleControlIdToReplace, AV44ddo_Lote_NomeTitleControlIdToReplace, AV50ddo_Lote_DataTitleControlIdToReplace, AV54ddo_Lote_UserCodTitleControlIdToReplace, AV58ddo_Lote_PessoaCodTitleControlIdToReplace, AV62ddo_Lote_UserNomTitleControlIdToReplace, AV66ddo_Lote_ValorPFTitleControlIdToReplace, AV72ddo_Lote_DataIniTitleControlIdToReplace, AV78ddo_Lote_DataFimTitleControlIdToReplace, AV82ddo_Lote_QtdeDmnTitleControlIdToReplace, AV86ddo_Lote_ValorTitleControlIdToReplace, AV34Lote_AreaTrabalhoCod, AV36Lote_ContratadaCod, AV94Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Lote_Nome1, AV18Lote_UserNom1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Lote_Nome2, AV23Lote_UserNom2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27Lote_Nome3, AV28Lote_UserNom3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV38TFLote_Numero, AV39TFLote_Numero_Sel, AV42TFLote_Nome, AV43TFLote_Nome_Sel, AV46TFLote_Data, AV47TFLote_Data_To, AV52TFLote_UserCod, AV53TFLote_UserCod_To, AV56TFLote_PessoaCod, AV57TFLote_PessoaCod_To, AV60TFLote_UserNom, AV61TFLote_UserNom_Sel, AV64TFLote_ValorPF, AV65TFLote_ValorPF_To, AV68TFLote_DataIni, AV69TFLote_DataIni_To, AV74TFLote_DataFim, AV75TFLote_DataFim_To, AV80TFLote_QtdeDmn, AV81TFLote_QtdeDmn_To, AV84TFLote_Valor, AV85TFLote_Valor_To, AV40ddo_Lote_NumeroTitleControlIdToReplace, AV44ddo_Lote_NomeTitleControlIdToReplace, AV50ddo_Lote_DataTitleControlIdToReplace, AV54ddo_Lote_UserCodTitleControlIdToReplace, AV58ddo_Lote_PessoaCodTitleControlIdToReplace, AV62ddo_Lote_UserNomTitleControlIdToReplace, AV66ddo_Lote_ValorPFTitleControlIdToReplace, AV72ddo_Lote_DataIniTitleControlIdToReplace, AV78ddo_Lote_DataFimTitleControlIdToReplace, AV82ddo_Lote_QtdeDmnTitleControlIdToReplace, AV86ddo_Lote_ValorTitleControlIdToReplace, AV34Lote_AreaTrabalhoCod, AV36Lote_ContratadaCod, AV94Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         subGrid_Islastpage = 1;
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Lote_Nome1, AV18Lote_UserNom1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Lote_Nome2, AV23Lote_UserNom2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27Lote_Nome3, AV28Lote_UserNom3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV38TFLote_Numero, AV39TFLote_Numero_Sel, AV42TFLote_Nome, AV43TFLote_Nome_Sel, AV46TFLote_Data, AV47TFLote_Data_To, AV52TFLote_UserCod, AV53TFLote_UserCod_To, AV56TFLote_PessoaCod, AV57TFLote_PessoaCod_To, AV60TFLote_UserNom, AV61TFLote_UserNom_Sel, AV64TFLote_ValorPF, AV65TFLote_ValorPF_To, AV68TFLote_DataIni, AV69TFLote_DataIni_To, AV74TFLote_DataFim, AV75TFLote_DataFim_To, AV80TFLote_QtdeDmn, AV81TFLote_QtdeDmn_To, AV84TFLote_Valor, AV85TFLote_Valor_To, AV40ddo_Lote_NumeroTitleControlIdToReplace, AV44ddo_Lote_NomeTitleControlIdToReplace, AV50ddo_Lote_DataTitleControlIdToReplace, AV54ddo_Lote_UserCodTitleControlIdToReplace, AV58ddo_Lote_PessoaCodTitleControlIdToReplace, AV62ddo_Lote_UserNomTitleControlIdToReplace, AV66ddo_Lote_ValorPFTitleControlIdToReplace, AV72ddo_Lote_DataIniTitleControlIdToReplace, AV78ddo_Lote_DataFimTitleControlIdToReplace, AV82ddo_Lote_QtdeDmnTitleControlIdToReplace, AV86ddo_Lote_ValorTitleControlIdToReplace, AV34Lote_AreaTrabalhoCod, AV36Lote_ContratadaCod, AV94Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Lote_Nome1, AV18Lote_UserNom1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Lote_Nome2, AV23Lote_UserNom2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27Lote_Nome3, AV28Lote_UserNom3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV38TFLote_Numero, AV39TFLote_Numero_Sel, AV42TFLote_Nome, AV43TFLote_Nome_Sel, AV46TFLote_Data, AV47TFLote_Data_To, AV52TFLote_UserCod, AV53TFLote_UserCod_To, AV56TFLote_PessoaCod, AV57TFLote_PessoaCod_To, AV60TFLote_UserNom, AV61TFLote_UserNom_Sel, AV64TFLote_ValorPF, AV65TFLote_ValorPF_To, AV68TFLote_DataIni, AV69TFLote_DataIni_To, AV74TFLote_DataFim, AV75TFLote_DataFim_To, AV80TFLote_QtdeDmn, AV81TFLote_QtdeDmn_To, AV84TFLote_Valor, AV85TFLote_Valor_To, AV40ddo_Lote_NumeroTitleControlIdToReplace, AV44ddo_Lote_NomeTitleControlIdToReplace, AV50ddo_Lote_DataTitleControlIdToReplace, AV54ddo_Lote_UserCodTitleControlIdToReplace, AV58ddo_Lote_PessoaCodTitleControlIdToReplace, AV62ddo_Lote_UserNomTitleControlIdToReplace, AV66ddo_Lote_ValorPFTitleControlIdToReplace, AV72ddo_Lote_DataIniTitleControlIdToReplace, AV78ddo_Lote_DataFimTitleControlIdToReplace, AV82ddo_Lote_QtdeDmnTitleControlIdToReplace, AV86ddo_Lote_ValorTitleControlIdToReplace, AV34Lote_AreaTrabalhoCod, AV36Lote_ContratadaCod, AV94Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         }
         return (int)(0) ;
      }

      protected void STRUPBR0( )
      {
         /* Before Start, stand alone formulas. */
         AV94Pgmname = "PromptLote";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E33BR2 */
         E33BR2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV87DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vLOTE_NUMEROTITLEFILTERDATA"), AV37Lote_NumeroTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vLOTE_NOMETITLEFILTERDATA"), AV41Lote_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vLOTE_DATATITLEFILTERDATA"), AV45Lote_DataTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vLOTE_USERCODTITLEFILTERDATA"), AV51Lote_UserCodTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vLOTE_PESSOACODTITLEFILTERDATA"), AV55Lote_PessoaCodTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vLOTE_USERNOMTITLEFILTERDATA"), AV59Lote_UserNomTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vLOTE_VALORPFTITLEFILTERDATA"), AV63Lote_ValorPFTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vLOTE_DATAINITITLEFILTERDATA"), AV67Lote_DataIniTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vLOTE_DATAFIMTITLEFILTERDATA"), AV73Lote_DataFimTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vLOTE_QTDEDMNTITLEFILTERDATA"), AV79Lote_QtdeDmnTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vLOTE_VALORTITLEFILTERDATA"), AV83Lote_ValorTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavLote_areatrabalhocod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavLote_areatrabalhocod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vLOTE_AREATRABALHOCOD");
               GX_FocusControl = edtavLote_areatrabalhocod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV34Lote_AreaTrabalhoCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Lote_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Lote_AreaTrabalhoCod), 6, 0)));
            }
            else
            {
               AV34Lote_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtavLote_areatrabalhocod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Lote_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Lote_AreaTrabalhoCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavLote_contratadacod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavLote_contratadacod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vLOTE_CONTRATADACOD");
               GX_FocusControl = edtavLote_contratadacod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV36Lote_ContratadaCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Lote_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Lote_ContratadaCod), 6, 0)));
            }
            else
            {
               AV36Lote_ContratadaCod = (int)(context.localUtil.CToN( cgiGet( edtavLote_contratadacod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Lote_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Lote_ContratadaCod), 6, 0)));
            }
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            AV17Lote_Nome1 = StringUtil.Upper( cgiGet( edtavLote_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Lote_Nome1", AV17Lote_Nome1);
            AV18Lote_UserNom1 = StringUtil.Upper( cgiGet( edtavLote_usernom1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Lote_UserNom1", AV18Lote_UserNom1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV20DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
            AV22Lote_Nome2 = StringUtil.Upper( cgiGet( edtavLote_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Lote_Nome2", AV22Lote_Nome2);
            AV23Lote_UserNom2 = StringUtil.Upper( cgiGet( edtavLote_usernom2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Lote_UserNom2", AV23Lote_UserNom2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV25DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
            AV27Lote_Nome3 = StringUtil.Upper( cgiGet( edtavLote_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Lote_Nome3", AV27Lote_Nome3);
            AV28Lote_UserNom3 = StringUtil.Upper( cgiGet( edtavLote_usernom3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Lote_UserNom3", AV28Lote_UserNom3);
            A596Lote_Codigo = (int)(context.localUtil.CToN( cgiGet( edtLote_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A596Lote_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A596Lote_Codigo), 6, 0)));
            AV19DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
            AV24DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
            AV38TFLote_Numero = cgiGet( edtavTflote_numero_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFLote_Numero", AV38TFLote_Numero);
            AV39TFLote_Numero_Sel = cgiGet( edtavTflote_numero_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFLote_Numero_Sel", AV39TFLote_Numero_Sel);
            AV42TFLote_Nome = StringUtil.Upper( cgiGet( edtavTflote_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFLote_Nome", AV42TFLote_Nome);
            AV43TFLote_Nome_Sel = StringUtil.Upper( cgiGet( edtavTflote_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFLote_Nome_Sel", AV43TFLote_Nome_Sel);
            if ( context.localUtil.VCDateTime( cgiGet( edtavTflote_data_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFLote_Data"}), 1, "vTFLOTE_DATA");
               GX_FocusControl = edtavTflote_data_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV46TFLote_Data = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFLote_Data", context.localUtil.TToC( AV46TFLote_Data, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV46TFLote_Data = context.localUtil.CToT( cgiGet( edtavTflote_data_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFLote_Data", context.localUtil.TToC( AV46TFLote_Data, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTflote_data_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFLote_Data_To"}), 1, "vTFLOTE_DATA_TO");
               GX_FocusControl = edtavTflote_data_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV47TFLote_Data_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFLote_Data_To", context.localUtil.TToC( AV47TFLote_Data_To, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV47TFLote_Data_To = context.localUtil.CToT( cgiGet( edtavTflote_data_to_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFLote_Data_To", context.localUtil.TToC( AV47TFLote_Data_To, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_lote_dataauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Lote_Data Aux Date"}), 1, "vDDO_LOTE_DATAAUXDATE");
               GX_FocusControl = edtavDdo_lote_dataauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV48DDO_Lote_DataAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48DDO_Lote_DataAuxDate", context.localUtil.Format(AV48DDO_Lote_DataAuxDate, "99/99/99"));
            }
            else
            {
               AV48DDO_Lote_DataAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_lote_dataauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48DDO_Lote_DataAuxDate", context.localUtil.Format(AV48DDO_Lote_DataAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_lote_dataauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Lote_Data Aux Date To"}), 1, "vDDO_LOTE_DATAAUXDATETO");
               GX_FocusControl = edtavDdo_lote_dataauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV49DDO_Lote_DataAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49DDO_Lote_DataAuxDateTo", context.localUtil.Format(AV49DDO_Lote_DataAuxDateTo, "99/99/99"));
            }
            else
            {
               AV49DDO_Lote_DataAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_lote_dataauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49DDO_Lote_DataAuxDateTo", context.localUtil.Format(AV49DDO_Lote_DataAuxDateTo, "99/99/99"));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTflote_usercod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTflote_usercod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFLOTE_USERCOD");
               GX_FocusControl = edtavTflote_usercod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV52TFLote_UserCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFLote_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52TFLote_UserCod), 6, 0)));
            }
            else
            {
               AV52TFLote_UserCod = (int)(context.localUtil.CToN( cgiGet( edtavTflote_usercod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFLote_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52TFLote_UserCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTflote_usercod_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTflote_usercod_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFLOTE_USERCOD_TO");
               GX_FocusControl = edtavTflote_usercod_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV53TFLote_UserCod_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFLote_UserCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53TFLote_UserCod_To), 6, 0)));
            }
            else
            {
               AV53TFLote_UserCod_To = (int)(context.localUtil.CToN( cgiGet( edtavTflote_usercod_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFLote_UserCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53TFLote_UserCod_To), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTflote_pessoacod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTflote_pessoacod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFLOTE_PESSOACOD");
               GX_FocusControl = edtavTflote_pessoacod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV56TFLote_PessoaCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFLote_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56TFLote_PessoaCod), 6, 0)));
            }
            else
            {
               AV56TFLote_PessoaCod = (int)(context.localUtil.CToN( cgiGet( edtavTflote_pessoacod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFLote_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56TFLote_PessoaCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTflote_pessoacod_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTflote_pessoacod_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFLOTE_PESSOACOD_TO");
               GX_FocusControl = edtavTflote_pessoacod_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV57TFLote_PessoaCod_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFLote_PessoaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV57TFLote_PessoaCod_To), 6, 0)));
            }
            else
            {
               AV57TFLote_PessoaCod_To = (int)(context.localUtil.CToN( cgiGet( edtavTflote_pessoacod_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFLote_PessoaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV57TFLote_PessoaCod_To), 6, 0)));
            }
            AV60TFLote_UserNom = StringUtil.Upper( cgiGet( edtavTflote_usernom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFLote_UserNom", AV60TFLote_UserNom);
            AV61TFLote_UserNom_Sel = StringUtil.Upper( cgiGet( edtavTflote_usernom_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFLote_UserNom_Sel", AV61TFLote_UserNom_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTflote_valorpf_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTflote_valorpf_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFLOTE_VALORPF");
               GX_FocusControl = edtavTflote_valorpf_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV64TFLote_ValorPF = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFLote_ValorPF", StringUtil.LTrim( StringUtil.Str( AV64TFLote_ValorPF, 18, 5)));
            }
            else
            {
               AV64TFLote_ValorPF = context.localUtil.CToN( cgiGet( edtavTflote_valorpf_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFLote_ValorPF", StringUtil.LTrim( StringUtil.Str( AV64TFLote_ValorPF, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTflote_valorpf_to_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTflote_valorpf_to_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFLOTE_VALORPF_TO");
               GX_FocusControl = edtavTflote_valorpf_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV65TFLote_ValorPF_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFLote_ValorPF_To", StringUtil.LTrim( StringUtil.Str( AV65TFLote_ValorPF_To, 18, 5)));
            }
            else
            {
               AV65TFLote_ValorPF_To = context.localUtil.CToN( cgiGet( edtavTflote_valorpf_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFLote_ValorPF_To", StringUtil.LTrim( StringUtil.Str( AV65TFLote_ValorPF_To, 18, 5)));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTflote_dataini_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFLote_Data Ini"}), 1, "vTFLOTE_DATAINI");
               GX_FocusControl = edtavTflote_dataini_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV68TFLote_DataIni = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFLote_DataIni", context.localUtil.Format(AV68TFLote_DataIni, "99/99/99"));
            }
            else
            {
               AV68TFLote_DataIni = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTflote_dataini_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFLote_DataIni", context.localUtil.Format(AV68TFLote_DataIni, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTflote_dataini_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFLote_Data Ini_To"}), 1, "vTFLOTE_DATAINI_TO");
               GX_FocusControl = edtavTflote_dataini_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV69TFLote_DataIni_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFLote_DataIni_To", context.localUtil.Format(AV69TFLote_DataIni_To, "99/99/99"));
            }
            else
            {
               AV69TFLote_DataIni_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTflote_dataini_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFLote_DataIni_To", context.localUtil.Format(AV69TFLote_DataIni_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_lote_datainiauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Lote_Data Ini Aux Date"}), 1, "vDDO_LOTE_DATAINIAUXDATE");
               GX_FocusControl = edtavDdo_lote_datainiauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV70DDO_Lote_DataIniAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70DDO_Lote_DataIniAuxDate", context.localUtil.Format(AV70DDO_Lote_DataIniAuxDate, "99/99/99"));
            }
            else
            {
               AV70DDO_Lote_DataIniAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_lote_datainiauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70DDO_Lote_DataIniAuxDate", context.localUtil.Format(AV70DDO_Lote_DataIniAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_lote_datainiauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Lote_Data Ini Aux Date To"}), 1, "vDDO_LOTE_DATAINIAUXDATETO");
               GX_FocusControl = edtavDdo_lote_datainiauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV71DDO_Lote_DataIniAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71DDO_Lote_DataIniAuxDateTo", context.localUtil.Format(AV71DDO_Lote_DataIniAuxDateTo, "99/99/99"));
            }
            else
            {
               AV71DDO_Lote_DataIniAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_lote_datainiauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71DDO_Lote_DataIniAuxDateTo", context.localUtil.Format(AV71DDO_Lote_DataIniAuxDateTo, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTflote_datafim_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFLote_Data Fim"}), 1, "vTFLOTE_DATAFIM");
               GX_FocusControl = edtavTflote_datafim_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV74TFLote_DataFim = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFLote_DataFim", context.localUtil.Format(AV74TFLote_DataFim, "99/99/99"));
            }
            else
            {
               AV74TFLote_DataFim = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTflote_datafim_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFLote_DataFim", context.localUtil.Format(AV74TFLote_DataFim, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTflote_datafim_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFLote_Data Fim_To"}), 1, "vTFLOTE_DATAFIM_TO");
               GX_FocusControl = edtavTflote_datafim_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV75TFLote_DataFim_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFLote_DataFim_To", context.localUtil.Format(AV75TFLote_DataFim_To, "99/99/99"));
            }
            else
            {
               AV75TFLote_DataFim_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTflote_datafim_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFLote_DataFim_To", context.localUtil.Format(AV75TFLote_DataFim_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_lote_datafimauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Lote_Data Fim Aux Date"}), 1, "vDDO_LOTE_DATAFIMAUXDATE");
               GX_FocusControl = edtavDdo_lote_datafimauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV76DDO_Lote_DataFimAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76DDO_Lote_DataFimAuxDate", context.localUtil.Format(AV76DDO_Lote_DataFimAuxDate, "99/99/99"));
            }
            else
            {
               AV76DDO_Lote_DataFimAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_lote_datafimauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76DDO_Lote_DataFimAuxDate", context.localUtil.Format(AV76DDO_Lote_DataFimAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_lote_datafimauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Lote_Data Fim Aux Date To"}), 1, "vDDO_LOTE_DATAFIMAUXDATETO");
               GX_FocusControl = edtavDdo_lote_datafimauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV77DDO_Lote_DataFimAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77DDO_Lote_DataFimAuxDateTo", context.localUtil.Format(AV77DDO_Lote_DataFimAuxDateTo, "99/99/99"));
            }
            else
            {
               AV77DDO_Lote_DataFimAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_lote_datafimauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77DDO_Lote_DataFimAuxDateTo", context.localUtil.Format(AV77DDO_Lote_DataFimAuxDateTo, "99/99/99"));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTflote_qtdedmn_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTflote_qtdedmn_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFLOTE_QTDEDMN");
               GX_FocusControl = edtavTflote_qtdedmn_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV80TFLote_QtdeDmn = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFLote_QtdeDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV80TFLote_QtdeDmn), 4, 0)));
            }
            else
            {
               AV80TFLote_QtdeDmn = (short)(context.localUtil.CToN( cgiGet( edtavTflote_qtdedmn_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFLote_QtdeDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV80TFLote_QtdeDmn), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTflote_qtdedmn_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTflote_qtdedmn_to_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFLOTE_QTDEDMN_TO");
               GX_FocusControl = edtavTflote_qtdedmn_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV81TFLote_QtdeDmn_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFLote_QtdeDmn_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV81TFLote_QtdeDmn_To), 4, 0)));
            }
            else
            {
               AV81TFLote_QtdeDmn_To = (short)(context.localUtil.CToN( cgiGet( edtavTflote_qtdedmn_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFLote_QtdeDmn_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV81TFLote_QtdeDmn_To), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTflote_valor_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTflote_valor_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFLOTE_VALOR");
               GX_FocusControl = edtavTflote_valor_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV84TFLote_Valor = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFLote_Valor", StringUtil.LTrim( StringUtil.Str( AV84TFLote_Valor, 18, 5)));
            }
            else
            {
               AV84TFLote_Valor = context.localUtil.CToN( cgiGet( edtavTflote_valor_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFLote_Valor", StringUtil.LTrim( StringUtil.Str( AV84TFLote_Valor, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTflote_valor_to_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTflote_valor_to_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFLOTE_VALOR_TO");
               GX_FocusControl = edtavTflote_valor_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV85TFLote_Valor_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85TFLote_Valor_To", StringUtil.LTrim( StringUtil.Str( AV85TFLote_Valor_To, 18, 5)));
            }
            else
            {
               AV85TFLote_Valor_To = context.localUtil.CToN( cgiGet( edtavTflote_valor_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85TFLote_Valor_To", StringUtil.LTrim( StringUtil.Str( AV85TFLote_Valor_To, 18, 5)));
            }
            AV40ddo_Lote_NumeroTitleControlIdToReplace = cgiGet( edtavDdo_lote_numerotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ddo_Lote_NumeroTitleControlIdToReplace", AV40ddo_Lote_NumeroTitleControlIdToReplace);
            AV44ddo_Lote_NomeTitleControlIdToReplace = cgiGet( edtavDdo_lote_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ddo_Lote_NomeTitleControlIdToReplace", AV44ddo_Lote_NomeTitleControlIdToReplace);
            AV50ddo_Lote_DataTitleControlIdToReplace = cgiGet( edtavDdo_lote_datatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ddo_Lote_DataTitleControlIdToReplace", AV50ddo_Lote_DataTitleControlIdToReplace);
            AV54ddo_Lote_UserCodTitleControlIdToReplace = cgiGet( edtavDdo_lote_usercodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54ddo_Lote_UserCodTitleControlIdToReplace", AV54ddo_Lote_UserCodTitleControlIdToReplace);
            AV58ddo_Lote_PessoaCodTitleControlIdToReplace = cgiGet( edtavDdo_lote_pessoacodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58ddo_Lote_PessoaCodTitleControlIdToReplace", AV58ddo_Lote_PessoaCodTitleControlIdToReplace);
            AV62ddo_Lote_UserNomTitleControlIdToReplace = cgiGet( edtavDdo_lote_usernomtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62ddo_Lote_UserNomTitleControlIdToReplace", AV62ddo_Lote_UserNomTitleControlIdToReplace);
            AV66ddo_Lote_ValorPFTitleControlIdToReplace = cgiGet( edtavDdo_lote_valorpftitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66ddo_Lote_ValorPFTitleControlIdToReplace", AV66ddo_Lote_ValorPFTitleControlIdToReplace);
            AV72ddo_Lote_DataIniTitleControlIdToReplace = cgiGet( edtavDdo_lote_datainititlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72ddo_Lote_DataIniTitleControlIdToReplace", AV72ddo_Lote_DataIniTitleControlIdToReplace);
            AV78ddo_Lote_DataFimTitleControlIdToReplace = cgiGet( edtavDdo_lote_datafimtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78ddo_Lote_DataFimTitleControlIdToReplace", AV78ddo_Lote_DataFimTitleControlIdToReplace);
            AV82ddo_Lote_QtdeDmnTitleControlIdToReplace = cgiGet( edtavDdo_lote_qtdedmntitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82ddo_Lote_QtdeDmnTitleControlIdToReplace", AV82ddo_Lote_QtdeDmnTitleControlIdToReplace);
            AV86ddo_Lote_ValorTitleControlIdToReplace = cgiGet( edtavDdo_lote_valortitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86ddo_Lote_ValorTitleControlIdToReplace", AV86ddo_Lote_ValorTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_94 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_94"), ",", "."));
            AV89GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV90GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_lote_numero_Caption = cgiGet( "DDO_LOTE_NUMERO_Caption");
            Ddo_lote_numero_Tooltip = cgiGet( "DDO_LOTE_NUMERO_Tooltip");
            Ddo_lote_numero_Cls = cgiGet( "DDO_LOTE_NUMERO_Cls");
            Ddo_lote_numero_Filteredtext_set = cgiGet( "DDO_LOTE_NUMERO_Filteredtext_set");
            Ddo_lote_numero_Selectedvalue_set = cgiGet( "DDO_LOTE_NUMERO_Selectedvalue_set");
            Ddo_lote_numero_Dropdownoptionstype = cgiGet( "DDO_LOTE_NUMERO_Dropdownoptionstype");
            Ddo_lote_numero_Titlecontrolidtoreplace = cgiGet( "DDO_LOTE_NUMERO_Titlecontrolidtoreplace");
            Ddo_lote_numero_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_LOTE_NUMERO_Includesortasc"));
            Ddo_lote_numero_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_LOTE_NUMERO_Includesortdsc"));
            Ddo_lote_numero_Sortedstatus = cgiGet( "DDO_LOTE_NUMERO_Sortedstatus");
            Ddo_lote_numero_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_LOTE_NUMERO_Includefilter"));
            Ddo_lote_numero_Filtertype = cgiGet( "DDO_LOTE_NUMERO_Filtertype");
            Ddo_lote_numero_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_LOTE_NUMERO_Filterisrange"));
            Ddo_lote_numero_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_LOTE_NUMERO_Includedatalist"));
            Ddo_lote_numero_Datalisttype = cgiGet( "DDO_LOTE_NUMERO_Datalisttype");
            Ddo_lote_numero_Datalistproc = cgiGet( "DDO_LOTE_NUMERO_Datalistproc");
            Ddo_lote_numero_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_LOTE_NUMERO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_lote_numero_Sortasc = cgiGet( "DDO_LOTE_NUMERO_Sortasc");
            Ddo_lote_numero_Sortdsc = cgiGet( "DDO_LOTE_NUMERO_Sortdsc");
            Ddo_lote_numero_Loadingdata = cgiGet( "DDO_LOTE_NUMERO_Loadingdata");
            Ddo_lote_numero_Cleanfilter = cgiGet( "DDO_LOTE_NUMERO_Cleanfilter");
            Ddo_lote_numero_Noresultsfound = cgiGet( "DDO_LOTE_NUMERO_Noresultsfound");
            Ddo_lote_numero_Searchbuttontext = cgiGet( "DDO_LOTE_NUMERO_Searchbuttontext");
            Ddo_lote_nome_Caption = cgiGet( "DDO_LOTE_NOME_Caption");
            Ddo_lote_nome_Tooltip = cgiGet( "DDO_LOTE_NOME_Tooltip");
            Ddo_lote_nome_Cls = cgiGet( "DDO_LOTE_NOME_Cls");
            Ddo_lote_nome_Filteredtext_set = cgiGet( "DDO_LOTE_NOME_Filteredtext_set");
            Ddo_lote_nome_Selectedvalue_set = cgiGet( "DDO_LOTE_NOME_Selectedvalue_set");
            Ddo_lote_nome_Dropdownoptionstype = cgiGet( "DDO_LOTE_NOME_Dropdownoptionstype");
            Ddo_lote_nome_Titlecontrolidtoreplace = cgiGet( "DDO_LOTE_NOME_Titlecontrolidtoreplace");
            Ddo_lote_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_LOTE_NOME_Includesortasc"));
            Ddo_lote_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_LOTE_NOME_Includesortdsc"));
            Ddo_lote_nome_Sortedstatus = cgiGet( "DDO_LOTE_NOME_Sortedstatus");
            Ddo_lote_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_LOTE_NOME_Includefilter"));
            Ddo_lote_nome_Filtertype = cgiGet( "DDO_LOTE_NOME_Filtertype");
            Ddo_lote_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_LOTE_NOME_Filterisrange"));
            Ddo_lote_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_LOTE_NOME_Includedatalist"));
            Ddo_lote_nome_Datalisttype = cgiGet( "DDO_LOTE_NOME_Datalisttype");
            Ddo_lote_nome_Datalistproc = cgiGet( "DDO_LOTE_NOME_Datalistproc");
            Ddo_lote_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_LOTE_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_lote_nome_Sortasc = cgiGet( "DDO_LOTE_NOME_Sortasc");
            Ddo_lote_nome_Sortdsc = cgiGet( "DDO_LOTE_NOME_Sortdsc");
            Ddo_lote_nome_Loadingdata = cgiGet( "DDO_LOTE_NOME_Loadingdata");
            Ddo_lote_nome_Cleanfilter = cgiGet( "DDO_LOTE_NOME_Cleanfilter");
            Ddo_lote_nome_Noresultsfound = cgiGet( "DDO_LOTE_NOME_Noresultsfound");
            Ddo_lote_nome_Searchbuttontext = cgiGet( "DDO_LOTE_NOME_Searchbuttontext");
            Ddo_lote_data_Caption = cgiGet( "DDO_LOTE_DATA_Caption");
            Ddo_lote_data_Tooltip = cgiGet( "DDO_LOTE_DATA_Tooltip");
            Ddo_lote_data_Cls = cgiGet( "DDO_LOTE_DATA_Cls");
            Ddo_lote_data_Filteredtext_set = cgiGet( "DDO_LOTE_DATA_Filteredtext_set");
            Ddo_lote_data_Filteredtextto_set = cgiGet( "DDO_LOTE_DATA_Filteredtextto_set");
            Ddo_lote_data_Dropdownoptionstype = cgiGet( "DDO_LOTE_DATA_Dropdownoptionstype");
            Ddo_lote_data_Titlecontrolidtoreplace = cgiGet( "DDO_LOTE_DATA_Titlecontrolidtoreplace");
            Ddo_lote_data_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_LOTE_DATA_Includesortasc"));
            Ddo_lote_data_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_LOTE_DATA_Includesortdsc"));
            Ddo_lote_data_Sortedstatus = cgiGet( "DDO_LOTE_DATA_Sortedstatus");
            Ddo_lote_data_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_LOTE_DATA_Includefilter"));
            Ddo_lote_data_Filtertype = cgiGet( "DDO_LOTE_DATA_Filtertype");
            Ddo_lote_data_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_LOTE_DATA_Filterisrange"));
            Ddo_lote_data_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_LOTE_DATA_Includedatalist"));
            Ddo_lote_data_Sortasc = cgiGet( "DDO_LOTE_DATA_Sortasc");
            Ddo_lote_data_Sortdsc = cgiGet( "DDO_LOTE_DATA_Sortdsc");
            Ddo_lote_data_Cleanfilter = cgiGet( "DDO_LOTE_DATA_Cleanfilter");
            Ddo_lote_data_Rangefilterfrom = cgiGet( "DDO_LOTE_DATA_Rangefilterfrom");
            Ddo_lote_data_Rangefilterto = cgiGet( "DDO_LOTE_DATA_Rangefilterto");
            Ddo_lote_data_Searchbuttontext = cgiGet( "DDO_LOTE_DATA_Searchbuttontext");
            Ddo_lote_usercod_Caption = cgiGet( "DDO_LOTE_USERCOD_Caption");
            Ddo_lote_usercod_Tooltip = cgiGet( "DDO_LOTE_USERCOD_Tooltip");
            Ddo_lote_usercod_Cls = cgiGet( "DDO_LOTE_USERCOD_Cls");
            Ddo_lote_usercod_Filteredtext_set = cgiGet( "DDO_LOTE_USERCOD_Filteredtext_set");
            Ddo_lote_usercod_Filteredtextto_set = cgiGet( "DDO_LOTE_USERCOD_Filteredtextto_set");
            Ddo_lote_usercod_Dropdownoptionstype = cgiGet( "DDO_LOTE_USERCOD_Dropdownoptionstype");
            Ddo_lote_usercod_Titlecontrolidtoreplace = cgiGet( "DDO_LOTE_USERCOD_Titlecontrolidtoreplace");
            Ddo_lote_usercod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_LOTE_USERCOD_Includesortasc"));
            Ddo_lote_usercod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_LOTE_USERCOD_Includesortdsc"));
            Ddo_lote_usercod_Sortedstatus = cgiGet( "DDO_LOTE_USERCOD_Sortedstatus");
            Ddo_lote_usercod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_LOTE_USERCOD_Includefilter"));
            Ddo_lote_usercod_Filtertype = cgiGet( "DDO_LOTE_USERCOD_Filtertype");
            Ddo_lote_usercod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_LOTE_USERCOD_Filterisrange"));
            Ddo_lote_usercod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_LOTE_USERCOD_Includedatalist"));
            Ddo_lote_usercod_Sortasc = cgiGet( "DDO_LOTE_USERCOD_Sortasc");
            Ddo_lote_usercod_Sortdsc = cgiGet( "DDO_LOTE_USERCOD_Sortdsc");
            Ddo_lote_usercod_Cleanfilter = cgiGet( "DDO_LOTE_USERCOD_Cleanfilter");
            Ddo_lote_usercod_Rangefilterfrom = cgiGet( "DDO_LOTE_USERCOD_Rangefilterfrom");
            Ddo_lote_usercod_Rangefilterto = cgiGet( "DDO_LOTE_USERCOD_Rangefilterto");
            Ddo_lote_usercod_Searchbuttontext = cgiGet( "DDO_LOTE_USERCOD_Searchbuttontext");
            Ddo_lote_pessoacod_Caption = cgiGet( "DDO_LOTE_PESSOACOD_Caption");
            Ddo_lote_pessoacod_Tooltip = cgiGet( "DDO_LOTE_PESSOACOD_Tooltip");
            Ddo_lote_pessoacod_Cls = cgiGet( "DDO_LOTE_PESSOACOD_Cls");
            Ddo_lote_pessoacod_Filteredtext_set = cgiGet( "DDO_LOTE_PESSOACOD_Filteredtext_set");
            Ddo_lote_pessoacod_Filteredtextto_set = cgiGet( "DDO_LOTE_PESSOACOD_Filteredtextto_set");
            Ddo_lote_pessoacod_Dropdownoptionstype = cgiGet( "DDO_LOTE_PESSOACOD_Dropdownoptionstype");
            Ddo_lote_pessoacod_Titlecontrolidtoreplace = cgiGet( "DDO_LOTE_PESSOACOD_Titlecontrolidtoreplace");
            Ddo_lote_pessoacod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_LOTE_PESSOACOD_Includesortasc"));
            Ddo_lote_pessoacod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_LOTE_PESSOACOD_Includesortdsc"));
            Ddo_lote_pessoacod_Sortedstatus = cgiGet( "DDO_LOTE_PESSOACOD_Sortedstatus");
            Ddo_lote_pessoacod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_LOTE_PESSOACOD_Includefilter"));
            Ddo_lote_pessoacod_Filtertype = cgiGet( "DDO_LOTE_PESSOACOD_Filtertype");
            Ddo_lote_pessoacod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_LOTE_PESSOACOD_Filterisrange"));
            Ddo_lote_pessoacod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_LOTE_PESSOACOD_Includedatalist"));
            Ddo_lote_pessoacod_Sortasc = cgiGet( "DDO_LOTE_PESSOACOD_Sortasc");
            Ddo_lote_pessoacod_Sortdsc = cgiGet( "DDO_LOTE_PESSOACOD_Sortdsc");
            Ddo_lote_pessoacod_Cleanfilter = cgiGet( "DDO_LOTE_PESSOACOD_Cleanfilter");
            Ddo_lote_pessoacod_Rangefilterfrom = cgiGet( "DDO_LOTE_PESSOACOD_Rangefilterfrom");
            Ddo_lote_pessoacod_Rangefilterto = cgiGet( "DDO_LOTE_PESSOACOD_Rangefilterto");
            Ddo_lote_pessoacod_Searchbuttontext = cgiGet( "DDO_LOTE_PESSOACOD_Searchbuttontext");
            Ddo_lote_usernom_Caption = cgiGet( "DDO_LOTE_USERNOM_Caption");
            Ddo_lote_usernom_Tooltip = cgiGet( "DDO_LOTE_USERNOM_Tooltip");
            Ddo_lote_usernom_Cls = cgiGet( "DDO_LOTE_USERNOM_Cls");
            Ddo_lote_usernom_Filteredtext_set = cgiGet( "DDO_LOTE_USERNOM_Filteredtext_set");
            Ddo_lote_usernom_Selectedvalue_set = cgiGet( "DDO_LOTE_USERNOM_Selectedvalue_set");
            Ddo_lote_usernom_Dropdownoptionstype = cgiGet( "DDO_LOTE_USERNOM_Dropdownoptionstype");
            Ddo_lote_usernom_Titlecontrolidtoreplace = cgiGet( "DDO_LOTE_USERNOM_Titlecontrolidtoreplace");
            Ddo_lote_usernom_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_LOTE_USERNOM_Includesortasc"));
            Ddo_lote_usernom_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_LOTE_USERNOM_Includesortdsc"));
            Ddo_lote_usernom_Sortedstatus = cgiGet( "DDO_LOTE_USERNOM_Sortedstatus");
            Ddo_lote_usernom_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_LOTE_USERNOM_Includefilter"));
            Ddo_lote_usernom_Filtertype = cgiGet( "DDO_LOTE_USERNOM_Filtertype");
            Ddo_lote_usernom_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_LOTE_USERNOM_Filterisrange"));
            Ddo_lote_usernom_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_LOTE_USERNOM_Includedatalist"));
            Ddo_lote_usernom_Datalisttype = cgiGet( "DDO_LOTE_USERNOM_Datalisttype");
            Ddo_lote_usernom_Datalistproc = cgiGet( "DDO_LOTE_USERNOM_Datalistproc");
            Ddo_lote_usernom_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_LOTE_USERNOM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_lote_usernom_Sortasc = cgiGet( "DDO_LOTE_USERNOM_Sortasc");
            Ddo_lote_usernom_Sortdsc = cgiGet( "DDO_LOTE_USERNOM_Sortdsc");
            Ddo_lote_usernom_Loadingdata = cgiGet( "DDO_LOTE_USERNOM_Loadingdata");
            Ddo_lote_usernom_Cleanfilter = cgiGet( "DDO_LOTE_USERNOM_Cleanfilter");
            Ddo_lote_usernom_Noresultsfound = cgiGet( "DDO_LOTE_USERNOM_Noresultsfound");
            Ddo_lote_usernom_Searchbuttontext = cgiGet( "DDO_LOTE_USERNOM_Searchbuttontext");
            Ddo_lote_valorpf_Caption = cgiGet( "DDO_LOTE_VALORPF_Caption");
            Ddo_lote_valorpf_Tooltip = cgiGet( "DDO_LOTE_VALORPF_Tooltip");
            Ddo_lote_valorpf_Cls = cgiGet( "DDO_LOTE_VALORPF_Cls");
            Ddo_lote_valorpf_Filteredtext_set = cgiGet( "DDO_LOTE_VALORPF_Filteredtext_set");
            Ddo_lote_valorpf_Filteredtextto_set = cgiGet( "DDO_LOTE_VALORPF_Filteredtextto_set");
            Ddo_lote_valorpf_Dropdownoptionstype = cgiGet( "DDO_LOTE_VALORPF_Dropdownoptionstype");
            Ddo_lote_valorpf_Titlecontrolidtoreplace = cgiGet( "DDO_LOTE_VALORPF_Titlecontrolidtoreplace");
            Ddo_lote_valorpf_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_LOTE_VALORPF_Includesortasc"));
            Ddo_lote_valorpf_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_LOTE_VALORPF_Includesortdsc"));
            Ddo_lote_valorpf_Sortedstatus = cgiGet( "DDO_LOTE_VALORPF_Sortedstatus");
            Ddo_lote_valorpf_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_LOTE_VALORPF_Includefilter"));
            Ddo_lote_valorpf_Filtertype = cgiGet( "DDO_LOTE_VALORPF_Filtertype");
            Ddo_lote_valorpf_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_LOTE_VALORPF_Filterisrange"));
            Ddo_lote_valorpf_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_LOTE_VALORPF_Includedatalist"));
            Ddo_lote_valorpf_Sortasc = cgiGet( "DDO_LOTE_VALORPF_Sortasc");
            Ddo_lote_valorpf_Sortdsc = cgiGet( "DDO_LOTE_VALORPF_Sortdsc");
            Ddo_lote_valorpf_Cleanfilter = cgiGet( "DDO_LOTE_VALORPF_Cleanfilter");
            Ddo_lote_valorpf_Rangefilterfrom = cgiGet( "DDO_LOTE_VALORPF_Rangefilterfrom");
            Ddo_lote_valorpf_Rangefilterto = cgiGet( "DDO_LOTE_VALORPF_Rangefilterto");
            Ddo_lote_valorpf_Searchbuttontext = cgiGet( "DDO_LOTE_VALORPF_Searchbuttontext");
            Ddo_lote_dataini_Caption = cgiGet( "DDO_LOTE_DATAINI_Caption");
            Ddo_lote_dataini_Tooltip = cgiGet( "DDO_LOTE_DATAINI_Tooltip");
            Ddo_lote_dataini_Cls = cgiGet( "DDO_LOTE_DATAINI_Cls");
            Ddo_lote_dataini_Filteredtext_set = cgiGet( "DDO_LOTE_DATAINI_Filteredtext_set");
            Ddo_lote_dataini_Filteredtextto_set = cgiGet( "DDO_LOTE_DATAINI_Filteredtextto_set");
            Ddo_lote_dataini_Dropdownoptionstype = cgiGet( "DDO_LOTE_DATAINI_Dropdownoptionstype");
            Ddo_lote_dataini_Titlecontrolidtoreplace = cgiGet( "DDO_LOTE_DATAINI_Titlecontrolidtoreplace");
            Ddo_lote_dataini_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_LOTE_DATAINI_Includesortasc"));
            Ddo_lote_dataini_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_LOTE_DATAINI_Includesortdsc"));
            Ddo_lote_dataini_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_LOTE_DATAINI_Includefilter"));
            Ddo_lote_dataini_Filtertype = cgiGet( "DDO_LOTE_DATAINI_Filtertype");
            Ddo_lote_dataini_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_LOTE_DATAINI_Filterisrange"));
            Ddo_lote_dataini_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_LOTE_DATAINI_Includedatalist"));
            Ddo_lote_dataini_Cleanfilter = cgiGet( "DDO_LOTE_DATAINI_Cleanfilter");
            Ddo_lote_dataini_Rangefilterfrom = cgiGet( "DDO_LOTE_DATAINI_Rangefilterfrom");
            Ddo_lote_dataini_Rangefilterto = cgiGet( "DDO_LOTE_DATAINI_Rangefilterto");
            Ddo_lote_dataini_Searchbuttontext = cgiGet( "DDO_LOTE_DATAINI_Searchbuttontext");
            Ddo_lote_datafim_Caption = cgiGet( "DDO_LOTE_DATAFIM_Caption");
            Ddo_lote_datafim_Tooltip = cgiGet( "DDO_LOTE_DATAFIM_Tooltip");
            Ddo_lote_datafim_Cls = cgiGet( "DDO_LOTE_DATAFIM_Cls");
            Ddo_lote_datafim_Filteredtext_set = cgiGet( "DDO_LOTE_DATAFIM_Filteredtext_set");
            Ddo_lote_datafim_Filteredtextto_set = cgiGet( "DDO_LOTE_DATAFIM_Filteredtextto_set");
            Ddo_lote_datafim_Dropdownoptionstype = cgiGet( "DDO_LOTE_DATAFIM_Dropdownoptionstype");
            Ddo_lote_datafim_Titlecontrolidtoreplace = cgiGet( "DDO_LOTE_DATAFIM_Titlecontrolidtoreplace");
            Ddo_lote_datafim_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_LOTE_DATAFIM_Includesortasc"));
            Ddo_lote_datafim_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_LOTE_DATAFIM_Includesortdsc"));
            Ddo_lote_datafim_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_LOTE_DATAFIM_Includefilter"));
            Ddo_lote_datafim_Filtertype = cgiGet( "DDO_LOTE_DATAFIM_Filtertype");
            Ddo_lote_datafim_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_LOTE_DATAFIM_Filterisrange"));
            Ddo_lote_datafim_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_LOTE_DATAFIM_Includedatalist"));
            Ddo_lote_datafim_Cleanfilter = cgiGet( "DDO_LOTE_DATAFIM_Cleanfilter");
            Ddo_lote_datafim_Rangefilterfrom = cgiGet( "DDO_LOTE_DATAFIM_Rangefilterfrom");
            Ddo_lote_datafim_Rangefilterto = cgiGet( "DDO_LOTE_DATAFIM_Rangefilterto");
            Ddo_lote_datafim_Searchbuttontext = cgiGet( "DDO_LOTE_DATAFIM_Searchbuttontext");
            Ddo_lote_qtdedmn_Caption = cgiGet( "DDO_LOTE_QTDEDMN_Caption");
            Ddo_lote_qtdedmn_Tooltip = cgiGet( "DDO_LOTE_QTDEDMN_Tooltip");
            Ddo_lote_qtdedmn_Cls = cgiGet( "DDO_LOTE_QTDEDMN_Cls");
            Ddo_lote_qtdedmn_Filteredtext_set = cgiGet( "DDO_LOTE_QTDEDMN_Filteredtext_set");
            Ddo_lote_qtdedmn_Filteredtextto_set = cgiGet( "DDO_LOTE_QTDEDMN_Filteredtextto_set");
            Ddo_lote_qtdedmn_Dropdownoptionstype = cgiGet( "DDO_LOTE_QTDEDMN_Dropdownoptionstype");
            Ddo_lote_qtdedmn_Titlecontrolidtoreplace = cgiGet( "DDO_LOTE_QTDEDMN_Titlecontrolidtoreplace");
            Ddo_lote_qtdedmn_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_LOTE_QTDEDMN_Includesortasc"));
            Ddo_lote_qtdedmn_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_LOTE_QTDEDMN_Includesortdsc"));
            Ddo_lote_qtdedmn_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_LOTE_QTDEDMN_Includefilter"));
            Ddo_lote_qtdedmn_Filtertype = cgiGet( "DDO_LOTE_QTDEDMN_Filtertype");
            Ddo_lote_qtdedmn_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_LOTE_QTDEDMN_Filterisrange"));
            Ddo_lote_qtdedmn_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_LOTE_QTDEDMN_Includedatalist"));
            Ddo_lote_qtdedmn_Cleanfilter = cgiGet( "DDO_LOTE_QTDEDMN_Cleanfilter");
            Ddo_lote_qtdedmn_Rangefilterfrom = cgiGet( "DDO_LOTE_QTDEDMN_Rangefilterfrom");
            Ddo_lote_qtdedmn_Rangefilterto = cgiGet( "DDO_LOTE_QTDEDMN_Rangefilterto");
            Ddo_lote_qtdedmn_Searchbuttontext = cgiGet( "DDO_LOTE_QTDEDMN_Searchbuttontext");
            Ddo_lote_valor_Caption = cgiGet( "DDO_LOTE_VALOR_Caption");
            Ddo_lote_valor_Tooltip = cgiGet( "DDO_LOTE_VALOR_Tooltip");
            Ddo_lote_valor_Cls = cgiGet( "DDO_LOTE_VALOR_Cls");
            Ddo_lote_valor_Filteredtext_set = cgiGet( "DDO_LOTE_VALOR_Filteredtext_set");
            Ddo_lote_valor_Filteredtextto_set = cgiGet( "DDO_LOTE_VALOR_Filteredtextto_set");
            Ddo_lote_valor_Dropdownoptionstype = cgiGet( "DDO_LOTE_VALOR_Dropdownoptionstype");
            Ddo_lote_valor_Titlecontrolidtoreplace = cgiGet( "DDO_LOTE_VALOR_Titlecontrolidtoreplace");
            Ddo_lote_valor_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_LOTE_VALOR_Includesortasc"));
            Ddo_lote_valor_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_LOTE_VALOR_Includesortdsc"));
            Ddo_lote_valor_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_LOTE_VALOR_Includefilter"));
            Ddo_lote_valor_Filtertype = cgiGet( "DDO_LOTE_VALOR_Filtertype");
            Ddo_lote_valor_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_LOTE_VALOR_Filterisrange"));
            Ddo_lote_valor_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_LOTE_VALOR_Includedatalist"));
            Ddo_lote_valor_Cleanfilter = cgiGet( "DDO_LOTE_VALOR_Cleanfilter");
            Ddo_lote_valor_Rangefilterfrom = cgiGet( "DDO_LOTE_VALOR_Rangefilterfrom");
            Ddo_lote_valor_Rangefilterto = cgiGet( "DDO_LOTE_VALOR_Rangefilterto");
            Ddo_lote_valor_Searchbuttontext = cgiGet( "DDO_LOTE_VALOR_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_lote_numero_Activeeventkey = cgiGet( "DDO_LOTE_NUMERO_Activeeventkey");
            Ddo_lote_numero_Filteredtext_get = cgiGet( "DDO_LOTE_NUMERO_Filteredtext_get");
            Ddo_lote_numero_Selectedvalue_get = cgiGet( "DDO_LOTE_NUMERO_Selectedvalue_get");
            Ddo_lote_nome_Activeeventkey = cgiGet( "DDO_LOTE_NOME_Activeeventkey");
            Ddo_lote_nome_Filteredtext_get = cgiGet( "DDO_LOTE_NOME_Filteredtext_get");
            Ddo_lote_nome_Selectedvalue_get = cgiGet( "DDO_LOTE_NOME_Selectedvalue_get");
            Ddo_lote_data_Activeeventkey = cgiGet( "DDO_LOTE_DATA_Activeeventkey");
            Ddo_lote_data_Filteredtext_get = cgiGet( "DDO_LOTE_DATA_Filteredtext_get");
            Ddo_lote_data_Filteredtextto_get = cgiGet( "DDO_LOTE_DATA_Filteredtextto_get");
            Ddo_lote_usercod_Activeeventkey = cgiGet( "DDO_LOTE_USERCOD_Activeeventkey");
            Ddo_lote_usercod_Filteredtext_get = cgiGet( "DDO_LOTE_USERCOD_Filteredtext_get");
            Ddo_lote_usercod_Filteredtextto_get = cgiGet( "DDO_LOTE_USERCOD_Filteredtextto_get");
            Ddo_lote_pessoacod_Activeeventkey = cgiGet( "DDO_LOTE_PESSOACOD_Activeeventkey");
            Ddo_lote_pessoacod_Filteredtext_get = cgiGet( "DDO_LOTE_PESSOACOD_Filteredtext_get");
            Ddo_lote_pessoacod_Filteredtextto_get = cgiGet( "DDO_LOTE_PESSOACOD_Filteredtextto_get");
            Ddo_lote_usernom_Activeeventkey = cgiGet( "DDO_LOTE_USERNOM_Activeeventkey");
            Ddo_lote_usernom_Filteredtext_get = cgiGet( "DDO_LOTE_USERNOM_Filteredtext_get");
            Ddo_lote_usernom_Selectedvalue_get = cgiGet( "DDO_LOTE_USERNOM_Selectedvalue_get");
            Ddo_lote_valorpf_Activeeventkey = cgiGet( "DDO_LOTE_VALORPF_Activeeventkey");
            Ddo_lote_valorpf_Filteredtext_get = cgiGet( "DDO_LOTE_VALORPF_Filteredtext_get");
            Ddo_lote_valorpf_Filteredtextto_get = cgiGet( "DDO_LOTE_VALORPF_Filteredtextto_get");
            Ddo_lote_dataini_Activeeventkey = cgiGet( "DDO_LOTE_DATAINI_Activeeventkey");
            Ddo_lote_dataini_Filteredtext_get = cgiGet( "DDO_LOTE_DATAINI_Filteredtext_get");
            Ddo_lote_dataini_Filteredtextto_get = cgiGet( "DDO_LOTE_DATAINI_Filteredtextto_get");
            Ddo_lote_datafim_Activeeventkey = cgiGet( "DDO_LOTE_DATAFIM_Activeeventkey");
            Ddo_lote_datafim_Filteredtext_get = cgiGet( "DDO_LOTE_DATAFIM_Filteredtext_get");
            Ddo_lote_datafim_Filteredtextto_get = cgiGet( "DDO_LOTE_DATAFIM_Filteredtextto_get");
            Ddo_lote_qtdedmn_Activeeventkey = cgiGet( "DDO_LOTE_QTDEDMN_Activeeventkey");
            Ddo_lote_qtdedmn_Filteredtext_get = cgiGet( "DDO_LOTE_QTDEDMN_Filteredtext_get");
            Ddo_lote_qtdedmn_Filteredtextto_get = cgiGet( "DDO_LOTE_QTDEDMN_Filteredtextto_get");
            Ddo_lote_valor_Activeeventkey = cgiGet( "DDO_LOTE_VALOR_Activeeventkey");
            Ddo_lote_valor_Filteredtext_get = cgiGet( "DDO_LOTE_VALOR_Filteredtext_get");
            Ddo_lote_valor_Filteredtextto_get = cgiGet( "DDO_LOTE_VALOR_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = "hsh" + "PromptLote";
            A596Lote_Codigo = (int)(context.localUtil.CToN( cgiGet( edtLote_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A596Lote_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A596Lote_Codigo), 6, 0)));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A596Lote_Codigo), "ZZZZZ9");
            hsh = cgiGet( "hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("promptlote:[SecurityCheckFailed value for]"+"Lote_Codigo:"+context.localUtil.Format( (decimal)(A596Lote_Codigo), "ZZZZZ9"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vLOTE_NOME1"), AV17Lote_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vLOTE_USERNOM1"), AV18Lote_UserNom1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV21DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vLOTE_NOME2"), AV22Lote_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vLOTE_USERNOM2"), AV23Lote_UserNom2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV25DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV26DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vLOTE_NOME3"), AV27Lote_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vLOTE_USERNOM3"), AV28Lote_UserNom3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV24DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFLOTE_NUMERO"), AV38TFLote_Numero) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFLOTE_NUMERO_SEL"), AV39TFLote_Numero_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFLOTE_NOME"), AV42TFLote_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFLOTE_NOME_SEL"), AV43TFLote_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFLOTE_DATA"), 0) != AV46TFLote_Data )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFLOTE_DATA_TO"), 0) != AV47TFLote_Data_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFLOTE_USERCOD"), ",", ".") != Convert.ToDecimal( AV52TFLote_UserCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFLOTE_USERCOD_TO"), ",", ".") != Convert.ToDecimal( AV53TFLote_UserCod_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFLOTE_PESSOACOD"), ",", ".") != Convert.ToDecimal( AV56TFLote_PessoaCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFLOTE_PESSOACOD_TO"), ",", ".") != Convert.ToDecimal( AV57TFLote_PessoaCod_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFLOTE_USERNOM"), AV60TFLote_UserNom) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFLOTE_USERNOM_SEL"), AV61TFLote_UserNom_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFLOTE_VALORPF"), ",", ".") != AV64TFLote_ValorPF )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFLOTE_VALORPF_TO"), ",", ".") != AV65TFLote_ValorPF_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFLOTE_DATAINI"), 0) != AV68TFLote_DataIni )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFLOTE_DATAINI_TO"), 0) != AV69TFLote_DataIni_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFLOTE_DATAFIM"), 0) != AV74TFLote_DataFim )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFLOTE_DATAFIM_TO"), 0) != AV75TFLote_DataFim_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFLOTE_QTDEDMN"), ",", ".") != Convert.ToDecimal( AV80TFLote_QtdeDmn )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFLOTE_QTDEDMN_TO"), ",", ".") != Convert.ToDecimal( AV81TFLote_QtdeDmn_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFLOTE_VALOR"), ",", ".") != AV84TFLote_Valor )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFLOTE_VALOR_TO"), ",", ".") != AV85TFLote_Valor_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E33BR2 */
         E33BR2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E33BR2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "LOTE_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV20DynamicFiltersSelector2 = "LOTE_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV25DynamicFiltersSelector3 = "LOTE_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTflote_numero_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTflote_numero_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflote_numero_Visible), 5, 0)));
         edtavTflote_numero_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTflote_numero_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflote_numero_sel_Visible), 5, 0)));
         edtavTflote_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTflote_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflote_nome_Visible), 5, 0)));
         edtavTflote_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTflote_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflote_nome_sel_Visible), 5, 0)));
         edtavTflote_data_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTflote_data_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflote_data_Visible), 5, 0)));
         edtavTflote_data_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTflote_data_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflote_data_to_Visible), 5, 0)));
         edtavTflote_usercod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTflote_usercod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflote_usercod_Visible), 5, 0)));
         edtavTflote_usercod_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTflote_usercod_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflote_usercod_to_Visible), 5, 0)));
         edtavTflote_pessoacod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTflote_pessoacod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflote_pessoacod_Visible), 5, 0)));
         edtavTflote_pessoacod_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTflote_pessoacod_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflote_pessoacod_to_Visible), 5, 0)));
         edtavTflote_usernom_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTflote_usernom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflote_usernom_Visible), 5, 0)));
         edtavTflote_usernom_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTflote_usernom_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflote_usernom_sel_Visible), 5, 0)));
         edtavTflote_valorpf_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTflote_valorpf_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflote_valorpf_Visible), 5, 0)));
         edtavTflote_valorpf_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTflote_valorpf_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflote_valorpf_to_Visible), 5, 0)));
         edtavTflote_dataini_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTflote_dataini_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflote_dataini_Visible), 5, 0)));
         edtavTflote_dataini_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTflote_dataini_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflote_dataini_to_Visible), 5, 0)));
         edtavTflote_datafim_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTflote_datafim_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflote_datafim_Visible), 5, 0)));
         edtavTflote_datafim_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTflote_datafim_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflote_datafim_to_Visible), 5, 0)));
         edtavTflote_qtdedmn_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTflote_qtdedmn_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflote_qtdedmn_Visible), 5, 0)));
         edtavTflote_qtdedmn_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTflote_qtdedmn_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflote_qtdedmn_to_Visible), 5, 0)));
         edtavTflote_valor_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTflote_valor_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflote_valor_Visible), 5, 0)));
         edtavTflote_valor_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTflote_valor_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflote_valor_to_Visible), 5, 0)));
         Ddo_lote_numero_Titlecontrolidtoreplace = subGrid_Internalname+"_Lote_Numero";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_numero_Internalname, "TitleControlIdToReplace", Ddo_lote_numero_Titlecontrolidtoreplace);
         AV40ddo_Lote_NumeroTitleControlIdToReplace = Ddo_lote_numero_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ddo_Lote_NumeroTitleControlIdToReplace", AV40ddo_Lote_NumeroTitleControlIdToReplace);
         edtavDdo_lote_numerotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_lote_numerotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_lote_numerotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_lote_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_Lote_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_nome_Internalname, "TitleControlIdToReplace", Ddo_lote_nome_Titlecontrolidtoreplace);
         AV44ddo_Lote_NomeTitleControlIdToReplace = Ddo_lote_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ddo_Lote_NomeTitleControlIdToReplace", AV44ddo_Lote_NomeTitleControlIdToReplace);
         edtavDdo_lote_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_lote_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_lote_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_lote_data_Titlecontrolidtoreplace = subGrid_Internalname+"_Lote_Data";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_data_Internalname, "TitleControlIdToReplace", Ddo_lote_data_Titlecontrolidtoreplace);
         AV50ddo_Lote_DataTitleControlIdToReplace = Ddo_lote_data_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ddo_Lote_DataTitleControlIdToReplace", AV50ddo_Lote_DataTitleControlIdToReplace);
         edtavDdo_lote_datatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_lote_datatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_lote_datatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_lote_usercod_Titlecontrolidtoreplace = subGrid_Internalname+"_Lote_UserCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_usercod_Internalname, "TitleControlIdToReplace", Ddo_lote_usercod_Titlecontrolidtoreplace);
         AV54ddo_Lote_UserCodTitleControlIdToReplace = Ddo_lote_usercod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54ddo_Lote_UserCodTitleControlIdToReplace", AV54ddo_Lote_UserCodTitleControlIdToReplace);
         edtavDdo_lote_usercodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_lote_usercodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_lote_usercodtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_lote_pessoacod_Titlecontrolidtoreplace = subGrid_Internalname+"_Lote_PessoaCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_pessoacod_Internalname, "TitleControlIdToReplace", Ddo_lote_pessoacod_Titlecontrolidtoreplace);
         AV58ddo_Lote_PessoaCodTitleControlIdToReplace = Ddo_lote_pessoacod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58ddo_Lote_PessoaCodTitleControlIdToReplace", AV58ddo_Lote_PessoaCodTitleControlIdToReplace);
         edtavDdo_lote_pessoacodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_lote_pessoacodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_lote_pessoacodtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_lote_usernom_Titlecontrolidtoreplace = subGrid_Internalname+"_Lote_UserNom";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_usernom_Internalname, "TitleControlIdToReplace", Ddo_lote_usernom_Titlecontrolidtoreplace);
         AV62ddo_Lote_UserNomTitleControlIdToReplace = Ddo_lote_usernom_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62ddo_Lote_UserNomTitleControlIdToReplace", AV62ddo_Lote_UserNomTitleControlIdToReplace);
         edtavDdo_lote_usernomtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_lote_usernomtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_lote_usernomtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_lote_valorpf_Titlecontrolidtoreplace = subGrid_Internalname+"_Lote_ValorPF";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_valorpf_Internalname, "TitleControlIdToReplace", Ddo_lote_valorpf_Titlecontrolidtoreplace);
         AV66ddo_Lote_ValorPFTitleControlIdToReplace = Ddo_lote_valorpf_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66ddo_Lote_ValorPFTitleControlIdToReplace", AV66ddo_Lote_ValorPFTitleControlIdToReplace);
         edtavDdo_lote_valorpftitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_lote_valorpftitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_lote_valorpftitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_lote_dataini_Titlecontrolidtoreplace = subGrid_Internalname+"_Lote_DataIni";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_dataini_Internalname, "TitleControlIdToReplace", Ddo_lote_dataini_Titlecontrolidtoreplace);
         AV72ddo_Lote_DataIniTitleControlIdToReplace = Ddo_lote_dataini_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72ddo_Lote_DataIniTitleControlIdToReplace", AV72ddo_Lote_DataIniTitleControlIdToReplace);
         edtavDdo_lote_datainititlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_lote_datainititlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_lote_datainititlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_lote_datafim_Titlecontrolidtoreplace = subGrid_Internalname+"_Lote_DataFim";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_datafim_Internalname, "TitleControlIdToReplace", Ddo_lote_datafim_Titlecontrolidtoreplace);
         AV78ddo_Lote_DataFimTitleControlIdToReplace = Ddo_lote_datafim_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78ddo_Lote_DataFimTitleControlIdToReplace", AV78ddo_Lote_DataFimTitleControlIdToReplace);
         edtavDdo_lote_datafimtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_lote_datafimtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_lote_datafimtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_lote_qtdedmn_Titlecontrolidtoreplace = subGrid_Internalname+"_Lote_QtdeDmn";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_qtdedmn_Internalname, "TitleControlIdToReplace", Ddo_lote_qtdedmn_Titlecontrolidtoreplace);
         AV82ddo_Lote_QtdeDmnTitleControlIdToReplace = Ddo_lote_qtdedmn_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82ddo_Lote_QtdeDmnTitleControlIdToReplace", AV82ddo_Lote_QtdeDmnTitleControlIdToReplace);
         edtavDdo_lote_qtdedmntitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_lote_qtdedmntitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_lote_qtdedmntitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_lote_valor_Titlecontrolidtoreplace = subGrid_Internalname+"_Lote_Valor";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_valor_Internalname, "TitleControlIdToReplace", Ddo_lote_valor_Titlecontrolidtoreplace);
         AV86ddo_Lote_ValorTitleControlIdToReplace = Ddo_lote_valor_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86ddo_Lote_ValorTitleControlIdToReplace", AV86ddo_Lote_ValorTitleControlIdToReplace);
         edtavDdo_lote_valortitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_lote_valortitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_lote_valortitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Selecione Lote";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         edtLote_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_Codigo_Visible), 5, 0)));
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Nome", 0);
         cmbavOrderedby.addItem("2", "Lote", 0);
         cmbavOrderedby.addItem("3", "Data", 0);
         cmbavOrderedby.addItem("4", "Usu�rio", 0);
         cmbavOrderedby.addItem("5", "Pessoa", 0);
         cmbavOrderedby.addItem("6", "Usu�rio", 0);
         cmbavOrderedby.addItem("7", "PF R$", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons2 = AV87DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons2) ;
         AV87DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons2;
      }

      protected void E34BR2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV37Lote_NumeroTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV41Lote_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV45Lote_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV51Lote_UserCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV55Lote_PessoaCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV59Lote_UserNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV63Lote_ValorPFTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV67Lote_DataIniTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV73Lote_DataFimTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV79Lote_QtdeDmnTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV83Lote_ValorTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         cmbavDynamicfiltersoperator1.removeAllItems();
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "LOTE_NOME") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "LOTE_USERNOM") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         if ( AV19DynamicFiltersEnabled2 )
         {
            cmbavDynamicfiltersoperator2.removeAllItems();
            if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "LOTE_NOME") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "LOTE_USERNOM") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            if ( AV24DynamicFiltersEnabled3 )
            {
               cmbavDynamicfiltersoperator3.removeAllItems();
               if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "LOTE_NOME") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
               else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "LOTE_USERNOM") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
            }
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtLote_Numero_Titleformat = 2;
         edtLote_Numero_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Lote", AV40ddo_Lote_NumeroTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_Numero_Internalname, "Title", edtLote_Numero_Title);
         edtLote_Nome_Titleformat = 2;
         edtLote_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV44ddo_Lote_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_Nome_Internalname, "Title", edtLote_Nome_Title);
         edtLote_Data_Titleformat = 2;
         edtLote_Data_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Data", AV50ddo_Lote_DataTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_Data_Internalname, "Title", edtLote_Data_Title);
         edtLote_UserCod_Titleformat = 2;
         edtLote_UserCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Usu�rio", AV54ddo_Lote_UserCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_UserCod_Internalname, "Title", edtLote_UserCod_Title);
         edtLote_PessoaCod_Titleformat = 2;
         edtLote_PessoaCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Pessoa", AV58ddo_Lote_PessoaCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_PessoaCod_Internalname, "Title", edtLote_PessoaCod_Title);
         edtLote_UserNom_Titleformat = 2;
         edtLote_UserNom_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Usu�rio", AV62ddo_Lote_UserNomTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_UserNom_Internalname, "Title", edtLote_UserNom_Title);
         edtLote_ValorPF_Titleformat = 2;
         edtLote_ValorPF_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "PF R$", AV66ddo_Lote_ValorPFTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_ValorPF_Internalname, "Title", edtLote_ValorPF_Title);
         edtLote_DataIni_Titleformat = 2;
         edtLote_DataIni_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ini", AV72ddo_Lote_DataIniTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_DataIni_Internalname, "Title", edtLote_DataIni_Title);
         edtLote_DataFim_Titleformat = 2;
         edtLote_DataFim_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Fim", AV78ddo_Lote_DataFimTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_DataFim_Internalname, "Title", edtLote_DataFim_Title);
         edtLote_QtdeDmn_Titleformat = 2;
         edtLote_QtdeDmn_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Dmn", AV82ddo_Lote_QtdeDmnTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_QtdeDmn_Internalname, "Title", edtLote_QtdeDmn_Title);
         edtLote_Valor_Titleformat = 2;
         edtLote_Valor_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "R$", AV86ddo_Lote_ValorTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLote_Valor_Internalname, "Title", edtLote_Valor_Title);
         AV89GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV89GridCurrentPage), 10, 0)));
         AV90GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV90GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV37Lote_NumeroTitleFilterData", AV37Lote_NumeroTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV41Lote_NomeTitleFilterData", AV41Lote_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV45Lote_DataTitleFilterData", AV45Lote_DataTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV51Lote_UserCodTitleFilterData", AV51Lote_UserCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV55Lote_PessoaCodTitleFilterData", AV55Lote_PessoaCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV59Lote_UserNomTitleFilterData", AV59Lote_UserNomTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV63Lote_ValorPFTitleFilterData", AV63Lote_ValorPFTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV67Lote_DataIniTitleFilterData", AV67Lote_DataIniTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV73Lote_DataFimTitleFilterData", AV73Lote_DataFimTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV79Lote_QtdeDmnTitleFilterData", AV79Lote_QtdeDmnTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV83Lote_ValorTitleFilterData", AV83Lote_ValorTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11BR2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV88PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV88PageToGo) ;
         }
      }

      protected void E12BR2( )
      {
         /* Ddo_lote_numero_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_lote_numero_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_lote_numero_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_numero_Internalname, "SortedStatus", Ddo_lote_numero_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_lote_numero_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_lote_numero_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_numero_Internalname, "SortedStatus", Ddo_lote_numero_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_lote_numero_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV38TFLote_Numero = Ddo_lote_numero_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFLote_Numero", AV38TFLote_Numero);
            AV39TFLote_Numero_Sel = Ddo_lote_numero_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFLote_Numero_Sel", AV39TFLote_Numero_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13BR2( )
      {
         /* Ddo_lote_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_lote_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_lote_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_nome_Internalname, "SortedStatus", Ddo_lote_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_lote_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_lote_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_nome_Internalname, "SortedStatus", Ddo_lote_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_lote_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV42TFLote_Nome = Ddo_lote_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFLote_Nome", AV42TFLote_Nome);
            AV43TFLote_Nome_Sel = Ddo_lote_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFLote_Nome_Sel", AV43TFLote_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14BR2( )
      {
         /* Ddo_lote_data_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_lote_data_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_lote_data_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_data_Internalname, "SortedStatus", Ddo_lote_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_lote_data_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_lote_data_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_data_Internalname, "SortedStatus", Ddo_lote_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_lote_data_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV46TFLote_Data = context.localUtil.CToT( Ddo_lote_data_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFLote_Data", context.localUtil.TToC( AV46TFLote_Data, 8, 5, 0, 3, "/", ":", " "));
            AV47TFLote_Data_To = context.localUtil.CToT( Ddo_lote_data_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFLote_Data_To", context.localUtil.TToC( AV47TFLote_Data_To, 8, 5, 0, 3, "/", ":", " "));
            if ( ! (DateTime.MinValue==AV47TFLote_Data_To) )
            {
               AV47TFLote_Data_To = context.localUtil.YMDHMSToT( (short)(DateTimeUtil.Year( AV47TFLote_Data_To)), (short)(DateTimeUtil.Month( AV47TFLote_Data_To)), (short)(DateTimeUtil.Day( AV47TFLote_Data_To)), 23, 59, 59);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFLote_Data_To", context.localUtil.TToC( AV47TFLote_Data_To, 8, 5, 0, 3, "/", ":", " "));
            }
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E15BR2( )
      {
         /* Ddo_lote_usercod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_lote_usercod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_lote_usercod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_usercod_Internalname, "SortedStatus", Ddo_lote_usercod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_lote_usercod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_lote_usercod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_usercod_Internalname, "SortedStatus", Ddo_lote_usercod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_lote_usercod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV52TFLote_UserCod = (int)(NumberUtil.Val( Ddo_lote_usercod_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFLote_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52TFLote_UserCod), 6, 0)));
            AV53TFLote_UserCod_To = (int)(NumberUtil.Val( Ddo_lote_usercod_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFLote_UserCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53TFLote_UserCod_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E16BR2( )
      {
         /* Ddo_lote_pessoacod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_lote_pessoacod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_lote_pessoacod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_pessoacod_Internalname, "SortedStatus", Ddo_lote_pessoacod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_lote_pessoacod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_lote_pessoacod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_pessoacod_Internalname, "SortedStatus", Ddo_lote_pessoacod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_lote_pessoacod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV56TFLote_PessoaCod = (int)(NumberUtil.Val( Ddo_lote_pessoacod_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFLote_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56TFLote_PessoaCod), 6, 0)));
            AV57TFLote_PessoaCod_To = (int)(NumberUtil.Val( Ddo_lote_pessoacod_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFLote_PessoaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV57TFLote_PessoaCod_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E17BR2( )
      {
         /* Ddo_lote_usernom_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_lote_usernom_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_lote_usernom_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_usernom_Internalname, "SortedStatus", Ddo_lote_usernom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_lote_usernom_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_lote_usernom_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_usernom_Internalname, "SortedStatus", Ddo_lote_usernom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_lote_usernom_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV60TFLote_UserNom = Ddo_lote_usernom_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFLote_UserNom", AV60TFLote_UserNom);
            AV61TFLote_UserNom_Sel = Ddo_lote_usernom_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFLote_UserNom_Sel", AV61TFLote_UserNom_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E18BR2( )
      {
         /* Ddo_lote_valorpf_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_lote_valorpf_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_lote_valorpf_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_valorpf_Internalname, "SortedStatus", Ddo_lote_valorpf_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_lote_valorpf_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_lote_valorpf_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_valorpf_Internalname, "SortedStatus", Ddo_lote_valorpf_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_lote_valorpf_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV64TFLote_ValorPF = NumberUtil.Val( Ddo_lote_valorpf_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFLote_ValorPF", StringUtil.LTrim( StringUtil.Str( AV64TFLote_ValorPF, 18, 5)));
            AV65TFLote_ValorPF_To = NumberUtil.Val( Ddo_lote_valorpf_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFLote_ValorPF_To", StringUtil.LTrim( StringUtil.Str( AV65TFLote_ValorPF_To, 18, 5)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E19BR2( )
      {
         /* Ddo_lote_dataini_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_lote_dataini_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV68TFLote_DataIni = context.localUtil.CToD( Ddo_lote_dataini_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFLote_DataIni", context.localUtil.Format(AV68TFLote_DataIni, "99/99/99"));
            AV69TFLote_DataIni_To = context.localUtil.CToD( Ddo_lote_dataini_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFLote_DataIni_To", context.localUtil.Format(AV69TFLote_DataIni_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
      }

      protected void E20BR2( )
      {
         /* Ddo_lote_datafim_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_lote_datafim_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV74TFLote_DataFim = context.localUtil.CToD( Ddo_lote_datafim_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFLote_DataFim", context.localUtil.Format(AV74TFLote_DataFim, "99/99/99"));
            AV75TFLote_DataFim_To = context.localUtil.CToD( Ddo_lote_datafim_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFLote_DataFim_To", context.localUtil.Format(AV75TFLote_DataFim_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
      }

      protected void E21BR2( )
      {
         /* Ddo_lote_qtdedmn_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_lote_qtdedmn_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV80TFLote_QtdeDmn = (short)(NumberUtil.Val( Ddo_lote_qtdedmn_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFLote_QtdeDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV80TFLote_QtdeDmn), 4, 0)));
            AV81TFLote_QtdeDmn_To = (short)(NumberUtil.Val( Ddo_lote_qtdedmn_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFLote_QtdeDmn_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV81TFLote_QtdeDmn_To), 4, 0)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E22BR2( )
      {
         /* Ddo_lote_valor_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_lote_valor_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV84TFLote_Valor = NumberUtil.Val( Ddo_lote_valor_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFLote_Valor", StringUtil.LTrim( StringUtil.Str( AV84TFLote_Valor, 18, 5)));
            AV85TFLote_Valor_To = NumberUtil.Val( Ddo_lote_valor_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85TFLote_Valor_To", StringUtil.LTrim( StringUtil.Str( AV85TFLote_Valor_To, 18, 5)));
            subgrid_firstpage( ) ;
         }
      }

      private void E35BR2( )
      {
         /* Grid_Load Routine */
         AV31Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV31Select);
         AV93Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 94;
         }
         if ( ( subGrid_Islastpage == 1 ) || ( subGrid_Rows == 0 ) || ( ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage ) && ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) )
         {
            sendrow_942( ) ;
            GRID_nEOF = 1;
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            if ( ( subGrid_Islastpage == 1 ) && ( ((int)((GRID_nCurrentRecord) % (subGrid_Recordsperpage( )))) == 0 ) )
            {
               GRID_nFirstRecordOnPage = GRID_nCurrentRecord;
            }
         }
         if ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) )
         {
            GRID_nEOF = 0;
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
         }
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_94_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(94, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E36BR2 */
         E36BR2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E36BR2( )
      {
         /* Enter Routine */
         AV35InOutLote_Codigo = A596Lote_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35InOutLote_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35InOutLote_Codigo), 6, 0)));
         AV8InOutLote_Nome = A563Lote_Nome;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutLote_Nome", AV8InOutLote_Nome);
         context.setWebReturnParms(new Object[] {(int)AV35InOutLote_Codigo,(String)AV8InOutLote_Nome});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E23BR2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E28BR2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV19DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Lote_Nome1, AV18Lote_UserNom1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Lote_Nome2, AV23Lote_UserNom2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27Lote_Nome3, AV28Lote_UserNom3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV38TFLote_Numero, AV39TFLote_Numero_Sel, AV42TFLote_Nome, AV43TFLote_Nome_Sel, AV46TFLote_Data, AV47TFLote_Data_To, AV52TFLote_UserCod, AV53TFLote_UserCod_To, AV56TFLote_PessoaCod, AV57TFLote_PessoaCod_To, AV60TFLote_UserNom, AV61TFLote_UserNom_Sel, AV64TFLote_ValorPF, AV65TFLote_ValorPF_To, AV68TFLote_DataIni, AV69TFLote_DataIni_To, AV74TFLote_DataFim, AV75TFLote_DataFim_To, AV80TFLote_QtdeDmn, AV81TFLote_QtdeDmn_To, AV84TFLote_Valor, AV85TFLote_Valor_To, AV40ddo_Lote_NumeroTitleControlIdToReplace, AV44ddo_Lote_NomeTitleControlIdToReplace, AV50ddo_Lote_DataTitleControlIdToReplace, AV54ddo_Lote_UserCodTitleControlIdToReplace, AV58ddo_Lote_PessoaCodTitleControlIdToReplace, AV62ddo_Lote_UserNomTitleControlIdToReplace, AV66ddo_Lote_ValorPFTitleControlIdToReplace, AV72ddo_Lote_DataIniTitleControlIdToReplace, AV78ddo_Lote_DataFimTitleControlIdToReplace, AV82ddo_Lote_QtdeDmnTitleControlIdToReplace, AV86ddo_Lote_ValorTitleControlIdToReplace, AV34Lote_AreaTrabalhoCod, AV36Lote_ContratadaCod, AV94Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
      }

      protected void E24BR2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV30DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV30DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Lote_Nome1, AV18Lote_UserNom1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Lote_Nome2, AV23Lote_UserNom2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27Lote_Nome3, AV28Lote_UserNom3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV38TFLote_Numero, AV39TFLote_Numero_Sel, AV42TFLote_Nome, AV43TFLote_Nome_Sel, AV46TFLote_Data, AV47TFLote_Data_To, AV52TFLote_UserCod, AV53TFLote_UserCod_To, AV56TFLote_PessoaCod, AV57TFLote_PessoaCod_To, AV60TFLote_UserNom, AV61TFLote_UserNom_Sel, AV64TFLote_ValorPF, AV65TFLote_ValorPF_To, AV68TFLote_DataIni, AV69TFLote_DataIni_To, AV74TFLote_DataFim, AV75TFLote_DataFim_To, AV80TFLote_QtdeDmn, AV81TFLote_QtdeDmn_To, AV84TFLote_Valor, AV85TFLote_Valor_To, AV40ddo_Lote_NumeroTitleControlIdToReplace, AV44ddo_Lote_NomeTitleControlIdToReplace, AV50ddo_Lote_DataTitleControlIdToReplace, AV54ddo_Lote_UserCodTitleControlIdToReplace, AV58ddo_Lote_PessoaCodTitleControlIdToReplace, AV62ddo_Lote_UserNomTitleControlIdToReplace, AV66ddo_Lote_ValorPFTitleControlIdToReplace, AV72ddo_Lote_DataIniTitleControlIdToReplace, AV78ddo_Lote_DataFimTitleControlIdToReplace, AV82ddo_Lote_QtdeDmnTitleControlIdToReplace, AV86ddo_Lote_ValorTitleControlIdToReplace, AV34Lote_AreaTrabalhoCod, AV36Lote_ContratadaCod, AV94Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E29BR2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E30BR2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV24DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Lote_Nome1, AV18Lote_UserNom1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Lote_Nome2, AV23Lote_UserNom2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27Lote_Nome3, AV28Lote_UserNom3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV38TFLote_Numero, AV39TFLote_Numero_Sel, AV42TFLote_Nome, AV43TFLote_Nome_Sel, AV46TFLote_Data, AV47TFLote_Data_To, AV52TFLote_UserCod, AV53TFLote_UserCod_To, AV56TFLote_PessoaCod, AV57TFLote_PessoaCod_To, AV60TFLote_UserNom, AV61TFLote_UserNom_Sel, AV64TFLote_ValorPF, AV65TFLote_ValorPF_To, AV68TFLote_DataIni, AV69TFLote_DataIni_To, AV74TFLote_DataFim, AV75TFLote_DataFim_To, AV80TFLote_QtdeDmn, AV81TFLote_QtdeDmn_To, AV84TFLote_Valor, AV85TFLote_Valor_To, AV40ddo_Lote_NumeroTitleControlIdToReplace, AV44ddo_Lote_NomeTitleControlIdToReplace, AV50ddo_Lote_DataTitleControlIdToReplace, AV54ddo_Lote_UserCodTitleControlIdToReplace, AV58ddo_Lote_PessoaCodTitleControlIdToReplace, AV62ddo_Lote_UserNomTitleControlIdToReplace, AV66ddo_Lote_ValorPFTitleControlIdToReplace, AV72ddo_Lote_DataIniTitleControlIdToReplace, AV78ddo_Lote_DataFimTitleControlIdToReplace, AV82ddo_Lote_QtdeDmnTitleControlIdToReplace, AV86ddo_Lote_ValorTitleControlIdToReplace, AV34Lote_AreaTrabalhoCod, AV36Lote_ContratadaCod, AV94Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
      }

      protected void E25BR2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Lote_Nome1, AV18Lote_UserNom1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Lote_Nome2, AV23Lote_UserNom2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27Lote_Nome3, AV28Lote_UserNom3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV38TFLote_Numero, AV39TFLote_Numero_Sel, AV42TFLote_Nome, AV43TFLote_Nome_Sel, AV46TFLote_Data, AV47TFLote_Data_To, AV52TFLote_UserCod, AV53TFLote_UserCod_To, AV56TFLote_PessoaCod, AV57TFLote_PessoaCod_To, AV60TFLote_UserNom, AV61TFLote_UserNom_Sel, AV64TFLote_ValorPF, AV65TFLote_ValorPF_To, AV68TFLote_DataIni, AV69TFLote_DataIni_To, AV74TFLote_DataFim, AV75TFLote_DataFim_To, AV80TFLote_QtdeDmn, AV81TFLote_QtdeDmn_To, AV84TFLote_Valor, AV85TFLote_Valor_To, AV40ddo_Lote_NumeroTitleControlIdToReplace, AV44ddo_Lote_NomeTitleControlIdToReplace, AV50ddo_Lote_DataTitleControlIdToReplace, AV54ddo_Lote_UserCodTitleControlIdToReplace, AV58ddo_Lote_PessoaCodTitleControlIdToReplace, AV62ddo_Lote_UserNomTitleControlIdToReplace, AV66ddo_Lote_ValorPFTitleControlIdToReplace, AV72ddo_Lote_DataIniTitleControlIdToReplace, AV78ddo_Lote_DataFimTitleControlIdToReplace, AV82ddo_Lote_QtdeDmnTitleControlIdToReplace, AV86ddo_Lote_ValorTitleControlIdToReplace, AV34Lote_AreaTrabalhoCod, AV36Lote_ContratadaCod, AV94Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E31BR2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         AV21DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E26BR2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV24DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Lote_Nome1, AV18Lote_UserNom1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22Lote_Nome2, AV23Lote_UserNom2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27Lote_Nome3, AV28Lote_UserNom3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV38TFLote_Numero, AV39TFLote_Numero_Sel, AV42TFLote_Nome, AV43TFLote_Nome_Sel, AV46TFLote_Data, AV47TFLote_Data_To, AV52TFLote_UserCod, AV53TFLote_UserCod_To, AV56TFLote_PessoaCod, AV57TFLote_PessoaCod_To, AV60TFLote_UserNom, AV61TFLote_UserNom_Sel, AV64TFLote_ValorPF, AV65TFLote_ValorPF_To, AV68TFLote_DataIni, AV69TFLote_DataIni_To, AV74TFLote_DataFim, AV75TFLote_DataFim_To, AV80TFLote_QtdeDmn, AV81TFLote_QtdeDmn_To, AV84TFLote_Valor, AV85TFLote_Valor_To, AV40ddo_Lote_NumeroTitleControlIdToReplace, AV44ddo_Lote_NomeTitleControlIdToReplace, AV50ddo_Lote_DataTitleControlIdToReplace, AV54ddo_Lote_UserCodTitleControlIdToReplace, AV58ddo_Lote_PessoaCodTitleControlIdToReplace, AV62ddo_Lote_UserNomTitleControlIdToReplace, AV66ddo_Lote_ValorPFTitleControlIdToReplace, AV72ddo_Lote_DataIniTitleControlIdToReplace, AV78ddo_Lote_DataFimTitleControlIdToReplace, AV82ddo_Lote_QtdeDmnTitleControlIdToReplace, AV86ddo_Lote_ValorTitleControlIdToReplace, AV34Lote_AreaTrabalhoCod, AV36Lote_ContratadaCod, AV94Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E32BR2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         AV26DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E27BR2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_lote_numero_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_numero_Internalname, "SortedStatus", Ddo_lote_numero_Sortedstatus);
         Ddo_lote_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_nome_Internalname, "SortedStatus", Ddo_lote_nome_Sortedstatus);
         Ddo_lote_data_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_data_Internalname, "SortedStatus", Ddo_lote_data_Sortedstatus);
         Ddo_lote_usercod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_usercod_Internalname, "SortedStatus", Ddo_lote_usercod_Sortedstatus);
         Ddo_lote_pessoacod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_pessoacod_Internalname, "SortedStatus", Ddo_lote_pessoacod_Sortedstatus);
         Ddo_lote_usernom_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_usernom_Internalname, "SortedStatus", Ddo_lote_usernom_Sortedstatus);
         Ddo_lote_valorpf_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_valorpf_Internalname, "SortedStatus", Ddo_lote_valorpf_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_lote_numero_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_numero_Internalname, "SortedStatus", Ddo_lote_numero_Sortedstatus);
         }
         else if ( AV13OrderedBy == 1 )
         {
            Ddo_lote_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_nome_Internalname, "SortedStatus", Ddo_lote_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_lote_data_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_data_Internalname, "SortedStatus", Ddo_lote_data_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_lote_usercod_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_usercod_Internalname, "SortedStatus", Ddo_lote_usercod_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_lote_pessoacod_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_pessoacod_Internalname, "SortedStatus", Ddo_lote_pessoacod_Sortedstatus);
         }
         else if ( AV13OrderedBy == 6 )
         {
            Ddo_lote_usernom_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_usernom_Internalname, "SortedStatus", Ddo_lote_usernom_Sortedstatus);
         }
         else if ( AV13OrderedBy == 7 )
         {
            Ddo_lote_valorpf_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_valorpf_Internalname, "SortedStatus", Ddo_lote_valorpf_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavLote_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_nome1_Visible), 5, 0)));
         edtavLote_usernom1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_usernom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_usernom1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "LOTE_NOME") == 0 )
         {
            edtavLote_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_nome1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "LOTE_USERNOM") == 0 )
         {
            edtavLote_usernom1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_usernom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_usernom1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavLote_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_nome2_Visible), 5, 0)));
         edtavLote_usernom2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_usernom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_usernom2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "LOTE_NOME") == 0 )
         {
            edtavLote_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_nome2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "LOTE_USERNOM") == 0 )
         {
            edtavLote_usernom2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_usernom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_usernom2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavLote_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_nome3_Visible), 5, 0)));
         edtavLote_usernom3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_usernom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_usernom3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "LOTE_NOME") == 0 )
         {
            edtavLote_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_nome3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "LOTE_USERNOM") == 0 )
         {
            edtavLote_usernom3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLote_usernom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLote_usernom3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         AV20DynamicFiltersSelector2 = "LOTE_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         AV21DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         AV22Lote_Nome2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Lote_Nome2", AV22Lote_Nome2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV24DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         AV25DynamicFiltersSelector3 = "LOTE_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         AV26DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
         AV27Lote_Nome3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Lote_Nome3", AV27Lote_Nome3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S202( )
      {
         /* 'CLEANFILTERS' Routine */
         AV34Lote_AreaTrabalhoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Lote_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Lote_AreaTrabalhoCod), 6, 0)));
         AV36Lote_ContratadaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36Lote_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36Lote_ContratadaCod), 6, 0)));
         AV38TFLote_Numero = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFLote_Numero", AV38TFLote_Numero);
         Ddo_lote_numero_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_numero_Internalname, "FilteredText_set", Ddo_lote_numero_Filteredtext_set);
         AV39TFLote_Numero_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFLote_Numero_Sel", AV39TFLote_Numero_Sel);
         Ddo_lote_numero_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_numero_Internalname, "SelectedValue_set", Ddo_lote_numero_Selectedvalue_set);
         AV42TFLote_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFLote_Nome", AV42TFLote_Nome);
         Ddo_lote_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_nome_Internalname, "FilteredText_set", Ddo_lote_nome_Filteredtext_set);
         AV43TFLote_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFLote_Nome_Sel", AV43TFLote_Nome_Sel);
         Ddo_lote_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_nome_Internalname, "SelectedValue_set", Ddo_lote_nome_Selectedvalue_set);
         AV46TFLote_Data = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFLote_Data", context.localUtil.TToC( AV46TFLote_Data, 8, 5, 0, 3, "/", ":", " "));
         Ddo_lote_data_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_data_Internalname, "FilteredText_set", Ddo_lote_data_Filteredtext_set);
         AV47TFLote_Data_To = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFLote_Data_To", context.localUtil.TToC( AV47TFLote_Data_To, 8, 5, 0, 3, "/", ":", " "));
         Ddo_lote_data_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_data_Internalname, "FilteredTextTo_set", Ddo_lote_data_Filteredtextto_set);
         AV52TFLote_UserCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFLote_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52TFLote_UserCod), 6, 0)));
         Ddo_lote_usercod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_usercod_Internalname, "FilteredText_set", Ddo_lote_usercod_Filteredtext_set);
         AV53TFLote_UserCod_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFLote_UserCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53TFLote_UserCod_To), 6, 0)));
         Ddo_lote_usercod_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_usercod_Internalname, "FilteredTextTo_set", Ddo_lote_usercod_Filteredtextto_set);
         AV56TFLote_PessoaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFLote_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56TFLote_PessoaCod), 6, 0)));
         Ddo_lote_pessoacod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_pessoacod_Internalname, "FilteredText_set", Ddo_lote_pessoacod_Filteredtext_set);
         AV57TFLote_PessoaCod_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFLote_PessoaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV57TFLote_PessoaCod_To), 6, 0)));
         Ddo_lote_pessoacod_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_pessoacod_Internalname, "FilteredTextTo_set", Ddo_lote_pessoacod_Filteredtextto_set);
         AV60TFLote_UserNom = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFLote_UserNom", AV60TFLote_UserNom);
         Ddo_lote_usernom_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_usernom_Internalname, "FilteredText_set", Ddo_lote_usernom_Filteredtext_set);
         AV61TFLote_UserNom_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFLote_UserNom_Sel", AV61TFLote_UserNom_Sel);
         Ddo_lote_usernom_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_usernom_Internalname, "SelectedValue_set", Ddo_lote_usernom_Selectedvalue_set);
         AV64TFLote_ValorPF = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFLote_ValorPF", StringUtil.LTrim( StringUtil.Str( AV64TFLote_ValorPF, 18, 5)));
         Ddo_lote_valorpf_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_valorpf_Internalname, "FilteredText_set", Ddo_lote_valorpf_Filteredtext_set);
         AV65TFLote_ValorPF_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFLote_ValorPF_To", StringUtil.LTrim( StringUtil.Str( AV65TFLote_ValorPF_To, 18, 5)));
         Ddo_lote_valorpf_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_valorpf_Internalname, "FilteredTextTo_set", Ddo_lote_valorpf_Filteredtextto_set);
         AV68TFLote_DataIni = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFLote_DataIni", context.localUtil.Format(AV68TFLote_DataIni, "99/99/99"));
         Ddo_lote_dataini_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_dataini_Internalname, "FilteredText_set", Ddo_lote_dataini_Filteredtext_set);
         AV69TFLote_DataIni_To = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFLote_DataIni_To", context.localUtil.Format(AV69TFLote_DataIni_To, "99/99/99"));
         Ddo_lote_dataini_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_dataini_Internalname, "FilteredTextTo_set", Ddo_lote_dataini_Filteredtextto_set);
         AV74TFLote_DataFim = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFLote_DataFim", context.localUtil.Format(AV74TFLote_DataFim, "99/99/99"));
         Ddo_lote_datafim_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_datafim_Internalname, "FilteredText_set", Ddo_lote_datafim_Filteredtext_set);
         AV75TFLote_DataFim_To = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFLote_DataFim_To", context.localUtil.Format(AV75TFLote_DataFim_To, "99/99/99"));
         Ddo_lote_datafim_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_datafim_Internalname, "FilteredTextTo_set", Ddo_lote_datafim_Filteredtextto_set);
         AV80TFLote_QtdeDmn = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFLote_QtdeDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV80TFLote_QtdeDmn), 4, 0)));
         Ddo_lote_qtdedmn_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_qtdedmn_Internalname, "FilteredText_set", Ddo_lote_qtdedmn_Filteredtext_set);
         AV81TFLote_QtdeDmn_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFLote_QtdeDmn_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV81TFLote_QtdeDmn_To), 4, 0)));
         Ddo_lote_qtdedmn_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_qtdedmn_Internalname, "FilteredTextTo_set", Ddo_lote_qtdedmn_Filteredtextto_set);
         AV84TFLote_Valor = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFLote_Valor", StringUtil.LTrim( StringUtil.Str( AV84TFLote_Valor, 18, 5)));
         Ddo_lote_valor_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_valor_Internalname, "FilteredText_set", Ddo_lote_valor_Filteredtext_set);
         AV85TFLote_Valor_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85TFLote_Valor_To", StringUtil.LTrim( StringUtil.Str( AV85TFLote_Valor_To, 18, 5)));
         Ddo_lote_valor_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lote_valor_Internalname, "FilteredTextTo_set", Ddo_lote_valor_Filteredtextto_set);
         AV15DynamicFiltersSelector1 = "LOTE_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         AV17Lote_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Lote_Nome1", AV17Lote_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "LOTE_NOME") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17Lote_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Lote_Nome1", AV17Lote_Nome1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "LOTE_USERNOM") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV18Lote_UserNom1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Lote_UserNom1", AV18Lote_UserNom1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV19DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV20DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "LOTE_NOME") == 0 )
               {
                  AV21DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
                  AV22Lote_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Lote_Nome2", AV22Lote_Nome2);
               }
               else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "LOTE_USERNOM") == 0 )
               {
                  AV21DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
                  AV23Lote_UserNom2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Lote_UserNom2", AV23Lote_UserNom2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV24DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV25DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "LOTE_NOME") == 0 )
                  {
                     AV26DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
                     AV27Lote_Nome3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Lote_Nome3", AV27Lote_Nome3);
                  }
                  else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "LOTE_USERNOM") == 0 )
                  {
                     AV26DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
                     AV28Lote_UserNom3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Lote_UserNom3", AV28Lote_UserNom3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV29DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! (0==AV34Lote_AreaTrabalhoCod) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "LOTE_AREATRABALHOCOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV34Lote_AreaTrabalhoCod), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV36Lote_ContratadaCod) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "LOTE_CONTRATADACOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV36Lote_ContratadaCod), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38TFLote_Numero)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFLOTE_NUMERO";
            AV11GridStateFilterValue.gxTpr_Value = AV38TFLote_Numero;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFLote_Numero_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFLOTE_NUMERO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV39TFLote_Numero_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42TFLote_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFLOTE_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV42TFLote_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TFLote_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFLOTE_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV43TFLote_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV46TFLote_Data) && (DateTime.MinValue==AV47TFLote_Data_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFLOTE_DATA";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.TToC( AV46TFLote_Data, 8, 5, 0, 3, "/", ":", " ");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.TToC( AV47TFLote_Data_To, 8, 5, 0, 3, "/", ":", " ");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV52TFLote_UserCod) && (0==AV53TFLote_UserCod_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFLOTE_USERCOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV52TFLote_UserCod), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV53TFLote_UserCod_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV56TFLote_PessoaCod) && (0==AV57TFLote_PessoaCod_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFLOTE_PESSOACOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV56TFLote_PessoaCod), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV57TFLote_PessoaCod_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60TFLote_UserNom)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFLOTE_USERNOM";
            AV11GridStateFilterValue.gxTpr_Value = AV60TFLote_UserNom;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61TFLote_UserNom_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFLOTE_USERNOM_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV61TFLote_UserNom_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV64TFLote_ValorPF) && (Convert.ToDecimal(0)==AV65TFLote_ValorPF_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFLOTE_VALORPF";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV64TFLote_ValorPF, 18, 5);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV65TFLote_ValorPF_To, 18, 5);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV68TFLote_DataIni) && (DateTime.MinValue==AV69TFLote_DataIni_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFLOTE_DATAINI";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV68TFLote_DataIni, 2, "/");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV69TFLote_DataIni_To, 2, "/");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV74TFLote_DataFim) && (DateTime.MinValue==AV75TFLote_DataFim_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFLOTE_DATAFIM";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV74TFLote_DataFim, 2, "/");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV75TFLote_DataFim_To, 2, "/");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV80TFLote_QtdeDmn) && (0==AV81TFLote_QtdeDmn_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFLOTE_QTDEDMN";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV80TFLote_QtdeDmn), 4, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV81TFLote_QtdeDmn_To), 4, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV84TFLote_Valor) && (Convert.ToDecimal(0)==AV85TFLote_Valor_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFLOTE_VALOR";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV84TFLote_Valor, 18, 5);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV85TFLote_Valor_To, 18, 5);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV94Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV30DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "LOTE_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Lote_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17Lote_Nome1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "LOTE_USERNOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Lote_UserNom1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV18Lote_UserNom1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV19DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV20DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "LOTE_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV22Lote_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV22Lote_Nome2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV21DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "LOTE_USERNOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV23Lote_UserNom2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV23Lote_UserNom2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV21DynamicFiltersOperator2;
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV24DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV25DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "LOTE_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV27Lote_Nome3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV27Lote_Nome3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV26DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "LOTE_USERNOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV28Lote_UserNom3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV28Lote_UserNom3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV26DynamicFiltersOperator3;
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_BR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_BR2( true) ;
         }
         else
         {
            wb_table2_5_BR2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_BR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_88_BR2( true) ;
         }
         else
         {
            wb_table3_88_BR2( false) ;
         }
         return  ;
      }

      protected void wb_table3_88_BR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_BR2e( true) ;
         }
         else
         {
            wb_table1_2_BR2e( false) ;
         }
      }

      protected void wb_table3_88_BR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_91_BR2( true) ;
         }
         else
         {
            wb_table4_91_BR2( false) ;
         }
         return  ;
      }

      protected void wb_table4_91_BR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_88_BR2e( true) ;
         }
         else
         {
            wb_table3_88_BR2e( false) ;
         }
      }

      protected void wb_table4_91_BR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"94\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtLote_Numero_Titleformat == 0 )
               {
                  context.SendWebValue( edtLote_Numero_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtLote_Numero_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtLote_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtLote_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtLote_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtLote_Data_Titleformat == 0 )
               {
                  context.SendWebValue( edtLote_Data_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtLote_Data_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtLote_UserCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtLote_UserCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtLote_UserCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtLote_PessoaCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtLote_PessoaCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtLote_PessoaCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtLote_UserNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtLote_UserNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtLote_UserNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtLote_ValorPF_Titleformat == 0 )
               {
                  context.SendWebValue( edtLote_ValorPF_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtLote_ValorPF_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtLote_DataIni_Titleformat == 0 )
               {
                  context.SendWebValue( edtLote_DataIni_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtLote_DataIni_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtLote_DataFim_Titleformat == 0 )
               {
                  context.SendWebValue( edtLote_DataFim_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtLote_DataFim_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtLote_QtdeDmn_Titleformat == 0 )
               {
                  context.SendWebValue( edtLote_QtdeDmn_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtLote_QtdeDmn_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtLote_Valor_Titleformat == 0 )
               {
                  context.SendWebValue( edtLote_Valor_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtLote_Valor_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV31Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A562Lote_Numero));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtLote_Numero_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtLote_Numero_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A563Lote_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtLote_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtLote_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( A564Lote_Data, 10, 8, 0, 3, "/", ":", " "));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtLote_Data_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtLote_Data_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A559Lote_UserCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtLote_UserCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtLote_UserCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A560Lote_PessoaCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtLote_PessoaCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtLote_PessoaCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A561Lote_UserNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtLote_UserNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtLote_UserNom_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A565Lote_ValorPF, 18, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtLote_ValorPF_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtLote_ValorPF_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A567Lote_DataIni, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtLote_DataIni_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtLote_DataIni_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A568Lote_DataFim, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtLote_DataFim_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtLote_DataFim_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A569Lote_QtdeDmn), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtLote_QtdeDmn_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtLote_QtdeDmn_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A572Lote_Valor, 18, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtLote_Valor_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtLote_Valor_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 94 )
         {
            wbEnd = 0;
            nRC_GXsfl_94 = (short)(nGXsfl_94_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_91_BR2e( true) ;
         }
         else
         {
            wb_table4_91_BR2e( false) ;
         }
      }

      protected void wb_table2_5_BR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptLote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_94_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptLote.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptLote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_14_BR2( true) ;
         }
         else
         {
            wb_table5_14_BR2( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_BR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_BR2e( true) ;
         }
         else
         {
            wb_table2_5_BR2e( false) ;
         }
      }

      protected void wb_table5_14_BR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptLote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextlote_areatrabalhocod_Internalname, "Area de Trabalho", "", "", lblFiltertextlote_areatrabalhocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptLote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLote_areatrabalhocod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV34Lote_AreaTrabalhoCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV34Lote_AreaTrabalhoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_areatrabalhocod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptLote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextlote_contratadacod_Internalname, "", "", "", lblFiltertextlote_contratadacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptLote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLote_contratadacod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36Lote_ContratadaCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV36Lote_ContratadaCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,27);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_contratadacod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptLote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_29_BR2( true) ;
         }
         else
         {
            wb_table6_29_BR2( false) ;
         }
         return  ;
      }

      protected void wb_table6_29_BR2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptLote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_BR2e( true) ;
         }
         else
         {
            wb_table5_14_BR2e( false) ;
         }
      }

      protected void wb_table6_29_BR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptLote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'" + sGXsfl_94_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,34);\"", "", true, "HLP_PromptLote.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptLote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_38_BR2( true) ;
         }
         else
         {
            wb_table7_38_BR2( false) ;
         }
         return  ;
      }

      protected void wb_table7_38_BR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptLote.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptLote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptLote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'" + sGXsfl_94_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV20DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,52);\"", "", true, "HLP_PromptLote.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptLote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_56_BR2( true) ;
         }
         else
         {
            wb_table8_56_BR2( false) ;
         }
         return  ;
      }

      protected void wb_table8_56_BR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptLote.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptLote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptLote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'',false,'" + sGXsfl_94_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV25DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,70);\"", "", true, "HLP_PromptLote.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptLote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_74_BR2( true) ;
         }
         else
         {
            wb_table9_74_BR2( false) ;
         }
         return  ;
      }

      protected void wb_table9_74_BR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptLote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_29_BR2e( true) ;
         }
         else
         {
            wb_table6_29_BR2e( false) ;
         }
      }

      protected void wb_table9_74_BR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'',false,'" + sGXsfl_94_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,77);\"", "", true, "HLP_PromptLote.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLote_nome3_Internalname, StringUtil.RTrim( AV27Lote_Nome3), StringUtil.RTrim( context.localUtil.Format( AV27Lote_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,79);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavLote_nome3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptLote.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLote_usernom3_Internalname, StringUtil.RTrim( AV28Lote_UserNom3), StringUtil.RTrim( context.localUtil.Format( AV28Lote_UserNom3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,80);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_usernom3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavLote_usernom3_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptLote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_74_BR2e( true) ;
         }
         else
         {
            wb_table9_74_BR2e( false) ;
         }
      }

      protected void wb_table8_56_BR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'" + sGXsfl_94_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", "", true, "HLP_PromptLote.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLote_nome2_Internalname, StringUtil.RTrim( AV22Lote_Nome2), StringUtil.RTrim( context.localUtil.Format( AV22Lote_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,61);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavLote_nome2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptLote.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLote_usernom2_Internalname, StringUtil.RTrim( AV23Lote_UserNom2), StringUtil.RTrim( context.localUtil.Format( AV23Lote_UserNom2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,62);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_usernom2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavLote_usernom2_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptLote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_56_BR2e( true) ;
         }
         else
         {
            wb_table8_56_BR2e( false) ;
         }
      }

      protected void wb_table7_38_BR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'" + sGXsfl_94_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,41);\"", "", true, "HLP_PromptLote.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLote_nome1_Internalname, StringUtil.RTrim( AV17Lote_Nome1), StringUtil.RTrim( context.localUtil.Format( AV17Lote_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,43);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavLote_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptLote.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLote_usernom1_Internalname, StringUtil.RTrim( AV18Lote_UserNom1), StringUtil.RTrim( context.localUtil.Format( AV18Lote_UserNom1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLote_usernom1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavLote_usernom1_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptLote.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_38_BR2e( true) ;
         }
         else
         {
            wb_table7_38_BR2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV35InOutLote_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35InOutLote_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35InOutLote_Codigo), 6, 0)));
         AV8InOutLote_Nome = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutLote_Nome", AV8InOutLote_Nome);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PABR2( ) ;
         WSBR2( ) ;
         WEBR2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202032423425666");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptlote.js", "?202032423425666");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_942( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_94_idx;
         edtLote_Numero_Internalname = "LOTE_NUMERO_"+sGXsfl_94_idx;
         edtLote_Nome_Internalname = "LOTE_NOME_"+sGXsfl_94_idx;
         edtLote_Data_Internalname = "LOTE_DATA_"+sGXsfl_94_idx;
         edtLote_UserCod_Internalname = "LOTE_USERCOD_"+sGXsfl_94_idx;
         edtLote_PessoaCod_Internalname = "LOTE_PESSOACOD_"+sGXsfl_94_idx;
         edtLote_UserNom_Internalname = "LOTE_USERNOM_"+sGXsfl_94_idx;
         edtLote_ValorPF_Internalname = "LOTE_VALORPF_"+sGXsfl_94_idx;
         edtLote_DataIni_Internalname = "LOTE_DATAINI_"+sGXsfl_94_idx;
         edtLote_DataFim_Internalname = "LOTE_DATAFIM_"+sGXsfl_94_idx;
         edtLote_QtdeDmn_Internalname = "LOTE_QTDEDMN_"+sGXsfl_94_idx;
         edtLote_Valor_Internalname = "LOTE_VALOR_"+sGXsfl_94_idx;
      }

      protected void SubsflControlProps_fel_942( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_94_fel_idx;
         edtLote_Numero_Internalname = "LOTE_NUMERO_"+sGXsfl_94_fel_idx;
         edtLote_Nome_Internalname = "LOTE_NOME_"+sGXsfl_94_fel_idx;
         edtLote_Data_Internalname = "LOTE_DATA_"+sGXsfl_94_fel_idx;
         edtLote_UserCod_Internalname = "LOTE_USERCOD_"+sGXsfl_94_fel_idx;
         edtLote_PessoaCod_Internalname = "LOTE_PESSOACOD_"+sGXsfl_94_fel_idx;
         edtLote_UserNom_Internalname = "LOTE_USERNOM_"+sGXsfl_94_fel_idx;
         edtLote_ValorPF_Internalname = "LOTE_VALORPF_"+sGXsfl_94_fel_idx;
         edtLote_DataIni_Internalname = "LOTE_DATAINI_"+sGXsfl_94_fel_idx;
         edtLote_DataFim_Internalname = "LOTE_DATAFIM_"+sGXsfl_94_fel_idx;
         edtLote_QtdeDmn_Internalname = "LOTE_QTDEDMN_"+sGXsfl_94_fel_idx;
         edtLote_Valor_Internalname = "LOTE_VALOR_"+sGXsfl_94_fel_idx;
      }

      protected void sendrow_942( )
      {
         SubsflControlProps_942( ) ;
         WBBR0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_94_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_94_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_94_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 95,'',false,'',94)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV31Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV31Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV93Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV31Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV31Select)) ? AV93Select_GXI : context.PathToRelativeUrl( AV31Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_94_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV31Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLote_Numero_Internalname,StringUtil.RTrim( A562Lote_Numero),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLote_Numero_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)10,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLote_Nome_Internalname,StringUtil.RTrim( A563Lote_Nome),StringUtil.RTrim( context.localUtil.Format( A563Lote_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLote_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLote_Data_Internalname,context.localUtil.TToC( A564Lote_Data, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A564Lote_Data, "99/99/99 99:99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLote_Data_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLote_UserCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A559Lote_UserCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A559Lote_UserCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLote_UserCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLote_PessoaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A560Lote_PessoaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A560Lote_PessoaCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLote_PessoaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLote_UserNom_Internalname,StringUtil.RTrim( A561Lote_UserNom),StringUtil.RTrim( context.localUtil.Format( A561Lote_UserNom, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLote_UserNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLote_ValorPF_Internalname,StringUtil.LTrim( StringUtil.NToC( A565Lote_ValorPF, 18, 5, ",", "")),context.localUtil.Format( A565Lote_ValorPF, "ZZZ,ZZZ,ZZZ,ZZ9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLote_ValorPF_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLote_DataIni_Internalname,context.localUtil.Format(A567Lote_DataIni, "99/99/99"),context.localUtil.Format( A567Lote_DataIni, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLote_DataIni_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLote_DataFim_Internalname,context.localUtil.Format(A568Lote_DataFim, "99/99/99"),context.localUtil.Format( A568Lote_DataFim, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLote_DataFim_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLote_QtdeDmn_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A569Lote_QtdeDmn), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A569Lote_QtdeDmn), "ZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLote_QtdeDmn_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLote_Valor_Internalname,StringUtil.LTrim( StringUtil.NToC( A572Lote_Valor, 18, 5, ",", "")),context.localUtil.Format( A572Lote_Valor, "ZZ,ZZZ,ZZZ,ZZ9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLote_Valor_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor3Casas",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_LOTE_NUMERO"+"_"+sGXsfl_94_idx, GetSecureSignedToken( sGXsfl_94_idx, StringUtil.RTrim( context.localUtil.Format( A562Lote_Numero, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_LOTE_NOME"+"_"+sGXsfl_94_idx, GetSecureSignedToken( sGXsfl_94_idx, StringUtil.RTrim( context.localUtil.Format( A563Lote_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_LOTE_DATA"+"_"+sGXsfl_94_idx, GetSecureSignedToken( sGXsfl_94_idx, context.localUtil.Format( A564Lote_Data, "99/99/99 99:99")));
            GxWebStd.gx_hidden_field( context, "gxhash_LOTE_USERCOD"+"_"+sGXsfl_94_idx, GetSecureSignedToken( sGXsfl_94_idx, context.localUtil.Format( (decimal)(A559Lote_UserCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_LOTE_VALORPF"+"_"+sGXsfl_94_idx, GetSecureSignedToken( sGXsfl_94_idx, context.localUtil.Format( A565Lote_ValorPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            GxWebStd.gx_hidden_field( context, "gxhash_LOTE_VALOR"+"_"+sGXsfl_94_idx, GetSecureSignedToken( sGXsfl_94_idx, context.localUtil.Format( A572Lote_Valor, "ZZ,ZZZ,ZZZ,ZZ9.99")));
            GridContainer.AddRow(GridRow);
            nGXsfl_94_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_94_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_94_idx+1));
            sGXsfl_94_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_94_idx), 4, 0)), 4, "0");
            SubsflControlProps_942( ) ;
         }
         /* End function sendrow_942 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblFiltertextlote_areatrabalhocod_Internalname = "FILTERTEXTLOTE_AREATRABALHOCOD";
         edtavLote_areatrabalhocod_Internalname = "vLOTE_AREATRABALHOCOD";
         lblFiltertextlote_contratadacod_Internalname = "FILTERTEXTLOTE_CONTRATADACOD";
         edtavLote_contratadacod_Internalname = "vLOTE_CONTRATADACOD";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavLote_nome1_Internalname = "vLOTE_NOME1";
         edtavLote_usernom1_Internalname = "vLOTE_USERNOM1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavLote_nome2_Internalname = "vLOTE_NOME2";
         edtavLote_usernom2_Internalname = "vLOTE_USERNOM2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavLote_nome3_Internalname = "vLOTE_NOME3";
         edtavLote_usernom3_Internalname = "vLOTE_USERNOM3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtLote_Numero_Internalname = "LOTE_NUMERO";
         edtLote_Nome_Internalname = "LOTE_NOME";
         edtLote_Data_Internalname = "LOTE_DATA";
         edtLote_UserCod_Internalname = "LOTE_USERCOD";
         edtLote_PessoaCod_Internalname = "LOTE_PESSOACOD";
         edtLote_UserNom_Internalname = "LOTE_USERNOM";
         edtLote_ValorPF_Internalname = "LOTE_VALORPF";
         edtLote_DataIni_Internalname = "LOTE_DATAINI";
         edtLote_DataFim_Internalname = "LOTE_DATAFIM";
         edtLote_QtdeDmn_Internalname = "LOTE_QTDEDMN";
         edtLote_Valor_Internalname = "LOTE_VALOR";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         edtLote_Codigo_Internalname = "LOTE_CODIGO";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTflote_numero_Internalname = "vTFLOTE_NUMERO";
         edtavTflote_numero_sel_Internalname = "vTFLOTE_NUMERO_SEL";
         edtavTflote_nome_Internalname = "vTFLOTE_NOME";
         edtavTflote_nome_sel_Internalname = "vTFLOTE_NOME_SEL";
         edtavTflote_data_Internalname = "vTFLOTE_DATA";
         edtavTflote_data_to_Internalname = "vTFLOTE_DATA_TO";
         edtavDdo_lote_dataauxdate_Internalname = "vDDO_LOTE_DATAAUXDATE";
         edtavDdo_lote_dataauxdateto_Internalname = "vDDO_LOTE_DATAAUXDATETO";
         divDdo_lote_dataauxdates_Internalname = "DDO_LOTE_DATAAUXDATES";
         edtavTflote_usercod_Internalname = "vTFLOTE_USERCOD";
         edtavTflote_usercod_to_Internalname = "vTFLOTE_USERCOD_TO";
         edtavTflote_pessoacod_Internalname = "vTFLOTE_PESSOACOD";
         edtavTflote_pessoacod_to_Internalname = "vTFLOTE_PESSOACOD_TO";
         edtavTflote_usernom_Internalname = "vTFLOTE_USERNOM";
         edtavTflote_usernom_sel_Internalname = "vTFLOTE_USERNOM_SEL";
         edtavTflote_valorpf_Internalname = "vTFLOTE_VALORPF";
         edtavTflote_valorpf_to_Internalname = "vTFLOTE_VALORPF_TO";
         edtavTflote_dataini_Internalname = "vTFLOTE_DATAINI";
         edtavTflote_dataini_to_Internalname = "vTFLOTE_DATAINI_TO";
         edtavDdo_lote_datainiauxdate_Internalname = "vDDO_LOTE_DATAINIAUXDATE";
         edtavDdo_lote_datainiauxdateto_Internalname = "vDDO_LOTE_DATAINIAUXDATETO";
         divDdo_lote_datainiauxdates_Internalname = "DDO_LOTE_DATAINIAUXDATES";
         edtavTflote_datafim_Internalname = "vTFLOTE_DATAFIM";
         edtavTflote_datafim_to_Internalname = "vTFLOTE_DATAFIM_TO";
         edtavDdo_lote_datafimauxdate_Internalname = "vDDO_LOTE_DATAFIMAUXDATE";
         edtavDdo_lote_datafimauxdateto_Internalname = "vDDO_LOTE_DATAFIMAUXDATETO";
         divDdo_lote_datafimauxdates_Internalname = "DDO_LOTE_DATAFIMAUXDATES";
         edtavTflote_qtdedmn_Internalname = "vTFLOTE_QTDEDMN";
         edtavTflote_qtdedmn_to_Internalname = "vTFLOTE_QTDEDMN_TO";
         edtavTflote_valor_Internalname = "vTFLOTE_VALOR";
         edtavTflote_valor_to_Internalname = "vTFLOTE_VALOR_TO";
         Ddo_lote_numero_Internalname = "DDO_LOTE_NUMERO";
         edtavDdo_lote_numerotitlecontrolidtoreplace_Internalname = "vDDO_LOTE_NUMEROTITLECONTROLIDTOREPLACE";
         Ddo_lote_nome_Internalname = "DDO_LOTE_NOME";
         edtavDdo_lote_nometitlecontrolidtoreplace_Internalname = "vDDO_LOTE_NOMETITLECONTROLIDTOREPLACE";
         Ddo_lote_data_Internalname = "DDO_LOTE_DATA";
         edtavDdo_lote_datatitlecontrolidtoreplace_Internalname = "vDDO_LOTE_DATATITLECONTROLIDTOREPLACE";
         Ddo_lote_usercod_Internalname = "DDO_LOTE_USERCOD";
         edtavDdo_lote_usercodtitlecontrolidtoreplace_Internalname = "vDDO_LOTE_USERCODTITLECONTROLIDTOREPLACE";
         Ddo_lote_pessoacod_Internalname = "DDO_LOTE_PESSOACOD";
         edtavDdo_lote_pessoacodtitlecontrolidtoreplace_Internalname = "vDDO_LOTE_PESSOACODTITLECONTROLIDTOREPLACE";
         Ddo_lote_usernom_Internalname = "DDO_LOTE_USERNOM";
         edtavDdo_lote_usernomtitlecontrolidtoreplace_Internalname = "vDDO_LOTE_USERNOMTITLECONTROLIDTOREPLACE";
         Ddo_lote_valorpf_Internalname = "DDO_LOTE_VALORPF";
         edtavDdo_lote_valorpftitlecontrolidtoreplace_Internalname = "vDDO_LOTE_VALORPFTITLECONTROLIDTOREPLACE";
         Ddo_lote_dataini_Internalname = "DDO_LOTE_DATAINI";
         edtavDdo_lote_datainititlecontrolidtoreplace_Internalname = "vDDO_LOTE_DATAINITITLECONTROLIDTOREPLACE";
         Ddo_lote_datafim_Internalname = "DDO_LOTE_DATAFIM";
         edtavDdo_lote_datafimtitlecontrolidtoreplace_Internalname = "vDDO_LOTE_DATAFIMTITLECONTROLIDTOREPLACE";
         Ddo_lote_qtdedmn_Internalname = "DDO_LOTE_QTDEDMN";
         edtavDdo_lote_qtdedmntitlecontrolidtoreplace_Internalname = "vDDO_LOTE_QTDEDMNTITLECONTROLIDTOREPLACE";
         Ddo_lote_valor_Internalname = "DDO_LOTE_VALOR";
         edtavDdo_lote_valortitlecontrolidtoreplace_Internalname = "vDDO_LOTE_VALORTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtLote_Valor_Jsonclick = "";
         edtLote_QtdeDmn_Jsonclick = "";
         edtLote_DataFim_Jsonclick = "";
         edtLote_DataIni_Jsonclick = "";
         edtLote_ValorPF_Jsonclick = "";
         edtLote_UserNom_Jsonclick = "";
         edtLote_PessoaCod_Jsonclick = "";
         edtLote_UserCod_Jsonclick = "";
         edtLote_Data_Jsonclick = "";
         edtLote_Nome_Jsonclick = "";
         edtLote_Numero_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavLote_usernom1_Jsonclick = "";
         edtavLote_nome1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavLote_usernom2_Jsonclick = "";
         edtavLote_nome2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavLote_usernom3_Jsonclick = "";
         edtavLote_nome3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavLote_contratadacod_Jsonclick = "";
         edtavLote_areatrabalhocod_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         edtLote_Valor_Titleformat = 0;
         edtLote_QtdeDmn_Titleformat = 0;
         edtLote_DataFim_Titleformat = 0;
         edtLote_DataIni_Titleformat = 0;
         edtLote_ValorPF_Titleformat = 0;
         edtLote_UserNom_Titleformat = 0;
         edtLote_PessoaCod_Titleformat = 0;
         edtLote_UserCod_Titleformat = 0;
         edtLote_Data_Titleformat = 0;
         edtLote_Nome_Titleformat = 0;
         edtLote_Numero_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavLote_usernom3_Visible = 1;
         edtavLote_nome3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavLote_usernom2_Visible = 1;
         edtavLote_nome2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavLote_usernom1_Visible = 1;
         edtavLote_nome1_Visible = 1;
         edtLote_Valor_Title = "R$";
         edtLote_QtdeDmn_Title = "Dmn";
         edtLote_DataFim_Title = "Fim";
         edtLote_DataIni_Title = "Ini";
         edtLote_ValorPF_Title = "PF R$";
         edtLote_UserNom_Title = "Usu�rio";
         edtLote_PessoaCod_Title = "Pessoa";
         edtLote_UserCod_Title = "Usu�rio";
         edtLote_Data_Title = "Data";
         edtLote_Nome_Title = "Nome";
         edtLote_Numero_Title = "Lote";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_lote_valortitlecontrolidtoreplace_Visible = 1;
         edtavDdo_lote_qtdedmntitlecontrolidtoreplace_Visible = 1;
         edtavDdo_lote_datafimtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_lote_datainititlecontrolidtoreplace_Visible = 1;
         edtavDdo_lote_valorpftitlecontrolidtoreplace_Visible = 1;
         edtavDdo_lote_usernomtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_lote_pessoacodtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_lote_usercodtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_lote_datatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_lote_nometitlecontrolidtoreplace_Visible = 1;
         edtavDdo_lote_numerotitlecontrolidtoreplace_Visible = 1;
         edtavTflote_valor_to_Jsonclick = "";
         edtavTflote_valor_to_Visible = 1;
         edtavTflote_valor_Jsonclick = "";
         edtavTflote_valor_Visible = 1;
         edtavTflote_qtdedmn_to_Jsonclick = "";
         edtavTflote_qtdedmn_to_Visible = 1;
         edtavTflote_qtdedmn_Jsonclick = "";
         edtavTflote_qtdedmn_Visible = 1;
         edtavDdo_lote_datafimauxdateto_Jsonclick = "";
         edtavDdo_lote_datafimauxdate_Jsonclick = "";
         edtavTflote_datafim_to_Jsonclick = "";
         edtavTflote_datafim_to_Visible = 1;
         edtavTflote_datafim_Jsonclick = "";
         edtavTflote_datafim_Visible = 1;
         edtavDdo_lote_datainiauxdateto_Jsonclick = "";
         edtavDdo_lote_datainiauxdate_Jsonclick = "";
         edtavTflote_dataini_to_Jsonclick = "";
         edtavTflote_dataini_to_Visible = 1;
         edtavTflote_dataini_Jsonclick = "";
         edtavTflote_dataini_Visible = 1;
         edtavTflote_valorpf_to_Jsonclick = "";
         edtavTflote_valorpf_to_Visible = 1;
         edtavTflote_valorpf_Jsonclick = "";
         edtavTflote_valorpf_Visible = 1;
         edtavTflote_usernom_sel_Jsonclick = "";
         edtavTflote_usernom_sel_Visible = 1;
         edtavTflote_usernom_Jsonclick = "";
         edtavTflote_usernom_Visible = 1;
         edtavTflote_pessoacod_to_Jsonclick = "";
         edtavTflote_pessoacod_to_Visible = 1;
         edtavTflote_pessoacod_Jsonclick = "";
         edtavTflote_pessoacod_Visible = 1;
         edtavTflote_usercod_to_Jsonclick = "";
         edtavTflote_usercod_to_Visible = 1;
         edtavTflote_usercod_Jsonclick = "";
         edtavTflote_usercod_Visible = 1;
         edtavDdo_lote_dataauxdateto_Jsonclick = "";
         edtavDdo_lote_dataauxdate_Jsonclick = "";
         edtavTflote_data_to_Jsonclick = "";
         edtavTflote_data_to_Visible = 1;
         edtavTflote_data_Jsonclick = "";
         edtavTflote_data_Visible = 1;
         edtavTflote_nome_sel_Jsonclick = "";
         edtavTflote_nome_sel_Visible = 1;
         edtavTflote_nome_Jsonclick = "";
         edtavTflote_nome_Visible = 1;
         edtavTflote_numero_sel_Jsonclick = "";
         edtavTflote_numero_sel_Visible = 1;
         edtavTflote_numero_Jsonclick = "";
         edtavTflote_numero_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         edtLote_Codigo_Jsonclick = "";
         edtLote_Codigo_Visible = 1;
         Ddo_lote_valor_Searchbuttontext = "Pesquisar";
         Ddo_lote_valor_Rangefilterto = "At�";
         Ddo_lote_valor_Rangefilterfrom = "Desde";
         Ddo_lote_valor_Cleanfilter = "Limpar pesquisa";
         Ddo_lote_valor_Includedatalist = Convert.ToBoolean( 0);
         Ddo_lote_valor_Filterisrange = Convert.ToBoolean( -1);
         Ddo_lote_valor_Filtertype = "Numeric";
         Ddo_lote_valor_Includefilter = Convert.ToBoolean( -1);
         Ddo_lote_valor_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_lote_valor_Includesortasc = Convert.ToBoolean( 0);
         Ddo_lote_valor_Titlecontrolidtoreplace = "";
         Ddo_lote_valor_Dropdownoptionstype = "GridTitleSettings";
         Ddo_lote_valor_Cls = "ColumnSettings";
         Ddo_lote_valor_Tooltip = "Op��es";
         Ddo_lote_valor_Caption = "";
         Ddo_lote_qtdedmn_Searchbuttontext = "Pesquisar";
         Ddo_lote_qtdedmn_Rangefilterto = "At�";
         Ddo_lote_qtdedmn_Rangefilterfrom = "Desde";
         Ddo_lote_qtdedmn_Cleanfilter = "Limpar pesquisa";
         Ddo_lote_qtdedmn_Includedatalist = Convert.ToBoolean( 0);
         Ddo_lote_qtdedmn_Filterisrange = Convert.ToBoolean( -1);
         Ddo_lote_qtdedmn_Filtertype = "Numeric";
         Ddo_lote_qtdedmn_Includefilter = Convert.ToBoolean( -1);
         Ddo_lote_qtdedmn_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_lote_qtdedmn_Includesortasc = Convert.ToBoolean( 0);
         Ddo_lote_qtdedmn_Titlecontrolidtoreplace = "";
         Ddo_lote_qtdedmn_Dropdownoptionstype = "GridTitleSettings";
         Ddo_lote_qtdedmn_Cls = "ColumnSettings";
         Ddo_lote_qtdedmn_Tooltip = "Op��es";
         Ddo_lote_qtdedmn_Caption = "";
         Ddo_lote_datafim_Searchbuttontext = "Pesquisar";
         Ddo_lote_datafim_Rangefilterto = "At�";
         Ddo_lote_datafim_Rangefilterfrom = "Desde";
         Ddo_lote_datafim_Cleanfilter = "Limpar pesquisa";
         Ddo_lote_datafim_Includedatalist = Convert.ToBoolean( 0);
         Ddo_lote_datafim_Filterisrange = Convert.ToBoolean( -1);
         Ddo_lote_datafim_Filtertype = "Date";
         Ddo_lote_datafim_Includefilter = Convert.ToBoolean( -1);
         Ddo_lote_datafim_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_lote_datafim_Includesortasc = Convert.ToBoolean( 0);
         Ddo_lote_datafim_Titlecontrolidtoreplace = "";
         Ddo_lote_datafim_Dropdownoptionstype = "GridTitleSettings";
         Ddo_lote_datafim_Cls = "ColumnSettings";
         Ddo_lote_datafim_Tooltip = "Op��es";
         Ddo_lote_datafim_Caption = "";
         Ddo_lote_dataini_Searchbuttontext = "Pesquisar";
         Ddo_lote_dataini_Rangefilterto = "At�";
         Ddo_lote_dataini_Rangefilterfrom = "Desde";
         Ddo_lote_dataini_Cleanfilter = "Limpar pesquisa";
         Ddo_lote_dataini_Includedatalist = Convert.ToBoolean( 0);
         Ddo_lote_dataini_Filterisrange = Convert.ToBoolean( -1);
         Ddo_lote_dataini_Filtertype = "Date";
         Ddo_lote_dataini_Includefilter = Convert.ToBoolean( -1);
         Ddo_lote_dataini_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_lote_dataini_Includesortasc = Convert.ToBoolean( 0);
         Ddo_lote_dataini_Titlecontrolidtoreplace = "";
         Ddo_lote_dataini_Dropdownoptionstype = "GridTitleSettings";
         Ddo_lote_dataini_Cls = "ColumnSettings";
         Ddo_lote_dataini_Tooltip = "Op��es";
         Ddo_lote_dataini_Caption = "";
         Ddo_lote_valorpf_Searchbuttontext = "Pesquisar";
         Ddo_lote_valorpf_Rangefilterto = "At�";
         Ddo_lote_valorpf_Rangefilterfrom = "Desde";
         Ddo_lote_valorpf_Cleanfilter = "Limpar pesquisa";
         Ddo_lote_valorpf_Sortdsc = "Ordenar de Z � A";
         Ddo_lote_valorpf_Sortasc = "Ordenar de A � Z";
         Ddo_lote_valorpf_Includedatalist = Convert.ToBoolean( 0);
         Ddo_lote_valorpf_Filterisrange = Convert.ToBoolean( -1);
         Ddo_lote_valorpf_Filtertype = "Numeric";
         Ddo_lote_valorpf_Includefilter = Convert.ToBoolean( -1);
         Ddo_lote_valorpf_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_lote_valorpf_Includesortasc = Convert.ToBoolean( -1);
         Ddo_lote_valorpf_Titlecontrolidtoreplace = "";
         Ddo_lote_valorpf_Dropdownoptionstype = "GridTitleSettings";
         Ddo_lote_valorpf_Cls = "ColumnSettings";
         Ddo_lote_valorpf_Tooltip = "Op��es";
         Ddo_lote_valorpf_Caption = "";
         Ddo_lote_usernom_Searchbuttontext = "Pesquisar";
         Ddo_lote_usernom_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_lote_usernom_Cleanfilter = "Limpar pesquisa";
         Ddo_lote_usernom_Loadingdata = "Carregando dados...";
         Ddo_lote_usernom_Sortdsc = "Ordenar de Z � A";
         Ddo_lote_usernom_Sortasc = "Ordenar de A � Z";
         Ddo_lote_usernom_Datalistupdateminimumcharacters = 0;
         Ddo_lote_usernom_Datalistproc = "GetPromptLoteFilterData";
         Ddo_lote_usernom_Datalisttype = "Dynamic";
         Ddo_lote_usernom_Includedatalist = Convert.ToBoolean( -1);
         Ddo_lote_usernom_Filterisrange = Convert.ToBoolean( 0);
         Ddo_lote_usernom_Filtertype = "Character";
         Ddo_lote_usernom_Includefilter = Convert.ToBoolean( -1);
         Ddo_lote_usernom_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_lote_usernom_Includesortasc = Convert.ToBoolean( -1);
         Ddo_lote_usernom_Titlecontrolidtoreplace = "";
         Ddo_lote_usernom_Dropdownoptionstype = "GridTitleSettings";
         Ddo_lote_usernom_Cls = "ColumnSettings";
         Ddo_lote_usernom_Tooltip = "Op��es";
         Ddo_lote_usernom_Caption = "";
         Ddo_lote_pessoacod_Searchbuttontext = "Pesquisar";
         Ddo_lote_pessoacod_Rangefilterto = "At�";
         Ddo_lote_pessoacod_Rangefilterfrom = "Desde";
         Ddo_lote_pessoacod_Cleanfilter = "Limpar pesquisa";
         Ddo_lote_pessoacod_Sortdsc = "Ordenar de Z � A";
         Ddo_lote_pessoacod_Sortasc = "Ordenar de A � Z";
         Ddo_lote_pessoacod_Includedatalist = Convert.ToBoolean( 0);
         Ddo_lote_pessoacod_Filterisrange = Convert.ToBoolean( -1);
         Ddo_lote_pessoacod_Filtertype = "Numeric";
         Ddo_lote_pessoacod_Includefilter = Convert.ToBoolean( -1);
         Ddo_lote_pessoacod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_lote_pessoacod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_lote_pessoacod_Titlecontrolidtoreplace = "";
         Ddo_lote_pessoacod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_lote_pessoacod_Cls = "ColumnSettings";
         Ddo_lote_pessoacod_Tooltip = "Op��es";
         Ddo_lote_pessoacod_Caption = "";
         Ddo_lote_usercod_Searchbuttontext = "Pesquisar";
         Ddo_lote_usercod_Rangefilterto = "At�";
         Ddo_lote_usercod_Rangefilterfrom = "Desde";
         Ddo_lote_usercod_Cleanfilter = "Limpar pesquisa";
         Ddo_lote_usercod_Sortdsc = "Ordenar de Z � A";
         Ddo_lote_usercod_Sortasc = "Ordenar de A � Z";
         Ddo_lote_usercod_Includedatalist = Convert.ToBoolean( 0);
         Ddo_lote_usercod_Filterisrange = Convert.ToBoolean( -1);
         Ddo_lote_usercod_Filtertype = "Numeric";
         Ddo_lote_usercod_Includefilter = Convert.ToBoolean( -1);
         Ddo_lote_usercod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_lote_usercod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_lote_usercod_Titlecontrolidtoreplace = "";
         Ddo_lote_usercod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_lote_usercod_Cls = "ColumnSettings";
         Ddo_lote_usercod_Tooltip = "Op��es";
         Ddo_lote_usercod_Caption = "";
         Ddo_lote_data_Searchbuttontext = "Pesquisar";
         Ddo_lote_data_Rangefilterto = "At�";
         Ddo_lote_data_Rangefilterfrom = "Desde";
         Ddo_lote_data_Cleanfilter = "Limpar pesquisa";
         Ddo_lote_data_Sortdsc = "Ordenar de Z � A";
         Ddo_lote_data_Sortasc = "Ordenar de A � Z";
         Ddo_lote_data_Includedatalist = Convert.ToBoolean( 0);
         Ddo_lote_data_Filterisrange = Convert.ToBoolean( -1);
         Ddo_lote_data_Filtertype = "Date";
         Ddo_lote_data_Includefilter = Convert.ToBoolean( -1);
         Ddo_lote_data_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_lote_data_Includesortasc = Convert.ToBoolean( -1);
         Ddo_lote_data_Titlecontrolidtoreplace = "";
         Ddo_lote_data_Dropdownoptionstype = "GridTitleSettings";
         Ddo_lote_data_Cls = "ColumnSettings";
         Ddo_lote_data_Tooltip = "Op��es";
         Ddo_lote_data_Caption = "";
         Ddo_lote_nome_Searchbuttontext = "Pesquisar";
         Ddo_lote_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_lote_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_lote_nome_Loadingdata = "Carregando dados...";
         Ddo_lote_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_lote_nome_Sortasc = "Ordenar de A � Z";
         Ddo_lote_nome_Datalistupdateminimumcharacters = 0;
         Ddo_lote_nome_Datalistproc = "GetPromptLoteFilterData";
         Ddo_lote_nome_Datalisttype = "Dynamic";
         Ddo_lote_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_lote_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_lote_nome_Filtertype = "Character";
         Ddo_lote_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_lote_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_lote_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_lote_nome_Titlecontrolidtoreplace = "";
         Ddo_lote_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_lote_nome_Cls = "ColumnSettings";
         Ddo_lote_nome_Tooltip = "Op��es";
         Ddo_lote_nome_Caption = "";
         Ddo_lote_numero_Searchbuttontext = "Pesquisar";
         Ddo_lote_numero_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_lote_numero_Cleanfilter = "Limpar pesquisa";
         Ddo_lote_numero_Loadingdata = "Carregando dados...";
         Ddo_lote_numero_Sortdsc = "Ordenar de Z � A";
         Ddo_lote_numero_Sortasc = "Ordenar de A � Z";
         Ddo_lote_numero_Datalistupdateminimumcharacters = 0;
         Ddo_lote_numero_Datalistproc = "GetPromptLoteFilterData";
         Ddo_lote_numero_Datalisttype = "Dynamic";
         Ddo_lote_numero_Includedatalist = Convert.ToBoolean( -1);
         Ddo_lote_numero_Filterisrange = Convert.ToBoolean( 0);
         Ddo_lote_numero_Filtertype = "Character";
         Ddo_lote_numero_Includefilter = Convert.ToBoolean( -1);
         Ddo_lote_numero_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_lote_numero_Includesortasc = Convert.ToBoolean( -1);
         Ddo_lote_numero_Titlecontrolidtoreplace = "";
         Ddo_lote_numero_Dropdownoptionstype = "GridTitleSettings";
         Ddo_lote_numero_Cls = "ColumnSettings";
         Ddo_lote_numero_Tooltip = "Op��es";
         Ddo_lote_numero_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Lote";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV40ddo_Lote_NumeroTitleControlIdToReplace',fld:'vDDO_LOTE_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_Lote_NomeTitleControlIdToReplace',fld:'vDDO_LOTE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Lote_DataTitleControlIdToReplace',fld:'vDDO_LOTE_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_Lote_UserCodTitleControlIdToReplace',fld:'vDDO_LOTE_USERCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Lote_PessoaCodTitleControlIdToReplace',fld:'vDDO_LOTE_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Lote_UserNomTitleControlIdToReplace',fld:'vDDO_LOTE_USERNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Lote_ValorPFTitleControlIdToReplace',fld:'vDDO_LOTE_VALORPFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Lote_DataIniTitleControlIdToReplace',fld:'vDDO_LOTE_DATAINITITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_Lote_DataFimTitleControlIdToReplace',fld:'vDDO_LOTE_DATAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_Lote_QtdeDmnTitleControlIdToReplace',fld:'vDDO_LOTE_QTDEDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_Lote_ValorTitleControlIdToReplace',fld:'vDDO_LOTE_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV34Lote_AreaTrabalhoCod',fld:'vLOTE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36Lote_ContratadaCod',fld:'vLOTE_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV38TFLote_Numero',fld:'vTFLOTE_NUMERO',pic:'',nv:''},{av:'AV39TFLote_Numero_Sel',fld:'vTFLOTE_NUMERO_SEL',pic:'',nv:''},{av:'AV42TFLote_Nome',fld:'vTFLOTE_NOME',pic:'@!',nv:''},{av:'AV43TFLote_Nome_Sel',fld:'vTFLOTE_NOME_SEL',pic:'@!',nv:''},{av:'AV46TFLote_Data',fld:'vTFLOTE_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLote_Data_To',fld:'vTFLOTE_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV52TFLote_UserCod',fld:'vTFLOTE_USERCOD',pic:'ZZZZZ9',nv:0},{av:'AV53TFLote_UserCod_To',fld:'vTFLOTE_USERCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV56TFLote_PessoaCod',fld:'vTFLOTE_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV57TFLote_PessoaCod_To',fld:'vTFLOTE_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV60TFLote_UserNom',fld:'vTFLOTE_USERNOM',pic:'@!',nv:''},{av:'AV61TFLote_UserNom_Sel',fld:'vTFLOTE_USERNOM_SEL',pic:'@!',nv:''},{av:'AV64TFLote_ValorPF',fld:'vTFLOTE_VALORPF',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFLote_ValorPF_To',fld:'vTFLOTE_VALORPF_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFLote_DataIni',fld:'vTFLOTE_DATAINI',pic:'',nv:''},{av:'AV69TFLote_DataIni_To',fld:'vTFLOTE_DATAINI_TO',pic:'',nv:''},{av:'AV74TFLote_DataFim',fld:'vTFLOTE_DATAFIM',pic:'',nv:''},{av:'AV75TFLote_DataFim_To',fld:'vTFLOTE_DATAFIM_TO',pic:'',nv:''},{av:'AV80TFLote_QtdeDmn',fld:'vTFLOTE_QTDEDMN',pic:'ZZZ9',nv:0},{av:'AV81TFLote_QtdeDmn_To',fld:'vTFLOTE_QTDEDMN_TO',pic:'ZZZ9',nv:0},{av:'AV84TFLote_Valor',fld:'vTFLOTE_VALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV85TFLote_Valor_To',fld:'vTFLOTE_VALOR_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV17Lote_Nome1',fld:'vLOTE_NOME1',pic:'@!',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Lote_UserNom1',fld:'vLOTE_USERNOM1',pic:'@!',nv:''},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22Lote_Nome2',fld:'vLOTE_NOME2',pic:'@!',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Lote_UserNom2',fld:'vLOTE_USERNOM2',pic:'@!',nv:''},{av:'AV27Lote_Nome3',fld:'vLOTE_NOME3',pic:'@!',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28Lote_UserNom3',fld:'vLOTE_USERNOM3',pic:'@!',nv:''}],oparms:[{av:'AV37Lote_NumeroTitleFilterData',fld:'vLOTE_NUMEROTITLEFILTERDATA',pic:'',nv:null},{av:'AV41Lote_NomeTitleFilterData',fld:'vLOTE_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV45Lote_DataTitleFilterData',fld:'vLOTE_DATATITLEFILTERDATA',pic:'',nv:null},{av:'AV51Lote_UserCodTitleFilterData',fld:'vLOTE_USERCODTITLEFILTERDATA',pic:'',nv:null},{av:'AV55Lote_PessoaCodTitleFilterData',fld:'vLOTE_PESSOACODTITLEFILTERDATA',pic:'',nv:null},{av:'AV59Lote_UserNomTitleFilterData',fld:'vLOTE_USERNOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV63Lote_ValorPFTitleFilterData',fld:'vLOTE_VALORPFTITLEFILTERDATA',pic:'',nv:null},{av:'AV67Lote_DataIniTitleFilterData',fld:'vLOTE_DATAINITITLEFILTERDATA',pic:'',nv:null},{av:'AV73Lote_DataFimTitleFilterData',fld:'vLOTE_DATAFIMTITLEFILTERDATA',pic:'',nv:null},{av:'AV79Lote_QtdeDmnTitleFilterData',fld:'vLOTE_QTDEDMNTITLEFILTERDATA',pic:'',nv:null},{av:'AV83Lote_ValorTitleFilterData',fld:'vLOTE_VALORTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtLote_Numero_Titleformat',ctrl:'LOTE_NUMERO',prop:'Titleformat'},{av:'edtLote_Numero_Title',ctrl:'LOTE_NUMERO',prop:'Title'},{av:'edtLote_Nome_Titleformat',ctrl:'LOTE_NOME',prop:'Titleformat'},{av:'edtLote_Nome_Title',ctrl:'LOTE_NOME',prop:'Title'},{av:'edtLote_Data_Titleformat',ctrl:'LOTE_DATA',prop:'Titleformat'},{av:'edtLote_Data_Title',ctrl:'LOTE_DATA',prop:'Title'},{av:'edtLote_UserCod_Titleformat',ctrl:'LOTE_USERCOD',prop:'Titleformat'},{av:'edtLote_UserCod_Title',ctrl:'LOTE_USERCOD',prop:'Title'},{av:'edtLote_PessoaCod_Titleformat',ctrl:'LOTE_PESSOACOD',prop:'Titleformat'},{av:'edtLote_PessoaCod_Title',ctrl:'LOTE_PESSOACOD',prop:'Title'},{av:'edtLote_UserNom_Titleformat',ctrl:'LOTE_USERNOM',prop:'Titleformat'},{av:'edtLote_UserNom_Title',ctrl:'LOTE_USERNOM',prop:'Title'},{av:'edtLote_ValorPF_Titleformat',ctrl:'LOTE_VALORPF',prop:'Titleformat'},{av:'edtLote_ValorPF_Title',ctrl:'LOTE_VALORPF',prop:'Title'},{av:'edtLote_DataIni_Titleformat',ctrl:'LOTE_DATAINI',prop:'Titleformat'},{av:'edtLote_DataIni_Title',ctrl:'LOTE_DATAINI',prop:'Title'},{av:'edtLote_DataFim_Titleformat',ctrl:'LOTE_DATAFIM',prop:'Titleformat'},{av:'edtLote_DataFim_Title',ctrl:'LOTE_DATAFIM',prop:'Title'},{av:'edtLote_QtdeDmn_Titleformat',ctrl:'LOTE_QTDEDMN',prop:'Titleformat'},{av:'edtLote_QtdeDmn_Title',ctrl:'LOTE_QTDEDMN',prop:'Title'},{av:'edtLote_Valor_Titleformat',ctrl:'LOTE_VALOR',prop:'Titleformat'},{av:'edtLote_Valor_Title',ctrl:'LOTE_VALOR',prop:'Title'},{av:'AV89GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV90GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11BR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Lote_Nome1',fld:'vLOTE_NOME1',pic:'@!',nv:''},{av:'AV18Lote_UserNom1',fld:'vLOTE_USERNOM1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Lote_Nome2',fld:'vLOTE_NOME2',pic:'@!',nv:''},{av:'AV23Lote_UserNom2',fld:'vLOTE_USERNOM2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Lote_Nome3',fld:'vLOTE_NOME3',pic:'@!',nv:''},{av:'AV28Lote_UserNom3',fld:'vLOTE_USERNOM3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV38TFLote_Numero',fld:'vTFLOTE_NUMERO',pic:'',nv:''},{av:'AV39TFLote_Numero_Sel',fld:'vTFLOTE_NUMERO_SEL',pic:'',nv:''},{av:'AV42TFLote_Nome',fld:'vTFLOTE_NOME',pic:'@!',nv:''},{av:'AV43TFLote_Nome_Sel',fld:'vTFLOTE_NOME_SEL',pic:'@!',nv:''},{av:'AV46TFLote_Data',fld:'vTFLOTE_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLote_Data_To',fld:'vTFLOTE_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV52TFLote_UserCod',fld:'vTFLOTE_USERCOD',pic:'ZZZZZ9',nv:0},{av:'AV53TFLote_UserCod_To',fld:'vTFLOTE_USERCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV56TFLote_PessoaCod',fld:'vTFLOTE_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV57TFLote_PessoaCod_To',fld:'vTFLOTE_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV60TFLote_UserNom',fld:'vTFLOTE_USERNOM',pic:'@!',nv:''},{av:'AV61TFLote_UserNom_Sel',fld:'vTFLOTE_USERNOM_SEL',pic:'@!',nv:''},{av:'AV64TFLote_ValorPF',fld:'vTFLOTE_VALORPF',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFLote_ValorPF_To',fld:'vTFLOTE_VALORPF_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFLote_DataIni',fld:'vTFLOTE_DATAINI',pic:'',nv:''},{av:'AV69TFLote_DataIni_To',fld:'vTFLOTE_DATAINI_TO',pic:'',nv:''},{av:'AV74TFLote_DataFim',fld:'vTFLOTE_DATAFIM',pic:'',nv:''},{av:'AV75TFLote_DataFim_To',fld:'vTFLOTE_DATAFIM_TO',pic:'',nv:''},{av:'AV80TFLote_QtdeDmn',fld:'vTFLOTE_QTDEDMN',pic:'ZZZ9',nv:0},{av:'AV81TFLote_QtdeDmn_To',fld:'vTFLOTE_QTDEDMN_TO',pic:'ZZZ9',nv:0},{av:'AV84TFLote_Valor',fld:'vTFLOTE_VALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV85TFLote_Valor_To',fld:'vTFLOTE_VALOR_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV40ddo_Lote_NumeroTitleControlIdToReplace',fld:'vDDO_LOTE_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_Lote_NomeTitleControlIdToReplace',fld:'vDDO_LOTE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Lote_DataTitleControlIdToReplace',fld:'vDDO_LOTE_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_Lote_UserCodTitleControlIdToReplace',fld:'vDDO_LOTE_USERCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Lote_PessoaCodTitleControlIdToReplace',fld:'vDDO_LOTE_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Lote_UserNomTitleControlIdToReplace',fld:'vDDO_LOTE_USERNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Lote_ValorPFTitleControlIdToReplace',fld:'vDDO_LOTE_VALORPFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Lote_DataIniTitleControlIdToReplace',fld:'vDDO_LOTE_DATAINITITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_Lote_DataFimTitleControlIdToReplace',fld:'vDDO_LOTE_DATAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_Lote_QtdeDmnTitleControlIdToReplace',fld:'vDDO_LOTE_QTDEDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_Lote_ValorTitleControlIdToReplace',fld:'vDDO_LOTE_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34Lote_AreaTrabalhoCod',fld:'vLOTE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36Lote_ContratadaCod',fld:'vLOTE_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_LOTE_NUMERO.ONOPTIONCLICKED","{handler:'E12BR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Lote_Nome1',fld:'vLOTE_NOME1',pic:'@!',nv:''},{av:'AV18Lote_UserNom1',fld:'vLOTE_USERNOM1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Lote_Nome2',fld:'vLOTE_NOME2',pic:'@!',nv:''},{av:'AV23Lote_UserNom2',fld:'vLOTE_USERNOM2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Lote_Nome3',fld:'vLOTE_NOME3',pic:'@!',nv:''},{av:'AV28Lote_UserNom3',fld:'vLOTE_USERNOM3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV38TFLote_Numero',fld:'vTFLOTE_NUMERO',pic:'',nv:''},{av:'AV39TFLote_Numero_Sel',fld:'vTFLOTE_NUMERO_SEL',pic:'',nv:''},{av:'AV42TFLote_Nome',fld:'vTFLOTE_NOME',pic:'@!',nv:''},{av:'AV43TFLote_Nome_Sel',fld:'vTFLOTE_NOME_SEL',pic:'@!',nv:''},{av:'AV46TFLote_Data',fld:'vTFLOTE_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLote_Data_To',fld:'vTFLOTE_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV52TFLote_UserCod',fld:'vTFLOTE_USERCOD',pic:'ZZZZZ9',nv:0},{av:'AV53TFLote_UserCod_To',fld:'vTFLOTE_USERCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV56TFLote_PessoaCod',fld:'vTFLOTE_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV57TFLote_PessoaCod_To',fld:'vTFLOTE_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV60TFLote_UserNom',fld:'vTFLOTE_USERNOM',pic:'@!',nv:''},{av:'AV61TFLote_UserNom_Sel',fld:'vTFLOTE_USERNOM_SEL',pic:'@!',nv:''},{av:'AV64TFLote_ValorPF',fld:'vTFLOTE_VALORPF',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFLote_ValorPF_To',fld:'vTFLOTE_VALORPF_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFLote_DataIni',fld:'vTFLOTE_DATAINI',pic:'',nv:''},{av:'AV69TFLote_DataIni_To',fld:'vTFLOTE_DATAINI_TO',pic:'',nv:''},{av:'AV74TFLote_DataFim',fld:'vTFLOTE_DATAFIM',pic:'',nv:''},{av:'AV75TFLote_DataFim_To',fld:'vTFLOTE_DATAFIM_TO',pic:'',nv:''},{av:'AV80TFLote_QtdeDmn',fld:'vTFLOTE_QTDEDMN',pic:'ZZZ9',nv:0},{av:'AV81TFLote_QtdeDmn_To',fld:'vTFLOTE_QTDEDMN_TO',pic:'ZZZ9',nv:0},{av:'AV84TFLote_Valor',fld:'vTFLOTE_VALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV85TFLote_Valor_To',fld:'vTFLOTE_VALOR_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV40ddo_Lote_NumeroTitleControlIdToReplace',fld:'vDDO_LOTE_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_Lote_NomeTitleControlIdToReplace',fld:'vDDO_LOTE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Lote_DataTitleControlIdToReplace',fld:'vDDO_LOTE_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_Lote_UserCodTitleControlIdToReplace',fld:'vDDO_LOTE_USERCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Lote_PessoaCodTitleControlIdToReplace',fld:'vDDO_LOTE_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Lote_UserNomTitleControlIdToReplace',fld:'vDDO_LOTE_USERNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Lote_ValorPFTitleControlIdToReplace',fld:'vDDO_LOTE_VALORPFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Lote_DataIniTitleControlIdToReplace',fld:'vDDO_LOTE_DATAINITITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_Lote_DataFimTitleControlIdToReplace',fld:'vDDO_LOTE_DATAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_Lote_QtdeDmnTitleControlIdToReplace',fld:'vDDO_LOTE_QTDEDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_Lote_ValorTitleControlIdToReplace',fld:'vDDO_LOTE_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34Lote_AreaTrabalhoCod',fld:'vLOTE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36Lote_ContratadaCod',fld:'vLOTE_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_lote_numero_Activeeventkey',ctrl:'DDO_LOTE_NUMERO',prop:'ActiveEventKey'},{av:'Ddo_lote_numero_Filteredtext_get',ctrl:'DDO_LOTE_NUMERO',prop:'FilteredText_get'},{av:'Ddo_lote_numero_Selectedvalue_get',ctrl:'DDO_LOTE_NUMERO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_lote_numero_Sortedstatus',ctrl:'DDO_LOTE_NUMERO',prop:'SortedStatus'},{av:'AV38TFLote_Numero',fld:'vTFLOTE_NUMERO',pic:'',nv:''},{av:'AV39TFLote_Numero_Sel',fld:'vTFLOTE_NUMERO_SEL',pic:'',nv:''},{av:'Ddo_lote_nome_Sortedstatus',ctrl:'DDO_LOTE_NOME',prop:'SortedStatus'},{av:'Ddo_lote_data_Sortedstatus',ctrl:'DDO_LOTE_DATA',prop:'SortedStatus'},{av:'Ddo_lote_usercod_Sortedstatus',ctrl:'DDO_LOTE_USERCOD',prop:'SortedStatus'},{av:'Ddo_lote_pessoacod_Sortedstatus',ctrl:'DDO_LOTE_PESSOACOD',prop:'SortedStatus'},{av:'Ddo_lote_usernom_Sortedstatus',ctrl:'DDO_LOTE_USERNOM',prop:'SortedStatus'},{av:'Ddo_lote_valorpf_Sortedstatus',ctrl:'DDO_LOTE_VALORPF',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_LOTE_NOME.ONOPTIONCLICKED","{handler:'E13BR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Lote_Nome1',fld:'vLOTE_NOME1',pic:'@!',nv:''},{av:'AV18Lote_UserNom1',fld:'vLOTE_USERNOM1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Lote_Nome2',fld:'vLOTE_NOME2',pic:'@!',nv:''},{av:'AV23Lote_UserNom2',fld:'vLOTE_USERNOM2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Lote_Nome3',fld:'vLOTE_NOME3',pic:'@!',nv:''},{av:'AV28Lote_UserNom3',fld:'vLOTE_USERNOM3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV38TFLote_Numero',fld:'vTFLOTE_NUMERO',pic:'',nv:''},{av:'AV39TFLote_Numero_Sel',fld:'vTFLOTE_NUMERO_SEL',pic:'',nv:''},{av:'AV42TFLote_Nome',fld:'vTFLOTE_NOME',pic:'@!',nv:''},{av:'AV43TFLote_Nome_Sel',fld:'vTFLOTE_NOME_SEL',pic:'@!',nv:''},{av:'AV46TFLote_Data',fld:'vTFLOTE_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLote_Data_To',fld:'vTFLOTE_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV52TFLote_UserCod',fld:'vTFLOTE_USERCOD',pic:'ZZZZZ9',nv:0},{av:'AV53TFLote_UserCod_To',fld:'vTFLOTE_USERCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV56TFLote_PessoaCod',fld:'vTFLOTE_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV57TFLote_PessoaCod_To',fld:'vTFLOTE_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV60TFLote_UserNom',fld:'vTFLOTE_USERNOM',pic:'@!',nv:''},{av:'AV61TFLote_UserNom_Sel',fld:'vTFLOTE_USERNOM_SEL',pic:'@!',nv:''},{av:'AV64TFLote_ValorPF',fld:'vTFLOTE_VALORPF',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFLote_ValorPF_To',fld:'vTFLOTE_VALORPF_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFLote_DataIni',fld:'vTFLOTE_DATAINI',pic:'',nv:''},{av:'AV69TFLote_DataIni_To',fld:'vTFLOTE_DATAINI_TO',pic:'',nv:''},{av:'AV74TFLote_DataFim',fld:'vTFLOTE_DATAFIM',pic:'',nv:''},{av:'AV75TFLote_DataFim_To',fld:'vTFLOTE_DATAFIM_TO',pic:'',nv:''},{av:'AV80TFLote_QtdeDmn',fld:'vTFLOTE_QTDEDMN',pic:'ZZZ9',nv:0},{av:'AV81TFLote_QtdeDmn_To',fld:'vTFLOTE_QTDEDMN_TO',pic:'ZZZ9',nv:0},{av:'AV84TFLote_Valor',fld:'vTFLOTE_VALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV85TFLote_Valor_To',fld:'vTFLOTE_VALOR_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV40ddo_Lote_NumeroTitleControlIdToReplace',fld:'vDDO_LOTE_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_Lote_NomeTitleControlIdToReplace',fld:'vDDO_LOTE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Lote_DataTitleControlIdToReplace',fld:'vDDO_LOTE_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_Lote_UserCodTitleControlIdToReplace',fld:'vDDO_LOTE_USERCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Lote_PessoaCodTitleControlIdToReplace',fld:'vDDO_LOTE_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Lote_UserNomTitleControlIdToReplace',fld:'vDDO_LOTE_USERNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Lote_ValorPFTitleControlIdToReplace',fld:'vDDO_LOTE_VALORPFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Lote_DataIniTitleControlIdToReplace',fld:'vDDO_LOTE_DATAINITITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_Lote_DataFimTitleControlIdToReplace',fld:'vDDO_LOTE_DATAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_Lote_QtdeDmnTitleControlIdToReplace',fld:'vDDO_LOTE_QTDEDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_Lote_ValorTitleControlIdToReplace',fld:'vDDO_LOTE_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34Lote_AreaTrabalhoCod',fld:'vLOTE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36Lote_ContratadaCod',fld:'vLOTE_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_lote_nome_Activeeventkey',ctrl:'DDO_LOTE_NOME',prop:'ActiveEventKey'},{av:'Ddo_lote_nome_Filteredtext_get',ctrl:'DDO_LOTE_NOME',prop:'FilteredText_get'},{av:'Ddo_lote_nome_Selectedvalue_get',ctrl:'DDO_LOTE_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_lote_nome_Sortedstatus',ctrl:'DDO_LOTE_NOME',prop:'SortedStatus'},{av:'AV42TFLote_Nome',fld:'vTFLOTE_NOME',pic:'@!',nv:''},{av:'AV43TFLote_Nome_Sel',fld:'vTFLOTE_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_lote_numero_Sortedstatus',ctrl:'DDO_LOTE_NUMERO',prop:'SortedStatus'},{av:'Ddo_lote_data_Sortedstatus',ctrl:'DDO_LOTE_DATA',prop:'SortedStatus'},{av:'Ddo_lote_usercod_Sortedstatus',ctrl:'DDO_LOTE_USERCOD',prop:'SortedStatus'},{av:'Ddo_lote_pessoacod_Sortedstatus',ctrl:'DDO_LOTE_PESSOACOD',prop:'SortedStatus'},{av:'Ddo_lote_usernom_Sortedstatus',ctrl:'DDO_LOTE_USERNOM',prop:'SortedStatus'},{av:'Ddo_lote_valorpf_Sortedstatus',ctrl:'DDO_LOTE_VALORPF',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_LOTE_DATA.ONOPTIONCLICKED","{handler:'E14BR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Lote_Nome1',fld:'vLOTE_NOME1',pic:'@!',nv:''},{av:'AV18Lote_UserNom1',fld:'vLOTE_USERNOM1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Lote_Nome2',fld:'vLOTE_NOME2',pic:'@!',nv:''},{av:'AV23Lote_UserNom2',fld:'vLOTE_USERNOM2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Lote_Nome3',fld:'vLOTE_NOME3',pic:'@!',nv:''},{av:'AV28Lote_UserNom3',fld:'vLOTE_USERNOM3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV38TFLote_Numero',fld:'vTFLOTE_NUMERO',pic:'',nv:''},{av:'AV39TFLote_Numero_Sel',fld:'vTFLOTE_NUMERO_SEL',pic:'',nv:''},{av:'AV42TFLote_Nome',fld:'vTFLOTE_NOME',pic:'@!',nv:''},{av:'AV43TFLote_Nome_Sel',fld:'vTFLOTE_NOME_SEL',pic:'@!',nv:''},{av:'AV46TFLote_Data',fld:'vTFLOTE_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLote_Data_To',fld:'vTFLOTE_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV52TFLote_UserCod',fld:'vTFLOTE_USERCOD',pic:'ZZZZZ9',nv:0},{av:'AV53TFLote_UserCod_To',fld:'vTFLOTE_USERCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV56TFLote_PessoaCod',fld:'vTFLOTE_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV57TFLote_PessoaCod_To',fld:'vTFLOTE_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV60TFLote_UserNom',fld:'vTFLOTE_USERNOM',pic:'@!',nv:''},{av:'AV61TFLote_UserNom_Sel',fld:'vTFLOTE_USERNOM_SEL',pic:'@!',nv:''},{av:'AV64TFLote_ValorPF',fld:'vTFLOTE_VALORPF',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFLote_ValorPF_To',fld:'vTFLOTE_VALORPF_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFLote_DataIni',fld:'vTFLOTE_DATAINI',pic:'',nv:''},{av:'AV69TFLote_DataIni_To',fld:'vTFLOTE_DATAINI_TO',pic:'',nv:''},{av:'AV74TFLote_DataFim',fld:'vTFLOTE_DATAFIM',pic:'',nv:''},{av:'AV75TFLote_DataFim_To',fld:'vTFLOTE_DATAFIM_TO',pic:'',nv:''},{av:'AV80TFLote_QtdeDmn',fld:'vTFLOTE_QTDEDMN',pic:'ZZZ9',nv:0},{av:'AV81TFLote_QtdeDmn_To',fld:'vTFLOTE_QTDEDMN_TO',pic:'ZZZ9',nv:0},{av:'AV84TFLote_Valor',fld:'vTFLOTE_VALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV85TFLote_Valor_To',fld:'vTFLOTE_VALOR_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV40ddo_Lote_NumeroTitleControlIdToReplace',fld:'vDDO_LOTE_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_Lote_NomeTitleControlIdToReplace',fld:'vDDO_LOTE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Lote_DataTitleControlIdToReplace',fld:'vDDO_LOTE_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_Lote_UserCodTitleControlIdToReplace',fld:'vDDO_LOTE_USERCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Lote_PessoaCodTitleControlIdToReplace',fld:'vDDO_LOTE_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Lote_UserNomTitleControlIdToReplace',fld:'vDDO_LOTE_USERNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Lote_ValorPFTitleControlIdToReplace',fld:'vDDO_LOTE_VALORPFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Lote_DataIniTitleControlIdToReplace',fld:'vDDO_LOTE_DATAINITITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_Lote_DataFimTitleControlIdToReplace',fld:'vDDO_LOTE_DATAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_Lote_QtdeDmnTitleControlIdToReplace',fld:'vDDO_LOTE_QTDEDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_Lote_ValorTitleControlIdToReplace',fld:'vDDO_LOTE_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34Lote_AreaTrabalhoCod',fld:'vLOTE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36Lote_ContratadaCod',fld:'vLOTE_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_lote_data_Activeeventkey',ctrl:'DDO_LOTE_DATA',prop:'ActiveEventKey'},{av:'Ddo_lote_data_Filteredtext_get',ctrl:'DDO_LOTE_DATA',prop:'FilteredText_get'},{av:'Ddo_lote_data_Filteredtextto_get',ctrl:'DDO_LOTE_DATA',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_lote_data_Sortedstatus',ctrl:'DDO_LOTE_DATA',prop:'SortedStatus'},{av:'AV46TFLote_Data',fld:'vTFLOTE_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLote_Data_To',fld:'vTFLOTE_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_lote_numero_Sortedstatus',ctrl:'DDO_LOTE_NUMERO',prop:'SortedStatus'},{av:'Ddo_lote_nome_Sortedstatus',ctrl:'DDO_LOTE_NOME',prop:'SortedStatus'},{av:'Ddo_lote_usercod_Sortedstatus',ctrl:'DDO_LOTE_USERCOD',prop:'SortedStatus'},{av:'Ddo_lote_pessoacod_Sortedstatus',ctrl:'DDO_LOTE_PESSOACOD',prop:'SortedStatus'},{av:'Ddo_lote_usernom_Sortedstatus',ctrl:'DDO_LOTE_USERNOM',prop:'SortedStatus'},{av:'Ddo_lote_valorpf_Sortedstatus',ctrl:'DDO_LOTE_VALORPF',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_LOTE_USERCOD.ONOPTIONCLICKED","{handler:'E15BR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Lote_Nome1',fld:'vLOTE_NOME1',pic:'@!',nv:''},{av:'AV18Lote_UserNom1',fld:'vLOTE_USERNOM1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Lote_Nome2',fld:'vLOTE_NOME2',pic:'@!',nv:''},{av:'AV23Lote_UserNom2',fld:'vLOTE_USERNOM2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Lote_Nome3',fld:'vLOTE_NOME3',pic:'@!',nv:''},{av:'AV28Lote_UserNom3',fld:'vLOTE_USERNOM3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV38TFLote_Numero',fld:'vTFLOTE_NUMERO',pic:'',nv:''},{av:'AV39TFLote_Numero_Sel',fld:'vTFLOTE_NUMERO_SEL',pic:'',nv:''},{av:'AV42TFLote_Nome',fld:'vTFLOTE_NOME',pic:'@!',nv:''},{av:'AV43TFLote_Nome_Sel',fld:'vTFLOTE_NOME_SEL',pic:'@!',nv:''},{av:'AV46TFLote_Data',fld:'vTFLOTE_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLote_Data_To',fld:'vTFLOTE_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV52TFLote_UserCod',fld:'vTFLOTE_USERCOD',pic:'ZZZZZ9',nv:0},{av:'AV53TFLote_UserCod_To',fld:'vTFLOTE_USERCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV56TFLote_PessoaCod',fld:'vTFLOTE_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV57TFLote_PessoaCod_To',fld:'vTFLOTE_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV60TFLote_UserNom',fld:'vTFLOTE_USERNOM',pic:'@!',nv:''},{av:'AV61TFLote_UserNom_Sel',fld:'vTFLOTE_USERNOM_SEL',pic:'@!',nv:''},{av:'AV64TFLote_ValorPF',fld:'vTFLOTE_VALORPF',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFLote_ValorPF_To',fld:'vTFLOTE_VALORPF_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFLote_DataIni',fld:'vTFLOTE_DATAINI',pic:'',nv:''},{av:'AV69TFLote_DataIni_To',fld:'vTFLOTE_DATAINI_TO',pic:'',nv:''},{av:'AV74TFLote_DataFim',fld:'vTFLOTE_DATAFIM',pic:'',nv:''},{av:'AV75TFLote_DataFim_To',fld:'vTFLOTE_DATAFIM_TO',pic:'',nv:''},{av:'AV80TFLote_QtdeDmn',fld:'vTFLOTE_QTDEDMN',pic:'ZZZ9',nv:0},{av:'AV81TFLote_QtdeDmn_To',fld:'vTFLOTE_QTDEDMN_TO',pic:'ZZZ9',nv:0},{av:'AV84TFLote_Valor',fld:'vTFLOTE_VALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV85TFLote_Valor_To',fld:'vTFLOTE_VALOR_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV40ddo_Lote_NumeroTitleControlIdToReplace',fld:'vDDO_LOTE_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_Lote_NomeTitleControlIdToReplace',fld:'vDDO_LOTE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Lote_DataTitleControlIdToReplace',fld:'vDDO_LOTE_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_Lote_UserCodTitleControlIdToReplace',fld:'vDDO_LOTE_USERCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Lote_PessoaCodTitleControlIdToReplace',fld:'vDDO_LOTE_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Lote_UserNomTitleControlIdToReplace',fld:'vDDO_LOTE_USERNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Lote_ValorPFTitleControlIdToReplace',fld:'vDDO_LOTE_VALORPFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Lote_DataIniTitleControlIdToReplace',fld:'vDDO_LOTE_DATAINITITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_Lote_DataFimTitleControlIdToReplace',fld:'vDDO_LOTE_DATAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_Lote_QtdeDmnTitleControlIdToReplace',fld:'vDDO_LOTE_QTDEDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_Lote_ValorTitleControlIdToReplace',fld:'vDDO_LOTE_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34Lote_AreaTrabalhoCod',fld:'vLOTE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36Lote_ContratadaCod',fld:'vLOTE_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_lote_usercod_Activeeventkey',ctrl:'DDO_LOTE_USERCOD',prop:'ActiveEventKey'},{av:'Ddo_lote_usercod_Filteredtext_get',ctrl:'DDO_LOTE_USERCOD',prop:'FilteredText_get'},{av:'Ddo_lote_usercod_Filteredtextto_get',ctrl:'DDO_LOTE_USERCOD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_lote_usercod_Sortedstatus',ctrl:'DDO_LOTE_USERCOD',prop:'SortedStatus'},{av:'AV52TFLote_UserCod',fld:'vTFLOTE_USERCOD',pic:'ZZZZZ9',nv:0},{av:'AV53TFLote_UserCod_To',fld:'vTFLOTE_USERCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_lote_numero_Sortedstatus',ctrl:'DDO_LOTE_NUMERO',prop:'SortedStatus'},{av:'Ddo_lote_nome_Sortedstatus',ctrl:'DDO_LOTE_NOME',prop:'SortedStatus'},{av:'Ddo_lote_data_Sortedstatus',ctrl:'DDO_LOTE_DATA',prop:'SortedStatus'},{av:'Ddo_lote_pessoacod_Sortedstatus',ctrl:'DDO_LOTE_PESSOACOD',prop:'SortedStatus'},{av:'Ddo_lote_usernom_Sortedstatus',ctrl:'DDO_LOTE_USERNOM',prop:'SortedStatus'},{av:'Ddo_lote_valorpf_Sortedstatus',ctrl:'DDO_LOTE_VALORPF',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_LOTE_PESSOACOD.ONOPTIONCLICKED","{handler:'E16BR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Lote_Nome1',fld:'vLOTE_NOME1',pic:'@!',nv:''},{av:'AV18Lote_UserNom1',fld:'vLOTE_USERNOM1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Lote_Nome2',fld:'vLOTE_NOME2',pic:'@!',nv:''},{av:'AV23Lote_UserNom2',fld:'vLOTE_USERNOM2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Lote_Nome3',fld:'vLOTE_NOME3',pic:'@!',nv:''},{av:'AV28Lote_UserNom3',fld:'vLOTE_USERNOM3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV38TFLote_Numero',fld:'vTFLOTE_NUMERO',pic:'',nv:''},{av:'AV39TFLote_Numero_Sel',fld:'vTFLOTE_NUMERO_SEL',pic:'',nv:''},{av:'AV42TFLote_Nome',fld:'vTFLOTE_NOME',pic:'@!',nv:''},{av:'AV43TFLote_Nome_Sel',fld:'vTFLOTE_NOME_SEL',pic:'@!',nv:''},{av:'AV46TFLote_Data',fld:'vTFLOTE_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLote_Data_To',fld:'vTFLOTE_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV52TFLote_UserCod',fld:'vTFLOTE_USERCOD',pic:'ZZZZZ9',nv:0},{av:'AV53TFLote_UserCod_To',fld:'vTFLOTE_USERCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV56TFLote_PessoaCod',fld:'vTFLOTE_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV57TFLote_PessoaCod_To',fld:'vTFLOTE_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV60TFLote_UserNom',fld:'vTFLOTE_USERNOM',pic:'@!',nv:''},{av:'AV61TFLote_UserNom_Sel',fld:'vTFLOTE_USERNOM_SEL',pic:'@!',nv:''},{av:'AV64TFLote_ValorPF',fld:'vTFLOTE_VALORPF',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFLote_ValorPF_To',fld:'vTFLOTE_VALORPF_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFLote_DataIni',fld:'vTFLOTE_DATAINI',pic:'',nv:''},{av:'AV69TFLote_DataIni_To',fld:'vTFLOTE_DATAINI_TO',pic:'',nv:''},{av:'AV74TFLote_DataFim',fld:'vTFLOTE_DATAFIM',pic:'',nv:''},{av:'AV75TFLote_DataFim_To',fld:'vTFLOTE_DATAFIM_TO',pic:'',nv:''},{av:'AV80TFLote_QtdeDmn',fld:'vTFLOTE_QTDEDMN',pic:'ZZZ9',nv:0},{av:'AV81TFLote_QtdeDmn_To',fld:'vTFLOTE_QTDEDMN_TO',pic:'ZZZ9',nv:0},{av:'AV84TFLote_Valor',fld:'vTFLOTE_VALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV85TFLote_Valor_To',fld:'vTFLOTE_VALOR_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV40ddo_Lote_NumeroTitleControlIdToReplace',fld:'vDDO_LOTE_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_Lote_NomeTitleControlIdToReplace',fld:'vDDO_LOTE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Lote_DataTitleControlIdToReplace',fld:'vDDO_LOTE_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_Lote_UserCodTitleControlIdToReplace',fld:'vDDO_LOTE_USERCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Lote_PessoaCodTitleControlIdToReplace',fld:'vDDO_LOTE_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Lote_UserNomTitleControlIdToReplace',fld:'vDDO_LOTE_USERNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Lote_ValorPFTitleControlIdToReplace',fld:'vDDO_LOTE_VALORPFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Lote_DataIniTitleControlIdToReplace',fld:'vDDO_LOTE_DATAINITITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_Lote_DataFimTitleControlIdToReplace',fld:'vDDO_LOTE_DATAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_Lote_QtdeDmnTitleControlIdToReplace',fld:'vDDO_LOTE_QTDEDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_Lote_ValorTitleControlIdToReplace',fld:'vDDO_LOTE_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34Lote_AreaTrabalhoCod',fld:'vLOTE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36Lote_ContratadaCod',fld:'vLOTE_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_lote_pessoacod_Activeeventkey',ctrl:'DDO_LOTE_PESSOACOD',prop:'ActiveEventKey'},{av:'Ddo_lote_pessoacod_Filteredtext_get',ctrl:'DDO_LOTE_PESSOACOD',prop:'FilteredText_get'},{av:'Ddo_lote_pessoacod_Filteredtextto_get',ctrl:'DDO_LOTE_PESSOACOD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_lote_pessoacod_Sortedstatus',ctrl:'DDO_LOTE_PESSOACOD',prop:'SortedStatus'},{av:'AV56TFLote_PessoaCod',fld:'vTFLOTE_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV57TFLote_PessoaCod_To',fld:'vTFLOTE_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_lote_numero_Sortedstatus',ctrl:'DDO_LOTE_NUMERO',prop:'SortedStatus'},{av:'Ddo_lote_nome_Sortedstatus',ctrl:'DDO_LOTE_NOME',prop:'SortedStatus'},{av:'Ddo_lote_data_Sortedstatus',ctrl:'DDO_LOTE_DATA',prop:'SortedStatus'},{av:'Ddo_lote_usercod_Sortedstatus',ctrl:'DDO_LOTE_USERCOD',prop:'SortedStatus'},{av:'Ddo_lote_usernom_Sortedstatus',ctrl:'DDO_LOTE_USERNOM',prop:'SortedStatus'},{av:'Ddo_lote_valorpf_Sortedstatus',ctrl:'DDO_LOTE_VALORPF',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_LOTE_USERNOM.ONOPTIONCLICKED","{handler:'E17BR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Lote_Nome1',fld:'vLOTE_NOME1',pic:'@!',nv:''},{av:'AV18Lote_UserNom1',fld:'vLOTE_USERNOM1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Lote_Nome2',fld:'vLOTE_NOME2',pic:'@!',nv:''},{av:'AV23Lote_UserNom2',fld:'vLOTE_USERNOM2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Lote_Nome3',fld:'vLOTE_NOME3',pic:'@!',nv:''},{av:'AV28Lote_UserNom3',fld:'vLOTE_USERNOM3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV38TFLote_Numero',fld:'vTFLOTE_NUMERO',pic:'',nv:''},{av:'AV39TFLote_Numero_Sel',fld:'vTFLOTE_NUMERO_SEL',pic:'',nv:''},{av:'AV42TFLote_Nome',fld:'vTFLOTE_NOME',pic:'@!',nv:''},{av:'AV43TFLote_Nome_Sel',fld:'vTFLOTE_NOME_SEL',pic:'@!',nv:''},{av:'AV46TFLote_Data',fld:'vTFLOTE_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLote_Data_To',fld:'vTFLOTE_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV52TFLote_UserCod',fld:'vTFLOTE_USERCOD',pic:'ZZZZZ9',nv:0},{av:'AV53TFLote_UserCod_To',fld:'vTFLOTE_USERCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV56TFLote_PessoaCod',fld:'vTFLOTE_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV57TFLote_PessoaCod_To',fld:'vTFLOTE_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV60TFLote_UserNom',fld:'vTFLOTE_USERNOM',pic:'@!',nv:''},{av:'AV61TFLote_UserNom_Sel',fld:'vTFLOTE_USERNOM_SEL',pic:'@!',nv:''},{av:'AV64TFLote_ValorPF',fld:'vTFLOTE_VALORPF',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFLote_ValorPF_To',fld:'vTFLOTE_VALORPF_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFLote_DataIni',fld:'vTFLOTE_DATAINI',pic:'',nv:''},{av:'AV69TFLote_DataIni_To',fld:'vTFLOTE_DATAINI_TO',pic:'',nv:''},{av:'AV74TFLote_DataFim',fld:'vTFLOTE_DATAFIM',pic:'',nv:''},{av:'AV75TFLote_DataFim_To',fld:'vTFLOTE_DATAFIM_TO',pic:'',nv:''},{av:'AV80TFLote_QtdeDmn',fld:'vTFLOTE_QTDEDMN',pic:'ZZZ9',nv:0},{av:'AV81TFLote_QtdeDmn_To',fld:'vTFLOTE_QTDEDMN_TO',pic:'ZZZ9',nv:0},{av:'AV84TFLote_Valor',fld:'vTFLOTE_VALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV85TFLote_Valor_To',fld:'vTFLOTE_VALOR_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV40ddo_Lote_NumeroTitleControlIdToReplace',fld:'vDDO_LOTE_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_Lote_NomeTitleControlIdToReplace',fld:'vDDO_LOTE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Lote_DataTitleControlIdToReplace',fld:'vDDO_LOTE_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_Lote_UserCodTitleControlIdToReplace',fld:'vDDO_LOTE_USERCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Lote_PessoaCodTitleControlIdToReplace',fld:'vDDO_LOTE_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Lote_UserNomTitleControlIdToReplace',fld:'vDDO_LOTE_USERNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Lote_ValorPFTitleControlIdToReplace',fld:'vDDO_LOTE_VALORPFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Lote_DataIniTitleControlIdToReplace',fld:'vDDO_LOTE_DATAINITITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_Lote_DataFimTitleControlIdToReplace',fld:'vDDO_LOTE_DATAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_Lote_QtdeDmnTitleControlIdToReplace',fld:'vDDO_LOTE_QTDEDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_Lote_ValorTitleControlIdToReplace',fld:'vDDO_LOTE_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34Lote_AreaTrabalhoCod',fld:'vLOTE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36Lote_ContratadaCod',fld:'vLOTE_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_lote_usernom_Activeeventkey',ctrl:'DDO_LOTE_USERNOM',prop:'ActiveEventKey'},{av:'Ddo_lote_usernom_Filteredtext_get',ctrl:'DDO_LOTE_USERNOM',prop:'FilteredText_get'},{av:'Ddo_lote_usernom_Selectedvalue_get',ctrl:'DDO_LOTE_USERNOM',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_lote_usernom_Sortedstatus',ctrl:'DDO_LOTE_USERNOM',prop:'SortedStatus'},{av:'AV60TFLote_UserNom',fld:'vTFLOTE_USERNOM',pic:'@!',nv:''},{av:'AV61TFLote_UserNom_Sel',fld:'vTFLOTE_USERNOM_SEL',pic:'@!',nv:''},{av:'Ddo_lote_numero_Sortedstatus',ctrl:'DDO_LOTE_NUMERO',prop:'SortedStatus'},{av:'Ddo_lote_nome_Sortedstatus',ctrl:'DDO_LOTE_NOME',prop:'SortedStatus'},{av:'Ddo_lote_data_Sortedstatus',ctrl:'DDO_LOTE_DATA',prop:'SortedStatus'},{av:'Ddo_lote_usercod_Sortedstatus',ctrl:'DDO_LOTE_USERCOD',prop:'SortedStatus'},{av:'Ddo_lote_pessoacod_Sortedstatus',ctrl:'DDO_LOTE_PESSOACOD',prop:'SortedStatus'},{av:'Ddo_lote_valorpf_Sortedstatus',ctrl:'DDO_LOTE_VALORPF',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_LOTE_VALORPF.ONOPTIONCLICKED","{handler:'E18BR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Lote_Nome1',fld:'vLOTE_NOME1',pic:'@!',nv:''},{av:'AV18Lote_UserNom1',fld:'vLOTE_USERNOM1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Lote_Nome2',fld:'vLOTE_NOME2',pic:'@!',nv:''},{av:'AV23Lote_UserNom2',fld:'vLOTE_USERNOM2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Lote_Nome3',fld:'vLOTE_NOME3',pic:'@!',nv:''},{av:'AV28Lote_UserNom3',fld:'vLOTE_USERNOM3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV38TFLote_Numero',fld:'vTFLOTE_NUMERO',pic:'',nv:''},{av:'AV39TFLote_Numero_Sel',fld:'vTFLOTE_NUMERO_SEL',pic:'',nv:''},{av:'AV42TFLote_Nome',fld:'vTFLOTE_NOME',pic:'@!',nv:''},{av:'AV43TFLote_Nome_Sel',fld:'vTFLOTE_NOME_SEL',pic:'@!',nv:''},{av:'AV46TFLote_Data',fld:'vTFLOTE_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLote_Data_To',fld:'vTFLOTE_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV52TFLote_UserCod',fld:'vTFLOTE_USERCOD',pic:'ZZZZZ9',nv:0},{av:'AV53TFLote_UserCod_To',fld:'vTFLOTE_USERCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV56TFLote_PessoaCod',fld:'vTFLOTE_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV57TFLote_PessoaCod_To',fld:'vTFLOTE_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV60TFLote_UserNom',fld:'vTFLOTE_USERNOM',pic:'@!',nv:''},{av:'AV61TFLote_UserNom_Sel',fld:'vTFLOTE_USERNOM_SEL',pic:'@!',nv:''},{av:'AV64TFLote_ValorPF',fld:'vTFLOTE_VALORPF',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFLote_ValorPF_To',fld:'vTFLOTE_VALORPF_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFLote_DataIni',fld:'vTFLOTE_DATAINI',pic:'',nv:''},{av:'AV69TFLote_DataIni_To',fld:'vTFLOTE_DATAINI_TO',pic:'',nv:''},{av:'AV74TFLote_DataFim',fld:'vTFLOTE_DATAFIM',pic:'',nv:''},{av:'AV75TFLote_DataFim_To',fld:'vTFLOTE_DATAFIM_TO',pic:'',nv:''},{av:'AV80TFLote_QtdeDmn',fld:'vTFLOTE_QTDEDMN',pic:'ZZZ9',nv:0},{av:'AV81TFLote_QtdeDmn_To',fld:'vTFLOTE_QTDEDMN_TO',pic:'ZZZ9',nv:0},{av:'AV84TFLote_Valor',fld:'vTFLOTE_VALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV85TFLote_Valor_To',fld:'vTFLOTE_VALOR_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV40ddo_Lote_NumeroTitleControlIdToReplace',fld:'vDDO_LOTE_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_Lote_NomeTitleControlIdToReplace',fld:'vDDO_LOTE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Lote_DataTitleControlIdToReplace',fld:'vDDO_LOTE_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_Lote_UserCodTitleControlIdToReplace',fld:'vDDO_LOTE_USERCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Lote_PessoaCodTitleControlIdToReplace',fld:'vDDO_LOTE_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Lote_UserNomTitleControlIdToReplace',fld:'vDDO_LOTE_USERNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Lote_ValorPFTitleControlIdToReplace',fld:'vDDO_LOTE_VALORPFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Lote_DataIniTitleControlIdToReplace',fld:'vDDO_LOTE_DATAINITITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_Lote_DataFimTitleControlIdToReplace',fld:'vDDO_LOTE_DATAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_Lote_QtdeDmnTitleControlIdToReplace',fld:'vDDO_LOTE_QTDEDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_Lote_ValorTitleControlIdToReplace',fld:'vDDO_LOTE_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34Lote_AreaTrabalhoCod',fld:'vLOTE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36Lote_ContratadaCod',fld:'vLOTE_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_lote_valorpf_Activeeventkey',ctrl:'DDO_LOTE_VALORPF',prop:'ActiveEventKey'},{av:'Ddo_lote_valorpf_Filteredtext_get',ctrl:'DDO_LOTE_VALORPF',prop:'FilteredText_get'},{av:'Ddo_lote_valorpf_Filteredtextto_get',ctrl:'DDO_LOTE_VALORPF',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_lote_valorpf_Sortedstatus',ctrl:'DDO_LOTE_VALORPF',prop:'SortedStatus'},{av:'AV64TFLote_ValorPF',fld:'vTFLOTE_VALORPF',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFLote_ValorPF_To',fld:'vTFLOTE_VALORPF_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_lote_numero_Sortedstatus',ctrl:'DDO_LOTE_NUMERO',prop:'SortedStatus'},{av:'Ddo_lote_nome_Sortedstatus',ctrl:'DDO_LOTE_NOME',prop:'SortedStatus'},{av:'Ddo_lote_data_Sortedstatus',ctrl:'DDO_LOTE_DATA',prop:'SortedStatus'},{av:'Ddo_lote_usercod_Sortedstatus',ctrl:'DDO_LOTE_USERCOD',prop:'SortedStatus'},{av:'Ddo_lote_pessoacod_Sortedstatus',ctrl:'DDO_LOTE_PESSOACOD',prop:'SortedStatus'},{av:'Ddo_lote_usernom_Sortedstatus',ctrl:'DDO_LOTE_USERNOM',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_LOTE_DATAINI.ONOPTIONCLICKED","{handler:'E19BR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Lote_Nome1',fld:'vLOTE_NOME1',pic:'@!',nv:''},{av:'AV18Lote_UserNom1',fld:'vLOTE_USERNOM1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Lote_Nome2',fld:'vLOTE_NOME2',pic:'@!',nv:''},{av:'AV23Lote_UserNom2',fld:'vLOTE_USERNOM2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Lote_Nome3',fld:'vLOTE_NOME3',pic:'@!',nv:''},{av:'AV28Lote_UserNom3',fld:'vLOTE_USERNOM3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV38TFLote_Numero',fld:'vTFLOTE_NUMERO',pic:'',nv:''},{av:'AV39TFLote_Numero_Sel',fld:'vTFLOTE_NUMERO_SEL',pic:'',nv:''},{av:'AV42TFLote_Nome',fld:'vTFLOTE_NOME',pic:'@!',nv:''},{av:'AV43TFLote_Nome_Sel',fld:'vTFLOTE_NOME_SEL',pic:'@!',nv:''},{av:'AV46TFLote_Data',fld:'vTFLOTE_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLote_Data_To',fld:'vTFLOTE_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV52TFLote_UserCod',fld:'vTFLOTE_USERCOD',pic:'ZZZZZ9',nv:0},{av:'AV53TFLote_UserCod_To',fld:'vTFLOTE_USERCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV56TFLote_PessoaCod',fld:'vTFLOTE_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV57TFLote_PessoaCod_To',fld:'vTFLOTE_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV60TFLote_UserNom',fld:'vTFLOTE_USERNOM',pic:'@!',nv:''},{av:'AV61TFLote_UserNom_Sel',fld:'vTFLOTE_USERNOM_SEL',pic:'@!',nv:''},{av:'AV64TFLote_ValorPF',fld:'vTFLOTE_VALORPF',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFLote_ValorPF_To',fld:'vTFLOTE_VALORPF_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFLote_DataIni',fld:'vTFLOTE_DATAINI',pic:'',nv:''},{av:'AV69TFLote_DataIni_To',fld:'vTFLOTE_DATAINI_TO',pic:'',nv:''},{av:'AV74TFLote_DataFim',fld:'vTFLOTE_DATAFIM',pic:'',nv:''},{av:'AV75TFLote_DataFim_To',fld:'vTFLOTE_DATAFIM_TO',pic:'',nv:''},{av:'AV80TFLote_QtdeDmn',fld:'vTFLOTE_QTDEDMN',pic:'ZZZ9',nv:0},{av:'AV81TFLote_QtdeDmn_To',fld:'vTFLOTE_QTDEDMN_TO',pic:'ZZZ9',nv:0},{av:'AV84TFLote_Valor',fld:'vTFLOTE_VALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV85TFLote_Valor_To',fld:'vTFLOTE_VALOR_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV40ddo_Lote_NumeroTitleControlIdToReplace',fld:'vDDO_LOTE_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_Lote_NomeTitleControlIdToReplace',fld:'vDDO_LOTE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Lote_DataTitleControlIdToReplace',fld:'vDDO_LOTE_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_Lote_UserCodTitleControlIdToReplace',fld:'vDDO_LOTE_USERCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Lote_PessoaCodTitleControlIdToReplace',fld:'vDDO_LOTE_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Lote_UserNomTitleControlIdToReplace',fld:'vDDO_LOTE_USERNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Lote_ValorPFTitleControlIdToReplace',fld:'vDDO_LOTE_VALORPFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Lote_DataIniTitleControlIdToReplace',fld:'vDDO_LOTE_DATAINITITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_Lote_DataFimTitleControlIdToReplace',fld:'vDDO_LOTE_DATAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_Lote_QtdeDmnTitleControlIdToReplace',fld:'vDDO_LOTE_QTDEDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_Lote_ValorTitleControlIdToReplace',fld:'vDDO_LOTE_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34Lote_AreaTrabalhoCod',fld:'vLOTE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36Lote_ContratadaCod',fld:'vLOTE_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_lote_dataini_Activeeventkey',ctrl:'DDO_LOTE_DATAINI',prop:'ActiveEventKey'},{av:'Ddo_lote_dataini_Filteredtext_get',ctrl:'DDO_LOTE_DATAINI',prop:'FilteredText_get'},{av:'Ddo_lote_dataini_Filteredtextto_get',ctrl:'DDO_LOTE_DATAINI',prop:'FilteredTextTo_get'}],oparms:[{av:'AV68TFLote_DataIni',fld:'vTFLOTE_DATAINI',pic:'',nv:''},{av:'AV69TFLote_DataIni_To',fld:'vTFLOTE_DATAINI_TO',pic:'',nv:''}]}");
         setEventMetadata("DDO_LOTE_DATAFIM.ONOPTIONCLICKED","{handler:'E20BR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Lote_Nome1',fld:'vLOTE_NOME1',pic:'@!',nv:''},{av:'AV18Lote_UserNom1',fld:'vLOTE_USERNOM1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Lote_Nome2',fld:'vLOTE_NOME2',pic:'@!',nv:''},{av:'AV23Lote_UserNom2',fld:'vLOTE_USERNOM2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Lote_Nome3',fld:'vLOTE_NOME3',pic:'@!',nv:''},{av:'AV28Lote_UserNom3',fld:'vLOTE_USERNOM3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV38TFLote_Numero',fld:'vTFLOTE_NUMERO',pic:'',nv:''},{av:'AV39TFLote_Numero_Sel',fld:'vTFLOTE_NUMERO_SEL',pic:'',nv:''},{av:'AV42TFLote_Nome',fld:'vTFLOTE_NOME',pic:'@!',nv:''},{av:'AV43TFLote_Nome_Sel',fld:'vTFLOTE_NOME_SEL',pic:'@!',nv:''},{av:'AV46TFLote_Data',fld:'vTFLOTE_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLote_Data_To',fld:'vTFLOTE_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV52TFLote_UserCod',fld:'vTFLOTE_USERCOD',pic:'ZZZZZ9',nv:0},{av:'AV53TFLote_UserCod_To',fld:'vTFLOTE_USERCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV56TFLote_PessoaCod',fld:'vTFLOTE_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV57TFLote_PessoaCod_To',fld:'vTFLOTE_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV60TFLote_UserNom',fld:'vTFLOTE_USERNOM',pic:'@!',nv:''},{av:'AV61TFLote_UserNom_Sel',fld:'vTFLOTE_USERNOM_SEL',pic:'@!',nv:''},{av:'AV64TFLote_ValorPF',fld:'vTFLOTE_VALORPF',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFLote_ValorPF_To',fld:'vTFLOTE_VALORPF_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFLote_DataIni',fld:'vTFLOTE_DATAINI',pic:'',nv:''},{av:'AV69TFLote_DataIni_To',fld:'vTFLOTE_DATAINI_TO',pic:'',nv:''},{av:'AV74TFLote_DataFim',fld:'vTFLOTE_DATAFIM',pic:'',nv:''},{av:'AV75TFLote_DataFim_To',fld:'vTFLOTE_DATAFIM_TO',pic:'',nv:''},{av:'AV80TFLote_QtdeDmn',fld:'vTFLOTE_QTDEDMN',pic:'ZZZ9',nv:0},{av:'AV81TFLote_QtdeDmn_To',fld:'vTFLOTE_QTDEDMN_TO',pic:'ZZZ9',nv:0},{av:'AV84TFLote_Valor',fld:'vTFLOTE_VALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV85TFLote_Valor_To',fld:'vTFLOTE_VALOR_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV40ddo_Lote_NumeroTitleControlIdToReplace',fld:'vDDO_LOTE_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_Lote_NomeTitleControlIdToReplace',fld:'vDDO_LOTE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Lote_DataTitleControlIdToReplace',fld:'vDDO_LOTE_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_Lote_UserCodTitleControlIdToReplace',fld:'vDDO_LOTE_USERCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Lote_PessoaCodTitleControlIdToReplace',fld:'vDDO_LOTE_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Lote_UserNomTitleControlIdToReplace',fld:'vDDO_LOTE_USERNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Lote_ValorPFTitleControlIdToReplace',fld:'vDDO_LOTE_VALORPFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Lote_DataIniTitleControlIdToReplace',fld:'vDDO_LOTE_DATAINITITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_Lote_DataFimTitleControlIdToReplace',fld:'vDDO_LOTE_DATAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_Lote_QtdeDmnTitleControlIdToReplace',fld:'vDDO_LOTE_QTDEDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_Lote_ValorTitleControlIdToReplace',fld:'vDDO_LOTE_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34Lote_AreaTrabalhoCod',fld:'vLOTE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36Lote_ContratadaCod',fld:'vLOTE_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_lote_datafim_Activeeventkey',ctrl:'DDO_LOTE_DATAFIM',prop:'ActiveEventKey'},{av:'Ddo_lote_datafim_Filteredtext_get',ctrl:'DDO_LOTE_DATAFIM',prop:'FilteredText_get'},{av:'Ddo_lote_datafim_Filteredtextto_get',ctrl:'DDO_LOTE_DATAFIM',prop:'FilteredTextTo_get'}],oparms:[{av:'AV74TFLote_DataFim',fld:'vTFLOTE_DATAFIM',pic:'',nv:''},{av:'AV75TFLote_DataFim_To',fld:'vTFLOTE_DATAFIM_TO',pic:'',nv:''}]}");
         setEventMetadata("DDO_LOTE_QTDEDMN.ONOPTIONCLICKED","{handler:'E21BR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Lote_Nome1',fld:'vLOTE_NOME1',pic:'@!',nv:''},{av:'AV18Lote_UserNom1',fld:'vLOTE_USERNOM1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Lote_Nome2',fld:'vLOTE_NOME2',pic:'@!',nv:''},{av:'AV23Lote_UserNom2',fld:'vLOTE_USERNOM2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Lote_Nome3',fld:'vLOTE_NOME3',pic:'@!',nv:''},{av:'AV28Lote_UserNom3',fld:'vLOTE_USERNOM3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV38TFLote_Numero',fld:'vTFLOTE_NUMERO',pic:'',nv:''},{av:'AV39TFLote_Numero_Sel',fld:'vTFLOTE_NUMERO_SEL',pic:'',nv:''},{av:'AV42TFLote_Nome',fld:'vTFLOTE_NOME',pic:'@!',nv:''},{av:'AV43TFLote_Nome_Sel',fld:'vTFLOTE_NOME_SEL',pic:'@!',nv:''},{av:'AV46TFLote_Data',fld:'vTFLOTE_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLote_Data_To',fld:'vTFLOTE_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV52TFLote_UserCod',fld:'vTFLOTE_USERCOD',pic:'ZZZZZ9',nv:0},{av:'AV53TFLote_UserCod_To',fld:'vTFLOTE_USERCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV56TFLote_PessoaCod',fld:'vTFLOTE_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV57TFLote_PessoaCod_To',fld:'vTFLOTE_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV60TFLote_UserNom',fld:'vTFLOTE_USERNOM',pic:'@!',nv:''},{av:'AV61TFLote_UserNom_Sel',fld:'vTFLOTE_USERNOM_SEL',pic:'@!',nv:''},{av:'AV64TFLote_ValorPF',fld:'vTFLOTE_VALORPF',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFLote_ValorPF_To',fld:'vTFLOTE_VALORPF_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFLote_DataIni',fld:'vTFLOTE_DATAINI',pic:'',nv:''},{av:'AV69TFLote_DataIni_To',fld:'vTFLOTE_DATAINI_TO',pic:'',nv:''},{av:'AV74TFLote_DataFim',fld:'vTFLOTE_DATAFIM',pic:'',nv:''},{av:'AV75TFLote_DataFim_To',fld:'vTFLOTE_DATAFIM_TO',pic:'',nv:''},{av:'AV80TFLote_QtdeDmn',fld:'vTFLOTE_QTDEDMN',pic:'ZZZ9',nv:0},{av:'AV81TFLote_QtdeDmn_To',fld:'vTFLOTE_QTDEDMN_TO',pic:'ZZZ9',nv:0},{av:'AV84TFLote_Valor',fld:'vTFLOTE_VALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV85TFLote_Valor_To',fld:'vTFLOTE_VALOR_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV40ddo_Lote_NumeroTitleControlIdToReplace',fld:'vDDO_LOTE_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_Lote_NomeTitleControlIdToReplace',fld:'vDDO_LOTE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Lote_DataTitleControlIdToReplace',fld:'vDDO_LOTE_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_Lote_UserCodTitleControlIdToReplace',fld:'vDDO_LOTE_USERCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Lote_PessoaCodTitleControlIdToReplace',fld:'vDDO_LOTE_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Lote_UserNomTitleControlIdToReplace',fld:'vDDO_LOTE_USERNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Lote_ValorPFTitleControlIdToReplace',fld:'vDDO_LOTE_VALORPFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Lote_DataIniTitleControlIdToReplace',fld:'vDDO_LOTE_DATAINITITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_Lote_DataFimTitleControlIdToReplace',fld:'vDDO_LOTE_DATAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_Lote_QtdeDmnTitleControlIdToReplace',fld:'vDDO_LOTE_QTDEDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_Lote_ValorTitleControlIdToReplace',fld:'vDDO_LOTE_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34Lote_AreaTrabalhoCod',fld:'vLOTE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36Lote_ContratadaCod',fld:'vLOTE_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_lote_qtdedmn_Activeeventkey',ctrl:'DDO_LOTE_QTDEDMN',prop:'ActiveEventKey'},{av:'Ddo_lote_qtdedmn_Filteredtext_get',ctrl:'DDO_LOTE_QTDEDMN',prop:'FilteredText_get'},{av:'Ddo_lote_qtdedmn_Filteredtextto_get',ctrl:'DDO_LOTE_QTDEDMN',prop:'FilteredTextTo_get'}],oparms:[{av:'AV80TFLote_QtdeDmn',fld:'vTFLOTE_QTDEDMN',pic:'ZZZ9',nv:0},{av:'AV81TFLote_QtdeDmn_To',fld:'vTFLOTE_QTDEDMN_TO',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("DDO_LOTE_VALOR.ONOPTIONCLICKED","{handler:'E22BR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Lote_Nome1',fld:'vLOTE_NOME1',pic:'@!',nv:''},{av:'AV18Lote_UserNom1',fld:'vLOTE_USERNOM1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Lote_Nome2',fld:'vLOTE_NOME2',pic:'@!',nv:''},{av:'AV23Lote_UserNom2',fld:'vLOTE_USERNOM2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Lote_Nome3',fld:'vLOTE_NOME3',pic:'@!',nv:''},{av:'AV28Lote_UserNom3',fld:'vLOTE_USERNOM3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV38TFLote_Numero',fld:'vTFLOTE_NUMERO',pic:'',nv:''},{av:'AV39TFLote_Numero_Sel',fld:'vTFLOTE_NUMERO_SEL',pic:'',nv:''},{av:'AV42TFLote_Nome',fld:'vTFLOTE_NOME',pic:'@!',nv:''},{av:'AV43TFLote_Nome_Sel',fld:'vTFLOTE_NOME_SEL',pic:'@!',nv:''},{av:'AV46TFLote_Data',fld:'vTFLOTE_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLote_Data_To',fld:'vTFLOTE_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV52TFLote_UserCod',fld:'vTFLOTE_USERCOD',pic:'ZZZZZ9',nv:0},{av:'AV53TFLote_UserCod_To',fld:'vTFLOTE_USERCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV56TFLote_PessoaCod',fld:'vTFLOTE_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV57TFLote_PessoaCod_To',fld:'vTFLOTE_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV60TFLote_UserNom',fld:'vTFLOTE_USERNOM',pic:'@!',nv:''},{av:'AV61TFLote_UserNom_Sel',fld:'vTFLOTE_USERNOM_SEL',pic:'@!',nv:''},{av:'AV64TFLote_ValorPF',fld:'vTFLOTE_VALORPF',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFLote_ValorPF_To',fld:'vTFLOTE_VALORPF_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFLote_DataIni',fld:'vTFLOTE_DATAINI',pic:'',nv:''},{av:'AV69TFLote_DataIni_To',fld:'vTFLOTE_DATAINI_TO',pic:'',nv:''},{av:'AV74TFLote_DataFim',fld:'vTFLOTE_DATAFIM',pic:'',nv:''},{av:'AV75TFLote_DataFim_To',fld:'vTFLOTE_DATAFIM_TO',pic:'',nv:''},{av:'AV80TFLote_QtdeDmn',fld:'vTFLOTE_QTDEDMN',pic:'ZZZ9',nv:0},{av:'AV81TFLote_QtdeDmn_To',fld:'vTFLOTE_QTDEDMN_TO',pic:'ZZZ9',nv:0},{av:'AV84TFLote_Valor',fld:'vTFLOTE_VALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV85TFLote_Valor_To',fld:'vTFLOTE_VALOR_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV40ddo_Lote_NumeroTitleControlIdToReplace',fld:'vDDO_LOTE_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_Lote_NomeTitleControlIdToReplace',fld:'vDDO_LOTE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Lote_DataTitleControlIdToReplace',fld:'vDDO_LOTE_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_Lote_UserCodTitleControlIdToReplace',fld:'vDDO_LOTE_USERCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Lote_PessoaCodTitleControlIdToReplace',fld:'vDDO_LOTE_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Lote_UserNomTitleControlIdToReplace',fld:'vDDO_LOTE_USERNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Lote_ValorPFTitleControlIdToReplace',fld:'vDDO_LOTE_VALORPFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Lote_DataIniTitleControlIdToReplace',fld:'vDDO_LOTE_DATAINITITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_Lote_DataFimTitleControlIdToReplace',fld:'vDDO_LOTE_DATAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_Lote_QtdeDmnTitleControlIdToReplace',fld:'vDDO_LOTE_QTDEDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_Lote_ValorTitleControlIdToReplace',fld:'vDDO_LOTE_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34Lote_AreaTrabalhoCod',fld:'vLOTE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36Lote_ContratadaCod',fld:'vLOTE_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_lote_valor_Activeeventkey',ctrl:'DDO_LOTE_VALOR',prop:'ActiveEventKey'},{av:'Ddo_lote_valor_Filteredtext_get',ctrl:'DDO_LOTE_VALOR',prop:'FilteredText_get'},{av:'Ddo_lote_valor_Filteredtextto_get',ctrl:'DDO_LOTE_VALOR',prop:'FilteredTextTo_get'}],oparms:[{av:'AV84TFLote_Valor',fld:'vTFLOTE_VALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV85TFLote_Valor_To',fld:'vTFLOTE_VALOR_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0}]}");
         setEventMetadata("GRID.LOAD","{handler:'E35BR2',iparms:[],oparms:[{av:'AV31Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E36BR2',iparms:[{av:'A596Lote_Codigo',fld:'LOTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A563Lote_Nome',fld:'LOTE_NOME',pic:'@!',hsh:true,nv:''}],oparms:[{av:'AV35InOutLote_Codigo',fld:'vINOUTLOTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8InOutLote_Nome',fld:'vINOUTLOTE_NOME',pic:'@!',nv:''}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E23BR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Lote_Nome1',fld:'vLOTE_NOME1',pic:'@!',nv:''},{av:'AV18Lote_UserNom1',fld:'vLOTE_USERNOM1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Lote_Nome2',fld:'vLOTE_NOME2',pic:'@!',nv:''},{av:'AV23Lote_UserNom2',fld:'vLOTE_USERNOM2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Lote_Nome3',fld:'vLOTE_NOME3',pic:'@!',nv:''},{av:'AV28Lote_UserNom3',fld:'vLOTE_USERNOM3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV38TFLote_Numero',fld:'vTFLOTE_NUMERO',pic:'',nv:''},{av:'AV39TFLote_Numero_Sel',fld:'vTFLOTE_NUMERO_SEL',pic:'',nv:''},{av:'AV42TFLote_Nome',fld:'vTFLOTE_NOME',pic:'@!',nv:''},{av:'AV43TFLote_Nome_Sel',fld:'vTFLOTE_NOME_SEL',pic:'@!',nv:''},{av:'AV46TFLote_Data',fld:'vTFLOTE_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLote_Data_To',fld:'vTFLOTE_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV52TFLote_UserCod',fld:'vTFLOTE_USERCOD',pic:'ZZZZZ9',nv:0},{av:'AV53TFLote_UserCod_To',fld:'vTFLOTE_USERCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV56TFLote_PessoaCod',fld:'vTFLOTE_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV57TFLote_PessoaCod_To',fld:'vTFLOTE_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV60TFLote_UserNom',fld:'vTFLOTE_USERNOM',pic:'@!',nv:''},{av:'AV61TFLote_UserNom_Sel',fld:'vTFLOTE_USERNOM_SEL',pic:'@!',nv:''},{av:'AV64TFLote_ValorPF',fld:'vTFLOTE_VALORPF',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFLote_ValorPF_To',fld:'vTFLOTE_VALORPF_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFLote_DataIni',fld:'vTFLOTE_DATAINI',pic:'',nv:''},{av:'AV69TFLote_DataIni_To',fld:'vTFLOTE_DATAINI_TO',pic:'',nv:''},{av:'AV74TFLote_DataFim',fld:'vTFLOTE_DATAFIM',pic:'',nv:''},{av:'AV75TFLote_DataFim_To',fld:'vTFLOTE_DATAFIM_TO',pic:'',nv:''},{av:'AV80TFLote_QtdeDmn',fld:'vTFLOTE_QTDEDMN',pic:'ZZZ9',nv:0},{av:'AV81TFLote_QtdeDmn_To',fld:'vTFLOTE_QTDEDMN_TO',pic:'ZZZ9',nv:0},{av:'AV84TFLote_Valor',fld:'vTFLOTE_VALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV85TFLote_Valor_To',fld:'vTFLOTE_VALOR_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV40ddo_Lote_NumeroTitleControlIdToReplace',fld:'vDDO_LOTE_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_Lote_NomeTitleControlIdToReplace',fld:'vDDO_LOTE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Lote_DataTitleControlIdToReplace',fld:'vDDO_LOTE_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_Lote_UserCodTitleControlIdToReplace',fld:'vDDO_LOTE_USERCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Lote_PessoaCodTitleControlIdToReplace',fld:'vDDO_LOTE_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Lote_UserNomTitleControlIdToReplace',fld:'vDDO_LOTE_USERNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Lote_ValorPFTitleControlIdToReplace',fld:'vDDO_LOTE_VALORPFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Lote_DataIniTitleControlIdToReplace',fld:'vDDO_LOTE_DATAINITITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_Lote_DataFimTitleControlIdToReplace',fld:'vDDO_LOTE_DATAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_Lote_QtdeDmnTitleControlIdToReplace',fld:'vDDO_LOTE_QTDEDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_Lote_ValorTitleControlIdToReplace',fld:'vDDO_LOTE_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34Lote_AreaTrabalhoCod',fld:'vLOTE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36Lote_ContratadaCod',fld:'vLOTE_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E28BR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Lote_Nome1',fld:'vLOTE_NOME1',pic:'@!',nv:''},{av:'AV18Lote_UserNom1',fld:'vLOTE_USERNOM1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Lote_Nome2',fld:'vLOTE_NOME2',pic:'@!',nv:''},{av:'AV23Lote_UserNom2',fld:'vLOTE_USERNOM2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Lote_Nome3',fld:'vLOTE_NOME3',pic:'@!',nv:''},{av:'AV28Lote_UserNom3',fld:'vLOTE_USERNOM3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV38TFLote_Numero',fld:'vTFLOTE_NUMERO',pic:'',nv:''},{av:'AV39TFLote_Numero_Sel',fld:'vTFLOTE_NUMERO_SEL',pic:'',nv:''},{av:'AV42TFLote_Nome',fld:'vTFLOTE_NOME',pic:'@!',nv:''},{av:'AV43TFLote_Nome_Sel',fld:'vTFLOTE_NOME_SEL',pic:'@!',nv:''},{av:'AV46TFLote_Data',fld:'vTFLOTE_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLote_Data_To',fld:'vTFLOTE_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV52TFLote_UserCod',fld:'vTFLOTE_USERCOD',pic:'ZZZZZ9',nv:0},{av:'AV53TFLote_UserCod_To',fld:'vTFLOTE_USERCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV56TFLote_PessoaCod',fld:'vTFLOTE_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV57TFLote_PessoaCod_To',fld:'vTFLOTE_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV60TFLote_UserNom',fld:'vTFLOTE_USERNOM',pic:'@!',nv:''},{av:'AV61TFLote_UserNom_Sel',fld:'vTFLOTE_USERNOM_SEL',pic:'@!',nv:''},{av:'AV64TFLote_ValorPF',fld:'vTFLOTE_VALORPF',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFLote_ValorPF_To',fld:'vTFLOTE_VALORPF_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFLote_DataIni',fld:'vTFLOTE_DATAINI',pic:'',nv:''},{av:'AV69TFLote_DataIni_To',fld:'vTFLOTE_DATAINI_TO',pic:'',nv:''},{av:'AV74TFLote_DataFim',fld:'vTFLOTE_DATAFIM',pic:'',nv:''},{av:'AV75TFLote_DataFim_To',fld:'vTFLOTE_DATAFIM_TO',pic:'',nv:''},{av:'AV80TFLote_QtdeDmn',fld:'vTFLOTE_QTDEDMN',pic:'ZZZ9',nv:0},{av:'AV81TFLote_QtdeDmn_To',fld:'vTFLOTE_QTDEDMN_TO',pic:'ZZZ9',nv:0},{av:'AV84TFLote_Valor',fld:'vTFLOTE_VALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV85TFLote_Valor_To',fld:'vTFLOTE_VALOR_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV40ddo_Lote_NumeroTitleControlIdToReplace',fld:'vDDO_LOTE_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_Lote_NomeTitleControlIdToReplace',fld:'vDDO_LOTE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Lote_DataTitleControlIdToReplace',fld:'vDDO_LOTE_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_Lote_UserCodTitleControlIdToReplace',fld:'vDDO_LOTE_USERCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Lote_PessoaCodTitleControlIdToReplace',fld:'vDDO_LOTE_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Lote_UserNomTitleControlIdToReplace',fld:'vDDO_LOTE_USERNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Lote_ValorPFTitleControlIdToReplace',fld:'vDDO_LOTE_VALORPFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Lote_DataIniTitleControlIdToReplace',fld:'vDDO_LOTE_DATAINITITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_Lote_DataFimTitleControlIdToReplace',fld:'vDDO_LOTE_DATAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_Lote_QtdeDmnTitleControlIdToReplace',fld:'vDDO_LOTE_QTDEDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_Lote_ValorTitleControlIdToReplace',fld:'vDDO_LOTE_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34Lote_AreaTrabalhoCod',fld:'vLOTE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36Lote_ContratadaCod',fld:'vLOTE_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E24BR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Lote_Nome1',fld:'vLOTE_NOME1',pic:'@!',nv:''},{av:'AV18Lote_UserNom1',fld:'vLOTE_USERNOM1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Lote_Nome2',fld:'vLOTE_NOME2',pic:'@!',nv:''},{av:'AV23Lote_UserNom2',fld:'vLOTE_USERNOM2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Lote_Nome3',fld:'vLOTE_NOME3',pic:'@!',nv:''},{av:'AV28Lote_UserNom3',fld:'vLOTE_USERNOM3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV38TFLote_Numero',fld:'vTFLOTE_NUMERO',pic:'',nv:''},{av:'AV39TFLote_Numero_Sel',fld:'vTFLOTE_NUMERO_SEL',pic:'',nv:''},{av:'AV42TFLote_Nome',fld:'vTFLOTE_NOME',pic:'@!',nv:''},{av:'AV43TFLote_Nome_Sel',fld:'vTFLOTE_NOME_SEL',pic:'@!',nv:''},{av:'AV46TFLote_Data',fld:'vTFLOTE_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLote_Data_To',fld:'vTFLOTE_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV52TFLote_UserCod',fld:'vTFLOTE_USERCOD',pic:'ZZZZZ9',nv:0},{av:'AV53TFLote_UserCod_To',fld:'vTFLOTE_USERCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV56TFLote_PessoaCod',fld:'vTFLOTE_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV57TFLote_PessoaCod_To',fld:'vTFLOTE_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV60TFLote_UserNom',fld:'vTFLOTE_USERNOM',pic:'@!',nv:''},{av:'AV61TFLote_UserNom_Sel',fld:'vTFLOTE_USERNOM_SEL',pic:'@!',nv:''},{av:'AV64TFLote_ValorPF',fld:'vTFLOTE_VALORPF',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFLote_ValorPF_To',fld:'vTFLOTE_VALORPF_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFLote_DataIni',fld:'vTFLOTE_DATAINI',pic:'',nv:''},{av:'AV69TFLote_DataIni_To',fld:'vTFLOTE_DATAINI_TO',pic:'',nv:''},{av:'AV74TFLote_DataFim',fld:'vTFLOTE_DATAFIM',pic:'',nv:''},{av:'AV75TFLote_DataFim_To',fld:'vTFLOTE_DATAFIM_TO',pic:'',nv:''},{av:'AV80TFLote_QtdeDmn',fld:'vTFLOTE_QTDEDMN',pic:'ZZZ9',nv:0},{av:'AV81TFLote_QtdeDmn_To',fld:'vTFLOTE_QTDEDMN_TO',pic:'ZZZ9',nv:0},{av:'AV84TFLote_Valor',fld:'vTFLOTE_VALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV85TFLote_Valor_To',fld:'vTFLOTE_VALOR_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV40ddo_Lote_NumeroTitleControlIdToReplace',fld:'vDDO_LOTE_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_Lote_NomeTitleControlIdToReplace',fld:'vDDO_LOTE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Lote_DataTitleControlIdToReplace',fld:'vDDO_LOTE_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_Lote_UserCodTitleControlIdToReplace',fld:'vDDO_LOTE_USERCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Lote_PessoaCodTitleControlIdToReplace',fld:'vDDO_LOTE_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Lote_UserNomTitleControlIdToReplace',fld:'vDDO_LOTE_USERNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Lote_ValorPFTitleControlIdToReplace',fld:'vDDO_LOTE_VALORPFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Lote_DataIniTitleControlIdToReplace',fld:'vDDO_LOTE_DATAINITITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_Lote_DataFimTitleControlIdToReplace',fld:'vDDO_LOTE_DATAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_Lote_QtdeDmnTitleControlIdToReplace',fld:'vDDO_LOTE_QTDEDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_Lote_ValorTitleControlIdToReplace',fld:'vDDO_LOTE_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34Lote_AreaTrabalhoCod',fld:'vLOTE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36Lote_ContratadaCod',fld:'vLOTE_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Lote_Nome2',fld:'vLOTE_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Lote_Nome3',fld:'vLOTE_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Lote_Nome1',fld:'vLOTE_NOME1',pic:'@!',nv:''},{av:'AV18Lote_UserNom1',fld:'vLOTE_USERNOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV23Lote_UserNom2',fld:'vLOTE_USERNOM2',pic:'@!',nv:''},{av:'AV28Lote_UserNom3',fld:'vLOTE_USERNOM3',pic:'@!',nv:''},{av:'edtavLote_nome2_Visible',ctrl:'vLOTE_NOME2',prop:'Visible'},{av:'edtavLote_usernom2_Visible',ctrl:'vLOTE_USERNOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavLote_nome3_Visible',ctrl:'vLOTE_NOME3',prop:'Visible'},{av:'edtavLote_usernom3_Visible',ctrl:'vLOTE_USERNOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavLote_nome1_Visible',ctrl:'vLOTE_NOME1',prop:'Visible'},{av:'edtavLote_usernom1_Visible',ctrl:'vLOTE_USERNOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E29BR2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'edtavLote_nome1_Visible',ctrl:'vLOTE_NOME1',prop:'Visible'},{av:'edtavLote_usernom1_Visible',ctrl:'vLOTE_USERNOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E30BR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Lote_Nome1',fld:'vLOTE_NOME1',pic:'@!',nv:''},{av:'AV18Lote_UserNom1',fld:'vLOTE_USERNOM1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Lote_Nome2',fld:'vLOTE_NOME2',pic:'@!',nv:''},{av:'AV23Lote_UserNom2',fld:'vLOTE_USERNOM2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Lote_Nome3',fld:'vLOTE_NOME3',pic:'@!',nv:''},{av:'AV28Lote_UserNom3',fld:'vLOTE_USERNOM3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV38TFLote_Numero',fld:'vTFLOTE_NUMERO',pic:'',nv:''},{av:'AV39TFLote_Numero_Sel',fld:'vTFLOTE_NUMERO_SEL',pic:'',nv:''},{av:'AV42TFLote_Nome',fld:'vTFLOTE_NOME',pic:'@!',nv:''},{av:'AV43TFLote_Nome_Sel',fld:'vTFLOTE_NOME_SEL',pic:'@!',nv:''},{av:'AV46TFLote_Data',fld:'vTFLOTE_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLote_Data_To',fld:'vTFLOTE_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV52TFLote_UserCod',fld:'vTFLOTE_USERCOD',pic:'ZZZZZ9',nv:0},{av:'AV53TFLote_UserCod_To',fld:'vTFLOTE_USERCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV56TFLote_PessoaCod',fld:'vTFLOTE_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV57TFLote_PessoaCod_To',fld:'vTFLOTE_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV60TFLote_UserNom',fld:'vTFLOTE_USERNOM',pic:'@!',nv:''},{av:'AV61TFLote_UserNom_Sel',fld:'vTFLOTE_USERNOM_SEL',pic:'@!',nv:''},{av:'AV64TFLote_ValorPF',fld:'vTFLOTE_VALORPF',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFLote_ValorPF_To',fld:'vTFLOTE_VALORPF_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFLote_DataIni',fld:'vTFLOTE_DATAINI',pic:'',nv:''},{av:'AV69TFLote_DataIni_To',fld:'vTFLOTE_DATAINI_TO',pic:'',nv:''},{av:'AV74TFLote_DataFim',fld:'vTFLOTE_DATAFIM',pic:'',nv:''},{av:'AV75TFLote_DataFim_To',fld:'vTFLOTE_DATAFIM_TO',pic:'',nv:''},{av:'AV80TFLote_QtdeDmn',fld:'vTFLOTE_QTDEDMN',pic:'ZZZ9',nv:0},{av:'AV81TFLote_QtdeDmn_To',fld:'vTFLOTE_QTDEDMN_TO',pic:'ZZZ9',nv:0},{av:'AV84TFLote_Valor',fld:'vTFLOTE_VALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV85TFLote_Valor_To',fld:'vTFLOTE_VALOR_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV40ddo_Lote_NumeroTitleControlIdToReplace',fld:'vDDO_LOTE_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_Lote_NomeTitleControlIdToReplace',fld:'vDDO_LOTE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Lote_DataTitleControlIdToReplace',fld:'vDDO_LOTE_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_Lote_UserCodTitleControlIdToReplace',fld:'vDDO_LOTE_USERCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Lote_PessoaCodTitleControlIdToReplace',fld:'vDDO_LOTE_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Lote_UserNomTitleControlIdToReplace',fld:'vDDO_LOTE_USERNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Lote_ValorPFTitleControlIdToReplace',fld:'vDDO_LOTE_VALORPFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Lote_DataIniTitleControlIdToReplace',fld:'vDDO_LOTE_DATAINITITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_Lote_DataFimTitleControlIdToReplace',fld:'vDDO_LOTE_DATAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_Lote_QtdeDmnTitleControlIdToReplace',fld:'vDDO_LOTE_QTDEDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_Lote_ValorTitleControlIdToReplace',fld:'vDDO_LOTE_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34Lote_AreaTrabalhoCod',fld:'vLOTE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36Lote_ContratadaCod',fld:'vLOTE_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E25BR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Lote_Nome1',fld:'vLOTE_NOME1',pic:'@!',nv:''},{av:'AV18Lote_UserNom1',fld:'vLOTE_USERNOM1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Lote_Nome2',fld:'vLOTE_NOME2',pic:'@!',nv:''},{av:'AV23Lote_UserNom2',fld:'vLOTE_USERNOM2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Lote_Nome3',fld:'vLOTE_NOME3',pic:'@!',nv:''},{av:'AV28Lote_UserNom3',fld:'vLOTE_USERNOM3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV38TFLote_Numero',fld:'vTFLOTE_NUMERO',pic:'',nv:''},{av:'AV39TFLote_Numero_Sel',fld:'vTFLOTE_NUMERO_SEL',pic:'',nv:''},{av:'AV42TFLote_Nome',fld:'vTFLOTE_NOME',pic:'@!',nv:''},{av:'AV43TFLote_Nome_Sel',fld:'vTFLOTE_NOME_SEL',pic:'@!',nv:''},{av:'AV46TFLote_Data',fld:'vTFLOTE_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLote_Data_To',fld:'vTFLOTE_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV52TFLote_UserCod',fld:'vTFLOTE_USERCOD',pic:'ZZZZZ9',nv:0},{av:'AV53TFLote_UserCod_To',fld:'vTFLOTE_USERCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV56TFLote_PessoaCod',fld:'vTFLOTE_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV57TFLote_PessoaCod_To',fld:'vTFLOTE_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV60TFLote_UserNom',fld:'vTFLOTE_USERNOM',pic:'@!',nv:''},{av:'AV61TFLote_UserNom_Sel',fld:'vTFLOTE_USERNOM_SEL',pic:'@!',nv:''},{av:'AV64TFLote_ValorPF',fld:'vTFLOTE_VALORPF',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFLote_ValorPF_To',fld:'vTFLOTE_VALORPF_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFLote_DataIni',fld:'vTFLOTE_DATAINI',pic:'',nv:''},{av:'AV69TFLote_DataIni_To',fld:'vTFLOTE_DATAINI_TO',pic:'',nv:''},{av:'AV74TFLote_DataFim',fld:'vTFLOTE_DATAFIM',pic:'',nv:''},{av:'AV75TFLote_DataFim_To',fld:'vTFLOTE_DATAFIM_TO',pic:'',nv:''},{av:'AV80TFLote_QtdeDmn',fld:'vTFLOTE_QTDEDMN',pic:'ZZZ9',nv:0},{av:'AV81TFLote_QtdeDmn_To',fld:'vTFLOTE_QTDEDMN_TO',pic:'ZZZ9',nv:0},{av:'AV84TFLote_Valor',fld:'vTFLOTE_VALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV85TFLote_Valor_To',fld:'vTFLOTE_VALOR_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV40ddo_Lote_NumeroTitleControlIdToReplace',fld:'vDDO_LOTE_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_Lote_NomeTitleControlIdToReplace',fld:'vDDO_LOTE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Lote_DataTitleControlIdToReplace',fld:'vDDO_LOTE_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_Lote_UserCodTitleControlIdToReplace',fld:'vDDO_LOTE_USERCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Lote_PessoaCodTitleControlIdToReplace',fld:'vDDO_LOTE_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Lote_UserNomTitleControlIdToReplace',fld:'vDDO_LOTE_USERNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Lote_ValorPFTitleControlIdToReplace',fld:'vDDO_LOTE_VALORPFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Lote_DataIniTitleControlIdToReplace',fld:'vDDO_LOTE_DATAINITITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_Lote_DataFimTitleControlIdToReplace',fld:'vDDO_LOTE_DATAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_Lote_QtdeDmnTitleControlIdToReplace',fld:'vDDO_LOTE_QTDEDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_Lote_ValorTitleControlIdToReplace',fld:'vDDO_LOTE_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34Lote_AreaTrabalhoCod',fld:'vLOTE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36Lote_ContratadaCod',fld:'vLOTE_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Lote_Nome2',fld:'vLOTE_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Lote_Nome3',fld:'vLOTE_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Lote_Nome1',fld:'vLOTE_NOME1',pic:'@!',nv:''},{av:'AV18Lote_UserNom1',fld:'vLOTE_USERNOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV23Lote_UserNom2',fld:'vLOTE_USERNOM2',pic:'@!',nv:''},{av:'AV28Lote_UserNom3',fld:'vLOTE_USERNOM3',pic:'@!',nv:''},{av:'edtavLote_nome2_Visible',ctrl:'vLOTE_NOME2',prop:'Visible'},{av:'edtavLote_usernom2_Visible',ctrl:'vLOTE_USERNOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavLote_nome3_Visible',ctrl:'vLOTE_NOME3',prop:'Visible'},{av:'edtavLote_usernom3_Visible',ctrl:'vLOTE_USERNOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavLote_nome1_Visible',ctrl:'vLOTE_NOME1',prop:'Visible'},{av:'edtavLote_usernom1_Visible',ctrl:'vLOTE_USERNOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E31BR2',iparms:[{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'edtavLote_nome2_Visible',ctrl:'vLOTE_NOME2',prop:'Visible'},{av:'edtavLote_usernom2_Visible',ctrl:'vLOTE_USERNOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E26BR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Lote_Nome1',fld:'vLOTE_NOME1',pic:'@!',nv:''},{av:'AV18Lote_UserNom1',fld:'vLOTE_USERNOM1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Lote_Nome2',fld:'vLOTE_NOME2',pic:'@!',nv:''},{av:'AV23Lote_UserNom2',fld:'vLOTE_USERNOM2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Lote_Nome3',fld:'vLOTE_NOME3',pic:'@!',nv:''},{av:'AV28Lote_UserNom3',fld:'vLOTE_USERNOM3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV38TFLote_Numero',fld:'vTFLOTE_NUMERO',pic:'',nv:''},{av:'AV39TFLote_Numero_Sel',fld:'vTFLOTE_NUMERO_SEL',pic:'',nv:''},{av:'AV42TFLote_Nome',fld:'vTFLOTE_NOME',pic:'@!',nv:''},{av:'AV43TFLote_Nome_Sel',fld:'vTFLOTE_NOME_SEL',pic:'@!',nv:''},{av:'AV46TFLote_Data',fld:'vTFLOTE_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLote_Data_To',fld:'vTFLOTE_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV52TFLote_UserCod',fld:'vTFLOTE_USERCOD',pic:'ZZZZZ9',nv:0},{av:'AV53TFLote_UserCod_To',fld:'vTFLOTE_USERCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV56TFLote_PessoaCod',fld:'vTFLOTE_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV57TFLote_PessoaCod_To',fld:'vTFLOTE_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV60TFLote_UserNom',fld:'vTFLOTE_USERNOM',pic:'@!',nv:''},{av:'AV61TFLote_UserNom_Sel',fld:'vTFLOTE_USERNOM_SEL',pic:'@!',nv:''},{av:'AV64TFLote_ValorPF',fld:'vTFLOTE_VALORPF',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFLote_ValorPF_To',fld:'vTFLOTE_VALORPF_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFLote_DataIni',fld:'vTFLOTE_DATAINI',pic:'',nv:''},{av:'AV69TFLote_DataIni_To',fld:'vTFLOTE_DATAINI_TO',pic:'',nv:''},{av:'AV74TFLote_DataFim',fld:'vTFLOTE_DATAFIM',pic:'',nv:''},{av:'AV75TFLote_DataFim_To',fld:'vTFLOTE_DATAFIM_TO',pic:'',nv:''},{av:'AV80TFLote_QtdeDmn',fld:'vTFLOTE_QTDEDMN',pic:'ZZZ9',nv:0},{av:'AV81TFLote_QtdeDmn_To',fld:'vTFLOTE_QTDEDMN_TO',pic:'ZZZ9',nv:0},{av:'AV84TFLote_Valor',fld:'vTFLOTE_VALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV85TFLote_Valor_To',fld:'vTFLOTE_VALOR_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV40ddo_Lote_NumeroTitleControlIdToReplace',fld:'vDDO_LOTE_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_Lote_NomeTitleControlIdToReplace',fld:'vDDO_LOTE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Lote_DataTitleControlIdToReplace',fld:'vDDO_LOTE_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_Lote_UserCodTitleControlIdToReplace',fld:'vDDO_LOTE_USERCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Lote_PessoaCodTitleControlIdToReplace',fld:'vDDO_LOTE_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Lote_UserNomTitleControlIdToReplace',fld:'vDDO_LOTE_USERNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Lote_ValorPFTitleControlIdToReplace',fld:'vDDO_LOTE_VALORPFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Lote_DataIniTitleControlIdToReplace',fld:'vDDO_LOTE_DATAINITITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_Lote_DataFimTitleControlIdToReplace',fld:'vDDO_LOTE_DATAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_Lote_QtdeDmnTitleControlIdToReplace',fld:'vDDO_LOTE_QTDEDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_Lote_ValorTitleControlIdToReplace',fld:'vDDO_LOTE_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34Lote_AreaTrabalhoCod',fld:'vLOTE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36Lote_ContratadaCod',fld:'vLOTE_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Lote_Nome2',fld:'vLOTE_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Lote_Nome3',fld:'vLOTE_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Lote_Nome1',fld:'vLOTE_NOME1',pic:'@!',nv:''},{av:'AV18Lote_UserNom1',fld:'vLOTE_USERNOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV23Lote_UserNom2',fld:'vLOTE_USERNOM2',pic:'@!',nv:''},{av:'AV28Lote_UserNom3',fld:'vLOTE_USERNOM3',pic:'@!',nv:''},{av:'edtavLote_nome2_Visible',ctrl:'vLOTE_NOME2',prop:'Visible'},{av:'edtavLote_usernom2_Visible',ctrl:'vLOTE_USERNOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavLote_nome3_Visible',ctrl:'vLOTE_NOME3',prop:'Visible'},{av:'edtavLote_usernom3_Visible',ctrl:'vLOTE_USERNOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavLote_nome1_Visible',ctrl:'vLOTE_NOME1',prop:'Visible'},{av:'edtavLote_usernom1_Visible',ctrl:'vLOTE_USERNOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E32BR2',iparms:[{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtavLote_nome3_Visible',ctrl:'vLOTE_NOME3',prop:'Visible'},{av:'edtavLote_usernom3_Visible',ctrl:'vLOTE_USERNOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E27BR2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Lote_Nome1',fld:'vLOTE_NOME1',pic:'@!',nv:''},{av:'AV18Lote_UserNom1',fld:'vLOTE_USERNOM1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Lote_Nome2',fld:'vLOTE_NOME2',pic:'@!',nv:''},{av:'AV23Lote_UserNom2',fld:'vLOTE_USERNOM2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Lote_Nome3',fld:'vLOTE_NOME3',pic:'@!',nv:''},{av:'AV28Lote_UserNom3',fld:'vLOTE_USERNOM3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV38TFLote_Numero',fld:'vTFLOTE_NUMERO',pic:'',nv:''},{av:'AV39TFLote_Numero_Sel',fld:'vTFLOTE_NUMERO_SEL',pic:'',nv:''},{av:'AV42TFLote_Nome',fld:'vTFLOTE_NOME',pic:'@!',nv:''},{av:'AV43TFLote_Nome_Sel',fld:'vTFLOTE_NOME_SEL',pic:'@!',nv:''},{av:'AV46TFLote_Data',fld:'vTFLOTE_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV47TFLote_Data_To',fld:'vTFLOTE_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV52TFLote_UserCod',fld:'vTFLOTE_USERCOD',pic:'ZZZZZ9',nv:0},{av:'AV53TFLote_UserCod_To',fld:'vTFLOTE_USERCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV56TFLote_PessoaCod',fld:'vTFLOTE_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV57TFLote_PessoaCod_To',fld:'vTFLOTE_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV60TFLote_UserNom',fld:'vTFLOTE_USERNOM',pic:'@!',nv:''},{av:'AV61TFLote_UserNom_Sel',fld:'vTFLOTE_USERNOM_SEL',pic:'@!',nv:''},{av:'AV64TFLote_ValorPF',fld:'vTFLOTE_VALORPF',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV65TFLote_ValorPF_To',fld:'vTFLOTE_VALORPF_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV68TFLote_DataIni',fld:'vTFLOTE_DATAINI',pic:'',nv:''},{av:'AV69TFLote_DataIni_To',fld:'vTFLOTE_DATAINI_TO',pic:'',nv:''},{av:'AV74TFLote_DataFim',fld:'vTFLOTE_DATAFIM',pic:'',nv:''},{av:'AV75TFLote_DataFim_To',fld:'vTFLOTE_DATAFIM_TO',pic:'',nv:''},{av:'AV80TFLote_QtdeDmn',fld:'vTFLOTE_QTDEDMN',pic:'ZZZ9',nv:0},{av:'AV81TFLote_QtdeDmn_To',fld:'vTFLOTE_QTDEDMN_TO',pic:'ZZZ9',nv:0},{av:'AV84TFLote_Valor',fld:'vTFLOTE_VALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV85TFLote_Valor_To',fld:'vTFLOTE_VALOR_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV40ddo_Lote_NumeroTitleControlIdToReplace',fld:'vDDO_LOTE_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44ddo_Lote_NomeTitleControlIdToReplace',fld:'vDDO_LOTE_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Lote_DataTitleControlIdToReplace',fld:'vDDO_LOTE_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_Lote_UserCodTitleControlIdToReplace',fld:'vDDO_LOTE_USERCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Lote_PessoaCodTitleControlIdToReplace',fld:'vDDO_LOTE_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Lote_UserNomTitleControlIdToReplace',fld:'vDDO_LOTE_USERNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Lote_ValorPFTitleControlIdToReplace',fld:'vDDO_LOTE_VALORPFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Lote_DataIniTitleControlIdToReplace',fld:'vDDO_LOTE_DATAINITITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_Lote_DataFimTitleControlIdToReplace',fld:'vDDO_LOTE_DATAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_Lote_QtdeDmnTitleControlIdToReplace',fld:'vDDO_LOTE_QTDEDMNTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV86ddo_Lote_ValorTitleControlIdToReplace',fld:'vDDO_LOTE_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34Lote_AreaTrabalhoCod',fld:'vLOTE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36Lote_ContratadaCod',fld:'vLOTE_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV34Lote_AreaTrabalhoCod',fld:'vLOTE_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV36Lote_ContratadaCod',fld:'vLOTE_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV38TFLote_Numero',fld:'vTFLOTE_NUMERO',pic:'',nv:''},{av:'Ddo_lote_numero_Filteredtext_set',ctrl:'DDO_LOTE_NUMERO',prop:'FilteredText_set'},{av:'AV39TFLote_Numero_Sel',fld:'vTFLOTE_NUMERO_SEL',pic:'',nv:''},{av:'Ddo_lote_numero_Selectedvalue_set',ctrl:'DDO_LOTE_NUMERO',prop:'SelectedValue_set'},{av:'AV42TFLote_Nome',fld:'vTFLOTE_NOME',pic:'@!',nv:''},{av:'Ddo_lote_nome_Filteredtext_set',ctrl:'DDO_LOTE_NOME',prop:'FilteredText_set'},{av:'AV43TFLote_Nome_Sel',fld:'vTFLOTE_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_lote_nome_Selectedvalue_set',ctrl:'DDO_LOTE_NOME',prop:'SelectedValue_set'},{av:'AV46TFLote_Data',fld:'vTFLOTE_DATA',pic:'99/99/99 99:99',nv:''},{av:'Ddo_lote_data_Filteredtext_set',ctrl:'DDO_LOTE_DATA',prop:'FilteredText_set'},{av:'AV47TFLote_Data_To',fld:'vTFLOTE_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_lote_data_Filteredtextto_set',ctrl:'DDO_LOTE_DATA',prop:'FilteredTextTo_set'},{av:'AV52TFLote_UserCod',fld:'vTFLOTE_USERCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_lote_usercod_Filteredtext_set',ctrl:'DDO_LOTE_USERCOD',prop:'FilteredText_set'},{av:'AV53TFLote_UserCod_To',fld:'vTFLOTE_USERCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_lote_usercod_Filteredtextto_set',ctrl:'DDO_LOTE_USERCOD',prop:'FilteredTextTo_set'},{av:'AV56TFLote_PessoaCod',fld:'vTFLOTE_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_lote_pessoacod_Filteredtext_set',ctrl:'DDO_LOTE_PESSOACOD',prop:'FilteredText_set'},{av:'AV57TFLote_PessoaCod_To',fld:'vTFLOTE_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_lote_pessoacod_Filteredtextto_set',ctrl:'DDO_LOTE_PESSOACOD',prop:'FilteredTextTo_set'},{av:'AV60TFLote_UserNom',fld:'vTFLOTE_USERNOM',pic:'@!',nv:''},{av:'Ddo_lote_usernom_Filteredtext_set',ctrl:'DDO_LOTE_USERNOM',prop:'FilteredText_set'},{av:'AV61TFLote_UserNom_Sel',fld:'vTFLOTE_USERNOM_SEL',pic:'@!',nv:''},{av:'Ddo_lote_usernom_Selectedvalue_set',ctrl:'DDO_LOTE_USERNOM',prop:'SelectedValue_set'},{av:'AV64TFLote_ValorPF',fld:'vTFLOTE_VALORPF',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_lote_valorpf_Filteredtext_set',ctrl:'DDO_LOTE_VALORPF',prop:'FilteredText_set'},{av:'AV65TFLote_ValorPF_To',fld:'vTFLOTE_VALORPF_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_lote_valorpf_Filteredtextto_set',ctrl:'DDO_LOTE_VALORPF',prop:'FilteredTextTo_set'},{av:'AV68TFLote_DataIni',fld:'vTFLOTE_DATAINI',pic:'',nv:''},{av:'Ddo_lote_dataini_Filteredtext_set',ctrl:'DDO_LOTE_DATAINI',prop:'FilteredText_set'},{av:'AV69TFLote_DataIni_To',fld:'vTFLOTE_DATAINI_TO',pic:'',nv:''},{av:'Ddo_lote_dataini_Filteredtextto_set',ctrl:'DDO_LOTE_DATAINI',prop:'FilteredTextTo_set'},{av:'AV74TFLote_DataFim',fld:'vTFLOTE_DATAFIM',pic:'',nv:''},{av:'Ddo_lote_datafim_Filteredtext_set',ctrl:'DDO_LOTE_DATAFIM',prop:'FilteredText_set'},{av:'AV75TFLote_DataFim_To',fld:'vTFLOTE_DATAFIM_TO',pic:'',nv:''},{av:'Ddo_lote_datafim_Filteredtextto_set',ctrl:'DDO_LOTE_DATAFIM',prop:'FilteredTextTo_set'},{av:'AV80TFLote_QtdeDmn',fld:'vTFLOTE_QTDEDMN',pic:'ZZZ9',nv:0},{av:'Ddo_lote_qtdedmn_Filteredtext_set',ctrl:'DDO_LOTE_QTDEDMN',prop:'FilteredText_set'},{av:'AV81TFLote_QtdeDmn_To',fld:'vTFLOTE_QTDEDMN_TO',pic:'ZZZ9',nv:0},{av:'Ddo_lote_qtdedmn_Filteredtextto_set',ctrl:'DDO_LOTE_QTDEDMN',prop:'FilteredTextTo_set'},{av:'AV84TFLote_Valor',fld:'vTFLOTE_VALOR',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_lote_valor_Filteredtext_set',ctrl:'DDO_LOTE_VALOR',prop:'FilteredText_set'},{av:'AV85TFLote_Valor_To',fld:'vTFLOTE_VALOR_TO',pic:'ZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_lote_valor_Filteredtextto_set',ctrl:'DDO_LOTE_VALOR',prop:'FilteredTextTo_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Lote_Nome1',fld:'vLOTE_NOME1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavLote_nome1_Visible',ctrl:'vLOTE_NOME1',prop:'Visible'},{av:'edtavLote_usernom1_Visible',ctrl:'vLOTE_USERNOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22Lote_Nome2',fld:'vLOTE_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27Lote_Nome3',fld:'vLOTE_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV18Lote_UserNom1',fld:'vLOTE_USERNOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV23Lote_UserNom2',fld:'vLOTE_USERNOM2',pic:'@!',nv:''},{av:'AV28Lote_UserNom3',fld:'vLOTE_USERNOM3',pic:'@!',nv:''},{av:'edtavLote_nome2_Visible',ctrl:'vLOTE_NOME2',prop:'Visible'},{av:'edtavLote_usernom2_Visible',ctrl:'vLOTE_USERNOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavLote_nome3_Visible',ctrl:'vLOTE_NOME3',prop:'Visible'},{av:'edtavLote_usernom3_Visible',ctrl:'vLOTE_USERNOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV8InOutLote_Nome = "";
         Gridpaginationbar_Selectedpage = "";
         Ddo_lote_numero_Activeeventkey = "";
         Ddo_lote_numero_Filteredtext_get = "";
         Ddo_lote_numero_Selectedvalue_get = "";
         Ddo_lote_nome_Activeeventkey = "";
         Ddo_lote_nome_Filteredtext_get = "";
         Ddo_lote_nome_Selectedvalue_get = "";
         Ddo_lote_data_Activeeventkey = "";
         Ddo_lote_data_Filteredtext_get = "";
         Ddo_lote_data_Filteredtextto_get = "";
         Ddo_lote_usercod_Activeeventkey = "";
         Ddo_lote_usercod_Filteredtext_get = "";
         Ddo_lote_usercod_Filteredtextto_get = "";
         Ddo_lote_pessoacod_Activeeventkey = "";
         Ddo_lote_pessoacod_Filteredtext_get = "";
         Ddo_lote_pessoacod_Filteredtextto_get = "";
         Ddo_lote_usernom_Activeeventkey = "";
         Ddo_lote_usernom_Filteredtext_get = "";
         Ddo_lote_usernom_Selectedvalue_get = "";
         Ddo_lote_valorpf_Activeeventkey = "";
         Ddo_lote_valorpf_Filteredtext_get = "";
         Ddo_lote_valorpf_Filteredtextto_get = "";
         Ddo_lote_dataini_Activeeventkey = "";
         Ddo_lote_dataini_Filteredtext_get = "";
         Ddo_lote_dataini_Filteredtextto_get = "";
         Ddo_lote_datafim_Activeeventkey = "";
         Ddo_lote_datafim_Filteredtext_get = "";
         Ddo_lote_datafim_Filteredtextto_get = "";
         Ddo_lote_qtdedmn_Activeeventkey = "";
         Ddo_lote_qtdedmn_Filteredtext_get = "";
         Ddo_lote_qtdedmn_Filteredtextto_get = "";
         Ddo_lote_valor_Activeeventkey = "";
         Ddo_lote_valor_Filteredtext_get = "";
         Ddo_lote_valor_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17Lote_Nome1 = "";
         AV18Lote_UserNom1 = "";
         AV20DynamicFiltersSelector2 = "";
         AV22Lote_Nome2 = "";
         AV23Lote_UserNom2 = "";
         AV25DynamicFiltersSelector3 = "";
         AV27Lote_Nome3 = "";
         AV28Lote_UserNom3 = "";
         AV38TFLote_Numero = "";
         AV39TFLote_Numero_Sel = "";
         AV42TFLote_Nome = "";
         AV43TFLote_Nome_Sel = "";
         AV46TFLote_Data = (DateTime)(DateTime.MinValue);
         AV47TFLote_Data_To = (DateTime)(DateTime.MinValue);
         AV60TFLote_UserNom = "";
         AV61TFLote_UserNom_Sel = "";
         AV68TFLote_DataIni = DateTime.MinValue;
         AV69TFLote_DataIni_To = DateTime.MinValue;
         AV74TFLote_DataFim = DateTime.MinValue;
         AV75TFLote_DataFim_To = DateTime.MinValue;
         AV40ddo_Lote_NumeroTitleControlIdToReplace = "";
         AV44ddo_Lote_NomeTitleControlIdToReplace = "";
         AV50ddo_Lote_DataTitleControlIdToReplace = "";
         AV54ddo_Lote_UserCodTitleControlIdToReplace = "";
         AV58ddo_Lote_PessoaCodTitleControlIdToReplace = "";
         AV62ddo_Lote_UserNomTitleControlIdToReplace = "";
         AV66ddo_Lote_ValorPFTitleControlIdToReplace = "";
         AV72ddo_Lote_DataIniTitleControlIdToReplace = "";
         AV78ddo_Lote_DataFimTitleControlIdToReplace = "";
         AV82ddo_Lote_QtdeDmnTitleControlIdToReplace = "";
         AV86ddo_Lote_ValorTitleControlIdToReplace = "";
         AV94Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         forbiddenHiddens = "";
         scmdbuf = "";
         H00BR2_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         H00BR2_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         H00BR2_A512ContagemResultado_ValorPF = new decimal[1] ;
         H00BR2_n512ContagemResultado_ValorPF = new bool[] {false} ;
         H00BR2_A456ContagemResultado_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV87DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV37Lote_NumeroTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV41Lote_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV45Lote_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV51Lote_UserCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV55Lote_PessoaCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV59Lote_UserNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV63Lote_ValorPFTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV67Lote_DataIniTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV73Lote_DataFimTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV79Lote_QtdeDmnTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV83Lote_ValorTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_lote_numero_Filteredtext_set = "";
         Ddo_lote_numero_Selectedvalue_set = "";
         Ddo_lote_numero_Sortedstatus = "";
         Ddo_lote_nome_Filteredtext_set = "";
         Ddo_lote_nome_Selectedvalue_set = "";
         Ddo_lote_nome_Sortedstatus = "";
         Ddo_lote_data_Filteredtext_set = "";
         Ddo_lote_data_Filteredtextto_set = "";
         Ddo_lote_data_Sortedstatus = "";
         Ddo_lote_usercod_Filteredtext_set = "";
         Ddo_lote_usercod_Filteredtextto_set = "";
         Ddo_lote_usercod_Sortedstatus = "";
         Ddo_lote_pessoacod_Filteredtext_set = "";
         Ddo_lote_pessoacod_Filteredtextto_set = "";
         Ddo_lote_pessoacod_Sortedstatus = "";
         Ddo_lote_usernom_Filteredtext_set = "";
         Ddo_lote_usernom_Selectedvalue_set = "";
         Ddo_lote_usernom_Sortedstatus = "";
         Ddo_lote_valorpf_Filteredtext_set = "";
         Ddo_lote_valorpf_Filteredtextto_set = "";
         Ddo_lote_valorpf_Sortedstatus = "";
         Ddo_lote_dataini_Filteredtext_set = "";
         Ddo_lote_dataini_Filteredtextto_set = "";
         Ddo_lote_datafim_Filteredtext_set = "";
         Ddo_lote_datafim_Filteredtextto_set = "";
         Ddo_lote_qtdedmn_Filteredtext_set = "";
         Ddo_lote_qtdedmn_Filteredtextto_set = "";
         Ddo_lote_valor_Filteredtext_set = "";
         Ddo_lote_valor_Filteredtextto_set = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV48DDO_Lote_DataAuxDate = DateTime.MinValue;
         AV49DDO_Lote_DataAuxDateTo = DateTime.MinValue;
         AV70DDO_Lote_DataIniAuxDate = DateTime.MinValue;
         AV71DDO_Lote_DataIniAuxDateTo = DateTime.MinValue;
         AV76DDO_Lote_DataFimAuxDate = DateTime.MinValue;
         AV77DDO_Lote_DataFimAuxDateTo = DateTime.MinValue;
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV31Select = "";
         AV93Select_GXI = "";
         A562Lote_Numero = "";
         A563Lote_Nome = "";
         A564Lote_Data = (DateTime)(DateTime.MinValue);
         A561Lote_UserNom = "";
         A567Lote_DataIni = DateTime.MinValue;
         A568Lote_DataFim = DateTime.MinValue;
         GridContainer = new GXWebGrid( context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         lV17Lote_Nome1 = "";
         lV18Lote_UserNom1 = "";
         lV22Lote_Nome2 = "";
         lV23Lote_UserNom2 = "";
         lV27Lote_Nome3 = "";
         lV28Lote_UserNom3 = "";
         lV38TFLote_Numero = "";
         lV42TFLote_Nome = "";
         lV60TFLote_UserNom = "";
         H00BR9_A595Lote_AreaTrabalhoCod = new int[1] ;
         H00BR9_A565Lote_ValorPF = new decimal[1] ;
         H00BR9_A561Lote_UserNom = new String[] {""} ;
         H00BR9_n561Lote_UserNom = new bool[] {false} ;
         H00BR9_A560Lote_PessoaCod = new int[1] ;
         H00BR9_n560Lote_PessoaCod = new bool[] {false} ;
         H00BR9_A559Lote_UserCod = new int[1] ;
         H00BR9_A564Lote_Data = new DateTime[] {DateTime.MinValue} ;
         H00BR9_A563Lote_Nome = new String[] {""} ;
         H00BR9_A562Lote_Numero = new String[] {""} ;
         H00BR9_A596Lote_Codigo = new int[1] ;
         H00BR9_A1231Lote_ContratadaCod = new int[1] ;
         H00BR9_A569Lote_QtdeDmn = new short[1] ;
         H00BR9_A568Lote_DataFim = new DateTime[] {DateTime.MinValue} ;
         H00BR9_A567Lote_DataIni = new DateTime[] {DateTime.MinValue} ;
         H00BR9_A1057Lote_ValorGlosas = new decimal[1] ;
         H00BR9_n1057Lote_ValorGlosas = new bool[] {false} ;
         hsh = "";
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons2 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         GridRow = new GXWebRow();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblFiltertextlote_areatrabalhocod_Jsonclick = "";
         lblFiltertextlote_contratadacod_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptlote__default(),
            new Object[][] {
                new Object[] {
               H00BR2_A597ContagemResultado_LoteAceiteCod, H00BR2_n597ContagemResultado_LoteAceiteCod, H00BR2_A512ContagemResultado_ValorPF, H00BR2_n512ContagemResultado_ValorPF, H00BR2_A456ContagemResultado_Codigo
               }
               , new Object[] {
               H00BR9_A595Lote_AreaTrabalhoCod, H00BR9_A565Lote_ValorPF, H00BR9_A561Lote_UserNom, H00BR9_n561Lote_UserNom, H00BR9_A560Lote_PessoaCod, H00BR9_n560Lote_PessoaCod, H00BR9_A559Lote_UserCod, H00BR9_A564Lote_Data, H00BR9_A563Lote_Nome, H00BR9_A562Lote_Numero,
               H00BR9_A596Lote_Codigo, H00BR9_A1231Lote_ContratadaCod, H00BR9_A569Lote_QtdeDmn, H00BR9_A568Lote_DataFim, H00BR9_A567Lote_DataIni, H00BR9_A1057Lote_ValorGlosas, H00BR9_n1057Lote_ValorGlosas
               }
            }
         );
         AV94Pgmname = "PromptLote";
         /* GeneXus formulas. */
         AV94Pgmname = "PromptLote";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_94 ;
      private short nGXsfl_94_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV21DynamicFiltersOperator2 ;
      private short AV26DynamicFiltersOperator3 ;
      private short AV80TFLote_QtdeDmn ;
      private short AV81TFLote_QtdeDmn_To ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short A569Lote_QtdeDmn ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_94_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtLote_Numero_Titleformat ;
      private short edtLote_Nome_Titleformat ;
      private short edtLote_Data_Titleformat ;
      private short edtLote_UserCod_Titleformat ;
      private short edtLote_PessoaCod_Titleformat ;
      private short edtLote_UserNom_Titleformat ;
      private short edtLote_ValorPF_Titleformat ;
      private short edtLote_DataIni_Titleformat ;
      private short edtLote_DataFim_Titleformat ;
      private short edtLote_QtdeDmn_Titleformat ;
      private short edtLote_Valor_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV35InOutLote_Codigo ;
      private int wcpOAV35InOutLote_Codigo ;
      private int subGrid_Rows ;
      private int AV52TFLote_UserCod ;
      private int AV53TFLote_UserCod_To ;
      private int AV56TFLote_PessoaCod ;
      private int AV57TFLote_PessoaCod_To ;
      private int AV34Lote_AreaTrabalhoCod ;
      private int AV36Lote_ContratadaCod ;
      private int A596Lote_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_lote_numero_Datalistupdateminimumcharacters ;
      private int Ddo_lote_nome_Datalistupdateminimumcharacters ;
      private int Ddo_lote_usernom_Datalistupdateminimumcharacters ;
      private int edtLote_Codigo_Visible ;
      private int edtavTflote_numero_Visible ;
      private int edtavTflote_numero_sel_Visible ;
      private int edtavTflote_nome_Visible ;
      private int edtavTflote_nome_sel_Visible ;
      private int edtavTflote_data_Visible ;
      private int edtavTflote_data_to_Visible ;
      private int edtavTflote_usercod_Visible ;
      private int edtavTflote_usercod_to_Visible ;
      private int edtavTflote_pessoacod_Visible ;
      private int edtavTflote_pessoacod_to_Visible ;
      private int edtavTflote_usernom_Visible ;
      private int edtavTflote_usernom_sel_Visible ;
      private int edtavTflote_valorpf_Visible ;
      private int edtavTflote_valorpf_to_Visible ;
      private int edtavTflote_dataini_Visible ;
      private int edtavTflote_dataini_to_Visible ;
      private int edtavTflote_datafim_Visible ;
      private int edtavTflote_datafim_to_Visible ;
      private int edtavTflote_qtdedmn_Visible ;
      private int edtavTflote_qtdedmn_to_Visible ;
      private int edtavTflote_valor_Visible ;
      private int edtavTflote_valor_to_Visible ;
      private int edtavDdo_lote_numerotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_lote_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_lote_datatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_lote_usercodtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_lote_pessoacodtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_lote_usernomtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_lote_valorpftitlecontrolidtoreplace_Visible ;
      private int edtavDdo_lote_datainititlecontrolidtoreplace_Visible ;
      private int edtavDdo_lote_datafimtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_lote_qtdedmntitlecontrolidtoreplace_Visible ;
      private int edtavDdo_lote_valortitlecontrolidtoreplace_Visible ;
      private int A559Lote_UserCod ;
      private int A560Lote_PessoaCod ;
      private int subGrid_Islastpage ;
      private int AV6WWPContext_gxTpr_Areatrabalho_codigo ;
      private int AV6WWPContext_gxTpr_Contratada_codigo ;
      private int A595Lote_AreaTrabalhoCod ;
      private int A1231Lote_ContratadaCod ;
      private int edtavOrdereddsc_Visible ;
      private int AV88PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavLote_nome1_Visible ;
      private int edtavLote_usernom1_Visible ;
      private int edtavLote_nome2_Visible ;
      private int edtavLote_usernom2_Visible ;
      private int edtavLote_nome3_Visible ;
      private int edtavLote_usernom3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV89GridCurrentPage ;
      private long AV90GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV64TFLote_ValorPF ;
      private decimal AV65TFLote_ValorPF_To ;
      private decimal AV84TFLote_Valor ;
      private decimal AV85TFLote_Valor_To ;
      private decimal A1058Lote_ValorOSs ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal GXt_decimal1 ;
      private decimal A606ContagemResultado_ValorFinal ;
      private decimal A1057Lote_ValorGlosas ;
      private decimal A565Lote_ValorPF ;
      private decimal A572Lote_Valor ;
      private String AV8InOutLote_Nome ;
      private String wcpOAV8InOutLote_Nome ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_lote_numero_Activeeventkey ;
      private String Ddo_lote_numero_Filteredtext_get ;
      private String Ddo_lote_numero_Selectedvalue_get ;
      private String Ddo_lote_nome_Activeeventkey ;
      private String Ddo_lote_nome_Filteredtext_get ;
      private String Ddo_lote_nome_Selectedvalue_get ;
      private String Ddo_lote_data_Activeeventkey ;
      private String Ddo_lote_data_Filteredtext_get ;
      private String Ddo_lote_data_Filteredtextto_get ;
      private String Ddo_lote_usercod_Activeeventkey ;
      private String Ddo_lote_usercod_Filteredtext_get ;
      private String Ddo_lote_usercod_Filteredtextto_get ;
      private String Ddo_lote_pessoacod_Activeeventkey ;
      private String Ddo_lote_pessoacod_Filteredtext_get ;
      private String Ddo_lote_pessoacod_Filteredtextto_get ;
      private String Ddo_lote_usernom_Activeeventkey ;
      private String Ddo_lote_usernom_Filteredtext_get ;
      private String Ddo_lote_usernom_Selectedvalue_get ;
      private String Ddo_lote_valorpf_Activeeventkey ;
      private String Ddo_lote_valorpf_Filteredtext_get ;
      private String Ddo_lote_valorpf_Filteredtextto_get ;
      private String Ddo_lote_dataini_Activeeventkey ;
      private String Ddo_lote_dataini_Filteredtext_get ;
      private String Ddo_lote_dataini_Filteredtextto_get ;
      private String Ddo_lote_datafim_Activeeventkey ;
      private String Ddo_lote_datafim_Filteredtext_get ;
      private String Ddo_lote_datafim_Filteredtextto_get ;
      private String Ddo_lote_qtdedmn_Activeeventkey ;
      private String Ddo_lote_qtdedmn_Filteredtext_get ;
      private String Ddo_lote_qtdedmn_Filteredtextto_get ;
      private String Ddo_lote_valor_Activeeventkey ;
      private String Ddo_lote_valor_Filteredtext_get ;
      private String Ddo_lote_valor_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_94_idx="0001" ;
      private String AV17Lote_Nome1 ;
      private String AV18Lote_UserNom1 ;
      private String AV22Lote_Nome2 ;
      private String AV23Lote_UserNom2 ;
      private String AV27Lote_Nome3 ;
      private String AV28Lote_UserNom3 ;
      private String AV38TFLote_Numero ;
      private String AV39TFLote_Numero_Sel ;
      private String AV42TFLote_Nome ;
      private String AV43TFLote_Nome_Sel ;
      private String AV60TFLote_UserNom ;
      private String AV61TFLote_UserNom_Sel ;
      private String AV94Pgmname ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String scmdbuf ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_lote_numero_Caption ;
      private String Ddo_lote_numero_Tooltip ;
      private String Ddo_lote_numero_Cls ;
      private String Ddo_lote_numero_Filteredtext_set ;
      private String Ddo_lote_numero_Selectedvalue_set ;
      private String Ddo_lote_numero_Dropdownoptionstype ;
      private String Ddo_lote_numero_Titlecontrolidtoreplace ;
      private String Ddo_lote_numero_Sortedstatus ;
      private String Ddo_lote_numero_Filtertype ;
      private String Ddo_lote_numero_Datalisttype ;
      private String Ddo_lote_numero_Datalistproc ;
      private String Ddo_lote_numero_Sortasc ;
      private String Ddo_lote_numero_Sortdsc ;
      private String Ddo_lote_numero_Loadingdata ;
      private String Ddo_lote_numero_Cleanfilter ;
      private String Ddo_lote_numero_Noresultsfound ;
      private String Ddo_lote_numero_Searchbuttontext ;
      private String Ddo_lote_nome_Caption ;
      private String Ddo_lote_nome_Tooltip ;
      private String Ddo_lote_nome_Cls ;
      private String Ddo_lote_nome_Filteredtext_set ;
      private String Ddo_lote_nome_Selectedvalue_set ;
      private String Ddo_lote_nome_Dropdownoptionstype ;
      private String Ddo_lote_nome_Titlecontrolidtoreplace ;
      private String Ddo_lote_nome_Sortedstatus ;
      private String Ddo_lote_nome_Filtertype ;
      private String Ddo_lote_nome_Datalisttype ;
      private String Ddo_lote_nome_Datalistproc ;
      private String Ddo_lote_nome_Sortasc ;
      private String Ddo_lote_nome_Sortdsc ;
      private String Ddo_lote_nome_Loadingdata ;
      private String Ddo_lote_nome_Cleanfilter ;
      private String Ddo_lote_nome_Noresultsfound ;
      private String Ddo_lote_nome_Searchbuttontext ;
      private String Ddo_lote_data_Caption ;
      private String Ddo_lote_data_Tooltip ;
      private String Ddo_lote_data_Cls ;
      private String Ddo_lote_data_Filteredtext_set ;
      private String Ddo_lote_data_Filteredtextto_set ;
      private String Ddo_lote_data_Dropdownoptionstype ;
      private String Ddo_lote_data_Titlecontrolidtoreplace ;
      private String Ddo_lote_data_Sortedstatus ;
      private String Ddo_lote_data_Filtertype ;
      private String Ddo_lote_data_Sortasc ;
      private String Ddo_lote_data_Sortdsc ;
      private String Ddo_lote_data_Cleanfilter ;
      private String Ddo_lote_data_Rangefilterfrom ;
      private String Ddo_lote_data_Rangefilterto ;
      private String Ddo_lote_data_Searchbuttontext ;
      private String Ddo_lote_usercod_Caption ;
      private String Ddo_lote_usercod_Tooltip ;
      private String Ddo_lote_usercod_Cls ;
      private String Ddo_lote_usercod_Filteredtext_set ;
      private String Ddo_lote_usercod_Filteredtextto_set ;
      private String Ddo_lote_usercod_Dropdownoptionstype ;
      private String Ddo_lote_usercod_Titlecontrolidtoreplace ;
      private String Ddo_lote_usercod_Sortedstatus ;
      private String Ddo_lote_usercod_Filtertype ;
      private String Ddo_lote_usercod_Sortasc ;
      private String Ddo_lote_usercod_Sortdsc ;
      private String Ddo_lote_usercod_Cleanfilter ;
      private String Ddo_lote_usercod_Rangefilterfrom ;
      private String Ddo_lote_usercod_Rangefilterto ;
      private String Ddo_lote_usercod_Searchbuttontext ;
      private String Ddo_lote_pessoacod_Caption ;
      private String Ddo_lote_pessoacod_Tooltip ;
      private String Ddo_lote_pessoacod_Cls ;
      private String Ddo_lote_pessoacod_Filteredtext_set ;
      private String Ddo_lote_pessoacod_Filteredtextto_set ;
      private String Ddo_lote_pessoacod_Dropdownoptionstype ;
      private String Ddo_lote_pessoacod_Titlecontrolidtoreplace ;
      private String Ddo_lote_pessoacod_Sortedstatus ;
      private String Ddo_lote_pessoacod_Filtertype ;
      private String Ddo_lote_pessoacod_Sortasc ;
      private String Ddo_lote_pessoacod_Sortdsc ;
      private String Ddo_lote_pessoacod_Cleanfilter ;
      private String Ddo_lote_pessoacod_Rangefilterfrom ;
      private String Ddo_lote_pessoacod_Rangefilterto ;
      private String Ddo_lote_pessoacod_Searchbuttontext ;
      private String Ddo_lote_usernom_Caption ;
      private String Ddo_lote_usernom_Tooltip ;
      private String Ddo_lote_usernom_Cls ;
      private String Ddo_lote_usernom_Filteredtext_set ;
      private String Ddo_lote_usernom_Selectedvalue_set ;
      private String Ddo_lote_usernom_Dropdownoptionstype ;
      private String Ddo_lote_usernom_Titlecontrolidtoreplace ;
      private String Ddo_lote_usernom_Sortedstatus ;
      private String Ddo_lote_usernom_Filtertype ;
      private String Ddo_lote_usernom_Datalisttype ;
      private String Ddo_lote_usernom_Datalistproc ;
      private String Ddo_lote_usernom_Sortasc ;
      private String Ddo_lote_usernom_Sortdsc ;
      private String Ddo_lote_usernom_Loadingdata ;
      private String Ddo_lote_usernom_Cleanfilter ;
      private String Ddo_lote_usernom_Noresultsfound ;
      private String Ddo_lote_usernom_Searchbuttontext ;
      private String Ddo_lote_valorpf_Caption ;
      private String Ddo_lote_valorpf_Tooltip ;
      private String Ddo_lote_valorpf_Cls ;
      private String Ddo_lote_valorpf_Filteredtext_set ;
      private String Ddo_lote_valorpf_Filteredtextto_set ;
      private String Ddo_lote_valorpf_Dropdownoptionstype ;
      private String Ddo_lote_valorpf_Titlecontrolidtoreplace ;
      private String Ddo_lote_valorpf_Sortedstatus ;
      private String Ddo_lote_valorpf_Filtertype ;
      private String Ddo_lote_valorpf_Sortasc ;
      private String Ddo_lote_valorpf_Sortdsc ;
      private String Ddo_lote_valorpf_Cleanfilter ;
      private String Ddo_lote_valorpf_Rangefilterfrom ;
      private String Ddo_lote_valorpf_Rangefilterto ;
      private String Ddo_lote_valorpf_Searchbuttontext ;
      private String Ddo_lote_dataini_Caption ;
      private String Ddo_lote_dataini_Tooltip ;
      private String Ddo_lote_dataini_Cls ;
      private String Ddo_lote_dataini_Filteredtext_set ;
      private String Ddo_lote_dataini_Filteredtextto_set ;
      private String Ddo_lote_dataini_Dropdownoptionstype ;
      private String Ddo_lote_dataini_Titlecontrolidtoreplace ;
      private String Ddo_lote_dataini_Filtertype ;
      private String Ddo_lote_dataini_Cleanfilter ;
      private String Ddo_lote_dataini_Rangefilterfrom ;
      private String Ddo_lote_dataini_Rangefilterto ;
      private String Ddo_lote_dataini_Searchbuttontext ;
      private String Ddo_lote_datafim_Caption ;
      private String Ddo_lote_datafim_Tooltip ;
      private String Ddo_lote_datafim_Cls ;
      private String Ddo_lote_datafim_Filteredtext_set ;
      private String Ddo_lote_datafim_Filteredtextto_set ;
      private String Ddo_lote_datafim_Dropdownoptionstype ;
      private String Ddo_lote_datafim_Titlecontrolidtoreplace ;
      private String Ddo_lote_datafim_Filtertype ;
      private String Ddo_lote_datafim_Cleanfilter ;
      private String Ddo_lote_datafim_Rangefilterfrom ;
      private String Ddo_lote_datafim_Rangefilterto ;
      private String Ddo_lote_datafim_Searchbuttontext ;
      private String Ddo_lote_qtdedmn_Caption ;
      private String Ddo_lote_qtdedmn_Tooltip ;
      private String Ddo_lote_qtdedmn_Cls ;
      private String Ddo_lote_qtdedmn_Filteredtext_set ;
      private String Ddo_lote_qtdedmn_Filteredtextto_set ;
      private String Ddo_lote_qtdedmn_Dropdownoptionstype ;
      private String Ddo_lote_qtdedmn_Titlecontrolidtoreplace ;
      private String Ddo_lote_qtdedmn_Filtertype ;
      private String Ddo_lote_qtdedmn_Cleanfilter ;
      private String Ddo_lote_qtdedmn_Rangefilterfrom ;
      private String Ddo_lote_qtdedmn_Rangefilterto ;
      private String Ddo_lote_qtdedmn_Searchbuttontext ;
      private String Ddo_lote_valor_Caption ;
      private String Ddo_lote_valor_Tooltip ;
      private String Ddo_lote_valor_Cls ;
      private String Ddo_lote_valor_Filteredtext_set ;
      private String Ddo_lote_valor_Filteredtextto_set ;
      private String Ddo_lote_valor_Dropdownoptionstype ;
      private String Ddo_lote_valor_Titlecontrolidtoreplace ;
      private String Ddo_lote_valor_Filtertype ;
      private String Ddo_lote_valor_Cleanfilter ;
      private String Ddo_lote_valor_Rangefilterfrom ;
      private String Ddo_lote_valor_Rangefilterto ;
      private String Ddo_lote_valor_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String edtLote_Codigo_Internalname ;
      private String edtLote_Codigo_Jsonclick ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTflote_numero_Internalname ;
      private String edtavTflote_numero_Jsonclick ;
      private String edtavTflote_numero_sel_Internalname ;
      private String edtavTflote_numero_sel_Jsonclick ;
      private String edtavTflote_nome_Internalname ;
      private String edtavTflote_nome_Jsonclick ;
      private String edtavTflote_nome_sel_Internalname ;
      private String edtavTflote_nome_sel_Jsonclick ;
      private String edtavTflote_data_Internalname ;
      private String edtavTflote_data_Jsonclick ;
      private String edtavTflote_data_to_Internalname ;
      private String edtavTflote_data_to_Jsonclick ;
      private String divDdo_lote_dataauxdates_Internalname ;
      private String edtavDdo_lote_dataauxdate_Internalname ;
      private String edtavDdo_lote_dataauxdate_Jsonclick ;
      private String edtavDdo_lote_dataauxdateto_Internalname ;
      private String edtavDdo_lote_dataauxdateto_Jsonclick ;
      private String edtavTflote_usercod_Internalname ;
      private String edtavTflote_usercod_Jsonclick ;
      private String edtavTflote_usercod_to_Internalname ;
      private String edtavTflote_usercod_to_Jsonclick ;
      private String edtavTflote_pessoacod_Internalname ;
      private String edtavTflote_pessoacod_Jsonclick ;
      private String edtavTflote_pessoacod_to_Internalname ;
      private String edtavTflote_pessoacod_to_Jsonclick ;
      private String edtavTflote_usernom_Internalname ;
      private String edtavTflote_usernom_Jsonclick ;
      private String edtavTflote_usernom_sel_Internalname ;
      private String edtavTflote_usernom_sel_Jsonclick ;
      private String edtavTflote_valorpf_Internalname ;
      private String edtavTflote_valorpf_Jsonclick ;
      private String edtavTflote_valorpf_to_Internalname ;
      private String edtavTflote_valorpf_to_Jsonclick ;
      private String edtavTflote_dataini_Internalname ;
      private String edtavTflote_dataini_Jsonclick ;
      private String edtavTflote_dataini_to_Internalname ;
      private String edtavTflote_dataini_to_Jsonclick ;
      private String divDdo_lote_datainiauxdates_Internalname ;
      private String edtavDdo_lote_datainiauxdate_Internalname ;
      private String edtavDdo_lote_datainiauxdate_Jsonclick ;
      private String edtavDdo_lote_datainiauxdateto_Internalname ;
      private String edtavDdo_lote_datainiauxdateto_Jsonclick ;
      private String edtavTflote_datafim_Internalname ;
      private String edtavTflote_datafim_Jsonclick ;
      private String edtavTflote_datafim_to_Internalname ;
      private String edtavTflote_datafim_to_Jsonclick ;
      private String divDdo_lote_datafimauxdates_Internalname ;
      private String edtavDdo_lote_datafimauxdate_Internalname ;
      private String edtavDdo_lote_datafimauxdate_Jsonclick ;
      private String edtavDdo_lote_datafimauxdateto_Internalname ;
      private String edtavDdo_lote_datafimauxdateto_Jsonclick ;
      private String edtavTflote_qtdedmn_Internalname ;
      private String edtavTflote_qtdedmn_Jsonclick ;
      private String edtavTflote_qtdedmn_to_Internalname ;
      private String edtavTflote_qtdedmn_to_Jsonclick ;
      private String edtavTflote_valor_Internalname ;
      private String edtavTflote_valor_Jsonclick ;
      private String edtavTflote_valor_to_Internalname ;
      private String edtavTflote_valor_to_Jsonclick ;
      private String edtavDdo_lote_numerotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_lote_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_lote_datatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_lote_usercodtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_lote_pessoacodtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_lote_usernomtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_lote_valorpftitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_lote_datainititlecontrolidtoreplace_Internalname ;
      private String edtavDdo_lote_datafimtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_lote_qtdedmntitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_lote_valortitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String A562Lote_Numero ;
      private String edtLote_Numero_Internalname ;
      private String A563Lote_Nome ;
      private String edtLote_Nome_Internalname ;
      private String edtLote_Data_Internalname ;
      private String edtLote_UserCod_Internalname ;
      private String edtLote_PessoaCod_Internalname ;
      private String A561Lote_UserNom ;
      private String edtLote_UserNom_Internalname ;
      private String edtLote_ValorPF_Internalname ;
      private String edtLote_DataIni_Internalname ;
      private String edtLote_DataFim_Internalname ;
      private String edtLote_QtdeDmn_Internalname ;
      private String edtLote_Valor_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String lV17Lote_Nome1 ;
      private String lV18Lote_UserNom1 ;
      private String lV22Lote_Nome2 ;
      private String lV23Lote_UserNom2 ;
      private String lV27Lote_Nome3 ;
      private String lV28Lote_UserNom3 ;
      private String lV38TFLote_Numero ;
      private String lV42TFLote_Nome ;
      private String lV60TFLote_UserNom ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavLote_areatrabalhocod_Internalname ;
      private String edtavLote_contratadacod_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavLote_nome1_Internalname ;
      private String edtavLote_usernom1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavLote_nome2_Internalname ;
      private String edtavLote_usernom2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavLote_nome3_Internalname ;
      private String edtavLote_usernom3_Internalname ;
      private String hsh ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_lote_numero_Internalname ;
      private String Ddo_lote_nome_Internalname ;
      private String Ddo_lote_data_Internalname ;
      private String Ddo_lote_usercod_Internalname ;
      private String Ddo_lote_pessoacod_Internalname ;
      private String Ddo_lote_usernom_Internalname ;
      private String Ddo_lote_valorpf_Internalname ;
      private String Ddo_lote_dataini_Internalname ;
      private String Ddo_lote_datafim_Internalname ;
      private String Ddo_lote_qtdedmn_Internalname ;
      private String Ddo_lote_valor_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtLote_Numero_Title ;
      private String edtLote_Nome_Title ;
      private String edtLote_Data_Title ;
      private String edtLote_UserCod_Title ;
      private String edtLote_PessoaCod_Title ;
      private String edtLote_UserNom_Title ;
      private String edtLote_ValorPF_Title ;
      private String edtLote_DataIni_Title ;
      private String edtLote_DataFim_Title ;
      private String edtLote_QtdeDmn_Title ;
      private String edtLote_Valor_Title ;
      private String edtavSelect_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblFiltertextlote_areatrabalhocod_Internalname ;
      private String lblFiltertextlote_areatrabalhocod_Jsonclick ;
      private String edtavLote_areatrabalhocod_Jsonclick ;
      private String lblFiltertextlote_contratadacod_Internalname ;
      private String lblFiltertextlote_contratadacod_Jsonclick ;
      private String edtavLote_contratadacod_Jsonclick ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavLote_nome3_Jsonclick ;
      private String edtavLote_usernom3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavLote_nome2_Jsonclick ;
      private String edtavLote_usernom2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavLote_nome1_Jsonclick ;
      private String edtavLote_usernom1_Jsonclick ;
      private String sGXsfl_94_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtLote_Numero_Jsonclick ;
      private String edtLote_Nome_Jsonclick ;
      private String edtLote_Data_Jsonclick ;
      private String edtLote_UserCod_Jsonclick ;
      private String edtLote_PessoaCod_Jsonclick ;
      private String edtLote_UserNom_Jsonclick ;
      private String edtLote_ValorPF_Jsonclick ;
      private String edtLote_DataIni_Jsonclick ;
      private String edtLote_DataFim_Jsonclick ;
      private String edtLote_QtdeDmn_Jsonclick ;
      private String edtLote_Valor_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV46TFLote_Data ;
      private DateTime AV47TFLote_Data_To ;
      private DateTime A564Lote_Data ;
      private DateTime AV68TFLote_DataIni ;
      private DateTime AV69TFLote_DataIni_To ;
      private DateTime AV74TFLote_DataFim ;
      private DateTime AV75TFLote_DataFim_To ;
      private DateTime AV48DDO_Lote_DataAuxDate ;
      private DateTime AV49DDO_Lote_DataAuxDateTo ;
      private DateTime AV70DDO_Lote_DataIniAuxDate ;
      private DateTime AV71DDO_Lote_DataIniAuxDateTo ;
      private DateTime AV76DDO_Lote_DataFimAuxDate ;
      private DateTime AV77DDO_Lote_DataFimAuxDateTo ;
      private DateTime A567Lote_DataIni ;
      private DateTime A568Lote_DataFim ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV19DynamicFiltersEnabled2 ;
      private bool AV24DynamicFiltersEnabled3 ;
      private bool AV30DynamicFiltersIgnoreFirst ;
      private bool AV29DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_lote_numero_Includesortasc ;
      private bool Ddo_lote_numero_Includesortdsc ;
      private bool Ddo_lote_numero_Includefilter ;
      private bool Ddo_lote_numero_Filterisrange ;
      private bool Ddo_lote_numero_Includedatalist ;
      private bool Ddo_lote_nome_Includesortasc ;
      private bool Ddo_lote_nome_Includesortdsc ;
      private bool Ddo_lote_nome_Includefilter ;
      private bool Ddo_lote_nome_Filterisrange ;
      private bool Ddo_lote_nome_Includedatalist ;
      private bool Ddo_lote_data_Includesortasc ;
      private bool Ddo_lote_data_Includesortdsc ;
      private bool Ddo_lote_data_Includefilter ;
      private bool Ddo_lote_data_Filterisrange ;
      private bool Ddo_lote_data_Includedatalist ;
      private bool Ddo_lote_usercod_Includesortasc ;
      private bool Ddo_lote_usercod_Includesortdsc ;
      private bool Ddo_lote_usercod_Includefilter ;
      private bool Ddo_lote_usercod_Filterisrange ;
      private bool Ddo_lote_usercod_Includedatalist ;
      private bool Ddo_lote_pessoacod_Includesortasc ;
      private bool Ddo_lote_pessoacod_Includesortdsc ;
      private bool Ddo_lote_pessoacod_Includefilter ;
      private bool Ddo_lote_pessoacod_Filterisrange ;
      private bool Ddo_lote_pessoacod_Includedatalist ;
      private bool Ddo_lote_usernom_Includesortasc ;
      private bool Ddo_lote_usernom_Includesortdsc ;
      private bool Ddo_lote_usernom_Includefilter ;
      private bool Ddo_lote_usernom_Filterisrange ;
      private bool Ddo_lote_usernom_Includedatalist ;
      private bool Ddo_lote_valorpf_Includesortasc ;
      private bool Ddo_lote_valorpf_Includesortdsc ;
      private bool Ddo_lote_valorpf_Includefilter ;
      private bool Ddo_lote_valorpf_Filterisrange ;
      private bool Ddo_lote_valorpf_Includedatalist ;
      private bool Ddo_lote_dataini_Includesortasc ;
      private bool Ddo_lote_dataini_Includesortdsc ;
      private bool Ddo_lote_dataini_Includefilter ;
      private bool Ddo_lote_dataini_Filterisrange ;
      private bool Ddo_lote_dataini_Includedatalist ;
      private bool Ddo_lote_datafim_Includesortasc ;
      private bool Ddo_lote_datafim_Includesortdsc ;
      private bool Ddo_lote_datafim_Includefilter ;
      private bool Ddo_lote_datafim_Filterisrange ;
      private bool Ddo_lote_datafim_Includedatalist ;
      private bool Ddo_lote_qtdedmn_Includesortasc ;
      private bool Ddo_lote_qtdedmn_Includesortdsc ;
      private bool Ddo_lote_qtdedmn_Includefilter ;
      private bool Ddo_lote_qtdedmn_Filterisrange ;
      private bool Ddo_lote_qtdedmn_Includedatalist ;
      private bool Ddo_lote_valor_Includesortasc ;
      private bool Ddo_lote_valor_Includesortdsc ;
      private bool Ddo_lote_valor_Includefilter ;
      private bool Ddo_lote_valor_Filterisrange ;
      private bool Ddo_lote_valor_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n560Lote_PessoaCod ;
      private bool n561Lote_UserNom ;
      private bool AV6WWPContext_gxTpr_Userehcontratante ;
      private bool n1057Lote_ValorGlosas ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV31Select_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV20DynamicFiltersSelector2 ;
      private String AV25DynamicFiltersSelector3 ;
      private String AV40ddo_Lote_NumeroTitleControlIdToReplace ;
      private String AV44ddo_Lote_NomeTitleControlIdToReplace ;
      private String AV50ddo_Lote_DataTitleControlIdToReplace ;
      private String AV54ddo_Lote_UserCodTitleControlIdToReplace ;
      private String AV58ddo_Lote_PessoaCodTitleControlIdToReplace ;
      private String AV62ddo_Lote_UserNomTitleControlIdToReplace ;
      private String AV66ddo_Lote_ValorPFTitleControlIdToReplace ;
      private String AV72ddo_Lote_DataIniTitleControlIdToReplace ;
      private String AV78ddo_Lote_DataFimTitleControlIdToReplace ;
      private String AV82ddo_Lote_QtdeDmnTitleControlIdToReplace ;
      private String AV86ddo_Lote_ValorTitleControlIdToReplace ;
      private String AV93Select_GXI ;
      private String AV31Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutLote_Codigo ;
      private String aP1_InOutLote_Nome ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H00BR2_A597ContagemResultado_LoteAceiteCod ;
      private bool[] H00BR2_n597ContagemResultado_LoteAceiteCod ;
      private decimal[] H00BR2_A512ContagemResultado_ValorPF ;
      private bool[] H00BR2_n512ContagemResultado_ValorPF ;
      private int[] H00BR2_A456ContagemResultado_Codigo ;
      private int[] H00BR9_A595Lote_AreaTrabalhoCod ;
      private decimal[] H00BR9_A565Lote_ValorPF ;
      private String[] H00BR9_A561Lote_UserNom ;
      private bool[] H00BR9_n561Lote_UserNom ;
      private int[] H00BR9_A560Lote_PessoaCod ;
      private bool[] H00BR9_n560Lote_PessoaCod ;
      private int[] H00BR9_A559Lote_UserCod ;
      private DateTime[] H00BR9_A564Lote_Data ;
      private String[] H00BR9_A563Lote_Nome ;
      private String[] H00BR9_A562Lote_Numero ;
      private int[] H00BR9_A596Lote_Codigo ;
      private int[] H00BR9_A1231Lote_ContratadaCod ;
      private short[] H00BR9_A569Lote_QtdeDmn ;
      private DateTime[] H00BR9_A568Lote_DataFim ;
      private DateTime[] H00BR9_A567Lote_DataIni ;
      private decimal[] H00BR9_A1057Lote_ValorGlosas ;
      private bool[] H00BR9_n1057Lote_ValorGlosas ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV37Lote_NumeroTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV41Lote_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV45Lote_DataTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV51Lote_UserCodTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV55Lote_PessoaCodTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV59Lote_UserNomTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV63Lote_ValorPFTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV67Lote_DataIniTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV73Lote_DataFimTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV79Lote_QtdeDmnTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV83Lote_ValorTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV87DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons2 ;
   }

   public class promptlote__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00BR9( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             String AV17Lote_Nome1 ,
                                             String AV18Lote_UserNom1 ,
                                             bool AV19DynamicFiltersEnabled2 ,
                                             String AV20DynamicFiltersSelector2 ,
                                             short AV21DynamicFiltersOperator2 ,
                                             String AV22Lote_Nome2 ,
                                             String AV23Lote_UserNom2 ,
                                             bool AV24DynamicFiltersEnabled3 ,
                                             String AV25DynamicFiltersSelector3 ,
                                             short AV26DynamicFiltersOperator3 ,
                                             String AV27Lote_Nome3 ,
                                             String AV28Lote_UserNom3 ,
                                             String AV39TFLote_Numero_Sel ,
                                             String AV38TFLote_Numero ,
                                             String AV43TFLote_Nome_Sel ,
                                             String AV42TFLote_Nome ,
                                             DateTime AV46TFLote_Data ,
                                             DateTime AV47TFLote_Data_To ,
                                             int AV52TFLote_UserCod ,
                                             int AV53TFLote_UserCod_To ,
                                             int AV56TFLote_PessoaCod ,
                                             int AV57TFLote_PessoaCod_To ,
                                             String AV61TFLote_UserNom_Sel ,
                                             String AV60TFLote_UserNom ,
                                             decimal AV64TFLote_ValorPF ,
                                             decimal AV65TFLote_ValorPF_To ,
                                             short AV80TFLote_QtdeDmn ,
                                             short AV81TFLote_QtdeDmn_To ,
                                             String A563Lote_Nome ,
                                             String A561Lote_UserNom ,
                                             String A562Lote_Numero ,
                                             DateTime A564Lote_Data ,
                                             int A559Lote_UserCod ,
                                             int A560Lote_PessoaCod ,
                                             decimal A565Lote_ValorPF ,
                                             short A569Lote_QtdeDmn ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A595Lote_AreaTrabalhoCod ,
                                             int AV6WWPContext_gxTpr_Areatrabalho_codigo ,
                                             bool AV6WWPContext_gxTpr_Userehcontratante ,
                                             int A1231Lote_ContratadaCod ,
                                             int AV6WWPContext_gxTpr_Contratada_codigo ,
                                             DateTime AV68TFLote_DataIni ,
                                             DateTime A567Lote_DataIni ,
                                             DateTime AV69TFLote_DataIni_To ,
                                             DateTime AV74TFLote_DataFim ,
                                             DateTime A568Lote_DataFim ,
                                             DateTime AV75TFLote_DataFim_To ,
                                             decimal AV84TFLote_Valor ,
                                             decimal A572Lote_Valor ,
                                             decimal AV85TFLote_Valor_To )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [39] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Lote_AreaTrabalhoCod], T1.[Lote_ValorPF], T3.[Pessoa_Nome] AS Lote_UserNom, T2.[Usuario_PessoaCod] AS Lote_PessoaCod, T1.[Lote_UserCod] AS Lote_UserCod, T1.[Lote_Data], T1.[Lote_Nome], T1.[Lote_Numero], T1.[Lote_Codigo], COALESCE( T4.[Lote_ContratadaCod], 0) AS Lote_ContratadaCod, COALESCE( T4.[Lote_QtdeDmn], 0) AS Lote_QtdeDmn, COALESCE( T6.[Lote_DataFim], convert( DATETIME, '17530101', 112 )) AS Lote_DataFim, COALESCE( T6.[Lote_DataIni], convert( DATETIME, '17530101', 112 )) AS Lote_DataIni, COALESCE( T5.[Lote_ValorGlosas], 0) AS Lote_ValorGlosas FROM ((((([Lote] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[Lote_UserCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) LEFT JOIN (SELECT MIN([ContagemResultado_ContratadaCod]) AS Lote_ContratadaCod, [ContagemResultado_LoteAceiteCod], COUNT(*) AS Lote_QtdeDmn FROM [ContagemResultado] WITH (NOLOCK) GROUP BY [ContagemResultado_LoteAceiteCod] ) T4 ON T4.[ContagemResultado_LoteAceiteCod] = T1.[Lote_Codigo]) INNER JOIN (SELECT COALESCE( T9.[GXC2], 0) + COALESCE( T8.[GXC3], 0) AS Lote_ValorGlosas, T7.[Lote_Codigo] FROM (([Lote] T7 WITH (NOLOCK) LEFT JOIN (SELECT SUM(T10.[ContagemResultadoIndicadores_Valor]) AS GXC3, T11.[Lote_Codigo] FROM [ContagemResultadoIndicadores] T10 WITH (NOLOCK),  [Lote] T11 WITH (NOLOCK) WHERE T10.[ContagemResultadoIndicadores_LoteCod] = T11.[Lote_Codigo] GROUP BY T11.[Lote_Codigo] ) T8 ON T8.[Lote_Codigo] = T7.[Lote_Codigo]) LEFT JOIN (SELECT SUM([ContagemResultado_GlsValor]) AS GXC2, [ContagemResultado_LoteAceiteCod] FROM [ContagemResultado] WITH (NOLOCK) GROUP BY [ContagemResultado_LoteAceiteCod] ) T9 ON T9.[ContagemResultado_LoteAceiteCod] = T7.[Lote_Codigo]) ) T5 ON T5.[Lote_Codigo] = T1.[Lote_Codigo]) LEFT JOIN";
         scmdbuf = scmdbuf + " (SELECT MAX(COALESCE( T8.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) AS Lote_DataFim, T7.[ContagemResultado_LoteAceiteCod], MIN(COALESCE( T8.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) AS Lote_DataIni FROM ([ContagemResultado] T7 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T8 ON T8.[ContagemResultado_Codigo] = T7.[ContagemResultado_Codigo]) GROUP BY T7.[ContagemResultado_LoteAceiteCod] ) T6 ON T6.[ContagemResultado_LoteAceiteCod] = T1.[Lote_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[Lote_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo)";
         scmdbuf = scmdbuf + " and (@AV6WWPCo_3Userehcontratante = 1 or ( COALESCE( T4.[Lote_ContratadaCod], 0) = @AV6WWPCo_2Contratada_codigo))";
         scmdbuf = scmdbuf + " and ((@AV68TFLote_DataIni = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T6.[Lote_DataIni], convert( DATETIME, '17530101', 112 )) >= @AV68TFLote_DataIni))";
         scmdbuf = scmdbuf + " and ((@AV69TFLote_DataIni_To = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T6.[Lote_DataIni], convert( DATETIME, '17530101', 112 )) <= @AV69TFLote_DataIni_To))";
         scmdbuf = scmdbuf + " and ((@AV74TFLote_DataFim = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T6.[Lote_DataFim], convert( DATETIME, '17530101', 112 )) >= @AV74TFLote_DataFim))";
         scmdbuf = scmdbuf + " and ((@AV75TFLote_DataFim_To = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T6.[Lote_DataFim], convert( DATETIME, '17530101', 112 )) <= @AV75TFLote_DataFim_To))";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "LOTE_NOME") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Lote_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Nome] like @lV17Lote_Nome1)";
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "LOTE_NOME") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Lote_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Nome] like '%' + @lV17Lote_Nome1)";
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "LOTE_USERNOM") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Lote_UserNom1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV18Lote_UserNom1)";
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "LOTE_USERNOM") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Lote_UserNom1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV18Lote_UserNom1)";
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "LOTE_NOME") == 0 ) && ( AV21DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22Lote_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Nome] like @lV22Lote_Nome2)";
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "LOTE_NOME") == 0 ) && ( AV21DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22Lote_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Nome] like '%' + @lV22Lote_Nome2)";
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "LOTE_USERNOM") == 0 ) && ( AV21DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23Lote_UserNom2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV23Lote_UserNom2)";
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "LOTE_USERNOM") == 0 ) && ( AV21DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23Lote_UserNom2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV23Lote_UserNom2)";
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "LOTE_NOME") == 0 ) && ( AV26DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27Lote_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Nome] like @lV27Lote_Nome3)";
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "LOTE_NOME") == 0 ) && ( AV26DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27Lote_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Nome] like '%' + @lV27Lote_Nome3)";
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "LOTE_USERNOM") == 0 ) && ( AV26DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28Lote_UserNom3)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV28Lote_UserNom3)";
         }
         else
         {
            GXv_int3[21] = 1;
         }
         if ( AV24DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "LOTE_USERNOM") == 0 ) && ( AV26DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28Lote_UserNom3)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV28Lote_UserNom3)";
         }
         else
         {
            GXv_int3[22] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV39TFLote_Numero_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38TFLote_Numero)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Numero] like @lV38TFLote_Numero)";
         }
         else
         {
            GXv_int3[23] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFLote_Numero_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Numero] = @AV39TFLote_Numero_Sel)";
         }
         else
         {
            GXv_int3[24] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV43TFLote_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42TFLote_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Nome] like @lV42TFLote_Nome)";
         }
         else
         {
            GXv_int3[25] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TFLote_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Nome] = @AV43TFLote_Nome_Sel)";
         }
         else
         {
            GXv_int3[26] = 1;
         }
         if ( ! (DateTime.MinValue==AV46TFLote_Data) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Data] >= @AV46TFLote_Data)";
         }
         else
         {
            GXv_int3[27] = 1;
         }
         if ( ! (DateTime.MinValue==AV47TFLote_Data_To) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_Data] <= @AV47TFLote_Data_To)";
         }
         else
         {
            GXv_int3[28] = 1;
         }
         if ( ! (0==AV52TFLote_UserCod) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_UserCod] >= @AV52TFLote_UserCod)";
         }
         else
         {
            GXv_int3[29] = 1;
         }
         if ( ! (0==AV53TFLote_UserCod_To) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_UserCod] <= @AV53TFLote_UserCod_To)";
         }
         else
         {
            GXv_int3[30] = 1;
         }
         if ( ! (0==AV56TFLote_PessoaCod) )
         {
            sWhereString = sWhereString + " and (T2.[Usuario_PessoaCod] >= @AV56TFLote_PessoaCod)";
         }
         else
         {
            GXv_int3[31] = 1;
         }
         if ( ! (0==AV57TFLote_PessoaCod_To) )
         {
            sWhereString = sWhereString + " and (T2.[Usuario_PessoaCod] <= @AV57TFLote_PessoaCod_To)";
         }
         else
         {
            GXv_int3[32] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV61TFLote_UserNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60TFLote_UserNom)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV60TFLote_UserNom)";
         }
         else
         {
            GXv_int3[33] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61TFLote_UserNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV61TFLote_UserNom_Sel)";
         }
         else
         {
            GXv_int3[34] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV64TFLote_ValorPF) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_ValorPF] >= @AV64TFLote_ValorPF)";
         }
         else
         {
            GXv_int3[35] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV65TFLote_ValorPF_To) )
         {
            sWhereString = sWhereString + " and (T1.[Lote_ValorPF] <= @AV65TFLote_ValorPF_To)";
         }
         else
         {
            GXv_int3[36] = 1;
         }
         if ( ! (0==AV80TFLote_QtdeDmn) )
         {
            sWhereString = sWhereString + " and (COALESCE( T4.[Lote_QtdeDmn], 0) >= @AV80TFLote_QtdeDmn)";
         }
         else
         {
            GXv_int3[37] = 1;
         }
         if ( ! (0==AV81TFLote_QtdeDmn_To) )
         {
            sWhereString = sWhereString + " and (COALESCE( T4.[Lote_QtdeDmn], 0) <= @AV81TFLote_QtdeDmn_To)";
         }
         else
         {
            GXv_int3[38] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Lote_Nome]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Lote_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Lote_Numero]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Lote_Numero] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Lote_Data]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Lote_Data] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Lote_UserCod]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Lote_UserCod] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T2.[Usuario_PessoaCod]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T2.[Usuario_PessoaCod] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T3.[Pessoa_Nome]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T3.[Pessoa_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Lote_ValorPF]";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Lote_ValorPF] DESC";
         }
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 1 :
                     return conditional_H00BR9(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (DateTime)dynConstraints[18] , (DateTime)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (int)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (decimal)dynConstraints[26] , (decimal)dynConstraints[27] , (short)dynConstraints[28] , (short)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (DateTime)dynConstraints[33] , (int)dynConstraints[34] , (int)dynConstraints[35] , (decimal)dynConstraints[36] , (short)dynConstraints[37] , (short)dynConstraints[38] , (bool)dynConstraints[39] , (int)dynConstraints[40] , (int)dynConstraints[41] , (bool)dynConstraints[42] , (int)dynConstraints[43] , (int)dynConstraints[44] , (DateTime)dynConstraints[45] , (DateTime)dynConstraints[46] , (DateTime)dynConstraints[47] , (DateTime)dynConstraints[48] , (DateTime)dynConstraints[49] , (DateTime)dynConstraints[50] , (decimal)dynConstraints[51] , (decimal)dynConstraints[52] , (decimal)dynConstraints[53] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00BR2 ;
          prmH00BR2 = new Object[] {
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00BR9 ;
          prmH00BR9 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV6WWPCo_3Userehcontratante",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV6WWPCo_2Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV68TFLote_DataIni",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV68TFLote_DataIni",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV69TFLote_DataIni_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV69TFLote_DataIni_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV74TFLote_DataFim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV74TFLote_DataFim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV75TFLote_DataFim_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV75TFLote_DataFim_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV17Lote_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV17Lote_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV18Lote_UserNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV18Lote_UserNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV22Lote_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV22Lote_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV23Lote_UserNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV23Lote_UserNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV27Lote_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV27Lote_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV28Lote_UserNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV28Lote_UserNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV38TFLote_Numero",SqlDbType.Char,10,0} ,
          new Object[] {"@AV39TFLote_Numero_Sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV42TFLote_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV43TFLote_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV46TFLote_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV47TFLote_Data_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV52TFLote_UserCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV53TFLote_UserCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV56TFLote_PessoaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV57TFLote_PessoaCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV60TFLote_UserNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV61TFLote_UserNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@AV64TFLote_ValorPF",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV65TFLote_ValorPF_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV80TFLote_QtdeDmn",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV81TFLote_QtdeDmn_To",SqlDbType.SmallInt,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00BR2", "SELECT [ContagemResultado_LoteAceiteCod], [ContagemResultado_ValorPF], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_LoteAceiteCod] = @Lote_Codigo ORDER BY [ContagemResultado_LoteAceiteCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BR2,100,0,true,false )
             ,new CursorDef("H00BR9", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BR9,11,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((decimal[]) buf[2])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((DateTime[]) buf[7])[0] = rslt.getGXDateTime(6) ;
                ((String[]) buf[8])[0] = rslt.getString(7, 50) ;
                ((String[]) buf[9])[0] = rslt.getString(8, 10) ;
                ((int[]) buf[10])[0] = rslt.getInt(9) ;
                ((int[]) buf[11])[0] = rslt.getInt(10) ;
                ((short[]) buf[12])[0] = rslt.getShort(11) ;
                ((DateTime[]) buf[13])[0] = rslt.getGXDate(12) ;
                ((DateTime[]) buf[14])[0] = rslt.getGXDate(13) ;
                ((decimal[]) buf[15])[0] = rslt.getDecimal(14) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(14);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[39]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[40]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[42]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[43]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[44]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[45]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[46]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[47]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[48]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[49]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[58]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[65]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[66]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[67]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[68]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[69]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[70]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[71]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[72]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[73]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[74]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[75]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[76]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[77]);
                }
                return;
       }
    }

 }

}
