/*
               File: PRC_EncerrarCicloCorrecao
        Description: Encerrar Ciclo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:8:55.24
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_encerrarciclocorrecao : GXProcedure
   {
      public prc_encerrarciclocorrecao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_encerrarciclocorrecao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo ,
                           DateTime aP1_DateTime )
      {
         this.AV8ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV9DateTime = aP1_DateTime;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_ContagemResultado_Codigo ,
                                 DateTime aP1_DateTime )
      {
         prc_encerrarciclocorrecao objprc_encerrarciclocorrecao;
         objprc_encerrarciclocorrecao = new prc_encerrarciclocorrecao();
         objprc_encerrarciclocorrecao.AV8ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_encerrarciclocorrecao.AV9DateTime = aP1_DateTime;
         objprc_encerrarciclocorrecao.context.SetSubmitInitialConfig(context);
         objprc_encerrarciclocorrecao.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_encerrarciclocorrecao);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_encerrarciclocorrecao)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00XJ2 */
         pr_default.execute(0, new Object[] {AV8ContagemResultado_Codigo, AV9DateTime});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = P00XJ2_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00XJ2_n1553ContagemResultado_CntSrvCod[0];
            A1407ContagemResultadoExecucao_Fim = P00XJ2_A1407ContagemResultadoExecucao_Fim[0];
            n1407ContagemResultadoExecucao_Fim = P00XJ2_n1407ContagemResultadoExecucao_Fim[0];
            A1406ContagemResultadoExecucao_Inicio = P00XJ2_A1406ContagemResultadoExecucao_Inicio[0];
            A1404ContagemResultadoExecucao_OSCod = P00XJ2_A1404ContagemResultadoExecucao_OSCod[0];
            A1611ContagemResultado_PrzTpDias = P00XJ2_A1611ContagemResultado_PrzTpDias[0];
            n1611ContagemResultado_PrzTpDias = P00XJ2_n1611ContagemResultado_PrzTpDias[0];
            A1410ContagemResultadoExecucao_Dias = P00XJ2_A1410ContagemResultadoExecucao_Dias[0];
            n1410ContagemResultadoExecucao_Dias = P00XJ2_n1410ContagemResultadoExecucao_Dias[0];
            A1405ContagemResultadoExecucao_Codigo = P00XJ2_A1405ContagemResultadoExecucao_Codigo[0];
            A1553ContagemResultado_CntSrvCod = P00XJ2_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00XJ2_n1553ContagemResultado_CntSrvCod[0];
            A1611ContagemResultado_PrzTpDias = P00XJ2_A1611ContagemResultado_PrzTpDias[0];
            n1611ContagemResultado_PrzTpDias = P00XJ2_n1611ContagemResultado_PrzTpDias[0];
            A1407ContagemResultadoExecucao_Fim = AV9DateTime;
            n1407ContagemResultadoExecucao_Fim = false;
            GXt_int1 = A1410ContagemResultadoExecucao_Dias;
            new prc_diasuteisentre(context ).execute(  DateTimeUtil.ResetTime( A1406ContagemResultadoExecucao_Inicio),  DateTimeUtil.ResetTime( A1407ContagemResultadoExecucao_Fim),  A1611ContagemResultado_PrzTpDias, out  GXt_int1) ;
            A1410ContagemResultadoExecucao_Dias = GXt_int1;
            n1410ContagemResultadoExecucao_Dias = false;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P00XJ3 */
            pr_default.execute(1, new Object[] {n1407ContagemResultadoExecucao_Fim, A1407ContagemResultadoExecucao_Fim, n1410ContagemResultadoExecucao_Dias, A1410ContagemResultadoExecucao_Dias, A1405ContagemResultadoExecucao_Codigo});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoExecucao") ;
            if (true) break;
            /* Using cursor P00XJ4 */
            pr_default.execute(2, new Object[] {n1407ContagemResultadoExecucao_Fim, A1407ContagemResultadoExecucao_Fim, n1410ContagemResultadoExecucao_Dias, A1410ContagemResultadoExecucao_Dias, A1405ContagemResultadoExecucao_Codigo});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoExecucao") ;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00XJ2_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00XJ2_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00XJ2_A1407ContagemResultadoExecucao_Fim = new DateTime[] {DateTime.MinValue} ;
         P00XJ2_n1407ContagemResultadoExecucao_Fim = new bool[] {false} ;
         P00XJ2_A1406ContagemResultadoExecucao_Inicio = new DateTime[] {DateTime.MinValue} ;
         P00XJ2_A1404ContagemResultadoExecucao_OSCod = new int[1] ;
         P00XJ2_A1611ContagemResultado_PrzTpDias = new String[] {""} ;
         P00XJ2_n1611ContagemResultado_PrzTpDias = new bool[] {false} ;
         P00XJ2_A1410ContagemResultadoExecucao_Dias = new short[1] ;
         P00XJ2_n1410ContagemResultadoExecucao_Dias = new bool[] {false} ;
         P00XJ2_A1405ContagemResultadoExecucao_Codigo = new int[1] ;
         A1407ContagemResultadoExecucao_Fim = (DateTime)(DateTime.MinValue);
         A1406ContagemResultadoExecucao_Inicio = (DateTime)(DateTime.MinValue);
         A1611ContagemResultado_PrzTpDias = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_encerrarciclocorrecao__default(),
            new Object[][] {
                new Object[] {
               P00XJ2_A1553ContagemResultado_CntSrvCod, P00XJ2_n1553ContagemResultado_CntSrvCod, P00XJ2_A1407ContagemResultadoExecucao_Fim, P00XJ2_n1407ContagemResultadoExecucao_Fim, P00XJ2_A1406ContagemResultadoExecucao_Inicio, P00XJ2_A1404ContagemResultadoExecucao_OSCod, P00XJ2_A1611ContagemResultado_PrzTpDias, P00XJ2_n1611ContagemResultado_PrzTpDias, P00XJ2_A1410ContagemResultadoExecucao_Dias, P00XJ2_n1410ContagemResultadoExecucao_Dias,
               P00XJ2_A1405ContagemResultadoExecucao_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A1410ContagemResultadoExecucao_Dias ;
      private short GXt_int1 ;
      private int AV8ContagemResultado_Codigo ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A1404ContagemResultadoExecucao_OSCod ;
      private int A1405ContagemResultadoExecucao_Codigo ;
      private String scmdbuf ;
      private String A1611ContagemResultado_PrzTpDias ;
      private DateTime AV9DateTime ;
      private DateTime A1407ContagemResultadoExecucao_Fim ;
      private DateTime A1406ContagemResultadoExecucao_Inicio ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1407ContagemResultadoExecucao_Fim ;
      private bool n1611ContagemResultado_PrzTpDias ;
      private bool n1410ContagemResultadoExecucao_Dias ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00XJ2_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00XJ2_n1553ContagemResultado_CntSrvCod ;
      private DateTime[] P00XJ2_A1407ContagemResultadoExecucao_Fim ;
      private bool[] P00XJ2_n1407ContagemResultadoExecucao_Fim ;
      private DateTime[] P00XJ2_A1406ContagemResultadoExecucao_Inicio ;
      private int[] P00XJ2_A1404ContagemResultadoExecucao_OSCod ;
      private String[] P00XJ2_A1611ContagemResultado_PrzTpDias ;
      private bool[] P00XJ2_n1611ContagemResultado_PrzTpDias ;
      private short[] P00XJ2_A1410ContagemResultadoExecucao_Dias ;
      private bool[] P00XJ2_n1410ContagemResultadoExecucao_Dias ;
      private int[] P00XJ2_A1405ContagemResultadoExecucao_Codigo ;
   }

   public class prc_encerrarciclocorrecao__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00XJ2 ;
          prmP00XJ2 = new Object[] {
          new Object[] {"@AV8ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9DateTime",SqlDbType.DateTime,8,5}
          } ;
          Object[] prmP00XJ3 ;
          prmP00XJ3 = new Object[] {
          new Object[] {"@ContagemResultadoExecucao_Fim",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoExecucao_Dias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoExecucao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00XJ4 ;
          prmP00XJ4 = new Object[] {
          new Object[] {"@ContagemResultadoExecucao_Fim",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoExecucao_Dias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultadoExecucao_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00XJ2", "SELECT TOP 1 T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultadoExecucao_Fim], T1.[ContagemResultadoExecucao_Inicio], T1.[ContagemResultadoExecucao_OSCod] AS ContagemResultadoExecucao_OSCod, T3.[ContratoServicos_PrazoTpDias] AS ContagemResultado_PrzTpDias, T1.[ContagemResultadoExecucao_Dias], T1.[ContagemResultadoExecucao_Codigo] FROM (([ContagemResultadoExecucao] T1 WITH (UPDLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultadoExecucao_OSCod]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) WHERE (T1.[ContagemResultadoExecucao_OSCod] = @AV8ContagemResultado_Codigo) AND ((T1.[ContagemResultadoExecucao_Fim] = convert( DATETIME, '17530101', 112 ))) AND (T1.[ContagemResultadoExecucao_Inicio] <= @AV9DateTime) ORDER BY T1.[ContagemResultadoExecucao_OSCod] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00XJ2,1,0,true,true )
             ,new CursorDef("P00XJ3", "UPDATE [ContagemResultadoExecucao] SET [ContagemResultadoExecucao_Fim]=@ContagemResultadoExecucao_Fim, [ContagemResultadoExecucao_Dias]=@ContagemResultadoExecucao_Dias  WHERE [ContagemResultadoExecucao_Codigo] = @ContagemResultadoExecucao_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00XJ3)
             ,new CursorDef("P00XJ4", "UPDATE [ContagemResultadoExecucao] SET [ContagemResultadoExecucao_Fim]=@ContagemResultadoExecucao_Fim, [ContagemResultadoExecucao_Dias]=@ContagemResultadoExecucao_Dias  WHERE [ContagemResultadoExecucao_Codigo] = @ContagemResultadoExecucao_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00XJ4)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[4])[0] = rslt.getGXDateTime(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((String[]) buf[6])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((short[]) buf[8])[0] = rslt.getShort(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameterDatetime(2, (DateTime)parms[1]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(2, (short)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(2, (short)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
       }
    }

 }

}
