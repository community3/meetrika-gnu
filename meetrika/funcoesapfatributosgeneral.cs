/*
               File: FuncoesAPFAtributosGeneral
        Description: Funcoes APFAtributos General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:18:24.78
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class funcoesapfatributosgeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public funcoesapfatributosgeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public funcoesapfatributosgeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_FuncaoAPF_Codigo ,
                           int aP1_FuncaoAPFAtributos_AtributosCod )
      {
         this.A165FuncaoAPF_Codigo = aP0_FuncaoAPF_Codigo;
         this.A364FuncaoAPFAtributos_AtributosCod = aP1_FuncaoAPFAtributos_AtributosCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         chkFuncoesAPFAtributos_Regra = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A165FuncaoAPF_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
                  A364FuncaoAPFAtributos_AtributosCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A364FuncaoAPFAtributos_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A364FuncaoAPFAtributos_AtributosCod), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A165FuncaoAPF_Codigo,(int)A364FuncaoAPFAtributos_AtributosCod});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA9O2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV15Pgmname = "FuncoesAPFAtributosGeneral";
               context.Gx_err = 0;
               /* Using cursor H009O2 */
               pr_default.execute(0, new Object[] {A165FuncaoAPF_Codigo});
               A166FuncaoAPF_Nome = H009O2_A166FuncaoAPF_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A166FuncaoAPF_Nome", A166FuncaoAPF_Nome);
               pr_default.close(0);
               /* Using cursor H009O3 */
               pr_default.execute(1, new Object[] {A364FuncaoAPFAtributos_AtributosCod});
               A366FuncaoAPFAtributos_AtrTabelaCod = H009O3_A366FuncaoAPFAtributos_AtrTabelaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A366FuncaoAPFAtributos_AtrTabelaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A366FuncaoAPFAtributos_AtrTabelaCod), 6, 0)));
               n366FuncaoAPFAtributos_AtrTabelaCod = H009O3_n366FuncaoAPFAtributos_AtrTabelaCod[0];
               A365FuncaoAPFAtributos_AtributosNom = H009O3_A365FuncaoAPFAtributos_AtributosNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A365FuncaoAPFAtributos_AtributosNom", A365FuncaoAPFAtributos_AtributosNom);
               n365FuncaoAPFAtributos_AtributosNom = H009O3_n365FuncaoAPFAtributos_AtributosNom[0];
               pr_default.close(1);
               /* Using cursor H009O4 */
               pr_default.execute(2, new Object[] {n366FuncaoAPFAtributos_AtrTabelaCod, A366FuncaoAPFAtributos_AtrTabelaCod});
               A367FuncaoAPFAtributos_AtrTabelaNom = H009O4_A367FuncaoAPFAtributos_AtrTabelaNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A367FuncaoAPFAtributos_AtrTabelaNom", A367FuncaoAPFAtributos_AtrTabelaNom);
               n367FuncaoAPFAtributos_AtrTabelaNom = H009O4_n367FuncaoAPFAtributos_AtrTabelaNom[0];
               pr_default.close(2);
               WS9O2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Funcoes APFAtributos General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117182492");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("funcoesapfatributosgeneral.aspx") + "?" + UrlEncode("" +A165FuncaoAPF_Codigo) + "," + UrlEncode("" +A364FuncaoAPFAtributos_AtributosCod)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA165FuncaoAPF_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA364FuncaoAPFAtributos_AtributosCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA364FuncaoAPFAtributos_AtributosCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A378FuncaoAPFAtributos_FuncaoDadosCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCOESAPFATRIBUTOS_REGRA", GetSecureSignedToken( sPrefix, A389FuncoesAPFAtributos_Regra));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCOESAPFATRIBUTOS_CODE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A383FuncoesAPFAtributos_Code, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCOESAPFATRIBUTOS_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A384FuncoesAPFAtributos_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCOESAPFATRIBUTOS_DESCRICAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A385FuncoesAPFAtributos_Descricao, "@!"))));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm9O2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("funcoesapfatributosgeneral.js", "?20203117182495");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "FuncoesAPFAtributosGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Funcoes APFAtributos General" ;
      }

      protected void WB9O0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "funcoesapfatributosgeneral.aspx");
            }
            wb_table1_2_9O2( true) ;
         }
         else
         {
            wb_table1_2_9O2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_9O2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoAPFAtributos_AtributosCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A364FuncaoAPFAtributos_AtributosCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A364FuncaoAPFAtributos_AtributosCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoAPFAtributos_AtributosCod_Jsonclick, 0, "Attribute", "", "", "", edtFuncaoAPFAtributos_AtributosCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_FuncoesAPFAtributosGeneral.htm");
         }
         wbLoad = true;
      }

      protected void START9O2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Funcoes APFAtributos General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP9O0( ) ;
            }
         }
      }

      protected void WS9O2( )
      {
         START9O2( ) ;
         EVT9O2( ) ;
      }

      protected void EVT9O2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9O0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9O0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E119O2 */
                                    E119O2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9O0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E129O2 */
                                    E129O2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9O0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E139O2 */
                                    E139O2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9O0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E149O2 */
                                    E149O2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9O0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9O0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE9O2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm9O2( ) ;
            }
         }
      }

      protected void PA9O2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            chkFuncoesAPFAtributos_Regra.Name = "FUNCOESAPFATRIBUTOS_REGRA";
            chkFuncoesAPFAtributos_Regra.WebTags = "";
            chkFuncoesAPFAtributos_Regra.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkFuncoesAPFAtributos_Regra_Internalname, "TitleCaption", chkFuncoesAPFAtributos_Regra.Caption);
            chkFuncoesAPFAtributos_Regra.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF9O2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV15Pgmname = "FuncoesAPFAtributosGeneral";
         context.Gx_err = 0;
      }

      protected void RF9O2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H009O5 */
            pr_default.execute(3, new Object[] {A165FuncaoAPF_Codigo, A364FuncaoAPFAtributos_AtributosCod});
            while ( (pr_default.getStatus(3) != 101) )
            {
               A385FuncoesAPFAtributos_Descricao = H009O5_A385FuncoesAPFAtributos_Descricao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A385FuncoesAPFAtributos_Descricao", A385FuncoesAPFAtributos_Descricao);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCOESAPFATRIBUTOS_DESCRICAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A385FuncoesAPFAtributos_Descricao, "@!"))));
               n385FuncoesAPFAtributos_Descricao = H009O5_n385FuncoesAPFAtributos_Descricao[0];
               A384FuncoesAPFAtributos_Nome = H009O5_A384FuncoesAPFAtributos_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A384FuncoesAPFAtributos_Nome", A384FuncoesAPFAtributos_Nome);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCOESAPFATRIBUTOS_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A384FuncoesAPFAtributos_Nome, "@!"))));
               n384FuncoesAPFAtributos_Nome = H009O5_n384FuncoesAPFAtributos_Nome[0];
               A383FuncoesAPFAtributos_Code = H009O5_A383FuncoesAPFAtributos_Code[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A383FuncoesAPFAtributos_Code", A383FuncoesAPFAtributos_Code);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCOESAPFATRIBUTOS_CODE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A383FuncoesAPFAtributos_Code, ""))));
               n383FuncoesAPFAtributos_Code = H009O5_n383FuncoesAPFAtributos_Code[0];
               A389FuncoesAPFAtributos_Regra = H009O5_A389FuncoesAPFAtributos_Regra[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A389FuncoesAPFAtributos_Regra", A389FuncoesAPFAtributos_Regra);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCOESAPFATRIBUTOS_REGRA", GetSecureSignedToken( sPrefix, A389FuncoesAPFAtributos_Regra));
               n389FuncoesAPFAtributos_Regra = H009O5_n389FuncoesAPFAtributos_Regra[0];
               A378FuncaoAPFAtributos_FuncaoDadosCod = H009O5_A378FuncaoAPFAtributos_FuncaoDadosCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A378FuncaoAPFAtributos_FuncaoDadosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A378FuncaoAPFAtributos_FuncaoDadosCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A378FuncaoAPFAtributos_FuncaoDadosCod), "ZZZZZ9")));
               n378FuncaoAPFAtributos_FuncaoDadosCod = H009O5_n378FuncaoAPFAtributos_FuncaoDadosCod[0];
               /* Execute user event: E129O2 */
               E129O2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(3);
            WB9O0( ) ;
         }
      }

      protected void STRUP9O0( )
      {
         /* Before Start, stand alone formulas. */
         AV15Pgmname = "FuncoesAPFAtributosGeneral";
         context.Gx_err = 0;
         /* Using cursor H009O6 */
         pr_default.execute(4, new Object[] {A165FuncaoAPF_Codigo});
         A166FuncaoAPF_Nome = H009O6_A166FuncaoAPF_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A166FuncaoAPF_Nome", A166FuncaoAPF_Nome);
         pr_default.close(4);
         /* Using cursor H009O7 */
         pr_default.execute(5, new Object[] {A364FuncaoAPFAtributos_AtributosCod});
         A366FuncaoAPFAtributos_AtrTabelaCod = H009O7_A366FuncaoAPFAtributos_AtrTabelaCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A366FuncaoAPFAtributos_AtrTabelaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A366FuncaoAPFAtributos_AtrTabelaCod), 6, 0)));
         n366FuncaoAPFAtributos_AtrTabelaCod = H009O7_n366FuncaoAPFAtributos_AtrTabelaCod[0];
         A365FuncaoAPFAtributos_AtributosNom = H009O7_A365FuncaoAPFAtributos_AtributosNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A365FuncaoAPFAtributos_AtributosNom", A365FuncaoAPFAtributos_AtributosNom);
         n365FuncaoAPFAtributos_AtributosNom = H009O7_n365FuncaoAPFAtributos_AtributosNom[0];
         pr_default.close(5);
         /* Using cursor H009O8 */
         pr_default.execute(6, new Object[] {n366FuncaoAPFAtributos_AtrTabelaCod, A366FuncaoAPFAtributos_AtrTabelaCod});
         A367FuncaoAPFAtributos_AtrTabelaNom = H009O8_A367FuncaoAPFAtributos_AtrTabelaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A367FuncaoAPFAtributos_AtrTabelaNom", A367FuncaoAPFAtributos_AtrTabelaNom);
         n367FuncaoAPFAtributos_AtrTabelaNom = H009O8_n367FuncaoAPFAtributos_AtrTabelaNom[0];
         pr_default.close(6);
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E119O2 */
         E119O2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A166FuncaoAPF_Nome = cgiGet( edtFuncaoAPF_Nome_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A166FuncaoAPF_Nome", A166FuncaoAPF_Nome);
            A365FuncaoAPFAtributos_AtributosNom = StringUtil.Upper( cgiGet( edtFuncaoAPFAtributos_AtributosNom_Internalname));
            n365FuncaoAPFAtributos_AtributosNom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A365FuncaoAPFAtributos_AtributosNom", A365FuncaoAPFAtributos_AtributosNom);
            A378FuncaoAPFAtributos_FuncaoDadosCod = (int)(context.localUtil.CToN( cgiGet( edtFuncaoAPFAtributos_FuncaoDadosCod_Internalname), ",", "."));
            n378FuncaoAPFAtributos_FuncaoDadosCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A378FuncaoAPFAtributos_FuncaoDadosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A378FuncaoAPFAtributos_FuncaoDadosCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A378FuncaoAPFAtributos_FuncaoDadosCod), "ZZZZZ9")));
            A366FuncaoAPFAtributos_AtrTabelaCod = (int)(context.localUtil.CToN( cgiGet( edtFuncaoAPFAtributos_AtrTabelaCod_Internalname), ",", "."));
            n366FuncaoAPFAtributos_AtrTabelaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A366FuncaoAPFAtributos_AtrTabelaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A366FuncaoAPFAtributos_AtrTabelaCod), 6, 0)));
            A367FuncaoAPFAtributos_AtrTabelaNom = StringUtil.Upper( cgiGet( edtFuncaoAPFAtributos_AtrTabelaNom_Internalname));
            n367FuncaoAPFAtributos_AtrTabelaNom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A367FuncaoAPFAtributos_AtrTabelaNom", A367FuncaoAPFAtributos_AtrTabelaNom);
            A389FuncoesAPFAtributos_Regra = StringUtil.StrToBool( cgiGet( chkFuncoesAPFAtributos_Regra_Internalname));
            n389FuncoesAPFAtributos_Regra = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A389FuncoesAPFAtributos_Regra", A389FuncoesAPFAtributos_Regra);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCOESAPFATRIBUTOS_REGRA", GetSecureSignedToken( sPrefix, A389FuncoesAPFAtributos_Regra));
            A383FuncoesAPFAtributos_Code = cgiGet( edtFuncoesAPFAtributos_Code_Internalname);
            n383FuncoesAPFAtributos_Code = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A383FuncoesAPFAtributos_Code", A383FuncoesAPFAtributos_Code);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCOESAPFATRIBUTOS_CODE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A383FuncoesAPFAtributos_Code, ""))));
            A384FuncoesAPFAtributos_Nome = StringUtil.Upper( cgiGet( edtFuncoesAPFAtributos_Nome_Internalname));
            n384FuncoesAPFAtributos_Nome = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A384FuncoesAPFAtributos_Nome", A384FuncoesAPFAtributos_Nome);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCOESAPFATRIBUTOS_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A384FuncoesAPFAtributos_Nome, "@!"))));
            A385FuncoesAPFAtributos_Descricao = StringUtil.Upper( cgiGet( edtFuncoesAPFAtributos_Descricao_Internalname));
            n385FuncoesAPFAtributos_Descricao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A385FuncoesAPFAtributos_Descricao", A385FuncoesAPFAtributos_Descricao);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCOESAPFATRIBUTOS_DESCRICAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A385FuncoesAPFAtributos_Descricao, "@!"))));
            /* Read saved values. */
            wcpOA165FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA165FuncaoAPF_Codigo"), ",", "."));
            wcpOA364FuncaoAPFAtributos_AtributosCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA364FuncaoAPFAtributos_AtributosCod"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E119O2 */
         E119O2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E119O2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         bttBtnupdate_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Visible), 5, 0)));
         bttBtndelete_Visible = (AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Visible), 5, 0)));
      }

      protected void nextLoad( )
      {
      }

      protected void E129O2( )
      {
         /* Load Routine */
         edtFuncaoAPFAtributos_AtrTabelaNom_Link = formatLink("viewtabela.aspx") + "?" + UrlEncode("" +A366FuncaoAPFAtributos_AtrTabelaCod) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoAPFAtributos_AtrTabelaNom_Internalname, "Link", edtFuncaoAPFAtributos_AtrTabelaNom_Link);
         edtFuncaoAPFAtributos_AtributosCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoAPFAtributos_AtributosCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPFAtributos_AtributosCod_Visible), 5, 0)));
      }

      protected void E139O2( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("funcoesapfatributos.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A165FuncaoAPF_Codigo) + "," + UrlEncode("" +A364FuncaoAPFAtributos_AtributosCod);
         context.wjLocDisableFrm = 1;
      }

      protected void E149O2( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("funcoesapfatributos.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A165FuncaoAPF_Codigo) + "," + UrlEncode("" +A364FuncaoAPFAtributos_AtributosCod);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV15Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = false;
         AV9TrnContext.gxTpr_Callerurl = AV12HTTPRequest.ScriptName+"?"+AV12HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "FuncoesAPFAtributos";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "FuncaoAPF_Codigo";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7FuncaoAPF_Codigo), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "FuncaoAPFAtributos_AtributosCod";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV8FuncaoAPFAtributos_AtributosCod), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV11Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_9O2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_9O2( true) ;
         }
         else
         {
            wb_table2_8_9O2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_9O2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_61_9O2( true) ;
         }
         else
         {
            wb_table3_61_9O2( false) ;
         }
         return  ;
      }

      protected void wb_table3_61_9O2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_9O2e( true) ;
         }
         else
         {
            wb_table1_2_9O2e( false) ;
         }
      }

      protected void wb_table3_61_9O2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, bttBtnupdate_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_FuncoesAPFAtributosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtndelete_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_FuncoesAPFAtributosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_61_9O2e( true) ;
         }
         else
         {
            wb_table3_61_9O2e( false) ;
         }
      }

      protected void wb_table2_8_9O2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapf_codigo_Internalname, "Fun��o de Transa��o", "", "", lblTextblockfuncaoapf_codigo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncoesAPFAtributosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoAPF_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoAPF_Codigo_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_FuncoesAPFAtributosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapf_nome_Internalname, "Fun��o de Transa��o", "", "", lblTextblockfuncaoapf_nome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncoesAPFAtributosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtFuncaoAPF_Nome_Internalname, A166FuncaoAPF_Nome, "", "", 0, 1, 0, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_FuncoesAPFAtributosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapfatributos_atributosnom_Internalname, "Atributo", "", "", lblTextblockfuncaoapfatributos_atributosnom_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncoesAPFAtributosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoAPFAtributos_AtributosNom_Internalname, StringUtil.RTrim( A365FuncaoAPFAtributos_AtributosNom), StringUtil.RTrim( context.localUtil.Format( A365FuncaoAPFAtributos_AtributosNom, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoAPFAtributos_AtributosNom_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_FuncoesAPFAtributosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapfatributos_funcaodadoscod_Internalname, "Dados", "", "", lblTextblockfuncaoapfatributos_funcaodadoscod_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncoesAPFAtributosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoAPFAtributos_FuncaoDadosCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A378FuncaoAPFAtributos_FuncaoDadosCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A378FuncaoAPFAtributos_FuncaoDadosCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoAPFAtributos_FuncaoDadosCod_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_FuncoesAPFAtributosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapfatributos_atrtabelacod_Internalname, "Tabela", "", "", lblTextblockfuncaoapfatributos_atrtabelacod_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncoesAPFAtributosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoAPFAtributos_AtrTabelaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A366FuncaoAPFAtributos_AtrTabelaCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A366FuncaoAPFAtributos_AtrTabelaCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoAPFAtributos_AtrTabelaCod_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_FuncoesAPFAtributosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapfatributos_atrtabelanom_Internalname, "Tabela", "", "", lblTextblockfuncaoapfatributos_atrtabelanom_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncoesAPFAtributosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoAPFAtributos_AtrTabelaNom_Internalname, StringUtil.RTrim( A367FuncaoAPFAtributos_AtrTabelaNom), StringUtil.RTrim( context.localUtil.Format( A367FuncaoAPFAtributos_AtrTabelaNom, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtFuncaoAPFAtributos_AtrTabelaNom_Link, "", "", "", edtFuncaoAPFAtributos_AtrTabelaNom_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_FuncoesAPFAtributosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncoesapfatributos_regra_Internalname, "de Neg�cio", "", "", lblTextblockfuncoesapfatributos_regra_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncoesAPFAtributosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Check box */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkFuncoesAPFAtributos_Regra_Internalname, StringUtil.BoolToStr( A389FuncoesAPFAtributos_Regra), "", "", 1, 0, "true", "", StyleString, ClassString, "", "");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncoesapfatributos_code_Internalname, "Codigo", "", "", lblTextblockfuncoesapfatributos_code_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncoesAPFAtributosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncoesAPFAtributos_Code_Internalname, A383FuncoesAPFAtributos_Code, StringUtil.RTrim( context.localUtil.Format( A383FuncoesAPFAtributos_Code, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncoesAPFAtributos_Code_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_FuncoesAPFAtributosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncoesapfatributos_nome_Internalname, "Nome", "", "", lblTextblockfuncoesapfatributos_nome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncoesAPFAtributosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncoesAPFAtributos_Nome_Internalname, StringUtil.RTrim( A384FuncoesAPFAtributos_Nome), StringUtil.RTrim( context.localUtil.Format( A384FuncoesAPFAtributos_Nome, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncoesAPFAtributos_Nome_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_FuncoesAPFAtributosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncoesapfatributos_descricao_Internalname, "Descri��o", "", "", lblTextblockfuncoesapfatributos_descricao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_FuncoesAPFAtributosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncoesAPFAtributos_Descricao_Internalname, A385FuncoesAPFAtributos_Descricao, StringUtil.RTrim( context.localUtil.Format( A385FuncoesAPFAtributos_Descricao, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncoesAPFAtributos_Descricao_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_FuncoesAPFAtributosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_9O2e( true) ;
         }
         else
         {
            wb_table2_8_9O2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A165FuncaoAPF_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
         A364FuncaoAPFAtributos_AtributosCod = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A364FuncaoAPFAtributos_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A364FuncaoAPFAtributos_AtributosCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA9O2( ) ;
         WS9O2( ) ;
         WE9O2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA165FuncaoAPF_Codigo = (String)((String)getParm(obj,0));
         sCtrlA364FuncaoAPFAtributos_AtributosCod = (String)((String)getParm(obj,1));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA9O2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "funcoesapfatributosgeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA9O2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A165FuncaoAPF_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
            A364FuncaoAPFAtributos_AtributosCod = Convert.ToInt32(getParm(obj,3));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A364FuncaoAPFAtributos_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A364FuncaoAPFAtributos_AtributosCod), 6, 0)));
         }
         wcpOA165FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA165FuncaoAPF_Codigo"), ",", "."));
         wcpOA364FuncaoAPFAtributos_AtributosCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA364FuncaoAPFAtributos_AtributosCod"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A165FuncaoAPF_Codigo != wcpOA165FuncaoAPF_Codigo ) || ( A364FuncaoAPFAtributos_AtributosCod != wcpOA364FuncaoAPFAtributos_AtributosCod ) ) )
         {
            setjustcreated();
         }
         wcpOA165FuncaoAPF_Codigo = A165FuncaoAPF_Codigo;
         wcpOA364FuncaoAPFAtributos_AtributosCod = A364FuncaoAPFAtributos_AtributosCod;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA165FuncaoAPF_Codigo = cgiGet( sPrefix+"A165FuncaoAPF_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA165FuncaoAPF_Codigo) > 0 )
         {
            A165FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA165FuncaoAPF_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
         }
         else
         {
            A165FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A165FuncaoAPF_Codigo_PARM"), ",", "."));
         }
         sCtrlA364FuncaoAPFAtributos_AtributosCod = cgiGet( sPrefix+"A364FuncaoAPFAtributos_AtributosCod_CTRL");
         if ( StringUtil.Len( sCtrlA364FuncaoAPFAtributos_AtributosCod) > 0 )
         {
            A364FuncaoAPFAtributos_AtributosCod = (int)(context.localUtil.CToN( cgiGet( sCtrlA364FuncaoAPFAtributos_AtributosCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A364FuncaoAPFAtributos_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A364FuncaoAPFAtributos_AtributosCod), 6, 0)));
         }
         else
         {
            A364FuncaoAPFAtributos_AtributosCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A364FuncaoAPFAtributos_AtributosCod_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA9O2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS9O2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS9O2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A165FuncaoAPF_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA165FuncaoAPF_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A165FuncaoAPF_Codigo_CTRL", StringUtil.RTrim( sCtrlA165FuncaoAPF_Codigo));
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"A364FuncaoAPFAtributos_AtributosCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A364FuncaoAPFAtributos_AtributosCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA364FuncaoAPFAtributos_AtributosCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A364FuncaoAPFAtributos_AtributosCod_CTRL", StringUtil.RTrim( sCtrlA364FuncaoAPFAtributos_AtributosCod));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE9O2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117182564");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("funcoesapfatributosgeneral.js", "?20203117182565");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockfuncaoapf_codigo_Internalname = sPrefix+"TEXTBLOCKFUNCAOAPF_CODIGO";
         edtFuncaoAPF_Codigo_Internalname = sPrefix+"FUNCAOAPF_CODIGO";
         lblTextblockfuncaoapf_nome_Internalname = sPrefix+"TEXTBLOCKFUNCAOAPF_NOME";
         edtFuncaoAPF_Nome_Internalname = sPrefix+"FUNCAOAPF_NOME";
         lblTextblockfuncaoapfatributos_atributosnom_Internalname = sPrefix+"TEXTBLOCKFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM";
         edtFuncaoAPFAtributos_AtributosNom_Internalname = sPrefix+"FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM";
         lblTextblockfuncaoapfatributos_funcaodadoscod_Internalname = sPrefix+"TEXTBLOCKFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD";
         edtFuncaoAPFAtributos_FuncaoDadosCod_Internalname = sPrefix+"FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD";
         lblTextblockfuncaoapfatributos_atrtabelacod_Internalname = sPrefix+"TEXTBLOCKFUNCAOAPFATRIBUTOS_ATRTABELACOD";
         edtFuncaoAPFAtributos_AtrTabelaCod_Internalname = sPrefix+"FUNCAOAPFATRIBUTOS_ATRTABELACOD";
         lblTextblockfuncaoapfatributos_atrtabelanom_Internalname = sPrefix+"TEXTBLOCKFUNCAOAPFATRIBUTOS_ATRTABELANOM";
         edtFuncaoAPFAtributos_AtrTabelaNom_Internalname = sPrefix+"FUNCAOAPFATRIBUTOS_ATRTABELANOM";
         lblTextblockfuncoesapfatributos_regra_Internalname = sPrefix+"TEXTBLOCKFUNCOESAPFATRIBUTOS_REGRA";
         chkFuncoesAPFAtributos_Regra_Internalname = sPrefix+"FUNCOESAPFATRIBUTOS_REGRA";
         lblTextblockfuncoesapfatributos_code_Internalname = sPrefix+"TEXTBLOCKFUNCOESAPFATRIBUTOS_CODE";
         edtFuncoesAPFAtributos_Code_Internalname = sPrefix+"FUNCOESAPFATRIBUTOS_CODE";
         lblTextblockfuncoesapfatributos_nome_Internalname = sPrefix+"TEXTBLOCKFUNCOESAPFATRIBUTOS_NOME";
         edtFuncoesAPFAtributos_Nome_Internalname = sPrefix+"FUNCOESAPFATRIBUTOS_NOME";
         lblTextblockfuncoesapfatributos_descricao_Internalname = sPrefix+"TEXTBLOCKFUNCOESAPFATRIBUTOS_DESCRICAO";
         edtFuncoesAPFAtributos_Descricao_Internalname = sPrefix+"FUNCOESAPFATRIBUTOS_DESCRICAO";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         edtFuncaoAPFAtributos_AtributosCod_Internalname = sPrefix+"FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtFuncoesAPFAtributos_Descricao_Jsonclick = "";
         edtFuncoesAPFAtributos_Nome_Jsonclick = "";
         edtFuncoesAPFAtributos_Code_Jsonclick = "";
         edtFuncaoAPFAtributos_AtrTabelaNom_Jsonclick = "";
         edtFuncaoAPFAtributos_AtrTabelaCod_Jsonclick = "";
         edtFuncaoAPFAtributos_FuncaoDadosCod_Jsonclick = "";
         edtFuncaoAPFAtributos_AtributosNom_Jsonclick = "";
         edtFuncaoAPF_Codigo_Jsonclick = "";
         bttBtndelete_Visible = 1;
         bttBtnupdate_Visible = 1;
         edtFuncaoAPFAtributos_AtrTabelaNom_Link = "";
         chkFuncoesAPFAtributos_Regra.Caption = "";
         edtFuncaoAPFAtributos_AtributosCod_Jsonclick = "";
         edtFuncaoAPFAtributos_AtributosCod_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E139O2',iparms:[{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A364FuncaoAPFAtributos_AtributosCod',fld:'FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E149O2',iparms:[{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A364FuncaoAPFAtributos_AtributosCod',fld:'FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV15Pgmname = "";
         scmdbuf = "";
         H009O2_A166FuncaoAPF_Nome = new String[] {""} ;
         A166FuncaoAPF_Nome = "";
         H009O3_A366FuncaoAPFAtributos_AtrTabelaCod = new int[1] ;
         H009O3_n366FuncaoAPFAtributos_AtrTabelaCod = new bool[] {false} ;
         H009O3_A365FuncaoAPFAtributos_AtributosNom = new String[] {""} ;
         H009O3_n365FuncaoAPFAtributos_AtributosNom = new bool[] {false} ;
         A365FuncaoAPFAtributos_AtributosNom = "";
         H009O4_A367FuncaoAPFAtributos_AtrTabelaNom = new String[] {""} ;
         H009O4_n367FuncaoAPFAtributos_AtrTabelaNom = new bool[] {false} ;
         A367FuncaoAPFAtributos_AtrTabelaNom = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A383FuncoesAPFAtributos_Code = "";
         A384FuncoesAPFAtributos_Nome = "";
         A385FuncoesAPFAtributos_Descricao = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         H009O5_A165FuncaoAPF_Codigo = new int[1] ;
         H009O5_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         H009O5_A385FuncoesAPFAtributos_Descricao = new String[] {""} ;
         H009O5_n385FuncoesAPFAtributos_Descricao = new bool[] {false} ;
         H009O5_A384FuncoesAPFAtributos_Nome = new String[] {""} ;
         H009O5_n384FuncoesAPFAtributos_Nome = new bool[] {false} ;
         H009O5_A383FuncoesAPFAtributos_Code = new String[] {""} ;
         H009O5_n383FuncoesAPFAtributos_Code = new bool[] {false} ;
         H009O5_A389FuncoesAPFAtributos_Regra = new bool[] {false} ;
         H009O5_n389FuncoesAPFAtributos_Regra = new bool[] {false} ;
         H009O5_A367FuncaoAPFAtributos_AtrTabelaNom = new String[] {""} ;
         H009O5_n367FuncaoAPFAtributos_AtrTabelaNom = new bool[] {false} ;
         H009O5_A366FuncaoAPFAtributos_AtrTabelaCod = new int[1] ;
         H009O5_n366FuncaoAPFAtributos_AtrTabelaCod = new bool[] {false} ;
         H009O5_A378FuncaoAPFAtributos_FuncaoDadosCod = new int[1] ;
         H009O5_n378FuncaoAPFAtributos_FuncaoDadosCod = new bool[] {false} ;
         H009O5_A365FuncaoAPFAtributos_AtributosNom = new String[] {""} ;
         H009O5_n365FuncaoAPFAtributos_AtributosNom = new bool[] {false} ;
         H009O5_A166FuncaoAPF_Nome = new String[] {""} ;
         H009O6_A166FuncaoAPF_Nome = new String[] {""} ;
         H009O7_A366FuncaoAPFAtributos_AtrTabelaCod = new int[1] ;
         H009O7_n366FuncaoAPFAtributos_AtrTabelaCod = new bool[] {false} ;
         H009O7_A365FuncaoAPFAtributos_AtributosNom = new String[] {""} ;
         H009O7_n365FuncaoAPFAtributos_AtributosNom = new bool[] {false} ;
         H009O8_A367FuncaoAPFAtributos_AtrTabelaNom = new String[] {""} ;
         H009O8_n367FuncaoAPFAtributos_AtrTabelaNom = new bool[] {false} ;
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV12HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV11Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         lblTextblockfuncaoapf_codigo_Jsonclick = "";
         lblTextblockfuncaoapf_nome_Jsonclick = "";
         lblTextblockfuncaoapfatributos_atributosnom_Jsonclick = "";
         lblTextblockfuncaoapfatributos_funcaodadoscod_Jsonclick = "";
         lblTextblockfuncaoapfatributos_atrtabelacod_Jsonclick = "";
         lblTextblockfuncaoapfatributos_atrtabelanom_Jsonclick = "";
         lblTextblockfuncoesapfatributos_regra_Jsonclick = "";
         lblTextblockfuncoesapfatributos_code_Jsonclick = "";
         lblTextblockfuncoesapfatributos_nome_Jsonclick = "";
         lblTextblockfuncoesapfatributos_descricao_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA165FuncaoAPF_Codigo = "";
         sCtrlA364FuncaoAPFAtributos_AtributosCod = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.funcoesapfatributosgeneral__default(),
            new Object[][] {
                new Object[] {
               H009O2_A166FuncaoAPF_Nome
               }
               , new Object[] {
               H009O3_A366FuncaoAPFAtributos_AtrTabelaCod, H009O3_n366FuncaoAPFAtributos_AtrTabelaCod, H009O3_A365FuncaoAPFAtributos_AtributosNom, H009O3_n365FuncaoAPFAtributos_AtributosNom
               }
               , new Object[] {
               H009O4_A367FuncaoAPFAtributos_AtrTabelaNom, H009O4_n367FuncaoAPFAtributos_AtrTabelaNom
               }
               , new Object[] {
               H009O5_A165FuncaoAPF_Codigo, H009O5_A364FuncaoAPFAtributos_AtributosCod, H009O5_A385FuncoesAPFAtributos_Descricao, H009O5_n385FuncoesAPFAtributos_Descricao, H009O5_A384FuncoesAPFAtributos_Nome, H009O5_n384FuncoesAPFAtributos_Nome, H009O5_A383FuncoesAPFAtributos_Code, H009O5_n383FuncoesAPFAtributos_Code, H009O5_A389FuncoesAPFAtributos_Regra, H009O5_n389FuncoesAPFAtributos_Regra,
               H009O5_A367FuncaoAPFAtributos_AtrTabelaNom, H009O5_n367FuncaoAPFAtributos_AtrTabelaNom, H009O5_A366FuncaoAPFAtributos_AtrTabelaCod, H009O5_n366FuncaoAPFAtributos_AtrTabelaCod, H009O5_A378FuncaoAPFAtributos_FuncaoDadosCod, H009O5_n378FuncaoAPFAtributos_FuncaoDadosCod, H009O5_A365FuncaoAPFAtributos_AtributosNom, H009O5_n365FuncaoAPFAtributos_AtributosNom, H009O5_A166FuncaoAPF_Nome
               }
               , new Object[] {
               H009O6_A166FuncaoAPF_Nome
               }
               , new Object[] {
               H009O7_A366FuncaoAPFAtributos_AtrTabelaCod, H009O7_n366FuncaoAPFAtributos_AtrTabelaCod, H009O7_A365FuncaoAPFAtributos_AtributosNom, H009O7_n365FuncaoAPFAtributos_AtributosNom
               }
               , new Object[] {
               H009O8_A367FuncaoAPFAtributos_AtrTabelaNom, H009O8_n367FuncaoAPFAtributos_AtrTabelaNom
               }
            }
         );
         AV15Pgmname = "FuncoesAPFAtributosGeneral";
         /* GeneXus formulas. */
         AV15Pgmname = "FuncoesAPFAtributosGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A165FuncaoAPF_Codigo ;
      private int A364FuncaoAPFAtributos_AtributosCod ;
      private int wcpOA165FuncaoAPF_Codigo ;
      private int wcpOA364FuncaoAPFAtributos_AtributosCod ;
      private int A366FuncaoAPFAtributos_AtrTabelaCod ;
      private int A378FuncaoAPFAtributos_FuncaoDadosCod ;
      private int edtFuncaoAPFAtributos_AtributosCod_Visible ;
      private int bttBtnupdate_Visible ;
      private int bttBtndelete_Visible ;
      private int AV7FuncaoAPF_Codigo ;
      private int AV8FuncaoAPFAtributos_AtributosCod ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV15Pgmname ;
      private String scmdbuf ;
      private String A365FuncaoAPFAtributos_AtributosNom ;
      private String A367FuncaoAPFAtributos_AtrTabelaNom ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A384FuncoesAPFAtributos_Nome ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String edtFuncaoAPFAtributos_AtributosCod_Internalname ;
      private String edtFuncaoAPFAtributos_AtributosCod_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String chkFuncoesAPFAtributos_Regra_Internalname ;
      private String edtFuncaoAPF_Nome_Internalname ;
      private String edtFuncaoAPFAtributos_AtributosNom_Internalname ;
      private String edtFuncaoAPFAtributos_FuncaoDadosCod_Internalname ;
      private String edtFuncaoAPFAtributos_AtrTabelaCod_Internalname ;
      private String edtFuncaoAPFAtributos_AtrTabelaNom_Internalname ;
      private String edtFuncoesAPFAtributos_Code_Internalname ;
      private String edtFuncoesAPFAtributos_Nome_Internalname ;
      private String edtFuncoesAPFAtributos_Descricao_Internalname ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String edtFuncaoAPFAtributos_AtrTabelaNom_Link ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockfuncaoapf_codigo_Internalname ;
      private String lblTextblockfuncaoapf_codigo_Jsonclick ;
      private String edtFuncaoAPF_Codigo_Internalname ;
      private String edtFuncaoAPF_Codigo_Jsonclick ;
      private String lblTextblockfuncaoapf_nome_Internalname ;
      private String lblTextblockfuncaoapf_nome_Jsonclick ;
      private String lblTextblockfuncaoapfatributos_atributosnom_Internalname ;
      private String lblTextblockfuncaoapfatributos_atributosnom_Jsonclick ;
      private String edtFuncaoAPFAtributos_AtributosNom_Jsonclick ;
      private String lblTextblockfuncaoapfatributos_funcaodadoscod_Internalname ;
      private String lblTextblockfuncaoapfatributos_funcaodadoscod_Jsonclick ;
      private String edtFuncaoAPFAtributos_FuncaoDadosCod_Jsonclick ;
      private String lblTextblockfuncaoapfatributos_atrtabelacod_Internalname ;
      private String lblTextblockfuncaoapfatributos_atrtabelacod_Jsonclick ;
      private String edtFuncaoAPFAtributos_AtrTabelaCod_Jsonclick ;
      private String lblTextblockfuncaoapfatributos_atrtabelanom_Internalname ;
      private String lblTextblockfuncaoapfatributos_atrtabelanom_Jsonclick ;
      private String edtFuncaoAPFAtributos_AtrTabelaNom_Jsonclick ;
      private String lblTextblockfuncoesapfatributos_regra_Internalname ;
      private String lblTextblockfuncoesapfatributos_regra_Jsonclick ;
      private String lblTextblockfuncoesapfatributos_code_Internalname ;
      private String lblTextblockfuncoesapfatributos_code_Jsonclick ;
      private String edtFuncoesAPFAtributos_Code_Jsonclick ;
      private String lblTextblockfuncoesapfatributos_nome_Internalname ;
      private String lblTextblockfuncoesapfatributos_nome_Jsonclick ;
      private String edtFuncoesAPFAtributos_Nome_Jsonclick ;
      private String lblTextblockfuncoesapfatributos_descricao_Internalname ;
      private String lblTextblockfuncoesapfatributos_descricao_Jsonclick ;
      private String edtFuncoesAPFAtributos_Descricao_Jsonclick ;
      private String sCtrlA165FuncaoAPF_Codigo ;
      private String sCtrlA364FuncaoAPFAtributos_AtributosCod ;
      private bool entryPointCalled ;
      private bool n366FuncaoAPFAtributos_AtrTabelaCod ;
      private bool n365FuncaoAPFAtributos_AtributosNom ;
      private bool n367FuncaoAPFAtributos_AtrTabelaNom ;
      private bool toggleJsOutput ;
      private bool A389FuncoesAPFAtributos_Regra ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n385FuncoesAPFAtributos_Descricao ;
      private bool n384FuncoesAPFAtributos_Nome ;
      private bool n383FuncoesAPFAtributos_Code ;
      private bool n389FuncoesAPFAtributos_Regra ;
      private bool n378FuncaoAPFAtributos_FuncaoDadosCod ;
      private bool returnInSub ;
      private String A166FuncaoAPF_Nome ;
      private String A383FuncoesAPFAtributos_Code ;
      private String A385FuncoesAPFAtributos_Descricao ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCheckbox chkFuncoesAPFAtributos_Regra ;
      private IDataStoreProvider pr_default ;
      private String[] H009O2_A166FuncaoAPF_Nome ;
      private int[] H009O3_A366FuncaoAPFAtributos_AtrTabelaCod ;
      private bool[] H009O3_n366FuncaoAPFAtributos_AtrTabelaCod ;
      private String[] H009O3_A365FuncaoAPFAtributos_AtributosNom ;
      private bool[] H009O3_n365FuncaoAPFAtributos_AtributosNom ;
      private String[] H009O4_A367FuncaoAPFAtributos_AtrTabelaNom ;
      private bool[] H009O4_n367FuncaoAPFAtributos_AtrTabelaNom ;
      private int[] H009O5_A165FuncaoAPF_Codigo ;
      private int[] H009O5_A364FuncaoAPFAtributos_AtributosCod ;
      private String[] H009O5_A385FuncoesAPFAtributos_Descricao ;
      private bool[] H009O5_n385FuncoesAPFAtributos_Descricao ;
      private String[] H009O5_A384FuncoesAPFAtributos_Nome ;
      private bool[] H009O5_n384FuncoesAPFAtributos_Nome ;
      private String[] H009O5_A383FuncoesAPFAtributos_Code ;
      private bool[] H009O5_n383FuncoesAPFAtributos_Code ;
      private bool[] H009O5_A389FuncoesAPFAtributos_Regra ;
      private bool[] H009O5_n389FuncoesAPFAtributos_Regra ;
      private String[] H009O5_A367FuncaoAPFAtributos_AtrTabelaNom ;
      private bool[] H009O5_n367FuncaoAPFAtributos_AtrTabelaNom ;
      private int[] H009O5_A366FuncaoAPFAtributos_AtrTabelaCod ;
      private bool[] H009O5_n366FuncaoAPFAtributos_AtrTabelaCod ;
      private int[] H009O5_A378FuncaoAPFAtributos_FuncaoDadosCod ;
      private bool[] H009O5_n378FuncaoAPFAtributos_FuncaoDadosCod ;
      private String[] H009O5_A365FuncaoAPFAtributos_AtributosNom ;
      private bool[] H009O5_n365FuncaoAPFAtributos_AtributosNom ;
      private String[] H009O5_A166FuncaoAPF_Nome ;
      private String[] H009O6_A166FuncaoAPF_Nome ;
      private int[] H009O7_A366FuncaoAPFAtributos_AtrTabelaCod ;
      private bool[] H009O7_n366FuncaoAPFAtributos_AtrTabelaCod ;
      private String[] H009O7_A365FuncaoAPFAtributos_AtributosNom ;
      private bool[] H009O7_n365FuncaoAPFAtributos_AtributosNom ;
      private String[] H009O8_A367FuncaoAPFAtributos_AtrTabelaNom ;
      private bool[] H009O8_n367FuncaoAPFAtributos_AtrTabelaNom ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV12HTTPRequest ;
      private IGxSession AV11Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
   }

   public class funcoesapfatributosgeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH009O2 ;
          prmH009O2 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH009O3 ;
          prmH009O3 = new Object[] {
          new Object[] {"@FuncaoAPFAtributos_AtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH009O4 ;
          prmH009O4 = new Object[] {
          new Object[] {"@FuncaoAPFAtributos_AtrTabelaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH009O5 ;
          prmH009O5 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPFAtributos_AtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH009O6 ;
          prmH009O6 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH009O7 ;
          prmH009O7 = new Object[] {
          new Object[] {"@FuncaoAPFAtributos_AtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH009O8 ;
          prmH009O8 = new Object[] {
          new Object[] {"@FuncaoAPFAtributos_AtrTabelaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H009O2", "SELECT [FuncaoAPF_Nome] FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009O2,1,0,true,true )
             ,new CursorDef("H009O3", "SELECT [Atributos_TabelaCod] AS FuncaoAPFAtributos_AtrTabelaCod, [Atributos_Nome] AS FuncaoAPFAtributos_AtributosNom FROM [Atributos] WITH (NOLOCK) WHERE [Atributos_Codigo] = @FuncaoAPFAtributos_AtributosCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009O3,1,0,true,true )
             ,new CursorDef("H009O4", "SELECT [Tabela_Nome] AS FuncaoAPFAtributos_AtrTabelaNom FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_Codigo] = @FuncaoAPFAtributos_AtrTabelaCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009O4,1,0,true,true )
             ,new CursorDef("H009O5", "SELECT T1.[FuncaoAPF_Codigo], T1.[FuncaoAPFAtributos_AtributosCod] AS FuncaoAPFAtributos_AtributosCod, T1.[FuncoesAPFAtributos_Descricao], T1.[FuncoesAPFAtributos_Nome], T1.[FuncoesAPFAtributos_Code], T1.[FuncoesAPFAtributos_Regra], T4.[Tabela_Nome] AS FuncaoAPFAtributos_AtrTabelaNom, T3.[Atributos_TabelaCod] AS FuncaoAPFAtributos_AtrTabelaCod, T1.[FuncaoAPFAtributos_FuncaoDadosCod], T3.[Atributos_Nome] AS FuncaoAPFAtributos_AtributosNom, T2.[FuncaoAPF_Nome] FROM ((([FuncoesAPFAtributos] T1 WITH (NOLOCK) INNER JOIN [FuncoesAPF] T2 WITH (NOLOCK) ON T2.[FuncaoAPF_Codigo] = T1.[FuncaoAPF_Codigo]) INNER JOIN [Atributos] T3 WITH (NOLOCK) ON T3.[Atributos_Codigo] = T1.[FuncaoAPFAtributos_AtributosCod]) LEFT JOIN [Tabela] T4 WITH (NOLOCK) ON T4.[Tabela_Codigo] = T3.[Atributos_TabelaCod]) WHERE T1.[FuncaoAPF_Codigo] = @FuncaoAPF_Codigo and T1.[FuncaoAPFAtributos_AtributosCod] = @FuncaoAPFAtributos_AtributosCod ORDER BY T1.[FuncaoAPF_Codigo], T1.[FuncaoAPFAtributos_AtributosCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009O5,1,0,true,true )
             ,new CursorDef("H009O6", "SELECT [FuncaoAPF_Nome] FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009O6,1,0,true,true )
             ,new CursorDef("H009O7", "SELECT [Atributos_TabelaCod] AS FuncaoAPFAtributos_AtrTabelaCod, [Atributos_Nome] AS FuncaoAPFAtributos_AtributosNom FROM [Atributos] WITH (NOLOCK) WHERE [Atributos_Codigo] = @FuncaoAPFAtributos_AtributosCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009O7,1,0,true,true )
             ,new CursorDef("H009O8", "SELECT [Tabela_Nome] AS FuncaoAPFAtributos_AtrTabelaNom FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_Codigo] = @FuncaoAPFAtributos_AtrTabelaCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009O8,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((bool[]) buf[8])[0] = rslt.getBool(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 50) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getString(10, 50) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getVarchar(11) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
