/*
               File: type_SdtSDT_WsFiltros
        Description: SDT_WsFiltros
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:37:7.2
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_WsFiltros" )]
   [XmlType(TypeName =  "SDT_WsFiltros" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtSDT_WsFiltros : GxUserType
   {
      public SdtSDT_WsFiltros( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_WsFiltros_Status = "";
         gxTv_SdtSDT_WsFiltros_Areatrabalhonome = "";
      }

      public SdtSDT_WsFiltros( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_WsFiltros deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_WsFiltros)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_WsFiltros obj ;
         obj = this;
         obj.gxTpr_Status = deserialized.gxTpr_Status;
         obj.gxTpr_Areatrabalhocodigo = deserialized.gxTpr_Areatrabalhocodigo;
         obj.gxTpr_Areatrabalhonome = deserialized.gxTpr_Areatrabalhonome;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Status") )
               {
                  gxTv_SdtSDT_WsFiltros_Status = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalhoCodigo") )
               {
                  gxTv_SdtSDT_WsFiltros_Areatrabalhocodigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalhoNome") )
               {
                  gxTv_SdtSDT_WsFiltros_Areatrabalhonome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_WsFiltros";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Status", StringUtil.RTrim( gxTv_SdtSDT_WsFiltros_Status));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("AreaTrabalhoCodigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WsFiltros_Areatrabalhocodigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("AreaTrabalhoNome", StringUtil.RTrim( gxTv_SdtSDT_WsFiltros_Areatrabalhonome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Status", gxTv_SdtSDT_WsFiltros_Status, false);
         AddObjectProperty("AreaTrabalhoCodigo", gxTv_SdtSDT_WsFiltros_Areatrabalhocodigo, false);
         AddObjectProperty("AreaTrabalhoNome", gxTv_SdtSDT_WsFiltros_Areatrabalhonome, false);
         return  ;
      }

      [  SoapElement( ElementName = "Status" )]
      [  XmlElement( ElementName = "Status"   )]
      public String gxTpr_Status
      {
         get {
            return gxTv_SdtSDT_WsFiltros_Status ;
         }

         set {
            gxTv_SdtSDT_WsFiltros_Status = (String)(value);
         }

      }

      [  SoapElement( ElementName = "AreaTrabalhoCodigo" )]
      [  XmlElement( ElementName = "AreaTrabalhoCodigo"   )]
      public int gxTpr_Areatrabalhocodigo
      {
         get {
            return gxTv_SdtSDT_WsFiltros_Areatrabalhocodigo ;
         }

         set {
            gxTv_SdtSDT_WsFiltros_Areatrabalhocodigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "AreaTrabalhoNome" )]
      [  XmlElement( ElementName = "AreaTrabalhoNome"   )]
      public String gxTpr_Areatrabalhonome
      {
         get {
            return gxTv_SdtSDT_WsFiltros_Areatrabalhonome ;
         }

         set {
            gxTv_SdtSDT_WsFiltros_Areatrabalhonome = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtSDT_WsFiltros_Status = "";
         gxTv_SdtSDT_WsFiltros_Areatrabalhonome = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected int gxTv_SdtSDT_WsFiltros_Areatrabalhocodigo ;
      protected String gxTv_SdtSDT_WsFiltros_Status ;
      protected String sTagName ;
      protected String gxTv_SdtSDT_WsFiltros_Areatrabalhonome ;
   }

   [DataContract(Name = @"SDT_WsFiltros", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtSDT_WsFiltros_RESTInterface : GxGenericCollectionItem<SdtSDT_WsFiltros>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_WsFiltros_RESTInterface( ) : base()
      {
      }

      public SdtSDT_WsFiltros_RESTInterface( SdtSDT_WsFiltros psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Status" , Order = 0 )]
      public String gxTpr_Status
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Status) ;
         }

         set {
            sdt.gxTpr_Status = (String)(value);
         }

      }

      [DataMember( Name = "AreaTrabalhoCodigo" , Order = 1 )]
      public Nullable<int> gxTpr_Areatrabalhocodigo
      {
         get {
            return sdt.gxTpr_Areatrabalhocodigo ;
         }

         set {
            sdt.gxTpr_Areatrabalhocodigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "AreaTrabalhoNome" , Order = 2 )]
      public String gxTpr_Areatrabalhonome
      {
         get {
            return sdt.gxTpr_Areatrabalhonome ;
         }

         set {
            sdt.gxTpr_Areatrabalhonome = (String)(value);
         }

      }

      public SdtSDT_WsFiltros sdt
      {
         get {
            return (SdtSDT_WsFiltros)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_WsFiltros() ;
         }
      }

   }

}
