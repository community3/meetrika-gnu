/**@preserve  GeneXus C# 10_3_14-114418 on 3/12/2020 0:19:40.44
*/
gx.evt.autoSkip = false;
gx.define('naoconformidade', false, function () {
   this.ServerClass =  "naoconformidade" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("trn");
   this.hasEnterEvent = true;
   this.skipOnEnter = false;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.SetStandaloneVars=function()
   {
      this.AV8NaoConformidade_Codigo=gx.fn.getIntegerValue("vNAOCONFORMIDADE_CODIGO",'.') ;
      this.AV11Insert_NaoConformidade_AreaTrabalhoCod=gx.fn.getIntegerValue("vINSERT_NAOCONFORMIDADE_AREATRABALHOCOD",'.') ;
      this.Gx_BScreen=gx.fn.getIntegerValue("vGXBSCREEN",'.') ;
      this.A428NaoConformidade_AreaTrabalhoCod=gx.fn.getIntegerValue("NAOCONFORMIDADE_AREATRABALHOCOD",'.') ;
      this.AV13Insert_NaoConformidade_TipoCod=gx.fn.getIntegerValue("vINSERT_NAOCONFORMIDADE_TIPOCOD",'.') ;
      this.A2025NaoConformidade_TipoCod=gx.fn.getIntegerValue("NAOCONFORMIDADE_TIPOCOD",'.') ;
      this.AV15Pgmname=gx.fn.getControlValue("vPGMNAME") ;
      this.Gx_mode=gx.fn.getControlValue("vMODE") ;
      this.AV9TrnContext=gx.fn.getControlValue("vTRNCONTEXT") ;
   };
   this.Valid_Naoconformidade_codigo=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("NAOCONFORMIDADE_CODIGO");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Valid_Naoconformidade_nome=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("NAOCONFORMIDADE_NOME");
         this.AnyError  = 0;
         if ( ((''==this.A427NaoConformidade_Nome)) )
         {
            try {
               gxballoon.setError("Não Conformidade é obrigatório.");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Valid_Naoconformidade_tipo=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("NAOCONFORMIDADE_TIPO");
         this.AnyError  = 0;
         if ( ! ( ( this.A429NaoConformidade_Tipo == 1 ) || ( this.A429NaoConformidade_Tipo == 2 ) || ( this.A429NaoConformidade_Tipo == 3 ) || ( this.A429NaoConformidade_Tipo == 4 ) || ( this.A429NaoConformidade_Tipo == 5 ) || ( this.A429NaoConformidade_Tipo == 6 ) ) )
         {
            try {
               gxballoon.setError("Campo Tipo fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }
         if ( ((0==this.A429NaoConformidade_Tipo)) )
         {
            try {
               gxballoon.setError("Tipo é obrigatório.");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.e121q2_client=function()
   {
      this.executeServerEvent("AFTER TRN", true, null, false, false);
   };
   this.e131q65_client=function()
   {
      this.executeServerEvent("ENTER", true, null, false, false);
   };
   this.e141q65_client=function()
   {
      this.executeServerEvent("CANCEL", true, null, false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,5,13,16,18,21,23,26,28,31,33,36,44];
   this.GXLastCtrlId =44;
   this.DVPANEL_TABLEATTRIBUTESContainer = gx.uc.getNew(this, 11, 0, "BootstrapPanel", "DVPANEL_TABLEATTRIBUTESContainer", "Dvpanel_tableattributes");
   var DVPANEL_TABLEATTRIBUTESContainer = this.DVPANEL_TABLEATTRIBUTESContainer;
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Width", "Width", "100%", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Height", "Height", "100", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("AutoWidth", "Autowidth", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("AutoHeight", "Autoheight", true, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Cls", "Cls", "GXUI-DVelop-Panel", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("ShowHeader", "Showheader", true, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Title", "Title", "Não Conformidade", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Collapsible", "Collapsible", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Collapsed", "Collapsed", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("ShowCollapseIcon", "Showcollapseicon", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("IconPosition", "Iconposition", "left", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("AutoScroll", "Autoscroll", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Visible", "Visible", true, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Enabled", "Enabled", true, "boolean");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Class", "Class", "", "char");
   DVPANEL_TABLEATTRIBUTESContainer.setC2ShowFunction(function(UC) { UC.show(); });
   this.setUserControl(DVPANEL_TABLEATTRIBUTESContainer);
   GXValidFnc[2]={fld:"TABLEMAIN",grid:0};
   GXValidFnc[5]={fld:"TABLECONTENT",grid:0};
   GXValidFnc[13]={fld:"TABLEATTRIBUTES",grid:0};
   GXValidFnc[16]={fld:"TEXTBLOCKNAOCONFORMIDADE_NOME", format:0,grid:0};
   GXValidFnc[18]={lvl:0,type:"char",len:50,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:this.Valid_Naoconformidade_nome,isvalid:null,rgrid:[],fld:"NAOCONFORMIDADE_NOME",gxz:"Z427NaoConformidade_Nome",gxold:"O427NaoConformidade_Nome",gxvar:"A427NaoConformidade_Nome",ucs:[],op:[18],ip:[18],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A427NaoConformidade_Nome=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z427NaoConformidade_Nome=Value},v2c:function(){gx.fn.setControlValue("NAOCONFORMIDADE_NOME",gx.O.A427NaoConformidade_Nome,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A427NaoConformidade_Nome=this.val()},val:function(){return gx.fn.getControlValue("NAOCONFORMIDADE_NOME")},nac:gx.falseFn};
   this.declareDomainHdlr( 18 , function() {
   });
   GXValidFnc[21]={fld:"TEXTBLOCKNAOCONFORMIDADE_TIPO", format:0,grid:0};
   GXValidFnc[23]={lvl:0,type:"int",len:2,dec:0,sign:false,pic:"Z9",ro:0,grid:0,gxgrid:null,fnc:this.Valid_Naoconformidade_tipo,isvalid:null,rgrid:[],fld:"NAOCONFORMIDADE_TIPO",gxz:"Z429NaoConformidade_Tipo",gxold:"O429NaoConformidade_Tipo",gxvar:"A429NaoConformidade_Tipo",ucs:[],op:[23],ip:[23],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.A429NaoConformidade_Tipo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z429NaoConformidade_Tipo=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("NAOCONFORMIDADE_TIPO",gx.O.A429NaoConformidade_Tipo);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A429NaoConformidade_Tipo=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("NAOCONFORMIDADE_TIPO",'.')},nac:gx.falseFn};
   this.declareDomainHdlr( 23 , function() {
   });
   GXValidFnc[26]={fld:"TEXTBLOCKNAOCONFORMIDADE_EHIMPEDITIVA", format:0,grid:0};
   GXValidFnc[28]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"NAOCONFORMIDADE_EHIMPEDITIVA",gxz:"Z1121NaoConformidade_EhImpeditiva",gxold:"O1121NaoConformidade_EhImpeditiva",gxvar:"A1121NaoConformidade_EhImpeditiva",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.A1121NaoConformidade_EhImpeditiva=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1121NaoConformidade_EhImpeditiva=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setComboBoxValue("NAOCONFORMIDADE_EHIMPEDITIVA",gx.O.A1121NaoConformidade_EhImpeditiva);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1121NaoConformidade_EhImpeditiva=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("NAOCONFORMIDADE_EHIMPEDITIVA")},nac:gx.falseFn};
   this.declareDomainHdlr( 28 , function() {
   });
   GXValidFnc[31]={fld:"TEXTBLOCKNAOCONFORMIDADE_GLOSAVEL", format:0,grid:0};
   GXValidFnc[33]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"NAOCONFORMIDADE_GLOSAVEL",gxz:"Z2029NaoConformidade_Glosavel",gxold:"O2029NaoConformidade_Glosavel",gxvar:"A2029NaoConformidade_Glosavel",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"checkbox",v2v:function(Value){if(Value!==undefined)gx.O.A2029NaoConformidade_Glosavel=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z2029NaoConformidade_Glosavel=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setCheckBoxValue("NAOCONFORMIDADE_GLOSAVEL",gx.O.A2029NaoConformidade_Glosavel,true);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A2029NaoConformidade_Glosavel=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("NAOCONFORMIDADE_GLOSAVEL")},nac:gx.falseFn,values:['true','false']};
   this.declareDomainHdlr( 33 , function() {
   });
   GXValidFnc[36]={fld:"TABLEACTIONS",grid:0};
   GXValidFnc[44]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,grid:0,gxgrid:null,fnc:this.Valid_Naoconformidade_codigo,isvalid:null,rgrid:[],fld:"NAOCONFORMIDADE_CODIGO",gxz:"Z426NaoConformidade_Codigo",gxold:"O426NaoConformidade_Codigo",gxvar:"A426NaoConformidade_Codigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A426NaoConformidade_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z426NaoConformidade_Codigo=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("NAOCONFORMIDADE_CODIGO",gx.O.A426NaoConformidade_Codigo,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A426NaoConformidade_Codigo=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("NAOCONFORMIDADE_CODIGO",'.')},nac:gx.falseFn};
   this.declareDomainHdlr( 44 , function() {
   });
   this.A427NaoConformidade_Nome = "" ;
   this.Z427NaoConformidade_Nome = "" ;
   this.O427NaoConformidade_Nome = "" ;
   this.A429NaoConformidade_Tipo = 0 ;
   this.Z429NaoConformidade_Tipo = 0 ;
   this.O429NaoConformidade_Tipo = 0 ;
   this.A1121NaoConformidade_EhImpeditiva = false ;
   this.Z1121NaoConformidade_EhImpeditiva = false ;
   this.O1121NaoConformidade_EhImpeditiva = false ;
   this.A2029NaoConformidade_Glosavel = false ;
   this.Z2029NaoConformidade_Glosavel = false ;
   this.O2029NaoConformidade_Glosavel = false ;
   this.A426NaoConformidade_Codigo = 0 ;
   this.Z426NaoConformidade_Codigo = 0 ;
   this.O426NaoConformidade_Codigo = 0 ;
   this.AV7WWPContext = {} ;
   this.AV9TrnContext = {} ;
   this.AV16GXV1 = 0 ;
   this.AV11Insert_NaoConformidade_AreaTrabalhoCod = 0 ;
   this.AV13Insert_NaoConformidade_TipoCod = 0 ;
   this.AV12TrnContextAtt = {} ;
   this.AV8NaoConformidade_Codigo = 0 ;
   this.AV10WebSession = {} ;
   this.A426NaoConformidade_Codigo = 0 ;
   this.A428NaoConformidade_AreaTrabalhoCod = 0 ;
   this.A2025NaoConformidade_TipoCod = 0 ;
   this.AV15Pgmname = "" ;
   this.Gx_BScreen = 0 ;
   this.A427NaoConformidade_Nome = "" ;
   this.A429NaoConformidade_Tipo = 0 ;
   this.A1121NaoConformidade_EhImpeditiva = false ;
   this.A2029NaoConformidade_Glosavel = false ;
   this.Gx_mode = "" ;
   this.Events = {"e121q2_client": ["AFTER TRN", true] ,"e131q65_client": ["ENTER", true] ,"e141q65_client": ["CANCEL", true]};
   this.EvtParms["ENTER"] = [[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV8NaoConformidade_Codigo',fld:'vNAOCONFORMIDADE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],[]];
   this.EvtParms["REFRESH"] = [[],[]];
   this.EvtParms["AFTER TRN"] = [[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],[]];
   this.EnterCtrl = ["BTN_TRN_ENTER"];
   this.setVCMap("AV8NaoConformidade_Codigo", "vNAOCONFORMIDADE_CODIGO", 0, "int");
   this.setVCMap("AV11Insert_NaoConformidade_AreaTrabalhoCod", "vINSERT_NAOCONFORMIDADE_AREATRABALHOCOD", 0, "int");
   this.setVCMap("Gx_BScreen", "vGXBSCREEN", 0, "int");
   this.setVCMap("A428NaoConformidade_AreaTrabalhoCod", "NAOCONFORMIDADE_AREATRABALHOCOD", 0, "int");
   this.setVCMap("AV13Insert_NaoConformidade_TipoCod", "vINSERT_NAOCONFORMIDADE_TIPOCOD", 0, "int");
   this.setVCMap("A2025NaoConformidade_TipoCod", "NAOCONFORMIDADE_TIPOCOD", 0, "int");
   this.setVCMap("AV15Pgmname", "vPGMNAME", 0, "char");
   this.setVCMap("Gx_mode", "vMODE", 0, "char");
   this.setVCMap("AV9TrnContext", "vTRNCONTEXT", 0, "WWPBaseObjects\WWPTransactionContext");
   this.InitStandaloneVars( );
});
gx.createParentObj(naoconformidade);
