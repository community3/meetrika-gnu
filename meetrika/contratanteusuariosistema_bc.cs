/*
               File: ContratanteUsuarioSistema_BC
        Description: Sistemas do Usuario
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:29:41.65
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratanteusuariosistema_bc : GXHttpHandler, IGxSilentTrn
   {
      public contratanteusuariosistema_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contratanteusuariosistema_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow4A190( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey4A190( ) ;
         standaloneModal( ) ;
         AddRow4A190( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z63ContratanteUsuario_ContratanteCod = A63ContratanteUsuario_ContratanteCod;
               Z60ContratanteUsuario_UsuarioCod = A60ContratanteUsuario_UsuarioCod;
               Z127Sistema_Codigo = A127Sistema_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_4A0( )
      {
         BeforeValidate4A190( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls4A190( ) ;
            }
            else
            {
               CheckExtendedTable4A190( ) ;
               if ( AnyError == 0 )
               {
                  ZM4A190( 2) ;
                  ZM4A190( 3) ;
               }
               CloseExtendedTableCursors4A190( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void E114A2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV7WWPContext) ;
      }

      protected void ZM4A190( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
         }
         if ( GX_JID == -1 )
         {
            Z63ContratanteUsuario_ContratanteCod = A63ContratanteUsuario_ContratanteCod;
            Z60ContratanteUsuario_UsuarioCod = A60ContratanteUsuario_UsuarioCod;
            Z127Sistema_Codigo = A127Sistema_Codigo;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
      }

      protected void Load4A190( )
      {
         /* Using cursor BC004A6 */
         pr_default.execute(4, new Object[] {A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod, A127Sistema_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound190 = 1;
            ZM4A190( -1) ;
         }
         pr_default.close(4);
         OnLoadActions4A190( ) ;
      }

      protected void OnLoadActions4A190( )
      {
      }

      protected void CheckExtendedTable4A190( )
      {
         standaloneModal( ) ;
         /* Using cursor BC004A4 */
         pr_default.execute(2, new Object[] {A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contratante Usuario'.", "ForeignKeyNotFound", 1, "CONTRATANTEUSUARIO_CONTRATANTECOD");
            AnyError = 1;
         }
         pr_default.close(2);
         /* Using cursor BC004A5 */
         pr_default.execute(3, new Object[] {A127Sistema_Codigo});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Sistema'.", "ForeignKeyNotFound", 1, "SISTEMA_CODIGO");
            AnyError = 1;
         }
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors4A190( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey4A190( )
      {
         /* Using cursor BC004A7 */
         pr_default.execute(5, new Object[] {A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod, A127Sistema_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound190 = 1;
         }
         else
         {
            RcdFound190 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC004A3 */
         pr_default.execute(1, new Object[] {A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod, A127Sistema_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM4A190( 1) ;
            RcdFound190 = 1;
            A63ContratanteUsuario_ContratanteCod = BC004A3_A63ContratanteUsuario_ContratanteCod[0];
            A60ContratanteUsuario_UsuarioCod = BC004A3_A60ContratanteUsuario_UsuarioCod[0];
            A127Sistema_Codigo = BC004A3_A127Sistema_Codigo[0];
            Z63ContratanteUsuario_ContratanteCod = A63ContratanteUsuario_ContratanteCod;
            Z60ContratanteUsuario_UsuarioCod = A60ContratanteUsuario_UsuarioCod;
            Z127Sistema_Codigo = A127Sistema_Codigo;
            sMode190 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load4A190( ) ;
            if ( AnyError == 1 )
            {
               RcdFound190 = 0;
               InitializeNonKey4A190( ) ;
            }
            Gx_mode = sMode190;
         }
         else
         {
            RcdFound190 = 0;
            InitializeNonKey4A190( ) ;
            sMode190 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode190;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey4A190( ) ;
         if ( RcdFound190 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_4A0( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency4A190( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC004A2 */
            pr_default.execute(0, new Object[] {A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod, A127Sistema_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratanteUsuarioSistema"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContratanteUsuarioSistema"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert4A190( )
      {
         BeforeValidate4A190( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4A190( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM4A190( 0) ;
            CheckOptimisticConcurrency4A190( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4A190( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert4A190( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC004A8 */
                     pr_default.execute(6, new Object[] {A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod, A127Sistema_Codigo});
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratanteUsuarioSistema") ;
                     if ( (pr_default.getStatus(6) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load4A190( ) ;
            }
            EndLevel4A190( ) ;
         }
         CloseExtendedTableCursors4A190( ) ;
      }

      protected void Update4A190( )
      {
         BeforeValidate4A190( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4A190( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4A190( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4A190( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate4A190( ) ;
                  if ( AnyError == 0 )
                  {
                     /* No attributes to update on table [ContratanteUsuarioSistema] */
                     DeferredUpdate4A190( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel4A190( ) ;
         }
         CloseExtendedTableCursors4A190( ) ;
      }

      protected void DeferredUpdate4A190( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate4A190( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4A190( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls4A190( ) ;
            AfterConfirm4A190( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete4A190( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC004A9 */
                  pr_default.execute(7, new Object[] {A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod, A127Sistema_Codigo});
                  pr_default.close(7);
                  dsDefault.SmartCacheProvider.SetUpdated("ContratanteUsuarioSistema") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode190 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel4A190( ) ;
         Gx_mode = sMode190;
      }

      protected void OnDeleteControls4A190( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel4A190( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete4A190( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart4A190( )
      {
         /* Scan By routine */
         /* Using cursor BC004A10 */
         pr_default.execute(8, new Object[] {A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod, A127Sistema_Codigo});
         RcdFound190 = 0;
         if ( (pr_default.getStatus(8) != 101) )
         {
            RcdFound190 = 1;
            A63ContratanteUsuario_ContratanteCod = BC004A10_A63ContratanteUsuario_ContratanteCod[0];
            A60ContratanteUsuario_UsuarioCod = BC004A10_A60ContratanteUsuario_UsuarioCod[0];
            A127Sistema_Codigo = BC004A10_A127Sistema_Codigo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext4A190( )
      {
         /* Scan next routine */
         pr_default.readNext(8);
         RcdFound190 = 0;
         ScanKeyLoad4A190( ) ;
      }

      protected void ScanKeyLoad4A190( )
      {
         sMode190 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(8) != 101) )
         {
            RcdFound190 = 1;
            A63ContratanteUsuario_ContratanteCod = BC004A10_A63ContratanteUsuario_ContratanteCod[0];
            A60ContratanteUsuario_UsuarioCod = BC004A10_A60ContratanteUsuario_UsuarioCod[0];
            A127Sistema_Codigo = BC004A10_A127Sistema_Codigo[0];
         }
         Gx_mode = sMode190;
      }

      protected void ScanKeyEnd4A190( )
      {
         pr_default.close(8);
      }

      protected void AfterConfirm4A190( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert4A190( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate4A190( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete4A190( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete4A190( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate4A190( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes4A190( )
      {
      }

      protected void AddRow4A190( )
      {
         VarsToRow190( bcContratanteUsuarioSistema) ;
      }

      protected void ReadRow4A190( )
      {
         RowToVars190( bcContratanteUsuarioSistema, 1) ;
      }

      protected void InitializeNonKey4A190( )
      {
      }

      protected void InitAll4A190( )
      {
         A63ContratanteUsuario_ContratanteCod = 0;
         A60ContratanteUsuario_UsuarioCod = 0;
         A127Sistema_Codigo = 0;
         InitializeNonKey4A190( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow190( SdtContratanteUsuarioSistema obj190 )
      {
         obj190.gxTpr_Mode = Gx_mode;
         obj190.gxTpr_Contratanteusuario_contratantecod = A63ContratanteUsuario_ContratanteCod;
         obj190.gxTpr_Contratanteusuario_usuariocod = A60ContratanteUsuario_UsuarioCod;
         obj190.gxTpr_Sistema_codigo = A127Sistema_Codigo;
         obj190.gxTpr_Contratanteusuario_contratantecod_Z = Z63ContratanteUsuario_ContratanteCod;
         obj190.gxTpr_Contratanteusuario_usuariocod_Z = Z60ContratanteUsuario_UsuarioCod;
         obj190.gxTpr_Sistema_codigo_Z = Z127Sistema_Codigo;
         obj190.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow190( SdtContratanteUsuarioSistema obj190 )
      {
         obj190.gxTpr_Contratanteusuario_contratantecod = A63ContratanteUsuario_ContratanteCod;
         obj190.gxTpr_Contratanteusuario_usuariocod = A60ContratanteUsuario_UsuarioCod;
         obj190.gxTpr_Sistema_codigo = A127Sistema_Codigo;
         return  ;
      }

      public void RowToVars190( SdtContratanteUsuarioSistema obj190 ,
                                int forceLoad )
      {
         Gx_mode = obj190.gxTpr_Mode;
         A63ContratanteUsuario_ContratanteCod = obj190.gxTpr_Contratanteusuario_contratantecod;
         A60ContratanteUsuario_UsuarioCod = obj190.gxTpr_Contratanteusuario_usuariocod;
         A127Sistema_Codigo = obj190.gxTpr_Sistema_codigo;
         Z63ContratanteUsuario_ContratanteCod = obj190.gxTpr_Contratanteusuario_contratantecod_Z;
         Z60ContratanteUsuario_UsuarioCod = obj190.gxTpr_Contratanteusuario_usuariocod_Z;
         Z127Sistema_Codigo = obj190.gxTpr_Sistema_codigo_Z;
         Gx_mode = obj190.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A63ContratanteUsuario_ContratanteCod = (int)getParm(obj,0);
         A60ContratanteUsuario_UsuarioCod = (int)getParm(obj,1);
         A127Sistema_Codigo = (int)getParm(obj,2);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey4A190( ) ;
         ScanKeyStart4A190( ) ;
         if ( RcdFound190 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC004A11 */
            pr_default.execute(9, new Object[] {A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod});
            if ( (pr_default.getStatus(9) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Contratante Usuario'.", "ForeignKeyNotFound", 1, "CONTRATANTEUSUARIO_CONTRATANTECOD");
               AnyError = 1;
            }
            pr_default.close(9);
            /* Using cursor BC004A12 */
            pr_default.execute(10, new Object[] {A127Sistema_Codigo});
            if ( (pr_default.getStatus(10) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Sistema'.", "ForeignKeyNotFound", 1, "SISTEMA_CODIGO");
               AnyError = 1;
            }
            pr_default.close(10);
         }
         else
         {
            Gx_mode = "UPD";
            Z63ContratanteUsuario_ContratanteCod = A63ContratanteUsuario_ContratanteCod;
            Z60ContratanteUsuario_UsuarioCod = A60ContratanteUsuario_UsuarioCod;
            Z127Sistema_Codigo = A127Sistema_Codigo;
         }
         ZM4A190( -1) ;
         OnLoadActions4A190( ) ;
         AddRow4A190( ) ;
         ScanKeyEnd4A190( ) ;
         if ( RcdFound190 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars190( bcContratanteUsuarioSistema, 0) ;
         ScanKeyStart4A190( ) ;
         if ( RcdFound190 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC004A11 */
            pr_default.execute(9, new Object[] {A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod});
            if ( (pr_default.getStatus(9) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Contratante Usuario'.", "ForeignKeyNotFound", 1, "CONTRATANTEUSUARIO_CONTRATANTECOD");
               AnyError = 1;
            }
            pr_default.close(9);
            /* Using cursor BC004A12 */
            pr_default.execute(10, new Object[] {A127Sistema_Codigo});
            if ( (pr_default.getStatus(10) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Sistema'.", "ForeignKeyNotFound", 1, "SISTEMA_CODIGO");
               AnyError = 1;
            }
            pr_default.close(10);
         }
         else
         {
            Gx_mode = "UPD";
            Z63ContratanteUsuario_ContratanteCod = A63ContratanteUsuario_ContratanteCod;
            Z60ContratanteUsuario_UsuarioCod = A60ContratanteUsuario_UsuarioCod;
            Z127Sistema_Codigo = A127Sistema_Codigo;
         }
         ZM4A190( -1) ;
         OnLoadActions4A190( ) ;
         AddRow4A190( ) ;
         ScanKeyEnd4A190( ) ;
         if ( RcdFound190 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars190( bcContratanteUsuarioSistema, 0) ;
         nKeyPressed = 1;
         GetKey4A190( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert4A190( ) ;
         }
         else
         {
            if ( RcdFound190 == 1 )
            {
               if ( ( A63ContratanteUsuario_ContratanteCod != Z63ContratanteUsuario_ContratanteCod ) || ( A60ContratanteUsuario_UsuarioCod != Z60ContratanteUsuario_UsuarioCod ) || ( A127Sistema_Codigo != Z127Sistema_Codigo ) )
               {
                  A63ContratanteUsuario_ContratanteCod = Z63ContratanteUsuario_ContratanteCod;
                  A60ContratanteUsuario_UsuarioCod = Z60ContratanteUsuario_UsuarioCod;
                  A127Sistema_Codigo = Z127Sistema_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update4A190( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( ( A63ContratanteUsuario_ContratanteCod != Z63ContratanteUsuario_ContratanteCod ) || ( A60ContratanteUsuario_UsuarioCod != Z60ContratanteUsuario_UsuarioCod ) || ( A127Sistema_Codigo != Z127Sistema_Codigo ) )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert4A190( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert4A190( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow190( bcContratanteUsuarioSistema) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars190( bcContratanteUsuarioSistema, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey4A190( ) ;
         if ( RcdFound190 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( ( A63ContratanteUsuario_ContratanteCod != Z63ContratanteUsuario_ContratanteCod ) || ( A60ContratanteUsuario_UsuarioCod != Z60ContratanteUsuario_UsuarioCod ) || ( A127Sistema_Codigo != Z127Sistema_Codigo ) )
            {
               A63ContratanteUsuario_ContratanteCod = Z63ContratanteUsuario_ContratanteCod;
               A60ContratanteUsuario_UsuarioCod = Z60ContratanteUsuario_UsuarioCod;
               A127Sistema_Codigo = Z127Sistema_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( ( A63ContratanteUsuario_ContratanteCod != Z63ContratanteUsuario_ContratanteCod ) || ( A60ContratanteUsuario_UsuarioCod != Z60ContratanteUsuario_UsuarioCod ) || ( A127Sistema_Codigo != Z127Sistema_Codigo ) )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(9);
         pr_default.close(10);
         context.RollbackDataStores( "ContratanteUsuarioSistema_BC");
         VarsToRow190( bcContratanteUsuarioSistema) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcContratanteUsuarioSistema.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcContratanteUsuarioSistema.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcContratanteUsuarioSistema )
         {
            bcContratanteUsuarioSistema = (SdtContratanteUsuarioSistema)(sdt);
            if ( StringUtil.StrCmp(bcContratanteUsuarioSistema.gxTpr_Mode, "") == 0 )
            {
               bcContratanteUsuarioSistema.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow190( bcContratanteUsuarioSistema) ;
            }
            else
            {
               RowToVars190( bcContratanteUsuarioSistema, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcContratanteUsuarioSistema.gxTpr_Mode, "") == 0 )
            {
               bcContratanteUsuarioSistema.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars190( bcContratanteUsuarioSistema, 1) ;
         return  ;
      }

      public SdtContratanteUsuarioSistema ContratanteUsuarioSistema_BC
      {
         get {
            return bcContratanteUsuarioSistema ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(9);
         pr_default.close(10);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         AV7WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         BC004A6_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         BC004A6_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         BC004A6_A127Sistema_Codigo = new int[1] ;
         BC004A4_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         BC004A5_A127Sistema_Codigo = new int[1] ;
         BC004A7_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         BC004A7_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         BC004A7_A127Sistema_Codigo = new int[1] ;
         BC004A3_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         BC004A3_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         BC004A3_A127Sistema_Codigo = new int[1] ;
         sMode190 = "";
         BC004A2_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         BC004A2_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         BC004A2_A127Sistema_Codigo = new int[1] ;
         BC004A10_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         BC004A10_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         BC004A10_A127Sistema_Codigo = new int[1] ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         BC004A11_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         BC004A12_A127Sistema_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratanteusuariosistema_bc__default(),
            new Object[][] {
                new Object[] {
               BC004A2_A63ContratanteUsuario_ContratanteCod, BC004A2_A60ContratanteUsuario_UsuarioCod, BC004A2_A127Sistema_Codigo
               }
               , new Object[] {
               BC004A3_A63ContratanteUsuario_ContratanteCod, BC004A3_A60ContratanteUsuario_UsuarioCod, BC004A3_A127Sistema_Codigo
               }
               , new Object[] {
               BC004A4_A63ContratanteUsuario_ContratanteCod
               }
               , new Object[] {
               BC004A5_A127Sistema_Codigo
               }
               , new Object[] {
               BC004A6_A63ContratanteUsuario_ContratanteCod, BC004A6_A60ContratanteUsuario_UsuarioCod, BC004A6_A127Sistema_Codigo
               }
               , new Object[] {
               BC004A7_A63ContratanteUsuario_ContratanteCod, BC004A7_A60ContratanteUsuario_UsuarioCod, BC004A7_A127Sistema_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC004A10_A63ContratanteUsuario_ContratanteCod, BC004A10_A60ContratanteUsuario_UsuarioCod, BC004A10_A127Sistema_Codigo
               }
               , new Object[] {
               BC004A11_A63ContratanteUsuario_ContratanteCod
               }
               , new Object[] {
               BC004A12_A127Sistema_Codigo
               }
            }
         );
         INITTRN();
         /* Execute Start event if defined. */
         /* Execute user event: E114A2 */
         E114A2 ();
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short RcdFound190 ;
      private int trnEnded ;
      private int Z63ContratanteUsuario_ContratanteCod ;
      private int A63ContratanteUsuario_ContratanteCod ;
      private int Z60ContratanteUsuario_UsuarioCod ;
      private int A60ContratanteUsuario_UsuarioCod ;
      private int Z127Sistema_Codigo ;
      private int A127Sistema_Codigo ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String sMode190 ;
      private SdtContratanteUsuarioSistema bcContratanteUsuarioSistema ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] BC004A6_A63ContratanteUsuario_ContratanteCod ;
      private int[] BC004A6_A60ContratanteUsuario_UsuarioCod ;
      private int[] BC004A6_A127Sistema_Codigo ;
      private int[] BC004A4_A63ContratanteUsuario_ContratanteCod ;
      private int[] BC004A5_A127Sistema_Codigo ;
      private int[] BC004A7_A63ContratanteUsuario_ContratanteCod ;
      private int[] BC004A7_A60ContratanteUsuario_UsuarioCod ;
      private int[] BC004A7_A127Sistema_Codigo ;
      private int[] BC004A3_A63ContratanteUsuario_ContratanteCod ;
      private int[] BC004A3_A60ContratanteUsuario_UsuarioCod ;
      private int[] BC004A3_A127Sistema_Codigo ;
      private int[] BC004A2_A63ContratanteUsuario_ContratanteCod ;
      private int[] BC004A2_A60ContratanteUsuario_UsuarioCod ;
      private int[] BC004A2_A127Sistema_Codigo ;
      private int[] BC004A10_A63ContratanteUsuario_ContratanteCod ;
      private int[] BC004A10_A60ContratanteUsuario_UsuarioCod ;
      private int[] BC004A10_A127Sistema_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private int[] BC004A11_A63ContratanteUsuario_ContratanteCod ;
      private int[] BC004A12_A127Sistema_Codigo ;
      private wwpbaseobjects.SdtWWPContext AV7WWPContext ;
   }

   public class contratanteusuariosistema_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC004A6 ;
          prmBC004A6 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004A4 ;
          prmBC004A4 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004A5 ;
          prmBC004A5 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004A7 ;
          prmBC004A7 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004A3 ;
          prmBC004A3 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004A2 ;
          prmBC004A2 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004A8 ;
          prmBC004A8 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004A9 ;
          prmBC004A9 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004A10 ;
          prmBC004A10 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004A11 ;
          prmBC004A11 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004A12 ;
          prmBC004A12 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC004A2", "SELECT [ContratanteUsuario_ContratanteCod], [ContratanteUsuario_UsuarioCod], [Sistema_Codigo] FROM [ContratanteUsuarioSistema] WITH (UPDLOCK) WHERE [ContratanteUsuario_ContratanteCod] = @ContratanteUsuario_ContratanteCod AND [ContratanteUsuario_UsuarioCod] = @ContratanteUsuario_UsuarioCod AND [Sistema_Codigo] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004A2,1,0,true,false )
             ,new CursorDef("BC004A3", "SELECT [ContratanteUsuario_ContratanteCod], [ContratanteUsuario_UsuarioCod], [Sistema_Codigo] FROM [ContratanteUsuarioSistema] WITH (NOLOCK) WHERE [ContratanteUsuario_ContratanteCod] = @ContratanteUsuario_ContratanteCod AND [ContratanteUsuario_UsuarioCod] = @ContratanteUsuario_UsuarioCod AND [Sistema_Codigo] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004A3,1,0,true,false )
             ,new CursorDef("BC004A4", "SELECT [ContratanteUsuario_ContratanteCod] FROM [ContratanteUsuario] WITH (NOLOCK) WHERE [ContratanteUsuario_ContratanteCod] = @ContratanteUsuario_ContratanteCod AND [ContratanteUsuario_UsuarioCod] = @ContratanteUsuario_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004A4,1,0,true,false )
             ,new CursorDef("BC004A5", "SELECT [Sistema_Codigo] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004A5,1,0,true,false )
             ,new CursorDef("BC004A6", "SELECT TM1.[ContratanteUsuario_ContratanteCod], TM1.[ContratanteUsuario_UsuarioCod], TM1.[Sistema_Codigo] FROM [ContratanteUsuarioSistema] TM1 WITH (NOLOCK) WHERE TM1.[ContratanteUsuario_ContratanteCod] = @ContratanteUsuario_ContratanteCod and TM1.[ContratanteUsuario_UsuarioCod] = @ContratanteUsuario_UsuarioCod and TM1.[Sistema_Codigo] = @Sistema_Codigo ORDER BY TM1.[ContratanteUsuario_ContratanteCod], TM1.[ContratanteUsuario_UsuarioCod], TM1.[Sistema_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC004A6,100,0,true,false )
             ,new CursorDef("BC004A7", "SELECT [ContratanteUsuario_ContratanteCod], [ContratanteUsuario_UsuarioCod], [Sistema_Codigo] FROM [ContratanteUsuarioSistema] WITH (NOLOCK) WHERE [ContratanteUsuario_ContratanteCod] = @ContratanteUsuario_ContratanteCod AND [ContratanteUsuario_UsuarioCod] = @ContratanteUsuario_UsuarioCod AND [Sistema_Codigo] = @Sistema_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC004A7,1,0,true,false )
             ,new CursorDef("BC004A8", "INSERT INTO [ContratanteUsuarioSistema]([ContratanteUsuario_ContratanteCod], [ContratanteUsuario_UsuarioCod], [Sistema_Codigo]) VALUES(@ContratanteUsuario_ContratanteCod, @ContratanteUsuario_UsuarioCod, @Sistema_Codigo)", GxErrorMask.GX_NOMASK,prmBC004A8)
             ,new CursorDef("BC004A9", "DELETE FROM [ContratanteUsuarioSistema]  WHERE [ContratanteUsuario_ContratanteCod] = @ContratanteUsuario_ContratanteCod AND [ContratanteUsuario_UsuarioCod] = @ContratanteUsuario_UsuarioCod AND [Sistema_Codigo] = @Sistema_Codigo", GxErrorMask.GX_NOMASK,prmBC004A9)
             ,new CursorDef("BC004A10", "SELECT TM1.[ContratanteUsuario_ContratanteCod], TM1.[ContratanteUsuario_UsuarioCod], TM1.[Sistema_Codigo] FROM [ContratanteUsuarioSistema] TM1 WITH (NOLOCK) WHERE TM1.[ContratanteUsuario_ContratanteCod] = @ContratanteUsuario_ContratanteCod and TM1.[ContratanteUsuario_UsuarioCod] = @ContratanteUsuario_UsuarioCod and TM1.[Sistema_Codigo] = @Sistema_Codigo ORDER BY TM1.[ContratanteUsuario_ContratanteCod], TM1.[ContratanteUsuario_UsuarioCod], TM1.[Sistema_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC004A10,100,0,true,false )
             ,new CursorDef("BC004A11", "SELECT [ContratanteUsuario_ContratanteCod] FROM [ContratanteUsuario] WITH (NOLOCK) WHERE [ContratanteUsuario_ContratanteCod] = @ContratanteUsuario_ContratanteCod AND [ContratanteUsuario_UsuarioCod] = @ContratanteUsuario_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004A11,1,0,true,false )
             ,new CursorDef("BC004A12", "SELECT [Sistema_Codigo] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004A12,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
