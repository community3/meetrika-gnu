/*
               File: GetExtraWWContagemResultadoExtraSelectionVs2FilterData
        Description: Get Extra WWContagem Resultado Extra Selection Vs2 Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 10/22/2019 21:9:41.30
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getextrawwcontagemresultadoextraselectionvs2filterdata : GXProcedure
   {
      public getextrawwcontagemresultadoextraselectionvs2filterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getextrawwcontagemresultadoextraselectionvs2filterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV19DDOName = aP0_DDOName;
         this.AV17SearchTxt = aP1_SearchTxt;
         this.AV18SearchTxtTo = aP2_SearchTxtTo;
         this.AV23OptionsJson = "" ;
         this.AV26OptionsDescJson = "" ;
         this.AV28OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV23OptionsJson;
         aP4_OptionsDescJson=this.AV26OptionsDescJson;
         aP5_OptionIndexesJson=this.AV28OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV19DDOName = aP0_DDOName;
         this.AV17SearchTxt = aP1_SearchTxt;
         this.AV18SearchTxtTo = aP2_SearchTxtTo;
         this.AV23OptionsJson = "" ;
         this.AV26OptionsDescJson = "" ;
         this.AV28OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV23OptionsJson;
         aP4_OptionsDescJson=this.AV26OptionsDescJson;
         aP5_OptionIndexesJson=this.AV28OptionIndexesJson;
         return AV28OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getextrawwcontagemresultadoextraselectionvs2filterdata objgetextrawwcontagemresultadoextraselectionvs2filterdata;
         objgetextrawwcontagemresultadoextraselectionvs2filterdata = new getextrawwcontagemresultadoextraselectionvs2filterdata();
         objgetextrawwcontagemresultadoextraselectionvs2filterdata.AV19DDOName = aP0_DDOName;
         objgetextrawwcontagemresultadoextraselectionvs2filterdata.AV17SearchTxt = aP1_SearchTxt;
         objgetextrawwcontagemresultadoextraselectionvs2filterdata.AV18SearchTxtTo = aP2_SearchTxtTo;
         objgetextrawwcontagemresultadoextraselectionvs2filterdata.AV23OptionsJson = "" ;
         objgetextrawwcontagemresultadoextraselectionvs2filterdata.AV26OptionsDescJson = "" ;
         objgetextrawwcontagemresultadoextraselectionvs2filterdata.AV28OptionIndexesJson = "" ;
         objgetextrawwcontagemresultadoextraselectionvs2filterdata.context.SetSubmitInitialConfig(context);
         objgetextrawwcontagemresultadoextraselectionvs2filterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetextrawwcontagemresultadoextraselectionvs2filterdata);
         aP3_OptionsJson=this.AV23OptionsJson;
         aP4_OptionsDescJson=this.AV26OptionsDescJson;
         aP5_OptionIndexesJson=this.AV28OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getextrawwcontagemresultadoextraselectionvs2filterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV22Options = (IGxCollection)(new GxSimpleCollection());
         AV25OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV27OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV19DDOName), "DDO_CONTAGEMRESULTADO_SERVICOSIGLA") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMRESULTADO_SERVICOSIGLAOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV23OptionsJson = AV22Options.ToJSonString(false);
         AV26OptionsDescJson = AV25OptionsDesc.ToJSonString(false);
         AV28OptionIndexesJson = AV27OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV30Session.Get("ExtraWWContagemResultadoExtraSelectionVs2GridState"), "") == 0 )
         {
            AV32GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "ExtraWWContagemResultadoExtraSelectionVs2GridState"), "");
         }
         else
         {
            AV32GridState.FromXml(AV30Session.Get("ExtraWWContagemResultadoExtraSelectionVs2GridState"), "");
         }
         AV163GXV1 = 1;
         while ( AV163GXV1 <= AV32GridState.gxTpr_Filtervalues.Count )
         {
            AV33GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV32GridState.gxTpr_Filtervalues.Item(AV163GXV1));
            if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "CONTRATADA_AREATRABALHOCOD") == 0 )
            {
               AV35Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( AV33GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "CONTAGEMRESULTADO_STATUSCNT") == 0 )
            {
               AV36ContagemResultado_StatusCnt = (short)(NumberUtil.Val( AV33GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_BASELINE_SEL") == 0 )
            {
               AV10TFContagemResultado_Baseline_Sel = (short)(NumberUtil.Val( AV33GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_SERVICOSIGLA") == 0 )
            {
               AV11TFContagemResultado_ServicoSigla = AV33GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_SERVICOSIGLA_SEL") == 0 )
            {
               AV12TFContagemResultado_ServicoSigla_Sel = AV33GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_PFFINAL") == 0 )
            {
               AV13TFContagemResultado_PFFinal = NumberUtil.Val( AV33GridStateFilterValue.gxTpr_Value, ".");
               AV14TFContagemResultado_PFFinal_To = NumberUtil.Val( AV33GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_VALORPF") == 0 )
            {
               AV15TFContagemResultado_ValorPF = NumberUtil.Val( AV33GridStateFilterValue.gxTpr_Value, ".");
               AV16TFContagemResultado_ValorPF_To = NumberUtil.Val( AV33GridStateFilterValue.gxTpr_Valueto, ".");
            }
            AV163GXV1 = (int)(AV163GXV1+1);
         }
         if ( AV32GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV34GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV32GridState.gxTpr_Dynamicfilters.Item(1));
            AV37DynamicFiltersSelector1 = AV34GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV37DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATACNT") == 0 )
            {
               AV39ContagemResultado_DataCnt1 = context.localUtil.CToD( AV34GridStateDynamicFilter.gxTpr_Value, 2);
               AV40ContagemResultado_DataCnt_To1 = context.localUtil.CToD( AV34GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV37DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
            {
               AV41ContagemResultado_StatusDmn1 = AV34GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37DynamicFiltersSelector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
            {
               AV38DynamicFiltersOperator1 = AV34GridStateDynamicFilter.gxTpr_Operator;
               AV42ContagemResultado_OsFsOsFm1 = AV34GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 )
            {
               AV43ContagemResultado_DataDmn1 = context.localUtil.CToD( AV34GridStateDynamicFilter.gxTpr_Value, 2);
               AV44ContagemResultado_DataDmn_To1 = context.localUtil.CToD( AV34GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV37DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
            {
               AV45ContagemResultado_DataPrevista1 = context.localUtil.CToD( AV34GridStateDynamicFilter.gxTpr_Value, 2);
               AV46ContagemResultado_DataPrevista_To1 = context.localUtil.CToD( AV34GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV37DynamicFiltersSelector1, "CONTAGEMRESULTADO_SERVICO") == 0 )
            {
               AV47ContagemResultado_Servico1 = (int)(NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV37DynamicFiltersSelector1, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
            {
               AV48ContagemResultado_ContadorFM1 = (int)(NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV37DynamicFiltersSelector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
            {
               AV49ContagemResultado_SistemaCod1 = (int)(NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV37DynamicFiltersSelector1, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
            {
               AV50ContagemResultado_ContratadaCod1 = (int)(NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV37DynamicFiltersSelector1, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 )
            {
               AV51ContagemResultado_ServicoGrupo1 = (int)(NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV37DynamicFiltersSelector1, "CONTAGEMRESULTADO_BASELINE") == 0 )
            {
               AV52ContagemResultado_Baseline1 = AV34GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37DynamicFiltersSelector1, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
            {
               AV53ContagemResultado_NaoCnfDmnCod1 = (int)(NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV37DynamicFiltersSelector1, "CONTAGEMRESULTADO_PFBFM") == 0 )
            {
               AV54ContagemResultado_PFBFM1 = NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, ".");
            }
            else if ( StringUtil.StrCmp(AV37DynamicFiltersSelector1, "CONTAGEMRESULTADO_PFBFS") == 0 )
            {
               AV55ContagemResultado_PFBFS1 = NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, ".");
            }
            else if ( StringUtil.StrCmp(AV37DynamicFiltersSelector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
            {
               AV56ContagemResultado_Agrupador1 = AV34GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37DynamicFiltersSelector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
            {
               AV57ContagemResultado_Descricao1 = AV34GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37DynamicFiltersSelector1, "CONTAGEMRESULTADO_OWNER") == 0 )
            {
               AV58ContagemResultado_Owner1 = (int)(NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV37DynamicFiltersSelector1, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 )
            {
               AV59ContagemResultado_TemPndHmlg1 = AV34GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37DynamicFiltersSelector1, "CONTAGEMRESULTADO_CNTCOD") == 0 )
            {
               AV60ContagemResultado_CntCod1 = (int)(NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, "."));
            }
            if ( AV32GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV61DynamicFiltersEnabled2 = true;
               AV34GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV32GridState.gxTpr_Dynamicfilters.Item(2));
               AV62DynamicFiltersSelector2 = AV34GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV62DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATACNT") == 0 )
               {
                  AV64ContagemResultado_DataCnt2 = context.localUtil.CToD( AV34GridStateDynamicFilter.gxTpr_Value, 2);
                  AV65ContagemResultado_DataCnt_To2 = context.localUtil.CToD( AV34GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               else if ( StringUtil.StrCmp(AV62DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
               {
                  AV66ContagemResultado_StatusDmn2 = AV34GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV62DynamicFiltersSelector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
               {
                  AV63DynamicFiltersOperator2 = AV34GridStateDynamicFilter.gxTpr_Operator;
                  AV67ContagemResultado_OsFsOsFm2 = AV34GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV62DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 )
               {
                  AV68ContagemResultado_DataDmn2 = context.localUtil.CToD( AV34GridStateDynamicFilter.gxTpr_Value, 2);
                  AV69ContagemResultado_DataDmn_To2 = context.localUtil.CToD( AV34GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               else if ( StringUtil.StrCmp(AV62DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
               {
                  AV70ContagemResultado_DataPrevista2 = context.localUtil.CToD( AV34GridStateDynamicFilter.gxTpr_Value, 2);
                  AV71ContagemResultado_DataPrevista_To2 = context.localUtil.CToD( AV34GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               else if ( StringUtil.StrCmp(AV62DynamicFiltersSelector2, "CONTAGEMRESULTADO_SERVICO") == 0 )
               {
                  AV72ContagemResultado_Servico2 = (int)(NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, "."));
               }
               else if ( StringUtil.StrCmp(AV62DynamicFiltersSelector2, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
               {
                  AV73ContagemResultado_ContadorFM2 = (int)(NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, "."));
               }
               else if ( StringUtil.StrCmp(AV62DynamicFiltersSelector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
               {
                  AV74ContagemResultado_SistemaCod2 = (int)(NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, "."));
               }
               else if ( StringUtil.StrCmp(AV62DynamicFiltersSelector2, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
               {
                  AV75ContagemResultado_ContratadaCod2 = (int)(NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, "."));
               }
               else if ( StringUtil.StrCmp(AV62DynamicFiltersSelector2, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 )
               {
                  AV76ContagemResultado_ServicoGrupo2 = (int)(NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, "."));
               }
               else if ( StringUtil.StrCmp(AV62DynamicFiltersSelector2, "CONTAGEMRESULTADO_BASELINE") == 0 )
               {
                  AV77ContagemResultado_Baseline2 = AV34GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV62DynamicFiltersSelector2, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
               {
                  AV78ContagemResultado_NaoCnfDmnCod2 = (int)(NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, "."));
               }
               else if ( StringUtil.StrCmp(AV62DynamicFiltersSelector2, "CONTAGEMRESULTADO_PFBFM") == 0 )
               {
                  AV79ContagemResultado_PFBFM2 = NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, ".");
               }
               else if ( StringUtil.StrCmp(AV62DynamicFiltersSelector2, "CONTAGEMRESULTADO_PFBFS") == 0 )
               {
                  AV80ContagemResultado_PFBFS2 = NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, ".");
               }
               else if ( StringUtil.StrCmp(AV62DynamicFiltersSelector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
               {
                  AV81ContagemResultado_Agrupador2 = AV34GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV62DynamicFiltersSelector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
               {
                  AV82ContagemResultado_Descricao2 = AV34GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV62DynamicFiltersSelector2, "CONTAGEMRESULTADO_OWNER") == 0 )
               {
                  AV83ContagemResultado_Owner2 = (int)(NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, "."));
               }
               else if ( StringUtil.StrCmp(AV62DynamicFiltersSelector2, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 )
               {
                  AV84ContagemResultado_TemPndHmlg2 = AV34GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV62DynamicFiltersSelector2, "CONTAGEMRESULTADO_CNTCOD") == 0 )
               {
                  AV85ContagemResultado_CntCod2 = (int)(NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, "."));
               }
               if ( AV32GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV86DynamicFiltersEnabled3 = true;
                  AV34GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV32GridState.gxTpr_Dynamicfilters.Item(3));
                  AV87DynamicFiltersSelector3 = AV34GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV87DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATACNT") == 0 )
                  {
                     AV89ContagemResultado_DataCnt3 = context.localUtil.CToD( AV34GridStateDynamicFilter.gxTpr_Value, 2);
                     AV90ContagemResultado_DataCnt_To3 = context.localUtil.CToD( AV34GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
                  else if ( StringUtil.StrCmp(AV87DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
                  {
                     AV91ContagemResultado_StatusDmn3 = AV34GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV87DynamicFiltersSelector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
                  {
                     AV88DynamicFiltersOperator3 = AV34GridStateDynamicFilter.gxTpr_Operator;
                     AV92ContagemResultado_OsFsOsFm3 = AV34GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV87DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 )
                  {
                     AV93ContagemResultado_DataDmn3 = context.localUtil.CToD( AV34GridStateDynamicFilter.gxTpr_Value, 2);
                     AV94ContagemResultado_DataDmn_To3 = context.localUtil.CToD( AV34GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
                  else if ( StringUtil.StrCmp(AV87DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
                  {
                     AV95ContagemResultado_DataPrevista3 = context.localUtil.CToD( AV34GridStateDynamicFilter.gxTpr_Value, 2);
                     AV96ContagemResultado_DataPrevista_To3 = context.localUtil.CToD( AV34GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
                  else if ( StringUtil.StrCmp(AV87DynamicFiltersSelector3, "CONTAGEMRESULTADO_SERVICO") == 0 )
                  {
                     AV97ContagemResultado_Servico3 = (int)(NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  else if ( StringUtil.StrCmp(AV87DynamicFiltersSelector3, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
                  {
                     AV98ContagemResultado_ContadorFM3 = (int)(NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  else if ( StringUtil.StrCmp(AV87DynamicFiltersSelector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
                  {
                     AV99ContagemResultado_SistemaCod3 = (int)(NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  else if ( StringUtil.StrCmp(AV87DynamicFiltersSelector3, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
                  {
                     AV100ContagemResultado_ContratadaCod3 = (int)(NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  else if ( StringUtil.StrCmp(AV87DynamicFiltersSelector3, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 )
                  {
                     AV101ContagemResultado_ServicoGrupo3 = (int)(NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  else if ( StringUtil.StrCmp(AV87DynamicFiltersSelector3, "CONTAGEMRESULTADO_BASELINE") == 0 )
                  {
                     AV102ContagemResultado_Baseline3 = AV34GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV87DynamicFiltersSelector3, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
                  {
                     AV103ContagemResultado_NaoCnfDmnCod3 = (int)(NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  else if ( StringUtil.StrCmp(AV87DynamicFiltersSelector3, "CONTAGEMRESULTADO_PFBFM") == 0 )
                  {
                     AV104ContagemResultado_PFBFM3 = NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, ".");
                  }
                  else if ( StringUtil.StrCmp(AV87DynamicFiltersSelector3, "CONTAGEMRESULTADO_PFBFS") == 0 )
                  {
                     AV105ContagemResultado_PFBFS3 = NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, ".");
                  }
                  else if ( StringUtil.StrCmp(AV87DynamicFiltersSelector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
                  {
                     AV106ContagemResultado_Agrupador3 = AV34GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV87DynamicFiltersSelector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
                  {
                     AV107ContagemResultado_Descricao3 = AV34GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV87DynamicFiltersSelector3, "CONTAGEMRESULTADO_OWNER") == 0 )
                  {
                     AV108ContagemResultado_Owner3 = (int)(NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  else if ( StringUtil.StrCmp(AV87DynamicFiltersSelector3, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 )
                  {
                     AV109ContagemResultado_TemPndHmlg3 = AV34GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV87DynamicFiltersSelector3, "CONTAGEMRESULTADO_CNTCOD") == 0 )
                  {
                     AV110ContagemResultado_CntCod3 = (int)(NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  if ( AV32GridState.gxTpr_Dynamicfilters.Count >= 4 )
                  {
                     AV111DynamicFiltersEnabled4 = true;
                     AV34GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV32GridState.gxTpr_Dynamicfilters.Item(4));
                     AV112DynamicFiltersSelector4 = AV34GridStateDynamicFilter.gxTpr_Selected;
                     if ( StringUtil.StrCmp(AV112DynamicFiltersSelector4, "CONTAGEMRESULTADO_DATACNT") == 0 )
                     {
                        AV114ContagemResultado_DataCnt4 = context.localUtil.CToD( AV34GridStateDynamicFilter.gxTpr_Value, 2);
                        AV115ContagemResultado_DataCnt_To4 = context.localUtil.CToD( AV34GridStateDynamicFilter.gxTpr_Valueto, 2);
                     }
                     else if ( StringUtil.StrCmp(AV112DynamicFiltersSelector4, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
                     {
                        AV116ContagemResultado_StatusDmn4 = AV34GridStateDynamicFilter.gxTpr_Value;
                     }
                     else if ( StringUtil.StrCmp(AV112DynamicFiltersSelector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
                     {
                        AV113DynamicFiltersOperator4 = AV34GridStateDynamicFilter.gxTpr_Operator;
                        AV117ContagemResultado_OsFsOsFm4 = AV34GridStateDynamicFilter.gxTpr_Value;
                     }
                     else if ( StringUtil.StrCmp(AV112DynamicFiltersSelector4, "CONTAGEMRESULTADO_DATADMN") == 0 )
                     {
                        AV118ContagemResultado_DataDmn4 = context.localUtil.CToD( AV34GridStateDynamicFilter.gxTpr_Value, 2);
                        AV119ContagemResultado_DataDmn_To4 = context.localUtil.CToD( AV34GridStateDynamicFilter.gxTpr_Valueto, 2);
                     }
                     else if ( StringUtil.StrCmp(AV112DynamicFiltersSelector4, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
                     {
                        AV120ContagemResultado_DataPrevista4 = context.localUtil.CToD( AV34GridStateDynamicFilter.gxTpr_Value, 2);
                        AV121ContagemResultado_DataPrevista_To4 = context.localUtil.CToD( AV34GridStateDynamicFilter.gxTpr_Valueto, 2);
                     }
                     else if ( StringUtil.StrCmp(AV112DynamicFiltersSelector4, "CONTAGEMRESULTADO_SERVICO") == 0 )
                     {
                        AV122ContagemResultado_Servico4 = (int)(NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, "."));
                     }
                     else if ( StringUtil.StrCmp(AV112DynamicFiltersSelector4, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
                     {
                        AV123ContagemResultado_ContadorFM4 = (int)(NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, "."));
                     }
                     else if ( StringUtil.StrCmp(AV112DynamicFiltersSelector4, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
                     {
                        AV124ContagemResultado_SistemaCod4 = (int)(NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, "."));
                     }
                     else if ( StringUtil.StrCmp(AV112DynamicFiltersSelector4, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
                     {
                        AV125ContagemResultado_ContratadaCod4 = (int)(NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, "."));
                     }
                     else if ( StringUtil.StrCmp(AV112DynamicFiltersSelector4, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 )
                     {
                        AV126ContagemResultado_ServicoGrupo4 = (int)(NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, "."));
                     }
                     else if ( StringUtil.StrCmp(AV112DynamicFiltersSelector4, "CONTAGEMRESULTADO_BASELINE") == 0 )
                     {
                        AV127ContagemResultado_Baseline4 = AV34GridStateDynamicFilter.gxTpr_Value;
                     }
                     else if ( StringUtil.StrCmp(AV112DynamicFiltersSelector4, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
                     {
                        AV128ContagemResultado_NaoCnfDmnCod4 = (int)(NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, "."));
                     }
                     else if ( StringUtil.StrCmp(AV112DynamicFiltersSelector4, "CONTAGEMRESULTADO_PFBFM") == 0 )
                     {
                        AV129ContagemResultado_PFBFM4 = NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, ".");
                     }
                     else if ( StringUtil.StrCmp(AV112DynamicFiltersSelector4, "CONTAGEMRESULTADO_PFBFS") == 0 )
                     {
                        AV130ContagemResultado_PFBFS4 = NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, ".");
                     }
                     else if ( StringUtil.StrCmp(AV112DynamicFiltersSelector4, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
                     {
                        AV131ContagemResultado_Agrupador4 = AV34GridStateDynamicFilter.gxTpr_Value;
                     }
                     else if ( StringUtil.StrCmp(AV112DynamicFiltersSelector4, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
                     {
                        AV132ContagemResultado_Descricao4 = AV34GridStateDynamicFilter.gxTpr_Value;
                     }
                     else if ( StringUtil.StrCmp(AV112DynamicFiltersSelector4, "CONTAGEMRESULTADO_OWNER") == 0 )
                     {
                        AV133ContagemResultado_Owner4 = (int)(NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, "."));
                     }
                     else if ( StringUtil.StrCmp(AV112DynamicFiltersSelector4, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 )
                     {
                        AV134ContagemResultado_TemPndHmlg4 = AV34GridStateDynamicFilter.gxTpr_Value;
                     }
                     else if ( StringUtil.StrCmp(AV112DynamicFiltersSelector4, "CONTAGEMRESULTADO_CNTCOD") == 0 )
                     {
                        AV135ContagemResultado_CntCod4 = (int)(NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, "."));
                     }
                     if ( AV32GridState.gxTpr_Dynamicfilters.Count >= 5 )
                     {
                        AV136DynamicFiltersEnabled5 = true;
                        AV34GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV32GridState.gxTpr_Dynamicfilters.Item(5));
                        AV137DynamicFiltersSelector5 = AV34GridStateDynamicFilter.gxTpr_Selected;
                        if ( StringUtil.StrCmp(AV137DynamicFiltersSelector5, "CONTAGEMRESULTADO_DATACNT") == 0 )
                        {
                           AV139ContagemResultado_DataCnt5 = context.localUtil.CToD( AV34GridStateDynamicFilter.gxTpr_Value, 2);
                           AV140ContagemResultado_DataCnt_To5 = context.localUtil.CToD( AV34GridStateDynamicFilter.gxTpr_Valueto, 2);
                        }
                        else if ( StringUtil.StrCmp(AV137DynamicFiltersSelector5, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
                        {
                           AV141ContagemResultado_StatusDmn5 = AV34GridStateDynamicFilter.gxTpr_Value;
                        }
                        else if ( StringUtil.StrCmp(AV137DynamicFiltersSelector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
                        {
                           AV138DynamicFiltersOperator5 = AV34GridStateDynamicFilter.gxTpr_Operator;
                           AV142ContagemResultado_OsFsOsFm5 = AV34GridStateDynamicFilter.gxTpr_Value;
                        }
                        else if ( StringUtil.StrCmp(AV137DynamicFiltersSelector5, "CONTAGEMRESULTADO_DATADMN") == 0 )
                        {
                           AV143ContagemResultado_DataDmn5 = context.localUtil.CToD( AV34GridStateDynamicFilter.gxTpr_Value, 2);
                           AV144ContagemResultado_DataDmn_To5 = context.localUtil.CToD( AV34GridStateDynamicFilter.gxTpr_Valueto, 2);
                        }
                        else if ( StringUtil.StrCmp(AV137DynamicFiltersSelector5, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
                        {
                           AV145ContagemResultado_DataPrevista5 = context.localUtil.CToD( AV34GridStateDynamicFilter.gxTpr_Value, 2);
                           AV146ContagemResultado_DataPrevista_To5 = context.localUtil.CToD( AV34GridStateDynamicFilter.gxTpr_Valueto, 2);
                        }
                        else if ( StringUtil.StrCmp(AV137DynamicFiltersSelector5, "CONTAGEMRESULTADO_SERVICO") == 0 )
                        {
                           AV147ContagemResultado_Servico5 = (int)(NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, "."));
                        }
                        else if ( StringUtil.StrCmp(AV137DynamicFiltersSelector5, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
                        {
                           AV148ContagemResultado_ContadorFM5 = (int)(NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, "."));
                        }
                        else if ( StringUtil.StrCmp(AV137DynamicFiltersSelector5, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
                        {
                           AV149ContagemResultado_SistemaCod5 = (int)(NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, "."));
                        }
                        else if ( StringUtil.StrCmp(AV137DynamicFiltersSelector5, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
                        {
                           AV150ContagemResultado_ContratadaCod5 = (int)(NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, "."));
                        }
                        else if ( StringUtil.StrCmp(AV137DynamicFiltersSelector5, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 )
                        {
                           AV151ContagemResultado_ServicoGrupo5 = (int)(NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, "."));
                        }
                        else if ( StringUtil.StrCmp(AV137DynamicFiltersSelector5, "CONTAGEMRESULTADO_BASELINE") == 0 )
                        {
                           AV152ContagemResultado_Baseline5 = AV34GridStateDynamicFilter.gxTpr_Value;
                        }
                        else if ( StringUtil.StrCmp(AV137DynamicFiltersSelector5, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
                        {
                           AV153ContagemResultado_NaoCnfDmnCod5 = (int)(NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, "."));
                        }
                        else if ( StringUtil.StrCmp(AV137DynamicFiltersSelector5, "CONTAGEMRESULTADO_PFBFM") == 0 )
                        {
                           AV154ContagemResultado_PFBFM5 = NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, ".");
                        }
                        else if ( StringUtil.StrCmp(AV137DynamicFiltersSelector5, "CONTAGEMRESULTADO_PFBFS") == 0 )
                        {
                           AV155ContagemResultado_PFBFS5 = NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, ".");
                        }
                        else if ( StringUtil.StrCmp(AV137DynamicFiltersSelector5, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
                        {
                           AV156ContagemResultado_Agrupador5 = AV34GridStateDynamicFilter.gxTpr_Value;
                        }
                        else if ( StringUtil.StrCmp(AV137DynamicFiltersSelector5, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
                        {
                           AV157ContagemResultado_Descricao5 = AV34GridStateDynamicFilter.gxTpr_Value;
                        }
                        else if ( StringUtil.StrCmp(AV137DynamicFiltersSelector5, "CONTAGEMRESULTADO_OWNER") == 0 )
                        {
                           AV158ContagemResultado_Owner5 = (int)(NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, "."));
                        }
                        else if ( StringUtil.StrCmp(AV137DynamicFiltersSelector5, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 )
                        {
                           AV159ContagemResultado_TemPndHmlg5 = AV34GridStateDynamicFilter.gxTpr_Value;
                        }
                        else if ( StringUtil.StrCmp(AV137DynamicFiltersSelector5, "CONTAGEMRESULTADO_CNTCOD") == 0 )
                        {
                           AV160ContagemResultado_CntCod5 = (int)(NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, "."));
                        }
                     }
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTAGEMRESULTADO_SERVICOSIGLAOPTIONS' Routine */
         AV11TFContagemResultado_ServicoSigla = AV17SearchTxt;
         AV12TFContagemResultado_ServicoSigla_Sel = "";
         AV165ExtraWWContagemResultadoExtraSelectionVs2DS_1_Contratada_areatrabalhocod = AV35Contratada_AreaTrabalhoCod;
         AV166ExtraWWContagemResultadoExtraSelectionVs2DS_2_Contagemresultado_statuscnt = AV36ContagemResultado_StatusCnt;
         AV167ExtraWWContagemResultadoExtraSelectionVs2DS_3_Dynamicfiltersselector1 = AV37DynamicFiltersSelector1;
         AV168ExtraWWContagemResultadoExtraSelectionVs2DS_4_Dynamicfiltersoperator1 = AV38DynamicFiltersOperator1;
         AV169ExtraWWContagemResultadoExtraSelectionVs2DS_5_Contagemresultado_datacnt1 = AV39ContagemResultado_DataCnt1;
         AV170ExtraWWContagemResultadoExtraSelectionVs2DS_6_Contagemresultado_datacnt_to1 = AV40ContagemResultado_DataCnt_To1;
         AV171ExtraWWContagemResultadoExtraSelectionVs2DS_7_Contagemresultado_statusdmn1 = AV41ContagemResultado_StatusDmn1;
         AV172ExtraWWContagemResultadoExtraSelectionVs2DS_8_Contagemresultado_osfsosfm1 = AV42ContagemResultado_OsFsOsFm1;
         AV173ExtraWWContagemResultadoExtraSelectionVs2DS_9_Contagemresultado_datadmn1 = AV43ContagemResultado_DataDmn1;
         AV174ExtraWWContagemResultadoExtraSelectionVs2DS_10_Contagemresultado_datadmn_to1 = AV44ContagemResultado_DataDmn_To1;
         AV175ExtraWWContagemResultadoExtraSelectionVs2DS_11_Contagemresultado_dataprevista1 = AV45ContagemResultado_DataPrevista1;
         AV176ExtraWWContagemResultadoExtraSelectionVs2DS_12_Contagemresultado_dataprevista_to1 = AV46ContagemResultado_DataPrevista_To1;
         AV177ExtraWWContagemResultadoExtraSelectionVs2DS_13_Contagemresultado_servico1 = AV47ContagemResultado_Servico1;
         AV178ExtraWWContagemResultadoExtraSelectionVs2DS_14_Contagemresultado_contadorfm1 = AV48ContagemResultado_ContadorFM1;
         AV179ExtraWWContagemResultadoExtraSelectionVs2DS_15_Contagemresultado_sistemacod1 = AV49ContagemResultado_SistemaCod1;
         AV180ExtraWWContagemResultadoExtraSelectionVs2DS_16_Contagemresultado_contratadacod1 = AV50ContagemResultado_ContratadaCod1;
         AV181ExtraWWContagemResultadoExtraSelectionVs2DS_17_Contagemresultado_servicogrupo1 = AV51ContagemResultado_ServicoGrupo1;
         AV182ExtraWWContagemResultadoExtraSelectionVs2DS_18_Contagemresultado_baseline1 = AV52ContagemResultado_Baseline1;
         AV183ExtraWWContagemResultadoExtraSelectionVs2DS_19_Contagemresultado_naocnfdmncod1 = AV53ContagemResultado_NaoCnfDmnCod1;
         AV184ExtraWWContagemResultadoExtraSelectionVs2DS_20_Contagemresultado_pfbfm1 = AV54ContagemResultado_PFBFM1;
         AV185ExtraWWContagemResultadoExtraSelectionVs2DS_21_Contagemresultado_pfbfs1 = AV55ContagemResultado_PFBFS1;
         AV186ExtraWWContagemResultadoExtraSelectionVs2DS_22_Contagemresultado_agrupador1 = AV56ContagemResultado_Agrupador1;
         AV187ExtraWWContagemResultadoExtraSelectionVs2DS_23_Contagemresultado_descricao1 = AV57ContagemResultado_Descricao1;
         AV188ExtraWWContagemResultadoExtraSelectionVs2DS_24_Contagemresultado_owner1 = AV58ContagemResultado_Owner1;
         AV189ExtraWWContagemResultadoExtraSelectionVs2DS_25_Contagemresultado_tempndhmlg1 = AV59ContagemResultado_TemPndHmlg1;
         AV190ExtraWWContagemResultadoExtraSelectionVs2DS_26_Contagemresultado_cntcod1 = AV60ContagemResultado_CntCod1;
         AV191ExtraWWContagemResultadoExtraSelectionVs2DS_27_Dynamicfiltersenabled2 = AV61DynamicFiltersEnabled2;
         AV192ExtraWWContagemResultadoExtraSelectionVs2DS_28_Dynamicfiltersselector2 = AV62DynamicFiltersSelector2;
         AV193ExtraWWContagemResultadoExtraSelectionVs2DS_29_Dynamicfiltersoperator2 = AV63DynamicFiltersOperator2;
         AV194ExtraWWContagemResultadoExtraSelectionVs2DS_30_Contagemresultado_datacnt2 = AV64ContagemResultado_DataCnt2;
         AV195ExtraWWContagemResultadoExtraSelectionVs2DS_31_Contagemresultado_datacnt_to2 = AV65ContagemResultado_DataCnt_To2;
         AV196ExtraWWContagemResultadoExtraSelectionVs2DS_32_Contagemresultado_statusdmn2 = AV66ContagemResultado_StatusDmn2;
         AV197ExtraWWContagemResultadoExtraSelectionVs2DS_33_Contagemresultado_osfsosfm2 = AV67ContagemResultado_OsFsOsFm2;
         AV198ExtraWWContagemResultadoExtraSelectionVs2DS_34_Contagemresultado_datadmn2 = AV68ContagemResultado_DataDmn2;
         AV199ExtraWWContagemResultadoExtraSelectionVs2DS_35_Contagemresultado_datadmn_to2 = AV69ContagemResultado_DataDmn_To2;
         AV200ExtraWWContagemResultadoExtraSelectionVs2DS_36_Contagemresultado_dataprevista2 = AV70ContagemResultado_DataPrevista2;
         AV201ExtraWWContagemResultadoExtraSelectionVs2DS_37_Contagemresultado_dataprevista_to2 = AV71ContagemResultado_DataPrevista_To2;
         AV202ExtraWWContagemResultadoExtraSelectionVs2DS_38_Contagemresultado_servico2 = AV72ContagemResultado_Servico2;
         AV203ExtraWWContagemResultadoExtraSelectionVs2DS_39_Contagemresultado_contadorfm2 = AV73ContagemResultado_ContadorFM2;
         AV204ExtraWWContagemResultadoExtraSelectionVs2DS_40_Contagemresultado_sistemacod2 = AV74ContagemResultado_SistemaCod2;
         AV205ExtraWWContagemResultadoExtraSelectionVs2DS_41_Contagemresultado_contratadacod2 = AV75ContagemResultado_ContratadaCod2;
         AV206ExtraWWContagemResultadoExtraSelectionVs2DS_42_Contagemresultado_servicogrupo2 = AV76ContagemResultado_ServicoGrupo2;
         AV207ExtraWWContagemResultadoExtraSelectionVs2DS_43_Contagemresultado_baseline2 = AV77ContagemResultado_Baseline2;
         AV208ExtraWWContagemResultadoExtraSelectionVs2DS_44_Contagemresultado_naocnfdmncod2 = AV78ContagemResultado_NaoCnfDmnCod2;
         AV209ExtraWWContagemResultadoExtraSelectionVs2DS_45_Contagemresultado_pfbfm2 = AV79ContagemResultado_PFBFM2;
         AV210ExtraWWContagemResultadoExtraSelectionVs2DS_46_Contagemresultado_pfbfs2 = AV80ContagemResultado_PFBFS2;
         AV211ExtraWWContagemResultadoExtraSelectionVs2DS_47_Contagemresultado_agrupador2 = AV81ContagemResultado_Agrupador2;
         AV212ExtraWWContagemResultadoExtraSelectionVs2DS_48_Contagemresultado_descricao2 = AV82ContagemResultado_Descricao2;
         AV213ExtraWWContagemResultadoExtraSelectionVs2DS_49_Contagemresultado_owner2 = AV83ContagemResultado_Owner2;
         AV214ExtraWWContagemResultadoExtraSelectionVs2DS_50_Contagemresultado_tempndhmlg2 = AV84ContagemResultado_TemPndHmlg2;
         AV215ExtraWWContagemResultadoExtraSelectionVs2DS_51_Contagemresultado_cntcod2 = AV85ContagemResultado_CntCod2;
         AV216ExtraWWContagemResultadoExtraSelectionVs2DS_52_Dynamicfiltersenabled3 = AV86DynamicFiltersEnabled3;
         AV217ExtraWWContagemResultadoExtraSelectionVs2DS_53_Dynamicfiltersselector3 = AV87DynamicFiltersSelector3;
         AV218ExtraWWContagemResultadoExtraSelectionVs2DS_54_Dynamicfiltersoperator3 = AV88DynamicFiltersOperator3;
         AV219ExtraWWContagemResultadoExtraSelectionVs2DS_55_Contagemresultado_datacnt3 = AV89ContagemResultado_DataCnt3;
         AV220ExtraWWContagemResultadoExtraSelectionVs2DS_56_Contagemresultado_datacnt_to3 = AV90ContagemResultado_DataCnt_To3;
         AV221ExtraWWContagemResultadoExtraSelectionVs2DS_57_Contagemresultado_statusdmn3 = AV91ContagemResultado_StatusDmn3;
         AV222ExtraWWContagemResultadoExtraSelectionVs2DS_58_Contagemresultado_osfsosfm3 = AV92ContagemResultado_OsFsOsFm3;
         AV223ExtraWWContagemResultadoExtraSelectionVs2DS_59_Contagemresultado_datadmn3 = AV93ContagemResultado_DataDmn3;
         AV224ExtraWWContagemResultadoExtraSelectionVs2DS_60_Contagemresultado_datadmn_to3 = AV94ContagemResultado_DataDmn_To3;
         AV225ExtraWWContagemResultadoExtraSelectionVs2DS_61_Contagemresultado_dataprevista3 = AV95ContagemResultado_DataPrevista3;
         AV226ExtraWWContagemResultadoExtraSelectionVs2DS_62_Contagemresultado_dataprevista_to3 = AV96ContagemResultado_DataPrevista_To3;
         AV227ExtraWWContagemResultadoExtraSelectionVs2DS_63_Contagemresultado_servico3 = AV97ContagemResultado_Servico3;
         AV228ExtraWWContagemResultadoExtraSelectionVs2DS_64_Contagemresultado_contadorfm3 = AV98ContagemResultado_ContadorFM3;
         AV229ExtraWWContagemResultadoExtraSelectionVs2DS_65_Contagemresultado_sistemacod3 = AV99ContagemResultado_SistemaCod3;
         AV230ExtraWWContagemResultadoExtraSelectionVs2DS_66_Contagemresultado_contratadacod3 = AV100ContagemResultado_ContratadaCod3;
         AV231ExtraWWContagemResultadoExtraSelectionVs2DS_67_Contagemresultado_servicogrupo3 = AV101ContagemResultado_ServicoGrupo3;
         AV232ExtraWWContagemResultadoExtraSelectionVs2DS_68_Contagemresultado_baseline3 = AV102ContagemResultado_Baseline3;
         AV233ExtraWWContagemResultadoExtraSelectionVs2DS_69_Contagemresultado_naocnfdmncod3 = AV103ContagemResultado_NaoCnfDmnCod3;
         AV234ExtraWWContagemResultadoExtraSelectionVs2DS_70_Contagemresultado_pfbfm3 = AV104ContagemResultado_PFBFM3;
         AV235ExtraWWContagemResultadoExtraSelectionVs2DS_71_Contagemresultado_pfbfs3 = AV105ContagemResultado_PFBFS3;
         AV236ExtraWWContagemResultadoExtraSelectionVs2DS_72_Contagemresultado_agrupador3 = AV106ContagemResultado_Agrupador3;
         AV237ExtraWWContagemResultadoExtraSelectionVs2DS_73_Contagemresultado_descricao3 = AV107ContagemResultado_Descricao3;
         AV238ExtraWWContagemResultadoExtraSelectionVs2DS_74_Contagemresultado_owner3 = AV108ContagemResultado_Owner3;
         AV239ExtraWWContagemResultadoExtraSelectionVs2DS_75_Contagemresultado_tempndhmlg3 = AV109ContagemResultado_TemPndHmlg3;
         AV240ExtraWWContagemResultadoExtraSelectionVs2DS_76_Contagemresultado_cntcod3 = AV110ContagemResultado_CntCod3;
         AV241ExtraWWContagemResultadoExtraSelectionVs2DS_77_Dynamicfiltersenabled4 = AV111DynamicFiltersEnabled4;
         AV242ExtraWWContagemResultadoExtraSelectionVs2DS_78_Dynamicfiltersselector4 = AV112DynamicFiltersSelector4;
         AV243ExtraWWContagemResultadoExtraSelectionVs2DS_79_Dynamicfiltersoperator4 = AV113DynamicFiltersOperator4;
         AV244ExtraWWContagemResultadoExtraSelectionVs2DS_80_Contagemresultado_datacnt4 = AV114ContagemResultado_DataCnt4;
         AV245ExtraWWContagemResultadoExtraSelectionVs2DS_81_Contagemresultado_datacnt_to4 = AV115ContagemResultado_DataCnt_To4;
         AV246ExtraWWContagemResultadoExtraSelectionVs2DS_82_Contagemresultado_statusdmn4 = AV116ContagemResultado_StatusDmn4;
         AV247ExtraWWContagemResultadoExtraSelectionVs2DS_83_Contagemresultado_osfsosfm4 = AV117ContagemResultado_OsFsOsFm4;
         AV248ExtraWWContagemResultadoExtraSelectionVs2DS_84_Contagemresultado_datadmn4 = AV118ContagemResultado_DataDmn4;
         AV249ExtraWWContagemResultadoExtraSelectionVs2DS_85_Contagemresultado_datadmn_to4 = AV119ContagemResultado_DataDmn_To4;
         AV250ExtraWWContagemResultadoExtraSelectionVs2DS_86_Contagemresultado_dataprevista4 = AV120ContagemResultado_DataPrevista4;
         AV251ExtraWWContagemResultadoExtraSelectionVs2DS_87_Contagemresultado_dataprevista_to4 = AV121ContagemResultado_DataPrevista_To4;
         AV252ExtraWWContagemResultadoExtraSelectionVs2DS_88_Contagemresultado_servico4 = AV122ContagemResultado_Servico4;
         AV253ExtraWWContagemResultadoExtraSelectionVs2DS_89_Contagemresultado_contadorfm4 = AV123ContagemResultado_ContadorFM4;
         AV254ExtraWWContagemResultadoExtraSelectionVs2DS_90_Contagemresultado_sistemacod4 = AV124ContagemResultado_SistemaCod4;
         AV255ExtraWWContagemResultadoExtraSelectionVs2DS_91_Contagemresultado_contratadacod4 = AV125ContagemResultado_ContratadaCod4;
         AV256ExtraWWContagemResultadoExtraSelectionVs2DS_92_Contagemresultado_servicogrupo4 = AV126ContagemResultado_ServicoGrupo4;
         AV257ExtraWWContagemResultadoExtraSelectionVs2DS_93_Contagemresultado_baseline4 = AV127ContagemResultado_Baseline4;
         AV258ExtraWWContagemResultadoExtraSelectionVs2DS_94_Contagemresultado_naocnfdmncod4 = AV128ContagemResultado_NaoCnfDmnCod4;
         AV259ExtraWWContagemResultadoExtraSelectionVs2DS_95_Contagemresultado_pfbfm4 = AV129ContagemResultado_PFBFM4;
         AV260ExtraWWContagemResultadoExtraSelectionVs2DS_96_Contagemresultado_pfbfs4 = AV130ContagemResultado_PFBFS4;
         AV261ExtraWWContagemResultadoExtraSelectionVs2DS_97_Contagemresultado_agrupador4 = AV131ContagemResultado_Agrupador4;
         AV262ExtraWWContagemResultadoExtraSelectionVs2DS_98_Contagemresultado_descricao4 = AV132ContagemResultado_Descricao4;
         AV263ExtraWWContagemResultadoExtraSelectionVs2DS_99_Contagemresultado_owner4 = AV133ContagemResultado_Owner4;
         AV264ExtraWWContagemResultadoExtraSelectionVs2DS_100_Contagemresultado_tempndhmlg4 = AV134ContagemResultado_TemPndHmlg4;
         AV265ExtraWWContagemResultadoExtraSelectionVs2DS_101_Contagemresultado_cntcod4 = AV135ContagemResultado_CntCod4;
         AV266ExtraWWContagemResultadoExtraSelectionVs2DS_102_Dynamicfiltersenabled5 = AV136DynamicFiltersEnabled5;
         AV267ExtraWWContagemResultadoExtraSelectionVs2DS_103_Dynamicfiltersselector5 = AV137DynamicFiltersSelector5;
         AV268ExtraWWContagemResultadoExtraSelectionVs2DS_104_Dynamicfiltersoperator5 = AV138DynamicFiltersOperator5;
         AV269ExtraWWContagemResultadoExtraSelectionVs2DS_105_Contagemresultado_datacnt5 = AV139ContagemResultado_DataCnt5;
         AV270ExtraWWContagemResultadoExtraSelectionVs2DS_106_Contagemresultado_datacnt_to5 = AV140ContagemResultado_DataCnt_To5;
         AV271ExtraWWContagemResultadoExtraSelectionVs2DS_107_Contagemresultado_statusdmn5 = AV141ContagemResultado_StatusDmn5;
         AV272ExtraWWContagemResultadoExtraSelectionVs2DS_108_Contagemresultado_osfsosfm5 = AV142ContagemResultado_OsFsOsFm5;
         AV273ExtraWWContagemResultadoExtraSelectionVs2DS_109_Contagemresultado_datadmn5 = AV143ContagemResultado_DataDmn5;
         AV274ExtraWWContagemResultadoExtraSelectionVs2DS_110_Contagemresultado_datadmn_to5 = AV144ContagemResultado_DataDmn_To5;
         AV275ExtraWWContagemResultadoExtraSelectionVs2DS_111_Contagemresultado_dataprevista5 = AV145ContagemResultado_DataPrevista5;
         AV276ExtraWWContagemResultadoExtraSelectionVs2DS_112_Contagemresultado_dataprevista_to5 = AV146ContagemResultado_DataPrevista_To5;
         AV277ExtraWWContagemResultadoExtraSelectionVs2DS_113_Contagemresultado_servico5 = AV147ContagemResultado_Servico5;
         AV278ExtraWWContagemResultadoExtraSelectionVs2DS_114_Contagemresultado_contadorfm5 = AV148ContagemResultado_ContadorFM5;
         AV279ExtraWWContagemResultadoExtraSelectionVs2DS_115_Contagemresultado_sistemacod5 = AV149ContagemResultado_SistemaCod5;
         AV280ExtraWWContagemResultadoExtraSelectionVs2DS_116_Contagemresultado_contratadacod5 = AV150ContagemResultado_ContratadaCod5;
         AV281ExtraWWContagemResultadoExtraSelectionVs2DS_117_Contagemresultado_servicogrupo5 = AV151ContagemResultado_ServicoGrupo5;
         AV282ExtraWWContagemResultadoExtraSelectionVs2DS_118_Contagemresultado_baseline5 = AV152ContagemResultado_Baseline5;
         AV283ExtraWWContagemResultadoExtraSelectionVs2DS_119_Contagemresultado_naocnfdmncod5 = AV153ContagemResultado_NaoCnfDmnCod5;
         AV284ExtraWWContagemResultadoExtraSelectionVs2DS_120_Contagemresultado_pfbfm5 = AV154ContagemResultado_PFBFM5;
         AV285ExtraWWContagemResultadoExtraSelectionVs2DS_121_Contagemresultado_pfbfs5 = AV155ContagemResultado_PFBFS5;
         AV286ExtraWWContagemResultadoExtraSelectionVs2DS_122_Contagemresultado_agrupador5 = AV156ContagemResultado_Agrupador5;
         AV287ExtraWWContagemResultadoExtraSelectionVs2DS_123_Contagemresultado_descricao5 = AV157ContagemResultado_Descricao5;
         AV288ExtraWWContagemResultadoExtraSelectionVs2DS_124_Contagemresultado_owner5 = AV158ContagemResultado_Owner5;
         AV289ExtraWWContagemResultadoExtraSelectionVs2DS_125_Contagemresultado_tempndhmlg5 = AV159ContagemResultado_TemPndHmlg5;
         AV290ExtraWWContagemResultadoExtraSelectionVs2DS_126_Contagemresultado_cntcod5 = AV160ContagemResultado_CntCod5;
         AV291ExtraWWContagemResultadoExtraSelectionVs2DS_127_Tfcontagemresultado_baseline_sel = AV10TFContagemResultado_Baseline_Sel;
         AV292ExtraWWContagemResultadoExtraSelectionVs2DS_128_Tfcontagemresultado_servicosigla = AV11TFContagemResultado_ServicoSigla;
         AV293ExtraWWContagemResultadoExtraSelectionVs2DS_129_Tfcontagemresultado_servicosigla_sel = AV12TFContagemResultado_ServicoSigla_Sel;
         AV294ExtraWWContagemResultadoExtraSelectionVs2DS_130_Tfcontagemresultado_pffinal = AV13TFContagemResultado_PFFinal;
         AV295ExtraWWContagemResultadoExtraSelectionVs2DS_131_Tfcontagemresultado_pffinal_to = AV14TFContagemResultado_PFFinal_To;
         AV296ExtraWWContagemResultadoExtraSelectionVs2DS_132_Tfcontagemresultado_valorpf = AV15TFContagemResultado_ValorPF;
         AV297ExtraWWContagemResultadoExtraSelectionVs2DS_133_Tfcontagemresultado_valorpf_to = AV16TFContagemResultado_ValorPF_To;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV9WWPContext.gxTpr_Contratada_codigo ,
                                              AV167ExtraWWContagemResultadoExtraSelectionVs2DS_3_Dynamicfiltersselector1 ,
                                              AV171ExtraWWContagemResultadoExtraSelectionVs2DS_7_Contagemresultado_statusdmn1 ,
                                              AV168ExtraWWContagemResultadoExtraSelectionVs2DS_4_Dynamicfiltersoperator1 ,
                                              AV172ExtraWWContagemResultadoExtraSelectionVs2DS_8_Contagemresultado_osfsosfm1 ,
                                              AV173ExtraWWContagemResultadoExtraSelectionVs2DS_9_Contagemresultado_datadmn1 ,
                                              AV174ExtraWWContagemResultadoExtraSelectionVs2DS_10_Contagemresultado_datadmn_to1 ,
                                              AV175ExtraWWContagemResultadoExtraSelectionVs2DS_11_Contagemresultado_dataprevista1 ,
                                              AV176ExtraWWContagemResultadoExtraSelectionVs2DS_12_Contagemresultado_dataprevista_to1 ,
                                              AV177ExtraWWContagemResultadoExtraSelectionVs2DS_13_Contagemresultado_servico1 ,
                                              AV179ExtraWWContagemResultadoExtraSelectionVs2DS_15_Contagemresultado_sistemacod1 ,
                                              AV180ExtraWWContagemResultadoExtraSelectionVs2DS_16_Contagemresultado_contratadacod1 ,
                                              AV165ExtraWWContagemResultadoExtraSelectionVs2DS_1_Contratada_areatrabalhocod ,
                                              AV181ExtraWWContagemResultadoExtraSelectionVs2DS_17_Contagemresultado_servicogrupo1 ,
                                              AV182ExtraWWContagemResultadoExtraSelectionVs2DS_18_Contagemresultado_baseline1 ,
                                              AV183ExtraWWContagemResultadoExtraSelectionVs2DS_19_Contagemresultado_naocnfdmncod1 ,
                                              AV186ExtraWWContagemResultadoExtraSelectionVs2DS_22_Contagemresultado_agrupador1 ,
                                              AV187ExtraWWContagemResultadoExtraSelectionVs2DS_23_Contagemresultado_descricao1 ,
                                              AV188ExtraWWContagemResultadoExtraSelectionVs2DS_24_Contagemresultado_owner1 ,
                                              AV190ExtraWWContagemResultadoExtraSelectionVs2DS_26_Contagemresultado_cntcod1 ,
                                              AV191ExtraWWContagemResultadoExtraSelectionVs2DS_27_Dynamicfiltersenabled2 ,
                                              AV192ExtraWWContagemResultadoExtraSelectionVs2DS_28_Dynamicfiltersselector2 ,
                                              AV196ExtraWWContagemResultadoExtraSelectionVs2DS_32_Contagemresultado_statusdmn2 ,
                                              AV193ExtraWWContagemResultadoExtraSelectionVs2DS_29_Dynamicfiltersoperator2 ,
                                              AV197ExtraWWContagemResultadoExtraSelectionVs2DS_33_Contagemresultado_osfsosfm2 ,
                                              AV198ExtraWWContagemResultadoExtraSelectionVs2DS_34_Contagemresultado_datadmn2 ,
                                              AV199ExtraWWContagemResultadoExtraSelectionVs2DS_35_Contagemresultado_datadmn_to2 ,
                                              AV200ExtraWWContagemResultadoExtraSelectionVs2DS_36_Contagemresultado_dataprevista2 ,
                                              AV201ExtraWWContagemResultadoExtraSelectionVs2DS_37_Contagemresultado_dataprevista_to2 ,
                                              AV202ExtraWWContagemResultadoExtraSelectionVs2DS_38_Contagemresultado_servico2 ,
                                              AV204ExtraWWContagemResultadoExtraSelectionVs2DS_40_Contagemresultado_sistemacod2 ,
                                              AV205ExtraWWContagemResultadoExtraSelectionVs2DS_41_Contagemresultado_contratadacod2 ,
                                              AV206ExtraWWContagemResultadoExtraSelectionVs2DS_42_Contagemresultado_servicogrupo2 ,
                                              AV207ExtraWWContagemResultadoExtraSelectionVs2DS_43_Contagemresultado_baseline2 ,
                                              AV208ExtraWWContagemResultadoExtraSelectionVs2DS_44_Contagemresultado_naocnfdmncod2 ,
                                              AV211ExtraWWContagemResultadoExtraSelectionVs2DS_47_Contagemresultado_agrupador2 ,
                                              AV212ExtraWWContagemResultadoExtraSelectionVs2DS_48_Contagemresultado_descricao2 ,
                                              AV213ExtraWWContagemResultadoExtraSelectionVs2DS_49_Contagemresultado_owner2 ,
                                              AV215ExtraWWContagemResultadoExtraSelectionVs2DS_51_Contagemresultado_cntcod2 ,
                                              AV216ExtraWWContagemResultadoExtraSelectionVs2DS_52_Dynamicfiltersenabled3 ,
                                              AV217ExtraWWContagemResultadoExtraSelectionVs2DS_53_Dynamicfiltersselector3 ,
                                              AV221ExtraWWContagemResultadoExtraSelectionVs2DS_57_Contagemresultado_statusdmn3 ,
                                              AV218ExtraWWContagemResultadoExtraSelectionVs2DS_54_Dynamicfiltersoperator3 ,
                                              AV222ExtraWWContagemResultadoExtraSelectionVs2DS_58_Contagemresultado_osfsosfm3 ,
                                              AV223ExtraWWContagemResultadoExtraSelectionVs2DS_59_Contagemresultado_datadmn3 ,
                                              AV224ExtraWWContagemResultadoExtraSelectionVs2DS_60_Contagemresultado_datadmn_to3 ,
                                              AV225ExtraWWContagemResultadoExtraSelectionVs2DS_61_Contagemresultado_dataprevista3 ,
                                              AV226ExtraWWContagemResultadoExtraSelectionVs2DS_62_Contagemresultado_dataprevista_to3 ,
                                              AV227ExtraWWContagemResultadoExtraSelectionVs2DS_63_Contagemresultado_servico3 ,
                                              AV229ExtraWWContagemResultadoExtraSelectionVs2DS_65_Contagemresultado_sistemacod3 ,
                                              AV230ExtraWWContagemResultadoExtraSelectionVs2DS_66_Contagemresultado_contratadacod3 ,
                                              AV231ExtraWWContagemResultadoExtraSelectionVs2DS_67_Contagemresultado_servicogrupo3 ,
                                              AV232ExtraWWContagemResultadoExtraSelectionVs2DS_68_Contagemresultado_baseline3 ,
                                              AV233ExtraWWContagemResultadoExtraSelectionVs2DS_69_Contagemresultado_naocnfdmncod3 ,
                                              AV236ExtraWWContagemResultadoExtraSelectionVs2DS_72_Contagemresultado_agrupador3 ,
                                              AV237ExtraWWContagemResultadoExtraSelectionVs2DS_73_Contagemresultado_descricao3 ,
                                              AV238ExtraWWContagemResultadoExtraSelectionVs2DS_74_Contagemresultado_owner3 ,
                                              AV240ExtraWWContagemResultadoExtraSelectionVs2DS_76_Contagemresultado_cntcod3 ,
                                              AV241ExtraWWContagemResultadoExtraSelectionVs2DS_77_Dynamicfiltersenabled4 ,
                                              AV242ExtraWWContagemResultadoExtraSelectionVs2DS_78_Dynamicfiltersselector4 ,
                                              AV246ExtraWWContagemResultadoExtraSelectionVs2DS_82_Contagemresultado_statusdmn4 ,
                                              AV243ExtraWWContagemResultadoExtraSelectionVs2DS_79_Dynamicfiltersoperator4 ,
                                              AV247ExtraWWContagemResultadoExtraSelectionVs2DS_83_Contagemresultado_osfsosfm4 ,
                                              AV248ExtraWWContagemResultadoExtraSelectionVs2DS_84_Contagemresultado_datadmn4 ,
                                              AV249ExtraWWContagemResultadoExtraSelectionVs2DS_85_Contagemresultado_datadmn_to4 ,
                                              AV250ExtraWWContagemResultadoExtraSelectionVs2DS_86_Contagemresultado_dataprevista4 ,
                                              AV251ExtraWWContagemResultadoExtraSelectionVs2DS_87_Contagemresultado_dataprevista_to4 ,
                                              AV252ExtraWWContagemResultadoExtraSelectionVs2DS_88_Contagemresultado_servico4 ,
                                              AV254ExtraWWContagemResultadoExtraSelectionVs2DS_90_Contagemresultado_sistemacod4 ,
                                              AV255ExtraWWContagemResultadoExtraSelectionVs2DS_91_Contagemresultado_contratadacod4 ,
                                              AV256ExtraWWContagemResultadoExtraSelectionVs2DS_92_Contagemresultado_servicogrupo4 ,
                                              AV257ExtraWWContagemResultadoExtraSelectionVs2DS_93_Contagemresultado_baseline4 ,
                                              AV258ExtraWWContagemResultadoExtraSelectionVs2DS_94_Contagemresultado_naocnfdmncod4 ,
                                              AV261ExtraWWContagemResultadoExtraSelectionVs2DS_97_Contagemresultado_agrupador4 ,
                                              AV262ExtraWWContagemResultadoExtraSelectionVs2DS_98_Contagemresultado_descricao4 ,
                                              AV263ExtraWWContagemResultadoExtraSelectionVs2DS_99_Contagemresultado_owner4 ,
                                              AV265ExtraWWContagemResultadoExtraSelectionVs2DS_101_Contagemresultado_cntcod4 ,
                                              AV266ExtraWWContagemResultadoExtraSelectionVs2DS_102_Dynamicfiltersenabled5 ,
                                              AV267ExtraWWContagemResultadoExtraSelectionVs2DS_103_Dynamicfiltersselector5 ,
                                              AV271ExtraWWContagemResultadoExtraSelectionVs2DS_107_Contagemresultado_statusdmn5 ,
                                              AV268ExtraWWContagemResultadoExtraSelectionVs2DS_104_Dynamicfiltersoperator5 ,
                                              AV272ExtraWWContagemResultadoExtraSelectionVs2DS_108_Contagemresultado_osfsosfm5 ,
                                              AV273ExtraWWContagemResultadoExtraSelectionVs2DS_109_Contagemresultado_datadmn5 ,
                                              AV274ExtraWWContagemResultadoExtraSelectionVs2DS_110_Contagemresultado_datadmn_to5 ,
                                              AV275ExtraWWContagemResultadoExtraSelectionVs2DS_111_Contagemresultado_dataprevista5 ,
                                              AV276ExtraWWContagemResultadoExtraSelectionVs2DS_112_Contagemresultado_dataprevista_to5 ,
                                              AV277ExtraWWContagemResultadoExtraSelectionVs2DS_113_Contagemresultado_servico5 ,
                                              AV279ExtraWWContagemResultadoExtraSelectionVs2DS_115_Contagemresultado_sistemacod5 ,
                                              AV280ExtraWWContagemResultadoExtraSelectionVs2DS_116_Contagemresultado_contratadacod5 ,
                                              AV281ExtraWWContagemResultadoExtraSelectionVs2DS_117_Contagemresultado_servicogrupo5 ,
                                              AV282ExtraWWContagemResultadoExtraSelectionVs2DS_118_Contagemresultado_baseline5 ,
                                              AV283ExtraWWContagemResultadoExtraSelectionVs2DS_119_Contagemresultado_naocnfdmncod5 ,
                                              AV286ExtraWWContagemResultadoExtraSelectionVs2DS_122_Contagemresultado_agrupador5 ,
                                              AV287ExtraWWContagemResultadoExtraSelectionVs2DS_123_Contagemresultado_descricao5 ,
                                              AV288ExtraWWContagemResultadoExtraSelectionVs2DS_124_Contagemresultado_owner5 ,
                                              AV290ExtraWWContagemResultadoExtraSelectionVs2DS_126_Contagemresultado_cntcod5 ,
                                              AV291ExtraWWContagemResultadoExtraSelectionVs2DS_127_Tfcontagemresultado_baseline_sel ,
                                              AV293ExtraWWContagemResultadoExtraSelectionVs2DS_129_Tfcontagemresultado_servicosigla_sel ,
                                              AV292ExtraWWContagemResultadoExtraSelectionVs2DS_128_Tfcontagemresultado_servicosigla ,
                                              AV296ExtraWWContagemResultadoExtraSelectionVs2DS_132_Tfcontagemresultado_valorpf ,
                                              AV297ExtraWWContagemResultadoExtraSelectionVs2DS_133_Tfcontagemresultado_valorpf_to ,
                                              A490ContagemResultado_ContratadaCod ,
                                              A484ContagemResultado_StatusDmn ,
                                              A457ContagemResultado_Demanda ,
                                              A493ContagemResultado_DemandaFM ,
                                              A471ContagemResultado_DataDmn ,
                                              A1351ContagemResultado_DataPrevista ,
                                              A601ContagemResultado_Servico ,
                                              A489ContagemResultado_SistemaCod ,
                                              A764ContagemResultado_ServicoGrupo ,
                                              A598ContagemResultado_Baseline ,
                                              A468ContagemResultado_NaoCnfDmnCod ,
                                              A1046ContagemResultado_Agrupador ,
                                              A494ContagemResultado_Descricao ,
                                              A508ContagemResultado_Owner ,
                                              A1603ContagemResultado_CntCod ,
                                              A801ContagemResultado_ServicoSigla ,
                                              A512ContagemResultado_ValorPF ,
                                              A531ContagemResultado_StatusUltCnt ,
                                              A1854ContagemResultado_VlrCnc ,
                                              AV169ExtraWWContagemResultadoExtraSelectionVs2DS_5_Contagemresultado_datacnt1 ,
                                              A566ContagemResultado_DataUltCnt ,
                                              AV170ExtraWWContagemResultadoExtraSelectionVs2DS_6_Contagemresultado_datacnt_to1 ,
                                              AV178ExtraWWContagemResultadoExtraSelectionVs2DS_14_Contagemresultado_contadorfm1 ,
                                              A584ContagemResultado_ContadorFM ,
                                              A684ContagemResultado_PFBFSUltima ,
                                              A682ContagemResultado_PFBFMUltima ,
                                              A685ContagemResultado_PFLFSUltima ,
                                              A683ContagemResultado_PFLFMUltima ,
                                              AV189ExtraWWContagemResultadoExtraSelectionVs2DS_25_Contagemresultado_tempndhmlg1 ,
                                              A1802ContagemResultado_TemPndHmlg ,
                                              AV194ExtraWWContagemResultadoExtraSelectionVs2DS_30_Contagemresultado_datacnt2 ,
                                              AV195ExtraWWContagemResultadoExtraSelectionVs2DS_31_Contagemresultado_datacnt_to2 ,
                                              AV203ExtraWWContagemResultadoExtraSelectionVs2DS_39_Contagemresultado_contadorfm2 ,
                                              AV214ExtraWWContagemResultadoExtraSelectionVs2DS_50_Contagemresultado_tempndhmlg2 ,
                                              AV219ExtraWWContagemResultadoExtraSelectionVs2DS_55_Contagemresultado_datacnt3 ,
                                              AV220ExtraWWContagemResultadoExtraSelectionVs2DS_56_Contagemresultado_datacnt_to3 ,
                                              AV228ExtraWWContagemResultadoExtraSelectionVs2DS_64_Contagemresultado_contadorfm3 ,
                                              AV239ExtraWWContagemResultadoExtraSelectionVs2DS_75_Contagemresultado_tempndhmlg3 ,
                                              AV244ExtraWWContagemResultadoExtraSelectionVs2DS_80_Contagemresultado_datacnt4 ,
                                              AV245ExtraWWContagemResultadoExtraSelectionVs2DS_81_Contagemresultado_datacnt_to4 ,
                                              AV253ExtraWWContagemResultadoExtraSelectionVs2DS_89_Contagemresultado_contadorfm4 ,
                                              AV264ExtraWWContagemResultadoExtraSelectionVs2DS_100_Contagemresultado_tempndhmlg4 ,
                                              AV269ExtraWWContagemResultadoExtraSelectionVs2DS_105_Contagemresultado_datacnt5 ,
                                              AV270ExtraWWContagemResultadoExtraSelectionVs2DS_106_Contagemresultado_datacnt_to5 ,
                                              AV278ExtraWWContagemResultadoExtraSelectionVs2DS_114_Contagemresultado_contadorfm5 ,
                                              AV289ExtraWWContagemResultadoExtraSelectionVs2DS_125_Contagemresultado_tempndhmlg5 ,
                                              AV294ExtraWWContagemResultadoExtraSelectionVs2DS_130_Tfcontagemresultado_pffinal ,
                                              A574ContagemResultado_PFFinal ,
                                              AV295ExtraWWContagemResultadoExtraSelectionVs2DS_131_Tfcontagemresultado_pffinal_to ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.INT, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         lV172ExtraWWContagemResultadoExtraSelectionVs2DS_8_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV172ExtraWWContagemResultadoExtraSelectionVs2DS_8_Contagemresultado_osfsosfm1), "%", "");
         lV172ExtraWWContagemResultadoExtraSelectionVs2DS_8_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV172ExtraWWContagemResultadoExtraSelectionVs2DS_8_Contagemresultado_osfsosfm1), "%", "");
         lV172ExtraWWContagemResultadoExtraSelectionVs2DS_8_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV172ExtraWWContagemResultadoExtraSelectionVs2DS_8_Contagemresultado_osfsosfm1), "%", "");
         lV172ExtraWWContagemResultadoExtraSelectionVs2DS_8_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV172ExtraWWContagemResultadoExtraSelectionVs2DS_8_Contagemresultado_osfsosfm1), "%", "");
         lV187ExtraWWContagemResultadoExtraSelectionVs2DS_23_Contagemresultado_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV187ExtraWWContagemResultadoExtraSelectionVs2DS_23_Contagemresultado_descricao1), "%", "");
         lV197ExtraWWContagemResultadoExtraSelectionVs2DS_33_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV197ExtraWWContagemResultadoExtraSelectionVs2DS_33_Contagemresultado_osfsosfm2), "%", "");
         lV197ExtraWWContagemResultadoExtraSelectionVs2DS_33_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV197ExtraWWContagemResultadoExtraSelectionVs2DS_33_Contagemresultado_osfsosfm2), "%", "");
         lV197ExtraWWContagemResultadoExtraSelectionVs2DS_33_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV197ExtraWWContagemResultadoExtraSelectionVs2DS_33_Contagemresultado_osfsosfm2), "%", "");
         lV197ExtraWWContagemResultadoExtraSelectionVs2DS_33_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV197ExtraWWContagemResultadoExtraSelectionVs2DS_33_Contagemresultado_osfsosfm2), "%", "");
         lV212ExtraWWContagemResultadoExtraSelectionVs2DS_48_Contagemresultado_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV212ExtraWWContagemResultadoExtraSelectionVs2DS_48_Contagemresultado_descricao2), "%", "");
         lV222ExtraWWContagemResultadoExtraSelectionVs2DS_58_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV222ExtraWWContagemResultadoExtraSelectionVs2DS_58_Contagemresultado_osfsosfm3), "%", "");
         lV222ExtraWWContagemResultadoExtraSelectionVs2DS_58_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV222ExtraWWContagemResultadoExtraSelectionVs2DS_58_Contagemresultado_osfsosfm3), "%", "");
         lV222ExtraWWContagemResultadoExtraSelectionVs2DS_58_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV222ExtraWWContagemResultadoExtraSelectionVs2DS_58_Contagemresultado_osfsosfm3), "%", "");
         lV222ExtraWWContagemResultadoExtraSelectionVs2DS_58_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV222ExtraWWContagemResultadoExtraSelectionVs2DS_58_Contagemresultado_osfsosfm3), "%", "");
         lV237ExtraWWContagemResultadoExtraSelectionVs2DS_73_Contagemresultado_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV237ExtraWWContagemResultadoExtraSelectionVs2DS_73_Contagemresultado_descricao3), "%", "");
         lV247ExtraWWContagemResultadoExtraSelectionVs2DS_83_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV247ExtraWWContagemResultadoExtraSelectionVs2DS_83_Contagemresultado_osfsosfm4), "%", "");
         lV247ExtraWWContagemResultadoExtraSelectionVs2DS_83_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV247ExtraWWContagemResultadoExtraSelectionVs2DS_83_Contagemresultado_osfsosfm4), "%", "");
         lV247ExtraWWContagemResultadoExtraSelectionVs2DS_83_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV247ExtraWWContagemResultadoExtraSelectionVs2DS_83_Contagemresultado_osfsosfm4), "%", "");
         lV247ExtraWWContagemResultadoExtraSelectionVs2DS_83_Contagemresultado_osfsosfm4 = StringUtil.Concat( StringUtil.RTrim( AV247ExtraWWContagemResultadoExtraSelectionVs2DS_83_Contagemresultado_osfsosfm4), "%", "");
         lV262ExtraWWContagemResultadoExtraSelectionVs2DS_98_Contagemresultado_descricao4 = StringUtil.Concat( StringUtil.RTrim( AV262ExtraWWContagemResultadoExtraSelectionVs2DS_98_Contagemresultado_descricao4), "%", "");
         lV272ExtraWWContagemResultadoExtraSelectionVs2DS_108_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV272ExtraWWContagemResultadoExtraSelectionVs2DS_108_Contagemresultado_osfsosfm5), "%", "");
         lV272ExtraWWContagemResultadoExtraSelectionVs2DS_108_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV272ExtraWWContagemResultadoExtraSelectionVs2DS_108_Contagemresultado_osfsosfm5), "%", "");
         lV272ExtraWWContagemResultadoExtraSelectionVs2DS_108_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV272ExtraWWContagemResultadoExtraSelectionVs2DS_108_Contagemresultado_osfsosfm5), "%", "");
         lV272ExtraWWContagemResultadoExtraSelectionVs2DS_108_Contagemresultado_osfsosfm5 = StringUtil.Concat( StringUtil.RTrim( AV272ExtraWWContagemResultadoExtraSelectionVs2DS_108_Contagemresultado_osfsosfm5), "%", "");
         lV287ExtraWWContagemResultadoExtraSelectionVs2DS_123_Contagemresultado_descricao5 = StringUtil.Concat( StringUtil.RTrim( AV287ExtraWWContagemResultadoExtraSelectionVs2DS_123_Contagemresultado_descricao5), "%", "");
         lV292ExtraWWContagemResultadoExtraSelectionVs2DS_128_Tfcontagemresultado_servicosigla = StringUtil.PadR( StringUtil.RTrim( AV292ExtraWWContagemResultadoExtraSelectionVs2DS_128_Tfcontagemresultado_servicosigla), 15, "%");
         /* Using cursor P00YD3 */
         pr_default.execute(0, new Object[] {AV171ExtraWWContagemResultadoExtraSelectionVs2DS_7_Contagemresultado_statusdmn1, AV196ExtraWWContagemResultadoExtraSelectionVs2DS_32_Contagemresultado_statusdmn2, AV221ExtraWWContagemResultadoExtraSelectionVs2DS_57_Contagemresultado_statusdmn3, AV246ExtraWWContagemResultadoExtraSelectionVs2DS_82_Contagemresultado_statusdmn4, AV271ExtraWWContagemResultadoExtraSelectionVs2DS_107_Contagemresultado_statusdmn5, AV167ExtraWWContagemResultadoExtraSelectionVs2DS_3_Dynamicfiltersselector1, AV169ExtraWWContagemResultadoExtraSelectionVs2DS_5_Contagemresultado_datacnt1, AV169ExtraWWContagemResultadoExtraSelectionVs2DS_5_Contagemresultado_datacnt1, AV167ExtraWWContagemResultadoExtraSelectionVs2DS_3_Dynamicfiltersselector1, AV170ExtraWWContagemResultadoExtraSelectionVs2DS_6_Contagemresultado_datacnt_to1, AV170ExtraWWContagemResultadoExtraSelectionVs2DS_6_Contagemresultado_datacnt_to1, AV167ExtraWWContagemResultadoExtraSelectionVs2DS_3_Dynamicfiltersselector1, AV178ExtraWWContagemResultadoExtraSelectionVs2DS_14_Contagemresultado_contadorfm1, AV178ExtraWWContagemResultadoExtraSelectionVs2DS_14_Contagemresultado_contadorfm1, AV178ExtraWWContagemResultadoExtraSelectionVs2DS_14_Contagemresultado_contadorfm1, AV167ExtraWWContagemResultadoExtraSelectionVs2DS_3_Dynamicfiltersselector1, AV167ExtraWWContagemResultadoExtraSelectionVs2DS_3_Dynamicfiltersselector1, AV191ExtraWWContagemResultadoExtraSelectionVs2DS_27_Dynamicfiltersenabled2, AV192ExtraWWContagemResultadoExtraSelectionVs2DS_28_Dynamicfiltersselector2, AV194ExtraWWContagemResultadoExtraSelectionVs2DS_30_Contagemresultado_datacnt2, AV194ExtraWWContagemResultadoExtraSelectionVs2DS_30_Contagemresultado_datacnt2, AV191ExtraWWContagemResultadoExtraSelectionVs2DS_27_Dynamicfiltersenabled2, AV192ExtraWWContagemResultadoExtraSelectionVs2DS_28_Dynamicfiltersselector2, AV195ExtraWWContagemResultadoExtraSelectionVs2DS_31_Contagemresultado_datacnt_to2, AV195ExtraWWContagemResultadoExtraSelectionVs2DS_31_Contagemresultado_datacnt_to2, AV191ExtraWWContagemResultadoExtraSelectionVs2DS_27_Dynamicfiltersenabled2, AV192ExtraWWContagemResultadoExtraSelectionVs2DS_28_Dynamicfiltersselector2, AV203ExtraWWContagemResultadoExtraSelectionVs2DS_39_Contagemresultado_contadorfm2, AV203ExtraWWContagemResultadoExtraSelectionVs2DS_39_Contagemresultado_contadorfm2, AV203ExtraWWContagemResultadoExtraSelectionVs2DS_39_Contagemresultado_contadorfm2, AV191ExtraWWContagemResultadoExtraSelectionVs2DS_27_Dynamicfiltersenabled2, AV192ExtraWWContagemResultadoExtraSelectionVs2DS_28_Dynamicfiltersselector2, AV191ExtraWWContagemResultadoExtraSelectionVs2DS_27_Dynamicfiltersenabled2, AV192ExtraWWContagemResultadoExtraSelectionVs2DS_28_Dynamicfiltersselector2, AV216ExtraWWContagemResultadoExtraSelectionVs2DS_52_Dynamicfiltersenabled3, AV217ExtraWWContagemResultadoExtraSelectionVs2DS_53_Dynamicfiltersselector3, AV219ExtraWWContagemResultadoExtraSelectionVs2DS_55_Contagemresultado_datacnt3, AV219ExtraWWContagemResultadoExtraSelectionVs2DS_55_Contagemresultado_datacnt3, AV216ExtraWWContagemResultadoExtraSelectionVs2DS_52_Dynamicfiltersenabled3, AV217ExtraWWContagemResultadoExtraSelectionVs2DS_53_Dynamicfiltersselector3, AV220ExtraWWContagemResultadoExtraSelectionVs2DS_56_Contagemresultado_datacnt_to3, AV220ExtraWWContagemResultadoExtraSelectionVs2DS_56_Contagemresultado_datacnt_to3, AV216ExtraWWContagemResultadoExtraSelectionVs2DS_52_Dynamicfiltersenabled3, AV217ExtraWWContagemResultadoExtraSelectionVs2DS_53_Dynamicfiltersselector3, AV228ExtraWWContagemResultadoExtraSelectionVs2DS_64_Contagemresultado_contadorfm3, AV228ExtraWWContagemResultadoExtraSelectionVs2DS_64_Contagemresultado_contadorfm3, AV228ExtraWWContagemResultadoExtraSelectionVs2DS_64_Contagemresultado_contadorfm3, AV216ExtraWWContagemResultadoExtraSelectionVs2DS_52_Dynamicfiltersenabled3, AV217ExtraWWContagemResultadoExtraSelectionVs2DS_53_Dynamicfiltersselector3, AV216ExtraWWContagemResultadoExtraSelectionVs2DS_52_Dynamicfiltersenabled3, AV217ExtraWWContagemResultadoExtraSelectionVs2DS_53_Dynamicfiltersselector3, AV241ExtraWWContagemResultadoExtraSelectionVs2DS_77_Dynamicfiltersenabled4, AV242ExtraWWContagemResultadoExtraSelectionVs2DS_78_Dynamicfiltersselector4, AV244ExtraWWContagemResultadoExtraSelectionVs2DS_80_Contagemresultado_datacnt4, AV244ExtraWWContagemResultadoExtraSelectionVs2DS_80_Contagemresultado_datacnt4, AV241ExtraWWContagemResultadoExtraSelectionVs2DS_77_Dynamicfiltersenabled4, AV242ExtraWWContagemResultadoExtraSelectionVs2DS_78_Dynamicfiltersselector4, AV245ExtraWWContagemResultadoExtraSelectionVs2DS_81_Contagemresultado_datacnt_to4, AV245ExtraWWContagemResultadoExtraSelectionVs2DS_81_Contagemresultado_datacnt_to4, AV241ExtraWWContagemResultadoExtraSelectionVs2DS_77_Dynamicfiltersenabled4, AV242ExtraWWContagemResultadoExtraSelectionVs2DS_78_Dynamicfiltersselector4, AV253ExtraWWContagemResultadoExtraSelectionVs2DS_89_Contagemresultado_contadorfm4, AV253ExtraWWContagemResultadoExtraSelectionVs2DS_89_Contagemresultado_contadorfm4, AV253ExtraWWContagemResultadoExtraSelectionVs2DS_89_Contagemresultado_contadorfm4, AV241ExtraWWContagemResultadoExtraSelectionVs2DS_77_Dynamicfiltersenabled4, AV242ExtraWWContagemResultadoExtraSelectionVs2DS_78_Dynamicfiltersselector4, AV241ExtraWWContagemResultadoExtraSelectionVs2DS_77_Dynamicfiltersenabled4, AV242ExtraWWContagemResultadoExtraSelectionVs2DS_78_Dynamicfiltersselector4, AV266ExtraWWContagemResultadoExtraSelectionVs2DS_102_Dynamicfiltersenabled5, AV267ExtraWWContagemResultadoExtraSelectionVs2DS_103_Dynamicfiltersselector5, AV269ExtraWWContagemResultadoExtraSelectionVs2DS_105_Contagemresultado_datacnt5, AV269ExtraWWContagemResultadoExtraSelectionVs2DS_105_Contagemresultado_datacnt5, AV266ExtraWWContagemResultadoExtraSelectionVs2DS_102_Dynamicfiltersenabled5, AV267ExtraWWContagemResultadoExtraSelectionVs2DS_103_Dynamicfiltersselector5, AV270ExtraWWContagemResultadoExtraSelectionVs2DS_106_Contagemresultado_datacnt_to5, AV270ExtraWWContagemResultadoExtraSelectionVs2DS_106_Contagemresultado_datacnt_to5, AV266ExtraWWContagemResultadoExtraSelectionVs2DS_102_Dynamicfiltersenabled5, AV267ExtraWWContagemResultadoExtraSelectionVs2DS_103_Dynamicfiltersselector5, AV278ExtraWWContagemResultadoExtraSelectionVs2DS_114_Contagemresultado_contadorfm5, AV278ExtraWWContagemResultadoExtraSelectionVs2DS_114_Contagemresultado_contadorfm5, AV278ExtraWWContagemResultadoExtraSelectionVs2DS_114_Contagemresultado_contadorfm5, AV266ExtraWWContagemResultadoExtraSelectionVs2DS_102_Dynamicfiltersenabled5, AV267ExtraWWContagemResultadoExtraSelectionVs2DS_103_Dynamicfiltersselector5, AV266ExtraWWContagemResultadoExtraSelectionVs2DS_102_Dynamicfiltersenabled5, AV267ExtraWWContagemResultadoExtraSelectionVs2DS_103_Dynamicfiltersselector5, AV9WWPContext.gxTpr_Areatrabalho_codigo, AV9WWPContext.gxTpr_Contratada_codigo, AV171ExtraWWContagemResultadoExtraSelectionVs2DS_7_Contagemresultado_statusdmn1, AV172ExtraWWContagemResultadoExtraSelectionVs2DS_8_Contagemresultado_osfsosfm1, AV172ExtraWWContagemResultadoExtraSelectionVs2DS_8_Contagemresultado_osfsosfm1, lV172ExtraWWContagemResultadoExtraSelectionVs2DS_8_Contagemresultado_osfsosfm1, lV172ExtraWWContagemResultadoExtraSelectionVs2DS_8_Contagemresultado_osfsosfm1, lV172ExtraWWContagemResultadoExtraSelectionVs2DS_8_Contagemresultado_osfsosfm1, lV172ExtraWWContagemResultadoExtraSelectionVs2DS_8_Contagemresultado_osfsosfm1, AV173ExtraWWContagemResultadoExtraSelectionVs2DS_9_Contagemresultado_datadmn1, AV174ExtraWWContagemResultadoExtraSelectionVs2DS_10_Contagemresultado_datadmn_to1, AV175ExtraWWContagemResultadoExtraSelectionVs2DS_11_Contagemresultado_dataprevista1, AV176ExtraWWContagemResultadoExtraSelectionVs2DS_12_Contagemresultado_dataprevista_to1, AV177ExtraWWContagemResultadoExtraSelectionVs2DS_13_Contagemresultado_servico1, AV179ExtraWWContagemResultadoExtraSelectionVs2DS_15_Contagemresultado_sistemacod1, AV180ExtraWWContagemResultadoExtraSelectionVs2DS_16_Contagemresultado_contratadacod1, AV181ExtraWWContagemResultadoExtraSelectionVs2DS_17_Contagemresultado_servicogrupo1, AV183ExtraWWContagemResultadoExtraSelectionVs2DS_19_Contagemresultado_naocnfdmncod1, AV186ExtraWWContagemResultadoExtraSelectionVs2DS_22_Contagemresultado_agrupador1, lV187ExtraWWContagemResultadoExtraSelectionVs2DS_23_Contagemresultado_descricao1, AV188ExtraWWContagemResultadoExtraSelectionVs2DS_24_Contagemresultado_owner1, AV190ExtraWWContagemResultadoExtraSelectionVs2DS_26_Contagemresultado_cntcod1, AV196ExtraWWContagemResultadoExtraSelectionVs2DS_32_Contagemresultado_statusdmn2, AV197ExtraWWContagemResultadoExtraSelectionVs2DS_33_Contagemresultado_osfsosfm2, AV197ExtraWWContagemResultadoExtraSelectionVs2DS_33_Contagemresultado_osfsosfm2, lV197ExtraWWContagemResultadoExtraSelectionVs2DS_33_Contagemresultado_osfsosfm2, lV197ExtraWWContagemResultadoExtraSelectionVs2DS_33_Contagemresultado_osfsosfm2, lV197ExtraWWContagemResultadoExtraSelectionVs2DS_33_Contagemresultado_osfsosfm2, lV197ExtraWWContagemResultadoExtraSelectionVs2DS_33_Contagemresultado_osfsosfm2, AV198ExtraWWContagemResultadoExtraSelectionVs2DS_34_Contagemresultado_datadmn2, AV199ExtraWWContagemResultadoExtraSelectionVs2DS_35_Contagemresultado_datadmn_to2, AV200ExtraWWContagemResultadoExtraSelectionVs2DS_36_Contagemresultado_dataprevista2, AV201ExtraWWContagemResultadoExtraSelectionVs2DS_37_Contagemresultado_dataprevista_to2, AV202ExtraWWContagemResultadoExtraSelectionVs2DS_38_Contagemresultado_servico2, AV204ExtraWWContagemResultadoExtraSelectionVs2DS_40_Contagemresultado_sistemacod2, AV205ExtraWWContagemResultadoExtraSelectionVs2DS_41_Contagemresultado_contratadacod2, AV206ExtraWWContagemResultadoExtraSelectionVs2DS_42_Contagemresultado_servicogrupo2,
         AV208ExtraWWContagemResultadoExtraSelectionVs2DS_44_Contagemresultado_naocnfdmncod2, AV211ExtraWWContagemResultadoExtraSelectionVs2DS_47_Contagemresultado_agrupador2, lV212ExtraWWContagemResultadoExtraSelectionVs2DS_48_Contagemresultado_descricao2, AV213ExtraWWContagemResultadoExtraSelectionVs2DS_49_Contagemresultado_owner2, AV215ExtraWWContagemResultadoExtraSelectionVs2DS_51_Contagemresultado_cntcod2, AV221ExtraWWContagemResultadoExtraSelectionVs2DS_57_Contagemresultado_statusdmn3, AV222ExtraWWContagemResultadoExtraSelectionVs2DS_58_Contagemresultado_osfsosfm3, AV222ExtraWWContagemResultadoExtraSelectionVs2DS_58_Contagemresultado_osfsosfm3, lV222ExtraWWContagemResultadoExtraSelectionVs2DS_58_Contagemresultado_osfsosfm3, lV222ExtraWWContagemResultadoExtraSelectionVs2DS_58_Contagemresultado_osfsosfm3, lV222ExtraWWContagemResultadoExtraSelectionVs2DS_58_Contagemresultado_osfsosfm3, lV222ExtraWWContagemResultadoExtraSelectionVs2DS_58_Contagemresultado_osfsosfm3, AV223ExtraWWContagemResultadoExtraSelectionVs2DS_59_Contagemresultado_datadmn3, AV224ExtraWWContagemResultadoExtraSelectionVs2DS_60_Contagemresultado_datadmn_to3, AV225ExtraWWContagemResultadoExtraSelectionVs2DS_61_Contagemresultado_dataprevista3, AV226ExtraWWContagemResultadoExtraSelectionVs2DS_62_Contagemresultado_dataprevista_to3, AV227ExtraWWContagemResultadoExtraSelectionVs2DS_63_Contagemresultado_servico3, AV229ExtraWWContagemResultadoExtraSelectionVs2DS_65_Contagemresultado_sistemacod3, AV230ExtraWWContagemResultadoExtraSelectionVs2DS_66_Contagemresultado_contratadacod3, AV231ExtraWWContagemResultadoExtraSelectionVs2DS_67_Contagemresultado_servicogrupo3, AV233ExtraWWContagemResultadoExtraSelectionVs2DS_69_Contagemresultado_naocnfdmncod3, AV236ExtraWWContagemResultadoExtraSelectionVs2DS_72_Contagemresultado_agrupador3, lV237ExtraWWContagemResultadoExtraSelectionVs2DS_73_Contagemresultado_descricao3, AV238ExtraWWContagemResultadoExtraSelectionVs2DS_74_Contagemresultado_owner3, AV240ExtraWWContagemResultadoExtraSelectionVs2DS_76_Contagemresultado_cntcod3, AV246ExtraWWContagemResultadoExtraSelectionVs2DS_82_Contagemresultado_statusdmn4, AV247ExtraWWContagemResultadoExtraSelectionVs2DS_83_Contagemresultado_osfsosfm4, AV247ExtraWWContagemResultadoExtraSelectionVs2DS_83_Contagemresultado_osfsosfm4, lV247ExtraWWContagemResultadoExtraSelectionVs2DS_83_Contagemresultado_osfsosfm4, lV247ExtraWWContagemResultadoExtraSelectionVs2DS_83_Contagemresultado_osfsosfm4, lV247ExtraWWContagemResultadoExtraSelectionVs2DS_83_Contagemresultado_osfsosfm4, lV247ExtraWWContagemResultadoExtraSelectionVs2DS_83_Contagemresultado_osfsosfm4, AV248ExtraWWContagemResultadoExtraSelectionVs2DS_84_Contagemresultado_datadmn4, AV249ExtraWWContagemResultadoExtraSelectionVs2DS_85_Contagemresultado_datadmn_to4, AV250ExtraWWContagemResultadoExtraSelectionVs2DS_86_Contagemresultado_dataprevista4, AV251ExtraWWContagemResultadoExtraSelectionVs2DS_87_Contagemresultado_dataprevista_to4, AV252ExtraWWContagemResultadoExtraSelectionVs2DS_88_Contagemresultado_servico4, AV254ExtraWWContagemResultadoExtraSelectionVs2DS_90_Contagemresultado_sistemacod4, AV255ExtraWWContagemResultadoExtraSelectionVs2DS_91_Contagemresultado_contratadacod4, AV256ExtraWWContagemResultadoExtraSelectionVs2DS_92_Contagemresultado_servicogrupo4, AV258ExtraWWContagemResultadoExtraSelectionVs2DS_94_Contagemresultado_naocnfdmncod4, AV261ExtraWWContagemResultadoExtraSelectionVs2DS_97_Contagemresultado_agrupador4, lV262ExtraWWContagemResultadoExtraSelectionVs2DS_98_Contagemresultado_descricao4, AV263ExtraWWContagemResultadoExtraSelectionVs2DS_99_Contagemresultado_owner4, AV265ExtraWWContagemResultadoExtraSelectionVs2DS_101_Contagemresultado_cntcod4, AV271ExtraWWContagemResultadoExtraSelectionVs2DS_107_Contagemresultado_statusdmn5, AV272ExtraWWContagemResultadoExtraSelectionVs2DS_108_Contagemresultado_osfsosfm5, AV272ExtraWWContagemResultadoExtraSelectionVs2DS_108_Contagemresultado_osfsosfm5, lV272ExtraWWContagemResultadoExtraSelectionVs2DS_108_Contagemresultado_osfsosfm5, lV272ExtraWWContagemResultadoExtraSelectionVs2DS_108_Contagemresultado_osfsosfm5, lV272ExtraWWContagemResultadoExtraSelectionVs2DS_108_Contagemresultado_osfsosfm5, lV272ExtraWWContagemResultadoExtraSelectionVs2DS_108_Contagemresultado_osfsosfm5, AV273ExtraWWContagemResultadoExtraSelectionVs2DS_109_Contagemresultado_datadmn5, AV274ExtraWWContagemResultadoExtraSelectionVs2DS_110_Contagemresultado_datadmn_to5, AV275ExtraWWContagemResultadoExtraSelectionVs2DS_111_Contagemresultado_dataprevista5, AV276ExtraWWContagemResultadoExtraSelectionVs2DS_112_Contagemresultado_dataprevista_to5, AV277ExtraWWContagemResultadoExtraSelectionVs2DS_113_Contagemresultado_servico5, AV279ExtraWWContagemResultadoExtraSelectionVs2DS_115_Contagemresultado_sistemacod5, AV280ExtraWWContagemResultadoExtraSelectionVs2DS_116_Contagemresultado_contratadacod5, AV281ExtraWWContagemResultadoExtraSelectionVs2DS_117_Contagemresultado_servicogrupo5, AV283ExtraWWContagemResultadoExtraSelectionVs2DS_119_Contagemresultado_naocnfdmncod5, AV286ExtraWWContagemResultadoExtraSelectionVs2DS_122_Contagemresultado_agrupador5, lV287ExtraWWContagemResultadoExtraSelectionVs2DS_123_Contagemresultado_descricao5, AV288ExtraWWContagemResultadoExtraSelectionVs2DS_124_Contagemresultado_owner5, AV290ExtraWWContagemResultadoExtraSelectionVs2DS_126_Contagemresultado_cntcod5, lV292ExtraWWContagemResultadoExtraSelectionVs2DS_128_Tfcontagemresultado_servicosigla, AV293ExtraWWContagemResultadoExtraSelectionVs2DS_129_Tfcontagemresultado_servicosigla_sel, AV296ExtraWWContagemResultadoExtraSelectionVs2DS_132_Tfcontagemresultado_valorpf, AV297ExtraWWContagemResultadoExtraSelectionVs2DS_133_Tfcontagemresultado_valorpf_to});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKYD2 = false;
            A1553ContagemResultado_CntSrvCod = P00YD3_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00YD3_n1553ContagemResultado_CntSrvCod[0];
            A52Contratada_AreaTrabalhoCod = P00YD3_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00YD3_n52Contratada_AreaTrabalhoCod[0];
            A801ContagemResultado_ServicoSigla = P00YD3_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00YD3_n801ContagemResultado_ServicoSigla[0];
            A512ContagemResultado_ValorPF = P00YD3_A512ContagemResultado_ValorPF[0];
            n512ContagemResultado_ValorPF = P00YD3_n512ContagemResultado_ValorPF[0];
            A1603ContagemResultado_CntCod = P00YD3_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P00YD3_n1603ContagemResultado_CntCod[0];
            A494ContagemResultado_Descricao = P00YD3_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = P00YD3_n494ContagemResultado_Descricao[0];
            A1046ContagemResultado_Agrupador = P00YD3_A1046ContagemResultado_Agrupador[0];
            n1046ContagemResultado_Agrupador = P00YD3_n1046ContagemResultado_Agrupador[0];
            A468ContagemResultado_NaoCnfDmnCod = P00YD3_A468ContagemResultado_NaoCnfDmnCod[0];
            n468ContagemResultado_NaoCnfDmnCod = P00YD3_n468ContagemResultado_NaoCnfDmnCod[0];
            A598ContagemResultado_Baseline = P00YD3_A598ContagemResultado_Baseline[0];
            n598ContagemResultado_Baseline = P00YD3_n598ContagemResultado_Baseline[0];
            A764ContagemResultado_ServicoGrupo = P00YD3_A764ContagemResultado_ServicoGrupo[0];
            n764ContagemResultado_ServicoGrupo = P00YD3_n764ContagemResultado_ServicoGrupo[0];
            A489ContagemResultado_SistemaCod = P00YD3_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00YD3_n489ContagemResultado_SistemaCod[0];
            A508ContagemResultado_Owner = P00YD3_A508ContagemResultado_Owner[0];
            A601ContagemResultado_Servico = P00YD3_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00YD3_n601ContagemResultado_Servico[0];
            A1351ContagemResultado_DataPrevista = P00YD3_A1351ContagemResultado_DataPrevista[0];
            n1351ContagemResultado_DataPrevista = P00YD3_n1351ContagemResultado_DataPrevista[0];
            A471ContagemResultado_DataDmn = P00YD3_A471ContagemResultado_DataDmn[0];
            A493ContagemResultado_DemandaFM = P00YD3_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00YD3_n493ContagemResultado_DemandaFM[0];
            A457ContagemResultado_Demanda = P00YD3_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00YD3_n457ContagemResultado_Demanda[0];
            A484ContagemResultado_StatusDmn = P00YD3_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00YD3_n484ContagemResultado_StatusDmn[0];
            A1854ContagemResultado_VlrCnc = P00YD3_A1854ContagemResultado_VlrCnc[0];
            n1854ContagemResultado_VlrCnc = P00YD3_n1854ContagemResultado_VlrCnc[0];
            A490ContagemResultado_ContratadaCod = P00YD3_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00YD3_n490ContagemResultado_ContratadaCod[0];
            A683ContagemResultado_PFLFMUltima = P00YD3_A683ContagemResultado_PFLFMUltima[0];
            A685ContagemResultado_PFLFSUltima = P00YD3_A685ContagemResultado_PFLFSUltima[0];
            A682ContagemResultado_PFBFMUltima = P00YD3_A682ContagemResultado_PFBFMUltima[0];
            A684ContagemResultado_PFBFSUltima = P00YD3_A684ContagemResultado_PFBFSUltima[0];
            A584ContagemResultado_ContadorFM = P00YD3_A584ContagemResultado_ContadorFM[0];
            A566ContagemResultado_DataUltCnt = P00YD3_A566ContagemResultado_DataUltCnt[0];
            A531ContagemResultado_StatusUltCnt = P00YD3_A531ContagemResultado_StatusUltCnt[0];
            A456ContagemResultado_Codigo = P00YD3_A456ContagemResultado_Codigo[0];
            A1603ContagemResultado_CntCod = P00YD3_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P00YD3_n1603ContagemResultado_CntCod[0];
            A601ContagemResultado_Servico = P00YD3_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00YD3_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = P00YD3_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00YD3_n801ContagemResultado_ServicoSigla[0];
            A764ContagemResultado_ServicoGrupo = P00YD3_A764ContagemResultado_ServicoGrupo[0];
            n764ContagemResultado_ServicoGrupo = P00YD3_n764ContagemResultado_ServicoGrupo[0];
            A52Contratada_AreaTrabalhoCod = P00YD3_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00YD3_n52Contratada_AreaTrabalhoCod[0];
            A683ContagemResultado_PFLFMUltima = P00YD3_A683ContagemResultado_PFLFMUltima[0];
            A685ContagemResultado_PFLFSUltima = P00YD3_A685ContagemResultado_PFLFSUltima[0];
            A682ContagemResultado_PFBFMUltima = P00YD3_A682ContagemResultado_PFBFMUltima[0];
            A684ContagemResultado_PFBFSUltima = P00YD3_A684ContagemResultado_PFBFSUltima[0];
            A584ContagemResultado_ContadorFM = P00YD3_A584ContagemResultado_ContadorFM[0];
            A566ContagemResultado_DataUltCnt = P00YD3_A566ContagemResultado_DataUltCnt[0];
            A531ContagemResultado_StatusUltCnt = P00YD3_A531ContagemResultado_StatusUltCnt[0];
            GXt_boolean1 = A1802ContagemResultado_TemPndHmlg;
            new prc_tempndparahomologar(context ).execute( ref  A456ContagemResultado_Codigo, out  GXt_boolean1) ;
            A1802ContagemResultado_TemPndHmlg = GXt_boolean1;
            if ( ! ( ( StringUtil.StrCmp(AV167ExtraWWContagemResultadoExtraSelectionVs2DS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV189ExtraWWContagemResultadoExtraSelectionVs2DS_25_Contagemresultado_tempndhmlg1, "C") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && A1802ContagemResultado_TemPndHmlg ) )
            {
               if ( ! ( ( StringUtil.StrCmp(AV167ExtraWWContagemResultadoExtraSelectionVs2DS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV189ExtraWWContagemResultadoExtraSelectionVs2DS_25_Contagemresultado_tempndhmlg1, "S") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && ( (false==A1802ContagemResultado_TemPndHmlg) || ! A1802ContagemResultado_TemPndHmlg ) ) )
               {
                  if ( ! ( AV191ExtraWWContagemResultadoExtraSelectionVs2DS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV192ExtraWWContagemResultadoExtraSelectionVs2DS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV214ExtraWWContagemResultadoExtraSelectionVs2DS_50_Contagemresultado_tempndhmlg2, "C") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && A1802ContagemResultado_TemPndHmlg ) )
                  {
                     if ( ! ( AV191ExtraWWContagemResultadoExtraSelectionVs2DS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV192ExtraWWContagemResultadoExtraSelectionVs2DS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV214ExtraWWContagemResultadoExtraSelectionVs2DS_50_Contagemresultado_tempndhmlg2, "S") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && ( (false==A1802ContagemResultado_TemPndHmlg) || ! A1802ContagemResultado_TemPndHmlg ) ) )
                     {
                        if ( ! ( AV216ExtraWWContagemResultadoExtraSelectionVs2DS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV217ExtraWWContagemResultadoExtraSelectionVs2DS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV239ExtraWWContagemResultadoExtraSelectionVs2DS_75_Contagemresultado_tempndhmlg3, "C") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && A1802ContagemResultado_TemPndHmlg ) )
                        {
                           if ( ! ( AV216ExtraWWContagemResultadoExtraSelectionVs2DS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV217ExtraWWContagemResultadoExtraSelectionVs2DS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV239ExtraWWContagemResultadoExtraSelectionVs2DS_75_Contagemresultado_tempndhmlg3, "S") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && ( (false==A1802ContagemResultado_TemPndHmlg) || ! A1802ContagemResultado_TemPndHmlg ) ) )
                           {
                              if ( ! ( AV241ExtraWWContagemResultadoExtraSelectionVs2DS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV242ExtraWWContagemResultadoExtraSelectionVs2DS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV264ExtraWWContagemResultadoExtraSelectionVs2DS_100_Contagemresultado_tempndhmlg4, "C") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && A1802ContagemResultado_TemPndHmlg ) )
                              {
                                 if ( ! ( AV241ExtraWWContagemResultadoExtraSelectionVs2DS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV242ExtraWWContagemResultadoExtraSelectionVs2DS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV264ExtraWWContagemResultadoExtraSelectionVs2DS_100_Contagemresultado_tempndhmlg4, "S") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && ( (false==A1802ContagemResultado_TemPndHmlg) || ! A1802ContagemResultado_TemPndHmlg ) ) )
                                 {
                                    if ( ! ( AV266ExtraWWContagemResultadoExtraSelectionVs2DS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV267ExtraWWContagemResultadoExtraSelectionVs2DS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV289ExtraWWContagemResultadoExtraSelectionVs2DS_125_Contagemresultado_tempndhmlg5, "C") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && A1802ContagemResultado_TemPndHmlg ) )
                                    {
                                       if ( ! ( AV266ExtraWWContagemResultadoExtraSelectionVs2DS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV267ExtraWWContagemResultadoExtraSelectionVs2DS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_TEMPNDHMLG") == 0 ) && ( ( StringUtil.StrCmp(AV289ExtraWWContagemResultadoExtraSelectionVs2DS_125_Contagemresultado_tempndhmlg5, "S") == 0 ) ) ) || ( ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) ) && ( (false==A1802ContagemResultado_TemPndHmlg) || ! A1802ContagemResultado_TemPndHmlg ) ) )
                                       {
                                          GXt_decimal2 = A574ContagemResultado_PFFinal;
                                          new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal2) ;
                                          A574ContagemResultado_PFFinal = GXt_decimal2;
                                          if ( (Convert.ToDecimal(0)==AV294ExtraWWContagemResultadoExtraSelectionVs2DS_130_Tfcontagemresultado_pffinal) || ( ( A574ContagemResultado_PFFinal >= AV294ExtraWWContagemResultadoExtraSelectionVs2DS_130_Tfcontagemresultado_pffinal ) ) )
                                          {
                                             if ( (Convert.ToDecimal(0)==AV295ExtraWWContagemResultadoExtraSelectionVs2DS_131_Tfcontagemresultado_pffinal_to) || ( ( A574ContagemResultado_PFFinal <= AV295ExtraWWContagemResultadoExtraSelectionVs2DS_131_Tfcontagemresultado_pffinal_to ) ) )
                                             {
                                                AV29count = 0;
                                                while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00YD3_A801ContagemResultado_ServicoSigla[0], A801ContagemResultado_ServicoSigla) == 0 ) )
                                                {
                                                   BRKYD2 = false;
                                                   A1553ContagemResultado_CntSrvCod = P00YD3_A1553ContagemResultado_CntSrvCod[0];
                                                   n1553ContagemResultado_CntSrvCod = P00YD3_n1553ContagemResultado_CntSrvCod[0];
                                                   A601ContagemResultado_Servico = P00YD3_A601ContagemResultado_Servico[0];
                                                   n601ContagemResultado_Servico = P00YD3_n601ContagemResultado_Servico[0];
                                                   A456ContagemResultado_Codigo = P00YD3_A456ContagemResultado_Codigo[0];
                                                   A601ContagemResultado_Servico = P00YD3_A601ContagemResultado_Servico[0];
                                                   n601ContagemResultado_Servico = P00YD3_n601ContagemResultado_Servico[0];
                                                   AV29count = (long)(AV29count+1);
                                                   BRKYD2 = true;
                                                   pr_default.readNext(0);
                                                }
                                                if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A801ContagemResultado_ServicoSigla)) )
                                                {
                                                   AV21Option = A801ContagemResultado_ServicoSigla;
                                                   AV22Options.Add(AV21Option, 0);
                                                   AV27OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV29count), "Z,ZZZ,ZZZ,ZZ9")), 0);
                                                }
                                                if ( AV22Options.Count == 50 )
                                                {
                                                   /* Exit For each command. Update data (if necessary), close cursors & exit. */
                                                   if (true) break;
                                                }
                                             }
                                          }
                                       }
                                    }
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
            if ( ! BRKYD2 )
            {
               BRKYD2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV22Options = new GxSimpleCollection();
         AV25OptionsDesc = new GxSimpleCollection();
         AV27OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV30Session = context.GetSession();
         AV32GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV33GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV11TFContagemResultado_ServicoSigla = "";
         AV12TFContagemResultado_ServicoSigla_Sel = "";
         AV34GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV37DynamicFiltersSelector1 = "";
         AV39ContagemResultado_DataCnt1 = DateTime.MinValue;
         AV40ContagemResultado_DataCnt_To1 = DateTime.MinValue;
         AV41ContagemResultado_StatusDmn1 = "";
         AV42ContagemResultado_OsFsOsFm1 = "";
         AV43ContagemResultado_DataDmn1 = DateTime.MinValue;
         AV44ContagemResultado_DataDmn_To1 = DateTime.MinValue;
         AV45ContagemResultado_DataPrevista1 = DateTime.MinValue;
         AV46ContagemResultado_DataPrevista_To1 = DateTime.MinValue;
         AV52ContagemResultado_Baseline1 = "";
         AV56ContagemResultado_Agrupador1 = "";
         AV57ContagemResultado_Descricao1 = "";
         AV59ContagemResultado_TemPndHmlg1 = "";
         AV62DynamicFiltersSelector2 = "";
         AV64ContagemResultado_DataCnt2 = DateTime.MinValue;
         AV65ContagemResultado_DataCnt_To2 = DateTime.MinValue;
         AV66ContagemResultado_StatusDmn2 = "";
         AV67ContagemResultado_OsFsOsFm2 = "";
         AV68ContagemResultado_DataDmn2 = DateTime.MinValue;
         AV69ContagemResultado_DataDmn_To2 = DateTime.MinValue;
         AV70ContagemResultado_DataPrevista2 = DateTime.MinValue;
         AV71ContagemResultado_DataPrevista_To2 = DateTime.MinValue;
         AV77ContagemResultado_Baseline2 = "";
         AV81ContagemResultado_Agrupador2 = "";
         AV82ContagemResultado_Descricao2 = "";
         AV84ContagemResultado_TemPndHmlg2 = "";
         AV87DynamicFiltersSelector3 = "";
         AV89ContagemResultado_DataCnt3 = DateTime.MinValue;
         AV90ContagemResultado_DataCnt_To3 = DateTime.MinValue;
         AV91ContagemResultado_StatusDmn3 = "";
         AV92ContagemResultado_OsFsOsFm3 = "";
         AV93ContagemResultado_DataDmn3 = DateTime.MinValue;
         AV94ContagemResultado_DataDmn_To3 = DateTime.MinValue;
         AV95ContagemResultado_DataPrevista3 = DateTime.MinValue;
         AV96ContagemResultado_DataPrevista_To3 = DateTime.MinValue;
         AV102ContagemResultado_Baseline3 = "";
         AV106ContagemResultado_Agrupador3 = "";
         AV107ContagemResultado_Descricao3 = "";
         AV109ContagemResultado_TemPndHmlg3 = "";
         AV112DynamicFiltersSelector4 = "";
         AV114ContagemResultado_DataCnt4 = DateTime.MinValue;
         AV115ContagemResultado_DataCnt_To4 = DateTime.MinValue;
         AV116ContagemResultado_StatusDmn4 = "";
         AV117ContagemResultado_OsFsOsFm4 = "";
         AV118ContagemResultado_DataDmn4 = DateTime.MinValue;
         AV119ContagemResultado_DataDmn_To4 = DateTime.MinValue;
         AV120ContagemResultado_DataPrevista4 = DateTime.MinValue;
         AV121ContagemResultado_DataPrevista_To4 = DateTime.MinValue;
         AV127ContagemResultado_Baseline4 = "";
         AV131ContagemResultado_Agrupador4 = "";
         AV132ContagemResultado_Descricao4 = "";
         AV134ContagemResultado_TemPndHmlg4 = "";
         AV137DynamicFiltersSelector5 = "";
         AV139ContagemResultado_DataCnt5 = DateTime.MinValue;
         AV140ContagemResultado_DataCnt_To5 = DateTime.MinValue;
         AV141ContagemResultado_StatusDmn5 = "";
         AV142ContagemResultado_OsFsOsFm5 = "";
         AV143ContagemResultado_DataDmn5 = DateTime.MinValue;
         AV144ContagemResultado_DataDmn_To5 = DateTime.MinValue;
         AV145ContagemResultado_DataPrevista5 = DateTime.MinValue;
         AV146ContagemResultado_DataPrevista_To5 = DateTime.MinValue;
         AV152ContagemResultado_Baseline5 = "";
         AV156ContagemResultado_Agrupador5 = "";
         AV157ContagemResultado_Descricao5 = "";
         AV159ContagemResultado_TemPndHmlg5 = "";
         AV167ExtraWWContagemResultadoExtraSelectionVs2DS_3_Dynamicfiltersselector1 = "";
         AV169ExtraWWContagemResultadoExtraSelectionVs2DS_5_Contagemresultado_datacnt1 = DateTime.MinValue;
         AV170ExtraWWContagemResultadoExtraSelectionVs2DS_6_Contagemresultado_datacnt_to1 = DateTime.MinValue;
         AV171ExtraWWContagemResultadoExtraSelectionVs2DS_7_Contagemresultado_statusdmn1 = "";
         AV172ExtraWWContagemResultadoExtraSelectionVs2DS_8_Contagemresultado_osfsosfm1 = "";
         AV173ExtraWWContagemResultadoExtraSelectionVs2DS_9_Contagemresultado_datadmn1 = DateTime.MinValue;
         AV174ExtraWWContagemResultadoExtraSelectionVs2DS_10_Contagemresultado_datadmn_to1 = DateTime.MinValue;
         AV175ExtraWWContagemResultadoExtraSelectionVs2DS_11_Contagemresultado_dataprevista1 = DateTime.MinValue;
         AV176ExtraWWContagemResultadoExtraSelectionVs2DS_12_Contagemresultado_dataprevista_to1 = DateTime.MinValue;
         AV182ExtraWWContagemResultadoExtraSelectionVs2DS_18_Contagemresultado_baseline1 = "";
         AV186ExtraWWContagemResultadoExtraSelectionVs2DS_22_Contagemresultado_agrupador1 = "";
         AV187ExtraWWContagemResultadoExtraSelectionVs2DS_23_Contagemresultado_descricao1 = "";
         AV189ExtraWWContagemResultadoExtraSelectionVs2DS_25_Contagemresultado_tempndhmlg1 = "";
         AV192ExtraWWContagemResultadoExtraSelectionVs2DS_28_Dynamicfiltersselector2 = "";
         AV194ExtraWWContagemResultadoExtraSelectionVs2DS_30_Contagemresultado_datacnt2 = DateTime.MinValue;
         AV195ExtraWWContagemResultadoExtraSelectionVs2DS_31_Contagemresultado_datacnt_to2 = DateTime.MinValue;
         AV196ExtraWWContagemResultadoExtraSelectionVs2DS_32_Contagemresultado_statusdmn2 = "";
         AV197ExtraWWContagemResultadoExtraSelectionVs2DS_33_Contagemresultado_osfsosfm2 = "";
         AV198ExtraWWContagemResultadoExtraSelectionVs2DS_34_Contagemresultado_datadmn2 = DateTime.MinValue;
         AV199ExtraWWContagemResultadoExtraSelectionVs2DS_35_Contagemresultado_datadmn_to2 = DateTime.MinValue;
         AV200ExtraWWContagemResultadoExtraSelectionVs2DS_36_Contagemresultado_dataprevista2 = DateTime.MinValue;
         AV201ExtraWWContagemResultadoExtraSelectionVs2DS_37_Contagemresultado_dataprevista_to2 = DateTime.MinValue;
         AV207ExtraWWContagemResultadoExtraSelectionVs2DS_43_Contagemresultado_baseline2 = "";
         AV211ExtraWWContagemResultadoExtraSelectionVs2DS_47_Contagemresultado_agrupador2 = "";
         AV212ExtraWWContagemResultadoExtraSelectionVs2DS_48_Contagemresultado_descricao2 = "";
         AV214ExtraWWContagemResultadoExtraSelectionVs2DS_50_Contagemresultado_tempndhmlg2 = "";
         AV217ExtraWWContagemResultadoExtraSelectionVs2DS_53_Dynamicfiltersselector3 = "";
         AV219ExtraWWContagemResultadoExtraSelectionVs2DS_55_Contagemresultado_datacnt3 = DateTime.MinValue;
         AV220ExtraWWContagemResultadoExtraSelectionVs2DS_56_Contagemresultado_datacnt_to3 = DateTime.MinValue;
         AV221ExtraWWContagemResultadoExtraSelectionVs2DS_57_Contagemresultado_statusdmn3 = "";
         AV222ExtraWWContagemResultadoExtraSelectionVs2DS_58_Contagemresultado_osfsosfm3 = "";
         AV223ExtraWWContagemResultadoExtraSelectionVs2DS_59_Contagemresultado_datadmn3 = DateTime.MinValue;
         AV224ExtraWWContagemResultadoExtraSelectionVs2DS_60_Contagemresultado_datadmn_to3 = DateTime.MinValue;
         AV225ExtraWWContagemResultadoExtraSelectionVs2DS_61_Contagemresultado_dataprevista3 = DateTime.MinValue;
         AV226ExtraWWContagemResultadoExtraSelectionVs2DS_62_Contagemresultado_dataprevista_to3 = DateTime.MinValue;
         AV232ExtraWWContagemResultadoExtraSelectionVs2DS_68_Contagemresultado_baseline3 = "";
         AV236ExtraWWContagemResultadoExtraSelectionVs2DS_72_Contagemresultado_agrupador3 = "";
         AV237ExtraWWContagemResultadoExtraSelectionVs2DS_73_Contagemresultado_descricao3 = "";
         AV239ExtraWWContagemResultadoExtraSelectionVs2DS_75_Contagemresultado_tempndhmlg3 = "";
         AV242ExtraWWContagemResultadoExtraSelectionVs2DS_78_Dynamicfiltersselector4 = "";
         AV244ExtraWWContagemResultadoExtraSelectionVs2DS_80_Contagemresultado_datacnt4 = DateTime.MinValue;
         AV245ExtraWWContagemResultadoExtraSelectionVs2DS_81_Contagemresultado_datacnt_to4 = DateTime.MinValue;
         AV246ExtraWWContagemResultadoExtraSelectionVs2DS_82_Contagemresultado_statusdmn4 = "";
         AV247ExtraWWContagemResultadoExtraSelectionVs2DS_83_Contagemresultado_osfsosfm4 = "";
         AV248ExtraWWContagemResultadoExtraSelectionVs2DS_84_Contagemresultado_datadmn4 = DateTime.MinValue;
         AV249ExtraWWContagemResultadoExtraSelectionVs2DS_85_Contagemresultado_datadmn_to4 = DateTime.MinValue;
         AV250ExtraWWContagemResultadoExtraSelectionVs2DS_86_Contagemresultado_dataprevista4 = DateTime.MinValue;
         AV251ExtraWWContagemResultadoExtraSelectionVs2DS_87_Contagemresultado_dataprevista_to4 = DateTime.MinValue;
         AV257ExtraWWContagemResultadoExtraSelectionVs2DS_93_Contagemresultado_baseline4 = "";
         AV261ExtraWWContagemResultadoExtraSelectionVs2DS_97_Contagemresultado_agrupador4 = "";
         AV262ExtraWWContagemResultadoExtraSelectionVs2DS_98_Contagemresultado_descricao4 = "";
         AV264ExtraWWContagemResultadoExtraSelectionVs2DS_100_Contagemresultado_tempndhmlg4 = "";
         AV267ExtraWWContagemResultadoExtraSelectionVs2DS_103_Dynamicfiltersselector5 = "";
         AV269ExtraWWContagemResultadoExtraSelectionVs2DS_105_Contagemresultado_datacnt5 = DateTime.MinValue;
         AV270ExtraWWContagemResultadoExtraSelectionVs2DS_106_Contagemresultado_datacnt_to5 = DateTime.MinValue;
         AV271ExtraWWContagemResultadoExtraSelectionVs2DS_107_Contagemresultado_statusdmn5 = "";
         AV272ExtraWWContagemResultadoExtraSelectionVs2DS_108_Contagemresultado_osfsosfm5 = "";
         AV273ExtraWWContagemResultadoExtraSelectionVs2DS_109_Contagemresultado_datadmn5 = DateTime.MinValue;
         AV274ExtraWWContagemResultadoExtraSelectionVs2DS_110_Contagemresultado_datadmn_to5 = DateTime.MinValue;
         AV275ExtraWWContagemResultadoExtraSelectionVs2DS_111_Contagemresultado_dataprevista5 = DateTime.MinValue;
         AV276ExtraWWContagemResultadoExtraSelectionVs2DS_112_Contagemresultado_dataprevista_to5 = DateTime.MinValue;
         AV282ExtraWWContagemResultadoExtraSelectionVs2DS_118_Contagemresultado_baseline5 = "";
         AV286ExtraWWContagemResultadoExtraSelectionVs2DS_122_Contagemresultado_agrupador5 = "";
         AV287ExtraWWContagemResultadoExtraSelectionVs2DS_123_Contagemresultado_descricao5 = "";
         AV289ExtraWWContagemResultadoExtraSelectionVs2DS_125_Contagemresultado_tempndhmlg5 = "";
         AV292ExtraWWContagemResultadoExtraSelectionVs2DS_128_Tfcontagemresultado_servicosigla = "";
         AV293ExtraWWContagemResultadoExtraSelectionVs2DS_129_Tfcontagemresultado_servicosigla_sel = "";
         scmdbuf = "";
         lV172ExtraWWContagemResultadoExtraSelectionVs2DS_8_Contagemresultado_osfsosfm1 = "";
         lV187ExtraWWContagemResultadoExtraSelectionVs2DS_23_Contagemresultado_descricao1 = "";
         lV197ExtraWWContagemResultadoExtraSelectionVs2DS_33_Contagemresultado_osfsosfm2 = "";
         lV212ExtraWWContagemResultadoExtraSelectionVs2DS_48_Contagemresultado_descricao2 = "";
         lV222ExtraWWContagemResultadoExtraSelectionVs2DS_58_Contagemresultado_osfsosfm3 = "";
         lV237ExtraWWContagemResultadoExtraSelectionVs2DS_73_Contagemresultado_descricao3 = "";
         lV247ExtraWWContagemResultadoExtraSelectionVs2DS_83_Contagemresultado_osfsosfm4 = "";
         lV262ExtraWWContagemResultadoExtraSelectionVs2DS_98_Contagemresultado_descricao4 = "";
         lV272ExtraWWContagemResultadoExtraSelectionVs2DS_108_Contagemresultado_osfsosfm5 = "";
         lV287ExtraWWContagemResultadoExtraSelectionVs2DS_123_Contagemresultado_descricao5 = "";
         lV292ExtraWWContagemResultadoExtraSelectionVs2DS_128_Tfcontagemresultado_servicosigla = "";
         A484ContagemResultado_StatusDmn = "";
         A457ContagemResultado_Demanda = "";
         A493ContagemResultado_DemandaFM = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A1351ContagemResultado_DataPrevista = (DateTime)(DateTime.MinValue);
         A1046ContagemResultado_Agrupador = "";
         A494ContagemResultado_Descricao = "";
         A801ContagemResultado_ServicoSigla = "";
         A566ContagemResultado_DataUltCnt = DateTime.MinValue;
         P00YD3_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00YD3_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00YD3_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00YD3_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00YD3_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P00YD3_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P00YD3_A512ContagemResultado_ValorPF = new decimal[1] ;
         P00YD3_n512ContagemResultado_ValorPF = new bool[] {false} ;
         P00YD3_A1603ContagemResultado_CntCod = new int[1] ;
         P00YD3_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P00YD3_A494ContagemResultado_Descricao = new String[] {""} ;
         P00YD3_n494ContagemResultado_Descricao = new bool[] {false} ;
         P00YD3_A1046ContagemResultado_Agrupador = new String[] {""} ;
         P00YD3_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         P00YD3_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         P00YD3_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         P00YD3_A598ContagemResultado_Baseline = new bool[] {false} ;
         P00YD3_n598ContagemResultado_Baseline = new bool[] {false} ;
         P00YD3_A764ContagemResultado_ServicoGrupo = new int[1] ;
         P00YD3_n764ContagemResultado_ServicoGrupo = new bool[] {false} ;
         P00YD3_A489ContagemResultado_SistemaCod = new int[1] ;
         P00YD3_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00YD3_A508ContagemResultado_Owner = new int[1] ;
         P00YD3_A601ContagemResultado_Servico = new int[1] ;
         P00YD3_n601ContagemResultado_Servico = new bool[] {false} ;
         P00YD3_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P00YD3_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         P00YD3_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00YD3_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00YD3_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00YD3_A457ContagemResultado_Demanda = new String[] {""} ;
         P00YD3_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00YD3_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00YD3_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00YD3_A1854ContagemResultado_VlrCnc = new decimal[1] ;
         P00YD3_n1854ContagemResultado_VlrCnc = new bool[] {false} ;
         P00YD3_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00YD3_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00YD3_A683ContagemResultado_PFLFMUltima = new decimal[1] ;
         P00YD3_A685ContagemResultado_PFLFSUltima = new decimal[1] ;
         P00YD3_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         P00YD3_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         P00YD3_A584ContagemResultado_ContadorFM = new int[1] ;
         P00YD3_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         P00YD3_A531ContagemResultado_StatusUltCnt = new short[1] ;
         P00YD3_A456ContagemResultado_Codigo = new int[1] ;
         AV21Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getextrawwcontagemresultadoextraselectionvs2filterdata__default(),
            new Object[][] {
                new Object[] {
               P00YD3_A1553ContagemResultado_CntSrvCod, P00YD3_n1553ContagemResultado_CntSrvCod, P00YD3_A52Contratada_AreaTrabalhoCod, P00YD3_n52Contratada_AreaTrabalhoCod, P00YD3_A801ContagemResultado_ServicoSigla, P00YD3_n801ContagemResultado_ServicoSigla, P00YD3_A512ContagemResultado_ValorPF, P00YD3_n512ContagemResultado_ValorPF, P00YD3_A1603ContagemResultado_CntCod, P00YD3_n1603ContagemResultado_CntCod,
               P00YD3_A494ContagemResultado_Descricao, P00YD3_n494ContagemResultado_Descricao, P00YD3_A1046ContagemResultado_Agrupador, P00YD3_n1046ContagemResultado_Agrupador, P00YD3_A468ContagemResultado_NaoCnfDmnCod, P00YD3_n468ContagemResultado_NaoCnfDmnCod, P00YD3_A598ContagemResultado_Baseline, P00YD3_n598ContagemResultado_Baseline, P00YD3_A764ContagemResultado_ServicoGrupo, P00YD3_n764ContagemResultado_ServicoGrupo,
               P00YD3_A489ContagemResultado_SistemaCod, P00YD3_n489ContagemResultado_SistemaCod, P00YD3_A508ContagemResultado_Owner, P00YD3_A601ContagemResultado_Servico, P00YD3_n601ContagemResultado_Servico, P00YD3_A1351ContagemResultado_DataPrevista, P00YD3_n1351ContagemResultado_DataPrevista, P00YD3_A471ContagemResultado_DataDmn, P00YD3_A493ContagemResultado_DemandaFM, P00YD3_n493ContagemResultado_DemandaFM,
               P00YD3_A457ContagemResultado_Demanda, P00YD3_n457ContagemResultado_Demanda, P00YD3_A484ContagemResultado_StatusDmn, P00YD3_n484ContagemResultado_StatusDmn, P00YD3_A1854ContagemResultado_VlrCnc, P00YD3_n1854ContagemResultado_VlrCnc, P00YD3_A490ContagemResultado_ContratadaCod, P00YD3_n490ContagemResultado_ContratadaCod, P00YD3_A683ContagemResultado_PFLFMUltima, P00YD3_A685ContagemResultado_PFLFSUltima,
               P00YD3_A682ContagemResultado_PFBFMUltima, P00YD3_A684ContagemResultado_PFBFSUltima, P00YD3_A584ContagemResultado_ContadorFM, P00YD3_A566ContagemResultado_DataUltCnt, P00YD3_A531ContagemResultado_StatusUltCnt, P00YD3_A456ContagemResultado_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV36ContagemResultado_StatusCnt ;
      private short AV10TFContagemResultado_Baseline_Sel ;
      private short AV38DynamicFiltersOperator1 ;
      private short AV63DynamicFiltersOperator2 ;
      private short AV88DynamicFiltersOperator3 ;
      private short AV113DynamicFiltersOperator4 ;
      private short AV138DynamicFiltersOperator5 ;
      private short AV166ExtraWWContagemResultadoExtraSelectionVs2DS_2_Contagemresultado_statuscnt ;
      private short AV168ExtraWWContagemResultadoExtraSelectionVs2DS_4_Dynamicfiltersoperator1 ;
      private short AV193ExtraWWContagemResultadoExtraSelectionVs2DS_29_Dynamicfiltersoperator2 ;
      private short AV218ExtraWWContagemResultadoExtraSelectionVs2DS_54_Dynamicfiltersoperator3 ;
      private short AV243ExtraWWContagemResultadoExtraSelectionVs2DS_79_Dynamicfiltersoperator4 ;
      private short AV268ExtraWWContagemResultadoExtraSelectionVs2DS_104_Dynamicfiltersoperator5 ;
      private short AV291ExtraWWContagemResultadoExtraSelectionVs2DS_127_Tfcontagemresultado_baseline_sel ;
      private short A531ContagemResultado_StatusUltCnt ;
      private int AV163GXV1 ;
      private int AV35Contratada_AreaTrabalhoCod ;
      private int AV47ContagemResultado_Servico1 ;
      private int AV48ContagemResultado_ContadorFM1 ;
      private int AV49ContagemResultado_SistemaCod1 ;
      private int AV50ContagemResultado_ContratadaCod1 ;
      private int AV51ContagemResultado_ServicoGrupo1 ;
      private int AV53ContagemResultado_NaoCnfDmnCod1 ;
      private int AV58ContagemResultado_Owner1 ;
      private int AV60ContagemResultado_CntCod1 ;
      private int AV72ContagemResultado_Servico2 ;
      private int AV73ContagemResultado_ContadorFM2 ;
      private int AV74ContagemResultado_SistemaCod2 ;
      private int AV75ContagemResultado_ContratadaCod2 ;
      private int AV76ContagemResultado_ServicoGrupo2 ;
      private int AV78ContagemResultado_NaoCnfDmnCod2 ;
      private int AV83ContagemResultado_Owner2 ;
      private int AV85ContagemResultado_CntCod2 ;
      private int AV97ContagemResultado_Servico3 ;
      private int AV98ContagemResultado_ContadorFM3 ;
      private int AV99ContagemResultado_SistemaCod3 ;
      private int AV100ContagemResultado_ContratadaCod3 ;
      private int AV101ContagemResultado_ServicoGrupo3 ;
      private int AV103ContagemResultado_NaoCnfDmnCod3 ;
      private int AV108ContagemResultado_Owner3 ;
      private int AV110ContagemResultado_CntCod3 ;
      private int AV122ContagemResultado_Servico4 ;
      private int AV123ContagemResultado_ContadorFM4 ;
      private int AV124ContagemResultado_SistemaCod4 ;
      private int AV125ContagemResultado_ContratadaCod4 ;
      private int AV126ContagemResultado_ServicoGrupo4 ;
      private int AV128ContagemResultado_NaoCnfDmnCod4 ;
      private int AV133ContagemResultado_Owner4 ;
      private int AV135ContagemResultado_CntCod4 ;
      private int AV147ContagemResultado_Servico5 ;
      private int AV148ContagemResultado_ContadorFM5 ;
      private int AV149ContagemResultado_SistemaCod5 ;
      private int AV150ContagemResultado_ContratadaCod5 ;
      private int AV151ContagemResultado_ServicoGrupo5 ;
      private int AV153ContagemResultado_NaoCnfDmnCod5 ;
      private int AV158ContagemResultado_Owner5 ;
      private int AV160ContagemResultado_CntCod5 ;
      private int AV165ExtraWWContagemResultadoExtraSelectionVs2DS_1_Contratada_areatrabalhocod ;
      private int AV177ExtraWWContagemResultadoExtraSelectionVs2DS_13_Contagemresultado_servico1 ;
      private int AV178ExtraWWContagemResultadoExtraSelectionVs2DS_14_Contagemresultado_contadorfm1 ;
      private int AV179ExtraWWContagemResultadoExtraSelectionVs2DS_15_Contagemresultado_sistemacod1 ;
      private int AV180ExtraWWContagemResultadoExtraSelectionVs2DS_16_Contagemresultado_contratadacod1 ;
      private int AV181ExtraWWContagemResultadoExtraSelectionVs2DS_17_Contagemresultado_servicogrupo1 ;
      private int AV183ExtraWWContagemResultadoExtraSelectionVs2DS_19_Contagemresultado_naocnfdmncod1 ;
      private int AV188ExtraWWContagemResultadoExtraSelectionVs2DS_24_Contagemresultado_owner1 ;
      private int AV190ExtraWWContagemResultadoExtraSelectionVs2DS_26_Contagemresultado_cntcod1 ;
      private int AV202ExtraWWContagemResultadoExtraSelectionVs2DS_38_Contagemresultado_servico2 ;
      private int AV203ExtraWWContagemResultadoExtraSelectionVs2DS_39_Contagemresultado_contadorfm2 ;
      private int AV204ExtraWWContagemResultadoExtraSelectionVs2DS_40_Contagemresultado_sistemacod2 ;
      private int AV205ExtraWWContagemResultadoExtraSelectionVs2DS_41_Contagemresultado_contratadacod2 ;
      private int AV206ExtraWWContagemResultadoExtraSelectionVs2DS_42_Contagemresultado_servicogrupo2 ;
      private int AV208ExtraWWContagemResultadoExtraSelectionVs2DS_44_Contagemresultado_naocnfdmncod2 ;
      private int AV213ExtraWWContagemResultadoExtraSelectionVs2DS_49_Contagemresultado_owner2 ;
      private int AV215ExtraWWContagemResultadoExtraSelectionVs2DS_51_Contagemresultado_cntcod2 ;
      private int AV227ExtraWWContagemResultadoExtraSelectionVs2DS_63_Contagemresultado_servico3 ;
      private int AV228ExtraWWContagemResultadoExtraSelectionVs2DS_64_Contagemresultado_contadorfm3 ;
      private int AV229ExtraWWContagemResultadoExtraSelectionVs2DS_65_Contagemresultado_sistemacod3 ;
      private int AV230ExtraWWContagemResultadoExtraSelectionVs2DS_66_Contagemresultado_contratadacod3 ;
      private int AV231ExtraWWContagemResultadoExtraSelectionVs2DS_67_Contagemresultado_servicogrupo3 ;
      private int AV233ExtraWWContagemResultadoExtraSelectionVs2DS_69_Contagemresultado_naocnfdmncod3 ;
      private int AV238ExtraWWContagemResultadoExtraSelectionVs2DS_74_Contagemresultado_owner3 ;
      private int AV240ExtraWWContagemResultadoExtraSelectionVs2DS_76_Contagemresultado_cntcod3 ;
      private int AV252ExtraWWContagemResultadoExtraSelectionVs2DS_88_Contagemresultado_servico4 ;
      private int AV253ExtraWWContagemResultadoExtraSelectionVs2DS_89_Contagemresultado_contadorfm4 ;
      private int AV254ExtraWWContagemResultadoExtraSelectionVs2DS_90_Contagemresultado_sistemacod4 ;
      private int AV255ExtraWWContagemResultadoExtraSelectionVs2DS_91_Contagemresultado_contratadacod4 ;
      private int AV256ExtraWWContagemResultadoExtraSelectionVs2DS_92_Contagemresultado_servicogrupo4 ;
      private int AV258ExtraWWContagemResultadoExtraSelectionVs2DS_94_Contagemresultado_naocnfdmncod4 ;
      private int AV263ExtraWWContagemResultadoExtraSelectionVs2DS_99_Contagemresultado_owner4 ;
      private int AV265ExtraWWContagemResultadoExtraSelectionVs2DS_101_Contagemresultado_cntcod4 ;
      private int AV277ExtraWWContagemResultadoExtraSelectionVs2DS_113_Contagemresultado_servico5 ;
      private int AV278ExtraWWContagemResultadoExtraSelectionVs2DS_114_Contagemresultado_contadorfm5 ;
      private int AV279ExtraWWContagemResultadoExtraSelectionVs2DS_115_Contagemresultado_sistemacod5 ;
      private int AV280ExtraWWContagemResultadoExtraSelectionVs2DS_116_Contagemresultado_contratadacod5 ;
      private int AV281ExtraWWContagemResultadoExtraSelectionVs2DS_117_Contagemresultado_servicogrupo5 ;
      private int AV283ExtraWWContagemResultadoExtraSelectionVs2DS_119_Contagemresultado_naocnfdmncod5 ;
      private int AV288ExtraWWContagemResultadoExtraSelectionVs2DS_124_Contagemresultado_owner5 ;
      private int AV290ExtraWWContagemResultadoExtraSelectionVs2DS_126_Contagemresultado_cntcod5 ;
      private int AV9WWPContext_gxTpr_Contratada_codigo ;
      private int AV9WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A601ContagemResultado_Servico ;
      private int A489ContagemResultado_SistemaCod ;
      private int A764ContagemResultado_ServicoGrupo ;
      private int A468ContagemResultado_NaoCnfDmnCod ;
      private int A508ContagemResultado_Owner ;
      private int A1603ContagemResultado_CntCod ;
      private int A584ContagemResultado_ContadorFM ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A456ContagemResultado_Codigo ;
      private long AV29count ;
      private decimal AV13TFContagemResultado_PFFinal ;
      private decimal AV14TFContagemResultado_PFFinal_To ;
      private decimal AV15TFContagemResultado_ValorPF ;
      private decimal AV16TFContagemResultado_ValorPF_To ;
      private decimal AV54ContagemResultado_PFBFM1 ;
      private decimal AV55ContagemResultado_PFBFS1 ;
      private decimal AV79ContagemResultado_PFBFM2 ;
      private decimal AV80ContagemResultado_PFBFS2 ;
      private decimal AV104ContagemResultado_PFBFM3 ;
      private decimal AV105ContagemResultado_PFBFS3 ;
      private decimal AV129ContagemResultado_PFBFM4 ;
      private decimal AV130ContagemResultado_PFBFS4 ;
      private decimal AV154ContagemResultado_PFBFM5 ;
      private decimal AV155ContagemResultado_PFBFS5 ;
      private decimal AV184ExtraWWContagemResultadoExtraSelectionVs2DS_20_Contagemresultado_pfbfm1 ;
      private decimal AV185ExtraWWContagemResultadoExtraSelectionVs2DS_21_Contagemresultado_pfbfs1 ;
      private decimal AV209ExtraWWContagemResultadoExtraSelectionVs2DS_45_Contagemresultado_pfbfm2 ;
      private decimal AV210ExtraWWContagemResultadoExtraSelectionVs2DS_46_Contagemresultado_pfbfs2 ;
      private decimal AV234ExtraWWContagemResultadoExtraSelectionVs2DS_70_Contagemresultado_pfbfm3 ;
      private decimal AV235ExtraWWContagemResultadoExtraSelectionVs2DS_71_Contagemresultado_pfbfs3 ;
      private decimal AV259ExtraWWContagemResultadoExtraSelectionVs2DS_95_Contagemresultado_pfbfm4 ;
      private decimal AV260ExtraWWContagemResultadoExtraSelectionVs2DS_96_Contagemresultado_pfbfs4 ;
      private decimal AV284ExtraWWContagemResultadoExtraSelectionVs2DS_120_Contagemresultado_pfbfm5 ;
      private decimal AV285ExtraWWContagemResultadoExtraSelectionVs2DS_121_Contagemresultado_pfbfs5 ;
      private decimal AV294ExtraWWContagemResultadoExtraSelectionVs2DS_130_Tfcontagemresultado_pffinal ;
      private decimal AV295ExtraWWContagemResultadoExtraSelectionVs2DS_131_Tfcontagemresultado_pffinal_to ;
      private decimal AV296ExtraWWContagemResultadoExtraSelectionVs2DS_132_Tfcontagemresultado_valorpf ;
      private decimal AV297ExtraWWContagemResultadoExtraSelectionVs2DS_133_Tfcontagemresultado_valorpf_to ;
      private decimal A512ContagemResultado_ValorPF ;
      private decimal A1854ContagemResultado_VlrCnc ;
      private decimal A684ContagemResultado_PFBFSUltima ;
      private decimal A682ContagemResultado_PFBFMUltima ;
      private decimal A685ContagemResultado_PFLFSUltima ;
      private decimal A683ContagemResultado_PFLFMUltima ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal GXt_decimal2 ;
      private String AV11TFContagemResultado_ServicoSigla ;
      private String AV12TFContagemResultado_ServicoSigla_Sel ;
      private String AV41ContagemResultado_StatusDmn1 ;
      private String AV52ContagemResultado_Baseline1 ;
      private String AV56ContagemResultado_Agrupador1 ;
      private String AV59ContagemResultado_TemPndHmlg1 ;
      private String AV66ContagemResultado_StatusDmn2 ;
      private String AV77ContagemResultado_Baseline2 ;
      private String AV81ContagemResultado_Agrupador2 ;
      private String AV84ContagemResultado_TemPndHmlg2 ;
      private String AV91ContagemResultado_StatusDmn3 ;
      private String AV102ContagemResultado_Baseline3 ;
      private String AV106ContagemResultado_Agrupador3 ;
      private String AV109ContagemResultado_TemPndHmlg3 ;
      private String AV116ContagemResultado_StatusDmn4 ;
      private String AV127ContagemResultado_Baseline4 ;
      private String AV131ContagemResultado_Agrupador4 ;
      private String AV134ContagemResultado_TemPndHmlg4 ;
      private String AV141ContagemResultado_StatusDmn5 ;
      private String AV152ContagemResultado_Baseline5 ;
      private String AV156ContagemResultado_Agrupador5 ;
      private String AV159ContagemResultado_TemPndHmlg5 ;
      private String AV171ExtraWWContagemResultadoExtraSelectionVs2DS_7_Contagemresultado_statusdmn1 ;
      private String AV182ExtraWWContagemResultadoExtraSelectionVs2DS_18_Contagemresultado_baseline1 ;
      private String AV186ExtraWWContagemResultadoExtraSelectionVs2DS_22_Contagemresultado_agrupador1 ;
      private String AV189ExtraWWContagemResultadoExtraSelectionVs2DS_25_Contagemresultado_tempndhmlg1 ;
      private String AV196ExtraWWContagemResultadoExtraSelectionVs2DS_32_Contagemresultado_statusdmn2 ;
      private String AV207ExtraWWContagemResultadoExtraSelectionVs2DS_43_Contagemresultado_baseline2 ;
      private String AV211ExtraWWContagemResultadoExtraSelectionVs2DS_47_Contagemresultado_agrupador2 ;
      private String AV214ExtraWWContagemResultadoExtraSelectionVs2DS_50_Contagemresultado_tempndhmlg2 ;
      private String AV221ExtraWWContagemResultadoExtraSelectionVs2DS_57_Contagemresultado_statusdmn3 ;
      private String AV232ExtraWWContagemResultadoExtraSelectionVs2DS_68_Contagemresultado_baseline3 ;
      private String AV236ExtraWWContagemResultadoExtraSelectionVs2DS_72_Contagemresultado_agrupador3 ;
      private String AV239ExtraWWContagemResultadoExtraSelectionVs2DS_75_Contagemresultado_tempndhmlg3 ;
      private String AV246ExtraWWContagemResultadoExtraSelectionVs2DS_82_Contagemresultado_statusdmn4 ;
      private String AV257ExtraWWContagemResultadoExtraSelectionVs2DS_93_Contagemresultado_baseline4 ;
      private String AV261ExtraWWContagemResultadoExtraSelectionVs2DS_97_Contagemresultado_agrupador4 ;
      private String AV264ExtraWWContagemResultadoExtraSelectionVs2DS_100_Contagemresultado_tempndhmlg4 ;
      private String AV271ExtraWWContagemResultadoExtraSelectionVs2DS_107_Contagemresultado_statusdmn5 ;
      private String AV282ExtraWWContagemResultadoExtraSelectionVs2DS_118_Contagemresultado_baseline5 ;
      private String AV286ExtraWWContagemResultadoExtraSelectionVs2DS_122_Contagemresultado_agrupador5 ;
      private String AV289ExtraWWContagemResultadoExtraSelectionVs2DS_125_Contagemresultado_tempndhmlg5 ;
      private String AV292ExtraWWContagemResultadoExtraSelectionVs2DS_128_Tfcontagemresultado_servicosigla ;
      private String AV293ExtraWWContagemResultadoExtraSelectionVs2DS_129_Tfcontagemresultado_servicosigla_sel ;
      private String scmdbuf ;
      private String lV292ExtraWWContagemResultadoExtraSelectionVs2DS_128_Tfcontagemresultado_servicosigla ;
      private String A484ContagemResultado_StatusDmn ;
      private String A1046ContagemResultado_Agrupador ;
      private String A801ContagemResultado_ServicoSigla ;
      private DateTime A1351ContagemResultado_DataPrevista ;
      private DateTime AV39ContagemResultado_DataCnt1 ;
      private DateTime AV40ContagemResultado_DataCnt_To1 ;
      private DateTime AV43ContagemResultado_DataDmn1 ;
      private DateTime AV44ContagemResultado_DataDmn_To1 ;
      private DateTime AV45ContagemResultado_DataPrevista1 ;
      private DateTime AV46ContagemResultado_DataPrevista_To1 ;
      private DateTime AV64ContagemResultado_DataCnt2 ;
      private DateTime AV65ContagemResultado_DataCnt_To2 ;
      private DateTime AV68ContagemResultado_DataDmn2 ;
      private DateTime AV69ContagemResultado_DataDmn_To2 ;
      private DateTime AV70ContagemResultado_DataPrevista2 ;
      private DateTime AV71ContagemResultado_DataPrevista_To2 ;
      private DateTime AV89ContagemResultado_DataCnt3 ;
      private DateTime AV90ContagemResultado_DataCnt_To3 ;
      private DateTime AV93ContagemResultado_DataDmn3 ;
      private DateTime AV94ContagemResultado_DataDmn_To3 ;
      private DateTime AV95ContagemResultado_DataPrevista3 ;
      private DateTime AV96ContagemResultado_DataPrevista_To3 ;
      private DateTime AV114ContagemResultado_DataCnt4 ;
      private DateTime AV115ContagemResultado_DataCnt_To4 ;
      private DateTime AV118ContagemResultado_DataDmn4 ;
      private DateTime AV119ContagemResultado_DataDmn_To4 ;
      private DateTime AV120ContagemResultado_DataPrevista4 ;
      private DateTime AV121ContagemResultado_DataPrevista_To4 ;
      private DateTime AV139ContagemResultado_DataCnt5 ;
      private DateTime AV140ContagemResultado_DataCnt_To5 ;
      private DateTime AV143ContagemResultado_DataDmn5 ;
      private DateTime AV144ContagemResultado_DataDmn_To5 ;
      private DateTime AV145ContagemResultado_DataPrevista5 ;
      private DateTime AV146ContagemResultado_DataPrevista_To5 ;
      private DateTime AV169ExtraWWContagemResultadoExtraSelectionVs2DS_5_Contagemresultado_datacnt1 ;
      private DateTime AV170ExtraWWContagemResultadoExtraSelectionVs2DS_6_Contagemresultado_datacnt_to1 ;
      private DateTime AV173ExtraWWContagemResultadoExtraSelectionVs2DS_9_Contagemresultado_datadmn1 ;
      private DateTime AV174ExtraWWContagemResultadoExtraSelectionVs2DS_10_Contagemresultado_datadmn_to1 ;
      private DateTime AV175ExtraWWContagemResultadoExtraSelectionVs2DS_11_Contagemresultado_dataprevista1 ;
      private DateTime AV176ExtraWWContagemResultadoExtraSelectionVs2DS_12_Contagemresultado_dataprevista_to1 ;
      private DateTime AV194ExtraWWContagemResultadoExtraSelectionVs2DS_30_Contagemresultado_datacnt2 ;
      private DateTime AV195ExtraWWContagemResultadoExtraSelectionVs2DS_31_Contagemresultado_datacnt_to2 ;
      private DateTime AV198ExtraWWContagemResultadoExtraSelectionVs2DS_34_Contagemresultado_datadmn2 ;
      private DateTime AV199ExtraWWContagemResultadoExtraSelectionVs2DS_35_Contagemresultado_datadmn_to2 ;
      private DateTime AV200ExtraWWContagemResultadoExtraSelectionVs2DS_36_Contagemresultado_dataprevista2 ;
      private DateTime AV201ExtraWWContagemResultadoExtraSelectionVs2DS_37_Contagemresultado_dataprevista_to2 ;
      private DateTime AV219ExtraWWContagemResultadoExtraSelectionVs2DS_55_Contagemresultado_datacnt3 ;
      private DateTime AV220ExtraWWContagemResultadoExtraSelectionVs2DS_56_Contagemresultado_datacnt_to3 ;
      private DateTime AV223ExtraWWContagemResultadoExtraSelectionVs2DS_59_Contagemresultado_datadmn3 ;
      private DateTime AV224ExtraWWContagemResultadoExtraSelectionVs2DS_60_Contagemresultado_datadmn_to3 ;
      private DateTime AV225ExtraWWContagemResultadoExtraSelectionVs2DS_61_Contagemresultado_dataprevista3 ;
      private DateTime AV226ExtraWWContagemResultadoExtraSelectionVs2DS_62_Contagemresultado_dataprevista_to3 ;
      private DateTime AV244ExtraWWContagemResultadoExtraSelectionVs2DS_80_Contagemresultado_datacnt4 ;
      private DateTime AV245ExtraWWContagemResultadoExtraSelectionVs2DS_81_Contagemresultado_datacnt_to4 ;
      private DateTime AV248ExtraWWContagemResultadoExtraSelectionVs2DS_84_Contagemresultado_datadmn4 ;
      private DateTime AV249ExtraWWContagemResultadoExtraSelectionVs2DS_85_Contagemresultado_datadmn_to4 ;
      private DateTime AV250ExtraWWContagemResultadoExtraSelectionVs2DS_86_Contagemresultado_dataprevista4 ;
      private DateTime AV251ExtraWWContagemResultadoExtraSelectionVs2DS_87_Contagemresultado_dataprevista_to4 ;
      private DateTime AV269ExtraWWContagemResultadoExtraSelectionVs2DS_105_Contagemresultado_datacnt5 ;
      private DateTime AV270ExtraWWContagemResultadoExtraSelectionVs2DS_106_Contagemresultado_datacnt_to5 ;
      private DateTime AV273ExtraWWContagemResultadoExtraSelectionVs2DS_109_Contagemresultado_datadmn5 ;
      private DateTime AV274ExtraWWContagemResultadoExtraSelectionVs2DS_110_Contagemresultado_datadmn_to5 ;
      private DateTime AV275ExtraWWContagemResultadoExtraSelectionVs2DS_111_Contagemresultado_dataprevista5 ;
      private DateTime AV276ExtraWWContagemResultadoExtraSelectionVs2DS_112_Contagemresultado_dataprevista_to5 ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime A566ContagemResultado_DataUltCnt ;
      private bool returnInSub ;
      private bool AV61DynamicFiltersEnabled2 ;
      private bool AV86DynamicFiltersEnabled3 ;
      private bool AV111DynamicFiltersEnabled4 ;
      private bool AV136DynamicFiltersEnabled5 ;
      private bool AV191ExtraWWContagemResultadoExtraSelectionVs2DS_27_Dynamicfiltersenabled2 ;
      private bool AV216ExtraWWContagemResultadoExtraSelectionVs2DS_52_Dynamicfiltersenabled3 ;
      private bool AV241ExtraWWContagemResultadoExtraSelectionVs2DS_77_Dynamicfiltersenabled4 ;
      private bool AV266ExtraWWContagemResultadoExtraSelectionVs2DS_102_Dynamicfiltersenabled5 ;
      private bool A598ContagemResultado_Baseline ;
      private bool A1802ContagemResultado_TemPndHmlg ;
      private bool BRKYD2 ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n801ContagemResultado_ServicoSigla ;
      private bool n512ContagemResultado_ValorPF ;
      private bool n1603ContagemResultado_CntCod ;
      private bool n494ContagemResultado_Descricao ;
      private bool n1046ContagemResultado_Agrupador ;
      private bool n468ContagemResultado_NaoCnfDmnCod ;
      private bool n598ContagemResultado_Baseline ;
      private bool n764ContagemResultado_ServicoGrupo ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n1351ContagemResultado_DataPrevista ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n457ContagemResultado_Demanda ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n1854ContagemResultado_VlrCnc ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool GXt_boolean1 ;
      private String AV28OptionIndexesJson ;
      private String AV23OptionsJson ;
      private String AV26OptionsDescJson ;
      private String AV19DDOName ;
      private String AV17SearchTxt ;
      private String AV18SearchTxtTo ;
      private String AV37DynamicFiltersSelector1 ;
      private String AV42ContagemResultado_OsFsOsFm1 ;
      private String AV57ContagemResultado_Descricao1 ;
      private String AV62DynamicFiltersSelector2 ;
      private String AV67ContagemResultado_OsFsOsFm2 ;
      private String AV82ContagemResultado_Descricao2 ;
      private String AV87DynamicFiltersSelector3 ;
      private String AV92ContagemResultado_OsFsOsFm3 ;
      private String AV107ContagemResultado_Descricao3 ;
      private String AV112DynamicFiltersSelector4 ;
      private String AV117ContagemResultado_OsFsOsFm4 ;
      private String AV132ContagemResultado_Descricao4 ;
      private String AV137DynamicFiltersSelector5 ;
      private String AV142ContagemResultado_OsFsOsFm5 ;
      private String AV157ContagemResultado_Descricao5 ;
      private String AV167ExtraWWContagemResultadoExtraSelectionVs2DS_3_Dynamicfiltersselector1 ;
      private String AV172ExtraWWContagemResultadoExtraSelectionVs2DS_8_Contagemresultado_osfsosfm1 ;
      private String AV187ExtraWWContagemResultadoExtraSelectionVs2DS_23_Contagemresultado_descricao1 ;
      private String AV192ExtraWWContagemResultadoExtraSelectionVs2DS_28_Dynamicfiltersselector2 ;
      private String AV197ExtraWWContagemResultadoExtraSelectionVs2DS_33_Contagemresultado_osfsosfm2 ;
      private String AV212ExtraWWContagemResultadoExtraSelectionVs2DS_48_Contagemresultado_descricao2 ;
      private String AV217ExtraWWContagemResultadoExtraSelectionVs2DS_53_Dynamicfiltersselector3 ;
      private String AV222ExtraWWContagemResultadoExtraSelectionVs2DS_58_Contagemresultado_osfsosfm3 ;
      private String AV237ExtraWWContagemResultadoExtraSelectionVs2DS_73_Contagemresultado_descricao3 ;
      private String AV242ExtraWWContagemResultadoExtraSelectionVs2DS_78_Dynamicfiltersselector4 ;
      private String AV247ExtraWWContagemResultadoExtraSelectionVs2DS_83_Contagemresultado_osfsosfm4 ;
      private String AV262ExtraWWContagemResultadoExtraSelectionVs2DS_98_Contagemresultado_descricao4 ;
      private String AV267ExtraWWContagemResultadoExtraSelectionVs2DS_103_Dynamicfiltersselector5 ;
      private String AV272ExtraWWContagemResultadoExtraSelectionVs2DS_108_Contagemresultado_osfsosfm5 ;
      private String AV287ExtraWWContagemResultadoExtraSelectionVs2DS_123_Contagemresultado_descricao5 ;
      private String lV172ExtraWWContagemResultadoExtraSelectionVs2DS_8_Contagemresultado_osfsosfm1 ;
      private String lV187ExtraWWContagemResultadoExtraSelectionVs2DS_23_Contagemresultado_descricao1 ;
      private String lV197ExtraWWContagemResultadoExtraSelectionVs2DS_33_Contagemresultado_osfsosfm2 ;
      private String lV212ExtraWWContagemResultadoExtraSelectionVs2DS_48_Contagemresultado_descricao2 ;
      private String lV222ExtraWWContagemResultadoExtraSelectionVs2DS_58_Contagemresultado_osfsosfm3 ;
      private String lV237ExtraWWContagemResultadoExtraSelectionVs2DS_73_Contagemresultado_descricao3 ;
      private String lV247ExtraWWContagemResultadoExtraSelectionVs2DS_83_Contagemresultado_osfsosfm4 ;
      private String lV262ExtraWWContagemResultadoExtraSelectionVs2DS_98_Contagemresultado_descricao4 ;
      private String lV272ExtraWWContagemResultadoExtraSelectionVs2DS_108_Contagemresultado_osfsosfm5 ;
      private String lV287ExtraWWContagemResultadoExtraSelectionVs2DS_123_Contagemresultado_descricao5 ;
      private String A457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String A494ContagemResultado_Descricao ;
      private String AV21Option ;
      private IGxSession AV30Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00YD3_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00YD3_n1553ContagemResultado_CntSrvCod ;
      private int[] P00YD3_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00YD3_n52Contratada_AreaTrabalhoCod ;
      private String[] P00YD3_A801ContagemResultado_ServicoSigla ;
      private bool[] P00YD3_n801ContagemResultado_ServicoSigla ;
      private decimal[] P00YD3_A512ContagemResultado_ValorPF ;
      private bool[] P00YD3_n512ContagemResultado_ValorPF ;
      private int[] P00YD3_A1603ContagemResultado_CntCod ;
      private bool[] P00YD3_n1603ContagemResultado_CntCod ;
      private String[] P00YD3_A494ContagemResultado_Descricao ;
      private bool[] P00YD3_n494ContagemResultado_Descricao ;
      private String[] P00YD3_A1046ContagemResultado_Agrupador ;
      private bool[] P00YD3_n1046ContagemResultado_Agrupador ;
      private int[] P00YD3_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] P00YD3_n468ContagemResultado_NaoCnfDmnCod ;
      private bool[] P00YD3_A598ContagemResultado_Baseline ;
      private bool[] P00YD3_n598ContagemResultado_Baseline ;
      private int[] P00YD3_A764ContagemResultado_ServicoGrupo ;
      private bool[] P00YD3_n764ContagemResultado_ServicoGrupo ;
      private int[] P00YD3_A489ContagemResultado_SistemaCod ;
      private bool[] P00YD3_n489ContagemResultado_SistemaCod ;
      private int[] P00YD3_A508ContagemResultado_Owner ;
      private int[] P00YD3_A601ContagemResultado_Servico ;
      private bool[] P00YD3_n601ContagemResultado_Servico ;
      private DateTime[] P00YD3_A1351ContagemResultado_DataPrevista ;
      private bool[] P00YD3_n1351ContagemResultado_DataPrevista ;
      private DateTime[] P00YD3_A471ContagemResultado_DataDmn ;
      private String[] P00YD3_A493ContagemResultado_DemandaFM ;
      private bool[] P00YD3_n493ContagemResultado_DemandaFM ;
      private String[] P00YD3_A457ContagemResultado_Demanda ;
      private bool[] P00YD3_n457ContagemResultado_Demanda ;
      private String[] P00YD3_A484ContagemResultado_StatusDmn ;
      private bool[] P00YD3_n484ContagemResultado_StatusDmn ;
      private decimal[] P00YD3_A1854ContagemResultado_VlrCnc ;
      private bool[] P00YD3_n1854ContagemResultado_VlrCnc ;
      private int[] P00YD3_A490ContagemResultado_ContratadaCod ;
      private bool[] P00YD3_n490ContagemResultado_ContratadaCod ;
      private decimal[] P00YD3_A683ContagemResultado_PFLFMUltima ;
      private decimal[] P00YD3_A685ContagemResultado_PFLFSUltima ;
      private decimal[] P00YD3_A682ContagemResultado_PFBFMUltima ;
      private decimal[] P00YD3_A684ContagemResultado_PFBFSUltima ;
      private int[] P00YD3_A584ContagemResultado_ContadorFM ;
      private DateTime[] P00YD3_A566ContagemResultado_DataUltCnt ;
      private short[] P00YD3_A531ContagemResultado_StatusUltCnt ;
      private int[] P00YD3_A456ContagemResultado_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV25OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV27OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV32GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV33GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV34GridStateDynamicFilter ;
   }

   public class getextrawwcontagemresultadoextraselectionvs2filterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00YD3( IGxContext context ,
                                             int AV9WWPContext_gxTpr_Contratada_codigo ,
                                             String AV167ExtraWWContagemResultadoExtraSelectionVs2DS_3_Dynamicfiltersselector1 ,
                                             String AV171ExtraWWContagemResultadoExtraSelectionVs2DS_7_Contagemresultado_statusdmn1 ,
                                             short AV168ExtraWWContagemResultadoExtraSelectionVs2DS_4_Dynamicfiltersoperator1 ,
                                             String AV172ExtraWWContagemResultadoExtraSelectionVs2DS_8_Contagemresultado_osfsosfm1 ,
                                             DateTime AV173ExtraWWContagemResultadoExtraSelectionVs2DS_9_Contagemresultado_datadmn1 ,
                                             DateTime AV174ExtraWWContagemResultadoExtraSelectionVs2DS_10_Contagemresultado_datadmn_to1 ,
                                             DateTime AV175ExtraWWContagemResultadoExtraSelectionVs2DS_11_Contagemresultado_dataprevista1 ,
                                             DateTime AV176ExtraWWContagemResultadoExtraSelectionVs2DS_12_Contagemresultado_dataprevista_to1 ,
                                             int AV177ExtraWWContagemResultadoExtraSelectionVs2DS_13_Contagemresultado_servico1 ,
                                             int AV179ExtraWWContagemResultadoExtraSelectionVs2DS_15_Contagemresultado_sistemacod1 ,
                                             int AV180ExtraWWContagemResultadoExtraSelectionVs2DS_16_Contagemresultado_contratadacod1 ,
                                             int AV165ExtraWWContagemResultadoExtraSelectionVs2DS_1_Contratada_areatrabalhocod ,
                                             int AV181ExtraWWContagemResultadoExtraSelectionVs2DS_17_Contagemresultado_servicogrupo1 ,
                                             String AV182ExtraWWContagemResultadoExtraSelectionVs2DS_18_Contagemresultado_baseline1 ,
                                             int AV183ExtraWWContagemResultadoExtraSelectionVs2DS_19_Contagemresultado_naocnfdmncod1 ,
                                             String AV186ExtraWWContagemResultadoExtraSelectionVs2DS_22_Contagemresultado_agrupador1 ,
                                             String AV187ExtraWWContagemResultadoExtraSelectionVs2DS_23_Contagemresultado_descricao1 ,
                                             int AV188ExtraWWContagemResultadoExtraSelectionVs2DS_24_Contagemresultado_owner1 ,
                                             int AV190ExtraWWContagemResultadoExtraSelectionVs2DS_26_Contagemresultado_cntcod1 ,
                                             bool AV191ExtraWWContagemResultadoExtraSelectionVs2DS_27_Dynamicfiltersenabled2 ,
                                             String AV192ExtraWWContagemResultadoExtraSelectionVs2DS_28_Dynamicfiltersselector2 ,
                                             String AV196ExtraWWContagemResultadoExtraSelectionVs2DS_32_Contagemresultado_statusdmn2 ,
                                             short AV193ExtraWWContagemResultadoExtraSelectionVs2DS_29_Dynamicfiltersoperator2 ,
                                             String AV197ExtraWWContagemResultadoExtraSelectionVs2DS_33_Contagemresultado_osfsosfm2 ,
                                             DateTime AV198ExtraWWContagemResultadoExtraSelectionVs2DS_34_Contagemresultado_datadmn2 ,
                                             DateTime AV199ExtraWWContagemResultadoExtraSelectionVs2DS_35_Contagemresultado_datadmn_to2 ,
                                             DateTime AV200ExtraWWContagemResultadoExtraSelectionVs2DS_36_Contagemresultado_dataprevista2 ,
                                             DateTime AV201ExtraWWContagemResultadoExtraSelectionVs2DS_37_Contagemresultado_dataprevista_to2 ,
                                             int AV202ExtraWWContagemResultadoExtraSelectionVs2DS_38_Contagemresultado_servico2 ,
                                             int AV204ExtraWWContagemResultadoExtraSelectionVs2DS_40_Contagemresultado_sistemacod2 ,
                                             int AV205ExtraWWContagemResultadoExtraSelectionVs2DS_41_Contagemresultado_contratadacod2 ,
                                             int AV206ExtraWWContagemResultadoExtraSelectionVs2DS_42_Contagemresultado_servicogrupo2 ,
                                             String AV207ExtraWWContagemResultadoExtraSelectionVs2DS_43_Contagemresultado_baseline2 ,
                                             int AV208ExtraWWContagemResultadoExtraSelectionVs2DS_44_Contagemresultado_naocnfdmncod2 ,
                                             String AV211ExtraWWContagemResultadoExtraSelectionVs2DS_47_Contagemresultado_agrupador2 ,
                                             String AV212ExtraWWContagemResultadoExtraSelectionVs2DS_48_Contagemresultado_descricao2 ,
                                             int AV213ExtraWWContagemResultadoExtraSelectionVs2DS_49_Contagemresultado_owner2 ,
                                             int AV215ExtraWWContagemResultadoExtraSelectionVs2DS_51_Contagemresultado_cntcod2 ,
                                             bool AV216ExtraWWContagemResultadoExtraSelectionVs2DS_52_Dynamicfiltersenabled3 ,
                                             String AV217ExtraWWContagemResultadoExtraSelectionVs2DS_53_Dynamicfiltersselector3 ,
                                             String AV221ExtraWWContagemResultadoExtraSelectionVs2DS_57_Contagemresultado_statusdmn3 ,
                                             short AV218ExtraWWContagemResultadoExtraSelectionVs2DS_54_Dynamicfiltersoperator3 ,
                                             String AV222ExtraWWContagemResultadoExtraSelectionVs2DS_58_Contagemresultado_osfsosfm3 ,
                                             DateTime AV223ExtraWWContagemResultadoExtraSelectionVs2DS_59_Contagemresultado_datadmn3 ,
                                             DateTime AV224ExtraWWContagemResultadoExtraSelectionVs2DS_60_Contagemresultado_datadmn_to3 ,
                                             DateTime AV225ExtraWWContagemResultadoExtraSelectionVs2DS_61_Contagemresultado_dataprevista3 ,
                                             DateTime AV226ExtraWWContagemResultadoExtraSelectionVs2DS_62_Contagemresultado_dataprevista_to3 ,
                                             int AV227ExtraWWContagemResultadoExtraSelectionVs2DS_63_Contagemresultado_servico3 ,
                                             int AV229ExtraWWContagemResultadoExtraSelectionVs2DS_65_Contagemresultado_sistemacod3 ,
                                             int AV230ExtraWWContagemResultadoExtraSelectionVs2DS_66_Contagemresultado_contratadacod3 ,
                                             int AV231ExtraWWContagemResultadoExtraSelectionVs2DS_67_Contagemresultado_servicogrupo3 ,
                                             String AV232ExtraWWContagemResultadoExtraSelectionVs2DS_68_Contagemresultado_baseline3 ,
                                             int AV233ExtraWWContagemResultadoExtraSelectionVs2DS_69_Contagemresultado_naocnfdmncod3 ,
                                             String AV236ExtraWWContagemResultadoExtraSelectionVs2DS_72_Contagemresultado_agrupador3 ,
                                             String AV237ExtraWWContagemResultadoExtraSelectionVs2DS_73_Contagemresultado_descricao3 ,
                                             int AV238ExtraWWContagemResultadoExtraSelectionVs2DS_74_Contagemresultado_owner3 ,
                                             int AV240ExtraWWContagemResultadoExtraSelectionVs2DS_76_Contagemresultado_cntcod3 ,
                                             bool AV241ExtraWWContagemResultadoExtraSelectionVs2DS_77_Dynamicfiltersenabled4 ,
                                             String AV242ExtraWWContagemResultadoExtraSelectionVs2DS_78_Dynamicfiltersselector4 ,
                                             String AV246ExtraWWContagemResultadoExtraSelectionVs2DS_82_Contagemresultado_statusdmn4 ,
                                             short AV243ExtraWWContagemResultadoExtraSelectionVs2DS_79_Dynamicfiltersoperator4 ,
                                             String AV247ExtraWWContagemResultadoExtraSelectionVs2DS_83_Contagemresultado_osfsosfm4 ,
                                             DateTime AV248ExtraWWContagemResultadoExtraSelectionVs2DS_84_Contagemresultado_datadmn4 ,
                                             DateTime AV249ExtraWWContagemResultadoExtraSelectionVs2DS_85_Contagemresultado_datadmn_to4 ,
                                             DateTime AV250ExtraWWContagemResultadoExtraSelectionVs2DS_86_Contagemresultado_dataprevista4 ,
                                             DateTime AV251ExtraWWContagemResultadoExtraSelectionVs2DS_87_Contagemresultado_dataprevista_to4 ,
                                             int AV252ExtraWWContagemResultadoExtraSelectionVs2DS_88_Contagemresultado_servico4 ,
                                             int AV254ExtraWWContagemResultadoExtraSelectionVs2DS_90_Contagemresultado_sistemacod4 ,
                                             int AV255ExtraWWContagemResultadoExtraSelectionVs2DS_91_Contagemresultado_contratadacod4 ,
                                             int AV256ExtraWWContagemResultadoExtraSelectionVs2DS_92_Contagemresultado_servicogrupo4 ,
                                             String AV257ExtraWWContagemResultadoExtraSelectionVs2DS_93_Contagemresultado_baseline4 ,
                                             int AV258ExtraWWContagemResultadoExtraSelectionVs2DS_94_Contagemresultado_naocnfdmncod4 ,
                                             String AV261ExtraWWContagemResultadoExtraSelectionVs2DS_97_Contagemresultado_agrupador4 ,
                                             String AV262ExtraWWContagemResultadoExtraSelectionVs2DS_98_Contagemresultado_descricao4 ,
                                             int AV263ExtraWWContagemResultadoExtraSelectionVs2DS_99_Contagemresultado_owner4 ,
                                             int AV265ExtraWWContagemResultadoExtraSelectionVs2DS_101_Contagemresultado_cntcod4 ,
                                             bool AV266ExtraWWContagemResultadoExtraSelectionVs2DS_102_Dynamicfiltersenabled5 ,
                                             String AV267ExtraWWContagemResultadoExtraSelectionVs2DS_103_Dynamicfiltersselector5 ,
                                             String AV271ExtraWWContagemResultadoExtraSelectionVs2DS_107_Contagemresultado_statusdmn5 ,
                                             short AV268ExtraWWContagemResultadoExtraSelectionVs2DS_104_Dynamicfiltersoperator5 ,
                                             String AV272ExtraWWContagemResultadoExtraSelectionVs2DS_108_Contagemresultado_osfsosfm5 ,
                                             DateTime AV273ExtraWWContagemResultadoExtraSelectionVs2DS_109_Contagemresultado_datadmn5 ,
                                             DateTime AV274ExtraWWContagemResultadoExtraSelectionVs2DS_110_Contagemresultado_datadmn_to5 ,
                                             DateTime AV275ExtraWWContagemResultadoExtraSelectionVs2DS_111_Contagemresultado_dataprevista5 ,
                                             DateTime AV276ExtraWWContagemResultadoExtraSelectionVs2DS_112_Contagemresultado_dataprevista_to5 ,
                                             int AV277ExtraWWContagemResultadoExtraSelectionVs2DS_113_Contagemresultado_servico5 ,
                                             int AV279ExtraWWContagemResultadoExtraSelectionVs2DS_115_Contagemresultado_sistemacod5 ,
                                             int AV280ExtraWWContagemResultadoExtraSelectionVs2DS_116_Contagemresultado_contratadacod5 ,
                                             int AV281ExtraWWContagemResultadoExtraSelectionVs2DS_117_Contagemresultado_servicogrupo5 ,
                                             String AV282ExtraWWContagemResultadoExtraSelectionVs2DS_118_Contagemresultado_baseline5 ,
                                             int AV283ExtraWWContagemResultadoExtraSelectionVs2DS_119_Contagemresultado_naocnfdmncod5 ,
                                             String AV286ExtraWWContagemResultadoExtraSelectionVs2DS_122_Contagemresultado_agrupador5 ,
                                             String AV287ExtraWWContagemResultadoExtraSelectionVs2DS_123_Contagemresultado_descricao5 ,
                                             int AV288ExtraWWContagemResultadoExtraSelectionVs2DS_124_Contagemresultado_owner5 ,
                                             int AV290ExtraWWContagemResultadoExtraSelectionVs2DS_126_Contagemresultado_cntcod5 ,
                                             short AV291ExtraWWContagemResultadoExtraSelectionVs2DS_127_Tfcontagemresultado_baseline_sel ,
                                             String AV293ExtraWWContagemResultadoExtraSelectionVs2DS_129_Tfcontagemresultado_servicosigla_sel ,
                                             String AV292ExtraWWContagemResultadoExtraSelectionVs2DS_128_Tfcontagemresultado_servicosigla ,
                                             decimal AV296ExtraWWContagemResultadoExtraSelectionVs2DS_132_Tfcontagemresultado_valorpf ,
                                             decimal AV297ExtraWWContagemResultadoExtraSelectionVs2DS_133_Tfcontagemresultado_valorpf_to ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             String A484ContagemResultado_StatusDmn ,
                                             String A457ContagemResultado_Demanda ,
                                             String A493ContagemResultado_DemandaFM ,
                                             DateTime A471ContagemResultado_DataDmn ,
                                             DateTime A1351ContagemResultado_DataPrevista ,
                                             int A601ContagemResultado_Servico ,
                                             int A489ContagemResultado_SistemaCod ,
                                             int A764ContagemResultado_ServicoGrupo ,
                                             bool A598ContagemResultado_Baseline ,
                                             int A468ContagemResultado_NaoCnfDmnCod ,
                                             String A1046ContagemResultado_Agrupador ,
                                             String A494ContagemResultado_Descricao ,
                                             int A508ContagemResultado_Owner ,
                                             int A1603ContagemResultado_CntCod ,
                                             String A801ContagemResultado_ServicoSigla ,
                                             decimal A512ContagemResultado_ValorPF ,
                                             short A531ContagemResultado_StatusUltCnt ,
                                             decimal A1854ContagemResultado_VlrCnc ,
                                             DateTime AV169ExtraWWContagemResultadoExtraSelectionVs2DS_5_Contagemresultado_datacnt1 ,
                                             DateTime A566ContagemResultado_DataUltCnt ,
                                             DateTime AV170ExtraWWContagemResultadoExtraSelectionVs2DS_6_Contagemresultado_datacnt_to1 ,
                                             int AV178ExtraWWContagemResultadoExtraSelectionVs2DS_14_Contagemresultado_contadorfm1 ,
                                             int A584ContagemResultado_ContadorFM ,
                                             decimal A684ContagemResultado_PFBFSUltima ,
                                             decimal A682ContagemResultado_PFBFMUltima ,
                                             decimal A685ContagemResultado_PFLFSUltima ,
                                             decimal A683ContagemResultado_PFLFMUltima ,
                                             String AV189ExtraWWContagemResultadoExtraSelectionVs2DS_25_Contagemresultado_tempndhmlg1 ,
                                             bool A1802ContagemResultado_TemPndHmlg ,
                                             DateTime AV194ExtraWWContagemResultadoExtraSelectionVs2DS_30_Contagemresultado_datacnt2 ,
                                             DateTime AV195ExtraWWContagemResultadoExtraSelectionVs2DS_31_Contagemresultado_datacnt_to2 ,
                                             int AV203ExtraWWContagemResultadoExtraSelectionVs2DS_39_Contagemresultado_contadorfm2 ,
                                             String AV214ExtraWWContagemResultadoExtraSelectionVs2DS_50_Contagemresultado_tempndhmlg2 ,
                                             DateTime AV219ExtraWWContagemResultadoExtraSelectionVs2DS_55_Contagemresultado_datacnt3 ,
                                             DateTime AV220ExtraWWContagemResultadoExtraSelectionVs2DS_56_Contagemresultado_datacnt_to3 ,
                                             int AV228ExtraWWContagemResultadoExtraSelectionVs2DS_64_Contagemresultado_contadorfm3 ,
                                             String AV239ExtraWWContagemResultadoExtraSelectionVs2DS_75_Contagemresultado_tempndhmlg3 ,
                                             DateTime AV244ExtraWWContagemResultadoExtraSelectionVs2DS_80_Contagemresultado_datacnt4 ,
                                             DateTime AV245ExtraWWContagemResultadoExtraSelectionVs2DS_81_Contagemresultado_datacnt_to4 ,
                                             int AV253ExtraWWContagemResultadoExtraSelectionVs2DS_89_Contagemresultado_contadorfm4 ,
                                             String AV264ExtraWWContagemResultadoExtraSelectionVs2DS_100_Contagemresultado_tempndhmlg4 ,
                                             DateTime AV269ExtraWWContagemResultadoExtraSelectionVs2DS_105_Contagemresultado_datacnt5 ,
                                             DateTime AV270ExtraWWContagemResultadoExtraSelectionVs2DS_106_Contagemresultado_datacnt_to5 ,
                                             int AV278ExtraWWContagemResultadoExtraSelectionVs2DS_114_Contagemresultado_contadorfm5 ,
                                             String AV289ExtraWWContagemResultadoExtraSelectionVs2DS_125_Contagemresultado_tempndhmlg5 ,
                                             decimal AV294ExtraWWContagemResultadoExtraSelectionVs2DS_130_Tfcontagemresultado_pffinal ,
                                             decimal A574ContagemResultado_PFFinal ,
                                             decimal AV295ExtraWWContagemResultadoExtraSelectionVs2DS_131_Tfcontagemresultado_pffinal_to ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [191] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T4.[Contratada_AreaTrabalhoCod], T3.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T1.[ContagemResultado_ValorPF], T2.[Contrato_Codigo] AS ContagemResultado_CntCod, T1.[ContagemResultado_Descricao], T1.[ContagemResultado_Agrupador], T1.[ContagemResultado_NaoCnfDmnCod], T1.[ContagemResultado_Baseline], T3.[ServicoGrupo_Codigo] AS ContagemResultado_ServicoGrupo, T1.[ContagemResultado_SistemaCod], T1.[ContagemResultado_Owner], T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_DataPrevista], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_VlrCnc], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, COALESCE( T5.[ContagemResultado_PFLFMUltima], 0) AS ContagemResultado_PFLFMUltima, COALESCE( T5.[ContagemResultado_PFLFSUltima], 0) AS ContagemResultado_PFLFSUltima, COALESCE( T5.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima, COALESCE( T5.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, COALESCE( T5.[ContagemResultado_ContadorFM], 0) AS ContagemResultado_ContadorFM, COALESCE( T5.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt, COALESCE( T5.[ContagemResultado_StatusUltCnt], 0) AS ContagemResultado_StatusUltCnt, T1.[ContagemResultado_Codigo] FROM (((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod])";
         scmdbuf = scmdbuf + " LEFT JOIN (SELECT MIN([ContagemResultado_PFLFM]) AS ContagemResultado_PFLFMUltima, [ContagemResultado_Codigo], MIN([ContagemResultado_PFLFS]) AS ContagemResultado_PFLFSUltima, MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima, MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima, MIN([ContagemResultado_ContadorFMCod]) AS ContagemResultado_ContadorFM, MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, MIN([ContagemResultado_StatusCnt]) AS ContagemResultado_StatusUltCnt FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T5 ON T5.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo])";
         scmdbuf = scmdbuf + " WHERE (Not ( Not ( @AV171ExtraWWContagemResultadoExtraSelectionVs2DS_7_Contagemresultado_statusdmn1 = 'N' or @AV196ExtraWWContagemResultadoExtraSelectionVs2DS_32_Contagemresultado_statusdmn2 = 'N' or @AV221ExtraWWContagemResultadoExtraSelectionVs2DS_57_Contagemresultado_statusdmn3 = 'N' or @AV246ExtraWWContagemResultadoExtraSelectionVs2DS_82_Contagemresultado_statusdmn4 = 'N' or @AV271ExtraWWContagemResultadoExtraSelectionVs2DS_107_Contagemresultado_statusdmn5 = 'N')) or ( COALESCE( T5.[ContagemResultado_StatusUltCnt], 0) = 5 or T1.[ContagemResultado_VlrCnc] > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV167ExtraWWContagemResultadoExtraSelectionVs2DS_3_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV169ExtraWWContagemResultadoExtraSelectionVs2DS_5_Contagemresultado_datacnt1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T5.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV169ExtraWWContagemResultadoExtraSelectionVs2DS_5_Contagemresultado_datacnt1))";
         scmdbuf = scmdbuf + " and (Not ( @AV167ExtraWWContagemResultadoExtraSelectionVs2DS_3_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV170ExtraWWContagemResultadoExtraSelectionVs2DS_6_Contagemresultado_datacnt_to1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T5.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV170ExtraWWContagemResultadoExtraSelectionVs2DS_6_Contagemresultado_datacnt_to1))";
         scmdbuf = scmdbuf + " and (Not ( @AV167ExtraWWContagemResultadoExtraSelectionVs2DS_3_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV178ExtraWWContagemResultadoExtraSelectionVs2DS_14_Contagemresultado_contadorfm1 = convert(int, 0)))) or ( COALESCE( T5.[ContagemResultado_ContadorFM], 0) = @AV178ExtraWWContagemResultadoExtraSelectionVs2DS_14_Contagemresultado_contadorfm1 or ( (COALESCE( T5.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV178ExtraWWContagemResultadoExtraSelectionVs2DS_14_Contagemresultado_contadorfm1)))";
         scmdbuf = scmdbuf + " and (@AV167ExtraWWContagemResultadoExtraSelectionVs2DS_3_Dynamicfiltersselector1 <> 'CONTAGEMRESULTADO_PFBFM' or ( COALESCE( T5.[ContagemResultado_PFBFSUltima], 0) <> COALESCE( T5.[ContagemResultado_PFBFMUltima], 0) and COALESCE( T5.[ContagemResultado_PFBFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (@AV167ExtraWWContagemResultadoExtraSelectionVs2DS_3_Dynamicfiltersselector1 <> 'CONTAGEMRESULTADO_PFBFS' or ( COALESCE( T5.[ContagemResultado_PFLFSUltima], 0) <> COALESCE( T5.[ContagemResultado_PFLFMUltima], 0) and COALESCE( T5.[ContagemResultado_PFLFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV191ExtraWWContagemResultadoExtraSelectionVs2DS_27_Dynamicfiltersenabled2 = 1 and @AV192ExtraWWContagemResultadoExtraSelectionVs2DS_28_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV194ExtraWWContagemResultadoExtraSelectionVs2DS_30_Contagemresultado_datacnt2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T5.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV194ExtraWWContagemResultadoExtraSelectionVs2DS_30_Contagemresultado_datacnt2))";
         scmdbuf = scmdbuf + " and (Not ( @AV191ExtraWWContagemResultadoExtraSelectionVs2DS_27_Dynamicfiltersenabled2 = 1 and @AV192ExtraWWContagemResultadoExtraSelectionVs2DS_28_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV195ExtraWWContagemResultadoExtraSelectionVs2DS_31_Contagemresultado_datacnt_to2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T5.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV195ExtraWWContagemResultadoExtraSelectionVs2DS_31_Contagemresultado_datacnt_to2))";
         scmdbuf = scmdbuf + " and (Not ( @AV191ExtraWWContagemResultadoExtraSelectionVs2DS_27_Dynamicfiltersenabled2 = 1 and @AV192ExtraWWContagemResultadoExtraSelectionVs2DS_28_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV203ExtraWWContagemResultadoExtraSelectionVs2DS_39_Contagemresultado_contadorfm2 = convert(int, 0)))) or ( COALESCE( T5.[ContagemResultado_ContadorFM], 0) = @AV203ExtraWWContagemResultadoExtraSelectionVs2DS_39_Contagemresultado_contadorfm2 or ( (COALESCE( T5.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV203ExtraWWContagemResultadoExtraSelectionVs2DS_39_Contagemresultado_contadorfm2)))";
         scmdbuf = scmdbuf + " and (Not ( @AV191ExtraWWContagemResultadoExtraSelectionVs2DS_27_Dynamicfiltersenabled2 = 1 and @AV192ExtraWWContagemResultadoExtraSelectionVs2DS_28_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_PFBFM') or ( COALESCE( T5.[ContagemResultado_PFBFSUltima], 0) <> COALESCE( T5.[ContagemResultado_PFBFMUltima], 0) and COALESCE( T5.[ContagemResultado_PFBFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV191ExtraWWContagemResultadoExtraSelectionVs2DS_27_Dynamicfiltersenabled2 = 1 and @AV192ExtraWWContagemResultadoExtraSelectionVs2DS_28_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_PFBFS') or ( COALESCE( T5.[ContagemResultado_PFLFSUltima], 0) <> COALESCE( T5.[ContagemResultado_PFLFMUltima], 0) and COALESCE( T5.[ContagemResultado_PFLFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV216ExtraWWContagemResultadoExtraSelectionVs2DS_52_Dynamicfiltersenabled3 = 1 and @AV217ExtraWWContagemResultadoExtraSelectionVs2DS_53_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV219ExtraWWContagemResultadoExtraSelectionVs2DS_55_Contagemresultado_datacnt3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T5.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV219ExtraWWContagemResultadoExtraSelectionVs2DS_55_Contagemresultado_datacnt3))";
         scmdbuf = scmdbuf + " and (Not ( @AV216ExtraWWContagemResultadoExtraSelectionVs2DS_52_Dynamicfiltersenabled3 = 1 and @AV217ExtraWWContagemResultadoExtraSelectionVs2DS_53_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV220ExtraWWContagemResultadoExtraSelectionVs2DS_56_Contagemresultado_datacnt_to3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T5.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV220ExtraWWContagemResultadoExtraSelectionVs2DS_56_Contagemresultado_datacnt_to3))";
         scmdbuf = scmdbuf + " and (Not ( @AV216ExtraWWContagemResultadoExtraSelectionVs2DS_52_Dynamicfiltersenabled3 = 1 and @AV217ExtraWWContagemResultadoExtraSelectionVs2DS_53_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV228ExtraWWContagemResultadoExtraSelectionVs2DS_64_Contagemresultado_contadorfm3 = convert(int, 0)))) or ( COALESCE( T5.[ContagemResultado_ContadorFM], 0) = @AV228ExtraWWContagemResultadoExtraSelectionVs2DS_64_Contagemresultado_contadorfm3 or ( (COALESCE( T5.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV228ExtraWWContagemResultadoExtraSelectionVs2DS_64_Contagemresultado_contadorfm3)))";
         scmdbuf = scmdbuf + " and (Not ( @AV216ExtraWWContagemResultadoExtraSelectionVs2DS_52_Dynamicfiltersenabled3 = 1 and @AV217ExtraWWContagemResultadoExtraSelectionVs2DS_53_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_PFBFM') or ( COALESCE( T5.[ContagemResultado_PFBFSUltima], 0) <> COALESCE( T5.[ContagemResultado_PFBFMUltima], 0) and COALESCE( T5.[ContagemResultado_PFBFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV216ExtraWWContagemResultadoExtraSelectionVs2DS_52_Dynamicfiltersenabled3 = 1 and @AV217ExtraWWContagemResultadoExtraSelectionVs2DS_53_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_PFBFS') or ( COALESCE( T5.[ContagemResultado_PFLFSUltima], 0) <> COALESCE( T5.[ContagemResultado_PFLFMUltima], 0) and COALESCE( T5.[ContagemResultado_PFLFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV241ExtraWWContagemResultadoExtraSelectionVs2DS_77_Dynamicfiltersenabled4 = 1 and @AV242ExtraWWContagemResultadoExtraSelectionVs2DS_78_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV244ExtraWWContagemResultadoExtraSelectionVs2DS_80_Contagemresultado_datacnt4 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T5.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV244ExtraWWContagemResultadoExtraSelectionVs2DS_80_Contagemresultado_datacnt4))";
         scmdbuf = scmdbuf + " and (Not ( @AV241ExtraWWContagemResultadoExtraSelectionVs2DS_77_Dynamicfiltersenabled4 = 1 and @AV242ExtraWWContagemResultadoExtraSelectionVs2DS_78_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV245ExtraWWContagemResultadoExtraSelectionVs2DS_81_Contagemresultado_datacnt_to4 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T5.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV245ExtraWWContagemResultadoExtraSelectionVs2DS_81_Contagemresultado_datacnt_to4))";
         scmdbuf = scmdbuf + " and (Not ( @AV241ExtraWWContagemResultadoExtraSelectionVs2DS_77_Dynamicfiltersenabled4 = 1 and @AV242ExtraWWContagemResultadoExtraSelectionVs2DS_78_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV253ExtraWWContagemResultadoExtraSelectionVs2DS_89_Contagemresultado_contadorfm4 = convert(int, 0)))) or ( COALESCE( T5.[ContagemResultado_ContadorFM], 0) = @AV253ExtraWWContagemResultadoExtraSelectionVs2DS_89_Contagemresultado_contadorfm4 or ( (COALESCE( T5.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV253ExtraWWContagemResultadoExtraSelectionVs2DS_89_Contagemresultado_contadorfm4)))";
         scmdbuf = scmdbuf + " and (Not ( @AV241ExtraWWContagemResultadoExtraSelectionVs2DS_77_Dynamicfiltersenabled4 = 1 and @AV242ExtraWWContagemResultadoExtraSelectionVs2DS_78_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_PFBFM') or ( COALESCE( T5.[ContagemResultado_PFBFSUltima], 0) <> COALESCE( T5.[ContagemResultado_PFBFMUltima], 0) and COALESCE( T5.[ContagemResultado_PFBFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV241ExtraWWContagemResultadoExtraSelectionVs2DS_77_Dynamicfiltersenabled4 = 1 and @AV242ExtraWWContagemResultadoExtraSelectionVs2DS_78_Dynamicfiltersselector4 = 'CONTAGEMRESULTADO_PFBFS') or ( COALESCE( T5.[ContagemResultado_PFLFSUltima], 0) <> COALESCE( T5.[ContagemResultado_PFLFMUltima], 0) and COALESCE( T5.[ContagemResultado_PFLFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV266ExtraWWContagemResultadoExtraSelectionVs2DS_102_Dynamicfiltersenabled5 = 1 and @AV267ExtraWWContagemResultadoExtraSelectionVs2DS_103_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV269ExtraWWContagemResultadoExtraSelectionVs2DS_105_Contagemresultado_datacnt5 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T5.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV269ExtraWWContagemResultadoExtraSelectionVs2DS_105_Contagemresultado_datacnt5))";
         scmdbuf = scmdbuf + " and (Not ( @AV266ExtraWWContagemResultadoExtraSelectionVs2DS_102_Dynamicfiltersenabled5 = 1 and @AV267ExtraWWContagemResultadoExtraSelectionVs2DS_103_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_DATACNT' and ( Not (@AV270ExtraWWContagemResultadoExtraSelectionVs2DS_106_Contagemresultado_datacnt_to5 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T5.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV270ExtraWWContagemResultadoExtraSelectionVs2DS_106_Contagemresultado_datacnt_to5))";
         scmdbuf = scmdbuf + " and (Not ( @AV266ExtraWWContagemResultadoExtraSelectionVs2DS_102_Dynamicfiltersenabled5 = 1 and @AV267ExtraWWContagemResultadoExtraSelectionVs2DS_103_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV278ExtraWWContagemResultadoExtraSelectionVs2DS_114_Contagemresultado_contadorfm5 = convert(int, 0)))) or ( COALESCE( T5.[ContagemResultado_ContadorFM], 0) = @AV278ExtraWWContagemResultadoExtraSelectionVs2DS_114_Contagemresultado_contadorfm5 or ( (COALESCE( T5.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T1.[ContagemResultado_Owner] = @AV278ExtraWWContagemResultadoExtraSelectionVs2DS_114_Contagemresultado_contadorfm5)))";
         scmdbuf = scmdbuf + " and (Not ( @AV266ExtraWWContagemResultadoExtraSelectionVs2DS_102_Dynamicfiltersenabled5 = 1 and @AV267ExtraWWContagemResultadoExtraSelectionVs2DS_103_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_PFBFM') or ( COALESCE( T5.[ContagemResultado_PFBFSUltima], 0) <> COALESCE( T5.[ContagemResultado_PFBFMUltima], 0) and COALESCE( T5.[ContagemResultado_PFBFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (Not ( @AV266ExtraWWContagemResultadoExtraSelectionVs2DS_102_Dynamicfiltersenabled5 = 1 and @AV267ExtraWWContagemResultadoExtraSelectionVs2DS_103_Dynamicfiltersselector5 = 'CONTAGEMRESULTADO_PFBFS') or ( COALESCE( T5.[ContagemResultado_PFLFSUltima], 0) <> COALESCE( T5.[ContagemResultado_PFLFMUltima], 0) and COALESCE( T5.[ContagemResultado_PFLFSUltima], 0) > 0))";
         scmdbuf = scmdbuf + " and (T4.[Contratada_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( AV9WWPContext_gxTpr_Contratada_codigo > 0 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV9WWPCo_2Contratada_codigo)";
         }
         else
         {
            GXv_int3[86] = 1;
         }
         if ( ( StringUtil.StrCmp(AV167ExtraWWContagemResultadoExtraSelectionVs2DS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV171ExtraWWContagemResultadoExtraSelectionVs2DS_7_Contagemresultado_statusdmn1)) && ! ( StringUtil.StrCmp(AV171ExtraWWContagemResultadoExtraSelectionVs2DS_7_Contagemresultado_statusdmn1, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV171ExtraWWContagemResultadoExtraSelectionVs2DS_7_Contagemresultado_statusdmn1)";
         }
         else
         {
            GXv_int3[87] = 1;
         }
         if ( ( StringUtil.StrCmp(AV167ExtraWWContagemResultadoExtraSelectionVs2DS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV171ExtraWWContagemResultadoExtraSelectionVs2DS_7_Contagemresultado_statusdmn1, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L' or T1.[ContagemResultado_StatusDmn] = 'X'))";
         }
         if ( ( StringUtil.StrCmp(AV167ExtraWWContagemResultadoExtraSelectionVs2DS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV168ExtraWWContagemResultadoExtraSelectionVs2DS_4_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV172ExtraWWContagemResultadoExtraSelectionVs2DS_8_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV172ExtraWWContagemResultadoExtraSelectionVs2DS_8_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] = @AV172ExtraWWContagemResultadoExtraSelectionVs2DS_8_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int3[88] = 1;
            GXv_int3[89] = 1;
         }
         if ( ( StringUtil.StrCmp(AV167ExtraWWContagemResultadoExtraSelectionVs2DS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV168ExtraWWContagemResultadoExtraSelectionVs2DS_4_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV172ExtraWWContagemResultadoExtraSelectionVs2DS_8_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV172ExtraWWContagemResultadoExtraSelectionVs2DS_8_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] like @lV172ExtraWWContagemResultadoExtraSelectionVs2DS_8_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int3[90] = 1;
            GXv_int3[91] = 1;
         }
         if ( ( StringUtil.StrCmp(AV167ExtraWWContagemResultadoExtraSelectionVs2DS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV168ExtraWWContagemResultadoExtraSelectionVs2DS_4_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV172ExtraWWContagemResultadoExtraSelectionVs2DS_8_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV172ExtraWWContagemResultadoExtraSelectionVs2DS_8_Contagemresultado_osfsosfm1 or T1.[ContagemResultado_DemandaFM] like '%' + @lV172ExtraWWContagemResultadoExtraSelectionVs2DS_8_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int3[92] = 1;
            GXv_int3[93] = 1;
         }
         if ( ( StringUtil.StrCmp(AV167ExtraWWContagemResultadoExtraSelectionVs2DS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV173ExtraWWContagemResultadoExtraSelectionVs2DS_9_Contagemresultado_datadmn1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV173ExtraWWContagemResultadoExtraSelectionVs2DS_9_Contagemresultado_datadmn1)";
         }
         else
         {
            GXv_int3[94] = 1;
         }
         if ( ( StringUtil.StrCmp(AV167ExtraWWContagemResultadoExtraSelectionVs2DS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV174ExtraWWContagemResultadoExtraSelectionVs2DS_10_Contagemresultado_datadmn_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV174ExtraWWContagemResultadoExtraSelectionVs2DS_10_Contagemresultado_datadmn_to1)";
         }
         else
         {
            GXv_int3[95] = 1;
         }
         if ( ( StringUtil.StrCmp(AV167ExtraWWContagemResultadoExtraSelectionVs2DS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV175ExtraWWContagemResultadoExtraSelectionVs2DS_11_Contagemresultado_dataprevista1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV175ExtraWWContagemResultadoExtraSelectionVs2DS_11_Contagemresultado_dataprevista1)";
         }
         else
         {
            GXv_int3[96] = 1;
         }
         if ( ( StringUtil.StrCmp(AV167ExtraWWContagemResultadoExtraSelectionVs2DS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV176ExtraWWContagemResultadoExtraSelectionVs2DS_12_Contagemresultado_dataprevista_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] < DATEADD( dd,1, @AV176ExtraWWContagemResultadoExtraSelectionVs2DS_12_Contagemresultado_dataprevista_to1))";
         }
         else
         {
            GXv_int3[97] = 1;
         }
         if ( ( StringUtil.StrCmp(AV167ExtraWWContagemResultadoExtraSelectionVs2DS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV177ExtraWWContagemResultadoExtraSelectionVs2DS_13_Contagemresultado_servico1) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV177ExtraWWContagemResultadoExtraSelectionVs2DS_13_Contagemresultado_servico1)";
         }
         else
         {
            GXv_int3[98] = 1;
         }
         if ( ( StringUtil.StrCmp(AV167ExtraWWContagemResultadoExtraSelectionVs2DS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV179ExtraWWContagemResultadoExtraSelectionVs2DS_15_Contagemresultado_sistemacod1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV179ExtraWWContagemResultadoExtraSelectionVs2DS_15_Contagemresultado_sistemacod1)";
         }
         else
         {
            GXv_int3[99] = 1;
         }
         if ( ( StringUtil.StrCmp(AV167ExtraWWContagemResultadoExtraSelectionVs2DS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV180ExtraWWContagemResultadoExtraSelectionVs2DS_16_Contagemresultado_contratadacod1 > 0 ) && ( AV165ExtraWWContagemResultadoExtraSelectionVs2DS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV180ExtraWWContagemResultadoExtraSelectionVs2DS_16_Contagemresultado_contratadacod1)";
         }
         else
         {
            GXv_int3[100] = 1;
         }
         if ( ( StringUtil.StrCmp(AV167ExtraWWContagemResultadoExtraSelectionVs2DS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 ) && ( ! (0==AV181ExtraWWContagemResultadoExtraSelectionVs2DS_17_Contagemresultado_servicogrupo1) ) )
         {
            sWhereString = sWhereString + " and (T3.[ServicoGrupo_Codigo] = @AV181ExtraWWContagemResultadoExtraSelectionVs2DS_17_Contagemresultado_servicogrupo1)";
         }
         else
         {
            GXv_int3[101] = 1;
         }
         if ( ( StringUtil.StrCmp(AV167ExtraWWContagemResultadoExtraSelectionVs2DS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV182ExtraWWContagemResultadoExtraSelectionVs2DS_18_Contagemresultado_baseline1, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( ( StringUtil.StrCmp(AV167ExtraWWContagemResultadoExtraSelectionVs2DS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV182ExtraWWContagemResultadoExtraSelectionVs2DS_18_Contagemresultado_baseline1, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[ContagemResultado_Baseline] = 1 or T1.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( ( StringUtil.StrCmp(AV167ExtraWWContagemResultadoExtraSelectionVs2DS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV183ExtraWWContagemResultadoExtraSelectionVs2DS_19_Contagemresultado_naocnfdmncod1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_NaoCnfDmnCod] = @AV183ExtraWWContagemResultadoExtraSelectionVs2DS_19_Contagemresultado_naocnfdmncod1)";
         }
         else
         {
            GXv_int3[102] = 1;
         }
         if ( ( StringUtil.StrCmp(AV167ExtraWWContagemResultadoExtraSelectionVs2DS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV186ExtraWWContagemResultadoExtraSelectionVs2DS_22_Contagemresultado_agrupador1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV186ExtraWWContagemResultadoExtraSelectionVs2DS_22_Contagemresultado_agrupador1)";
         }
         else
         {
            GXv_int3[103] = 1;
         }
         if ( ( StringUtil.StrCmp(AV167ExtraWWContagemResultadoExtraSelectionVs2DS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV187ExtraWWContagemResultadoExtraSelectionVs2DS_23_Contagemresultado_descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV187ExtraWWContagemResultadoExtraSelectionVs2DS_23_Contagemresultado_descricao1 + '%')";
         }
         else
         {
            GXv_int3[104] = 1;
         }
         if ( ( StringUtil.StrCmp(AV167ExtraWWContagemResultadoExtraSelectionVs2DS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV188ExtraWWContagemResultadoExtraSelectionVs2DS_24_Contagemresultado_owner1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Owner] = @AV188ExtraWWContagemResultadoExtraSelectionVs2DS_24_Contagemresultado_owner1)";
         }
         else
         {
            GXv_int3[105] = 1;
         }
         if ( ( StringUtil.StrCmp(AV167ExtraWWContagemResultadoExtraSelectionVs2DS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_CNTCOD") == 0 ) && ( ! (0==AV190ExtraWWContagemResultadoExtraSelectionVs2DS_26_Contagemresultado_cntcod1) ) )
         {
            sWhereString = sWhereString + " and (T2.[Contrato_Codigo] = @AV190ExtraWWContagemResultadoExtraSelectionVs2DS_26_Contagemresultado_cntcod1)";
         }
         else
         {
            GXv_int3[106] = 1;
         }
         if ( AV191ExtraWWContagemResultadoExtraSelectionVs2DS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV192ExtraWWContagemResultadoExtraSelectionVs2DS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV196ExtraWWContagemResultadoExtraSelectionVs2DS_32_Contagemresultado_statusdmn2)) && ! ( StringUtil.StrCmp(AV196ExtraWWContagemResultadoExtraSelectionVs2DS_32_Contagemresultado_statusdmn2, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV196ExtraWWContagemResultadoExtraSelectionVs2DS_32_Contagemresultado_statusdmn2)";
         }
         else
         {
            GXv_int3[107] = 1;
         }
         if ( AV191ExtraWWContagemResultadoExtraSelectionVs2DS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV192ExtraWWContagemResultadoExtraSelectionVs2DS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV196ExtraWWContagemResultadoExtraSelectionVs2DS_32_Contagemresultado_statusdmn2, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L' or T1.[ContagemResultado_StatusDmn] = 'X'))";
         }
         if ( AV191ExtraWWContagemResultadoExtraSelectionVs2DS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV192ExtraWWContagemResultadoExtraSelectionVs2DS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV193ExtraWWContagemResultadoExtraSelectionVs2DS_29_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV197ExtraWWContagemResultadoExtraSelectionVs2DS_33_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV197ExtraWWContagemResultadoExtraSelectionVs2DS_33_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] = @AV197ExtraWWContagemResultadoExtraSelectionVs2DS_33_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int3[108] = 1;
            GXv_int3[109] = 1;
         }
         if ( AV191ExtraWWContagemResultadoExtraSelectionVs2DS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV192ExtraWWContagemResultadoExtraSelectionVs2DS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV193ExtraWWContagemResultadoExtraSelectionVs2DS_29_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV197ExtraWWContagemResultadoExtraSelectionVs2DS_33_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV197ExtraWWContagemResultadoExtraSelectionVs2DS_33_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] like @lV197ExtraWWContagemResultadoExtraSelectionVs2DS_33_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int3[110] = 1;
            GXv_int3[111] = 1;
         }
         if ( AV191ExtraWWContagemResultadoExtraSelectionVs2DS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV192ExtraWWContagemResultadoExtraSelectionVs2DS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV193ExtraWWContagemResultadoExtraSelectionVs2DS_29_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV197ExtraWWContagemResultadoExtraSelectionVs2DS_33_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV197ExtraWWContagemResultadoExtraSelectionVs2DS_33_Contagemresultado_osfsosfm2 or T1.[ContagemResultado_DemandaFM] like '%' + @lV197ExtraWWContagemResultadoExtraSelectionVs2DS_33_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int3[112] = 1;
            GXv_int3[113] = 1;
         }
         if ( AV191ExtraWWContagemResultadoExtraSelectionVs2DS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV192ExtraWWContagemResultadoExtraSelectionVs2DS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV198ExtraWWContagemResultadoExtraSelectionVs2DS_34_Contagemresultado_datadmn2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV198ExtraWWContagemResultadoExtraSelectionVs2DS_34_Contagemresultado_datadmn2)";
         }
         else
         {
            GXv_int3[114] = 1;
         }
         if ( AV191ExtraWWContagemResultadoExtraSelectionVs2DS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV192ExtraWWContagemResultadoExtraSelectionVs2DS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV199ExtraWWContagemResultadoExtraSelectionVs2DS_35_Contagemresultado_datadmn_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV199ExtraWWContagemResultadoExtraSelectionVs2DS_35_Contagemresultado_datadmn_to2)";
         }
         else
         {
            GXv_int3[115] = 1;
         }
         if ( AV191ExtraWWContagemResultadoExtraSelectionVs2DS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV192ExtraWWContagemResultadoExtraSelectionVs2DS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV200ExtraWWContagemResultadoExtraSelectionVs2DS_36_Contagemresultado_dataprevista2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV200ExtraWWContagemResultadoExtraSelectionVs2DS_36_Contagemresultado_dataprevista2)";
         }
         else
         {
            GXv_int3[116] = 1;
         }
         if ( AV191ExtraWWContagemResultadoExtraSelectionVs2DS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV192ExtraWWContagemResultadoExtraSelectionVs2DS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV201ExtraWWContagemResultadoExtraSelectionVs2DS_37_Contagemresultado_dataprevista_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] < DATEADD( dd,1, @AV201ExtraWWContagemResultadoExtraSelectionVs2DS_37_Contagemresultado_dataprevista_to2))";
         }
         else
         {
            GXv_int3[117] = 1;
         }
         if ( AV191ExtraWWContagemResultadoExtraSelectionVs2DS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV192ExtraWWContagemResultadoExtraSelectionVs2DS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV202ExtraWWContagemResultadoExtraSelectionVs2DS_38_Contagemresultado_servico2) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV202ExtraWWContagemResultadoExtraSelectionVs2DS_38_Contagemresultado_servico2)";
         }
         else
         {
            GXv_int3[118] = 1;
         }
         if ( AV191ExtraWWContagemResultadoExtraSelectionVs2DS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV192ExtraWWContagemResultadoExtraSelectionVs2DS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV204ExtraWWContagemResultadoExtraSelectionVs2DS_40_Contagemresultado_sistemacod2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV204ExtraWWContagemResultadoExtraSelectionVs2DS_40_Contagemresultado_sistemacod2)";
         }
         else
         {
            GXv_int3[119] = 1;
         }
         if ( AV191ExtraWWContagemResultadoExtraSelectionVs2DS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV192ExtraWWContagemResultadoExtraSelectionVs2DS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV205ExtraWWContagemResultadoExtraSelectionVs2DS_41_Contagemresultado_contratadacod2 > 0 ) && ( AV165ExtraWWContagemResultadoExtraSelectionVs2DS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV205ExtraWWContagemResultadoExtraSelectionVs2DS_41_Contagemresultado_contratadacod2)";
         }
         else
         {
            GXv_int3[120] = 1;
         }
         if ( AV191ExtraWWContagemResultadoExtraSelectionVs2DS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV192ExtraWWContagemResultadoExtraSelectionVs2DS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 ) && ( ! (0==AV206ExtraWWContagemResultadoExtraSelectionVs2DS_42_Contagemresultado_servicogrupo2) ) )
         {
            sWhereString = sWhereString + " and (T3.[ServicoGrupo_Codigo] = @AV206ExtraWWContagemResultadoExtraSelectionVs2DS_42_Contagemresultado_servicogrupo2)";
         }
         else
         {
            GXv_int3[121] = 1;
         }
         if ( AV191ExtraWWContagemResultadoExtraSelectionVs2DS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV192ExtraWWContagemResultadoExtraSelectionVs2DS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV207ExtraWWContagemResultadoExtraSelectionVs2DS_43_Contagemresultado_baseline2, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV191ExtraWWContagemResultadoExtraSelectionVs2DS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV192ExtraWWContagemResultadoExtraSelectionVs2DS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV207ExtraWWContagemResultadoExtraSelectionVs2DS_43_Contagemresultado_baseline2, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[ContagemResultado_Baseline] = 1 or T1.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV191ExtraWWContagemResultadoExtraSelectionVs2DS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV192ExtraWWContagemResultadoExtraSelectionVs2DS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV208ExtraWWContagemResultadoExtraSelectionVs2DS_44_Contagemresultado_naocnfdmncod2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_NaoCnfDmnCod] = @AV208ExtraWWContagemResultadoExtraSelectionVs2DS_44_Contagemresultado_naocnfdmncod2)";
         }
         else
         {
            GXv_int3[122] = 1;
         }
         if ( AV191ExtraWWContagemResultadoExtraSelectionVs2DS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV192ExtraWWContagemResultadoExtraSelectionVs2DS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV211ExtraWWContagemResultadoExtraSelectionVs2DS_47_Contagemresultado_agrupador2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV211ExtraWWContagemResultadoExtraSelectionVs2DS_47_Contagemresultado_agrupador2)";
         }
         else
         {
            GXv_int3[123] = 1;
         }
         if ( AV191ExtraWWContagemResultadoExtraSelectionVs2DS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV192ExtraWWContagemResultadoExtraSelectionVs2DS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV212ExtraWWContagemResultadoExtraSelectionVs2DS_48_Contagemresultado_descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV212ExtraWWContagemResultadoExtraSelectionVs2DS_48_Contagemresultado_descricao2 + '%')";
         }
         else
         {
            GXv_int3[124] = 1;
         }
         if ( AV191ExtraWWContagemResultadoExtraSelectionVs2DS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV192ExtraWWContagemResultadoExtraSelectionVs2DS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV213ExtraWWContagemResultadoExtraSelectionVs2DS_49_Contagemresultado_owner2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Owner] = @AV213ExtraWWContagemResultadoExtraSelectionVs2DS_49_Contagemresultado_owner2)";
         }
         else
         {
            GXv_int3[125] = 1;
         }
         if ( AV191ExtraWWContagemResultadoExtraSelectionVs2DS_27_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV192ExtraWWContagemResultadoExtraSelectionVs2DS_28_Dynamicfiltersselector2, "CONTAGEMRESULTADO_CNTCOD") == 0 ) && ( ! (0==AV215ExtraWWContagemResultadoExtraSelectionVs2DS_51_Contagemresultado_cntcod2) ) )
         {
            sWhereString = sWhereString + " and (T2.[Contrato_Codigo] = @AV215ExtraWWContagemResultadoExtraSelectionVs2DS_51_Contagemresultado_cntcod2)";
         }
         else
         {
            GXv_int3[126] = 1;
         }
         if ( AV216ExtraWWContagemResultadoExtraSelectionVs2DS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV217ExtraWWContagemResultadoExtraSelectionVs2DS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV221ExtraWWContagemResultadoExtraSelectionVs2DS_57_Contagemresultado_statusdmn3)) && ! ( StringUtil.StrCmp(AV221ExtraWWContagemResultadoExtraSelectionVs2DS_57_Contagemresultado_statusdmn3, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV221ExtraWWContagemResultadoExtraSelectionVs2DS_57_Contagemresultado_statusdmn3)";
         }
         else
         {
            GXv_int3[127] = 1;
         }
         if ( AV216ExtraWWContagemResultadoExtraSelectionVs2DS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV217ExtraWWContagemResultadoExtraSelectionVs2DS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV221ExtraWWContagemResultadoExtraSelectionVs2DS_57_Contagemresultado_statusdmn3, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L' or T1.[ContagemResultado_StatusDmn] = 'X'))";
         }
         if ( AV216ExtraWWContagemResultadoExtraSelectionVs2DS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV217ExtraWWContagemResultadoExtraSelectionVs2DS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV218ExtraWWContagemResultadoExtraSelectionVs2DS_54_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV222ExtraWWContagemResultadoExtraSelectionVs2DS_58_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV222ExtraWWContagemResultadoExtraSelectionVs2DS_58_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] = @AV222ExtraWWContagemResultadoExtraSelectionVs2DS_58_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int3[128] = 1;
            GXv_int3[129] = 1;
         }
         if ( AV216ExtraWWContagemResultadoExtraSelectionVs2DS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV217ExtraWWContagemResultadoExtraSelectionVs2DS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV218ExtraWWContagemResultadoExtraSelectionVs2DS_54_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV222ExtraWWContagemResultadoExtraSelectionVs2DS_58_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV222ExtraWWContagemResultadoExtraSelectionVs2DS_58_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] like @lV222ExtraWWContagemResultadoExtraSelectionVs2DS_58_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int3[130] = 1;
            GXv_int3[131] = 1;
         }
         if ( AV216ExtraWWContagemResultadoExtraSelectionVs2DS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV217ExtraWWContagemResultadoExtraSelectionVs2DS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV218ExtraWWContagemResultadoExtraSelectionVs2DS_54_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV222ExtraWWContagemResultadoExtraSelectionVs2DS_58_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV222ExtraWWContagemResultadoExtraSelectionVs2DS_58_Contagemresultado_osfsosfm3 or T1.[ContagemResultado_DemandaFM] like '%' + @lV222ExtraWWContagemResultadoExtraSelectionVs2DS_58_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int3[132] = 1;
            GXv_int3[133] = 1;
         }
         if ( AV216ExtraWWContagemResultadoExtraSelectionVs2DS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV217ExtraWWContagemResultadoExtraSelectionVs2DS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV223ExtraWWContagemResultadoExtraSelectionVs2DS_59_Contagemresultado_datadmn3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV223ExtraWWContagemResultadoExtraSelectionVs2DS_59_Contagemresultado_datadmn3)";
         }
         else
         {
            GXv_int3[134] = 1;
         }
         if ( AV216ExtraWWContagemResultadoExtraSelectionVs2DS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV217ExtraWWContagemResultadoExtraSelectionVs2DS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV224ExtraWWContagemResultadoExtraSelectionVs2DS_60_Contagemresultado_datadmn_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV224ExtraWWContagemResultadoExtraSelectionVs2DS_60_Contagemresultado_datadmn_to3)";
         }
         else
         {
            GXv_int3[135] = 1;
         }
         if ( AV216ExtraWWContagemResultadoExtraSelectionVs2DS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV217ExtraWWContagemResultadoExtraSelectionVs2DS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV225ExtraWWContagemResultadoExtraSelectionVs2DS_61_Contagemresultado_dataprevista3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV225ExtraWWContagemResultadoExtraSelectionVs2DS_61_Contagemresultado_dataprevista3)";
         }
         else
         {
            GXv_int3[136] = 1;
         }
         if ( AV216ExtraWWContagemResultadoExtraSelectionVs2DS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV217ExtraWWContagemResultadoExtraSelectionVs2DS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV226ExtraWWContagemResultadoExtraSelectionVs2DS_62_Contagemresultado_dataprevista_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] < DATEADD( dd,1, @AV226ExtraWWContagemResultadoExtraSelectionVs2DS_62_Contagemresultado_dataprevista_to3))";
         }
         else
         {
            GXv_int3[137] = 1;
         }
         if ( AV216ExtraWWContagemResultadoExtraSelectionVs2DS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV217ExtraWWContagemResultadoExtraSelectionVs2DS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV227ExtraWWContagemResultadoExtraSelectionVs2DS_63_Contagemresultado_servico3) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV227ExtraWWContagemResultadoExtraSelectionVs2DS_63_Contagemresultado_servico3)";
         }
         else
         {
            GXv_int3[138] = 1;
         }
         if ( AV216ExtraWWContagemResultadoExtraSelectionVs2DS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV217ExtraWWContagemResultadoExtraSelectionVs2DS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV229ExtraWWContagemResultadoExtraSelectionVs2DS_65_Contagemresultado_sistemacod3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV229ExtraWWContagemResultadoExtraSelectionVs2DS_65_Contagemresultado_sistemacod3)";
         }
         else
         {
            GXv_int3[139] = 1;
         }
         if ( AV216ExtraWWContagemResultadoExtraSelectionVs2DS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV217ExtraWWContagemResultadoExtraSelectionVs2DS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV230ExtraWWContagemResultadoExtraSelectionVs2DS_66_Contagemresultado_contratadacod3 > 0 ) && ( AV165ExtraWWContagemResultadoExtraSelectionVs2DS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV230ExtraWWContagemResultadoExtraSelectionVs2DS_66_Contagemresultado_contratadacod3)";
         }
         else
         {
            GXv_int3[140] = 1;
         }
         if ( AV216ExtraWWContagemResultadoExtraSelectionVs2DS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV217ExtraWWContagemResultadoExtraSelectionVs2DS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 ) && ( ! (0==AV231ExtraWWContagemResultadoExtraSelectionVs2DS_67_Contagemresultado_servicogrupo3) ) )
         {
            sWhereString = sWhereString + " and (T3.[ServicoGrupo_Codigo] = @AV231ExtraWWContagemResultadoExtraSelectionVs2DS_67_Contagemresultado_servicogrupo3)";
         }
         else
         {
            GXv_int3[141] = 1;
         }
         if ( AV216ExtraWWContagemResultadoExtraSelectionVs2DS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV217ExtraWWContagemResultadoExtraSelectionVs2DS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV232ExtraWWContagemResultadoExtraSelectionVs2DS_68_Contagemresultado_baseline3, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV216ExtraWWContagemResultadoExtraSelectionVs2DS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV217ExtraWWContagemResultadoExtraSelectionVs2DS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV232ExtraWWContagemResultadoExtraSelectionVs2DS_68_Contagemresultado_baseline3, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[ContagemResultado_Baseline] = 1 or T1.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV216ExtraWWContagemResultadoExtraSelectionVs2DS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV217ExtraWWContagemResultadoExtraSelectionVs2DS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV233ExtraWWContagemResultadoExtraSelectionVs2DS_69_Contagemresultado_naocnfdmncod3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_NaoCnfDmnCod] = @AV233ExtraWWContagemResultadoExtraSelectionVs2DS_69_Contagemresultado_naocnfdmncod3)";
         }
         else
         {
            GXv_int3[142] = 1;
         }
         if ( AV216ExtraWWContagemResultadoExtraSelectionVs2DS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV217ExtraWWContagemResultadoExtraSelectionVs2DS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV236ExtraWWContagemResultadoExtraSelectionVs2DS_72_Contagemresultado_agrupador3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV236ExtraWWContagemResultadoExtraSelectionVs2DS_72_Contagemresultado_agrupador3)";
         }
         else
         {
            GXv_int3[143] = 1;
         }
         if ( AV216ExtraWWContagemResultadoExtraSelectionVs2DS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV217ExtraWWContagemResultadoExtraSelectionVs2DS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV237ExtraWWContagemResultadoExtraSelectionVs2DS_73_Contagemresultado_descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV237ExtraWWContagemResultadoExtraSelectionVs2DS_73_Contagemresultado_descricao3 + '%')";
         }
         else
         {
            GXv_int3[144] = 1;
         }
         if ( AV216ExtraWWContagemResultadoExtraSelectionVs2DS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV217ExtraWWContagemResultadoExtraSelectionVs2DS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV238ExtraWWContagemResultadoExtraSelectionVs2DS_74_Contagemresultado_owner3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Owner] = @AV238ExtraWWContagemResultadoExtraSelectionVs2DS_74_Contagemresultado_owner3)";
         }
         else
         {
            GXv_int3[145] = 1;
         }
         if ( AV216ExtraWWContagemResultadoExtraSelectionVs2DS_52_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV217ExtraWWContagemResultadoExtraSelectionVs2DS_53_Dynamicfiltersselector3, "CONTAGEMRESULTADO_CNTCOD") == 0 ) && ( ! (0==AV240ExtraWWContagemResultadoExtraSelectionVs2DS_76_Contagemresultado_cntcod3) ) )
         {
            sWhereString = sWhereString + " and (T2.[Contrato_Codigo] = @AV240ExtraWWContagemResultadoExtraSelectionVs2DS_76_Contagemresultado_cntcod3)";
         }
         else
         {
            GXv_int3[146] = 1;
         }
         if ( AV241ExtraWWContagemResultadoExtraSelectionVs2DS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV242ExtraWWContagemResultadoExtraSelectionVs2DS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV246ExtraWWContagemResultadoExtraSelectionVs2DS_82_Contagemresultado_statusdmn4)) && ! ( StringUtil.StrCmp(AV246ExtraWWContagemResultadoExtraSelectionVs2DS_82_Contagemresultado_statusdmn4, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV246ExtraWWContagemResultadoExtraSelectionVs2DS_82_Contagemresultado_statusdmn4)";
         }
         else
         {
            GXv_int3[147] = 1;
         }
         if ( AV241ExtraWWContagemResultadoExtraSelectionVs2DS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV242ExtraWWContagemResultadoExtraSelectionVs2DS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV246ExtraWWContagemResultadoExtraSelectionVs2DS_82_Contagemresultado_statusdmn4, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L' or T1.[ContagemResultado_StatusDmn] = 'X'))";
         }
         if ( AV241ExtraWWContagemResultadoExtraSelectionVs2DS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV242ExtraWWContagemResultadoExtraSelectionVs2DS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV243ExtraWWContagemResultadoExtraSelectionVs2DS_79_Dynamicfiltersoperator4 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV247ExtraWWContagemResultadoExtraSelectionVs2DS_83_Contagemresultado_osfsosfm4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV247ExtraWWContagemResultadoExtraSelectionVs2DS_83_Contagemresultado_osfsosfm4 or T1.[ContagemResultado_DemandaFM] = @AV247ExtraWWContagemResultadoExtraSelectionVs2DS_83_Contagemresultado_osfsosfm4)";
         }
         else
         {
            GXv_int3[148] = 1;
            GXv_int3[149] = 1;
         }
         if ( AV241ExtraWWContagemResultadoExtraSelectionVs2DS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV242ExtraWWContagemResultadoExtraSelectionVs2DS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV243ExtraWWContagemResultadoExtraSelectionVs2DS_79_Dynamicfiltersoperator4 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV247ExtraWWContagemResultadoExtraSelectionVs2DS_83_Contagemresultado_osfsosfm4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV247ExtraWWContagemResultadoExtraSelectionVs2DS_83_Contagemresultado_osfsosfm4 or T1.[ContagemResultado_DemandaFM] like @lV247ExtraWWContagemResultadoExtraSelectionVs2DS_83_Contagemresultado_osfsosfm4)";
         }
         else
         {
            GXv_int3[150] = 1;
            GXv_int3[151] = 1;
         }
         if ( AV241ExtraWWContagemResultadoExtraSelectionVs2DS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV242ExtraWWContagemResultadoExtraSelectionVs2DS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV243ExtraWWContagemResultadoExtraSelectionVs2DS_79_Dynamicfiltersoperator4 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV247ExtraWWContagemResultadoExtraSelectionVs2DS_83_Contagemresultado_osfsosfm4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV247ExtraWWContagemResultadoExtraSelectionVs2DS_83_Contagemresultado_osfsosfm4 or T1.[ContagemResultado_DemandaFM] like '%' + @lV247ExtraWWContagemResultadoExtraSelectionVs2DS_83_Contagemresultado_osfsosfm4)";
         }
         else
         {
            GXv_int3[152] = 1;
            GXv_int3[153] = 1;
         }
         if ( AV241ExtraWWContagemResultadoExtraSelectionVs2DS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV242ExtraWWContagemResultadoExtraSelectionVs2DS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV248ExtraWWContagemResultadoExtraSelectionVs2DS_84_Contagemresultado_datadmn4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV248ExtraWWContagemResultadoExtraSelectionVs2DS_84_Contagemresultado_datadmn4)";
         }
         else
         {
            GXv_int3[154] = 1;
         }
         if ( AV241ExtraWWContagemResultadoExtraSelectionVs2DS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV242ExtraWWContagemResultadoExtraSelectionVs2DS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV249ExtraWWContagemResultadoExtraSelectionVs2DS_85_Contagemresultado_datadmn_to4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV249ExtraWWContagemResultadoExtraSelectionVs2DS_85_Contagemresultado_datadmn_to4)";
         }
         else
         {
            GXv_int3[155] = 1;
         }
         if ( AV241ExtraWWContagemResultadoExtraSelectionVs2DS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV242ExtraWWContagemResultadoExtraSelectionVs2DS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV250ExtraWWContagemResultadoExtraSelectionVs2DS_86_Contagemresultado_dataprevista4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV250ExtraWWContagemResultadoExtraSelectionVs2DS_86_Contagemresultado_dataprevista4)";
         }
         else
         {
            GXv_int3[156] = 1;
         }
         if ( AV241ExtraWWContagemResultadoExtraSelectionVs2DS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV242ExtraWWContagemResultadoExtraSelectionVs2DS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV251ExtraWWContagemResultadoExtraSelectionVs2DS_87_Contagemresultado_dataprevista_to4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] < DATEADD( dd,1, @AV251ExtraWWContagemResultadoExtraSelectionVs2DS_87_Contagemresultado_dataprevista_to4))";
         }
         else
         {
            GXv_int3[157] = 1;
         }
         if ( AV241ExtraWWContagemResultadoExtraSelectionVs2DS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV242ExtraWWContagemResultadoExtraSelectionVs2DS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV252ExtraWWContagemResultadoExtraSelectionVs2DS_88_Contagemresultado_servico4) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV252ExtraWWContagemResultadoExtraSelectionVs2DS_88_Contagemresultado_servico4)";
         }
         else
         {
            GXv_int3[158] = 1;
         }
         if ( AV241ExtraWWContagemResultadoExtraSelectionVs2DS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV242ExtraWWContagemResultadoExtraSelectionVs2DS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV254ExtraWWContagemResultadoExtraSelectionVs2DS_90_Contagemresultado_sistemacod4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV254ExtraWWContagemResultadoExtraSelectionVs2DS_90_Contagemresultado_sistemacod4)";
         }
         else
         {
            GXv_int3[159] = 1;
         }
         if ( AV241ExtraWWContagemResultadoExtraSelectionVs2DS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV242ExtraWWContagemResultadoExtraSelectionVs2DS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV255ExtraWWContagemResultadoExtraSelectionVs2DS_91_Contagemresultado_contratadacod4 > 0 ) && ( AV165ExtraWWContagemResultadoExtraSelectionVs2DS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV255ExtraWWContagemResultadoExtraSelectionVs2DS_91_Contagemresultado_contratadacod4)";
         }
         else
         {
            GXv_int3[160] = 1;
         }
         if ( AV241ExtraWWContagemResultadoExtraSelectionVs2DS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV242ExtraWWContagemResultadoExtraSelectionVs2DS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 ) && ( ! (0==AV256ExtraWWContagemResultadoExtraSelectionVs2DS_92_Contagemresultado_servicogrupo4) ) )
         {
            sWhereString = sWhereString + " and (T3.[ServicoGrupo_Codigo] = @AV256ExtraWWContagemResultadoExtraSelectionVs2DS_92_Contagemresultado_servicogrupo4)";
         }
         else
         {
            GXv_int3[161] = 1;
         }
         if ( AV241ExtraWWContagemResultadoExtraSelectionVs2DS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV242ExtraWWContagemResultadoExtraSelectionVs2DS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV257ExtraWWContagemResultadoExtraSelectionVs2DS_93_Contagemresultado_baseline4, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV241ExtraWWContagemResultadoExtraSelectionVs2DS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV242ExtraWWContagemResultadoExtraSelectionVs2DS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV257ExtraWWContagemResultadoExtraSelectionVs2DS_93_Contagemresultado_baseline4, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[ContagemResultado_Baseline] = 1 or T1.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV241ExtraWWContagemResultadoExtraSelectionVs2DS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV242ExtraWWContagemResultadoExtraSelectionVs2DS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV258ExtraWWContagemResultadoExtraSelectionVs2DS_94_Contagemresultado_naocnfdmncod4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_NaoCnfDmnCod] = @AV258ExtraWWContagemResultadoExtraSelectionVs2DS_94_Contagemresultado_naocnfdmncod4)";
         }
         else
         {
            GXv_int3[162] = 1;
         }
         if ( AV241ExtraWWContagemResultadoExtraSelectionVs2DS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV242ExtraWWContagemResultadoExtraSelectionVs2DS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV261ExtraWWContagemResultadoExtraSelectionVs2DS_97_Contagemresultado_agrupador4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV261ExtraWWContagemResultadoExtraSelectionVs2DS_97_Contagemresultado_agrupador4)";
         }
         else
         {
            GXv_int3[163] = 1;
         }
         if ( AV241ExtraWWContagemResultadoExtraSelectionVs2DS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV242ExtraWWContagemResultadoExtraSelectionVs2DS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV262ExtraWWContagemResultadoExtraSelectionVs2DS_98_Contagemresultado_descricao4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV262ExtraWWContagemResultadoExtraSelectionVs2DS_98_Contagemresultado_descricao4 + '%')";
         }
         else
         {
            GXv_int3[164] = 1;
         }
         if ( AV241ExtraWWContagemResultadoExtraSelectionVs2DS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV242ExtraWWContagemResultadoExtraSelectionVs2DS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV263ExtraWWContagemResultadoExtraSelectionVs2DS_99_Contagemresultado_owner4) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Owner] = @AV263ExtraWWContagemResultadoExtraSelectionVs2DS_99_Contagemresultado_owner4)";
         }
         else
         {
            GXv_int3[165] = 1;
         }
         if ( AV241ExtraWWContagemResultadoExtraSelectionVs2DS_77_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV242ExtraWWContagemResultadoExtraSelectionVs2DS_78_Dynamicfiltersselector4, "CONTAGEMRESULTADO_CNTCOD") == 0 ) && ( ! (0==AV265ExtraWWContagemResultadoExtraSelectionVs2DS_101_Contagemresultado_cntcod4) ) )
         {
            sWhereString = sWhereString + " and (T2.[Contrato_Codigo] = @AV265ExtraWWContagemResultadoExtraSelectionVs2DS_101_Contagemresultado_cntcod4)";
         }
         else
         {
            GXv_int3[166] = 1;
         }
         if ( AV266ExtraWWContagemResultadoExtraSelectionVs2DS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV267ExtraWWContagemResultadoExtraSelectionVs2DS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV271ExtraWWContagemResultadoExtraSelectionVs2DS_107_Contagemresultado_statusdmn5)) && ! ( StringUtil.StrCmp(AV271ExtraWWContagemResultadoExtraSelectionVs2DS_107_Contagemresultado_statusdmn5, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV271ExtraWWContagemResultadoExtraSelectionVs2DS_107_Contagemresultado_statusdmn5)";
         }
         else
         {
            GXv_int3[167] = 1;
         }
         if ( AV266ExtraWWContagemResultadoExtraSelectionVs2DS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV267ExtraWWContagemResultadoExtraSelectionVs2DS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ( StringUtil.StrCmp(AV271ExtraWWContagemResultadoExtraSelectionVs2DS_107_Contagemresultado_statusdmn5, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not ( T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L' or T1.[ContagemResultado_StatusDmn] = 'X'))";
         }
         if ( AV266ExtraWWContagemResultadoExtraSelectionVs2DS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV267ExtraWWContagemResultadoExtraSelectionVs2DS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV268ExtraWWContagemResultadoExtraSelectionVs2DS_104_Dynamicfiltersoperator5 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV272ExtraWWContagemResultadoExtraSelectionVs2DS_108_Contagemresultado_osfsosfm5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV272ExtraWWContagemResultadoExtraSelectionVs2DS_108_Contagemresultado_osfsosfm5 or T1.[ContagemResultado_DemandaFM] = @AV272ExtraWWContagemResultadoExtraSelectionVs2DS_108_Contagemresultado_osfsosfm5)";
         }
         else
         {
            GXv_int3[168] = 1;
            GXv_int3[169] = 1;
         }
         if ( AV266ExtraWWContagemResultadoExtraSelectionVs2DS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV267ExtraWWContagemResultadoExtraSelectionVs2DS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV268ExtraWWContagemResultadoExtraSelectionVs2DS_104_Dynamicfiltersoperator5 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV272ExtraWWContagemResultadoExtraSelectionVs2DS_108_Contagemresultado_osfsosfm5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV272ExtraWWContagemResultadoExtraSelectionVs2DS_108_Contagemresultado_osfsosfm5 or T1.[ContagemResultado_DemandaFM] like @lV272ExtraWWContagemResultadoExtraSelectionVs2DS_108_Contagemresultado_osfsosfm5)";
         }
         else
         {
            GXv_int3[170] = 1;
            GXv_int3[171] = 1;
         }
         if ( AV266ExtraWWContagemResultadoExtraSelectionVs2DS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV267ExtraWWContagemResultadoExtraSelectionVs2DS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV268ExtraWWContagemResultadoExtraSelectionVs2DS_104_Dynamicfiltersoperator5 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV272ExtraWWContagemResultadoExtraSelectionVs2DS_108_Contagemresultado_osfsosfm5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV272ExtraWWContagemResultadoExtraSelectionVs2DS_108_Contagemresultado_osfsosfm5 or T1.[ContagemResultado_DemandaFM] like '%' + @lV272ExtraWWContagemResultadoExtraSelectionVs2DS_108_Contagemresultado_osfsosfm5)";
         }
         else
         {
            GXv_int3[172] = 1;
            GXv_int3[173] = 1;
         }
         if ( AV266ExtraWWContagemResultadoExtraSelectionVs2DS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV267ExtraWWContagemResultadoExtraSelectionVs2DS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV273ExtraWWContagemResultadoExtraSelectionVs2DS_109_Contagemresultado_datadmn5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV273ExtraWWContagemResultadoExtraSelectionVs2DS_109_Contagemresultado_datadmn5)";
         }
         else
         {
            GXv_int3[174] = 1;
         }
         if ( AV266ExtraWWContagemResultadoExtraSelectionVs2DS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV267ExtraWWContagemResultadoExtraSelectionVs2DS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV274ExtraWWContagemResultadoExtraSelectionVs2DS_110_Contagemresultado_datadmn_to5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV274ExtraWWContagemResultadoExtraSelectionVs2DS_110_Contagemresultado_datadmn_to5)";
         }
         else
         {
            GXv_int3[175] = 1;
         }
         if ( AV266ExtraWWContagemResultadoExtraSelectionVs2DS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV267ExtraWWContagemResultadoExtraSelectionVs2DS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV275ExtraWWContagemResultadoExtraSelectionVs2DS_111_Contagemresultado_dataprevista5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] >= @AV275ExtraWWContagemResultadoExtraSelectionVs2DS_111_Contagemresultado_dataprevista5)";
         }
         else
         {
            GXv_int3[176] = 1;
         }
         if ( AV266ExtraWWContagemResultadoExtraSelectionVs2DS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV267ExtraWWContagemResultadoExtraSelectionVs2DS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 ) && ( ! (DateTime.MinValue==AV276ExtraWWContagemResultadoExtraSelectionVs2DS_112_Contagemresultado_dataprevista_to5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataPrevista] < DATEADD( dd,1, @AV276ExtraWWContagemResultadoExtraSelectionVs2DS_112_Contagemresultado_dataprevista_to5))";
         }
         else
         {
            GXv_int3[177] = 1;
         }
         if ( AV266ExtraWWContagemResultadoExtraSelectionVs2DS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV267ExtraWWContagemResultadoExtraSelectionVs2DS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV277ExtraWWContagemResultadoExtraSelectionVs2DS_113_Contagemresultado_servico5) ) )
         {
            sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV277ExtraWWContagemResultadoExtraSelectionVs2DS_113_Contagemresultado_servico5)";
         }
         else
         {
            GXv_int3[178] = 1;
         }
         if ( AV266ExtraWWContagemResultadoExtraSelectionVs2DS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV267ExtraWWContagemResultadoExtraSelectionVs2DS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV279ExtraWWContagemResultadoExtraSelectionVs2DS_115_Contagemresultado_sistemacod5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_SistemaCod] = @AV279ExtraWWContagemResultadoExtraSelectionVs2DS_115_Contagemresultado_sistemacod5)";
         }
         else
         {
            GXv_int3[179] = 1;
         }
         if ( AV266ExtraWWContagemResultadoExtraSelectionVs2DS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV267ExtraWWContagemResultadoExtraSelectionVs2DS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV280ExtraWWContagemResultadoExtraSelectionVs2DS_116_Contagemresultado_contratadacod5 > 0 ) && ( AV165ExtraWWContagemResultadoExtraSelectionVs2DS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV280ExtraWWContagemResultadoExtraSelectionVs2DS_116_Contagemresultado_contratadacod5)";
         }
         else
         {
            GXv_int3[180] = 1;
         }
         if ( AV266ExtraWWContagemResultadoExtraSelectionVs2DS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV267ExtraWWContagemResultadoExtraSelectionVs2DS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 ) && ( ! (0==AV281ExtraWWContagemResultadoExtraSelectionVs2DS_117_Contagemresultado_servicogrupo5) ) )
         {
            sWhereString = sWhereString + " and (T3.[ServicoGrupo_Codigo] = @AV281ExtraWWContagemResultadoExtraSelectionVs2DS_117_Contagemresultado_servicogrupo5)";
         }
         else
         {
            GXv_int3[181] = 1;
         }
         if ( AV266ExtraWWContagemResultadoExtraSelectionVs2DS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV267ExtraWWContagemResultadoExtraSelectionVs2DS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV282ExtraWWContagemResultadoExtraSelectionVs2DS_118_Contagemresultado_baseline5, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV266ExtraWWContagemResultadoExtraSelectionVs2DS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV267ExtraWWContagemResultadoExtraSelectionVs2DS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV282ExtraWWContagemResultadoExtraSelectionVs2DS_118_Contagemresultado_baseline5, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[ContagemResultado_Baseline] = 1 or T1.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV266ExtraWWContagemResultadoExtraSelectionVs2DS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV267ExtraWWContagemResultadoExtraSelectionVs2DS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV283ExtraWWContagemResultadoExtraSelectionVs2DS_119_Contagemresultado_naocnfdmncod5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_NaoCnfDmnCod] = @AV283ExtraWWContagemResultadoExtraSelectionVs2DS_119_Contagemresultado_naocnfdmncod5)";
         }
         else
         {
            GXv_int3[182] = 1;
         }
         if ( AV266ExtraWWContagemResultadoExtraSelectionVs2DS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV267ExtraWWContagemResultadoExtraSelectionVs2DS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV286ExtraWWContagemResultadoExtraSelectionVs2DS_122_Contagemresultado_agrupador5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Agrupador] = @AV286ExtraWWContagemResultadoExtraSelectionVs2DS_122_Contagemresultado_agrupador5)";
         }
         else
         {
            GXv_int3[183] = 1;
         }
         if ( AV266ExtraWWContagemResultadoExtraSelectionVs2DS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV267ExtraWWContagemResultadoExtraSelectionVs2DS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV287ExtraWWContagemResultadoExtraSelectionVs2DS_123_Contagemresultado_descricao5)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Descricao] like '%' + @lV287ExtraWWContagemResultadoExtraSelectionVs2DS_123_Contagemresultado_descricao5 + '%')";
         }
         else
         {
            GXv_int3[184] = 1;
         }
         if ( AV266ExtraWWContagemResultadoExtraSelectionVs2DS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV267ExtraWWContagemResultadoExtraSelectionVs2DS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_OWNER") == 0 ) && ( ! (0==AV288ExtraWWContagemResultadoExtraSelectionVs2DS_124_Contagemresultado_owner5) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Owner] = @AV288ExtraWWContagemResultadoExtraSelectionVs2DS_124_Contagemresultado_owner5)";
         }
         else
         {
            GXv_int3[185] = 1;
         }
         if ( AV266ExtraWWContagemResultadoExtraSelectionVs2DS_102_Dynamicfiltersenabled5 && ( StringUtil.StrCmp(AV267ExtraWWContagemResultadoExtraSelectionVs2DS_103_Dynamicfiltersselector5, "CONTAGEMRESULTADO_CNTCOD") == 0 ) && ( ! (0==AV290ExtraWWContagemResultadoExtraSelectionVs2DS_126_Contagemresultado_cntcod5) ) )
         {
            sWhereString = sWhereString + " and (T2.[Contrato_Codigo] = @AV290ExtraWWContagemResultadoExtraSelectionVs2DS_126_Contagemresultado_cntcod5)";
         }
         else
         {
            GXv_int3[186] = 1;
         }
         if ( AV291ExtraWWContagemResultadoExtraSelectionVs2DS_127_Tfcontagemresultado_baseline_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV291ExtraWWContagemResultadoExtraSelectionVs2DS_127_Tfcontagemresultado_baseline_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Baseline] = 0)";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV293ExtraWWContagemResultadoExtraSelectionVs2DS_129_Tfcontagemresultado_servicosigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV292ExtraWWContagemResultadoExtraSelectionVs2DS_128_Tfcontagemresultado_servicosigla)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Sigla] like @lV292ExtraWWContagemResultadoExtraSelectionVs2DS_128_Tfcontagemresultado_servicosigla)";
         }
         else
         {
            GXv_int3[187] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV293ExtraWWContagemResultadoExtraSelectionVs2DS_129_Tfcontagemresultado_servicosigla_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Sigla] = @AV293ExtraWWContagemResultadoExtraSelectionVs2DS_129_Tfcontagemresultado_servicosigla_sel)";
         }
         else
         {
            GXv_int3[188] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV296ExtraWWContagemResultadoExtraSelectionVs2DS_132_Tfcontagemresultado_valorpf) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ValorPF] >= @AV296ExtraWWContagemResultadoExtraSelectionVs2DS_132_Tfcontagemresultado_valorpf)";
         }
         else
         {
            GXv_int3[189] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV297ExtraWWContagemResultadoExtraSelectionVs2DS_133_Tfcontagemresultado_valorpf_to) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ValorPF] <= @AV297ExtraWWContagemResultadoExtraSelectionVs2DS_133_Tfcontagemresultado_valorpf_to)";
         }
         else
         {
            GXv_int3[190] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T3.[Servico_Sigla]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00YD3(context, (int)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (bool)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (short)dynConstraints[23] , (String)dynConstraints[24] , (DateTime)dynConstraints[25] , (DateTime)dynConstraints[26] , (DateTime)dynConstraints[27] , (DateTime)dynConstraints[28] , (int)dynConstraints[29] , (int)dynConstraints[30] , (int)dynConstraints[31] , (int)dynConstraints[32] , (String)dynConstraints[33] , (int)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (int)dynConstraints[37] , (int)dynConstraints[38] , (bool)dynConstraints[39] , (String)dynConstraints[40] , (String)dynConstraints[41] , (short)dynConstraints[42] , (String)dynConstraints[43] , (DateTime)dynConstraints[44] , (DateTime)dynConstraints[45] , (DateTime)dynConstraints[46] , (DateTime)dynConstraints[47] , (int)dynConstraints[48] , (int)dynConstraints[49] , (int)dynConstraints[50] , (int)dynConstraints[51] , (String)dynConstraints[52] , (int)dynConstraints[53] , (String)dynConstraints[54] , (String)dynConstraints[55] , (int)dynConstraints[56] , (int)dynConstraints[57] , (bool)dynConstraints[58] , (String)dynConstraints[59] , (String)dynConstraints[60] , (short)dynConstraints[61] , (String)dynConstraints[62] , (DateTime)dynConstraints[63] , (DateTime)dynConstraints[64] , (DateTime)dynConstraints[65] , (DateTime)dynConstraints[66] , (int)dynConstraints[67] , (int)dynConstraints[68] , (int)dynConstraints[69] , (int)dynConstraints[70] , (String)dynConstraints[71] , (int)dynConstraints[72] , (String)dynConstraints[73] , (String)dynConstraints[74] , (int)dynConstraints[75] , (int)dynConstraints[76] , (bool)dynConstraints[77] , (String)dynConstraints[78] , (String)dynConstraints[79] , (short)dynConstraints[80] , (String)dynConstraints[81] , (DateTime)dynConstraints[82] , (DateTime)dynConstraints[83] , (DateTime)dynConstraints[84] , (DateTime)dynConstraints[85] , (int)dynConstraints[86] , (int)dynConstraints[87] , (int)dynConstraints[88] , (int)dynConstraints[89] , (String)dynConstraints[90] , (int)dynConstraints[91] , (String)dynConstraints[92] , (String)dynConstraints[93] , (int)dynConstraints[94] , (int)dynConstraints[95] , (short)dynConstraints[96] , (String)dynConstraints[97] , (String)dynConstraints[98] , (decimal)dynConstraints[99] , (decimal)dynConstraints[100] , (int)dynConstraints[101] , (String)dynConstraints[102] , (String)dynConstraints[103] , (String)dynConstraints[104] , (DateTime)dynConstraints[105] , (DateTime)dynConstraints[106] , (int)dynConstraints[107] , (int)dynConstraints[108] , (int)dynConstraints[109] , (bool)dynConstraints[110] , (int)dynConstraints[111] , (String)dynConstraints[112] , (String)dynConstraints[113] , (int)dynConstraints[114] , (int)dynConstraints[115] , (String)dynConstraints[116] , (decimal)dynConstraints[117] , (short)dynConstraints[118] , (decimal)dynConstraints[119] , (DateTime)dynConstraints[120] , (DateTime)dynConstraints[121] , (DateTime)dynConstraints[122] , (int)dynConstraints[123] , (int)dynConstraints[124] , (decimal)dynConstraints[125] , (decimal)dynConstraints[126] , (decimal)dynConstraints[127] , (decimal)dynConstraints[128] , (String)dynConstraints[129] , (bool)dynConstraints[130] , (DateTime)dynConstraints[131] , (DateTime)dynConstraints[132] , (int)dynConstraints[133] , (String)dynConstraints[134] , (DateTime)dynConstraints[135] , (DateTime)dynConstraints[136] , (int)dynConstraints[137] , (String)dynConstraints[138] , (DateTime)dynConstraints[139] , (DateTime)dynConstraints[140] , (int)dynConstraints[141] , (String)dynConstraints[142] , (DateTime)dynConstraints[143] , (DateTime)dynConstraints[144] , (int)dynConstraints[145] , (String)dynConstraints[146] , (decimal)dynConstraints[147] , (decimal)dynConstraints[148] , (decimal)dynConstraints[149] , (int)dynConstraints[150] , (int)dynConstraints[151] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00YD3 ;
          prmP00YD3 = new Object[] {
          new Object[] {"@AV171ExtraWWContagemResultadoExtraSelectionVs2DS_7_Contagemresultado_statusdmn1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV196ExtraWWContagemResultadoExtraSelectionVs2DS_32_Contagemresultado_statusdmn2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV221ExtraWWContagemResultadoExtraSelectionVs2DS_57_Contagemresultado_statusdmn3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV246ExtraWWContagemResultadoExtraSelectionVs2DS_82_Contagemresultado_statusdmn4",SqlDbType.Char,1,0} ,
          new Object[] {"@AV271ExtraWWContagemResultadoExtraSelectionVs2DS_107_Contagemresultado_statusdmn5",SqlDbType.Char,1,0} ,
          new Object[] {"@AV167ExtraWWContagemResultadoExtraSelectionVs2DS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV169ExtraWWContagemResultadoExtraSelectionVs2DS_5_Contagemresultado_datacnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV169ExtraWWContagemResultadoExtraSelectionVs2DS_5_Contagemresultado_datacnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV167ExtraWWContagemResultadoExtraSelectionVs2DS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV170ExtraWWContagemResultadoExtraSelectionVs2DS_6_Contagemresultado_datacnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV170ExtraWWContagemResultadoExtraSelectionVs2DS_6_Contagemresultado_datacnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV167ExtraWWContagemResultadoExtraSelectionVs2DS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV178ExtraWWContagemResultadoExtraSelectionVs2DS_14_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV178ExtraWWContagemResultadoExtraSelectionVs2DS_14_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV178ExtraWWContagemResultadoExtraSelectionVs2DS_14_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV167ExtraWWContagemResultadoExtraSelectionVs2DS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV167ExtraWWContagemResultadoExtraSelectionVs2DS_3_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV191ExtraWWContagemResultadoExtraSelectionVs2DS_27_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV192ExtraWWContagemResultadoExtraSelectionVs2DS_28_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV194ExtraWWContagemResultadoExtraSelectionVs2DS_30_Contagemresultado_datacnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV194ExtraWWContagemResultadoExtraSelectionVs2DS_30_Contagemresultado_datacnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV191ExtraWWContagemResultadoExtraSelectionVs2DS_27_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV192ExtraWWContagemResultadoExtraSelectionVs2DS_28_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV195ExtraWWContagemResultadoExtraSelectionVs2DS_31_Contagemresultado_datacnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV195ExtraWWContagemResultadoExtraSelectionVs2DS_31_Contagemresultado_datacnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV191ExtraWWContagemResultadoExtraSelectionVs2DS_27_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV192ExtraWWContagemResultadoExtraSelectionVs2DS_28_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV203ExtraWWContagemResultadoExtraSelectionVs2DS_39_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV203ExtraWWContagemResultadoExtraSelectionVs2DS_39_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV203ExtraWWContagemResultadoExtraSelectionVs2DS_39_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV191ExtraWWContagemResultadoExtraSelectionVs2DS_27_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV192ExtraWWContagemResultadoExtraSelectionVs2DS_28_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV191ExtraWWContagemResultadoExtraSelectionVs2DS_27_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV192ExtraWWContagemResultadoExtraSelectionVs2DS_28_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV216ExtraWWContagemResultadoExtraSelectionVs2DS_52_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV217ExtraWWContagemResultadoExtraSelectionVs2DS_53_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV219ExtraWWContagemResultadoExtraSelectionVs2DS_55_Contagemresultado_datacnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV219ExtraWWContagemResultadoExtraSelectionVs2DS_55_Contagemresultado_datacnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV216ExtraWWContagemResultadoExtraSelectionVs2DS_52_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV217ExtraWWContagemResultadoExtraSelectionVs2DS_53_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV220ExtraWWContagemResultadoExtraSelectionVs2DS_56_Contagemresultado_datacnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV220ExtraWWContagemResultadoExtraSelectionVs2DS_56_Contagemresultado_datacnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV216ExtraWWContagemResultadoExtraSelectionVs2DS_52_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV217ExtraWWContagemResultadoExtraSelectionVs2DS_53_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV228ExtraWWContagemResultadoExtraSelectionVs2DS_64_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV228ExtraWWContagemResultadoExtraSelectionVs2DS_64_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV228ExtraWWContagemResultadoExtraSelectionVs2DS_64_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV216ExtraWWContagemResultadoExtraSelectionVs2DS_52_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV217ExtraWWContagemResultadoExtraSelectionVs2DS_53_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV216ExtraWWContagemResultadoExtraSelectionVs2DS_52_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV217ExtraWWContagemResultadoExtraSelectionVs2DS_53_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV241ExtraWWContagemResultadoExtraSelectionVs2DS_77_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV242ExtraWWContagemResultadoExtraSelectionVs2DS_78_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV244ExtraWWContagemResultadoExtraSelectionVs2DS_80_Contagemresultado_datacnt4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV244ExtraWWContagemResultadoExtraSelectionVs2DS_80_Contagemresultado_datacnt4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV241ExtraWWContagemResultadoExtraSelectionVs2DS_77_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV242ExtraWWContagemResultadoExtraSelectionVs2DS_78_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV245ExtraWWContagemResultadoExtraSelectionVs2DS_81_Contagemresultado_datacnt_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV245ExtraWWContagemResultadoExtraSelectionVs2DS_81_Contagemresultado_datacnt_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV241ExtraWWContagemResultadoExtraSelectionVs2DS_77_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV242ExtraWWContagemResultadoExtraSelectionVs2DS_78_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV253ExtraWWContagemResultadoExtraSelectionVs2DS_89_Contagemresultado_contadorfm4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV253ExtraWWContagemResultadoExtraSelectionVs2DS_89_Contagemresultado_contadorfm4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV253ExtraWWContagemResultadoExtraSelectionVs2DS_89_Contagemresultado_contadorfm4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV241ExtraWWContagemResultadoExtraSelectionVs2DS_77_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV242ExtraWWContagemResultadoExtraSelectionVs2DS_78_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV241ExtraWWContagemResultadoExtraSelectionVs2DS_77_Dynamicfiltersenabled4",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV242ExtraWWContagemResultadoExtraSelectionVs2DS_78_Dynamicfiltersselector4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV266ExtraWWContagemResultadoExtraSelectionVs2DS_102_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV267ExtraWWContagemResultadoExtraSelectionVs2DS_103_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV269ExtraWWContagemResultadoExtraSelectionVs2DS_105_Contagemresultado_datacnt5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV269ExtraWWContagemResultadoExtraSelectionVs2DS_105_Contagemresultado_datacnt5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV266ExtraWWContagemResultadoExtraSelectionVs2DS_102_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV267ExtraWWContagemResultadoExtraSelectionVs2DS_103_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV270ExtraWWContagemResultadoExtraSelectionVs2DS_106_Contagemresultado_datacnt_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV270ExtraWWContagemResultadoExtraSelectionVs2DS_106_Contagemresultado_datacnt_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV266ExtraWWContagemResultadoExtraSelectionVs2DS_102_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV267ExtraWWContagemResultadoExtraSelectionVs2DS_103_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV278ExtraWWContagemResultadoExtraSelectionVs2DS_114_Contagemresultado_contadorfm5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV278ExtraWWContagemResultadoExtraSelectionVs2DS_114_Contagemresultado_contadorfm5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV278ExtraWWContagemResultadoExtraSelectionVs2DS_114_Contagemresultado_contadorfm5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV266ExtraWWContagemResultadoExtraSelectionVs2DS_102_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV267ExtraWWContagemResultadoExtraSelectionVs2DS_103_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV266ExtraWWContagemResultadoExtraSelectionVs2DS_102_Dynamicfiltersenabled5",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV267ExtraWWContagemResultadoExtraSelectionVs2DS_103_Dynamicfiltersselector5",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9WWPCo_2Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV171ExtraWWContagemResultadoExtraSelectionVs2DS_7_Contagemresultado_statusdmn1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV172ExtraWWContagemResultadoExtraSelectionVs2DS_8_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV172ExtraWWContagemResultadoExtraSelectionVs2DS_8_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV172ExtraWWContagemResultadoExtraSelectionVs2DS_8_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV172ExtraWWContagemResultadoExtraSelectionVs2DS_8_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV172ExtraWWContagemResultadoExtraSelectionVs2DS_8_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV172ExtraWWContagemResultadoExtraSelectionVs2DS_8_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV173ExtraWWContagemResultadoExtraSelectionVs2DS_9_Contagemresultado_datadmn1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV174ExtraWWContagemResultadoExtraSelectionVs2DS_10_Contagemresultado_datadmn_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV175ExtraWWContagemResultadoExtraSelectionVs2DS_11_Contagemresultado_dataprevista1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV176ExtraWWContagemResultadoExtraSelectionVs2DS_12_Contagemresultado_dataprevista_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV177ExtraWWContagemResultadoExtraSelectionVs2DS_13_Contagemresultado_servico1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV179ExtraWWContagemResultadoExtraSelectionVs2DS_15_Contagemresultado_sistemacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV180ExtraWWContagemResultadoExtraSelectionVs2DS_16_Contagemresultado_contratadacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV181ExtraWWContagemResultadoExtraSelectionVs2DS_17_Contagemresultado_servicogrupo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV183ExtraWWContagemResultadoExtraSelectionVs2DS_19_Contagemresultado_naocnfdmncod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV186ExtraWWContagemResultadoExtraSelectionVs2DS_22_Contagemresultado_agrupador1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV187ExtraWWContagemResultadoExtraSelectionVs2DS_23_Contagemresultado_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV188ExtraWWContagemResultadoExtraSelectionVs2DS_24_Contagemresultado_owner1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV190ExtraWWContagemResultadoExtraSelectionVs2DS_26_Contagemresultado_cntcod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV196ExtraWWContagemResultadoExtraSelectionVs2DS_32_Contagemresultado_statusdmn2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV197ExtraWWContagemResultadoExtraSelectionVs2DS_33_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV197ExtraWWContagemResultadoExtraSelectionVs2DS_33_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV197ExtraWWContagemResultadoExtraSelectionVs2DS_33_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV197ExtraWWContagemResultadoExtraSelectionVs2DS_33_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV197ExtraWWContagemResultadoExtraSelectionVs2DS_33_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV197ExtraWWContagemResultadoExtraSelectionVs2DS_33_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV198ExtraWWContagemResultadoExtraSelectionVs2DS_34_Contagemresultado_datadmn2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV199ExtraWWContagemResultadoExtraSelectionVs2DS_35_Contagemresultado_datadmn_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV200ExtraWWContagemResultadoExtraSelectionVs2DS_36_Contagemresultado_dataprevista2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV201ExtraWWContagemResultadoExtraSelectionVs2DS_37_Contagemresultado_dataprevista_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV202ExtraWWContagemResultadoExtraSelectionVs2DS_38_Contagemresultado_servico2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV204ExtraWWContagemResultadoExtraSelectionVs2DS_40_Contagemresultado_sistemacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV205ExtraWWContagemResultadoExtraSelectionVs2DS_41_Contagemresultado_contratadacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV206ExtraWWContagemResultadoExtraSelectionVs2DS_42_Contagemresultado_servicogrupo2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV208ExtraWWContagemResultadoExtraSelectionVs2DS_44_Contagemresultado_naocnfdmncod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV211ExtraWWContagemResultadoExtraSelectionVs2DS_47_Contagemresultado_agrupador2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV212ExtraWWContagemResultadoExtraSelectionVs2DS_48_Contagemresultado_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV213ExtraWWContagemResultadoExtraSelectionVs2DS_49_Contagemresultado_owner2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV215ExtraWWContagemResultadoExtraSelectionVs2DS_51_Contagemresultado_cntcod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV221ExtraWWContagemResultadoExtraSelectionVs2DS_57_Contagemresultado_statusdmn3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV222ExtraWWContagemResultadoExtraSelectionVs2DS_58_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV222ExtraWWContagemResultadoExtraSelectionVs2DS_58_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV222ExtraWWContagemResultadoExtraSelectionVs2DS_58_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV222ExtraWWContagemResultadoExtraSelectionVs2DS_58_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV222ExtraWWContagemResultadoExtraSelectionVs2DS_58_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV222ExtraWWContagemResultadoExtraSelectionVs2DS_58_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV223ExtraWWContagemResultadoExtraSelectionVs2DS_59_Contagemresultado_datadmn3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV224ExtraWWContagemResultadoExtraSelectionVs2DS_60_Contagemresultado_datadmn_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV225ExtraWWContagemResultadoExtraSelectionVs2DS_61_Contagemresultado_dataprevista3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV226ExtraWWContagemResultadoExtraSelectionVs2DS_62_Contagemresultado_dataprevista_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV227ExtraWWContagemResultadoExtraSelectionVs2DS_63_Contagemresultado_servico3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV229ExtraWWContagemResultadoExtraSelectionVs2DS_65_Contagemresultado_sistemacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV230ExtraWWContagemResultadoExtraSelectionVs2DS_66_Contagemresultado_contratadacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV231ExtraWWContagemResultadoExtraSelectionVs2DS_67_Contagemresultado_servicogrupo3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV233ExtraWWContagemResultadoExtraSelectionVs2DS_69_Contagemresultado_naocnfdmncod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV236ExtraWWContagemResultadoExtraSelectionVs2DS_72_Contagemresultado_agrupador3",SqlDbType.Char,15,0} ,
          new Object[] {"@lV237ExtraWWContagemResultadoExtraSelectionVs2DS_73_Contagemresultado_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV238ExtraWWContagemResultadoExtraSelectionVs2DS_74_Contagemresultado_owner3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV240ExtraWWContagemResultadoExtraSelectionVs2DS_76_Contagemresultado_cntcod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV246ExtraWWContagemResultadoExtraSelectionVs2DS_82_Contagemresultado_statusdmn4",SqlDbType.Char,1,0} ,
          new Object[] {"@AV247ExtraWWContagemResultadoExtraSelectionVs2DS_83_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV247ExtraWWContagemResultadoExtraSelectionVs2DS_83_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV247ExtraWWContagemResultadoExtraSelectionVs2DS_83_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV247ExtraWWContagemResultadoExtraSelectionVs2DS_83_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV247ExtraWWContagemResultadoExtraSelectionVs2DS_83_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV247ExtraWWContagemResultadoExtraSelectionVs2DS_83_Contagemresultado_osfsosfm4",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV248ExtraWWContagemResultadoExtraSelectionVs2DS_84_Contagemresultado_datadmn4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV249ExtraWWContagemResultadoExtraSelectionVs2DS_85_Contagemresultado_datadmn_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV250ExtraWWContagemResultadoExtraSelectionVs2DS_86_Contagemresultado_dataprevista4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV251ExtraWWContagemResultadoExtraSelectionVs2DS_87_Contagemresultado_dataprevista_to4",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV252ExtraWWContagemResultadoExtraSelectionVs2DS_88_Contagemresultado_servico4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV254ExtraWWContagemResultadoExtraSelectionVs2DS_90_Contagemresultado_sistemacod4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV255ExtraWWContagemResultadoExtraSelectionVs2DS_91_Contagemresultado_contratadacod4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV256ExtraWWContagemResultadoExtraSelectionVs2DS_92_Contagemresultado_servicogrupo4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV258ExtraWWContagemResultadoExtraSelectionVs2DS_94_Contagemresultado_naocnfdmncod4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV261ExtraWWContagemResultadoExtraSelectionVs2DS_97_Contagemresultado_agrupador4",SqlDbType.Char,15,0} ,
          new Object[] {"@lV262ExtraWWContagemResultadoExtraSelectionVs2DS_98_Contagemresultado_descricao4",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV263ExtraWWContagemResultadoExtraSelectionVs2DS_99_Contagemresultado_owner4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV265ExtraWWContagemResultadoExtraSelectionVs2DS_101_Contagemresultado_cntcod4",SqlDbType.Int,6,0} ,
          new Object[] {"@AV271ExtraWWContagemResultadoExtraSelectionVs2DS_107_Contagemresultado_statusdmn5",SqlDbType.Char,1,0} ,
          new Object[] {"@AV272ExtraWWContagemResultadoExtraSelectionVs2DS_108_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV272ExtraWWContagemResultadoExtraSelectionVs2DS_108_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV272ExtraWWContagemResultadoExtraSelectionVs2DS_108_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV272ExtraWWContagemResultadoExtraSelectionVs2DS_108_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV272ExtraWWContagemResultadoExtraSelectionVs2DS_108_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV272ExtraWWContagemResultadoExtraSelectionVs2DS_108_Contagemresultado_osfsosfm5",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV273ExtraWWContagemResultadoExtraSelectionVs2DS_109_Contagemresultado_datadmn5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV274ExtraWWContagemResultadoExtraSelectionVs2DS_110_Contagemresultado_datadmn_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV275ExtraWWContagemResultadoExtraSelectionVs2DS_111_Contagemresultado_dataprevista5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV276ExtraWWContagemResultadoExtraSelectionVs2DS_112_Contagemresultado_dataprevista_to5",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV277ExtraWWContagemResultadoExtraSelectionVs2DS_113_Contagemresultado_servico5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV279ExtraWWContagemResultadoExtraSelectionVs2DS_115_Contagemresultado_sistemacod5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV280ExtraWWContagemResultadoExtraSelectionVs2DS_116_Contagemresultado_contratadacod5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV281ExtraWWContagemResultadoExtraSelectionVs2DS_117_Contagemresultado_servicogrupo5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV283ExtraWWContagemResultadoExtraSelectionVs2DS_119_Contagemresultado_naocnfdmncod5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV286ExtraWWContagemResultadoExtraSelectionVs2DS_122_Contagemresultado_agrupador5",SqlDbType.Char,15,0} ,
          new Object[] {"@lV287ExtraWWContagemResultadoExtraSelectionVs2DS_123_Contagemresultado_descricao5",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV288ExtraWWContagemResultadoExtraSelectionVs2DS_124_Contagemresultado_owner5",SqlDbType.Int,6,0} ,
          new Object[] {"@AV290ExtraWWContagemResultadoExtraSelectionVs2DS_126_Contagemresultado_cntcod5",SqlDbType.Int,6,0} ,
          new Object[] {"@lV292ExtraWWContagemResultadoExtraSelectionVs2DS_128_Tfcontagemresultado_servicosigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV293ExtraWWContagemResultadoExtraSelectionVs2DS_129_Tfcontagemresultado_servicosigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV296ExtraWWContagemResultadoExtraSelectionVs2DS_132_Tfcontagemresultado_valorpf",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV297ExtraWWContagemResultadoExtraSelectionVs2DS_133_Tfcontagemresultado_valorpf_to",SqlDbType.Decimal,18,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00YD3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YD3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 15) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((String[]) buf[12])[0] = rslt.getString(7, 15) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((int[]) buf[14])[0] = rslt.getInt(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((bool[]) buf[16])[0] = rslt.getBool(9) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((int[]) buf[18])[0] = rslt.getInt(10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((int[]) buf[20])[0] = rslt.getInt(11) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(11);
                ((int[]) buf[22])[0] = rslt.getInt(12) ;
                ((int[]) buf[23])[0] = rslt.getInt(13) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(13);
                ((DateTime[]) buf[25])[0] = rslt.getGXDateTime(14) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(14);
                ((DateTime[]) buf[27])[0] = rslt.getGXDate(15) ;
                ((String[]) buf[28])[0] = rslt.getVarchar(16) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(16);
                ((String[]) buf[30])[0] = rslt.getVarchar(17) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(17);
                ((String[]) buf[32])[0] = rslt.getString(18, 1) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(18);
                ((decimal[]) buf[34])[0] = rslt.getDecimal(19) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(19);
                ((int[]) buf[36])[0] = rslt.getInt(20) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(20);
                ((decimal[]) buf[38])[0] = rslt.getDecimal(21) ;
                ((decimal[]) buf[39])[0] = rslt.getDecimal(22) ;
                ((decimal[]) buf[40])[0] = rslt.getDecimal(23) ;
                ((decimal[]) buf[41])[0] = rslt.getDecimal(24) ;
                ((int[]) buf[42])[0] = rslt.getInt(25) ;
                ((DateTime[]) buf[43])[0] = rslt.getGXDate(26) ;
                ((short[]) buf[44])[0] = rslt.getShort(27) ;
                ((int[]) buf[45])[0] = rslt.getInt(28) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[191]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[192]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[193]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[194]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[195]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[196]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[197]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[198]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[199]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[200]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[201]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[202]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[203]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[204]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[205]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[206]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[207]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[208]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[209]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[210]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[211]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[212]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[213]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[214]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[215]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[216]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[217]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[218]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[219]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[220]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[221]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[222]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[223]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[224]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[225]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[226]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[227]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[228]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[229]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[230]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[231]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[232]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[233]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[234]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[235]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[236]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[237]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[238]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[239]);
                }
                if ( (short)parms[49] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[240]);
                }
                if ( (short)parms[50] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[241]);
                }
                if ( (short)parms[51] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[242]);
                }
                if ( (short)parms[52] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[243]);
                }
                if ( (short)parms[53] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[244]);
                }
                if ( (short)parms[54] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[245]);
                }
                if ( (short)parms[55] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[246]);
                }
                if ( (short)parms[56] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[247]);
                }
                if ( (short)parms[57] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[248]);
                }
                if ( (short)parms[58] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[249]);
                }
                if ( (short)parms[59] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[250]);
                }
                if ( (short)parms[60] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[251]);
                }
                if ( (short)parms[61] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[252]);
                }
                if ( (short)parms[62] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[253]);
                }
                if ( (short)parms[63] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[254]);
                }
                if ( (short)parms[64] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[255]);
                }
                if ( (short)parms[65] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[256]);
                }
                if ( (short)parms[66] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[257]);
                }
                if ( (short)parms[67] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[258]);
                }
                if ( (short)parms[68] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[259]);
                }
                if ( (short)parms[69] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[260]);
                }
                if ( (short)parms[70] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[261]);
                }
                if ( (short)parms[71] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[262]);
                }
                if ( (short)parms[72] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[263]);
                }
                if ( (short)parms[73] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[264]);
                }
                if ( (short)parms[74] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[265]);
                }
                if ( (short)parms[75] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[266]);
                }
                if ( (short)parms[76] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[267]);
                }
                if ( (short)parms[77] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[268]);
                }
                if ( (short)parms[78] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[269]);
                }
                if ( (short)parms[79] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[270]);
                }
                if ( (short)parms[80] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[271]);
                }
                if ( (short)parms[81] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[272]);
                }
                if ( (short)parms[82] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[273]);
                }
                if ( (short)parms[83] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[274]);
                }
                if ( (short)parms[84] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[275]);
                }
                if ( (short)parms[85] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[276]);
                }
                if ( (short)parms[86] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[277]);
                }
                if ( (short)parms[87] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[278]);
                }
                if ( (short)parms[88] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[279]);
                }
                if ( (short)parms[89] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[280]);
                }
                if ( (short)parms[90] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[281]);
                }
                if ( (short)parms[91] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[282]);
                }
                if ( (short)parms[92] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[283]);
                }
                if ( (short)parms[93] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[284]);
                }
                if ( (short)parms[94] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[285]);
                }
                if ( (short)parms[95] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[286]);
                }
                if ( (short)parms[96] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[287]);
                }
                if ( (short)parms[97] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[288]);
                }
                if ( (short)parms[98] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[289]);
                }
                if ( (short)parms[99] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[290]);
                }
                if ( (short)parms[100] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[291]);
                }
                if ( (short)parms[101] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[292]);
                }
                if ( (short)parms[102] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[293]);
                }
                if ( (short)parms[103] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[294]);
                }
                if ( (short)parms[104] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[295]);
                }
                if ( (short)parms[105] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[296]);
                }
                if ( (short)parms[106] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[297]);
                }
                if ( (short)parms[107] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[298]);
                }
                if ( (short)parms[108] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[299]);
                }
                if ( (short)parms[109] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[300]);
                }
                if ( (short)parms[110] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[301]);
                }
                if ( (short)parms[111] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[302]);
                }
                if ( (short)parms[112] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[303]);
                }
                if ( (short)parms[113] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[304]);
                }
                if ( (short)parms[114] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[305]);
                }
                if ( (short)parms[115] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[306]);
                }
                if ( (short)parms[116] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[307]);
                }
                if ( (short)parms[117] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[308]);
                }
                if ( (short)parms[118] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[309]);
                }
                if ( (short)parms[119] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[310]);
                }
                if ( (short)parms[120] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[311]);
                }
                if ( (short)parms[121] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[312]);
                }
                if ( (short)parms[122] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[313]);
                }
                if ( (short)parms[123] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[314]);
                }
                if ( (short)parms[124] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[315]);
                }
                if ( (short)parms[125] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[316]);
                }
                if ( (short)parms[126] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[317]);
                }
                if ( (short)parms[127] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[318]);
                }
                if ( (short)parms[128] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[319]);
                }
                if ( (short)parms[129] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[320]);
                }
                if ( (short)parms[130] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[321]);
                }
                if ( (short)parms[131] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[322]);
                }
                if ( (short)parms[132] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[323]);
                }
                if ( (short)parms[133] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[324]);
                }
                if ( (short)parms[134] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[325]);
                }
                if ( (short)parms[135] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[326]);
                }
                if ( (short)parms[136] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[327]);
                }
                if ( (short)parms[137] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[328]);
                }
                if ( (short)parms[138] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[329]);
                }
                if ( (short)parms[139] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[330]);
                }
                if ( (short)parms[140] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[331]);
                }
                if ( (short)parms[141] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[332]);
                }
                if ( (short)parms[142] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[333]);
                }
                if ( (short)parms[143] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[334]);
                }
                if ( (short)parms[144] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[335]);
                }
                if ( (short)parms[145] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[336]);
                }
                if ( (short)parms[146] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[337]);
                }
                if ( (short)parms[147] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[338]);
                }
                if ( (short)parms[148] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[339]);
                }
                if ( (short)parms[149] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[340]);
                }
                if ( (short)parms[150] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[341]);
                }
                if ( (short)parms[151] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[342]);
                }
                if ( (short)parms[152] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[343]);
                }
                if ( (short)parms[153] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[344]);
                }
                if ( (short)parms[154] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[345]);
                }
                if ( (short)parms[155] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[346]);
                }
                if ( (short)parms[156] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[347]);
                }
                if ( (short)parms[157] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[348]);
                }
                if ( (short)parms[158] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[349]);
                }
                if ( (short)parms[159] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[350]);
                }
                if ( (short)parms[160] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[351]);
                }
                if ( (short)parms[161] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[352]);
                }
                if ( (short)parms[162] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[353]);
                }
                if ( (short)parms[163] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[354]);
                }
                if ( (short)parms[164] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[355]);
                }
                if ( (short)parms[165] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[356]);
                }
                if ( (short)parms[166] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[357]);
                }
                if ( (short)parms[167] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[358]);
                }
                if ( (short)parms[168] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[359]);
                }
                if ( (short)parms[169] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[360]);
                }
                if ( (short)parms[170] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[361]);
                }
                if ( (short)parms[171] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[362]);
                }
                if ( (short)parms[172] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[363]);
                }
                if ( (short)parms[173] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[364]);
                }
                if ( (short)parms[174] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[365]);
                }
                if ( (short)parms[175] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[366]);
                }
                if ( (short)parms[176] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[367]);
                }
                if ( (short)parms[177] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[368]);
                }
                if ( (short)parms[178] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[369]);
                }
                if ( (short)parms[179] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[370]);
                }
                if ( (short)parms[180] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[371]);
                }
                if ( (short)parms[181] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[372]);
                }
                if ( (short)parms[182] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[373]);
                }
                if ( (short)parms[183] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[374]);
                }
                if ( (short)parms[184] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[375]);
                }
                if ( (short)parms[185] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[376]);
                }
                if ( (short)parms[186] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[377]);
                }
                if ( (short)parms[187] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[378]);
                }
                if ( (short)parms[188] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[379]);
                }
                if ( (short)parms[189] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[380]);
                }
                if ( (short)parms[190] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[381]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getextrawwcontagemresultadoextraselectionvs2filterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getextrawwcontagemresultadoextraselectionvs2filterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getextrawwcontagemresultadoextraselectionvs2filterdata") )
          {
             return  ;
          }
          getextrawwcontagemresultadoextraselectionvs2filterdata worker = new getextrawwcontagemresultadoextraselectionvs2filterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
