/*
               File: PRC_SetDataEntregaExecucao
        Description: Set Data de Entrega e Execucao
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:13:21.89
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_setdataentregaexecucao : GXProcedure
   {
      public prc_setdataentregaexecucao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_setdataentregaexecucao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultado_Codigo ,
                           ref DateTime aP1_DateTime )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV9DateTime = aP1_DateTime;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP1_DateTime=this.AV9DateTime;
      }

      public DateTime executeUdp( ref int aP0_ContagemResultado_Codigo )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV9DateTime = aP1_DateTime;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP1_DateTime=this.AV9DateTime;
         return AV9DateTime ;
      }

      public void executeSubmit( ref int aP0_ContagemResultado_Codigo ,
                                 ref DateTime aP1_DateTime )
      {
         prc_setdataentregaexecucao objprc_setdataentregaexecucao;
         objprc_setdataentregaexecucao = new prc_setdataentregaexecucao();
         objprc_setdataentregaexecucao.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_setdataentregaexecucao.AV9DateTime = aP1_DateTime;
         objprc_setdataentregaexecucao.context.SetSubmitInitialConfig(context);
         objprc_setdataentregaexecucao.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_setdataentregaexecucao);
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP1_DateTime=this.AV9DateTime;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_setdataentregaexecucao)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00W92 */
         pr_default.execute(0, new Object[] {A456ContagemResultado_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1349ContagemResultado_DataExecucao = P00W92_A1349ContagemResultado_DataExecucao[0];
            n1349ContagemResultado_DataExecucao = P00W92_n1349ContagemResultado_DataExecucao[0];
            A2017ContagemResultado_DataEntregaReal = P00W92_A2017ContagemResultado_DataEntregaReal[0];
            n2017ContagemResultado_DataEntregaReal = P00W92_n2017ContagemResultado_DataEntregaReal[0];
            if ( (DateTime.MinValue==A1349ContagemResultado_DataExecucao) )
            {
               A1349ContagemResultado_DataExecucao = AV9DateTime;
               n1349ContagemResultado_DataExecucao = false;
            }
            A2017ContagemResultado_DataEntregaReal = AV9DateTime;
            n2017ContagemResultado_DataEntregaReal = false;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P00W93 */
            pr_default.execute(1, new Object[] {n1349ContagemResultado_DataExecucao, A1349ContagemResultado_DataExecucao, n2017ContagemResultado_DataEntregaReal, A2017ContagemResultado_DataEntregaReal, A456ContagemResultado_Codigo});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            if (true) break;
            /* Using cursor P00W94 */
            pr_default.execute(2, new Object[] {n1349ContagemResultado_DataExecucao, A1349ContagemResultado_DataExecucao, n2017ContagemResultado_DataEntregaReal, A2017ContagemResultado_DataEntregaReal, A456ContagemResultado_Codigo});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00W92_A456ContagemResultado_Codigo = new int[1] ;
         P00W92_A1349ContagemResultado_DataExecucao = new DateTime[] {DateTime.MinValue} ;
         P00W92_n1349ContagemResultado_DataExecucao = new bool[] {false} ;
         P00W92_A2017ContagemResultado_DataEntregaReal = new DateTime[] {DateTime.MinValue} ;
         P00W92_n2017ContagemResultado_DataEntregaReal = new bool[] {false} ;
         A1349ContagemResultado_DataExecucao = (DateTime)(DateTime.MinValue);
         A2017ContagemResultado_DataEntregaReal = (DateTime)(DateTime.MinValue);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_setdataentregaexecucao__default(),
            new Object[][] {
                new Object[] {
               P00W92_A456ContagemResultado_Codigo, P00W92_A1349ContagemResultado_DataExecucao, P00W92_n1349ContagemResultado_DataExecucao, P00W92_A2017ContagemResultado_DataEntregaReal, P00W92_n2017ContagemResultado_DataEntregaReal
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A456ContagemResultado_Codigo ;
      private String scmdbuf ;
      private DateTime AV9DateTime ;
      private DateTime A1349ContagemResultado_DataExecucao ;
      private DateTime A2017ContagemResultado_DataEntregaReal ;
      private bool n1349ContagemResultado_DataExecucao ;
      private bool n2017ContagemResultado_DataEntregaReal ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultado_Codigo ;
      private DateTime aP1_DateTime ;
      private IDataStoreProvider pr_default ;
      private int[] P00W92_A456ContagemResultado_Codigo ;
      private DateTime[] P00W92_A1349ContagemResultado_DataExecucao ;
      private bool[] P00W92_n1349ContagemResultado_DataExecucao ;
      private DateTime[] P00W92_A2017ContagemResultado_DataEntregaReal ;
      private bool[] P00W92_n2017ContagemResultado_DataEntregaReal ;
   }

   public class prc_setdataentregaexecucao__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00W92 ;
          prmP00W92 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00W93 ;
          prmP00W93 = new Object[] {
          new Object[] {"@ContagemResultado_DataExecucao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_DataEntregaReal",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00W94 ;
          prmP00W94 = new Object[] {
          new Object[] {"@ContagemResultado_DataExecucao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_DataEntregaReal",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00W92", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_DataExecucao], [ContagemResultado_DataEntregaReal] FROM [ContagemResultado] WITH (UPDLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY [ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00W92,1,0,true,true )
             ,new CursorDef("P00W93", "UPDATE [ContagemResultado] SET [ContagemResultado_DataExecucao]=@ContagemResultado_DataExecucao, [ContagemResultado_DataEntregaReal]=@ContagemResultado_DataEntregaReal  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00W93)
             ,new CursorDef("P00W94", "UPDATE [ContagemResultado] SET [ContagemResultado_DataExecucao]=@ContagemResultado_DataExecucao, [ContagemResultado_DataEntregaReal]=@ContagemResultado_DataEntregaReal  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00W94)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[3])[0] = rslt.getGXDateTime(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(2, (DateTime)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(2, (DateTime)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
       }
    }

 }

}
