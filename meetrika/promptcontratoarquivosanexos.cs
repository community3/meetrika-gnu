/*
               File: PromptContratoArquivosAnexos
        Description: Selecione Contrato Arquivos Anexos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:35:11.81
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptcontratoarquivosanexos : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptcontratoarquivosanexos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptcontratoarquivosanexos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutContratoArquivosAnexos_Codigo ,
                           ref String aP1_InOutContratoArquivosAnexos_Descricao )
      {
         this.AV7InOutContratoArquivosAnexos_Codigo = aP0_InOutContratoArquivosAnexos_Codigo;
         this.AV8InOutContratoArquivosAnexos_Descricao = aP1_InOutContratoArquivosAnexos_Descricao;
         executePrivate();
         aP0_InOutContratoArquivosAnexos_Codigo=this.AV7InOutContratoArquivosAnexos_Codigo;
         aP1_InOutContratoArquivosAnexos_Descricao=this.AV8InOutContratoArquivosAnexos_Descricao;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_80 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_80_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_80_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17ContratoArquivosAnexos_Descricao1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoArquivosAnexos_Descricao1", AV17ContratoArquivosAnexos_Descricao1);
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
               AV21ContratoArquivosAnexos_Descricao2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoArquivosAnexos_Descricao2", AV21ContratoArquivosAnexos_Descricao2);
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
               AV25ContratoArquivosAnexos_Descricao3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoArquivosAnexos_Descricao3", AV25ContratoArquivosAnexos_Descricao3);
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV31TFContratoArquivosAnexos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFContratoArquivosAnexos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFContratoArquivosAnexos_Codigo), 6, 0)));
               AV32TFContratoArquivosAnexos_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFContratoArquivosAnexos_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFContratoArquivosAnexos_Codigo_To), 6, 0)));
               AV35TFContrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFContrato_Codigo), 6, 0)));
               AV36TFContrato_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFContrato_Codigo_To), 6, 0)));
               AV39TFContrato_Numero = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContrato_Numero", AV39TFContrato_Numero);
               AV40TFContrato_Numero_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContrato_Numero_Sel", AV40TFContrato_Numero_Sel);
               AV43TFContratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFContratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFContratada_Codigo), 6, 0)));
               AV44TFContratada_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContratada_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFContratada_Codigo_To), 6, 0)));
               AV47TFContratada_PessoaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFContratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47TFContratada_PessoaCod), 6, 0)));
               AV48TFContratada_PessoaCod_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFContratada_PessoaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48TFContratada_PessoaCod_To), 6, 0)));
               AV51TFContratada_PessoaNom = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFContratada_PessoaNom", AV51TFContratada_PessoaNom);
               AV52TFContratada_PessoaNom_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFContratada_PessoaNom_Sel", AV52TFContratada_PessoaNom_Sel);
               AV55TFContratada_PessoaCNPJ = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFContratada_PessoaCNPJ", AV55TFContratada_PessoaCNPJ);
               AV56TFContratada_PessoaCNPJ_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFContratada_PessoaCNPJ_Sel", AV56TFContratada_PessoaCNPJ_Sel);
               AV59TFContratoArquivosAnexos_Descricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContratoArquivosAnexos_Descricao", AV59TFContratoArquivosAnexos_Descricao);
               AV60TFContratoArquivosAnexos_Descricao_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContratoArquivosAnexos_Descricao_Sel", AV60TFContratoArquivosAnexos_Descricao_Sel);
               AV63TFContratoArquivosAnexos_Data = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFContratoArquivosAnexos_Data", context.localUtil.TToC( AV63TFContratoArquivosAnexos_Data, 8, 5, 0, 3, "/", ":", " "));
               AV64TFContratoArquivosAnexos_Data_To = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFContratoArquivosAnexos_Data_To", context.localUtil.TToC( AV64TFContratoArquivosAnexos_Data_To, 8, 5, 0, 3, "/", ":", " "));
               AV33ddo_ContratoArquivosAnexos_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ddo_ContratoArquivosAnexos_CodigoTitleControlIdToReplace", AV33ddo_ContratoArquivosAnexos_CodigoTitleControlIdToReplace);
               AV37ddo_Contrato_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_Contrato_CodigoTitleControlIdToReplace", AV37ddo_Contrato_CodigoTitleControlIdToReplace);
               AV41ddo_Contrato_NumeroTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_Contrato_NumeroTitleControlIdToReplace", AV41ddo_Contrato_NumeroTitleControlIdToReplace);
               AV45ddo_Contratada_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_Contratada_CodigoTitleControlIdToReplace", AV45ddo_Contratada_CodigoTitleControlIdToReplace);
               AV49ddo_Contratada_PessoaCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_Contratada_PessoaCodTitleControlIdToReplace", AV49ddo_Contratada_PessoaCodTitleControlIdToReplace);
               AV53ddo_Contratada_PessoaNomTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_Contratada_PessoaNomTitleControlIdToReplace", AV53ddo_Contratada_PessoaNomTitleControlIdToReplace);
               AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace", AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace);
               AV61ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace", AV61ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace);
               AV67ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace", AV67ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace);
               AV78Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoArquivosAnexos_Descricao1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoArquivosAnexos_Descricao2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ContratoArquivosAnexos_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFContratoArquivosAnexos_Codigo, AV32TFContratoArquivosAnexos_Codigo_To, AV35TFContrato_Codigo, AV36TFContrato_Codigo_To, AV39TFContrato_Numero, AV40TFContrato_Numero_Sel, AV43TFContratada_Codigo, AV44TFContratada_Codigo_To, AV47TFContratada_PessoaCod, AV48TFContratada_PessoaCod_To, AV51TFContratada_PessoaNom, AV52TFContratada_PessoaNom_Sel, AV55TFContratada_PessoaCNPJ, AV56TFContratada_PessoaCNPJ_Sel, AV59TFContratoArquivosAnexos_Descricao, AV60TFContratoArquivosAnexos_Descricao_Sel, AV63TFContratoArquivosAnexos_Data, AV64TFContratoArquivosAnexos_Data_To, AV33ddo_ContratoArquivosAnexos_CodigoTitleControlIdToReplace, AV37ddo_Contrato_CodigoTitleControlIdToReplace, AV41ddo_Contrato_NumeroTitleControlIdToReplace, AV45ddo_Contratada_CodigoTitleControlIdToReplace, AV49ddo_Contratada_PessoaCodTitleControlIdToReplace, AV53ddo_Contratada_PessoaNomTitleControlIdToReplace, AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV61ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace, AV67ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace, AV78Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutContratoArquivosAnexos_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContratoArquivosAnexos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContratoArquivosAnexos_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV8InOutContratoArquivosAnexos_Descricao = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContratoArquivosAnexos_Descricao", AV8InOutContratoArquivosAnexos_Descricao);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PA6M2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV78Pgmname = "PromptContratoArquivosAnexos";
               context.Gx_err = 0;
               WS6M2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WE6M2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117351242");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptcontratoarquivosanexos.aspx") + "?" + UrlEncode("" +AV7InOutContratoArquivosAnexos_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV8InOutContratoArquivosAnexos_Descricao))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOARQUIVOSANEXOS_DESCRICAO1", AV17ContratoArquivosAnexos_Descricao1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOARQUIVOSANEXOS_DESCRICAO2", AV21ContratoArquivosAnexos_Descricao2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOARQUIVOSANEXOS_DESCRICAO3", AV25ContratoArquivosAnexos_Descricao3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOARQUIVOSANEXOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31TFContratoArquivosAnexos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOARQUIVOSANEXOS_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32TFContratoArquivosAnexos_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35TFContrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATO_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36TFContrato_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATO_NUMERO", StringUtil.RTrim( AV39TFContrato_Numero));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATO_NUMERO_SEL", StringUtil.RTrim( AV40TFContrato_Numero_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV43TFContratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADA_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV44TFContratada_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADA_PESSOACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV47TFContratada_PessoaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADA_PESSOACOD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV48TFContratada_PessoaCod_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADA_PESSOANOM", StringUtil.RTrim( AV51TFContratada_PessoaNom));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADA_PESSOANOM_SEL", StringUtil.RTrim( AV52TFContratada_PessoaNom_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADA_PESSOACNPJ", AV55TFContratada_PessoaCNPJ);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADA_PESSOACNPJ_SEL", AV56TFContratada_PessoaCNPJ_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOARQUIVOSANEXOS_DESCRICAO", AV59TFContratoArquivosAnexos_Descricao);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOARQUIVOSANEXOS_DESCRICAO_SEL", AV60TFContratoArquivosAnexos_Descricao_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOARQUIVOSANEXOS_DATA", context.localUtil.TToC( AV63TFContratoArquivosAnexos_Data, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOARQUIVOSANEXOS_DATA_TO", context.localUtil.TToC( AV64TFContratoArquivosAnexos_Data_To, 10, 8, 0, 3, "/", ":", " "));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_80", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_80), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV70GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV71GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV68DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV68DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOARQUIVOSANEXOS_CODIGOTITLEFILTERDATA", AV30ContratoArquivosAnexos_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOARQUIVOSANEXOS_CODIGOTITLEFILTERDATA", AV30ContratoArquivosAnexos_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATO_CODIGOTITLEFILTERDATA", AV34Contrato_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATO_CODIGOTITLEFILTERDATA", AV34Contrato_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATO_NUMEROTITLEFILTERDATA", AV38Contrato_NumeroTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATO_NUMEROTITLEFILTERDATA", AV38Contrato_NumeroTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATADA_CODIGOTITLEFILTERDATA", AV42Contratada_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATADA_CODIGOTITLEFILTERDATA", AV42Contratada_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATADA_PESSOACODTITLEFILTERDATA", AV46Contratada_PessoaCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATADA_PESSOACODTITLEFILTERDATA", AV46Contratada_PessoaCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATADA_PESSOANOMTITLEFILTERDATA", AV50Contratada_PessoaNomTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATADA_PESSOANOMTITLEFILTERDATA", AV50Contratada_PessoaNomTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATADA_PESSOACNPJTITLEFILTERDATA", AV54Contratada_PessoaCNPJTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATADA_PESSOACNPJTITLEFILTERDATA", AV54Contratada_PessoaCNPJTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOARQUIVOSANEXOS_DESCRICAOTITLEFILTERDATA", AV58ContratoArquivosAnexos_DescricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOARQUIVOSANEXOS_DESCRICAOTITLEFILTERDATA", AV58ContratoArquivosAnexos_DescricaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOARQUIVOSANEXOS_DATATITLEFILTERDATA", AV62ContratoArquivosAnexos_DataTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOARQUIVOSANEXOS_DATATITLEFILTERDATA", AV62ContratoArquivosAnexos_DataTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV78Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTCONTRATOARQUIVOSANEXOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutContratoArquivosAnexos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTCONTRATOARQUIVOSANEXOS_DESCRICAO", AV8InOutContratoArquivosAnexos_Descricao);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_CODIGO_Caption", StringUtil.RTrim( Ddo_contratoarquivosanexos_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_CODIGO_Tooltip", StringUtil.RTrim( Ddo_contratoarquivosanexos_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_CODIGO_Cls", StringUtil.RTrim( Ddo_contratoarquivosanexos_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_contratoarquivosanexos_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoarquivosanexos_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoarquivosanexos_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoarquivosanexos_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_contratoarquivosanexos_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoarquivosanexos_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_contratoarquivosanexos_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_contratoarquivosanexos_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_CODIGO_Filtertype", StringUtil.RTrim( Ddo_contratoarquivosanexos_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_contratoarquivosanexos_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_contratoarquivosanexos_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_CODIGO_Sortasc", StringUtil.RTrim( Ddo_contratoarquivosanexos_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_contratoarquivosanexos_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_contratoarquivosanexos_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoarquivosanexos_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_contratoarquivosanexos_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_contratoarquivosanexos_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Caption", StringUtil.RTrim( Ddo_contrato_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Tooltip", StringUtil.RTrim( Ddo_contrato_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Cls", StringUtil.RTrim( Ddo_contrato_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_contrato_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_contrato_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contrato_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contrato_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_contrato_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_contrato_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_contrato_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_contrato_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Filtertype", StringUtil.RTrim( Ddo_contrato_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_contrato_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_contrato_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Sortasc", StringUtil.RTrim( Ddo_contrato_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_contrato_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_contrato_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_contrato_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_contrato_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_contrato_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Caption", StringUtil.RTrim( Ddo_contrato_numero_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Tooltip", StringUtil.RTrim( Ddo_contrato_numero_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Cls", StringUtil.RTrim( Ddo_contrato_numero_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filteredtext_set", StringUtil.RTrim( Ddo_contrato_numero_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Selectedvalue_set", StringUtil.RTrim( Ddo_contrato_numero_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contrato_numero_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contrato_numero_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includesortasc", StringUtil.BoolToStr( Ddo_contrato_numero_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includesortdsc", StringUtil.BoolToStr( Ddo_contrato_numero_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Sortedstatus", StringUtil.RTrim( Ddo_contrato_numero_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includefilter", StringUtil.BoolToStr( Ddo_contrato_numero_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filtertype", StringUtil.RTrim( Ddo_contrato_numero_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filterisrange", StringUtil.BoolToStr( Ddo_contrato_numero_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includedatalist", StringUtil.BoolToStr( Ddo_contrato_numero_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Datalisttype", StringUtil.RTrim( Ddo_contrato_numero_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Datalistproc", StringUtil.RTrim( Ddo_contrato_numero_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contrato_numero_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Sortasc", StringUtil.RTrim( Ddo_contrato_numero_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Sortdsc", StringUtil.RTrim( Ddo_contrato_numero_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Loadingdata", StringUtil.RTrim( Ddo_contrato_numero_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Cleanfilter", StringUtil.RTrim( Ddo_contrato_numero_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Noresultsfound", StringUtil.RTrim( Ddo_contrato_numero_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Searchbuttontext", StringUtil.RTrim( Ddo_contrato_numero_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Caption", StringUtil.RTrim( Ddo_contratada_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Tooltip", StringUtil.RTrim( Ddo_contratada_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Cls", StringUtil.RTrim( Ddo_contratada_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_contratada_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_contratada_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratada_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratada_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_contratada_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratada_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_contratada_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_contratada_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Filtertype", StringUtil.RTrim( Ddo_contratada_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_contratada_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_contratada_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Sortasc", StringUtil.RTrim( Ddo_contratada_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_contratada_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_contratada_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_contratada_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_contratada_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_contratada_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Caption", StringUtil.RTrim( Ddo_contratada_pessoacod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Tooltip", StringUtil.RTrim( Ddo_contratada_pessoacod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Cls", StringUtil.RTrim( Ddo_contratada_pessoacod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Filteredtext_set", StringUtil.RTrim( Ddo_contratada_pessoacod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Filteredtextto_set", StringUtil.RTrim( Ddo_contratada_pessoacod_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratada_pessoacod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratada_pessoacod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Includesortasc", StringUtil.BoolToStr( Ddo_contratada_pessoacod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Includesortdsc", StringUtil.BoolToStr( Ddo_contratada_pessoacod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Sortedstatus", StringUtil.RTrim( Ddo_contratada_pessoacod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Includefilter", StringUtil.BoolToStr( Ddo_contratada_pessoacod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Filtertype", StringUtil.RTrim( Ddo_contratada_pessoacod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Filterisrange", StringUtil.BoolToStr( Ddo_contratada_pessoacod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Includedatalist", StringUtil.BoolToStr( Ddo_contratada_pessoacod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Sortasc", StringUtil.RTrim( Ddo_contratada_pessoacod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Sortdsc", StringUtil.RTrim( Ddo_contratada_pessoacod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Cleanfilter", StringUtil.RTrim( Ddo_contratada_pessoacod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Rangefilterfrom", StringUtil.RTrim( Ddo_contratada_pessoacod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Rangefilterto", StringUtil.RTrim( Ddo_contratada_pessoacod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Searchbuttontext", StringUtil.RTrim( Ddo_contratada_pessoacod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Caption", StringUtil.RTrim( Ddo_contratada_pessoanom_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Tooltip", StringUtil.RTrim( Ddo_contratada_pessoanom_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Cls", StringUtil.RTrim( Ddo_contratada_pessoanom_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Filteredtext_set", StringUtil.RTrim( Ddo_contratada_pessoanom_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Selectedvalue_set", StringUtil.RTrim( Ddo_contratada_pessoanom_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratada_pessoanom_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratada_pessoanom_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Includesortasc", StringUtil.BoolToStr( Ddo_contratada_pessoanom_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Includesortdsc", StringUtil.BoolToStr( Ddo_contratada_pessoanom_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Sortedstatus", StringUtil.RTrim( Ddo_contratada_pessoanom_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Includefilter", StringUtil.BoolToStr( Ddo_contratada_pessoanom_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Filtertype", StringUtil.RTrim( Ddo_contratada_pessoanom_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Filterisrange", StringUtil.BoolToStr( Ddo_contratada_pessoanom_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Includedatalist", StringUtil.BoolToStr( Ddo_contratada_pessoanom_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Datalisttype", StringUtil.RTrim( Ddo_contratada_pessoanom_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Datalistproc", StringUtil.RTrim( Ddo_contratada_pessoanom_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratada_pessoanom_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Sortasc", StringUtil.RTrim( Ddo_contratada_pessoanom_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Sortdsc", StringUtil.RTrim( Ddo_contratada_pessoanom_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Loadingdata", StringUtil.RTrim( Ddo_contratada_pessoanom_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Cleanfilter", StringUtil.RTrim( Ddo_contratada_pessoanom_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Noresultsfound", StringUtil.RTrim( Ddo_contratada_pessoanom_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Searchbuttontext", StringUtil.RTrim( Ddo_contratada_pessoanom_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Caption", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Tooltip", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Cls", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Filteredtext_set", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Selectedvalue_set", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Includesortasc", StringUtil.BoolToStr( Ddo_contratada_pessoacnpj_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Includesortdsc", StringUtil.BoolToStr( Ddo_contratada_pessoacnpj_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Sortedstatus", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Includefilter", StringUtil.BoolToStr( Ddo_contratada_pessoacnpj_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Filtertype", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Filterisrange", StringUtil.BoolToStr( Ddo_contratada_pessoacnpj_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Includedatalist", StringUtil.BoolToStr( Ddo_contratada_pessoacnpj_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Datalisttype", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Datalistproc", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratada_pessoacnpj_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Sortasc", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Sortdsc", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Loadingdata", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Cleanfilter", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Noresultsfound", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Searchbuttontext", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Caption", StringUtil.RTrim( Ddo_contratoarquivosanexos_descricao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_contratoarquivosanexos_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Cls", StringUtil.RTrim( Ddo_contratoarquivosanexos_descricao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_contratoarquivosanexos_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoarquivosanexos_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoarquivosanexos_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoarquivosanexos_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_contratoarquivosanexos_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoarquivosanexos_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_contratoarquivosanexos_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_contratoarquivosanexos_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_contratoarquivosanexos_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_contratoarquivosanexos_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_contratoarquivosanexos_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_contratoarquivosanexos_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_contratoarquivosanexos_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratoarquivosanexos_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_contratoarquivosanexos_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_contratoarquivosanexos_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_contratoarquivosanexos_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_contratoarquivosanexos_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_contratoarquivosanexos_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_contratoarquivosanexos_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DATA_Caption", StringUtil.RTrim( Ddo_contratoarquivosanexos_data_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DATA_Tooltip", StringUtil.RTrim( Ddo_contratoarquivosanexos_data_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DATA_Cls", StringUtil.RTrim( Ddo_contratoarquivosanexos_data_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DATA_Filteredtext_set", StringUtil.RTrim( Ddo_contratoarquivosanexos_data_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DATA_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoarquivosanexos_data_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DATA_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoarquivosanexos_data_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DATA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoarquivosanexos_data_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DATA_Includesortasc", StringUtil.BoolToStr( Ddo_contratoarquivosanexos_data_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DATA_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoarquivosanexos_data_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DATA_Sortedstatus", StringUtil.RTrim( Ddo_contratoarquivosanexos_data_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DATA_Includefilter", StringUtil.BoolToStr( Ddo_contratoarquivosanexos_data_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DATA_Filtertype", StringUtil.RTrim( Ddo_contratoarquivosanexos_data_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DATA_Filterisrange", StringUtil.BoolToStr( Ddo_contratoarquivosanexos_data_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DATA_Includedatalist", StringUtil.BoolToStr( Ddo_contratoarquivosanexos_data_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DATA_Sortasc", StringUtil.RTrim( Ddo_contratoarquivosanexos_data_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DATA_Sortdsc", StringUtil.RTrim( Ddo_contratoarquivosanexos_data_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DATA_Cleanfilter", StringUtil.RTrim( Ddo_contratoarquivosanexos_data_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DATA_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoarquivosanexos_data_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DATA_Rangefilterto", StringUtil.RTrim( Ddo_contratoarquivosanexos_data_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DATA_Searchbuttontext", StringUtil.RTrim( Ddo_contratoarquivosanexos_data_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_contratoarquivosanexos_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_contratoarquivosanexos_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoarquivosanexos_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_contrato_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_contrato_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_contrato_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Activeeventkey", StringUtil.RTrim( Ddo_contrato_numero_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filteredtext_get", StringUtil.RTrim( Ddo_contrato_numero_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Selectedvalue_get", StringUtil.RTrim( Ddo_contrato_numero_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_contratada_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_contratada_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_contratada_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Activeeventkey", StringUtil.RTrim( Ddo_contratada_pessoacod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Filteredtext_get", StringUtil.RTrim( Ddo_contratada_pessoacod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACOD_Filteredtextto_get", StringUtil.RTrim( Ddo_contratada_pessoacod_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Activeeventkey", StringUtil.RTrim( Ddo_contratada_pessoanom_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Filteredtext_get", StringUtil.RTrim( Ddo_contratada_pessoanom_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Selectedvalue_get", StringUtil.RTrim( Ddo_contratada_pessoanom_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Activeeventkey", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Filteredtext_get", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Selectedvalue_get", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_contratoarquivosanexos_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_contratoarquivosanexos_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoarquivosanexos_descricao_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DATA_Activeeventkey", StringUtil.RTrim( Ddo_contratoarquivosanexos_data_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DATA_Filteredtext_get", StringUtil.RTrim( Ddo_contratoarquivosanexos_data_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOARQUIVOSANEXOS_DATA_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoarquivosanexos_data_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm6M2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptContratoArquivosAnexos" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Contrato Arquivos Anexos" ;
      }

      protected void WB6M0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_6M2( true) ;
         }
         else
         {
            wb_table1_2_6M2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_6M2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(98, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,98);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(99, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,99);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoarquivosanexos_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31TFContratoArquivosAnexos_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV31TFContratoArquivosAnexos_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,100);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoarquivosanexos_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoarquivosanexos_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoArquivosAnexos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoarquivosanexos_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32TFContratoArquivosAnexos_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV32TFContratoArquivosAnexos_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,101);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoarquivosanexos_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoarquivosanexos_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoArquivosAnexos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontrato_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35TFContrato_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV35TFContrato_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,102);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontrato_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontrato_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoArquivosAnexos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontrato_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36TFContrato_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV36TFContrato_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,103);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontrato_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontrato_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoArquivosAnexos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontrato_numero_Internalname, StringUtil.RTrim( AV39TFContrato_Numero), StringUtil.RTrim( context.localUtil.Format( AV39TFContrato_Numero, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,104);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontrato_numero_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontrato_numero_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoArquivosAnexos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontrato_numero_sel_Internalname, StringUtil.RTrim( AV40TFContrato_Numero_Sel), StringUtil.RTrim( context.localUtil.Format( AV40TFContrato_Numero_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,105);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontrato_numero_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontrato_numero_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoArquivosAnexos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratada_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV43TFContratada_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV43TFContratada_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,106);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratada_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratada_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoArquivosAnexos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratada_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV44TFContratada_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV44TFContratada_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,107);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratada_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratada_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoArquivosAnexos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratada_pessoacod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV47TFContratada_PessoaCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV47TFContratada_PessoaCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,108);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratada_pessoacod_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratada_pessoacod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoArquivosAnexos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratada_pessoacod_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV48TFContratada_PessoaCod_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV48TFContratada_PessoaCod_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,109);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratada_pessoacod_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratada_pessoacod_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoArquivosAnexos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratada_pessoanom_Internalname, StringUtil.RTrim( AV51TFContratada_PessoaNom), StringUtil.RTrim( context.localUtil.Format( AV51TFContratada_PessoaNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,110);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratada_pessoanom_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontratada_pessoanom_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoArquivosAnexos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratada_pessoanom_sel_Internalname, StringUtil.RTrim( AV52TFContratada_PessoaNom_Sel), StringUtil.RTrim( context.localUtil.Format( AV52TFContratada_PessoaNom_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,111);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratada_pessoanom_sel_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontratada_pessoanom_sel_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoArquivosAnexos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratada_pessoacnpj_Internalname, AV55TFContratada_PessoaCNPJ, StringUtil.RTrim( context.localUtil.Format( AV55TFContratada_PessoaCNPJ, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,112);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratada_pessoacnpj_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratada_pessoacnpj_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoArquivosAnexos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratada_pessoacnpj_sel_Internalname, AV56TFContratada_PessoaCNPJ_Sel, StringUtil.RTrim( context.localUtil.Format( AV56TFContratada_PessoaCNPJ_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,113);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratada_pessoacnpj_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratada_pessoacnpj_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContratoArquivosAnexos.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfcontratoarquivosanexos_descricao_Internalname, AV59TFContratoArquivosAnexos_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,114);\"", 0, edtavTfcontratoarquivosanexos_descricao_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_PromptContratoArquivosAnexos.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfcontratoarquivosanexos_descricao_sel_Internalname, AV60TFContratoArquivosAnexos_Descricao_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,115);\"", 0, edtavTfcontratoarquivosanexos_descricao_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_PromptContratoArquivosAnexos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'',false,'" + sGXsfl_80_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratoarquivosanexos_data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoarquivosanexos_data_Internalname, context.localUtil.TToC( AV63TFContratoArquivosAnexos_Data, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV63TFContratoArquivosAnexos_Data, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,116);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoarquivosanexos_data_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoarquivosanexos_data_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoArquivosAnexos.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratoarquivosanexos_data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratoarquivosanexos_data_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoArquivosAnexos.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'',false,'" + sGXsfl_80_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratoarquivosanexos_data_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoarquivosanexos_data_to_Internalname, context.localUtil.TToC( AV64TFContratoArquivosAnexos_Data_To, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV64TFContratoArquivosAnexos_Data_To, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,117);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoarquivosanexos_data_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoarquivosanexos_data_to_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoArquivosAnexos.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratoarquivosanexos_data_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratoarquivosanexos_data_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoArquivosAnexos.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_contratoarquivosanexos_dataauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'',false,'" + sGXsfl_80_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratoarquivosanexos_dataauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratoarquivosanexos_dataauxdate_Internalname, context.localUtil.Format(AV65DDO_ContratoArquivosAnexos_DataAuxDate, "99/99/99"), context.localUtil.Format( AV65DDO_ContratoArquivosAnexos_DataAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,119);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratoarquivosanexos_dataauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoArquivosAnexos.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratoarquivosanexos_dataauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoArquivosAnexos.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 120,'',false,'" + sGXsfl_80_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratoarquivosanexos_dataauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratoarquivosanexos_dataauxdateto_Internalname, context.localUtil.Format(AV66DDO_ContratoArquivosAnexos_DataAuxDateTo, "99/99/99"), context.localUtil.Format( AV66DDO_ContratoArquivosAnexos_DataAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,120);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratoarquivosanexos_dataauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContratoArquivosAnexos.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratoarquivosanexos_dataauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContratoArquivosAnexos.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOARQUIVOSANEXOS_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 122,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoarquivosanexos_codigotitlecontrolidtoreplace_Internalname, AV33ddo_ContratoArquivosAnexos_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,122);\"", 0, edtavDdo_contratoarquivosanexos_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoArquivosAnexos.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATO_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 124,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contrato_codigotitlecontrolidtoreplace_Internalname, AV37ddo_Contrato_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,124);\"", 0, edtavDdo_contrato_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoArquivosAnexos.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATO_NUMEROContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 126,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname, AV41ddo_Contrato_NumeroTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,126);\"", 0, edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoArquivosAnexos.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATADA_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 128,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratada_codigotitlecontrolidtoreplace_Internalname, AV45ddo_Contratada_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,128);\"", 0, edtavDdo_contratada_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoArquivosAnexos.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATADA_PESSOACODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 130,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratada_pessoacodtitlecontrolidtoreplace_Internalname, AV49ddo_Contratada_PessoaCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,130);\"", 0, edtavDdo_contratada_pessoacodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoArquivosAnexos.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATADA_PESSOANOMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 132,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Internalname, AV53ddo_Contratada_PessoaNomTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,132);\"", 0, edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoArquivosAnexos.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATADA_PESSOACNPJContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 134,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Internalname, AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,134);\"", 0, edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoArquivosAnexos.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOARQUIVOSANEXOS_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 136,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoarquivosanexos_descricaotitlecontrolidtoreplace_Internalname, AV61ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,136);\"", 0, edtavDdo_contratoarquivosanexos_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoArquivosAnexos.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOARQUIVOSANEXOS_DATAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 138,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoarquivosanexos_datatitlecontrolidtoreplace_Internalname, AV67ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,138);\"", 0, edtavDdo_contratoarquivosanexos_datatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContratoArquivosAnexos.htm");
         }
         wbLoad = true;
      }

      protected void START6M2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Contrato Arquivos Anexos", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP6M0( ) ;
      }

      protected void WS6M2( )
      {
         START6M2( ) ;
         EVT6M2( ) ;
      }

      protected void EVT6M2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E116M2 */
                           E116M2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOARQUIVOSANEXOS_CODIGO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E126M2 */
                           E126M2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATO_CODIGO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E136M2 */
                           E136M2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATO_NUMERO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E146M2 */
                           E146M2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATADA_CODIGO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E156M2 */
                           E156M2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATADA_PESSOACOD.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E166M2 */
                           E166M2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATADA_PESSOANOM.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E176M2 */
                           E176M2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATADA_PESSOACNPJ.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E186M2 */
                           E186M2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E196M2 */
                           E196M2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOARQUIVOSANEXOS_DATA.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E206M2 */
                           E206M2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E216M2 */
                           E216M2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E226M2 */
                           E226M2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E236M2 */
                           E236M2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E246M2 */
                           E246M2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E256M2 */
                           E256M2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E266M2 */
                           E266M2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E276M2 */
                           E276M2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E286M2 */
                           E286M2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E296M2 */
                           E296M2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E306M2 */
                           E306M2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_80_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
                           SubsflControlProps_802( ) ;
                           AV28Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV77Select_GXI : context.convertURL( context.PathToRelativeUrl( AV28Select))));
                           A108ContratoArquivosAnexos_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoArquivosAnexos_Codigo_Internalname), ",", "."));
                           A74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContrato_Codigo_Internalname), ",", "."));
                           A77Contrato_Numero = cgiGet( edtContrato_Numero_Internalname);
                           A39Contratada_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratada_Codigo_Internalname), ",", "."));
                           A40Contratada_PessoaCod = (int)(context.localUtil.CToN( cgiGet( edtContratada_PessoaCod_Internalname), ",", "."));
                           A41Contratada_PessoaNom = StringUtil.Upper( cgiGet( edtContratada_PessoaNom_Internalname));
                           n41Contratada_PessoaNom = false;
                           A42Contratada_PessoaCNPJ = cgiGet( edtContratada_PessoaCNPJ_Internalname);
                           n42Contratada_PessoaCNPJ = false;
                           A110ContratoArquivosAnexos_Descricao = cgiGet( edtContratoArquivosAnexos_Descricao_Internalname);
                           A111ContratoArquivosAnexos_Arquivo = cgiGet( edtContratoArquivosAnexos_Arquivo_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoArquivosAnexos_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A111ContratoArquivosAnexos_Arquivo));
                           A113ContratoArquivosAnexos_Data = context.localUtil.CToT( cgiGet( edtContratoArquivosAnexos_Data_Internalname), 0);
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E316M2 */
                                 E316M2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E326M2 */
                                 E326M2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E336M2 */
                                 E336M2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratoarquivosanexos_descricao1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATOARQUIVOSANEXOS_DESCRICAO1"), AV17ContratoArquivosAnexos_Descricao1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratoarquivosanexos_descricao2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATOARQUIVOSANEXOS_DESCRICAO2"), AV21ContratoArquivosAnexos_Descricao2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contratoarquivosanexos_descricao3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATOARQUIVOSANEXOS_DESCRICAO3"), AV25ContratoArquivosAnexos_Descricao3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoarquivosanexos_codigo Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOARQUIVOSANEXOS_CODIGO"), ",", ".") != Convert.ToDecimal( AV31TFContratoArquivosAnexos_Codigo )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoarquivosanexos_codigo_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOARQUIVOSANEXOS_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV32TFContratoArquivosAnexos_Codigo_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontrato_codigo Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATO_CODIGO"), ",", ".") != Convert.ToDecimal( AV35TFContrato_Codigo )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontrato_codigo_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV36TFContrato_Codigo_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontrato_numero Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO"), AV39TFContrato_Numero) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontrato_numero_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO_SEL"), AV40TFContrato_Numero_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratada_codigo Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATADA_CODIGO"), ",", ".") != Convert.ToDecimal( AV43TFContratada_Codigo )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratada_codigo_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATADA_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV44TFContratada_Codigo_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratada_pessoacod Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATADA_PESSOACOD"), ",", ".") != Convert.ToDecimal( AV47TFContratada_PessoaCod )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratada_pessoacod_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATADA_PESSOACOD_TO"), ",", ".") != Convert.ToDecimal( AV48TFContratada_PessoaCod_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratada_pessoanom Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOANOM"), AV51TFContratada_PessoaNom) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratada_pessoanom_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOANOM_SEL"), AV52TFContratada_PessoaNom_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratada_pessoacnpj Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOACNPJ"), AV55TFContratada_PessoaCNPJ) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratada_pessoacnpj_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOACNPJ_SEL"), AV56TFContratada_PessoaCNPJ_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoarquivosanexos_descricao Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOARQUIVOSANEXOS_DESCRICAO"), AV59TFContratoArquivosAnexos_Descricao) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoarquivosanexos_descricao_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOARQUIVOSANEXOS_DESCRICAO_SEL"), AV60TFContratoArquivosAnexos_Descricao_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoarquivosanexos_data Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOARQUIVOSANEXOS_DATA"), 0) != AV63TFContratoArquivosAnexos_Data )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratoarquivosanexos_data_to Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOARQUIVOSANEXOS_DATA_TO"), 0) != AV64TFContratoArquivosAnexos_Data_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E346M2 */
                                       E346M2 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WE6M2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm6M2( ) ;
            }
         }
      }

      protected void PA6M2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CONTRATOARQUIVOSANEXOS_DESCRICAO", "Descri��o do Arquvo", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CONTRATOARQUIVOSANEXOS_DESCRICAO", "Descri��o do Arquvo", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("CONTRATOARQUIVOSANEXOS_DESCRICAO", "Descri��o do Arquvo", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_802( ) ;
         while ( nGXsfl_80_idx <= nRC_GXsfl_80 )
         {
            sendrow_802( ) ;
            nGXsfl_80_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_80_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_80_idx+1));
            sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
            SubsflControlProps_802( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       String AV17ContratoArquivosAnexos_Descricao1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       short AV20DynamicFiltersOperator2 ,
                                       String AV21ContratoArquivosAnexos_Descricao2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       short AV24DynamicFiltersOperator3 ,
                                       String AV25ContratoArquivosAnexos_Descricao3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       int AV31TFContratoArquivosAnexos_Codigo ,
                                       int AV32TFContratoArquivosAnexos_Codigo_To ,
                                       int AV35TFContrato_Codigo ,
                                       int AV36TFContrato_Codigo_To ,
                                       String AV39TFContrato_Numero ,
                                       String AV40TFContrato_Numero_Sel ,
                                       int AV43TFContratada_Codigo ,
                                       int AV44TFContratada_Codigo_To ,
                                       int AV47TFContratada_PessoaCod ,
                                       int AV48TFContratada_PessoaCod_To ,
                                       String AV51TFContratada_PessoaNom ,
                                       String AV52TFContratada_PessoaNom_Sel ,
                                       String AV55TFContratada_PessoaCNPJ ,
                                       String AV56TFContratada_PessoaCNPJ_Sel ,
                                       String AV59TFContratoArquivosAnexos_Descricao ,
                                       String AV60TFContratoArquivosAnexos_Descricao_Sel ,
                                       DateTime AV63TFContratoArquivosAnexos_Data ,
                                       DateTime AV64TFContratoArquivosAnexos_Data_To ,
                                       String AV33ddo_ContratoArquivosAnexos_CodigoTitleControlIdToReplace ,
                                       String AV37ddo_Contrato_CodigoTitleControlIdToReplace ,
                                       String AV41ddo_Contrato_NumeroTitleControlIdToReplace ,
                                       String AV45ddo_Contratada_CodigoTitleControlIdToReplace ,
                                       String AV49ddo_Contratada_PessoaCodTitleControlIdToReplace ,
                                       String AV53ddo_Contratada_PessoaNomTitleControlIdToReplace ,
                                       String AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace ,
                                       String AV61ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace ,
                                       String AV67ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace ,
                                       String AV78Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF6M2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOARQUIVOSANEXOS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A108ContratoArquivosAnexos_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATOARQUIVOSANEXOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A108ContratoArquivosAnexos_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOARQUIVOSANEXOS_DESCRICAO", GetSecureSignedToken( "", A110ContratoArquivosAnexos_Descricao));
         GxWebStd.gx_hidden_field( context, "CONTRATOARQUIVOSANEXOS_DESCRICAO", A110ContratoArquivosAnexos_Descricao);
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOARQUIVOSANEXOS_DATA", GetSecureSignedToken( "", context.localUtil.Format( A113ContratoArquivosAnexos_Data, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, "CONTRATOARQUIVOSANEXOS_DATA", context.localUtil.TToC( A113ContratoArquivosAnexos_Data, 10, 8, 0, 3, "/", ":", " "));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF6M2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV78Pgmname = "PromptContratoArquivosAnexos";
         context.Gx_err = 0;
      }

      protected void RF6M2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 80;
         /* Execute user event: E326M2 */
         E326M2 ();
         nGXsfl_80_idx = 1;
         sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
         SubsflControlProps_802( ) ;
         nGXsfl_80_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_802( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV15DynamicFiltersSelector1 ,
                                                 AV16DynamicFiltersOperator1 ,
                                                 AV17ContratoArquivosAnexos_Descricao1 ,
                                                 AV18DynamicFiltersEnabled2 ,
                                                 AV19DynamicFiltersSelector2 ,
                                                 AV20DynamicFiltersOperator2 ,
                                                 AV21ContratoArquivosAnexos_Descricao2 ,
                                                 AV22DynamicFiltersEnabled3 ,
                                                 AV23DynamicFiltersSelector3 ,
                                                 AV24DynamicFiltersOperator3 ,
                                                 AV25ContratoArquivosAnexos_Descricao3 ,
                                                 AV31TFContratoArquivosAnexos_Codigo ,
                                                 AV32TFContratoArquivosAnexos_Codigo_To ,
                                                 AV35TFContrato_Codigo ,
                                                 AV36TFContrato_Codigo_To ,
                                                 AV40TFContrato_Numero_Sel ,
                                                 AV39TFContrato_Numero ,
                                                 AV43TFContratada_Codigo ,
                                                 AV44TFContratada_Codigo_To ,
                                                 AV47TFContratada_PessoaCod ,
                                                 AV48TFContratada_PessoaCod_To ,
                                                 AV52TFContratada_PessoaNom_Sel ,
                                                 AV51TFContratada_PessoaNom ,
                                                 AV56TFContratada_PessoaCNPJ_Sel ,
                                                 AV55TFContratada_PessoaCNPJ ,
                                                 AV60TFContratoArquivosAnexos_Descricao_Sel ,
                                                 AV59TFContratoArquivosAnexos_Descricao ,
                                                 AV63TFContratoArquivosAnexos_Data ,
                                                 AV64TFContratoArquivosAnexos_Data_To ,
                                                 A110ContratoArquivosAnexos_Descricao ,
                                                 A108ContratoArquivosAnexos_Codigo ,
                                                 A74Contrato_Codigo ,
                                                 A77Contrato_Numero ,
                                                 A39Contratada_Codigo ,
                                                 A40Contratada_PessoaCod ,
                                                 A41Contratada_PessoaNom ,
                                                 A42Contratada_PessoaCNPJ ,
                                                 A113ContratoArquivosAnexos_Data ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                                 TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                                 TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE,
                                                 TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV17ContratoArquivosAnexos_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV17ContratoArquivosAnexos_Descricao1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoArquivosAnexos_Descricao1", AV17ContratoArquivosAnexos_Descricao1);
            lV17ContratoArquivosAnexos_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV17ContratoArquivosAnexos_Descricao1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoArquivosAnexos_Descricao1", AV17ContratoArquivosAnexos_Descricao1);
            lV21ContratoArquivosAnexos_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV21ContratoArquivosAnexos_Descricao2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoArquivosAnexos_Descricao2", AV21ContratoArquivosAnexos_Descricao2);
            lV21ContratoArquivosAnexos_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV21ContratoArquivosAnexos_Descricao2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoArquivosAnexos_Descricao2", AV21ContratoArquivosAnexos_Descricao2);
            lV25ContratoArquivosAnexos_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV25ContratoArquivosAnexos_Descricao3), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoArquivosAnexos_Descricao3", AV25ContratoArquivosAnexos_Descricao3);
            lV25ContratoArquivosAnexos_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV25ContratoArquivosAnexos_Descricao3), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoArquivosAnexos_Descricao3", AV25ContratoArquivosAnexos_Descricao3);
            lV39TFContrato_Numero = StringUtil.PadR( StringUtil.RTrim( AV39TFContrato_Numero), 20, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContrato_Numero", AV39TFContrato_Numero);
            lV51TFContratada_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV51TFContratada_PessoaNom), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFContratada_PessoaNom", AV51TFContratada_PessoaNom);
            lV55TFContratada_PessoaCNPJ = StringUtil.Concat( StringUtil.RTrim( AV55TFContratada_PessoaCNPJ), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFContratada_PessoaCNPJ", AV55TFContratada_PessoaCNPJ);
            lV59TFContratoArquivosAnexos_Descricao = StringUtil.Concat( StringUtil.RTrim( AV59TFContratoArquivosAnexos_Descricao), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContratoArquivosAnexos_Descricao", AV59TFContratoArquivosAnexos_Descricao);
            /* Using cursor H006M2 */
            pr_default.execute(0, new Object[] {lV17ContratoArquivosAnexos_Descricao1, lV17ContratoArquivosAnexos_Descricao1, lV21ContratoArquivosAnexos_Descricao2, lV21ContratoArquivosAnexos_Descricao2, lV25ContratoArquivosAnexos_Descricao3, lV25ContratoArquivosAnexos_Descricao3, AV31TFContratoArquivosAnexos_Codigo, AV32TFContratoArquivosAnexos_Codigo_To, AV35TFContrato_Codigo, AV36TFContrato_Codigo_To, lV39TFContrato_Numero, AV40TFContrato_Numero_Sel, AV43TFContratada_Codigo, AV44TFContratada_Codigo_To, AV47TFContratada_PessoaCod, AV48TFContratada_PessoaCod_To, lV51TFContratada_PessoaNom, AV52TFContratada_PessoaNom_Sel, lV55TFContratada_PessoaCNPJ, AV56TFContratada_PessoaCNPJ_Sel, lV59TFContratoArquivosAnexos_Descricao, AV60TFContratoArquivosAnexos_Descricao_Sel, AV63TFContratoArquivosAnexos_Data, AV64TFContratoArquivosAnexos_Data_To, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_80_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A112ContratoArquivosAnexos_NomeArq = H006M2_A112ContratoArquivosAnexos_NomeArq[0];
               edtContratoArquivosAnexos_Arquivo_Filename = A112ContratoArquivosAnexos_NomeArq;
               A109ContratoArquivosAnexos_TipoArq = H006M2_A109ContratoArquivosAnexos_TipoArq[0];
               edtContratoArquivosAnexos_Arquivo_Filetype = A109ContratoArquivosAnexos_TipoArq;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoArquivosAnexos_Arquivo_Internalname, "Filetype", edtContratoArquivosAnexos_Arquivo_Filetype);
               A113ContratoArquivosAnexos_Data = H006M2_A113ContratoArquivosAnexos_Data[0];
               A110ContratoArquivosAnexos_Descricao = H006M2_A110ContratoArquivosAnexos_Descricao[0];
               A42Contratada_PessoaCNPJ = H006M2_A42Contratada_PessoaCNPJ[0];
               n42Contratada_PessoaCNPJ = H006M2_n42Contratada_PessoaCNPJ[0];
               A41Contratada_PessoaNom = H006M2_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = H006M2_n41Contratada_PessoaNom[0];
               A40Contratada_PessoaCod = H006M2_A40Contratada_PessoaCod[0];
               A39Contratada_Codigo = H006M2_A39Contratada_Codigo[0];
               A77Contrato_Numero = H006M2_A77Contrato_Numero[0];
               A74Contrato_Codigo = H006M2_A74Contrato_Codigo[0];
               A108ContratoArquivosAnexos_Codigo = H006M2_A108ContratoArquivosAnexos_Codigo[0];
               A111ContratoArquivosAnexos_Arquivo = H006M2_A111ContratoArquivosAnexos_Arquivo[0];
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoArquivosAnexos_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A111ContratoArquivosAnexos_Arquivo));
               A39Contratada_Codigo = H006M2_A39Contratada_Codigo[0];
               A77Contrato_Numero = H006M2_A77Contrato_Numero[0];
               A40Contratada_PessoaCod = H006M2_A40Contratada_PessoaCod[0];
               A42Contratada_PessoaCNPJ = H006M2_A42Contratada_PessoaCNPJ[0];
               n42Contratada_PessoaCNPJ = H006M2_n42Contratada_PessoaCNPJ[0];
               A41Contratada_PessoaNom = H006M2_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = H006M2_n41Contratada_PessoaNom[0];
               /* Execute user event: E336M2 */
               E336M2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 80;
            WB6M0( ) ;
         }
         nGXsfl_80_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV15DynamicFiltersSelector1 ,
                                              AV16DynamicFiltersOperator1 ,
                                              AV17ContratoArquivosAnexos_Descricao1 ,
                                              AV18DynamicFiltersEnabled2 ,
                                              AV19DynamicFiltersSelector2 ,
                                              AV20DynamicFiltersOperator2 ,
                                              AV21ContratoArquivosAnexos_Descricao2 ,
                                              AV22DynamicFiltersEnabled3 ,
                                              AV23DynamicFiltersSelector3 ,
                                              AV24DynamicFiltersOperator3 ,
                                              AV25ContratoArquivosAnexos_Descricao3 ,
                                              AV31TFContratoArquivosAnexos_Codigo ,
                                              AV32TFContratoArquivosAnexos_Codigo_To ,
                                              AV35TFContrato_Codigo ,
                                              AV36TFContrato_Codigo_To ,
                                              AV40TFContrato_Numero_Sel ,
                                              AV39TFContrato_Numero ,
                                              AV43TFContratada_Codigo ,
                                              AV44TFContratada_Codigo_To ,
                                              AV47TFContratada_PessoaCod ,
                                              AV48TFContratada_PessoaCod_To ,
                                              AV52TFContratada_PessoaNom_Sel ,
                                              AV51TFContratada_PessoaNom ,
                                              AV56TFContratada_PessoaCNPJ_Sel ,
                                              AV55TFContratada_PessoaCNPJ ,
                                              AV60TFContratoArquivosAnexos_Descricao_Sel ,
                                              AV59TFContratoArquivosAnexos_Descricao ,
                                              AV63TFContratoArquivosAnexos_Data ,
                                              AV64TFContratoArquivosAnexos_Data_To ,
                                              A110ContratoArquivosAnexos_Descricao ,
                                              A108ContratoArquivosAnexos_Codigo ,
                                              A74Contrato_Codigo ,
                                              A77Contrato_Numero ,
                                              A39Contratada_Codigo ,
                                              A40Contratada_PessoaCod ,
                                              A41Contratada_PessoaNom ,
                                              A42Contratada_PessoaCNPJ ,
                                              A113ContratoArquivosAnexos_Data ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE,
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV17ContratoArquivosAnexos_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV17ContratoArquivosAnexos_Descricao1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoArquivosAnexos_Descricao1", AV17ContratoArquivosAnexos_Descricao1);
         lV17ContratoArquivosAnexos_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV17ContratoArquivosAnexos_Descricao1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoArquivosAnexos_Descricao1", AV17ContratoArquivosAnexos_Descricao1);
         lV21ContratoArquivosAnexos_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV21ContratoArquivosAnexos_Descricao2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoArquivosAnexos_Descricao2", AV21ContratoArquivosAnexos_Descricao2);
         lV21ContratoArquivosAnexos_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV21ContratoArquivosAnexos_Descricao2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoArquivosAnexos_Descricao2", AV21ContratoArquivosAnexos_Descricao2);
         lV25ContratoArquivosAnexos_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV25ContratoArquivosAnexos_Descricao3), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoArquivosAnexos_Descricao3", AV25ContratoArquivosAnexos_Descricao3);
         lV25ContratoArquivosAnexos_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV25ContratoArquivosAnexos_Descricao3), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoArquivosAnexos_Descricao3", AV25ContratoArquivosAnexos_Descricao3);
         lV39TFContrato_Numero = StringUtil.PadR( StringUtil.RTrim( AV39TFContrato_Numero), 20, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContrato_Numero", AV39TFContrato_Numero);
         lV51TFContratada_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV51TFContratada_PessoaNom), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFContratada_PessoaNom", AV51TFContratada_PessoaNom);
         lV55TFContratada_PessoaCNPJ = StringUtil.Concat( StringUtil.RTrim( AV55TFContratada_PessoaCNPJ), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFContratada_PessoaCNPJ", AV55TFContratada_PessoaCNPJ);
         lV59TFContratoArquivosAnexos_Descricao = StringUtil.Concat( StringUtil.RTrim( AV59TFContratoArquivosAnexos_Descricao), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContratoArquivosAnexos_Descricao", AV59TFContratoArquivosAnexos_Descricao);
         /* Using cursor H006M3 */
         pr_default.execute(1, new Object[] {lV17ContratoArquivosAnexos_Descricao1, lV17ContratoArquivosAnexos_Descricao1, lV21ContratoArquivosAnexos_Descricao2, lV21ContratoArquivosAnexos_Descricao2, lV25ContratoArquivosAnexos_Descricao3, lV25ContratoArquivosAnexos_Descricao3, AV31TFContratoArquivosAnexos_Codigo, AV32TFContratoArquivosAnexos_Codigo_To, AV35TFContrato_Codigo, AV36TFContrato_Codigo_To, lV39TFContrato_Numero, AV40TFContrato_Numero_Sel, AV43TFContratada_Codigo, AV44TFContratada_Codigo_To, AV47TFContratada_PessoaCod, AV48TFContratada_PessoaCod_To, lV51TFContratada_PessoaNom, AV52TFContratada_PessoaNom_Sel, lV55TFContratada_PessoaCNPJ, AV56TFContratada_PessoaCNPJ_Sel, lV59TFContratoArquivosAnexos_Descricao, AV60TFContratoArquivosAnexos_Descricao_Sel, AV63TFContratoArquivosAnexos_Data, AV64TFContratoArquivosAnexos_Data_To});
         GRID_nRecordCount = H006M3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoArquivosAnexos_Descricao1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoArquivosAnexos_Descricao2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ContratoArquivosAnexos_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFContratoArquivosAnexos_Codigo, AV32TFContratoArquivosAnexos_Codigo_To, AV35TFContrato_Codigo, AV36TFContrato_Codigo_To, AV39TFContrato_Numero, AV40TFContrato_Numero_Sel, AV43TFContratada_Codigo, AV44TFContratada_Codigo_To, AV47TFContratada_PessoaCod, AV48TFContratada_PessoaCod_To, AV51TFContratada_PessoaNom, AV52TFContratada_PessoaNom_Sel, AV55TFContratada_PessoaCNPJ, AV56TFContratada_PessoaCNPJ_Sel, AV59TFContratoArquivosAnexos_Descricao, AV60TFContratoArquivosAnexos_Descricao_Sel, AV63TFContratoArquivosAnexos_Data, AV64TFContratoArquivosAnexos_Data_To, AV33ddo_ContratoArquivosAnexos_CodigoTitleControlIdToReplace, AV37ddo_Contrato_CodigoTitleControlIdToReplace, AV41ddo_Contrato_NumeroTitleControlIdToReplace, AV45ddo_Contratada_CodigoTitleControlIdToReplace, AV49ddo_Contratada_PessoaCodTitleControlIdToReplace, AV53ddo_Contratada_PessoaNomTitleControlIdToReplace, AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV61ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace, AV67ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace, AV78Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoArquivosAnexos_Descricao1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoArquivosAnexos_Descricao2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ContratoArquivosAnexos_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFContratoArquivosAnexos_Codigo, AV32TFContratoArquivosAnexos_Codigo_To, AV35TFContrato_Codigo, AV36TFContrato_Codigo_To, AV39TFContrato_Numero, AV40TFContrato_Numero_Sel, AV43TFContratada_Codigo, AV44TFContratada_Codigo_To, AV47TFContratada_PessoaCod, AV48TFContratada_PessoaCod_To, AV51TFContratada_PessoaNom, AV52TFContratada_PessoaNom_Sel, AV55TFContratada_PessoaCNPJ, AV56TFContratada_PessoaCNPJ_Sel, AV59TFContratoArquivosAnexos_Descricao, AV60TFContratoArquivosAnexos_Descricao_Sel, AV63TFContratoArquivosAnexos_Data, AV64TFContratoArquivosAnexos_Data_To, AV33ddo_ContratoArquivosAnexos_CodigoTitleControlIdToReplace, AV37ddo_Contrato_CodigoTitleControlIdToReplace, AV41ddo_Contrato_NumeroTitleControlIdToReplace, AV45ddo_Contratada_CodigoTitleControlIdToReplace, AV49ddo_Contratada_PessoaCodTitleControlIdToReplace, AV53ddo_Contratada_PessoaNomTitleControlIdToReplace, AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV61ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace, AV67ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace, AV78Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoArquivosAnexos_Descricao1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoArquivosAnexos_Descricao2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ContratoArquivosAnexos_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFContratoArquivosAnexos_Codigo, AV32TFContratoArquivosAnexos_Codigo_To, AV35TFContrato_Codigo, AV36TFContrato_Codigo_To, AV39TFContrato_Numero, AV40TFContrato_Numero_Sel, AV43TFContratada_Codigo, AV44TFContratada_Codigo_To, AV47TFContratada_PessoaCod, AV48TFContratada_PessoaCod_To, AV51TFContratada_PessoaNom, AV52TFContratada_PessoaNom_Sel, AV55TFContratada_PessoaCNPJ, AV56TFContratada_PessoaCNPJ_Sel, AV59TFContratoArquivosAnexos_Descricao, AV60TFContratoArquivosAnexos_Descricao_Sel, AV63TFContratoArquivosAnexos_Data, AV64TFContratoArquivosAnexos_Data_To, AV33ddo_ContratoArquivosAnexos_CodigoTitleControlIdToReplace, AV37ddo_Contrato_CodigoTitleControlIdToReplace, AV41ddo_Contrato_NumeroTitleControlIdToReplace, AV45ddo_Contratada_CodigoTitleControlIdToReplace, AV49ddo_Contratada_PessoaCodTitleControlIdToReplace, AV53ddo_Contratada_PessoaNomTitleControlIdToReplace, AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV61ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace, AV67ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace, AV78Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoArquivosAnexos_Descricao1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoArquivosAnexos_Descricao2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ContratoArquivosAnexos_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFContratoArquivosAnexos_Codigo, AV32TFContratoArquivosAnexos_Codigo_To, AV35TFContrato_Codigo, AV36TFContrato_Codigo_To, AV39TFContrato_Numero, AV40TFContrato_Numero_Sel, AV43TFContratada_Codigo, AV44TFContratada_Codigo_To, AV47TFContratada_PessoaCod, AV48TFContratada_PessoaCod_To, AV51TFContratada_PessoaNom, AV52TFContratada_PessoaNom_Sel, AV55TFContratada_PessoaCNPJ, AV56TFContratada_PessoaCNPJ_Sel, AV59TFContratoArquivosAnexos_Descricao, AV60TFContratoArquivosAnexos_Descricao_Sel, AV63TFContratoArquivosAnexos_Data, AV64TFContratoArquivosAnexos_Data_To, AV33ddo_ContratoArquivosAnexos_CodigoTitleControlIdToReplace, AV37ddo_Contrato_CodigoTitleControlIdToReplace, AV41ddo_Contrato_NumeroTitleControlIdToReplace, AV45ddo_Contratada_CodigoTitleControlIdToReplace, AV49ddo_Contratada_PessoaCodTitleControlIdToReplace, AV53ddo_Contratada_PessoaNomTitleControlIdToReplace, AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV61ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace, AV67ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace, AV78Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoArquivosAnexos_Descricao1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoArquivosAnexos_Descricao2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ContratoArquivosAnexos_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFContratoArquivosAnexos_Codigo, AV32TFContratoArquivosAnexos_Codigo_To, AV35TFContrato_Codigo, AV36TFContrato_Codigo_To, AV39TFContrato_Numero, AV40TFContrato_Numero_Sel, AV43TFContratada_Codigo, AV44TFContratada_Codigo_To, AV47TFContratada_PessoaCod, AV48TFContratada_PessoaCod_To, AV51TFContratada_PessoaNom, AV52TFContratada_PessoaNom_Sel, AV55TFContratada_PessoaCNPJ, AV56TFContratada_PessoaCNPJ_Sel, AV59TFContratoArquivosAnexos_Descricao, AV60TFContratoArquivosAnexos_Descricao_Sel, AV63TFContratoArquivosAnexos_Data, AV64TFContratoArquivosAnexos_Data_To, AV33ddo_ContratoArquivosAnexos_CodigoTitleControlIdToReplace, AV37ddo_Contrato_CodigoTitleControlIdToReplace, AV41ddo_Contrato_NumeroTitleControlIdToReplace, AV45ddo_Contratada_CodigoTitleControlIdToReplace, AV49ddo_Contratada_PessoaCodTitleControlIdToReplace, AV53ddo_Contratada_PessoaNomTitleControlIdToReplace, AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV61ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace, AV67ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace, AV78Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return (int)(0) ;
      }

      protected void STRUP6M0( )
      {
         /* Before Start, stand alone formulas. */
         AV78Pgmname = "PromptContratoArquivosAnexos";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E316M2 */
         E316M2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV68DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOARQUIVOSANEXOS_CODIGOTITLEFILTERDATA"), AV30ContratoArquivosAnexos_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATO_CODIGOTITLEFILTERDATA"), AV34Contrato_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATO_NUMEROTITLEFILTERDATA"), AV38Contrato_NumeroTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATADA_CODIGOTITLEFILTERDATA"), AV42Contratada_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATADA_PESSOACODTITLEFILTERDATA"), AV46Contratada_PessoaCodTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATADA_PESSOANOMTITLEFILTERDATA"), AV50Contratada_PessoaNomTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATADA_PESSOACNPJTITLEFILTERDATA"), AV54Contratada_PessoaCNPJTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOARQUIVOSANEXOS_DESCRICAOTITLEFILTERDATA"), AV58ContratoArquivosAnexos_DescricaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOARQUIVOSANEXOS_DATATITLEFILTERDATA"), AV62ContratoArquivosAnexos_DataTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            AV17ContratoArquivosAnexos_Descricao1 = cgiGet( edtavContratoarquivosanexos_descricao1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoArquivosAnexos_Descricao1", AV17ContratoArquivosAnexos_Descricao1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            AV21ContratoArquivosAnexos_Descricao2 = cgiGet( edtavContratoarquivosanexos_descricao2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoArquivosAnexos_Descricao2", AV21ContratoArquivosAnexos_Descricao2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            AV25ContratoArquivosAnexos_Descricao3 = cgiGet( edtavContratoarquivosanexos_descricao3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoArquivosAnexos_Descricao3", AV25ContratoArquivosAnexos_Descricao3);
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoarquivosanexos_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoarquivosanexos_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOARQUIVOSANEXOS_CODIGO");
               GX_FocusControl = edtavTfcontratoarquivosanexos_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV31TFContratoArquivosAnexos_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFContratoArquivosAnexos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFContratoArquivosAnexos_Codigo), 6, 0)));
            }
            else
            {
               AV31TFContratoArquivosAnexos_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratoarquivosanexos_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFContratoArquivosAnexos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFContratoArquivosAnexos_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoarquivosanexos_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoarquivosanexos_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOARQUIVOSANEXOS_CODIGO_TO");
               GX_FocusControl = edtavTfcontratoarquivosanexos_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV32TFContratoArquivosAnexos_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFContratoArquivosAnexos_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFContratoArquivosAnexos_Codigo_To), 6, 0)));
            }
            else
            {
               AV32TFContratoArquivosAnexos_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratoarquivosanexos_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFContratoArquivosAnexos_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFContratoArquivosAnexos_Codigo_To), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontrato_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontrato_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATO_CODIGO");
               GX_FocusControl = edtavTfcontrato_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV35TFContrato_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFContrato_Codigo), 6, 0)));
            }
            else
            {
               AV35TFContrato_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfcontrato_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFContrato_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontrato_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontrato_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATO_CODIGO_TO");
               GX_FocusControl = edtavTfcontrato_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV36TFContrato_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFContrato_Codigo_To), 6, 0)));
            }
            else
            {
               AV36TFContrato_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontrato_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFContrato_Codigo_To), 6, 0)));
            }
            AV39TFContrato_Numero = cgiGet( edtavTfcontrato_numero_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContrato_Numero", AV39TFContrato_Numero);
            AV40TFContrato_Numero_Sel = cgiGet( edtavTfcontrato_numero_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContrato_Numero_Sel", AV40TFContrato_Numero_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratada_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratada_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATADA_CODIGO");
               GX_FocusControl = edtavTfcontratada_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV43TFContratada_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFContratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFContratada_Codigo), 6, 0)));
            }
            else
            {
               AV43TFContratada_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratada_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFContratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFContratada_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratada_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratada_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATADA_CODIGO_TO");
               GX_FocusControl = edtavTfcontratada_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV44TFContratada_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContratada_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFContratada_Codigo_To), 6, 0)));
            }
            else
            {
               AV44TFContratada_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratada_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContratada_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFContratada_Codigo_To), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratada_pessoacod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratada_pessoacod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATADA_PESSOACOD");
               GX_FocusControl = edtavTfcontratada_pessoacod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV47TFContratada_PessoaCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFContratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47TFContratada_PessoaCod), 6, 0)));
            }
            else
            {
               AV47TFContratada_PessoaCod = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratada_pessoacod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFContratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47TFContratada_PessoaCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratada_pessoacod_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratada_pessoacod_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATADA_PESSOACOD_TO");
               GX_FocusControl = edtavTfcontratada_pessoacod_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV48TFContratada_PessoaCod_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFContratada_PessoaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48TFContratada_PessoaCod_To), 6, 0)));
            }
            else
            {
               AV48TFContratada_PessoaCod_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratada_pessoacod_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFContratada_PessoaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48TFContratada_PessoaCod_To), 6, 0)));
            }
            AV51TFContratada_PessoaNom = StringUtil.Upper( cgiGet( edtavTfcontratada_pessoanom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFContratada_PessoaNom", AV51TFContratada_PessoaNom);
            AV52TFContratada_PessoaNom_Sel = StringUtil.Upper( cgiGet( edtavTfcontratada_pessoanom_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFContratada_PessoaNom_Sel", AV52TFContratada_PessoaNom_Sel);
            AV55TFContratada_PessoaCNPJ = cgiGet( edtavTfcontratada_pessoacnpj_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFContratada_PessoaCNPJ", AV55TFContratada_PessoaCNPJ);
            AV56TFContratada_PessoaCNPJ_Sel = cgiGet( edtavTfcontratada_pessoacnpj_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFContratada_PessoaCNPJ_Sel", AV56TFContratada_PessoaCNPJ_Sel);
            AV59TFContratoArquivosAnexos_Descricao = cgiGet( edtavTfcontratoarquivosanexos_descricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContratoArquivosAnexos_Descricao", AV59TFContratoArquivosAnexos_Descricao);
            AV60TFContratoArquivosAnexos_Descricao_Sel = cgiGet( edtavTfcontratoarquivosanexos_descricao_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContratoArquivosAnexos_Descricao_Sel", AV60TFContratoArquivosAnexos_Descricao_Sel);
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratoarquivosanexos_data_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFContrato Arquivos Anexos_Data"}), 1, "vTFCONTRATOARQUIVOSANEXOS_DATA");
               GX_FocusControl = edtavTfcontratoarquivosanexos_data_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV63TFContratoArquivosAnexos_Data = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFContratoArquivosAnexos_Data", context.localUtil.TToC( AV63TFContratoArquivosAnexos_Data, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV63TFContratoArquivosAnexos_Data = context.localUtil.CToT( cgiGet( edtavTfcontratoarquivosanexos_data_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFContratoArquivosAnexos_Data", context.localUtil.TToC( AV63TFContratoArquivosAnexos_Data, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratoarquivosanexos_data_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFContrato Arquivos Anexos_Data_To"}), 1, "vTFCONTRATOARQUIVOSANEXOS_DATA_TO");
               GX_FocusControl = edtavTfcontratoarquivosanexos_data_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV64TFContratoArquivosAnexos_Data_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFContratoArquivosAnexos_Data_To", context.localUtil.TToC( AV64TFContratoArquivosAnexos_Data_To, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV64TFContratoArquivosAnexos_Data_To = context.localUtil.CToT( cgiGet( edtavTfcontratoarquivosanexos_data_to_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFContratoArquivosAnexos_Data_To", context.localUtil.TToC( AV64TFContratoArquivosAnexos_Data_To, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratoarquivosanexos_dataauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contrato Arquivos Anexos_Data Aux Date"}), 1, "vDDO_CONTRATOARQUIVOSANEXOS_DATAAUXDATE");
               GX_FocusControl = edtavDdo_contratoarquivosanexos_dataauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV65DDO_ContratoArquivosAnexos_DataAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65DDO_ContratoArquivosAnexos_DataAuxDate", context.localUtil.Format(AV65DDO_ContratoArquivosAnexos_DataAuxDate, "99/99/99"));
            }
            else
            {
               AV65DDO_ContratoArquivosAnexos_DataAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratoarquivosanexos_dataauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65DDO_ContratoArquivosAnexos_DataAuxDate", context.localUtil.Format(AV65DDO_ContratoArquivosAnexos_DataAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratoarquivosanexos_dataauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contrato Arquivos Anexos_Data Aux Date To"}), 1, "vDDO_CONTRATOARQUIVOSANEXOS_DATAAUXDATETO");
               GX_FocusControl = edtavDdo_contratoarquivosanexos_dataauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV66DDO_ContratoArquivosAnexos_DataAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66DDO_ContratoArquivosAnexos_DataAuxDateTo", context.localUtil.Format(AV66DDO_ContratoArquivosAnexos_DataAuxDateTo, "99/99/99"));
            }
            else
            {
               AV66DDO_ContratoArquivosAnexos_DataAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratoarquivosanexos_dataauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66DDO_ContratoArquivosAnexos_DataAuxDateTo", context.localUtil.Format(AV66DDO_ContratoArquivosAnexos_DataAuxDateTo, "99/99/99"));
            }
            AV33ddo_ContratoArquivosAnexos_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_contratoarquivosanexos_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ddo_ContratoArquivosAnexos_CodigoTitleControlIdToReplace", AV33ddo_ContratoArquivosAnexos_CodigoTitleControlIdToReplace);
            AV37ddo_Contrato_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_contrato_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_Contrato_CodigoTitleControlIdToReplace", AV37ddo_Contrato_CodigoTitleControlIdToReplace);
            AV41ddo_Contrato_NumeroTitleControlIdToReplace = cgiGet( edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_Contrato_NumeroTitleControlIdToReplace", AV41ddo_Contrato_NumeroTitleControlIdToReplace);
            AV45ddo_Contratada_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_contratada_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_Contratada_CodigoTitleControlIdToReplace", AV45ddo_Contratada_CodigoTitleControlIdToReplace);
            AV49ddo_Contratada_PessoaCodTitleControlIdToReplace = cgiGet( edtavDdo_contratada_pessoacodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_Contratada_PessoaCodTitleControlIdToReplace", AV49ddo_Contratada_PessoaCodTitleControlIdToReplace);
            AV53ddo_Contratada_PessoaNomTitleControlIdToReplace = cgiGet( edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_Contratada_PessoaNomTitleControlIdToReplace", AV53ddo_Contratada_PessoaNomTitleControlIdToReplace);
            AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace = cgiGet( edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace", AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace);
            AV61ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace = cgiGet( edtavDdo_contratoarquivosanexos_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace", AV61ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace);
            AV67ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace = cgiGet( edtavDdo_contratoarquivosanexos_datatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace", AV67ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_80 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_80"), ",", "."));
            AV70GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV71GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_contratoarquivosanexos_codigo_Caption = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_CODIGO_Caption");
            Ddo_contratoarquivosanexos_codigo_Tooltip = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_CODIGO_Tooltip");
            Ddo_contratoarquivosanexos_codigo_Cls = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_CODIGO_Cls");
            Ddo_contratoarquivosanexos_codigo_Filteredtext_set = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_CODIGO_Filteredtext_set");
            Ddo_contratoarquivosanexos_codigo_Filteredtextto_set = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_CODIGO_Filteredtextto_set");
            Ddo_contratoarquivosanexos_codigo_Dropdownoptionstype = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_CODIGO_Dropdownoptionstype");
            Ddo_contratoarquivosanexos_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_CODIGO_Titlecontrolidtoreplace");
            Ddo_contratoarquivosanexos_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_CODIGO_Includesortasc"));
            Ddo_contratoarquivosanexos_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_CODIGO_Includesortdsc"));
            Ddo_contratoarquivosanexos_codigo_Sortedstatus = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_CODIGO_Sortedstatus");
            Ddo_contratoarquivosanexos_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_CODIGO_Includefilter"));
            Ddo_contratoarquivosanexos_codigo_Filtertype = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_CODIGO_Filtertype");
            Ddo_contratoarquivosanexos_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_CODIGO_Filterisrange"));
            Ddo_contratoarquivosanexos_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_CODIGO_Includedatalist"));
            Ddo_contratoarquivosanexos_codigo_Sortasc = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_CODIGO_Sortasc");
            Ddo_contratoarquivosanexos_codigo_Sortdsc = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_CODIGO_Sortdsc");
            Ddo_contratoarquivosanexos_codigo_Cleanfilter = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_CODIGO_Cleanfilter");
            Ddo_contratoarquivosanexos_codigo_Rangefilterfrom = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_CODIGO_Rangefilterfrom");
            Ddo_contratoarquivosanexos_codigo_Rangefilterto = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_CODIGO_Rangefilterto");
            Ddo_contratoarquivosanexos_codigo_Searchbuttontext = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_CODIGO_Searchbuttontext");
            Ddo_contrato_codigo_Caption = cgiGet( "DDO_CONTRATO_CODIGO_Caption");
            Ddo_contrato_codigo_Tooltip = cgiGet( "DDO_CONTRATO_CODIGO_Tooltip");
            Ddo_contrato_codigo_Cls = cgiGet( "DDO_CONTRATO_CODIGO_Cls");
            Ddo_contrato_codigo_Filteredtext_set = cgiGet( "DDO_CONTRATO_CODIGO_Filteredtext_set");
            Ddo_contrato_codigo_Filteredtextto_set = cgiGet( "DDO_CONTRATO_CODIGO_Filteredtextto_set");
            Ddo_contrato_codigo_Dropdownoptionstype = cgiGet( "DDO_CONTRATO_CODIGO_Dropdownoptionstype");
            Ddo_contrato_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATO_CODIGO_Titlecontrolidtoreplace");
            Ddo_contrato_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_CODIGO_Includesortasc"));
            Ddo_contrato_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_CODIGO_Includesortdsc"));
            Ddo_contrato_codigo_Sortedstatus = cgiGet( "DDO_CONTRATO_CODIGO_Sortedstatus");
            Ddo_contrato_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_CODIGO_Includefilter"));
            Ddo_contrato_codigo_Filtertype = cgiGet( "DDO_CONTRATO_CODIGO_Filtertype");
            Ddo_contrato_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_CODIGO_Filterisrange"));
            Ddo_contrato_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_CODIGO_Includedatalist"));
            Ddo_contrato_codigo_Sortasc = cgiGet( "DDO_CONTRATO_CODIGO_Sortasc");
            Ddo_contrato_codigo_Sortdsc = cgiGet( "DDO_CONTRATO_CODIGO_Sortdsc");
            Ddo_contrato_codigo_Cleanfilter = cgiGet( "DDO_CONTRATO_CODIGO_Cleanfilter");
            Ddo_contrato_codigo_Rangefilterfrom = cgiGet( "DDO_CONTRATO_CODIGO_Rangefilterfrom");
            Ddo_contrato_codigo_Rangefilterto = cgiGet( "DDO_CONTRATO_CODIGO_Rangefilterto");
            Ddo_contrato_codigo_Searchbuttontext = cgiGet( "DDO_CONTRATO_CODIGO_Searchbuttontext");
            Ddo_contrato_numero_Caption = cgiGet( "DDO_CONTRATO_NUMERO_Caption");
            Ddo_contrato_numero_Tooltip = cgiGet( "DDO_CONTRATO_NUMERO_Tooltip");
            Ddo_contrato_numero_Cls = cgiGet( "DDO_CONTRATO_NUMERO_Cls");
            Ddo_contrato_numero_Filteredtext_set = cgiGet( "DDO_CONTRATO_NUMERO_Filteredtext_set");
            Ddo_contrato_numero_Selectedvalue_set = cgiGet( "DDO_CONTRATO_NUMERO_Selectedvalue_set");
            Ddo_contrato_numero_Dropdownoptionstype = cgiGet( "DDO_CONTRATO_NUMERO_Dropdownoptionstype");
            Ddo_contrato_numero_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATO_NUMERO_Titlecontrolidtoreplace");
            Ddo_contrato_numero_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includesortasc"));
            Ddo_contrato_numero_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includesortdsc"));
            Ddo_contrato_numero_Sortedstatus = cgiGet( "DDO_CONTRATO_NUMERO_Sortedstatus");
            Ddo_contrato_numero_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includefilter"));
            Ddo_contrato_numero_Filtertype = cgiGet( "DDO_CONTRATO_NUMERO_Filtertype");
            Ddo_contrato_numero_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Filterisrange"));
            Ddo_contrato_numero_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includedatalist"));
            Ddo_contrato_numero_Datalisttype = cgiGet( "DDO_CONTRATO_NUMERO_Datalisttype");
            Ddo_contrato_numero_Datalistproc = cgiGet( "DDO_CONTRATO_NUMERO_Datalistproc");
            Ddo_contrato_numero_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATO_NUMERO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contrato_numero_Sortasc = cgiGet( "DDO_CONTRATO_NUMERO_Sortasc");
            Ddo_contrato_numero_Sortdsc = cgiGet( "DDO_CONTRATO_NUMERO_Sortdsc");
            Ddo_contrato_numero_Loadingdata = cgiGet( "DDO_CONTRATO_NUMERO_Loadingdata");
            Ddo_contrato_numero_Cleanfilter = cgiGet( "DDO_CONTRATO_NUMERO_Cleanfilter");
            Ddo_contrato_numero_Noresultsfound = cgiGet( "DDO_CONTRATO_NUMERO_Noresultsfound");
            Ddo_contrato_numero_Searchbuttontext = cgiGet( "DDO_CONTRATO_NUMERO_Searchbuttontext");
            Ddo_contratada_codigo_Caption = cgiGet( "DDO_CONTRATADA_CODIGO_Caption");
            Ddo_contratada_codigo_Tooltip = cgiGet( "DDO_CONTRATADA_CODIGO_Tooltip");
            Ddo_contratada_codigo_Cls = cgiGet( "DDO_CONTRATADA_CODIGO_Cls");
            Ddo_contratada_codigo_Filteredtext_set = cgiGet( "DDO_CONTRATADA_CODIGO_Filteredtext_set");
            Ddo_contratada_codigo_Filteredtextto_set = cgiGet( "DDO_CONTRATADA_CODIGO_Filteredtextto_set");
            Ddo_contratada_codigo_Dropdownoptionstype = cgiGet( "DDO_CONTRATADA_CODIGO_Dropdownoptionstype");
            Ddo_contratada_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATADA_CODIGO_Titlecontrolidtoreplace");
            Ddo_contratada_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_CODIGO_Includesortasc"));
            Ddo_contratada_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_CODIGO_Includesortdsc"));
            Ddo_contratada_codigo_Sortedstatus = cgiGet( "DDO_CONTRATADA_CODIGO_Sortedstatus");
            Ddo_contratada_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_CODIGO_Includefilter"));
            Ddo_contratada_codigo_Filtertype = cgiGet( "DDO_CONTRATADA_CODIGO_Filtertype");
            Ddo_contratada_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_CODIGO_Filterisrange"));
            Ddo_contratada_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_CODIGO_Includedatalist"));
            Ddo_contratada_codigo_Sortasc = cgiGet( "DDO_CONTRATADA_CODIGO_Sortasc");
            Ddo_contratada_codigo_Sortdsc = cgiGet( "DDO_CONTRATADA_CODIGO_Sortdsc");
            Ddo_contratada_codigo_Cleanfilter = cgiGet( "DDO_CONTRATADA_CODIGO_Cleanfilter");
            Ddo_contratada_codigo_Rangefilterfrom = cgiGet( "DDO_CONTRATADA_CODIGO_Rangefilterfrom");
            Ddo_contratada_codigo_Rangefilterto = cgiGet( "DDO_CONTRATADA_CODIGO_Rangefilterto");
            Ddo_contratada_codigo_Searchbuttontext = cgiGet( "DDO_CONTRATADA_CODIGO_Searchbuttontext");
            Ddo_contratada_pessoacod_Caption = cgiGet( "DDO_CONTRATADA_PESSOACOD_Caption");
            Ddo_contratada_pessoacod_Tooltip = cgiGet( "DDO_CONTRATADA_PESSOACOD_Tooltip");
            Ddo_contratada_pessoacod_Cls = cgiGet( "DDO_CONTRATADA_PESSOACOD_Cls");
            Ddo_contratada_pessoacod_Filteredtext_set = cgiGet( "DDO_CONTRATADA_PESSOACOD_Filteredtext_set");
            Ddo_contratada_pessoacod_Filteredtextto_set = cgiGet( "DDO_CONTRATADA_PESSOACOD_Filteredtextto_set");
            Ddo_contratada_pessoacod_Dropdownoptionstype = cgiGet( "DDO_CONTRATADA_PESSOACOD_Dropdownoptionstype");
            Ddo_contratada_pessoacod_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATADA_PESSOACOD_Titlecontrolidtoreplace");
            Ddo_contratada_pessoacod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOACOD_Includesortasc"));
            Ddo_contratada_pessoacod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOACOD_Includesortdsc"));
            Ddo_contratada_pessoacod_Sortedstatus = cgiGet( "DDO_CONTRATADA_PESSOACOD_Sortedstatus");
            Ddo_contratada_pessoacod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOACOD_Includefilter"));
            Ddo_contratada_pessoacod_Filtertype = cgiGet( "DDO_CONTRATADA_PESSOACOD_Filtertype");
            Ddo_contratada_pessoacod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOACOD_Filterisrange"));
            Ddo_contratada_pessoacod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOACOD_Includedatalist"));
            Ddo_contratada_pessoacod_Sortasc = cgiGet( "DDO_CONTRATADA_PESSOACOD_Sortasc");
            Ddo_contratada_pessoacod_Sortdsc = cgiGet( "DDO_CONTRATADA_PESSOACOD_Sortdsc");
            Ddo_contratada_pessoacod_Cleanfilter = cgiGet( "DDO_CONTRATADA_PESSOACOD_Cleanfilter");
            Ddo_contratada_pessoacod_Rangefilterfrom = cgiGet( "DDO_CONTRATADA_PESSOACOD_Rangefilterfrom");
            Ddo_contratada_pessoacod_Rangefilterto = cgiGet( "DDO_CONTRATADA_PESSOACOD_Rangefilterto");
            Ddo_contratada_pessoacod_Searchbuttontext = cgiGet( "DDO_CONTRATADA_PESSOACOD_Searchbuttontext");
            Ddo_contratada_pessoanom_Caption = cgiGet( "DDO_CONTRATADA_PESSOANOM_Caption");
            Ddo_contratada_pessoanom_Tooltip = cgiGet( "DDO_CONTRATADA_PESSOANOM_Tooltip");
            Ddo_contratada_pessoanom_Cls = cgiGet( "DDO_CONTRATADA_PESSOANOM_Cls");
            Ddo_contratada_pessoanom_Filteredtext_set = cgiGet( "DDO_CONTRATADA_PESSOANOM_Filteredtext_set");
            Ddo_contratada_pessoanom_Selectedvalue_set = cgiGet( "DDO_CONTRATADA_PESSOANOM_Selectedvalue_set");
            Ddo_contratada_pessoanom_Dropdownoptionstype = cgiGet( "DDO_CONTRATADA_PESSOANOM_Dropdownoptionstype");
            Ddo_contratada_pessoanom_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATADA_PESSOANOM_Titlecontrolidtoreplace");
            Ddo_contratada_pessoanom_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOANOM_Includesortasc"));
            Ddo_contratada_pessoanom_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOANOM_Includesortdsc"));
            Ddo_contratada_pessoanom_Sortedstatus = cgiGet( "DDO_CONTRATADA_PESSOANOM_Sortedstatus");
            Ddo_contratada_pessoanom_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOANOM_Includefilter"));
            Ddo_contratada_pessoanom_Filtertype = cgiGet( "DDO_CONTRATADA_PESSOANOM_Filtertype");
            Ddo_contratada_pessoanom_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOANOM_Filterisrange"));
            Ddo_contratada_pessoanom_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOANOM_Includedatalist"));
            Ddo_contratada_pessoanom_Datalisttype = cgiGet( "DDO_CONTRATADA_PESSOANOM_Datalisttype");
            Ddo_contratada_pessoanom_Datalistproc = cgiGet( "DDO_CONTRATADA_PESSOANOM_Datalistproc");
            Ddo_contratada_pessoanom_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATADA_PESSOANOM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratada_pessoanom_Sortasc = cgiGet( "DDO_CONTRATADA_PESSOANOM_Sortasc");
            Ddo_contratada_pessoanom_Sortdsc = cgiGet( "DDO_CONTRATADA_PESSOANOM_Sortdsc");
            Ddo_contratada_pessoanom_Loadingdata = cgiGet( "DDO_CONTRATADA_PESSOANOM_Loadingdata");
            Ddo_contratada_pessoanom_Cleanfilter = cgiGet( "DDO_CONTRATADA_PESSOANOM_Cleanfilter");
            Ddo_contratada_pessoanom_Noresultsfound = cgiGet( "DDO_CONTRATADA_PESSOANOM_Noresultsfound");
            Ddo_contratada_pessoanom_Searchbuttontext = cgiGet( "DDO_CONTRATADA_PESSOANOM_Searchbuttontext");
            Ddo_contratada_pessoacnpj_Caption = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Caption");
            Ddo_contratada_pessoacnpj_Tooltip = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Tooltip");
            Ddo_contratada_pessoacnpj_Cls = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Cls");
            Ddo_contratada_pessoacnpj_Filteredtext_set = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Filteredtext_set");
            Ddo_contratada_pessoacnpj_Selectedvalue_set = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Selectedvalue_set");
            Ddo_contratada_pessoacnpj_Dropdownoptionstype = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Dropdownoptionstype");
            Ddo_contratada_pessoacnpj_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Titlecontrolidtoreplace");
            Ddo_contratada_pessoacnpj_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Includesortasc"));
            Ddo_contratada_pessoacnpj_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Includesortdsc"));
            Ddo_contratada_pessoacnpj_Sortedstatus = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Sortedstatus");
            Ddo_contratada_pessoacnpj_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Includefilter"));
            Ddo_contratada_pessoacnpj_Filtertype = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Filtertype");
            Ddo_contratada_pessoacnpj_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Filterisrange"));
            Ddo_contratada_pessoacnpj_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Includedatalist"));
            Ddo_contratada_pessoacnpj_Datalisttype = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Datalisttype");
            Ddo_contratada_pessoacnpj_Datalistproc = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Datalistproc");
            Ddo_contratada_pessoacnpj_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratada_pessoacnpj_Sortasc = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Sortasc");
            Ddo_contratada_pessoacnpj_Sortdsc = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Sortdsc");
            Ddo_contratada_pessoacnpj_Loadingdata = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Loadingdata");
            Ddo_contratada_pessoacnpj_Cleanfilter = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Cleanfilter");
            Ddo_contratada_pessoacnpj_Noresultsfound = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Noresultsfound");
            Ddo_contratada_pessoacnpj_Searchbuttontext = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Searchbuttontext");
            Ddo_contratoarquivosanexos_descricao_Caption = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Caption");
            Ddo_contratoarquivosanexos_descricao_Tooltip = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Tooltip");
            Ddo_contratoarquivosanexos_descricao_Cls = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Cls");
            Ddo_contratoarquivosanexos_descricao_Filteredtext_set = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Filteredtext_set");
            Ddo_contratoarquivosanexos_descricao_Selectedvalue_set = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Selectedvalue_set");
            Ddo_contratoarquivosanexos_descricao_Dropdownoptionstype = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Dropdownoptionstype");
            Ddo_contratoarquivosanexos_descricao_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_contratoarquivosanexos_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Includesortasc"));
            Ddo_contratoarquivosanexos_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Includesortdsc"));
            Ddo_contratoarquivosanexos_descricao_Sortedstatus = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Sortedstatus");
            Ddo_contratoarquivosanexos_descricao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Includefilter"));
            Ddo_contratoarquivosanexos_descricao_Filtertype = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Filtertype");
            Ddo_contratoarquivosanexos_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Filterisrange"));
            Ddo_contratoarquivosanexos_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Includedatalist"));
            Ddo_contratoarquivosanexos_descricao_Datalisttype = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Datalisttype");
            Ddo_contratoarquivosanexos_descricao_Datalistproc = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Datalistproc");
            Ddo_contratoarquivosanexos_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratoarquivosanexos_descricao_Sortasc = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Sortasc");
            Ddo_contratoarquivosanexos_descricao_Sortdsc = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Sortdsc");
            Ddo_contratoarquivosanexos_descricao_Loadingdata = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Loadingdata");
            Ddo_contratoarquivosanexos_descricao_Cleanfilter = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Cleanfilter");
            Ddo_contratoarquivosanexos_descricao_Noresultsfound = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Noresultsfound");
            Ddo_contratoarquivosanexos_descricao_Searchbuttontext = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Searchbuttontext");
            Ddo_contratoarquivosanexos_data_Caption = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DATA_Caption");
            Ddo_contratoarquivosanexos_data_Tooltip = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DATA_Tooltip");
            Ddo_contratoarquivosanexos_data_Cls = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DATA_Cls");
            Ddo_contratoarquivosanexos_data_Filteredtext_set = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DATA_Filteredtext_set");
            Ddo_contratoarquivosanexos_data_Filteredtextto_set = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DATA_Filteredtextto_set");
            Ddo_contratoarquivosanexos_data_Dropdownoptionstype = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DATA_Dropdownoptionstype");
            Ddo_contratoarquivosanexos_data_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DATA_Titlecontrolidtoreplace");
            Ddo_contratoarquivosanexos_data_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DATA_Includesortasc"));
            Ddo_contratoarquivosanexos_data_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DATA_Includesortdsc"));
            Ddo_contratoarquivosanexos_data_Sortedstatus = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DATA_Sortedstatus");
            Ddo_contratoarquivosanexos_data_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DATA_Includefilter"));
            Ddo_contratoarquivosanexos_data_Filtertype = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DATA_Filtertype");
            Ddo_contratoarquivosanexos_data_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DATA_Filterisrange"));
            Ddo_contratoarquivosanexos_data_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DATA_Includedatalist"));
            Ddo_contratoarquivosanexos_data_Sortasc = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DATA_Sortasc");
            Ddo_contratoarquivosanexos_data_Sortdsc = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DATA_Sortdsc");
            Ddo_contratoarquivosanexos_data_Cleanfilter = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DATA_Cleanfilter");
            Ddo_contratoarquivosanexos_data_Rangefilterfrom = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DATA_Rangefilterfrom");
            Ddo_contratoarquivosanexos_data_Rangefilterto = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DATA_Rangefilterto");
            Ddo_contratoarquivosanexos_data_Searchbuttontext = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DATA_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contratoarquivosanexos_codigo_Activeeventkey = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_CODIGO_Activeeventkey");
            Ddo_contratoarquivosanexos_codigo_Filteredtext_get = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_CODIGO_Filteredtext_get");
            Ddo_contratoarquivosanexos_codigo_Filteredtextto_get = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_CODIGO_Filteredtextto_get");
            Ddo_contrato_codigo_Activeeventkey = cgiGet( "DDO_CONTRATO_CODIGO_Activeeventkey");
            Ddo_contrato_codigo_Filteredtext_get = cgiGet( "DDO_CONTRATO_CODIGO_Filteredtext_get");
            Ddo_contrato_codigo_Filteredtextto_get = cgiGet( "DDO_CONTRATO_CODIGO_Filteredtextto_get");
            Ddo_contrato_numero_Activeeventkey = cgiGet( "DDO_CONTRATO_NUMERO_Activeeventkey");
            Ddo_contrato_numero_Filteredtext_get = cgiGet( "DDO_CONTRATO_NUMERO_Filteredtext_get");
            Ddo_contrato_numero_Selectedvalue_get = cgiGet( "DDO_CONTRATO_NUMERO_Selectedvalue_get");
            Ddo_contratada_codigo_Activeeventkey = cgiGet( "DDO_CONTRATADA_CODIGO_Activeeventkey");
            Ddo_contratada_codigo_Filteredtext_get = cgiGet( "DDO_CONTRATADA_CODIGO_Filteredtext_get");
            Ddo_contratada_codigo_Filteredtextto_get = cgiGet( "DDO_CONTRATADA_CODIGO_Filteredtextto_get");
            Ddo_contratada_pessoacod_Activeeventkey = cgiGet( "DDO_CONTRATADA_PESSOACOD_Activeeventkey");
            Ddo_contratada_pessoacod_Filteredtext_get = cgiGet( "DDO_CONTRATADA_PESSOACOD_Filteredtext_get");
            Ddo_contratada_pessoacod_Filteredtextto_get = cgiGet( "DDO_CONTRATADA_PESSOACOD_Filteredtextto_get");
            Ddo_contratada_pessoanom_Activeeventkey = cgiGet( "DDO_CONTRATADA_PESSOANOM_Activeeventkey");
            Ddo_contratada_pessoanom_Filteredtext_get = cgiGet( "DDO_CONTRATADA_PESSOANOM_Filteredtext_get");
            Ddo_contratada_pessoanom_Selectedvalue_get = cgiGet( "DDO_CONTRATADA_PESSOANOM_Selectedvalue_get");
            Ddo_contratada_pessoacnpj_Activeeventkey = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Activeeventkey");
            Ddo_contratada_pessoacnpj_Filteredtext_get = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Filteredtext_get");
            Ddo_contratada_pessoacnpj_Selectedvalue_get = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Selectedvalue_get");
            Ddo_contratoarquivosanexos_descricao_Activeeventkey = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Activeeventkey");
            Ddo_contratoarquivosanexos_descricao_Filteredtext_get = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Filteredtext_get");
            Ddo_contratoarquivosanexos_descricao_Selectedvalue_get = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO_Selectedvalue_get");
            Ddo_contratoarquivosanexos_data_Activeeventkey = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DATA_Activeeventkey");
            Ddo_contratoarquivosanexos_data_Filteredtext_get = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DATA_Filteredtext_get");
            Ddo_contratoarquivosanexos_data_Filteredtextto_get = cgiGet( "DDO_CONTRATOARQUIVOSANEXOS_DATA_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATOARQUIVOSANEXOS_DESCRICAO1"), AV17ContratoArquivosAnexos_Descricao1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATOARQUIVOSANEXOS_DESCRICAO2"), AV21ContratoArquivosAnexos_Descricao2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATOARQUIVOSANEXOS_DESCRICAO3"), AV25ContratoArquivosAnexos_Descricao3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOARQUIVOSANEXOS_CODIGO"), ",", ".") != Convert.ToDecimal( AV31TFContratoArquivosAnexos_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOARQUIVOSANEXOS_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV32TFContratoArquivosAnexos_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATO_CODIGO"), ",", ".") != Convert.ToDecimal( AV35TFContrato_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV36TFContrato_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO"), AV39TFContrato_Numero) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO_SEL"), AV40TFContrato_Numero_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATADA_CODIGO"), ",", ".") != Convert.ToDecimal( AV43TFContratada_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATADA_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV44TFContratada_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATADA_PESSOACOD"), ",", ".") != Convert.ToDecimal( AV47TFContratada_PessoaCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATADA_PESSOACOD_TO"), ",", ".") != Convert.ToDecimal( AV48TFContratada_PessoaCod_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOANOM"), AV51TFContratada_PessoaNom) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOANOM_SEL"), AV52TFContratada_PessoaNom_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOACNPJ"), AV55TFContratada_PessoaCNPJ) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOACNPJ_SEL"), AV56TFContratada_PessoaCNPJ_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOARQUIVOSANEXOS_DESCRICAO"), AV59TFContratoArquivosAnexos_Descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOARQUIVOSANEXOS_DESCRICAO_SEL"), AV60TFContratoArquivosAnexos_Descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOARQUIVOSANEXOS_DATA"), 0) != AV63TFContratoArquivosAnexos_Data )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOARQUIVOSANEXOS_DATA_TO"), 0) != AV64TFContratoArquivosAnexos_Data_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E316M2 */
         E316M2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E316M2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "CONTRATOARQUIVOSANEXOS_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "CONTRATOARQUIVOSANEXOS_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector3 = "CONTRATOARQUIVOSANEXOS_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfcontratoarquivosanexos_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoarquivosanexos_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoarquivosanexos_codigo_Visible), 5, 0)));
         edtavTfcontratoarquivosanexos_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoarquivosanexos_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoarquivosanexos_codigo_to_Visible), 5, 0)));
         edtavTfcontrato_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontrato_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontrato_codigo_Visible), 5, 0)));
         edtavTfcontrato_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontrato_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontrato_codigo_to_Visible), 5, 0)));
         edtavTfcontrato_numero_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontrato_numero_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontrato_numero_Visible), 5, 0)));
         edtavTfcontrato_numero_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontrato_numero_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontrato_numero_sel_Visible), 5, 0)));
         edtavTfcontratada_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratada_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratada_codigo_Visible), 5, 0)));
         edtavTfcontratada_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratada_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratada_codigo_to_Visible), 5, 0)));
         edtavTfcontratada_pessoacod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratada_pessoacod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratada_pessoacod_Visible), 5, 0)));
         edtavTfcontratada_pessoacod_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratada_pessoacod_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratada_pessoacod_to_Visible), 5, 0)));
         edtavTfcontratada_pessoanom_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratada_pessoanom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratada_pessoanom_Visible), 5, 0)));
         edtavTfcontratada_pessoanom_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratada_pessoanom_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratada_pessoanom_sel_Visible), 5, 0)));
         edtavTfcontratada_pessoacnpj_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratada_pessoacnpj_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratada_pessoacnpj_Visible), 5, 0)));
         edtavTfcontratada_pessoacnpj_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratada_pessoacnpj_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratada_pessoacnpj_sel_Visible), 5, 0)));
         edtavTfcontratoarquivosanexos_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoarquivosanexos_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoarquivosanexos_descricao_Visible), 5, 0)));
         edtavTfcontratoarquivosanexos_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoarquivosanexos_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoarquivosanexos_descricao_sel_Visible), 5, 0)));
         edtavTfcontratoarquivosanexos_data_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoarquivosanexos_data_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoarquivosanexos_data_Visible), 5, 0)));
         edtavTfcontratoarquivosanexos_data_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoarquivosanexos_data_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoarquivosanexos_data_to_Visible), 5, 0)));
         Ddo_contratoarquivosanexos_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoArquivosAnexos_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_codigo_Internalname, "TitleControlIdToReplace", Ddo_contratoarquivosanexos_codigo_Titlecontrolidtoreplace);
         AV33ddo_ContratoArquivosAnexos_CodigoTitleControlIdToReplace = Ddo_contratoarquivosanexos_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ddo_ContratoArquivosAnexos_CodigoTitleControlIdToReplace", AV33ddo_ContratoArquivosAnexos_CodigoTitleControlIdToReplace);
         edtavDdo_contratoarquivosanexos_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoarquivosanexos_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoarquivosanexos_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contrato_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_Contrato_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_codigo_Internalname, "TitleControlIdToReplace", Ddo_contrato_codigo_Titlecontrolidtoreplace);
         AV37ddo_Contrato_CodigoTitleControlIdToReplace = Ddo_contrato_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_Contrato_CodigoTitleControlIdToReplace", AV37ddo_Contrato_CodigoTitleControlIdToReplace);
         edtavDdo_contrato_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contrato_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contrato_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contrato_numero_Titlecontrolidtoreplace = subGrid_Internalname+"_Contrato_Numero";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "TitleControlIdToReplace", Ddo_contrato_numero_Titlecontrolidtoreplace);
         AV41ddo_Contrato_NumeroTitleControlIdToReplace = Ddo_contrato_numero_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_Contrato_NumeroTitleControlIdToReplace", AV41ddo_Contrato_NumeroTitleControlIdToReplace);
         edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratada_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_Contratada_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_codigo_Internalname, "TitleControlIdToReplace", Ddo_contratada_codigo_Titlecontrolidtoreplace);
         AV45ddo_Contratada_CodigoTitleControlIdToReplace = Ddo_contratada_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_Contratada_CodigoTitleControlIdToReplace", AV45ddo_Contratada_CodigoTitleControlIdToReplace);
         edtavDdo_contratada_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratada_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratada_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratada_pessoacod_Titlecontrolidtoreplace = subGrid_Internalname+"_Contratada_PessoaCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacod_Internalname, "TitleControlIdToReplace", Ddo_contratada_pessoacod_Titlecontrolidtoreplace);
         AV49ddo_Contratada_PessoaCodTitleControlIdToReplace = Ddo_contratada_pessoacod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_Contratada_PessoaCodTitleControlIdToReplace", AV49ddo_Contratada_PessoaCodTitleControlIdToReplace);
         edtavDdo_contratada_pessoacodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratada_pessoacodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratada_pessoacodtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratada_pessoanom_Titlecontrolidtoreplace = subGrid_Internalname+"_Contratada_PessoaNom";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "TitleControlIdToReplace", Ddo_contratada_pessoanom_Titlecontrolidtoreplace);
         AV53ddo_Contratada_PessoaNomTitleControlIdToReplace = Ddo_contratada_pessoanom_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_Contratada_PessoaNomTitleControlIdToReplace", AV53ddo_Contratada_PessoaNomTitleControlIdToReplace);
         edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratada_pessoacnpj_Titlecontrolidtoreplace = subGrid_Internalname+"_Contratada_PessoaCNPJ";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "TitleControlIdToReplace", Ddo_contratada_pessoacnpj_Titlecontrolidtoreplace);
         AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace = Ddo_contratada_pessoacnpj_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace", AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace);
         edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoarquivosanexos_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoArquivosAnexos_Descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_descricao_Internalname, "TitleControlIdToReplace", Ddo_contratoarquivosanexos_descricao_Titlecontrolidtoreplace);
         AV61ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace = Ddo_contratoarquivosanexos_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace", AV61ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace);
         edtavDdo_contratoarquivosanexos_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoarquivosanexos_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoarquivosanexos_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoarquivosanexos_data_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoArquivosAnexos_Data";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_data_Internalname, "TitleControlIdToReplace", Ddo_contratoarquivosanexos_data_Titlecontrolidtoreplace);
         AV67ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace = Ddo_contratoarquivosanexos_data_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace", AV67ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace);
         edtavDdo_contratoarquivosanexos_datatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoarquivosanexos_datatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoarquivosanexos_datatitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Select Contrato Arquivos Anexos";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Descri��o do Arquvo", 0);
         cmbavOrderedby.addItem("2", "C�digo", 0);
         cmbavOrderedby.addItem("3", "Contrato", 0);
         cmbavOrderedby.addItem("4", "N�mero do Contrato", 0);
         cmbavOrderedby.addItem("5", "Contratada", 0);
         cmbavOrderedby.addItem("6", "C�d. da Pessoa", 0);
         cmbavOrderedby.addItem("7", "Nome", 0);
         cmbavOrderedby.addItem("8", " CNPJ da Pessoa", 0);
         cmbavOrderedby.addItem("9", "Data/Hora Upload", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV68DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV68DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E326M2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV30ContratoArquivosAnexos_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV34Contrato_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38Contrato_NumeroTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV42Contratada_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV46Contratada_PessoaCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV50Contratada_PessoaNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV54Contratada_PessoaCNPJTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV58ContratoArquivosAnexos_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV62ContratoArquivosAnexos_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContratoArquivosAnexos_Codigo_Titleformat = 2;
         edtContratoArquivosAnexos_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "C�digo", AV33ddo_ContratoArquivosAnexos_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoArquivosAnexos_Codigo_Internalname, "Title", edtContratoArquivosAnexos_Codigo_Title);
         edtContrato_Codigo_Titleformat = 2;
         edtContrato_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Contrato", AV37ddo_Contrato_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Codigo_Internalname, "Title", edtContrato_Codigo_Title);
         edtContrato_Numero_Titleformat = 2;
         edtContrato_Numero_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "N�mero do Contrato", AV41ddo_Contrato_NumeroTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Numero_Internalname, "Title", edtContrato_Numero_Title);
         edtContratada_Codigo_Titleformat = 2;
         edtContratada_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Contratada", AV45ddo_Contratada_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_Codigo_Internalname, "Title", edtContratada_Codigo_Title);
         edtContratada_PessoaCod_Titleformat = 2;
         edtContratada_PessoaCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "C�d. da Pessoa", AV49ddo_Contratada_PessoaCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_PessoaCod_Internalname, "Title", edtContratada_PessoaCod_Title);
         edtContratada_PessoaNom_Titleformat = 2;
         edtContratada_PessoaNom_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV53ddo_Contratada_PessoaNomTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_PessoaNom_Internalname, "Title", edtContratada_PessoaNom_Title);
         edtContratada_PessoaCNPJ_Titleformat = 2;
         edtContratada_PessoaCNPJ_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", " CNPJ da Pessoa", AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_PessoaCNPJ_Internalname, "Title", edtContratada_PessoaCNPJ_Title);
         edtContratoArquivosAnexos_Descricao_Titleformat = 2;
         edtContratoArquivosAnexos_Descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Descri��o do Arquvo", AV61ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoArquivosAnexos_Descricao_Internalname, "Title", edtContratoArquivosAnexos_Descricao_Title);
         edtContratoArquivosAnexos_Data_Titleformat = 2;
         edtContratoArquivosAnexos_Data_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Data/Hora Upload", AV67ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoArquivosAnexos_Data_Internalname, "Title", edtContratoArquivosAnexos_Data_Title);
         AV70GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV70GridCurrentPage), 10, 0)));
         AV71GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV71GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV30ContratoArquivosAnexos_CodigoTitleFilterData", AV30ContratoArquivosAnexos_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV34Contrato_CodigoTitleFilterData", AV34Contrato_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV38Contrato_NumeroTitleFilterData", AV38Contrato_NumeroTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV42Contratada_CodigoTitleFilterData", AV42Contratada_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV46Contratada_PessoaCodTitleFilterData", AV46Contratada_PessoaCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV50Contratada_PessoaNomTitleFilterData", AV50Contratada_PessoaNomTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV54Contratada_PessoaCNPJTitleFilterData", AV54Contratada_PessoaCNPJTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV58ContratoArquivosAnexos_DescricaoTitleFilterData", AV58ContratoArquivosAnexos_DescricaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV62ContratoArquivosAnexos_DataTitleFilterData", AV62ContratoArquivosAnexos_DataTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E116M2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV69PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV69PageToGo) ;
         }
      }

      protected void E126M2( )
      {
         /* Ddo_contratoarquivosanexos_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoarquivosanexos_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoarquivosanexos_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_codigo_Internalname, "SortedStatus", Ddo_contratoarquivosanexos_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoarquivosanexos_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoarquivosanexos_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_codigo_Internalname, "SortedStatus", Ddo_contratoarquivosanexos_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoarquivosanexos_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV31TFContratoArquivosAnexos_Codigo = (int)(NumberUtil.Val( Ddo_contratoarquivosanexos_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFContratoArquivosAnexos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFContratoArquivosAnexos_Codigo), 6, 0)));
            AV32TFContratoArquivosAnexos_Codigo_To = (int)(NumberUtil.Val( Ddo_contratoarquivosanexos_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFContratoArquivosAnexos_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFContratoArquivosAnexos_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E136M2( )
      {
         /* Ddo_contrato_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contrato_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contrato_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_codigo_Internalname, "SortedStatus", Ddo_contrato_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contrato_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contrato_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_codigo_Internalname, "SortedStatus", Ddo_contrato_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contrato_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV35TFContrato_Codigo = (int)(NumberUtil.Val( Ddo_contrato_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFContrato_Codigo), 6, 0)));
            AV36TFContrato_Codigo_To = (int)(NumberUtil.Val( Ddo_contrato_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFContrato_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E146M2( )
      {
         /* Ddo_contrato_numero_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contrato_numero_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contrato_numero_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contrato_numero_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contrato_numero_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contrato_numero_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV39TFContrato_Numero = Ddo_contrato_numero_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContrato_Numero", AV39TFContrato_Numero);
            AV40TFContrato_Numero_Sel = Ddo_contrato_numero_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContrato_Numero_Sel", AV40TFContrato_Numero_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E156M2( )
      {
         /* Ddo_contratada_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratada_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_codigo_Internalname, "SortedStatus", Ddo_contratada_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_codigo_Internalname, "SortedStatus", Ddo_contratada_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV43TFContratada_Codigo = (int)(NumberUtil.Val( Ddo_contratada_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFContratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFContratada_Codigo), 6, 0)));
            AV44TFContratada_Codigo_To = (int)(NumberUtil.Val( Ddo_contratada_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContratada_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFContratada_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E166M2( )
      {
         /* Ddo_contratada_pessoacod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratada_pessoacod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_pessoacod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacod_Internalname, "SortedStatus", Ddo_contratada_pessoacod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_pessoacod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_pessoacod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacod_Internalname, "SortedStatus", Ddo_contratada_pessoacod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_pessoacod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV47TFContratada_PessoaCod = (int)(NumberUtil.Val( Ddo_contratada_pessoacod_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFContratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47TFContratada_PessoaCod), 6, 0)));
            AV48TFContratada_PessoaCod_To = (int)(NumberUtil.Val( Ddo_contratada_pessoacod_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFContratada_PessoaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48TFContratada_PessoaCod_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E176M2( )
      {
         /* Ddo_contratada_pessoanom_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratada_pessoanom_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_pessoanom_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "SortedStatus", Ddo_contratada_pessoanom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_pessoanom_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_pessoanom_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "SortedStatus", Ddo_contratada_pessoanom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_pessoanom_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV51TFContratada_PessoaNom = Ddo_contratada_pessoanom_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFContratada_PessoaNom", AV51TFContratada_PessoaNom);
            AV52TFContratada_PessoaNom_Sel = Ddo_contratada_pessoanom_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFContratada_PessoaNom_Sel", AV52TFContratada_PessoaNom_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E186M2( )
      {
         /* Ddo_contratada_pessoacnpj_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratada_pessoacnpj_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_pessoacnpj_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "SortedStatus", Ddo_contratada_pessoacnpj_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_pessoacnpj_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_pessoacnpj_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "SortedStatus", Ddo_contratada_pessoacnpj_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_pessoacnpj_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV55TFContratada_PessoaCNPJ = Ddo_contratada_pessoacnpj_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFContratada_PessoaCNPJ", AV55TFContratada_PessoaCNPJ);
            AV56TFContratada_PessoaCNPJ_Sel = Ddo_contratada_pessoacnpj_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFContratada_PessoaCNPJ_Sel", AV56TFContratada_PessoaCNPJ_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E196M2( )
      {
         /* Ddo_contratoarquivosanexos_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoarquivosanexos_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoarquivosanexos_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_descricao_Internalname, "SortedStatus", Ddo_contratoarquivosanexos_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoarquivosanexos_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoarquivosanexos_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_descricao_Internalname, "SortedStatus", Ddo_contratoarquivosanexos_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoarquivosanexos_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV59TFContratoArquivosAnexos_Descricao = Ddo_contratoarquivosanexos_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContratoArquivosAnexos_Descricao", AV59TFContratoArquivosAnexos_Descricao);
            AV60TFContratoArquivosAnexos_Descricao_Sel = Ddo_contratoarquivosanexos_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContratoArquivosAnexos_Descricao_Sel", AV60TFContratoArquivosAnexos_Descricao_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E206M2( )
      {
         /* Ddo_contratoarquivosanexos_data_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoarquivosanexos_data_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 9;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoarquivosanexos_data_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_data_Internalname, "SortedStatus", Ddo_contratoarquivosanexos_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoarquivosanexos_data_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 9;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoarquivosanexos_data_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_data_Internalname, "SortedStatus", Ddo_contratoarquivosanexos_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoarquivosanexos_data_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV63TFContratoArquivosAnexos_Data = context.localUtil.CToT( Ddo_contratoarquivosanexos_data_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFContratoArquivosAnexos_Data", context.localUtil.TToC( AV63TFContratoArquivosAnexos_Data, 8, 5, 0, 3, "/", ":", " "));
            AV64TFContratoArquivosAnexos_Data_To = context.localUtil.CToT( Ddo_contratoarquivosanexos_data_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFContratoArquivosAnexos_Data_To", context.localUtil.TToC( AV64TFContratoArquivosAnexos_Data_To, 8, 5, 0, 3, "/", ":", " "));
            if ( ! (DateTime.MinValue==AV64TFContratoArquivosAnexos_Data_To) )
            {
               AV64TFContratoArquivosAnexos_Data_To = context.localUtil.YMDHMSToT( (short)(DateTimeUtil.Year( AV64TFContratoArquivosAnexos_Data_To)), (short)(DateTimeUtil.Month( AV64TFContratoArquivosAnexos_Data_To)), (short)(DateTimeUtil.Day( AV64TFContratoArquivosAnexos_Data_To)), 23, 59, 59);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFContratoArquivosAnexos_Data_To", context.localUtil.TToC( AV64TFContratoArquivosAnexos_Data_To, 8, 5, 0, 3, "/", ":", " "));
            }
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E336M2( )
      {
         /* Grid_Load Routine */
         AV28Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV28Select);
         AV77Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 80;
         }
         sendrow_802( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_80_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(80, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E346M2 */
         E346M2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E346M2( )
      {
         /* Enter Routine */
         AV7InOutContratoArquivosAnexos_Codigo = A108ContratoArquivosAnexos_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContratoArquivosAnexos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContratoArquivosAnexos_Codigo), 6, 0)));
         AV8InOutContratoArquivosAnexos_Descricao = A110ContratoArquivosAnexos_Descricao;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContratoArquivosAnexos_Descricao", AV8InOutContratoArquivosAnexos_Descricao);
         context.setWebReturnParms(new Object[] {(int)AV7InOutContratoArquivosAnexos_Codigo,(String)AV8InOutContratoArquivosAnexos_Descricao});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E216M2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E266M2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E226M2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoArquivosAnexos_Descricao1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoArquivosAnexos_Descricao2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ContratoArquivosAnexos_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFContratoArquivosAnexos_Codigo, AV32TFContratoArquivosAnexos_Codigo_To, AV35TFContrato_Codigo, AV36TFContrato_Codigo_To, AV39TFContrato_Numero, AV40TFContrato_Numero_Sel, AV43TFContratada_Codigo, AV44TFContratada_Codigo_To, AV47TFContratada_PessoaCod, AV48TFContratada_PessoaCod_To, AV51TFContratada_PessoaNom, AV52TFContratada_PessoaNom_Sel, AV55TFContratada_PessoaCNPJ, AV56TFContratada_PessoaCNPJ_Sel, AV59TFContratoArquivosAnexos_Descricao, AV60TFContratoArquivosAnexos_Descricao_Sel, AV63TFContratoArquivosAnexos_Data, AV64TFContratoArquivosAnexos_Data_To, AV33ddo_ContratoArquivosAnexos_CodigoTitleControlIdToReplace, AV37ddo_Contrato_CodigoTitleControlIdToReplace, AV41ddo_Contrato_NumeroTitleControlIdToReplace, AV45ddo_Contratada_CodigoTitleControlIdToReplace, AV49ddo_Contratada_PessoaCodTitleControlIdToReplace, AV53ddo_Contratada_PessoaNomTitleControlIdToReplace, AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV61ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace, AV67ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace, AV78Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E276M2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E286M2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E236M2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoArquivosAnexos_Descricao1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoArquivosAnexos_Descricao2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ContratoArquivosAnexos_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFContratoArquivosAnexos_Codigo, AV32TFContratoArquivosAnexos_Codigo_To, AV35TFContrato_Codigo, AV36TFContrato_Codigo_To, AV39TFContrato_Numero, AV40TFContrato_Numero_Sel, AV43TFContratada_Codigo, AV44TFContratada_Codigo_To, AV47TFContratada_PessoaCod, AV48TFContratada_PessoaCod_To, AV51TFContratada_PessoaNom, AV52TFContratada_PessoaNom_Sel, AV55TFContratada_PessoaCNPJ, AV56TFContratada_PessoaCNPJ_Sel, AV59TFContratoArquivosAnexos_Descricao, AV60TFContratoArquivosAnexos_Descricao_Sel, AV63TFContratoArquivosAnexos_Data, AV64TFContratoArquivosAnexos_Data_To, AV33ddo_ContratoArquivosAnexos_CodigoTitleControlIdToReplace, AV37ddo_Contrato_CodigoTitleControlIdToReplace, AV41ddo_Contrato_NumeroTitleControlIdToReplace, AV45ddo_Contratada_CodigoTitleControlIdToReplace, AV49ddo_Contratada_PessoaCodTitleControlIdToReplace, AV53ddo_Contratada_PessoaNomTitleControlIdToReplace, AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV61ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace, AV67ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace, AV78Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E296M2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E246M2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoArquivosAnexos_Descricao1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoArquivosAnexos_Descricao2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ContratoArquivosAnexos_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFContratoArquivosAnexos_Codigo, AV32TFContratoArquivosAnexos_Codigo_To, AV35TFContrato_Codigo, AV36TFContrato_Codigo_To, AV39TFContrato_Numero, AV40TFContrato_Numero_Sel, AV43TFContratada_Codigo, AV44TFContratada_Codigo_To, AV47TFContratada_PessoaCod, AV48TFContratada_PessoaCod_To, AV51TFContratada_PessoaNom, AV52TFContratada_PessoaNom_Sel, AV55TFContratada_PessoaCNPJ, AV56TFContratada_PessoaCNPJ_Sel, AV59TFContratoArquivosAnexos_Descricao, AV60TFContratoArquivosAnexos_Descricao_Sel, AV63TFContratoArquivosAnexos_Data, AV64TFContratoArquivosAnexos_Data_To, AV33ddo_ContratoArquivosAnexos_CodigoTitleControlIdToReplace, AV37ddo_Contrato_CodigoTitleControlIdToReplace, AV41ddo_Contrato_NumeroTitleControlIdToReplace, AV45ddo_Contratada_CodigoTitleControlIdToReplace, AV49ddo_Contratada_PessoaCodTitleControlIdToReplace, AV53ddo_Contratada_PessoaNomTitleControlIdToReplace, AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV61ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace, AV67ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace, AV78Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E306M2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E256M2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contratoarquivosanexos_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_codigo_Internalname, "SortedStatus", Ddo_contratoarquivosanexos_codigo_Sortedstatus);
         Ddo_contrato_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_codigo_Internalname, "SortedStatus", Ddo_contrato_codigo_Sortedstatus);
         Ddo_contrato_numero_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
         Ddo_contratada_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_codigo_Internalname, "SortedStatus", Ddo_contratada_codigo_Sortedstatus);
         Ddo_contratada_pessoacod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacod_Internalname, "SortedStatus", Ddo_contratada_pessoacod_Sortedstatus);
         Ddo_contratada_pessoanom_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "SortedStatus", Ddo_contratada_pessoanom_Sortedstatus);
         Ddo_contratada_pessoacnpj_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "SortedStatus", Ddo_contratada_pessoacnpj_Sortedstatus);
         Ddo_contratoarquivosanexos_descricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_descricao_Internalname, "SortedStatus", Ddo_contratoarquivosanexos_descricao_Sortedstatus);
         Ddo_contratoarquivosanexos_data_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_data_Internalname, "SortedStatus", Ddo_contratoarquivosanexos_data_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_contratoarquivosanexos_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_codigo_Internalname, "SortedStatus", Ddo_contratoarquivosanexos_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_contrato_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_codigo_Internalname, "SortedStatus", Ddo_contrato_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_contrato_numero_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_contratada_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_codigo_Internalname, "SortedStatus", Ddo_contratada_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 6 )
         {
            Ddo_contratada_pessoacod_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacod_Internalname, "SortedStatus", Ddo_contratada_pessoacod_Sortedstatus);
         }
         else if ( AV13OrderedBy == 7 )
         {
            Ddo_contratada_pessoanom_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "SortedStatus", Ddo_contratada_pessoanom_Sortedstatus);
         }
         else if ( AV13OrderedBy == 8 )
         {
            Ddo_contratada_pessoacnpj_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "SortedStatus", Ddo_contratada_pessoacnpj_Sortedstatus);
         }
         else if ( AV13OrderedBy == 1 )
         {
            Ddo_contratoarquivosanexos_descricao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_descricao_Internalname, "SortedStatus", Ddo_contratoarquivosanexos_descricao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 9 )
         {
            Ddo_contratoarquivosanexos_data_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_data_Internalname, "SortedStatus", Ddo_contratoarquivosanexos_data_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavContratoarquivosanexos_descricao1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoarquivosanexos_descricao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoarquivosanexos_descricao1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 )
         {
            edtavContratoarquivosanexos_descricao1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoarquivosanexos_descricao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoarquivosanexos_descricao1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavContratoarquivosanexos_descricao2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoarquivosanexos_descricao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoarquivosanexos_descricao2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 )
         {
            edtavContratoarquivosanexos_descricao2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoarquivosanexos_descricao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoarquivosanexos_descricao2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavContratoarquivosanexos_descricao3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoarquivosanexos_descricao3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoarquivosanexos_descricao3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 )
         {
            edtavContratoarquivosanexos_descricao3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoarquivosanexos_descricao3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoarquivosanexos_descricao3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "CONTRATOARQUIVOSANEXOS_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV20DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         AV21ContratoArquivosAnexos_Descricao2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoArquivosAnexos_Descricao2", AV21ContratoArquivosAnexos_Descricao2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "CONTRATOARQUIVOSANEXOS_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV24DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         AV25ContratoArquivosAnexos_Descricao3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoArquivosAnexos_Descricao3", AV25ContratoArquivosAnexos_Descricao3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S202( )
      {
         /* 'CLEANFILTERS' Routine */
         AV31TFContratoArquivosAnexos_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFContratoArquivosAnexos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFContratoArquivosAnexos_Codigo), 6, 0)));
         Ddo_contratoarquivosanexos_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_codigo_Internalname, "FilteredText_set", Ddo_contratoarquivosanexos_codigo_Filteredtext_set);
         AV32TFContratoArquivosAnexos_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFContratoArquivosAnexos_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFContratoArquivosAnexos_Codigo_To), 6, 0)));
         Ddo_contratoarquivosanexos_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_codigo_Internalname, "FilteredTextTo_set", Ddo_contratoarquivosanexos_codigo_Filteredtextto_set);
         AV35TFContrato_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFContrato_Codigo), 6, 0)));
         Ddo_contrato_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_codigo_Internalname, "FilteredText_set", Ddo_contrato_codigo_Filteredtext_set);
         AV36TFContrato_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFContrato_Codigo_To), 6, 0)));
         Ddo_contrato_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_codigo_Internalname, "FilteredTextTo_set", Ddo_contrato_codigo_Filteredtextto_set);
         AV39TFContrato_Numero = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContrato_Numero", AV39TFContrato_Numero);
         Ddo_contrato_numero_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "FilteredText_set", Ddo_contrato_numero_Filteredtext_set);
         AV40TFContrato_Numero_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContrato_Numero_Sel", AV40TFContrato_Numero_Sel);
         Ddo_contrato_numero_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SelectedValue_set", Ddo_contrato_numero_Selectedvalue_set);
         AV43TFContratada_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFContratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFContratada_Codigo), 6, 0)));
         Ddo_contratada_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_codigo_Internalname, "FilteredText_set", Ddo_contratada_codigo_Filteredtext_set);
         AV44TFContratada_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContratada_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFContratada_Codigo_To), 6, 0)));
         Ddo_contratada_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_codigo_Internalname, "FilteredTextTo_set", Ddo_contratada_codigo_Filteredtextto_set);
         AV47TFContratada_PessoaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFContratada_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47TFContratada_PessoaCod), 6, 0)));
         Ddo_contratada_pessoacod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacod_Internalname, "FilteredText_set", Ddo_contratada_pessoacod_Filteredtext_set);
         AV48TFContratada_PessoaCod_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFContratada_PessoaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48TFContratada_PessoaCod_To), 6, 0)));
         Ddo_contratada_pessoacod_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacod_Internalname, "FilteredTextTo_set", Ddo_contratada_pessoacod_Filteredtextto_set);
         AV51TFContratada_PessoaNom = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFContratada_PessoaNom", AV51TFContratada_PessoaNom);
         Ddo_contratada_pessoanom_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "FilteredText_set", Ddo_contratada_pessoanom_Filteredtext_set);
         AV52TFContratada_PessoaNom_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFContratada_PessoaNom_Sel", AV52TFContratada_PessoaNom_Sel);
         Ddo_contratada_pessoanom_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "SelectedValue_set", Ddo_contratada_pessoanom_Selectedvalue_set);
         AV55TFContratada_PessoaCNPJ = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFContratada_PessoaCNPJ", AV55TFContratada_PessoaCNPJ);
         Ddo_contratada_pessoacnpj_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "FilteredText_set", Ddo_contratada_pessoacnpj_Filteredtext_set);
         AV56TFContratada_PessoaCNPJ_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFContratada_PessoaCNPJ_Sel", AV56TFContratada_PessoaCNPJ_Sel);
         Ddo_contratada_pessoacnpj_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "SelectedValue_set", Ddo_contratada_pessoacnpj_Selectedvalue_set);
         AV59TFContratoArquivosAnexos_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFContratoArquivosAnexos_Descricao", AV59TFContratoArquivosAnexos_Descricao);
         Ddo_contratoarquivosanexos_descricao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_descricao_Internalname, "FilteredText_set", Ddo_contratoarquivosanexos_descricao_Filteredtext_set);
         AV60TFContratoArquivosAnexos_Descricao_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContratoArquivosAnexos_Descricao_Sel", AV60TFContratoArquivosAnexos_Descricao_Sel);
         Ddo_contratoarquivosanexos_descricao_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_descricao_Internalname, "SelectedValue_set", Ddo_contratoarquivosanexos_descricao_Selectedvalue_set);
         AV63TFContratoArquivosAnexos_Data = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFContratoArquivosAnexos_Data", context.localUtil.TToC( AV63TFContratoArquivosAnexos_Data, 8, 5, 0, 3, "/", ":", " "));
         Ddo_contratoarquivosanexos_data_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_data_Internalname, "FilteredText_set", Ddo_contratoarquivosanexos_data_Filteredtext_set);
         AV64TFContratoArquivosAnexos_Data_To = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFContratoArquivosAnexos_Data_To", context.localUtil.TToC( AV64TFContratoArquivosAnexos_Data_To, 8, 5, 0, 3, "/", ":", " "));
         Ddo_contratoarquivosanexos_data_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoarquivosanexos_data_Internalname, "FilteredTextTo_set", Ddo_contratoarquivosanexos_data_Filteredtextto_set);
         AV15DynamicFiltersSelector1 = "CONTRATOARQUIVOSANEXOS_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         AV17ContratoArquivosAnexos_Descricao1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoArquivosAnexos_Descricao1", AV17ContratoArquivosAnexos_Descricao1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17ContratoArquivosAnexos_Descricao1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoArquivosAnexos_Descricao1", AV17ContratoArquivosAnexos_Descricao1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 )
               {
                  AV20DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
                  AV21ContratoArquivosAnexos_Descricao2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoArquivosAnexos_Descricao2", AV21ContratoArquivosAnexos_Descricao2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 )
                  {
                     AV24DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
                     AV25ContratoArquivosAnexos_Descricao3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoArquivosAnexos_Descricao3", AV25ContratoArquivosAnexos_Descricao3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (0==AV31TFContratoArquivosAnexos_Codigo) && (0==AV32TFContratoArquivosAnexos_Codigo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOARQUIVOSANEXOS_CODIGO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV31TFContratoArquivosAnexos_Codigo), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV32TFContratoArquivosAnexos_Codigo_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV35TFContrato_Codigo) && (0==AV36TFContrato_Codigo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATO_CODIGO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV35TFContrato_Codigo), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV36TFContrato_Codigo_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFContrato_Numero)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATO_NUMERO";
            AV11GridStateFilterValue.gxTpr_Value = AV39TFContrato_Numero;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFContrato_Numero_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATO_NUMERO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV40TFContrato_Numero_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV43TFContratada_Codigo) && (0==AV44TFContratada_Codigo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATADA_CODIGO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV43TFContratada_Codigo), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV44TFContratada_Codigo_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV47TFContratada_PessoaCod) && (0==AV48TFContratada_PessoaCod_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATADA_PESSOACOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV47TFContratada_PessoaCod), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV48TFContratada_PessoaCod_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51TFContratada_PessoaNom)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATADA_PESSOANOM";
            AV11GridStateFilterValue.gxTpr_Value = AV51TFContratada_PessoaNom;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52TFContratada_PessoaNom_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATADA_PESSOANOM_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV52TFContratada_PessoaNom_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55TFContratada_PessoaCNPJ)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATADA_PESSOACNPJ";
            AV11GridStateFilterValue.gxTpr_Value = AV55TFContratada_PessoaCNPJ;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56TFContratada_PessoaCNPJ_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATADA_PESSOACNPJ_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV56TFContratada_PessoaCNPJ_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59TFContratoArquivosAnexos_Descricao)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOARQUIVOSANEXOS_DESCRICAO";
            AV11GridStateFilterValue.gxTpr_Value = AV59TFContratoArquivosAnexos_Descricao;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60TFContratoArquivosAnexos_Descricao_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOARQUIVOSANEXOS_DESCRICAO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV60TFContratoArquivosAnexos_Descricao_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV63TFContratoArquivosAnexos_Data) && (DateTime.MinValue==AV64TFContratoArquivosAnexos_Data_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOARQUIVOSANEXOS_DATA";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.TToC( AV63TFContratoArquivosAnexos_Data, 8, 5, 0, 3, "/", ":", " ");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.TToC( AV64TFContratoArquivosAnexos_Data_To, 8, 5, 0, 3, "/", ":", " ");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV78Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17ContratoArquivosAnexos_Descricao1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17ContratoArquivosAnexos_Descricao1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV21ContratoArquivosAnexos_Descricao2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV21ContratoArquivosAnexos_Descricao2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV20DynamicFiltersOperator2;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV25ContratoArquivosAnexos_Descricao3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV25ContratoArquivosAnexos_Descricao3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV24DynamicFiltersOperator3;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_6M2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_6M2( true) ;
         }
         else
         {
            wb_table2_5_6M2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_6M2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_74_6M2( true) ;
         }
         else
         {
            wb_table3_74_6M2( false) ;
         }
         return  ;
      }

      protected void wb_table3_74_6M2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_6M2e( true) ;
         }
         else
         {
            wb_table1_2_6M2e( false) ;
         }
      }

      protected void wb_table3_74_6M2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_77_6M2( true) ;
         }
         else
         {
            wb_table4_77_6M2( false) ;
         }
         return  ;
      }

      protected void wb_table4_77_6M2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_74_6M2e( true) ;
         }
         else
         {
            wb_table3_74_6M2e( false) ;
         }
      }

      protected void wb_table4_77_6M2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"80\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoArquivosAnexos_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoArquivosAnexos_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoArquivosAnexos_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContrato_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtContrato_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContrato_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(127), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContrato_Numero_Titleformat == 0 )
               {
                  context.SendWebValue( edtContrato_Numero_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContrato_Numero_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratada_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratada_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratada_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratada_PessoaCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratada_PessoaCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratada_PessoaCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratada_PessoaNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratada_PessoaNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratada_PessoaNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratada_PessoaCNPJ_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratada_PessoaCNPJ_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratada_PessoaCNPJ_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoArquivosAnexos_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoArquivosAnexos_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoArquivosAnexos_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Arquivo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Nome do Arquivo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Tipo de Arquivo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoArquivosAnexos_Data_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoArquivosAnexos_Data_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoArquivosAnexos_Data_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A108ContratoArquivosAnexos_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoArquivosAnexos_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoArquivosAnexos_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContrato_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContrato_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A77Contrato_Numero));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContrato_Numero_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContrato_Numero_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratada_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A40Contratada_PessoaCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratada_PessoaCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_PessoaCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A41Contratada_PessoaNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratada_PessoaNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_PessoaNom_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A42Contratada_PessoaCNPJ);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratada_PessoaCNPJ_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_PessoaCNPJ_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A110ContratoArquivosAnexos_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoArquivosAnexos_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoArquivosAnexos_Descricao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A111ContratoArquivosAnexos_Arquivo);
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A112ContratoArquivosAnexos_NomeArq));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A109ContratoArquivosAnexos_TipoArq));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( A113ContratoArquivosAnexos_Data, 10, 8, 0, 3, "/", ":", " "));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoArquivosAnexos_Data_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoArquivosAnexos_Data_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 80 )
         {
            wbEnd = 0;
            nRC_GXsfl_80 = (short)(nGXsfl_80_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_77_6M2e( true) ;
         }
         else
         {
            wb_table4_77_6M2e( false) ;
         }
      }

      protected void wb_table2_5_6M2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptContratoArquivosAnexos.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_14_6M2( true) ;
         }
         else
         {
            wb_table5_14_6M2( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_6M2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_6M2e( true) ;
         }
         else
         {
            wb_table2_5_6M2e( false) ;
         }
      }

      protected void wb_table5_14_6M2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_19_6M2( true) ;
         }
         else
         {
            wb_table6_19_6M2( false) ;
         }
         return  ;
      }

      protected void wb_table6_19_6M2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_6M2e( true) ;
         }
         else
         {
            wb_table5_14_6M2e( false) ;
         }
      }

      protected void wb_table6_19_6M2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_PromptContratoArquivosAnexos.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_6M2( true) ;
         }
         else
         {
            wb_table7_28_6M2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_6M2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoArquivosAnexos.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,41);\"", "", true, "HLP_PromptContratoArquivosAnexos.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_45_6M2( true) ;
         }
         else
         {
            wb_table8_45_6M2( false) ;
         }
         return  ;
      }

      protected void wb_table8_45_6M2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoArquivosAnexos.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,58);\"", "", true, "HLP_PromptContratoArquivosAnexos.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_62_6M2( true) ;
         }
         else
         {
            wb_table9_62_6M2( false) ;
         }
         return  ;
      }

      protected void wb_table9_62_6M2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_19_6M2e( true) ;
         }
         else
         {
            wb_table6_19_6M2e( false) ;
         }
      }

      protected void wb_table9_62_6M2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,65);\"", "", true, "HLP_PromptContratoArquivosAnexos.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavContratoarquivosanexos_descricao3_Internalname, AV25ContratoArquivosAnexos_Descricao3, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,67);\"", 0, edtavContratoarquivosanexos_descricao3_Visible, 1, 0, 80, "chr", 7, "row", StyleString, ClassString, "", "500", -1, "", "", -1, true, "", "HLP_PromptContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_62_6M2e( true) ;
         }
         else
         {
            wb_table9_62_6M2e( false) ;
         }
      }

      protected void wb_table8_45_6M2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,48);\"", "", true, "HLP_PromptContratoArquivosAnexos.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavContratoarquivosanexos_descricao2_Internalname, AV21ContratoArquivosAnexos_Descricao2, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", 0, edtavContratoarquivosanexos_descricao2_Visible, 1, 0, 80, "chr", 7, "row", StyleString, ClassString, "", "500", -1, "", "", -1, true, "", "HLP_PromptContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_45_6M2e( true) ;
         }
         else
         {
            wb_table8_45_6M2e( false) ;
         }
      }

      protected void wb_table7_28_6M2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_PromptContratoArquivosAnexos.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavContratoarquivosanexos_descricao1_Internalname, AV17ContratoArquivosAnexos_Descricao1, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", 0, edtavContratoarquivosanexos_descricao1_Visible, 1, 0, 80, "chr", 7, "row", StyleString, ClassString, "", "500", -1, "", "", -1, true, "", "HLP_PromptContratoArquivosAnexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_6M2e( true) ;
         }
         else
         {
            wb_table7_28_6M2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutContratoArquivosAnexos_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContratoArquivosAnexos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContratoArquivosAnexos_Codigo), 6, 0)));
         AV8InOutContratoArquivosAnexos_Descricao = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContratoArquivosAnexos_Descricao", AV8InOutContratoArquivosAnexos_Descricao);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA6M2( ) ;
         WS6M2( ) ;
         WE6M2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117352792");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptcontratoarquivosanexos.js", "?20203117352792");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_802( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_80_idx;
         edtContratoArquivosAnexos_Codigo_Internalname = "CONTRATOARQUIVOSANEXOS_CODIGO_"+sGXsfl_80_idx;
         edtContrato_Codigo_Internalname = "CONTRATO_CODIGO_"+sGXsfl_80_idx;
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO_"+sGXsfl_80_idx;
         edtContratada_Codigo_Internalname = "CONTRATADA_CODIGO_"+sGXsfl_80_idx;
         edtContratada_PessoaCod_Internalname = "CONTRATADA_PESSOACOD_"+sGXsfl_80_idx;
         edtContratada_PessoaNom_Internalname = "CONTRATADA_PESSOANOM_"+sGXsfl_80_idx;
         edtContratada_PessoaCNPJ_Internalname = "CONTRATADA_PESSOACNPJ_"+sGXsfl_80_idx;
         edtContratoArquivosAnexos_Descricao_Internalname = "CONTRATOARQUIVOSANEXOS_DESCRICAO_"+sGXsfl_80_idx;
         edtContratoArquivosAnexos_Arquivo_Internalname = "CONTRATOARQUIVOSANEXOS_ARQUIVO_"+sGXsfl_80_idx;
         edtContratoArquivosAnexos_NomeArq_Internalname = "CONTRATOARQUIVOSANEXOS_NOMEARQ_"+sGXsfl_80_idx;
         edtContratoArquivosAnexos_TipoArq_Internalname = "CONTRATOARQUIVOSANEXOS_TIPOARQ_"+sGXsfl_80_idx;
         edtContratoArquivosAnexos_Data_Internalname = "CONTRATOARQUIVOSANEXOS_DATA_"+sGXsfl_80_idx;
      }

      protected void SubsflControlProps_fel_802( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_80_fel_idx;
         edtContratoArquivosAnexos_Codigo_Internalname = "CONTRATOARQUIVOSANEXOS_CODIGO_"+sGXsfl_80_fel_idx;
         edtContrato_Codigo_Internalname = "CONTRATO_CODIGO_"+sGXsfl_80_fel_idx;
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO_"+sGXsfl_80_fel_idx;
         edtContratada_Codigo_Internalname = "CONTRATADA_CODIGO_"+sGXsfl_80_fel_idx;
         edtContratada_PessoaCod_Internalname = "CONTRATADA_PESSOACOD_"+sGXsfl_80_fel_idx;
         edtContratada_PessoaNom_Internalname = "CONTRATADA_PESSOANOM_"+sGXsfl_80_fel_idx;
         edtContratada_PessoaCNPJ_Internalname = "CONTRATADA_PESSOACNPJ_"+sGXsfl_80_fel_idx;
         edtContratoArquivosAnexos_Descricao_Internalname = "CONTRATOARQUIVOSANEXOS_DESCRICAO_"+sGXsfl_80_fel_idx;
         edtContratoArquivosAnexos_Arquivo_Internalname = "CONTRATOARQUIVOSANEXOS_ARQUIVO_"+sGXsfl_80_fel_idx;
         edtContratoArquivosAnexos_NomeArq_Internalname = "CONTRATOARQUIVOSANEXOS_NOMEARQ_"+sGXsfl_80_fel_idx;
         edtContratoArquivosAnexos_TipoArq_Internalname = "CONTRATOARQUIVOSANEXOS_TIPOARQ_"+sGXsfl_80_fel_idx;
         edtContratoArquivosAnexos_Data_Internalname = "CONTRATOARQUIVOSANEXOS_DATA_"+sGXsfl_80_fel_idx;
      }

      protected void sendrow_802( )
      {
         SubsflControlProps_802( ) ;
         WB6M0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_80_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_80_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_80_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 81,'',false,'',80)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV28Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV77Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV77Select_GXI : context.PathToRelativeUrl( AV28Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_80_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV28Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoArquivosAnexos_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A108ContratoArquivosAnexos_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A108ContratoArquivosAnexos_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoArquivosAnexos_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContrato_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContrato_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContrato_Numero_Internalname,StringUtil.RTrim( A77Contrato_Numero),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContrato_Numero_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)127,(String)"px",(short)17,(String)"px",(short)20,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)-1,(bool)true,(String)"NumeroContrato",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A39Contratada_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_PessoaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A40Contratada_PessoaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A40Contratada_PessoaCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_PessoaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_PessoaNom_Internalname,StringUtil.RTrim( A41Contratada_PessoaNom),StringUtil.RTrim( context.localUtil.Format( A41Contratada_PessoaNom, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_PessoaNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_PessoaCNPJ_Internalname,(String)A42Contratada_PessoaCNPJ,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_PessoaCNPJ_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)-1,(bool)true,(String)"Docto",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoArquivosAnexos_Descricao_Internalname,(String)A110ContratoArquivosAnexos_Descricao,(String)A110ContratoArquivosAnexos_Descricao,(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoArquivosAnexos_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)500,(short)0,(short)0,(short)80,(short)1,(short)0,(short)-1,(bool)true,(String)"DescricaoLonga",(String)"left",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            ClassString = "Image";
            StyleString = "";
            edtContratoArquivosAnexos_Arquivo_Filename = A112ContratoArquivosAnexos_NomeArq;
            edtContratoArquivosAnexos_Arquivo_Filetype = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoArquivosAnexos_Arquivo_Internalname, "Filetype", edtContratoArquivosAnexos_Arquivo_Filetype);
            edtContratoArquivosAnexos_Arquivo_Filetype = A109ContratoArquivosAnexos_TipoArq;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoArquivosAnexos_Arquivo_Internalname, "Filetype", edtContratoArquivosAnexos_Arquivo_Filetype);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A111ContratoArquivosAnexos_Arquivo)) )
            {
               gxblobfileaux.Source = A111ContratoArquivosAnexos_Arquivo;
               if ( ! gxblobfileaux.HasExtension() || ( StringUtil.StrCmp(edtContratoArquivosAnexos_Arquivo_Filetype, "tmp") != 0 ) )
               {
                  gxblobfileaux.SetExtension(StringUtil.Trim( edtContratoArquivosAnexos_Arquivo_Filetype));
               }
               if ( gxblobfileaux.ErrCode == 0 )
               {
                  A111ContratoArquivosAnexos_Arquivo = gxblobfileaux.GetAbsoluteName();
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoArquivosAnexos_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A111ContratoArquivosAnexos_Arquivo));
                  edtContratoArquivosAnexos_Arquivo_Filetype = gxblobfileaux.GetExtension();
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoArquivosAnexos_Arquivo_Internalname, "Filetype", edtContratoArquivosAnexos_Arquivo_Filetype);
               }
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoArquivosAnexos_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A111ContratoArquivosAnexos_Arquivo));
            }
            GridRow.AddColumnProperties("blob", 2, isAjaxCallMode( ), new Object[] {(String)edtContratoArquivosAnexos_Arquivo_Internalname,StringUtil.RTrim( A111ContratoArquivosAnexos_Arquivo),context.PathToRelativeUrl( A111ContratoArquivosAnexos_Arquivo),(String.IsNullOrEmpty(StringUtil.RTrim( edtContratoArquivosAnexos_Arquivo_Contenttype)) ? context.GetContentType( (String.IsNullOrEmpty(StringUtil.RTrim( edtContratoArquivosAnexos_Arquivo_Filetype)) ? A111ContratoArquivosAnexos_Arquivo : edtContratoArquivosAnexos_Arquivo_Filetype)) : edtContratoArquivosAnexos_Arquivo_Contenttype),(bool)true,(String)"",(String)edtContratoArquivosAnexos_Arquivo_Parameters,(short)0,(short)0,(short)-1,(String)"",(String)"",(short)0,(short)-1,(short)0,(String)"px",(short)60,(String)"px",(short)0,(short)0,(short)0,(String)edtContratoArquivosAnexos_Arquivo_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)StyleString,(String)ClassString,(String)"",(String)""+"",(String)"",(String)""});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoArquivosAnexos_NomeArq_Internalname,StringUtil.RTrim( A112ContratoArquivosAnexos_NomeArq),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoArquivosAnexos_NomeArq_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)-1,(bool)true,(String)"NomeArq",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoArquivosAnexos_TipoArq_Internalname,StringUtil.RTrim( A109ContratoArquivosAnexos_TipoArq),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoArquivosAnexos_TipoArq_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)10,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)-1,(bool)true,(String)"TipoArq",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoArquivosAnexos_Data_Internalname,context.localUtil.TToC( A113ContratoArquivosAnexos_Data, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A113ContratoArquivosAnexos_Data, "99/99/99 99:99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoArquivosAnexos_Data_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"DataHora",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOARQUIVOSANEXOS_CODIGO"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, context.localUtil.Format( (decimal)(A108ContratoArquivosAnexos_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATO_CODIGO"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOARQUIVOSANEXOS_DESCRICAO"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, A110ContratoArquivosAnexos_Descricao));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOARQUIVOSANEXOS_DATA"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, context.localUtil.Format( A113ContratoArquivosAnexos_Data, "99/99/99 99:99")));
            GridContainer.AddRow(GridRow);
            nGXsfl_80_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_80_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_80_idx+1));
            sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
            SubsflControlProps_802( ) ;
         }
         /* End function sendrow_802 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavContratoarquivosanexos_descricao1_Internalname = "vCONTRATOARQUIVOSANEXOS_DESCRICAO1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavContratoarquivosanexos_descricao2_Internalname = "vCONTRATOARQUIVOSANEXOS_DESCRICAO2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavContratoarquivosanexos_descricao3_Internalname = "vCONTRATOARQUIVOSANEXOS_DESCRICAO3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtContratoArquivosAnexos_Codigo_Internalname = "CONTRATOARQUIVOSANEXOS_CODIGO";
         edtContrato_Codigo_Internalname = "CONTRATO_CODIGO";
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO";
         edtContratada_Codigo_Internalname = "CONTRATADA_CODIGO";
         edtContratada_PessoaCod_Internalname = "CONTRATADA_PESSOACOD";
         edtContratada_PessoaNom_Internalname = "CONTRATADA_PESSOANOM";
         edtContratada_PessoaCNPJ_Internalname = "CONTRATADA_PESSOACNPJ";
         edtContratoArquivosAnexos_Descricao_Internalname = "CONTRATOARQUIVOSANEXOS_DESCRICAO";
         edtContratoArquivosAnexos_Arquivo_Internalname = "CONTRATOARQUIVOSANEXOS_ARQUIVO";
         edtContratoArquivosAnexos_NomeArq_Internalname = "CONTRATOARQUIVOSANEXOS_NOMEARQ";
         edtContratoArquivosAnexos_TipoArq_Internalname = "CONTRATOARQUIVOSANEXOS_TIPOARQ";
         edtContratoArquivosAnexos_Data_Internalname = "CONTRATOARQUIVOSANEXOS_DATA";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfcontratoarquivosanexos_codigo_Internalname = "vTFCONTRATOARQUIVOSANEXOS_CODIGO";
         edtavTfcontratoarquivosanexos_codigo_to_Internalname = "vTFCONTRATOARQUIVOSANEXOS_CODIGO_TO";
         edtavTfcontrato_codigo_Internalname = "vTFCONTRATO_CODIGO";
         edtavTfcontrato_codigo_to_Internalname = "vTFCONTRATO_CODIGO_TO";
         edtavTfcontrato_numero_Internalname = "vTFCONTRATO_NUMERO";
         edtavTfcontrato_numero_sel_Internalname = "vTFCONTRATO_NUMERO_SEL";
         edtavTfcontratada_codigo_Internalname = "vTFCONTRATADA_CODIGO";
         edtavTfcontratada_codigo_to_Internalname = "vTFCONTRATADA_CODIGO_TO";
         edtavTfcontratada_pessoacod_Internalname = "vTFCONTRATADA_PESSOACOD";
         edtavTfcontratada_pessoacod_to_Internalname = "vTFCONTRATADA_PESSOACOD_TO";
         edtavTfcontratada_pessoanom_Internalname = "vTFCONTRATADA_PESSOANOM";
         edtavTfcontratada_pessoanom_sel_Internalname = "vTFCONTRATADA_PESSOANOM_SEL";
         edtavTfcontratada_pessoacnpj_Internalname = "vTFCONTRATADA_PESSOACNPJ";
         edtavTfcontratada_pessoacnpj_sel_Internalname = "vTFCONTRATADA_PESSOACNPJ_SEL";
         edtavTfcontratoarquivosanexos_descricao_Internalname = "vTFCONTRATOARQUIVOSANEXOS_DESCRICAO";
         edtavTfcontratoarquivosanexos_descricao_sel_Internalname = "vTFCONTRATOARQUIVOSANEXOS_DESCRICAO_SEL";
         edtavTfcontratoarquivosanexos_data_Internalname = "vTFCONTRATOARQUIVOSANEXOS_DATA";
         edtavTfcontratoarquivosanexos_data_to_Internalname = "vTFCONTRATOARQUIVOSANEXOS_DATA_TO";
         edtavDdo_contratoarquivosanexos_dataauxdate_Internalname = "vDDO_CONTRATOARQUIVOSANEXOS_DATAAUXDATE";
         edtavDdo_contratoarquivosanexos_dataauxdateto_Internalname = "vDDO_CONTRATOARQUIVOSANEXOS_DATAAUXDATETO";
         divDdo_contratoarquivosanexos_dataauxdates_Internalname = "DDO_CONTRATOARQUIVOSANEXOS_DATAAUXDATES";
         Ddo_contratoarquivosanexos_codigo_Internalname = "DDO_CONTRATOARQUIVOSANEXOS_CODIGO";
         edtavDdo_contratoarquivosanexos_codigotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOARQUIVOSANEXOS_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_contrato_codigo_Internalname = "DDO_CONTRATO_CODIGO";
         edtavDdo_contrato_codigotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_contrato_numero_Internalname = "DDO_CONTRATO_NUMERO";
         edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE";
         Ddo_contratada_codigo_Internalname = "DDO_CONTRATADA_CODIGO";
         edtavDdo_contratada_codigotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_contratada_pessoacod_Internalname = "DDO_CONTRATADA_PESSOACOD";
         edtavDdo_contratada_pessoacodtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATADA_PESSOACODTITLECONTROLIDTOREPLACE";
         Ddo_contratada_pessoanom_Internalname = "DDO_CONTRATADA_PESSOANOM";
         edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE";
         Ddo_contratada_pessoacnpj_Internalname = "DDO_CONTRATADA_PESSOACNPJ";
         edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE";
         Ddo_contratoarquivosanexos_descricao_Internalname = "DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO";
         edtavDdo_contratoarquivosanexos_descricaotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOARQUIVOSANEXOS_DESCRICAOTITLECONTROLIDTOREPLACE";
         Ddo_contratoarquivosanexos_data_Internalname = "DDO_CONTRATOARQUIVOSANEXOS_DATA";
         edtavDdo_contratoarquivosanexos_datatitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOARQUIVOSANEXOS_DATATITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtContratoArquivosAnexos_Data_Jsonclick = "";
         edtContratoArquivosAnexos_TipoArq_Jsonclick = "";
         edtContratoArquivosAnexos_NomeArq_Jsonclick = "";
         edtContratoArquivosAnexos_Arquivo_Jsonclick = "";
         edtContratoArquivosAnexos_Arquivo_Parameters = "";
         edtContratoArquivosAnexos_Arquivo_Contenttype = "";
         edtContratoArquivosAnexos_Descricao_Jsonclick = "";
         edtContratada_PessoaCNPJ_Jsonclick = "";
         edtContratada_PessoaNom_Jsonclick = "";
         edtContratada_PessoaCod_Jsonclick = "";
         edtContratada_Codigo_Jsonclick = "";
         edtContrato_Numero_Jsonclick = "";
         edtContrato_Codigo_Jsonclick = "";
         edtContratoArquivosAnexos_Codigo_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         edtContratoArquivosAnexos_Data_Titleformat = 0;
         edtContratoArquivosAnexos_Descricao_Titleformat = 0;
         edtContratada_PessoaCNPJ_Titleformat = 0;
         edtContratada_PessoaNom_Titleformat = 0;
         edtContratada_PessoaCod_Titleformat = 0;
         edtContratada_Codigo_Titleformat = 0;
         edtContrato_Numero_Titleformat = 0;
         edtContrato_Codigo_Titleformat = 0;
         edtContratoArquivosAnexos_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavContratoarquivosanexos_descricao3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavContratoarquivosanexos_descricao2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavContratoarquivosanexos_descricao1_Visible = 1;
         edtContratoArquivosAnexos_Data_Title = "Data/Hora Upload";
         edtContratoArquivosAnexos_Descricao_Title = "Descri��o do Arquvo";
         edtContratada_PessoaCNPJ_Title = " CNPJ da Pessoa";
         edtContratada_PessoaNom_Title = "Nome";
         edtContratada_PessoaCod_Title = "C�d. da Pessoa";
         edtContratada_Codigo_Title = "Contratada";
         edtContrato_Numero_Title = "N�mero do Contrato";
         edtContrato_Codigo_Title = "Contrato";
         edtContratoArquivosAnexos_Codigo_Title = "C�digo";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         edtContratoArquivosAnexos_Arquivo_Filetype = "";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_contratoarquivosanexos_datatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoarquivosanexos_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratada_pessoacodtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratada_codigotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contrato_codigotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoarquivosanexos_codigotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoarquivosanexos_dataauxdateto_Jsonclick = "";
         edtavDdo_contratoarquivosanexos_dataauxdate_Jsonclick = "";
         edtavTfcontratoarquivosanexos_data_to_Jsonclick = "";
         edtavTfcontratoarquivosanexos_data_to_Visible = 1;
         edtavTfcontratoarquivosanexos_data_Jsonclick = "";
         edtavTfcontratoarquivosanexos_data_Visible = 1;
         edtavTfcontratoarquivosanexos_descricao_sel_Visible = 1;
         edtavTfcontratoarquivosanexos_descricao_Visible = 1;
         edtavTfcontratada_pessoacnpj_sel_Jsonclick = "";
         edtavTfcontratada_pessoacnpj_sel_Visible = 1;
         edtavTfcontratada_pessoacnpj_Jsonclick = "";
         edtavTfcontratada_pessoacnpj_Visible = 1;
         edtavTfcontratada_pessoanom_sel_Jsonclick = "";
         edtavTfcontratada_pessoanom_sel_Visible = 1;
         edtavTfcontratada_pessoanom_Jsonclick = "";
         edtavTfcontratada_pessoanom_Visible = 1;
         edtavTfcontratada_pessoacod_to_Jsonclick = "";
         edtavTfcontratada_pessoacod_to_Visible = 1;
         edtavTfcontratada_pessoacod_Jsonclick = "";
         edtavTfcontratada_pessoacod_Visible = 1;
         edtavTfcontratada_codigo_to_Jsonclick = "";
         edtavTfcontratada_codigo_to_Visible = 1;
         edtavTfcontratada_codigo_Jsonclick = "";
         edtavTfcontratada_codigo_Visible = 1;
         edtavTfcontrato_numero_sel_Jsonclick = "";
         edtavTfcontrato_numero_sel_Visible = 1;
         edtavTfcontrato_numero_Jsonclick = "";
         edtavTfcontrato_numero_Visible = 1;
         edtavTfcontrato_codigo_to_Jsonclick = "";
         edtavTfcontrato_codigo_to_Visible = 1;
         edtavTfcontrato_codigo_Jsonclick = "";
         edtavTfcontrato_codigo_Visible = 1;
         edtavTfcontratoarquivosanexos_codigo_to_Jsonclick = "";
         edtavTfcontratoarquivosanexos_codigo_to_Visible = 1;
         edtavTfcontratoarquivosanexos_codigo_Jsonclick = "";
         edtavTfcontratoarquivosanexos_codigo_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_contratoarquivosanexos_data_Searchbuttontext = "Pesquisar";
         Ddo_contratoarquivosanexos_data_Rangefilterto = "At�";
         Ddo_contratoarquivosanexos_data_Rangefilterfrom = "Desde";
         Ddo_contratoarquivosanexos_data_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoarquivosanexos_data_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoarquivosanexos_data_Sortasc = "Ordenar de A � Z";
         Ddo_contratoarquivosanexos_data_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoarquivosanexos_data_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoarquivosanexos_data_Filtertype = "Date";
         Ddo_contratoarquivosanexos_data_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoarquivosanexos_data_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoarquivosanexos_data_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoarquivosanexos_data_Titlecontrolidtoreplace = "";
         Ddo_contratoarquivosanexos_data_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoarquivosanexos_data_Cls = "ColumnSettings";
         Ddo_contratoarquivosanexos_data_Tooltip = "Op��es";
         Ddo_contratoarquivosanexos_data_Caption = "";
         Ddo_contratoarquivosanexos_descricao_Searchbuttontext = "Pesquisar";
         Ddo_contratoarquivosanexos_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratoarquivosanexos_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoarquivosanexos_descricao_Loadingdata = "Carregando dados...";
         Ddo_contratoarquivosanexos_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoarquivosanexos_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_contratoarquivosanexos_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_contratoarquivosanexos_descricao_Datalistproc = "GetPromptContratoArquivosAnexosFilterData";
         Ddo_contratoarquivosanexos_descricao_Datalisttype = "Dynamic";
         Ddo_contratoarquivosanexos_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoarquivosanexos_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratoarquivosanexos_descricao_Filtertype = "Character";
         Ddo_contratoarquivosanexos_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoarquivosanexos_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoarquivosanexos_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoarquivosanexos_descricao_Titlecontrolidtoreplace = "";
         Ddo_contratoarquivosanexos_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoarquivosanexos_descricao_Cls = "ColumnSettings";
         Ddo_contratoarquivosanexos_descricao_Tooltip = "Op��es";
         Ddo_contratoarquivosanexos_descricao_Caption = "";
         Ddo_contratada_pessoacnpj_Searchbuttontext = "Pesquisar";
         Ddo_contratada_pessoacnpj_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratada_pessoacnpj_Cleanfilter = "Limpar pesquisa";
         Ddo_contratada_pessoacnpj_Loadingdata = "Carregando dados...";
         Ddo_contratada_pessoacnpj_Sortdsc = "Ordenar de Z � A";
         Ddo_contratada_pessoacnpj_Sortasc = "Ordenar de A � Z";
         Ddo_contratada_pessoacnpj_Datalistupdateminimumcharacters = 0;
         Ddo_contratada_pessoacnpj_Datalistproc = "GetPromptContratoArquivosAnexosFilterData";
         Ddo_contratada_pessoacnpj_Datalisttype = "Dynamic";
         Ddo_contratada_pessoacnpj_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratada_pessoacnpj_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratada_pessoacnpj_Filtertype = "Character";
         Ddo_contratada_pessoacnpj_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratada_pessoacnpj_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratada_pessoacnpj_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratada_pessoacnpj_Titlecontrolidtoreplace = "";
         Ddo_contratada_pessoacnpj_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratada_pessoacnpj_Cls = "ColumnSettings";
         Ddo_contratada_pessoacnpj_Tooltip = "Op��es";
         Ddo_contratada_pessoacnpj_Caption = "";
         Ddo_contratada_pessoanom_Searchbuttontext = "Pesquisar";
         Ddo_contratada_pessoanom_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratada_pessoanom_Cleanfilter = "Limpar pesquisa";
         Ddo_contratada_pessoanom_Loadingdata = "Carregando dados...";
         Ddo_contratada_pessoanom_Sortdsc = "Ordenar de Z � A";
         Ddo_contratada_pessoanom_Sortasc = "Ordenar de A � Z";
         Ddo_contratada_pessoanom_Datalistupdateminimumcharacters = 0;
         Ddo_contratada_pessoanom_Datalistproc = "GetPromptContratoArquivosAnexosFilterData";
         Ddo_contratada_pessoanom_Datalisttype = "Dynamic";
         Ddo_contratada_pessoanom_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratada_pessoanom_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratada_pessoanom_Filtertype = "Character";
         Ddo_contratada_pessoanom_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratada_pessoanom_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratada_pessoanom_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratada_pessoanom_Titlecontrolidtoreplace = "";
         Ddo_contratada_pessoanom_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratada_pessoanom_Cls = "ColumnSettings";
         Ddo_contratada_pessoanom_Tooltip = "Op��es";
         Ddo_contratada_pessoanom_Caption = "";
         Ddo_contratada_pessoacod_Searchbuttontext = "Pesquisar";
         Ddo_contratada_pessoacod_Rangefilterto = "At�";
         Ddo_contratada_pessoacod_Rangefilterfrom = "Desde";
         Ddo_contratada_pessoacod_Cleanfilter = "Limpar pesquisa";
         Ddo_contratada_pessoacod_Sortdsc = "Ordenar de Z � A";
         Ddo_contratada_pessoacod_Sortasc = "Ordenar de A � Z";
         Ddo_contratada_pessoacod_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratada_pessoacod_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratada_pessoacod_Filtertype = "Numeric";
         Ddo_contratada_pessoacod_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratada_pessoacod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratada_pessoacod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratada_pessoacod_Titlecontrolidtoreplace = "";
         Ddo_contratada_pessoacod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratada_pessoacod_Cls = "ColumnSettings";
         Ddo_contratada_pessoacod_Tooltip = "Op��es";
         Ddo_contratada_pessoacod_Caption = "";
         Ddo_contratada_codigo_Searchbuttontext = "Pesquisar";
         Ddo_contratada_codigo_Rangefilterto = "At�";
         Ddo_contratada_codigo_Rangefilterfrom = "Desde";
         Ddo_contratada_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_contratada_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_contratada_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_contratada_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratada_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratada_codigo_Filtertype = "Numeric";
         Ddo_contratada_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratada_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratada_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratada_codigo_Titlecontrolidtoreplace = "";
         Ddo_contratada_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratada_codigo_Cls = "ColumnSettings";
         Ddo_contratada_codigo_Tooltip = "Op��es";
         Ddo_contratada_codigo_Caption = "";
         Ddo_contrato_numero_Searchbuttontext = "Pesquisar";
         Ddo_contrato_numero_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contrato_numero_Cleanfilter = "Limpar pesquisa";
         Ddo_contrato_numero_Loadingdata = "Carregando dados...";
         Ddo_contrato_numero_Sortdsc = "Ordenar de Z � A";
         Ddo_contrato_numero_Sortasc = "Ordenar de A � Z";
         Ddo_contrato_numero_Datalistupdateminimumcharacters = 0;
         Ddo_contrato_numero_Datalistproc = "GetPromptContratoArquivosAnexosFilterData";
         Ddo_contrato_numero_Datalisttype = "Dynamic";
         Ddo_contrato_numero_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contrato_numero_Filtertype = "Character";
         Ddo_contrato_numero_Includefilter = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Titlecontrolidtoreplace = "";
         Ddo_contrato_numero_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contrato_numero_Cls = "ColumnSettings";
         Ddo_contrato_numero_Tooltip = "Op��es";
         Ddo_contrato_numero_Caption = "";
         Ddo_contrato_codigo_Searchbuttontext = "Pesquisar";
         Ddo_contrato_codigo_Rangefilterto = "At�";
         Ddo_contrato_codigo_Rangefilterfrom = "Desde";
         Ddo_contrato_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_contrato_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_contrato_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_contrato_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contrato_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contrato_codigo_Filtertype = "Numeric";
         Ddo_contrato_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_contrato_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contrato_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contrato_codigo_Titlecontrolidtoreplace = "";
         Ddo_contrato_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contrato_codigo_Cls = "ColumnSettings";
         Ddo_contrato_codigo_Tooltip = "Op��es";
         Ddo_contrato_codigo_Caption = "";
         Ddo_contratoarquivosanexos_codigo_Searchbuttontext = "Pesquisar";
         Ddo_contratoarquivosanexos_codigo_Rangefilterto = "At�";
         Ddo_contratoarquivosanexos_codigo_Rangefilterfrom = "Desde";
         Ddo_contratoarquivosanexos_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoarquivosanexos_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoarquivosanexos_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_contratoarquivosanexos_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoarquivosanexos_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoarquivosanexos_codigo_Filtertype = "Numeric";
         Ddo_contratoarquivosanexos_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoarquivosanexos_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoarquivosanexos_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoarquivosanexos_codigo_Titlecontrolidtoreplace = "";
         Ddo_contratoarquivosanexos_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoarquivosanexos_codigo_Cls = "ColumnSettings";
         Ddo_contratoarquivosanexos_codigo_Tooltip = "Op��es";
         Ddo_contratoarquivosanexos_codigo_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Contrato Arquivos Anexos";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV33ddo_ContratoArquivosAnexos_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Contratada_PessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV31TFContratoArquivosAnexos_Codigo',fld:'vTFCONTRATOARQUIVOSANEXOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoArquivosAnexos_Codigo_To',fld:'vTFCONTRATOARQUIVOSANEXOS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV40TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV43TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFContratada_PessoaCod',fld:'vTFCONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratada_PessoaCod_To',fld:'vTFCONTRATADA_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV52TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV55TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV56TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV59TFContratoArquivosAnexos_Descricao',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO',pic:'',nv:''},{av:'AV60TFContratoArquivosAnexos_Descricao_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO_SEL',pic:'',nv:''},{av:'AV63TFContratoArquivosAnexos_Data',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV64TFContratoArquivosAnexos_Data_To',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratoArquivosAnexos_Descricao1',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ContratoArquivosAnexos_Descricao2',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ContratoArquivosAnexos_Descricao3',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0}],oparms:[{av:'AV30ContratoArquivosAnexos_CodigoTitleFilterData',fld:'vCONTRATOARQUIVOSANEXOS_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV34Contrato_CodigoTitleFilterData',fld:'vCONTRATO_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV38Contrato_NumeroTitleFilterData',fld:'vCONTRATO_NUMEROTITLEFILTERDATA',pic:'',nv:null},{av:'AV42Contratada_CodigoTitleFilterData',fld:'vCONTRATADA_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV46Contratada_PessoaCodTitleFilterData',fld:'vCONTRATADA_PESSOACODTITLEFILTERDATA',pic:'',nv:null},{av:'AV50Contratada_PessoaNomTitleFilterData',fld:'vCONTRATADA_PESSOANOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV54Contratada_PessoaCNPJTitleFilterData',fld:'vCONTRATADA_PESSOACNPJTITLEFILTERDATA',pic:'',nv:null},{av:'AV58ContratoArquivosAnexos_DescricaoTitleFilterData',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV62ContratoArquivosAnexos_DataTitleFilterData',fld:'vCONTRATOARQUIVOSANEXOS_DATATITLEFILTERDATA',pic:'',nv:null},{av:'edtContratoArquivosAnexos_Codigo_Titleformat',ctrl:'CONTRATOARQUIVOSANEXOS_CODIGO',prop:'Titleformat'},{av:'edtContratoArquivosAnexos_Codigo_Title',ctrl:'CONTRATOARQUIVOSANEXOS_CODIGO',prop:'Title'},{av:'edtContrato_Codigo_Titleformat',ctrl:'CONTRATO_CODIGO',prop:'Titleformat'},{av:'edtContrato_Codigo_Title',ctrl:'CONTRATO_CODIGO',prop:'Title'},{av:'edtContrato_Numero_Titleformat',ctrl:'CONTRATO_NUMERO',prop:'Titleformat'},{av:'edtContrato_Numero_Title',ctrl:'CONTRATO_NUMERO',prop:'Title'},{av:'edtContratada_Codigo_Titleformat',ctrl:'CONTRATADA_CODIGO',prop:'Titleformat'},{av:'edtContratada_Codigo_Title',ctrl:'CONTRATADA_CODIGO',prop:'Title'},{av:'edtContratada_PessoaCod_Titleformat',ctrl:'CONTRATADA_PESSOACOD',prop:'Titleformat'},{av:'edtContratada_PessoaCod_Title',ctrl:'CONTRATADA_PESSOACOD',prop:'Title'},{av:'edtContratada_PessoaNom_Titleformat',ctrl:'CONTRATADA_PESSOANOM',prop:'Titleformat'},{av:'edtContratada_PessoaNom_Title',ctrl:'CONTRATADA_PESSOANOM',prop:'Title'},{av:'edtContratada_PessoaCNPJ_Titleformat',ctrl:'CONTRATADA_PESSOACNPJ',prop:'Titleformat'},{av:'edtContratada_PessoaCNPJ_Title',ctrl:'CONTRATADA_PESSOACNPJ',prop:'Title'},{av:'edtContratoArquivosAnexos_Descricao_Titleformat',ctrl:'CONTRATOARQUIVOSANEXOS_DESCRICAO',prop:'Titleformat'},{av:'edtContratoArquivosAnexos_Descricao_Title',ctrl:'CONTRATOARQUIVOSANEXOS_DESCRICAO',prop:'Title'},{av:'edtContratoArquivosAnexos_Data_Titleformat',ctrl:'CONTRATOARQUIVOSANEXOS_DATA',prop:'Titleformat'},{av:'edtContratoArquivosAnexos_Data_Title',ctrl:'CONTRATOARQUIVOSANEXOS_DATA',prop:'Title'},{av:'AV70GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV71GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E116M2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoArquivosAnexos_Descricao1',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoArquivosAnexos_Descricao2',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoArquivosAnexos_Descricao3',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoArquivosAnexos_Codigo',fld:'vTFCONTRATOARQUIVOSANEXOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoArquivosAnexos_Codigo_To',fld:'vTFCONTRATOARQUIVOSANEXOS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV40TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV43TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFContratada_PessoaCod',fld:'vTFCONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratada_PessoaCod_To',fld:'vTFCONTRATADA_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV52TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV55TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV56TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV59TFContratoArquivosAnexos_Descricao',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO',pic:'',nv:''},{av:'AV60TFContratoArquivosAnexos_Descricao_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO_SEL',pic:'',nv:''},{av:'AV63TFContratoArquivosAnexos_Data',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV64TFContratoArquivosAnexos_Data_To',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV33ddo_ContratoArquivosAnexos_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Contratada_PessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTRATOARQUIVOSANEXOS_CODIGO.ONOPTIONCLICKED","{handler:'E126M2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoArquivosAnexos_Descricao1',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoArquivosAnexos_Descricao2',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoArquivosAnexos_Descricao3',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoArquivosAnexos_Codigo',fld:'vTFCONTRATOARQUIVOSANEXOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoArquivosAnexos_Codigo_To',fld:'vTFCONTRATOARQUIVOSANEXOS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV40TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV43TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFContratada_PessoaCod',fld:'vTFCONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratada_PessoaCod_To',fld:'vTFCONTRATADA_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV52TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV55TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV56TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV59TFContratoArquivosAnexos_Descricao',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO',pic:'',nv:''},{av:'AV60TFContratoArquivosAnexos_Descricao_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO_SEL',pic:'',nv:''},{av:'AV63TFContratoArquivosAnexos_Data',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV64TFContratoArquivosAnexos_Data_To',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV33ddo_ContratoArquivosAnexos_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Contratada_PessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratoarquivosanexos_codigo_Activeeventkey',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_contratoarquivosanexos_codigo_Filteredtext_get',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_CODIGO',prop:'FilteredText_get'},{av:'Ddo_contratoarquivosanexos_codigo_Filteredtextto_get',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoarquivosanexos_codigo_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_CODIGO',prop:'SortedStatus'},{av:'AV31TFContratoArquivosAnexos_Codigo',fld:'vTFCONTRATOARQUIVOSANEXOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoArquivosAnexos_Codigo_To',fld:'vTFCONTRATOARQUIVOSANEXOS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratada_codigo_Sortedstatus',ctrl:'DDO_CONTRATADA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacod_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACOD',prop:'SortedStatus'},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'Ddo_contratoarquivosanexos_descricao_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratoarquivosanexos_data_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DATA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATO_CODIGO.ONOPTIONCLICKED","{handler:'E136M2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoArquivosAnexos_Descricao1',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoArquivosAnexos_Descricao2',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoArquivosAnexos_Descricao3',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoArquivosAnexos_Codigo',fld:'vTFCONTRATOARQUIVOSANEXOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoArquivosAnexos_Codigo_To',fld:'vTFCONTRATOARQUIVOSANEXOS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV40TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV43TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFContratada_PessoaCod',fld:'vTFCONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratada_PessoaCod_To',fld:'vTFCONTRATADA_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV52TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV55TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV56TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV59TFContratoArquivosAnexos_Descricao',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO',pic:'',nv:''},{av:'AV60TFContratoArquivosAnexos_Descricao_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO_SEL',pic:'',nv:''},{av:'AV63TFContratoArquivosAnexos_Data',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV64TFContratoArquivosAnexos_Data_To',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV33ddo_ContratoArquivosAnexos_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Contratada_PessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contrato_codigo_Activeeventkey',ctrl:'DDO_CONTRATO_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_contrato_codigo_Filteredtext_get',ctrl:'DDO_CONTRATO_CODIGO',prop:'FilteredText_get'},{av:'Ddo_contrato_codigo_Filteredtextto_get',ctrl:'DDO_CONTRATO_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'AV35TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoarquivosanexos_codigo_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratada_codigo_Sortedstatus',ctrl:'DDO_CONTRATADA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacod_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACOD',prop:'SortedStatus'},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'Ddo_contratoarquivosanexos_descricao_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratoarquivosanexos_data_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DATA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATO_NUMERO.ONOPTIONCLICKED","{handler:'E146M2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoArquivosAnexos_Descricao1',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoArquivosAnexos_Descricao2',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoArquivosAnexos_Descricao3',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoArquivosAnexos_Codigo',fld:'vTFCONTRATOARQUIVOSANEXOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoArquivosAnexos_Codigo_To',fld:'vTFCONTRATOARQUIVOSANEXOS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV40TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV43TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFContratada_PessoaCod',fld:'vTFCONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratada_PessoaCod_To',fld:'vTFCONTRATADA_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV52TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV55TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV56TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV59TFContratoArquivosAnexos_Descricao',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO',pic:'',nv:''},{av:'AV60TFContratoArquivosAnexos_Descricao_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO_SEL',pic:'',nv:''},{av:'AV63TFContratoArquivosAnexos_Data',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV64TFContratoArquivosAnexos_Data_To',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV33ddo_ContratoArquivosAnexos_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Contratada_PessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contrato_numero_Activeeventkey',ctrl:'DDO_CONTRATO_NUMERO',prop:'ActiveEventKey'},{av:'Ddo_contrato_numero_Filteredtext_get',ctrl:'DDO_CONTRATO_NUMERO',prop:'FilteredText_get'},{av:'Ddo_contrato_numero_Selectedvalue_get',ctrl:'DDO_CONTRATO_NUMERO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'AV39TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV40TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'Ddo_contratoarquivosanexos_codigo_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratada_codigo_Sortedstatus',ctrl:'DDO_CONTRATADA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacod_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACOD',prop:'SortedStatus'},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'Ddo_contratoarquivosanexos_descricao_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratoarquivosanexos_data_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DATA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATADA_CODIGO.ONOPTIONCLICKED","{handler:'E156M2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoArquivosAnexos_Descricao1',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoArquivosAnexos_Descricao2',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoArquivosAnexos_Descricao3',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoArquivosAnexos_Codigo',fld:'vTFCONTRATOARQUIVOSANEXOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoArquivosAnexos_Codigo_To',fld:'vTFCONTRATOARQUIVOSANEXOS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV40TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV43TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFContratada_PessoaCod',fld:'vTFCONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratada_PessoaCod_To',fld:'vTFCONTRATADA_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV52TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV55TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV56TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV59TFContratoArquivosAnexos_Descricao',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO',pic:'',nv:''},{av:'AV60TFContratoArquivosAnexos_Descricao_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO_SEL',pic:'',nv:''},{av:'AV63TFContratoArquivosAnexos_Data',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV64TFContratoArquivosAnexos_Data_To',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV33ddo_ContratoArquivosAnexos_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Contratada_PessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratada_codigo_Activeeventkey',ctrl:'DDO_CONTRATADA_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_contratada_codigo_Filteredtext_get',ctrl:'DDO_CONTRATADA_CODIGO',prop:'FilteredText_get'},{av:'Ddo_contratada_codigo_Filteredtextto_get',ctrl:'DDO_CONTRATADA_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratada_codigo_Sortedstatus',ctrl:'DDO_CONTRATADA_CODIGO',prop:'SortedStatus'},{av:'AV43TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoarquivosanexos_codigo_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacod_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACOD',prop:'SortedStatus'},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'Ddo_contratoarquivosanexos_descricao_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratoarquivosanexos_data_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DATA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATADA_PESSOACOD.ONOPTIONCLICKED","{handler:'E166M2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoArquivosAnexos_Descricao1',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoArquivosAnexos_Descricao2',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoArquivosAnexos_Descricao3',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoArquivosAnexos_Codigo',fld:'vTFCONTRATOARQUIVOSANEXOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoArquivosAnexos_Codigo_To',fld:'vTFCONTRATOARQUIVOSANEXOS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV40TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV43TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFContratada_PessoaCod',fld:'vTFCONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratada_PessoaCod_To',fld:'vTFCONTRATADA_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV52TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV55TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV56TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV59TFContratoArquivosAnexos_Descricao',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO',pic:'',nv:''},{av:'AV60TFContratoArquivosAnexos_Descricao_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO_SEL',pic:'',nv:''},{av:'AV63TFContratoArquivosAnexos_Data',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV64TFContratoArquivosAnexos_Data_To',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV33ddo_ContratoArquivosAnexos_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Contratada_PessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratada_pessoacod_Activeeventkey',ctrl:'DDO_CONTRATADA_PESSOACOD',prop:'ActiveEventKey'},{av:'Ddo_contratada_pessoacod_Filteredtext_get',ctrl:'DDO_CONTRATADA_PESSOACOD',prop:'FilteredText_get'},{av:'Ddo_contratada_pessoacod_Filteredtextto_get',ctrl:'DDO_CONTRATADA_PESSOACOD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratada_pessoacod_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACOD',prop:'SortedStatus'},{av:'AV47TFContratada_PessoaCod',fld:'vTFCONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratada_PessoaCod_To',fld:'vTFCONTRATADA_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoarquivosanexos_codigo_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratada_codigo_Sortedstatus',ctrl:'DDO_CONTRATADA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'Ddo_contratoarquivosanexos_descricao_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratoarquivosanexos_data_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DATA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATADA_PESSOANOM.ONOPTIONCLICKED","{handler:'E176M2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoArquivosAnexos_Descricao1',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoArquivosAnexos_Descricao2',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoArquivosAnexos_Descricao3',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoArquivosAnexos_Codigo',fld:'vTFCONTRATOARQUIVOSANEXOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoArquivosAnexos_Codigo_To',fld:'vTFCONTRATOARQUIVOSANEXOS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV40TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV43TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFContratada_PessoaCod',fld:'vTFCONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratada_PessoaCod_To',fld:'vTFCONTRATADA_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV52TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV55TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV56TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV59TFContratoArquivosAnexos_Descricao',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO',pic:'',nv:''},{av:'AV60TFContratoArquivosAnexos_Descricao_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO_SEL',pic:'',nv:''},{av:'AV63TFContratoArquivosAnexos_Data',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV64TFContratoArquivosAnexos_Data_To',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV33ddo_ContratoArquivosAnexos_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Contratada_PessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratada_pessoanom_Activeeventkey',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'ActiveEventKey'},{av:'Ddo_contratada_pessoanom_Filteredtext_get',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'FilteredText_get'},{av:'Ddo_contratada_pessoanom_Selectedvalue_get',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'AV51TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV52TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'Ddo_contratoarquivosanexos_codigo_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratada_codigo_Sortedstatus',ctrl:'DDO_CONTRATADA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacod_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACOD',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'Ddo_contratoarquivosanexos_descricao_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratoarquivosanexos_data_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DATA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATADA_PESSOACNPJ.ONOPTIONCLICKED","{handler:'E186M2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoArquivosAnexos_Descricao1',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoArquivosAnexos_Descricao2',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoArquivosAnexos_Descricao3',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoArquivosAnexos_Codigo',fld:'vTFCONTRATOARQUIVOSANEXOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoArquivosAnexos_Codigo_To',fld:'vTFCONTRATOARQUIVOSANEXOS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV40TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV43TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFContratada_PessoaCod',fld:'vTFCONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratada_PessoaCod_To',fld:'vTFCONTRATADA_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV52TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV55TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV56TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV59TFContratoArquivosAnexos_Descricao',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO',pic:'',nv:''},{av:'AV60TFContratoArquivosAnexos_Descricao_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO_SEL',pic:'',nv:''},{av:'AV63TFContratoArquivosAnexos_Data',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV64TFContratoArquivosAnexos_Data_To',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV33ddo_ContratoArquivosAnexos_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Contratada_PessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratada_pessoacnpj_Activeeventkey',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'ActiveEventKey'},{av:'Ddo_contratada_pessoacnpj_Filteredtext_get',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'FilteredText_get'},{av:'Ddo_contratada_pessoacnpj_Selectedvalue_get',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'AV55TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV56TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'Ddo_contratoarquivosanexos_codigo_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratada_codigo_Sortedstatus',ctrl:'DDO_CONTRATADA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacod_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACOD',prop:'SortedStatus'},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratoarquivosanexos_descricao_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratoarquivosanexos_data_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DATA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO.ONOPTIONCLICKED","{handler:'E196M2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoArquivosAnexos_Descricao1',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoArquivosAnexos_Descricao2',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoArquivosAnexos_Descricao3',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoArquivosAnexos_Codigo',fld:'vTFCONTRATOARQUIVOSANEXOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoArquivosAnexos_Codigo_To',fld:'vTFCONTRATOARQUIVOSANEXOS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV40TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV43TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFContratada_PessoaCod',fld:'vTFCONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratada_PessoaCod_To',fld:'vTFCONTRATADA_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV52TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV55TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV56TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV59TFContratoArquivosAnexos_Descricao',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO',pic:'',nv:''},{av:'AV60TFContratoArquivosAnexos_Descricao_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO_SEL',pic:'',nv:''},{av:'AV63TFContratoArquivosAnexos_Data',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV64TFContratoArquivosAnexos_Data_To',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV33ddo_ContratoArquivosAnexos_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Contratada_PessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratoarquivosanexos_descricao_Activeeventkey',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_contratoarquivosanexos_descricao_Filteredtext_get',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_contratoarquivosanexos_descricao_Selectedvalue_get',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoarquivosanexos_descricao_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO',prop:'SortedStatus'},{av:'AV59TFContratoArquivosAnexos_Descricao',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO',pic:'',nv:''},{av:'AV60TFContratoArquivosAnexos_Descricao_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO_SEL',pic:'',nv:''},{av:'Ddo_contratoarquivosanexos_codigo_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratada_codigo_Sortedstatus',ctrl:'DDO_CONTRATADA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacod_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACOD',prop:'SortedStatus'},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'Ddo_contratoarquivosanexos_data_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DATA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOARQUIVOSANEXOS_DATA.ONOPTIONCLICKED","{handler:'E206M2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoArquivosAnexos_Descricao1',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoArquivosAnexos_Descricao2',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoArquivosAnexos_Descricao3',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoArquivosAnexos_Codigo',fld:'vTFCONTRATOARQUIVOSANEXOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoArquivosAnexos_Codigo_To',fld:'vTFCONTRATOARQUIVOSANEXOS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV40TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV43TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFContratada_PessoaCod',fld:'vTFCONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratada_PessoaCod_To',fld:'vTFCONTRATADA_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV52TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV55TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV56TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV59TFContratoArquivosAnexos_Descricao',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO',pic:'',nv:''},{av:'AV60TFContratoArquivosAnexos_Descricao_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO_SEL',pic:'',nv:''},{av:'AV63TFContratoArquivosAnexos_Data',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV64TFContratoArquivosAnexos_Data_To',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV33ddo_ContratoArquivosAnexos_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Contratada_PessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratoarquivosanexos_data_Activeeventkey',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DATA',prop:'ActiveEventKey'},{av:'Ddo_contratoarquivosanexos_data_Filteredtext_get',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DATA',prop:'FilteredText_get'},{av:'Ddo_contratoarquivosanexos_data_Filteredtextto_get',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DATA',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoarquivosanexos_data_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DATA',prop:'SortedStatus'},{av:'AV63TFContratoArquivosAnexos_Data',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV64TFContratoArquivosAnexos_Data_To',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_contratoarquivosanexos_codigo_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratada_codigo_Sortedstatus',ctrl:'DDO_CONTRATADA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacod_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACOD',prop:'SortedStatus'},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'Ddo_contratoarquivosanexos_descricao_Sortedstatus',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E336M2',iparms:[],oparms:[{av:'AV28Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E346M2',iparms:[{av:'A108ContratoArquivosAnexos_Codigo',fld:'CONTRATOARQUIVOSANEXOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A110ContratoArquivosAnexos_Descricao',fld:'CONTRATOARQUIVOSANEXOS_DESCRICAO',pic:'',hsh:true,nv:''}],oparms:[{av:'AV7InOutContratoArquivosAnexos_Codigo',fld:'vINOUTCONTRATOARQUIVOSANEXOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8InOutContratoArquivosAnexos_Descricao',fld:'vINOUTCONTRATOARQUIVOSANEXOS_DESCRICAO',pic:'',nv:''}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E216M2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoArquivosAnexos_Descricao1',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoArquivosAnexos_Descricao2',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoArquivosAnexos_Descricao3',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoArquivosAnexos_Codigo',fld:'vTFCONTRATOARQUIVOSANEXOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoArquivosAnexos_Codigo_To',fld:'vTFCONTRATOARQUIVOSANEXOS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV40TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV43TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFContratada_PessoaCod',fld:'vTFCONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratada_PessoaCod_To',fld:'vTFCONTRATADA_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV52TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV55TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV56TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV59TFContratoArquivosAnexos_Descricao',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO',pic:'',nv:''},{av:'AV60TFContratoArquivosAnexos_Descricao_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO_SEL',pic:'',nv:''},{av:'AV63TFContratoArquivosAnexos_Data',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV64TFContratoArquivosAnexos_Data_To',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV33ddo_ContratoArquivosAnexos_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Contratada_PessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E266M2',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E226M2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoArquivosAnexos_Descricao1',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoArquivosAnexos_Descricao2',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoArquivosAnexos_Descricao3',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoArquivosAnexos_Codigo',fld:'vTFCONTRATOARQUIVOSANEXOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoArquivosAnexos_Codigo_To',fld:'vTFCONTRATOARQUIVOSANEXOS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV40TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV43TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFContratada_PessoaCod',fld:'vTFCONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratada_PessoaCod_To',fld:'vTFCONTRATADA_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV52TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV55TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV56TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV59TFContratoArquivosAnexos_Descricao',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO',pic:'',nv:''},{av:'AV60TFContratoArquivosAnexos_Descricao_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO_SEL',pic:'',nv:''},{av:'AV63TFContratoArquivosAnexos_Data',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV64TFContratoArquivosAnexos_Data_To',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV33ddo_ContratoArquivosAnexos_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Contratada_PessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoArquivosAnexos_Descricao2',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoArquivosAnexos_Descricao3',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoArquivosAnexos_Descricao1',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavContratoarquivosanexos_descricao2_Visible',ctrl:'vCONTRATOARQUIVOSANEXOS_DESCRICAO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContratoarquivosanexos_descricao3_Visible',ctrl:'vCONTRATOARQUIVOSANEXOS_DESCRICAO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContratoarquivosanexos_descricao1_Visible',ctrl:'vCONTRATOARQUIVOSANEXOS_DESCRICAO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E276M2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavContratoarquivosanexos_descricao1_Visible',ctrl:'vCONTRATOARQUIVOSANEXOS_DESCRICAO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E286M2',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E236M2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoArquivosAnexos_Descricao1',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoArquivosAnexos_Descricao2',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoArquivosAnexos_Descricao3',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoArquivosAnexos_Codigo',fld:'vTFCONTRATOARQUIVOSANEXOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoArquivosAnexos_Codigo_To',fld:'vTFCONTRATOARQUIVOSANEXOS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV40TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV43TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFContratada_PessoaCod',fld:'vTFCONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratada_PessoaCod_To',fld:'vTFCONTRATADA_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV52TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV55TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV56TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV59TFContratoArquivosAnexos_Descricao',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO',pic:'',nv:''},{av:'AV60TFContratoArquivosAnexos_Descricao_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO_SEL',pic:'',nv:''},{av:'AV63TFContratoArquivosAnexos_Data',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV64TFContratoArquivosAnexos_Data_To',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV33ddo_ContratoArquivosAnexos_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Contratada_PessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoArquivosAnexos_Descricao2',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoArquivosAnexos_Descricao3',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoArquivosAnexos_Descricao1',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavContratoarquivosanexos_descricao2_Visible',ctrl:'vCONTRATOARQUIVOSANEXOS_DESCRICAO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContratoarquivosanexos_descricao3_Visible',ctrl:'vCONTRATOARQUIVOSANEXOS_DESCRICAO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContratoarquivosanexos_descricao1_Visible',ctrl:'vCONTRATOARQUIVOSANEXOS_DESCRICAO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E296M2',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavContratoarquivosanexos_descricao2_Visible',ctrl:'vCONTRATOARQUIVOSANEXOS_DESCRICAO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E246M2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoArquivosAnexos_Descricao1',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoArquivosAnexos_Descricao2',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoArquivosAnexos_Descricao3',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoArquivosAnexos_Codigo',fld:'vTFCONTRATOARQUIVOSANEXOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoArquivosAnexos_Codigo_To',fld:'vTFCONTRATOARQUIVOSANEXOS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV40TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV43TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFContratada_PessoaCod',fld:'vTFCONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratada_PessoaCod_To',fld:'vTFCONTRATADA_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV52TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV55TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV56TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV59TFContratoArquivosAnexos_Descricao',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO',pic:'',nv:''},{av:'AV60TFContratoArquivosAnexos_Descricao_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO_SEL',pic:'',nv:''},{av:'AV63TFContratoArquivosAnexos_Data',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV64TFContratoArquivosAnexos_Data_To',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV33ddo_ContratoArquivosAnexos_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Contratada_PessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoArquivosAnexos_Descricao2',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoArquivosAnexos_Descricao3',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoArquivosAnexos_Descricao1',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavContratoarquivosanexos_descricao2_Visible',ctrl:'vCONTRATOARQUIVOSANEXOS_DESCRICAO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContratoarquivosanexos_descricao3_Visible',ctrl:'vCONTRATOARQUIVOSANEXOS_DESCRICAO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContratoarquivosanexos_descricao1_Visible',ctrl:'vCONTRATOARQUIVOSANEXOS_DESCRICAO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E306M2',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavContratoarquivosanexos_descricao3_Visible',ctrl:'vCONTRATOARQUIVOSANEXOS_DESCRICAO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E256M2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoArquivosAnexos_Descricao1',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoArquivosAnexos_Descricao2',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoArquivosAnexos_Descricao3',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContratoArquivosAnexos_Codigo',fld:'vTFCONTRATOARQUIVOSANEXOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContratoArquivosAnexos_Codigo_To',fld:'vTFCONTRATOARQUIVOSANEXOS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV40TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV43TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFContratada_PessoaCod',fld:'vTFCONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'AV48TFContratada_PessoaCod_To',fld:'vTFCONTRATADA_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV51TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV52TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV55TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV56TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV59TFContratoArquivosAnexos_Descricao',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO',pic:'',nv:''},{av:'AV60TFContratoArquivosAnexos_Descricao_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO_SEL',pic:'',nv:''},{av:'AV63TFContratoArquivosAnexos_Data',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV64TFContratoArquivosAnexos_Data_To',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV33ddo_ContratoArquivosAnexos_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_Contratada_PessoaCodTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOARQUIVOSANEXOS_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV31TFContratoArquivosAnexos_Codigo',fld:'vTFCONTRATOARQUIVOSANEXOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoarquivosanexos_codigo_Filteredtext_set',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_CODIGO',prop:'FilteredText_set'},{av:'AV32TFContratoArquivosAnexos_Codigo_To',fld:'vTFCONTRATOARQUIVOSANEXOS_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoarquivosanexos_codigo_Filteredtextto_set',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_CODIGO',prop:'FilteredTextTo_set'},{av:'AV35TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contrato_codigo_Filteredtext_set',ctrl:'DDO_CONTRATO_CODIGO',prop:'FilteredText_set'},{av:'AV36TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contrato_codigo_Filteredtextto_set',ctrl:'DDO_CONTRATO_CODIGO',prop:'FilteredTextTo_set'},{av:'AV39TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'Ddo_contrato_numero_Filteredtext_set',ctrl:'DDO_CONTRATO_NUMERO',prop:'FilteredText_set'},{av:'AV40TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'Ddo_contrato_numero_Selectedvalue_set',ctrl:'DDO_CONTRATO_NUMERO',prop:'SelectedValue_set'},{av:'AV43TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratada_codigo_Filteredtext_set',ctrl:'DDO_CONTRATADA_CODIGO',prop:'FilteredText_set'},{av:'AV44TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratada_codigo_Filteredtextto_set',ctrl:'DDO_CONTRATADA_CODIGO',prop:'FilteredTextTo_set'},{av:'AV47TFContratada_PessoaCod',fld:'vTFCONTRATADA_PESSOACOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratada_pessoacod_Filteredtext_set',ctrl:'DDO_CONTRATADA_PESSOACOD',prop:'FilteredText_set'},{av:'AV48TFContratada_PessoaCod_To',fld:'vTFCONTRATADA_PESSOACOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratada_pessoacod_Filteredtextto_set',ctrl:'DDO_CONTRATADA_PESSOACOD',prop:'FilteredTextTo_set'},{av:'AV51TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'Ddo_contratada_pessoanom_Filteredtext_set',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'FilteredText_set'},{av:'AV52TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'Ddo_contratada_pessoanom_Selectedvalue_set',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SelectedValue_set'},{av:'AV55TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'Ddo_contratada_pessoacnpj_Filteredtext_set',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'FilteredText_set'},{av:'AV56TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'Ddo_contratada_pessoacnpj_Selectedvalue_set',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SelectedValue_set'},{av:'AV59TFContratoArquivosAnexos_Descricao',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO',pic:'',nv:''},{av:'Ddo_contratoarquivosanexos_descricao_Filteredtext_set',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO',prop:'FilteredText_set'},{av:'AV60TFContratoArquivosAnexos_Descricao_Sel',fld:'vTFCONTRATOARQUIVOSANEXOS_DESCRICAO_SEL',pic:'',nv:''},{av:'Ddo_contratoarquivosanexos_descricao_Selectedvalue_set',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DESCRICAO',prop:'SelectedValue_set'},{av:'AV63TFContratoArquivosAnexos_Data',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA',pic:'99/99/99 99:99',nv:''},{av:'Ddo_contratoarquivosanexos_data_Filteredtext_set',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DATA',prop:'FilteredText_set'},{av:'AV64TFContratoArquivosAnexos_Data_To',fld:'vTFCONTRATOARQUIVOSANEXOS_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_contratoarquivosanexos_data_Filteredtextto_set',ctrl:'DDO_CONTRATOARQUIVOSANEXOS_DATA',prop:'FilteredTextTo_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoArquivosAnexos_Descricao1',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO1',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavContratoarquivosanexos_descricao1_Visible',ctrl:'vCONTRATOARQUIVOSANEXOS_DESCRICAO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoArquivosAnexos_Descricao2',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoArquivosAnexos_Descricao3',fld:'vCONTRATOARQUIVOSANEXOS_DESCRICAO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavContratoarquivosanexos_descricao2_Visible',ctrl:'vCONTRATOARQUIVOSANEXOS_DESCRICAO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContratoarquivosanexos_descricao3_Visible',ctrl:'vCONTRATOARQUIVOSANEXOS_DESCRICAO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV8InOutContratoArquivosAnexos_Descricao = "";
         Gridpaginationbar_Selectedpage = "";
         Ddo_contratoarquivosanexos_codigo_Activeeventkey = "";
         Ddo_contratoarquivosanexos_codigo_Filteredtext_get = "";
         Ddo_contratoarquivosanexos_codigo_Filteredtextto_get = "";
         Ddo_contrato_codigo_Activeeventkey = "";
         Ddo_contrato_codigo_Filteredtext_get = "";
         Ddo_contrato_codigo_Filteredtextto_get = "";
         Ddo_contrato_numero_Activeeventkey = "";
         Ddo_contrato_numero_Filteredtext_get = "";
         Ddo_contrato_numero_Selectedvalue_get = "";
         Ddo_contratada_codigo_Activeeventkey = "";
         Ddo_contratada_codigo_Filteredtext_get = "";
         Ddo_contratada_codigo_Filteredtextto_get = "";
         Ddo_contratada_pessoacod_Activeeventkey = "";
         Ddo_contratada_pessoacod_Filteredtext_get = "";
         Ddo_contratada_pessoacod_Filteredtextto_get = "";
         Ddo_contratada_pessoanom_Activeeventkey = "";
         Ddo_contratada_pessoanom_Filteredtext_get = "";
         Ddo_contratada_pessoanom_Selectedvalue_get = "";
         Ddo_contratada_pessoacnpj_Activeeventkey = "";
         Ddo_contratada_pessoacnpj_Filteredtext_get = "";
         Ddo_contratada_pessoacnpj_Selectedvalue_get = "";
         Ddo_contratoarquivosanexos_descricao_Activeeventkey = "";
         Ddo_contratoarquivosanexos_descricao_Filteredtext_get = "";
         Ddo_contratoarquivosanexos_descricao_Selectedvalue_get = "";
         Ddo_contratoarquivosanexos_data_Activeeventkey = "";
         Ddo_contratoarquivosanexos_data_Filteredtext_get = "";
         Ddo_contratoarquivosanexos_data_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17ContratoArquivosAnexos_Descricao1 = "";
         AV19DynamicFiltersSelector2 = "";
         AV21ContratoArquivosAnexos_Descricao2 = "";
         AV23DynamicFiltersSelector3 = "";
         AV25ContratoArquivosAnexos_Descricao3 = "";
         AV39TFContrato_Numero = "";
         AV40TFContrato_Numero_Sel = "";
         AV51TFContratada_PessoaNom = "";
         AV52TFContratada_PessoaNom_Sel = "";
         AV55TFContratada_PessoaCNPJ = "";
         AV56TFContratada_PessoaCNPJ_Sel = "";
         AV59TFContratoArquivosAnexos_Descricao = "";
         AV60TFContratoArquivosAnexos_Descricao_Sel = "";
         AV63TFContratoArquivosAnexos_Data = (DateTime)(DateTime.MinValue);
         AV64TFContratoArquivosAnexos_Data_To = (DateTime)(DateTime.MinValue);
         AV33ddo_ContratoArquivosAnexos_CodigoTitleControlIdToReplace = "";
         AV37ddo_Contrato_CodigoTitleControlIdToReplace = "";
         AV41ddo_Contrato_NumeroTitleControlIdToReplace = "";
         AV45ddo_Contratada_CodigoTitleControlIdToReplace = "";
         AV49ddo_Contratada_PessoaCodTitleControlIdToReplace = "";
         AV53ddo_Contratada_PessoaNomTitleControlIdToReplace = "";
         AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace = "";
         AV61ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace = "";
         AV67ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace = "";
         AV78Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV68DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV30ContratoArquivosAnexos_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV34Contrato_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38Contrato_NumeroTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV42Contratada_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV46Contratada_PessoaCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV50Contratada_PessoaNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV54Contratada_PessoaCNPJTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV58ContratoArquivosAnexos_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV62ContratoArquivosAnexos_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_contratoarquivosanexos_codigo_Filteredtext_set = "";
         Ddo_contratoarquivosanexos_codigo_Filteredtextto_set = "";
         Ddo_contratoarquivosanexos_codigo_Sortedstatus = "";
         Ddo_contrato_codigo_Filteredtext_set = "";
         Ddo_contrato_codigo_Filteredtextto_set = "";
         Ddo_contrato_codigo_Sortedstatus = "";
         Ddo_contrato_numero_Filteredtext_set = "";
         Ddo_contrato_numero_Selectedvalue_set = "";
         Ddo_contrato_numero_Sortedstatus = "";
         Ddo_contratada_codigo_Filteredtext_set = "";
         Ddo_contratada_codigo_Filteredtextto_set = "";
         Ddo_contratada_codigo_Sortedstatus = "";
         Ddo_contratada_pessoacod_Filteredtext_set = "";
         Ddo_contratada_pessoacod_Filteredtextto_set = "";
         Ddo_contratada_pessoacod_Sortedstatus = "";
         Ddo_contratada_pessoanom_Filteredtext_set = "";
         Ddo_contratada_pessoanom_Selectedvalue_set = "";
         Ddo_contratada_pessoanom_Sortedstatus = "";
         Ddo_contratada_pessoacnpj_Filteredtext_set = "";
         Ddo_contratada_pessoacnpj_Selectedvalue_set = "";
         Ddo_contratada_pessoacnpj_Sortedstatus = "";
         Ddo_contratoarquivosanexos_descricao_Filteredtext_set = "";
         Ddo_contratoarquivosanexos_descricao_Selectedvalue_set = "";
         Ddo_contratoarquivosanexos_descricao_Sortedstatus = "";
         Ddo_contratoarquivosanexos_data_Filteredtext_set = "";
         Ddo_contratoarquivosanexos_data_Filteredtextto_set = "";
         Ddo_contratoarquivosanexos_data_Sortedstatus = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV65DDO_ContratoArquivosAnexos_DataAuxDate = DateTime.MinValue;
         AV66DDO_ContratoArquivosAnexos_DataAuxDateTo = DateTime.MinValue;
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Select = "";
         AV77Select_GXI = "";
         A77Contrato_Numero = "";
         A41Contratada_PessoaNom = "";
         A42Contratada_PessoaCNPJ = "";
         A110ContratoArquivosAnexos_Descricao = "";
         A111ContratoArquivosAnexos_Arquivo = "";
         A113ContratoArquivosAnexos_Data = (DateTime)(DateTime.MinValue);
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV17ContratoArquivosAnexos_Descricao1 = "";
         lV21ContratoArquivosAnexos_Descricao2 = "";
         lV25ContratoArquivosAnexos_Descricao3 = "";
         lV39TFContrato_Numero = "";
         lV51TFContratada_PessoaNom = "";
         lV55TFContratada_PessoaCNPJ = "";
         lV59TFContratoArquivosAnexos_Descricao = "";
         H006M2_A112ContratoArquivosAnexos_NomeArq = new String[] {""} ;
         H006M2_A109ContratoArquivosAnexos_TipoArq = new String[] {""} ;
         H006M2_A113ContratoArquivosAnexos_Data = new DateTime[] {DateTime.MinValue} ;
         H006M2_A110ContratoArquivosAnexos_Descricao = new String[] {""} ;
         H006M2_A42Contratada_PessoaCNPJ = new String[] {""} ;
         H006M2_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         H006M2_A41Contratada_PessoaNom = new String[] {""} ;
         H006M2_n41Contratada_PessoaNom = new bool[] {false} ;
         H006M2_A40Contratada_PessoaCod = new int[1] ;
         H006M2_A39Contratada_Codigo = new int[1] ;
         H006M2_A77Contrato_Numero = new String[] {""} ;
         H006M2_A74Contrato_Codigo = new int[1] ;
         H006M2_A108ContratoArquivosAnexos_Codigo = new int[1] ;
         H006M2_A111ContratoArquivosAnexos_Arquivo = new String[] {""} ;
         A112ContratoArquivosAnexos_NomeArq = "";
         edtContratoArquivosAnexos_Arquivo_Filename = "";
         A109ContratoArquivosAnexos_TipoArq = "";
         H006M3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         gxblobfileaux = new GxFile(context.GetPhysicalPath());
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptcontratoarquivosanexos__default(),
            new Object[][] {
                new Object[] {
               H006M2_A112ContratoArquivosAnexos_NomeArq, H006M2_A109ContratoArquivosAnexos_TipoArq, H006M2_A113ContratoArquivosAnexos_Data, H006M2_A110ContratoArquivosAnexos_Descricao, H006M2_A42Contratada_PessoaCNPJ, H006M2_n42Contratada_PessoaCNPJ, H006M2_A41Contratada_PessoaNom, H006M2_n41Contratada_PessoaNom, H006M2_A40Contratada_PessoaCod, H006M2_A39Contratada_Codigo,
               H006M2_A77Contrato_Numero, H006M2_A74Contrato_Codigo, H006M2_A108ContratoArquivosAnexos_Codigo, H006M2_A111ContratoArquivosAnexos_Arquivo
               }
               , new Object[] {
               H006M3_AGRID_nRecordCount
               }
            }
         );
         AV78Pgmname = "PromptContratoArquivosAnexos";
         /* GeneXus formulas. */
         AV78Pgmname = "PromptContratoArquivosAnexos";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_80 ;
      private short nGXsfl_80_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV20DynamicFiltersOperator2 ;
      private short AV24DynamicFiltersOperator3 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_80_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContratoArquivosAnexos_Codigo_Titleformat ;
      private short edtContrato_Codigo_Titleformat ;
      private short edtContrato_Numero_Titleformat ;
      private short edtContratada_Codigo_Titleformat ;
      private short edtContratada_PessoaCod_Titleformat ;
      private short edtContratada_PessoaNom_Titleformat ;
      private short edtContratada_PessoaCNPJ_Titleformat ;
      private short edtContratoArquivosAnexos_Descricao_Titleformat ;
      private short edtContratoArquivosAnexos_Data_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutContratoArquivosAnexos_Codigo ;
      private int wcpOAV7InOutContratoArquivosAnexos_Codigo ;
      private int subGrid_Rows ;
      private int AV31TFContratoArquivosAnexos_Codigo ;
      private int AV32TFContratoArquivosAnexos_Codigo_To ;
      private int AV35TFContrato_Codigo ;
      private int AV36TFContrato_Codigo_To ;
      private int AV43TFContratada_Codigo ;
      private int AV44TFContratada_Codigo_To ;
      private int AV47TFContratada_PessoaCod ;
      private int AV48TFContratada_PessoaCod_To ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_contrato_numero_Datalistupdateminimumcharacters ;
      private int Ddo_contratada_pessoanom_Datalistupdateminimumcharacters ;
      private int Ddo_contratada_pessoacnpj_Datalistupdateminimumcharacters ;
      private int Ddo_contratoarquivosanexos_descricao_Datalistupdateminimumcharacters ;
      private int edtavTfcontratoarquivosanexos_codigo_Visible ;
      private int edtavTfcontratoarquivosanexos_codigo_to_Visible ;
      private int edtavTfcontrato_codigo_Visible ;
      private int edtavTfcontrato_codigo_to_Visible ;
      private int edtavTfcontrato_numero_Visible ;
      private int edtavTfcontrato_numero_sel_Visible ;
      private int edtavTfcontratada_codigo_Visible ;
      private int edtavTfcontratada_codigo_to_Visible ;
      private int edtavTfcontratada_pessoacod_Visible ;
      private int edtavTfcontratada_pessoacod_to_Visible ;
      private int edtavTfcontratada_pessoanom_Visible ;
      private int edtavTfcontratada_pessoanom_sel_Visible ;
      private int edtavTfcontratada_pessoacnpj_Visible ;
      private int edtavTfcontratada_pessoacnpj_sel_Visible ;
      private int edtavTfcontratoarquivosanexos_descricao_Visible ;
      private int edtavTfcontratoarquivosanexos_descricao_sel_Visible ;
      private int edtavTfcontratoarquivosanexos_data_Visible ;
      private int edtavTfcontratoarquivosanexos_data_to_Visible ;
      private int edtavDdo_contratoarquivosanexos_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contrato_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratada_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratada_pessoacodtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoarquivosanexos_descricaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoarquivosanexos_datatitlecontrolidtoreplace_Visible ;
      private int A108ContratoArquivosAnexos_Codigo ;
      private int A74Contrato_Codigo ;
      private int A39Contratada_Codigo ;
      private int A40Contratada_PessoaCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV69PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavContratoarquivosanexos_descricao1_Visible ;
      private int edtavContratoarquivosanexos_descricao2_Visible ;
      private int edtavContratoarquivosanexos_descricao3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV70GridCurrentPage ;
      private long AV71GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contratoarquivosanexos_codigo_Activeeventkey ;
      private String Ddo_contratoarquivosanexos_codigo_Filteredtext_get ;
      private String Ddo_contratoarquivosanexos_codigo_Filteredtextto_get ;
      private String Ddo_contrato_codigo_Activeeventkey ;
      private String Ddo_contrato_codigo_Filteredtext_get ;
      private String Ddo_contrato_codigo_Filteredtextto_get ;
      private String Ddo_contrato_numero_Activeeventkey ;
      private String Ddo_contrato_numero_Filteredtext_get ;
      private String Ddo_contrato_numero_Selectedvalue_get ;
      private String Ddo_contratada_codigo_Activeeventkey ;
      private String Ddo_contratada_codigo_Filteredtext_get ;
      private String Ddo_contratada_codigo_Filteredtextto_get ;
      private String Ddo_contratada_pessoacod_Activeeventkey ;
      private String Ddo_contratada_pessoacod_Filteredtext_get ;
      private String Ddo_contratada_pessoacod_Filteredtextto_get ;
      private String Ddo_contratada_pessoanom_Activeeventkey ;
      private String Ddo_contratada_pessoanom_Filteredtext_get ;
      private String Ddo_contratada_pessoanom_Selectedvalue_get ;
      private String Ddo_contratada_pessoacnpj_Activeeventkey ;
      private String Ddo_contratada_pessoacnpj_Filteredtext_get ;
      private String Ddo_contratada_pessoacnpj_Selectedvalue_get ;
      private String Ddo_contratoarquivosanexos_descricao_Activeeventkey ;
      private String Ddo_contratoarquivosanexos_descricao_Filteredtext_get ;
      private String Ddo_contratoarquivosanexos_descricao_Selectedvalue_get ;
      private String Ddo_contratoarquivosanexos_data_Activeeventkey ;
      private String Ddo_contratoarquivosanexos_data_Filteredtext_get ;
      private String Ddo_contratoarquivosanexos_data_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_80_idx="0001" ;
      private String AV39TFContrato_Numero ;
      private String AV40TFContrato_Numero_Sel ;
      private String AV51TFContratada_PessoaNom ;
      private String AV52TFContratada_PessoaNom_Sel ;
      private String AV78Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_contratoarquivosanexos_codigo_Caption ;
      private String Ddo_contratoarquivosanexos_codigo_Tooltip ;
      private String Ddo_contratoarquivosanexos_codigo_Cls ;
      private String Ddo_contratoarquivosanexos_codigo_Filteredtext_set ;
      private String Ddo_contratoarquivosanexos_codigo_Filteredtextto_set ;
      private String Ddo_contratoarquivosanexos_codigo_Dropdownoptionstype ;
      private String Ddo_contratoarquivosanexos_codigo_Titlecontrolidtoreplace ;
      private String Ddo_contratoarquivosanexos_codigo_Sortedstatus ;
      private String Ddo_contratoarquivosanexos_codigo_Filtertype ;
      private String Ddo_contratoarquivosanexos_codigo_Sortasc ;
      private String Ddo_contratoarquivosanexos_codigo_Sortdsc ;
      private String Ddo_contratoarquivosanexos_codigo_Cleanfilter ;
      private String Ddo_contratoarquivosanexos_codigo_Rangefilterfrom ;
      private String Ddo_contratoarquivosanexos_codigo_Rangefilterto ;
      private String Ddo_contratoarquivosanexos_codigo_Searchbuttontext ;
      private String Ddo_contrato_codigo_Caption ;
      private String Ddo_contrato_codigo_Tooltip ;
      private String Ddo_contrato_codigo_Cls ;
      private String Ddo_contrato_codigo_Filteredtext_set ;
      private String Ddo_contrato_codigo_Filteredtextto_set ;
      private String Ddo_contrato_codigo_Dropdownoptionstype ;
      private String Ddo_contrato_codigo_Titlecontrolidtoreplace ;
      private String Ddo_contrato_codigo_Sortedstatus ;
      private String Ddo_contrato_codigo_Filtertype ;
      private String Ddo_contrato_codigo_Sortasc ;
      private String Ddo_contrato_codigo_Sortdsc ;
      private String Ddo_contrato_codigo_Cleanfilter ;
      private String Ddo_contrato_codigo_Rangefilterfrom ;
      private String Ddo_contrato_codigo_Rangefilterto ;
      private String Ddo_contrato_codigo_Searchbuttontext ;
      private String Ddo_contrato_numero_Caption ;
      private String Ddo_contrato_numero_Tooltip ;
      private String Ddo_contrato_numero_Cls ;
      private String Ddo_contrato_numero_Filteredtext_set ;
      private String Ddo_contrato_numero_Selectedvalue_set ;
      private String Ddo_contrato_numero_Dropdownoptionstype ;
      private String Ddo_contrato_numero_Titlecontrolidtoreplace ;
      private String Ddo_contrato_numero_Sortedstatus ;
      private String Ddo_contrato_numero_Filtertype ;
      private String Ddo_contrato_numero_Datalisttype ;
      private String Ddo_contrato_numero_Datalistproc ;
      private String Ddo_contrato_numero_Sortasc ;
      private String Ddo_contrato_numero_Sortdsc ;
      private String Ddo_contrato_numero_Loadingdata ;
      private String Ddo_contrato_numero_Cleanfilter ;
      private String Ddo_contrato_numero_Noresultsfound ;
      private String Ddo_contrato_numero_Searchbuttontext ;
      private String Ddo_contratada_codigo_Caption ;
      private String Ddo_contratada_codigo_Tooltip ;
      private String Ddo_contratada_codigo_Cls ;
      private String Ddo_contratada_codigo_Filteredtext_set ;
      private String Ddo_contratada_codigo_Filteredtextto_set ;
      private String Ddo_contratada_codigo_Dropdownoptionstype ;
      private String Ddo_contratada_codigo_Titlecontrolidtoreplace ;
      private String Ddo_contratada_codigo_Sortedstatus ;
      private String Ddo_contratada_codigo_Filtertype ;
      private String Ddo_contratada_codigo_Sortasc ;
      private String Ddo_contratada_codigo_Sortdsc ;
      private String Ddo_contratada_codigo_Cleanfilter ;
      private String Ddo_contratada_codigo_Rangefilterfrom ;
      private String Ddo_contratada_codigo_Rangefilterto ;
      private String Ddo_contratada_codigo_Searchbuttontext ;
      private String Ddo_contratada_pessoacod_Caption ;
      private String Ddo_contratada_pessoacod_Tooltip ;
      private String Ddo_contratada_pessoacod_Cls ;
      private String Ddo_contratada_pessoacod_Filteredtext_set ;
      private String Ddo_contratada_pessoacod_Filteredtextto_set ;
      private String Ddo_contratada_pessoacod_Dropdownoptionstype ;
      private String Ddo_contratada_pessoacod_Titlecontrolidtoreplace ;
      private String Ddo_contratada_pessoacod_Sortedstatus ;
      private String Ddo_contratada_pessoacod_Filtertype ;
      private String Ddo_contratada_pessoacod_Sortasc ;
      private String Ddo_contratada_pessoacod_Sortdsc ;
      private String Ddo_contratada_pessoacod_Cleanfilter ;
      private String Ddo_contratada_pessoacod_Rangefilterfrom ;
      private String Ddo_contratada_pessoacod_Rangefilterto ;
      private String Ddo_contratada_pessoacod_Searchbuttontext ;
      private String Ddo_contratada_pessoanom_Caption ;
      private String Ddo_contratada_pessoanom_Tooltip ;
      private String Ddo_contratada_pessoanom_Cls ;
      private String Ddo_contratada_pessoanom_Filteredtext_set ;
      private String Ddo_contratada_pessoanom_Selectedvalue_set ;
      private String Ddo_contratada_pessoanom_Dropdownoptionstype ;
      private String Ddo_contratada_pessoanom_Titlecontrolidtoreplace ;
      private String Ddo_contratada_pessoanom_Sortedstatus ;
      private String Ddo_contratada_pessoanom_Filtertype ;
      private String Ddo_contratada_pessoanom_Datalisttype ;
      private String Ddo_contratada_pessoanom_Datalistproc ;
      private String Ddo_contratada_pessoanom_Sortasc ;
      private String Ddo_contratada_pessoanom_Sortdsc ;
      private String Ddo_contratada_pessoanom_Loadingdata ;
      private String Ddo_contratada_pessoanom_Cleanfilter ;
      private String Ddo_contratada_pessoanom_Noresultsfound ;
      private String Ddo_contratada_pessoanom_Searchbuttontext ;
      private String Ddo_contratada_pessoacnpj_Caption ;
      private String Ddo_contratada_pessoacnpj_Tooltip ;
      private String Ddo_contratada_pessoacnpj_Cls ;
      private String Ddo_contratada_pessoacnpj_Filteredtext_set ;
      private String Ddo_contratada_pessoacnpj_Selectedvalue_set ;
      private String Ddo_contratada_pessoacnpj_Dropdownoptionstype ;
      private String Ddo_contratada_pessoacnpj_Titlecontrolidtoreplace ;
      private String Ddo_contratada_pessoacnpj_Sortedstatus ;
      private String Ddo_contratada_pessoacnpj_Filtertype ;
      private String Ddo_contratada_pessoacnpj_Datalisttype ;
      private String Ddo_contratada_pessoacnpj_Datalistproc ;
      private String Ddo_contratada_pessoacnpj_Sortasc ;
      private String Ddo_contratada_pessoacnpj_Sortdsc ;
      private String Ddo_contratada_pessoacnpj_Loadingdata ;
      private String Ddo_contratada_pessoacnpj_Cleanfilter ;
      private String Ddo_contratada_pessoacnpj_Noresultsfound ;
      private String Ddo_contratada_pessoacnpj_Searchbuttontext ;
      private String Ddo_contratoarquivosanexos_descricao_Caption ;
      private String Ddo_contratoarquivosanexos_descricao_Tooltip ;
      private String Ddo_contratoarquivosanexos_descricao_Cls ;
      private String Ddo_contratoarquivosanexos_descricao_Filteredtext_set ;
      private String Ddo_contratoarquivosanexos_descricao_Selectedvalue_set ;
      private String Ddo_contratoarquivosanexos_descricao_Dropdownoptionstype ;
      private String Ddo_contratoarquivosanexos_descricao_Titlecontrolidtoreplace ;
      private String Ddo_contratoarquivosanexos_descricao_Sortedstatus ;
      private String Ddo_contratoarquivosanexos_descricao_Filtertype ;
      private String Ddo_contratoarquivosanexos_descricao_Datalisttype ;
      private String Ddo_contratoarquivosanexos_descricao_Datalistproc ;
      private String Ddo_contratoarquivosanexos_descricao_Sortasc ;
      private String Ddo_contratoarquivosanexos_descricao_Sortdsc ;
      private String Ddo_contratoarquivosanexos_descricao_Loadingdata ;
      private String Ddo_contratoarquivosanexos_descricao_Cleanfilter ;
      private String Ddo_contratoarquivosanexos_descricao_Noresultsfound ;
      private String Ddo_contratoarquivosanexos_descricao_Searchbuttontext ;
      private String Ddo_contratoarquivosanexos_data_Caption ;
      private String Ddo_contratoarquivosanexos_data_Tooltip ;
      private String Ddo_contratoarquivosanexos_data_Cls ;
      private String Ddo_contratoarquivosanexos_data_Filteredtext_set ;
      private String Ddo_contratoarquivosanexos_data_Filteredtextto_set ;
      private String Ddo_contratoarquivosanexos_data_Dropdownoptionstype ;
      private String Ddo_contratoarquivosanexos_data_Titlecontrolidtoreplace ;
      private String Ddo_contratoarquivosanexos_data_Sortedstatus ;
      private String Ddo_contratoarquivosanexos_data_Filtertype ;
      private String Ddo_contratoarquivosanexos_data_Sortasc ;
      private String Ddo_contratoarquivosanexos_data_Sortdsc ;
      private String Ddo_contratoarquivosanexos_data_Cleanfilter ;
      private String Ddo_contratoarquivosanexos_data_Rangefilterfrom ;
      private String Ddo_contratoarquivosanexos_data_Rangefilterto ;
      private String Ddo_contratoarquivosanexos_data_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfcontratoarquivosanexos_codigo_Internalname ;
      private String edtavTfcontratoarquivosanexos_codigo_Jsonclick ;
      private String edtavTfcontratoarquivosanexos_codigo_to_Internalname ;
      private String edtavTfcontratoarquivosanexos_codigo_to_Jsonclick ;
      private String edtavTfcontrato_codigo_Internalname ;
      private String edtavTfcontrato_codigo_Jsonclick ;
      private String edtavTfcontrato_codigo_to_Internalname ;
      private String edtavTfcontrato_codigo_to_Jsonclick ;
      private String edtavTfcontrato_numero_Internalname ;
      private String edtavTfcontrato_numero_Jsonclick ;
      private String edtavTfcontrato_numero_sel_Internalname ;
      private String edtavTfcontrato_numero_sel_Jsonclick ;
      private String edtavTfcontratada_codigo_Internalname ;
      private String edtavTfcontratada_codigo_Jsonclick ;
      private String edtavTfcontratada_codigo_to_Internalname ;
      private String edtavTfcontratada_codigo_to_Jsonclick ;
      private String edtavTfcontratada_pessoacod_Internalname ;
      private String edtavTfcontratada_pessoacod_Jsonclick ;
      private String edtavTfcontratada_pessoacod_to_Internalname ;
      private String edtavTfcontratada_pessoacod_to_Jsonclick ;
      private String edtavTfcontratada_pessoanom_Internalname ;
      private String edtavTfcontratada_pessoanom_Jsonclick ;
      private String edtavTfcontratada_pessoanom_sel_Internalname ;
      private String edtavTfcontratada_pessoanom_sel_Jsonclick ;
      private String edtavTfcontratada_pessoacnpj_Internalname ;
      private String edtavTfcontratada_pessoacnpj_Jsonclick ;
      private String edtavTfcontratada_pessoacnpj_sel_Internalname ;
      private String edtavTfcontratada_pessoacnpj_sel_Jsonclick ;
      private String edtavTfcontratoarquivosanexos_descricao_Internalname ;
      private String edtavTfcontratoarquivosanexos_descricao_sel_Internalname ;
      private String edtavTfcontratoarquivosanexos_data_Internalname ;
      private String edtavTfcontratoarquivosanexos_data_Jsonclick ;
      private String edtavTfcontratoarquivosanexos_data_to_Internalname ;
      private String edtavTfcontratoarquivosanexos_data_to_Jsonclick ;
      private String divDdo_contratoarquivosanexos_dataauxdates_Internalname ;
      private String edtavDdo_contratoarquivosanexos_dataauxdate_Internalname ;
      private String edtavDdo_contratoarquivosanexos_dataauxdate_Jsonclick ;
      private String edtavDdo_contratoarquivosanexos_dataauxdateto_Internalname ;
      private String edtavDdo_contratoarquivosanexos_dataauxdateto_Jsonclick ;
      private String edtavDdo_contratoarquivosanexos_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contrato_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratada_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratada_pessoacodtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoarquivosanexos_descricaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoarquivosanexos_datatitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtContratoArquivosAnexos_Codigo_Internalname ;
      private String edtContrato_Codigo_Internalname ;
      private String A77Contrato_Numero ;
      private String edtContrato_Numero_Internalname ;
      private String edtContratada_Codigo_Internalname ;
      private String edtContratada_PessoaCod_Internalname ;
      private String A41Contratada_PessoaNom ;
      private String edtContratada_PessoaNom_Internalname ;
      private String edtContratada_PessoaCNPJ_Internalname ;
      private String edtContratoArquivosAnexos_Descricao_Internalname ;
      private String edtContratoArquivosAnexos_Arquivo_Internalname ;
      private String edtContratoArquivosAnexos_Data_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV39TFContrato_Numero ;
      private String lV51TFContratada_PessoaNom ;
      private String A112ContratoArquivosAnexos_NomeArq ;
      private String edtContratoArquivosAnexos_Arquivo_Filename ;
      private String A109ContratoArquivosAnexos_TipoArq ;
      private String edtContratoArquivosAnexos_Arquivo_Filetype ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavContratoarquivosanexos_descricao1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavContratoarquivosanexos_descricao2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavContratoarquivosanexos_descricao3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_contratoarquivosanexos_codigo_Internalname ;
      private String Ddo_contrato_codigo_Internalname ;
      private String Ddo_contrato_numero_Internalname ;
      private String Ddo_contratada_codigo_Internalname ;
      private String Ddo_contratada_pessoacod_Internalname ;
      private String Ddo_contratada_pessoanom_Internalname ;
      private String Ddo_contratada_pessoacnpj_Internalname ;
      private String Ddo_contratoarquivosanexos_descricao_Internalname ;
      private String Ddo_contratoarquivosanexos_data_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtContratoArquivosAnexos_Codigo_Title ;
      private String edtContrato_Codigo_Title ;
      private String edtContrato_Numero_Title ;
      private String edtContratada_Codigo_Title ;
      private String edtContratada_PessoaCod_Title ;
      private String edtContratada_PessoaNom_Title ;
      private String edtContratada_PessoaCNPJ_Title ;
      private String edtContratoArquivosAnexos_Descricao_Title ;
      private String edtContratoArquivosAnexos_Data_Title ;
      private String edtavSelect_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtContratoArquivosAnexos_NomeArq_Internalname ;
      private String edtContratoArquivosAnexos_TipoArq_Internalname ;
      private String sGXsfl_80_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtContratoArquivosAnexos_Codigo_Jsonclick ;
      private String edtContrato_Codigo_Jsonclick ;
      private String edtContrato_Numero_Jsonclick ;
      private String edtContratada_Codigo_Jsonclick ;
      private String edtContratada_PessoaCod_Jsonclick ;
      private String edtContratada_PessoaNom_Jsonclick ;
      private String edtContratada_PessoaCNPJ_Jsonclick ;
      private String edtContratoArquivosAnexos_Descricao_Jsonclick ;
      private String edtContratoArquivosAnexos_Arquivo_Contenttype ;
      private String edtContratoArquivosAnexos_Arquivo_Parameters ;
      private String edtContratoArquivosAnexos_Arquivo_Jsonclick ;
      private String edtContratoArquivosAnexos_NomeArq_Jsonclick ;
      private String edtContratoArquivosAnexos_TipoArq_Jsonclick ;
      private String edtContratoArquivosAnexos_Data_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV63TFContratoArquivosAnexos_Data ;
      private DateTime AV64TFContratoArquivosAnexos_Data_To ;
      private DateTime A113ContratoArquivosAnexos_Data ;
      private DateTime AV65DDO_ContratoArquivosAnexos_DataAuxDate ;
      private DateTime AV66DDO_ContratoArquivosAnexos_DataAuxDateTo ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contratoarquivosanexos_codigo_Includesortasc ;
      private bool Ddo_contratoarquivosanexos_codigo_Includesortdsc ;
      private bool Ddo_contratoarquivosanexos_codigo_Includefilter ;
      private bool Ddo_contratoarquivosanexos_codigo_Filterisrange ;
      private bool Ddo_contratoarquivosanexos_codigo_Includedatalist ;
      private bool Ddo_contrato_codigo_Includesortasc ;
      private bool Ddo_contrato_codigo_Includesortdsc ;
      private bool Ddo_contrato_codigo_Includefilter ;
      private bool Ddo_contrato_codigo_Filterisrange ;
      private bool Ddo_contrato_codigo_Includedatalist ;
      private bool Ddo_contrato_numero_Includesortasc ;
      private bool Ddo_contrato_numero_Includesortdsc ;
      private bool Ddo_contrato_numero_Includefilter ;
      private bool Ddo_contrato_numero_Filterisrange ;
      private bool Ddo_contrato_numero_Includedatalist ;
      private bool Ddo_contratada_codigo_Includesortasc ;
      private bool Ddo_contratada_codigo_Includesortdsc ;
      private bool Ddo_contratada_codigo_Includefilter ;
      private bool Ddo_contratada_codigo_Filterisrange ;
      private bool Ddo_contratada_codigo_Includedatalist ;
      private bool Ddo_contratada_pessoacod_Includesortasc ;
      private bool Ddo_contratada_pessoacod_Includesortdsc ;
      private bool Ddo_contratada_pessoacod_Includefilter ;
      private bool Ddo_contratada_pessoacod_Filterisrange ;
      private bool Ddo_contratada_pessoacod_Includedatalist ;
      private bool Ddo_contratada_pessoanom_Includesortasc ;
      private bool Ddo_contratada_pessoanom_Includesortdsc ;
      private bool Ddo_contratada_pessoanom_Includefilter ;
      private bool Ddo_contratada_pessoanom_Filterisrange ;
      private bool Ddo_contratada_pessoanom_Includedatalist ;
      private bool Ddo_contratada_pessoacnpj_Includesortasc ;
      private bool Ddo_contratada_pessoacnpj_Includesortdsc ;
      private bool Ddo_contratada_pessoacnpj_Includefilter ;
      private bool Ddo_contratada_pessoacnpj_Filterisrange ;
      private bool Ddo_contratada_pessoacnpj_Includedatalist ;
      private bool Ddo_contratoarquivosanexos_descricao_Includesortasc ;
      private bool Ddo_contratoarquivosanexos_descricao_Includesortdsc ;
      private bool Ddo_contratoarquivosanexos_descricao_Includefilter ;
      private bool Ddo_contratoarquivosanexos_descricao_Filterisrange ;
      private bool Ddo_contratoarquivosanexos_descricao_Includedatalist ;
      private bool Ddo_contratoarquivosanexos_data_Includesortasc ;
      private bool Ddo_contratoarquivosanexos_data_Includesortdsc ;
      private bool Ddo_contratoarquivosanexos_data_Includefilter ;
      private bool Ddo_contratoarquivosanexos_data_Filterisrange ;
      private bool Ddo_contratoarquivosanexos_data_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n41Contratada_PessoaNom ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Select_IsBlob ;
      private String AV8InOutContratoArquivosAnexos_Descricao ;
      private String wcpOAV8InOutContratoArquivosAnexos_Descricao ;
      private String AV17ContratoArquivosAnexos_Descricao1 ;
      private String AV21ContratoArquivosAnexos_Descricao2 ;
      private String AV25ContratoArquivosAnexos_Descricao3 ;
      private String A110ContratoArquivosAnexos_Descricao ;
      private String lV17ContratoArquivosAnexos_Descricao1 ;
      private String lV21ContratoArquivosAnexos_Descricao2 ;
      private String lV25ContratoArquivosAnexos_Descricao3 ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV55TFContratada_PessoaCNPJ ;
      private String AV56TFContratada_PessoaCNPJ_Sel ;
      private String AV59TFContratoArquivosAnexos_Descricao ;
      private String AV60TFContratoArquivosAnexos_Descricao_Sel ;
      private String AV33ddo_ContratoArquivosAnexos_CodigoTitleControlIdToReplace ;
      private String AV37ddo_Contrato_CodigoTitleControlIdToReplace ;
      private String AV41ddo_Contrato_NumeroTitleControlIdToReplace ;
      private String AV45ddo_Contratada_CodigoTitleControlIdToReplace ;
      private String AV49ddo_Contratada_PessoaCodTitleControlIdToReplace ;
      private String AV53ddo_Contratada_PessoaNomTitleControlIdToReplace ;
      private String AV57ddo_Contratada_PessoaCNPJTitleControlIdToReplace ;
      private String AV61ddo_ContratoArquivosAnexos_DescricaoTitleControlIdToReplace ;
      private String AV67ddo_ContratoArquivosAnexos_DataTitleControlIdToReplace ;
      private String AV77Select_GXI ;
      private String A42Contratada_PessoaCNPJ ;
      private String lV55TFContratada_PessoaCNPJ ;
      private String lV59TFContratoArquivosAnexos_Descricao ;
      private String AV28Select ;
      private String A111ContratoArquivosAnexos_Arquivo ;
      private GxFile gxblobfileaux ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutContratoArquivosAnexos_Codigo ;
      private String aP1_InOutContratoArquivosAnexos_Descricao ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private String[] H006M2_A112ContratoArquivosAnexos_NomeArq ;
      private String[] H006M2_A109ContratoArquivosAnexos_TipoArq ;
      private DateTime[] H006M2_A113ContratoArquivosAnexos_Data ;
      private String[] H006M2_A110ContratoArquivosAnexos_Descricao ;
      private String[] H006M2_A42Contratada_PessoaCNPJ ;
      private bool[] H006M2_n42Contratada_PessoaCNPJ ;
      private String[] H006M2_A41Contratada_PessoaNom ;
      private bool[] H006M2_n41Contratada_PessoaNom ;
      private int[] H006M2_A40Contratada_PessoaCod ;
      private int[] H006M2_A39Contratada_Codigo ;
      private String[] H006M2_A77Contrato_Numero ;
      private int[] H006M2_A74Contrato_Codigo ;
      private int[] H006M2_A108ContratoArquivosAnexos_Codigo ;
      private String[] H006M2_A111ContratoArquivosAnexos_Arquivo ;
      private long[] H006M3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV30ContratoArquivosAnexos_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV34Contrato_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV38Contrato_NumeroTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV42Contratada_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV46Contratada_PessoaCodTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV50Contratada_PessoaNomTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV54Contratada_PessoaCNPJTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV58ContratoArquivosAnexos_DescricaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV62ContratoArquivosAnexos_DataTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV68DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class promptcontratoarquivosanexos__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H006M2( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             String AV17ContratoArquivosAnexos_Descricao1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             short AV20DynamicFiltersOperator2 ,
                                             String AV21ContratoArquivosAnexos_Descricao2 ,
                                             bool AV22DynamicFiltersEnabled3 ,
                                             String AV23DynamicFiltersSelector3 ,
                                             short AV24DynamicFiltersOperator3 ,
                                             String AV25ContratoArquivosAnexos_Descricao3 ,
                                             int AV31TFContratoArquivosAnexos_Codigo ,
                                             int AV32TFContratoArquivosAnexos_Codigo_To ,
                                             int AV35TFContrato_Codigo ,
                                             int AV36TFContrato_Codigo_To ,
                                             String AV40TFContrato_Numero_Sel ,
                                             String AV39TFContrato_Numero ,
                                             int AV43TFContratada_Codigo ,
                                             int AV44TFContratada_Codigo_To ,
                                             int AV47TFContratada_PessoaCod ,
                                             int AV48TFContratada_PessoaCod_To ,
                                             String AV52TFContratada_PessoaNom_Sel ,
                                             String AV51TFContratada_PessoaNom ,
                                             String AV56TFContratada_PessoaCNPJ_Sel ,
                                             String AV55TFContratada_PessoaCNPJ ,
                                             String AV60TFContratoArquivosAnexos_Descricao_Sel ,
                                             String AV59TFContratoArquivosAnexos_Descricao ,
                                             DateTime AV63TFContratoArquivosAnexos_Data ,
                                             DateTime AV64TFContratoArquivosAnexos_Data_To ,
                                             String A110ContratoArquivosAnexos_Descricao ,
                                             int A108ContratoArquivosAnexos_Codigo ,
                                             int A74Contrato_Codigo ,
                                             String A77Contrato_Numero ,
                                             int A39Contratada_Codigo ,
                                             int A40Contratada_PessoaCod ,
                                             String A41Contratada_PessoaNom ,
                                             String A42Contratada_PessoaCNPJ ,
                                             DateTime A113ContratoArquivosAnexos_Data ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [29] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[ContratoArquivosAnexos_NomeArq], T1.[ContratoArquivosAnexos_TipoArq], T1.[ContratoArquivosAnexos_Data], T1.[ContratoArquivosAnexos_Descricao], T4.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T4.[Pessoa_Nome] AS Contratada_PessoaNom, T3.[Contratada_PessoaCod] AS Contratada_PessoaCod, T2.[Contratada_Codigo], T2.[Contrato_Numero], T1.[Contrato_Codigo], T1.[ContratoArquivosAnexos_Codigo], T1.[ContratoArquivosAnexos_Arquivo]";
         sFromString = " FROM ((([ContratoArquivosAnexos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17ContratoArquivosAnexos_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV17ContratoArquivosAnexos_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV17ContratoArquivosAnexos_Descricao1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17ContratoArquivosAnexos_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV17ContratoArquivosAnexos_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV17ContratoArquivosAnexos_Descricao1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV20DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21ContratoArquivosAnexos_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV21ContratoArquivosAnexos_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV21ContratoArquivosAnexos_Descricao2)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV20DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21ContratoArquivosAnexos_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV21ContratoArquivosAnexos_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV21ContratoArquivosAnexos_Descricao2)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV24DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25ContratoArquivosAnexos_Descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV25ContratoArquivosAnexos_Descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV25ContratoArquivosAnexos_Descricao3)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV24DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25ContratoArquivosAnexos_Descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV25ContratoArquivosAnexos_Descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV25ContratoArquivosAnexos_Descricao3)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! (0==AV31TFContratoArquivosAnexos_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Codigo] >= @AV31TFContratoArquivosAnexos_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Codigo] >= @AV31TFContratoArquivosAnexos_Codigo)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ! (0==AV32TFContratoArquivosAnexos_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Codigo] <= @AV32TFContratoArquivosAnexos_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Codigo] <= @AV32TFContratoArquivosAnexos_Codigo_To)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ! (0==AV35TFContrato_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] >= @AV35TFContrato_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] >= @AV35TFContrato_Codigo)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! (0==AV36TFContrato_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] <= @AV36TFContrato_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] <= @AV36TFContrato_Codigo_To)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV40TFContrato_Numero_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFContrato_Numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV39TFContrato_Numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV39TFContrato_Numero)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFContrato_Numero_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV40TFContrato_Numero_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV40TFContrato_Numero_Sel)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( ! (0==AV43TFContratada_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contratada_Codigo] >= @AV43TFContratada_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contratada_Codigo] >= @AV43TFContratada_Codigo)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( ! (0==AV44TFContratada_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contratada_Codigo] <= @AV44TFContratada_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contratada_Codigo] <= @AV44TFContratada_Codigo_To)";
            }
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( ! (0==AV47TFContratada_PessoaCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contratada_PessoaCod] >= @AV47TFContratada_PessoaCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contratada_PessoaCod] >= @AV47TFContratada_PessoaCod)";
            }
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( ! (0==AV48TFContratada_PessoaCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contratada_PessoaCod] <= @AV48TFContratada_PessoaCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contratada_PessoaCod] <= @AV48TFContratada_PessoaCod_To)";
            }
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV52TFContratada_PessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51TFContratada_PessoaNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV51TFContratada_PessoaNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV51TFContratada_PessoaNom)";
            }
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52TFContratada_PessoaNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV52TFContratada_PessoaNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] = @AV52TFContratada_PessoaNom_Sel)";
            }
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV56TFContratada_PessoaCNPJ_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55TFContratada_PessoaCNPJ)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] like @lV55TFContratada_PessoaCNPJ)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] like @lV55TFContratada_PessoaCNPJ)";
            }
         }
         else
         {
            GXv_int2[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56TFContratada_PessoaCNPJ_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] = @AV56TFContratada_PessoaCNPJ_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] = @AV56TFContratada_PessoaCNPJ_Sel)";
            }
         }
         else
         {
            GXv_int2[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV60TFContratoArquivosAnexos_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59TFContratoArquivosAnexos_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV59TFContratoArquivosAnexos_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV59TFContratoArquivosAnexos_Descricao)";
            }
         }
         else
         {
            GXv_int2[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60TFContratoArquivosAnexos_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] = @AV60TFContratoArquivosAnexos_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] = @AV60TFContratoArquivosAnexos_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int2[21] = 1;
         }
         if ( ! (DateTime.MinValue==AV63TFContratoArquivosAnexos_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] >= @AV63TFContratoArquivosAnexos_Data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] >= @AV63TFContratoArquivosAnexos_Data)";
            }
         }
         else
         {
            GXv_int2[22] = 1;
         }
         if ( ! (DateTime.MinValue==AV64TFContratoArquivosAnexos_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] <= @AV64TFContratoArquivosAnexos_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] <= @AV64TFContratoArquivosAnexos_Data_To)";
            }
         }
         else
         {
            GXv_int2[23] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoArquivosAnexos_Descricao]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoArquivosAnexos_Descricao] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoArquivosAnexos_Codigo]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoArquivosAnexos_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Codigo]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contrato_Numero]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contrato_Numero] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contratada_Codigo]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contratada_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Contratada_PessoaCod]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Contratada_PessoaCod] DESC";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Pessoa_Nome]";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Pessoa_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Pessoa_Docto]";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Pessoa_Docto] DESC";
         }
         else if ( ( AV13OrderedBy == 9 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoArquivosAnexos_Data]";
         }
         else if ( ( AV13OrderedBy == 9 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoArquivosAnexos_Data] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoArquivosAnexos_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H006M3( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             String AV17ContratoArquivosAnexos_Descricao1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             short AV20DynamicFiltersOperator2 ,
                                             String AV21ContratoArquivosAnexos_Descricao2 ,
                                             bool AV22DynamicFiltersEnabled3 ,
                                             String AV23DynamicFiltersSelector3 ,
                                             short AV24DynamicFiltersOperator3 ,
                                             String AV25ContratoArquivosAnexos_Descricao3 ,
                                             int AV31TFContratoArquivosAnexos_Codigo ,
                                             int AV32TFContratoArquivosAnexos_Codigo_To ,
                                             int AV35TFContrato_Codigo ,
                                             int AV36TFContrato_Codigo_To ,
                                             String AV40TFContrato_Numero_Sel ,
                                             String AV39TFContrato_Numero ,
                                             int AV43TFContratada_Codigo ,
                                             int AV44TFContratada_Codigo_To ,
                                             int AV47TFContratada_PessoaCod ,
                                             int AV48TFContratada_PessoaCod_To ,
                                             String AV52TFContratada_PessoaNom_Sel ,
                                             String AV51TFContratada_PessoaNom ,
                                             String AV56TFContratada_PessoaCNPJ_Sel ,
                                             String AV55TFContratada_PessoaCNPJ ,
                                             String AV60TFContratoArquivosAnexos_Descricao_Sel ,
                                             String AV59TFContratoArquivosAnexos_Descricao ,
                                             DateTime AV63TFContratoArquivosAnexos_Data ,
                                             DateTime AV64TFContratoArquivosAnexos_Data_To ,
                                             String A110ContratoArquivosAnexos_Descricao ,
                                             int A108ContratoArquivosAnexos_Codigo ,
                                             int A74Contrato_Codigo ,
                                             String A77Contrato_Numero ,
                                             int A39Contratada_Codigo ,
                                             int A40Contratada_PessoaCod ,
                                             String A41Contratada_PessoaNom ,
                                             String A42Contratada_PessoaCNPJ ,
                                             DateTime A113ContratoArquivosAnexos_Data ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [24] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ((([ContratoArquivosAnexos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17ContratoArquivosAnexos_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV17ContratoArquivosAnexos_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV17ContratoArquivosAnexos_Descricao1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17ContratoArquivosAnexos_Descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV17ContratoArquivosAnexos_Descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV17ContratoArquivosAnexos_Descricao1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV20DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21ContratoArquivosAnexos_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV21ContratoArquivosAnexos_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV21ContratoArquivosAnexos_Descricao2)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV20DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21ContratoArquivosAnexos_Descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV21ContratoArquivosAnexos_Descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV21ContratoArquivosAnexos_Descricao2)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV24DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25ContratoArquivosAnexos_Descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV25ContratoArquivosAnexos_Descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV25ContratoArquivosAnexos_Descricao3)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOARQUIVOSANEXOS_DESCRICAO") == 0 ) && ( AV24DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25ContratoArquivosAnexos_Descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV25ContratoArquivosAnexos_Descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like '%' + @lV25ContratoArquivosAnexos_Descricao3)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! (0==AV31TFContratoArquivosAnexos_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Codigo] >= @AV31TFContratoArquivosAnexos_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Codigo] >= @AV31TFContratoArquivosAnexos_Codigo)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ! (0==AV32TFContratoArquivosAnexos_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Codigo] <= @AV32TFContratoArquivosAnexos_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Codigo] <= @AV32TFContratoArquivosAnexos_Codigo_To)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ! (0==AV35TFContrato_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] >= @AV35TFContrato_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] >= @AV35TFContrato_Codigo)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! (0==AV36TFContrato_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] <= @AV36TFContrato_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] <= @AV36TFContrato_Codigo_To)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV40TFContrato_Numero_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFContrato_Numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV39TFContrato_Numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV39TFContrato_Numero)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFContrato_Numero_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV40TFContrato_Numero_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV40TFContrato_Numero_Sel)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( ! (0==AV43TFContratada_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contratada_Codigo] >= @AV43TFContratada_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contratada_Codigo] >= @AV43TFContratada_Codigo)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( ! (0==AV44TFContratada_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contratada_Codigo] <= @AV44TFContratada_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contratada_Codigo] <= @AV44TFContratada_Codigo_To)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( ! (0==AV47TFContratada_PessoaCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contratada_PessoaCod] >= @AV47TFContratada_PessoaCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contratada_PessoaCod] >= @AV47TFContratada_PessoaCod)";
            }
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( ! (0==AV48TFContratada_PessoaCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contratada_PessoaCod] <= @AV48TFContratada_PessoaCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contratada_PessoaCod] <= @AV48TFContratada_PessoaCod_To)";
            }
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV52TFContratada_PessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51TFContratada_PessoaNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV51TFContratada_PessoaNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV51TFContratada_PessoaNom)";
            }
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52TFContratada_PessoaNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV52TFContratada_PessoaNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] = @AV52TFContratada_PessoaNom_Sel)";
            }
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV56TFContratada_PessoaCNPJ_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55TFContratada_PessoaCNPJ)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] like @lV55TFContratada_PessoaCNPJ)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] like @lV55TFContratada_PessoaCNPJ)";
            }
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56TFContratada_PessoaCNPJ_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] = @AV56TFContratada_PessoaCNPJ_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] = @AV56TFContratada_PessoaCNPJ_Sel)";
            }
         }
         else
         {
            GXv_int4[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV60TFContratoArquivosAnexos_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59TFContratoArquivosAnexos_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] like @lV59TFContratoArquivosAnexos_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] like @lV59TFContratoArquivosAnexos_Descricao)";
            }
         }
         else
         {
            GXv_int4[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60TFContratoArquivosAnexos_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Descricao] = @AV60TFContratoArquivosAnexos_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Descricao] = @AV60TFContratoArquivosAnexos_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int4[21] = 1;
         }
         if ( ! (DateTime.MinValue==AV63TFContratoArquivosAnexos_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] >= @AV63TFContratoArquivosAnexos_Data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] >= @AV63TFContratoArquivosAnexos_Data)";
            }
         }
         else
         {
            GXv_int4[22] = 1;
         }
         if ( ! (DateTime.MinValue==AV64TFContratoArquivosAnexos_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoArquivosAnexos_Data] <= @AV64TFContratoArquivosAnexos_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoArquivosAnexos_Data] <= @AV64TFContratoArquivosAnexos_Data_To)";
            }
         }
         else
         {
            GXv_int4[23] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 9 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 9 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H006M2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (DateTime)dynConstraints[27] , (DateTime)dynConstraints[28] , (String)dynConstraints[29] , (int)dynConstraints[30] , (int)dynConstraints[31] , (String)dynConstraints[32] , (int)dynConstraints[33] , (int)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (DateTime)dynConstraints[37] , (short)dynConstraints[38] , (bool)dynConstraints[39] );
               case 1 :
                     return conditional_H006M3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (DateTime)dynConstraints[27] , (DateTime)dynConstraints[28] , (String)dynConstraints[29] , (int)dynConstraints[30] , (int)dynConstraints[31] , (String)dynConstraints[32] , (int)dynConstraints[33] , (int)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (DateTime)dynConstraints[37] , (short)dynConstraints[38] , (bool)dynConstraints[39] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH006M2 ;
          prmH006M2 = new Object[] {
          new Object[] {"@lV17ContratoArquivosAnexos_Descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV17ContratoArquivosAnexos_Descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV21ContratoArquivosAnexos_Descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV21ContratoArquivosAnexos_Descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV25ContratoArquivosAnexos_Descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV25ContratoArquivosAnexos_Descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV31TFContratoArquivosAnexos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV32TFContratoArquivosAnexos_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV35TFContrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV36TFContrato_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV39TFContrato_Numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV40TFContrato_Numero_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV43TFContratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV44TFContratada_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV47TFContratada_PessoaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV48TFContratada_PessoaCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV51TFContratada_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV52TFContratada_PessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV55TFContratada_PessoaCNPJ",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV56TFContratada_PessoaCNPJ_Sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV59TFContratoArquivosAnexos_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV60TFContratoArquivosAnexos_Descricao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV63TFContratoArquivosAnexos_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV64TFContratoArquivosAnexos_Data_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH006M3 ;
          prmH006M3 = new Object[] {
          new Object[] {"@lV17ContratoArquivosAnexos_Descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV17ContratoArquivosAnexos_Descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV21ContratoArquivosAnexos_Descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV21ContratoArquivosAnexos_Descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV25ContratoArquivosAnexos_Descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV25ContratoArquivosAnexos_Descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV31TFContratoArquivosAnexos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV32TFContratoArquivosAnexos_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV35TFContrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV36TFContrato_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV39TFContrato_Numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV40TFContrato_Numero_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV43TFContratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV44TFContratada_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV47TFContratada_PessoaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV48TFContratada_PessoaCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV51TFContratada_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV52TFContratada_PessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV55TFContratada_PessoaCNPJ",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV56TFContratada_PessoaCNPJ_Sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV59TFContratoArquivosAnexos_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV60TFContratoArquivosAnexos_Descricao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV63TFContratoArquivosAnexos_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV64TFContratoArquivosAnexos_Data_To",SqlDbType.DateTime,8,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("H006M2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006M2,11,0,true,false )
             ,new CursorDef("H006M3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006M3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 10) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((String[]) buf[6])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                ((int[]) buf[9])[0] = rslt.getInt(8) ;
                ((String[]) buf[10])[0] = rslt.getString(9, 20) ;
                ((int[]) buf[11])[0] = rslt.getInt(10) ;
                ((int[]) buf[12])[0] = rslt.getInt(11) ;
                ((String[]) buf[13])[0] = rslt.getBLOBFile(12, rslt.getString(2, 10), rslt.getString(1, 50)) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[38]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[42]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[51]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[52]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[53]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[54]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[55]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[56]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[57]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[38]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[39]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[46]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[47]);
                }
                return;
       }
    }

 }

}
