/*
               File: WWEmail
        Description:  Email
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:3:48.22
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwemail : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwemail( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwemail( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_88 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_88_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_88_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17Email_Titulo1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Email_Titulo1", AV17Email_Titulo1);
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
               AV21Email_Titulo2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Email_Titulo2", AV21Email_Titulo2);
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
               AV25Email_Titulo3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Email_Titulo3", AV25Email_Titulo3);
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV34TFEmail_Titulo = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFEmail_Titulo", AV34TFEmail_Titulo);
               AV35TFEmail_Titulo_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFEmail_Titulo_Sel", AV35TFEmail_Titulo_Sel);
               AV38TFEmail_Key = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFEmail_Key", AV38TFEmail_Key);
               AV39TFEmail_Key_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFEmail_Key_Sel", AV39TFEmail_Key_Sel);
               AV36ddo_Email_TituloTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ddo_Email_TituloTitleControlIdToReplace", AV36ddo_Email_TituloTitleControlIdToReplace);
               AV40ddo_Email_KeyTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ddo_Email_KeyTitleControlIdToReplace", AV40ddo_Email_KeyTitleControlIdToReplace);
               AV64Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               A1665Email_Guid = (Guid)(StringUtil.StrToGuid( GetNextPar( )));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Email_Titulo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Email_Titulo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Email_Titulo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFEmail_Titulo, AV35TFEmail_Titulo_Sel, AV38TFEmail_Key, AV39TFEmail_Key_Sel, AV36ddo_Email_TituloTitleControlIdToReplace, AV40ddo_Email_KeyTitleControlIdToReplace, AV64Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1665Email_Guid) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAMS2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTMS2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311934844");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwemail.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vEMAIL_TITULO1", AV17Email_Titulo1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vEMAIL_TITULO2", AV21Email_Titulo2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vEMAIL_TITULO3", AV25Email_Titulo3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFEMAIL_TITULO", AV34TFEmail_Titulo);
         GxWebStd.gx_hidden_field( context, "GXH_vTFEMAIL_TITULO_SEL", AV35TFEmail_Titulo_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFEMAIL_KEY", AV38TFEmail_Key);
         GxWebStd.gx_hidden_field( context, "GXH_vTFEMAIL_KEY_SEL", AV39TFEmail_Key_Sel);
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_88", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_88), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV43GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV44GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV41DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV41DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vEMAIL_TITULOTITLEFILTERDATA", AV33Email_TituloTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vEMAIL_TITULOTITLEFILTERDATA", AV33Email_TituloTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vEMAIL_KEYTITLEFILTERDATA", AV37Email_KeyTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vEMAIL_KEYTITLEFILTERDATA", AV37Email_KeyTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV64Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_TITULO_Caption", StringUtil.RTrim( Ddo_email_titulo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_TITULO_Tooltip", StringUtil.RTrim( Ddo_email_titulo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_TITULO_Cls", StringUtil.RTrim( Ddo_email_titulo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_TITULO_Filteredtext_set", StringUtil.RTrim( Ddo_email_titulo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_TITULO_Selectedvalue_set", StringUtil.RTrim( Ddo_email_titulo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_TITULO_Dropdownoptionstype", StringUtil.RTrim( Ddo_email_titulo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_TITULO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_email_titulo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_TITULO_Includesortasc", StringUtil.BoolToStr( Ddo_email_titulo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_TITULO_Includesortdsc", StringUtil.BoolToStr( Ddo_email_titulo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_TITULO_Sortedstatus", StringUtil.RTrim( Ddo_email_titulo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_TITULO_Includefilter", StringUtil.BoolToStr( Ddo_email_titulo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_TITULO_Filtertype", StringUtil.RTrim( Ddo_email_titulo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_TITULO_Filterisrange", StringUtil.BoolToStr( Ddo_email_titulo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_TITULO_Includedatalist", StringUtil.BoolToStr( Ddo_email_titulo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_TITULO_Datalisttype", StringUtil.RTrim( Ddo_email_titulo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_TITULO_Datalistproc", StringUtil.RTrim( Ddo_email_titulo_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_TITULO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_email_titulo_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_TITULO_Sortasc", StringUtil.RTrim( Ddo_email_titulo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_TITULO_Sortdsc", StringUtil.RTrim( Ddo_email_titulo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_TITULO_Loadingdata", StringUtil.RTrim( Ddo_email_titulo_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_TITULO_Cleanfilter", StringUtil.RTrim( Ddo_email_titulo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_TITULO_Noresultsfound", StringUtil.RTrim( Ddo_email_titulo_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_TITULO_Searchbuttontext", StringUtil.RTrim( Ddo_email_titulo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_KEY_Caption", StringUtil.RTrim( Ddo_email_key_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_KEY_Tooltip", StringUtil.RTrim( Ddo_email_key_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_KEY_Cls", StringUtil.RTrim( Ddo_email_key_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_KEY_Filteredtext_set", StringUtil.RTrim( Ddo_email_key_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_KEY_Selectedvalue_set", StringUtil.RTrim( Ddo_email_key_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_KEY_Dropdownoptionstype", StringUtil.RTrim( Ddo_email_key_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_KEY_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_email_key_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_KEY_Includesortasc", StringUtil.BoolToStr( Ddo_email_key_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_KEY_Includesortdsc", StringUtil.BoolToStr( Ddo_email_key_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_KEY_Sortedstatus", StringUtil.RTrim( Ddo_email_key_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_KEY_Includefilter", StringUtil.BoolToStr( Ddo_email_key_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_KEY_Filtertype", StringUtil.RTrim( Ddo_email_key_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_KEY_Filterisrange", StringUtil.BoolToStr( Ddo_email_key_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_KEY_Includedatalist", StringUtil.BoolToStr( Ddo_email_key_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_KEY_Datalisttype", StringUtil.RTrim( Ddo_email_key_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_KEY_Datalistproc", StringUtil.RTrim( Ddo_email_key_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_KEY_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_email_key_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_KEY_Sortasc", StringUtil.RTrim( Ddo_email_key_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_KEY_Sortdsc", StringUtil.RTrim( Ddo_email_key_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_KEY_Loadingdata", StringUtil.RTrim( Ddo_email_key_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_KEY_Cleanfilter", StringUtil.RTrim( Ddo_email_key_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_KEY_Noresultsfound", StringUtil.RTrim( Ddo_email_key_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_KEY_Searchbuttontext", StringUtil.RTrim( Ddo_email_key_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_TITULO_Activeeventkey", StringUtil.RTrim( Ddo_email_titulo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_TITULO_Filteredtext_get", StringUtil.RTrim( Ddo_email_titulo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_TITULO_Selectedvalue_get", StringUtil.RTrim( Ddo_email_titulo_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_KEY_Activeeventkey", StringUtil.RTrim( Ddo_email_key_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_KEY_Filteredtext_get", StringUtil.RTrim( Ddo_email_key_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_EMAIL_KEY_Selectedvalue_get", StringUtil.RTrim( Ddo_email_key_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEMS2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTMS2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwemail.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWEmail" ;
      }

      public override String GetPgmdesc( )
      {
         return " Email" ;
      }

      protected void WBMS0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_MS2( true) ;
         }
         else
         {
            wb_table1_2_MS2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_MS2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(98, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,98);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(99, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,99);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfemail_titulo_Internalname, AV34TFEmail_Titulo, StringUtil.RTrim( context.localUtil.Format( AV34TFEmail_Titulo, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,100);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfemail_titulo_Jsonclick, 0, "Attribute", "", "", "", edtavTfemail_titulo_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWEmail.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfemail_titulo_sel_Internalname, AV35TFEmail_Titulo_Sel, StringUtil.RTrim( context.localUtil.Format( AV35TFEmail_Titulo_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,101);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfemail_titulo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfemail_titulo_sel_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWEmail.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfemail_key_Internalname, AV38TFEmail_Key, StringUtil.RTrim( context.localUtil.Format( AV38TFEmail_Key, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,102);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfemail_key_Jsonclick, 0, "Attribute", "", "", "", edtavTfemail_key_Visible, 1, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWEmail.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfemail_key_sel_Internalname, AV39TFEmail_Key_Sel, StringUtil.RTrim( context.localUtil.Format( AV39TFEmail_Key_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,103);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfemail_key_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfemail_key_sel_Visible, 1, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWEmail.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_EMAIL_TITULOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_email_titulotitlecontrolidtoreplace_Internalname, AV36ddo_Email_TituloTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,105);\"", 0, edtavDdo_email_titulotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWEmail.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_EMAIL_KEYContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_email_keytitlecontrolidtoreplace_Internalname, AV40ddo_Email_KeyTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,107);\"", 0, edtavDdo_email_keytitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWEmail.htm");
         }
         wbLoad = true;
      }

      protected void STARTMS2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Email", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPMS0( ) ;
      }

      protected void WSMS2( )
      {
         STARTMS2( ) ;
         EVTMS2( ) ;
      }

      protected void EVTMS2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11MS2 */
                              E11MS2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_EMAIL_TITULO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12MS2 */
                              E12MS2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_EMAIL_KEY.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13MS2 */
                              E13MS2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14MS2 */
                              E14MS2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15MS2 */
                              E15MS2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16MS2 */
                              E16MS2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17MS2 */
                              E17MS2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18MS2 */
                              E18MS2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19MS2 */
                              E19MS2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20MS2 */
                              E20MS2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21MS2 */
                              E21MS2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22MS2 */
                              E22MS2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E23MS2 */
                              E23MS2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E24MS2 */
                              E24MS2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_88_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_88_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_88_idx), 4, 0)), 4, "0");
                              SubsflControlProps_882( ) ;
                              AV28Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV62Update_GXI : context.convertURL( context.PathToRelativeUrl( AV28Update))));
                              AV29Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV63Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV29Delete))));
                              A1665Email_Guid = (Guid)(StringUtil.StrToGuid( cgiGet( edtEmail_Guid_Internalname)));
                              A1669Email_Titulo = cgiGet( edtEmail_Titulo_Internalname);
                              A1672Email_Key = cgiGet( edtEmail_Key_Internalname);
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E25MS2 */
                                    E25MS2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E26MS2 */
                                    E26MS2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E27MS2 */
                                    E27MS2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Email_titulo1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vEMAIL_TITULO1"), AV17Email_Titulo1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Email_titulo2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vEMAIL_TITULO2"), AV21Email_Titulo2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Email_titulo3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vEMAIL_TITULO3"), AV25Email_Titulo3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfemail_titulo Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFEMAIL_TITULO"), AV34TFEmail_Titulo) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfemail_titulo_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFEMAIL_TITULO_SEL"), AV35TFEmail_Titulo_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfemail_key Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFEMAIL_KEY"), AV38TFEmail_Key) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfemail_key_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFEMAIL_KEY_SEL"), AV39TFEmail_Key_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEMS2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAMS2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("EMAIL_TITULO", "Assunto", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("EMAIL_TITULO", "Assunto", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("EMAIL_TITULO", "Assunto", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_882( ) ;
         while ( nGXsfl_88_idx <= nRC_GXsfl_88 )
         {
            sendrow_882( ) ;
            nGXsfl_88_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_88_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_88_idx+1));
            sGXsfl_88_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_88_idx), 4, 0)), 4, "0");
            SubsflControlProps_882( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       String AV17Email_Titulo1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       short AV20DynamicFiltersOperator2 ,
                                       String AV21Email_Titulo2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       short AV24DynamicFiltersOperator3 ,
                                       String AV25Email_Titulo3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       String AV34TFEmail_Titulo ,
                                       String AV35TFEmail_Titulo_Sel ,
                                       String AV38TFEmail_Key ,
                                       String AV39TFEmail_Key_Sel ,
                                       String AV36ddo_Email_TituloTitleControlIdToReplace ,
                                       String AV40ddo_Email_KeyTitleControlIdToReplace ,
                                       String AV64Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving ,
                                       Guid A1665Email_Guid )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFMS2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_EMAIL_GUID", GetSecureSignedToken( "", A1665Email_Guid));
         GxWebStd.gx_hidden_field( context, "EMAIL_GUID", A1665Email_Guid.ToString());
         GxWebStd.gx_hidden_field( context, "gxhash_EMAIL_TITULO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1669Email_Titulo, ""))));
         GxWebStd.gx_hidden_field( context, "EMAIL_TITULO", A1669Email_Titulo);
         GxWebStd.gx_hidden_field( context, "gxhash_EMAIL_KEY", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1672Email_Key, ""))));
         GxWebStd.gx_hidden_field( context, "EMAIL_KEY", A1672Email_Key);
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFMS2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV64Pgmname = "WWEmail";
         context.Gx_err = 0;
      }

      protected void RFMS2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 88;
         /* Execute user event: E26MS2 */
         E26MS2 ();
         nGXsfl_88_idx = 1;
         sGXsfl_88_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_88_idx), 4, 0)), 4, "0");
         SubsflControlProps_882( ) ;
         nGXsfl_88_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_882( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV47WWEmailDS_1_Dynamicfiltersselector1 ,
                                                 AV48WWEmailDS_2_Dynamicfiltersoperator1 ,
                                                 AV49WWEmailDS_3_Email_titulo1 ,
                                                 AV50WWEmailDS_4_Dynamicfiltersenabled2 ,
                                                 AV51WWEmailDS_5_Dynamicfiltersselector2 ,
                                                 AV52WWEmailDS_6_Dynamicfiltersoperator2 ,
                                                 AV53WWEmailDS_7_Email_titulo2 ,
                                                 AV54WWEmailDS_8_Dynamicfiltersenabled3 ,
                                                 AV55WWEmailDS_9_Dynamicfiltersselector3 ,
                                                 AV56WWEmailDS_10_Dynamicfiltersoperator3 ,
                                                 AV57WWEmailDS_11_Email_titulo3 ,
                                                 AV59WWEmailDS_13_Tfemail_titulo_sel ,
                                                 AV58WWEmailDS_12_Tfemail_titulo ,
                                                 AV61WWEmailDS_15_Tfemail_key_sel ,
                                                 AV60WWEmailDS_14_Tfemail_key ,
                                                 A1669Email_Titulo ,
                                                 A1672Email_Key ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV49WWEmailDS_3_Email_titulo1 = StringUtil.Concat( StringUtil.RTrim( AV49WWEmailDS_3_Email_titulo1), "%", "");
            lV49WWEmailDS_3_Email_titulo1 = StringUtil.Concat( StringUtil.RTrim( AV49WWEmailDS_3_Email_titulo1), "%", "");
            lV53WWEmailDS_7_Email_titulo2 = StringUtil.Concat( StringUtil.RTrim( AV53WWEmailDS_7_Email_titulo2), "%", "");
            lV53WWEmailDS_7_Email_titulo2 = StringUtil.Concat( StringUtil.RTrim( AV53WWEmailDS_7_Email_titulo2), "%", "");
            lV57WWEmailDS_11_Email_titulo3 = StringUtil.Concat( StringUtil.RTrim( AV57WWEmailDS_11_Email_titulo3), "%", "");
            lV57WWEmailDS_11_Email_titulo3 = StringUtil.Concat( StringUtil.RTrim( AV57WWEmailDS_11_Email_titulo3), "%", "");
            lV58WWEmailDS_12_Tfemail_titulo = StringUtil.Concat( StringUtil.RTrim( AV58WWEmailDS_12_Tfemail_titulo), "%", "");
            lV60WWEmailDS_14_Tfemail_key = StringUtil.Concat( StringUtil.RTrim( AV60WWEmailDS_14_Tfemail_key), "%", "");
            /* Using cursor H00MS2 */
            pr_default.execute(0, new Object[] {lV49WWEmailDS_3_Email_titulo1, lV49WWEmailDS_3_Email_titulo1, lV53WWEmailDS_7_Email_titulo2, lV53WWEmailDS_7_Email_titulo2, lV57WWEmailDS_11_Email_titulo3, lV57WWEmailDS_11_Email_titulo3, lV58WWEmailDS_12_Tfemail_titulo, AV59WWEmailDS_13_Tfemail_titulo_sel, lV60WWEmailDS_14_Tfemail_key, AV61WWEmailDS_15_Tfemail_key_sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_88_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1672Email_Key = H00MS2_A1672Email_Key[0];
               A1669Email_Titulo = H00MS2_A1669Email_Titulo[0];
               A1665Email_Guid = (Guid)((Guid)(H00MS2_A1665Email_Guid[0]));
               /* Execute user event: E27MS2 */
               E27MS2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 88;
            WBMS0( ) ;
         }
         nGXsfl_88_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV47WWEmailDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV48WWEmailDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV49WWEmailDS_3_Email_titulo1 = AV17Email_Titulo1;
         AV50WWEmailDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV51WWEmailDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV52WWEmailDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV53WWEmailDS_7_Email_titulo2 = AV21Email_Titulo2;
         AV54WWEmailDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV55WWEmailDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV56WWEmailDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV57WWEmailDS_11_Email_titulo3 = AV25Email_Titulo3;
         AV58WWEmailDS_12_Tfemail_titulo = AV34TFEmail_Titulo;
         AV59WWEmailDS_13_Tfemail_titulo_sel = AV35TFEmail_Titulo_Sel;
         AV60WWEmailDS_14_Tfemail_key = AV38TFEmail_Key;
         AV61WWEmailDS_15_Tfemail_key_sel = AV39TFEmail_Key_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV47WWEmailDS_1_Dynamicfiltersselector1 ,
                                              AV48WWEmailDS_2_Dynamicfiltersoperator1 ,
                                              AV49WWEmailDS_3_Email_titulo1 ,
                                              AV50WWEmailDS_4_Dynamicfiltersenabled2 ,
                                              AV51WWEmailDS_5_Dynamicfiltersselector2 ,
                                              AV52WWEmailDS_6_Dynamicfiltersoperator2 ,
                                              AV53WWEmailDS_7_Email_titulo2 ,
                                              AV54WWEmailDS_8_Dynamicfiltersenabled3 ,
                                              AV55WWEmailDS_9_Dynamicfiltersselector3 ,
                                              AV56WWEmailDS_10_Dynamicfiltersoperator3 ,
                                              AV57WWEmailDS_11_Email_titulo3 ,
                                              AV59WWEmailDS_13_Tfemail_titulo_sel ,
                                              AV58WWEmailDS_12_Tfemail_titulo ,
                                              AV61WWEmailDS_15_Tfemail_key_sel ,
                                              AV60WWEmailDS_14_Tfemail_key ,
                                              A1669Email_Titulo ,
                                              A1672Email_Key ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV49WWEmailDS_3_Email_titulo1 = StringUtil.Concat( StringUtil.RTrim( AV49WWEmailDS_3_Email_titulo1), "%", "");
         lV49WWEmailDS_3_Email_titulo1 = StringUtil.Concat( StringUtil.RTrim( AV49WWEmailDS_3_Email_titulo1), "%", "");
         lV53WWEmailDS_7_Email_titulo2 = StringUtil.Concat( StringUtil.RTrim( AV53WWEmailDS_7_Email_titulo2), "%", "");
         lV53WWEmailDS_7_Email_titulo2 = StringUtil.Concat( StringUtil.RTrim( AV53WWEmailDS_7_Email_titulo2), "%", "");
         lV57WWEmailDS_11_Email_titulo3 = StringUtil.Concat( StringUtil.RTrim( AV57WWEmailDS_11_Email_titulo3), "%", "");
         lV57WWEmailDS_11_Email_titulo3 = StringUtil.Concat( StringUtil.RTrim( AV57WWEmailDS_11_Email_titulo3), "%", "");
         lV58WWEmailDS_12_Tfemail_titulo = StringUtil.Concat( StringUtil.RTrim( AV58WWEmailDS_12_Tfemail_titulo), "%", "");
         lV60WWEmailDS_14_Tfemail_key = StringUtil.Concat( StringUtil.RTrim( AV60WWEmailDS_14_Tfemail_key), "%", "");
         /* Using cursor H00MS3 */
         pr_default.execute(1, new Object[] {lV49WWEmailDS_3_Email_titulo1, lV49WWEmailDS_3_Email_titulo1, lV53WWEmailDS_7_Email_titulo2, lV53WWEmailDS_7_Email_titulo2, lV57WWEmailDS_11_Email_titulo3, lV57WWEmailDS_11_Email_titulo3, lV58WWEmailDS_12_Tfemail_titulo, AV59WWEmailDS_13_Tfemail_titulo_sel, lV60WWEmailDS_14_Tfemail_key, AV61WWEmailDS_15_Tfemail_key_sel});
         GRID_nRecordCount = H00MS3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV47WWEmailDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV48WWEmailDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV49WWEmailDS_3_Email_titulo1 = AV17Email_Titulo1;
         AV50WWEmailDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV51WWEmailDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV52WWEmailDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV53WWEmailDS_7_Email_titulo2 = AV21Email_Titulo2;
         AV54WWEmailDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV55WWEmailDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV56WWEmailDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV57WWEmailDS_11_Email_titulo3 = AV25Email_Titulo3;
         AV58WWEmailDS_12_Tfemail_titulo = AV34TFEmail_Titulo;
         AV59WWEmailDS_13_Tfemail_titulo_sel = AV35TFEmail_Titulo_Sel;
         AV60WWEmailDS_14_Tfemail_key = AV38TFEmail_Key;
         AV61WWEmailDS_15_Tfemail_key_sel = AV39TFEmail_Key_Sel;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Email_Titulo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Email_Titulo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Email_Titulo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFEmail_Titulo, AV35TFEmail_Titulo_Sel, AV38TFEmail_Key, AV39TFEmail_Key_Sel, AV36ddo_Email_TituloTitleControlIdToReplace, AV40ddo_Email_KeyTitleControlIdToReplace, AV64Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1665Email_Guid) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV47WWEmailDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV48WWEmailDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV49WWEmailDS_3_Email_titulo1 = AV17Email_Titulo1;
         AV50WWEmailDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV51WWEmailDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV52WWEmailDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV53WWEmailDS_7_Email_titulo2 = AV21Email_Titulo2;
         AV54WWEmailDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV55WWEmailDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV56WWEmailDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV57WWEmailDS_11_Email_titulo3 = AV25Email_Titulo3;
         AV58WWEmailDS_12_Tfemail_titulo = AV34TFEmail_Titulo;
         AV59WWEmailDS_13_Tfemail_titulo_sel = AV35TFEmail_Titulo_Sel;
         AV60WWEmailDS_14_Tfemail_key = AV38TFEmail_Key;
         AV61WWEmailDS_15_Tfemail_key_sel = AV39TFEmail_Key_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Email_Titulo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Email_Titulo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Email_Titulo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFEmail_Titulo, AV35TFEmail_Titulo_Sel, AV38TFEmail_Key, AV39TFEmail_Key_Sel, AV36ddo_Email_TituloTitleControlIdToReplace, AV40ddo_Email_KeyTitleControlIdToReplace, AV64Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1665Email_Guid) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV47WWEmailDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV48WWEmailDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV49WWEmailDS_3_Email_titulo1 = AV17Email_Titulo1;
         AV50WWEmailDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV51WWEmailDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV52WWEmailDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV53WWEmailDS_7_Email_titulo2 = AV21Email_Titulo2;
         AV54WWEmailDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV55WWEmailDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV56WWEmailDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV57WWEmailDS_11_Email_titulo3 = AV25Email_Titulo3;
         AV58WWEmailDS_12_Tfemail_titulo = AV34TFEmail_Titulo;
         AV59WWEmailDS_13_Tfemail_titulo_sel = AV35TFEmail_Titulo_Sel;
         AV60WWEmailDS_14_Tfemail_key = AV38TFEmail_Key;
         AV61WWEmailDS_15_Tfemail_key_sel = AV39TFEmail_Key_Sel;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Email_Titulo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Email_Titulo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Email_Titulo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFEmail_Titulo, AV35TFEmail_Titulo_Sel, AV38TFEmail_Key, AV39TFEmail_Key_Sel, AV36ddo_Email_TituloTitleControlIdToReplace, AV40ddo_Email_KeyTitleControlIdToReplace, AV64Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1665Email_Guid) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV47WWEmailDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV48WWEmailDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV49WWEmailDS_3_Email_titulo1 = AV17Email_Titulo1;
         AV50WWEmailDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV51WWEmailDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV52WWEmailDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV53WWEmailDS_7_Email_titulo2 = AV21Email_Titulo2;
         AV54WWEmailDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV55WWEmailDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV56WWEmailDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV57WWEmailDS_11_Email_titulo3 = AV25Email_Titulo3;
         AV58WWEmailDS_12_Tfemail_titulo = AV34TFEmail_Titulo;
         AV59WWEmailDS_13_Tfemail_titulo_sel = AV35TFEmail_Titulo_Sel;
         AV60WWEmailDS_14_Tfemail_key = AV38TFEmail_Key;
         AV61WWEmailDS_15_Tfemail_key_sel = AV39TFEmail_Key_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Email_Titulo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Email_Titulo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Email_Titulo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFEmail_Titulo, AV35TFEmail_Titulo_Sel, AV38TFEmail_Key, AV39TFEmail_Key_Sel, AV36ddo_Email_TituloTitleControlIdToReplace, AV40ddo_Email_KeyTitleControlIdToReplace, AV64Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1665Email_Guid) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV47WWEmailDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV48WWEmailDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV49WWEmailDS_3_Email_titulo1 = AV17Email_Titulo1;
         AV50WWEmailDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV51WWEmailDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV52WWEmailDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV53WWEmailDS_7_Email_titulo2 = AV21Email_Titulo2;
         AV54WWEmailDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV55WWEmailDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV56WWEmailDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV57WWEmailDS_11_Email_titulo3 = AV25Email_Titulo3;
         AV58WWEmailDS_12_Tfemail_titulo = AV34TFEmail_Titulo;
         AV59WWEmailDS_13_Tfemail_titulo_sel = AV35TFEmail_Titulo_Sel;
         AV60WWEmailDS_14_Tfemail_key = AV38TFEmail_Key;
         AV61WWEmailDS_15_Tfemail_key_sel = AV39TFEmail_Key_Sel;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Email_Titulo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Email_Titulo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Email_Titulo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFEmail_Titulo, AV35TFEmail_Titulo_Sel, AV38TFEmail_Key, AV39TFEmail_Key_Sel, AV36ddo_Email_TituloTitleControlIdToReplace, AV40ddo_Email_KeyTitleControlIdToReplace, AV64Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1665Email_Guid) ;
         }
         return (int)(0) ;
      }

      protected void STRUPMS0( )
      {
         /* Before Start, stand alone formulas. */
         AV64Pgmname = "WWEmail";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E25MS2 */
         E25MS2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV41DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vEMAIL_TITULOTITLEFILTERDATA"), AV33Email_TituloTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vEMAIL_KEYTITLEFILTERDATA"), AV37Email_KeyTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            AV17Email_Titulo1 = cgiGet( edtavEmail_titulo1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Email_Titulo1", AV17Email_Titulo1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            AV21Email_Titulo2 = cgiGet( edtavEmail_titulo2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Email_Titulo2", AV21Email_Titulo2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            AV25Email_Titulo3 = cgiGet( edtavEmail_titulo3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Email_Titulo3", AV25Email_Titulo3);
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            AV34TFEmail_Titulo = cgiGet( edtavTfemail_titulo_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFEmail_Titulo", AV34TFEmail_Titulo);
            AV35TFEmail_Titulo_Sel = cgiGet( edtavTfemail_titulo_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFEmail_Titulo_Sel", AV35TFEmail_Titulo_Sel);
            AV38TFEmail_Key = cgiGet( edtavTfemail_key_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFEmail_Key", AV38TFEmail_Key);
            AV39TFEmail_Key_Sel = cgiGet( edtavTfemail_key_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFEmail_Key_Sel", AV39TFEmail_Key_Sel);
            AV36ddo_Email_TituloTitleControlIdToReplace = cgiGet( edtavDdo_email_titulotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ddo_Email_TituloTitleControlIdToReplace", AV36ddo_Email_TituloTitleControlIdToReplace);
            AV40ddo_Email_KeyTitleControlIdToReplace = cgiGet( edtavDdo_email_keytitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ddo_Email_KeyTitleControlIdToReplace", AV40ddo_Email_KeyTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_88 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_88"), ",", "."));
            AV43GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV44GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_email_titulo_Caption = cgiGet( "DDO_EMAIL_TITULO_Caption");
            Ddo_email_titulo_Tooltip = cgiGet( "DDO_EMAIL_TITULO_Tooltip");
            Ddo_email_titulo_Cls = cgiGet( "DDO_EMAIL_TITULO_Cls");
            Ddo_email_titulo_Filteredtext_set = cgiGet( "DDO_EMAIL_TITULO_Filteredtext_set");
            Ddo_email_titulo_Selectedvalue_set = cgiGet( "DDO_EMAIL_TITULO_Selectedvalue_set");
            Ddo_email_titulo_Dropdownoptionstype = cgiGet( "DDO_EMAIL_TITULO_Dropdownoptionstype");
            Ddo_email_titulo_Titlecontrolidtoreplace = cgiGet( "DDO_EMAIL_TITULO_Titlecontrolidtoreplace");
            Ddo_email_titulo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_EMAIL_TITULO_Includesortasc"));
            Ddo_email_titulo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_EMAIL_TITULO_Includesortdsc"));
            Ddo_email_titulo_Sortedstatus = cgiGet( "DDO_EMAIL_TITULO_Sortedstatus");
            Ddo_email_titulo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_EMAIL_TITULO_Includefilter"));
            Ddo_email_titulo_Filtertype = cgiGet( "DDO_EMAIL_TITULO_Filtertype");
            Ddo_email_titulo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_EMAIL_TITULO_Filterisrange"));
            Ddo_email_titulo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_EMAIL_TITULO_Includedatalist"));
            Ddo_email_titulo_Datalisttype = cgiGet( "DDO_EMAIL_TITULO_Datalisttype");
            Ddo_email_titulo_Datalistproc = cgiGet( "DDO_EMAIL_TITULO_Datalistproc");
            Ddo_email_titulo_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_EMAIL_TITULO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_email_titulo_Sortasc = cgiGet( "DDO_EMAIL_TITULO_Sortasc");
            Ddo_email_titulo_Sortdsc = cgiGet( "DDO_EMAIL_TITULO_Sortdsc");
            Ddo_email_titulo_Loadingdata = cgiGet( "DDO_EMAIL_TITULO_Loadingdata");
            Ddo_email_titulo_Cleanfilter = cgiGet( "DDO_EMAIL_TITULO_Cleanfilter");
            Ddo_email_titulo_Noresultsfound = cgiGet( "DDO_EMAIL_TITULO_Noresultsfound");
            Ddo_email_titulo_Searchbuttontext = cgiGet( "DDO_EMAIL_TITULO_Searchbuttontext");
            Ddo_email_key_Caption = cgiGet( "DDO_EMAIL_KEY_Caption");
            Ddo_email_key_Tooltip = cgiGet( "DDO_EMAIL_KEY_Tooltip");
            Ddo_email_key_Cls = cgiGet( "DDO_EMAIL_KEY_Cls");
            Ddo_email_key_Filteredtext_set = cgiGet( "DDO_EMAIL_KEY_Filteredtext_set");
            Ddo_email_key_Selectedvalue_set = cgiGet( "DDO_EMAIL_KEY_Selectedvalue_set");
            Ddo_email_key_Dropdownoptionstype = cgiGet( "DDO_EMAIL_KEY_Dropdownoptionstype");
            Ddo_email_key_Titlecontrolidtoreplace = cgiGet( "DDO_EMAIL_KEY_Titlecontrolidtoreplace");
            Ddo_email_key_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_EMAIL_KEY_Includesortasc"));
            Ddo_email_key_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_EMAIL_KEY_Includesortdsc"));
            Ddo_email_key_Sortedstatus = cgiGet( "DDO_EMAIL_KEY_Sortedstatus");
            Ddo_email_key_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_EMAIL_KEY_Includefilter"));
            Ddo_email_key_Filtertype = cgiGet( "DDO_EMAIL_KEY_Filtertype");
            Ddo_email_key_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_EMAIL_KEY_Filterisrange"));
            Ddo_email_key_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_EMAIL_KEY_Includedatalist"));
            Ddo_email_key_Datalisttype = cgiGet( "DDO_EMAIL_KEY_Datalisttype");
            Ddo_email_key_Datalistproc = cgiGet( "DDO_EMAIL_KEY_Datalistproc");
            Ddo_email_key_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_EMAIL_KEY_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_email_key_Sortasc = cgiGet( "DDO_EMAIL_KEY_Sortasc");
            Ddo_email_key_Sortdsc = cgiGet( "DDO_EMAIL_KEY_Sortdsc");
            Ddo_email_key_Loadingdata = cgiGet( "DDO_EMAIL_KEY_Loadingdata");
            Ddo_email_key_Cleanfilter = cgiGet( "DDO_EMAIL_KEY_Cleanfilter");
            Ddo_email_key_Noresultsfound = cgiGet( "DDO_EMAIL_KEY_Noresultsfound");
            Ddo_email_key_Searchbuttontext = cgiGet( "DDO_EMAIL_KEY_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_email_titulo_Activeeventkey = cgiGet( "DDO_EMAIL_TITULO_Activeeventkey");
            Ddo_email_titulo_Filteredtext_get = cgiGet( "DDO_EMAIL_TITULO_Filteredtext_get");
            Ddo_email_titulo_Selectedvalue_get = cgiGet( "DDO_EMAIL_TITULO_Selectedvalue_get");
            Ddo_email_key_Activeeventkey = cgiGet( "DDO_EMAIL_KEY_Activeeventkey");
            Ddo_email_key_Filteredtext_get = cgiGet( "DDO_EMAIL_KEY_Filteredtext_get");
            Ddo_email_key_Selectedvalue_get = cgiGet( "DDO_EMAIL_KEY_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vEMAIL_TITULO1"), AV17Email_Titulo1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vEMAIL_TITULO2"), AV21Email_Titulo2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vEMAIL_TITULO3"), AV25Email_Titulo3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFEMAIL_TITULO"), AV34TFEmail_Titulo) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFEMAIL_TITULO_SEL"), AV35TFEmail_Titulo_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFEMAIL_KEY"), AV38TFEmail_Key) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFEMAIL_KEY_SEL"), AV39TFEmail_Key_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E25MS2 */
         E25MS2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E25MS2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "EMAIL_TITULO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "EMAIL_TITULO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector3 = "EMAIL_TITULO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfemail_titulo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfemail_titulo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfemail_titulo_Visible), 5, 0)));
         edtavTfemail_titulo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfemail_titulo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfemail_titulo_sel_Visible), 5, 0)));
         edtavTfemail_key_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfemail_key_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfemail_key_Visible), 5, 0)));
         edtavTfemail_key_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfemail_key_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfemail_key_sel_Visible), 5, 0)));
         Ddo_email_titulo_Titlecontrolidtoreplace = subGrid_Internalname+"_Email_Titulo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_email_titulo_Internalname, "TitleControlIdToReplace", Ddo_email_titulo_Titlecontrolidtoreplace);
         AV36ddo_Email_TituloTitleControlIdToReplace = Ddo_email_titulo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ddo_Email_TituloTitleControlIdToReplace", AV36ddo_Email_TituloTitleControlIdToReplace);
         edtavDdo_email_titulotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_email_titulotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_email_titulotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_email_key_Titlecontrolidtoreplace = subGrid_Internalname+"_Email_Key";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_email_key_Internalname, "TitleControlIdToReplace", Ddo_email_key_Titlecontrolidtoreplace);
         AV40ddo_Email_KeyTitleControlIdToReplace = Ddo_email_key_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ddo_Email_KeyTitleControlIdToReplace", AV40ddo_Email_KeyTitleControlIdToReplace);
         edtavDdo_email_keytitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_email_keytitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_email_keytitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Email";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Assunto", 0);
         cmbavOrderedby.addItem("2", "Key", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV41DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV41DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E26MS2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV33Email_TituloTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV37Email_KeyTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtEmail_Titulo_Titleformat = 2;
         edtEmail_Titulo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Assunto", AV36ddo_Email_TituloTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEmail_Titulo_Internalname, "Title", edtEmail_Titulo_Title);
         edtEmail_Key_Titleformat = 2;
         edtEmail_Key_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Key", AV40ddo_Email_KeyTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEmail_Key_Internalname, "Title", edtEmail_Key_Title);
         AV43GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43GridCurrentPage), 10, 0)));
         AV44GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44GridPageCount), 10, 0)));
         AV47WWEmailDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV48WWEmailDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV49WWEmailDS_3_Email_titulo1 = AV17Email_Titulo1;
         AV50WWEmailDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV51WWEmailDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV52WWEmailDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV53WWEmailDS_7_Email_titulo2 = AV21Email_Titulo2;
         AV54WWEmailDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV55WWEmailDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV56WWEmailDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV57WWEmailDS_11_Email_titulo3 = AV25Email_Titulo3;
         AV58WWEmailDS_12_Tfemail_titulo = AV34TFEmail_Titulo;
         AV59WWEmailDS_13_Tfemail_titulo_sel = AV35TFEmail_Titulo_Sel;
         AV60WWEmailDS_14_Tfemail_key = AV38TFEmail_Key;
         AV61WWEmailDS_15_Tfemail_key_sel = AV39TFEmail_Key_Sel;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV33Email_TituloTitleFilterData", AV33Email_TituloTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV37Email_KeyTitleFilterData", AV37Email_KeyTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11MS2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV42PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV42PageToGo) ;
         }
      }

      protected void E12MS2( )
      {
         /* Ddo_email_titulo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_email_titulo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_email_titulo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_email_titulo_Internalname, "SortedStatus", Ddo_email_titulo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_email_titulo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_email_titulo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_email_titulo_Internalname, "SortedStatus", Ddo_email_titulo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_email_titulo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV34TFEmail_Titulo = Ddo_email_titulo_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFEmail_Titulo", AV34TFEmail_Titulo);
            AV35TFEmail_Titulo_Sel = Ddo_email_titulo_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFEmail_Titulo_Sel", AV35TFEmail_Titulo_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13MS2( )
      {
         /* Ddo_email_key_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_email_key_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_email_key_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_email_key_Internalname, "SortedStatus", Ddo_email_key_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_email_key_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_email_key_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_email_key_Internalname, "SortedStatus", Ddo_email_key_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_email_key_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV38TFEmail_Key = Ddo_email_key_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFEmail_Key", AV38TFEmail_Key);
            AV39TFEmail_Key_Sel = Ddo_email_key_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFEmail_Key_Sel", AV39TFEmail_Key_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E27MS2( )
      {
         /* Grid_Load Routine */
         AV28Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV28Update);
         AV62Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("email.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode(A1665Email_Guid.ToString());
         AV29Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV29Delete);
         AV63Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("email.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode(A1665Email_Guid.ToString());
         edtEmail_Titulo_Link = formatLink("viewemail.aspx") + "?" + UrlEncode(A1665Email_Guid.ToString()) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 88;
         }
         sendrow_882( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_88_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(88, GridRow);
         }
      }

      protected void E14MS2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E20MS2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E15MS2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Email_Titulo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Email_Titulo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Email_Titulo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFEmail_Titulo, AV35TFEmail_Titulo_Sel, AV38TFEmail_Key, AV39TFEmail_Key_Sel, AV36ddo_Email_TituloTitleControlIdToReplace, AV40ddo_Email_KeyTitleControlIdToReplace, AV64Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1665Email_Guid) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E21MS2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E22MS2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E16MS2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Email_Titulo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Email_Titulo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Email_Titulo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFEmail_Titulo, AV35TFEmail_Titulo_Sel, AV38TFEmail_Key, AV39TFEmail_Key_Sel, AV36ddo_Email_TituloTitleControlIdToReplace, AV40ddo_Email_KeyTitleControlIdToReplace, AV64Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1665Email_Guid) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E23MS2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E17MS2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Email_Titulo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Email_Titulo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Email_Titulo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFEmail_Titulo, AV35TFEmail_Titulo_Sel, AV38TFEmail_Key, AV39TFEmail_Key_Sel, AV36ddo_Email_TituloTitleControlIdToReplace, AV40ddo_Email_KeyTitleControlIdToReplace, AV64Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1665Email_Guid) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E24MS2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E18MS2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E19MS2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("email.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode(System.Guid.Empty.ToString());
         context.wjLocDisableFrm = 1;
      }

      protected void S182( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_email_titulo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_email_titulo_Internalname, "SortedStatus", Ddo_email_titulo_Sortedstatus);
         Ddo_email_key_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_email_key_Internalname, "SortedStatus", Ddo_email_key_Sortedstatus);
      }

      protected void S162( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_email_titulo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_email_titulo_Internalname, "SortedStatus", Ddo_email_titulo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_email_key_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_email_key_Internalname, "SortedStatus", Ddo_email_key_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavEmail_titulo1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEmail_titulo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEmail_titulo1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "EMAIL_TITULO") == 0 )
         {
            edtavEmail_titulo1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEmail_titulo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEmail_titulo1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavEmail_titulo2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEmail_titulo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEmail_titulo2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "EMAIL_TITULO") == 0 )
         {
            edtavEmail_titulo2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEmail_titulo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEmail_titulo2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavEmail_titulo3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEmail_titulo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEmail_titulo3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "EMAIL_TITULO") == 0 )
         {
            edtavEmail_titulo3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEmail_titulo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEmail_titulo3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S202( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "EMAIL_TITULO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV20DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         AV21Email_Titulo2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Email_Titulo2", AV21Email_Titulo2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "EMAIL_TITULO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV24DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         AV25Email_Titulo3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Email_Titulo3", AV25Email_Titulo3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S222( )
      {
         /* 'CLEANFILTERS' Routine */
         AV34TFEmail_Titulo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFEmail_Titulo", AV34TFEmail_Titulo);
         Ddo_email_titulo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_email_titulo_Internalname, "FilteredText_set", Ddo_email_titulo_Filteredtext_set);
         AV35TFEmail_Titulo_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFEmail_Titulo_Sel", AV35TFEmail_Titulo_Sel);
         Ddo_email_titulo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_email_titulo_Internalname, "SelectedValue_set", Ddo_email_titulo_Selectedvalue_set);
         AV38TFEmail_Key = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFEmail_Key", AV38TFEmail_Key);
         Ddo_email_key_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_email_key_Internalname, "FilteredText_set", Ddo_email_key_Filteredtext_set);
         AV39TFEmail_Key_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFEmail_Key_Sel", AV39TFEmail_Key_Sel);
         Ddo_email_key_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_email_key_Internalname, "SelectedValue_set", Ddo_email_key_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "EMAIL_TITULO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         AV17Email_Titulo1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Email_Titulo1", AV17Email_Titulo1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV30Session.Get(AV64Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV64Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV30Session.Get(AV64Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S232( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV65GXV1 = 1;
         while ( AV65GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV65GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFEMAIL_TITULO") == 0 )
            {
               AV34TFEmail_Titulo = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFEmail_Titulo", AV34TFEmail_Titulo);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34TFEmail_Titulo)) )
               {
                  Ddo_email_titulo_Filteredtext_set = AV34TFEmail_Titulo;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_email_titulo_Internalname, "FilteredText_set", Ddo_email_titulo_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFEMAIL_TITULO_SEL") == 0 )
            {
               AV35TFEmail_Titulo_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFEmail_Titulo_Sel", AV35TFEmail_Titulo_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35TFEmail_Titulo_Sel)) )
               {
                  Ddo_email_titulo_Selectedvalue_set = AV35TFEmail_Titulo_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_email_titulo_Internalname, "SelectedValue_set", Ddo_email_titulo_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFEMAIL_KEY") == 0 )
            {
               AV38TFEmail_Key = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFEmail_Key", AV38TFEmail_Key);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38TFEmail_Key)) )
               {
                  Ddo_email_key_Filteredtext_set = AV38TFEmail_Key;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_email_key_Internalname, "FilteredText_set", Ddo_email_key_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFEMAIL_KEY_SEL") == 0 )
            {
               AV39TFEmail_Key_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFEmail_Key_Sel", AV39TFEmail_Key_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFEmail_Key_Sel)) )
               {
                  Ddo_email_key_Selectedvalue_set = AV39TFEmail_Key_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_email_key_Internalname, "SelectedValue_set", Ddo_email_key_Selectedvalue_set);
               }
            }
            AV65GXV1 = (int)(AV65GXV1+1);
         }
      }

      protected void S212( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "EMAIL_TITULO") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17Email_Titulo1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Email_Titulo1", AV17Email_Titulo1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "EMAIL_TITULO") == 0 )
               {
                  AV20DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
                  AV21Email_Titulo2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Email_Titulo2", AV21Email_Titulo2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "EMAIL_TITULO") == 0 )
                  {
                     AV24DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
                     AV25Email_Titulo3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Email_Titulo3", AV25Email_Titulo3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S172( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV30Session.Get(AV64Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34TFEmail_Titulo)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFEMAIL_TITULO";
            AV11GridStateFilterValue.gxTpr_Value = AV34TFEmail_Titulo;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35TFEmail_Titulo_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFEMAIL_TITULO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV35TFEmail_Titulo_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38TFEmail_Key)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFEMAIL_KEY";
            AV11GridStateFilterValue.gxTpr_Value = AV38TFEmail_Key;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFEmail_Key_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFEMAIL_KEY_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV39TFEmail_Key_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV64Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S192( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "EMAIL_TITULO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Email_Titulo1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17Email_Titulo1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "EMAIL_TITULO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV21Email_Titulo2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV21Email_Titulo2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV20DynamicFiltersOperator2;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "EMAIL_TITULO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV25Email_Titulo3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV25Email_Titulo3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV24DynamicFiltersOperator3;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV64Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Email";
         AV30Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_MS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_MS2( true) ;
         }
         else
         {
            wb_table2_8_MS2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_MS2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_82_MS2( true) ;
         }
         else
         {
            wb_table3_82_MS2( false) ;
         }
         return  ;
      }

      protected void wb_table3_82_MS2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_MS2e( true) ;
         }
         else
         {
            wb_table1_2_MS2e( false) ;
         }
      }

      protected void wb_table3_82_MS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_85_MS2( true) ;
         }
         else
         {
            wb_table4_85_MS2( false) ;
         }
         return  ;
      }

      protected void wb_table4_85_MS2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_82_MS2e( true) ;
         }
         else
         {
            wb_table3_82_MS2e( false) ;
         }
      }

      protected void wb_table4_85_MS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"88\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Email_Guid") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtEmail_Titulo_Titleformat == 0 )
               {
                  context.SendWebValue( edtEmail_Titulo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtEmail_Titulo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtEmail_Key_Titleformat == 0 )
               {
                  context.SendWebValue( edtEmail_Key_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtEmail_Key_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV29Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A1665Email_Guid.ToString());
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A1669Email_Titulo);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtEmail_Titulo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtEmail_Titulo_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtEmail_Titulo_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A1672Email_Key);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtEmail_Key_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtEmail_Key_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 88 )
         {
            wbEnd = 0;
            nRC_GXsfl_88 = (short)(nGXsfl_88_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_85_MS2e( true) ;
         }
         else
         {
            wb_table4_85_MS2e( false) ;
         }
      }

      protected void wb_table2_8_MS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblEmailtitle_Internalname, "Email", "", "", lblEmailtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWEmail.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_13_MS2( true) ;
         }
         else
         {
            wb_table5_13_MS2( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_MS2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWEmail.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWEmail.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWEmail.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_23_MS2( true) ;
         }
         else
         {
            wb_table6_23_MS2( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_MS2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_MS2e( true) ;
         }
         else
         {
            wb_table2_8_MS2e( false) ;
         }
      }

      protected void wb_table6_23_MS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWEmail.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_MS2( true) ;
         }
         else
         {
            wb_table7_28_MS2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_MS2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWEmail.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_MS2e( true) ;
         }
         else
         {
            wb_table6_23_MS2e( false) ;
         }
      }

      protected void wb_table7_28_MS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWEmail.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_WWEmail.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWEmail.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_37_MS2( true) ;
         }
         else
         {
            wb_table8_37_MS2( false) ;
         }
         return  ;
      }

      protected void wb_table8_37_MS2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWEmail.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWEmail.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWEmail.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", "", true, "HLP_WWEmail.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWEmail.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_54_MS2( true) ;
         }
         else
         {
            wb_table9_54_MS2( false) ;
         }
         return  ;
      }

      protected void wb_table9_54_MS2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWEmail.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWEmail.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWEmail.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,67);\"", "", true, "HLP_WWEmail.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWEmail.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_71_MS2( true) ;
         }
         else
         {
            wb_table10_71_MS2( false) ;
         }
         return  ;
      }

      protected void wb_table10_71_MS2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWEmail.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_MS2e( true) ;
         }
         else
         {
            wb_table7_28_MS2e( false) ;
         }
      }

      protected void wb_table10_71_MS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,74);\"", "", true, "HLP_WWEmail.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavEmail_titulo3_Internalname, AV25Email_Titulo3, StringUtil.RTrim( context.localUtil.Format( AV25Email_Titulo3, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,76);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavEmail_titulo3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavEmail_titulo3_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWEmail.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_71_MS2e( true) ;
         }
         else
         {
            wb_table10_71_MS2e( false) ;
         }
      }

      protected void wb_table9_54_MS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,57);\"", "", true, "HLP_WWEmail.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavEmail_titulo2_Internalname, AV21Email_Titulo2, StringUtil.RTrim( context.localUtil.Format( AV21Email_Titulo2, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavEmail_titulo2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavEmail_titulo2_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWEmail.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_54_MS2e( true) ;
         }
         else
         {
            wb_table9_54_MS2e( false) ;
         }
      }

      protected void wb_table8_37_MS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", "", true, "HLP_WWEmail.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavEmail_titulo1_Internalname, AV17Email_Titulo1, StringUtil.RTrim( context.localUtil.Format( AV17Email_Titulo1, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavEmail_titulo1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavEmail_titulo1_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWEmail.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_37_MS2e( true) ;
         }
         else
         {
            wb_table8_37_MS2e( false) ;
         }
      }

      protected void wb_table5_13_MS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWEmail.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_MS2e( true) ;
         }
         else
         {
            wb_table5_13_MS2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAMS2( ) ;
         WSMS2( ) ;
         WEMS2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311935246");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwemail.js", "?2020311935246");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_882( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_88_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_88_idx;
         edtEmail_Guid_Internalname = "EMAIL_GUID_"+sGXsfl_88_idx;
         edtEmail_Titulo_Internalname = "EMAIL_TITULO_"+sGXsfl_88_idx;
         edtEmail_Key_Internalname = "EMAIL_KEY_"+sGXsfl_88_idx;
      }

      protected void SubsflControlProps_fel_882( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_88_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_88_fel_idx;
         edtEmail_Guid_Internalname = "EMAIL_GUID_"+sGXsfl_88_fel_idx;
         edtEmail_Titulo_Internalname = "EMAIL_TITULO_"+sGXsfl_88_fel_idx;
         edtEmail_Key_Internalname = "EMAIL_KEY_"+sGXsfl_88_fel_idx;
      }

      protected void sendrow_882( )
      {
         SubsflControlProps_882( ) ;
         WBMS0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_88_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_88_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_88_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV28Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV62Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV62Update_GXI : context.PathToRelativeUrl( AV28Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV28Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV29Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV63Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV63Delete_GXI : context.PathToRelativeUrl( AV29Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV29Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtEmail_Guid_Internalname,A1665Email_Guid.ToString(),A1665Email_Guid.ToString(),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtEmail_Guid_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)36,(short)0,(short)0,(short)88,(short)1,(short)0,(short)0,(bool)true,(String)"",(String)"",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtEmail_Titulo_Internalname,(String)A1669Email_Titulo,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtEmail_Titulo_Link,(String)"",(String)"",(String)"",(String)edtEmail_Titulo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)88,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtEmail_Key_Internalname,(String)A1672Email_Key,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtEmail_Key_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)40,(short)0,(short)0,(short)88,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, "gxhash_EMAIL_GUID"+"_"+sGXsfl_88_idx, GetSecureSignedToken( sGXsfl_88_idx, A1665Email_Guid));
            GxWebStd.gx_hidden_field( context, "gxhash_EMAIL_TITULO"+"_"+sGXsfl_88_idx, GetSecureSignedToken( sGXsfl_88_idx, StringUtil.RTrim( context.localUtil.Format( A1669Email_Titulo, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_EMAIL_KEY"+"_"+sGXsfl_88_idx, GetSecureSignedToken( sGXsfl_88_idx, StringUtil.RTrim( context.localUtil.Format( A1672Email_Key, ""))));
            GridContainer.AddRow(GridRow);
            nGXsfl_88_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_88_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_88_idx+1));
            sGXsfl_88_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_88_idx), 4, 0)), 4, "0");
            SubsflControlProps_882( ) ;
         }
         /* End function sendrow_882 */
      }

      protected void init_default_properties( )
      {
         lblEmailtitle_Internalname = "EMAILTITLE";
         imgInsert_Internalname = "INSERT";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavEmail_titulo1_Internalname = "vEMAIL_TITULO1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavEmail_titulo2_Internalname = "vEMAIL_TITULO2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavEmail_titulo3_Internalname = "vEMAIL_TITULO3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtEmail_Guid_Internalname = "EMAIL_GUID";
         edtEmail_Titulo_Internalname = "EMAIL_TITULO";
         edtEmail_Key_Internalname = "EMAIL_KEY";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfemail_titulo_Internalname = "vTFEMAIL_TITULO";
         edtavTfemail_titulo_sel_Internalname = "vTFEMAIL_TITULO_SEL";
         edtavTfemail_key_Internalname = "vTFEMAIL_KEY";
         edtavTfemail_key_sel_Internalname = "vTFEMAIL_KEY_SEL";
         Ddo_email_titulo_Internalname = "DDO_EMAIL_TITULO";
         edtavDdo_email_titulotitlecontrolidtoreplace_Internalname = "vDDO_EMAIL_TITULOTITLECONTROLIDTOREPLACE";
         Ddo_email_key_Internalname = "DDO_EMAIL_KEY";
         edtavDdo_email_keytitlecontrolidtoreplace_Internalname = "vDDO_EMAIL_KEYTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtEmail_Key_Jsonclick = "";
         edtEmail_Titulo_Jsonclick = "";
         edtEmail_Guid_Jsonclick = "";
         edtavEmail_titulo1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavEmail_titulo2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavEmail_titulo3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtEmail_Titulo_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtEmail_Key_Titleformat = 0;
         edtEmail_Titulo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavEmail_titulo3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavEmail_titulo2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavEmail_titulo1_Visible = 1;
         edtEmail_Key_Title = "Key";
         edtEmail_Titulo_Title = "Assunto";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_email_keytitlecontrolidtoreplace_Visible = 1;
         edtavDdo_email_titulotitlecontrolidtoreplace_Visible = 1;
         edtavTfemail_key_sel_Jsonclick = "";
         edtavTfemail_key_sel_Visible = 1;
         edtavTfemail_key_Jsonclick = "";
         edtavTfemail_key_Visible = 1;
         edtavTfemail_titulo_sel_Jsonclick = "";
         edtavTfemail_titulo_sel_Visible = 1;
         edtavTfemail_titulo_Jsonclick = "";
         edtavTfemail_titulo_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_email_key_Searchbuttontext = "Pesquisar";
         Ddo_email_key_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_email_key_Cleanfilter = "Limpar pesquisa";
         Ddo_email_key_Loadingdata = "Carregando dados...";
         Ddo_email_key_Sortdsc = "Ordenar de Z � A";
         Ddo_email_key_Sortasc = "Ordenar de A � Z";
         Ddo_email_key_Datalistupdateminimumcharacters = 0;
         Ddo_email_key_Datalistproc = "GetWWEmailFilterData";
         Ddo_email_key_Datalisttype = "Dynamic";
         Ddo_email_key_Includedatalist = Convert.ToBoolean( -1);
         Ddo_email_key_Filterisrange = Convert.ToBoolean( 0);
         Ddo_email_key_Filtertype = "Character";
         Ddo_email_key_Includefilter = Convert.ToBoolean( -1);
         Ddo_email_key_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_email_key_Includesortasc = Convert.ToBoolean( -1);
         Ddo_email_key_Titlecontrolidtoreplace = "";
         Ddo_email_key_Dropdownoptionstype = "GridTitleSettings";
         Ddo_email_key_Cls = "ColumnSettings";
         Ddo_email_key_Tooltip = "Op��es";
         Ddo_email_key_Caption = "";
         Ddo_email_titulo_Searchbuttontext = "Pesquisar";
         Ddo_email_titulo_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_email_titulo_Cleanfilter = "Limpar pesquisa";
         Ddo_email_titulo_Loadingdata = "Carregando dados...";
         Ddo_email_titulo_Sortdsc = "Ordenar de Z � A";
         Ddo_email_titulo_Sortasc = "Ordenar de A � Z";
         Ddo_email_titulo_Datalistupdateminimumcharacters = 0;
         Ddo_email_titulo_Datalistproc = "GetWWEmailFilterData";
         Ddo_email_titulo_Datalisttype = "Dynamic";
         Ddo_email_titulo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_email_titulo_Filterisrange = Convert.ToBoolean( 0);
         Ddo_email_titulo_Filtertype = "Character";
         Ddo_email_titulo_Includefilter = Convert.ToBoolean( -1);
         Ddo_email_titulo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_email_titulo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_email_titulo_Titlecontrolidtoreplace = "";
         Ddo_email_titulo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_email_titulo_Cls = "ColumnSettings";
         Ddo_email_titulo_Tooltip = "Op��es";
         Ddo_email_titulo_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Email";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A1665Email_Guid',fld:'EMAIL_GUID',pic:'',hsh:true,nv:'00000000-0000-0000-0000-000000000000'},{av:'AV36ddo_Email_TituloTitleControlIdToReplace',fld:'vDDO_EMAIL_TITULOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Email_KeyTitleControlIdToReplace',fld:'vDDO_EMAIL_KEYTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Email_Titulo1',fld:'vEMAIL_TITULO1',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Email_Titulo2',fld:'vEMAIL_TITULO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Email_Titulo3',fld:'vEMAIL_TITULO3',pic:'',nv:''},{av:'AV34TFEmail_Titulo',fld:'vTFEMAIL_TITULO',pic:'',nv:''},{av:'AV35TFEmail_Titulo_Sel',fld:'vTFEMAIL_TITULO_SEL',pic:'',nv:''},{av:'AV38TFEmail_Key',fld:'vTFEMAIL_KEY',pic:'',nv:''},{av:'AV39TFEmail_Key_Sel',fld:'vTFEMAIL_KEY_SEL',pic:'',nv:''},{av:'AV64Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV33Email_TituloTitleFilterData',fld:'vEMAIL_TITULOTITLEFILTERDATA',pic:'',nv:null},{av:'AV37Email_KeyTitleFilterData',fld:'vEMAIL_KEYTITLEFILTERDATA',pic:'',nv:null},{av:'edtEmail_Titulo_Titleformat',ctrl:'EMAIL_TITULO',prop:'Titleformat'},{av:'edtEmail_Titulo_Title',ctrl:'EMAIL_TITULO',prop:'Title'},{av:'edtEmail_Key_Titleformat',ctrl:'EMAIL_KEY',prop:'Titleformat'},{av:'edtEmail_Key_Title',ctrl:'EMAIL_KEY',prop:'Title'},{av:'AV43GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV44GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11MS2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Email_Titulo1',fld:'vEMAIL_TITULO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Email_Titulo2',fld:'vEMAIL_TITULO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Email_Titulo3',fld:'vEMAIL_TITULO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFEmail_Titulo',fld:'vTFEMAIL_TITULO',pic:'',nv:''},{av:'AV35TFEmail_Titulo_Sel',fld:'vTFEMAIL_TITULO_SEL',pic:'',nv:''},{av:'AV38TFEmail_Key',fld:'vTFEMAIL_KEY',pic:'',nv:''},{av:'AV39TFEmail_Key_Sel',fld:'vTFEMAIL_KEY_SEL',pic:'',nv:''},{av:'AV36ddo_Email_TituloTitleControlIdToReplace',fld:'vDDO_EMAIL_TITULOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Email_KeyTitleControlIdToReplace',fld:'vDDO_EMAIL_KEYTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1665Email_Guid',fld:'EMAIL_GUID',pic:'',hsh:true,nv:'00000000-0000-0000-0000-000000000000'},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_EMAIL_TITULO.ONOPTIONCLICKED","{handler:'E12MS2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Email_Titulo1',fld:'vEMAIL_TITULO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Email_Titulo2',fld:'vEMAIL_TITULO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Email_Titulo3',fld:'vEMAIL_TITULO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFEmail_Titulo',fld:'vTFEMAIL_TITULO',pic:'',nv:''},{av:'AV35TFEmail_Titulo_Sel',fld:'vTFEMAIL_TITULO_SEL',pic:'',nv:''},{av:'AV38TFEmail_Key',fld:'vTFEMAIL_KEY',pic:'',nv:''},{av:'AV39TFEmail_Key_Sel',fld:'vTFEMAIL_KEY_SEL',pic:'',nv:''},{av:'AV36ddo_Email_TituloTitleControlIdToReplace',fld:'vDDO_EMAIL_TITULOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Email_KeyTitleControlIdToReplace',fld:'vDDO_EMAIL_KEYTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1665Email_Guid',fld:'EMAIL_GUID',pic:'',hsh:true,nv:'00000000-0000-0000-0000-000000000000'},{av:'Ddo_email_titulo_Activeeventkey',ctrl:'DDO_EMAIL_TITULO',prop:'ActiveEventKey'},{av:'Ddo_email_titulo_Filteredtext_get',ctrl:'DDO_EMAIL_TITULO',prop:'FilteredText_get'},{av:'Ddo_email_titulo_Selectedvalue_get',ctrl:'DDO_EMAIL_TITULO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_email_titulo_Sortedstatus',ctrl:'DDO_EMAIL_TITULO',prop:'SortedStatus'},{av:'AV34TFEmail_Titulo',fld:'vTFEMAIL_TITULO',pic:'',nv:''},{av:'AV35TFEmail_Titulo_Sel',fld:'vTFEMAIL_TITULO_SEL',pic:'',nv:''},{av:'Ddo_email_key_Sortedstatus',ctrl:'DDO_EMAIL_KEY',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_EMAIL_KEY.ONOPTIONCLICKED","{handler:'E13MS2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Email_Titulo1',fld:'vEMAIL_TITULO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Email_Titulo2',fld:'vEMAIL_TITULO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Email_Titulo3',fld:'vEMAIL_TITULO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFEmail_Titulo',fld:'vTFEMAIL_TITULO',pic:'',nv:''},{av:'AV35TFEmail_Titulo_Sel',fld:'vTFEMAIL_TITULO_SEL',pic:'',nv:''},{av:'AV38TFEmail_Key',fld:'vTFEMAIL_KEY',pic:'',nv:''},{av:'AV39TFEmail_Key_Sel',fld:'vTFEMAIL_KEY_SEL',pic:'',nv:''},{av:'AV36ddo_Email_TituloTitleControlIdToReplace',fld:'vDDO_EMAIL_TITULOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Email_KeyTitleControlIdToReplace',fld:'vDDO_EMAIL_KEYTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1665Email_Guid',fld:'EMAIL_GUID',pic:'',hsh:true,nv:'00000000-0000-0000-0000-000000000000'},{av:'Ddo_email_key_Activeeventkey',ctrl:'DDO_EMAIL_KEY',prop:'ActiveEventKey'},{av:'Ddo_email_key_Filteredtext_get',ctrl:'DDO_EMAIL_KEY',prop:'FilteredText_get'},{av:'Ddo_email_key_Selectedvalue_get',ctrl:'DDO_EMAIL_KEY',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_email_key_Sortedstatus',ctrl:'DDO_EMAIL_KEY',prop:'SortedStatus'},{av:'AV38TFEmail_Key',fld:'vTFEMAIL_KEY',pic:'',nv:''},{av:'AV39TFEmail_Key_Sel',fld:'vTFEMAIL_KEY_SEL',pic:'',nv:''},{av:'Ddo_email_titulo_Sortedstatus',ctrl:'DDO_EMAIL_TITULO',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E27MS2',iparms:[{av:'A1665Email_Guid',fld:'EMAIL_GUID',pic:'',hsh:true,nv:'00000000-0000-0000-0000-000000000000'}],oparms:[{av:'AV28Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV29Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'edtEmail_Titulo_Link',ctrl:'EMAIL_TITULO',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E14MS2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Email_Titulo1',fld:'vEMAIL_TITULO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Email_Titulo2',fld:'vEMAIL_TITULO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Email_Titulo3',fld:'vEMAIL_TITULO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFEmail_Titulo',fld:'vTFEMAIL_TITULO',pic:'',nv:''},{av:'AV35TFEmail_Titulo_Sel',fld:'vTFEMAIL_TITULO_SEL',pic:'',nv:''},{av:'AV38TFEmail_Key',fld:'vTFEMAIL_KEY',pic:'',nv:''},{av:'AV39TFEmail_Key_Sel',fld:'vTFEMAIL_KEY_SEL',pic:'',nv:''},{av:'AV36ddo_Email_TituloTitleControlIdToReplace',fld:'vDDO_EMAIL_TITULOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Email_KeyTitleControlIdToReplace',fld:'vDDO_EMAIL_KEYTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1665Email_Guid',fld:'EMAIL_GUID',pic:'',hsh:true,nv:'00000000-0000-0000-0000-000000000000'}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E20MS2',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E15MS2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Email_Titulo1',fld:'vEMAIL_TITULO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Email_Titulo2',fld:'vEMAIL_TITULO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Email_Titulo3',fld:'vEMAIL_TITULO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFEmail_Titulo',fld:'vTFEMAIL_TITULO',pic:'',nv:''},{av:'AV35TFEmail_Titulo_Sel',fld:'vTFEMAIL_TITULO_SEL',pic:'',nv:''},{av:'AV38TFEmail_Key',fld:'vTFEMAIL_KEY',pic:'',nv:''},{av:'AV39TFEmail_Key_Sel',fld:'vTFEMAIL_KEY_SEL',pic:'',nv:''},{av:'AV36ddo_Email_TituloTitleControlIdToReplace',fld:'vDDO_EMAIL_TITULOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Email_KeyTitleControlIdToReplace',fld:'vDDO_EMAIL_KEYTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1665Email_Guid',fld:'EMAIL_GUID',pic:'',hsh:true,nv:'00000000-0000-0000-0000-000000000000'}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Email_Titulo2',fld:'vEMAIL_TITULO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Email_Titulo3',fld:'vEMAIL_TITULO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Email_Titulo1',fld:'vEMAIL_TITULO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavEmail_titulo2_Visible',ctrl:'vEMAIL_TITULO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavEmail_titulo3_Visible',ctrl:'vEMAIL_TITULO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavEmail_titulo1_Visible',ctrl:'vEMAIL_TITULO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E21MS2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavEmail_titulo1_Visible',ctrl:'vEMAIL_TITULO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E22MS2',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E16MS2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Email_Titulo1',fld:'vEMAIL_TITULO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Email_Titulo2',fld:'vEMAIL_TITULO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Email_Titulo3',fld:'vEMAIL_TITULO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFEmail_Titulo',fld:'vTFEMAIL_TITULO',pic:'',nv:''},{av:'AV35TFEmail_Titulo_Sel',fld:'vTFEMAIL_TITULO_SEL',pic:'',nv:''},{av:'AV38TFEmail_Key',fld:'vTFEMAIL_KEY',pic:'',nv:''},{av:'AV39TFEmail_Key_Sel',fld:'vTFEMAIL_KEY_SEL',pic:'',nv:''},{av:'AV36ddo_Email_TituloTitleControlIdToReplace',fld:'vDDO_EMAIL_TITULOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Email_KeyTitleControlIdToReplace',fld:'vDDO_EMAIL_KEYTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1665Email_Guid',fld:'EMAIL_GUID',pic:'',hsh:true,nv:'00000000-0000-0000-0000-000000000000'}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Email_Titulo2',fld:'vEMAIL_TITULO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Email_Titulo3',fld:'vEMAIL_TITULO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Email_Titulo1',fld:'vEMAIL_TITULO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavEmail_titulo2_Visible',ctrl:'vEMAIL_TITULO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavEmail_titulo3_Visible',ctrl:'vEMAIL_TITULO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavEmail_titulo1_Visible',ctrl:'vEMAIL_TITULO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E23MS2',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavEmail_titulo2_Visible',ctrl:'vEMAIL_TITULO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E17MS2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Email_Titulo1',fld:'vEMAIL_TITULO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Email_Titulo2',fld:'vEMAIL_TITULO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Email_Titulo3',fld:'vEMAIL_TITULO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFEmail_Titulo',fld:'vTFEMAIL_TITULO',pic:'',nv:''},{av:'AV35TFEmail_Titulo_Sel',fld:'vTFEMAIL_TITULO_SEL',pic:'',nv:''},{av:'AV38TFEmail_Key',fld:'vTFEMAIL_KEY',pic:'',nv:''},{av:'AV39TFEmail_Key_Sel',fld:'vTFEMAIL_KEY_SEL',pic:'',nv:''},{av:'AV36ddo_Email_TituloTitleControlIdToReplace',fld:'vDDO_EMAIL_TITULOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Email_KeyTitleControlIdToReplace',fld:'vDDO_EMAIL_KEYTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1665Email_Guid',fld:'EMAIL_GUID',pic:'',hsh:true,nv:'00000000-0000-0000-0000-000000000000'}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Email_Titulo2',fld:'vEMAIL_TITULO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Email_Titulo3',fld:'vEMAIL_TITULO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Email_Titulo1',fld:'vEMAIL_TITULO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavEmail_titulo2_Visible',ctrl:'vEMAIL_TITULO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavEmail_titulo3_Visible',ctrl:'vEMAIL_TITULO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavEmail_titulo1_Visible',ctrl:'vEMAIL_TITULO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E24MS2',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavEmail_titulo3_Visible',ctrl:'vEMAIL_TITULO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E18MS2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Email_Titulo1',fld:'vEMAIL_TITULO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Email_Titulo2',fld:'vEMAIL_TITULO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Email_Titulo3',fld:'vEMAIL_TITULO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFEmail_Titulo',fld:'vTFEMAIL_TITULO',pic:'',nv:''},{av:'AV35TFEmail_Titulo_Sel',fld:'vTFEMAIL_TITULO_SEL',pic:'',nv:''},{av:'AV38TFEmail_Key',fld:'vTFEMAIL_KEY',pic:'',nv:''},{av:'AV39TFEmail_Key_Sel',fld:'vTFEMAIL_KEY_SEL',pic:'',nv:''},{av:'AV36ddo_Email_TituloTitleControlIdToReplace',fld:'vDDO_EMAIL_TITULOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Email_KeyTitleControlIdToReplace',fld:'vDDO_EMAIL_KEYTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1665Email_Guid',fld:'EMAIL_GUID',pic:'',hsh:true,nv:'00000000-0000-0000-0000-000000000000'}],oparms:[{av:'AV34TFEmail_Titulo',fld:'vTFEMAIL_TITULO',pic:'',nv:''},{av:'Ddo_email_titulo_Filteredtext_set',ctrl:'DDO_EMAIL_TITULO',prop:'FilteredText_set'},{av:'AV35TFEmail_Titulo_Sel',fld:'vTFEMAIL_TITULO_SEL',pic:'',nv:''},{av:'Ddo_email_titulo_Selectedvalue_set',ctrl:'DDO_EMAIL_TITULO',prop:'SelectedValue_set'},{av:'AV38TFEmail_Key',fld:'vTFEMAIL_KEY',pic:'',nv:''},{av:'Ddo_email_key_Filteredtext_set',ctrl:'DDO_EMAIL_KEY',prop:'FilteredText_set'},{av:'AV39TFEmail_Key_Sel',fld:'vTFEMAIL_KEY_SEL',pic:'',nv:''},{av:'Ddo_email_key_Selectedvalue_set',ctrl:'DDO_EMAIL_KEY',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Email_Titulo1',fld:'vEMAIL_TITULO1',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavEmail_titulo1_Visible',ctrl:'vEMAIL_TITULO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Email_Titulo2',fld:'vEMAIL_TITULO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Email_Titulo3',fld:'vEMAIL_TITULO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavEmail_titulo2_Visible',ctrl:'vEMAIL_TITULO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavEmail_titulo3_Visible',ctrl:'vEMAIL_TITULO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E19MS2',iparms:[{av:'A1665Email_Guid',fld:'EMAIL_GUID',pic:'',hsh:true,nv:'00000000-0000-0000-0000-000000000000'}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_email_titulo_Activeeventkey = "";
         Ddo_email_titulo_Filteredtext_get = "";
         Ddo_email_titulo_Selectedvalue_get = "";
         Ddo_email_key_Activeeventkey = "";
         Ddo_email_key_Filteredtext_get = "";
         Ddo_email_key_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17Email_Titulo1 = "";
         AV19DynamicFiltersSelector2 = "";
         AV21Email_Titulo2 = "";
         AV23DynamicFiltersSelector3 = "";
         AV25Email_Titulo3 = "";
         AV34TFEmail_Titulo = "";
         AV35TFEmail_Titulo_Sel = "";
         AV38TFEmail_Key = "";
         AV39TFEmail_Key_Sel = "";
         AV36ddo_Email_TituloTitleControlIdToReplace = "";
         AV40ddo_Email_KeyTitleControlIdToReplace = "";
         AV64Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         A1665Email_Guid = (Guid)(System.Guid.Empty);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV41DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV33Email_TituloTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV37Email_KeyTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_email_titulo_Filteredtext_set = "";
         Ddo_email_titulo_Selectedvalue_set = "";
         Ddo_email_titulo_Sortedstatus = "";
         Ddo_email_key_Filteredtext_set = "";
         Ddo_email_key_Selectedvalue_set = "";
         Ddo_email_key_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Update = "";
         AV62Update_GXI = "";
         AV29Delete = "";
         AV63Delete_GXI = "";
         A1669Email_Titulo = "";
         A1672Email_Key = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV49WWEmailDS_3_Email_titulo1 = "";
         lV53WWEmailDS_7_Email_titulo2 = "";
         lV57WWEmailDS_11_Email_titulo3 = "";
         lV58WWEmailDS_12_Tfemail_titulo = "";
         lV60WWEmailDS_14_Tfemail_key = "";
         AV47WWEmailDS_1_Dynamicfiltersselector1 = "";
         AV49WWEmailDS_3_Email_titulo1 = "";
         AV51WWEmailDS_5_Dynamicfiltersselector2 = "";
         AV53WWEmailDS_7_Email_titulo2 = "";
         AV55WWEmailDS_9_Dynamicfiltersselector3 = "";
         AV57WWEmailDS_11_Email_titulo3 = "";
         AV59WWEmailDS_13_Tfemail_titulo_sel = "";
         AV58WWEmailDS_12_Tfemail_titulo = "";
         AV61WWEmailDS_15_Tfemail_key_sel = "";
         AV60WWEmailDS_14_Tfemail_key = "";
         H00MS2_A1672Email_Key = new String[] {""} ;
         H00MS2_A1669Email_Titulo = new String[] {""} ;
         H00MS2_A1665Email_Guid = new Guid[] {System.Guid.Empty} ;
         H00MS3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV30Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblEmailtitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwemail__default(),
            new Object[][] {
                new Object[] {
               H00MS2_A1672Email_Key, H00MS2_A1669Email_Titulo, H00MS2_A1665Email_Guid
               }
               , new Object[] {
               H00MS3_AGRID_nRecordCount
               }
            }
         );
         AV64Pgmname = "WWEmail";
         /* GeneXus formulas. */
         AV64Pgmname = "WWEmail";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_88 ;
      private short nGXsfl_88_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV20DynamicFiltersOperator2 ;
      private short AV24DynamicFiltersOperator3 ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_88_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV48WWEmailDS_2_Dynamicfiltersoperator1 ;
      private short AV52WWEmailDS_6_Dynamicfiltersoperator2 ;
      private short AV56WWEmailDS_10_Dynamicfiltersoperator3 ;
      private short edtEmail_Titulo_Titleformat ;
      private short edtEmail_Key_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_email_titulo_Datalistupdateminimumcharacters ;
      private int Ddo_email_key_Datalistupdateminimumcharacters ;
      private int edtavTfemail_titulo_Visible ;
      private int edtavTfemail_titulo_sel_Visible ;
      private int edtavTfemail_key_Visible ;
      private int edtavTfemail_key_sel_Visible ;
      private int edtavDdo_email_titulotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_email_keytitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV42PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavEmail_titulo1_Visible ;
      private int edtavEmail_titulo2_Visible ;
      private int edtavEmail_titulo3_Visible ;
      private int AV65GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV43GridCurrentPage ;
      private long AV44GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_email_titulo_Activeeventkey ;
      private String Ddo_email_titulo_Filteredtext_get ;
      private String Ddo_email_titulo_Selectedvalue_get ;
      private String Ddo_email_key_Activeeventkey ;
      private String Ddo_email_key_Filteredtext_get ;
      private String Ddo_email_key_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_88_idx="0001" ;
      private String AV64Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_email_titulo_Caption ;
      private String Ddo_email_titulo_Tooltip ;
      private String Ddo_email_titulo_Cls ;
      private String Ddo_email_titulo_Filteredtext_set ;
      private String Ddo_email_titulo_Selectedvalue_set ;
      private String Ddo_email_titulo_Dropdownoptionstype ;
      private String Ddo_email_titulo_Titlecontrolidtoreplace ;
      private String Ddo_email_titulo_Sortedstatus ;
      private String Ddo_email_titulo_Filtertype ;
      private String Ddo_email_titulo_Datalisttype ;
      private String Ddo_email_titulo_Datalistproc ;
      private String Ddo_email_titulo_Sortasc ;
      private String Ddo_email_titulo_Sortdsc ;
      private String Ddo_email_titulo_Loadingdata ;
      private String Ddo_email_titulo_Cleanfilter ;
      private String Ddo_email_titulo_Noresultsfound ;
      private String Ddo_email_titulo_Searchbuttontext ;
      private String Ddo_email_key_Caption ;
      private String Ddo_email_key_Tooltip ;
      private String Ddo_email_key_Cls ;
      private String Ddo_email_key_Filteredtext_set ;
      private String Ddo_email_key_Selectedvalue_set ;
      private String Ddo_email_key_Dropdownoptionstype ;
      private String Ddo_email_key_Titlecontrolidtoreplace ;
      private String Ddo_email_key_Sortedstatus ;
      private String Ddo_email_key_Filtertype ;
      private String Ddo_email_key_Datalisttype ;
      private String Ddo_email_key_Datalistproc ;
      private String Ddo_email_key_Sortasc ;
      private String Ddo_email_key_Sortdsc ;
      private String Ddo_email_key_Loadingdata ;
      private String Ddo_email_key_Cleanfilter ;
      private String Ddo_email_key_Noresultsfound ;
      private String Ddo_email_key_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfemail_titulo_Internalname ;
      private String edtavTfemail_titulo_Jsonclick ;
      private String edtavTfemail_titulo_sel_Internalname ;
      private String edtavTfemail_titulo_sel_Jsonclick ;
      private String edtavTfemail_key_Internalname ;
      private String edtavTfemail_key_Jsonclick ;
      private String edtavTfemail_key_sel_Internalname ;
      private String edtavTfemail_key_sel_Jsonclick ;
      private String edtavDdo_email_titulotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_email_keytitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtEmail_Guid_Internalname ;
      private String edtEmail_Titulo_Internalname ;
      private String edtEmail_Key_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavEmail_titulo1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavEmail_titulo2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavEmail_titulo3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_email_titulo_Internalname ;
      private String Ddo_email_key_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtEmail_Titulo_Title ;
      private String edtEmail_Key_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtEmail_Titulo_Link ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblEmailtitle_Internalname ;
      private String lblEmailtitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavEmail_titulo3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavEmail_titulo2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavEmail_titulo1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_88_fel_idx="0001" ;
      private String ROClassString ;
      private String edtEmail_Guid_Jsonclick ;
      private String edtEmail_Titulo_Jsonclick ;
      private String edtEmail_Key_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_email_titulo_Includesortasc ;
      private bool Ddo_email_titulo_Includesortdsc ;
      private bool Ddo_email_titulo_Includefilter ;
      private bool Ddo_email_titulo_Filterisrange ;
      private bool Ddo_email_titulo_Includedatalist ;
      private bool Ddo_email_key_Includesortasc ;
      private bool Ddo_email_key_Includesortdsc ;
      private bool Ddo_email_key_Includefilter ;
      private bool Ddo_email_key_Filterisrange ;
      private bool Ddo_email_key_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV50WWEmailDS_4_Dynamicfiltersenabled2 ;
      private bool AV54WWEmailDS_8_Dynamicfiltersenabled3 ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Update_IsBlob ;
      private bool AV29Delete_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV17Email_Titulo1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV21Email_Titulo2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV25Email_Titulo3 ;
      private String AV34TFEmail_Titulo ;
      private String AV35TFEmail_Titulo_Sel ;
      private String AV38TFEmail_Key ;
      private String AV39TFEmail_Key_Sel ;
      private String AV36ddo_Email_TituloTitleControlIdToReplace ;
      private String AV40ddo_Email_KeyTitleControlIdToReplace ;
      private String AV62Update_GXI ;
      private String AV63Delete_GXI ;
      private String A1669Email_Titulo ;
      private String A1672Email_Key ;
      private String lV49WWEmailDS_3_Email_titulo1 ;
      private String lV53WWEmailDS_7_Email_titulo2 ;
      private String lV57WWEmailDS_11_Email_titulo3 ;
      private String lV58WWEmailDS_12_Tfemail_titulo ;
      private String lV60WWEmailDS_14_Tfemail_key ;
      private String AV47WWEmailDS_1_Dynamicfiltersselector1 ;
      private String AV49WWEmailDS_3_Email_titulo1 ;
      private String AV51WWEmailDS_5_Dynamicfiltersselector2 ;
      private String AV53WWEmailDS_7_Email_titulo2 ;
      private String AV55WWEmailDS_9_Dynamicfiltersselector3 ;
      private String AV57WWEmailDS_11_Email_titulo3 ;
      private String AV59WWEmailDS_13_Tfemail_titulo_sel ;
      private String AV58WWEmailDS_12_Tfemail_titulo ;
      private String AV61WWEmailDS_15_Tfemail_key_sel ;
      private String AV60WWEmailDS_14_Tfemail_key ;
      private String AV28Update ;
      private String AV29Delete ;
      private Guid A1665Email_Guid ;
      private IGxSession AV30Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private String[] H00MS2_A1672Email_Key ;
      private String[] H00MS2_A1669Email_Titulo ;
      private Guid[] H00MS2_A1665Email_Guid ;
      private long[] H00MS3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV33Email_TituloTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV37Email_KeyTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV41DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwemail__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00MS2( IGxContext context ,
                                             String AV47WWEmailDS_1_Dynamicfiltersselector1 ,
                                             short AV48WWEmailDS_2_Dynamicfiltersoperator1 ,
                                             String AV49WWEmailDS_3_Email_titulo1 ,
                                             bool AV50WWEmailDS_4_Dynamicfiltersenabled2 ,
                                             String AV51WWEmailDS_5_Dynamicfiltersselector2 ,
                                             short AV52WWEmailDS_6_Dynamicfiltersoperator2 ,
                                             String AV53WWEmailDS_7_Email_titulo2 ,
                                             bool AV54WWEmailDS_8_Dynamicfiltersenabled3 ,
                                             String AV55WWEmailDS_9_Dynamicfiltersselector3 ,
                                             short AV56WWEmailDS_10_Dynamicfiltersoperator3 ,
                                             String AV57WWEmailDS_11_Email_titulo3 ,
                                             String AV59WWEmailDS_13_Tfemail_titulo_sel ,
                                             String AV58WWEmailDS_12_Tfemail_titulo ,
                                             String AV61WWEmailDS_15_Tfemail_key_sel ,
                                             String AV60WWEmailDS_14_Tfemail_key ,
                                             String A1669Email_Titulo ,
                                             String A1672Email_Key ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [15] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [Email_Key], [Email_Titulo], [Email_Guid]";
         sFromString = " FROM [Email] WITH (NOLOCK)";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV47WWEmailDS_1_Dynamicfiltersselector1, "EMAIL_TITULO") == 0 ) && ( AV48WWEmailDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49WWEmailDS_3_Email_titulo1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Email_Titulo] like @lV49WWEmailDS_3_Email_titulo1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Email_Titulo] like @lV49WWEmailDS_3_Email_titulo1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV47WWEmailDS_1_Dynamicfiltersselector1, "EMAIL_TITULO") == 0 ) && ( AV48WWEmailDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49WWEmailDS_3_Email_titulo1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Email_Titulo] like '%' + @lV49WWEmailDS_3_Email_titulo1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Email_Titulo] like '%' + @lV49WWEmailDS_3_Email_titulo1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( AV50WWEmailDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV51WWEmailDS_5_Dynamicfiltersselector2, "EMAIL_TITULO") == 0 ) && ( AV52WWEmailDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWEmailDS_7_Email_titulo2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Email_Titulo] like @lV53WWEmailDS_7_Email_titulo2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Email_Titulo] like @lV53WWEmailDS_7_Email_titulo2)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV50WWEmailDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV51WWEmailDS_5_Dynamicfiltersselector2, "EMAIL_TITULO") == 0 ) && ( AV52WWEmailDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWEmailDS_7_Email_titulo2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Email_Titulo] like '%' + @lV53WWEmailDS_7_Email_titulo2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Email_Titulo] like '%' + @lV53WWEmailDS_7_Email_titulo2)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV54WWEmailDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV55WWEmailDS_9_Dynamicfiltersselector3, "EMAIL_TITULO") == 0 ) && ( AV56WWEmailDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWEmailDS_11_Email_titulo3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Email_Titulo] like @lV57WWEmailDS_11_Email_titulo3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Email_Titulo] like @lV57WWEmailDS_11_Email_titulo3)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV54WWEmailDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV55WWEmailDS_9_Dynamicfiltersselector3, "EMAIL_TITULO") == 0 ) && ( AV56WWEmailDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWEmailDS_11_Email_titulo3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Email_Titulo] like '%' + @lV57WWEmailDS_11_Email_titulo3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Email_Titulo] like '%' + @lV57WWEmailDS_11_Email_titulo3)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV59WWEmailDS_13_Tfemail_titulo_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWEmailDS_12_Tfemail_titulo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Email_Titulo] like @lV58WWEmailDS_12_Tfemail_titulo)";
            }
            else
            {
               sWhereString = sWhereString + " ([Email_Titulo] like @lV58WWEmailDS_12_Tfemail_titulo)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWEmailDS_13_Tfemail_titulo_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Email_Titulo] = @AV59WWEmailDS_13_Tfemail_titulo_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Email_Titulo] = @AV59WWEmailDS_13_Tfemail_titulo_sel)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV61WWEmailDS_15_Tfemail_key_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWEmailDS_14_Tfemail_key)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Email_Key] like @lV60WWEmailDS_14_Tfemail_key)";
            }
            else
            {
               sWhereString = sWhereString + " ([Email_Key] like @lV60WWEmailDS_14_Tfemail_key)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWEmailDS_15_Tfemail_key_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Email_Key] = @AV61WWEmailDS_15_Tfemail_key_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Email_Key] = @AV61WWEmailDS_15_Tfemail_key_sel)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Email_Titulo]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Email_Titulo] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Email_Key]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Email_Key] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [Email_Guid]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00MS3( IGxContext context ,
                                             String AV47WWEmailDS_1_Dynamicfiltersselector1 ,
                                             short AV48WWEmailDS_2_Dynamicfiltersoperator1 ,
                                             String AV49WWEmailDS_3_Email_titulo1 ,
                                             bool AV50WWEmailDS_4_Dynamicfiltersenabled2 ,
                                             String AV51WWEmailDS_5_Dynamicfiltersselector2 ,
                                             short AV52WWEmailDS_6_Dynamicfiltersoperator2 ,
                                             String AV53WWEmailDS_7_Email_titulo2 ,
                                             bool AV54WWEmailDS_8_Dynamicfiltersenabled3 ,
                                             String AV55WWEmailDS_9_Dynamicfiltersselector3 ,
                                             short AV56WWEmailDS_10_Dynamicfiltersoperator3 ,
                                             String AV57WWEmailDS_11_Email_titulo3 ,
                                             String AV59WWEmailDS_13_Tfemail_titulo_sel ,
                                             String AV58WWEmailDS_12_Tfemail_titulo ,
                                             String AV61WWEmailDS_15_Tfemail_key_sel ,
                                             String AV60WWEmailDS_14_Tfemail_key ,
                                             String A1669Email_Titulo ,
                                             String A1672Email_Key ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [10] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [Email] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV47WWEmailDS_1_Dynamicfiltersselector1, "EMAIL_TITULO") == 0 ) && ( AV48WWEmailDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49WWEmailDS_3_Email_titulo1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Email_Titulo] like @lV49WWEmailDS_3_Email_titulo1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Email_Titulo] like @lV49WWEmailDS_3_Email_titulo1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV47WWEmailDS_1_Dynamicfiltersselector1, "EMAIL_TITULO") == 0 ) && ( AV48WWEmailDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49WWEmailDS_3_Email_titulo1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Email_Titulo] like '%' + @lV49WWEmailDS_3_Email_titulo1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Email_Titulo] like '%' + @lV49WWEmailDS_3_Email_titulo1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( AV50WWEmailDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV51WWEmailDS_5_Dynamicfiltersselector2, "EMAIL_TITULO") == 0 ) && ( AV52WWEmailDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWEmailDS_7_Email_titulo2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Email_Titulo] like @lV53WWEmailDS_7_Email_titulo2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Email_Titulo] like @lV53WWEmailDS_7_Email_titulo2)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV50WWEmailDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV51WWEmailDS_5_Dynamicfiltersselector2, "EMAIL_TITULO") == 0 ) && ( AV52WWEmailDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWEmailDS_7_Email_titulo2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Email_Titulo] like '%' + @lV53WWEmailDS_7_Email_titulo2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Email_Titulo] like '%' + @lV53WWEmailDS_7_Email_titulo2)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV54WWEmailDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV55WWEmailDS_9_Dynamicfiltersselector3, "EMAIL_TITULO") == 0 ) && ( AV56WWEmailDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWEmailDS_11_Email_titulo3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Email_Titulo] like @lV57WWEmailDS_11_Email_titulo3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Email_Titulo] like @lV57WWEmailDS_11_Email_titulo3)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV54WWEmailDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV55WWEmailDS_9_Dynamicfiltersselector3, "EMAIL_TITULO") == 0 ) && ( AV56WWEmailDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWEmailDS_11_Email_titulo3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Email_Titulo] like '%' + @lV57WWEmailDS_11_Email_titulo3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Email_Titulo] like '%' + @lV57WWEmailDS_11_Email_titulo3)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV59WWEmailDS_13_Tfemail_titulo_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWEmailDS_12_Tfemail_titulo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Email_Titulo] like @lV58WWEmailDS_12_Tfemail_titulo)";
            }
            else
            {
               sWhereString = sWhereString + " ([Email_Titulo] like @lV58WWEmailDS_12_Tfemail_titulo)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWEmailDS_13_Tfemail_titulo_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Email_Titulo] = @AV59WWEmailDS_13_Tfemail_titulo_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Email_Titulo] = @AV59WWEmailDS_13_Tfemail_titulo_sel)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV61WWEmailDS_15_Tfemail_key_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWEmailDS_14_Tfemail_key)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Email_Key] like @lV60WWEmailDS_14_Tfemail_key)";
            }
            else
            {
               sWhereString = sWhereString + " ([Email_Key] like @lV60WWEmailDS_14_Tfemail_key)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWEmailDS_15_Tfemail_key_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Email_Key] = @AV61WWEmailDS_15_Tfemail_key_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Email_Key] = @AV61WWEmailDS_15_Tfemail_key_sel)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00MS2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (short)dynConstraints[17] , (bool)dynConstraints[18] );
               case 1 :
                     return conditional_H00MS3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (short)dynConstraints[17] , (bool)dynConstraints[18] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00MS2 ;
          prmH00MS2 = new Object[] {
          new Object[] {"@lV49WWEmailDS_3_Email_titulo1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV49WWEmailDS_3_Email_titulo1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV53WWEmailDS_7_Email_titulo2",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV53WWEmailDS_7_Email_titulo2",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV57WWEmailDS_11_Email_titulo3",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV57WWEmailDS_11_Email_titulo3",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV58WWEmailDS_12_Tfemail_titulo",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV59WWEmailDS_13_Tfemail_titulo_sel",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV60WWEmailDS_14_Tfemail_key",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV61WWEmailDS_15_Tfemail_key_sel",SqlDbType.VarChar,40,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00MS3 ;
          prmH00MS3 = new Object[] {
          new Object[] {"@lV49WWEmailDS_3_Email_titulo1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV49WWEmailDS_3_Email_titulo1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV53WWEmailDS_7_Email_titulo2",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV53WWEmailDS_7_Email_titulo2",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV57WWEmailDS_11_Email_titulo3",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV57WWEmailDS_11_Email_titulo3",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV58WWEmailDS_12_Tfemail_titulo",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV59WWEmailDS_13_Tfemail_titulo_sel",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV60WWEmailDS_14_Tfemail_key",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV61WWEmailDS_15_Tfemail_key_sel",SqlDbType.VarChar,40,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00MS2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MS2,11,0,true,false )
             ,new CursorDef("H00MS3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MS3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((Guid[]) buf[2])[0] = rslt.getGuid(3) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                return;
       }
    }

 }

}
