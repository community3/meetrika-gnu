/*
               File: PRC_EnviarEmailSemNotificacao
        Description: PRC_Enviar Email Sem Notificacao
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:9:43.68
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Mail;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_enviaremailsemnotificacao : GXProcedure
   {
      public prc_enviaremailsemnotificacao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_enviaremailsemnotificacao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_AreaTrabalho_Codigo ,
                           String aP1_Subject ,
                           String aP2_EmailText ,
                           IGxCollection aP3_Attachments ,
                           ref String aP4_Resultado )
      {
         this.AV8AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV20Subject = aP1_Subject;
         this.AV13EmailText = aP2_EmailText;
         this.AV10Attachments = aP3_Attachments;
         this.AV18Resultado = aP4_Resultado;
         initialize();
         executePrivate();
         aP4_Resultado=this.AV18Resultado;
      }

      public String executeUdp( int aP0_AreaTrabalho_Codigo ,
                                String aP1_Subject ,
                                String aP2_EmailText ,
                                IGxCollection aP3_Attachments )
      {
         this.AV8AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV20Subject = aP1_Subject;
         this.AV13EmailText = aP2_EmailText;
         this.AV10Attachments = aP3_Attachments;
         this.AV18Resultado = aP4_Resultado;
         initialize();
         executePrivate();
         aP4_Resultado=this.AV18Resultado;
         return AV18Resultado ;
      }

      public void executeSubmit( int aP0_AreaTrabalho_Codigo ,
                                 String aP1_Subject ,
                                 String aP2_EmailText ,
                                 IGxCollection aP3_Attachments ,
                                 ref String aP4_Resultado )
      {
         prc_enviaremailsemnotificacao objprc_enviaremailsemnotificacao;
         objprc_enviaremailsemnotificacao = new prc_enviaremailsemnotificacao();
         objprc_enviaremailsemnotificacao.AV8AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         objprc_enviaremailsemnotificacao.AV20Subject = aP1_Subject;
         objprc_enviaremailsemnotificacao.AV13EmailText = aP2_EmailText;
         objprc_enviaremailsemnotificacao.AV10Attachments = aP3_Attachments;
         objprc_enviaremailsemnotificacao.AV18Resultado = aP4_Resultado;
         objprc_enviaremailsemnotificacao.context.SetSubmitInitialConfig(context);
         objprc_enviaremailsemnotificacao.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_enviaremailsemnotificacao);
         aP4_Resultado=this.AV18Resultado;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_enviaremailsemnotificacao)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV22Usuarios.FromXml(AV23WebSession.Get("SendTo"), "Collection");
         AV25i = 1;
         if ( AV22Usuarios.Count == 0 )
         {
            /* Using cursor P00BO2 */
            pr_default.execute(0, new Object[] {AV8AreaTrabalho_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A66ContratadaUsuario_ContratadaCod = P00BO2_A66ContratadaUsuario_ContratadaCod[0];
               A69ContratadaUsuario_UsuarioCod = P00BO2_A69ContratadaUsuario_UsuarioCod[0];
               A538Usuario_EhGestor = P00BO2_A538Usuario_EhGestor[0];
               n538Usuario_EhGestor = P00BO2_n538Usuario_EhGestor[0];
               A516Contratada_TipoFabrica = P00BO2_A516Contratada_TipoFabrica[0];
               n516Contratada_TipoFabrica = P00BO2_n516Contratada_TipoFabrica[0];
               A1228ContratadaUsuario_AreaTrabalhoCod = P00BO2_A1228ContratadaUsuario_AreaTrabalhoCod[0];
               n1228ContratadaUsuario_AreaTrabalhoCod = P00BO2_n1228ContratadaUsuario_AreaTrabalhoCod[0];
               A341Usuario_UserGamGuid = P00BO2_A341Usuario_UserGamGuid[0];
               n341Usuario_UserGamGuid = P00BO2_n341Usuario_UserGamGuid[0];
               A516Contratada_TipoFabrica = P00BO2_A516Contratada_TipoFabrica[0];
               n516Contratada_TipoFabrica = P00BO2_n516Contratada_TipoFabrica[0];
               A1228ContratadaUsuario_AreaTrabalhoCod = P00BO2_A1228ContratadaUsuario_AreaTrabalhoCod[0];
               n1228ContratadaUsuario_AreaTrabalhoCod = P00BO2_n1228ContratadaUsuario_AreaTrabalhoCod[0];
               A538Usuario_EhGestor = P00BO2_A538Usuario_EhGestor[0];
               n538Usuario_EhGestor = P00BO2_n538Usuario_EhGestor[0];
               A341Usuario_UserGamGuid = P00BO2_A341Usuario_UserGamGuid[0];
               n341Usuario_UserGamGuid = P00BO2_n341Usuario_UserGamGuid[0];
               AV14GamUser.load( A341Usuario_UserGamGuid);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14GamUser.gxTpr_Email)) )
               {
                  AV15MailRecipient.Name = StringUtil.Trim( AV14GamUser.gxTpr_Email);
                  AV15MailRecipient.Address = StringUtil.Trim( AV14GamUser.gxTpr_Email);
                  if ( AV25i == 1 )
                  {
                     AV12Email.To.Add(AV15MailRecipient) ;
                     AV11Destinatarios = AV11Destinatarios + AV15MailRecipient.Address;
                  }
                  else
                  {
                     AV12Email.BCC.Add(AV15MailRecipient) ;
                  }
                  AV15MailRecipient = new GeneXus.Mail.GXMailRecipient();
                  AV25i = (short)(AV25i+1);
               }
               pr_default.readNext(0);
            }
            pr_default.close(0);
         }
         else
         {
            pr_default.dynParam(1, new Object[]{ new Object[]{
                                                 A1Usuario_Codigo ,
                                                 AV22Usuarios },
                                                 new int[] {
                                                 TypeConstants.INT
                                                 }
            });
            /* Using cursor P00BO3 */
            pr_default.execute(1);
            while ( (pr_default.getStatus(1) != 101) )
            {
               A1Usuario_Codigo = P00BO3_A1Usuario_Codigo[0];
               A1647Usuario_Email = P00BO3_A1647Usuario_Email[0];
               n1647Usuario_Email = P00BO3_n1647Usuario_Email[0];
               A341Usuario_UserGamGuid = P00BO3_A341Usuario_UserGamGuid[0];
               n341Usuario_UserGamGuid = P00BO3_n341Usuario_UserGamGuid[0];
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1647Usuario_Email)) && ! P00BO3_n1647Usuario_Email[0] )
               {
                  AV15MailRecipient.Name = StringUtil.Trim( A1647Usuario_Email);
                  AV15MailRecipient.Address = StringUtil.Trim( A1647Usuario_Email);
                  if ( AV25i == 1 )
                  {
                     AV12Email.To.Add(AV15MailRecipient) ;
                     AV11Destinatarios = AV11Destinatarios + AV15MailRecipient.Address;
                  }
                  else
                  {
                     AV12Email.BCC.Add(AV15MailRecipient) ;
                  }
                  AV15MailRecipient = new GeneXus.Mail.GXMailRecipient();
                  AV25i = (short)(AV25i+1);
               }
               else
               {
                  AV14GamUser.load( A341Usuario_UserGamGuid);
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14GamUser.gxTpr_Email)) )
                  {
                     AV15MailRecipient.Name = StringUtil.Trim( AV14GamUser.gxTpr_Email);
                     AV15MailRecipient.Address = StringUtil.Trim( AV14GamUser.gxTpr_Email);
                     if ( AV25i == 1 )
                     {
                        AV12Email.To.Add(AV15MailRecipient) ;
                        AV11Destinatarios = AV11Destinatarios + AV15MailRecipient.Address;
                     }
                     else
                     {
                        AV12Email.BCC.Add(AV15MailRecipient) ;
                     }
                     AV15MailRecipient = new GeneXus.Mail.GXMailRecipient();
                     AV25i = (short)(AV25i+1);
                  }
               }
               pr_default.readNext(1);
            }
            pr_default.close(1);
         }
         if ( AV12Email.To.Count > 0 )
         {
            /* Using cursor P00BO4 */
            pr_default.execute(2);
            while ( (pr_default.getStatus(2) != 101) )
            {
               A331ParametrosSistema_NomeSistema = P00BO4_A331ParametrosSistema_NomeSistema[0];
               A330ParametrosSistema_Codigo = P00BO4_A330ParametrosSistema_Codigo[0];
               AV26NomeSistema = StringUtil.Trim( A331ParametrosSistema_NomeSistema);
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(2);
            }
            pr_default.close(2);
            /* Using cursor P00BO5 */
            pr_default.execute(3, new Object[] {AV8AreaTrabalho_Codigo});
            while ( (pr_default.getStatus(3) != 101) )
            {
               A29Contratante_Codigo = P00BO5_A29Contratante_Codigo[0];
               n29Contratante_Codigo = P00BO5_n29Contratante_Codigo[0];
               A5AreaTrabalho_Codigo = P00BO5_A5AreaTrabalho_Codigo[0];
               A548Contratante_EmailSdaUser = P00BO5_A548Contratante_EmailSdaUser[0];
               n548Contratante_EmailSdaUser = P00BO5_n548Contratante_EmailSdaUser[0];
               A547Contratante_EmailSdaHost = P00BO5_A547Contratante_EmailSdaHost[0];
               n547Contratante_EmailSdaHost = P00BO5_n547Contratante_EmailSdaHost[0];
               A549Contratante_EmailSdaPass = P00BO5_A549Contratante_EmailSdaPass[0];
               n549Contratante_EmailSdaPass = P00BO5_n549Contratante_EmailSdaPass[0];
               A550Contratante_EmailSdaKey = P00BO5_A550Contratante_EmailSdaKey[0];
               n550Contratante_EmailSdaKey = P00BO5_n550Contratante_EmailSdaKey[0];
               A552Contratante_EmailSdaPort = P00BO5_A552Contratante_EmailSdaPort[0];
               n552Contratante_EmailSdaPort = P00BO5_n552Contratante_EmailSdaPort[0];
               A551Contratante_EmailSdaAut = P00BO5_A551Contratante_EmailSdaAut[0];
               n551Contratante_EmailSdaAut = P00BO5_n551Contratante_EmailSdaAut[0];
               A1048Contratante_EmailSdaSec = P00BO5_A1048Contratante_EmailSdaSec[0];
               n1048Contratante_EmailSdaSec = P00BO5_n1048Contratante_EmailSdaSec[0];
               A548Contratante_EmailSdaUser = P00BO5_A548Contratante_EmailSdaUser[0];
               n548Contratante_EmailSdaUser = P00BO5_n548Contratante_EmailSdaUser[0];
               A547Contratante_EmailSdaHost = P00BO5_A547Contratante_EmailSdaHost[0];
               n547Contratante_EmailSdaHost = P00BO5_n547Contratante_EmailSdaHost[0];
               A549Contratante_EmailSdaPass = P00BO5_A549Contratante_EmailSdaPass[0];
               n549Contratante_EmailSdaPass = P00BO5_n549Contratante_EmailSdaPass[0];
               A550Contratante_EmailSdaKey = P00BO5_A550Contratante_EmailSdaKey[0];
               n550Contratante_EmailSdaKey = P00BO5_n550Contratante_EmailSdaKey[0];
               A552Contratante_EmailSdaPort = P00BO5_A552Contratante_EmailSdaPort[0];
               n552Contratante_EmailSdaPort = P00BO5_n552Contratante_EmailSdaPort[0];
               A551Contratante_EmailSdaAut = P00BO5_A551Contratante_EmailSdaAut[0];
               n551Contratante_EmailSdaAut = P00BO5_n551Contratante_EmailSdaAut[0];
               A1048Contratante_EmailSdaSec = P00BO5_A1048Contratante_EmailSdaSec[0];
               n1048Contratante_EmailSdaSec = P00BO5_n1048Contratante_EmailSdaSec[0];
               AV19SMTPSession.AttachDir = "\\";
               AV19SMTPSession.Sender.Name = StringUtil.Trim( A548Contratante_EmailSdaUser);
               AV19SMTPSession.Sender.Address = StringUtil.Trim( A548Contratante_EmailSdaUser);
               AV19SMTPSession.Host = StringUtil.Trim( A547Contratante_EmailSdaHost);
               AV19SMTPSession.UserName = StringUtil.Trim( A548Contratante_EmailSdaUser);
               if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( A549Contratante_EmailSdaPass)) || P00BO5_n549Contratante_EmailSdaPass[0] ) )
               {
                  AV19SMTPSession.Password = Crypto.Decrypt64( A549Contratante_EmailSdaPass, A550Contratante_EmailSdaKey);
               }
               AV19SMTPSession.Port = A552Contratante_EmailSdaPort;
               AV19SMTPSession.Authentication = (short)((A551Contratante_EmailSdaAut ? 1 : 0));
               AV19SMTPSession.Secure = A1048Contratante_EmailSdaSec;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(3);
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV19SMTPSession.Host)) || (0==AV19SMTPSession.Port) || String.IsNullOrEmpty(StringUtil.RTrim( AV19SMTPSession.UserName)) )
            {
               AV18Resultado = "Cadastro da Contratante sem dados configurados para envio de e-mails!" + StringUtil.NewLine( );
            }
            else
            {
               if ( StringUtil.Like( AV26NomeSistema , StringUtil.PadR( "%" + StringUtil.Trim( AV20Subject) + "%" , 256 , "%"),  ' ' ) )
               {
                  AV12Email.Subject = AV20Subject;
               }
               else
               {
                  AV12Email.Subject = "["+AV26NomeSistema+"] "+AV20Subject;
               }
               AV12Email.Text = AV13EmailText;
               AV12Email.Text = AV12Email.Text+StringUtil.Chr( 13)+"Enviado automaticamente pelo aplicativo "+AV26NomeSistema+StringUtil.Chr( 13);
               AV35GXV1 = 1;
               while ( AV35GXV1 <= AV10Attachments.Count )
               {
                  AV9Attachment = ((String)AV10Attachments.Item(AV35GXV1));
                  AV12Email.Attachments.Add(AV9Attachment) ;
                  AV35GXV1 = (int)(AV35GXV1+1);
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( AV18Resultado)) )
               {
                  AV18Resultado = "Envio de notificação";
               }
               AV19SMTPSession.Login();
               if ( (0==AV19SMTPSession.ErrCode) )
               {
                  AV19SMTPSession.Send(AV12Email);
                  if ( (0==AV19SMTPSession.ErrCode) )
                  {
                     if ( StringUtil.StringSearch( AV18Resultado, "sucesso", 1) == 0 )
                     {
                        AV18Resultado = AV18Resultado + " para " + AV11Destinatarios + " com sucesso";
                     }
                  }
                  else
                  {
                     AV18Resultado = AV18Resultado + " retornou: " + AV19SMTPSession.ErrDescription;
                     AV16Message.gxTpr_Description = AV19SMTPSession.ErrDescription;
                     AV17Messages.Add(AV16Message, 0);
                  }
                  AV19SMTPSession.Logout();
               }
               else
               {
                  AV18Resultado = "Login para " + StringUtil.Lower( AV18Resultado) + StringUtil.Trim( AV19SMTPSession.Sender.Address) + " retornou: " + AV19SMTPSession.ErrDescription;
               }
            }
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV22Usuarios = new GxSimpleCollection();
         AV23WebSession = context.GetSession();
         scmdbuf = "";
         P00BO2_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         P00BO2_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         P00BO2_A538Usuario_EhGestor = new bool[] {false} ;
         P00BO2_n538Usuario_EhGestor = new bool[] {false} ;
         P00BO2_A516Contratada_TipoFabrica = new String[] {""} ;
         P00BO2_n516Contratada_TipoFabrica = new bool[] {false} ;
         P00BO2_A1228ContratadaUsuario_AreaTrabalhoCod = new int[1] ;
         P00BO2_n1228ContratadaUsuario_AreaTrabalhoCod = new bool[] {false} ;
         P00BO2_A341Usuario_UserGamGuid = new String[] {""} ;
         P00BO2_n341Usuario_UserGamGuid = new bool[] {false} ;
         A516Contratada_TipoFabrica = "";
         A341Usuario_UserGamGuid = "";
         AV14GamUser = new SdtGAMUser(context);
         AV15MailRecipient = new GeneXus.Mail.GXMailRecipient();
         AV12Email = new GeneXus.Mail.GXMailMessage();
         AV11Destinatarios = "";
         P00BO3_A1Usuario_Codigo = new int[1] ;
         P00BO3_A1647Usuario_Email = new String[] {""} ;
         P00BO3_n1647Usuario_Email = new bool[] {false} ;
         P00BO3_A341Usuario_UserGamGuid = new String[] {""} ;
         P00BO3_n341Usuario_UserGamGuid = new bool[] {false} ;
         A1647Usuario_Email = "";
         P00BO4_A331ParametrosSistema_NomeSistema = new String[] {""} ;
         P00BO4_A330ParametrosSistema_Codigo = new int[1] ;
         A331ParametrosSistema_NomeSistema = "";
         AV26NomeSistema = "";
         P00BO5_A29Contratante_Codigo = new int[1] ;
         P00BO5_n29Contratante_Codigo = new bool[] {false} ;
         P00BO5_A5AreaTrabalho_Codigo = new int[1] ;
         P00BO5_A548Contratante_EmailSdaUser = new String[] {""} ;
         P00BO5_n548Contratante_EmailSdaUser = new bool[] {false} ;
         P00BO5_A547Contratante_EmailSdaHost = new String[] {""} ;
         P00BO5_n547Contratante_EmailSdaHost = new bool[] {false} ;
         P00BO5_A549Contratante_EmailSdaPass = new String[] {""} ;
         P00BO5_n549Contratante_EmailSdaPass = new bool[] {false} ;
         P00BO5_A550Contratante_EmailSdaKey = new String[] {""} ;
         P00BO5_n550Contratante_EmailSdaKey = new bool[] {false} ;
         P00BO5_A552Contratante_EmailSdaPort = new short[1] ;
         P00BO5_n552Contratante_EmailSdaPort = new bool[] {false} ;
         P00BO5_A551Contratante_EmailSdaAut = new bool[] {false} ;
         P00BO5_n551Contratante_EmailSdaAut = new bool[] {false} ;
         P00BO5_A1048Contratante_EmailSdaSec = new short[1] ;
         P00BO5_n1048Contratante_EmailSdaSec = new bool[] {false} ;
         A548Contratante_EmailSdaUser = "";
         A547Contratante_EmailSdaHost = "";
         A549Contratante_EmailSdaPass = "";
         A550Contratante_EmailSdaKey = "";
         AV19SMTPSession = new GeneXus.Mail.GXSMTPSession(context.GetPhysicalPath());
         AV9Attachment = "";
         AV16Message = new SdtMessages_Message(context);
         AV17Messages = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_enviaremailsemnotificacao__default(),
            new Object[][] {
                new Object[] {
               P00BO2_A66ContratadaUsuario_ContratadaCod, P00BO2_A69ContratadaUsuario_UsuarioCod, P00BO2_A538Usuario_EhGestor, P00BO2_n538Usuario_EhGestor, P00BO2_A516Contratada_TipoFabrica, P00BO2_n516Contratada_TipoFabrica, P00BO2_A1228ContratadaUsuario_AreaTrabalhoCod, P00BO2_n1228ContratadaUsuario_AreaTrabalhoCod, P00BO2_A341Usuario_UserGamGuid, P00BO2_n341Usuario_UserGamGuid
               }
               , new Object[] {
               P00BO3_A1Usuario_Codigo, P00BO3_A1647Usuario_Email, P00BO3_n1647Usuario_Email, P00BO3_A341Usuario_UserGamGuid
               }
               , new Object[] {
               P00BO4_A331ParametrosSistema_NomeSistema, P00BO4_A330ParametrosSistema_Codigo
               }
               , new Object[] {
               P00BO5_A29Contratante_Codigo, P00BO5_n29Contratante_Codigo, P00BO5_A5AreaTrabalho_Codigo, P00BO5_A548Contratante_EmailSdaUser, P00BO5_n548Contratante_EmailSdaUser, P00BO5_A547Contratante_EmailSdaHost, P00BO5_n547Contratante_EmailSdaHost, P00BO5_A549Contratante_EmailSdaPass, P00BO5_n549Contratante_EmailSdaPass, P00BO5_A550Contratante_EmailSdaKey,
               P00BO5_n550Contratante_EmailSdaKey, P00BO5_A552Contratante_EmailSdaPort, P00BO5_n552Contratante_EmailSdaPort, P00BO5_A551Contratante_EmailSdaAut, P00BO5_n551Contratante_EmailSdaAut, P00BO5_A1048Contratante_EmailSdaSec, P00BO5_n1048Contratante_EmailSdaSec
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV25i ;
      private short A552Contratante_EmailSdaPort ;
      private short A1048Contratante_EmailSdaSec ;
      private int AV8AreaTrabalho_Codigo ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A1228ContratadaUsuario_AreaTrabalhoCod ;
      private int A1Usuario_Codigo ;
      private int A330ParametrosSistema_Codigo ;
      private int A29Contratante_Codigo ;
      private int A5AreaTrabalho_Codigo ;
      private int AV35GXV1 ;
      private String AV20Subject ;
      private String AV13EmailText ;
      private String AV18Resultado ;
      private String scmdbuf ;
      private String A516Contratada_TipoFabrica ;
      private String A341Usuario_UserGamGuid ;
      private String A331ParametrosSistema_NomeSistema ;
      private String AV26NomeSistema ;
      private String A550Contratante_EmailSdaKey ;
      private bool A538Usuario_EhGestor ;
      private bool n538Usuario_EhGestor ;
      private bool n516Contratada_TipoFabrica ;
      private bool n1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool n341Usuario_UserGamGuid ;
      private bool n1647Usuario_Email ;
      private bool n29Contratante_Codigo ;
      private bool n548Contratante_EmailSdaUser ;
      private bool n547Contratante_EmailSdaHost ;
      private bool n549Contratante_EmailSdaPass ;
      private bool n550Contratante_EmailSdaKey ;
      private bool n552Contratante_EmailSdaPort ;
      private bool A551Contratante_EmailSdaAut ;
      private bool n551Contratante_EmailSdaAut ;
      private bool n1048Contratante_EmailSdaSec ;
      private String AV9Attachment ;
      private String AV11Destinatarios ;
      private String A1647Usuario_Email ;
      private String A548Contratante_EmailSdaUser ;
      private String A547Contratante_EmailSdaHost ;
      private String A549Contratante_EmailSdaPass ;
      private IGxSession AV23WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP4_Resultado ;
      private IDataStoreProvider pr_default ;
      private int[] P00BO2_A66ContratadaUsuario_ContratadaCod ;
      private int[] P00BO2_A69ContratadaUsuario_UsuarioCod ;
      private bool[] P00BO2_A538Usuario_EhGestor ;
      private bool[] P00BO2_n538Usuario_EhGestor ;
      private String[] P00BO2_A516Contratada_TipoFabrica ;
      private bool[] P00BO2_n516Contratada_TipoFabrica ;
      private int[] P00BO2_A1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] P00BO2_n1228ContratadaUsuario_AreaTrabalhoCod ;
      private String[] P00BO2_A341Usuario_UserGamGuid ;
      private bool[] P00BO2_n341Usuario_UserGamGuid ;
      private int[] P00BO3_A1Usuario_Codigo ;
      private String[] P00BO3_A1647Usuario_Email ;
      private bool[] P00BO3_n1647Usuario_Email ;
      private String[] P00BO3_A341Usuario_UserGamGuid ;
      private bool[] P00BO3_n341Usuario_UserGamGuid ;
      private String[] P00BO4_A331ParametrosSistema_NomeSistema ;
      private int[] P00BO4_A330ParametrosSistema_Codigo ;
      private int[] P00BO5_A29Contratante_Codigo ;
      private bool[] P00BO5_n29Contratante_Codigo ;
      private int[] P00BO5_A5AreaTrabalho_Codigo ;
      private String[] P00BO5_A548Contratante_EmailSdaUser ;
      private bool[] P00BO5_n548Contratante_EmailSdaUser ;
      private String[] P00BO5_A547Contratante_EmailSdaHost ;
      private bool[] P00BO5_n547Contratante_EmailSdaHost ;
      private String[] P00BO5_A549Contratante_EmailSdaPass ;
      private bool[] P00BO5_n549Contratante_EmailSdaPass ;
      private String[] P00BO5_A550Contratante_EmailSdaKey ;
      private bool[] P00BO5_n550Contratante_EmailSdaKey ;
      private short[] P00BO5_A552Contratante_EmailSdaPort ;
      private bool[] P00BO5_n552Contratante_EmailSdaPort ;
      private bool[] P00BO5_A551Contratante_EmailSdaAut ;
      private bool[] P00BO5_n551Contratante_EmailSdaAut ;
      private short[] P00BO5_A1048Contratante_EmailSdaSec ;
      private bool[] P00BO5_n1048Contratante_EmailSdaSec ;
      private GeneXus.Mail.GXMailMessage AV12Email ;
      private GeneXus.Mail.GXMailRecipient AV15MailRecipient ;
      private GeneXus.Mail.GXSMTPSession AV19SMTPSession ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV22Usuarios ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV10Attachments ;
      [ObjectCollection(ItemType=typeof( SdtMessages_Message ))]
      private IGxCollection AV17Messages ;
      private SdtGAMUser AV14GamUser ;
      private SdtMessages_Message AV16Message ;
   }

   public class prc_enviaremailsemnotificacao__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00BO3( IGxContext context ,
                                             int A1Usuario_Codigo ,
                                             IGxCollection AV22Usuarios )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object1 ;
         GXv_Object1 = new Object [2] ;
         scmdbuf = "SELECT [Usuario_Codigo], [Usuario_Email], [Usuario_UserGamGuid] FROM [Usuario] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV22Usuarios, "[Usuario_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [Usuario_Codigo]";
         GXv_Object1[0] = scmdbuf;
         return GXv_Object1 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 1 :
                     return conditional_P00BO3(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00BO2 ;
          prmP00BO2 = new Object[] {
          new Object[] {"@AV8AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00BO4 ;
          prmP00BO4 = new Object[] {
          } ;
          Object[] prmP00BO5 ;
          prmP00BO5 = new Object[] {
          new Object[] {"@AV8AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00BO3 ;
          prmP00BO3 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("P00BO2", "SELECT T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T3.[Usuario_EhGestor], T2.[Contratada_TipoFabrica], T2.[Contratada_AreaTrabalhoCod] AS ContratadaUsuario_AreaTrabalhoCod, T3.[Usuario_UserGamGuid] FROM (([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) WHERE (T3.[Usuario_EhGestor] = 1) AND (T2.[Contratada_AreaTrabalhoCod] = @AV8AreaTrabalho_Codigo) AND (T2.[Contratada_TipoFabrica] = 'M') ORDER BY T1.[ContratadaUsuario_ContratadaCod], T1.[ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00BO2,100,0,true,false )
             ,new CursorDef("P00BO3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00BO3,100,0,true,false )
             ,new CursorDef("P00BO4", "SELECT TOP 1 [ParametrosSistema_NomeSistema], [ParametrosSistema_Codigo] FROM [ParametrosSistema] WITH (NOLOCK) ORDER BY [ParametrosSistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00BO4,1,0,false,true )
             ,new CursorDef("P00BO5", "SELECT TOP 1 T1.[Contratante_Codigo], T1.[AreaTrabalho_Codigo], T2.[Contratante_EmailSdaUser], T2.[Contratante_EmailSdaHost], T2.[Contratante_EmailSdaPass], T2.[Contratante_EmailSdaKey], T2.[Contratante_EmailSdaPort], T2.[Contratante_EmailSdaAut], T2.[Contratante_EmailSdaSec] FROM ([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[Contratante_Codigo]) WHERE T1.[AreaTrabalho_Codigo] = @AV8AreaTrabalho_Codigo ORDER BY T1.[AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00BO5,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 40) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 40) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 32) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((short[]) buf[11])[0] = rslt.getShort(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((bool[]) buf[13])[0] = rslt.getBool(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((short[]) buf[15])[0] = rslt.getShort(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
