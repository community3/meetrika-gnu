/*
               File: PRC_ContratanteSelUsrPrestadora
        Description: Verifica se a Contratante Seleciona o usu�rio da Prestadora
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:13:47.89
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_contratanteselusrprestadora : GXProcedure
   {
      public prc_contratanteselusrprestadora( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_contratanteselusrprestadora( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Contratante_Codigo ,
                           ref bool aP1_Seleciona )
      {
         this.A29Contratante_Codigo = aP0_Contratante_Codigo;
         this.AV8Seleciona = aP1_Seleciona;
         initialize();
         executePrivate();
         aP0_Contratante_Codigo=this.A29Contratante_Codigo;
         aP1_Seleciona=this.AV8Seleciona;
      }

      public bool executeUdp( ref int aP0_Contratante_Codigo )
      {
         this.A29Contratante_Codigo = aP0_Contratante_Codigo;
         this.AV8Seleciona = aP1_Seleciona;
         initialize();
         executePrivate();
         aP0_Contratante_Codigo=this.A29Contratante_Codigo;
         aP1_Seleciona=this.AV8Seleciona;
         return AV8Seleciona ;
      }

      public void executeSubmit( ref int aP0_Contratante_Codigo ,
                                 ref bool aP1_Seleciona )
      {
         prc_contratanteselusrprestadora objprc_contratanteselusrprestadora;
         objprc_contratanteselusrprestadora = new prc_contratanteselusrprestadora();
         objprc_contratanteselusrprestadora.A29Contratante_Codigo = aP0_Contratante_Codigo;
         objprc_contratanteselusrprestadora.AV8Seleciona = aP1_Seleciona;
         objprc_contratanteselusrprestadora.context.SetSubmitInitialConfig(context);
         objprc_contratanteselusrprestadora.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_contratanteselusrprestadora);
         aP0_Contratante_Codigo=this.A29Contratante_Codigo;
         aP1_Seleciona=this.AV8Seleciona;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_contratanteselusrprestadora)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00XO2 */
         pr_default.execute(0, new Object[] {A29Contratante_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A2089Contratante_SelecionaResponsavelOS = P00XO2_A2089Contratante_SelecionaResponsavelOS[0];
            AV8Seleciona = A2089Contratante_SelecionaResponsavelOS;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00XO2_A29Contratante_Codigo = new int[1] ;
         P00XO2_A2089Contratante_SelecionaResponsavelOS = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_contratanteselusrprestadora__default(),
            new Object[][] {
                new Object[] {
               P00XO2_A29Contratante_Codigo, P00XO2_A2089Contratante_SelecionaResponsavelOS
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A29Contratante_Codigo ;
      private String scmdbuf ;
      private bool AV8Seleciona ;
      private bool A2089Contratante_SelecionaResponsavelOS ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Contratante_Codigo ;
      private bool aP1_Seleciona ;
      private IDataStoreProvider pr_default ;
      private int[] P00XO2_A29Contratante_Codigo ;
      private bool[] P00XO2_A2089Contratante_SelecionaResponsavelOS ;
   }

   public class prc_contratanteselusrprestadora__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00XO2 ;
          prmP00XO2 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00XO2", "SELECT TOP 1 [Contratante_Codigo], [Contratante_SelecionaResponsavelOS] FROM [Contratante] WITH (NOLOCK) WHERE [Contratante_Codigo] = @Contratante_Codigo ORDER BY [Contratante_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00XO2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
