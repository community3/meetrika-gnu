/*
               File: PRC_JaRecebido
        Description: Ja Recebido
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:8:30.8
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_jarecebido : GXProcedure
   {
      public prc_jarecebido( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_jarecebido( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultado_Codigo ,
                           out decimal aP1_JaRecebido )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV10JaRecebido = 0 ;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP1_JaRecebido=this.AV10JaRecebido;
      }

      public decimal executeUdp( ref int aP0_ContagemResultado_Codigo )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV10JaRecebido = 0 ;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP1_JaRecebido=this.AV10JaRecebido;
         return AV10JaRecebido ;
      }

      public void executeSubmit( ref int aP0_ContagemResultado_Codigo ,
                                 out decimal aP1_JaRecebido )
      {
         prc_jarecebido objprc_jarecebido;
         objprc_jarecebido = new prc_jarecebido();
         objprc_jarecebido.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_jarecebido.AV10JaRecebido = 0 ;
         objprc_jarecebido.context.SetSubmitInitialConfig(context);
         objprc_jarecebido.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_jarecebido);
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP1_JaRecebido=this.AV10JaRecebido;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_jarecebido)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00632 */
         pr_default.execute(0, new Object[] {A456ContagemResultado_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A484ContagemResultado_StatusDmn = P00632_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00632_n484ContagemResultado_StatusDmn[0];
            GXt_decimal1 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
            A574ContagemResultado_PFFinal = GXt_decimal1;
            AV10JaRecebido = A574ContagemResultado_PFFinal;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00632_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00632_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00632_A456ContagemResultado_Codigo = new int[1] ;
         A484ContagemResultado_StatusDmn = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_jarecebido__default(),
            new Object[][] {
                new Object[] {
               P00632_A484ContagemResultado_StatusDmn, P00632_n484ContagemResultado_StatusDmn, P00632_A456ContagemResultado_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A456ContagemResultado_Codigo ;
      private decimal AV10JaRecebido ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal GXt_decimal1 ;
      private String scmdbuf ;
      private String A484ContagemResultado_StatusDmn ;
      private bool n484ContagemResultado_StatusDmn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultado_Codigo ;
      private IDataStoreProvider pr_default ;
      private String[] P00632_A484ContagemResultado_StatusDmn ;
      private bool[] P00632_n484ContagemResultado_StatusDmn ;
      private int[] P00632_A456ContagemResultado_Codigo ;
      private decimal aP1_JaRecebido ;
   }

   public class prc_jarecebido__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00632 ;
          prmP00632 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00632", "SELECT TOP 1 [ContagemResultado_StatusDmn], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE ([ContagemResultado_Codigo] = @ContagemResultado_Codigo) AND ([ContagemResultado_StatusDmn] = 'R' or [ContagemResultado_StatusDmn] = 'C' or [ContagemResultado_StatusDmn] = 'H' or [ContagemResultado_StatusDmn] = 'O' or [ContagemResultado_StatusDmn] = 'P' or [ContagemResultado_StatusDmn] = 'L') ORDER BY [ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00632,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
