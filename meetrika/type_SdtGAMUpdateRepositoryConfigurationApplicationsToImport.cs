/*
               File: type_SdtGAMUpdateRepositoryConfigurationApplicationsToImport
        Description: GAMUpdateRepositoryConfigurationApplicationsToImport
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/26/2020 3:44:41.57
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtGAMUpdateRepositoryConfigurationApplicationsToImport : GxUserType, IGxExternalObject
   {
      public SdtGAMUpdateRepositoryConfigurationApplicationsToImport( )
      {
         initialize();
      }

      public SdtGAMUpdateRepositoryConfigurationApplicationsToImport( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public String tostring( )
      {
         String returntostring ;
         if ( GAMUpdateRepositoryConfigurationApplicationsToImport_externalReference == null )
         {
            GAMUpdateRepositoryConfigurationApplicationsToImport_externalReference = new Artech.Security.GAMUpdateRepositoryConfigurationApplicationsToImport(context);
         }
         returntostring = "";
         returntostring = (String)(GAMUpdateRepositoryConfigurationApplicationsToImport_externalReference.ToString());
         return returntostring ;
      }

      public String gxTpr_Identifier
      {
         get {
            if ( GAMUpdateRepositoryConfigurationApplicationsToImport_externalReference == null )
            {
               GAMUpdateRepositoryConfigurationApplicationsToImport_externalReference = new Artech.Security.GAMUpdateRepositoryConfigurationApplicationsToImport(context);
            }
            return GAMUpdateRepositoryConfigurationApplicationsToImport_externalReference.Identifier ;
         }

         set {
            if ( GAMUpdateRepositoryConfigurationApplicationsToImport_externalReference == null )
            {
               GAMUpdateRepositoryConfigurationApplicationsToImport_externalReference = new Artech.Security.GAMUpdateRepositoryConfigurationApplicationsToImport(context);
            }
            GAMUpdateRepositoryConfigurationApplicationsToImport_externalReference.Identifier = value;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( GAMUpdateRepositoryConfigurationApplicationsToImport_externalReference == null )
            {
               GAMUpdateRepositoryConfigurationApplicationsToImport_externalReference = new Artech.Security.GAMUpdateRepositoryConfigurationApplicationsToImport(context);
            }
            return GAMUpdateRepositoryConfigurationApplicationsToImport_externalReference ;
         }

         set {
            GAMUpdateRepositoryConfigurationApplicationsToImport_externalReference = (Artech.Security.GAMUpdateRepositoryConfigurationApplicationsToImport)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected Artech.Security.GAMUpdateRepositoryConfigurationApplicationsToImport GAMUpdateRepositoryConfigurationApplicationsToImport_externalReference=null ;
   }

}
