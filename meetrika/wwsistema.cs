/*
               File: WWSistema
        Description:  Sistema
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/26/2020 9:12:31.57
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwsistema : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwsistema( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwsistema( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavSistema_tipo1 = new GXCombobox();
         cmbavSistema_tecnica1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavSistema_tipo2 = new GXCombobox();
         cmbavSistema_tecnica2 = new GXCombobox();
         cmbSistema_Tecnica = new GXCombobox();
         chkSistema_Ativo = new GXCheckbox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_89 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_89_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_89_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV67Sistema_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67Sistema_Nome1", AV67Sistema_Nome1);
               AV62Sistema_Sigla1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62Sistema_Sigla1", AV62Sistema_Sigla1);
               AV73Sistema_Tipo1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73Sistema_Tipo1", AV73Sistema_Tipo1);
               AV75Sistema_Tecnica1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75Sistema_Tecnica1", AV75Sistema_Tecnica1);
               AV76Sistema_Coordenacao1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76Sistema_Coordenacao1", AV76Sistema_Coordenacao1);
               AV80Sistema_ProjetoNome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80Sistema_ProjetoNome1", AV80Sistema_ProjetoNome1);
               AV21DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               AV68Sistema_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68Sistema_Nome2", AV68Sistema_Nome2);
               AV63Sistema_Sigla2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63Sistema_Sigla2", AV63Sistema_Sigla2);
               AV74Sistema_Tipo2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74Sistema_Tipo2", AV74Sistema_Tipo2);
               AV77Sistema_Tecnica2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77Sistema_Tecnica2", AV77Sistema_Tecnica2);
               AV78Sistema_Coordenacao2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78Sistema_Coordenacao2", AV78Sistema_Coordenacao2);
               AV81Sistema_ProjetoNome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81Sistema_ProjetoNome2", AV81Sistema_ProjetoNome2);
               AV20DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV88TFSistema_Sigla = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88TFSistema_Sigla", AV88TFSistema_Sigla);
               AV89TFSistema_Sigla_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89TFSistema_Sigla_Sel", AV89TFSistema_Sigla_Sel);
               AV100TFSistema_Coordenacao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV100TFSistema_Coordenacao", AV100TFSistema_Coordenacao);
               AV101TFSistema_Coordenacao_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV101TFSistema_Coordenacao_Sel", AV101TFSistema_Coordenacao_Sel);
               AV104TFAmbienteTecnologico_Descricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV104TFAmbienteTecnologico_Descricao", AV104TFAmbienteTecnologico_Descricao);
               AV105TFAmbienteTecnologico_Descricao_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105TFAmbienteTecnologico_Descricao_Sel", AV105TFAmbienteTecnologico_Descricao_Sel);
               AV108TFMetodologia_Descricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV108TFMetodologia_Descricao", AV108TFMetodologia_Descricao);
               AV109TFMetodologia_Descricao_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV109TFMetodologia_Descricao_Sel", AV109TFMetodologia_Descricao_Sel);
               AV120TFSistema_ProjetoNome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV120TFSistema_ProjetoNome", AV120TFSistema_ProjetoNome);
               AV121TFSistema_ProjetoNome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV121TFSistema_ProjetoNome_Sel", AV121TFSistema_ProjetoNome_Sel);
               AV140TFSistemaVersao_Id = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV140TFSistemaVersao_Id", AV140TFSistemaVersao_Id);
               AV141TFSistemaVersao_Id_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV141TFSistemaVersao_Id_Sel", AV141TFSistemaVersao_Id_Sel);
               AV124TFSistema_Ativo_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV124TFSistema_Ativo_Sel", StringUtil.Str( (decimal)(AV124TFSistema_Ativo_Sel), 1, 0));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               AV90ddo_Sistema_SiglaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90ddo_Sistema_SiglaTitleControlIdToReplace", AV90ddo_Sistema_SiglaTitleControlIdToReplace);
               AV98ddo_Sistema_TecnicaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV98ddo_Sistema_TecnicaTitleControlIdToReplace", AV98ddo_Sistema_TecnicaTitleControlIdToReplace);
               AV102ddo_Sistema_CoordenacaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV102ddo_Sistema_CoordenacaoTitleControlIdToReplace", AV102ddo_Sistema_CoordenacaoTitleControlIdToReplace);
               AV106ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV106ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace", AV106ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace);
               AV110ddo_Metodologia_DescricaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV110ddo_Metodologia_DescricaoTitleControlIdToReplace", AV110ddo_Metodologia_DescricaoTitleControlIdToReplace);
               AV122ddo_Sistema_ProjetoNomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV122ddo_Sistema_ProjetoNomeTitleControlIdToReplace", AV122ddo_Sistema_ProjetoNomeTitleControlIdToReplace);
               AV142ddo_SistemaVersao_IdTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV142ddo_SistemaVersao_IdTitleControlIdToReplace", AV142ddo_SistemaVersao_IdTitleControlIdToReplace);
               AV125ddo_Sistema_AtivoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV125ddo_Sistema_AtivoTitleControlIdToReplace", AV125ddo_Sistema_AtivoTitleControlIdToReplace);
               AV137Paginando = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV137Paginando", AV137Paginando);
               AV134Registros = (long)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV134Registros", StringUtil.LTrim( StringUtil.Str( (decimal)(AV134Registros), 10, 0)));
               AV135PreView = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV135PreView", StringUtil.LTrim( StringUtil.Str( (decimal)(AV135PreView), 4, 0)));
               AV136PaginaAtual = (long)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV136PaginaAtual", StringUtil.LTrim( StringUtil.Str( (decimal)(AV136PaginaAtual), 10, 0)));
               AV60Sistema_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60Sistema_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60Sistema_AreaTrabalhoCod), 6, 0)));
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV82Sistema_PF1 = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82Sistema_PF1", StringUtil.LTrim( StringUtil.Str( AV82Sistema_PF1, 14, 5)));
               AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
               AV83Sistema_PF2 = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83Sistema_PF2", StringUtil.LTrim( StringUtil.Str( AV83Sistema_PF2, 14, 5)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV97TFSistema_Tecnica_Sels);
               AV183Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV33DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33DynamicFiltersIgnoreFirst", AV33DynamicFiltersIgnoreFirst);
               AV32DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
               A127Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A351AmbienteTecnologico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n351AmbienteTecnologico_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A351AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0)));
               A137Metodologia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n137Metodologia_Codigo = false;
               A691Sistema_ProjetoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n691Sistema_ProjetoCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A691Sistema_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A691Sistema_ProjetoCod), 6, 0)));
               A1859SistemaVersao_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n1859SistemaVersao_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1859SistemaVersao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1859SistemaVersao_Codigo), 6, 0)));
               A130Sistema_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
               A707Sistema_TipoSigla = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A707Sistema_TipoSigla", A707Sistema_TipoSigla);
               A395Sistema_PF = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A395Sistema_PF", StringUtil.LTrim( StringUtil.Str( A395Sistema_PF, 14, 5)));
               A690Sistema_PFA = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A690Sistema_PFA", StringUtil.LTrim( StringUtil.Str( A690Sistema_PFA, 14, 5)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV67Sistema_Nome1, AV62Sistema_Sigla1, AV73Sistema_Tipo1, AV75Sistema_Tecnica1, AV76Sistema_Coordenacao1, AV80Sistema_ProjetoNome1, AV21DynamicFiltersSelector2, AV68Sistema_Nome2, AV63Sistema_Sigla2, AV74Sistema_Tipo2, AV77Sistema_Tecnica2, AV78Sistema_Coordenacao2, AV81Sistema_ProjetoNome2, AV20DynamicFiltersEnabled2, AV88TFSistema_Sigla, AV89TFSistema_Sigla_Sel, AV100TFSistema_Coordenacao, AV101TFSistema_Coordenacao_Sel, AV104TFAmbienteTecnologico_Descricao, AV105TFAmbienteTecnologico_Descricao_Sel, AV108TFMetodologia_Descricao, AV109TFMetodologia_Descricao_Sel, AV120TFSistema_ProjetoNome, AV121TFSistema_ProjetoNome_Sel, AV140TFSistemaVersao_Id, AV141TFSistemaVersao_Id_Sel, AV124TFSistema_Ativo_Sel, AV6WWPContext, AV90ddo_Sistema_SiglaTitleControlIdToReplace, AV98ddo_Sistema_TecnicaTitleControlIdToReplace, AV102ddo_Sistema_CoordenacaoTitleControlIdToReplace, AV106ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, AV110ddo_Metodologia_DescricaoTitleControlIdToReplace, AV122ddo_Sistema_ProjetoNomeTitleControlIdToReplace, AV142ddo_SistemaVersao_IdTitleControlIdToReplace, AV125ddo_Sistema_AtivoTitleControlIdToReplace, AV137Paginando, AV134Registros, AV135PreView, AV136PaginaAtual, AV60Sistema_AreaTrabalhoCod, AV16DynamicFiltersOperator1, AV82Sistema_PF1, AV22DynamicFiltersOperator2, AV83Sistema_PF2, AV97TFSistema_Tecnica_Sels, AV183Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A127Sistema_Codigo, A351AmbienteTecnologico_Codigo, A137Metodologia_Codigo, A691Sistema_ProjetoCod, A1859SistemaVersao_Codigo, A130Sistema_Ativo, A707Sistema_TipoSigla, A395Sistema_PF, A690Sistema_PFA) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA3G2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START3G2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203269123264");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("Window/InNewWindowRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwsistema.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vSISTEMA_NOME1", AV67Sistema_Nome1);
         GxWebStd.gx_hidden_field( context, "GXH_vSISTEMA_SIGLA1", StringUtil.RTrim( AV62Sistema_Sigla1));
         GxWebStd.gx_hidden_field( context, "GXH_vSISTEMA_TIPO1", StringUtil.RTrim( AV73Sistema_Tipo1));
         GxWebStd.gx_hidden_field( context, "GXH_vSISTEMA_TECNICA1", StringUtil.RTrim( AV75Sistema_Tecnica1));
         GxWebStd.gx_hidden_field( context, "GXH_vSISTEMA_COORDENACAO1", AV76Sistema_Coordenacao1);
         GxWebStd.gx_hidden_field( context, "GXH_vSISTEMA_PROJETONOME1", StringUtil.RTrim( AV80Sistema_ProjetoNome1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV21DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vSISTEMA_NOME2", AV68Sistema_Nome2);
         GxWebStd.gx_hidden_field( context, "GXH_vSISTEMA_SIGLA2", StringUtil.RTrim( AV63Sistema_Sigla2));
         GxWebStd.gx_hidden_field( context, "GXH_vSISTEMA_TIPO2", StringUtil.RTrim( AV74Sistema_Tipo2));
         GxWebStd.gx_hidden_field( context, "GXH_vSISTEMA_TECNICA2", StringUtil.RTrim( AV77Sistema_Tecnica2));
         GxWebStd.gx_hidden_field( context, "GXH_vSISTEMA_COORDENACAO2", AV78Sistema_Coordenacao2);
         GxWebStd.gx_hidden_field( context, "GXH_vSISTEMA_PROJETONOME2", StringUtil.RTrim( AV81Sistema_ProjetoNome2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV20DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSISTEMA_SIGLA", StringUtil.RTrim( AV88TFSistema_Sigla));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSISTEMA_SIGLA_SEL", StringUtil.RTrim( AV89TFSistema_Sigla_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSISTEMA_COORDENACAO", AV100TFSistema_Coordenacao);
         GxWebStd.gx_hidden_field( context, "GXH_vTFSISTEMA_COORDENACAO_SEL", AV101TFSistema_Coordenacao_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFAMBIENTETECNOLOGICO_DESCRICAO", AV104TFAmbienteTecnologico_Descricao);
         GxWebStd.gx_hidden_field( context, "GXH_vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL", AV105TFAmbienteTecnologico_Descricao_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFMETODOLOGIA_DESCRICAO", AV108TFMetodologia_Descricao);
         GxWebStd.gx_hidden_field( context, "GXH_vTFMETODOLOGIA_DESCRICAO_SEL", AV109TFMetodologia_Descricao_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFSISTEMA_PROJETONOME", StringUtil.RTrim( AV120TFSistema_ProjetoNome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSISTEMA_PROJETONOME_SEL", StringUtil.RTrim( AV121TFSistema_ProjetoNome_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSISTEMAVERSAO_ID", StringUtil.RTrim( AV140TFSistemaVersao_Id));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSISTEMAVERSAO_ID_SEL", StringUtil.RTrim( AV141TFSistemaVersao_Id_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSISTEMA_ATIVO_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV124TFSistema_Ativo_Sel), 1, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_89", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_89), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV128GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV129GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV126DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV126DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSISTEMA_SIGLATITLEFILTERDATA", AV87Sistema_SiglaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSISTEMA_SIGLATITLEFILTERDATA", AV87Sistema_SiglaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSISTEMA_TECNICATITLEFILTERDATA", AV95Sistema_TecnicaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSISTEMA_TECNICATITLEFILTERDATA", AV95Sistema_TecnicaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSISTEMA_COORDENACAOTITLEFILTERDATA", AV99Sistema_CoordenacaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSISTEMA_COORDENACAOTITLEFILTERDATA", AV99Sistema_CoordenacaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAMBIENTETECNOLOGICO_DESCRICAOTITLEFILTERDATA", AV103AmbienteTecnologico_DescricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAMBIENTETECNOLOGICO_DESCRICAOTITLEFILTERDATA", AV103AmbienteTecnologico_DescricaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vMETODOLOGIA_DESCRICAOTITLEFILTERDATA", AV107Metodologia_DescricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMETODOLOGIA_DESCRICAOTITLEFILTERDATA", AV107Metodologia_DescricaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSISTEMA_PROJETONOMETITLEFILTERDATA", AV119Sistema_ProjetoNomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSISTEMA_PROJETONOMETITLEFILTERDATA", AV119Sistema_ProjetoNomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSISTEMAVERSAO_IDTITLEFILTERDATA", AV139SistemaVersao_IdTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSISTEMAVERSAO_IDTITLEFILTERDATA", AV139SistemaVersao_IdTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSISTEMA_ATIVOTITLEFILTERDATA", AV123Sistema_AtivoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSISTEMA_ATIVOTITLEFILTERDATA", AV123Sistema_AtivoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vPAGINANDO", AV137Paginando);
         GxWebStd.gx_hidden_field( context, "vREGISTROS", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV134Registros), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPREVIEW", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV135PreView), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPAGINAATUAL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV136PaginaAtual), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFSISTEMA_TECNICA_SELS", AV97TFSistema_Tecnica_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFSISTEMA_TECNICA_SELS", AV97TFSistema_Tecnica_Sels);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV183Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV33DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV32DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "AMBIENTETECNOLOGICO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SISTEMA_PROJETOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A691Sistema_ProjetoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SISTEMAVERSAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1859SistemaVersao_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vNOTIFICATIONINFO", AV61NotificationInfo);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vNOTIFICATIONINFO", AV61NotificationInfo);
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SISTEMA_PF", StringUtil.LTrim( StringUtil.NToC( A395Sistema_PF, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "SISTEMA_FATORAJUSTE", StringUtil.LTrim( StringUtil.NToC( A686Sistema_FatorAjuste, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "SISTEMA_PFA", StringUtil.LTrim( StringUtil.NToC( A690Sistema_PFA, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "SISTEMA_TIPO", StringUtil.RTrim( A699Sistema_Tipo));
         GxWebStd.gx_hidden_field( context, "SISTEMA_TIPOSIGLA", StringUtil.RTrim( A707Sistema_TipoSigla));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "INNEWWINDOW1_Width", StringUtil.RTrim( Innewwindow1_Width));
         GxWebStd.gx_hidden_field( context, "INNEWWINDOW1_Height", StringUtil.RTrim( Innewwindow1_Height));
         GxWebStd.gx_hidden_field( context, "INNEWWINDOW1_Target", StringUtil.RTrim( Innewwindow1_Target));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Caption", StringUtil.RTrim( Ddo_sistema_sigla_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Tooltip", StringUtil.RTrim( Ddo_sistema_sigla_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Cls", StringUtil.RTrim( Ddo_sistema_sigla_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Filteredtext_set", StringUtil.RTrim( Ddo_sistema_sigla_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Selectedvalue_set", StringUtil.RTrim( Ddo_sistema_sigla_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Dropdownoptionstype", StringUtil.RTrim( Ddo_sistema_sigla_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_sistema_sigla_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Includesortasc", StringUtil.BoolToStr( Ddo_sistema_sigla_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Includesortdsc", StringUtil.BoolToStr( Ddo_sistema_sigla_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Sortedstatus", StringUtil.RTrim( Ddo_sistema_sigla_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Includefilter", StringUtil.BoolToStr( Ddo_sistema_sigla_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Filtertype", StringUtil.RTrim( Ddo_sistema_sigla_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Filterisrange", StringUtil.BoolToStr( Ddo_sistema_sigla_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Includedatalist", StringUtil.BoolToStr( Ddo_sistema_sigla_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Datalisttype", StringUtil.RTrim( Ddo_sistema_sigla_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Datalistproc", StringUtil.RTrim( Ddo_sistema_sigla_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_sistema_sigla_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Sortasc", StringUtil.RTrim( Ddo_sistema_sigla_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Sortdsc", StringUtil.RTrim( Ddo_sistema_sigla_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Loadingdata", StringUtil.RTrim( Ddo_sistema_sigla_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Cleanfilter", StringUtil.RTrim( Ddo_sistema_sigla_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Noresultsfound", StringUtil.RTrim( Ddo_sistema_sigla_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Searchbuttontext", StringUtil.RTrim( Ddo_sistema_sigla_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_TECNICA_Caption", StringUtil.RTrim( Ddo_sistema_tecnica_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_TECNICA_Tooltip", StringUtil.RTrim( Ddo_sistema_tecnica_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_TECNICA_Cls", StringUtil.RTrim( Ddo_sistema_tecnica_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_TECNICA_Selectedvalue_set", StringUtil.RTrim( Ddo_sistema_tecnica_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_TECNICA_Dropdownoptionstype", StringUtil.RTrim( Ddo_sistema_tecnica_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_TECNICA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_sistema_tecnica_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_TECNICA_Includesortasc", StringUtil.BoolToStr( Ddo_sistema_tecnica_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_TECNICA_Includesortdsc", StringUtil.BoolToStr( Ddo_sistema_tecnica_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_TECNICA_Sortedstatus", StringUtil.RTrim( Ddo_sistema_tecnica_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_TECNICA_Includefilter", StringUtil.BoolToStr( Ddo_sistema_tecnica_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_TECNICA_Includedatalist", StringUtil.BoolToStr( Ddo_sistema_tecnica_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_TECNICA_Datalisttype", StringUtil.RTrim( Ddo_sistema_tecnica_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_TECNICA_Allowmultipleselection", StringUtil.BoolToStr( Ddo_sistema_tecnica_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_TECNICA_Datalistfixedvalues", StringUtil.RTrim( Ddo_sistema_tecnica_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_TECNICA_Sortasc", StringUtil.RTrim( Ddo_sistema_tecnica_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_TECNICA_Sortdsc", StringUtil.RTrim( Ddo_sistema_tecnica_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_TECNICA_Cleanfilter", StringUtil.RTrim( Ddo_sistema_tecnica_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_TECNICA_Searchbuttontext", StringUtil.RTrim( Ddo_sistema_tecnica_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_COORDENACAO_Caption", StringUtil.RTrim( Ddo_sistema_coordenacao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_COORDENACAO_Tooltip", StringUtil.RTrim( Ddo_sistema_coordenacao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_COORDENACAO_Cls", StringUtil.RTrim( Ddo_sistema_coordenacao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_COORDENACAO_Filteredtext_set", StringUtil.RTrim( Ddo_sistema_coordenacao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_COORDENACAO_Selectedvalue_set", StringUtil.RTrim( Ddo_sistema_coordenacao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_COORDENACAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_sistema_coordenacao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_COORDENACAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_sistema_coordenacao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_COORDENACAO_Includesortasc", StringUtil.BoolToStr( Ddo_sistema_coordenacao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_COORDENACAO_Includesortdsc", StringUtil.BoolToStr( Ddo_sistema_coordenacao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_COORDENACAO_Sortedstatus", StringUtil.RTrim( Ddo_sistema_coordenacao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_COORDENACAO_Includefilter", StringUtil.BoolToStr( Ddo_sistema_coordenacao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_COORDENACAO_Filtertype", StringUtil.RTrim( Ddo_sistema_coordenacao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_COORDENACAO_Filterisrange", StringUtil.BoolToStr( Ddo_sistema_coordenacao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_COORDENACAO_Includedatalist", StringUtil.BoolToStr( Ddo_sistema_coordenacao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_COORDENACAO_Datalisttype", StringUtil.RTrim( Ddo_sistema_coordenacao_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_COORDENACAO_Datalistproc", StringUtil.RTrim( Ddo_sistema_coordenacao_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_COORDENACAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_sistema_coordenacao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_COORDENACAO_Sortasc", StringUtil.RTrim( Ddo_sistema_coordenacao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_COORDENACAO_Sortdsc", StringUtil.RTrim( Ddo_sistema_coordenacao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_COORDENACAO_Loadingdata", StringUtil.RTrim( Ddo_sistema_coordenacao_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_COORDENACAO_Cleanfilter", StringUtil.RTrim( Ddo_sistema_coordenacao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_COORDENACAO_Noresultsfound", StringUtil.RTrim( Ddo_sistema_coordenacao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_COORDENACAO_Searchbuttontext", StringUtil.RTrim( Ddo_sistema_coordenacao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Caption", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Cls", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_ambientetecnologico_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_ambientetecnologico_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_ambientetecnologico_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_ambientetecnologico_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_ambientetecnologico_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_ambientetecnologico_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Caption", StringUtil.RTrim( Ddo_metodologia_descricao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_metodologia_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Cls", StringUtil.RTrim( Ddo_metodologia_descricao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_metodologia_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_metodologia_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_metodologia_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_metodologia_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_metodologia_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_metodologia_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_metodologia_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_metodologia_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_metodologia_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_metodologia_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_metodologia_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_metodologia_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_metodologia_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_metodologia_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_metodologia_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_metodologia_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_metodologia_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_metodologia_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_metodologia_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_metodologia_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_PROJETONOME_Caption", StringUtil.RTrim( Ddo_sistema_projetonome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_PROJETONOME_Tooltip", StringUtil.RTrim( Ddo_sistema_projetonome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_PROJETONOME_Cls", StringUtil.RTrim( Ddo_sistema_projetonome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_PROJETONOME_Filteredtext_set", StringUtil.RTrim( Ddo_sistema_projetonome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_PROJETONOME_Selectedvalue_set", StringUtil.RTrim( Ddo_sistema_projetonome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_PROJETONOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_sistema_projetonome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_PROJETONOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_sistema_projetonome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_PROJETONOME_Includesortasc", StringUtil.BoolToStr( Ddo_sistema_projetonome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_PROJETONOME_Includesortdsc", StringUtil.BoolToStr( Ddo_sistema_projetonome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_PROJETONOME_Sortedstatus", StringUtil.RTrim( Ddo_sistema_projetonome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_PROJETONOME_Includefilter", StringUtil.BoolToStr( Ddo_sistema_projetonome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_PROJETONOME_Filtertype", StringUtil.RTrim( Ddo_sistema_projetonome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_PROJETONOME_Filterisrange", StringUtil.BoolToStr( Ddo_sistema_projetonome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_PROJETONOME_Includedatalist", StringUtil.BoolToStr( Ddo_sistema_projetonome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_PROJETONOME_Datalisttype", StringUtil.RTrim( Ddo_sistema_projetonome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_PROJETONOME_Datalistproc", StringUtil.RTrim( Ddo_sistema_projetonome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_PROJETONOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_sistema_projetonome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_PROJETONOME_Sortasc", StringUtil.RTrim( Ddo_sistema_projetonome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_PROJETONOME_Sortdsc", StringUtil.RTrim( Ddo_sistema_projetonome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_PROJETONOME_Loadingdata", StringUtil.RTrim( Ddo_sistema_projetonome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_PROJETONOME_Cleanfilter", StringUtil.RTrim( Ddo_sistema_projetonome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_PROJETONOME_Noresultsfound", StringUtil.RTrim( Ddo_sistema_projetonome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_PROJETONOME_Searchbuttontext", StringUtil.RTrim( Ddo_sistema_projetonome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Caption", StringUtil.RTrim( Ddo_sistemaversao_id_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Tooltip", StringUtil.RTrim( Ddo_sistemaversao_id_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Cls", StringUtil.RTrim( Ddo_sistemaversao_id_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Filteredtext_set", StringUtil.RTrim( Ddo_sistemaversao_id_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Selectedvalue_set", StringUtil.RTrim( Ddo_sistemaversao_id_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Dropdownoptionstype", StringUtil.RTrim( Ddo_sistemaversao_id_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_sistemaversao_id_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Includesortasc", StringUtil.BoolToStr( Ddo_sistemaversao_id_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Includesortdsc", StringUtil.BoolToStr( Ddo_sistemaversao_id_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Sortedstatus", StringUtil.RTrim( Ddo_sistemaversao_id_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Includefilter", StringUtil.BoolToStr( Ddo_sistemaversao_id_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Filtertype", StringUtil.RTrim( Ddo_sistemaversao_id_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Filterisrange", StringUtil.BoolToStr( Ddo_sistemaversao_id_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Includedatalist", StringUtil.BoolToStr( Ddo_sistemaversao_id_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Datalisttype", StringUtil.RTrim( Ddo_sistemaversao_id_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Datalistproc", StringUtil.RTrim( Ddo_sistemaversao_id_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_sistemaversao_id_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Sortasc", StringUtil.RTrim( Ddo_sistemaversao_id_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Sortdsc", StringUtil.RTrim( Ddo_sistemaversao_id_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Loadingdata", StringUtil.RTrim( Ddo_sistemaversao_id_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Cleanfilter", StringUtil.RTrim( Ddo_sistemaversao_id_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Noresultsfound", StringUtil.RTrim( Ddo_sistemaversao_id_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Searchbuttontext", StringUtil.RTrim( Ddo_sistemaversao_id_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_ATIVO_Caption", StringUtil.RTrim( Ddo_sistema_ativo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_ATIVO_Tooltip", StringUtil.RTrim( Ddo_sistema_ativo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_ATIVO_Cls", StringUtil.RTrim( Ddo_sistema_ativo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_ATIVO_Selectedvalue_set", StringUtil.RTrim( Ddo_sistema_ativo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_ATIVO_Dropdownoptionstype", StringUtil.RTrim( Ddo_sistema_ativo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_ATIVO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_sistema_ativo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_ATIVO_Includesortasc", StringUtil.BoolToStr( Ddo_sistema_ativo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_ATIVO_Includesortdsc", StringUtil.BoolToStr( Ddo_sistema_ativo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_ATIVO_Sortedstatus", StringUtil.RTrim( Ddo_sistema_ativo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_ATIVO_Includefilter", StringUtil.BoolToStr( Ddo_sistema_ativo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_ATIVO_Includedatalist", StringUtil.BoolToStr( Ddo_sistema_ativo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_ATIVO_Datalisttype", StringUtil.RTrim( Ddo_sistema_ativo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_ATIVO_Datalistfixedvalues", StringUtil.RTrim( Ddo_sistema_ativo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_ATIVO_Sortasc", StringUtil.RTrim( Ddo_sistema_ativo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_ATIVO_Sortdsc", StringUtil.RTrim( Ddo_sistema_ativo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_ATIVO_Cleanfilter", StringUtil.RTrim( Ddo_sistema_ativo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_ATIVO_Searchbuttontext", StringUtil.RTrim( Ddo_sistema_ativo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Activeeventkey", StringUtil.RTrim( Ddo_sistema_sigla_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Filteredtext_get", StringUtil.RTrim( Ddo_sistema_sigla_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_SIGLA_Selectedvalue_get", StringUtil.RTrim( Ddo_sistema_sigla_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_TECNICA_Activeeventkey", StringUtil.RTrim( Ddo_sistema_tecnica_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_TECNICA_Selectedvalue_get", StringUtil.RTrim( Ddo_sistema_tecnica_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_COORDENACAO_Activeeventkey", StringUtil.RTrim( Ddo_sistema_coordenacao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_COORDENACAO_Filteredtext_get", StringUtil.RTrim( Ddo_sistema_coordenacao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_COORDENACAO_Selectedvalue_get", StringUtil.RTrim( Ddo_sistema_coordenacao_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_ambientetecnologico_descricao_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_metodologia_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_metodologia_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_METODOLOGIA_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_metodologia_descricao_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_PROJETONOME_Activeeventkey", StringUtil.RTrim( Ddo_sistema_projetonome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_PROJETONOME_Filteredtext_get", StringUtil.RTrim( Ddo_sistema_projetonome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_PROJETONOME_Selectedvalue_get", StringUtil.RTrim( Ddo_sistema_projetonome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Activeeventkey", StringUtil.RTrim( Ddo_sistemaversao_id_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Filteredtext_get", StringUtil.RTrim( Ddo_sistemaversao_id_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMAVERSAO_ID_Selectedvalue_get", StringUtil.RTrim( Ddo_sistemaversao_id_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_ATIVO_Activeeventkey", StringUtil.RTrim( Ddo_sistema_ativo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_ATIVO_Selectedvalue_get", StringUtil.RTrim( Ddo_sistema_ativo_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE3G2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT3G2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwsistema.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWSistema" ;
      }

      public override String GetPgmdesc( )
      {
         return " Sistema" ;
      }

      protected void WB3G0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_3G2( true) ;
         }
         else
         {
            wb_table1_2_3G2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_3G2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"INNEWWINDOW1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'" + sGXsfl_89_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV20DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(113, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,113);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsistema_sigla_Internalname, StringUtil.RTrim( AV88TFSistema_Sigla), StringUtil.RTrim( context.localUtil.Format( AV88TFSistema_Sigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,114);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsistema_sigla_Jsonclick, 0, "Attribute", "", "", "", edtavTfsistema_sigla_Visible, 1, 0, "text", "", 25, "chr", 1, "row", 25, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWSistema.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsistema_sigla_sel_Internalname, StringUtil.RTrim( AV89TFSistema_Sigla_Sel), StringUtil.RTrim( context.localUtil.Format( AV89TFSistema_Sigla_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,115);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsistema_sigla_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfsistema_sigla_sel_Visible, 1, 0, "text", "", 25, "chr", 1, "row", 25, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWSistema.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsistema_coordenacao_Internalname, AV100TFSistema_Coordenacao, StringUtil.RTrim( context.localUtil.Format( AV100TFSistema_Coordenacao, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,116);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsistema_coordenacao_Jsonclick, 0, "Attribute", "", "", "", edtavTfsistema_coordenacao_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWSistema.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsistema_coordenacao_sel_Internalname, AV101TFSistema_Coordenacao_Sel, StringUtil.RTrim( context.localUtil.Format( AV101TFSistema_Coordenacao_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,117);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsistema_coordenacao_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfsistema_coordenacao_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWSistema.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfambientetecnologico_descricao_Internalname, AV104TFAmbienteTecnologico_Descricao, StringUtil.RTrim( context.localUtil.Format( AV104TFAmbienteTecnologico_Descricao, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,118);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfambientetecnologico_descricao_Jsonclick, 0, "Attribute", "", "", "", edtavTfambientetecnologico_descricao_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWSistema.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfambientetecnologico_descricao_sel_Internalname, AV105TFAmbienteTecnologico_Descricao_Sel, StringUtil.RTrim( context.localUtil.Format( AV105TFAmbienteTecnologico_Descricao_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,119);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfambientetecnologico_descricao_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfambientetecnologico_descricao_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWSistema.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 120,'',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfmetodologia_descricao_Internalname, AV108TFMetodologia_Descricao, StringUtil.RTrim( context.localUtil.Format( AV108TFMetodologia_Descricao, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,120);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfmetodologia_descricao_Jsonclick, 0, "Attribute", "", "", "", edtavTfmetodologia_descricao_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWSistema.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 121,'',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfmetodologia_descricao_sel_Internalname, AV109TFMetodologia_Descricao_Sel, StringUtil.RTrim( context.localUtil.Format( AV109TFMetodologia_Descricao_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,121);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfmetodologia_descricao_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfmetodologia_descricao_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWSistema.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 122,'',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsistema_projetonome_Internalname, StringUtil.RTrim( AV120TFSistema_ProjetoNome), StringUtil.RTrim( context.localUtil.Format( AV120TFSistema_ProjetoNome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,122);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsistema_projetonome_Jsonclick, 0, "Attribute", "", "", "", edtavTfsistema_projetonome_Visible, 1, 0, "text", "", 100, "%", 1, "row", 50, 0, 0, 0, 1, -1, 0, true, "", "left", true, "HLP_WWSistema.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 123,'',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsistema_projetonome_sel_Internalname, StringUtil.RTrim( AV121TFSistema_ProjetoNome_Sel), StringUtil.RTrim( context.localUtil.Format( AV121TFSistema_ProjetoNome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,123);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsistema_projetonome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfsistema_projetonome_sel_Visible, 1, 0, "text", "", 100, "%", 1, "row", 50, 0, 0, 0, 1, -1, 0, true, "", "left", true, "HLP_WWSistema.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 124,'',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsistemaversao_id_Internalname, StringUtil.RTrim( AV140TFSistemaVersao_Id), StringUtil.RTrim( context.localUtil.Format( AV140TFSistemaVersao_Id, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,124);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsistemaversao_id_Jsonclick, 0, "Attribute", "", "", "", edtavTfsistemaversao_id_Visible, 1, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWSistema.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 125,'',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsistemaversao_id_sel_Internalname, StringUtil.RTrim( AV141TFSistemaVersao_Id_Sel), StringUtil.RTrim( context.localUtil.Format( AV141TFSistemaVersao_Id_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,125);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsistemaversao_id_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfsistemaversao_id_sel_Visible, 1, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWSistema.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 126,'',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsistema_ativo_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV124TFSistema_Ativo_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV124TFSistema_Ativo_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,126);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsistema_ativo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfsistema_ativo_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWSistema.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SISTEMA_SIGLAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 128,'',false,'" + sGXsfl_89_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_sistema_siglatitlecontrolidtoreplace_Internalname, AV90ddo_Sistema_SiglaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,128);\"", 0, edtavDdo_sistema_siglatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWSistema.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SISTEMA_TECNICAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 130,'',false,'" + sGXsfl_89_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_sistema_tecnicatitlecontrolidtoreplace_Internalname, AV98ddo_Sistema_TecnicaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,130);\"", 0, edtavDdo_sistema_tecnicatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWSistema.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SISTEMA_COORDENACAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 132,'',false,'" + sGXsfl_89_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_sistema_coordenacaotitlecontrolidtoreplace_Internalname, AV102ddo_Sistema_CoordenacaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,132);\"", 0, edtavDdo_sistema_coordenacaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWSistema.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_AMBIENTETECNOLOGICO_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 134,'',false,'" + sGXsfl_89_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Internalname, AV106ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,134);\"", 0, edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWSistema.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_METODOLOGIA_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 136,'',false,'" + sGXsfl_89_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_metodologia_descricaotitlecontrolidtoreplace_Internalname, AV110ddo_Metodologia_DescricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,136);\"", 0, edtavDdo_metodologia_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWSistema.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SISTEMA_PROJETONOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 138,'',false,'" + sGXsfl_89_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_sistema_projetonometitlecontrolidtoreplace_Internalname, AV122ddo_Sistema_ProjetoNomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,138);\"", 0, edtavDdo_sistema_projetonometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWSistema.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SISTEMAVERSAO_IDContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 140,'',false,'" + sGXsfl_89_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_sistemaversao_idtitlecontrolidtoreplace_Internalname, AV142ddo_SistemaVersao_IdTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,140);\"", 0, edtavDdo_sistemaversao_idtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWSistema.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SISTEMA_ATIVOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 142,'',false,'" + sGXsfl_89_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_sistema_ativotitlecontrolidtoreplace_Internalname, AV125ddo_Sistema_AtivoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,142);\"", 0, edtavDdo_sistema_ativotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWSistema.htm");
         }
         wbLoad = true;
      }

      protected void START3G2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Sistema", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP3G0( ) ;
      }

      protected void WS3G2( )
      {
         START3G2( ) ;
         EVT3G2( ) ;
      }

      protected void EVT3G2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E113G2 */
                              E113G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SISTEMA_SIGLA.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E123G2 */
                              E123G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SISTEMA_TECNICA.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E133G2 */
                              E133G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SISTEMA_COORDENACAO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E143G2 */
                              E143G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_AMBIENTETECNOLOGICO_DESCRICAO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E153G2 */
                              E153G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_METODOLOGIA_DESCRICAO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E163G2 */
                              E163G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SISTEMA_PROJETONOME.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E173G2 */
                              E173G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SISTEMAVERSAO_ID.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E183G2 */
                              E183G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SISTEMA_ATIVO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E193G2 */
                              E193G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E203G2 */
                              E203G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E213G2 */
                              E213G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E223G2 */
                              E223G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E233G2 */
                              E233G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E243G2 */
                              E243G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOEXPORTREPORT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E253G2 */
                              E253G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E263G2 */
                              E263G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E273G2 */
                              E273G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E283G2 */
                              E283G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ONMESSAGE_GX1") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E293G2 */
                              E293G2 ();
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 13), "ONMESSAGE_GX1") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_89_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_89_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_89_idx), 4, 0)), 4, "0");
                              SubsflControlProps_892( ) ;
                              AV34Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV34Update)) ? AV179Update_GXI : context.convertURL( context.PathToRelativeUrl( AV34Update))));
                              AV35Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV35Delete)) ? AV180Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV35Delete))));
                              AV58Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV58Display)) ? AV181Display_GXI : context.convertURL( context.PathToRelativeUrl( AV58Display))));
                              A127Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( edtSistema_Codigo_Internalname), ",", "."));
                              A416Sistema_Nome = StringUtil.Upper( cgiGet( edtSistema_Nome_Internalname));
                              A129Sistema_Sigla = StringUtil.Upper( cgiGet( edtSistema_Sigla_Internalname));
                              AV130Sistema_TipoSigla = StringUtil.Upper( cgiGet( edtavSistema_tiposigla_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSistema_tiposigla_Internalname, AV130Sistema_TipoSigla);
                              cmbSistema_Tecnica.Name = cmbSistema_Tecnica_Internalname;
                              cmbSistema_Tecnica.CurrentValue = cgiGet( cmbSistema_Tecnica_Internalname);
                              A700Sistema_Tecnica = cgiGet( cmbSistema_Tecnica_Internalname);
                              n700Sistema_Tecnica = false;
                              A513Sistema_Coordenacao = StringUtil.Upper( cgiGet( edtSistema_Coordenacao_Internalname));
                              n513Sistema_Coordenacao = false;
                              A352AmbienteTecnologico_Descricao = StringUtil.Upper( cgiGet( edtAmbienteTecnologico_Descricao_Internalname));
                              A137Metodologia_Codigo = (int)(context.localUtil.CToN( cgiGet( edtMetodologia_Codigo_Internalname), ",", "."));
                              n137Metodologia_Codigo = false;
                              A138Metodologia_Descricao = StringUtil.Upper( cgiGet( edtMetodologia_Descricao_Internalname));
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavPfb_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavPfb_Internalname), ",", ".") > 99999999.99999m ) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vPFB");
                                 GX_FocusControl = edtavPfb_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV131PFB = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfb_Internalname, StringUtil.LTrim( StringUtil.Str( AV131PFB, 14, 5)));
                              }
                              else
                              {
                                 AV131PFB = context.localUtil.CToN( cgiGet( edtavPfb_Internalname), ",", ".");
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfb_Internalname, StringUtil.LTrim( StringUtil.Str( AV131PFB, 14, 5)));
                              }
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavPfa_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavPfa_Internalname), ",", ".") > 99999999.99999m ) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vPFA");
                                 GX_FocusControl = edtavPfa_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV133PFA = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfa_Internalname, StringUtil.LTrim( StringUtil.Str( AV133PFA, 14, 5)));
                              }
                              else
                              {
                                 AV133PFA = context.localUtil.CToN( cgiGet( edtavPfa_Internalname), ",", ".");
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfa_Internalname, StringUtil.LTrim( StringUtil.Str( AV133PFA, 14, 5)));
                              }
                              A703Sistema_ProjetoNome = StringUtil.Upper( cgiGet( edtSistema_ProjetoNome_Internalname));
                              n703Sistema_ProjetoNome = false;
                              A1860SistemaVersao_Id = cgiGet( edtSistemaVersao_Id_Internalname);
                              A130Sistema_Ativo = StringUtil.StrToBool( cgiGet( chkSistema_Ativo_Internalname));
                              AV84AssociarClientes = cgiGet( edtavAssociarclientes_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAssociarclientes_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV84AssociarClientes)) ? AV182Associarclientes_GXI : context.convertURL( context.PathToRelativeUrl( AV84AssociarClientes))));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E303G2 */
                                    E303G2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E313G2 */
                                    E313G2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E323G2 */
                                    E323G2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ONMESSAGE_GX1") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E293G2 */
                                    E293G2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Sistema_nome1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vSISTEMA_NOME1"), AV67Sistema_Nome1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Sistema_sigla1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vSISTEMA_SIGLA1"), AV62Sistema_Sigla1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Sistema_tipo1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vSISTEMA_TIPO1"), AV73Sistema_Tipo1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Sistema_tecnica1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vSISTEMA_TECNICA1"), AV75Sistema_Tecnica1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Sistema_coordenacao1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vSISTEMA_COORDENACAO1"), AV76Sistema_Coordenacao1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Sistema_projetonome1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vSISTEMA_PROJETONOME1"), AV80Sistema_ProjetoNome1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Sistema_nome2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vSISTEMA_NOME2"), AV68Sistema_Nome2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Sistema_sigla2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vSISTEMA_SIGLA2"), AV63Sistema_Sigla2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Sistema_tipo2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vSISTEMA_TIPO2"), AV74Sistema_Tipo2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Sistema_tecnica2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vSISTEMA_TECNICA2"), AV77Sistema_Tecnica2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Sistema_coordenacao2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vSISTEMA_COORDENACAO2"), AV78Sistema_Coordenacao2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Sistema_projetonome2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vSISTEMA_PROJETONOME2"), AV81Sistema_ProjetoNome2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfsistema_sigla Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSISTEMA_SIGLA"), AV88TFSistema_Sigla) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfsistema_sigla_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSISTEMA_SIGLA_SEL"), AV89TFSistema_Sigla_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfsistema_coordenacao Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSISTEMA_COORDENACAO"), AV100TFSistema_Coordenacao) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfsistema_coordenacao_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSISTEMA_COORDENACAO_SEL"), AV101TFSistema_Coordenacao_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfambientetecnologico_descricao Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFAMBIENTETECNOLOGICO_DESCRICAO"), AV104TFAmbienteTecnologico_Descricao) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfambientetecnologico_descricao_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL"), AV105TFAmbienteTecnologico_Descricao_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfmetodologia_descricao Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMETODOLOGIA_DESCRICAO"), AV108TFMetodologia_Descricao) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfmetodologia_descricao_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMETODOLOGIA_DESCRICAO_SEL"), AV109TFMetodologia_Descricao_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfsistema_projetonome Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSISTEMA_PROJETONOME"), AV120TFSistema_ProjetoNome) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfsistema_projetonome_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSISTEMA_PROJETONOME_SEL"), AV121TFSistema_ProjetoNome_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfsistemaversao_id Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSISTEMAVERSAO_ID"), AV140TFSistemaVersao_Id) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfsistemaversao_id_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSISTEMAVERSAO_ID_SEL"), AV141TFSistemaVersao_Id_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfsistema_ativo_sel Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSISTEMA_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV124TFSistema_Ativo_Sel )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ONMESSAGE_GX1") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E293G2 */
                                    E293G2 ();
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE3G2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA3G2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("SISTEMA_NOME", "Nome", 0);
            cmbavDynamicfiltersselector1.addItem("SISTEMA_SIGLA", "Sigla", 0);
            cmbavDynamicfiltersselector1.addItem("SISTEMA_TIPO", "Tipo", 0);
            cmbavDynamicfiltersselector1.addItem("SISTEMA_TECNICA", "T�cnica", 0);
            cmbavDynamicfiltersselector1.addItem("SISTEMA_COORDENACAO", "Coordena��o", 0);
            cmbavDynamicfiltersselector1.addItem("SISTEMA_PROJETONOME", "Projeto", 0);
            cmbavDynamicfiltersselector1.addItem("SISTEMA_PF", "PF.B", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "<", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "=", 0);
            cmbavDynamicfiltersoperator1.addItem("2", ">", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            cmbavSistema_tipo1.Name = "vSISTEMA_TIPO1";
            cmbavSistema_tipo1.WebTags = "";
            cmbavSistema_tipo1.addItem("", "Todos", 0);
            cmbavSistema_tipo1.addItem("D", "Desenvolvimento", 0);
            cmbavSistema_tipo1.addItem("M", "Melhoria", 0);
            cmbavSistema_tipo1.addItem("A", "Aplica��o", 0);
            if ( cmbavSistema_tipo1.ItemCount > 0 )
            {
               AV73Sistema_Tipo1 = cmbavSistema_tipo1.getValidValue(AV73Sistema_Tipo1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73Sistema_Tipo1", AV73Sistema_Tipo1);
            }
            cmbavSistema_tecnica1.Name = "vSISTEMA_TECNICA1";
            cmbavSistema_tecnica1.WebTags = "";
            cmbavSistema_tecnica1.addItem("", "Todos", 0);
            cmbavSistema_tecnica1.addItem("I", "Indicativa", 0);
            cmbavSistema_tecnica1.addItem("E", "Estimada", 0);
            cmbavSistema_tecnica1.addItem("D", "Detalhada", 0);
            if ( cmbavSistema_tecnica1.ItemCount > 0 )
            {
               AV75Sistema_Tecnica1 = cmbavSistema_tecnica1.getValidValue(AV75Sistema_Tecnica1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75Sistema_Tecnica1", AV75Sistema_Tecnica1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("SISTEMA_NOME", "Nome", 0);
            cmbavDynamicfiltersselector2.addItem("SISTEMA_SIGLA", "Sigla", 0);
            cmbavDynamicfiltersselector2.addItem("SISTEMA_TIPO", "Tipo", 0);
            cmbavDynamicfiltersselector2.addItem("SISTEMA_TECNICA", "T�cnica", 0);
            cmbavDynamicfiltersselector2.addItem("SISTEMA_COORDENACAO", "Coordena��o", 0);
            cmbavDynamicfiltersselector2.addItem("SISTEMA_PROJETONOME", "Projeto", 0);
            cmbavDynamicfiltersselector2.addItem("SISTEMA_PF", "PF.B", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "<", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "=", 0);
            cmbavDynamicfiltersoperator2.addItem("2", ">", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
            }
            cmbavSistema_tipo2.Name = "vSISTEMA_TIPO2";
            cmbavSistema_tipo2.WebTags = "";
            cmbavSistema_tipo2.addItem("", "Todos", 0);
            cmbavSistema_tipo2.addItem("D", "Desenvolvimento", 0);
            cmbavSistema_tipo2.addItem("M", "Melhoria", 0);
            cmbavSistema_tipo2.addItem("A", "Aplica��o", 0);
            if ( cmbavSistema_tipo2.ItemCount > 0 )
            {
               AV74Sistema_Tipo2 = cmbavSistema_tipo2.getValidValue(AV74Sistema_Tipo2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74Sistema_Tipo2", AV74Sistema_Tipo2);
            }
            cmbavSistema_tecnica2.Name = "vSISTEMA_TECNICA2";
            cmbavSistema_tecnica2.WebTags = "";
            cmbavSistema_tecnica2.addItem("", "Todos", 0);
            cmbavSistema_tecnica2.addItem("I", "Indicativa", 0);
            cmbavSistema_tecnica2.addItem("E", "Estimada", 0);
            cmbavSistema_tecnica2.addItem("D", "Detalhada", 0);
            if ( cmbavSistema_tecnica2.ItemCount > 0 )
            {
               AV77Sistema_Tecnica2 = cmbavSistema_tecnica2.getValidValue(AV77Sistema_Tecnica2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77Sistema_Tecnica2", AV77Sistema_Tecnica2);
            }
            GXCCtl = "SISTEMA_TECNICA_" + sGXsfl_89_idx;
            cmbSistema_Tecnica.Name = GXCCtl;
            cmbSistema_Tecnica.WebTags = "";
            cmbSistema_Tecnica.addItem("", "(Nenhum)", 0);
            cmbSistema_Tecnica.addItem("I", "Indicativa", 0);
            cmbSistema_Tecnica.addItem("E", "Estimada", 0);
            cmbSistema_Tecnica.addItem("D", "Detalhada", 0);
            if ( cmbSistema_Tecnica.ItemCount > 0 )
            {
               A700Sistema_Tecnica = cmbSistema_Tecnica.getValidValue(A700Sistema_Tecnica);
               n700Sistema_Tecnica = false;
            }
            GXCCtl = "SISTEMA_ATIVO_" + sGXsfl_89_idx;
            chkSistema_Ativo.Name = GXCCtl;
            chkSistema_Ativo.WebTags = "";
            chkSistema_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkSistema_Ativo_Internalname, "TitleCaption", chkSistema_Ativo.Caption);
            chkSistema_Ativo.CheckedValue = "false";
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_892( ) ;
         while ( nGXsfl_89_idx <= nRC_GXsfl_89 )
         {
            sendrow_892( ) ;
            nGXsfl_89_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_89_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_89_idx+1));
            sGXsfl_89_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_89_idx), 4, 0)), 4, "0");
            SubsflControlProps_892( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       String AV67Sistema_Nome1 ,
                                       String AV62Sistema_Sigla1 ,
                                       String AV73Sistema_Tipo1 ,
                                       String AV75Sistema_Tecnica1 ,
                                       String AV76Sistema_Coordenacao1 ,
                                       String AV80Sistema_ProjetoNome1 ,
                                       String AV21DynamicFiltersSelector2 ,
                                       String AV68Sistema_Nome2 ,
                                       String AV63Sistema_Sigla2 ,
                                       String AV74Sistema_Tipo2 ,
                                       String AV77Sistema_Tecnica2 ,
                                       String AV78Sistema_Coordenacao2 ,
                                       String AV81Sistema_ProjetoNome2 ,
                                       bool AV20DynamicFiltersEnabled2 ,
                                       String AV88TFSistema_Sigla ,
                                       String AV89TFSistema_Sigla_Sel ,
                                       String AV100TFSistema_Coordenacao ,
                                       String AV101TFSistema_Coordenacao_Sel ,
                                       String AV104TFAmbienteTecnologico_Descricao ,
                                       String AV105TFAmbienteTecnologico_Descricao_Sel ,
                                       String AV108TFMetodologia_Descricao ,
                                       String AV109TFMetodologia_Descricao_Sel ,
                                       String AV120TFSistema_ProjetoNome ,
                                       String AV121TFSistema_ProjetoNome_Sel ,
                                       String AV140TFSistemaVersao_Id ,
                                       String AV141TFSistemaVersao_Id_Sel ,
                                       short AV124TFSistema_Ativo_Sel ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       String AV90ddo_Sistema_SiglaTitleControlIdToReplace ,
                                       String AV98ddo_Sistema_TecnicaTitleControlIdToReplace ,
                                       String AV102ddo_Sistema_CoordenacaoTitleControlIdToReplace ,
                                       String AV106ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace ,
                                       String AV110ddo_Metodologia_DescricaoTitleControlIdToReplace ,
                                       String AV122ddo_Sistema_ProjetoNomeTitleControlIdToReplace ,
                                       String AV142ddo_SistemaVersao_IdTitleControlIdToReplace ,
                                       String AV125ddo_Sistema_AtivoTitleControlIdToReplace ,
                                       bool AV137Paginando ,
                                       long AV134Registros ,
                                       short AV135PreView ,
                                       long AV136PaginaAtual ,
                                       int AV60Sistema_AreaTrabalhoCod ,
                                       short AV16DynamicFiltersOperator1 ,
                                       decimal AV82Sistema_PF1 ,
                                       short AV22DynamicFiltersOperator2 ,
                                       decimal AV83Sistema_PF2 ,
                                       IGxCollection AV97TFSistema_Tecnica_Sels ,
                                       String AV183Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV33DynamicFiltersIgnoreFirst ,
                                       bool AV32DynamicFiltersRemoving ,
                                       int A127Sistema_Codigo ,
                                       int A351AmbienteTecnologico_Codigo ,
                                       int A137Metodologia_Codigo ,
                                       int A691Sistema_ProjetoCod ,
                                       int A1859SistemaVersao_Codigo ,
                                       bool A130Sistema_Ativo ,
                                       String A707Sistema_TipoSigla ,
                                       decimal A395Sistema_PF ,
                                       decimal A690Sistema_PFA )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF3G2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_SISTEMA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "SISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_SISTEMA_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A416Sistema_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "SISTEMA_NOME", A416Sistema_Nome);
         GxWebStd.gx_hidden_field( context, "gxhash_SISTEMA_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A129Sistema_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, "SISTEMA_SIGLA", StringUtil.RTrim( A129Sistema_Sigla));
         GxWebStd.gx_hidden_field( context, "gxhash_SISTEMA_TECNICA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A700Sistema_Tecnica, ""))));
         GxWebStd.gx_hidden_field( context, "SISTEMA_TECNICA", StringUtil.RTrim( A700Sistema_Tecnica));
         GxWebStd.gx_hidden_field( context, "gxhash_SISTEMA_COORDENACAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A513Sistema_Coordenacao, "@!"))));
         GxWebStd.gx_hidden_field( context, "SISTEMA_COORDENACAO", A513Sistema_Coordenacao);
         GxWebStd.gx_hidden_field( context, "gxhash_METODOLOGIA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A137Metodologia_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "METODOLOGIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A137Metodologia_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_SISTEMA_ATIVO", GetSecureSignedToken( "", A130Sistema_Ativo));
         GxWebStd.gx_hidden_field( context, "SISTEMA_ATIVO", StringUtil.BoolToStr( A130Sistema_Ativo));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavSistema_tipo1.ItemCount > 0 )
         {
            AV73Sistema_Tipo1 = cmbavSistema_tipo1.getValidValue(AV73Sistema_Tipo1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73Sistema_Tipo1", AV73Sistema_Tipo1);
         }
         if ( cmbavSistema_tecnica1.ItemCount > 0 )
         {
            AV75Sistema_Tecnica1 = cmbavSistema_tecnica1.getValidValue(AV75Sistema_Tecnica1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75Sistema_Tecnica1", AV75Sistema_Tecnica1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavSistema_tipo2.ItemCount > 0 )
         {
            AV74Sistema_Tipo2 = cmbavSistema_tipo2.getValidValue(AV74Sistema_Tipo2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74Sistema_Tipo2", AV74Sistema_Tipo2);
         }
         if ( cmbavSistema_tecnica2.ItemCount > 0 )
         {
            AV77Sistema_Tecnica2 = cmbavSistema_tecnica2.getValidValue(AV77Sistema_Tecnica2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77Sistema_Tecnica2", AV77Sistema_Tecnica2);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF3G2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV183Pgmname = "WWSistema";
         context.Gx_err = 0;
         edtavSistema_tiposigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistema_tiposigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_tiposigla_Enabled), 5, 0)));
         edtavPfb_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfb_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfb_Enabled), 5, 0)));
         edtavPfa_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfa_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfa_Enabled), 5, 0)));
      }

      protected void RF3G2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 89;
         /* Execute user event: E313G2 */
         E313G2 ();
         nGXsfl_89_idx = 1;
         sGXsfl_89_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_89_idx), 4, 0)), 4, "0");
         SubsflControlProps_892( ) ;
         nGXsfl_89_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_892( ) ;
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 A700Sistema_Tecnica ,
                                                 AV167WWSistemaDS_23_Tfsistema_tecnica_sels ,
                                                 AV146WWSistemaDS_2_Dynamicfiltersselector1 ,
                                                 AV148WWSistemaDS_4_Sistema_nome1 ,
                                                 AV149WWSistemaDS_5_Sistema_sigla1 ,
                                                 AV150WWSistemaDS_6_Sistema_tipo1 ,
                                                 AV151WWSistemaDS_7_Sistema_tecnica1 ,
                                                 AV152WWSistemaDS_8_Sistema_coordenacao1 ,
                                                 AV153WWSistemaDS_9_Sistema_projetonome1 ,
                                                 AV155WWSistemaDS_11_Dynamicfiltersenabled2 ,
                                                 AV156WWSistemaDS_12_Dynamicfiltersselector2 ,
                                                 AV158WWSistemaDS_14_Sistema_nome2 ,
                                                 AV159WWSistemaDS_15_Sistema_sigla2 ,
                                                 AV160WWSistemaDS_16_Sistema_tipo2 ,
                                                 AV161WWSistemaDS_17_Sistema_tecnica2 ,
                                                 AV162WWSistemaDS_18_Sistema_coordenacao2 ,
                                                 AV163WWSistemaDS_19_Sistema_projetonome2 ,
                                                 AV166WWSistemaDS_22_Tfsistema_sigla_sel ,
                                                 AV165WWSistemaDS_21_Tfsistema_sigla ,
                                                 AV167WWSistemaDS_23_Tfsistema_tecnica_sels.Count ,
                                                 AV169WWSistemaDS_25_Tfsistema_coordenacao_sel ,
                                                 AV168WWSistemaDS_24_Tfsistema_coordenacao ,
                                                 AV171WWSistemaDS_27_Tfambientetecnologico_descricao_sel ,
                                                 AV170WWSistemaDS_26_Tfambientetecnologico_descricao ,
                                                 AV173WWSistemaDS_29_Tfmetodologia_descricao_sel ,
                                                 AV172WWSistemaDS_28_Tfmetodologia_descricao ,
                                                 AV175WWSistemaDS_31_Tfsistema_projetonome_sel ,
                                                 AV174WWSistemaDS_30_Tfsistema_projetonome ,
                                                 AV177WWSistemaDS_33_Tfsistemaversao_id_sel ,
                                                 AV176WWSistemaDS_32_Tfsistemaversao_id ,
                                                 AV178WWSistemaDS_34_Tfsistema_ativo_sel ,
                                                 A416Sistema_Nome ,
                                                 A129Sistema_Sigla ,
                                                 A699Sistema_Tipo ,
                                                 A513Sistema_Coordenacao ,
                                                 A703Sistema_ProjetoNome ,
                                                 A352AmbienteTecnologico_Descricao ,
                                                 A138Metodologia_Descricao ,
                                                 A1860SistemaVersao_Id ,
                                                 A130Sistema_Ativo ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 AV147WWSistemaDS_3_Dynamicfiltersoperator1 ,
                                                 AV154WWSistemaDS_10_Sistema_pf1 ,
                                                 A395Sistema_PF ,
                                                 AV157WWSistemaDS_13_Dynamicfiltersoperator2 ,
                                                 AV164WWSistemaDS_20_Sistema_pf2 ,
                                                 A135Sistema_AreaTrabalhoCod ,
                                                 AV6WWPContext.gxTpr_Areatrabalho_codigo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.DECIMAL,
                                                 TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            lV148WWSistemaDS_4_Sistema_nome1 = StringUtil.Concat( StringUtil.RTrim( AV148WWSistemaDS_4_Sistema_nome1), "%", "");
            lV149WWSistemaDS_5_Sistema_sigla1 = StringUtil.PadR( StringUtil.RTrim( AV149WWSistemaDS_5_Sistema_sigla1), 25, "%");
            lV152WWSistemaDS_8_Sistema_coordenacao1 = StringUtil.Concat( StringUtil.RTrim( AV152WWSistemaDS_8_Sistema_coordenacao1), "%", "");
            lV153WWSistemaDS_9_Sistema_projetonome1 = StringUtil.PadR( StringUtil.RTrim( AV153WWSistemaDS_9_Sistema_projetonome1), 50, "%");
            lV158WWSistemaDS_14_Sistema_nome2 = StringUtil.Concat( StringUtil.RTrim( AV158WWSistemaDS_14_Sistema_nome2), "%", "");
            lV159WWSistemaDS_15_Sistema_sigla2 = StringUtil.PadR( StringUtil.RTrim( AV159WWSistemaDS_15_Sistema_sigla2), 25, "%");
            lV162WWSistemaDS_18_Sistema_coordenacao2 = StringUtil.Concat( StringUtil.RTrim( AV162WWSistemaDS_18_Sistema_coordenacao2), "%", "");
            lV163WWSistemaDS_19_Sistema_projetonome2 = StringUtil.PadR( StringUtil.RTrim( AV163WWSistemaDS_19_Sistema_projetonome2), 50, "%");
            lV165WWSistemaDS_21_Tfsistema_sigla = StringUtil.PadR( StringUtil.RTrim( AV165WWSistemaDS_21_Tfsistema_sigla), 25, "%");
            lV168WWSistemaDS_24_Tfsistema_coordenacao = StringUtil.Concat( StringUtil.RTrim( AV168WWSistemaDS_24_Tfsistema_coordenacao), "%", "");
            lV170WWSistemaDS_26_Tfambientetecnologico_descricao = StringUtil.Concat( StringUtil.RTrim( AV170WWSistemaDS_26_Tfambientetecnologico_descricao), "%", "");
            lV172WWSistemaDS_28_Tfmetodologia_descricao = StringUtil.Concat( StringUtil.RTrim( AV172WWSistemaDS_28_Tfmetodologia_descricao), "%", "");
            lV174WWSistemaDS_30_Tfsistema_projetonome = StringUtil.PadR( StringUtil.RTrim( AV174WWSistemaDS_30_Tfsistema_projetonome), 50, "%");
            lV176WWSistemaDS_32_Tfsistemaversao_id = StringUtil.PadR( StringUtil.RTrim( AV176WWSistemaDS_32_Tfsistemaversao_id), 20, "%");
            /* Using cursor H003G2 */
            pr_default.execute(0, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, lV148WWSistemaDS_4_Sistema_nome1, lV149WWSistemaDS_5_Sistema_sigla1, AV150WWSistemaDS_6_Sistema_tipo1, AV151WWSistemaDS_7_Sistema_tecnica1, lV152WWSistemaDS_8_Sistema_coordenacao1, lV153WWSistemaDS_9_Sistema_projetonome1, lV158WWSistemaDS_14_Sistema_nome2, lV159WWSistemaDS_15_Sistema_sigla2, AV160WWSistemaDS_16_Sistema_tipo2, AV161WWSistemaDS_17_Sistema_tecnica2, lV162WWSistemaDS_18_Sistema_coordenacao2, lV163WWSistemaDS_19_Sistema_projetonome2, lV165WWSistemaDS_21_Tfsistema_sigla, AV166WWSistemaDS_22_Tfsistema_sigla_sel, lV168WWSistemaDS_24_Tfsistema_coordenacao, AV169WWSistemaDS_25_Tfsistema_coordenacao_sel, lV170WWSistemaDS_26_Tfambientetecnologico_descricao, AV171WWSistemaDS_27_Tfambientetecnologico_descricao_sel, lV172WWSistemaDS_28_Tfmetodologia_descricao, AV173WWSistemaDS_29_Tfmetodologia_descricao_sel, lV174WWSistemaDS_30_Tfsistema_projetonome, AV175WWSistemaDS_31_Tfsistema_projetonome_sel, lV176WWSistemaDS_32_Tfsistemaversao_id, AV177WWSistemaDS_33_Tfsistemaversao_id_sel});
            nGXsfl_89_idx = 1;
            GRID_nEOF = 0;
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) ) )
            {
               A135Sistema_AreaTrabalhoCod = H003G2_A135Sistema_AreaTrabalhoCod[0];
               A351AmbienteTecnologico_Codigo = H003G2_A351AmbienteTecnologico_Codigo[0];
               n351AmbienteTecnologico_Codigo = H003G2_n351AmbienteTecnologico_Codigo[0];
               A691Sistema_ProjetoCod = H003G2_A691Sistema_ProjetoCod[0];
               n691Sistema_ProjetoCod = H003G2_n691Sistema_ProjetoCod[0];
               A1859SistemaVersao_Codigo = H003G2_A1859SistemaVersao_Codigo[0];
               n1859SistemaVersao_Codigo = H003G2_n1859SistemaVersao_Codigo[0];
               A130Sistema_Ativo = H003G2_A130Sistema_Ativo[0];
               A1860SistemaVersao_Id = H003G2_A1860SistemaVersao_Id[0];
               A703Sistema_ProjetoNome = H003G2_A703Sistema_ProjetoNome[0];
               n703Sistema_ProjetoNome = H003G2_n703Sistema_ProjetoNome[0];
               A138Metodologia_Descricao = H003G2_A138Metodologia_Descricao[0];
               A137Metodologia_Codigo = H003G2_A137Metodologia_Codigo[0];
               n137Metodologia_Codigo = H003G2_n137Metodologia_Codigo[0];
               A352AmbienteTecnologico_Descricao = H003G2_A352AmbienteTecnologico_Descricao[0];
               A513Sistema_Coordenacao = H003G2_A513Sistema_Coordenacao[0];
               n513Sistema_Coordenacao = H003G2_n513Sistema_Coordenacao[0];
               A700Sistema_Tecnica = H003G2_A700Sistema_Tecnica[0];
               n700Sistema_Tecnica = H003G2_n700Sistema_Tecnica[0];
               A129Sistema_Sigla = H003G2_A129Sistema_Sigla[0];
               A416Sistema_Nome = H003G2_A416Sistema_Nome[0];
               A686Sistema_FatorAjuste = H003G2_A686Sistema_FatorAjuste[0];
               n686Sistema_FatorAjuste = H003G2_n686Sistema_FatorAjuste[0];
               A127Sistema_Codigo = H003G2_A127Sistema_Codigo[0];
               A699Sistema_Tipo = H003G2_A699Sistema_Tipo[0];
               n699Sistema_Tipo = H003G2_n699Sistema_Tipo[0];
               A352AmbienteTecnologico_Descricao = H003G2_A352AmbienteTecnologico_Descricao[0];
               A703Sistema_ProjetoNome = H003G2_A703Sistema_ProjetoNome[0];
               n703Sistema_ProjetoNome = H003G2_n703Sistema_ProjetoNome[0];
               A1860SistemaVersao_Id = H003G2_A1860SistemaVersao_Id[0];
               A138Metodologia_Descricao = H003G2_A138Metodologia_Descricao[0];
               if ( StringUtil.StrCmp(A699Sistema_Tipo, "D") == 0 )
               {
                  A707Sistema_TipoSigla = "PD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A707Sistema_TipoSigla", A707Sistema_TipoSigla);
               }
               else
               {
                  if ( StringUtil.StrCmp(A699Sistema_Tipo, "M") == 0 )
                  {
                     A707Sistema_TipoSigla = "PM";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A707Sistema_TipoSigla", A707Sistema_TipoSigla);
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(A699Sistema_Tipo, "C") == 0 )
                     {
                        A707Sistema_TipoSigla = "PC";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A707Sistema_TipoSigla", A707Sistema_TipoSigla);
                     }
                     else
                     {
                        if ( StringUtil.StrCmp(A699Sistema_Tipo, "A") == 0 )
                        {
                           A707Sistema_TipoSigla = "A";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A707Sistema_TipoSigla", A707Sistema_TipoSigla);
                        }
                        else
                        {
                           A707Sistema_TipoSigla = "";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A707Sistema_TipoSigla", A707Sistema_TipoSigla);
                        }
                     }
                  }
               }
               GXt_decimal1 = A395Sistema_PF;
               new prc_sistema_pf(context ).execute( ref  A127Sistema_Codigo, ref  GXt_decimal1) ;
               A395Sistema_PF = GXt_decimal1;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A395Sistema_PF", StringUtil.LTrim( StringUtil.Str( A395Sistema_PF, 14, 5)));
               if ( ! ( ( StringUtil.StrCmp(AV146WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_PF") == 0 ) && ( AV147WWSistemaDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! (Convert.ToDecimal(0)==AV154WWSistemaDS_10_Sistema_pf1) ) ) || ( ( A395Sistema_PF < AV154WWSistemaDS_10_Sistema_pf1 ) ) )
               {
                  if ( ! ( ( StringUtil.StrCmp(AV146WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_PF") == 0 ) && ( AV147WWSistemaDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! (Convert.ToDecimal(0)==AV154WWSistemaDS_10_Sistema_pf1) ) ) || ( ( A395Sistema_PF == AV154WWSistemaDS_10_Sistema_pf1 ) ) )
                  {
                     if ( ! ( ( StringUtil.StrCmp(AV146WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_PF") == 0 ) && ( AV147WWSistemaDS_3_Dynamicfiltersoperator1 == 2 ) && ( ! (Convert.ToDecimal(0)==AV154WWSistemaDS_10_Sistema_pf1) ) ) || ( ( A395Sistema_PF > AV154WWSistemaDS_10_Sistema_pf1 ) ) )
                     {
                        if ( ! ( AV155WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV156WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_PF") == 0 ) && ( AV157WWSistemaDS_13_Dynamicfiltersoperator2 == 0 ) && ( ! (Convert.ToDecimal(0)==AV164WWSistemaDS_20_Sistema_pf2) ) ) || ( ( A395Sistema_PF < AV164WWSistemaDS_20_Sistema_pf2 ) ) )
                        {
                           if ( ! ( AV155WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV156WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_PF") == 0 ) && ( AV157WWSistemaDS_13_Dynamicfiltersoperator2 == 1 ) && ( ! (Convert.ToDecimal(0)==AV164WWSistemaDS_20_Sistema_pf2) ) ) || ( ( A395Sistema_PF == AV164WWSistemaDS_20_Sistema_pf2 ) ) )
                           {
                              if ( ! ( AV155WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV156WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_PF") == 0 ) && ( AV157WWSistemaDS_13_Dynamicfiltersoperator2 == 2 ) && ( ! (Convert.ToDecimal(0)==AV164WWSistemaDS_20_Sistema_pf2) ) ) || ( ( A395Sistema_PF > AV164WWSistemaDS_20_Sistema_pf2 ) ) )
                              {
                                 A690Sistema_PFA = (decimal)(A395Sistema_PF*A686Sistema_FatorAjuste);
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A690Sistema_PFA", StringUtil.LTrim( StringUtil.Str( A690Sistema_PFA, 14, 5)));
                                 /* Execute user event: E323G2 */
                                 E323G2 ();
                              }
                           }
                        }
                     }
                  }
               }
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 89;
            WB3G0( ) ;
         }
         nGXsfl_89_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV145WWSistemaDS_1_Sistema_areatrabalhocod = AV60Sistema_AreaTrabalhoCod;
         AV146WWSistemaDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV147WWSistemaDS_3_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV148WWSistemaDS_4_Sistema_nome1 = AV67Sistema_Nome1;
         AV149WWSistemaDS_5_Sistema_sigla1 = AV62Sistema_Sigla1;
         AV150WWSistemaDS_6_Sistema_tipo1 = AV73Sistema_Tipo1;
         AV151WWSistemaDS_7_Sistema_tecnica1 = AV75Sistema_Tecnica1;
         AV152WWSistemaDS_8_Sistema_coordenacao1 = AV76Sistema_Coordenacao1;
         AV153WWSistemaDS_9_Sistema_projetonome1 = AV80Sistema_ProjetoNome1;
         AV154WWSistemaDS_10_Sistema_pf1 = AV82Sistema_PF1;
         AV155WWSistemaDS_11_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV156WWSistemaDS_12_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV157WWSistemaDS_13_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV158WWSistemaDS_14_Sistema_nome2 = AV68Sistema_Nome2;
         AV159WWSistemaDS_15_Sistema_sigla2 = AV63Sistema_Sigla2;
         AV160WWSistemaDS_16_Sistema_tipo2 = AV74Sistema_Tipo2;
         AV161WWSistemaDS_17_Sistema_tecnica2 = AV77Sistema_Tecnica2;
         AV162WWSistemaDS_18_Sistema_coordenacao2 = AV78Sistema_Coordenacao2;
         AV163WWSistemaDS_19_Sistema_projetonome2 = AV81Sistema_ProjetoNome2;
         AV164WWSistemaDS_20_Sistema_pf2 = AV83Sistema_PF2;
         AV165WWSistemaDS_21_Tfsistema_sigla = AV88TFSistema_Sigla;
         AV166WWSistemaDS_22_Tfsistema_sigla_sel = AV89TFSistema_Sigla_Sel;
         AV167WWSistemaDS_23_Tfsistema_tecnica_sels = AV97TFSistema_Tecnica_Sels;
         AV168WWSistemaDS_24_Tfsistema_coordenacao = AV100TFSistema_Coordenacao;
         AV169WWSistemaDS_25_Tfsistema_coordenacao_sel = AV101TFSistema_Coordenacao_Sel;
         AV170WWSistemaDS_26_Tfambientetecnologico_descricao = AV104TFAmbienteTecnologico_Descricao;
         AV171WWSistemaDS_27_Tfambientetecnologico_descricao_sel = AV105TFAmbienteTecnologico_Descricao_Sel;
         AV172WWSistemaDS_28_Tfmetodologia_descricao = AV108TFMetodologia_Descricao;
         AV173WWSistemaDS_29_Tfmetodologia_descricao_sel = AV109TFMetodologia_Descricao_Sel;
         AV174WWSistemaDS_30_Tfsistema_projetonome = AV120TFSistema_ProjetoNome;
         AV175WWSistemaDS_31_Tfsistema_projetonome_sel = AV121TFSistema_ProjetoNome_Sel;
         AV176WWSistemaDS_32_Tfsistemaversao_id = AV140TFSistemaVersao_Id;
         AV177WWSistemaDS_33_Tfsistemaversao_id_sel = AV141TFSistemaVersao_Id_Sel;
         AV178WWSistemaDS_34_Tfsistema_ativo_sel = AV124TFSistema_Ativo_Sel;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV67Sistema_Nome1, AV62Sistema_Sigla1, AV73Sistema_Tipo1, AV75Sistema_Tecnica1, AV76Sistema_Coordenacao1, AV80Sistema_ProjetoNome1, AV21DynamicFiltersSelector2, AV68Sistema_Nome2, AV63Sistema_Sigla2, AV74Sistema_Tipo2, AV77Sistema_Tecnica2, AV78Sistema_Coordenacao2, AV81Sistema_ProjetoNome2, AV20DynamicFiltersEnabled2, AV88TFSistema_Sigla, AV89TFSistema_Sigla_Sel, AV100TFSistema_Coordenacao, AV101TFSistema_Coordenacao_Sel, AV104TFAmbienteTecnologico_Descricao, AV105TFAmbienteTecnologico_Descricao_Sel, AV108TFMetodologia_Descricao, AV109TFMetodologia_Descricao_Sel, AV120TFSistema_ProjetoNome, AV121TFSistema_ProjetoNome_Sel, AV140TFSistemaVersao_Id, AV141TFSistemaVersao_Id_Sel, AV124TFSistema_Ativo_Sel, AV6WWPContext, AV90ddo_Sistema_SiglaTitleControlIdToReplace, AV98ddo_Sistema_TecnicaTitleControlIdToReplace, AV102ddo_Sistema_CoordenacaoTitleControlIdToReplace, AV106ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, AV110ddo_Metodologia_DescricaoTitleControlIdToReplace, AV122ddo_Sistema_ProjetoNomeTitleControlIdToReplace, AV142ddo_SistemaVersao_IdTitleControlIdToReplace, AV125ddo_Sistema_AtivoTitleControlIdToReplace, AV137Paginando, AV134Registros, AV135PreView, AV136PaginaAtual, AV60Sistema_AreaTrabalhoCod, AV16DynamicFiltersOperator1, AV82Sistema_PF1, AV22DynamicFiltersOperator2, AV83Sistema_PF2, AV97TFSistema_Tecnica_Sels, AV183Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A127Sistema_Codigo, A351AmbienteTecnologico_Codigo, A137Metodologia_Codigo, A691Sistema_ProjetoCod, A1859SistemaVersao_Codigo, A130Sistema_Ativo, A707Sistema_TipoSigla, A395Sistema_PF, A690Sistema_PFA) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV145WWSistemaDS_1_Sistema_areatrabalhocod = AV60Sistema_AreaTrabalhoCod;
         AV146WWSistemaDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV147WWSistemaDS_3_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV148WWSistemaDS_4_Sistema_nome1 = AV67Sistema_Nome1;
         AV149WWSistemaDS_5_Sistema_sigla1 = AV62Sistema_Sigla1;
         AV150WWSistemaDS_6_Sistema_tipo1 = AV73Sistema_Tipo1;
         AV151WWSistemaDS_7_Sistema_tecnica1 = AV75Sistema_Tecnica1;
         AV152WWSistemaDS_8_Sistema_coordenacao1 = AV76Sistema_Coordenacao1;
         AV153WWSistemaDS_9_Sistema_projetonome1 = AV80Sistema_ProjetoNome1;
         AV154WWSistemaDS_10_Sistema_pf1 = AV82Sistema_PF1;
         AV155WWSistemaDS_11_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV156WWSistemaDS_12_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV157WWSistemaDS_13_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV158WWSistemaDS_14_Sistema_nome2 = AV68Sistema_Nome2;
         AV159WWSistemaDS_15_Sistema_sigla2 = AV63Sistema_Sigla2;
         AV160WWSistemaDS_16_Sistema_tipo2 = AV74Sistema_Tipo2;
         AV161WWSistemaDS_17_Sistema_tecnica2 = AV77Sistema_Tecnica2;
         AV162WWSistemaDS_18_Sistema_coordenacao2 = AV78Sistema_Coordenacao2;
         AV163WWSistemaDS_19_Sistema_projetonome2 = AV81Sistema_ProjetoNome2;
         AV164WWSistemaDS_20_Sistema_pf2 = AV83Sistema_PF2;
         AV165WWSistemaDS_21_Tfsistema_sigla = AV88TFSistema_Sigla;
         AV166WWSistemaDS_22_Tfsistema_sigla_sel = AV89TFSistema_Sigla_Sel;
         AV167WWSistemaDS_23_Tfsistema_tecnica_sels = AV97TFSistema_Tecnica_Sels;
         AV168WWSistemaDS_24_Tfsistema_coordenacao = AV100TFSistema_Coordenacao;
         AV169WWSistemaDS_25_Tfsistema_coordenacao_sel = AV101TFSistema_Coordenacao_Sel;
         AV170WWSistemaDS_26_Tfambientetecnologico_descricao = AV104TFAmbienteTecnologico_Descricao;
         AV171WWSistemaDS_27_Tfambientetecnologico_descricao_sel = AV105TFAmbienteTecnologico_Descricao_Sel;
         AV172WWSistemaDS_28_Tfmetodologia_descricao = AV108TFMetodologia_Descricao;
         AV173WWSistemaDS_29_Tfmetodologia_descricao_sel = AV109TFMetodologia_Descricao_Sel;
         AV174WWSistemaDS_30_Tfsistema_projetonome = AV120TFSistema_ProjetoNome;
         AV175WWSistemaDS_31_Tfsistema_projetonome_sel = AV121TFSistema_ProjetoNome_Sel;
         AV176WWSistemaDS_32_Tfsistemaversao_id = AV140TFSistemaVersao_Id;
         AV177WWSistemaDS_33_Tfsistemaversao_id_sel = AV141TFSistemaVersao_Id_Sel;
         AV178WWSistemaDS_34_Tfsistema_ativo_sel = AV124TFSistema_Ativo_Sel;
         if ( GRID_nEOF == 0 )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV67Sistema_Nome1, AV62Sistema_Sigla1, AV73Sistema_Tipo1, AV75Sistema_Tecnica1, AV76Sistema_Coordenacao1, AV80Sistema_ProjetoNome1, AV21DynamicFiltersSelector2, AV68Sistema_Nome2, AV63Sistema_Sigla2, AV74Sistema_Tipo2, AV77Sistema_Tecnica2, AV78Sistema_Coordenacao2, AV81Sistema_ProjetoNome2, AV20DynamicFiltersEnabled2, AV88TFSistema_Sigla, AV89TFSistema_Sigla_Sel, AV100TFSistema_Coordenacao, AV101TFSistema_Coordenacao_Sel, AV104TFAmbienteTecnologico_Descricao, AV105TFAmbienteTecnologico_Descricao_Sel, AV108TFMetodologia_Descricao, AV109TFMetodologia_Descricao_Sel, AV120TFSistema_ProjetoNome, AV121TFSistema_ProjetoNome_Sel, AV140TFSistemaVersao_Id, AV141TFSistemaVersao_Id_Sel, AV124TFSistema_Ativo_Sel, AV6WWPContext, AV90ddo_Sistema_SiglaTitleControlIdToReplace, AV98ddo_Sistema_TecnicaTitleControlIdToReplace, AV102ddo_Sistema_CoordenacaoTitleControlIdToReplace, AV106ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, AV110ddo_Metodologia_DescricaoTitleControlIdToReplace, AV122ddo_Sistema_ProjetoNomeTitleControlIdToReplace, AV142ddo_SistemaVersao_IdTitleControlIdToReplace, AV125ddo_Sistema_AtivoTitleControlIdToReplace, AV137Paginando, AV134Registros, AV135PreView, AV136PaginaAtual, AV60Sistema_AreaTrabalhoCod, AV16DynamicFiltersOperator1, AV82Sistema_PF1, AV22DynamicFiltersOperator2, AV83Sistema_PF2, AV97TFSistema_Tecnica_Sels, AV183Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A127Sistema_Codigo, A351AmbienteTecnologico_Codigo, A137Metodologia_Codigo, A691Sistema_ProjetoCod, A1859SistemaVersao_Codigo, A130Sistema_Ativo, A707Sistema_TipoSigla, A395Sistema_PF, A690Sistema_PFA) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV145WWSistemaDS_1_Sistema_areatrabalhocod = AV60Sistema_AreaTrabalhoCod;
         AV146WWSistemaDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV147WWSistemaDS_3_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV148WWSistemaDS_4_Sistema_nome1 = AV67Sistema_Nome1;
         AV149WWSistemaDS_5_Sistema_sigla1 = AV62Sistema_Sigla1;
         AV150WWSistemaDS_6_Sistema_tipo1 = AV73Sistema_Tipo1;
         AV151WWSistemaDS_7_Sistema_tecnica1 = AV75Sistema_Tecnica1;
         AV152WWSistemaDS_8_Sistema_coordenacao1 = AV76Sistema_Coordenacao1;
         AV153WWSistemaDS_9_Sistema_projetonome1 = AV80Sistema_ProjetoNome1;
         AV154WWSistemaDS_10_Sistema_pf1 = AV82Sistema_PF1;
         AV155WWSistemaDS_11_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV156WWSistemaDS_12_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV157WWSistemaDS_13_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV158WWSistemaDS_14_Sistema_nome2 = AV68Sistema_Nome2;
         AV159WWSistemaDS_15_Sistema_sigla2 = AV63Sistema_Sigla2;
         AV160WWSistemaDS_16_Sistema_tipo2 = AV74Sistema_Tipo2;
         AV161WWSistemaDS_17_Sistema_tecnica2 = AV77Sistema_Tecnica2;
         AV162WWSistemaDS_18_Sistema_coordenacao2 = AV78Sistema_Coordenacao2;
         AV163WWSistemaDS_19_Sistema_projetonome2 = AV81Sistema_ProjetoNome2;
         AV164WWSistemaDS_20_Sistema_pf2 = AV83Sistema_PF2;
         AV165WWSistemaDS_21_Tfsistema_sigla = AV88TFSistema_Sigla;
         AV166WWSistemaDS_22_Tfsistema_sigla_sel = AV89TFSistema_Sigla_Sel;
         AV167WWSistemaDS_23_Tfsistema_tecnica_sels = AV97TFSistema_Tecnica_Sels;
         AV168WWSistemaDS_24_Tfsistema_coordenacao = AV100TFSistema_Coordenacao;
         AV169WWSistemaDS_25_Tfsistema_coordenacao_sel = AV101TFSistema_Coordenacao_Sel;
         AV170WWSistemaDS_26_Tfambientetecnologico_descricao = AV104TFAmbienteTecnologico_Descricao;
         AV171WWSistemaDS_27_Tfambientetecnologico_descricao_sel = AV105TFAmbienteTecnologico_Descricao_Sel;
         AV172WWSistemaDS_28_Tfmetodologia_descricao = AV108TFMetodologia_Descricao;
         AV173WWSistemaDS_29_Tfmetodologia_descricao_sel = AV109TFMetodologia_Descricao_Sel;
         AV174WWSistemaDS_30_Tfsistema_projetonome = AV120TFSistema_ProjetoNome;
         AV175WWSistemaDS_31_Tfsistema_projetonome_sel = AV121TFSistema_ProjetoNome_Sel;
         AV176WWSistemaDS_32_Tfsistemaversao_id = AV140TFSistemaVersao_Id;
         AV177WWSistemaDS_33_Tfsistemaversao_id_sel = AV141TFSistemaVersao_Id_Sel;
         AV178WWSistemaDS_34_Tfsistema_ativo_sel = AV124TFSistema_Ativo_Sel;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV67Sistema_Nome1, AV62Sistema_Sigla1, AV73Sistema_Tipo1, AV75Sistema_Tecnica1, AV76Sistema_Coordenacao1, AV80Sistema_ProjetoNome1, AV21DynamicFiltersSelector2, AV68Sistema_Nome2, AV63Sistema_Sigla2, AV74Sistema_Tipo2, AV77Sistema_Tecnica2, AV78Sistema_Coordenacao2, AV81Sistema_ProjetoNome2, AV20DynamicFiltersEnabled2, AV88TFSistema_Sigla, AV89TFSistema_Sigla_Sel, AV100TFSistema_Coordenacao, AV101TFSistema_Coordenacao_Sel, AV104TFAmbienteTecnologico_Descricao, AV105TFAmbienteTecnologico_Descricao_Sel, AV108TFMetodologia_Descricao, AV109TFMetodologia_Descricao_Sel, AV120TFSistema_ProjetoNome, AV121TFSistema_ProjetoNome_Sel, AV140TFSistemaVersao_Id, AV141TFSistemaVersao_Id_Sel, AV124TFSistema_Ativo_Sel, AV6WWPContext, AV90ddo_Sistema_SiglaTitleControlIdToReplace, AV98ddo_Sistema_TecnicaTitleControlIdToReplace, AV102ddo_Sistema_CoordenacaoTitleControlIdToReplace, AV106ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, AV110ddo_Metodologia_DescricaoTitleControlIdToReplace, AV122ddo_Sistema_ProjetoNomeTitleControlIdToReplace, AV142ddo_SistemaVersao_IdTitleControlIdToReplace, AV125ddo_Sistema_AtivoTitleControlIdToReplace, AV137Paginando, AV134Registros, AV135PreView, AV136PaginaAtual, AV60Sistema_AreaTrabalhoCod, AV16DynamicFiltersOperator1, AV82Sistema_PF1, AV22DynamicFiltersOperator2, AV83Sistema_PF2, AV97TFSistema_Tecnica_Sels, AV183Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A127Sistema_Codigo, A351AmbienteTecnologico_Codigo, A137Metodologia_Codigo, A691Sistema_ProjetoCod, A1859SistemaVersao_Codigo, A130Sistema_Ativo, A707Sistema_TipoSigla, A395Sistema_PF, A690Sistema_PFA) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV145WWSistemaDS_1_Sistema_areatrabalhocod = AV60Sistema_AreaTrabalhoCod;
         AV146WWSistemaDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV147WWSistemaDS_3_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV148WWSistemaDS_4_Sistema_nome1 = AV67Sistema_Nome1;
         AV149WWSistemaDS_5_Sistema_sigla1 = AV62Sistema_Sigla1;
         AV150WWSistemaDS_6_Sistema_tipo1 = AV73Sistema_Tipo1;
         AV151WWSistemaDS_7_Sistema_tecnica1 = AV75Sistema_Tecnica1;
         AV152WWSistemaDS_8_Sistema_coordenacao1 = AV76Sistema_Coordenacao1;
         AV153WWSistemaDS_9_Sistema_projetonome1 = AV80Sistema_ProjetoNome1;
         AV154WWSistemaDS_10_Sistema_pf1 = AV82Sistema_PF1;
         AV155WWSistemaDS_11_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV156WWSistemaDS_12_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV157WWSistemaDS_13_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV158WWSistemaDS_14_Sistema_nome2 = AV68Sistema_Nome2;
         AV159WWSistemaDS_15_Sistema_sigla2 = AV63Sistema_Sigla2;
         AV160WWSistemaDS_16_Sistema_tipo2 = AV74Sistema_Tipo2;
         AV161WWSistemaDS_17_Sistema_tecnica2 = AV77Sistema_Tecnica2;
         AV162WWSistemaDS_18_Sistema_coordenacao2 = AV78Sistema_Coordenacao2;
         AV163WWSistemaDS_19_Sistema_projetonome2 = AV81Sistema_ProjetoNome2;
         AV164WWSistemaDS_20_Sistema_pf2 = AV83Sistema_PF2;
         AV165WWSistemaDS_21_Tfsistema_sigla = AV88TFSistema_Sigla;
         AV166WWSistemaDS_22_Tfsistema_sigla_sel = AV89TFSistema_Sigla_Sel;
         AV167WWSistemaDS_23_Tfsistema_tecnica_sels = AV97TFSistema_Tecnica_Sels;
         AV168WWSistemaDS_24_Tfsistema_coordenacao = AV100TFSistema_Coordenacao;
         AV169WWSistemaDS_25_Tfsistema_coordenacao_sel = AV101TFSistema_Coordenacao_Sel;
         AV170WWSistemaDS_26_Tfambientetecnologico_descricao = AV104TFAmbienteTecnologico_Descricao;
         AV171WWSistemaDS_27_Tfambientetecnologico_descricao_sel = AV105TFAmbienteTecnologico_Descricao_Sel;
         AV172WWSistemaDS_28_Tfmetodologia_descricao = AV108TFMetodologia_Descricao;
         AV173WWSistemaDS_29_Tfmetodologia_descricao_sel = AV109TFMetodologia_Descricao_Sel;
         AV174WWSistemaDS_30_Tfsistema_projetonome = AV120TFSistema_ProjetoNome;
         AV175WWSistemaDS_31_Tfsistema_projetonome_sel = AV121TFSistema_ProjetoNome_Sel;
         AV176WWSistemaDS_32_Tfsistemaversao_id = AV140TFSistemaVersao_Id;
         AV177WWSistemaDS_33_Tfsistemaversao_id_sel = AV141TFSistemaVersao_Id_Sel;
         AV178WWSistemaDS_34_Tfsistema_ativo_sel = AV124TFSistema_Ativo_Sel;
         subGrid_Islastpage = 1;
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV67Sistema_Nome1, AV62Sistema_Sigla1, AV73Sistema_Tipo1, AV75Sistema_Tecnica1, AV76Sistema_Coordenacao1, AV80Sistema_ProjetoNome1, AV21DynamicFiltersSelector2, AV68Sistema_Nome2, AV63Sistema_Sigla2, AV74Sistema_Tipo2, AV77Sistema_Tecnica2, AV78Sistema_Coordenacao2, AV81Sistema_ProjetoNome2, AV20DynamicFiltersEnabled2, AV88TFSistema_Sigla, AV89TFSistema_Sigla_Sel, AV100TFSistema_Coordenacao, AV101TFSistema_Coordenacao_Sel, AV104TFAmbienteTecnologico_Descricao, AV105TFAmbienteTecnologico_Descricao_Sel, AV108TFMetodologia_Descricao, AV109TFMetodologia_Descricao_Sel, AV120TFSistema_ProjetoNome, AV121TFSistema_ProjetoNome_Sel, AV140TFSistemaVersao_Id, AV141TFSistemaVersao_Id_Sel, AV124TFSistema_Ativo_Sel, AV6WWPContext, AV90ddo_Sistema_SiglaTitleControlIdToReplace, AV98ddo_Sistema_TecnicaTitleControlIdToReplace, AV102ddo_Sistema_CoordenacaoTitleControlIdToReplace, AV106ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, AV110ddo_Metodologia_DescricaoTitleControlIdToReplace, AV122ddo_Sistema_ProjetoNomeTitleControlIdToReplace, AV142ddo_SistemaVersao_IdTitleControlIdToReplace, AV125ddo_Sistema_AtivoTitleControlIdToReplace, AV137Paginando, AV134Registros, AV135PreView, AV136PaginaAtual, AV60Sistema_AreaTrabalhoCod, AV16DynamicFiltersOperator1, AV82Sistema_PF1, AV22DynamicFiltersOperator2, AV83Sistema_PF2, AV97TFSistema_Tecnica_Sels, AV183Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A127Sistema_Codigo, A351AmbienteTecnologico_Codigo, A137Metodologia_Codigo, A691Sistema_ProjetoCod, A1859SistemaVersao_Codigo, A130Sistema_Ativo, A707Sistema_TipoSigla, A395Sistema_PF, A690Sistema_PFA) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV145WWSistemaDS_1_Sistema_areatrabalhocod = AV60Sistema_AreaTrabalhoCod;
         AV146WWSistemaDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV147WWSistemaDS_3_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV148WWSistemaDS_4_Sistema_nome1 = AV67Sistema_Nome1;
         AV149WWSistemaDS_5_Sistema_sigla1 = AV62Sistema_Sigla1;
         AV150WWSistemaDS_6_Sistema_tipo1 = AV73Sistema_Tipo1;
         AV151WWSistemaDS_7_Sistema_tecnica1 = AV75Sistema_Tecnica1;
         AV152WWSistemaDS_8_Sistema_coordenacao1 = AV76Sistema_Coordenacao1;
         AV153WWSistemaDS_9_Sistema_projetonome1 = AV80Sistema_ProjetoNome1;
         AV154WWSistemaDS_10_Sistema_pf1 = AV82Sistema_PF1;
         AV155WWSistemaDS_11_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV156WWSistemaDS_12_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV157WWSistemaDS_13_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV158WWSistemaDS_14_Sistema_nome2 = AV68Sistema_Nome2;
         AV159WWSistemaDS_15_Sistema_sigla2 = AV63Sistema_Sigla2;
         AV160WWSistemaDS_16_Sistema_tipo2 = AV74Sistema_Tipo2;
         AV161WWSistemaDS_17_Sistema_tecnica2 = AV77Sistema_Tecnica2;
         AV162WWSistemaDS_18_Sistema_coordenacao2 = AV78Sistema_Coordenacao2;
         AV163WWSistemaDS_19_Sistema_projetonome2 = AV81Sistema_ProjetoNome2;
         AV164WWSistemaDS_20_Sistema_pf2 = AV83Sistema_PF2;
         AV165WWSistemaDS_21_Tfsistema_sigla = AV88TFSistema_Sigla;
         AV166WWSistemaDS_22_Tfsistema_sigla_sel = AV89TFSistema_Sigla_Sel;
         AV167WWSistemaDS_23_Tfsistema_tecnica_sels = AV97TFSistema_Tecnica_Sels;
         AV168WWSistemaDS_24_Tfsistema_coordenacao = AV100TFSistema_Coordenacao;
         AV169WWSistemaDS_25_Tfsistema_coordenacao_sel = AV101TFSistema_Coordenacao_Sel;
         AV170WWSistemaDS_26_Tfambientetecnologico_descricao = AV104TFAmbienteTecnologico_Descricao;
         AV171WWSistemaDS_27_Tfambientetecnologico_descricao_sel = AV105TFAmbienteTecnologico_Descricao_Sel;
         AV172WWSistemaDS_28_Tfmetodologia_descricao = AV108TFMetodologia_Descricao;
         AV173WWSistemaDS_29_Tfmetodologia_descricao_sel = AV109TFMetodologia_Descricao_Sel;
         AV174WWSistemaDS_30_Tfsistema_projetonome = AV120TFSistema_ProjetoNome;
         AV175WWSistemaDS_31_Tfsistema_projetonome_sel = AV121TFSistema_ProjetoNome_Sel;
         AV176WWSistemaDS_32_Tfsistemaversao_id = AV140TFSistemaVersao_Id;
         AV177WWSistemaDS_33_Tfsistemaversao_id_sel = AV141TFSistemaVersao_Id_Sel;
         AV178WWSistemaDS_34_Tfsistema_ativo_sel = AV124TFSistema_Ativo_Sel;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV67Sistema_Nome1, AV62Sistema_Sigla1, AV73Sistema_Tipo1, AV75Sistema_Tecnica1, AV76Sistema_Coordenacao1, AV80Sistema_ProjetoNome1, AV21DynamicFiltersSelector2, AV68Sistema_Nome2, AV63Sistema_Sigla2, AV74Sistema_Tipo2, AV77Sistema_Tecnica2, AV78Sistema_Coordenacao2, AV81Sistema_ProjetoNome2, AV20DynamicFiltersEnabled2, AV88TFSistema_Sigla, AV89TFSistema_Sigla_Sel, AV100TFSistema_Coordenacao, AV101TFSistema_Coordenacao_Sel, AV104TFAmbienteTecnologico_Descricao, AV105TFAmbienteTecnologico_Descricao_Sel, AV108TFMetodologia_Descricao, AV109TFMetodologia_Descricao_Sel, AV120TFSistema_ProjetoNome, AV121TFSistema_ProjetoNome_Sel, AV140TFSistemaVersao_Id, AV141TFSistemaVersao_Id_Sel, AV124TFSistema_Ativo_Sel, AV6WWPContext, AV90ddo_Sistema_SiglaTitleControlIdToReplace, AV98ddo_Sistema_TecnicaTitleControlIdToReplace, AV102ddo_Sistema_CoordenacaoTitleControlIdToReplace, AV106ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, AV110ddo_Metodologia_DescricaoTitleControlIdToReplace, AV122ddo_Sistema_ProjetoNomeTitleControlIdToReplace, AV142ddo_SistemaVersao_IdTitleControlIdToReplace, AV125ddo_Sistema_AtivoTitleControlIdToReplace, AV137Paginando, AV134Registros, AV135PreView, AV136PaginaAtual, AV60Sistema_AreaTrabalhoCod, AV16DynamicFiltersOperator1, AV82Sistema_PF1, AV22DynamicFiltersOperator2, AV83Sistema_PF2, AV97TFSistema_Tecnica_Sels, AV183Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A127Sistema_Codigo, A351AmbienteTecnologico_Codigo, A137Metodologia_Codigo, A691Sistema_ProjetoCod, A1859SistemaVersao_Codigo, A130Sistema_Ativo, A707Sistema_TipoSigla, A395Sistema_PF, A690Sistema_PFA) ;
         }
         return (int)(0) ;
      }

      protected void STRUP3G0( )
      {
         /* Before Start, stand alone formulas. */
         AV183Pgmname = "WWSistema";
         context.Gx_err = 0;
         edtavSistema_tiposigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistema_tiposigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_tiposigla_Enabled), 5, 0)));
         edtavPfb_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfb_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfb_Enabled), 5, 0)));
         edtavPfa_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfa_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfa_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E303G2 */
         E303G2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV126DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vSISTEMA_SIGLATITLEFILTERDATA"), AV87Sistema_SiglaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSISTEMA_TECNICATITLEFILTERDATA"), AV95Sistema_TecnicaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSISTEMA_COORDENACAOTITLEFILTERDATA"), AV99Sistema_CoordenacaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vAMBIENTETECNOLOGICO_DESCRICAOTITLEFILTERDATA"), AV103AmbienteTecnologico_DescricaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vMETODOLOGIA_DESCRICAOTITLEFILTERDATA"), AV107Metodologia_DescricaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSISTEMA_PROJETONOMETITLEFILTERDATA"), AV119Sistema_ProjetoNomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSISTEMAVERSAO_IDTITLEFILTERDATA"), AV139SistemaVersao_IdTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSISTEMA_ATIVOTITLEFILTERDATA"), AV123Sistema_AtivoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vNOTIFICATIONINFO"), AV61NotificationInfo);
            ajax_req_read_hidden_sdt(cgiGet( "vWWPCONTEXT"), AV6WWPContext);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavSistema_areatrabalhocod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavSistema_areatrabalhocod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vSISTEMA_AREATRABALHOCOD");
               GX_FocusControl = edtavSistema_areatrabalhocod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV60Sistema_AreaTrabalhoCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60Sistema_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60Sistema_AreaTrabalhoCod), 6, 0)));
            }
            else
            {
               AV60Sistema_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtavSistema_areatrabalhocod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60Sistema_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60Sistema_AreaTrabalhoCod), 6, 0)));
            }
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            AV67Sistema_Nome1 = StringUtil.Upper( cgiGet( edtavSistema_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67Sistema_Nome1", AV67Sistema_Nome1);
            AV62Sistema_Sigla1 = StringUtil.Upper( cgiGet( edtavSistema_sigla1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62Sistema_Sigla1", AV62Sistema_Sigla1);
            cmbavSistema_tipo1.Name = cmbavSistema_tipo1_Internalname;
            cmbavSistema_tipo1.CurrentValue = cgiGet( cmbavSistema_tipo1_Internalname);
            AV73Sistema_Tipo1 = cgiGet( cmbavSistema_tipo1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73Sistema_Tipo1", AV73Sistema_Tipo1);
            cmbavSistema_tecnica1.Name = cmbavSistema_tecnica1_Internalname;
            cmbavSistema_tecnica1.CurrentValue = cgiGet( cmbavSistema_tecnica1_Internalname);
            AV75Sistema_Tecnica1 = cgiGet( cmbavSistema_tecnica1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75Sistema_Tecnica1", AV75Sistema_Tecnica1);
            AV76Sistema_Coordenacao1 = StringUtil.Upper( cgiGet( edtavSistema_coordenacao1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76Sistema_Coordenacao1", AV76Sistema_Coordenacao1);
            AV80Sistema_ProjetoNome1 = StringUtil.Upper( cgiGet( edtavSistema_projetonome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80Sistema_ProjetoNome1", AV80Sistema_ProjetoNome1);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavSistema_pf1_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavSistema_pf1_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vSISTEMA_PF1");
               GX_FocusControl = edtavSistema_pf1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV82Sistema_PF1 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82Sistema_PF1", StringUtil.LTrim( StringUtil.Str( AV82Sistema_PF1, 14, 5)));
            }
            else
            {
               AV82Sistema_PF1 = context.localUtil.CToN( cgiGet( edtavSistema_pf1_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82Sistema_PF1", StringUtil.LTrim( StringUtil.Str( AV82Sistema_PF1, 14, 5)));
            }
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV21DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
            AV68Sistema_Nome2 = StringUtil.Upper( cgiGet( edtavSistema_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68Sistema_Nome2", AV68Sistema_Nome2);
            AV63Sistema_Sigla2 = StringUtil.Upper( cgiGet( edtavSistema_sigla2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63Sistema_Sigla2", AV63Sistema_Sigla2);
            cmbavSistema_tipo2.Name = cmbavSistema_tipo2_Internalname;
            cmbavSistema_tipo2.CurrentValue = cgiGet( cmbavSistema_tipo2_Internalname);
            AV74Sistema_Tipo2 = cgiGet( cmbavSistema_tipo2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74Sistema_Tipo2", AV74Sistema_Tipo2);
            cmbavSistema_tecnica2.Name = cmbavSistema_tecnica2_Internalname;
            cmbavSistema_tecnica2.CurrentValue = cgiGet( cmbavSistema_tecnica2_Internalname);
            AV77Sistema_Tecnica2 = cgiGet( cmbavSistema_tecnica2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77Sistema_Tecnica2", AV77Sistema_Tecnica2);
            AV78Sistema_Coordenacao2 = StringUtil.Upper( cgiGet( edtavSistema_coordenacao2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78Sistema_Coordenacao2", AV78Sistema_Coordenacao2);
            AV81Sistema_ProjetoNome2 = StringUtil.Upper( cgiGet( edtavSistema_projetonome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81Sistema_ProjetoNome2", AV81Sistema_ProjetoNome2);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavSistema_pf2_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavSistema_pf2_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vSISTEMA_PF2");
               GX_FocusControl = edtavSistema_pf2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV83Sistema_PF2 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83Sistema_PF2", StringUtil.LTrim( StringUtil.Str( AV83Sistema_PF2, 14, 5)));
            }
            else
            {
               AV83Sistema_PF2 = context.localUtil.CToN( cgiGet( edtavSistema_pf2_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83Sistema_PF2", StringUtil.LTrim( StringUtil.Str( AV83Sistema_PF2, 14, 5)));
            }
            AV20DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
            AV88TFSistema_Sigla = StringUtil.Upper( cgiGet( edtavTfsistema_sigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88TFSistema_Sigla", AV88TFSistema_Sigla);
            AV89TFSistema_Sigla_Sel = StringUtil.Upper( cgiGet( edtavTfsistema_sigla_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89TFSistema_Sigla_Sel", AV89TFSistema_Sigla_Sel);
            AV100TFSistema_Coordenacao = StringUtil.Upper( cgiGet( edtavTfsistema_coordenacao_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV100TFSistema_Coordenacao", AV100TFSistema_Coordenacao);
            AV101TFSistema_Coordenacao_Sel = StringUtil.Upper( cgiGet( edtavTfsistema_coordenacao_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV101TFSistema_Coordenacao_Sel", AV101TFSistema_Coordenacao_Sel);
            AV104TFAmbienteTecnologico_Descricao = StringUtil.Upper( cgiGet( edtavTfambientetecnologico_descricao_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV104TFAmbienteTecnologico_Descricao", AV104TFAmbienteTecnologico_Descricao);
            AV105TFAmbienteTecnologico_Descricao_Sel = StringUtil.Upper( cgiGet( edtavTfambientetecnologico_descricao_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105TFAmbienteTecnologico_Descricao_Sel", AV105TFAmbienteTecnologico_Descricao_Sel);
            AV108TFMetodologia_Descricao = StringUtil.Upper( cgiGet( edtavTfmetodologia_descricao_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV108TFMetodologia_Descricao", AV108TFMetodologia_Descricao);
            AV109TFMetodologia_Descricao_Sel = StringUtil.Upper( cgiGet( edtavTfmetodologia_descricao_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV109TFMetodologia_Descricao_Sel", AV109TFMetodologia_Descricao_Sel);
            AV120TFSistema_ProjetoNome = StringUtil.Upper( cgiGet( edtavTfsistema_projetonome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV120TFSistema_ProjetoNome", AV120TFSistema_ProjetoNome);
            AV121TFSistema_ProjetoNome_Sel = StringUtil.Upper( cgiGet( edtavTfsistema_projetonome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV121TFSistema_ProjetoNome_Sel", AV121TFSistema_ProjetoNome_Sel);
            AV140TFSistemaVersao_Id = cgiGet( edtavTfsistemaversao_id_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV140TFSistemaVersao_Id", AV140TFSistemaVersao_Id);
            AV141TFSistemaVersao_Id_Sel = cgiGet( edtavTfsistemaversao_id_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV141TFSistemaVersao_Id_Sel", AV141TFSistemaVersao_Id_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsistema_ativo_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsistema_ativo_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSISTEMA_ATIVO_SEL");
               GX_FocusControl = edtavTfsistema_ativo_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV124TFSistema_Ativo_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV124TFSistema_Ativo_Sel", StringUtil.Str( (decimal)(AV124TFSistema_Ativo_Sel), 1, 0));
            }
            else
            {
               AV124TFSistema_Ativo_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfsistema_ativo_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV124TFSistema_Ativo_Sel", StringUtil.Str( (decimal)(AV124TFSistema_Ativo_Sel), 1, 0));
            }
            AV90ddo_Sistema_SiglaTitleControlIdToReplace = cgiGet( edtavDdo_sistema_siglatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90ddo_Sistema_SiglaTitleControlIdToReplace", AV90ddo_Sistema_SiglaTitleControlIdToReplace);
            AV98ddo_Sistema_TecnicaTitleControlIdToReplace = cgiGet( edtavDdo_sistema_tecnicatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV98ddo_Sistema_TecnicaTitleControlIdToReplace", AV98ddo_Sistema_TecnicaTitleControlIdToReplace);
            AV102ddo_Sistema_CoordenacaoTitleControlIdToReplace = cgiGet( edtavDdo_sistema_coordenacaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV102ddo_Sistema_CoordenacaoTitleControlIdToReplace", AV102ddo_Sistema_CoordenacaoTitleControlIdToReplace);
            AV106ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace = cgiGet( edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV106ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace", AV106ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace);
            AV110ddo_Metodologia_DescricaoTitleControlIdToReplace = cgiGet( edtavDdo_metodologia_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV110ddo_Metodologia_DescricaoTitleControlIdToReplace", AV110ddo_Metodologia_DescricaoTitleControlIdToReplace);
            AV122ddo_Sistema_ProjetoNomeTitleControlIdToReplace = cgiGet( edtavDdo_sistema_projetonometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV122ddo_Sistema_ProjetoNomeTitleControlIdToReplace", AV122ddo_Sistema_ProjetoNomeTitleControlIdToReplace);
            AV142ddo_SistemaVersao_IdTitleControlIdToReplace = cgiGet( edtavDdo_sistemaversao_idtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV142ddo_SistemaVersao_IdTitleControlIdToReplace", AV142ddo_SistemaVersao_IdTitleControlIdToReplace);
            AV125ddo_Sistema_AtivoTitleControlIdToReplace = cgiGet( edtavDdo_sistema_ativotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV125ddo_Sistema_AtivoTitleControlIdToReplace", AV125ddo_Sistema_AtivoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_89 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_89"), ",", "."));
            AV128GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV129GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Innewwindow1_Width = cgiGet( "INNEWWINDOW1_Width");
            Innewwindow1_Height = cgiGet( "INNEWWINDOW1_Height");
            Innewwindow1_Target = cgiGet( "INNEWWINDOW1_Target");
            Ddo_sistema_sigla_Caption = cgiGet( "DDO_SISTEMA_SIGLA_Caption");
            Ddo_sistema_sigla_Tooltip = cgiGet( "DDO_SISTEMA_SIGLA_Tooltip");
            Ddo_sistema_sigla_Cls = cgiGet( "DDO_SISTEMA_SIGLA_Cls");
            Ddo_sistema_sigla_Filteredtext_set = cgiGet( "DDO_SISTEMA_SIGLA_Filteredtext_set");
            Ddo_sistema_sigla_Selectedvalue_set = cgiGet( "DDO_SISTEMA_SIGLA_Selectedvalue_set");
            Ddo_sistema_sigla_Dropdownoptionstype = cgiGet( "DDO_SISTEMA_SIGLA_Dropdownoptionstype");
            Ddo_sistema_sigla_Titlecontrolidtoreplace = cgiGet( "DDO_SISTEMA_SIGLA_Titlecontrolidtoreplace");
            Ddo_sistema_sigla_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SISTEMA_SIGLA_Includesortasc"));
            Ddo_sistema_sigla_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SISTEMA_SIGLA_Includesortdsc"));
            Ddo_sistema_sigla_Sortedstatus = cgiGet( "DDO_SISTEMA_SIGLA_Sortedstatus");
            Ddo_sistema_sigla_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SISTEMA_SIGLA_Includefilter"));
            Ddo_sistema_sigla_Filtertype = cgiGet( "DDO_SISTEMA_SIGLA_Filtertype");
            Ddo_sistema_sigla_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SISTEMA_SIGLA_Filterisrange"));
            Ddo_sistema_sigla_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SISTEMA_SIGLA_Includedatalist"));
            Ddo_sistema_sigla_Datalisttype = cgiGet( "DDO_SISTEMA_SIGLA_Datalisttype");
            Ddo_sistema_sigla_Datalistproc = cgiGet( "DDO_SISTEMA_SIGLA_Datalistproc");
            Ddo_sistema_sigla_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_SISTEMA_SIGLA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_sistema_sigla_Sortasc = cgiGet( "DDO_SISTEMA_SIGLA_Sortasc");
            Ddo_sistema_sigla_Sortdsc = cgiGet( "DDO_SISTEMA_SIGLA_Sortdsc");
            Ddo_sistema_sigla_Loadingdata = cgiGet( "DDO_SISTEMA_SIGLA_Loadingdata");
            Ddo_sistema_sigla_Cleanfilter = cgiGet( "DDO_SISTEMA_SIGLA_Cleanfilter");
            Ddo_sistema_sigla_Noresultsfound = cgiGet( "DDO_SISTEMA_SIGLA_Noresultsfound");
            Ddo_sistema_sigla_Searchbuttontext = cgiGet( "DDO_SISTEMA_SIGLA_Searchbuttontext");
            Ddo_sistema_tecnica_Caption = cgiGet( "DDO_SISTEMA_TECNICA_Caption");
            Ddo_sistema_tecnica_Tooltip = cgiGet( "DDO_SISTEMA_TECNICA_Tooltip");
            Ddo_sistema_tecnica_Cls = cgiGet( "DDO_SISTEMA_TECNICA_Cls");
            Ddo_sistema_tecnica_Selectedvalue_set = cgiGet( "DDO_SISTEMA_TECNICA_Selectedvalue_set");
            Ddo_sistema_tecnica_Dropdownoptionstype = cgiGet( "DDO_SISTEMA_TECNICA_Dropdownoptionstype");
            Ddo_sistema_tecnica_Titlecontrolidtoreplace = cgiGet( "DDO_SISTEMA_TECNICA_Titlecontrolidtoreplace");
            Ddo_sistema_tecnica_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SISTEMA_TECNICA_Includesortasc"));
            Ddo_sistema_tecnica_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SISTEMA_TECNICA_Includesortdsc"));
            Ddo_sistema_tecnica_Sortedstatus = cgiGet( "DDO_SISTEMA_TECNICA_Sortedstatus");
            Ddo_sistema_tecnica_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SISTEMA_TECNICA_Includefilter"));
            Ddo_sistema_tecnica_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SISTEMA_TECNICA_Includedatalist"));
            Ddo_sistema_tecnica_Datalisttype = cgiGet( "DDO_SISTEMA_TECNICA_Datalisttype");
            Ddo_sistema_tecnica_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_SISTEMA_TECNICA_Allowmultipleselection"));
            Ddo_sistema_tecnica_Datalistfixedvalues = cgiGet( "DDO_SISTEMA_TECNICA_Datalistfixedvalues");
            Ddo_sistema_tecnica_Sortasc = cgiGet( "DDO_SISTEMA_TECNICA_Sortasc");
            Ddo_sistema_tecnica_Sortdsc = cgiGet( "DDO_SISTEMA_TECNICA_Sortdsc");
            Ddo_sistema_tecnica_Cleanfilter = cgiGet( "DDO_SISTEMA_TECNICA_Cleanfilter");
            Ddo_sistema_tecnica_Searchbuttontext = cgiGet( "DDO_SISTEMA_TECNICA_Searchbuttontext");
            Ddo_sistema_coordenacao_Caption = cgiGet( "DDO_SISTEMA_COORDENACAO_Caption");
            Ddo_sistema_coordenacao_Tooltip = cgiGet( "DDO_SISTEMA_COORDENACAO_Tooltip");
            Ddo_sistema_coordenacao_Cls = cgiGet( "DDO_SISTEMA_COORDENACAO_Cls");
            Ddo_sistema_coordenacao_Filteredtext_set = cgiGet( "DDO_SISTEMA_COORDENACAO_Filteredtext_set");
            Ddo_sistema_coordenacao_Selectedvalue_set = cgiGet( "DDO_SISTEMA_COORDENACAO_Selectedvalue_set");
            Ddo_sistema_coordenacao_Dropdownoptionstype = cgiGet( "DDO_SISTEMA_COORDENACAO_Dropdownoptionstype");
            Ddo_sistema_coordenacao_Titlecontrolidtoreplace = cgiGet( "DDO_SISTEMA_COORDENACAO_Titlecontrolidtoreplace");
            Ddo_sistema_coordenacao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SISTEMA_COORDENACAO_Includesortasc"));
            Ddo_sistema_coordenacao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SISTEMA_COORDENACAO_Includesortdsc"));
            Ddo_sistema_coordenacao_Sortedstatus = cgiGet( "DDO_SISTEMA_COORDENACAO_Sortedstatus");
            Ddo_sistema_coordenacao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SISTEMA_COORDENACAO_Includefilter"));
            Ddo_sistema_coordenacao_Filtertype = cgiGet( "DDO_SISTEMA_COORDENACAO_Filtertype");
            Ddo_sistema_coordenacao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SISTEMA_COORDENACAO_Filterisrange"));
            Ddo_sistema_coordenacao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SISTEMA_COORDENACAO_Includedatalist"));
            Ddo_sistema_coordenacao_Datalisttype = cgiGet( "DDO_SISTEMA_COORDENACAO_Datalisttype");
            Ddo_sistema_coordenacao_Datalistproc = cgiGet( "DDO_SISTEMA_COORDENACAO_Datalistproc");
            Ddo_sistema_coordenacao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_SISTEMA_COORDENACAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_sistema_coordenacao_Sortasc = cgiGet( "DDO_SISTEMA_COORDENACAO_Sortasc");
            Ddo_sistema_coordenacao_Sortdsc = cgiGet( "DDO_SISTEMA_COORDENACAO_Sortdsc");
            Ddo_sistema_coordenacao_Loadingdata = cgiGet( "DDO_SISTEMA_COORDENACAO_Loadingdata");
            Ddo_sistema_coordenacao_Cleanfilter = cgiGet( "DDO_SISTEMA_COORDENACAO_Cleanfilter");
            Ddo_sistema_coordenacao_Noresultsfound = cgiGet( "DDO_SISTEMA_COORDENACAO_Noresultsfound");
            Ddo_sistema_coordenacao_Searchbuttontext = cgiGet( "DDO_SISTEMA_COORDENACAO_Searchbuttontext");
            Ddo_ambientetecnologico_descricao_Caption = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Caption");
            Ddo_ambientetecnologico_descricao_Tooltip = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Tooltip");
            Ddo_ambientetecnologico_descricao_Cls = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Cls");
            Ddo_ambientetecnologico_descricao_Filteredtext_set = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Filteredtext_set");
            Ddo_ambientetecnologico_descricao_Selectedvalue_set = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Selectedvalue_set");
            Ddo_ambientetecnologico_descricao_Dropdownoptionstype = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Dropdownoptionstype");
            Ddo_ambientetecnologico_descricao_Titlecontrolidtoreplace = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_ambientetecnologico_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Includesortasc"));
            Ddo_ambientetecnologico_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Includesortdsc"));
            Ddo_ambientetecnologico_descricao_Sortedstatus = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Sortedstatus");
            Ddo_ambientetecnologico_descricao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Includefilter"));
            Ddo_ambientetecnologico_descricao_Filtertype = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Filtertype");
            Ddo_ambientetecnologico_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Filterisrange"));
            Ddo_ambientetecnologico_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Includedatalist"));
            Ddo_ambientetecnologico_descricao_Datalisttype = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Datalisttype");
            Ddo_ambientetecnologico_descricao_Datalistproc = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Datalistproc");
            Ddo_ambientetecnologico_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_ambientetecnologico_descricao_Sortasc = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Sortasc");
            Ddo_ambientetecnologico_descricao_Sortdsc = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Sortdsc");
            Ddo_ambientetecnologico_descricao_Loadingdata = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Loadingdata");
            Ddo_ambientetecnologico_descricao_Cleanfilter = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Cleanfilter");
            Ddo_ambientetecnologico_descricao_Noresultsfound = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Noresultsfound");
            Ddo_ambientetecnologico_descricao_Searchbuttontext = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Searchbuttontext");
            Ddo_metodologia_descricao_Caption = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Caption");
            Ddo_metodologia_descricao_Tooltip = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Tooltip");
            Ddo_metodologia_descricao_Cls = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Cls");
            Ddo_metodologia_descricao_Filteredtext_set = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Filteredtext_set");
            Ddo_metodologia_descricao_Selectedvalue_set = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Selectedvalue_set");
            Ddo_metodologia_descricao_Dropdownoptionstype = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Dropdownoptionstype");
            Ddo_metodologia_descricao_Titlecontrolidtoreplace = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_metodologia_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_METODOLOGIA_DESCRICAO_Includesortasc"));
            Ddo_metodologia_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_METODOLOGIA_DESCRICAO_Includesortdsc"));
            Ddo_metodologia_descricao_Sortedstatus = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Sortedstatus");
            Ddo_metodologia_descricao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_METODOLOGIA_DESCRICAO_Includefilter"));
            Ddo_metodologia_descricao_Filtertype = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Filtertype");
            Ddo_metodologia_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_METODOLOGIA_DESCRICAO_Filterisrange"));
            Ddo_metodologia_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_METODOLOGIA_DESCRICAO_Includedatalist"));
            Ddo_metodologia_descricao_Datalisttype = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Datalisttype");
            Ddo_metodologia_descricao_Datalistproc = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Datalistproc");
            Ddo_metodologia_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_METODOLOGIA_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_metodologia_descricao_Sortasc = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Sortasc");
            Ddo_metodologia_descricao_Sortdsc = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Sortdsc");
            Ddo_metodologia_descricao_Loadingdata = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Loadingdata");
            Ddo_metodologia_descricao_Cleanfilter = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Cleanfilter");
            Ddo_metodologia_descricao_Noresultsfound = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Noresultsfound");
            Ddo_metodologia_descricao_Searchbuttontext = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Searchbuttontext");
            Ddo_sistema_projetonome_Caption = cgiGet( "DDO_SISTEMA_PROJETONOME_Caption");
            Ddo_sistema_projetonome_Tooltip = cgiGet( "DDO_SISTEMA_PROJETONOME_Tooltip");
            Ddo_sistema_projetonome_Cls = cgiGet( "DDO_SISTEMA_PROJETONOME_Cls");
            Ddo_sistema_projetonome_Filteredtext_set = cgiGet( "DDO_SISTEMA_PROJETONOME_Filteredtext_set");
            Ddo_sistema_projetonome_Selectedvalue_set = cgiGet( "DDO_SISTEMA_PROJETONOME_Selectedvalue_set");
            Ddo_sistema_projetonome_Dropdownoptionstype = cgiGet( "DDO_SISTEMA_PROJETONOME_Dropdownoptionstype");
            Ddo_sistema_projetonome_Titlecontrolidtoreplace = cgiGet( "DDO_SISTEMA_PROJETONOME_Titlecontrolidtoreplace");
            Ddo_sistema_projetonome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SISTEMA_PROJETONOME_Includesortasc"));
            Ddo_sistema_projetonome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SISTEMA_PROJETONOME_Includesortdsc"));
            Ddo_sistema_projetonome_Sortedstatus = cgiGet( "DDO_SISTEMA_PROJETONOME_Sortedstatus");
            Ddo_sistema_projetonome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SISTEMA_PROJETONOME_Includefilter"));
            Ddo_sistema_projetonome_Filtertype = cgiGet( "DDO_SISTEMA_PROJETONOME_Filtertype");
            Ddo_sistema_projetonome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SISTEMA_PROJETONOME_Filterisrange"));
            Ddo_sistema_projetonome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SISTEMA_PROJETONOME_Includedatalist"));
            Ddo_sistema_projetonome_Datalisttype = cgiGet( "DDO_SISTEMA_PROJETONOME_Datalisttype");
            Ddo_sistema_projetonome_Datalistproc = cgiGet( "DDO_SISTEMA_PROJETONOME_Datalistproc");
            Ddo_sistema_projetonome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_SISTEMA_PROJETONOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_sistema_projetonome_Sortasc = cgiGet( "DDO_SISTEMA_PROJETONOME_Sortasc");
            Ddo_sistema_projetonome_Sortdsc = cgiGet( "DDO_SISTEMA_PROJETONOME_Sortdsc");
            Ddo_sistema_projetonome_Loadingdata = cgiGet( "DDO_SISTEMA_PROJETONOME_Loadingdata");
            Ddo_sistema_projetonome_Cleanfilter = cgiGet( "DDO_SISTEMA_PROJETONOME_Cleanfilter");
            Ddo_sistema_projetonome_Noresultsfound = cgiGet( "DDO_SISTEMA_PROJETONOME_Noresultsfound");
            Ddo_sistema_projetonome_Searchbuttontext = cgiGet( "DDO_SISTEMA_PROJETONOME_Searchbuttontext");
            Ddo_sistemaversao_id_Caption = cgiGet( "DDO_SISTEMAVERSAO_ID_Caption");
            Ddo_sistemaversao_id_Tooltip = cgiGet( "DDO_SISTEMAVERSAO_ID_Tooltip");
            Ddo_sistemaversao_id_Cls = cgiGet( "DDO_SISTEMAVERSAO_ID_Cls");
            Ddo_sistemaversao_id_Filteredtext_set = cgiGet( "DDO_SISTEMAVERSAO_ID_Filteredtext_set");
            Ddo_sistemaversao_id_Selectedvalue_set = cgiGet( "DDO_SISTEMAVERSAO_ID_Selectedvalue_set");
            Ddo_sistemaversao_id_Dropdownoptionstype = cgiGet( "DDO_SISTEMAVERSAO_ID_Dropdownoptionstype");
            Ddo_sistemaversao_id_Titlecontrolidtoreplace = cgiGet( "DDO_SISTEMAVERSAO_ID_Titlecontrolidtoreplace");
            Ddo_sistemaversao_id_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SISTEMAVERSAO_ID_Includesortasc"));
            Ddo_sistemaversao_id_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SISTEMAVERSAO_ID_Includesortdsc"));
            Ddo_sistemaversao_id_Sortedstatus = cgiGet( "DDO_SISTEMAVERSAO_ID_Sortedstatus");
            Ddo_sistemaversao_id_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SISTEMAVERSAO_ID_Includefilter"));
            Ddo_sistemaversao_id_Filtertype = cgiGet( "DDO_SISTEMAVERSAO_ID_Filtertype");
            Ddo_sistemaversao_id_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SISTEMAVERSAO_ID_Filterisrange"));
            Ddo_sistemaversao_id_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SISTEMAVERSAO_ID_Includedatalist"));
            Ddo_sistemaversao_id_Datalisttype = cgiGet( "DDO_SISTEMAVERSAO_ID_Datalisttype");
            Ddo_sistemaversao_id_Datalistproc = cgiGet( "DDO_SISTEMAVERSAO_ID_Datalistproc");
            Ddo_sistemaversao_id_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_SISTEMAVERSAO_ID_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_sistemaversao_id_Sortasc = cgiGet( "DDO_SISTEMAVERSAO_ID_Sortasc");
            Ddo_sistemaversao_id_Sortdsc = cgiGet( "DDO_SISTEMAVERSAO_ID_Sortdsc");
            Ddo_sistemaversao_id_Loadingdata = cgiGet( "DDO_SISTEMAVERSAO_ID_Loadingdata");
            Ddo_sistemaversao_id_Cleanfilter = cgiGet( "DDO_SISTEMAVERSAO_ID_Cleanfilter");
            Ddo_sistemaversao_id_Noresultsfound = cgiGet( "DDO_SISTEMAVERSAO_ID_Noresultsfound");
            Ddo_sistemaversao_id_Searchbuttontext = cgiGet( "DDO_SISTEMAVERSAO_ID_Searchbuttontext");
            Ddo_sistema_ativo_Caption = cgiGet( "DDO_SISTEMA_ATIVO_Caption");
            Ddo_sistema_ativo_Tooltip = cgiGet( "DDO_SISTEMA_ATIVO_Tooltip");
            Ddo_sistema_ativo_Cls = cgiGet( "DDO_SISTEMA_ATIVO_Cls");
            Ddo_sistema_ativo_Selectedvalue_set = cgiGet( "DDO_SISTEMA_ATIVO_Selectedvalue_set");
            Ddo_sistema_ativo_Dropdownoptionstype = cgiGet( "DDO_SISTEMA_ATIVO_Dropdownoptionstype");
            Ddo_sistema_ativo_Titlecontrolidtoreplace = cgiGet( "DDO_SISTEMA_ATIVO_Titlecontrolidtoreplace");
            Ddo_sistema_ativo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SISTEMA_ATIVO_Includesortasc"));
            Ddo_sistema_ativo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SISTEMA_ATIVO_Includesortdsc"));
            Ddo_sistema_ativo_Sortedstatus = cgiGet( "DDO_SISTEMA_ATIVO_Sortedstatus");
            Ddo_sistema_ativo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SISTEMA_ATIVO_Includefilter"));
            Ddo_sistema_ativo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SISTEMA_ATIVO_Includedatalist"));
            Ddo_sistema_ativo_Datalisttype = cgiGet( "DDO_SISTEMA_ATIVO_Datalisttype");
            Ddo_sistema_ativo_Datalistfixedvalues = cgiGet( "DDO_SISTEMA_ATIVO_Datalistfixedvalues");
            Ddo_sistema_ativo_Sortasc = cgiGet( "DDO_SISTEMA_ATIVO_Sortasc");
            Ddo_sistema_ativo_Sortdsc = cgiGet( "DDO_SISTEMA_ATIVO_Sortdsc");
            Ddo_sistema_ativo_Cleanfilter = cgiGet( "DDO_SISTEMA_ATIVO_Cleanfilter");
            Ddo_sistema_ativo_Searchbuttontext = cgiGet( "DDO_SISTEMA_ATIVO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_sistema_sigla_Activeeventkey = cgiGet( "DDO_SISTEMA_SIGLA_Activeeventkey");
            Ddo_sistema_sigla_Filteredtext_get = cgiGet( "DDO_SISTEMA_SIGLA_Filteredtext_get");
            Ddo_sistema_sigla_Selectedvalue_get = cgiGet( "DDO_SISTEMA_SIGLA_Selectedvalue_get");
            Ddo_sistema_tecnica_Activeeventkey = cgiGet( "DDO_SISTEMA_TECNICA_Activeeventkey");
            Ddo_sistema_tecnica_Selectedvalue_get = cgiGet( "DDO_SISTEMA_TECNICA_Selectedvalue_get");
            Ddo_sistema_coordenacao_Activeeventkey = cgiGet( "DDO_SISTEMA_COORDENACAO_Activeeventkey");
            Ddo_sistema_coordenacao_Filteredtext_get = cgiGet( "DDO_SISTEMA_COORDENACAO_Filteredtext_get");
            Ddo_sistema_coordenacao_Selectedvalue_get = cgiGet( "DDO_SISTEMA_COORDENACAO_Selectedvalue_get");
            Ddo_ambientetecnologico_descricao_Activeeventkey = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Activeeventkey");
            Ddo_ambientetecnologico_descricao_Filteredtext_get = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Filteredtext_get");
            Ddo_ambientetecnologico_descricao_Selectedvalue_get = cgiGet( "DDO_AMBIENTETECNOLOGICO_DESCRICAO_Selectedvalue_get");
            Ddo_metodologia_descricao_Activeeventkey = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Activeeventkey");
            Ddo_metodologia_descricao_Filteredtext_get = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Filteredtext_get");
            Ddo_metodologia_descricao_Selectedvalue_get = cgiGet( "DDO_METODOLOGIA_DESCRICAO_Selectedvalue_get");
            Ddo_sistema_projetonome_Activeeventkey = cgiGet( "DDO_SISTEMA_PROJETONOME_Activeeventkey");
            Ddo_sistema_projetonome_Filteredtext_get = cgiGet( "DDO_SISTEMA_PROJETONOME_Filteredtext_get");
            Ddo_sistema_projetonome_Selectedvalue_get = cgiGet( "DDO_SISTEMA_PROJETONOME_Selectedvalue_get");
            Ddo_sistemaversao_id_Activeeventkey = cgiGet( "DDO_SISTEMAVERSAO_ID_Activeeventkey");
            Ddo_sistemaversao_id_Filteredtext_get = cgiGet( "DDO_SISTEMAVERSAO_ID_Filteredtext_get");
            Ddo_sistemaversao_id_Selectedvalue_get = cgiGet( "DDO_SISTEMAVERSAO_ID_Selectedvalue_get");
            Ddo_sistema_ativo_Activeeventkey = cgiGet( "DDO_SISTEMA_ATIVO_Activeeventkey");
            Ddo_sistema_ativo_Selectedvalue_get = cgiGet( "DDO_SISTEMA_ATIVO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSISTEMA_NOME1"), AV67Sistema_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSISTEMA_SIGLA1"), AV62Sistema_Sigla1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSISTEMA_TIPO1"), AV73Sistema_Tipo1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSISTEMA_TECNICA1"), AV75Sistema_Tecnica1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSISTEMA_COORDENACAO1"), AV76Sistema_Coordenacao1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSISTEMA_PROJETONOME1"), AV80Sistema_ProjetoNome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSISTEMA_NOME2"), AV68Sistema_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSISTEMA_SIGLA2"), AV63Sistema_Sigla2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSISTEMA_TIPO2"), AV74Sistema_Tipo2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSISTEMA_TECNICA2"), AV77Sistema_Tecnica2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSISTEMA_COORDENACAO2"), AV78Sistema_Coordenacao2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSISTEMA_PROJETONOME2"), AV81Sistema_ProjetoNome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSISTEMA_SIGLA"), AV88TFSistema_Sigla) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSISTEMA_SIGLA_SEL"), AV89TFSistema_Sigla_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSISTEMA_COORDENACAO"), AV100TFSistema_Coordenacao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSISTEMA_COORDENACAO_SEL"), AV101TFSistema_Coordenacao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFAMBIENTETECNOLOGICO_DESCRICAO"), AV104TFAmbienteTecnologico_Descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL"), AV105TFAmbienteTecnologico_Descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMETODOLOGIA_DESCRICAO"), AV108TFMetodologia_Descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMETODOLOGIA_DESCRICAO_SEL"), AV109TFMetodologia_Descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSISTEMA_PROJETONOME"), AV120TFSistema_ProjetoNome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSISTEMA_PROJETONOME_SEL"), AV121TFSistema_ProjetoNome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSISTEMAVERSAO_ID"), AV140TFSistemaVersao_Id) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSISTEMAVERSAO_ID_SEL"), AV141TFSistemaVersao_Id_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSISTEMA_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV124TFSistema_Ativo_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E303G2 */
         E303G2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E303G2( )
      {
         /* Start Routine */
         Form.Meta.addItem("Versao", "1.0 - Data: 16/03/2020 20:40", 0) ;
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         AV135PreView = 25;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV135PreView", StringUtil.LTrim( StringUtil.Str( (decimal)(AV135PreView), 4, 0)));
         subGrid_Rows = AV135PreView;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         AV73Sistema_Tipo1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73Sistema_Tipo1", AV73Sistema_Tipo1);
         AV75Sistema_Tecnica1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75Sistema_Tecnica1", AV75Sistema_Tecnica1);
         AV15DynamicFiltersSelector1 = "SISTEMA_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV74Sistema_Tipo2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74Sistema_Tipo2", AV74Sistema_Tipo2);
         AV77Sistema_Tecnica2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77Sistema_Tecnica2", AV77Sistema_Tecnica2);
         AV21DynamicFiltersSelector2 = "SISTEMA_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         edtavTfsistema_sigla_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsistema_sigla_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsistema_sigla_Visible), 5, 0)));
         edtavTfsistema_sigla_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsistema_sigla_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsistema_sigla_sel_Visible), 5, 0)));
         edtavTfsistema_coordenacao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsistema_coordenacao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsistema_coordenacao_Visible), 5, 0)));
         edtavTfsistema_coordenacao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsistema_coordenacao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsistema_coordenacao_sel_Visible), 5, 0)));
         edtavTfambientetecnologico_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfambientetecnologico_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfambientetecnologico_descricao_Visible), 5, 0)));
         edtavTfambientetecnologico_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfambientetecnologico_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfambientetecnologico_descricao_sel_Visible), 5, 0)));
         edtavTfmetodologia_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfmetodologia_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfmetodologia_descricao_Visible), 5, 0)));
         edtavTfmetodologia_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfmetodologia_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfmetodologia_descricao_sel_Visible), 5, 0)));
         edtavTfsistema_projetonome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsistema_projetonome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsistema_projetonome_Visible), 5, 0)));
         edtavTfsistema_projetonome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsistema_projetonome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsistema_projetonome_sel_Visible), 5, 0)));
         edtavTfsistemaversao_id_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsistemaversao_id_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsistemaversao_id_Visible), 5, 0)));
         edtavTfsistemaversao_id_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsistemaversao_id_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsistemaversao_id_sel_Visible), 5, 0)));
         edtavTfsistema_ativo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsistema_ativo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsistema_ativo_sel_Visible), 5, 0)));
         Ddo_sistema_sigla_Titlecontrolidtoreplace = subGrid_Internalname+"_Sistema_Sigla";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_sigla_Internalname, "TitleControlIdToReplace", Ddo_sistema_sigla_Titlecontrolidtoreplace);
         AV90ddo_Sistema_SiglaTitleControlIdToReplace = Ddo_sistema_sigla_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90ddo_Sistema_SiglaTitleControlIdToReplace", AV90ddo_Sistema_SiglaTitleControlIdToReplace);
         edtavDdo_sistema_siglatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_sistema_siglatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_sistema_siglatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_sistema_tecnica_Titlecontrolidtoreplace = subGrid_Internalname+"_Sistema_Tecnica";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_tecnica_Internalname, "TitleControlIdToReplace", Ddo_sistema_tecnica_Titlecontrolidtoreplace);
         AV98ddo_Sistema_TecnicaTitleControlIdToReplace = Ddo_sistema_tecnica_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV98ddo_Sistema_TecnicaTitleControlIdToReplace", AV98ddo_Sistema_TecnicaTitleControlIdToReplace);
         edtavDdo_sistema_tecnicatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_sistema_tecnicatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_sistema_tecnicatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_sistema_coordenacao_Titlecontrolidtoreplace = subGrid_Internalname+"_Sistema_Coordenacao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_coordenacao_Internalname, "TitleControlIdToReplace", Ddo_sistema_coordenacao_Titlecontrolidtoreplace);
         AV102ddo_Sistema_CoordenacaoTitleControlIdToReplace = Ddo_sistema_coordenacao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV102ddo_Sistema_CoordenacaoTitleControlIdToReplace", AV102ddo_Sistema_CoordenacaoTitleControlIdToReplace);
         edtavDdo_sistema_coordenacaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_sistema_coordenacaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_sistema_coordenacaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_ambientetecnologico_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_AmbienteTecnologico_Descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_descricao_Internalname, "TitleControlIdToReplace", Ddo_ambientetecnologico_descricao_Titlecontrolidtoreplace);
         AV106ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace = Ddo_ambientetecnologico_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV106ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace", AV106ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace);
         edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_metodologia_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_Metodologia_Descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologia_descricao_Internalname, "TitleControlIdToReplace", Ddo_metodologia_descricao_Titlecontrolidtoreplace);
         AV110ddo_Metodologia_DescricaoTitleControlIdToReplace = Ddo_metodologia_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV110ddo_Metodologia_DescricaoTitleControlIdToReplace", AV110ddo_Metodologia_DescricaoTitleControlIdToReplace);
         edtavDdo_metodologia_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_metodologia_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_metodologia_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_sistema_projetonome_Titlecontrolidtoreplace = subGrid_Internalname+"_Sistema_ProjetoNome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_projetonome_Internalname, "TitleControlIdToReplace", Ddo_sistema_projetonome_Titlecontrolidtoreplace);
         AV122ddo_Sistema_ProjetoNomeTitleControlIdToReplace = Ddo_sistema_projetonome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV122ddo_Sistema_ProjetoNomeTitleControlIdToReplace", AV122ddo_Sistema_ProjetoNomeTitleControlIdToReplace);
         edtavDdo_sistema_projetonometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_sistema_projetonometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_sistema_projetonometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_sistemaversao_id_Titlecontrolidtoreplace = subGrid_Internalname+"_SistemaVersao_Id";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_id_Internalname, "TitleControlIdToReplace", Ddo_sistemaversao_id_Titlecontrolidtoreplace);
         AV142ddo_SistemaVersao_IdTitleControlIdToReplace = Ddo_sistemaversao_id_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV142ddo_SistemaVersao_IdTitleControlIdToReplace", AV142ddo_SistemaVersao_IdTitleControlIdToReplace);
         edtavDdo_sistemaversao_idtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_sistemaversao_idtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_sistemaversao_idtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_sistema_ativo_Titlecontrolidtoreplace = subGrid_Internalname+"_Sistema_Ativo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_ativo_Internalname, "TitleControlIdToReplace", Ddo_sistema_ativo_Titlecontrolidtoreplace);
         AV125ddo_Sistema_AtivoTitleControlIdToReplace = Ddo_sistema_ativo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV125ddo_Sistema_AtivoTitleControlIdToReplace", AV125ddo_Sistema_AtivoTitleControlIdToReplace);
         edtavDdo_sistema_ativotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_sistema_ativotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_sistema_ativotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Sistema";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Nome", 0);
         cmbavOrderedby.addItem("2", "Sigla", 0);
         cmbavOrderedby.addItem("3", "T�cnica", 0);
         cmbavOrderedby.addItem("4", "Coordena��o", 0);
         cmbavOrderedby.addItem("5", "Amb.Tec.", 0);
         cmbavOrderedby.addItem("6", "Met.", 0);
         cmbavOrderedby.addItem("7", "Projeto", 0);
         cmbavOrderedby.addItem("8", "Vers�o", 0);
         cmbavOrderedby.addItem("9", "Ativo", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons2 = AV126DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons2) ;
         AV126DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons2;
         /* Execute user subroutine: 'PRECONSULTA' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV136PaginaAtual = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV136PaginaAtual", StringUtil.LTrim( StringUtil.Str( (decimal)(AV136PaginaAtual), 10, 0)));
      }

      protected void E313G2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV87Sistema_SiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV95Sistema_TecnicaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV99Sistema_CoordenacaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV103AmbienteTecnologico_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV107Metodologia_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV119Sistema_ProjetoNomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV139SistemaVersao_IdTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV123Sistema_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtSistema_Sigla_Titleformat = 2;
         edtSistema_Sigla_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Sigla", AV90ddo_Sistema_SiglaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistema_Sigla_Internalname, "Title", edtSistema_Sigla_Title);
         cmbSistema_Tecnica_Titleformat = 2;
         cmbSistema_Tecnica.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "T�cnica", AV98ddo_Sistema_TecnicaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbSistema_Tecnica_Internalname, "Title", cmbSistema_Tecnica.Title.Text);
         edtSistema_Coordenacao_Titleformat = 2;
         edtSistema_Coordenacao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Coordena��o", AV102ddo_Sistema_CoordenacaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistema_Coordenacao_Internalname, "Title", edtSistema_Coordenacao_Title);
         edtAmbienteTecnologico_Descricao_Titleformat = 2;
         edtAmbienteTecnologico_Descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Amb.Tec.", AV106ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAmbienteTecnologico_Descricao_Internalname, "Title", edtAmbienteTecnologico_Descricao_Title);
         edtMetodologia_Descricao_Titleformat = 2;
         edtMetodologia_Descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Met.", AV110ddo_Metodologia_DescricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMetodologia_Descricao_Internalname, "Title", edtMetodologia_Descricao_Title);
         edtSistema_ProjetoNome_Titleformat = 2;
         edtSistema_ProjetoNome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Projeto", AV122ddo_Sistema_ProjetoNomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistema_ProjetoNome_Internalname, "Title", edtSistema_ProjetoNome_Title);
         edtSistemaVersao_Id_Titleformat = 2;
         edtSistemaVersao_Id_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Vers�o", AV142ddo_SistemaVersao_IdTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistemaVersao_Id_Internalname, "Title", edtSistemaVersao_Id_Title);
         chkSistema_Ativo_Titleformat = 2;
         chkSistema_Ativo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ativo", AV125ddo_Sistema_AtivoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkSistema_Ativo_Internalname, "Title", chkSistema_Ativo.Title.Text);
         AV128GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV128GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV128GridCurrentPage), 10, 0)));
         AV129GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV129GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV129GridPageCount), 10, 0)));
         if ( AV137Paginando )
         {
            AV137Paginando = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV137Paginando", AV137Paginando);
         }
         else
         {
            /* Execute user subroutine: 'PRECONSULTA' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV136PaginaAtual = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV136PaginaAtual", StringUtil.LTrim( StringUtil.Str( (decimal)(AV136PaginaAtual), 10, 0)));
         }
         GXt_int3 = AV129GridPageCount;
         new prc_pagecount(context ).execute(  AV134Registros, ref  AV135PreView, out  GXt_int3) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV134Registros", StringUtil.LTrim( StringUtil.Str( (decimal)(AV134Registros), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV135PreView", StringUtil.LTrim( StringUtil.Str( (decimal)(AV135PreView), 4, 0)));
         AV129GridPageCount = GXt_int3;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV129GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV129GridPageCount), 10, 0)));
         AV128GridCurrentPage = AV136PaginaAtual;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV128GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV128GridCurrentPage), 10, 0)));
         edtavAssociarclientes_Visible = ((AV6WWPContext.gxTpr_Contratante_codigo>0) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAssociarclientes_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAssociarclientes_Visible), 5, 0)));
         edtavAssociarclientes_Title = "Usu�rios";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAssociarclientes_Internalname, "Title", edtavAssociarclientes_Title);
         imgInsert_Visible = (AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Visible), 5, 0)));
         edtavUpdate_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUpdate_Visible), 5, 0)));
         edtavDelete_Visible = (AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDelete_Visible), 5, 0)));
         edtavDisplay_Visible = (AV6WWPContext.gxTpr_Display ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDisplay_Visible), 5, 0)));
         AV145WWSistemaDS_1_Sistema_areatrabalhocod = AV60Sistema_AreaTrabalhoCod;
         AV146WWSistemaDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV147WWSistemaDS_3_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV148WWSistemaDS_4_Sistema_nome1 = AV67Sistema_Nome1;
         AV149WWSistemaDS_5_Sistema_sigla1 = AV62Sistema_Sigla1;
         AV150WWSistemaDS_6_Sistema_tipo1 = AV73Sistema_Tipo1;
         AV151WWSistemaDS_7_Sistema_tecnica1 = AV75Sistema_Tecnica1;
         AV152WWSistemaDS_8_Sistema_coordenacao1 = AV76Sistema_Coordenacao1;
         AV153WWSistemaDS_9_Sistema_projetonome1 = AV80Sistema_ProjetoNome1;
         AV154WWSistemaDS_10_Sistema_pf1 = AV82Sistema_PF1;
         AV155WWSistemaDS_11_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV156WWSistemaDS_12_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV157WWSistemaDS_13_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV158WWSistemaDS_14_Sistema_nome2 = AV68Sistema_Nome2;
         AV159WWSistemaDS_15_Sistema_sigla2 = AV63Sistema_Sigla2;
         AV160WWSistemaDS_16_Sistema_tipo2 = AV74Sistema_Tipo2;
         AV161WWSistemaDS_17_Sistema_tecnica2 = AV77Sistema_Tecnica2;
         AV162WWSistemaDS_18_Sistema_coordenacao2 = AV78Sistema_Coordenacao2;
         AV163WWSistemaDS_19_Sistema_projetonome2 = AV81Sistema_ProjetoNome2;
         AV164WWSistemaDS_20_Sistema_pf2 = AV83Sistema_PF2;
         AV165WWSistemaDS_21_Tfsistema_sigla = AV88TFSistema_Sigla;
         AV166WWSistemaDS_22_Tfsistema_sigla_sel = AV89TFSistema_Sigla_Sel;
         AV167WWSistemaDS_23_Tfsistema_tecnica_sels = AV97TFSistema_Tecnica_Sels;
         AV168WWSistemaDS_24_Tfsistema_coordenacao = AV100TFSistema_Coordenacao;
         AV169WWSistemaDS_25_Tfsistema_coordenacao_sel = AV101TFSistema_Coordenacao_Sel;
         AV170WWSistemaDS_26_Tfambientetecnologico_descricao = AV104TFAmbienteTecnologico_Descricao;
         AV171WWSistemaDS_27_Tfambientetecnologico_descricao_sel = AV105TFAmbienteTecnologico_Descricao_Sel;
         AV172WWSistemaDS_28_Tfmetodologia_descricao = AV108TFMetodologia_Descricao;
         AV173WWSistemaDS_29_Tfmetodologia_descricao_sel = AV109TFMetodologia_Descricao_Sel;
         AV174WWSistemaDS_30_Tfsistema_projetonome = AV120TFSistema_ProjetoNome;
         AV175WWSistemaDS_31_Tfsistema_projetonome_sel = AV121TFSistema_ProjetoNome_Sel;
         AV176WWSistemaDS_32_Tfsistemaversao_id = AV140TFSistemaVersao_Id;
         AV177WWSistemaDS_33_Tfsistemaversao_id_sel = AV141TFSistemaVersao_Id_Sel;
         AV178WWSistemaDS_34_Tfsistema_ativo_sel = AV124TFSistema_Ativo_Sel;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV87Sistema_SiglaTitleFilterData", AV87Sistema_SiglaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV95Sistema_TecnicaTitleFilterData", AV95Sistema_TecnicaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV99Sistema_CoordenacaoTitleFilterData", AV99Sistema_CoordenacaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV103AmbienteTecnologico_DescricaoTitleFilterData", AV103AmbienteTecnologico_DescricaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV107Metodologia_DescricaoTitleFilterData", AV107Metodologia_DescricaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV119Sistema_ProjetoNomeTitleFilterData", AV119Sistema_ProjetoNomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV139SistemaVersao_IdTitleFilterData", AV139SistemaVersao_IdTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV123Sistema_AtivoTitleFilterData", AV123Sistema_AtivoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E113G2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         GXt_int3 = AV136PaginaAtual;
         new prc_currentpage(context ).execute(  Gridpaginationbar_Selectedpage, out  GXt_int3) ;
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Gridpaginationbar_Internalname, "SelectedPage", Gridpaginationbar_Selectedpage);
         AV136PaginaAtual = GXt_int3;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV136PaginaAtual", StringUtil.LTrim( StringUtil.Str( (decimal)(AV136PaginaAtual), 10, 0)));
         AV137Paginando = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV137Paginando", AV137Paginando);
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV127PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV127PageToGo) ;
         }
      }

      protected void E123G2( )
      {
         /* Ddo_sistema_sigla_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_sistema_sigla_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_sistema_sigla_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_sigla_Internalname, "SortedStatus", Ddo_sistema_sigla_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_sistema_sigla_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_sistema_sigla_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_sigla_Internalname, "SortedStatus", Ddo_sistema_sigla_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_sistema_sigla_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV88TFSistema_Sigla = Ddo_sistema_sigla_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88TFSistema_Sigla", AV88TFSistema_Sigla);
            AV89TFSistema_Sigla_Sel = Ddo_sistema_sigla_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89TFSistema_Sigla_Sel", AV89TFSistema_Sigla_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E133G2( )
      {
         /* Ddo_sistema_tecnica_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_sistema_tecnica_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_sistema_tecnica_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_tecnica_Internalname, "SortedStatus", Ddo_sistema_tecnica_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_sistema_tecnica_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_sistema_tecnica_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_tecnica_Internalname, "SortedStatus", Ddo_sistema_tecnica_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_sistema_tecnica_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV96TFSistema_Tecnica_SelsJson = Ddo_sistema_tecnica_Selectedvalue_get;
            AV97TFSistema_Tecnica_Sels.FromJSonString(AV96TFSistema_Tecnica_SelsJson);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV97TFSistema_Tecnica_Sels", AV97TFSistema_Tecnica_Sels);
      }

      protected void E143G2( )
      {
         /* Ddo_sistema_coordenacao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_sistema_coordenacao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_sistema_coordenacao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_coordenacao_Internalname, "SortedStatus", Ddo_sistema_coordenacao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_sistema_coordenacao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_sistema_coordenacao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_coordenacao_Internalname, "SortedStatus", Ddo_sistema_coordenacao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_sistema_coordenacao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV100TFSistema_Coordenacao = Ddo_sistema_coordenacao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV100TFSistema_Coordenacao", AV100TFSistema_Coordenacao);
            AV101TFSistema_Coordenacao_Sel = Ddo_sistema_coordenacao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV101TFSistema_Coordenacao_Sel", AV101TFSistema_Coordenacao_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E153G2( )
      {
         /* Ddo_ambientetecnologico_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_ambientetecnologico_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_ambientetecnologico_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_descricao_Internalname, "SortedStatus", Ddo_ambientetecnologico_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_ambientetecnologico_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_ambientetecnologico_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_descricao_Internalname, "SortedStatus", Ddo_ambientetecnologico_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_ambientetecnologico_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV104TFAmbienteTecnologico_Descricao = Ddo_ambientetecnologico_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV104TFAmbienteTecnologico_Descricao", AV104TFAmbienteTecnologico_Descricao);
            AV105TFAmbienteTecnologico_Descricao_Sel = Ddo_ambientetecnologico_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105TFAmbienteTecnologico_Descricao_Sel", AV105TFAmbienteTecnologico_Descricao_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E163G2( )
      {
         /* Ddo_metodologia_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_metodologia_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_metodologia_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologia_descricao_Internalname, "SortedStatus", Ddo_metodologia_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_metodologia_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_metodologia_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologia_descricao_Internalname, "SortedStatus", Ddo_metodologia_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_metodologia_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV108TFMetodologia_Descricao = Ddo_metodologia_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV108TFMetodologia_Descricao", AV108TFMetodologia_Descricao);
            AV109TFMetodologia_Descricao_Sel = Ddo_metodologia_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV109TFMetodologia_Descricao_Sel", AV109TFMetodologia_Descricao_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E173G2( )
      {
         /* Ddo_sistema_projetonome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_sistema_projetonome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_sistema_projetonome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_projetonome_Internalname, "SortedStatus", Ddo_sistema_projetonome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_sistema_projetonome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_sistema_projetonome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_projetonome_Internalname, "SortedStatus", Ddo_sistema_projetonome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_sistema_projetonome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV120TFSistema_ProjetoNome = Ddo_sistema_projetonome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV120TFSistema_ProjetoNome", AV120TFSistema_ProjetoNome);
            AV121TFSistema_ProjetoNome_Sel = Ddo_sistema_projetonome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV121TFSistema_ProjetoNome_Sel", AV121TFSistema_ProjetoNome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E183G2( )
      {
         /* Ddo_sistemaversao_id_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_sistemaversao_id_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_sistemaversao_id_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_id_Internalname, "SortedStatus", Ddo_sistemaversao_id_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_sistemaversao_id_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_sistemaversao_id_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_id_Internalname, "SortedStatus", Ddo_sistemaversao_id_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_sistemaversao_id_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV140TFSistemaVersao_Id = Ddo_sistemaversao_id_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV140TFSistemaVersao_Id", AV140TFSistemaVersao_Id);
            AV141TFSistemaVersao_Id_Sel = Ddo_sistemaversao_id_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV141TFSistemaVersao_Id_Sel", AV141TFSistemaVersao_Id_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E193G2( )
      {
         /* Ddo_sistema_ativo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_sistema_ativo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 9;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_sistema_ativo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_ativo_Internalname, "SortedStatus", Ddo_sistema_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_sistema_ativo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 9;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_sistema_ativo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_ativo_Internalname, "SortedStatus", Ddo_sistema_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_sistema_ativo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV124TFSistema_Ativo_Sel = (short)(NumberUtil.Val( Ddo_sistema_ativo_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV124TFSistema_Ativo_Sel", StringUtil.Str( (decimal)(AV124TFSistema_Ativo_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E323G2( )
      {
         /* Grid_Load Routine */
         AV34Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV34Update);
         AV179Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         AV35Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV35Delete);
         AV180Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         AV58Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV58Display);
         AV181Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
         edtavDisplay_Tooltiptext = "Mostrar";
         AV34Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV34Update);
         AV179Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("sistema.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A127Sistema_Codigo);
         AV35Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV35Delete);
         AV180Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("sistema.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A127Sistema_Codigo);
         AV58Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV58Display);
         AV181Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = formatLink("viewsistema.aspx") + "?" + UrlEncode("" +A127Sistema_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         AV84AssociarClientes = context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAssociarclientes_Internalname, AV84AssociarClientes);
         AV182Associarclientes_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( )));
         edtavAssociarclientes_Tooltiptext = "Associar os usu�riosa da contratante clientes deste sistema";
         edtAmbienteTecnologico_Descricao_Link = formatLink("viewambientetecnologico.aspx") + "?" + UrlEncode("" +A351AmbienteTecnologico_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         edtMetodologia_Descricao_Link = formatLink("viewmetodologia.aspx") + "?" + UrlEncode("" +A137Metodologia_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         edtSistema_ProjetoNome_Link = formatLink("viewprojeto.aspx") + "?" + UrlEncode("" +A691Sistema_ProjetoCod) + "," + UrlEncode(StringUtil.RTrim(""));
         edtSistemaVersao_Id_Link = formatLink("viewsistemaversao.aspx") + "?" + UrlEncode("" +A1859SistemaVersao_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         if ( A130Sistema_Ativo )
         {
            AV138Color = GXUtil.RGB( 0, 0, 0);
         }
         else
         {
            AV138Color = GXUtil.RGB( 255, 0, 0);
         }
         edtSistema_Sigla_Forecolor = (int)(AV138Color);
         edtavSistema_tiposigla_Forecolor = (int)(AV138Color);
         cmbSistema_Tecnica.ForeColor = (int)(AV138Color);
         edtSistema_Coordenacao_Forecolor = (int)(AV138Color);
         edtAmbienteTecnologico_Descricao_Forecolor = (int)(AV138Color);
         edtMetodologia_Descricao_Forecolor = (int)(AV138Color);
         edtavPfb_Forecolor = (int)(AV138Color);
         edtavPfa_Forecolor = (int)(AV138Color);
         edtSistema_ProjetoNome_Forecolor = (int)(AV138Color);
         AV130Sistema_TipoSigla = A707Sistema_TipoSigla;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSistema_tiposigla_Internalname, AV130Sistema_TipoSigla);
         AV131PFB = A395Sistema_PF;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfb_Internalname, StringUtil.LTrim( StringUtil.Str( AV131PFB, 14, 5)));
         AV133PFA = A690Sistema_PFA;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfa_Internalname, StringUtil.LTrim( StringUtil.Str( AV133PFA, 14, 5)));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 89;
         }
         if ( ( subGrid_Islastpage == 1 ) || ( subGrid_Rows == 0 ) || ( ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage ) && ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) )
         {
            sendrow_892( ) ;
            GRID_nEOF = 1;
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            if ( ( subGrid_Islastpage == 1 ) && ( ((int)((GRID_nCurrentRecord) % (subGrid_Recordsperpage( )))) == 0 ) )
            {
               GRID_nFirstRecordOnPage = GRID_nCurrentRecord;
            }
         }
         if ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) )
         {
            GRID_nEOF = 0;
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
         }
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_89_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(89, GridRow);
         }
      }

      protected void E203G2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E263G2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV20DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E213G2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV32DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         AV33DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33DynamicFiltersIgnoreFirst", AV33DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV32DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         AV33DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33DynamicFiltersIgnoreFirst", AV33DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV67Sistema_Nome1, AV62Sistema_Sigla1, AV73Sistema_Tipo1, AV75Sistema_Tecnica1, AV76Sistema_Coordenacao1, AV80Sistema_ProjetoNome1, AV21DynamicFiltersSelector2, AV68Sistema_Nome2, AV63Sistema_Sigla2, AV74Sistema_Tipo2, AV77Sistema_Tecnica2, AV78Sistema_Coordenacao2, AV81Sistema_ProjetoNome2, AV20DynamicFiltersEnabled2, AV88TFSistema_Sigla, AV89TFSistema_Sigla_Sel, AV100TFSistema_Coordenacao, AV101TFSistema_Coordenacao_Sel, AV104TFAmbienteTecnologico_Descricao, AV105TFAmbienteTecnologico_Descricao_Sel, AV108TFMetodologia_Descricao, AV109TFMetodologia_Descricao_Sel, AV120TFSistema_ProjetoNome, AV121TFSistema_ProjetoNome_Sel, AV140TFSistemaVersao_Id, AV141TFSistemaVersao_Id_Sel, AV124TFSistema_Ativo_Sel, AV6WWPContext, AV90ddo_Sistema_SiglaTitleControlIdToReplace, AV98ddo_Sistema_TecnicaTitleControlIdToReplace, AV102ddo_Sistema_CoordenacaoTitleControlIdToReplace, AV106ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, AV110ddo_Metodologia_DescricaoTitleControlIdToReplace, AV122ddo_Sistema_ProjetoNomeTitleControlIdToReplace, AV142ddo_SistemaVersao_IdTitleControlIdToReplace, AV125ddo_Sistema_AtivoTitleControlIdToReplace, AV137Paginando, AV134Registros, AV135PreView, AV136PaginaAtual, AV60Sistema_AreaTrabalhoCod, AV16DynamicFiltersOperator1, AV82Sistema_PF1, AV22DynamicFiltersOperator2, AV83Sistema_PF2, AV97TFSistema_Tecnica_Sels, AV183Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A127Sistema_Codigo, A351AmbienteTecnologico_Codigo, A137Metodologia_Codigo, A691Sistema_ProjetoCod, A1859SistemaVersao_Codigo, A130Sistema_Ativo, A707Sistema_TipoSigla, A395Sistema_PF, A690Sistema_PFA) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavSistema_tecnica1.CurrentValue = StringUtil.RTrim( AV75Sistema_Tecnica1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_tecnica1_Internalname, "Values", cmbavSistema_tecnica1.ToJavascriptSource());
         cmbavSistema_tipo1.CurrentValue = StringUtil.RTrim( AV73Sistema_Tipo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_tipo1_Internalname, "Values", cmbavSistema_tipo1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavSistema_tecnica2.CurrentValue = StringUtil.RTrim( AV77Sistema_Tecnica2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_tecnica2_Internalname, "Values", cmbavSistema_tecnica2.ToJavascriptSource());
         cmbavSistema_tipo2.CurrentValue = StringUtil.RTrim( AV74Sistema_Tipo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_tipo2_Internalname, "Values", cmbavSistema_tipo2.ToJavascriptSource());
      }

      protected void E273G2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E223G2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV32DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV32DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV67Sistema_Nome1, AV62Sistema_Sigla1, AV73Sistema_Tipo1, AV75Sistema_Tecnica1, AV76Sistema_Coordenacao1, AV80Sistema_ProjetoNome1, AV21DynamicFiltersSelector2, AV68Sistema_Nome2, AV63Sistema_Sigla2, AV74Sistema_Tipo2, AV77Sistema_Tecnica2, AV78Sistema_Coordenacao2, AV81Sistema_ProjetoNome2, AV20DynamicFiltersEnabled2, AV88TFSistema_Sigla, AV89TFSistema_Sigla_Sel, AV100TFSistema_Coordenacao, AV101TFSistema_Coordenacao_Sel, AV104TFAmbienteTecnologico_Descricao, AV105TFAmbienteTecnologico_Descricao_Sel, AV108TFMetodologia_Descricao, AV109TFMetodologia_Descricao_Sel, AV120TFSistema_ProjetoNome, AV121TFSistema_ProjetoNome_Sel, AV140TFSistemaVersao_Id, AV141TFSistemaVersao_Id_Sel, AV124TFSistema_Ativo_Sel, AV6WWPContext, AV90ddo_Sistema_SiglaTitleControlIdToReplace, AV98ddo_Sistema_TecnicaTitleControlIdToReplace, AV102ddo_Sistema_CoordenacaoTitleControlIdToReplace, AV106ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, AV110ddo_Metodologia_DescricaoTitleControlIdToReplace, AV122ddo_Sistema_ProjetoNomeTitleControlIdToReplace, AV142ddo_SistemaVersao_IdTitleControlIdToReplace, AV125ddo_Sistema_AtivoTitleControlIdToReplace, AV137Paginando, AV134Registros, AV135PreView, AV136PaginaAtual, AV60Sistema_AreaTrabalhoCod, AV16DynamicFiltersOperator1, AV82Sistema_PF1, AV22DynamicFiltersOperator2, AV83Sistema_PF2, AV97TFSistema_Tecnica_Sels, AV183Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A127Sistema_Codigo, A351AmbienteTecnologico_Codigo, A137Metodologia_Codigo, A691Sistema_ProjetoCod, A1859SistemaVersao_Codigo, A130Sistema_Ativo, A707Sistema_TipoSigla, A395Sistema_PF, A690Sistema_PFA) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavSistema_tecnica1.CurrentValue = StringUtil.RTrim( AV75Sistema_Tecnica1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_tecnica1_Internalname, "Values", cmbavSistema_tecnica1.ToJavascriptSource());
         cmbavSistema_tipo1.CurrentValue = StringUtil.RTrim( AV73Sistema_Tipo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_tipo1_Internalname, "Values", cmbavSistema_tipo1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavSistema_tecnica2.CurrentValue = StringUtil.RTrim( AV77Sistema_Tecnica2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_tecnica2_Internalname, "Values", cmbavSistema_tecnica2.ToJavascriptSource());
         cmbavSistema_tipo2.CurrentValue = StringUtil.RTrim( AV74Sistema_Tipo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_tipo2_Internalname, "Values", cmbavSistema_tipo2.ToJavascriptSource());
      }

      protected void E283G2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E233G2( )
      {
         /* 'DoCleanFilters' Routine */
         AV136PaginaAtual = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV136PaginaAtual", StringUtil.LTrim( StringUtil.Str( (decimal)(AV136PaginaAtual), 10, 0)));
         /* Execute user subroutine: 'CLEANFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV97TFSistema_Tecnica_Sels", AV97TFSistema_Tecnica_Sels);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavSistema_tecnica1.CurrentValue = StringUtil.RTrim( AV75Sistema_Tecnica1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_tecnica1_Internalname, "Values", cmbavSistema_tecnica1.ToJavascriptSource());
         cmbavSistema_tipo1.CurrentValue = StringUtil.RTrim( AV73Sistema_Tipo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_tipo1_Internalname, "Values", cmbavSistema_tipo1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavSistema_tecnica2.CurrentValue = StringUtil.RTrim( AV77Sistema_Tecnica2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_tecnica2_Internalname, "Values", cmbavSistema_tecnica2.ToJavascriptSource());
         cmbavSistema_tipo2.CurrentValue = StringUtil.RTrim( AV74Sistema_Tipo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_tipo2_Internalname, "Values", cmbavSistema_tipo2.ToJavascriptSource());
      }

      protected void E243G2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("sistema.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void E253G2( )
      {
         /* 'DoExportReport' Routine */
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV96TFSistema_Tecnica_SelsJson = Ddo_sistema_tecnica_Selectedvalue_get;
         Innewwindow1_Target = formatLink("exportreportwwsistema.aspx") + "?" + UrlEncode("" +AV60Sistema_AreaTrabalhoCod) + "," + UrlEncode(StringUtil.RTrim(AV88TFSistema_Sigla)) + "," + UrlEncode(StringUtil.RTrim(AV89TFSistema_Sigla_Sel)) + "," + UrlEncode(StringUtil.RTrim(AV96TFSistema_Tecnica_SelsJson)) + "," + UrlEncode(StringUtil.RTrim(AV100TFSistema_Coordenacao)) + "," + UrlEncode(StringUtil.RTrim(AV101TFSistema_Coordenacao_Sel)) + "," + UrlEncode(StringUtil.RTrim(AV104TFAmbienteTecnologico_Descricao)) + "," + UrlEncode(StringUtil.RTrim(AV105TFAmbienteTecnologico_Descricao_Sel)) + "," + UrlEncode(StringUtil.RTrim(AV108TFMetodologia_Descricao)) + "," + UrlEncode(StringUtil.RTrim(AV109TFMetodologia_Descricao_Sel)) + "," + UrlEncode(StringUtil.RTrim(AV120TFSistema_ProjetoNome)) + "," + UrlEncode(StringUtil.RTrim(AV121TFSistema_ProjetoNome_Sel)) + "," + UrlEncode(StringUtil.RTrim(AV140TFSistemaVersao_Id)) + "," + UrlEncode(StringUtil.RTrim(AV141TFSistemaVersao_Id_Sel)) + "," + UrlEncode("" +AV124TFSistema_Ativo_Sel) + "," + UrlEncode("" +AV13OrderedBy) + "," + UrlEncode(StringUtil.BoolToStr(AV14OrderedDsc)) + "," + UrlEncode(StringUtil.RTrim(AV10GridState.gxTpr_Dynamicfilters.ToXml(false, true, "Collection", "GxEv3Up14_Meetrika")));
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Innewwindow1_Internalname, "Target", Innewwindow1_Target);
         Innewwindow1_Height = "600";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Innewwindow1_Internalname, "Height", Innewwindow1_Height);
         Innewwindow1_Width = "800";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Innewwindow1_Internalname, "Width", Innewwindow1_Width);
         this.executeUsercontrolMethod("", false, "INNEWWINDOW1Container", "OpenWindow", "", new Object[] {});
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void S182( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_sistema_sigla_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_sigla_Internalname, "SortedStatus", Ddo_sistema_sigla_Sortedstatus);
         Ddo_sistema_tecnica_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_tecnica_Internalname, "SortedStatus", Ddo_sistema_tecnica_Sortedstatus);
         Ddo_sistema_coordenacao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_coordenacao_Internalname, "SortedStatus", Ddo_sistema_coordenacao_Sortedstatus);
         Ddo_ambientetecnologico_descricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_descricao_Internalname, "SortedStatus", Ddo_ambientetecnologico_descricao_Sortedstatus);
         Ddo_metodologia_descricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologia_descricao_Internalname, "SortedStatus", Ddo_metodologia_descricao_Sortedstatus);
         Ddo_sistema_projetonome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_projetonome_Internalname, "SortedStatus", Ddo_sistema_projetonome_Sortedstatus);
         Ddo_sistemaversao_id_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_id_Internalname, "SortedStatus", Ddo_sistemaversao_id_Sortedstatus);
         Ddo_sistema_ativo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_ativo_Internalname, "SortedStatus", Ddo_sistema_ativo_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_sistema_sigla_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_sigla_Internalname, "SortedStatus", Ddo_sistema_sigla_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_sistema_tecnica_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_tecnica_Internalname, "SortedStatus", Ddo_sistema_tecnica_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_sistema_coordenacao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_coordenacao_Internalname, "SortedStatus", Ddo_sistema_coordenacao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_ambientetecnologico_descricao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_descricao_Internalname, "SortedStatus", Ddo_ambientetecnologico_descricao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 6 )
         {
            Ddo_metodologia_descricao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologia_descricao_Internalname, "SortedStatus", Ddo_metodologia_descricao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 7 )
         {
            Ddo_sistema_projetonome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_projetonome_Internalname, "SortedStatus", Ddo_sistema_projetonome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 8 )
         {
            Ddo_sistemaversao_id_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_id_Internalname, "SortedStatus", Ddo_sistemaversao_id_Sortedstatus);
         }
         else if ( AV13OrderedBy == 9 )
         {
            Ddo_sistema_ativo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_ativo_Internalname, "SortedStatus", Ddo_sistema_ativo_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavSistema_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistema_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_nome1_Visible), 5, 0)));
         edtavSistema_sigla1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistema_sigla1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_sigla1_Visible), 5, 0)));
         cmbavSistema_tipo1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_tipo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavSistema_tipo1.Visible), 5, 0)));
         cmbavSistema_tecnica1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_tecnica1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavSistema_tecnica1.Visible), 5, 0)));
         edtavSistema_coordenacao1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistema_coordenacao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_coordenacao1_Visible), 5, 0)));
         edtavSistema_projetonome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistema_projetonome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_projetonome1_Visible), 5, 0)));
         edtavSistema_pf1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistema_pf1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_pf1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SISTEMA_NOME") == 0 )
         {
            edtavSistema_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistema_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_nome1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SISTEMA_SIGLA") == 0 )
         {
            edtavSistema_sigla1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistema_sigla1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_sigla1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SISTEMA_TIPO") == 0 )
         {
            cmbavSistema_tipo1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_tipo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavSistema_tipo1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SISTEMA_TECNICA") == 0 )
         {
            cmbavSistema_tecnica1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_tecnica1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavSistema_tecnica1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SISTEMA_COORDENACAO") == 0 )
         {
            edtavSistema_coordenacao1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistema_coordenacao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_coordenacao1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SISTEMA_PROJETONOME") == 0 )
         {
            edtavSistema_projetonome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistema_projetonome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_projetonome1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SISTEMA_PF") == 0 )
         {
            edtavSistema_pf1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistema_pf1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_pf1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavSistema_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistema_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_nome2_Visible), 5, 0)));
         edtavSistema_sigla2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistema_sigla2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_sigla2_Visible), 5, 0)));
         cmbavSistema_tipo2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_tipo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavSistema_tipo2.Visible), 5, 0)));
         cmbavSistema_tecnica2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_tecnica2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavSistema_tecnica2.Visible), 5, 0)));
         edtavSistema_coordenacao2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistema_coordenacao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_coordenacao2_Visible), 5, 0)));
         edtavSistema_projetonome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistema_projetonome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_projetonome2_Visible), 5, 0)));
         edtavSistema_pf2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistema_pf2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_pf2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "SISTEMA_NOME") == 0 )
         {
            edtavSistema_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistema_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_nome2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "SISTEMA_SIGLA") == 0 )
         {
            edtavSistema_sigla2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistema_sigla2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_sigla2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "SISTEMA_TIPO") == 0 )
         {
            cmbavSistema_tipo2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_tipo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavSistema_tipo2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "SISTEMA_TECNICA") == 0 )
         {
            cmbavSistema_tecnica2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_tecnica2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavSistema_tecnica2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "SISTEMA_COORDENACAO") == 0 )
         {
            edtavSistema_coordenacao2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistema_coordenacao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_coordenacao2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "SISTEMA_PROJETONOME") == 0 )
         {
            edtavSistema_projetonome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistema_projetonome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_projetonome2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "SISTEMA_PF") == 0 )
         {
            edtavSistema_pf2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistema_pf2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_pf2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S202( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         AV21DynamicFiltersSelector2 = "SISTEMA_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         AV68Sistema_Nome2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68Sistema_Nome2", AV68Sistema_Nome2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S222( )
      {
         /* 'CLEANFILTERS' Routine */
         AV60Sistema_AreaTrabalhoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60Sistema_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60Sistema_AreaTrabalhoCod), 6, 0)));
         AV88TFSistema_Sigla = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88TFSistema_Sigla", AV88TFSistema_Sigla);
         Ddo_sistema_sigla_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_sigla_Internalname, "FilteredText_set", Ddo_sistema_sigla_Filteredtext_set);
         AV89TFSistema_Sigla_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89TFSistema_Sigla_Sel", AV89TFSistema_Sigla_Sel);
         Ddo_sistema_sigla_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_sigla_Internalname, "SelectedValue_set", Ddo_sistema_sigla_Selectedvalue_set);
         AV97TFSistema_Tecnica_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_sistema_tecnica_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_tecnica_Internalname, "SelectedValue_set", Ddo_sistema_tecnica_Selectedvalue_set);
         AV100TFSistema_Coordenacao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV100TFSistema_Coordenacao", AV100TFSistema_Coordenacao);
         Ddo_sistema_coordenacao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_coordenacao_Internalname, "FilteredText_set", Ddo_sistema_coordenacao_Filteredtext_set);
         AV101TFSistema_Coordenacao_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV101TFSistema_Coordenacao_Sel", AV101TFSistema_Coordenacao_Sel);
         Ddo_sistema_coordenacao_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_coordenacao_Internalname, "SelectedValue_set", Ddo_sistema_coordenacao_Selectedvalue_set);
         AV104TFAmbienteTecnologico_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV104TFAmbienteTecnologico_Descricao", AV104TFAmbienteTecnologico_Descricao);
         Ddo_ambientetecnologico_descricao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_descricao_Internalname, "FilteredText_set", Ddo_ambientetecnologico_descricao_Filteredtext_set);
         AV105TFAmbienteTecnologico_Descricao_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105TFAmbienteTecnologico_Descricao_Sel", AV105TFAmbienteTecnologico_Descricao_Sel);
         Ddo_ambientetecnologico_descricao_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_descricao_Internalname, "SelectedValue_set", Ddo_ambientetecnologico_descricao_Selectedvalue_set);
         AV108TFMetodologia_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV108TFMetodologia_Descricao", AV108TFMetodologia_Descricao);
         Ddo_metodologia_descricao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologia_descricao_Internalname, "FilteredText_set", Ddo_metodologia_descricao_Filteredtext_set);
         AV109TFMetodologia_Descricao_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV109TFMetodologia_Descricao_Sel", AV109TFMetodologia_Descricao_Sel);
         Ddo_metodologia_descricao_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologia_descricao_Internalname, "SelectedValue_set", Ddo_metodologia_descricao_Selectedvalue_set);
         AV120TFSistema_ProjetoNome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV120TFSistema_ProjetoNome", AV120TFSistema_ProjetoNome);
         Ddo_sistema_projetonome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_projetonome_Internalname, "FilteredText_set", Ddo_sistema_projetonome_Filteredtext_set);
         AV121TFSistema_ProjetoNome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV121TFSistema_ProjetoNome_Sel", AV121TFSistema_ProjetoNome_Sel);
         Ddo_sistema_projetonome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_projetonome_Internalname, "SelectedValue_set", Ddo_sistema_projetonome_Selectedvalue_set);
         AV140TFSistemaVersao_Id = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV140TFSistemaVersao_Id", AV140TFSistemaVersao_Id);
         Ddo_sistemaversao_id_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_id_Internalname, "FilteredText_set", Ddo_sistemaversao_id_Filteredtext_set);
         AV141TFSistemaVersao_Id_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV141TFSistemaVersao_Id_Sel", AV141TFSistemaVersao_Id_Sel);
         Ddo_sistemaversao_id_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_id_Internalname, "SelectedValue_set", Ddo_sistemaversao_id_Selectedvalue_set);
         AV124TFSistema_Ativo_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV124TFSistema_Ativo_Sel", StringUtil.Str( (decimal)(AV124TFSistema_Ativo_Sel), 1, 0));
         Ddo_sistema_ativo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_ativo_Internalname, "SelectedValue_set", Ddo_sistema_ativo_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "SISTEMA_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV67Sistema_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67Sistema_Nome1", AV67Sistema_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.DoAjaxRefresh();
      }

      protected void S142( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV36Session.Get(AV183Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV183Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV36Session.Get(AV183Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S232( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV184GXV1 = 1;
         while ( AV184GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV184GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "SISTEMA_AREATRABALHOCOD") == 0 )
            {
               AV60Sistema_AreaTrabalhoCod = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60Sistema_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60Sistema_AreaTrabalhoCod), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSISTEMA_SIGLA") == 0 )
            {
               AV88TFSistema_Sigla = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88TFSistema_Sigla", AV88TFSistema_Sigla);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88TFSistema_Sigla)) )
               {
                  Ddo_sistema_sigla_Filteredtext_set = AV88TFSistema_Sigla;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_sigla_Internalname, "FilteredText_set", Ddo_sistema_sigla_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSISTEMA_SIGLA_SEL") == 0 )
            {
               AV89TFSistema_Sigla_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89TFSistema_Sigla_Sel", AV89TFSistema_Sigla_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89TFSistema_Sigla_Sel)) )
               {
                  Ddo_sistema_sigla_Selectedvalue_set = AV89TFSistema_Sigla_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_sigla_Internalname, "SelectedValue_set", Ddo_sistema_sigla_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSISTEMA_TECNICA_SEL") == 0 )
            {
               AV96TFSistema_Tecnica_SelsJson = AV11GridStateFilterValue.gxTpr_Value;
               AV97TFSistema_Tecnica_Sels.FromJSonString(AV96TFSistema_Tecnica_SelsJson);
               if ( ! ( AV97TFSistema_Tecnica_Sels.Count == 0 ) )
               {
                  Ddo_sistema_tecnica_Selectedvalue_set = AV96TFSistema_Tecnica_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_tecnica_Internalname, "SelectedValue_set", Ddo_sistema_tecnica_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSISTEMA_COORDENACAO") == 0 )
            {
               AV100TFSistema_Coordenacao = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV100TFSistema_Coordenacao", AV100TFSistema_Coordenacao);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100TFSistema_Coordenacao)) )
               {
                  Ddo_sistema_coordenacao_Filteredtext_set = AV100TFSistema_Coordenacao;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_coordenacao_Internalname, "FilteredText_set", Ddo_sistema_coordenacao_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSISTEMA_COORDENACAO_SEL") == 0 )
            {
               AV101TFSistema_Coordenacao_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV101TFSistema_Coordenacao_Sel", AV101TFSistema_Coordenacao_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101TFSistema_Coordenacao_Sel)) )
               {
                  Ddo_sistema_coordenacao_Selectedvalue_set = AV101TFSistema_Coordenacao_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_coordenacao_Internalname, "SelectedValue_set", Ddo_sistema_coordenacao_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFAMBIENTETECNOLOGICO_DESCRICAO") == 0 )
            {
               AV104TFAmbienteTecnologico_Descricao = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV104TFAmbienteTecnologico_Descricao", AV104TFAmbienteTecnologico_Descricao);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104TFAmbienteTecnologico_Descricao)) )
               {
                  Ddo_ambientetecnologico_descricao_Filteredtext_set = AV104TFAmbienteTecnologico_Descricao;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_descricao_Internalname, "FilteredText_set", Ddo_ambientetecnologico_descricao_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFAMBIENTETECNOLOGICO_DESCRICAO_SEL") == 0 )
            {
               AV105TFAmbienteTecnologico_Descricao_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV105TFAmbienteTecnologico_Descricao_Sel", AV105TFAmbienteTecnologico_Descricao_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV105TFAmbienteTecnologico_Descricao_Sel)) )
               {
                  Ddo_ambientetecnologico_descricao_Selectedvalue_set = AV105TFAmbienteTecnologico_Descricao_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_ambientetecnologico_descricao_Internalname, "SelectedValue_set", Ddo_ambientetecnologico_descricao_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFMETODOLOGIA_DESCRICAO") == 0 )
            {
               AV108TFMetodologia_Descricao = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV108TFMetodologia_Descricao", AV108TFMetodologia_Descricao);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV108TFMetodologia_Descricao)) )
               {
                  Ddo_metodologia_descricao_Filteredtext_set = AV108TFMetodologia_Descricao;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologia_descricao_Internalname, "FilteredText_set", Ddo_metodologia_descricao_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFMETODOLOGIA_DESCRICAO_SEL") == 0 )
            {
               AV109TFMetodologia_Descricao_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV109TFMetodologia_Descricao_Sel", AV109TFMetodologia_Descricao_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV109TFMetodologia_Descricao_Sel)) )
               {
                  Ddo_metodologia_descricao_Selectedvalue_set = AV109TFMetodologia_Descricao_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_metodologia_descricao_Internalname, "SelectedValue_set", Ddo_metodologia_descricao_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSISTEMA_PROJETONOME") == 0 )
            {
               AV120TFSistema_ProjetoNome = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV120TFSistema_ProjetoNome", AV120TFSistema_ProjetoNome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV120TFSistema_ProjetoNome)) )
               {
                  Ddo_sistema_projetonome_Filteredtext_set = AV120TFSistema_ProjetoNome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_projetonome_Internalname, "FilteredText_set", Ddo_sistema_projetonome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSISTEMA_PROJETONOME_SEL") == 0 )
            {
               AV121TFSistema_ProjetoNome_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV121TFSistema_ProjetoNome_Sel", AV121TFSistema_ProjetoNome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV121TFSistema_ProjetoNome_Sel)) )
               {
                  Ddo_sistema_projetonome_Selectedvalue_set = AV121TFSistema_ProjetoNome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_projetonome_Internalname, "SelectedValue_set", Ddo_sistema_projetonome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSISTEMAVERSAO_ID") == 0 )
            {
               AV140TFSistemaVersao_Id = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV140TFSistemaVersao_Id", AV140TFSistemaVersao_Id);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV140TFSistemaVersao_Id)) )
               {
                  Ddo_sistemaversao_id_Filteredtext_set = AV140TFSistemaVersao_Id;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_id_Internalname, "FilteredText_set", Ddo_sistemaversao_id_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSISTEMAVERSAO_ID_SEL") == 0 )
            {
               AV141TFSistemaVersao_Id_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV141TFSistemaVersao_Id_Sel", AV141TFSistemaVersao_Id_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV141TFSistemaVersao_Id_Sel)) )
               {
                  Ddo_sistemaversao_id_Selectedvalue_set = AV141TFSistemaVersao_Id_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistemaversao_id_Internalname, "SelectedValue_set", Ddo_sistemaversao_id_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSISTEMA_ATIVO_SEL") == 0 )
            {
               AV124TFSistema_Ativo_Sel = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV124TFSistema_Ativo_Sel", StringUtil.Str( (decimal)(AV124TFSistema_Ativo_Sel), 1, 0));
               if ( ! (0==AV124TFSistema_Ativo_Sel) )
               {
                  Ddo_sistema_ativo_Selectedvalue_set = StringUtil.Str( (decimal)(AV124TFSistema_Ativo_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_ativo_Internalname, "SelectedValue_set", Ddo_sistema_ativo_Selectedvalue_set);
               }
            }
            AV184GXV1 = (int)(AV184GXV1+1);
         }
      }

      protected void S212( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SISTEMA_NOME") == 0 )
            {
               AV67Sistema_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67Sistema_Nome1", AV67Sistema_Nome1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SISTEMA_SIGLA") == 0 )
            {
               AV62Sistema_Sigla1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62Sistema_Sigla1", AV62Sistema_Sigla1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SISTEMA_TIPO") == 0 )
            {
               AV73Sistema_Tipo1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73Sistema_Tipo1", AV73Sistema_Tipo1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SISTEMA_TECNICA") == 0 )
            {
               AV75Sistema_Tecnica1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75Sistema_Tecnica1", AV75Sistema_Tecnica1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SISTEMA_COORDENACAO") == 0 )
            {
               AV76Sistema_Coordenacao1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76Sistema_Coordenacao1", AV76Sistema_Coordenacao1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SISTEMA_PROJETONOME") == 0 )
            {
               AV80Sistema_ProjetoNome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80Sistema_ProjetoNome1", AV80Sistema_ProjetoNome1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SISTEMA_PF") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV82Sistema_PF1 = NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82Sistema_PF1", StringUtil.LTrim( StringUtil.Str( AV82Sistema_PF1, 14, 5)));
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV20DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV21DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "SISTEMA_NOME") == 0 )
               {
                  AV68Sistema_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68Sistema_Nome2", AV68Sistema_Nome2);
               }
               else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "SISTEMA_SIGLA") == 0 )
               {
                  AV63Sistema_Sigla2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63Sistema_Sigla2", AV63Sistema_Sigla2);
               }
               else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "SISTEMA_TIPO") == 0 )
               {
                  AV74Sistema_Tipo2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74Sistema_Tipo2", AV74Sistema_Tipo2);
               }
               else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "SISTEMA_TECNICA") == 0 )
               {
                  AV77Sistema_Tecnica2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77Sistema_Tecnica2", AV77Sistema_Tecnica2);
               }
               else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "SISTEMA_COORDENACAO") == 0 )
               {
                  AV78Sistema_Coordenacao2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78Sistema_Coordenacao2", AV78Sistema_Coordenacao2);
               }
               else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "SISTEMA_PROJETONOME") == 0 )
               {
                  AV81Sistema_ProjetoNome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81Sistema_ProjetoNome2", AV81Sistema_ProjetoNome2);
               }
               else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "SISTEMA_PF") == 0 )
               {
                  AV22DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
                  AV83Sistema_PF2 = NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83Sistema_PF2", StringUtil.LTrim( StringUtil.Str( AV83Sistema_PF2, 14, 5)));
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV32DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S172( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV36Session.Get(AV183Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! (0==AV60Sistema_AreaTrabalhoCod) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "SISTEMA_AREATRABALHOCOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV60Sistema_AreaTrabalhoCod), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88TFSistema_Sigla)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSISTEMA_SIGLA";
            AV11GridStateFilterValue.gxTpr_Value = AV88TFSistema_Sigla;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89TFSistema_Sigla_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSISTEMA_SIGLA_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV89TFSistema_Sigla_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( AV97TFSistema_Tecnica_Sels.Count == 0 ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSISTEMA_TECNICA_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV97TFSistema_Tecnica_Sels.ToJSonString(false);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100TFSistema_Coordenacao)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSISTEMA_COORDENACAO";
            AV11GridStateFilterValue.gxTpr_Value = AV100TFSistema_Coordenacao;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101TFSistema_Coordenacao_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSISTEMA_COORDENACAO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV101TFSistema_Coordenacao_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104TFAmbienteTecnologico_Descricao)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFAMBIENTETECNOLOGICO_DESCRICAO";
            AV11GridStateFilterValue.gxTpr_Value = AV104TFAmbienteTecnologico_Descricao;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV105TFAmbienteTecnologico_Descricao_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFAMBIENTETECNOLOGICO_DESCRICAO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV105TFAmbienteTecnologico_Descricao_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV108TFMetodologia_Descricao)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFMETODOLOGIA_DESCRICAO";
            AV11GridStateFilterValue.gxTpr_Value = AV108TFMetodologia_Descricao;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV109TFMetodologia_Descricao_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFMETODOLOGIA_DESCRICAO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV109TFMetodologia_Descricao_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV120TFSistema_ProjetoNome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSISTEMA_PROJETONOME";
            AV11GridStateFilterValue.gxTpr_Value = AV120TFSistema_ProjetoNome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV121TFSistema_ProjetoNome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSISTEMA_PROJETONOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV121TFSistema_ProjetoNome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV140TFSistemaVersao_Id)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSISTEMAVERSAO_ID";
            AV11GridStateFilterValue.gxTpr_Value = AV140TFSistemaVersao_Id;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV141TFSistemaVersao_Id_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSISTEMAVERSAO_ID_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV141TFSistemaVersao_Id_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV124TFSistema_Ativo_Sel) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSISTEMA_ATIVO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV124TFSistema_Ativo_Sel), 1, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV183Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S192( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV33DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SISTEMA_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV67Sistema_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV67Sistema_Nome1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SISTEMA_SIGLA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV62Sistema_Sigla1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV62Sistema_Sigla1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SISTEMA_TIPO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV73Sistema_Tipo1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV73Sistema_Tipo1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SISTEMA_TECNICA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV75Sistema_Tecnica1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV75Sistema_Tecnica1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SISTEMA_COORDENACAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV76Sistema_Coordenacao1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV76Sistema_Coordenacao1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SISTEMA_PROJETONOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV80Sistema_ProjetoNome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV80Sistema_ProjetoNome1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SISTEMA_PF") == 0 ) && ! (Convert.ToDecimal(0)==AV82Sistema_PF1) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( AV82Sistema_PF1, 14, 5);
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            if ( AV32DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV20DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV21DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "SISTEMA_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV68Sistema_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV68Sistema_Nome2;
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "SISTEMA_SIGLA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV63Sistema_Sigla2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV63Sistema_Sigla2;
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "SISTEMA_TIPO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV74Sistema_Tipo2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV74Sistema_Tipo2;
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "SISTEMA_TECNICA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV77Sistema_Tecnica2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV77Sistema_Tecnica2;
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "SISTEMA_COORDENACAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV78Sistema_Coordenacao2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV78Sistema_Coordenacao2;
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "SISTEMA_PROJETONOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV81Sistema_ProjetoNome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV81Sistema_ProjetoNome2;
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "SISTEMA_PF") == 0 ) && ! (Convert.ToDecimal(0)==AV83Sistema_PF2) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( AV83Sistema_PF2, 14, 5);
               AV12GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator2;
            }
            if ( AV32DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S132( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV183Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Sistema";
         AV36Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void E293G2( )
      {
         /* Onmessage_gx1 Routine */
         if ( StringUtil.StrCmp(AV61NotificationInfo.gxTpr_Id, "ALTERAR_AREA_TRABALHO") == 0 )
         {
            new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV67Sistema_Nome1, AV62Sistema_Sigla1, AV73Sistema_Tipo1, AV75Sistema_Tecnica1, AV76Sistema_Coordenacao1, AV80Sistema_ProjetoNome1, AV21DynamicFiltersSelector2, AV68Sistema_Nome2, AV63Sistema_Sigla2, AV74Sistema_Tipo2, AV77Sistema_Tecnica2, AV78Sistema_Coordenacao2, AV81Sistema_ProjetoNome2, AV20DynamicFiltersEnabled2, AV88TFSistema_Sigla, AV89TFSistema_Sigla_Sel, AV100TFSistema_Coordenacao, AV101TFSistema_Coordenacao_Sel, AV104TFAmbienteTecnologico_Descricao, AV105TFAmbienteTecnologico_Descricao_Sel, AV108TFMetodologia_Descricao, AV109TFMetodologia_Descricao_Sel, AV120TFSistema_ProjetoNome, AV121TFSistema_ProjetoNome_Sel, AV140TFSistemaVersao_Id, AV141TFSistemaVersao_Id_Sel, AV124TFSistema_Ativo_Sel, AV6WWPContext, AV90ddo_Sistema_SiglaTitleControlIdToReplace, AV98ddo_Sistema_TecnicaTitleControlIdToReplace, AV102ddo_Sistema_CoordenacaoTitleControlIdToReplace, AV106ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace, AV110ddo_Metodologia_DescricaoTitleControlIdToReplace, AV122ddo_Sistema_ProjetoNomeTitleControlIdToReplace, AV142ddo_SistemaVersao_IdTitleControlIdToReplace, AV125ddo_Sistema_AtivoTitleControlIdToReplace, AV137Paginando, AV134Registros, AV135PreView, AV136PaginaAtual, AV60Sistema_AreaTrabalhoCod, AV16DynamicFiltersOperator1, AV82Sistema_PF1, AV22DynamicFiltersOperator2, AV83Sistema_PF2, AV97TFSistema_Tecnica_Sels, AV183Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A127Sistema_Codigo, A351AmbienteTecnologico_Codigo, A137Metodologia_Codigo, A691Sistema_ProjetoCod, A1859SistemaVersao_Codigo, A130Sistema_Ativo, A707Sistema_TipoSigla, A395Sistema_PF, A690Sistema_PFA) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
      }

      protected void S162( )
      {
         /* 'PRECONSULTA' Routine */
         AV134Registros = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV134Registros", StringUtil.LTrim( StringUtil.Str( (decimal)(AV134Registros), 10, 0)));
         AV145WWSistemaDS_1_Sistema_areatrabalhocod = AV60Sistema_AreaTrabalhoCod;
         AV146WWSistemaDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV147WWSistemaDS_3_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV148WWSistemaDS_4_Sistema_nome1 = AV67Sistema_Nome1;
         AV149WWSistemaDS_5_Sistema_sigla1 = AV62Sistema_Sigla1;
         AV150WWSistemaDS_6_Sistema_tipo1 = AV73Sistema_Tipo1;
         AV151WWSistemaDS_7_Sistema_tecnica1 = AV75Sistema_Tecnica1;
         AV152WWSistemaDS_8_Sistema_coordenacao1 = AV76Sistema_Coordenacao1;
         AV153WWSistemaDS_9_Sistema_projetonome1 = AV80Sistema_ProjetoNome1;
         AV154WWSistemaDS_10_Sistema_pf1 = AV82Sistema_PF1;
         AV155WWSistemaDS_11_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV156WWSistemaDS_12_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV157WWSistemaDS_13_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV158WWSistemaDS_14_Sistema_nome2 = AV68Sistema_Nome2;
         AV159WWSistemaDS_15_Sistema_sigla2 = AV63Sistema_Sigla2;
         AV160WWSistemaDS_16_Sistema_tipo2 = AV74Sistema_Tipo2;
         AV161WWSistemaDS_17_Sistema_tecnica2 = AV77Sistema_Tecnica2;
         AV162WWSistemaDS_18_Sistema_coordenacao2 = AV78Sistema_Coordenacao2;
         AV163WWSistemaDS_19_Sistema_projetonome2 = AV81Sistema_ProjetoNome2;
         AV164WWSistemaDS_20_Sistema_pf2 = AV83Sistema_PF2;
         AV165WWSistemaDS_21_Tfsistema_sigla = AV88TFSistema_Sigla;
         AV166WWSistemaDS_22_Tfsistema_sigla_sel = AV89TFSistema_Sigla_Sel;
         AV167WWSistemaDS_23_Tfsistema_tecnica_sels = AV97TFSistema_Tecnica_Sels;
         AV168WWSistemaDS_24_Tfsistema_coordenacao = AV100TFSistema_Coordenacao;
         AV169WWSistemaDS_25_Tfsistema_coordenacao_sel = AV101TFSistema_Coordenacao_Sel;
         AV170WWSistemaDS_26_Tfambientetecnologico_descricao = AV104TFAmbienteTecnologico_Descricao;
         AV171WWSistemaDS_27_Tfambientetecnologico_descricao_sel = AV105TFAmbienteTecnologico_Descricao_Sel;
         AV172WWSistemaDS_28_Tfmetodologia_descricao = AV108TFMetodologia_Descricao;
         AV173WWSistemaDS_29_Tfmetodologia_descricao_sel = AV109TFMetodologia_Descricao_Sel;
         AV174WWSistemaDS_30_Tfsistema_projetonome = AV120TFSistema_ProjetoNome;
         AV175WWSistemaDS_31_Tfsistema_projetonome_sel = AV121TFSistema_ProjetoNome_Sel;
         AV176WWSistemaDS_32_Tfsistemaversao_id = AV140TFSistemaVersao_Id;
         AV177WWSistemaDS_33_Tfsistemaversao_id_sel = AV141TFSistemaVersao_Id_Sel;
         AV178WWSistemaDS_34_Tfsistema_ativo_sel = AV124TFSistema_Ativo_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A700Sistema_Tecnica ,
                                              AV167WWSistemaDS_23_Tfsistema_tecnica_sels ,
                                              AV146WWSistemaDS_2_Dynamicfiltersselector1 ,
                                              AV148WWSistemaDS_4_Sistema_nome1 ,
                                              AV149WWSistemaDS_5_Sistema_sigla1 ,
                                              AV150WWSistemaDS_6_Sistema_tipo1 ,
                                              AV151WWSistemaDS_7_Sistema_tecnica1 ,
                                              AV152WWSistemaDS_8_Sistema_coordenacao1 ,
                                              AV153WWSistemaDS_9_Sistema_projetonome1 ,
                                              AV155WWSistemaDS_11_Dynamicfiltersenabled2 ,
                                              AV156WWSistemaDS_12_Dynamicfiltersselector2 ,
                                              AV158WWSistemaDS_14_Sistema_nome2 ,
                                              AV159WWSistemaDS_15_Sistema_sigla2 ,
                                              AV160WWSistemaDS_16_Sistema_tipo2 ,
                                              AV161WWSistemaDS_17_Sistema_tecnica2 ,
                                              AV162WWSistemaDS_18_Sistema_coordenacao2 ,
                                              AV163WWSistemaDS_19_Sistema_projetonome2 ,
                                              AV166WWSistemaDS_22_Tfsistema_sigla_sel ,
                                              AV165WWSistemaDS_21_Tfsistema_sigla ,
                                              AV167WWSistemaDS_23_Tfsistema_tecnica_sels.Count ,
                                              AV169WWSistemaDS_25_Tfsistema_coordenacao_sel ,
                                              AV168WWSistemaDS_24_Tfsistema_coordenacao ,
                                              AV171WWSistemaDS_27_Tfambientetecnologico_descricao_sel ,
                                              AV170WWSistemaDS_26_Tfambientetecnologico_descricao ,
                                              AV173WWSistemaDS_29_Tfmetodologia_descricao_sel ,
                                              AV172WWSistemaDS_28_Tfmetodologia_descricao ,
                                              AV175WWSistemaDS_31_Tfsistema_projetonome_sel ,
                                              AV174WWSistemaDS_30_Tfsistema_projetonome ,
                                              AV177WWSistemaDS_33_Tfsistemaversao_id_sel ,
                                              AV176WWSistemaDS_32_Tfsistemaversao_id ,
                                              AV178WWSistemaDS_34_Tfsistema_ativo_sel ,
                                              A416Sistema_Nome ,
                                              A129Sistema_Sigla ,
                                              A699Sistema_Tipo ,
                                              A513Sistema_Coordenacao ,
                                              A703Sistema_ProjetoNome ,
                                              A352AmbienteTecnologico_Descricao ,
                                              A138Metodologia_Descricao ,
                                              A1860SistemaVersao_Id ,
                                              A130Sistema_Ativo ,
                                              AV147WWSistemaDS_3_Dynamicfiltersoperator1 ,
                                              AV154WWSistemaDS_10_Sistema_pf1 ,
                                              A395Sistema_PF ,
                                              AV157WWSistemaDS_13_Dynamicfiltersoperator2 ,
                                              AV164WWSistemaDS_20_Sistema_pf2 ,
                                              AV6WWPContext.gxTpr_Areatrabalho_codigo ,
                                              A135Sistema_AreaTrabalhoCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV148WWSistemaDS_4_Sistema_nome1 = StringUtil.Concat( StringUtil.RTrim( AV148WWSistemaDS_4_Sistema_nome1), "%", "");
         lV149WWSistemaDS_5_Sistema_sigla1 = StringUtil.PadR( StringUtil.RTrim( AV149WWSistemaDS_5_Sistema_sigla1), 25, "%");
         lV152WWSistemaDS_8_Sistema_coordenacao1 = StringUtil.Concat( StringUtil.RTrim( AV152WWSistemaDS_8_Sistema_coordenacao1), "%", "");
         lV153WWSistemaDS_9_Sistema_projetonome1 = StringUtil.PadR( StringUtil.RTrim( AV153WWSistemaDS_9_Sistema_projetonome1), 50, "%");
         lV158WWSistemaDS_14_Sistema_nome2 = StringUtil.Concat( StringUtil.RTrim( AV158WWSistemaDS_14_Sistema_nome2), "%", "");
         lV159WWSistemaDS_15_Sistema_sigla2 = StringUtil.PadR( StringUtil.RTrim( AV159WWSistemaDS_15_Sistema_sigla2), 25, "%");
         lV162WWSistemaDS_18_Sistema_coordenacao2 = StringUtil.Concat( StringUtil.RTrim( AV162WWSistemaDS_18_Sistema_coordenacao2), "%", "");
         lV163WWSistemaDS_19_Sistema_projetonome2 = StringUtil.PadR( StringUtil.RTrim( AV163WWSistemaDS_19_Sistema_projetonome2), 50, "%");
         lV165WWSistemaDS_21_Tfsistema_sigla = StringUtil.PadR( StringUtil.RTrim( AV165WWSistemaDS_21_Tfsistema_sigla), 25, "%");
         lV168WWSistemaDS_24_Tfsistema_coordenacao = StringUtil.Concat( StringUtil.RTrim( AV168WWSistemaDS_24_Tfsistema_coordenacao), "%", "");
         lV170WWSistemaDS_26_Tfambientetecnologico_descricao = StringUtil.Concat( StringUtil.RTrim( AV170WWSistemaDS_26_Tfambientetecnologico_descricao), "%", "");
         lV172WWSistemaDS_28_Tfmetodologia_descricao = StringUtil.Concat( StringUtil.RTrim( AV172WWSistemaDS_28_Tfmetodologia_descricao), "%", "");
         lV174WWSistemaDS_30_Tfsistema_projetonome = StringUtil.PadR( StringUtil.RTrim( AV174WWSistemaDS_30_Tfsistema_projetonome), 50, "%");
         lV176WWSistemaDS_32_Tfsistemaversao_id = StringUtil.PadR( StringUtil.RTrim( AV176WWSistemaDS_32_Tfsistemaversao_id), 20, "%");
         /* Using cursor H003G3 */
         pr_default.execute(1, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, lV148WWSistemaDS_4_Sistema_nome1, lV149WWSistemaDS_5_Sistema_sigla1, AV150WWSistemaDS_6_Sistema_tipo1, AV151WWSistemaDS_7_Sistema_tecnica1, lV152WWSistemaDS_8_Sistema_coordenacao1, lV153WWSistemaDS_9_Sistema_projetonome1, lV158WWSistemaDS_14_Sistema_nome2, lV159WWSistemaDS_15_Sistema_sigla2, AV160WWSistemaDS_16_Sistema_tipo2, AV161WWSistemaDS_17_Sistema_tecnica2, lV162WWSistemaDS_18_Sistema_coordenacao2, lV163WWSistemaDS_19_Sistema_projetonome2, lV165WWSistemaDS_21_Tfsistema_sigla, AV166WWSistemaDS_22_Tfsistema_sigla_sel, lV168WWSistemaDS_24_Tfsistema_coordenacao, AV169WWSistemaDS_25_Tfsistema_coordenacao_sel, lV170WWSistemaDS_26_Tfambientetecnologico_descricao, AV171WWSistemaDS_27_Tfambientetecnologico_descricao_sel, lV172WWSistemaDS_28_Tfmetodologia_descricao, AV173WWSistemaDS_29_Tfmetodologia_descricao_sel, lV174WWSistemaDS_30_Tfsistema_projetonome, AV175WWSistemaDS_31_Tfsistema_projetonome_sel, lV176WWSistemaDS_32_Tfsistemaversao_id, AV177WWSistemaDS_33_Tfsistemaversao_id_sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A137Metodologia_Codigo = H003G3_A137Metodologia_Codigo[0];
            n137Metodologia_Codigo = H003G3_n137Metodologia_Codigo[0];
            A351AmbienteTecnologico_Codigo = H003G3_A351AmbienteTecnologico_Codigo[0];
            n351AmbienteTecnologico_Codigo = H003G3_n351AmbienteTecnologico_Codigo[0];
            A1859SistemaVersao_Codigo = H003G3_A1859SistemaVersao_Codigo[0];
            n1859SistemaVersao_Codigo = H003G3_n1859SistemaVersao_Codigo[0];
            A691Sistema_ProjetoCod = H003G3_A691Sistema_ProjetoCod[0];
            n691Sistema_ProjetoCod = H003G3_n691Sistema_ProjetoCod[0];
            A130Sistema_Ativo = H003G3_A130Sistema_Ativo[0];
            A1860SistemaVersao_Id = H003G3_A1860SistemaVersao_Id[0];
            A138Metodologia_Descricao = H003G3_A138Metodologia_Descricao[0];
            A352AmbienteTecnologico_Descricao = H003G3_A352AmbienteTecnologico_Descricao[0];
            A703Sistema_ProjetoNome = H003G3_A703Sistema_ProjetoNome[0];
            n703Sistema_ProjetoNome = H003G3_n703Sistema_ProjetoNome[0];
            A513Sistema_Coordenacao = H003G3_A513Sistema_Coordenacao[0];
            n513Sistema_Coordenacao = H003G3_n513Sistema_Coordenacao[0];
            A700Sistema_Tecnica = H003G3_A700Sistema_Tecnica[0];
            n700Sistema_Tecnica = H003G3_n700Sistema_Tecnica[0];
            A699Sistema_Tipo = H003G3_A699Sistema_Tipo[0];
            n699Sistema_Tipo = H003G3_n699Sistema_Tipo[0];
            A129Sistema_Sigla = H003G3_A129Sistema_Sigla[0];
            A416Sistema_Nome = H003G3_A416Sistema_Nome[0];
            A135Sistema_AreaTrabalhoCod = H003G3_A135Sistema_AreaTrabalhoCod[0];
            A127Sistema_Codigo = H003G3_A127Sistema_Codigo[0];
            A138Metodologia_Descricao = H003G3_A138Metodologia_Descricao[0];
            A352AmbienteTecnologico_Descricao = H003G3_A352AmbienteTecnologico_Descricao[0];
            A1860SistemaVersao_Id = H003G3_A1860SistemaVersao_Id[0];
            A703Sistema_ProjetoNome = H003G3_A703Sistema_ProjetoNome[0];
            n703Sistema_ProjetoNome = H003G3_n703Sistema_ProjetoNome[0];
            GXt_decimal1 = A395Sistema_PF;
            new prc_sistema_pf(context ).execute( ref  A127Sistema_Codigo, ref  GXt_decimal1) ;
            A395Sistema_PF = GXt_decimal1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A395Sistema_PF", StringUtil.LTrim( StringUtil.Str( A395Sistema_PF, 14, 5)));
            if ( ! ( ( StringUtil.StrCmp(AV146WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_PF") == 0 ) && ( AV147WWSistemaDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! (Convert.ToDecimal(0)==AV154WWSistemaDS_10_Sistema_pf1) ) ) || ( ( A395Sistema_PF < AV154WWSistemaDS_10_Sistema_pf1 ) ) )
            {
               if ( ! ( ( StringUtil.StrCmp(AV146WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_PF") == 0 ) && ( AV147WWSistemaDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! (Convert.ToDecimal(0)==AV154WWSistemaDS_10_Sistema_pf1) ) ) || ( ( A395Sistema_PF == AV154WWSistemaDS_10_Sistema_pf1 ) ) )
               {
                  if ( ! ( ( StringUtil.StrCmp(AV146WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_PF") == 0 ) && ( AV147WWSistemaDS_3_Dynamicfiltersoperator1 == 2 ) && ( ! (Convert.ToDecimal(0)==AV154WWSistemaDS_10_Sistema_pf1) ) ) || ( ( A395Sistema_PF > AV154WWSistemaDS_10_Sistema_pf1 ) ) )
                  {
                     if ( ! ( AV155WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV156WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_PF") == 0 ) && ( AV157WWSistemaDS_13_Dynamicfiltersoperator2 == 0 ) && ( ! (Convert.ToDecimal(0)==AV164WWSistemaDS_20_Sistema_pf2) ) ) || ( ( A395Sistema_PF < AV164WWSistemaDS_20_Sistema_pf2 ) ) )
                     {
                        if ( ! ( AV155WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV156WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_PF") == 0 ) && ( AV157WWSistemaDS_13_Dynamicfiltersoperator2 == 1 ) && ( ! (Convert.ToDecimal(0)==AV164WWSistemaDS_20_Sistema_pf2) ) ) || ( ( A395Sistema_PF == AV164WWSistemaDS_20_Sistema_pf2 ) ) )
                        {
                           if ( ! ( AV155WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV156WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_PF") == 0 ) && ( AV157WWSistemaDS_13_Dynamicfiltersoperator2 == 2 ) && ( ! (Convert.ToDecimal(0)==AV164WWSistemaDS_20_Sistema_pf2) ) ) || ( ( A395Sistema_PF > AV164WWSistemaDS_20_Sistema_pf2 ) ) )
                           {
                              AV134Registros = (long)(AV134Registros+1);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV134Registros", StringUtil.LTrim( StringUtil.Str( (decimal)(AV134Registros), 10, 0)));
                           }
                        }
                     }
                  }
               }
            }
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void wb_table1_2_3G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_3G2( true) ;
         }
         else
         {
            wb_table2_8_3G2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_3G2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_83_3G2( true) ;
         }
         else
         {
            wb_table3_83_3G2( false) ;
         }
         return  ;
      }

      protected void wb_table3_83_3G2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_3G2e( true) ;
         }
         else
         {
            wb_table1_2_3G2e( false) ;
         }
      }

      protected void wb_table3_83_3G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable3_Internalname, tblUnnamedtable3_Internalname, "", "TableGridHeader", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_86_3G2( true) ;
         }
         else
         {
            wb_table4_86_3G2( false) ;
         }
         return  ;
      }

      protected void wb_table4_86_3G2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_83_3G2e( true) ;
         }
         else
         {
            wb_table3_83_3G2e( false) ;
         }
      }

      protected void wb_table4_86_3G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"89\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDisplay_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(410), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Nome") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSistema_Sigla_Titleformat == 0 )
               {
                  context.SendWebValue( edtSistema_Sigla_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSistema_Sigla_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Tipo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbSistema_Tecnica_Titleformat == 0 )
               {
                  context.SendWebValue( cmbSistema_Tecnica.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbSistema_Tecnica.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSistema_Coordenacao_Titleformat == 0 )
               {
                  context.SendWebValue( edtSistema_Coordenacao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSistema_Coordenacao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAmbienteTecnologico_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtAmbienteTecnologico_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAmbienteTecnologico_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Metodolog�a") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtMetodologia_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtMetodologia_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtMetodologia_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "PF.B") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "PF.A") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(260), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSistema_ProjetoNome_Titleformat == 0 )
               {
                  context.SendWebValue( edtSistema_ProjetoNome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSistema_ProjetoNome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSistemaVersao_Id_Titleformat == 0 )
               {
                  context.SendWebValue( edtSistemaVersao_Id_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSistemaVersao_Id_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkSistema_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( chkSistema_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkSistema_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavAssociarclientes_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( edtavAssociarclientes_Title) ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV34Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV35Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV58Display));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDisplay_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A416Sistema_Nome);
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A129Sistema_Sigla));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSistema_Sigla_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSistema_Sigla_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSistema_Sigla_Forecolor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV130Sistema_TipoSigla));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavSistema_tiposigla_Forecolor), 9, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavSistema_tiposigla_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A700Sistema_Tecnica));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbSistema_Tecnica.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbSistema_Tecnica_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbSistema_Tecnica.ForeColor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A513Sistema_Coordenacao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSistema_Coordenacao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSistema_Coordenacao_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSistema_Coordenacao_Forecolor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A352AmbienteTecnologico_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAmbienteTecnologico_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAmbienteTecnologico_Descricao_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAmbienteTecnologico_Descricao_Forecolor), 9, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtAmbienteTecnologico_Descricao_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A137Metodologia_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A138Metodologia_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtMetodologia_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtMetodologia_Descricao_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtMetodologia_Descricao_Forecolor), 9, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtMetodologia_Descricao_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV131PFB, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavPfb_Forecolor), 9, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavPfb_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV133PFA, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavPfa_Forecolor), 9, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavPfa_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A703Sistema_ProjetoNome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSistema_ProjetoNome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSistema_ProjetoNome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSistema_ProjetoNome_Forecolor), 9, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtSistema_ProjetoNome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1860SistemaVersao_Id));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSistemaVersao_Id_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSistemaVersao_Id_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtSistemaVersao_Id_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A130Sistema_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkSistema_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkSistema_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV84AssociarClientes));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtavAssociarclientes_Title));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavAssociarclientes_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavAssociarclientes_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 89 )
         {
            wbEnd = 0;
            nRC_GXsfl_89 = (short)(nGXsfl_89_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_86_3G2e( true) ;
         }
         else
         {
            wb_table4_86_3G2e( false) ;
         }
      }

      protected void wb_table2_8_3G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable2_Internalname, tblUnnamedtable2_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblSistematitle_Internalname, "Sistemas / Planos de Contagem", "", "", lblSistematitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_13_3G2( true) ;
         }
         else
         {
            wb_table5_13_3G2( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_3G2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'" + sGXsfl_89_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,22);\"", "", true, "HLP_WWSistema.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,23);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_25_3G2( true) ;
         }
         else
         {
            wb_table6_25_3G2( false) ;
         }
         return  ;
      }

      protected void wb_table6_25_3G2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_3G2e( true) ;
         }
         else
         {
            wb_table2_8_3G2e( false) ;
         }
      }

      protected void wb_table6_25_3G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextsistema_areatrabalhocod_Internalname, "C�d. �rea Trabalho", "", "", lblFiltertextsistema_areatrabalhocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSistema_areatrabalhocod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV60Sistema_AreaTrabalhoCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV60Sistema_AreaTrabalhoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,32);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSistema_areatrabalhocod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_34_3G2( true) ;
         }
         else
         {
            wb_table7_34_3G2( false) ;
         }
         return  ;
      }

      protected void wb_table7_34_3G2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_25_3G2e( true) ;
         }
         else
         {
            wb_table6_25_3G2e( false) ;
         }
      }

      protected void wb_table7_34_3G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'" + sGXsfl_89_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,39);\"", "", true, "HLP_WWSistema.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_43_3G2( true) ;
         }
         else
         {
            wb_table8_43_3G2( false) ;
         }
         return  ;
      }

      protected void wb_table8_43_3G2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWSistema.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'" + sGXsfl_89_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV21DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,62);\"", "", true, "HLP_WWSistema.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_66_3G2( true) ;
         }
         else
         {
            wb_table9_66_3G2( false) ;
         }
         return  ;
      }

      protected void wb_table9_66_3G2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_34_3G2e( true) ;
         }
         else
         {
            wb_table7_34_3G2e( false) ;
         }
      }

      protected void wb_table9_66_3G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'" + sGXsfl_89_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,69);\"", "", true, "HLP_WWSistema.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSistema_nome2_Internalname, AV68Sistema_Nome2, StringUtil.RTrim( context.localUtil.Format( AV68Sistema_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,71);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSistema_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavSistema_nome2_Visible, 1, 0, "text", "", 300, "px", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWSistema.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSistema_sigla2_Internalname, StringUtil.RTrim( AV63Sistema_Sigla2), StringUtil.RTrim( context.localUtil.Format( AV63Sistema_Sigla2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,72);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSistema_sigla2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavSistema_sigla2_Visible, 1, 0, "text", "", 25, "chr", 1, "row", 25, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWSistema.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'" + sGXsfl_89_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavSistema_tipo2, cmbavSistema_tipo2_Internalname, StringUtil.RTrim( AV74Sistema_Tipo2), 1, cmbavSistema_tipo2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavSistema_tipo2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,73);\"", "", true, "HLP_WWSistema.htm");
            cmbavSistema_tipo2.CurrentValue = StringUtil.RTrim( AV74Sistema_Tipo2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_tipo2_Internalname, "Values", (String)(cmbavSistema_tipo2.ToJavascriptSource()));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'" + sGXsfl_89_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavSistema_tecnica2, cmbavSistema_tecnica2_Internalname, StringUtil.RTrim( AV77Sistema_Tecnica2), 1, cmbavSistema_tecnica2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavSistema_tecnica2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,74);\"", "", true, "HLP_WWSistema.htm");
            cmbavSistema_tecnica2.CurrentValue = StringUtil.RTrim( AV77Sistema_Tecnica2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_tecnica2_Internalname, "Values", (String)(cmbavSistema_tecnica2.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSistema_coordenacao2_Internalname, AV78Sistema_Coordenacao2, StringUtil.RTrim( context.localUtil.Format( AV78Sistema_Coordenacao2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,75);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSistema_coordenacao2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavSistema_coordenacao2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWSistema.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSistema_projetonome2_Internalname, StringUtil.RTrim( AV81Sistema_ProjetoNome2), StringUtil.RTrim( context.localUtil.Format( AV81Sistema_ProjetoNome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,76);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSistema_projetonome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavSistema_projetonome2_Visible, 1, 0, "text", "", 100, "%", 1, "row", 50, 0, 0, 0, 1, -1, 0, true, "", "left", true, "HLP_WWSistema.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSistema_pf2_Internalname, StringUtil.LTrim( StringUtil.NToC( AV83Sistema_PF2, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV83Sistema_PF2, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,77);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSistema_pf2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavSistema_pf2_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_66_3G2e( true) ;
         }
         else
         {
            wb_table9_66_3G2e( false) ;
         }
      }

      protected void wb_table8_43_3G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'" + sGXsfl_89_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,46);\"", "", true, "HLP_WWSistema.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSistema_nome1_Internalname, AV67Sistema_Nome1, StringUtil.RTrim( context.localUtil.Format( AV67Sistema_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,48);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSistema_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavSistema_nome1_Visible, 1, 0, "text", "", 300, "px", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWSistema.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSistema_sigla1_Internalname, StringUtil.RTrim( AV62Sistema_Sigla1), StringUtil.RTrim( context.localUtil.Format( AV62Sistema_Sigla1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSistema_sigla1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavSistema_sigla1_Visible, 1, 0, "text", "", 25, "chr", 1, "row", 25, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWSistema.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_89_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavSistema_tipo1, cmbavSistema_tipo1_Internalname, StringUtil.RTrim( AV73Sistema_Tipo1), 1, cmbavSistema_tipo1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavSistema_tipo1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", "", true, "HLP_WWSistema.htm");
            cmbavSistema_tipo1.CurrentValue = StringUtil.RTrim( AV73Sistema_Tipo1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_tipo1_Internalname, "Values", (String)(cmbavSistema_tipo1.ToJavascriptSource()));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'" + sGXsfl_89_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavSistema_tecnica1, cmbavSistema_tecnica1_Internalname, StringUtil.RTrim( AV75Sistema_Tecnica1), 1, cmbavSistema_tecnica1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavSistema_tecnica1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,51);\"", "", true, "HLP_WWSistema.htm");
            cmbavSistema_tecnica1.CurrentValue = StringUtil.RTrim( AV75Sistema_Tecnica1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSistema_tecnica1_Internalname, "Values", (String)(cmbavSistema_tecnica1.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSistema_coordenacao1_Internalname, AV76Sistema_Coordenacao1, StringUtil.RTrim( context.localUtil.Format( AV76Sistema_Coordenacao1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,52);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSistema_coordenacao1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavSistema_coordenacao1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWSistema.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSistema_projetonome1_Internalname, StringUtil.RTrim( AV80Sistema_ProjetoNome1), StringUtil.RTrim( context.localUtil.Format( AV80Sistema_ProjetoNome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,53);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSistema_projetonome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavSistema_projetonome1_Visible, 1, 0, "text", "", 100, "%", 1, "row", 50, 0, 0, 0, 1, -1, 0, true, "", "left", true, "HLP_WWSistema.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'" + sGXsfl_89_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSistema_pf1_Internalname, StringUtil.LTrim( StringUtil.NToC( AV82Sistema_PF1, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV82Sistema_PF1, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSistema_pf1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavSistema_pf1_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_43_3G2e( true) ;
         }
         else
         {
            wb_table8_43_3G2e( false) ;
         }
      }

      protected void wb_table5_13_3G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgInsert_Visible, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgExportreport_Internalname, context.GetImagePath( "776fb79c-a0a1-4302-b5e5-d773dbe1a297", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Exportar � PDF", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgExportreport_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOEXPORTREPORT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_3G2e( true) ;
         }
         else
         {
            wb_table5_13_3G2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA3G2( ) ;
         WS3G2( ) ;
         WE3G2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203269124581");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwsistema.js", "?20203269124582");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("Window/InNewWindowRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_892( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_89_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_89_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_89_idx;
         edtSistema_Codigo_Internalname = "SISTEMA_CODIGO_"+sGXsfl_89_idx;
         edtSistema_Nome_Internalname = "SISTEMA_NOME_"+sGXsfl_89_idx;
         edtSistema_Sigla_Internalname = "SISTEMA_SIGLA_"+sGXsfl_89_idx;
         edtavSistema_tiposigla_Internalname = "vSISTEMA_TIPOSIGLA_"+sGXsfl_89_idx;
         cmbSistema_Tecnica_Internalname = "SISTEMA_TECNICA_"+sGXsfl_89_idx;
         edtSistema_Coordenacao_Internalname = "SISTEMA_COORDENACAO_"+sGXsfl_89_idx;
         edtAmbienteTecnologico_Descricao_Internalname = "AMBIENTETECNOLOGICO_DESCRICAO_"+sGXsfl_89_idx;
         edtMetodologia_Codigo_Internalname = "METODOLOGIA_CODIGO_"+sGXsfl_89_idx;
         edtMetodologia_Descricao_Internalname = "METODOLOGIA_DESCRICAO_"+sGXsfl_89_idx;
         edtavPfb_Internalname = "vPFB_"+sGXsfl_89_idx;
         edtavPfa_Internalname = "vPFA_"+sGXsfl_89_idx;
         edtSistema_ProjetoNome_Internalname = "SISTEMA_PROJETONOME_"+sGXsfl_89_idx;
         edtSistemaVersao_Id_Internalname = "SISTEMAVERSAO_ID_"+sGXsfl_89_idx;
         chkSistema_Ativo_Internalname = "SISTEMA_ATIVO_"+sGXsfl_89_idx;
         edtavAssociarclientes_Internalname = "vASSOCIARCLIENTES_"+sGXsfl_89_idx;
      }

      protected void SubsflControlProps_fel_892( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_89_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_89_fel_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_89_fel_idx;
         edtSistema_Codigo_Internalname = "SISTEMA_CODIGO_"+sGXsfl_89_fel_idx;
         edtSistema_Nome_Internalname = "SISTEMA_NOME_"+sGXsfl_89_fel_idx;
         edtSistema_Sigla_Internalname = "SISTEMA_SIGLA_"+sGXsfl_89_fel_idx;
         edtavSistema_tiposigla_Internalname = "vSISTEMA_TIPOSIGLA_"+sGXsfl_89_fel_idx;
         cmbSistema_Tecnica_Internalname = "SISTEMA_TECNICA_"+sGXsfl_89_fel_idx;
         edtSistema_Coordenacao_Internalname = "SISTEMA_COORDENACAO_"+sGXsfl_89_fel_idx;
         edtAmbienteTecnologico_Descricao_Internalname = "AMBIENTETECNOLOGICO_DESCRICAO_"+sGXsfl_89_fel_idx;
         edtMetodologia_Codigo_Internalname = "METODOLOGIA_CODIGO_"+sGXsfl_89_fel_idx;
         edtMetodologia_Descricao_Internalname = "METODOLOGIA_DESCRICAO_"+sGXsfl_89_fel_idx;
         edtavPfb_Internalname = "vPFB_"+sGXsfl_89_fel_idx;
         edtavPfa_Internalname = "vPFA_"+sGXsfl_89_fel_idx;
         edtSistema_ProjetoNome_Internalname = "SISTEMA_PROJETONOME_"+sGXsfl_89_fel_idx;
         edtSistemaVersao_Id_Internalname = "SISTEMAVERSAO_ID_"+sGXsfl_89_fel_idx;
         chkSistema_Ativo_Internalname = "SISTEMA_ATIVO_"+sGXsfl_89_fel_idx;
         edtavAssociarclientes_Internalname = "vASSOCIARCLIENTES_"+sGXsfl_89_fel_idx;
      }

      protected void sendrow_892( )
      {
         SubsflControlProps_892( ) ;
         WB3G0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_89_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_89_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_89_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV34Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV34Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV179Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV34Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV34Update)) ? AV179Update_GXI : context.PathToRelativeUrl( AV34Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavUpdate_Visible,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV34Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV35Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV35Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV180Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV35Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV35Delete)) ? AV180Delete_GXI : context.PathToRelativeUrl( AV35Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavDelete_Visible,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV35Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDisplay_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV58Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV58Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV181Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV58Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV58Display)) ? AV181Display_GXI : context.PathToRelativeUrl( AV58Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavDisplay_Visible,(short)1,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV58Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSistema_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSistema_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)89,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSistema_Nome_Internalname,(String)A416Sistema_Nome,StringUtil.RTrim( context.localUtil.Format( A416Sistema_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSistema_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)410,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)89,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSistema_Sigla_Internalname,StringUtil.RTrim( A129Sistema_Sigla),StringUtil.RTrim( context.localUtil.Format( A129Sistema_Sigla, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSistema_Sigla_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtSistema_Sigla_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)25,(short)0,(short)0,(short)89,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavSistema_tiposigla_Enabled!=0)&&(edtavSistema_tiposigla_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 96,'',false,'"+sGXsfl_89_idx+"',89)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavSistema_tiposigla_Internalname,StringUtil.RTrim( AV130Sistema_TipoSigla),StringUtil.RTrim( context.localUtil.Format( AV130Sistema_TipoSigla, "@!")),TempTags+((edtavSistema_tiposigla_Enabled!=0)&&(edtavSistema_tiposigla_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavSistema_tiposigla_Enabled!=0)&&(edtavSistema_tiposigla_Visible!=0) ? " onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,96);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavSistema_tiposigla_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtavSistema_tiposigla_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(int)edtavSistema_tiposigla_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)89,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_89_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "SISTEMA_TECNICA_" + sGXsfl_89_idx;
               cmbSistema_Tecnica.Name = GXCCtl;
               cmbSistema_Tecnica.WebTags = "";
               cmbSistema_Tecnica.addItem("", "(Nenhum)", 0);
               cmbSistema_Tecnica.addItem("I", "Indicativa", 0);
               cmbSistema_Tecnica.addItem("E", "Estimada", 0);
               cmbSistema_Tecnica.addItem("D", "Detalhada", 0);
               if ( cmbSistema_Tecnica.ItemCount > 0 )
               {
                  A700Sistema_Tecnica = cmbSistema_Tecnica.getValidValue(A700Sistema_Tecnica);
                  n700Sistema_Tecnica = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbSistema_Tecnica,(String)cmbSistema_Tecnica_Internalname,StringUtil.RTrim( A700Sistema_Tecnica),(short)1,(String)cmbSistema_Tecnica_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px","color:"+context.BuildHTMLColor( cmbSistema_Tecnica.ForeColor)+";",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbSistema_Tecnica.CurrentValue = StringUtil.RTrim( A700Sistema_Tecnica);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbSistema_Tecnica_Internalname, "Values", (String)(cmbSistema_Tecnica.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSistema_Coordenacao_Internalname,(String)A513Sistema_Coordenacao,StringUtil.RTrim( context.localUtil.Format( A513Sistema_Coordenacao, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSistema_Coordenacao_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtSistema_Coordenacao_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)89,(short)1,(short)-1,(short)-1,(bool)true,(String)"Descricao",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAmbienteTecnologico_Descricao_Internalname,(String)A352AmbienteTecnologico_Descricao,StringUtil.RTrim( context.localUtil.Format( A352AmbienteTecnologico_Descricao, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtAmbienteTecnologico_Descricao_Link,(String)"",(String)"",(String)"",(String)edtAmbienteTecnologico_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtAmbienteTecnologico_Descricao_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)89,(short)1,(short)-1,(short)-1,(bool)true,(String)"Descricao",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtMetodologia_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A137Metodologia_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A137Metodologia_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtMetodologia_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)89,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtMetodologia_Descricao_Internalname,(String)A138Metodologia_Descricao,StringUtil.RTrim( context.localUtil.Format( A138Metodologia_Descricao, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtMetodologia_Descricao_Link,(String)"",(String)"",(String)"",(String)edtMetodologia_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtMetodologia_Descricao_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)89,(short)1,(short)-1,(short)-1,(bool)true,(String)"Descricao",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavPfb_Enabled!=0)&&(edtavPfb_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 102,'',false,'"+sGXsfl_89_idx+"',89)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavPfb_Internalname,StringUtil.LTrim( StringUtil.NToC( AV131PFB, 14, 5, ",", "")),((edtavPfb_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV131PFB, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV131PFB, "ZZ,ZZZ,ZZ9.999")),TempTags+((edtavPfb_Enabled!=0)&&(edtavPfb_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavPfb_Enabled!=0)&&(edtavPfb_Visible!=0) ? " onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,102);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavPfb_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtavPfb_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(int)edtavPfb_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)89,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavPfa_Enabled!=0)&&(edtavPfa_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 103,'',false,'"+sGXsfl_89_idx+"',89)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavPfa_Internalname,StringUtil.LTrim( StringUtil.NToC( AV133PFA, 14, 5, ",", "")),((edtavPfa_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV133PFA, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV133PFA, "ZZ,ZZZ,ZZ9.999")),TempTags+((edtavPfa_Enabled!=0)&&(edtavPfa_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavPfa_Enabled!=0)&&(edtavPfa_Visible!=0) ? " onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,103);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavPfa_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtavPfa_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(int)edtavPfa_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)89,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSistema_ProjetoNome_Internalname,StringUtil.RTrim( A703Sistema_ProjetoNome),StringUtil.RTrim( context.localUtil.Format( A703Sistema_ProjetoNome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtSistema_ProjetoNome_Link,(String)"",(String)"",(String)"",(String)edtSistema_ProjetoNome_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtSistema_ProjetoNome_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)260,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)89,(short)1,(short)-1,(short)0,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSistemaVersao_Id_Internalname,StringUtil.RTrim( A1860SistemaVersao_Id),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtSistemaVersao_Id_Link,(String)"",(String)"",(String)"",(String)edtSistemaVersao_Id_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)20,(short)0,(short)0,(short)89,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkSistema_Ativo_Internalname,StringUtil.BoolToStr( A130Sistema_Ativo),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavAssociarclientes_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavAssociarclientes_Enabled!=0)&&(edtavAssociarclientes_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 107,'',false,'',89)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV84AssociarClientes_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV84AssociarClientes))&&String.IsNullOrEmpty(StringUtil.RTrim( AV182Associarclientes_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV84AssociarClientes)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavAssociarclientes_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV84AssociarClientes)) ? AV182Associarclientes_GXI : context.PathToRelativeUrl( AV84AssociarClientes)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavAssociarclientes_Visible,(short)1,(String)"",(String)edtavAssociarclientes_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)7,(String)edtavAssociarclientes_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+"e333g2_client"+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV84AssociarClientes_IsBlob,(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_SISTEMA_CODIGO"+"_"+sGXsfl_89_idx, GetSecureSignedToken( sGXsfl_89_idx, context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_SISTEMA_NOME"+"_"+sGXsfl_89_idx, GetSecureSignedToken( sGXsfl_89_idx, StringUtil.RTrim( context.localUtil.Format( A416Sistema_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_SISTEMA_SIGLA"+"_"+sGXsfl_89_idx, GetSecureSignedToken( sGXsfl_89_idx, StringUtil.RTrim( context.localUtil.Format( A129Sistema_Sigla, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_SISTEMA_TECNICA"+"_"+sGXsfl_89_idx, GetSecureSignedToken( sGXsfl_89_idx, StringUtil.RTrim( context.localUtil.Format( A700Sistema_Tecnica, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_SISTEMA_COORDENACAO"+"_"+sGXsfl_89_idx, GetSecureSignedToken( sGXsfl_89_idx, StringUtil.RTrim( context.localUtil.Format( A513Sistema_Coordenacao, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_METODOLOGIA_CODIGO"+"_"+sGXsfl_89_idx, GetSecureSignedToken( sGXsfl_89_idx, context.localUtil.Format( (decimal)(A137Metodologia_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_SISTEMA_ATIVO"+"_"+sGXsfl_89_idx, GetSecureSignedToken( sGXsfl_89_idx, A130Sistema_Ativo));
            GridContainer.AddRow(GridRow);
            nGXsfl_89_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_89_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_89_idx+1));
            sGXsfl_89_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_89_idx), 4, 0)), 4, "0");
            SubsflControlProps_892( ) ;
         }
         /* End function sendrow_892 */
      }

      protected void init_default_properties( )
      {
         lblSistematitle_Internalname = "SISTEMATITLE";
         imgInsert_Internalname = "INSERT";
         imgExportreport_Internalname = "EXPORTREPORT";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblFiltertextsistema_areatrabalhocod_Internalname = "FILTERTEXTSISTEMA_AREATRABALHOCOD";
         edtavSistema_areatrabalhocod_Internalname = "vSISTEMA_AREATRABALHOCOD";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavSistema_nome1_Internalname = "vSISTEMA_NOME1";
         edtavSistema_sigla1_Internalname = "vSISTEMA_SIGLA1";
         cmbavSistema_tipo1_Internalname = "vSISTEMA_TIPO1";
         cmbavSistema_tecnica1_Internalname = "vSISTEMA_TECNICA1";
         edtavSistema_coordenacao1_Internalname = "vSISTEMA_COORDENACAO1";
         edtavSistema_projetonome1_Internalname = "vSISTEMA_PROJETONOME1";
         edtavSistema_pf1_Internalname = "vSISTEMA_PF1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavSistema_nome2_Internalname = "vSISTEMA_NOME2";
         edtavSistema_sigla2_Internalname = "vSISTEMA_SIGLA2";
         cmbavSistema_tipo2_Internalname = "vSISTEMA_TIPO2";
         cmbavSistema_tecnica2_Internalname = "vSISTEMA_TECNICA2";
         edtavSistema_coordenacao2_Internalname = "vSISTEMA_COORDENACAO2";
         edtavSistema_projetonome2_Internalname = "vSISTEMA_PROJETONOME2";
         edtavSistema_pf2_Internalname = "vSISTEMA_PF2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblUnnamedtable2_Internalname = "UNNAMEDTABLE2";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtavDisplay_Internalname = "vDISPLAY";
         edtSistema_Codigo_Internalname = "SISTEMA_CODIGO";
         edtSistema_Nome_Internalname = "SISTEMA_NOME";
         edtSistema_Sigla_Internalname = "SISTEMA_SIGLA";
         edtavSistema_tiposigla_Internalname = "vSISTEMA_TIPOSIGLA";
         cmbSistema_Tecnica_Internalname = "SISTEMA_TECNICA";
         edtSistema_Coordenacao_Internalname = "SISTEMA_COORDENACAO";
         edtAmbienteTecnologico_Descricao_Internalname = "AMBIENTETECNOLOGICO_DESCRICAO";
         edtMetodologia_Codigo_Internalname = "METODOLOGIA_CODIGO";
         edtMetodologia_Descricao_Internalname = "METODOLOGIA_DESCRICAO";
         edtavPfb_Internalname = "vPFB";
         edtavPfa_Internalname = "vPFA";
         edtSistema_ProjetoNome_Internalname = "SISTEMA_PROJETONOME";
         edtSistemaVersao_Id_Internalname = "SISTEMAVERSAO_ID";
         chkSistema_Ativo_Internalname = "SISTEMA_ATIVO";
         edtavAssociarclientes_Internalname = "vASSOCIARCLIENTES";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblUnnamedtable3_Internalname = "UNNAMEDTABLE3";
         tblUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         Innewwindow1_Internalname = "INNEWWINDOW1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         edtavTfsistema_sigla_Internalname = "vTFSISTEMA_SIGLA";
         edtavTfsistema_sigla_sel_Internalname = "vTFSISTEMA_SIGLA_SEL";
         edtavTfsistema_coordenacao_Internalname = "vTFSISTEMA_COORDENACAO";
         edtavTfsistema_coordenacao_sel_Internalname = "vTFSISTEMA_COORDENACAO_SEL";
         edtavTfambientetecnologico_descricao_Internalname = "vTFAMBIENTETECNOLOGICO_DESCRICAO";
         edtavTfambientetecnologico_descricao_sel_Internalname = "vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL";
         edtavTfmetodologia_descricao_Internalname = "vTFMETODOLOGIA_DESCRICAO";
         edtavTfmetodologia_descricao_sel_Internalname = "vTFMETODOLOGIA_DESCRICAO_SEL";
         edtavTfsistema_projetonome_Internalname = "vTFSISTEMA_PROJETONOME";
         edtavTfsistema_projetonome_sel_Internalname = "vTFSISTEMA_PROJETONOME_SEL";
         edtavTfsistemaversao_id_Internalname = "vTFSISTEMAVERSAO_ID";
         edtavTfsistemaversao_id_sel_Internalname = "vTFSISTEMAVERSAO_ID_SEL";
         edtavTfsistema_ativo_sel_Internalname = "vTFSISTEMA_ATIVO_SEL";
         Ddo_sistema_sigla_Internalname = "DDO_SISTEMA_SIGLA";
         edtavDdo_sistema_siglatitlecontrolidtoreplace_Internalname = "vDDO_SISTEMA_SIGLATITLECONTROLIDTOREPLACE";
         Ddo_sistema_tecnica_Internalname = "DDO_SISTEMA_TECNICA";
         edtavDdo_sistema_tecnicatitlecontrolidtoreplace_Internalname = "vDDO_SISTEMA_TECNICATITLECONTROLIDTOREPLACE";
         Ddo_sistema_coordenacao_Internalname = "DDO_SISTEMA_COORDENACAO";
         edtavDdo_sistema_coordenacaotitlecontrolidtoreplace_Internalname = "vDDO_SISTEMA_COORDENACAOTITLECONTROLIDTOREPLACE";
         Ddo_ambientetecnologico_descricao_Internalname = "DDO_AMBIENTETECNOLOGICO_DESCRICAO";
         edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Internalname = "vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE";
         Ddo_metodologia_descricao_Internalname = "DDO_METODOLOGIA_DESCRICAO";
         edtavDdo_metodologia_descricaotitlecontrolidtoreplace_Internalname = "vDDO_METODOLOGIA_DESCRICAOTITLECONTROLIDTOREPLACE";
         Ddo_sistema_projetonome_Internalname = "DDO_SISTEMA_PROJETONOME";
         edtavDdo_sistema_projetonometitlecontrolidtoreplace_Internalname = "vDDO_SISTEMA_PROJETONOMETITLECONTROLIDTOREPLACE";
         Ddo_sistemaversao_id_Internalname = "DDO_SISTEMAVERSAO_ID";
         edtavDdo_sistemaversao_idtitlecontrolidtoreplace_Internalname = "vDDO_SISTEMAVERSAO_IDTITLECONTROLIDTOREPLACE";
         Ddo_sistema_ativo_Internalname = "DDO_SISTEMA_ATIVO";
         edtavDdo_sistema_ativotitlecontrolidtoreplace_Internalname = "vDDO_SISTEMA_ATIVOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavAssociarclientes_Jsonclick = "";
         edtavAssociarclientes_Enabled = 1;
         edtSistemaVersao_Id_Jsonclick = "";
         edtSistema_ProjetoNome_Jsonclick = "";
         edtavPfa_Jsonclick = "";
         edtavPfa_Visible = -1;
         edtavPfb_Jsonclick = "";
         edtavPfb_Visible = -1;
         edtMetodologia_Descricao_Jsonclick = "";
         edtMetodologia_Codigo_Jsonclick = "";
         edtAmbienteTecnologico_Descricao_Jsonclick = "";
         edtSistema_Coordenacao_Jsonclick = "";
         cmbSistema_Tecnica_Jsonclick = "";
         edtavSistema_tiposigla_Jsonclick = "";
         edtavSistema_tiposigla_Visible = -1;
         edtSistema_Sigla_Jsonclick = "";
         edtSistema_Nome_Jsonclick = "";
         edtSistema_Codigo_Jsonclick = "";
         imgInsert_Visible = 1;
         edtavSistema_pf1_Jsonclick = "";
         edtavSistema_projetonome1_Jsonclick = "";
         edtavSistema_coordenacao1_Jsonclick = "";
         cmbavSistema_tecnica1_Jsonclick = "";
         cmbavSistema_tipo1_Jsonclick = "";
         edtavSistema_sigla1_Jsonclick = "";
         edtavSistema_nome1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavSistema_pf2_Jsonclick = "";
         edtavSistema_projetonome2_Jsonclick = "";
         edtavSistema_coordenacao2_Jsonclick = "";
         cmbavSistema_tecnica2_Jsonclick = "";
         cmbavSistema_tipo2_Jsonclick = "";
         edtavSistema_sigla2_Jsonclick = "";
         edtavSistema_nome2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavSistema_areatrabalhocod_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavAssociarclientes_Tooltiptext = "Associar os usu�riosa da contratante clientes deste sistema";
         edtSistemaVersao_Id_Link = "";
         edtSistema_ProjetoNome_Link = "";
         edtSistema_ProjetoNome_Forecolor = (int)(0xFFFFFF);
         edtavPfa_Enabled = 1;
         edtavPfa_Forecolor = (int)(0xFFFFFF);
         edtavPfb_Enabled = 1;
         edtavPfb_Forecolor = (int)(0xFFFFFF);
         edtMetodologia_Descricao_Link = "";
         edtMetodologia_Descricao_Forecolor = (int)(0xFFFFFF);
         edtAmbienteTecnologico_Descricao_Link = "";
         edtAmbienteTecnologico_Descricao_Forecolor = (int)(0xFFFFFF);
         edtSistema_Coordenacao_Forecolor = (int)(0xFFFFFF);
         cmbSistema_Tecnica.ForeColor = (int)(0xFFFFFF);
         edtavSistema_tiposigla_Enabled = 1;
         edtavSistema_tiposigla_Forecolor = (int)(0xFFFFFF);
         edtSistema_Sigla_Forecolor = (int)(0xFFFFFF);
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         chkSistema_Ativo_Titleformat = 0;
         edtSistemaVersao_Id_Titleformat = 0;
         edtSistema_ProjetoNome_Titleformat = 0;
         edtMetodologia_Descricao_Titleformat = 0;
         edtAmbienteTecnologico_Descricao_Titleformat = 0;
         edtSistema_Coordenacao_Titleformat = 0;
         cmbSistema_Tecnica_Titleformat = 0;
         edtSistema_Sigla_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavSistema_pf2_Visible = 1;
         edtavSistema_projetonome2_Visible = 1;
         edtavSistema_coordenacao2_Visible = 1;
         cmbavSistema_tecnica2.Visible = 1;
         cmbavSistema_tipo2.Visible = 1;
         edtavSistema_sigla2_Visible = 1;
         edtavSistema_nome2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavSistema_pf1_Visible = 1;
         edtavSistema_projetonome1_Visible = 1;
         edtavSistema_coordenacao1_Visible = 1;
         cmbavSistema_tecnica1.Visible = 1;
         cmbavSistema_tipo1.Visible = 1;
         edtavSistema_sigla1_Visible = 1;
         edtavSistema_nome1_Visible = 1;
         edtavDisplay_Visible = -1;
         edtavDelete_Visible = -1;
         edtavUpdate_Visible = -1;
         edtavAssociarclientes_Title = "";
         edtavAssociarclientes_Visible = -1;
         chkSistema_Ativo.Title.Text = "Ativo";
         edtSistemaVersao_Id_Title = "Vers�o";
         edtSistema_ProjetoNome_Title = "Projeto";
         edtMetodologia_Descricao_Title = "Met.";
         edtAmbienteTecnologico_Descricao_Title = "Amb.Tec.";
         edtSistema_Coordenacao_Title = "Coordena��o";
         cmbSistema_Tecnica.Title.Text = "T�cnica";
         edtSistema_Sigla_Title = "Sigla";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled2.Caption = "";
         chkSistema_Ativo.Caption = "";
         edtavDdo_sistema_ativotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_sistemaversao_idtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_sistema_projetonometitlecontrolidtoreplace_Visible = 1;
         edtavDdo_metodologia_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_sistema_coordenacaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_sistema_tecnicatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_sistema_siglatitlecontrolidtoreplace_Visible = 1;
         edtavTfsistema_ativo_sel_Jsonclick = "";
         edtavTfsistema_ativo_sel_Visible = 1;
         edtavTfsistemaversao_id_sel_Jsonclick = "";
         edtavTfsistemaversao_id_sel_Visible = 1;
         edtavTfsistemaversao_id_Jsonclick = "";
         edtavTfsistemaversao_id_Visible = 1;
         edtavTfsistema_projetonome_sel_Jsonclick = "";
         edtavTfsistema_projetonome_sel_Visible = 1;
         edtavTfsistema_projetonome_Jsonclick = "";
         edtavTfsistema_projetonome_Visible = 1;
         edtavTfmetodologia_descricao_sel_Jsonclick = "";
         edtavTfmetodologia_descricao_sel_Visible = 1;
         edtavTfmetodologia_descricao_Jsonclick = "";
         edtavTfmetodologia_descricao_Visible = 1;
         edtavTfambientetecnologico_descricao_sel_Jsonclick = "";
         edtavTfambientetecnologico_descricao_sel_Visible = 1;
         edtavTfambientetecnologico_descricao_Jsonclick = "";
         edtavTfambientetecnologico_descricao_Visible = 1;
         edtavTfsistema_coordenacao_sel_Jsonclick = "";
         edtavTfsistema_coordenacao_sel_Visible = 1;
         edtavTfsistema_coordenacao_Jsonclick = "";
         edtavTfsistema_coordenacao_Visible = 1;
         edtavTfsistema_sigla_sel_Jsonclick = "";
         edtavTfsistema_sigla_sel_Visible = 1;
         edtavTfsistema_sigla_Jsonclick = "";
         edtavTfsistema_sigla_Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_sistema_ativo_Searchbuttontext = "Pesquisar";
         Ddo_sistema_ativo_Cleanfilter = "Limpar pesquisa";
         Ddo_sistema_ativo_Sortdsc = "Ordenar de Z � A";
         Ddo_sistema_ativo_Sortasc = "Ordenar de A � Z";
         Ddo_sistema_ativo_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_sistema_ativo_Datalisttype = "FixedValues";
         Ddo_sistema_ativo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_sistema_ativo_Includefilter = Convert.ToBoolean( 0);
         Ddo_sistema_ativo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_sistema_ativo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_sistema_ativo_Titlecontrolidtoreplace = "";
         Ddo_sistema_ativo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_sistema_ativo_Cls = "ColumnSettings";
         Ddo_sistema_ativo_Tooltip = "Op��es";
         Ddo_sistema_ativo_Caption = "";
         Ddo_sistemaversao_id_Searchbuttontext = "Pesquisar";
         Ddo_sistemaversao_id_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_sistemaversao_id_Cleanfilter = "Limpar pesquisa";
         Ddo_sistemaversao_id_Loadingdata = "Carregando dados...";
         Ddo_sistemaversao_id_Sortdsc = "Ordenar de Z � A";
         Ddo_sistemaversao_id_Sortasc = "Ordenar de A � Z";
         Ddo_sistemaversao_id_Datalistupdateminimumcharacters = 0;
         Ddo_sistemaversao_id_Datalistproc = "GetWWSistemaFilterData";
         Ddo_sistemaversao_id_Datalisttype = "Dynamic";
         Ddo_sistemaversao_id_Includedatalist = Convert.ToBoolean( -1);
         Ddo_sistemaversao_id_Filterisrange = Convert.ToBoolean( 0);
         Ddo_sistemaversao_id_Filtertype = "Character";
         Ddo_sistemaversao_id_Includefilter = Convert.ToBoolean( -1);
         Ddo_sistemaversao_id_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_sistemaversao_id_Includesortasc = Convert.ToBoolean( -1);
         Ddo_sistemaversao_id_Titlecontrolidtoreplace = "";
         Ddo_sistemaversao_id_Dropdownoptionstype = "GridTitleSettings";
         Ddo_sistemaversao_id_Cls = "ColumnSettings";
         Ddo_sistemaversao_id_Tooltip = "Op��es";
         Ddo_sistemaversao_id_Caption = "";
         Ddo_sistema_projetonome_Searchbuttontext = "Pesquisar";
         Ddo_sistema_projetonome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_sistema_projetonome_Cleanfilter = "Limpar pesquisa";
         Ddo_sistema_projetonome_Loadingdata = "Carregando dados...";
         Ddo_sistema_projetonome_Sortdsc = "Ordenar de Z � A";
         Ddo_sistema_projetonome_Sortasc = "Ordenar de A � Z";
         Ddo_sistema_projetonome_Datalistupdateminimumcharacters = 0;
         Ddo_sistema_projetonome_Datalistproc = "GetWWSistemaFilterData";
         Ddo_sistema_projetonome_Datalisttype = "Dynamic";
         Ddo_sistema_projetonome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_sistema_projetonome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_sistema_projetonome_Filtertype = "Character";
         Ddo_sistema_projetonome_Includefilter = Convert.ToBoolean( -1);
         Ddo_sistema_projetonome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_sistema_projetonome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_sistema_projetonome_Titlecontrolidtoreplace = "";
         Ddo_sistema_projetonome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_sistema_projetonome_Cls = "ColumnSettings";
         Ddo_sistema_projetonome_Tooltip = "Op��es";
         Ddo_sistema_projetonome_Caption = "";
         Ddo_metodologia_descricao_Searchbuttontext = "Pesquisar";
         Ddo_metodologia_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_metodologia_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_metodologia_descricao_Loadingdata = "Carregando dados...";
         Ddo_metodologia_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_metodologia_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_metodologia_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_metodologia_descricao_Datalistproc = "GetWWSistemaFilterData";
         Ddo_metodologia_descricao_Datalisttype = "Dynamic";
         Ddo_metodologia_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_metodologia_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_metodologia_descricao_Filtertype = "Character";
         Ddo_metodologia_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_metodologia_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_metodologia_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_metodologia_descricao_Titlecontrolidtoreplace = "";
         Ddo_metodologia_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_metodologia_descricao_Cls = "ColumnSettings";
         Ddo_metodologia_descricao_Tooltip = "Op��es";
         Ddo_metodologia_descricao_Caption = "";
         Ddo_ambientetecnologico_descricao_Searchbuttontext = "Pesquisar";
         Ddo_ambientetecnologico_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_ambientetecnologico_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_ambientetecnologico_descricao_Loadingdata = "Carregando dados...";
         Ddo_ambientetecnologico_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_ambientetecnologico_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_ambientetecnologico_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_ambientetecnologico_descricao_Datalistproc = "GetWWSistemaFilterData";
         Ddo_ambientetecnologico_descricao_Datalisttype = "Dynamic";
         Ddo_ambientetecnologico_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_ambientetecnologico_descricao_Filtertype = "Character";
         Ddo_ambientetecnologico_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_ambientetecnologico_descricao_Titlecontrolidtoreplace = "";
         Ddo_ambientetecnologico_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_ambientetecnologico_descricao_Cls = "ColumnSettings";
         Ddo_ambientetecnologico_descricao_Tooltip = "Op��es";
         Ddo_ambientetecnologico_descricao_Caption = "";
         Ddo_sistema_coordenacao_Searchbuttontext = "Pesquisar";
         Ddo_sistema_coordenacao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_sistema_coordenacao_Cleanfilter = "Limpar pesquisa";
         Ddo_sistema_coordenacao_Loadingdata = "Carregando dados...";
         Ddo_sistema_coordenacao_Sortdsc = "Ordenar de Z � A";
         Ddo_sistema_coordenacao_Sortasc = "Ordenar de A � Z";
         Ddo_sistema_coordenacao_Datalistupdateminimumcharacters = 0;
         Ddo_sistema_coordenacao_Datalistproc = "GetWWSistemaFilterData";
         Ddo_sistema_coordenacao_Datalisttype = "Dynamic";
         Ddo_sistema_coordenacao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_sistema_coordenacao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_sistema_coordenacao_Filtertype = "Character";
         Ddo_sistema_coordenacao_Includefilter = Convert.ToBoolean( -1);
         Ddo_sistema_coordenacao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_sistema_coordenacao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_sistema_coordenacao_Titlecontrolidtoreplace = "";
         Ddo_sistema_coordenacao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_sistema_coordenacao_Cls = "ColumnSettings";
         Ddo_sistema_coordenacao_Tooltip = "Op��es";
         Ddo_sistema_coordenacao_Caption = "";
         Ddo_sistema_tecnica_Searchbuttontext = "Filtrar Selecionados";
         Ddo_sistema_tecnica_Cleanfilter = "Limpar pesquisa";
         Ddo_sistema_tecnica_Sortdsc = "Ordenar de Z � A";
         Ddo_sistema_tecnica_Sortasc = "Ordenar de A � Z";
         Ddo_sistema_tecnica_Datalistfixedvalues = "I:Indicativa,E:Estimada,D:Detalhada";
         Ddo_sistema_tecnica_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_sistema_tecnica_Datalisttype = "FixedValues";
         Ddo_sistema_tecnica_Includedatalist = Convert.ToBoolean( -1);
         Ddo_sistema_tecnica_Includefilter = Convert.ToBoolean( 0);
         Ddo_sistema_tecnica_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_sistema_tecnica_Includesortasc = Convert.ToBoolean( -1);
         Ddo_sistema_tecnica_Titlecontrolidtoreplace = "";
         Ddo_sistema_tecnica_Dropdownoptionstype = "GridTitleSettings";
         Ddo_sistema_tecnica_Cls = "ColumnSettings";
         Ddo_sistema_tecnica_Tooltip = "Op��es";
         Ddo_sistema_tecnica_Caption = "";
         Ddo_sistema_sigla_Searchbuttontext = "Pesquisar";
         Ddo_sistema_sigla_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_sistema_sigla_Cleanfilter = "Limpar pesquisa";
         Ddo_sistema_sigla_Loadingdata = "Carregando dados...";
         Ddo_sistema_sigla_Sortdsc = "Ordenar de Z � A";
         Ddo_sistema_sigla_Sortasc = "Ordenar de A � Z";
         Ddo_sistema_sigla_Datalistupdateminimumcharacters = 0;
         Ddo_sistema_sigla_Datalistproc = "GetWWSistemaFilterData";
         Ddo_sistema_sigla_Datalisttype = "Dynamic";
         Ddo_sistema_sigla_Includedatalist = Convert.ToBoolean( -1);
         Ddo_sistema_sigla_Filterisrange = Convert.ToBoolean( 0);
         Ddo_sistema_sigla_Filtertype = "Character";
         Ddo_sistema_sigla_Includefilter = Convert.ToBoolean( -1);
         Ddo_sistema_sigla_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_sistema_sigla_Includesortasc = Convert.ToBoolean( -1);
         Ddo_sistema_sigla_Titlecontrolidtoreplace = "";
         Ddo_sistema_sigla_Dropdownoptionstype = "GridTitleSettings";
         Ddo_sistema_sigla_Cls = "ColumnSettings";
         Ddo_sistema_sigla_Tooltip = "Op��es";
         Ddo_sistema_sigla_Caption = "";
         Innewwindow1_Target = "";
         Innewwindow1_Height = "50";
         Innewwindow1_Width = "50";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Sistema";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A351AmbienteTecnologico_Codigo',fld:'AMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A137Metodologia_Codigo',fld:'METODOLOGIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A691Sistema_ProjetoCod',fld:'SISTEMA_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'A1859SistemaVersao_Codigo',fld:'SISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A130Sistema_Ativo',fld:'SISTEMA_ATIVO',pic:'',hsh:true,nv:false},{av:'A707Sistema_TipoSigla',fld:'SISTEMA_TIPOSIGLA',pic:'@!',nv:''},{av:'A395Sistema_PF',fld:'SISTEMA_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A690Sistema_PFA',fld:'SISTEMA_PFA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV90ddo_Sistema_SiglaTitleControlIdToReplace',fld:'vDDO_SISTEMA_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_Sistema_TecnicaTitleControlIdToReplace',fld:'vDDO_SISTEMA_TECNICATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Sistema_CoordenacaoTitleControlIdToReplace',fld:'vDDO_SISTEMA_COORDENACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_Metodologia_DescricaoTitleControlIdToReplace',fld:'vDDO_METODOLOGIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_Sistema_ProjetoNomeTitleControlIdToReplace',fld:'vDDO_SISTEMA_PROJETONOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV142ddo_SistemaVersao_IdTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_IDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV125ddo_Sistema_AtivoTitleControlIdToReplace',fld:'vDDO_SISTEMA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV137Paginando',fld:'vPAGINANDO',pic:'',nv:false},{av:'AV134Registros',fld:'vREGISTROS',pic:'ZZZZZZZZZ9',nv:0},{av:'AV135PreView',fld:'vPREVIEW',pic:'ZZZ9',nv:0},{av:'AV136PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZZZZZZZ9',nv:0},{av:'AV60Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV67Sistema_Nome1',fld:'vSISTEMA_NOME1',pic:'@!',nv:''},{av:'AV62Sistema_Sigla1',fld:'vSISTEMA_SIGLA1',pic:'@!',nv:''},{av:'AV73Sistema_Tipo1',fld:'vSISTEMA_TIPO1',pic:'',nv:''},{av:'AV75Sistema_Tecnica1',fld:'vSISTEMA_TECNICA1',pic:'',nv:''},{av:'AV76Sistema_Coordenacao1',fld:'vSISTEMA_COORDENACAO1',pic:'@!',nv:''},{av:'AV80Sistema_ProjetoNome1',fld:'vSISTEMA_PROJETONOME1',pic:'@!',nv:''},{av:'AV82Sistema_PF1',fld:'vSISTEMA_PF1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV68Sistema_Nome2',fld:'vSISTEMA_NOME2',pic:'@!',nv:''},{av:'AV63Sistema_Sigla2',fld:'vSISTEMA_SIGLA2',pic:'@!',nv:''},{av:'AV74Sistema_Tipo2',fld:'vSISTEMA_TIPO2',pic:'',nv:''},{av:'AV77Sistema_Tecnica2',fld:'vSISTEMA_TECNICA2',pic:'',nv:''},{av:'AV78Sistema_Coordenacao2',fld:'vSISTEMA_COORDENACAO2',pic:'@!',nv:''},{av:'AV81Sistema_ProjetoNome2',fld:'vSISTEMA_PROJETONOME2',pic:'@!',nv:''},{av:'AV83Sistema_PF2',fld:'vSISTEMA_PF2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV88TFSistema_Sigla',fld:'vTFSISTEMA_SIGLA',pic:'@!',nv:''},{av:'AV89TFSistema_Sigla_Sel',fld:'vTFSISTEMA_SIGLA_SEL',pic:'@!',nv:''},{av:'AV97TFSistema_Tecnica_Sels',fld:'vTFSISTEMA_TECNICA_SELS',pic:'',nv:null},{av:'AV100TFSistema_Coordenacao',fld:'vTFSISTEMA_COORDENACAO',pic:'@!',nv:''},{av:'AV101TFSistema_Coordenacao_Sel',fld:'vTFSISTEMA_COORDENACAO_SEL',pic:'@!',nv:''},{av:'AV104TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV105TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV108TFMetodologia_Descricao',fld:'vTFMETODOLOGIA_DESCRICAO',pic:'@!',nv:''},{av:'AV109TFMetodologia_Descricao_Sel',fld:'vTFMETODOLOGIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV120TFSistema_ProjetoNome',fld:'vTFSISTEMA_PROJETONOME',pic:'@!',nv:''},{av:'AV121TFSistema_ProjetoNome_Sel',fld:'vTFSISTEMA_PROJETONOME_SEL',pic:'@!',nv:''},{av:'AV140TFSistemaVersao_Id',fld:'vTFSISTEMAVERSAO_ID',pic:'',nv:''},{av:'AV141TFSistemaVersao_Id_Sel',fld:'vTFSISTEMAVERSAO_ID_SEL',pic:'',nv:''},{av:'AV124TFSistema_Ativo_Sel',fld:'vTFSISTEMA_ATIVO_SEL',pic:'9',nv:0},{av:'AV183Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV87Sistema_SiglaTitleFilterData',fld:'vSISTEMA_SIGLATITLEFILTERDATA',pic:'',nv:null},{av:'AV95Sistema_TecnicaTitleFilterData',fld:'vSISTEMA_TECNICATITLEFILTERDATA',pic:'',nv:null},{av:'AV99Sistema_CoordenacaoTitleFilterData',fld:'vSISTEMA_COORDENACAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV103AmbienteTecnologico_DescricaoTitleFilterData',fld:'vAMBIENTETECNOLOGICO_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV107Metodologia_DescricaoTitleFilterData',fld:'vMETODOLOGIA_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV119Sistema_ProjetoNomeTitleFilterData',fld:'vSISTEMA_PROJETONOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV139SistemaVersao_IdTitleFilterData',fld:'vSISTEMAVERSAO_IDTITLEFILTERDATA',pic:'',nv:null},{av:'AV123Sistema_AtivoTitleFilterData',fld:'vSISTEMA_ATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtSistema_Sigla_Titleformat',ctrl:'SISTEMA_SIGLA',prop:'Titleformat'},{av:'edtSistema_Sigla_Title',ctrl:'SISTEMA_SIGLA',prop:'Title'},{av:'cmbSistema_Tecnica'},{av:'edtSistema_Coordenacao_Titleformat',ctrl:'SISTEMA_COORDENACAO',prop:'Titleformat'},{av:'edtSistema_Coordenacao_Title',ctrl:'SISTEMA_COORDENACAO',prop:'Title'},{av:'edtAmbienteTecnologico_Descricao_Titleformat',ctrl:'AMBIENTETECNOLOGICO_DESCRICAO',prop:'Titleformat'},{av:'edtAmbienteTecnologico_Descricao_Title',ctrl:'AMBIENTETECNOLOGICO_DESCRICAO',prop:'Title'},{av:'edtMetodologia_Descricao_Titleformat',ctrl:'METODOLOGIA_DESCRICAO',prop:'Titleformat'},{av:'edtMetodologia_Descricao_Title',ctrl:'METODOLOGIA_DESCRICAO',prop:'Title'},{av:'edtSistema_ProjetoNome_Titleformat',ctrl:'SISTEMA_PROJETONOME',prop:'Titleformat'},{av:'edtSistema_ProjetoNome_Title',ctrl:'SISTEMA_PROJETONOME',prop:'Title'},{av:'edtSistemaVersao_Id_Titleformat',ctrl:'SISTEMAVERSAO_ID',prop:'Titleformat'},{av:'edtSistemaVersao_Id_Title',ctrl:'SISTEMAVERSAO_ID',prop:'Title'},{av:'chkSistema_Ativo_Titleformat',ctrl:'SISTEMA_ATIVO',prop:'Titleformat'},{av:'chkSistema_Ativo.Title.Text',ctrl:'SISTEMA_ATIVO',prop:'Title'},{av:'AV128GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV129GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV137Paginando',fld:'vPAGINANDO',pic:'',nv:false},{av:'AV136PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZZZZZZZ9',nv:0},{av:'edtavAssociarclientes_Visible',ctrl:'vASSOCIARCLIENTES',prop:'Visible'},{av:'edtavAssociarclientes_Title',ctrl:'vASSOCIARCLIENTES',prop:'Title'},{av:'imgInsert_Visible',ctrl:'INSERT',prop:'Visible'},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'},{av:'edtavDisplay_Visible',ctrl:'vDISPLAY',prop:'Visible'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV134Registros',fld:'vREGISTROS',pic:'ZZZZZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E113G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV67Sistema_Nome1',fld:'vSISTEMA_NOME1',pic:'@!',nv:''},{av:'AV62Sistema_Sigla1',fld:'vSISTEMA_SIGLA1',pic:'@!',nv:''},{av:'AV73Sistema_Tipo1',fld:'vSISTEMA_TIPO1',pic:'',nv:''},{av:'AV75Sistema_Tecnica1',fld:'vSISTEMA_TECNICA1',pic:'',nv:''},{av:'AV76Sistema_Coordenacao1',fld:'vSISTEMA_COORDENACAO1',pic:'@!',nv:''},{av:'AV80Sistema_ProjetoNome1',fld:'vSISTEMA_PROJETONOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV68Sistema_Nome2',fld:'vSISTEMA_NOME2',pic:'@!',nv:''},{av:'AV63Sistema_Sigla2',fld:'vSISTEMA_SIGLA2',pic:'@!',nv:''},{av:'AV74Sistema_Tipo2',fld:'vSISTEMA_TIPO2',pic:'',nv:''},{av:'AV77Sistema_Tecnica2',fld:'vSISTEMA_TECNICA2',pic:'',nv:''},{av:'AV78Sistema_Coordenacao2',fld:'vSISTEMA_COORDENACAO2',pic:'@!',nv:''},{av:'AV81Sistema_ProjetoNome2',fld:'vSISTEMA_PROJETONOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV88TFSistema_Sigla',fld:'vTFSISTEMA_SIGLA',pic:'@!',nv:''},{av:'AV89TFSistema_Sigla_Sel',fld:'vTFSISTEMA_SIGLA_SEL',pic:'@!',nv:''},{av:'AV100TFSistema_Coordenacao',fld:'vTFSISTEMA_COORDENACAO',pic:'@!',nv:''},{av:'AV101TFSistema_Coordenacao_Sel',fld:'vTFSISTEMA_COORDENACAO_SEL',pic:'@!',nv:''},{av:'AV104TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV105TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV108TFMetodologia_Descricao',fld:'vTFMETODOLOGIA_DESCRICAO',pic:'@!',nv:''},{av:'AV109TFMetodologia_Descricao_Sel',fld:'vTFMETODOLOGIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV120TFSistema_ProjetoNome',fld:'vTFSISTEMA_PROJETONOME',pic:'@!',nv:''},{av:'AV121TFSistema_ProjetoNome_Sel',fld:'vTFSISTEMA_PROJETONOME_SEL',pic:'@!',nv:''},{av:'AV140TFSistemaVersao_Id',fld:'vTFSISTEMAVERSAO_ID',pic:'',nv:''},{av:'AV141TFSistemaVersao_Id_Sel',fld:'vTFSISTEMAVERSAO_ID_SEL',pic:'',nv:''},{av:'AV124TFSistema_Ativo_Sel',fld:'vTFSISTEMA_ATIVO_SEL',pic:'9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV90ddo_Sistema_SiglaTitleControlIdToReplace',fld:'vDDO_SISTEMA_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_Sistema_TecnicaTitleControlIdToReplace',fld:'vDDO_SISTEMA_TECNICATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Sistema_CoordenacaoTitleControlIdToReplace',fld:'vDDO_SISTEMA_COORDENACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_Metodologia_DescricaoTitleControlIdToReplace',fld:'vDDO_METODOLOGIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_Sistema_ProjetoNomeTitleControlIdToReplace',fld:'vDDO_SISTEMA_PROJETONOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV142ddo_SistemaVersao_IdTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_IDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV125ddo_Sistema_AtivoTitleControlIdToReplace',fld:'vDDO_SISTEMA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV137Paginando',fld:'vPAGINANDO',pic:'',nv:false},{av:'AV134Registros',fld:'vREGISTROS',pic:'ZZZZZZZZZ9',nv:0},{av:'AV135PreView',fld:'vPREVIEW',pic:'ZZZ9',nv:0},{av:'AV136PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZZZZZZZ9',nv:0},{av:'AV60Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV82Sistema_PF1',fld:'vSISTEMA_PF1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV83Sistema_PF2',fld:'vSISTEMA_PF2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV97TFSistema_Tecnica_Sels',fld:'vTFSISTEMA_TECNICA_SELS',pic:'',nv:null},{av:'AV183Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A351AmbienteTecnologico_Codigo',fld:'AMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A137Metodologia_Codigo',fld:'METODOLOGIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A691Sistema_ProjetoCod',fld:'SISTEMA_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'A1859SistemaVersao_Codigo',fld:'SISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A130Sistema_Ativo',fld:'SISTEMA_ATIVO',pic:'',hsh:true,nv:false},{av:'A707Sistema_TipoSigla',fld:'SISTEMA_TIPOSIGLA',pic:'@!',nv:''},{av:'A395Sistema_PF',fld:'SISTEMA_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A690Sistema_PFA',fld:'SISTEMA_PFA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[{av:'AV136PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZZZZZZZ9',nv:0},{av:'AV137Paginando',fld:'vPAGINANDO',pic:'',nv:false}]}");
         setEventMetadata("DDO_SISTEMA_SIGLA.ONOPTIONCLICKED","{handler:'E123G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV67Sistema_Nome1',fld:'vSISTEMA_NOME1',pic:'@!',nv:''},{av:'AV62Sistema_Sigla1',fld:'vSISTEMA_SIGLA1',pic:'@!',nv:''},{av:'AV73Sistema_Tipo1',fld:'vSISTEMA_TIPO1',pic:'',nv:''},{av:'AV75Sistema_Tecnica1',fld:'vSISTEMA_TECNICA1',pic:'',nv:''},{av:'AV76Sistema_Coordenacao1',fld:'vSISTEMA_COORDENACAO1',pic:'@!',nv:''},{av:'AV80Sistema_ProjetoNome1',fld:'vSISTEMA_PROJETONOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV68Sistema_Nome2',fld:'vSISTEMA_NOME2',pic:'@!',nv:''},{av:'AV63Sistema_Sigla2',fld:'vSISTEMA_SIGLA2',pic:'@!',nv:''},{av:'AV74Sistema_Tipo2',fld:'vSISTEMA_TIPO2',pic:'',nv:''},{av:'AV77Sistema_Tecnica2',fld:'vSISTEMA_TECNICA2',pic:'',nv:''},{av:'AV78Sistema_Coordenacao2',fld:'vSISTEMA_COORDENACAO2',pic:'@!',nv:''},{av:'AV81Sistema_ProjetoNome2',fld:'vSISTEMA_PROJETONOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV88TFSistema_Sigla',fld:'vTFSISTEMA_SIGLA',pic:'@!',nv:''},{av:'AV89TFSistema_Sigla_Sel',fld:'vTFSISTEMA_SIGLA_SEL',pic:'@!',nv:''},{av:'AV100TFSistema_Coordenacao',fld:'vTFSISTEMA_COORDENACAO',pic:'@!',nv:''},{av:'AV101TFSistema_Coordenacao_Sel',fld:'vTFSISTEMA_COORDENACAO_SEL',pic:'@!',nv:''},{av:'AV104TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV105TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV108TFMetodologia_Descricao',fld:'vTFMETODOLOGIA_DESCRICAO',pic:'@!',nv:''},{av:'AV109TFMetodologia_Descricao_Sel',fld:'vTFMETODOLOGIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV120TFSistema_ProjetoNome',fld:'vTFSISTEMA_PROJETONOME',pic:'@!',nv:''},{av:'AV121TFSistema_ProjetoNome_Sel',fld:'vTFSISTEMA_PROJETONOME_SEL',pic:'@!',nv:''},{av:'AV140TFSistemaVersao_Id',fld:'vTFSISTEMAVERSAO_ID',pic:'',nv:''},{av:'AV141TFSistemaVersao_Id_Sel',fld:'vTFSISTEMAVERSAO_ID_SEL',pic:'',nv:''},{av:'AV124TFSistema_Ativo_Sel',fld:'vTFSISTEMA_ATIVO_SEL',pic:'9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV90ddo_Sistema_SiglaTitleControlIdToReplace',fld:'vDDO_SISTEMA_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_Sistema_TecnicaTitleControlIdToReplace',fld:'vDDO_SISTEMA_TECNICATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Sistema_CoordenacaoTitleControlIdToReplace',fld:'vDDO_SISTEMA_COORDENACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_Metodologia_DescricaoTitleControlIdToReplace',fld:'vDDO_METODOLOGIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_Sistema_ProjetoNomeTitleControlIdToReplace',fld:'vDDO_SISTEMA_PROJETONOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV142ddo_SistemaVersao_IdTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_IDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV125ddo_Sistema_AtivoTitleControlIdToReplace',fld:'vDDO_SISTEMA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV137Paginando',fld:'vPAGINANDO',pic:'',nv:false},{av:'AV134Registros',fld:'vREGISTROS',pic:'ZZZZZZZZZ9',nv:0},{av:'AV135PreView',fld:'vPREVIEW',pic:'ZZZ9',nv:0},{av:'AV136PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZZZZZZZ9',nv:0},{av:'AV60Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV82Sistema_PF1',fld:'vSISTEMA_PF1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV83Sistema_PF2',fld:'vSISTEMA_PF2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV97TFSistema_Tecnica_Sels',fld:'vTFSISTEMA_TECNICA_SELS',pic:'',nv:null},{av:'AV183Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A351AmbienteTecnologico_Codigo',fld:'AMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A137Metodologia_Codigo',fld:'METODOLOGIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A691Sistema_ProjetoCod',fld:'SISTEMA_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'A1859SistemaVersao_Codigo',fld:'SISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A130Sistema_Ativo',fld:'SISTEMA_ATIVO',pic:'',hsh:true,nv:false},{av:'A707Sistema_TipoSigla',fld:'SISTEMA_TIPOSIGLA',pic:'@!',nv:''},{av:'A395Sistema_PF',fld:'SISTEMA_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A690Sistema_PFA',fld:'SISTEMA_PFA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_sistema_sigla_Activeeventkey',ctrl:'DDO_SISTEMA_SIGLA',prop:'ActiveEventKey'},{av:'Ddo_sistema_sigla_Filteredtext_get',ctrl:'DDO_SISTEMA_SIGLA',prop:'FilteredText_get'},{av:'Ddo_sistema_sigla_Selectedvalue_get',ctrl:'DDO_SISTEMA_SIGLA',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_sistema_sigla_Sortedstatus',ctrl:'DDO_SISTEMA_SIGLA',prop:'SortedStatus'},{av:'AV88TFSistema_Sigla',fld:'vTFSISTEMA_SIGLA',pic:'@!',nv:''},{av:'AV89TFSistema_Sigla_Sel',fld:'vTFSISTEMA_SIGLA_SEL',pic:'@!',nv:''},{av:'Ddo_sistema_tecnica_Sortedstatus',ctrl:'DDO_SISTEMA_TECNICA',prop:'SortedStatus'},{av:'Ddo_sistema_coordenacao_Sortedstatus',ctrl:'DDO_SISTEMA_COORDENACAO',prop:'SortedStatus'},{av:'Ddo_ambientetecnologico_descricao_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_metodologia_descricao_Sortedstatus',ctrl:'DDO_METODOLOGIA_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_sistema_projetonome_Sortedstatus',ctrl:'DDO_SISTEMA_PROJETONOME',prop:'SortedStatus'},{av:'Ddo_sistemaversao_id_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_ID',prop:'SortedStatus'},{av:'Ddo_sistema_ativo_Sortedstatus',ctrl:'DDO_SISTEMA_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SISTEMA_TECNICA.ONOPTIONCLICKED","{handler:'E133G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV67Sistema_Nome1',fld:'vSISTEMA_NOME1',pic:'@!',nv:''},{av:'AV62Sistema_Sigla1',fld:'vSISTEMA_SIGLA1',pic:'@!',nv:''},{av:'AV73Sistema_Tipo1',fld:'vSISTEMA_TIPO1',pic:'',nv:''},{av:'AV75Sistema_Tecnica1',fld:'vSISTEMA_TECNICA1',pic:'',nv:''},{av:'AV76Sistema_Coordenacao1',fld:'vSISTEMA_COORDENACAO1',pic:'@!',nv:''},{av:'AV80Sistema_ProjetoNome1',fld:'vSISTEMA_PROJETONOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV68Sistema_Nome2',fld:'vSISTEMA_NOME2',pic:'@!',nv:''},{av:'AV63Sistema_Sigla2',fld:'vSISTEMA_SIGLA2',pic:'@!',nv:''},{av:'AV74Sistema_Tipo2',fld:'vSISTEMA_TIPO2',pic:'',nv:''},{av:'AV77Sistema_Tecnica2',fld:'vSISTEMA_TECNICA2',pic:'',nv:''},{av:'AV78Sistema_Coordenacao2',fld:'vSISTEMA_COORDENACAO2',pic:'@!',nv:''},{av:'AV81Sistema_ProjetoNome2',fld:'vSISTEMA_PROJETONOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV88TFSistema_Sigla',fld:'vTFSISTEMA_SIGLA',pic:'@!',nv:''},{av:'AV89TFSistema_Sigla_Sel',fld:'vTFSISTEMA_SIGLA_SEL',pic:'@!',nv:''},{av:'AV100TFSistema_Coordenacao',fld:'vTFSISTEMA_COORDENACAO',pic:'@!',nv:''},{av:'AV101TFSistema_Coordenacao_Sel',fld:'vTFSISTEMA_COORDENACAO_SEL',pic:'@!',nv:''},{av:'AV104TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV105TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV108TFMetodologia_Descricao',fld:'vTFMETODOLOGIA_DESCRICAO',pic:'@!',nv:''},{av:'AV109TFMetodologia_Descricao_Sel',fld:'vTFMETODOLOGIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV120TFSistema_ProjetoNome',fld:'vTFSISTEMA_PROJETONOME',pic:'@!',nv:''},{av:'AV121TFSistema_ProjetoNome_Sel',fld:'vTFSISTEMA_PROJETONOME_SEL',pic:'@!',nv:''},{av:'AV140TFSistemaVersao_Id',fld:'vTFSISTEMAVERSAO_ID',pic:'',nv:''},{av:'AV141TFSistemaVersao_Id_Sel',fld:'vTFSISTEMAVERSAO_ID_SEL',pic:'',nv:''},{av:'AV124TFSistema_Ativo_Sel',fld:'vTFSISTEMA_ATIVO_SEL',pic:'9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV90ddo_Sistema_SiglaTitleControlIdToReplace',fld:'vDDO_SISTEMA_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_Sistema_TecnicaTitleControlIdToReplace',fld:'vDDO_SISTEMA_TECNICATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Sistema_CoordenacaoTitleControlIdToReplace',fld:'vDDO_SISTEMA_COORDENACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_Metodologia_DescricaoTitleControlIdToReplace',fld:'vDDO_METODOLOGIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_Sistema_ProjetoNomeTitleControlIdToReplace',fld:'vDDO_SISTEMA_PROJETONOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV142ddo_SistemaVersao_IdTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_IDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV125ddo_Sistema_AtivoTitleControlIdToReplace',fld:'vDDO_SISTEMA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV137Paginando',fld:'vPAGINANDO',pic:'',nv:false},{av:'AV134Registros',fld:'vREGISTROS',pic:'ZZZZZZZZZ9',nv:0},{av:'AV135PreView',fld:'vPREVIEW',pic:'ZZZ9',nv:0},{av:'AV136PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZZZZZZZ9',nv:0},{av:'AV60Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV82Sistema_PF1',fld:'vSISTEMA_PF1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV83Sistema_PF2',fld:'vSISTEMA_PF2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV97TFSistema_Tecnica_Sels',fld:'vTFSISTEMA_TECNICA_SELS',pic:'',nv:null},{av:'AV183Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A351AmbienteTecnologico_Codigo',fld:'AMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A137Metodologia_Codigo',fld:'METODOLOGIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A691Sistema_ProjetoCod',fld:'SISTEMA_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'A1859SistemaVersao_Codigo',fld:'SISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A130Sistema_Ativo',fld:'SISTEMA_ATIVO',pic:'',hsh:true,nv:false},{av:'A707Sistema_TipoSigla',fld:'SISTEMA_TIPOSIGLA',pic:'@!',nv:''},{av:'A395Sistema_PF',fld:'SISTEMA_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A690Sistema_PFA',fld:'SISTEMA_PFA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_sistema_tecnica_Activeeventkey',ctrl:'DDO_SISTEMA_TECNICA',prop:'ActiveEventKey'},{av:'Ddo_sistema_tecnica_Selectedvalue_get',ctrl:'DDO_SISTEMA_TECNICA',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_sistema_tecnica_Sortedstatus',ctrl:'DDO_SISTEMA_TECNICA',prop:'SortedStatus'},{av:'AV97TFSistema_Tecnica_Sels',fld:'vTFSISTEMA_TECNICA_SELS',pic:'',nv:null},{av:'Ddo_sistema_sigla_Sortedstatus',ctrl:'DDO_SISTEMA_SIGLA',prop:'SortedStatus'},{av:'Ddo_sistema_coordenacao_Sortedstatus',ctrl:'DDO_SISTEMA_COORDENACAO',prop:'SortedStatus'},{av:'Ddo_ambientetecnologico_descricao_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_metodologia_descricao_Sortedstatus',ctrl:'DDO_METODOLOGIA_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_sistema_projetonome_Sortedstatus',ctrl:'DDO_SISTEMA_PROJETONOME',prop:'SortedStatus'},{av:'Ddo_sistemaversao_id_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_ID',prop:'SortedStatus'},{av:'Ddo_sistema_ativo_Sortedstatus',ctrl:'DDO_SISTEMA_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SISTEMA_COORDENACAO.ONOPTIONCLICKED","{handler:'E143G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV67Sistema_Nome1',fld:'vSISTEMA_NOME1',pic:'@!',nv:''},{av:'AV62Sistema_Sigla1',fld:'vSISTEMA_SIGLA1',pic:'@!',nv:''},{av:'AV73Sistema_Tipo1',fld:'vSISTEMA_TIPO1',pic:'',nv:''},{av:'AV75Sistema_Tecnica1',fld:'vSISTEMA_TECNICA1',pic:'',nv:''},{av:'AV76Sistema_Coordenacao1',fld:'vSISTEMA_COORDENACAO1',pic:'@!',nv:''},{av:'AV80Sistema_ProjetoNome1',fld:'vSISTEMA_PROJETONOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV68Sistema_Nome2',fld:'vSISTEMA_NOME2',pic:'@!',nv:''},{av:'AV63Sistema_Sigla2',fld:'vSISTEMA_SIGLA2',pic:'@!',nv:''},{av:'AV74Sistema_Tipo2',fld:'vSISTEMA_TIPO2',pic:'',nv:''},{av:'AV77Sistema_Tecnica2',fld:'vSISTEMA_TECNICA2',pic:'',nv:''},{av:'AV78Sistema_Coordenacao2',fld:'vSISTEMA_COORDENACAO2',pic:'@!',nv:''},{av:'AV81Sistema_ProjetoNome2',fld:'vSISTEMA_PROJETONOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV88TFSistema_Sigla',fld:'vTFSISTEMA_SIGLA',pic:'@!',nv:''},{av:'AV89TFSistema_Sigla_Sel',fld:'vTFSISTEMA_SIGLA_SEL',pic:'@!',nv:''},{av:'AV100TFSistema_Coordenacao',fld:'vTFSISTEMA_COORDENACAO',pic:'@!',nv:''},{av:'AV101TFSistema_Coordenacao_Sel',fld:'vTFSISTEMA_COORDENACAO_SEL',pic:'@!',nv:''},{av:'AV104TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV105TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV108TFMetodologia_Descricao',fld:'vTFMETODOLOGIA_DESCRICAO',pic:'@!',nv:''},{av:'AV109TFMetodologia_Descricao_Sel',fld:'vTFMETODOLOGIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV120TFSistema_ProjetoNome',fld:'vTFSISTEMA_PROJETONOME',pic:'@!',nv:''},{av:'AV121TFSistema_ProjetoNome_Sel',fld:'vTFSISTEMA_PROJETONOME_SEL',pic:'@!',nv:''},{av:'AV140TFSistemaVersao_Id',fld:'vTFSISTEMAVERSAO_ID',pic:'',nv:''},{av:'AV141TFSistemaVersao_Id_Sel',fld:'vTFSISTEMAVERSAO_ID_SEL',pic:'',nv:''},{av:'AV124TFSistema_Ativo_Sel',fld:'vTFSISTEMA_ATIVO_SEL',pic:'9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV90ddo_Sistema_SiglaTitleControlIdToReplace',fld:'vDDO_SISTEMA_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_Sistema_TecnicaTitleControlIdToReplace',fld:'vDDO_SISTEMA_TECNICATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Sistema_CoordenacaoTitleControlIdToReplace',fld:'vDDO_SISTEMA_COORDENACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_Metodologia_DescricaoTitleControlIdToReplace',fld:'vDDO_METODOLOGIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_Sistema_ProjetoNomeTitleControlIdToReplace',fld:'vDDO_SISTEMA_PROJETONOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV142ddo_SistemaVersao_IdTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_IDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV125ddo_Sistema_AtivoTitleControlIdToReplace',fld:'vDDO_SISTEMA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV137Paginando',fld:'vPAGINANDO',pic:'',nv:false},{av:'AV134Registros',fld:'vREGISTROS',pic:'ZZZZZZZZZ9',nv:0},{av:'AV135PreView',fld:'vPREVIEW',pic:'ZZZ9',nv:0},{av:'AV136PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZZZZZZZ9',nv:0},{av:'AV60Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV82Sistema_PF1',fld:'vSISTEMA_PF1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV83Sistema_PF2',fld:'vSISTEMA_PF2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV97TFSistema_Tecnica_Sels',fld:'vTFSISTEMA_TECNICA_SELS',pic:'',nv:null},{av:'AV183Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A351AmbienteTecnologico_Codigo',fld:'AMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A137Metodologia_Codigo',fld:'METODOLOGIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A691Sistema_ProjetoCod',fld:'SISTEMA_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'A1859SistemaVersao_Codigo',fld:'SISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A130Sistema_Ativo',fld:'SISTEMA_ATIVO',pic:'',hsh:true,nv:false},{av:'A707Sistema_TipoSigla',fld:'SISTEMA_TIPOSIGLA',pic:'@!',nv:''},{av:'A395Sistema_PF',fld:'SISTEMA_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A690Sistema_PFA',fld:'SISTEMA_PFA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_sistema_coordenacao_Activeeventkey',ctrl:'DDO_SISTEMA_COORDENACAO',prop:'ActiveEventKey'},{av:'Ddo_sistema_coordenacao_Filteredtext_get',ctrl:'DDO_SISTEMA_COORDENACAO',prop:'FilteredText_get'},{av:'Ddo_sistema_coordenacao_Selectedvalue_get',ctrl:'DDO_SISTEMA_COORDENACAO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_sistema_coordenacao_Sortedstatus',ctrl:'DDO_SISTEMA_COORDENACAO',prop:'SortedStatus'},{av:'AV100TFSistema_Coordenacao',fld:'vTFSISTEMA_COORDENACAO',pic:'@!',nv:''},{av:'AV101TFSistema_Coordenacao_Sel',fld:'vTFSISTEMA_COORDENACAO_SEL',pic:'@!',nv:''},{av:'Ddo_sistema_sigla_Sortedstatus',ctrl:'DDO_SISTEMA_SIGLA',prop:'SortedStatus'},{av:'Ddo_sistema_tecnica_Sortedstatus',ctrl:'DDO_SISTEMA_TECNICA',prop:'SortedStatus'},{av:'Ddo_ambientetecnologico_descricao_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_metodologia_descricao_Sortedstatus',ctrl:'DDO_METODOLOGIA_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_sistema_projetonome_Sortedstatus',ctrl:'DDO_SISTEMA_PROJETONOME',prop:'SortedStatus'},{av:'Ddo_sistemaversao_id_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_ID',prop:'SortedStatus'},{av:'Ddo_sistema_ativo_Sortedstatus',ctrl:'DDO_SISTEMA_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_AMBIENTETECNOLOGICO_DESCRICAO.ONOPTIONCLICKED","{handler:'E153G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV67Sistema_Nome1',fld:'vSISTEMA_NOME1',pic:'@!',nv:''},{av:'AV62Sistema_Sigla1',fld:'vSISTEMA_SIGLA1',pic:'@!',nv:''},{av:'AV73Sistema_Tipo1',fld:'vSISTEMA_TIPO1',pic:'',nv:''},{av:'AV75Sistema_Tecnica1',fld:'vSISTEMA_TECNICA1',pic:'',nv:''},{av:'AV76Sistema_Coordenacao1',fld:'vSISTEMA_COORDENACAO1',pic:'@!',nv:''},{av:'AV80Sistema_ProjetoNome1',fld:'vSISTEMA_PROJETONOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV68Sistema_Nome2',fld:'vSISTEMA_NOME2',pic:'@!',nv:''},{av:'AV63Sistema_Sigla2',fld:'vSISTEMA_SIGLA2',pic:'@!',nv:''},{av:'AV74Sistema_Tipo2',fld:'vSISTEMA_TIPO2',pic:'',nv:''},{av:'AV77Sistema_Tecnica2',fld:'vSISTEMA_TECNICA2',pic:'',nv:''},{av:'AV78Sistema_Coordenacao2',fld:'vSISTEMA_COORDENACAO2',pic:'@!',nv:''},{av:'AV81Sistema_ProjetoNome2',fld:'vSISTEMA_PROJETONOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV88TFSistema_Sigla',fld:'vTFSISTEMA_SIGLA',pic:'@!',nv:''},{av:'AV89TFSistema_Sigla_Sel',fld:'vTFSISTEMA_SIGLA_SEL',pic:'@!',nv:''},{av:'AV100TFSistema_Coordenacao',fld:'vTFSISTEMA_COORDENACAO',pic:'@!',nv:''},{av:'AV101TFSistema_Coordenacao_Sel',fld:'vTFSISTEMA_COORDENACAO_SEL',pic:'@!',nv:''},{av:'AV104TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV105TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV108TFMetodologia_Descricao',fld:'vTFMETODOLOGIA_DESCRICAO',pic:'@!',nv:''},{av:'AV109TFMetodologia_Descricao_Sel',fld:'vTFMETODOLOGIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV120TFSistema_ProjetoNome',fld:'vTFSISTEMA_PROJETONOME',pic:'@!',nv:''},{av:'AV121TFSistema_ProjetoNome_Sel',fld:'vTFSISTEMA_PROJETONOME_SEL',pic:'@!',nv:''},{av:'AV140TFSistemaVersao_Id',fld:'vTFSISTEMAVERSAO_ID',pic:'',nv:''},{av:'AV141TFSistemaVersao_Id_Sel',fld:'vTFSISTEMAVERSAO_ID_SEL',pic:'',nv:''},{av:'AV124TFSistema_Ativo_Sel',fld:'vTFSISTEMA_ATIVO_SEL',pic:'9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV90ddo_Sistema_SiglaTitleControlIdToReplace',fld:'vDDO_SISTEMA_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_Sistema_TecnicaTitleControlIdToReplace',fld:'vDDO_SISTEMA_TECNICATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Sistema_CoordenacaoTitleControlIdToReplace',fld:'vDDO_SISTEMA_COORDENACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_Metodologia_DescricaoTitleControlIdToReplace',fld:'vDDO_METODOLOGIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_Sistema_ProjetoNomeTitleControlIdToReplace',fld:'vDDO_SISTEMA_PROJETONOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV142ddo_SistemaVersao_IdTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_IDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV125ddo_Sistema_AtivoTitleControlIdToReplace',fld:'vDDO_SISTEMA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV137Paginando',fld:'vPAGINANDO',pic:'',nv:false},{av:'AV134Registros',fld:'vREGISTROS',pic:'ZZZZZZZZZ9',nv:0},{av:'AV135PreView',fld:'vPREVIEW',pic:'ZZZ9',nv:0},{av:'AV136PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZZZZZZZ9',nv:0},{av:'AV60Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV82Sistema_PF1',fld:'vSISTEMA_PF1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV83Sistema_PF2',fld:'vSISTEMA_PF2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV97TFSistema_Tecnica_Sels',fld:'vTFSISTEMA_TECNICA_SELS',pic:'',nv:null},{av:'AV183Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A351AmbienteTecnologico_Codigo',fld:'AMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A137Metodologia_Codigo',fld:'METODOLOGIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A691Sistema_ProjetoCod',fld:'SISTEMA_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'A1859SistemaVersao_Codigo',fld:'SISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A130Sistema_Ativo',fld:'SISTEMA_ATIVO',pic:'',hsh:true,nv:false},{av:'A707Sistema_TipoSigla',fld:'SISTEMA_TIPOSIGLA',pic:'@!',nv:''},{av:'A395Sistema_PF',fld:'SISTEMA_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A690Sistema_PFA',fld:'SISTEMA_PFA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_ambientetecnologico_descricao_Activeeventkey',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_ambientetecnologico_descricao_Filteredtext_get',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_ambientetecnologico_descricao_Selectedvalue_get',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_ambientetecnologico_descricao_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'SortedStatus'},{av:'AV104TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV105TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'Ddo_sistema_sigla_Sortedstatus',ctrl:'DDO_SISTEMA_SIGLA',prop:'SortedStatus'},{av:'Ddo_sistema_tecnica_Sortedstatus',ctrl:'DDO_SISTEMA_TECNICA',prop:'SortedStatus'},{av:'Ddo_sistema_coordenacao_Sortedstatus',ctrl:'DDO_SISTEMA_COORDENACAO',prop:'SortedStatus'},{av:'Ddo_metodologia_descricao_Sortedstatus',ctrl:'DDO_METODOLOGIA_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_sistema_projetonome_Sortedstatus',ctrl:'DDO_SISTEMA_PROJETONOME',prop:'SortedStatus'},{av:'Ddo_sistemaversao_id_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_ID',prop:'SortedStatus'},{av:'Ddo_sistema_ativo_Sortedstatus',ctrl:'DDO_SISTEMA_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_METODOLOGIA_DESCRICAO.ONOPTIONCLICKED","{handler:'E163G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV67Sistema_Nome1',fld:'vSISTEMA_NOME1',pic:'@!',nv:''},{av:'AV62Sistema_Sigla1',fld:'vSISTEMA_SIGLA1',pic:'@!',nv:''},{av:'AV73Sistema_Tipo1',fld:'vSISTEMA_TIPO1',pic:'',nv:''},{av:'AV75Sistema_Tecnica1',fld:'vSISTEMA_TECNICA1',pic:'',nv:''},{av:'AV76Sistema_Coordenacao1',fld:'vSISTEMA_COORDENACAO1',pic:'@!',nv:''},{av:'AV80Sistema_ProjetoNome1',fld:'vSISTEMA_PROJETONOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV68Sistema_Nome2',fld:'vSISTEMA_NOME2',pic:'@!',nv:''},{av:'AV63Sistema_Sigla2',fld:'vSISTEMA_SIGLA2',pic:'@!',nv:''},{av:'AV74Sistema_Tipo2',fld:'vSISTEMA_TIPO2',pic:'',nv:''},{av:'AV77Sistema_Tecnica2',fld:'vSISTEMA_TECNICA2',pic:'',nv:''},{av:'AV78Sistema_Coordenacao2',fld:'vSISTEMA_COORDENACAO2',pic:'@!',nv:''},{av:'AV81Sistema_ProjetoNome2',fld:'vSISTEMA_PROJETONOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV88TFSistema_Sigla',fld:'vTFSISTEMA_SIGLA',pic:'@!',nv:''},{av:'AV89TFSistema_Sigla_Sel',fld:'vTFSISTEMA_SIGLA_SEL',pic:'@!',nv:''},{av:'AV100TFSistema_Coordenacao',fld:'vTFSISTEMA_COORDENACAO',pic:'@!',nv:''},{av:'AV101TFSistema_Coordenacao_Sel',fld:'vTFSISTEMA_COORDENACAO_SEL',pic:'@!',nv:''},{av:'AV104TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV105TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV108TFMetodologia_Descricao',fld:'vTFMETODOLOGIA_DESCRICAO',pic:'@!',nv:''},{av:'AV109TFMetodologia_Descricao_Sel',fld:'vTFMETODOLOGIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV120TFSistema_ProjetoNome',fld:'vTFSISTEMA_PROJETONOME',pic:'@!',nv:''},{av:'AV121TFSistema_ProjetoNome_Sel',fld:'vTFSISTEMA_PROJETONOME_SEL',pic:'@!',nv:''},{av:'AV140TFSistemaVersao_Id',fld:'vTFSISTEMAVERSAO_ID',pic:'',nv:''},{av:'AV141TFSistemaVersao_Id_Sel',fld:'vTFSISTEMAVERSAO_ID_SEL',pic:'',nv:''},{av:'AV124TFSistema_Ativo_Sel',fld:'vTFSISTEMA_ATIVO_SEL',pic:'9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV90ddo_Sistema_SiglaTitleControlIdToReplace',fld:'vDDO_SISTEMA_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_Sistema_TecnicaTitleControlIdToReplace',fld:'vDDO_SISTEMA_TECNICATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Sistema_CoordenacaoTitleControlIdToReplace',fld:'vDDO_SISTEMA_COORDENACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_Metodologia_DescricaoTitleControlIdToReplace',fld:'vDDO_METODOLOGIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_Sistema_ProjetoNomeTitleControlIdToReplace',fld:'vDDO_SISTEMA_PROJETONOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV142ddo_SistemaVersao_IdTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_IDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV125ddo_Sistema_AtivoTitleControlIdToReplace',fld:'vDDO_SISTEMA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV137Paginando',fld:'vPAGINANDO',pic:'',nv:false},{av:'AV134Registros',fld:'vREGISTROS',pic:'ZZZZZZZZZ9',nv:0},{av:'AV135PreView',fld:'vPREVIEW',pic:'ZZZ9',nv:0},{av:'AV136PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZZZZZZZ9',nv:0},{av:'AV60Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV82Sistema_PF1',fld:'vSISTEMA_PF1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV83Sistema_PF2',fld:'vSISTEMA_PF2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV97TFSistema_Tecnica_Sels',fld:'vTFSISTEMA_TECNICA_SELS',pic:'',nv:null},{av:'AV183Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A351AmbienteTecnologico_Codigo',fld:'AMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A137Metodologia_Codigo',fld:'METODOLOGIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A691Sistema_ProjetoCod',fld:'SISTEMA_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'A1859SistemaVersao_Codigo',fld:'SISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A130Sistema_Ativo',fld:'SISTEMA_ATIVO',pic:'',hsh:true,nv:false},{av:'A707Sistema_TipoSigla',fld:'SISTEMA_TIPOSIGLA',pic:'@!',nv:''},{av:'A395Sistema_PF',fld:'SISTEMA_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A690Sistema_PFA',fld:'SISTEMA_PFA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_metodologia_descricao_Activeeventkey',ctrl:'DDO_METODOLOGIA_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_metodologia_descricao_Filteredtext_get',ctrl:'DDO_METODOLOGIA_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_metodologia_descricao_Selectedvalue_get',ctrl:'DDO_METODOLOGIA_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_metodologia_descricao_Sortedstatus',ctrl:'DDO_METODOLOGIA_DESCRICAO',prop:'SortedStatus'},{av:'AV108TFMetodologia_Descricao',fld:'vTFMETODOLOGIA_DESCRICAO',pic:'@!',nv:''},{av:'AV109TFMetodologia_Descricao_Sel',fld:'vTFMETODOLOGIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'Ddo_sistema_sigla_Sortedstatus',ctrl:'DDO_SISTEMA_SIGLA',prop:'SortedStatus'},{av:'Ddo_sistema_tecnica_Sortedstatus',ctrl:'DDO_SISTEMA_TECNICA',prop:'SortedStatus'},{av:'Ddo_sistema_coordenacao_Sortedstatus',ctrl:'DDO_SISTEMA_COORDENACAO',prop:'SortedStatus'},{av:'Ddo_ambientetecnologico_descricao_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_sistema_projetonome_Sortedstatus',ctrl:'DDO_SISTEMA_PROJETONOME',prop:'SortedStatus'},{av:'Ddo_sistemaversao_id_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_ID',prop:'SortedStatus'},{av:'Ddo_sistema_ativo_Sortedstatus',ctrl:'DDO_SISTEMA_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SISTEMA_PROJETONOME.ONOPTIONCLICKED","{handler:'E173G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV67Sistema_Nome1',fld:'vSISTEMA_NOME1',pic:'@!',nv:''},{av:'AV62Sistema_Sigla1',fld:'vSISTEMA_SIGLA1',pic:'@!',nv:''},{av:'AV73Sistema_Tipo1',fld:'vSISTEMA_TIPO1',pic:'',nv:''},{av:'AV75Sistema_Tecnica1',fld:'vSISTEMA_TECNICA1',pic:'',nv:''},{av:'AV76Sistema_Coordenacao1',fld:'vSISTEMA_COORDENACAO1',pic:'@!',nv:''},{av:'AV80Sistema_ProjetoNome1',fld:'vSISTEMA_PROJETONOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV68Sistema_Nome2',fld:'vSISTEMA_NOME2',pic:'@!',nv:''},{av:'AV63Sistema_Sigla2',fld:'vSISTEMA_SIGLA2',pic:'@!',nv:''},{av:'AV74Sistema_Tipo2',fld:'vSISTEMA_TIPO2',pic:'',nv:''},{av:'AV77Sistema_Tecnica2',fld:'vSISTEMA_TECNICA2',pic:'',nv:''},{av:'AV78Sistema_Coordenacao2',fld:'vSISTEMA_COORDENACAO2',pic:'@!',nv:''},{av:'AV81Sistema_ProjetoNome2',fld:'vSISTEMA_PROJETONOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV88TFSistema_Sigla',fld:'vTFSISTEMA_SIGLA',pic:'@!',nv:''},{av:'AV89TFSistema_Sigla_Sel',fld:'vTFSISTEMA_SIGLA_SEL',pic:'@!',nv:''},{av:'AV100TFSistema_Coordenacao',fld:'vTFSISTEMA_COORDENACAO',pic:'@!',nv:''},{av:'AV101TFSistema_Coordenacao_Sel',fld:'vTFSISTEMA_COORDENACAO_SEL',pic:'@!',nv:''},{av:'AV104TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV105TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV108TFMetodologia_Descricao',fld:'vTFMETODOLOGIA_DESCRICAO',pic:'@!',nv:''},{av:'AV109TFMetodologia_Descricao_Sel',fld:'vTFMETODOLOGIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV120TFSistema_ProjetoNome',fld:'vTFSISTEMA_PROJETONOME',pic:'@!',nv:''},{av:'AV121TFSistema_ProjetoNome_Sel',fld:'vTFSISTEMA_PROJETONOME_SEL',pic:'@!',nv:''},{av:'AV140TFSistemaVersao_Id',fld:'vTFSISTEMAVERSAO_ID',pic:'',nv:''},{av:'AV141TFSistemaVersao_Id_Sel',fld:'vTFSISTEMAVERSAO_ID_SEL',pic:'',nv:''},{av:'AV124TFSistema_Ativo_Sel',fld:'vTFSISTEMA_ATIVO_SEL',pic:'9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV90ddo_Sistema_SiglaTitleControlIdToReplace',fld:'vDDO_SISTEMA_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_Sistema_TecnicaTitleControlIdToReplace',fld:'vDDO_SISTEMA_TECNICATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Sistema_CoordenacaoTitleControlIdToReplace',fld:'vDDO_SISTEMA_COORDENACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_Metodologia_DescricaoTitleControlIdToReplace',fld:'vDDO_METODOLOGIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_Sistema_ProjetoNomeTitleControlIdToReplace',fld:'vDDO_SISTEMA_PROJETONOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV142ddo_SistemaVersao_IdTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_IDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV125ddo_Sistema_AtivoTitleControlIdToReplace',fld:'vDDO_SISTEMA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV137Paginando',fld:'vPAGINANDO',pic:'',nv:false},{av:'AV134Registros',fld:'vREGISTROS',pic:'ZZZZZZZZZ9',nv:0},{av:'AV135PreView',fld:'vPREVIEW',pic:'ZZZ9',nv:0},{av:'AV136PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZZZZZZZ9',nv:0},{av:'AV60Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV82Sistema_PF1',fld:'vSISTEMA_PF1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV83Sistema_PF2',fld:'vSISTEMA_PF2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV97TFSistema_Tecnica_Sels',fld:'vTFSISTEMA_TECNICA_SELS',pic:'',nv:null},{av:'AV183Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A351AmbienteTecnologico_Codigo',fld:'AMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A137Metodologia_Codigo',fld:'METODOLOGIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A691Sistema_ProjetoCod',fld:'SISTEMA_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'A1859SistemaVersao_Codigo',fld:'SISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A130Sistema_Ativo',fld:'SISTEMA_ATIVO',pic:'',hsh:true,nv:false},{av:'A707Sistema_TipoSigla',fld:'SISTEMA_TIPOSIGLA',pic:'@!',nv:''},{av:'A395Sistema_PF',fld:'SISTEMA_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A690Sistema_PFA',fld:'SISTEMA_PFA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_sistema_projetonome_Activeeventkey',ctrl:'DDO_SISTEMA_PROJETONOME',prop:'ActiveEventKey'},{av:'Ddo_sistema_projetonome_Filteredtext_get',ctrl:'DDO_SISTEMA_PROJETONOME',prop:'FilteredText_get'},{av:'Ddo_sistema_projetonome_Selectedvalue_get',ctrl:'DDO_SISTEMA_PROJETONOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_sistema_projetonome_Sortedstatus',ctrl:'DDO_SISTEMA_PROJETONOME',prop:'SortedStatus'},{av:'AV120TFSistema_ProjetoNome',fld:'vTFSISTEMA_PROJETONOME',pic:'@!',nv:''},{av:'AV121TFSistema_ProjetoNome_Sel',fld:'vTFSISTEMA_PROJETONOME_SEL',pic:'@!',nv:''},{av:'Ddo_sistema_sigla_Sortedstatus',ctrl:'DDO_SISTEMA_SIGLA',prop:'SortedStatus'},{av:'Ddo_sistema_tecnica_Sortedstatus',ctrl:'DDO_SISTEMA_TECNICA',prop:'SortedStatus'},{av:'Ddo_sistema_coordenacao_Sortedstatus',ctrl:'DDO_SISTEMA_COORDENACAO',prop:'SortedStatus'},{av:'Ddo_ambientetecnologico_descricao_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_metodologia_descricao_Sortedstatus',ctrl:'DDO_METODOLOGIA_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_sistemaversao_id_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_ID',prop:'SortedStatus'},{av:'Ddo_sistema_ativo_Sortedstatus',ctrl:'DDO_SISTEMA_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SISTEMAVERSAO_ID.ONOPTIONCLICKED","{handler:'E183G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV67Sistema_Nome1',fld:'vSISTEMA_NOME1',pic:'@!',nv:''},{av:'AV62Sistema_Sigla1',fld:'vSISTEMA_SIGLA1',pic:'@!',nv:''},{av:'AV73Sistema_Tipo1',fld:'vSISTEMA_TIPO1',pic:'',nv:''},{av:'AV75Sistema_Tecnica1',fld:'vSISTEMA_TECNICA1',pic:'',nv:''},{av:'AV76Sistema_Coordenacao1',fld:'vSISTEMA_COORDENACAO1',pic:'@!',nv:''},{av:'AV80Sistema_ProjetoNome1',fld:'vSISTEMA_PROJETONOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV68Sistema_Nome2',fld:'vSISTEMA_NOME2',pic:'@!',nv:''},{av:'AV63Sistema_Sigla2',fld:'vSISTEMA_SIGLA2',pic:'@!',nv:''},{av:'AV74Sistema_Tipo2',fld:'vSISTEMA_TIPO2',pic:'',nv:''},{av:'AV77Sistema_Tecnica2',fld:'vSISTEMA_TECNICA2',pic:'',nv:''},{av:'AV78Sistema_Coordenacao2',fld:'vSISTEMA_COORDENACAO2',pic:'@!',nv:''},{av:'AV81Sistema_ProjetoNome2',fld:'vSISTEMA_PROJETONOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV88TFSistema_Sigla',fld:'vTFSISTEMA_SIGLA',pic:'@!',nv:''},{av:'AV89TFSistema_Sigla_Sel',fld:'vTFSISTEMA_SIGLA_SEL',pic:'@!',nv:''},{av:'AV100TFSistema_Coordenacao',fld:'vTFSISTEMA_COORDENACAO',pic:'@!',nv:''},{av:'AV101TFSistema_Coordenacao_Sel',fld:'vTFSISTEMA_COORDENACAO_SEL',pic:'@!',nv:''},{av:'AV104TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV105TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV108TFMetodologia_Descricao',fld:'vTFMETODOLOGIA_DESCRICAO',pic:'@!',nv:''},{av:'AV109TFMetodologia_Descricao_Sel',fld:'vTFMETODOLOGIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV120TFSistema_ProjetoNome',fld:'vTFSISTEMA_PROJETONOME',pic:'@!',nv:''},{av:'AV121TFSistema_ProjetoNome_Sel',fld:'vTFSISTEMA_PROJETONOME_SEL',pic:'@!',nv:''},{av:'AV140TFSistemaVersao_Id',fld:'vTFSISTEMAVERSAO_ID',pic:'',nv:''},{av:'AV141TFSistemaVersao_Id_Sel',fld:'vTFSISTEMAVERSAO_ID_SEL',pic:'',nv:''},{av:'AV124TFSistema_Ativo_Sel',fld:'vTFSISTEMA_ATIVO_SEL',pic:'9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV90ddo_Sistema_SiglaTitleControlIdToReplace',fld:'vDDO_SISTEMA_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_Sistema_TecnicaTitleControlIdToReplace',fld:'vDDO_SISTEMA_TECNICATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Sistema_CoordenacaoTitleControlIdToReplace',fld:'vDDO_SISTEMA_COORDENACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_Metodologia_DescricaoTitleControlIdToReplace',fld:'vDDO_METODOLOGIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_Sistema_ProjetoNomeTitleControlIdToReplace',fld:'vDDO_SISTEMA_PROJETONOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV142ddo_SistemaVersao_IdTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_IDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV125ddo_Sistema_AtivoTitleControlIdToReplace',fld:'vDDO_SISTEMA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV137Paginando',fld:'vPAGINANDO',pic:'',nv:false},{av:'AV134Registros',fld:'vREGISTROS',pic:'ZZZZZZZZZ9',nv:0},{av:'AV135PreView',fld:'vPREVIEW',pic:'ZZZ9',nv:0},{av:'AV136PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZZZZZZZ9',nv:0},{av:'AV60Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV82Sistema_PF1',fld:'vSISTEMA_PF1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV83Sistema_PF2',fld:'vSISTEMA_PF2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV97TFSistema_Tecnica_Sels',fld:'vTFSISTEMA_TECNICA_SELS',pic:'',nv:null},{av:'AV183Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A351AmbienteTecnologico_Codigo',fld:'AMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A137Metodologia_Codigo',fld:'METODOLOGIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A691Sistema_ProjetoCod',fld:'SISTEMA_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'A1859SistemaVersao_Codigo',fld:'SISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A130Sistema_Ativo',fld:'SISTEMA_ATIVO',pic:'',hsh:true,nv:false},{av:'A707Sistema_TipoSigla',fld:'SISTEMA_TIPOSIGLA',pic:'@!',nv:''},{av:'A395Sistema_PF',fld:'SISTEMA_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A690Sistema_PFA',fld:'SISTEMA_PFA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_sistemaversao_id_Activeeventkey',ctrl:'DDO_SISTEMAVERSAO_ID',prop:'ActiveEventKey'},{av:'Ddo_sistemaversao_id_Filteredtext_get',ctrl:'DDO_SISTEMAVERSAO_ID',prop:'FilteredText_get'},{av:'Ddo_sistemaversao_id_Selectedvalue_get',ctrl:'DDO_SISTEMAVERSAO_ID',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_sistemaversao_id_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_ID',prop:'SortedStatus'},{av:'AV140TFSistemaVersao_Id',fld:'vTFSISTEMAVERSAO_ID',pic:'',nv:''},{av:'AV141TFSistemaVersao_Id_Sel',fld:'vTFSISTEMAVERSAO_ID_SEL',pic:'',nv:''},{av:'Ddo_sistema_sigla_Sortedstatus',ctrl:'DDO_SISTEMA_SIGLA',prop:'SortedStatus'},{av:'Ddo_sistema_tecnica_Sortedstatus',ctrl:'DDO_SISTEMA_TECNICA',prop:'SortedStatus'},{av:'Ddo_sistema_coordenacao_Sortedstatus',ctrl:'DDO_SISTEMA_COORDENACAO',prop:'SortedStatus'},{av:'Ddo_ambientetecnologico_descricao_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_metodologia_descricao_Sortedstatus',ctrl:'DDO_METODOLOGIA_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_sistema_projetonome_Sortedstatus',ctrl:'DDO_SISTEMA_PROJETONOME',prop:'SortedStatus'},{av:'Ddo_sistema_ativo_Sortedstatus',ctrl:'DDO_SISTEMA_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SISTEMA_ATIVO.ONOPTIONCLICKED","{handler:'E193G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV67Sistema_Nome1',fld:'vSISTEMA_NOME1',pic:'@!',nv:''},{av:'AV62Sistema_Sigla1',fld:'vSISTEMA_SIGLA1',pic:'@!',nv:''},{av:'AV73Sistema_Tipo1',fld:'vSISTEMA_TIPO1',pic:'',nv:''},{av:'AV75Sistema_Tecnica1',fld:'vSISTEMA_TECNICA1',pic:'',nv:''},{av:'AV76Sistema_Coordenacao1',fld:'vSISTEMA_COORDENACAO1',pic:'@!',nv:''},{av:'AV80Sistema_ProjetoNome1',fld:'vSISTEMA_PROJETONOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV68Sistema_Nome2',fld:'vSISTEMA_NOME2',pic:'@!',nv:''},{av:'AV63Sistema_Sigla2',fld:'vSISTEMA_SIGLA2',pic:'@!',nv:''},{av:'AV74Sistema_Tipo2',fld:'vSISTEMA_TIPO2',pic:'',nv:''},{av:'AV77Sistema_Tecnica2',fld:'vSISTEMA_TECNICA2',pic:'',nv:''},{av:'AV78Sistema_Coordenacao2',fld:'vSISTEMA_COORDENACAO2',pic:'@!',nv:''},{av:'AV81Sistema_ProjetoNome2',fld:'vSISTEMA_PROJETONOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV88TFSistema_Sigla',fld:'vTFSISTEMA_SIGLA',pic:'@!',nv:''},{av:'AV89TFSistema_Sigla_Sel',fld:'vTFSISTEMA_SIGLA_SEL',pic:'@!',nv:''},{av:'AV100TFSistema_Coordenacao',fld:'vTFSISTEMA_COORDENACAO',pic:'@!',nv:''},{av:'AV101TFSistema_Coordenacao_Sel',fld:'vTFSISTEMA_COORDENACAO_SEL',pic:'@!',nv:''},{av:'AV104TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV105TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV108TFMetodologia_Descricao',fld:'vTFMETODOLOGIA_DESCRICAO',pic:'@!',nv:''},{av:'AV109TFMetodologia_Descricao_Sel',fld:'vTFMETODOLOGIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV120TFSistema_ProjetoNome',fld:'vTFSISTEMA_PROJETONOME',pic:'@!',nv:''},{av:'AV121TFSistema_ProjetoNome_Sel',fld:'vTFSISTEMA_PROJETONOME_SEL',pic:'@!',nv:''},{av:'AV140TFSistemaVersao_Id',fld:'vTFSISTEMAVERSAO_ID',pic:'',nv:''},{av:'AV141TFSistemaVersao_Id_Sel',fld:'vTFSISTEMAVERSAO_ID_SEL',pic:'',nv:''},{av:'AV124TFSistema_Ativo_Sel',fld:'vTFSISTEMA_ATIVO_SEL',pic:'9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV90ddo_Sistema_SiglaTitleControlIdToReplace',fld:'vDDO_SISTEMA_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_Sistema_TecnicaTitleControlIdToReplace',fld:'vDDO_SISTEMA_TECNICATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Sistema_CoordenacaoTitleControlIdToReplace',fld:'vDDO_SISTEMA_COORDENACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_Metodologia_DescricaoTitleControlIdToReplace',fld:'vDDO_METODOLOGIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_Sistema_ProjetoNomeTitleControlIdToReplace',fld:'vDDO_SISTEMA_PROJETONOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV142ddo_SistemaVersao_IdTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_IDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV125ddo_Sistema_AtivoTitleControlIdToReplace',fld:'vDDO_SISTEMA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV137Paginando',fld:'vPAGINANDO',pic:'',nv:false},{av:'AV134Registros',fld:'vREGISTROS',pic:'ZZZZZZZZZ9',nv:0},{av:'AV135PreView',fld:'vPREVIEW',pic:'ZZZ9',nv:0},{av:'AV136PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZZZZZZZ9',nv:0},{av:'AV60Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV82Sistema_PF1',fld:'vSISTEMA_PF1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV83Sistema_PF2',fld:'vSISTEMA_PF2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV97TFSistema_Tecnica_Sels',fld:'vTFSISTEMA_TECNICA_SELS',pic:'',nv:null},{av:'AV183Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A351AmbienteTecnologico_Codigo',fld:'AMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A137Metodologia_Codigo',fld:'METODOLOGIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A691Sistema_ProjetoCod',fld:'SISTEMA_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'A1859SistemaVersao_Codigo',fld:'SISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A130Sistema_Ativo',fld:'SISTEMA_ATIVO',pic:'',hsh:true,nv:false},{av:'A707Sistema_TipoSigla',fld:'SISTEMA_TIPOSIGLA',pic:'@!',nv:''},{av:'A395Sistema_PF',fld:'SISTEMA_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A690Sistema_PFA',fld:'SISTEMA_PFA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_sistema_ativo_Activeeventkey',ctrl:'DDO_SISTEMA_ATIVO',prop:'ActiveEventKey'},{av:'Ddo_sistema_ativo_Selectedvalue_get',ctrl:'DDO_SISTEMA_ATIVO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_sistema_ativo_Sortedstatus',ctrl:'DDO_SISTEMA_ATIVO',prop:'SortedStatus'},{av:'AV124TFSistema_Ativo_Sel',fld:'vTFSISTEMA_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_sistema_sigla_Sortedstatus',ctrl:'DDO_SISTEMA_SIGLA',prop:'SortedStatus'},{av:'Ddo_sistema_tecnica_Sortedstatus',ctrl:'DDO_SISTEMA_TECNICA',prop:'SortedStatus'},{av:'Ddo_sistema_coordenacao_Sortedstatus',ctrl:'DDO_SISTEMA_COORDENACAO',prop:'SortedStatus'},{av:'Ddo_ambientetecnologico_descricao_Sortedstatus',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_metodologia_descricao_Sortedstatus',ctrl:'DDO_METODOLOGIA_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_sistema_projetonome_Sortedstatus',ctrl:'DDO_SISTEMA_PROJETONOME',prop:'SortedStatus'},{av:'Ddo_sistemaversao_id_Sortedstatus',ctrl:'DDO_SISTEMAVERSAO_ID',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E323G2',iparms:[{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A351AmbienteTecnologico_Codigo',fld:'AMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A137Metodologia_Codigo',fld:'METODOLOGIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A691Sistema_ProjetoCod',fld:'SISTEMA_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'A1859SistemaVersao_Codigo',fld:'SISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A130Sistema_Ativo',fld:'SISTEMA_ATIVO',pic:'',hsh:true,nv:false},{av:'A707Sistema_TipoSigla',fld:'SISTEMA_TIPOSIGLA',pic:'@!',nv:''},{av:'A395Sistema_PF',fld:'SISTEMA_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A690Sistema_PFA',fld:'SISTEMA_PFA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0}],oparms:[{av:'AV34Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'AV35Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'AV58Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'AV84AssociarClientes',fld:'vASSOCIARCLIENTES',pic:'',nv:''},{av:'edtavAssociarclientes_Tooltiptext',ctrl:'vASSOCIARCLIENTES',prop:'Tooltiptext'},{av:'edtAmbienteTecnologico_Descricao_Link',ctrl:'AMBIENTETECNOLOGICO_DESCRICAO',prop:'Link'},{av:'edtMetodologia_Descricao_Link',ctrl:'METODOLOGIA_DESCRICAO',prop:'Link'},{av:'edtSistema_ProjetoNome_Link',ctrl:'SISTEMA_PROJETONOME',prop:'Link'},{av:'edtSistemaVersao_Id_Link',ctrl:'SISTEMAVERSAO_ID',prop:'Link'},{av:'edtSistema_Sigla_Forecolor',ctrl:'SISTEMA_SIGLA',prop:'Forecolor'},{av:'edtavSistema_tiposigla_Forecolor',ctrl:'vSISTEMA_TIPOSIGLA',prop:'Forecolor'},{av:'cmbSistema_Tecnica'},{av:'edtSistema_Coordenacao_Forecolor',ctrl:'SISTEMA_COORDENACAO',prop:'Forecolor'},{av:'edtAmbienteTecnologico_Descricao_Forecolor',ctrl:'AMBIENTETECNOLOGICO_DESCRICAO',prop:'Forecolor'},{av:'edtMetodologia_Descricao_Forecolor',ctrl:'METODOLOGIA_DESCRICAO',prop:'Forecolor'},{av:'edtavPfb_Forecolor',ctrl:'vPFB',prop:'Forecolor'},{av:'edtavPfa_Forecolor',ctrl:'vPFA',prop:'Forecolor'},{av:'edtSistema_ProjetoNome_Forecolor',ctrl:'SISTEMA_PROJETONOME',prop:'Forecolor'},{av:'AV130Sistema_TipoSigla',fld:'vSISTEMA_TIPOSIGLA',pic:'@!',nv:''},{av:'AV131PFB',fld:'vPFB',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV133PFA',fld:'vPFA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E203G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV67Sistema_Nome1',fld:'vSISTEMA_NOME1',pic:'@!',nv:''},{av:'AV62Sistema_Sigla1',fld:'vSISTEMA_SIGLA1',pic:'@!',nv:''},{av:'AV73Sistema_Tipo1',fld:'vSISTEMA_TIPO1',pic:'',nv:''},{av:'AV75Sistema_Tecnica1',fld:'vSISTEMA_TECNICA1',pic:'',nv:''},{av:'AV76Sistema_Coordenacao1',fld:'vSISTEMA_COORDENACAO1',pic:'@!',nv:''},{av:'AV80Sistema_ProjetoNome1',fld:'vSISTEMA_PROJETONOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV68Sistema_Nome2',fld:'vSISTEMA_NOME2',pic:'@!',nv:''},{av:'AV63Sistema_Sigla2',fld:'vSISTEMA_SIGLA2',pic:'@!',nv:''},{av:'AV74Sistema_Tipo2',fld:'vSISTEMA_TIPO2',pic:'',nv:''},{av:'AV77Sistema_Tecnica2',fld:'vSISTEMA_TECNICA2',pic:'',nv:''},{av:'AV78Sistema_Coordenacao2',fld:'vSISTEMA_COORDENACAO2',pic:'@!',nv:''},{av:'AV81Sistema_ProjetoNome2',fld:'vSISTEMA_PROJETONOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV88TFSistema_Sigla',fld:'vTFSISTEMA_SIGLA',pic:'@!',nv:''},{av:'AV89TFSistema_Sigla_Sel',fld:'vTFSISTEMA_SIGLA_SEL',pic:'@!',nv:''},{av:'AV100TFSistema_Coordenacao',fld:'vTFSISTEMA_COORDENACAO',pic:'@!',nv:''},{av:'AV101TFSistema_Coordenacao_Sel',fld:'vTFSISTEMA_COORDENACAO_SEL',pic:'@!',nv:''},{av:'AV104TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV105TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV108TFMetodologia_Descricao',fld:'vTFMETODOLOGIA_DESCRICAO',pic:'@!',nv:''},{av:'AV109TFMetodologia_Descricao_Sel',fld:'vTFMETODOLOGIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV120TFSistema_ProjetoNome',fld:'vTFSISTEMA_PROJETONOME',pic:'@!',nv:''},{av:'AV121TFSistema_ProjetoNome_Sel',fld:'vTFSISTEMA_PROJETONOME_SEL',pic:'@!',nv:''},{av:'AV140TFSistemaVersao_Id',fld:'vTFSISTEMAVERSAO_ID',pic:'',nv:''},{av:'AV141TFSistemaVersao_Id_Sel',fld:'vTFSISTEMAVERSAO_ID_SEL',pic:'',nv:''},{av:'AV124TFSistema_Ativo_Sel',fld:'vTFSISTEMA_ATIVO_SEL',pic:'9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV90ddo_Sistema_SiglaTitleControlIdToReplace',fld:'vDDO_SISTEMA_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_Sistema_TecnicaTitleControlIdToReplace',fld:'vDDO_SISTEMA_TECNICATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Sistema_CoordenacaoTitleControlIdToReplace',fld:'vDDO_SISTEMA_COORDENACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_Metodologia_DescricaoTitleControlIdToReplace',fld:'vDDO_METODOLOGIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_Sistema_ProjetoNomeTitleControlIdToReplace',fld:'vDDO_SISTEMA_PROJETONOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV142ddo_SistemaVersao_IdTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_IDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV125ddo_Sistema_AtivoTitleControlIdToReplace',fld:'vDDO_SISTEMA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV137Paginando',fld:'vPAGINANDO',pic:'',nv:false},{av:'AV134Registros',fld:'vREGISTROS',pic:'ZZZZZZZZZ9',nv:0},{av:'AV135PreView',fld:'vPREVIEW',pic:'ZZZ9',nv:0},{av:'AV136PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZZZZZZZ9',nv:0},{av:'AV60Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV82Sistema_PF1',fld:'vSISTEMA_PF1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV83Sistema_PF2',fld:'vSISTEMA_PF2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV97TFSistema_Tecnica_Sels',fld:'vTFSISTEMA_TECNICA_SELS',pic:'',nv:null},{av:'AV183Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A351AmbienteTecnologico_Codigo',fld:'AMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A137Metodologia_Codigo',fld:'METODOLOGIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A691Sistema_ProjetoCod',fld:'SISTEMA_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'A1859SistemaVersao_Codigo',fld:'SISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A130Sistema_Ativo',fld:'SISTEMA_ATIVO',pic:'',hsh:true,nv:false},{av:'A707Sistema_TipoSigla',fld:'SISTEMA_TIPOSIGLA',pic:'@!',nv:''},{av:'A395Sistema_PF',fld:'SISTEMA_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A690Sistema_PFA',fld:'SISTEMA_PFA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E263G2',iparms:[],oparms:[{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E213G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV67Sistema_Nome1',fld:'vSISTEMA_NOME1',pic:'@!',nv:''},{av:'AV62Sistema_Sigla1',fld:'vSISTEMA_SIGLA1',pic:'@!',nv:''},{av:'AV73Sistema_Tipo1',fld:'vSISTEMA_TIPO1',pic:'',nv:''},{av:'AV75Sistema_Tecnica1',fld:'vSISTEMA_TECNICA1',pic:'',nv:''},{av:'AV76Sistema_Coordenacao1',fld:'vSISTEMA_COORDENACAO1',pic:'@!',nv:''},{av:'AV80Sistema_ProjetoNome1',fld:'vSISTEMA_PROJETONOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV68Sistema_Nome2',fld:'vSISTEMA_NOME2',pic:'@!',nv:''},{av:'AV63Sistema_Sigla2',fld:'vSISTEMA_SIGLA2',pic:'@!',nv:''},{av:'AV74Sistema_Tipo2',fld:'vSISTEMA_TIPO2',pic:'',nv:''},{av:'AV77Sistema_Tecnica2',fld:'vSISTEMA_TECNICA2',pic:'',nv:''},{av:'AV78Sistema_Coordenacao2',fld:'vSISTEMA_COORDENACAO2',pic:'@!',nv:''},{av:'AV81Sistema_ProjetoNome2',fld:'vSISTEMA_PROJETONOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV88TFSistema_Sigla',fld:'vTFSISTEMA_SIGLA',pic:'@!',nv:''},{av:'AV89TFSistema_Sigla_Sel',fld:'vTFSISTEMA_SIGLA_SEL',pic:'@!',nv:''},{av:'AV100TFSistema_Coordenacao',fld:'vTFSISTEMA_COORDENACAO',pic:'@!',nv:''},{av:'AV101TFSistema_Coordenacao_Sel',fld:'vTFSISTEMA_COORDENACAO_SEL',pic:'@!',nv:''},{av:'AV104TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV105TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV108TFMetodologia_Descricao',fld:'vTFMETODOLOGIA_DESCRICAO',pic:'@!',nv:''},{av:'AV109TFMetodologia_Descricao_Sel',fld:'vTFMETODOLOGIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV120TFSistema_ProjetoNome',fld:'vTFSISTEMA_PROJETONOME',pic:'@!',nv:''},{av:'AV121TFSistema_ProjetoNome_Sel',fld:'vTFSISTEMA_PROJETONOME_SEL',pic:'@!',nv:''},{av:'AV140TFSistemaVersao_Id',fld:'vTFSISTEMAVERSAO_ID',pic:'',nv:''},{av:'AV141TFSistemaVersao_Id_Sel',fld:'vTFSISTEMAVERSAO_ID_SEL',pic:'',nv:''},{av:'AV124TFSistema_Ativo_Sel',fld:'vTFSISTEMA_ATIVO_SEL',pic:'9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV90ddo_Sistema_SiglaTitleControlIdToReplace',fld:'vDDO_SISTEMA_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_Sistema_TecnicaTitleControlIdToReplace',fld:'vDDO_SISTEMA_TECNICATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Sistema_CoordenacaoTitleControlIdToReplace',fld:'vDDO_SISTEMA_COORDENACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_Metodologia_DescricaoTitleControlIdToReplace',fld:'vDDO_METODOLOGIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_Sistema_ProjetoNomeTitleControlIdToReplace',fld:'vDDO_SISTEMA_PROJETONOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV142ddo_SistemaVersao_IdTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_IDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV125ddo_Sistema_AtivoTitleControlIdToReplace',fld:'vDDO_SISTEMA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV137Paginando',fld:'vPAGINANDO',pic:'',nv:false},{av:'AV134Registros',fld:'vREGISTROS',pic:'ZZZZZZZZZ9',nv:0},{av:'AV135PreView',fld:'vPREVIEW',pic:'ZZZ9',nv:0},{av:'AV136PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZZZZZZZ9',nv:0},{av:'AV60Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV82Sistema_PF1',fld:'vSISTEMA_PF1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV83Sistema_PF2',fld:'vSISTEMA_PF2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV97TFSistema_Tecnica_Sels',fld:'vTFSISTEMA_TECNICA_SELS',pic:'',nv:null},{av:'AV183Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A351AmbienteTecnologico_Codigo',fld:'AMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A137Metodologia_Codigo',fld:'METODOLOGIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A691Sistema_ProjetoCod',fld:'SISTEMA_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'A1859SistemaVersao_Codigo',fld:'SISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A130Sistema_Ativo',fld:'SISTEMA_ATIVO',pic:'',hsh:true,nv:false},{av:'A707Sistema_TipoSigla',fld:'SISTEMA_TIPOSIGLA',pic:'@!',nv:''},{av:'A395Sistema_PF',fld:'SISTEMA_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A690Sistema_PFA',fld:'SISTEMA_PFA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0}],oparms:[{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV68Sistema_Nome2',fld:'vSISTEMA_NOME2',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'cmbavDynamicfiltersoperator2'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV82Sistema_PF1',fld:'vSISTEMA_PF1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV80Sistema_ProjetoNome1',fld:'vSISTEMA_PROJETONOME1',pic:'@!',nv:''},{av:'AV76Sistema_Coordenacao1',fld:'vSISTEMA_COORDENACAO1',pic:'@!',nv:''},{av:'AV75Sistema_Tecnica1',fld:'vSISTEMA_TECNICA1',pic:'',nv:''},{av:'AV73Sistema_Tipo1',fld:'vSISTEMA_TIPO1',pic:'',nv:''},{av:'AV62Sistema_Sigla1',fld:'vSISTEMA_SIGLA1',pic:'@!',nv:''},{av:'AV67Sistema_Nome1',fld:'vSISTEMA_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV83Sistema_PF2',fld:'vSISTEMA_PF2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV81Sistema_ProjetoNome2',fld:'vSISTEMA_PROJETONOME2',pic:'@!',nv:''},{av:'AV78Sistema_Coordenacao2',fld:'vSISTEMA_COORDENACAO2',pic:'@!',nv:''},{av:'AV77Sistema_Tecnica2',fld:'vSISTEMA_TECNICA2',pic:'',nv:''},{av:'AV74Sistema_Tipo2',fld:'vSISTEMA_TIPO2',pic:'',nv:''},{av:'AV63Sistema_Sigla2',fld:'vSISTEMA_SIGLA2',pic:'@!',nv:''},{av:'edtavSistema_nome2_Visible',ctrl:'vSISTEMA_NOME2',prop:'Visible'},{av:'edtavSistema_sigla2_Visible',ctrl:'vSISTEMA_SIGLA2',prop:'Visible'},{av:'cmbavSistema_tipo2'},{av:'cmbavSistema_tecnica2'},{av:'edtavSistema_coordenacao2_Visible',ctrl:'vSISTEMA_COORDENACAO2',prop:'Visible'},{av:'edtavSistema_projetonome2_Visible',ctrl:'vSISTEMA_PROJETONOME2',prop:'Visible'},{av:'edtavSistema_pf2_Visible',ctrl:'vSISTEMA_PF2',prop:'Visible'},{av:'edtavSistema_nome1_Visible',ctrl:'vSISTEMA_NOME1',prop:'Visible'},{av:'edtavSistema_sigla1_Visible',ctrl:'vSISTEMA_SIGLA1',prop:'Visible'},{av:'cmbavSistema_tipo1'},{av:'cmbavSistema_tecnica1'},{av:'edtavSistema_coordenacao1_Visible',ctrl:'vSISTEMA_COORDENACAO1',prop:'Visible'},{av:'edtavSistema_projetonome1_Visible',ctrl:'vSISTEMA_PROJETONOME1',prop:'Visible'},{av:'edtavSistema_pf1_Visible',ctrl:'vSISTEMA_PF1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E273G2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavSistema_nome1_Visible',ctrl:'vSISTEMA_NOME1',prop:'Visible'},{av:'edtavSistema_sigla1_Visible',ctrl:'vSISTEMA_SIGLA1',prop:'Visible'},{av:'cmbavSistema_tipo1'},{av:'cmbavSistema_tecnica1'},{av:'edtavSistema_coordenacao1_Visible',ctrl:'vSISTEMA_COORDENACAO1',prop:'Visible'},{av:'edtavSistema_projetonome1_Visible',ctrl:'vSISTEMA_PROJETONOME1',prop:'Visible'},{av:'edtavSistema_pf1_Visible',ctrl:'vSISTEMA_PF1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E223G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV67Sistema_Nome1',fld:'vSISTEMA_NOME1',pic:'@!',nv:''},{av:'AV62Sistema_Sigla1',fld:'vSISTEMA_SIGLA1',pic:'@!',nv:''},{av:'AV73Sistema_Tipo1',fld:'vSISTEMA_TIPO1',pic:'',nv:''},{av:'AV75Sistema_Tecnica1',fld:'vSISTEMA_TECNICA1',pic:'',nv:''},{av:'AV76Sistema_Coordenacao1',fld:'vSISTEMA_COORDENACAO1',pic:'@!',nv:''},{av:'AV80Sistema_ProjetoNome1',fld:'vSISTEMA_PROJETONOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV68Sistema_Nome2',fld:'vSISTEMA_NOME2',pic:'@!',nv:''},{av:'AV63Sistema_Sigla2',fld:'vSISTEMA_SIGLA2',pic:'@!',nv:''},{av:'AV74Sistema_Tipo2',fld:'vSISTEMA_TIPO2',pic:'',nv:''},{av:'AV77Sistema_Tecnica2',fld:'vSISTEMA_TECNICA2',pic:'',nv:''},{av:'AV78Sistema_Coordenacao2',fld:'vSISTEMA_COORDENACAO2',pic:'@!',nv:''},{av:'AV81Sistema_ProjetoNome2',fld:'vSISTEMA_PROJETONOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV88TFSistema_Sigla',fld:'vTFSISTEMA_SIGLA',pic:'@!',nv:''},{av:'AV89TFSistema_Sigla_Sel',fld:'vTFSISTEMA_SIGLA_SEL',pic:'@!',nv:''},{av:'AV100TFSistema_Coordenacao',fld:'vTFSISTEMA_COORDENACAO',pic:'@!',nv:''},{av:'AV101TFSistema_Coordenacao_Sel',fld:'vTFSISTEMA_COORDENACAO_SEL',pic:'@!',nv:''},{av:'AV104TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV105TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV108TFMetodologia_Descricao',fld:'vTFMETODOLOGIA_DESCRICAO',pic:'@!',nv:''},{av:'AV109TFMetodologia_Descricao_Sel',fld:'vTFMETODOLOGIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV120TFSistema_ProjetoNome',fld:'vTFSISTEMA_PROJETONOME',pic:'@!',nv:''},{av:'AV121TFSistema_ProjetoNome_Sel',fld:'vTFSISTEMA_PROJETONOME_SEL',pic:'@!',nv:''},{av:'AV140TFSistemaVersao_Id',fld:'vTFSISTEMAVERSAO_ID',pic:'',nv:''},{av:'AV141TFSistemaVersao_Id_Sel',fld:'vTFSISTEMAVERSAO_ID_SEL',pic:'',nv:''},{av:'AV124TFSistema_Ativo_Sel',fld:'vTFSISTEMA_ATIVO_SEL',pic:'9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV90ddo_Sistema_SiglaTitleControlIdToReplace',fld:'vDDO_SISTEMA_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_Sistema_TecnicaTitleControlIdToReplace',fld:'vDDO_SISTEMA_TECNICATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Sistema_CoordenacaoTitleControlIdToReplace',fld:'vDDO_SISTEMA_COORDENACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_Metodologia_DescricaoTitleControlIdToReplace',fld:'vDDO_METODOLOGIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_Sistema_ProjetoNomeTitleControlIdToReplace',fld:'vDDO_SISTEMA_PROJETONOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV142ddo_SistemaVersao_IdTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_IDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV125ddo_Sistema_AtivoTitleControlIdToReplace',fld:'vDDO_SISTEMA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV137Paginando',fld:'vPAGINANDO',pic:'',nv:false},{av:'AV134Registros',fld:'vREGISTROS',pic:'ZZZZZZZZZ9',nv:0},{av:'AV135PreView',fld:'vPREVIEW',pic:'ZZZ9',nv:0},{av:'AV136PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZZZZZZZ9',nv:0},{av:'AV60Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV82Sistema_PF1',fld:'vSISTEMA_PF1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV83Sistema_PF2',fld:'vSISTEMA_PF2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV97TFSistema_Tecnica_Sels',fld:'vTFSISTEMA_TECNICA_SELS',pic:'',nv:null},{av:'AV183Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A351AmbienteTecnologico_Codigo',fld:'AMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A137Metodologia_Codigo',fld:'METODOLOGIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A691Sistema_ProjetoCod',fld:'SISTEMA_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'A1859SistemaVersao_Codigo',fld:'SISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A130Sistema_Ativo',fld:'SISTEMA_ATIVO',pic:'',hsh:true,nv:false},{av:'A707Sistema_TipoSigla',fld:'SISTEMA_TIPOSIGLA',pic:'@!',nv:''},{av:'A395Sistema_PF',fld:'SISTEMA_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A690Sistema_PFA',fld:'SISTEMA_PFA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0}],oparms:[{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV68Sistema_Nome2',fld:'vSISTEMA_NOME2',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'cmbavDynamicfiltersoperator2'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV82Sistema_PF1',fld:'vSISTEMA_PF1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV80Sistema_ProjetoNome1',fld:'vSISTEMA_PROJETONOME1',pic:'@!',nv:''},{av:'AV76Sistema_Coordenacao1',fld:'vSISTEMA_COORDENACAO1',pic:'@!',nv:''},{av:'AV75Sistema_Tecnica1',fld:'vSISTEMA_TECNICA1',pic:'',nv:''},{av:'AV73Sistema_Tipo1',fld:'vSISTEMA_TIPO1',pic:'',nv:''},{av:'AV62Sistema_Sigla1',fld:'vSISTEMA_SIGLA1',pic:'@!',nv:''},{av:'AV67Sistema_Nome1',fld:'vSISTEMA_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV83Sistema_PF2',fld:'vSISTEMA_PF2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV81Sistema_ProjetoNome2',fld:'vSISTEMA_PROJETONOME2',pic:'@!',nv:''},{av:'AV78Sistema_Coordenacao2',fld:'vSISTEMA_COORDENACAO2',pic:'@!',nv:''},{av:'AV77Sistema_Tecnica2',fld:'vSISTEMA_TECNICA2',pic:'',nv:''},{av:'AV74Sistema_Tipo2',fld:'vSISTEMA_TIPO2',pic:'',nv:''},{av:'AV63Sistema_Sigla2',fld:'vSISTEMA_SIGLA2',pic:'@!',nv:''},{av:'edtavSistema_nome2_Visible',ctrl:'vSISTEMA_NOME2',prop:'Visible'},{av:'edtavSistema_sigla2_Visible',ctrl:'vSISTEMA_SIGLA2',prop:'Visible'},{av:'cmbavSistema_tipo2'},{av:'cmbavSistema_tecnica2'},{av:'edtavSistema_coordenacao2_Visible',ctrl:'vSISTEMA_COORDENACAO2',prop:'Visible'},{av:'edtavSistema_projetonome2_Visible',ctrl:'vSISTEMA_PROJETONOME2',prop:'Visible'},{av:'edtavSistema_pf2_Visible',ctrl:'vSISTEMA_PF2',prop:'Visible'},{av:'edtavSistema_nome1_Visible',ctrl:'vSISTEMA_NOME1',prop:'Visible'},{av:'edtavSistema_sigla1_Visible',ctrl:'vSISTEMA_SIGLA1',prop:'Visible'},{av:'cmbavSistema_tipo1'},{av:'cmbavSistema_tecnica1'},{av:'edtavSistema_coordenacao1_Visible',ctrl:'vSISTEMA_COORDENACAO1',prop:'Visible'},{av:'edtavSistema_projetonome1_Visible',ctrl:'vSISTEMA_PROJETONOME1',prop:'Visible'},{av:'edtavSistema_pf1_Visible',ctrl:'vSISTEMA_PF1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E283G2',iparms:[{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavSistema_nome2_Visible',ctrl:'vSISTEMA_NOME2',prop:'Visible'},{av:'edtavSistema_sigla2_Visible',ctrl:'vSISTEMA_SIGLA2',prop:'Visible'},{av:'cmbavSistema_tipo2'},{av:'cmbavSistema_tecnica2'},{av:'edtavSistema_coordenacao2_Visible',ctrl:'vSISTEMA_COORDENACAO2',prop:'Visible'},{av:'edtavSistema_projetonome2_Visible',ctrl:'vSISTEMA_PROJETONOME2',prop:'Visible'},{av:'edtavSistema_pf2_Visible',ctrl:'vSISTEMA_PF2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E233G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV67Sistema_Nome1',fld:'vSISTEMA_NOME1',pic:'@!',nv:''},{av:'AV62Sistema_Sigla1',fld:'vSISTEMA_SIGLA1',pic:'@!',nv:''},{av:'AV73Sistema_Tipo1',fld:'vSISTEMA_TIPO1',pic:'',nv:''},{av:'AV75Sistema_Tecnica1',fld:'vSISTEMA_TECNICA1',pic:'',nv:''},{av:'AV76Sistema_Coordenacao1',fld:'vSISTEMA_COORDENACAO1',pic:'@!',nv:''},{av:'AV80Sistema_ProjetoNome1',fld:'vSISTEMA_PROJETONOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV68Sistema_Nome2',fld:'vSISTEMA_NOME2',pic:'@!',nv:''},{av:'AV63Sistema_Sigla2',fld:'vSISTEMA_SIGLA2',pic:'@!',nv:''},{av:'AV74Sistema_Tipo2',fld:'vSISTEMA_TIPO2',pic:'',nv:''},{av:'AV77Sistema_Tecnica2',fld:'vSISTEMA_TECNICA2',pic:'',nv:''},{av:'AV78Sistema_Coordenacao2',fld:'vSISTEMA_COORDENACAO2',pic:'@!',nv:''},{av:'AV81Sistema_ProjetoNome2',fld:'vSISTEMA_PROJETONOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV88TFSistema_Sigla',fld:'vTFSISTEMA_SIGLA',pic:'@!',nv:''},{av:'AV89TFSistema_Sigla_Sel',fld:'vTFSISTEMA_SIGLA_SEL',pic:'@!',nv:''},{av:'AV100TFSistema_Coordenacao',fld:'vTFSISTEMA_COORDENACAO',pic:'@!',nv:''},{av:'AV101TFSistema_Coordenacao_Sel',fld:'vTFSISTEMA_COORDENACAO_SEL',pic:'@!',nv:''},{av:'AV104TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV105TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV108TFMetodologia_Descricao',fld:'vTFMETODOLOGIA_DESCRICAO',pic:'@!',nv:''},{av:'AV109TFMetodologia_Descricao_Sel',fld:'vTFMETODOLOGIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV120TFSistema_ProjetoNome',fld:'vTFSISTEMA_PROJETONOME',pic:'@!',nv:''},{av:'AV121TFSistema_ProjetoNome_Sel',fld:'vTFSISTEMA_PROJETONOME_SEL',pic:'@!',nv:''},{av:'AV140TFSistemaVersao_Id',fld:'vTFSISTEMAVERSAO_ID',pic:'',nv:''},{av:'AV141TFSistemaVersao_Id_Sel',fld:'vTFSISTEMAVERSAO_ID_SEL',pic:'',nv:''},{av:'AV124TFSistema_Ativo_Sel',fld:'vTFSISTEMA_ATIVO_SEL',pic:'9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV90ddo_Sistema_SiglaTitleControlIdToReplace',fld:'vDDO_SISTEMA_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_Sistema_TecnicaTitleControlIdToReplace',fld:'vDDO_SISTEMA_TECNICATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Sistema_CoordenacaoTitleControlIdToReplace',fld:'vDDO_SISTEMA_COORDENACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_Metodologia_DescricaoTitleControlIdToReplace',fld:'vDDO_METODOLOGIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_Sistema_ProjetoNomeTitleControlIdToReplace',fld:'vDDO_SISTEMA_PROJETONOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV142ddo_SistemaVersao_IdTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_IDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV125ddo_Sistema_AtivoTitleControlIdToReplace',fld:'vDDO_SISTEMA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV137Paginando',fld:'vPAGINANDO',pic:'',nv:false},{av:'AV134Registros',fld:'vREGISTROS',pic:'ZZZZZZZZZ9',nv:0},{av:'AV135PreView',fld:'vPREVIEW',pic:'ZZZ9',nv:0},{av:'AV136PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZZZZZZZ9',nv:0},{av:'AV60Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV82Sistema_PF1',fld:'vSISTEMA_PF1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV83Sistema_PF2',fld:'vSISTEMA_PF2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV97TFSistema_Tecnica_Sels',fld:'vTFSISTEMA_TECNICA_SELS',pic:'',nv:null},{av:'AV183Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A351AmbienteTecnologico_Codigo',fld:'AMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A137Metodologia_Codigo',fld:'METODOLOGIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A691Sistema_ProjetoCod',fld:'SISTEMA_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'A1859SistemaVersao_Codigo',fld:'SISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A130Sistema_Ativo',fld:'SISTEMA_ATIVO',pic:'',hsh:true,nv:false},{av:'A707Sistema_TipoSigla',fld:'SISTEMA_TIPOSIGLA',pic:'@!',nv:''},{av:'A395Sistema_PF',fld:'SISTEMA_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A690Sistema_PFA',fld:'SISTEMA_PFA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0}],oparms:[{av:'AV136PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZZZZZZZ9',nv:0},{av:'AV60Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV88TFSistema_Sigla',fld:'vTFSISTEMA_SIGLA',pic:'@!',nv:''},{av:'Ddo_sistema_sigla_Filteredtext_set',ctrl:'DDO_SISTEMA_SIGLA',prop:'FilteredText_set'},{av:'AV89TFSistema_Sigla_Sel',fld:'vTFSISTEMA_SIGLA_SEL',pic:'@!',nv:''},{av:'Ddo_sistema_sigla_Selectedvalue_set',ctrl:'DDO_SISTEMA_SIGLA',prop:'SelectedValue_set'},{av:'AV97TFSistema_Tecnica_Sels',fld:'vTFSISTEMA_TECNICA_SELS',pic:'',nv:null},{av:'Ddo_sistema_tecnica_Selectedvalue_set',ctrl:'DDO_SISTEMA_TECNICA',prop:'SelectedValue_set'},{av:'AV100TFSistema_Coordenacao',fld:'vTFSISTEMA_COORDENACAO',pic:'@!',nv:''},{av:'Ddo_sistema_coordenacao_Filteredtext_set',ctrl:'DDO_SISTEMA_COORDENACAO',prop:'FilteredText_set'},{av:'AV101TFSistema_Coordenacao_Sel',fld:'vTFSISTEMA_COORDENACAO_SEL',pic:'@!',nv:''},{av:'Ddo_sistema_coordenacao_Selectedvalue_set',ctrl:'DDO_SISTEMA_COORDENACAO',prop:'SelectedValue_set'},{av:'AV104TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'Ddo_ambientetecnologico_descricao_Filteredtext_set',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'FilteredText_set'},{av:'AV105TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'Ddo_ambientetecnologico_descricao_Selectedvalue_set',ctrl:'DDO_AMBIENTETECNOLOGICO_DESCRICAO',prop:'SelectedValue_set'},{av:'AV108TFMetodologia_Descricao',fld:'vTFMETODOLOGIA_DESCRICAO',pic:'@!',nv:''},{av:'Ddo_metodologia_descricao_Filteredtext_set',ctrl:'DDO_METODOLOGIA_DESCRICAO',prop:'FilteredText_set'},{av:'AV109TFMetodologia_Descricao_Sel',fld:'vTFMETODOLOGIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'Ddo_metodologia_descricao_Selectedvalue_set',ctrl:'DDO_METODOLOGIA_DESCRICAO',prop:'SelectedValue_set'},{av:'AV120TFSistema_ProjetoNome',fld:'vTFSISTEMA_PROJETONOME',pic:'@!',nv:''},{av:'Ddo_sistema_projetonome_Filteredtext_set',ctrl:'DDO_SISTEMA_PROJETONOME',prop:'FilteredText_set'},{av:'AV121TFSistema_ProjetoNome_Sel',fld:'vTFSISTEMA_PROJETONOME_SEL',pic:'@!',nv:''},{av:'Ddo_sistema_projetonome_Selectedvalue_set',ctrl:'DDO_SISTEMA_PROJETONOME',prop:'SelectedValue_set'},{av:'AV140TFSistemaVersao_Id',fld:'vTFSISTEMAVERSAO_ID',pic:'',nv:''},{av:'Ddo_sistemaversao_id_Filteredtext_set',ctrl:'DDO_SISTEMAVERSAO_ID',prop:'FilteredText_set'},{av:'AV141TFSistemaVersao_Id_Sel',fld:'vTFSISTEMAVERSAO_ID_SEL',pic:'',nv:''},{av:'Ddo_sistemaversao_id_Selectedvalue_set',ctrl:'DDO_SISTEMAVERSAO_ID',prop:'SelectedValue_set'},{av:'AV124TFSistema_Ativo_Sel',fld:'vTFSISTEMA_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_sistema_ativo_Selectedvalue_set',ctrl:'DDO_SISTEMA_ATIVO',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV67Sistema_Nome1',fld:'vSISTEMA_NOME1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavSistema_nome1_Visible',ctrl:'vSISTEMA_NOME1',prop:'Visible'},{av:'edtavSistema_sigla1_Visible',ctrl:'vSISTEMA_SIGLA1',prop:'Visible'},{av:'cmbavSistema_tipo1'},{av:'cmbavSistema_tecnica1'},{av:'edtavSistema_coordenacao1_Visible',ctrl:'vSISTEMA_COORDENACAO1',prop:'Visible'},{av:'edtavSistema_projetonome1_Visible',ctrl:'vSISTEMA_PROJETONOME1',prop:'Visible'},{av:'edtavSistema_pf1_Visible',ctrl:'vSISTEMA_PF1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV68Sistema_Nome2',fld:'vSISTEMA_NOME2',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV82Sistema_PF1',fld:'vSISTEMA_PF1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV80Sistema_ProjetoNome1',fld:'vSISTEMA_PROJETONOME1',pic:'@!',nv:''},{av:'AV76Sistema_Coordenacao1',fld:'vSISTEMA_COORDENACAO1',pic:'@!',nv:''},{av:'AV75Sistema_Tecnica1',fld:'vSISTEMA_TECNICA1',pic:'',nv:''},{av:'AV73Sistema_Tipo1',fld:'vSISTEMA_TIPO1',pic:'',nv:''},{av:'AV62Sistema_Sigla1',fld:'vSISTEMA_SIGLA1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV83Sistema_PF2',fld:'vSISTEMA_PF2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV81Sistema_ProjetoNome2',fld:'vSISTEMA_PROJETONOME2',pic:'@!',nv:''},{av:'AV78Sistema_Coordenacao2',fld:'vSISTEMA_COORDENACAO2',pic:'@!',nv:''},{av:'AV77Sistema_Tecnica2',fld:'vSISTEMA_TECNICA2',pic:'',nv:''},{av:'AV74Sistema_Tipo2',fld:'vSISTEMA_TIPO2',pic:'',nv:''},{av:'AV63Sistema_Sigla2',fld:'vSISTEMA_SIGLA2',pic:'@!',nv:''},{av:'edtavSistema_nome2_Visible',ctrl:'vSISTEMA_NOME2',prop:'Visible'},{av:'edtavSistema_sigla2_Visible',ctrl:'vSISTEMA_SIGLA2',prop:'Visible'},{av:'cmbavSistema_tipo2'},{av:'cmbavSistema_tecnica2'},{av:'edtavSistema_coordenacao2_Visible',ctrl:'vSISTEMA_COORDENACAO2',prop:'Visible'},{av:'edtavSistema_projetonome2_Visible',ctrl:'vSISTEMA_PROJETONOME2',prop:'Visible'},{av:'edtavSistema_pf2_Visible',ctrl:'vSISTEMA_PF2',prop:'Visible'}]}");
         setEventMetadata("'DOASSOCIARCLIENTES'","{handler:'E333G2',iparms:[{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}],oparms:[]}");
         setEventMetadata("'DOINSERT'","{handler:'E243G2',iparms:[],oparms:[]}");
         setEventMetadata("'DOEXPORTREPORT'","{handler:'E253G2',iparms:[{av:'Ddo_sistema_tecnica_Selectedvalue_get',ctrl:'DDO_SISTEMA_TECNICA',prop:'SelectedValue_get'},{av:'AV60Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV88TFSistema_Sigla',fld:'vTFSISTEMA_SIGLA',pic:'@!',nv:''},{av:'AV89TFSistema_Sigla_Sel',fld:'vTFSISTEMA_SIGLA_SEL',pic:'@!',nv:''},{av:'AV100TFSistema_Coordenacao',fld:'vTFSISTEMA_COORDENACAO',pic:'@!',nv:''},{av:'AV101TFSistema_Coordenacao_Sel',fld:'vTFSISTEMA_COORDENACAO_SEL',pic:'@!',nv:''},{av:'AV104TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV105TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV108TFMetodologia_Descricao',fld:'vTFMETODOLOGIA_DESCRICAO',pic:'@!',nv:''},{av:'AV109TFMetodologia_Descricao_Sel',fld:'vTFMETODOLOGIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV120TFSistema_ProjetoNome',fld:'vTFSISTEMA_PROJETONOME',pic:'@!',nv:''},{av:'AV121TFSistema_ProjetoNome_Sel',fld:'vTFSISTEMA_PROJETONOME_SEL',pic:'@!',nv:''},{av:'AV140TFSistemaVersao_Id',fld:'vTFSISTEMAVERSAO_ID',pic:'',nv:''},{av:'AV141TFSistemaVersao_Id_Sel',fld:'vTFSISTEMAVERSAO_ID_SEL',pic:'',nv:''},{av:'AV124TFSistema_Ativo_Sel',fld:'vTFSISTEMA_ATIVO_SEL',pic:'9',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV67Sistema_Nome1',fld:'vSISTEMA_NOME1',pic:'@!',nv:''},{av:'AV62Sistema_Sigla1',fld:'vSISTEMA_SIGLA1',pic:'@!',nv:''},{av:'AV73Sistema_Tipo1',fld:'vSISTEMA_TIPO1',pic:'',nv:''},{av:'AV75Sistema_Tecnica1',fld:'vSISTEMA_TECNICA1',pic:'',nv:''},{av:'AV76Sistema_Coordenacao1',fld:'vSISTEMA_COORDENACAO1',pic:'@!',nv:''},{av:'AV80Sistema_ProjetoNome1',fld:'vSISTEMA_PROJETONOME1',pic:'@!',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV82Sistema_PF1',fld:'vSISTEMA_PF1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV68Sistema_Nome2',fld:'vSISTEMA_NOME2',pic:'@!',nv:''},{av:'AV63Sistema_Sigla2',fld:'vSISTEMA_SIGLA2',pic:'@!',nv:''},{av:'AV74Sistema_Tipo2',fld:'vSISTEMA_TIPO2',pic:'',nv:''},{av:'AV77Sistema_Tecnica2',fld:'vSISTEMA_TECNICA2',pic:'',nv:''},{av:'AV78Sistema_Coordenacao2',fld:'vSISTEMA_COORDENACAO2',pic:'@!',nv:''},{av:'AV81Sistema_ProjetoNome2',fld:'vSISTEMA_PROJETONOME2',pic:'@!',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV83Sistema_PF2',fld:'vSISTEMA_PF2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0}],oparms:[{av:'Innewwindow1_Target',ctrl:'INNEWWINDOW1',prop:'Target'},{av:'Innewwindow1_Height',ctrl:'INNEWWINDOW1',prop:'Height'},{av:'Innewwindow1_Width',ctrl:'INNEWWINDOW1',prop:'Width'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("ONMESSAGE_GX1","{handler:'E293G2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV67Sistema_Nome1',fld:'vSISTEMA_NOME1',pic:'@!',nv:''},{av:'AV62Sistema_Sigla1',fld:'vSISTEMA_SIGLA1',pic:'@!',nv:''},{av:'AV73Sistema_Tipo1',fld:'vSISTEMA_TIPO1',pic:'',nv:''},{av:'AV75Sistema_Tecnica1',fld:'vSISTEMA_TECNICA1',pic:'',nv:''},{av:'AV76Sistema_Coordenacao1',fld:'vSISTEMA_COORDENACAO1',pic:'@!',nv:''},{av:'AV80Sistema_ProjetoNome1',fld:'vSISTEMA_PROJETONOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV68Sistema_Nome2',fld:'vSISTEMA_NOME2',pic:'@!',nv:''},{av:'AV63Sistema_Sigla2',fld:'vSISTEMA_SIGLA2',pic:'@!',nv:''},{av:'AV74Sistema_Tipo2',fld:'vSISTEMA_TIPO2',pic:'',nv:''},{av:'AV77Sistema_Tecnica2',fld:'vSISTEMA_TECNICA2',pic:'',nv:''},{av:'AV78Sistema_Coordenacao2',fld:'vSISTEMA_COORDENACAO2',pic:'@!',nv:''},{av:'AV81Sistema_ProjetoNome2',fld:'vSISTEMA_PROJETONOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV88TFSistema_Sigla',fld:'vTFSISTEMA_SIGLA',pic:'@!',nv:''},{av:'AV89TFSistema_Sigla_Sel',fld:'vTFSISTEMA_SIGLA_SEL',pic:'@!',nv:''},{av:'AV100TFSistema_Coordenacao',fld:'vTFSISTEMA_COORDENACAO',pic:'@!',nv:''},{av:'AV101TFSistema_Coordenacao_Sel',fld:'vTFSISTEMA_COORDENACAO_SEL',pic:'@!',nv:''},{av:'AV104TFAmbienteTecnologico_Descricao',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO',pic:'@!',nv:''},{av:'AV105TFAmbienteTecnologico_Descricao_Sel',fld:'vTFAMBIENTETECNOLOGICO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV108TFMetodologia_Descricao',fld:'vTFMETODOLOGIA_DESCRICAO',pic:'@!',nv:''},{av:'AV109TFMetodologia_Descricao_Sel',fld:'vTFMETODOLOGIA_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV120TFSistema_ProjetoNome',fld:'vTFSISTEMA_PROJETONOME',pic:'@!',nv:''},{av:'AV121TFSistema_ProjetoNome_Sel',fld:'vTFSISTEMA_PROJETONOME_SEL',pic:'@!',nv:''},{av:'AV140TFSistemaVersao_Id',fld:'vTFSISTEMAVERSAO_ID',pic:'',nv:''},{av:'AV141TFSistemaVersao_Id_Sel',fld:'vTFSISTEMAVERSAO_ID_SEL',pic:'',nv:''},{av:'AV124TFSistema_Ativo_Sel',fld:'vTFSISTEMA_ATIVO_SEL',pic:'9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV90ddo_Sistema_SiglaTitleControlIdToReplace',fld:'vDDO_SISTEMA_SIGLATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV98ddo_Sistema_TecnicaTitleControlIdToReplace',fld:'vDDO_SISTEMA_TECNICATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV102ddo_Sistema_CoordenacaoTitleControlIdToReplace',fld:'vDDO_SISTEMA_COORDENACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV106ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace',fld:'vDDO_AMBIENTETECNOLOGICO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110ddo_Metodologia_DescricaoTitleControlIdToReplace',fld:'vDDO_METODOLOGIA_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_Sistema_ProjetoNomeTitleControlIdToReplace',fld:'vDDO_SISTEMA_PROJETONOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV142ddo_SistemaVersao_IdTitleControlIdToReplace',fld:'vDDO_SISTEMAVERSAO_IDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV125ddo_Sistema_AtivoTitleControlIdToReplace',fld:'vDDO_SISTEMA_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV137Paginando',fld:'vPAGINANDO',pic:'',nv:false},{av:'AV134Registros',fld:'vREGISTROS',pic:'ZZZZZZZZZ9',nv:0},{av:'AV135PreView',fld:'vPREVIEW',pic:'ZZZ9',nv:0},{av:'AV136PaginaAtual',fld:'vPAGINAATUAL',pic:'ZZZZZZZZZ9',nv:0},{av:'AV60Sistema_AreaTrabalhoCod',fld:'vSISTEMA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV82Sistema_PF1',fld:'vSISTEMA_PF1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV83Sistema_PF2',fld:'vSISTEMA_PF2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV97TFSistema_Tecnica_Sels',fld:'vTFSISTEMA_TECNICA_SELS',pic:'',nv:null},{av:'AV183Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A351AmbienteTecnologico_Codigo',fld:'AMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A137Metodologia_Codigo',fld:'METODOLOGIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A691Sistema_ProjetoCod',fld:'SISTEMA_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'A1859SistemaVersao_Codigo',fld:'SISTEMAVERSAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A130Sistema_Ativo',fld:'SISTEMA_ATIVO',pic:'',hsh:true,nv:false},{av:'A707Sistema_TipoSigla',fld:'SISTEMA_TIPOSIGLA',pic:'@!',nv:''},{av:'A395Sistema_PF',fld:'SISTEMA_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A690Sistema_PFA',fld:'SISTEMA_PFA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV61NotificationInfo',fld:'vNOTIFICATIONINFO',pic:'',nv:null}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_sistema_sigla_Activeeventkey = "";
         Ddo_sistema_sigla_Filteredtext_get = "";
         Ddo_sistema_sigla_Selectedvalue_get = "";
         Ddo_sistema_tecnica_Activeeventkey = "";
         Ddo_sistema_tecnica_Selectedvalue_get = "";
         Ddo_sistema_coordenacao_Activeeventkey = "";
         Ddo_sistema_coordenacao_Filteredtext_get = "";
         Ddo_sistema_coordenacao_Selectedvalue_get = "";
         Ddo_ambientetecnologico_descricao_Activeeventkey = "";
         Ddo_ambientetecnologico_descricao_Filteredtext_get = "";
         Ddo_ambientetecnologico_descricao_Selectedvalue_get = "";
         Ddo_metodologia_descricao_Activeeventkey = "";
         Ddo_metodologia_descricao_Filteredtext_get = "";
         Ddo_metodologia_descricao_Selectedvalue_get = "";
         Ddo_sistema_projetonome_Activeeventkey = "";
         Ddo_sistema_projetonome_Filteredtext_get = "";
         Ddo_sistema_projetonome_Selectedvalue_get = "";
         Ddo_sistemaversao_id_Activeeventkey = "";
         Ddo_sistemaversao_id_Filteredtext_get = "";
         Ddo_sistemaversao_id_Selectedvalue_get = "";
         Ddo_sistema_ativo_Activeeventkey = "";
         Ddo_sistema_ativo_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV67Sistema_Nome1 = "";
         AV62Sistema_Sigla1 = "";
         AV73Sistema_Tipo1 = "A";
         AV75Sistema_Tecnica1 = "";
         AV76Sistema_Coordenacao1 = "";
         AV80Sistema_ProjetoNome1 = "";
         AV21DynamicFiltersSelector2 = "";
         AV68Sistema_Nome2 = "";
         AV63Sistema_Sigla2 = "";
         AV74Sistema_Tipo2 = "A";
         AV77Sistema_Tecnica2 = "";
         AV78Sistema_Coordenacao2 = "";
         AV81Sistema_ProjetoNome2 = "";
         AV88TFSistema_Sigla = "";
         AV89TFSistema_Sigla_Sel = "";
         AV100TFSistema_Coordenacao = "";
         AV101TFSistema_Coordenacao_Sel = "";
         AV104TFAmbienteTecnologico_Descricao = "";
         AV105TFAmbienteTecnologico_Descricao_Sel = "";
         AV108TFMetodologia_Descricao = "";
         AV109TFMetodologia_Descricao_Sel = "";
         AV120TFSistema_ProjetoNome = "";
         AV121TFSistema_ProjetoNome_Sel = "";
         AV140TFSistemaVersao_Id = "";
         AV141TFSistemaVersao_Id_Sel = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV90ddo_Sistema_SiglaTitleControlIdToReplace = "";
         AV98ddo_Sistema_TecnicaTitleControlIdToReplace = "";
         AV102ddo_Sistema_CoordenacaoTitleControlIdToReplace = "";
         AV106ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace = "";
         AV110ddo_Metodologia_DescricaoTitleControlIdToReplace = "";
         AV122ddo_Sistema_ProjetoNomeTitleControlIdToReplace = "";
         AV142ddo_SistemaVersao_IdTitleControlIdToReplace = "";
         AV125ddo_Sistema_AtivoTitleControlIdToReplace = "";
         AV97TFSistema_Tecnica_Sels = new GxSimpleCollection();
         AV183Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         A707Sistema_TipoSigla = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV126DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV87Sistema_SiglaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV95Sistema_TecnicaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV99Sistema_CoordenacaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV103AmbienteTecnologico_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV107Metodologia_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV119Sistema_ProjetoNomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV139SistemaVersao_IdTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV123Sistema_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV61NotificationInfo = new SdtNotificationInfo(context);
         A699Sistema_Tipo = "";
         Ddo_sistema_sigla_Filteredtext_set = "";
         Ddo_sistema_sigla_Selectedvalue_set = "";
         Ddo_sistema_sigla_Sortedstatus = "";
         Ddo_sistema_tecnica_Selectedvalue_set = "";
         Ddo_sistema_tecnica_Sortedstatus = "";
         Ddo_sistema_coordenacao_Filteredtext_set = "";
         Ddo_sistema_coordenacao_Selectedvalue_set = "";
         Ddo_sistema_coordenacao_Sortedstatus = "";
         Ddo_ambientetecnologico_descricao_Filteredtext_set = "";
         Ddo_ambientetecnologico_descricao_Selectedvalue_set = "";
         Ddo_ambientetecnologico_descricao_Sortedstatus = "";
         Ddo_metodologia_descricao_Filteredtext_set = "";
         Ddo_metodologia_descricao_Selectedvalue_set = "";
         Ddo_metodologia_descricao_Sortedstatus = "";
         Ddo_sistema_projetonome_Filteredtext_set = "";
         Ddo_sistema_projetonome_Selectedvalue_set = "";
         Ddo_sistema_projetonome_Sortedstatus = "";
         Ddo_sistemaversao_id_Filteredtext_set = "";
         Ddo_sistemaversao_id_Selectedvalue_set = "";
         Ddo_sistemaversao_id_Sortedstatus = "";
         Ddo_sistema_ativo_Selectedvalue_set = "";
         Ddo_sistema_ativo_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV34Update = "";
         AV179Update_GXI = "";
         AV35Delete = "";
         AV180Delete_GXI = "";
         AV58Display = "";
         AV181Display_GXI = "";
         A416Sistema_Nome = "";
         A129Sistema_Sigla = "";
         AV130Sistema_TipoSigla = "";
         A700Sistema_Tecnica = "";
         A513Sistema_Coordenacao = "";
         A352AmbienteTecnologico_Descricao = "";
         A138Metodologia_Descricao = "";
         A703Sistema_ProjetoNome = "";
         A1860SistemaVersao_Id = "";
         AV84AssociarClientes = "";
         AV182Associarclientes_GXI = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         AV167WWSistemaDS_23_Tfsistema_tecnica_sels = new GxSimpleCollection();
         scmdbuf = "";
         lV148WWSistemaDS_4_Sistema_nome1 = "";
         lV149WWSistemaDS_5_Sistema_sigla1 = "";
         lV152WWSistemaDS_8_Sistema_coordenacao1 = "";
         lV153WWSistemaDS_9_Sistema_projetonome1 = "";
         lV158WWSistemaDS_14_Sistema_nome2 = "";
         lV159WWSistemaDS_15_Sistema_sigla2 = "";
         lV162WWSistemaDS_18_Sistema_coordenacao2 = "";
         lV163WWSistemaDS_19_Sistema_projetonome2 = "";
         lV165WWSistemaDS_21_Tfsistema_sigla = "";
         lV168WWSistemaDS_24_Tfsistema_coordenacao = "";
         lV170WWSistemaDS_26_Tfambientetecnologico_descricao = "";
         lV172WWSistemaDS_28_Tfmetodologia_descricao = "";
         lV174WWSistemaDS_30_Tfsistema_projetonome = "";
         lV176WWSistemaDS_32_Tfsistemaversao_id = "";
         AV146WWSistemaDS_2_Dynamicfiltersselector1 = "";
         AV148WWSistemaDS_4_Sistema_nome1 = "";
         AV149WWSistemaDS_5_Sistema_sigla1 = "";
         AV150WWSistemaDS_6_Sistema_tipo1 = "";
         AV151WWSistemaDS_7_Sistema_tecnica1 = "";
         AV152WWSistemaDS_8_Sistema_coordenacao1 = "";
         AV153WWSistemaDS_9_Sistema_projetonome1 = "";
         AV156WWSistemaDS_12_Dynamicfiltersselector2 = "";
         AV158WWSistemaDS_14_Sistema_nome2 = "";
         AV159WWSistemaDS_15_Sistema_sigla2 = "";
         AV160WWSistemaDS_16_Sistema_tipo2 = "";
         AV161WWSistemaDS_17_Sistema_tecnica2 = "";
         AV162WWSistemaDS_18_Sistema_coordenacao2 = "";
         AV163WWSistemaDS_19_Sistema_projetonome2 = "";
         AV166WWSistemaDS_22_Tfsistema_sigla_sel = "";
         AV165WWSistemaDS_21_Tfsistema_sigla = "";
         AV169WWSistemaDS_25_Tfsistema_coordenacao_sel = "";
         AV168WWSistemaDS_24_Tfsistema_coordenacao = "";
         AV171WWSistemaDS_27_Tfambientetecnologico_descricao_sel = "";
         AV170WWSistemaDS_26_Tfambientetecnologico_descricao = "";
         AV173WWSistemaDS_29_Tfmetodologia_descricao_sel = "";
         AV172WWSistemaDS_28_Tfmetodologia_descricao = "";
         AV175WWSistemaDS_31_Tfsistema_projetonome_sel = "";
         AV174WWSistemaDS_30_Tfsistema_projetonome = "";
         AV177WWSistemaDS_33_Tfsistemaversao_id_sel = "";
         AV176WWSistemaDS_32_Tfsistemaversao_id = "";
         H003G2_A135Sistema_AreaTrabalhoCod = new int[1] ;
         H003G2_A351AmbienteTecnologico_Codigo = new int[1] ;
         H003G2_n351AmbienteTecnologico_Codigo = new bool[] {false} ;
         H003G2_A691Sistema_ProjetoCod = new int[1] ;
         H003G2_n691Sistema_ProjetoCod = new bool[] {false} ;
         H003G2_A1859SistemaVersao_Codigo = new int[1] ;
         H003G2_n1859SistemaVersao_Codigo = new bool[] {false} ;
         H003G2_A130Sistema_Ativo = new bool[] {false} ;
         H003G2_A1860SistemaVersao_Id = new String[] {""} ;
         H003G2_A703Sistema_ProjetoNome = new String[] {""} ;
         H003G2_n703Sistema_ProjetoNome = new bool[] {false} ;
         H003G2_A138Metodologia_Descricao = new String[] {""} ;
         H003G2_A137Metodologia_Codigo = new int[1] ;
         H003G2_n137Metodologia_Codigo = new bool[] {false} ;
         H003G2_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         H003G2_A513Sistema_Coordenacao = new String[] {""} ;
         H003G2_n513Sistema_Coordenacao = new bool[] {false} ;
         H003G2_A700Sistema_Tecnica = new String[] {""} ;
         H003G2_n700Sistema_Tecnica = new bool[] {false} ;
         H003G2_A129Sistema_Sigla = new String[] {""} ;
         H003G2_A416Sistema_Nome = new String[] {""} ;
         H003G2_A686Sistema_FatorAjuste = new decimal[1] ;
         H003G2_n686Sistema_FatorAjuste = new bool[] {false} ;
         H003G2_A127Sistema_Codigo = new int[1] ;
         H003G2_A699Sistema_Tipo = new String[] {""} ;
         H003G2_n699Sistema_Tipo = new bool[] {false} ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons2 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV96TFSistema_Tecnica_SelsJson = "";
         GridRow = new GXWebRow();
         AV36Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         H003G3_A137Metodologia_Codigo = new int[1] ;
         H003G3_n137Metodologia_Codigo = new bool[] {false} ;
         H003G3_A351AmbienteTecnologico_Codigo = new int[1] ;
         H003G3_n351AmbienteTecnologico_Codigo = new bool[] {false} ;
         H003G3_A1859SistemaVersao_Codigo = new int[1] ;
         H003G3_n1859SistemaVersao_Codigo = new bool[] {false} ;
         H003G3_A691Sistema_ProjetoCod = new int[1] ;
         H003G3_n691Sistema_ProjetoCod = new bool[] {false} ;
         H003G3_A130Sistema_Ativo = new bool[] {false} ;
         H003G3_A1860SistemaVersao_Id = new String[] {""} ;
         H003G3_A138Metodologia_Descricao = new String[] {""} ;
         H003G3_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         H003G3_A703Sistema_ProjetoNome = new String[] {""} ;
         H003G3_n703Sistema_ProjetoNome = new bool[] {false} ;
         H003G3_A513Sistema_Coordenacao = new String[] {""} ;
         H003G3_n513Sistema_Coordenacao = new bool[] {false} ;
         H003G3_A700Sistema_Tecnica = new String[] {""} ;
         H003G3_n700Sistema_Tecnica = new bool[] {false} ;
         H003G3_A699Sistema_Tipo = new String[] {""} ;
         H003G3_n699Sistema_Tipo = new bool[] {false} ;
         H003G3_A129Sistema_Sigla = new String[] {""} ;
         H003G3_A416Sistema_Nome = new String[] {""} ;
         H003G3_A135Sistema_AreaTrabalhoCod = new int[1] ;
         H003G3_A127Sistema_Codigo = new int[1] ;
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblSistematitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblFiltertextsistema_areatrabalhocod_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         imgInsert_Jsonclick = "";
         imgExportreport_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwsistema__default(),
            new Object[][] {
                new Object[] {
               H003G2_A135Sistema_AreaTrabalhoCod, H003G2_A351AmbienteTecnologico_Codigo, H003G2_n351AmbienteTecnologico_Codigo, H003G2_A691Sistema_ProjetoCod, H003G2_n691Sistema_ProjetoCod, H003G2_A1859SistemaVersao_Codigo, H003G2_n1859SistemaVersao_Codigo, H003G2_A130Sistema_Ativo, H003G2_A1860SistemaVersao_Id, H003G2_A703Sistema_ProjetoNome,
               H003G2_n703Sistema_ProjetoNome, H003G2_A138Metodologia_Descricao, H003G2_A137Metodologia_Codigo, H003G2_n137Metodologia_Codigo, H003G2_A352AmbienteTecnologico_Descricao, H003G2_A513Sistema_Coordenacao, H003G2_n513Sistema_Coordenacao, H003G2_A700Sistema_Tecnica, H003G2_n700Sistema_Tecnica, H003G2_A129Sistema_Sigla,
               H003G2_A416Sistema_Nome, H003G2_A686Sistema_FatorAjuste, H003G2_n686Sistema_FatorAjuste, H003G2_A127Sistema_Codigo, H003G2_A699Sistema_Tipo, H003G2_n699Sistema_Tipo
               }
               , new Object[] {
               H003G3_A137Metodologia_Codigo, H003G3_n137Metodologia_Codigo, H003G3_A351AmbienteTecnologico_Codigo, H003G3_n351AmbienteTecnologico_Codigo, H003G3_A1859SistemaVersao_Codigo, H003G3_n1859SistemaVersao_Codigo, H003G3_A691Sistema_ProjetoCod, H003G3_n691Sistema_ProjetoCod, H003G3_A130Sistema_Ativo, H003G3_A1860SistemaVersao_Id,
               H003G3_A138Metodologia_Descricao, H003G3_A352AmbienteTecnologico_Descricao, H003G3_A703Sistema_ProjetoNome, H003G3_n703Sistema_ProjetoNome, H003G3_A513Sistema_Coordenacao, H003G3_n513Sistema_Coordenacao, H003G3_A700Sistema_Tecnica, H003G3_n700Sistema_Tecnica, H003G3_A699Sistema_Tipo, H003G3_n699Sistema_Tipo,
               H003G3_A129Sistema_Sigla, H003G3_A416Sistema_Nome, H003G3_A135Sistema_AreaTrabalhoCod, H003G3_A127Sistema_Codigo
               }
            }
         );
         AV183Pgmname = "WWSistema";
         /* GeneXus formulas. */
         AV183Pgmname = "WWSistema";
         context.Gx_err = 0;
         edtavSistema_tiposigla_Enabled = 0;
         edtavPfb_Enabled = 0;
         edtavPfa_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_89 ;
      private short nGXsfl_89_idx=1 ;
      private short AV13OrderedBy ;
      private short AV124TFSistema_Ativo_Sel ;
      private short AV135PreView ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV22DynamicFiltersOperator2 ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_89_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV178WWSistemaDS_34_Tfsistema_ativo_sel ;
      private short AV147WWSistemaDS_3_Dynamicfiltersoperator1 ;
      private short AV157WWSistemaDS_13_Dynamicfiltersoperator2 ;
      private short edtSistema_Sigla_Titleformat ;
      private short cmbSistema_Tecnica_Titleformat ;
      private short edtSistema_Coordenacao_Titleformat ;
      private short edtAmbienteTecnologico_Descricao_Titleformat ;
      private short edtMetodologia_Descricao_Titleformat ;
      private short edtSistema_ProjetoNome_Titleformat ;
      private short edtSistemaVersao_Id_Titleformat ;
      private short chkSistema_Ativo_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV60Sistema_AreaTrabalhoCod ;
      private int A127Sistema_Codigo ;
      private int A351AmbienteTecnologico_Codigo ;
      private int A137Metodologia_Codigo ;
      private int A691Sistema_ProjetoCod ;
      private int A1859SistemaVersao_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_sistema_sigla_Datalistupdateminimumcharacters ;
      private int Ddo_sistema_coordenacao_Datalistupdateminimumcharacters ;
      private int Ddo_ambientetecnologico_descricao_Datalistupdateminimumcharacters ;
      private int Ddo_metodologia_descricao_Datalistupdateminimumcharacters ;
      private int Ddo_sistema_projetonome_Datalistupdateminimumcharacters ;
      private int Ddo_sistemaversao_id_Datalistupdateminimumcharacters ;
      private int edtavTfsistema_sigla_Visible ;
      private int edtavTfsistema_sigla_sel_Visible ;
      private int edtavTfsistema_coordenacao_Visible ;
      private int edtavTfsistema_coordenacao_sel_Visible ;
      private int edtavTfambientetecnologico_descricao_Visible ;
      private int edtavTfambientetecnologico_descricao_sel_Visible ;
      private int edtavTfmetodologia_descricao_Visible ;
      private int edtavTfmetodologia_descricao_sel_Visible ;
      private int edtavTfsistema_projetonome_Visible ;
      private int edtavTfsistema_projetonome_sel_Visible ;
      private int edtavTfsistemaversao_id_Visible ;
      private int edtavTfsistemaversao_id_sel_Visible ;
      private int edtavTfsistema_ativo_sel_Visible ;
      private int edtavDdo_sistema_siglatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_sistema_tecnicatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_sistema_coordenacaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_metodologia_descricaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_sistema_projetonometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_sistemaversao_idtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_sistema_ativotitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int edtavSistema_tiposigla_Enabled ;
      private int edtavPfb_Enabled ;
      private int edtavPfa_Enabled ;
      private int AV167WWSistemaDS_23_Tfsistema_tecnica_sels_Count ;
      private int AV6WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A135Sistema_AreaTrabalhoCod ;
      private int AV145WWSistemaDS_1_Sistema_areatrabalhocod ;
      private int edtavOrdereddsc_Visible ;
      private int edtavAssociarclientes_Visible ;
      private int imgInsert_Visible ;
      private int edtavUpdate_Visible ;
      private int edtavDelete_Visible ;
      private int edtavDisplay_Visible ;
      private int AV127PageToGo ;
      private int edtSistema_Sigla_Forecolor ;
      private int edtavSistema_tiposigla_Forecolor ;
      private int edtSistema_Coordenacao_Forecolor ;
      private int edtAmbienteTecnologico_Descricao_Forecolor ;
      private int edtMetodologia_Descricao_Forecolor ;
      private int edtavPfb_Forecolor ;
      private int edtavPfa_Forecolor ;
      private int edtSistema_ProjetoNome_Forecolor ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int edtavSistema_nome1_Visible ;
      private int edtavSistema_sigla1_Visible ;
      private int edtavSistema_coordenacao1_Visible ;
      private int edtavSistema_projetonome1_Visible ;
      private int edtavSistema_pf1_Visible ;
      private int edtavSistema_nome2_Visible ;
      private int edtavSistema_sigla2_Visible ;
      private int edtavSistema_coordenacao2_Visible ;
      private int edtavSistema_projetonome2_Visible ;
      private int edtavSistema_pf2_Visible ;
      private int AV184GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSistema_tiposigla_Visible ;
      private int edtavPfb_Visible ;
      private int edtavPfa_Visible ;
      private int edtavAssociarclientes_Enabled ;
      private long AV134Registros ;
      private long AV136PaginaAtual ;
      private long GRID_nFirstRecordOnPage ;
      private long AV128GridCurrentPage ;
      private long AV129GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private long GXt_int3 ;
      private long AV138Color ;
      private decimal AV82Sistema_PF1 ;
      private decimal AV83Sistema_PF2 ;
      private decimal A395Sistema_PF ;
      private decimal A690Sistema_PFA ;
      private decimal A686Sistema_FatorAjuste ;
      private decimal AV131PFB ;
      private decimal AV133PFA ;
      private decimal AV154WWSistemaDS_10_Sistema_pf1 ;
      private decimal AV164WWSistemaDS_20_Sistema_pf2 ;
      private decimal GXt_decimal1 ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_sistema_sigla_Activeeventkey ;
      private String Ddo_sistema_sigla_Filteredtext_get ;
      private String Ddo_sistema_sigla_Selectedvalue_get ;
      private String Ddo_sistema_tecnica_Activeeventkey ;
      private String Ddo_sistema_tecnica_Selectedvalue_get ;
      private String Ddo_sistema_coordenacao_Activeeventkey ;
      private String Ddo_sistema_coordenacao_Filteredtext_get ;
      private String Ddo_sistema_coordenacao_Selectedvalue_get ;
      private String Ddo_ambientetecnologico_descricao_Activeeventkey ;
      private String Ddo_ambientetecnologico_descricao_Filteredtext_get ;
      private String Ddo_ambientetecnologico_descricao_Selectedvalue_get ;
      private String Ddo_metodologia_descricao_Activeeventkey ;
      private String Ddo_metodologia_descricao_Filteredtext_get ;
      private String Ddo_metodologia_descricao_Selectedvalue_get ;
      private String Ddo_sistema_projetonome_Activeeventkey ;
      private String Ddo_sistema_projetonome_Filteredtext_get ;
      private String Ddo_sistema_projetonome_Selectedvalue_get ;
      private String Ddo_sistemaversao_id_Activeeventkey ;
      private String Ddo_sistemaversao_id_Filteredtext_get ;
      private String Ddo_sistemaversao_id_Selectedvalue_get ;
      private String Ddo_sistema_ativo_Activeeventkey ;
      private String Ddo_sistema_ativo_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_89_idx="0001" ;
      private String AV62Sistema_Sigla1 ;
      private String AV73Sistema_Tipo1 ;
      private String AV75Sistema_Tecnica1 ;
      private String AV80Sistema_ProjetoNome1 ;
      private String AV63Sistema_Sigla2 ;
      private String AV74Sistema_Tipo2 ;
      private String AV77Sistema_Tecnica2 ;
      private String AV81Sistema_ProjetoNome2 ;
      private String AV88TFSistema_Sigla ;
      private String AV89TFSistema_Sigla_Sel ;
      private String AV120TFSistema_ProjetoNome ;
      private String AV121TFSistema_ProjetoNome_Sel ;
      private String AV140TFSistemaVersao_Id ;
      private String AV141TFSistemaVersao_Id_Sel ;
      private String AV183Pgmname ;
      private String A707Sistema_TipoSigla ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A699Sistema_Tipo ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Innewwindow1_Width ;
      private String Innewwindow1_Height ;
      private String Innewwindow1_Target ;
      private String Ddo_sistema_sigla_Caption ;
      private String Ddo_sistema_sigla_Tooltip ;
      private String Ddo_sistema_sigla_Cls ;
      private String Ddo_sistema_sigla_Filteredtext_set ;
      private String Ddo_sistema_sigla_Selectedvalue_set ;
      private String Ddo_sistema_sigla_Dropdownoptionstype ;
      private String Ddo_sistema_sigla_Titlecontrolidtoreplace ;
      private String Ddo_sistema_sigla_Sortedstatus ;
      private String Ddo_sistema_sigla_Filtertype ;
      private String Ddo_sistema_sigla_Datalisttype ;
      private String Ddo_sistema_sigla_Datalistproc ;
      private String Ddo_sistema_sigla_Sortasc ;
      private String Ddo_sistema_sigla_Sortdsc ;
      private String Ddo_sistema_sigla_Loadingdata ;
      private String Ddo_sistema_sigla_Cleanfilter ;
      private String Ddo_sistema_sigla_Noresultsfound ;
      private String Ddo_sistema_sigla_Searchbuttontext ;
      private String Ddo_sistema_tecnica_Caption ;
      private String Ddo_sistema_tecnica_Tooltip ;
      private String Ddo_sistema_tecnica_Cls ;
      private String Ddo_sistema_tecnica_Selectedvalue_set ;
      private String Ddo_sistema_tecnica_Dropdownoptionstype ;
      private String Ddo_sistema_tecnica_Titlecontrolidtoreplace ;
      private String Ddo_sistema_tecnica_Sortedstatus ;
      private String Ddo_sistema_tecnica_Datalisttype ;
      private String Ddo_sistema_tecnica_Datalistfixedvalues ;
      private String Ddo_sistema_tecnica_Sortasc ;
      private String Ddo_sistema_tecnica_Sortdsc ;
      private String Ddo_sistema_tecnica_Cleanfilter ;
      private String Ddo_sistema_tecnica_Searchbuttontext ;
      private String Ddo_sistema_coordenacao_Caption ;
      private String Ddo_sistema_coordenacao_Tooltip ;
      private String Ddo_sistema_coordenacao_Cls ;
      private String Ddo_sistema_coordenacao_Filteredtext_set ;
      private String Ddo_sistema_coordenacao_Selectedvalue_set ;
      private String Ddo_sistema_coordenacao_Dropdownoptionstype ;
      private String Ddo_sistema_coordenacao_Titlecontrolidtoreplace ;
      private String Ddo_sistema_coordenacao_Sortedstatus ;
      private String Ddo_sistema_coordenacao_Filtertype ;
      private String Ddo_sistema_coordenacao_Datalisttype ;
      private String Ddo_sistema_coordenacao_Datalistproc ;
      private String Ddo_sistema_coordenacao_Sortasc ;
      private String Ddo_sistema_coordenacao_Sortdsc ;
      private String Ddo_sistema_coordenacao_Loadingdata ;
      private String Ddo_sistema_coordenacao_Cleanfilter ;
      private String Ddo_sistema_coordenacao_Noresultsfound ;
      private String Ddo_sistema_coordenacao_Searchbuttontext ;
      private String Ddo_ambientetecnologico_descricao_Caption ;
      private String Ddo_ambientetecnologico_descricao_Tooltip ;
      private String Ddo_ambientetecnologico_descricao_Cls ;
      private String Ddo_ambientetecnologico_descricao_Filteredtext_set ;
      private String Ddo_ambientetecnologico_descricao_Selectedvalue_set ;
      private String Ddo_ambientetecnologico_descricao_Dropdownoptionstype ;
      private String Ddo_ambientetecnologico_descricao_Titlecontrolidtoreplace ;
      private String Ddo_ambientetecnologico_descricao_Sortedstatus ;
      private String Ddo_ambientetecnologico_descricao_Filtertype ;
      private String Ddo_ambientetecnologico_descricao_Datalisttype ;
      private String Ddo_ambientetecnologico_descricao_Datalistproc ;
      private String Ddo_ambientetecnologico_descricao_Sortasc ;
      private String Ddo_ambientetecnologico_descricao_Sortdsc ;
      private String Ddo_ambientetecnologico_descricao_Loadingdata ;
      private String Ddo_ambientetecnologico_descricao_Cleanfilter ;
      private String Ddo_ambientetecnologico_descricao_Noresultsfound ;
      private String Ddo_ambientetecnologico_descricao_Searchbuttontext ;
      private String Ddo_metodologia_descricao_Caption ;
      private String Ddo_metodologia_descricao_Tooltip ;
      private String Ddo_metodologia_descricao_Cls ;
      private String Ddo_metodologia_descricao_Filteredtext_set ;
      private String Ddo_metodologia_descricao_Selectedvalue_set ;
      private String Ddo_metodologia_descricao_Dropdownoptionstype ;
      private String Ddo_metodologia_descricao_Titlecontrolidtoreplace ;
      private String Ddo_metodologia_descricao_Sortedstatus ;
      private String Ddo_metodologia_descricao_Filtertype ;
      private String Ddo_metodologia_descricao_Datalisttype ;
      private String Ddo_metodologia_descricao_Datalistproc ;
      private String Ddo_metodologia_descricao_Sortasc ;
      private String Ddo_metodologia_descricao_Sortdsc ;
      private String Ddo_metodologia_descricao_Loadingdata ;
      private String Ddo_metodologia_descricao_Cleanfilter ;
      private String Ddo_metodologia_descricao_Noresultsfound ;
      private String Ddo_metodologia_descricao_Searchbuttontext ;
      private String Ddo_sistema_projetonome_Caption ;
      private String Ddo_sistema_projetonome_Tooltip ;
      private String Ddo_sistema_projetonome_Cls ;
      private String Ddo_sistema_projetonome_Filteredtext_set ;
      private String Ddo_sistema_projetonome_Selectedvalue_set ;
      private String Ddo_sistema_projetonome_Dropdownoptionstype ;
      private String Ddo_sistema_projetonome_Titlecontrolidtoreplace ;
      private String Ddo_sistema_projetonome_Sortedstatus ;
      private String Ddo_sistema_projetonome_Filtertype ;
      private String Ddo_sistema_projetonome_Datalisttype ;
      private String Ddo_sistema_projetonome_Datalistproc ;
      private String Ddo_sistema_projetonome_Sortasc ;
      private String Ddo_sistema_projetonome_Sortdsc ;
      private String Ddo_sistema_projetonome_Loadingdata ;
      private String Ddo_sistema_projetonome_Cleanfilter ;
      private String Ddo_sistema_projetonome_Noresultsfound ;
      private String Ddo_sistema_projetonome_Searchbuttontext ;
      private String Ddo_sistemaversao_id_Caption ;
      private String Ddo_sistemaversao_id_Tooltip ;
      private String Ddo_sistemaversao_id_Cls ;
      private String Ddo_sistemaversao_id_Filteredtext_set ;
      private String Ddo_sistemaversao_id_Selectedvalue_set ;
      private String Ddo_sistemaversao_id_Dropdownoptionstype ;
      private String Ddo_sistemaversao_id_Titlecontrolidtoreplace ;
      private String Ddo_sistemaversao_id_Sortedstatus ;
      private String Ddo_sistemaversao_id_Filtertype ;
      private String Ddo_sistemaversao_id_Datalisttype ;
      private String Ddo_sistemaversao_id_Datalistproc ;
      private String Ddo_sistemaversao_id_Sortasc ;
      private String Ddo_sistemaversao_id_Sortdsc ;
      private String Ddo_sistemaversao_id_Loadingdata ;
      private String Ddo_sistemaversao_id_Cleanfilter ;
      private String Ddo_sistemaversao_id_Noresultsfound ;
      private String Ddo_sistemaversao_id_Searchbuttontext ;
      private String Ddo_sistema_ativo_Caption ;
      private String Ddo_sistema_ativo_Tooltip ;
      private String Ddo_sistema_ativo_Cls ;
      private String Ddo_sistema_ativo_Selectedvalue_set ;
      private String Ddo_sistema_ativo_Dropdownoptionstype ;
      private String Ddo_sistema_ativo_Titlecontrolidtoreplace ;
      private String Ddo_sistema_ativo_Sortedstatus ;
      private String Ddo_sistema_ativo_Datalisttype ;
      private String Ddo_sistema_ativo_Datalistfixedvalues ;
      private String Ddo_sistema_ativo_Sortasc ;
      private String Ddo_sistema_ativo_Sortdsc ;
      private String Ddo_sistema_ativo_Cleanfilter ;
      private String Ddo_sistema_ativo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String edtavTfsistema_sigla_Internalname ;
      private String edtavTfsistema_sigla_Jsonclick ;
      private String edtavTfsistema_sigla_sel_Internalname ;
      private String edtavTfsistema_sigla_sel_Jsonclick ;
      private String edtavTfsistema_coordenacao_Internalname ;
      private String edtavTfsistema_coordenacao_Jsonclick ;
      private String edtavTfsistema_coordenacao_sel_Internalname ;
      private String edtavTfsistema_coordenacao_sel_Jsonclick ;
      private String edtavTfambientetecnologico_descricao_Internalname ;
      private String edtavTfambientetecnologico_descricao_Jsonclick ;
      private String edtavTfambientetecnologico_descricao_sel_Internalname ;
      private String edtavTfambientetecnologico_descricao_sel_Jsonclick ;
      private String edtavTfmetodologia_descricao_Internalname ;
      private String edtavTfmetodologia_descricao_Jsonclick ;
      private String edtavTfmetodologia_descricao_sel_Internalname ;
      private String edtavTfmetodologia_descricao_sel_Jsonclick ;
      private String edtavTfsistema_projetonome_Internalname ;
      private String edtavTfsistema_projetonome_Jsonclick ;
      private String edtavTfsistema_projetonome_sel_Internalname ;
      private String edtavTfsistema_projetonome_sel_Jsonclick ;
      private String edtavTfsistemaversao_id_Internalname ;
      private String edtavTfsistemaversao_id_Jsonclick ;
      private String edtavTfsistemaversao_id_sel_Internalname ;
      private String edtavTfsistemaversao_id_sel_Jsonclick ;
      private String edtavTfsistema_ativo_sel_Internalname ;
      private String edtavTfsistema_ativo_sel_Jsonclick ;
      private String edtavDdo_sistema_siglatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_sistema_tecnicatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_sistema_coordenacaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_ambientetecnologico_descricaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_metodologia_descricaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_sistema_projetonometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_sistemaversao_idtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_sistema_ativotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtavDisplay_Internalname ;
      private String edtSistema_Codigo_Internalname ;
      private String edtSistema_Nome_Internalname ;
      private String A129Sistema_Sigla ;
      private String edtSistema_Sigla_Internalname ;
      private String AV130Sistema_TipoSigla ;
      private String edtavSistema_tiposigla_Internalname ;
      private String cmbSistema_Tecnica_Internalname ;
      private String A700Sistema_Tecnica ;
      private String edtSistema_Coordenacao_Internalname ;
      private String edtAmbienteTecnologico_Descricao_Internalname ;
      private String edtMetodologia_Codigo_Internalname ;
      private String edtMetodologia_Descricao_Internalname ;
      private String edtavPfb_Internalname ;
      private String edtavPfa_Internalname ;
      private String A703Sistema_ProjetoNome ;
      private String edtSistema_ProjetoNome_Internalname ;
      private String A1860SistemaVersao_Id ;
      private String edtSistemaVersao_Id_Internalname ;
      private String chkSistema_Ativo_Internalname ;
      private String edtavAssociarclientes_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV149WWSistemaDS_5_Sistema_sigla1 ;
      private String lV153WWSistemaDS_9_Sistema_projetonome1 ;
      private String lV159WWSistemaDS_15_Sistema_sigla2 ;
      private String lV163WWSistemaDS_19_Sistema_projetonome2 ;
      private String lV165WWSistemaDS_21_Tfsistema_sigla ;
      private String lV174WWSistemaDS_30_Tfsistema_projetonome ;
      private String lV176WWSistemaDS_32_Tfsistemaversao_id ;
      private String AV149WWSistemaDS_5_Sistema_sigla1 ;
      private String AV150WWSistemaDS_6_Sistema_tipo1 ;
      private String AV151WWSistemaDS_7_Sistema_tecnica1 ;
      private String AV153WWSistemaDS_9_Sistema_projetonome1 ;
      private String AV159WWSistemaDS_15_Sistema_sigla2 ;
      private String AV160WWSistemaDS_16_Sistema_tipo2 ;
      private String AV161WWSistemaDS_17_Sistema_tecnica2 ;
      private String AV163WWSistemaDS_19_Sistema_projetonome2 ;
      private String AV166WWSistemaDS_22_Tfsistema_sigla_sel ;
      private String AV165WWSistemaDS_21_Tfsistema_sigla ;
      private String AV175WWSistemaDS_31_Tfsistema_projetonome_sel ;
      private String AV174WWSistemaDS_30_Tfsistema_projetonome ;
      private String AV177WWSistemaDS_33_Tfsistemaversao_id_sel ;
      private String AV176WWSistemaDS_32_Tfsistemaversao_id ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavSistema_areatrabalhocod_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavSistema_nome1_Internalname ;
      private String edtavSistema_sigla1_Internalname ;
      private String cmbavSistema_tipo1_Internalname ;
      private String cmbavSistema_tecnica1_Internalname ;
      private String edtavSistema_coordenacao1_Internalname ;
      private String edtavSistema_projetonome1_Internalname ;
      private String edtavSistema_pf1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavSistema_nome2_Internalname ;
      private String edtavSistema_sigla2_Internalname ;
      private String cmbavSistema_tipo2_Internalname ;
      private String cmbavSistema_tecnica2_Internalname ;
      private String edtavSistema_coordenacao2_Internalname ;
      private String edtavSistema_projetonome2_Internalname ;
      private String edtavSistema_pf2_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_sistema_sigla_Internalname ;
      private String Ddo_sistema_tecnica_Internalname ;
      private String Ddo_sistema_coordenacao_Internalname ;
      private String Ddo_ambientetecnologico_descricao_Internalname ;
      private String Ddo_metodologia_descricao_Internalname ;
      private String Ddo_sistema_projetonome_Internalname ;
      private String Ddo_sistemaversao_id_Internalname ;
      private String Ddo_sistema_ativo_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtSistema_Sigla_Title ;
      private String edtSistema_Coordenacao_Title ;
      private String edtAmbienteTecnologico_Descricao_Title ;
      private String edtMetodologia_Descricao_Title ;
      private String edtSistema_ProjetoNome_Title ;
      private String edtSistemaVersao_Id_Title ;
      private String edtavAssociarclientes_Title ;
      private String imgInsert_Internalname ;
      private String Gridpaginationbar_Internalname ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Link ;
      private String edtavDisplay_Link ;
      private String edtavAssociarclientes_Tooltiptext ;
      private String edtAmbienteTecnologico_Descricao_Link ;
      private String edtMetodologia_Descricao_Link ;
      private String edtSistema_ProjetoNome_Link ;
      private String edtSistemaVersao_Id_Link ;
      private String Innewwindow1_Internalname ;
      private String sStyleString ;
      private String tblUnnamedtable1_Internalname ;
      private String tblUnnamedtable3_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblUnnamedtable2_Internalname ;
      private String lblSistematitle_Internalname ;
      private String lblSistematitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblFiltertextsistema_areatrabalhocod_Internalname ;
      private String lblFiltertextsistema_areatrabalhocod_Jsonclick ;
      private String edtavSistema_areatrabalhocod_Jsonclick ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavSistema_nome2_Jsonclick ;
      private String edtavSistema_sigla2_Jsonclick ;
      private String cmbavSistema_tipo2_Jsonclick ;
      private String cmbavSistema_tecnica2_Jsonclick ;
      private String edtavSistema_coordenacao2_Jsonclick ;
      private String edtavSistema_projetonome2_Jsonclick ;
      private String edtavSistema_pf2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavSistema_nome1_Jsonclick ;
      private String edtavSistema_sigla1_Jsonclick ;
      private String cmbavSistema_tipo1_Jsonclick ;
      private String cmbavSistema_tecnica1_Jsonclick ;
      private String edtavSistema_coordenacao1_Jsonclick ;
      private String edtavSistema_projetonome1_Jsonclick ;
      private String edtavSistema_pf1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Jsonclick ;
      private String imgExportreport_Internalname ;
      private String imgExportreport_Jsonclick ;
      private String sGXsfl_89_fel_idx="0001" ;
      private String ROClassString ;
      private String edtSistema_Codigo_Jsonclick ;
      private String edtSistema_Nome_Jsonclick ;
      private String edtSistema_Sigla_Jsonclick ;
      private String edtavSistema_tiposigla_Jsonclick ;
      private String cmbSistema_Tecnica_Jsonclick ;
      private String edtSistema_Coordenacao_Jsonclick ;
      private String edtAmbienteTecnologico_Descricao_Jsonclick ;
      private String edtMetodologia_Codigo_Jsonclick ;
      private String edtMetodologia_Descricao_Jsonclick ;
      private String edtavPfb_Jsonclick ;
      private String edtavPfa_Jsonclick ;
      private String edtSistema_ProjetoNome_Jsonclick ;
      private String edtSistemaVersao_Id_Jsonclick ;
      private String edtavAssociarclientes_Jsonclick ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV20DynamicFiltersEnabled2 ;
      private bool AV137Paginando ;
      private bool AV33DynamicFiltersIgnoreFirst ;
      private bool AV32DynamicFiltersRemoving ;
      private bool n351AmbienteTecnologico_Codigo ;
      private bool n137Metodologia_Codigo ;
      private bool n691Sistema_ProjetoCod ;
      private bool n1859SistemaVersao_Codigo ;
      private bool A130Sistema_Ativo ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_sistema_sigla_Includesortasc ;
      private bool Ddo_sistema_sigla_Includesortdsc ;
      private bool Ddo_sistema_sigla_Includefilter ;
      private bool Ddo_sistema_sigla_Filterisrange ;
      private bool Ddo_sistema_sigla_Includedatalist ;
      private bool Ddo_sistema_tecnica_Includesortasc ;
      private bool Ddo_sistema_tecnica_Includesortdsc ;
      private bool Ddo_sistema_tecnica_Includefilter ;
      private bool Ddo_sistema_tecnica_Includedatalist ;
      private bool Ddo_sistema_tecnica_Allowmultipleselection ;
      private bool Ddo_sistema_coordenacao_Includesortasc ;
      private bool Ddo_sistema_coordenacao_Includesortdsc ;
      private bool Ddo_sistema_coordenacao_Includefilter ;
      private bool Ddo_sistema_coordenacao_Filterisrange ;
      private bool Ddo_sistema_coordenacao_Includedatalist ;
      private bool Ddo_ambientetecnologico_descricao_Includesortasc ;
      private bool Ddo_ambientetecnologico_descricao_Includesortdsc ;
      private bool Ddo_ambientetecnologico_descricao_Includefilter ;
      private bool Ddo_ambientetecnologico_descricao_Filterisrange ;
      private bool Ddo_ambientetecnologico_descricao_Includedatalist ;
      private bool Ddo_metodologia_descricao_Includesortasc ;
      private bool Ddo_metodologia_descricao_Includesortdsc ;
      private bool Ddo_metodologia_descricao_Includefilter ;
      private bool Ddo_metodologia_descricao_Filterisrange ;
      private bool Ddo_metodologia_descricao_Includedatalist ;
      private bool Ddo_sistema_projetonome_Includesortasc ;
      private bool Ddo_sistema_projetonome_Includesortdsc ;
      private bool Ddo_sistema_projetonome_Includefilter ;
      private bool Ddo_sistema_projetonome_Filterisrange ;
      private bool Ddo_sistema_projetonome_Includedatalist ;
      private bool Ddo_sistemaversao_id_Includesortasc ;
      private bool Ddo_sistemaversao_id_Includesortdsc ;
      private bool Ddo_sistemaversao_id_Includefilter ;
      private bool Ddo_sistemaversao_id_Filterisrange ;
      private bool Ddo_sistemaversao_id_Includedatalist ;
      private bool Ddo_sistema_ativo_Includesortasc ;
      private bool Ddo_sistema_ativo_Includesortdsc ;
      private bool Ddo_sistema_ativo_Includefilter ;
      private bool Ddo_sistema_ativo_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n700Sistema_Tecnica ;
      private bool n513Sistema_Coordenacao ;
      private bool n703Sistema_ProjetoNome ;
      private bool AV155WWSistemaDS_11_Dynamicfiltersenabled2 ;
      private bool n686Sistema_FatorAjuste ;
      private bool n699Sistema_Tipo ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV34Update_IsBlob ;
      private bool AV35Delete_IsBlob ;
      private bool AV58Display_IsBlob ;
      private bool AV84AssociarClientes_IsBlob ;
      private String AV96TFSistema_Tecnica_SelsJson ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV67Sistema_Nome1 ;
      private String AV76Sistema_Coordenacao1 ;
      private String AV21DynamicFiltersSelector2 ;
      private String AV68Sistema_Nome2 ;
      private String AV78Sistema_Coordenacao2 ;
      private String AV100TFSistema_Coordenacao ;
      private String AV101TFSistema_Coordenacao_Sel ;
      private String AV104TFAmbienteTecnologico_Descricao ;
      private String AV105TFAmbienteTecnologico_Descricao_Sel ;
      private String AV108TFMetodologia_Descricao ;
      private String AV109TFMetodologia_Descricao_Sel ;
      private String AV90ddo_Sistema_SiglaTitleControlIdToReplace ;
      private String AV98ddo_Sistema_TecnicaTitleControlIdToReplace ;
      private String AV102ddo_Sistema_CoordenacaoTitleControlIdToReplace ;
      private String AV106ddo_AmbienteTecnologico_DescricaoTitleControlIdToReplace ;
      private String AV110ddo_Metodologia_DescricaoTitleControlIdToReplace ;
      private String AV122ddo_Sistema_ProjetoNomeTitleControlIdToReplace ;
      private String AV142ddo_SistemaVersao_IdTitleControlIdToReplace ;
      private String AV125ddo_Sistema_AtivoTitleControlIdToReplace ;
      private String AV179Update_GXI ;
      private String AV180Delete_GXI ;
      private String AV181Display_GXI ;
      private String A416Sistema_Nome ;
      private String A513Sistema_Coordenacao ;
      private String A352AmbienteTecnologico_Descricao ;
      private String A138Metodologia_Descricao ;
      private String AV182Associarclientes_GXI ;
      private String lV148WWSistemaDS_4_Sistema_nome1 ;
      private String lV152WWSistemaDS_8_Sistema_coordenacao1 ;
      private String lV158WWSistemaDS_14_Sistema_nome2 ;
      private String lV162WWSistemaDS_18_Sistema_coordenacao2 ;
      private String lV168WWSistemaDS_24_Tfsistema_coordenacao ;
      private String lV170WWSistemaDS_26_Tfambientetecnologico_descricao ;
      private String lV172WWSistemaDS_28_Tfmetodologia_descricao ;
      private String AV146WWSistemaDS_2_Dynamicfiltersselector1 ;
      private String AV148WWSistemaDS_4_Sistema_nome1 ;
      private String AV152WWSistemaDS_8_Sistema_coordenacao1 ;
      private String AV156WWSistemaDS_12_Dynamicfiltersselector2 ;
      private String AV158WWSistemaDS_14_Sistema_nome2 ;
      private String AV162WWSistemaDS_18_Sistema_coordenacao2 ;
      private String AV169WWSistemaDS_25_Tfsistema_coordenacao_sel ;
      private String AV168WWSistemaDS_24_Tfsistema_coordenacao ;
      private String AV171WWSistemaDS_27_Tfambientetecnologico_descricao_sel ;
      private String AV170WWSistemaDS_26_Tfambientetecnologico_descricao ;
      private String AV173WWSistemaDS_29_Tfmetodologia_descricao_sel ;
      private String AV172WWSistemaDS_28_Tfmetodologia_descricao ;
      private String AV34Update ;
      private String AV35Delete ;
      private String AV58Display ;
      private String AV84AssociarClientes ;
      private IGxSession AV36Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavSistema_tipo1 ;
      private GXCombobox cmbavSistema_tecnica1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavSistema_tipo2 ;
      private GXCombobox cmbavSistema_tecnica2 ;
      private GXCombobox cmbSistema_Tecnica ;
      private GXCheckbox chkSistema_Ativo ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private IDataStoreProvider pr_default ;
      private int[] H003G2_A135Sistema_AreaTrabalhoCod ;
      private int[] H003G2_A351AmbienteTecnologico_Codigo ;
      private bool[] H003G2_n351AmbienteTecnologico_Codigo ;
      private int[] H003G2_A691Sistema_ProjetoCod ;
      private bool[] H003G2_n691Sistema_ProjetoCod ;
      private int[] H003G2_A1859SistemaVersao_Codigo ;
      private bool[] H003G2_n1859SistemaVersao_Codigo ;
      private bool[] H003G2_A130Sistema_Ativo ;
      private String[] H003G2_A1860SistemaVersao_Id ;
      private String[] H003G2_A703Sistema_ProjetoNome ;
      private bool[] H003G2_n703Sistema_ProjetoNome ;
      private String[] H003G2_A138Metodologia_Descricao ;
      private int[] H003G2_A137Metodologia_Codigo ;
      private bool[] H003G2_n137Metodologia_Codigo ;
      private String[] H003G2_A352AmbienteTecnologico_Descricao ;
      private String[] H003G2_A513Sistema_Coordenacao ;
      private bool[] H003G2_n513Sistema_Coordenacao ;
      private String[] H003G2_A700Sistema_Tecnica ;
      private bool[] H003G2_n700Sistema_Tecnica ;
      private String[] H003G2_A129Sistema_Sigla ;
      private String[] H003G2_A416Sistema_Nome ;
      private decimal[] H003G2_A686Sistema_FatorAjuste ;
      private bool[] H003G2_n686Sistema_FatorAjuste ;
      private int[] H003G2_A127Sistema_Codigo ;
      private String[] H003G2_A699Sistema_Tipo ;
      private bool[] H003G2_n699Sistema_Tipo ;
      private int[] H003G3_A137Metodologia_Codigo ;
      private bool[] H003G3_n137Metodologia_Codigo ;
      private int[] H003G3_A351AmbienteTecnologico_Codigo ;
      private bool[] H003G3_n351AmbienteTecnologico_Codigo ;
      private int[] H003G3_A1859SistemaVersao_Codigo ;
      private bool[] H003G3_n1859SistemaVersao_Codigo ;
      private int[] H003G3_A691Sistema_ProjetoCod ;
      private bool[] H003G3_n691Sistema_ProjetoCod ;
      private bool[] H003G3_A130Sistema_Ativo ;
      private String[] H003G3_A1860SistemaVersao_Id ;
      private String[] H003G3_A138Metodologia_Descricao ;
      private String[] H003G3_A352AmbienteTecnologico_Descricao ;
      private String[] H003G3_A703Sistema_ProjetoNome ;
      private bool[] H003G3_n703Sistema_ProjetoNome ;
      private String[] H003G3_A513Sistema_Coordenacao ;
      private bool[] H003G3_n513Sistema_Coordenacao ;
      private String[] H003G3_A700Sistema_Tecnica ;
      private bool[] H003G3_n700Sistema_Tecnica ;
      private String[] H003G3_A699Sistema_Tipo ;
      private bool[] H003G3_n699Sistema_Tipo ;
      private String[] H003G3_A129Sistema_Sigla ;
      private String[] H003G3_A416Sistema_Nome ;
      private int[] H003G3_A135Sistema_AreaTrabalhoCod ;
      private int[] H003G3_A127Sistema_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV97TFSistema_Tecnica_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV167WWSistemaDS_23_Tfsistema_tecnica_sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV87Sistema_SiglaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV95Sistema_TecnicaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV99Sistema_CoordenacaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV103AmbienteTecnologico_DescricaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV107Metodologia_DescricaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV119Sistema_ProjetoNomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV139SistemaVersao_IdTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV123Sistema_AtivoTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private SdtNotificationInfo AV61NotificationInfo ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV126DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons2 ;
   }

   public class wwsistema__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H003G2( IGxContext context ,
                                             String A700Sistema_Tecnica ,
                                             IGxCollection AV167WWSistemaDS_23_Tfsistema_tecnica_sels ,
                                             String AV146WWSistemaDS_2_Dynamicfiltersselector1 ,
                                             String AV148WWSistemaDS_4_Sistema_nome1 ,
                                             String AV149WWSistemaDS_5_Sistema_sigla1 ,
                                             String AV150WWSistemaDS_6_Sistema_tipo1 ,
                                             String AV151WWSistemaDS_7_Sistema_tecnica1 ,
                                             String AV152WWSistemaDS_8_Sistema_coordenacao1 ,
                                             String AV153WWSistemaDS_9_Sistema_projetonome1 ,
                                             bool AV155WWSistemaDS_11_Dynamicfiltersenabled2 ,
                                             String AV156WWSistemaDS_12_Dynamicfiltersselector2 ,
                                             String AV158WWSistemaDS_14_Sistema_nome2 ,
                                             String AV159WWSistemaDS_15_Sistema_sigla2 ,
                                             String AV160WWSistemaDS_16_Sistema_tipo2 ,
                                             String AV161WWSistemaDS_17_Sistema_tecnica2 ,
                                             String AV162WWSistemaDS_18_Sistema_coordenacao2 ,
                                             String AV163WWSistemaDS_19_Sistema_projetonome2 ,
                                             String AV166WWSistemaDS_22_Tfsistema_sigla_sel ,
                                             String AV165WWSistemaDS_21_Tfsistema_sigla ,
                                             int AV167WWSistemaDS_23_Tfsistema_tecnica_sels_Count ,
                                             String AV169WWSistemaDS_25_Tfsistema_coordenacao_sel ,
                                             String AV168WWSistemaDS_24_Tfsistema_coordenacao ,
                                             String AV171WWSistemaDS_27_Tfambientetecnologico_descricao_sel ,
                                             String AV170WWSistemaDS_26_Tfambientetecnologico_descricao ,
                                             String AV173WWSistemaDS_29_Tfmetodologia_descricao_sel ,
                                             String AV172WWSistemaDS_28_Tfmetodologia_descricao ,
                                             String AV175WWSistemaDS_31_Tfsistema_projetonome_sel ,
                                             String AV174WWSistemaDS_30_Tfsistema_projetonome ,
                                             String AV177WWSistemaDS_33_Tfsistemaversao_id_sel ,
                                             String AV176WWSistemaDS_32_Tfsistemaversao_id ,
                                             short AV178WWSistemaDS_34_Tfsistema_ativo_sel ,
                                             String A416Sistema_Nome ,
                                             String A129Sistema_Sigla ,
                                             String A699Sistema_Tipo ,
                                             String A513Sistema_Coordenacao ,
                                             String A703Sistema_ProjetoNome ,
                                             String A352AmbienteTecnologico_Descricao ,
                                             String A138Metodologia_Descricao ,
                                             String A1860SistemaVersao_Id ,
                                             bool A130Sistema_Ativo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             short AV147WWSistemaDS_3_Dynamicfiltersoperator1 ,
                                             decimal AV154WWSistemaDS_10_Sistema_pf1 ,
                                             decimal A395Sistema_PF ,
                                             short AV157WWSistemaDS_13_Dynamicfiltersoperator2 ,
                                             decimal AV164WWSistemaDS_20_Sistema_pf2 ,
                                             int A135Sistema_AreaTrabalhoCod ,
                                             int AV6WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [25] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT T1.[Sistema_AreaTrabalhoCod], T1.[AmbienteTecnologico_Codigo], T1.[Sistema_ProjetoCod] AS Sistema_ProjetoCod, T1.[SistemaVersao_Codigo], T1.[Sistema_Ativo], T4.[SistemaVersao_Id], T3.[Projeto_Nome] AS Sistema_ProjetoNome, T5.[Metodologia_Descricao], T1.[Metodologia_Codigo], T2.[AmbienteTecnologico_Descricao], T1.[Sistema_Coordenacao], T1.[Sistema_Tecnica], T1.[Sistema_Sigla], T1.[Sistema_Nome], T1.[Sistema_FatorAjuste], T1.[Sistema_Codigo], T1.[Sistema_Tipo] FROM (((([Sistema] T1 WITH (NOLOCK) LEFT JOIN [AmbienteTecnologico] T2 WITH (NOLOCK) ON T2.[AmbienteTecnologico_Codigo] = T1.[AmbienteTecnologico_Codigo]) LEFT JOIN [Projeto] T3 WITH (NOLOCK) ON T3.[Projeto_Codigo] = T1.[Sistema_ProjetoCod]) LEFT JOIN [SistemaVersao] T4 WITH (NOLOCK) ON T4.[SistemaVersao_Codigo] = T1.[SistemaVersao_Codigo]) LEFT JOIN [Metodologia] T5 WITH (NOLOCK) ON T5.[Metodologia_Codigo] = T1.[Metodologia_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[Sistema_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV146WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV148WWSistemaDS_4_Sistema_nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Nome] like '%' + @lV148WWSistemaDS_4_Sistema_nome1)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV146WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV149WWSistemaDS_5_Sistema_sigla1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] like '%' + @lV149WWSistemaDS_5_Sistema_sigla1)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV146WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV150WWSistemaDS_6_Sistema_tipo1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Tipo] = @AV150WWSistemaDS_6_Sistema_tipo1)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV146WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_TECNICA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV151WWSistemaDS_7_Sistema_tecnica1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Tecnica] = @AV151WWSistemaDS_7_Sistema_tecnica1)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV146WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_COORDENACAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV152WWSistemaDS_8_Sistema_coordenacao1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Coordenacao] like '%' + @lV152WWSistemaDS_8_Sistema_coordenacao1 + '%')";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV146WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_PROJETONOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV153WWSistemaDS_9_Sistema_projetonome1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Projeto_Nome] like '%' + @lV153WWSistemaDS_9_Sistema_projetonome1 + '%')";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV155WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV156WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV158WWSistemaDS_14_Sistema_nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Nome] like '%' + @lV158WWSistemaDS_14_Sistema_nome2)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( AV155WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV156WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV159WWSistemaDS_15_Sistema_sigla2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] like '%' + @lV159WWSistemaDS_15_Sistema_sigla2)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( AV155WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV156WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV160WWSistemaDS_16_Sistema_tipo2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Tipo] = @AV160WWSistemaDS_16_Sistema_tipo2)";
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( AV155WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV156WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_TECNICA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV161WWSistemaDS_17_Sistema_tecnica2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Tecnica] = @AV161WWSistemaDS_17_Sistema_tecnica2)";
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( AV155WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV156WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_COORDENACAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV162WWSistemaDS_18_Sistema_coordenacao2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Coordenacao] like '%' + @lV162WWSistemaDS_18_Sistema_coordenacao2 + '%')";
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( AV155WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV156WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_PROJETONOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV163WWSistemaDS_19_Sistema_projetonome2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Projeto_Nome] like '%' + @lV163WWSistemaDS_19_Sistema_projetonome2 + '%')";
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV166WWSistemaDS_22_Tfsistema_sigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV165WWSistemaDS_21_Tfsistema_sigla)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] like @lV165WWSistemaDS_21_Tfsistema_sigla)";
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV166WWSistemaDS_22_Tfsistema_sigla_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] = @AV166WWSistemaDS_22_Tfsistema_sigla_sel)";
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( AV167WWSistemaDS_23_Tfsistema_tecnica_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV167WWSistemaDS_23_Tfsistema_tecnica_sels, "T1.[Sistema_Tecnica] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV169WWSistemaDS_25_Tfsistema_coordenacao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV168WWSistemaDS_24_Tfsistema_coordenacao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Coordenacao] like @lV168WWSistemaDS_24_Tfsistema_coordenacao)";
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV169WWSistemaDS_25_Tfsistema_coordenacao_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Coordenacao] = @AV169WWSistemaDS_25_Tfsistema_coordenacao_sel)";
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV171WWSistemaDS_27_Tfambientetecnologico_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV170WWSistemaDS_26_Tfambientetecnologico_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T2.[AmbienteTecnologico_Descricao] like @lV170WWSistemaDS_26_Tfambientetecnologico_descricao)";
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV171WWSistemaDS_27_Tfambientetecnologico_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[AmbienteTecnologico_Descricao] = @AV171WWSistemaDS_27_Tfambientetecnologico_descricao_sel)";
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV173WWSistemaDS_29_Tfmetodologia_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV172WWSistemaDS_28_Tfmetodologia_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Metodologia_Descricao] like @lV172WWSistemaDS_28_Tfmetodologia_descricao)";
         }
         else
         {
            GXv_int4[19] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV173WWSistemaDS_29_Tfmetodologia_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T5.[Metodologia_Descricao] = @AV173WWSistemaDS_29_Tfmetodologia_descricao_sel)";
         }
         else
         {
            GXv_int4[20] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV175WWSistemaDS_31_Tfsistema_projetonome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV174WWSistemaDS_30_Tfsistema_projetonome)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Projeto_Nome] like @lV174WWSistemaDS_30_Tfsistema_projetonome)";
         }
         else
         {
            GXv_int4[21] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV175WWSistemaDS_31_Tfsistema_projetonome_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Projeto_Nome] = @AV175WWSistemaDS_31_Tfsistema_projetonome_sel)";
         }
         else
         {
            GXv_int4[22] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV177WWSistemaDS_33_Tfsistemaversao_id_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV176WWSistemaDS_32_Tfsistemaversao_id)) ) )
         {
            sWhereString = sWhereString + " and (T4.[SistemaVersao_Id] like @lV176WWSistemaDS_32_Tfsistemaversao_id)";
         }
         else
         {
            GXv_int4[23] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV177WWSistemaDS_33_Tfsistemaversao_id_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[SistemaVersao_Id] = @AV177WWSistemaDS_33_Tfsistemaversao_id_sel)";
         }
         else
         {
            GXv_int4[24] = 1;
         }
         if ( AV178WWSistemaDS_34_Tfsistema_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Ativo] = 1)";
         }
         if ( AV178WWSistemaDS_34_Tfsistema_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( AV13OrderedBy == 1 )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Sistema_Nome]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Sistema_Sigla]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Sistema_Sigla] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Sistema_Tecnica]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Sistema_Tecnica] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Sistema_Coordenacao]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Sistema_Coordenacao] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T2.[AmbienteTecnologico_Descricao]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T2.[AmbienteTecnologico_Descricao] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T5.[Metodologia_Descricao]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T5.[Metodologia_Descricao] DESC";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T3.[Projeto_Nome]";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T3.[Projeto_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T4.[SistemaVersao_Id]";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T4.[SistemaVersao_Id] DESC";
         }
         else if ( ( AV13OrderedBy == 9 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Sistema_Ativo]";
         }
         else if ( ( AV13OrderedBy == 9 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[Sistema_Ativo] DESC";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      protected Object[] conditional_H003G3( IGxContext context ,
                                             String A700Sistema_Tecnica ,
                                             IGxCollection AV167WWSistemaDS_23_Tfsistema_tecnica_sels ,
                                             String AV146WWSistemaDS_2_Dynamicfiltersselector1 ,
                                             String AV148WWSistemaDS_4_Sistema_nome1 ,
                                             String AV149WWSistemaDS_5_Sistema_sigla1 ,
                                             String AV150WWSistemaDS_6_Sistema_tipo1 ,
                                             String AV151WWSistemaDS_7_Sistema_tecnica1 ,
                                             String AV152WWSistemaDS_8_Sistema_coordenacao1 ,
                                             String AV153WWSistemaDS_9_Sistema_projetonome1 ,
                                             bool AV155WWSistemaDS_11_Dynamicfiltersenabled2 ,
                                             String AV156WWSistemaDS_12_Dynamicfiltersselector2 ,
                                             String AV158WWSistemaDS_14_Sistema_nome2 ,
                                             String AV159WWSistemaDS_15_Sistema_sigla2 ,
                                             String AV160WWSistemaDS_16_Sistema_tipo2 ,
                                             String AV161WWSistemaDS_17_Sistema_tecnica2 ,
                                             String AV162WWSistemaDS_18_Sistema_coordenacao2 ,
                                             String AV163WWSistemaDS_19_Sistema_projetonome2 ,
                                             String AV166WWSistemaDS_22_Tfsistema_sigla_sel ,
                                             String AV165WWSistemaDS_21_Tfsistema_sigla ,
                                             int AV167WWSistemaDS_23_Tfsistema_tecnica_sels_Count ,
                                             String AV169WWSistemaDS_25_Tfsistema_coordenacao_sel ,
                                             String AV168WWSistemaDS_24_Tfsistema_coordenacao ,
                                             String AV171WWSistemaDS_27_Tfambientetecnologico_descricao_sel ,
                                             String AV170WWSistemaDS_26_Tfambientetecnologico_descricao ,
                                             String AV173WWSistemaDS_29_Tfmetodologia_descricao_sel ,
                                             String AV172WWSistemaDS_28_Tfmetodologia_descricao ,
                                             String AV175WWSistemaDS_31_Tfsistema_projetonome_sel ,
                                             String AV174WWSistemaDS_30_Tfsistema_projetonome ,
                                             String AV177WWSistemaDS_33_Tfsistemaversao_id_sel ,
                                             String AV176WWSistemaDS_32_Tfsistemaversao_id ,
                                             short AV178WWSistemaDS_34_Tfsistema_ativo_sel ,
                                             String A416Sistema_Nome ,
                                             String A129Sistema_Sigla ,
                                             String A699Sistema_Tipo ,
                                             String A513Sistema_Coordenacao ,
                                             String A703Sistema_ProjetoNome ,
                                             String A352AmbienteTecnologico_Descricao ,
                                             String A138Metodologia_Descricao ,
                                             String A1860SistemaVersao_Id ,
                                             bool A130Sistema_Ativo ,
                                             short AV147WWSistemaDS_3_Dynamicfiltersoperator1 ,
                                             decimal AV154WWSistemaDS_10_Sistema_pf1 ,
                                             decimal A395Sistema_PF ,
                                             short AV157WWSistemaDS_13_Dynamicfiltersoperator2 ,
                                             decimal AV164WWSistemaDS_20_Sistema_pf2 ,
                                             int AV6WWPContext_gxTpr_Areatrabalho_codigo ,
                                             int A135Sistema_AreaTrabalhoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int6 ;
         GXv_int6 = new short [25] ;
         Object[] GXv_Object7 ;
         GXv_Object7 = new Object [2] ;
         scmdbuf = "SELECT T1.[Metodologia_Codigo], T1.[AmbienteTecnologico_Codigo], T1.[SistemaVersao_Codigo], T1.[Sistema_ProjetoCod] AS Sistema_ProjetoCod, T1.[Sistema_Ativo], T4.[SistemaVersao_Id], T2.[Metodologia_Descricao], T3.[AmbienteTecnologico_Descricao], T5.[Projeto_Nome] AS Sistema_ProjetoNome, T1.[Sistema_Coordenacao], T1.[Sistema_Tecnica], T1.[Sistema_Tipo], T1.[Sistema_Sigla], T1.[Sistema_Nome], T1.[Sistema_AreaTrabalhoCod], T1.[Sistema_Codigo] FROM (((([Sistema] T1 WITH (NOLOCK) LEFT JOIN [Metodologia] T2 WITH (NOLOCK) ON T2.[Metodologia_Codigo] = T1.[Metodologia_Codigo]) LEFT JOIN [AmbienteTecnologico] T3 WITH (NOLOCK) ON T3.[AmbienteTecnologico_Codigo] = T1.[AmbienteTecnologico_Codigo]) LEFT JOIN [SistemaVersao] T4 WITH (NOLOCK) ON T4.[SistemaVersao_Codigo] = T1.[SistemaVersao_Codigo]) LEFT JOIN [Projeto] T5 WITH (NOLOCK) ON T5.[Projeto_Codigo] = T1.[Sistema_ProjetoCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[Sistema_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV146WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV148WWSistemaDS_4_Sistema_nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Nome] like '%' + @lV148WWSistemaDS_4_Sistema_nome1)";
         }
         else
         {
            GXv_int6[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV146WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV149WWSistemaDS_5_Sistema_sigla1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] like '%' + @lV149WWSistemaDS_5_Sistema_sigla1)";
         }
         else
         {
            GXv_int6[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV146WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV150WWSistemaDS_6_Sistema_tipo1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Tipo] = @AV150WWSistemaDS_6_Sistema_tipo1)";
         }
         else
         {
            GXv_int6[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV146WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_TECNICA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV151WWSistemaDS_7_Sistema_tecnica1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Tecnica] = @AV151WWSistemaDS_7_Sistema_tecnica1)";
         }
         else
         {
            GXv_int6[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV146WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_COORDENACAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV152WWSistemaDS_8_Sistema_coordenacao1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Coordenacao] like '%' + @lV152WWSistemaDS_8_Sistema_coordenacao1 + '%')";
         }
         else
         {
            GXv_int6[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV146WWSistemaDS_2_Dynamicfiltersselector1, "SISTEMA_PROJETONOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV153WWSistemaDS_9_Sistema_projetonome1)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Projeto_Nome] like '%' + @lV153WWSistemaDS_9_Sistema_projetonome1 + '%')";
         }
         else
         {
            GXv_int6[6] = 1;
         }
         if ( AV155WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV156WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV158WWSistemaDS_14_Sistema_nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Nome] like '%' + @lV158WWSistemaDS_14_Sistema_nome2)";
         }
         else
         {
            GXv_int6[7] = 1;
         }
         if ( AV155WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV156WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV159WWSistemaDS_15_Sistema_sigla2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] like '%' + @lV159WWSistemaDS_15_Sistema_sigla2)";
         }
         else
         {
            GXv_int6[8] = 1;
         }
         if ( AV155WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV156WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV160WWSistemaDS_16_Sistema_tipo2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Tipo] = @AV160WWSistemaDS_16_Sistema_tipo2)";
         }
         else
         {
            GXv_int6[9] = 1;
         }
         if ( AV155WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV156WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_TECNICA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV161WWSistemaDS_17_Sistema_tecnica2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Tecnica] = @AV161WWSistemaDS_17_Sistema_tecnica2)";
         }
         else
         {
            GXv_int6[10] = 1;
         }
         if ( AV155WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV156WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_COORDENACAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV162WWSistemaDS_18_Sistema_coordenacao2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Coordenacao] like '%' + @lV162WWSistemaDS_18_Sistema_coordenacao2 + '%')";
         }
         else
         {
            GXv_int6[11] = 1;
         }
         if ( AV155WWSistemaDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV156WWSistemaDS_12_Dynamicfiltersselector2, "SISTEMA_PROJETONOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV163WWSistemaDS_19_Sistema_projetonome2)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Projeto_Nome] like '%' + @lV163WWSistemaDS_19_Sistema_projetonome2 + '%')";
         }
         else
         {
            GXv_int6[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV166WWSistemaDS_22_Tfsistema_sigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV165WWSistemaDS_21_Tfsistema_sigla)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] like @lV165WWSistemaDS_21_Tfsistema_sigla)";
         }
         else
         {
            GXv_int6[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV166WWSistemaDS_22_Tfsistema_sigla_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Sigla] = @AV166WWSistemaDS_22_Tfsistema_sigla_sel)";
         }
         else
         {
            GXv_int6[14] = 1;
         }
         if ( AV167WWSistemaDS_23_Tfsistema_tecnica_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV167WWSistemaDS_23_Tfsistema_tecnica_sels, "T1.[Sistema_Tecnica] IN (", ")") + ")";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV169WWSistemaDS_25_Tfsistema_coordenacao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV168WWSistemaDS_24_Tfsistema_coordenacao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Coordenacao] like @lV168WWSistemaDS_24_Tfsistema_coordenacao)";
         }
         else
         {
            GXv_int6[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV169WWSistemaDS_25_Tfsistema_coordenacao_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Coordenacao] = @AV169WWSistemaDS_25_Tfsistema_coordenacao_sel)";
         }
         else
         {
            GXv_int6[16] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV171WWSistemaDS_27_Tfambientetecnologico_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV170WWSistemaDS_26_Tfambientetecnologico_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AmbienteTecnologico_Descricao] like @lV170WWSistemaDS_26_Tfambientetecnologico_descricao)";
         }
         else
         {
            GXv_int6[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV171WWSistemaDS_27_Tfambientetecnologico_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[AmbienteTecnologico_Descricao] = @AV171WWSistemaDS_27_Tfambientetecnologico_descricao_sel)";
         }
         else
         {
            GXv_int6[18] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV173WWSistemaDS_29_Tfmetodologia_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV172WWSistemaDS_28_Tfmetodologia_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] like @lV172WWSistemaDS_28_Tfmetodologia_descricao)";
         }
         else
         {
            GXv_int6[19] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV173WWSistemaDS_29_Tfmetodologia_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Metodologia_Descricao] = @AV173WWSistemaDS_29_Tfmetodologia_descricao_sel)";
         }
         else
         {
            GXv_int6[20] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV175WWSistemaDS_31_Tfsistema_projetonome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV174WWSistemaDS_30_Tfsistema_projetonome)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Projeto_Nome] like @lV174WWSistemaDS_30_Tfsistema_projetonome)";
         }
         else
         {
            GXv_int6[21] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV175WWSistemaDS_31_Tfsistema_projetonome_sel)) )
         {
            sWhereString = sWhereString + " and (T5.[Projeto_Nome] = @AV175WWSistemaDS_31_Tfsistema_projetonome_sel)";
         }
         else
         {
            GXv_int6[22] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV177WWSistemaDS_33_Tfsistemaversao_id_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV176WWSistemaDS_32_Tfsistemaversao_id)) ) )
         {
            sWhereString = sWhereString + " and (T4.[SistemaVersao_Id] like @lV176WWSistemaDS_32_Tfsistemaversao_id)";
         }
         else
         {
            GXv_int6[23] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV177WWSistemaDS_33_Tfsistemaversao_id_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[SistemaVersao_Id] = @AV177WWSistemaDS_33_Tfsistemaversao_id_sel)";
         }
         else
         {
            GXv_int6[24] = 1;
         }
         if ( AV178WWSistemaDS_34_Tfsistema_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Ativo] = 1)";
         }
         if ( AV178WWSistemaDS_34_Tfsistema_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[Sistema_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Sistema_AreaTrabalhoCod]";
         GXv_Object7[0] = scmdbuf;
         GXv_Object7[1] = GXv_int6;
         return GXv_Object7 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H003G2(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (short)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (String)dynConstraints[38] , (bool)dynConstraints[39] , (short)dynConstraints[40] , (bool)dynConstraints[41] , (short)dynConstraints[42] , (decimal)dynConstraints[43] , (decimal)dynConstraints[44] , (short)dynConstraints[45] , (decimal)dynConstraints[46] , (int)dynConstraints[47] , (int)dynConstraints[48] );
               case 1 :
                     return conditional_H003G3(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (short)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (String)dynConstraints[38] , (bool)dynConstraints[39] , (short)dynConstraints[40] , (decimal)dynConstraints[41] , (decimal)dynConstraints[42] , (short)dynConstraints[43] , (decimal)dynConstraints[44] , (int)dynConstraints[45] , (int)dynConstraints[46] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH003G2 ;
          prmH003G2 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV148WWSistemaDS_4_Sistema_nome1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV149WWSistemaDS_5_Sistema_sigla1",SqlDbType.Char,25,0} ,
          new Object[] {"@AV150WWSistemaDS_6_Sistema_tipo1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV151WWSistemaDS_7_Sistema_tecnica1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV152WWSistemaDS_8_Sistema_coordenacao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV153WWSistemaDS_9_Sistema_projetonome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV158WWSistemaDS_14_Sistema_nome2",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV159WWSistemaDS_15_Sistema_sigla2",SqlDbType.Char,25,0} ,
          new Object[] {"@AV160WWSistemaDS_16_Sistema_tipo2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV161WWSistemaDS_17_Sistema_tecnica2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV162WWSistemaDS_18_Sistema_coordenacao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV163WWSistemaDS_19_Sistema_projetonome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV165WWSistemaDS_21_Tfsistema_sigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV166WWSistemaDS_22_Tfsistema_sigla_sel",SqlDbType.Char,25,0} ,
          new Object[] {"@lV168WWSistemaDS_24_Tfsistema_coordenacao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV169WWSistemaDS_25_Tfsistema_coordenacao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV170WWSistemaDS_26_Tfambientetecnologico_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV171WWSistemaDS_27_Tfambientetecnologico_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV172WWSistemaDS_28_Tfmetodologia_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV173WWSistemaDS_29_Tfmetodologia_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV174WWSistemaDS_30_Tfsistema_projetonome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV175WWSistemaDS_31_Tfsistema_projetonome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV176WWSistemaDS_32_Tfsistemaversao_id",SqlDbType.Char,20,0} ,
          new Object[] {"@AV177WWSistemaDS_33_Tfsistemaversao_id_sel",SqlDbType.Char,20,0}
          } ;
          Object[] prmH003G3 ;
          prmH003G3 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV148WWSistemaDS_4_Sistema_nome1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV149WWSistemaDS_5_Sistema_sigla1",SqlDbType.Char,25,0} ,
          new Object[] {"@AV150WWSistemaDS_6_Sistema_tipo1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV151WWSistemaDS_7_Sistema_tecnica1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV152WWSistemaDS_8_Sistema_coordenacao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV153WWSistemaDS_9_Sistema_projetonome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV158WWSistemaDS_14_Sistema_nome2",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV159WWSistemaDS_15_Sistema_sigla2",SqlDbType.Char,25,0} ,
          new Object[] {"@AV160WWSistemaDS_16_Sistema_tipo2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV161WWSistemaDS_17_Sistema_tecnica2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV162WWSistemaDS_18_Sistema_coordenacao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV163WWSistemaDS_19_Sistema_projetonome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV165WWSistemaDS_21_Tfsistema_sigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV166WWSistemaDS_22_Tfsistema_sigla_sel",SqlDbType.Char,25,0} ,
          new Object[] {"@lV168WWSistemaDS_24_Tfsistema_coordenacao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV169WWSistemaDS_25_Tfsistema_coordenacao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV170WWSistemaDS_26_Tfambientetecnologico_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV171WWSistemaDS_27_Tfambientetecnologico_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV172WWSistemaDS_28_Tfmetodologia_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV173WWSistemaDS_29_Tfmetodologia_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV174WWSistemaDS_30_Tfsistema_projetonome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV175WWSistemaDS_31_Tfsistema_projetonome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV176WWSistemaDS_32_Tfsistemaversao_id",SqlDbType.Char,20,0} ,
          new Object[] {"@AV177WWSistemaDS_33_Tfsistemaversao_id_sel",SqlDbType.Char,20,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H003G2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH003G2,11,0,true,false )
             ,new CursorDef("H003G3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH003G3,11,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((bool[]) buf[7])[0] = rslt.getBool(5) ;
                ((String[]) buf[8])[0] = rslt.getString(6, 20) ;
                ((String[]) buf[9])[0] = rslt.getString(7, 50) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getVarchar(8) ;
                ((int[]) buf[12])[0] = rslt.getInt(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((String[]) buf[14])[0] = rslt.getVarchar(10) ;
                ((String[]) buf[15])[0] = rslt.getVarchar(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((String[]) buf[17])[0] = rslt.getString(12, 1) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(12);
                ((String[]) buf[19])[0] = rslt.getString(13, 25) ;
                ((String[]) buf[20])[0] = rslt.getVarchar(14) ;
                ((decimal[]) buf[21])[0] = rslt.getDecimal(15) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(15);
                ((int[]) buf[23])[0] = rslt.getInt(16) ;
                ((String[]) buf[24])[0] = rslt.getString(17, 1) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(17);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((bool[]) buf[8])[0] = rslt.getBool(5) ;
                ((String[]) buf[9])[0] = rslt.getString(6, 20) ;
                ((String[]) buf[10])[0] = rslt.getVarchar(7) ;
                ((String[]) buf[11])[0] = rslt.getVarchar(8) ;
                ((String[]) buf[12])[0] = rslt.getString(9, 50) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((String[]) buf[14])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((String[]) buf[16])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((String[]) buf[18])[0] = rslt.getString(12, 1) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                ((String[]) buf[20])[0] = rslt.getString(13, 25) ;
                ((String[]) buf[21])[0] = rslt.getVarchar(14) ;
                ((int[]) buf[22])[0] = rslt.getInt(15) ;
                ((int[]) buf[23])[0] = rslt.getInt(16) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                return;
       }
    }

 }

}
