/*
               File: ServicoResponsavel_BC
        Description: Servico Responsavel
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:29:1.91
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class servicoresponsavel_bc : GXHttpHandler, IGxSilentTrn
   {
      public servicoresponsavel_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public servicoresponsavel_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow3W177( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey3W177( ) ;
         standaloneModal( ) ;
         AddRow3W177( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z1548ServicoResponsavel_Codigo = A1548ServicoResponsavel_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_3W0( )
      {
         BeforeValidate3W177( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls3W177( ) ;
            }
            else
            {
               CheckExtendedTable3W177( ) ;
               if ( AnyError == 0 )
               {
                  ZM3W177( 2) ;
                  ZM3W177( 3) ;
               }
               CloseExtendedTableCursors3W177( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void ZM3W177( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
            Z1549ServicoResponsavel_SrvCod = A1549ServicoResponsavel_SrvCod;
            Z1552ServicoResponsavel_CteCteCod = A1552ServicoResponsavel_CteCteCod;
            Z1550ServicoResponsavel_CteUsrCod = A1550ServicoResponsavel_CteUsrCod;
            Z1643ServicoResponsavel_CteAreaCod = A1643ServicoResponsavel_CteAreaCod;
         }
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
            Z1643ServicoResponsavel_CteAreaCod = A1643ServicoResponsavel_CteAreaCod;
         }
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
            Z1643ServicoResponsavel_CteAreaCod = A1643ServicoResponsavel_CteAreaCod;
         }
         if ( GX_JID == -1 )
         {
            Z1548ServicoResponsavel_Codigo = A1548ServicoResponsavel_Codigo;
            Z1549ServicoResponsavel_SrvCod = A1549ServicoResponsavel_SrvCod;
            Z1552ServicoResponsavel_CteCteCod = A1552ServicoResponsavel_CteCteCod;
            Z1550ServicoResponsavel_CteUsrCod = A1550ServicoResponsavel_CteUsrCod;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
      }

      protected void Load3W177( )
      {
         /* Using cursor BC003W7 */
         pr_default.execute(4, new Object[] {A1548ServicoResponsavel_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound177 = 1;
            A1549ServicoResponsavel_SrvCod = BC003W7_A1549ServicoResponsavel_SrvCod[0];
            n1549ServicoResponsavel_SrvCod = BC003W7_n1549ServicoResponsavel_SrvCod[0];
            A1552ServicoResponsavel_CteCteCod = BC003W7_A1552ServicoResponsavel_CteCteCod[0];
            n1552ServicoResponsavel_CteCteCod = BC003W7_n1552ServicoResponsavel_CteCteCod[0];
            A1550ServicoResponsavel_CteUsrCod = BC003W7_A1550ServicoResponsavel_CteUsrCod[0];
            n1550ServicoResponsavel_CteUsrCod = BC003W7_n1550ServicoResponsavel_CteUsrCod[0];
            ZM3W177( -1) ;
         }
         pr_default.close(4);
         OnLoadActions3W177( ) ;
      }

      protected void OnLoadActions3W177( )
      {
         /* Using cursor BC003W5 */
         pr_default.execute(2, new Object[] {n1552ServicoResponsavel_CteCteCod, A1552ServicoResponsavel_CteCteCod});
         if ( (pr_default.getStatus(2) != 101) )
         {
            A1643ServicoResponsavel_CteAreaCod = BC003W5_A1643ServicoResponsavel_CteAreaCod[0];
            n1643ServicoResponsavel_CteAreaCod = BC003W5_n1643ServicoResponsavel_CteAreaCod[0];
         }
         else
         {
            A1643ServicoResponsavel_CteAreaCod = 0;
            n1643ServicoResponsavel_CteAreaCod = false;
         }
         pr_default.close(2);
      }

      protected void CheckExtendedTable3W177( )
      {
         standaloneModal( ) ;
         /* Using cursor BC003W5 */
         pr_default.execute(2, new Object[] {n1552ServicoResponsavel_CteCteCod, A1552ServicoResponsavel_CteCteCod});
         if ( (pr_default.getStatus(2) != 101) )
         {
            A1643ServicoResponsavel_CteAreaCod = BC003W5_A1643ServicoResponsavel_CteAreaCod[0];
            n1643ServicoResponsavel_CteAreaCod = BC003W5_n1643ServicoResponsavel_CteAreaCod[0];
         }
         else
         {
            A1643ServicoResponsavel_CteAreaCod = 0;
            n1643ServicoResponsavel_CteAreaCod = false;
         }
         pr_default.close(2);
         /* Using cursor BC003W6 */
         pr_default.execute(3, new Object[] {n1552ServicoResponsavel_CteCteCod, A1552ServicoResponsavel_CteCteCod, n1550ServicoResponsavel_CteUsrCod, A1550ServicoResponsavel_CteUsrCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            if ( ! ( (0==A1552ServicoResponsavel_CteCteCod) || (0==A1550ServicoResponsavel_CteUsrCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Servico Responsavel_Contratante Usuario'.", "ForeignKeyNotFound", 1, "SERVICORESPONSAVEL_CTECTECOD");
               AnyError = 1;
            }
         }
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors3W177( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey3W177( )
      {
         /* Using cursor BC003W8 */
         pr_default.execute(5, new Object[] {A1548ServicoResponsavel_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound177 = 1;
         }
         else
         {
            RcdFound177 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC003W3 */
         pr_default.execute(1, new Object[] {A1548ServicoResponsavel_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM3W177( 1) ;
            RcdFound177 = 1;
            A1548ServicoResponsavel_Codigo = BC003W3_A1548ServicoResponsavel_Codigo[0];
            A1549ServicoResponsavel_SrvCod = BC003W3_A1549ServicoResponsavel_SrvCod[0];
            n1549ServicoResponsavel_SrvCod = BC003W3_n1549ServicoResponsavel_SrvCod[0];
            A1552ServicoResponsavel_CteCteCod = BC003W3_A1552ServicoResponsavel_CteCteCod[0];
            n1552ServicoResponsavel_CteCteCod = BC003W3_n1552ServicoResponsavel_CteCteCod[0];
            A1550ServicoResponsavel_CteUsrCod = BC003W3_A1550ServicoResponsavel_CteUsrCod[0];
            n1550ServicoResponsavel_CteUsrCod = BC003W3_n1550ServicoResponsavel_CteUsrCod[0];
            Z1548ServicoResponsavel_Codigo = A1548ServicoResponsavel_Codigo;
            sMode177 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load3W177( ) ;
            if ( AnyError == 1 )
            {
               RcdFound177 = 0;
               InitializeNonKey3W177( ) ;
            }
            Gx_mode = sMode177;
         }
         else
         {
            RcdFound177 = 0;
            InitializeNonKey3W177( ) ;
            sMode177 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode177;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey3W177( ) ;
         if ( RcdFound177 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_3W0( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency3W177( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC003W2 */
            pr_default.execute(0, new Object[] {A1548ServicoResponsavel_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ServicoResponsavel"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z1549ServicoResponsavel_SrvCod != BC003W2_A1549ServicoResponsavel_SrvCod[0] ) || ( Z1552ServicoResponsavel_CteCteCod != BC003W2_A1552ServicoResponsavel_CteCteCod[0] ) || ( Z1550ServicoResponsavel_CteUsrCod != BC003W2_A1550ServicoResponsavel_CteUsrCod[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ServicoResponsavel"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert3W177( )
      {
         BeforeValidate3W177( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3W177( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM3W177( 0) ;
            CheckOptimisticConcurrency3W177( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3W177( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert3W177( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC003W9 */
                     pr_default.execute(6, new Object[] {n1549ServicoResponsavel_SrvCod, A1549ServicoResponsavel_SrvCod, n1552ServicoResponsavel_CteCteCod, A1552ServicoResponsavel_CteCteCod, n1550ServicoResponsavel_CteUsrCod, A1550ServicoResponsavel_CteUsrCod});
                     A1548ServicoResponsavel_Codigo = BC003W9_A1548ServicoResponsavel_Codigo[0];
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("ServicoResponsavel") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load3W177( ) ;
            }
            EndLevel3W177( ) ;
         }
         CloseExtendedTableCursors3W177( ) ;
      }

      protected void Update3W177( )
      {
         BeforeValidate3W177( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3W177( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3W177( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3W177( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate3W177( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC003W10 */
                     pr_default.execute(7, new Object[] {n1549ServicoResponsavel_SrvCod, A1549ServicoResponsavel_SrvCod, n1552ServicoResponsavel_CteCteCod, A1552ServicoResponsavel_CteCteCod, n1550ServicoResponsavel_CteUsrCod, A1550ServicoResponsavel_CteUsrCod, A1548ServicoResponsavel_Codigo});
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("ServicoResponsavel") ;
                     if ( (pr_default.getStatus(7) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ServicoResponsavel"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate3W177( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel3W177( ) ;
         }
         CloseExtendedTableCursors3W177( ) ;
      }

      protected void DeferredUpdate3W177( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate3W177( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3W177( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls3W177( ) ;
            AfterConfirm3W177( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete3W177( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC003W11 */
                  pr_default.execute(8, new Object[] {A1548ServicoResponsavel_Codigo});
                  pr_default.close(8);
                  dsDefault.SmartCacheProvider.SetUpdated("ServicoResponsavel") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode177 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel3W177( ) ;
         Gx_mode = sMode177;
      }

      protected void OnDeleteControls3W177( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC003W13 */
            pr_default.execute(9, new Object[] {n1552ServicoResponsavel_CteCteCod, A1552ServicoResponsavel_CteCteCod});
            if ( (pr_default.getStatus(9) != 101) )
            {
               A1643ServicoResponsavel_CteAreaCod = BC003W13_A1643ServicoResponsavel_CteAreaCod[0];
               n1643ServicoResponsavel_CteAreaCod = BC003W13_n1643ServicoResponsavel_CteAreaCod[0];
            }
            else
            {
               A1643ServicoResponsavel_CteAreaCod = 0;
               n1643ServicoResponsavel_CteAreaCod = false;
            }
            pr_default.close(9);
         }
      }

      protected void EndLevel3W177( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete3W177( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart3W177( )
      {
         /* Using cursor BC003W14 */
         pr_default.execute(10, new Object[] {A1548ServicoResponsavel_Codigo});
         RcdFound177 = 0;
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound177 = 1;
            A1548ServicoResponsavel_Codigo = BC003W14_A1548ServicoResponsavel_Codigo[0];
            A1549ServicoResponsavel_SrvCod = BC003W14_A1549ServicoResponsavel_SrvCod[0];
            n1549ServicoResponsavel_SrvCod = BC003W14_n1549ServicoResponsavel_SrvCod[0];
            A1552ServicoResponsavel_CteCteCod = BC003W14_A1552ServicoResponsavel_CteCteCod[0];
            n1552ServicoResponsavel_CteCteCod = BC003W14_n1552ServicoResponsavel_CteCteCod[0];
            A1550ServicoResponsavel_CteUsrCod = BC003W14_A1550ServicoResponsavel_CteUsrCod[0];
            n1550ServicoResponsavel_CteUsrCod = BC003W14_n1550ServicoResponsavel_CteUsrCod[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext3W177( )
      {
         /* Scan next routine */
         pr_default.readNext(10);
         RcdFound177 = 0;
         ScanKeyLoad3W177( ) ;
      }

      protected void ScanKeyLoad3W177( )
      {
         sMode177 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound177 = 1;
            A1548ServicoResponsavel_Codigo = BC003W14_A1548ServicoResponsavel_Codigo[0];
            A1549ServicoResponsavel_SrvCod = BC003W14_A1549ServicoResponsavel_SrvCod[0];
            n1549ServicoResponsavel_SrvCod = BC003W14_n1549ServicoResponsavel_SrvCod[0];
            A1552ServicoResponsavel_CteCteCod = BC003W14_A1552ServicoResponsavel_CteCteCod[0];
            n1552ServicoResponsavel_CteCteCod = BC003W14_n1552ServicoResponsavel_CteCteCod[0];
            A1550ServicoResponsavel_CteUsrCod = BC003W14_A1550ServicoResponsavel_CteUsrCod[0];
            n1550ServicoResponsavel_CteUsrCod = BC003W14_n1550ServicoResponsavel_CteUsrCod[0];
         }
         Gx_mode = sMode177;
      }

      protected void ScanKeyEnd3W177( )
      {
         pr_default.close(10);
      }

      protected void AfterConfirm3W177( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert3W177( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate3W177( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete3W177( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete3W177( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate3W177( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes3W177( )
      {
      }

      protected void AddRow3W177( )
      {
         VarsToRow177( bcServicoResponsavel) ;
      }

      protected void ReadRow3W177( )
      {
         RowToVars177( bcServicoResponsavel, 1) ;
      }

      protected void InitializeNonKey3W177( )
      {
         A1549ServicoResponsavel_SrvCod = 0;
         n1549ServicoResponsavel_SrvCod = false;
         A1552ServicoResponsavel_CteCteCod = 0;
         n1552ServicoResponsavel_CteCteCod = false;
         A1550ServicoResponsavel_CteUsrCod = 0;
         n1550ServicoResponsavel_CteUsrCod = false;
         A1643ServicoResponsavel_CteAreaCod = 0;
         n1643ServicoResponsavel_CteAreaCod = false;
         Z1549ServicoResponsavel_SrvCod = 0;
         Z1552ServicoResponsavel_CteCteCod = 0;
         Z1550ServicoResponsavel_CteUsrCod = 0;
      }

      protected void InitAll3W177( )
      {
         A1548ServicoResponsavel_Codigo = 0;
         InitializeNonKey3W177( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow177( SdtServicoResponsavel obj177 )
      {
         obj177.gxTpr_Mode = Gx_mode;
         obj177.gxTpr_Servicoresponsavel_srvcod = A1549ServicoResponsavel_SrvCod;
         obj177.gxTpr_Servicoresponsavel_ctectecod = A1552ServicoResponsavel_CteCteCod;
         obj177.gxTpr_Servicoresponsavel_cteusrcod = A1550ServicoResponsavel_CteUsrCod;
         obj177.gxTpr_Servicoresponsavel_cteareacod = A1643ServicoResponsavel_CteAreaCod;
         obj177.gxTpr_Servicoresponsavel_codigo = A1548ServicoResponsavel_Codigo;
         obj177.gxTpr_Servicoresponsavel_codigo_Z = Z1548ServicoResponsavel_Codigo;
         obj177.gxTpr_Servicoresponsavel_srvcod_Z = Z1549ServicoResponsavel_SrvCod;
         obj177.gxTpr_Servicoresponsavel_ctectecod_Z = Z1552ServicoResponsavel_CteCteCod;
         obj177.gxTpr_Servicoresponsavel_cteusrcod_Z = Z1550ServicoResponsavel_CteUsrCod;
         obj177.gxTpr_Servicoresponsavel_cteareacod_Z = Z1643ServicoResponsavel_CteAreaCod;
         obj177.gxTpr_Servicoresponsavel_srvcod_N = (short)(Convert.ToInt16(n1549ServicoResponsavel_SrvCod));
         obj177.gxTpr_Servicoresponsavel_ctectecod_N = (short)(Convert.ToInt16(n1552ServicoResponsavel_CteCteCod));
         obj177.gxTpr_Servicoresponsavel_cteusrcod_N = (short)(Convert.ToInt16(n1550ServicoResponsavel_CteUsrCod));
         obj177.gxTpr_Servicoresponsavel_cteareacod_N = (short)(Convert.ToInt16(n1643ServicoResponsavel_CteAreaCod));
         obj177.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow177( SdtServicoResponsavel obj177 )
      {
         obj177.gxTpr_Servicoresponsavel_codigo = A1548ServicoResponsavel_Codigo;
         return  ;
      }

      public void RowToVars177( SdtServicoResponsavel obj177 ,
                                int forceLoad )
      {
         Gx_mode = obj177.gxTpr_Mode;
         A1549ServicoResponsavel_SrvCod = obj177.gxTpr_Servicoresponsavel_srvcod;
         n1549ServicoResponsavel_SrvCod = false;
         A1552ServicoResponsavel_CteCteCod = obj177.gxTpr_Servicoresponsavel_ctectecod;
         n1552ServicoResponsavel_CteCteCod = false;
         A1550ServicoResponsavel_CteUsrCod = obj177.gxTpr_Servicoresponsavel_cteusrcod;
         n1550ServicoResponsavel_CteUsrCod = false;
         A1643ServicoResponsavel_CteAreaCod = obj177.gxTpr_Servicoresponsavel_cteareacod;
         n1643ServicoResponsavel_CteAreaCod = false;
         A1548ServicoResponsavel_Codigo = obj177.gxTpr_Servicoresponsavel_codigo;
         Z1548ServicoResponsavel_Codigo = obj177.gxTpr_Servicoresponsavel_codigo_Z;
         Z1549ServicoResponsavel_SrvCod = obj177.gxTpr_Servicoresponsavel_srvcod_Z;
         Z1552ServicoResponsavel_CteCteCod = obj177.gxTpr_Servicoresponsavel_ctectecod_Z;
         Z1550ServicoResponsavel_CteUsrCod = obj177.gxTpr_Servicoresponsavel_cteusrcod_Z;
         Z1643ServicoResponsavel_CteAreaCod = obj177.gxTpr_Servicoresponsavel_cteareacod_Z;
         n1549ServicoResponsavel_SrvCod = (bool)(Convert.ToBoolean(obj177.gxTpr_Servicoresponsavel_srvcod_N));
         n1552ServicoResponsavel_CteCteCod = (bool)(Convert.ToBoolean(obj177.gxTpr_Servicoresponsavel_ctectecod_N));
         n1550ServicoResponsavel_CteUsrCod = (bool)(Convert.ToBoolean(obj177.gxTpr_Servicoresponsavel_cteusrcod_N));
         n1643ServicoResponsavel_CteAreaCod = (bool)(Convert.ToBoolean(obj177.gxTpr_Servicoresponsavel_cteareacod_N));
         Gx_mode = obj177.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A1548ServicoResponsavel_Codigo = (int)getParm(obj,0);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey3W177( ) ;
         ScanKeyStart3W177( ) ;
         if ( RcdFound177 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z1548ServicoResponsavel_Codigo = A1548ServicoResponsavel_Codigo;
         }
         ZM3W177( -1) ;
         OnLoadActions3W177( ) ;
         AddRow3W177( ) ;
         ScanKeyEnd3W177( ) ;
         if ( RcdFound177 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars177( bcServicoResponsavel, 0) ;
         ScanKeyStart3W177( ) ;
         if ( RcdFound177 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z1548ServicoResponsavel_Codigo = A1548ServicoResponsavel_Codigo;
         }
         ZM3W177( -1) ;
         OnLoadActions3W177( ) ;
         AddRow3W177( ) ;
         ScanKeyEnd3W177( ) ;
         if ( RcdFound177 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars177( bcServicoResponsavel, 0) ;
         nKeyPressed = 1;
         GetKey3W177( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert3W177( ) ;
         }
         else
         {
            if ( RcdFound177 == 1 )
            {
               if ( A1548ServicoResponsavel_Codigo != Z1548ServicoResponsavel_Codigo )
               {
                  A1548ServicoResponsavel_Codigo = Z1548ServicoResponsavel_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update3W177( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A1548ServicoResponsavel_Codigo != Z1548ServicoResponsavel_Codigo )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert3W177( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert3W177( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow177( bcServicoResponsavel) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars177( bcServicoResponsavel, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey3W177( ) ;
         if ( RcdFound177 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A1548ServicoResponsavel_Codigo != Z1548ServicoResponsavel_Codigo )
            {
               A1548ServicoResponsavel_Codigo = Z1548ServicoResponsavel_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A1548ServicoResponsavel_Codigo != Z1548ServicoResponsavel_Codigo )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(9);
         context.RollbackDataStores( "ServicoResponsavel_BC");
         VarsToRow177( bcServicoResponsavel) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcServicoResponsavel.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcServicoResponsavel.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcServicoResponsavel )
         {
            bcServicoResponsavel = (SdtServicoResponsavel)(sdt);
            if ( StringUtil.StrCmp(bcServicoResponsavel.gxTpr_Mode, "") == 0 )
            {
               bcServicoResponsavel.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow177( bcServicoResponsavel) ;
            }
            else
            {
               RowToVars177( bcServicoResponsavel, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcServicoResponsavel.gxTpr_Mode, "") == 0 )
            {
               bcServicoResponsavel.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars177( bcServicoResponsavel, 1) ;
         return  ;
      }

      public SdtServicoResponsavel ServicoResponsavel_BC
      {
         get {
            return bcServicoResponsavel ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(9);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         BC003W7_A1548ServicoResponsavel_Codigo = new int[1] ;
         BC003W7_A1549ServicoResponsavel_SrvCod = new int[1] ;
         BC003W7_n1549ServicoResponsavel_SrvCod = new bool[] {false} ;
         BC003W7_A1552ServicoResponsavel_CteCteCod = new int[1] ;
         BC003W7_n1552ServicoResponsavel_CteCteCod = new bool[] {false} ;
         BC003W7_A1550ServicoResponsavel_CteUsrCod = new int[1] ;
         BC003W7_n1550ServicoResponsavel_CteUsrCod = new bool[] {false} ;
         BC003W5_A1643ServicoResponsavel_CteAreaCod = new int[1] ;
         BC003W5_n1643ServicoResponsavel_CteAreaCod = new bool[] {false} ;
         BC003W6_A1552ServicoResponsavel_CteCteCod = new int[1] ;
         BC003W6_n1552ServicoResponsavel_CteCteCod = new bool[] {false} ;
         BC003W8_A1548ServicoResponsavel_Codigo = new int[1] ;
         BC003W3_A1548ServicoResponsavel_Codigo = new int[1] ;
         BC003W3_A1549ServicoResponsavel_SrvCod = new int[1] ;
         BC003W3_n1549ServicoResponsavel_SrvCod = new bool[] {false} ;
         BC003W3_A1552ServicoResponsavel_CteCteCod = new int[1] ;
         BC003W3_n1552ServicoResponsavel_CteCteCod = new bool[] {false} ;
         BC003W3_A1550ServicoResponsavel_CteUsrCod = new int[1] ;
         BC003W3_n1550ServicoResponsavel_CteUsrCod = new bool[] {false} ;
         sMode177 = "";
         BC003W2_A1548ServicoResponsavel_Codigo = new int[1] ;
         BC003W2_A1549ServicoResponsavel_SrvCod = new int[1] ;
         BC003W2_n1549ServicoResponsavel_SrvCod = new bool[] {false} ;
         BC003W2_A1552ServicoResponsavel_CteCteCod = new int[1] ;
         BC003W2_n1552ServicoResponsavel_CteCteCod = new bool[] {false} ;
         BC003W2_A1550ServicoResponsavel_CteUsrCod = new int[1] ;
         BC003W2_n1550ServicoResponsavel_CteUsrCod = new bool[] {false} ;
         BC003W9_A1548ServicoResponsavel_Codigo = new int[1] ;
         BC003W13_A1643ServicoResponsavel_CteAreaCod = new int[1] ;
         BC003W13_n1643ServicoResponsavel_CteAreaCod = new bool[] {false} ;
         BC003W14_A1548ServicoResponsavel_Codigo = new int[1] ;
         BC003W14_A1549ServicoResponsavel_SrvCod = new int[1] ;
         BC003W14_n1549ServicoResponsavel_SrvCod = new bool[] {false} ;
         BC003W14_A1552ServicoResponsavel_CteCteCod = new int[1] ;
         BC003W14_n1552ServicoResponsavel_CteCteCod = new bool[] {false} ;
         BC003W14_A1550ServicoResponsavel_CteUsrCod = new int[1] ;
         BC003W14_n1550ServicoResponsavel_CteUsrCod = new bool[] {false} ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.servicoresponsavel_bc__default(),
            new Object[][] {
                new Object[] {
               BC003W2_A1548ServicoResponsavel_Codigo, BC003W2_A1549ServicoResponsavel_SrvCod, BC003W2_n1549ServicoResponsavel_SrvCod, BC003W2_A1552ServicoResponsavel_CteCteCod, BC003W2_n1552ServicoResponsavel_CteCteCod, BC003W2_A1550ServicoResponsavel_CteUsrCod, BC003W2_n1550ServicoResponsavel_CteUsrCod
               }
               , new Object[] {
               BC003W3_A1548ServicoResponsavel_Codigo, BC003W3_A1549ServicoResponsavel_SrvCod, BC003W3_n1549ServicoResponsavel_SrvCod, BC003W3_A1552ServicoResponsavel_CteCteCod, BC003W3_n1552ServicoResponsavel_CteCteCod, BC003W3_A1550ServicoResponsavel_CteUsrCod, BC003W3_n1550ServicoResponsavel_CteUsrCod
               }
               , new Object[] {
               BC003W5_A1643ServicoResponsavel_CteAreaCod, BC003W5_n1643ServicoResponsavel_CteAreaCod
               }
               , new Object[] {
               BC003W6_A1552ServicoResponsavel_CteCteCod
               }
               , new Object[] {
               BC003W7_A1548ServicoResponsavel_Codigo, BC003W7_A1549ServicoResponsavel_SrvCod, BC003W7_n1549ServicoResponsavel_SrvCod, BC003W7_A1552ServicoResponsavel_CteCteCod, BC003W7_n1552ServicoResponsavel_CteCteCod, BC003W7_A1550ServicoResponsavel_CteUsrCod, BC003W7_n1550ServicoResponsavel_CteUsrCod
               }
               , new Object[] {
               BC003W8_A1548ServicoResponsavel_Codigo
               }
               , new Object[] {
               BC003W9_A1548ServicoResponsavel_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC003W13_A1643ServicoResponsavel_CteAreaCod, BC003W13_n1643ServicoResponsavel_CteAreaCod
               }
               , new Object[] {
               BC003W14_A1548ServicoResponsavel_Codigo, BC003W14_A1549ServicoResponsavel_SrvCod, BC003W14_n1549ServicoResponsavel_SrvCod, BC003W14_A1552ServicoResponsavel_CteCteCod, BC003W14_n1552ServicoResponsavel_CteCteCod, BC003W14_A1550ServicoResponsavel_CteUsrCod, BC003W14_n1550ServicoResponsavel_CteUsrCod
               }
            }
         );
         INITTRN();
         /* Execute Start event if defined. */
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short RcdFound177 ;
      private int trnEnded ;
      private int Z1548ServicoResponsavel_Codigo ;
      private int A1548ServicoResponsavel_Codigo ;
      private int Z1549ServicoResponsavel_SrvCod ;
      private int A1549ServicoResponsavel_SrvCod ;
      private int Z1552ServicoResponsavel_CteCteCod ;
      private int A1552ServicoResponsavel_CteCteCod ;
      private int Z1550ServicoResponsavel_CteUsrCod ;
      private int A1550ServicoResponsavel_CteUsrCod ;
      private int Z1643ServicoResponsavel_CteAreaCod ;
      private int A1643ServicoResponsavel_CteAreaCod ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String sMode177 ;
      private bool n1549ServicoResponsavel_SrvCod ;
      private bool n1552ServicoResponsavel_CteCteCod ;
      private bool n1550ServicoResponsavel_CteUsrCod ;
      private bool n1643ServicoResponsavel_CteAreaCod ;
      private SdtServicoResponsavel bcServicoResponsavel ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] BC003W7_A1548ServicoResponsavel_Codigo ;
      private int[] BC003W7_A1549ServicoResponsavel_SrvCod ;
      private bool[] BC003W7_n1549ServicoResponsavel_SrvCod ;
      private int[] BC003W7_A1552ServicoResponsavel_CteCteCod ;
      private bool[] BC003W7_n1552ServicoResponsavel_CteCteCod ;
      private int[] BC003W7_A1550ServicoResponsavel_CteUsrCod ;
      private bool[] BC003W7_n1550ServicoResponsavel_CteUsrCod ;
      private int[] BC003W5_A1643ServicoResponsavel_CteAreaCod ;
      private bool[] BC003W5_n1643ServicoResponsavel_CteAreaCod ;
      private int[] BC003W6_A1552ServicoResponsavel_CteCteCod ;
      private bool[] BC003W6_n1552ServicoResponsavel_CteCteCod ;
      private int[] BC003W8_A1548ServicoResponsavel_Codigo ;
      private int[] BC003W3_A1548ServicoResponsavel_Codigo ;
      private int[] BC003W3_A1549ServicoResponsavel_SrvCod ;
      private bool[] BC003W3_n1549ServicoResponsavel_SrvCod ;
      private int[] BC003W3_A1552ServicoResponsavel_CteCteCod ;
      private bool[] BC003W3_n1552ServicoResponsavel_CteCteCod ;
      private int[] BC003W3_A1550ServicoResponsavel_CteUsrCod ;
      private bool[] BC003W3_n1550ServicoResponsavel_CteUsrCod ;
      private int[] BC003W2_A1548ServicoResponsavel_Codigo ;
      private int[] BC003W2_A1549ServicoResponsavel_SrvCod ;
      private bool[] BC003W2_n1549ServicoResponsavel_SrvCod ;
      private int[] BC003W2_A1552ServicoResponsavel_CteCteCod ;
      private bool[] BC003W2_n1552ServicoResponsavel_CteCteCod ;
      private int[] BC003W2_A1550ServicoResponsavel_CteUsrCod ;
      private bool[] BC003W2_n1550ServicoResponsavel_CteUsrCod ;
      private int[] BC003W9_A1548ServicoResponsavel_Codigo ;
      private int[] BC003W13_A1643ServicoResponsavel_CteAreaCod ;
      private bool[] BC003W13_n1643ServicoResponsavel_CteAreaCod ;
      private int[] BC003W14_A1548ServicoResponsavel_Codigo ;
      private int[] BC003W14_A1549ServicoResponsavel_SrvCod ;
      private bool[] BC003W14_n1549ServicoResponsavel_SrvCod ;
      private int[] BC003W14_A1552ServicoResponsavel_CteCteCod ;
      private bool[] BC003W14_n1552ServicoResponsavel_CteCteCod ;
      private int[] BC003W14_A1550ServicoResponsavel_CteUsrCod ;
      private bool[] BC003W14_n1550ServicoResponsavel_CteUsrCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
   }

   public class servicoresponsavel_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC003W7 ;
          prmBC003W7 = new Object[] {
          new Object[] {"@ServicoResponsavel_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003W5 ;
          prmBC003W5 = new Object[] {
          new Object[] {"@ServicoResponsavel_CteCteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003W6 ;
          prmBC003W6 = new Object[] {
          new Object[] {"@ServicoResponsavel_CteCteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoResponsavel_CteUsrCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003W8 ;
          prmBC003W8 = new Object[] {
          new Object[] {"@ServicoResponsavel_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003W3 ;
          prmBC003W3 = new Object[] {
          new Object[] {"@ServicoResponsavel_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003W2 ;
          prmBC003W2 = new Object[] {
          new Object[] {"@ServicoResponsavel_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003W9 ;
          prmBC003W9 = new Object[] {
          new Object[] {"@ServicoResponsavel_SrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoResponsavel_CteCteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoResponsavel_CteUsrCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003W10 ;
          prmBC003W10 = new Object[] {
          new Object[] {"@ServicoResponsavel_SrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoResponsavel_CteCteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoResponsavel_CteUsrCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoResponsavel_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003W11 ;
          prmBC003W11 = new Object[] {
          new Object[] {"@ServicoResponsavel_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003W13 ;
          prmBC003W13 = new Object[] {
          new Object[] {"@ServicoResponsavel_CteCteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003W14 ;
          prmBC003W14 = new Object[] {
          new Object[] {"@ServicoResponsavel_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC003W2", "SELECT [ServicoResponsavel_Codigo], [ServicoResponsavel_SrvCod], [ServicoResponsavel_CteCteCod] AS ServicoResponsavel_CteCteCod, [ServicoResponsavel_CteUsrCod] AS ServicoResponsavel_CteUsrCod FROM [ServicoResponsavel] WITH (UPDLOCK) WHERE [ServicoResponsavel_Codigo] = @ServicoResponsavel_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003W2,1,0,true,false )
             ,new CursorDef("BC003W3", "SELECT [ServicoResponsavel_Codigo], [ServicoResponsavel_SrvCod], [ServicoResponsavel_CteCteCod] AS ServicoResponsavel_CteCteCod, [ServicoResponsavel_CteUsrCod] AS ServicoResponsavel_CteUsrCod FROM [ServicoResponsavel] WITH (NOLOCK) WHERE [ServicoResponsavel_Codigo] = @ServicoResponsavel_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003W3,1,0,true,false )
             ,new CursorDef("BC003W5", "SELECT COALESCE( T1.[ServicoResponsavel_CteAreaCod], 0) AS ServicoResponsavel_CteAreaCod FROM (SELECT MIN([AreaTrabalho_Codigo]) AS ServicoResponsavel_CteAreaCod FROM [AreaTrabalho] WITH (NOLOCK) WHERE [Contratante_Codigo] = @ServicoResponsavel_CteCteCod ) T1 ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003W5,1,0,true,false )
             ,new CursorDef("BC003W6", "SELECT [ContratanteUsuario_ContratanteCod] AS ServicoResponsavel_CteCteCod FROM [ContratanteUsuario] WITH (NOLOCK) WHERE [ContratanteUsuario_ContratanteCod] = @ServicoResponsavel_CteCteCod AND [ContratanteUsuario_UsuarioCod] = @ServicoResponsavel_CteUsrCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003W6,1,0,true,false )
             ,new CursorDef("BC003W7", "SELECT TM1.[ServicoResponsavel_Codigo], TM1.[ServicoResponsavel_SrvCod], TM1.[ServicoResponsavel_CteCteCod] AS ServicoResponsavel_CteCteCod, TM1.[ServicoResponsavel_CteUsrCod] AS ServicoResponsavel_CteUsrCod FROM [ServicoResponsavel] TM1 WITH (NOLOCK) WHERE TM1.[ServicoResponsavel_Codigo] = @ServicoResponsavel_Codigo ORDER BY TM1.[ServicoResponsavel_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC003W7,100,0,true,false )
             ,new CursorDef("BC003W8", "SELECT [ServicoResponsavel_Codigo] FROM [ServicoResponsavel] WITH (NOLOCK) WHERE [ServicoResponsavel_Codigo] = @ServicoResponsavel_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC003W8,1,0,true,false )
             ,new CursorDef("BC003W9", "INSERT INTO [ServicoResponsavel]([ServicoResponsavel_SrvCod], [ServicoResponsavel_CteCteCod], [ServicoResponsavel_CteUsrCod]) VALUES(@ServicoResponsavel_SrvCod, @ServicoResponsavel_CteCteCod, @ServicoResponsavel_CteUsrCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmBC003W9)
             ,new CursorDef("BC003W10", "UPDATE [ServicoResponsavel] SET [ServicoResponsavel_SrvCod]=@ServicoResponsavel_SrvCod, [ServicoResponsavel_CteCteCod]=@ServicoResponsavel_CteCteCod, [ServicoResponsavel_CteUsrCod]=@ServicoResponsavel_CteUsrCod  WHERE [ServicoResponsavel_Codigo] = @ServicoResponsavel_Codigo", GxErrorMask.GX_NOMASK,prmBC003W10)
             ,new CursorDef("BC003W11", "DELETE FROM [ServicoResponsavel]  WHERE [ServicoResponsavel_Codigo] = @ServicoResponsavel_Codigo", GxErrorMask.GX_NOMASK,prmBC003W11)
             ,new CursorDef("BC003W13", "SELECT COALESCE( T1.[ServicoResponsavel_CteAreaCod], 0) AS ServicoResponsavel_CteAreaCod FROM (SELECT MIN([AreaTrabalho_Codigo]) AS ServicoResponsavel_CteAreaCod FROM [AreaTrabalho] WITH (NOLOCK) WHERE [Contratante_Codigo] = @ServicoResponsavel_CteCteCod ) T1 ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003W13,1,0,true,false )
             ,new CursorDef("BC003W14", "SELECT TM1.[ServicoResponsavel_Codigo], TM1.[ServicoResponsavel_SrvCod], TM1.[ServicoResponsavel_CteCteCod] AS ServicoResponsavel_CteCteCod, TM1.[ServicoResponsavel_CteUsrCod] AS ServicoResponsavel_CteUsrCod FROM [ServicoResponsavel] TM1 WITH (NOLOCK) WHERE TM1.[ServicoResponsavel_Codigo] = @ServicoResponsavel_Codigo ORDER BY TM1.[ServicoResponsavel_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC003W14,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[5]);
                }
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[5]);
                }
                stmt.SetParameter(4, (int)parms[6]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
