/*
               File: type_SdtSDT_RedmineUsers_user_custom_fields
        Description: SDT_RedmineUsers
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:37:6.2
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_RedmineUsers.user.custom_fields" )]
   [XmlType(TypeName =  "SDT_RedmineUsers.user.custom_fields" , Namespace = "" )]
   [System.Xml.Serialization.XmlInclude( typeof( SdtSDT_RedmineUsers_user_custom_fields_custom_field ))]
   [Serializable]
   public class SdtSDT_RedmineUsers_user_custom_fields : GxUserType
   {
      public SdtSDT_RedmineUsers_user_custom_fields( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_RedmineUsers_user_custom_fields_Type = "";
      }

      public SdtSDT_RedmineUsers_user_custom_fields( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_RedmineUsers_user_custom_fields deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType());
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_RedmineUsers_user_custom_fields)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_RedmineUsers_user_custom_fields obj ;
         obj = this;
         obj.gxTpr_Type = deserialized.gxTpr_Type;
         obj.gxTpr_Custom_field = deserialized.gxTpr_Custom_field;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         if ( oReader.ExistsAttribute("type") == 1 )
         {
            gxTv_SdtSDT_RedmineUsers_user_custom_fields_Type = oReader.GetAttributeByName("type");
            if ( GXSoapError > 0 )
            {
               readOk = 1;
            }
         }
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "custom_field") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  if ( gxTv_SdtSDT_RedmineUsers_user_custom_fields_Custom_field == null )
                  {
                     gxTv_SdtSDT_RedmineUsers_user_custom_fields_Custom_field = new SdtSDT_RedmineUsers_user_custom_fields_custom_field(context);
                  }
                  if ( ( oReader.IsSimple == 0 ) || ( oReader.AttributeCount > 0 ) )
                  {
                     GXSoapError = gxTv_SdtSDT_RedmineUsers_user_custom_fields_Custom_field.readxml(oReader, "custom_field");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_RedmineUsers.user.custom_fields";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteAttribute("type", StringUtil.RTrim( gxTv_SdtSDT_RedmineUsers_user_custom_fields_Type));
         if ( gxTv_SdtSDT_RedmineUsers_user_custom_fields_Custom_field != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "";
            }
            else
            {
               sNameSpace1 = "";
            }
            gxTv_SdtSDT_RedmineUsers_user_custom_fields_Custom_field.writexml(oWriter, "custom_field", sNameSpace1);
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("type", gxTv_SdtSDT_RedmineUsers_user_custom_fields_Type, false);
         if ( gxTv_SdtSDT_RedmineUsers_user_custom_fields_Custom_field != null )
         {
            AddObjectProperty("custom_field", gxTv_SdtSDT_RedmineUsers_user_custom_fields_Custom_field, false);
         }
         return  ;
      }

      [SoapAttribute( AttributeName = "type" )]
      [XmlAttribute( AttributeName = "type" )]
      public String gxTpr_Type
      {
         get {
            return gxTv_SdtSDT_RedmineUsers_user_custom_fields_Type ;
         }

         set {
            gxTv_SdtSDT_RedmineUsers_user_custom_fields_Type = (String)(value);
         }

      }

      [  SoapElement( ElementName = "custom_field" )]
      [  XmlElement( ElementName = "custom_field"   )]
      public SdtSDT_RedmineUsers_user_custom_fields_custom_field gxTpr_Custom_field
      {
         get {
            if ( gxTv_SdtSDT_RedmineUsers_user_custom_fields_Custom_field == null )
            {
               gxTv_SdtSDT_RedmineUsers_user_custom_fields_Custom_field = new SdtSDT_RedmineUsers_user_custom_fields_custom_field(context);
            }
            return gxTv_SdtSDT_RedmineUsers_user_custom_fields_Custom_field ;
         }

         set {
            gxTv_SdtSDT_RedmineUsers_user_custom_fields_Custom_field = value;
         }

      }

      public void gxTv_SdtSDT_RedmineUsers_user_custom_fields_Custom_field_SetNull( )
      {
         gxTv_SdtSDT_RedmineUsers_user_custom_fields_Custom_field = null;
         return  ;
      }

      public bool gxTv_SdtSDT_RedmineUsers_user_custom_fields_Custom_field_IsNull( )
      {
         if ( gxTv_SdtSDT_RedmineUsers_user_custom_fields_Custom_field == null )
         {
            return true ;
         }
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtSDT_RedmineUsers_user_custom_fields_Type = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String gxTv_SdtSDT_RedmineUsers_user_custom_fields_Type ;
      protected String sTagName ;
      protected SdtSDT_RedmineUsers_user_custom_fields_custom_field gxTv_SdtSDT_RedmineUsers_user_custom_fields_Custom_field=null ;
   }

   [DataContract(Name = @"SDT_RedmineUsers.user.custom_fields", Namespace = "")]
   public class SdtSDT_RedmineUsers_user_custom_fields_RESTInterface : GxGenericCollectionItem<SdtSDT_RedmineUsers_user_custom_fields>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_RedmineUsers_user_custom_fields_RESTInterface( ) : base()
      {
      }

      public SdtSDT_RedmineUsers_user_custom_fields_RESTInterface( SdtSDT_RedmineUsers_user_custom_fields psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "type" , Order = 0 )]
      public String gxTpr_Type
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Type) ;
         }

         set {
            sdt.gxTpr_Type = (String)(value);
         }

      }

      [DataMember( Name = "custom_field" , Order = 1 )]
      public SdtSDT_RedmineUsers_user_custom_fields_custom_field_RESTInterface gxTpr_Custom_field
      {
         get {
            return new SdtSDT_RedmineUsers_user_custom_fields_custom_field_RESTInterface(sdt.gxTpr_Custom_field) ;
         }

         set {
            sdt.gxTpr_Custom_field = value.sdt;
         }

      }

      public SdtSDT_RedmineUsers_user_custom_fields sdt
      {
         get {
            return (SdtSDT_RedmineUsers_user_custom_fields)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_RedmineUsers_user_custom_fields() ;
         }
      }

   }

}
