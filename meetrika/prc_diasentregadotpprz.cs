/*
               File: PRC_DiasEntregaDoTpPrz
        Description: Dias Entrega Do Tipo Prazo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:9:37.42
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_diasentregadotpprz : GXProcedure
   {
      public prc_diasentregadotpprz( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_diasentregadotpprz( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContratoServicosPrazo_CntSrvCod ,
                           decimal aP1_Unidades ,
                           short aP2_DiasComplexidade ,
                           out short aP3_Dias )
      {
         this.A903ContratoServicosPrazo_CntSrvCod = aP0_ContratoServicosPrazo_CntSrvCod;
         this.AV13Unidades = aP1_Unidades;
         this.AV9DiasComplexidade = aP2_DiasComplexidade;
         this.AV8Dias = 0 ;
         initialize();
         executePrivate();
         aP0_ContratoServicosPrazo_CntSrvCod=this.A903ContratoServicosPrazo_CntSrvCod;
         aP3_Dias=this.AV8Dias;
      }

      public short executeUdp( ref int aP0_ContratoServicosPrazo_CntSrvCod ,
                               decimal aP1_Unidades ,
                               short aP2_DiasComplexidade )
      {
         this.A903ContratoServicosPrazo_CntSrvCod = aP0_ContratoServicosPrazo_CntSrvCod;
         this.AV13Unidades = aP1_Unidades;
         this.AV9DiasComplexidade = aP2_DiasComplexidade;
         this.AV8Dias = 0 ;
         initialize();
         executePrivate();
         aP0_ContratoServicosPrazo_CntSrvCod=this.A903ContratoServicosPrazo_CntSrvCod;
         aP3_Dias=this.AV8Dias;
         return AV8Dias ;
      }

      public void executeSubmit( ref int aP0_ContratoServicosPrazo_CntSrvCod ,
                                 decimal aP1_Unidades ,
                                 short aP2_DiasComplexidade ,
                                 out short aP3_Dias )
      {
         prc_diasentregadotpprz objprc_diasentregadotpprz;
         objprc_diasentregadotpprz = new prc_diasentregadotpprz();
         objprc_diasentregadotpprz.A903ContratoServicosPrazo_CntSrvCod = aP0_ContratoServicosPrazo_CntSrvCod;
         objprc_diasentregadotpprz.AV13Unidades = aP1_Unidades;
         objprc_diasentregadotpprz.AV9DiasComplexidade = aP2_DiasComplexidade;
         objprc_diasentregadotpprz.AV8Dias = 0 ;
         objprc_diasentregadotpprz.context.SetSubmitInitialConfig(context);
         objprc_diasentregadotpprz.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_diasentregadotpprz);
         aP0_ContratoServicosPrazo_CntSrvCod=this.A903ContratoServicosPrazo_CntSrvCod;
         aP3_Dias=this.AV8Dias;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_diasentregadotpprz)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00BB2 */
         pr_default.execute(0, new Object[] {A903ContratoServicosPrazo_CntSrvCod});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1823ContratoServicosPrazo_Momento = P00BB2_A1823ContratoServicosPrazo_Momento[0];
            n1823ContratoServicosPrazo_Momento = P00BB2_n1823ContratoServicosPrazo_Momento[0];
            A904ContratoServicosPrazo_Tipo = P00BB2_A904ContratoServicosPrazo_Tipo[0];
            A905ContratoServicosPrazo_Dias = P00BB2_A905ContratoServicosPrazo_Dias[0];
            n905ContratoServicosPrazo_Dias = P00BB2_n905ContratoServicosPrazo_Dias[0];
            A1456ContratoServicosPrazo_Cada = P00BB2_A1456ContratoServicosPrazo_Cada[0];
            n1456ContratoServicosPrazo_Cada = P00BB2_n1456ContratoServicosPrazo_Cada[0];
            A1649ContratoServicos_PrazoInicio = P00BB2_A1649ContratoServicos_PrazoInicio[0];
            n1649ContratoServicos_PrazoInicio = P00BB2_n1649ContratoServicos_PrazoInicio[0];
            A1454ContratoServicos_PrazoTpDias = P00BB2_A1454ContratoServicos_PrazoTpDias[0];
            n1454ContratoServicos_PrazoTpDias = P00BB2_n1454ContratoServicos_PrazoTpDias[0];
            A1649ContratoServicos_PrazoInicio = P00BB2_A1649ContratoServicos_PrazoInicio[0];
            n1649ContratoServicos_PrazoInicio = P00BB2_n1649ContratoServicos_PrazoInicio[0];
            A1454ContratoServicos_PrazoTpDias = P00BB2_A1454ContratoServicos_PrazoTpDias[0];
            n1454ContratoServicos_PrazoTpDias = P00BB2_n1454ContratoServicos_PrazoTpDias[0];
            if ( ( A1823ContratoServicosPrazo_Momento == 1 ) && ( StringUtil.StrCmp(AV16WebSession.Get("Momento"), "Solicitacao") == 0 ) )
            {
               AV8Dias = 0;
            }
            else if ( ( StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "F") == 0 ) || ( StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "E") == 0 ) )
            {
               AV8Dias = (short)(AV8Dias+A905ContratoServicosPrazo_Dias);
            }
            else if ( StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "C") == 0 )
            {
               AV8Dias = (short)(AV8Dias+AV9DiasComplexidade);
            }
            else if ( StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "E") == 0 )
            {
               AV11DiasParaCada = 1;
               if ( ( A1456ContratoServicosPrazo_Cada > Convert.ToDecimal( 0 )) )
               {
                  AV11DiasParaCada = (short)(AV13Unidades/ (decimal)(A1456ContratoServicosPrazo_Cada));
                  if ( AV11DiasParaCada > NumberUtil.Int( AV11DiasParaCada) )
                  {
                     AV11DiasParaCada = (short)(NumberUtil.Int( AV11DiasParaCada)+1);
                  }
               }
               AV8Dias = (short)(AV8Dias+(AV11DiasParaCada*A905ContratoServicosPrazo_Dias));
            }
            else if ( StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "V") == 0 )
            {
               /* Using cursor P00BB3 */
               pr_default.execute(1, new Object[] {A903ContratoServicosPrazo_CntSrvCod, AV13Unidades});
               while ( (pr_default.getStatus(1) != 101) )
               {
                  A908ContratoServicosPrazoRegra_Fim = P00BB3_A908ContratoServicosPrazoRegra_Fim[0];
                  A907ContratoServicosPrazoRegra_Inicio = P00BB3_A907ContratoServicosPrazoRegra_Inicio[0];
                  A909ContratoServicosPrazoRegra_Dias = P00BB3_A909ContratoServicosPrazoRegra_Dias[0];
                  A906ContratoServicosPrazoRegra_Sequencial = P00BB3_A906ContratoServicosPrazoRegra_Sequencial[0];
                  AV8Dias = (short)(AV8Dias+A909ContratoServicosPrazoRegra_Dias);
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  pr_default.readNext(1);
               }
               pr_default.close(1);
            }
            else if ( StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "A") == 0 )
            {
               AV12PrazoAnalise = DateTimeUtil.ResetTime( DateTimeUtil.ServerDate( context, "DEFAULT") ) ;
               if ( A1649ContratoServicos_PrazoInicio == 2 )
               {
                  new prc_datadeinicioutil(context ).execute( ref  AV12PrazoAnalise) ;
               }
               GXt_dtime1 = AV12PrazoAnalise;
               new prc_adddiasuteis(context ).execute(  AV12PrazoAnalise,  AV8Dias,  A1454ContratoServicos_PrazoTpDias, out  GXt_dtime1) ;
               AV12PrazoAnalise = GXt_dtime1;
               GXt_dtime1 = AV10DiaDisponivel;
               new prc_diadisponivelagendaatendimento(context ).execute( ref  A903ContratoServicosPrazo_CntSrvCod,  AV12PrazoAnalise,  AV13Unidades, out  GXt_dtime1) ;
               AV10DiaDisponivel = GXt_dtime1;
               GXt_int2 = AV8Dias;
               new prc_diasuteisentre(context ).execute(  DateTimeUtil.ResetTime( AV12PrazoAnalise),  DateTimeUtil.ResetTime( AV10DiaDisponivel),  A1454ContratoServicos_PrazoTpDias, out  GXt_int2) ;
               AV8Dias = (short)(AV8Dias+(GXt_int2));
            }
            else if ( StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "P") == 0 )
            {
               AV14DiasPF = (decimal)(AV13Unidades/ (decimal)(A1456ContratoServicosPrazo_Cada));
               if ( ( AV14DiasPF != Convert.ToDecimal( NumberUtil.Int( (long)(AV14DiasPF)) )) )
               {
                  AV14DiasPF = (decimal)(NumberUtil.Int( (long)(AV14DiasPF))+1);
               }
               AV8Dias = (short)(AV8Dias+(AV14DiasPF*A905ContratoServicosPrazo_Dias));
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00BB2_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         P00BB2_A1823ContratoServicosPrazo_Momento = new short[1] ;
         P00BB2_n1823ContratoServicosPrazo_Momento = new bool[] {false} ;
         P00BB2_A904ContratoServicosPrazo_Tipo = new String[] {""} ;
         P00BB2_A905ContratoServicosPrazo_Dias = new short[1] ;
         P00BB2_n905ContratoServicosPrazo_Dias = new bool[] {false} ;
         P00BB2_A1456ContratoServicosPrazo_Cada = new decimal[1] ;
         P00BB2_n1456ContratoServicosPrazo_Cada = new bool[] {false} ;
         P00BB2_A1649ContratoServicos_PrazoInicio = new short[1] ;
         P00BB2_n1649ContratoServicos_PrazoInicio = new bool[] {false} ;
         P00BB2_A1454ContratoServicos_PrazoTpDias = new String[] {""} ;
         P00BB2_n1454ContratoServicos_PrazoTpDias = new bool[] {false} ;
         A904ContratoServicosPrazo_Tipo = "";
         A1454ContratoServicos_PrazoTpDias = "";
         AV16WebSession = context.GetSession();
         P00BB3_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         P00BB3_A908ContratoServicosPrazoRegra_Fim = new decimal[1] ;
         P00BB3_A907ContratoServicosPrazoRegra_Inicio = new decimal[1] ;
         P00BB3_A909ContratoServicosPrazoRegra_Dias = new short[1] ;
         P00BB3_A906ContratoServicosPrazoRegra_Sequencial = new short[1] ;
         AV12PrazoAnalise = (DateTime)(DateTime.MinValue);
         AV10DiaDisponivel = (DateTime)(DateTime.MinValue);
         GXt_dtime1 = (DateTime)(DateTime.MinValue);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_diasentregadotpprz__default(),
            new Object[][] {
                new Object[] {
               P00BB2_A903ContratoServicosPrazo_CntSrvCod, P00BB2_A1823ContratoServicosPrazo_Momento, P00BB2_n1823ContratoServicosPrazo_Momento, P00BB2_A904ContratoServicosPrazo_Tipo, P00BB2_A905ContratoServicosPrazo_Dias, P00BB2_n905ContratoServicosPrazo_Dias, P00BB2_A1456ContratoServicosPrazo_Cada, P00BB2_n1456ContratoServicosPrazo_Cada, P00BB2_A1649ContratoServicos_PrazoInicio, P00BB2_n1649ContratoServicos_PrazoInicio,
               P00BB2_A1454ContratoServicos_PrazoTpDias, P00BB2_n1454ContratoServicos_PrazoTpDias
               }
               , new Object[] {
               P00BB3_A903ContratoServicosPrazo_CntSrvCod, P00BB3_A908ContratoServicosPrazoRegra_Fim, P00BB3_A907ContratoServicosPrazoRegra_Inicio, P00BB3_A909ContratoServicosPrazoRegra_Dias, P00BB3_A906ContratoServicosPrazoRegra_Sequencial
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV9DiasComplexidade ;
      private short AV8Dias ;
      private short A1823ContratoServicosPrazo_Momento ;
      private short A905ContratoServicosPrazo_Dias ;
      private short A1649ContratoServicos_PrazoInicio ;
      private short AV11DiasParaCada ;
      private short A909ContratoServicosPrazoRegra_Dias ;
      private short A906ContratoServicosPrazoRegra_Sequencial ;
      private short GXt_int2 ;
      private int A903ContratoServicosPrazo_CntSrvCod ;
      private decimal AV13Unidades ;
      private decimal A1456ContratoServicosPrazo_Cada ;
      private decimal A908ContratoServicosPrazoRegra_Fim ;
      private decimal A907ContratoServicosPrazoRegra_Inicio ;
      private decimal AV14DiasPF ;
      private String scmdbuf ;
      private String A904ContratoServicosPrazo_Tipo ;
      private String A1454ContratoServicos_PrazoTpDias ;
      private DateTime AV12PrazoAnalise ;
      private DateTime AV10DiaDisponivel ;
      private DateTime GXt_dtime1 ;
      private bool n1823ContratoServicosPrazo_Momento ;
      private bool n905ContratoServicosPrazo_Dias ;
      private bool n1456ContratoServicosPrazo_Cada ;
      private bool n1649ContratoServicos_PrazoInicio ;
      private bool n1454ContratoServicos_PrazoTpDias ;
      private IGxSession AV16WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContratoServicosPrazo_CntSrvCod ;
      private IDataStoreProvider pr_default ;
      private int[] P00BB2_A903ContratoServicosPrazo_CntSrvCod ;
      private short[] P00BB2_A1823ContratoServicosPrazo_Momento ;
      private bool[] P00BB2_n1823ContratoServicosPrazo_Momento ;
      private String[] P00BB2_A904ContratoServicosPrazo_Tipo ;
      private short[] P00BB2_A905ContratoServicosPrazo_Dias ;
      private bool[] P00BB2_n905ContratoServicosPrazo_Dias ;
      private decimal[] P00BB2_A1456ContratoServicosPrazo_Cada ;
      private bool[] P00BB2_n1456ContratoServicosPrazo_Cada ;
      private short[] P00BB2_A1649ContratoServicos_PrazoInicio ;
      private bool[] P00BB2_n1649ContratoServicos_PrazoInicio ;
      private String[] P00BB2_A1454ContratoServicos_PrazoTpDias ;
      private bool[] P00BB2_n1454ContratoServicos_PrazoTpDias ;
      private int[] P00BB3_A903ContratoServicosPrazo_CntSrvCod ;
      private decimal[] P00BB3_A908ContratoServicosPrazoRegra_Fim ;
      private decimal[] P00BB3_A907ContratoServicosPrazoRegra_Inicio ;
      private short[] P00BB3_A909ContratoServicosPrazoRegra_Dias ;
      private short[] P00BB3_A906ContratoServicosPrazoRegra_Sequencial ;
      private short aP3_Dias ;
   }

   public class prc_diasentregadotpprz__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00BB2 ;
          prmP00BB2 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00BB3 ;
          prmP00BB3 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13Unidades",SqlDbType.Decimal,14,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00BB2", "SELECT TOP 1 T1.[ContratoServicosPrazo_CntSrvCod] AS ContratoServicosPrazo_CntSrvCod, T1.[ContratoServicosPrazo_Momento], T1.[ContratoServicosPrazo_Tipo], T1.[ContratoServicosPrazo_Dias], T1.[ContratoServicosPrazo_Cada], T2.[ContratoServicos_PrazoInicio], T2.[ContratoServicos_PrazoTpDias] FROM ([ContratoServicosPrazo] T1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoServicosPrazo_CntSrvCod]) WHERE T1.[ContratoServicosPrazo_CntSrvCod] = @ContratoServicosPrazo_CntSrvCod ORDER BY T1.[ContratoServicosPrazo_CntSrvCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00BB2,1,0,true,true )
             ,new CursorDef("P00BB3", "SELECT TOP 1 [ContratoServicosPrazo_CntSrvCod] AS ContratoServicosPrazo_CntSrvCod, [ContratoServicosPrazoRegra_Fim], [ContratoServicosPrazoRegra_Inicio], [ContratoServicosPrazoRegra_Dias], [ContratoServicosPrazoRegra_Sequencial] FROM [ContratoServicosPrazoContratoServicosPrazoRegra] WITH (NOLOCK) WHERE ([ContratoServicosPrazo_CntSrvCod] = @ContratoServicosPrazo_CntSrvCod) AND ([ContratoServicosPrazoRegra_Inicio] <= @AV13Unidades) AND ([ContratoServicosPrazoRegra_Fim] = 0 or [ContratoServicosPrazoRegra_Fim] >= @AV13Unidades) ORDER BY [ContratoServicosPrazo_CntSrvCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00BB3,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 20) ;
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((short[]) buf[8])[0] = rslt.getShort(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 1) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (decimal)parms[1]);
                return;
       }
    }

 }

}
