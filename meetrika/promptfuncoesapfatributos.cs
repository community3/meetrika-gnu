/*
               File: PromptFuncoesAPFAtributos
        Description: Selecione Atributos das Fun��es de APF
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:38:50.29
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptfuncoesapfatributos : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptfuncoesapfatributos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptfuncoesapfatributos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutFuncaoAPF_Codigo ,
                           ref int aP1_InOutFuncaoAPFAtributos_AtributosCod ,
                           ref String aP2_InOutFuncaoAPF_Nome )
      {
         this.AV7InOutFuncaoAPF_Codigo = aP0_InOutFuncaoAPF_Codigo;
         this.AV8InOutFuncaoAPFAtributos_AtributosCod = aP1_InOutFuncaoAPFAtributos_AtributosCod;
         this.AV9InOutFuncaoAPF_Nome = aP2_InOutFuncaoAPF_Nome;
         executePrivate();
         aP0_InOutFuncaoAPF_Codigo=this.AV7InOutFuncaoAPF_Codigo;
         aP1_InOutFuncaoAPFAtributos_AtributosCod=this.AV8InOutFuncaoAPFAtributos_AtributosCod;
         aP2_InOutFuncaoAPF_Nome=this.AV9InOutFuncaoAPF_Nome;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         chkFuncoesAPFAtributos_Regra = new GXCheckbox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_83 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_83_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_83_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV14OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
               AV15OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
               AV16DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
               AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV18FuncaoAPF_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18FuncaoAPF_Nome1", AV18FuncaoAPF_Nome1);
               AV19FuncaoAPFAtributos_AtributosNom1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19FuncaoAPFAtributos_AtributosNom1", AV19FuncaoAPFAtributos_AtributosNom1);
               AV21DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
               AV23FuncaoAPF_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23FuncaoAPF_Nome2", AV23FuncaoAPF_Nome2);
               AV24FuncaoAPFAtributos_AtributosNom2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24FuncaoAPFAtributos_AtributosNom2", AV24FuncaoAPFAtributos_AtributosNom2);
               AV26DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
               AV27DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
               AV28FuncaoAPF_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28FuncaoAPF_Nome3", AV28FuncaoAPF_Nome3);
               AV29FuncaoAPFAtributos_AtributosNom3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29FuncaoAPFAtributos_AtributosNom3", AV29FuncaoAPFAtributos_AtributosNom3);
               AV20DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV25DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
               AV35TFFuncaoAPF_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFFuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFFuncaoAPF_Codigo), 6, 0)));
               AV36TFFuncaoAPF_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFFuncaoAPF_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFFuncaoAPF_Codigo_To), 6, 0)));
               AV39TFFuncaoAPF_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFFuncaoAPF_Nome", AV39TFFuncaoAPF_Nome);
               AV40TFFuncaoAPF_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFFuncaoAPF_Nome_Sel", AV40TFFuncaoAPF_Nome_Sel);
               AV43TFFuncaoAPFAtributos_AtributosCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFFuncaoAPFAtributos_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFFuncaoAPFAtributos_AtributosCod), 6, 0)));
               AV44TFFuncaoAPFAtributos_AtributosCod_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFFuncaoAPFAtributos_AtributosCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFFuncaoAPFAtributos_AtributosCod_To), 6, 0)));
               AV47TFFuncaoAPFAtributos_AtributosNom = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFFuncaoAPFAtributos_AtributosNom", AV47TFFuncaoAPFAtributos_AtributosNom);
               AV48TFFuncaoAPFAtributos_AtributosNom_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFFuncaoAPFAtributos_AtributosNom_Sel", AV48TFFuncaoAPFAtributos_AtributosNom_Sel);
               AV51TFFuncaoAPFAtributos_FuncaoDadosCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFFuncaoAPFAtributos_FuncaoDadosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51TFFuncaoAPFAtributos_FuncaoDadosCod), 6, 0)));
               AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To), 6, 0)));
               AV55TFFuncaoAPFAtributos_AtrTabelaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFFuncaoAPFAtributos_AtrTabelaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFFuncaoAPFAtributos_AtrTabelaCod), 6, 0)));
               AV56TFFuncaoAPFAtributos_AtrTabelaCod_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFFuncaoAPFAtributos_AtrTabelaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56TFFuncaoAPFAtributos_AtrTabelaCod_To), 6, 0)));
               AV59TFFuncaoAPFAtributos_AtrTabelaNom = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFFuncaoAPFAtributos_AtrTabelaNom", AV59TFFuncaoAPFAtributos_AtrTabelaNom);
               AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel", AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel);
               AV63TFFuncoesAPFAtributos_Regra_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFFuncoesAPFAtributos_Regra_Sel", StringUtil.Str( (decimal)(AV63TFFuncoesAPFAtributos_Regra_Sel), 1, 0));
               AV66TFFuncoesAPFAtributos_Code = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFFuncoesAPFAtributos_Code", AV66TFFuncoesAPFAtributos_Code);
               AV67TFFuncoesAPFAtributos_Code_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFFuncoesAPFAtributos_Code_Sel", AV67TFFuncoesAPFAtributos_Code_Sel);
               AV70TFFuncoesAPFAtributos_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFFuncoesAPFAtributos_Nome", AV70TFFuncoesAPFAtributos_Nome);
               AV71TFFuncoesAPFAtributos_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFFuncoesAPFAtributos_Nome_Sel", AV71TFFuncoesAPFAtributos_Nome_Sel);
               AV74TFFuncoesAPFAtributos_Descricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFFuncoesAPFAtributos_Descricao", AV74TFFuncoesAPFAtributos_Descricao);
               AV75TFFuncoesAPFAtributos_Descricao_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFFuncoesAPFAtributos_Descricao_Sel", AV75TFFuncoesAPFAtributos_Descricao_Sel);
               AV37ddo_FuncaoAPF_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_FuncaoAPF_CodigoTitleControlIdToReplace", AV37ddo_FuncaoAPF_CodigoTitleControlIdToReplace);
               AV41ddo_FuncaoAPF_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_FuncaoAPF_NomeTitleControlIdToReplace", AV41ddo_FuncaoAPF_NomeTitleControlIdToReplace);
               AV45ddo_FuncaoAPFAtributos_AtributosCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_FuncaoAPFAtributos_AtributosCodTitleControlIdToReplace", AV45ddo_FuncaoAPFAtributos_AtributosCodTitleControlIdToReplace);
               AV49ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace", AV49ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace);
               AV53ddo_FuncaoAPFAtributos_FuncaoDadosCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_FuncaoAPFAtributos_FuncaoDadosCodTitleControlIdToReplace", AV53ddo_FuncaoAPFAtributos_FuncaoDadosCodTitleControlIdToReplace);
               AV57ddo_FuncaoAPFAtributos_AtrTabelaCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_FuncaoAPFAtributos_AtrTabelaCodTitleControlIdToReplace", AV57ddo_FuncaoAPFAtributos_AtrTabelaCodTitleControlIdToReplace);
               AV61ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace", AV61ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace);
               AV64ddo_FuncoesAPFAtributos_RegraTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64ddo_FuncoesAPFAtributos_RegraTitleControlIdToReplace", AV64ddo_FuncoesAPFAtributos_RegraTitleControlIdToReplace);
               AV68ddo_FuncoesAPFAtributos_CodeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ddo_FuncoesAPFAtributos_CodeTitleControlIdToReplace", AV68ddo_FuncoesAPFAtributos_CodeTitleControlIdToReplace);
               AV72ddo_FuncoesAPFAtributos_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72ddo_FuncoesAPFAtributos_NomeTitleControlIdToReplace", AV72ddo_FuncoesAPFAtributos_NomeTitleControlIdToReplace);
               AV76ddo_FuncoesAPFAtributos_DescricaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76ddo_FuncoesAPFAtributos_DescricaoTitleControlIdToReplace", AV76ddo_FuncoesAPFAtributos_DescricaoTitleControlIdToReplace);
               AV84Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV11GridState);
               AV31DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31DynamicFiltersIgnoreFirst", AV31DynamicFiltersIgnoreFirst);
               AV30DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18FuncaoAPF_Nome1, AV19FuncaoAPFAtributos_AtributosNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23FuncaoAPF_Nome2, AV24FuncaoAPFAtributos_AtributosNom2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28FuncaoAPF_Nome3, AV29FuncaoAPFAtributos_AtributosNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV35TFFuncaoAPF_Codigo, AV36TFFuncaoAPF_Codigo_To, AV39TFFuncaoAPF_Nome, AV40TFFuncaoAPF_Nome_Sel, AV43TFFuncaoAPFAtributos_AtributosCod, AV44TFFuncaoAPFAtributos_AtributosCod_To, AV47TFFuncaoAPFAtributos_AtributosNom, AV48TFFuncaoAPFAtributos_AtributosNom_Sel, AV51TFFuncaoAPFAtributos_FuncaoDadosCod, AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To, AV55TFFuncaoAPFAtributos_AtrTabelaCod, AV56TFFuncaoAPFAtributos_AtrTabelaCod_To, AV59TFFuncaoAPFAtributos_AtrTabelaNom, AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel, AV63TFFuncoesAPFAtributos_Regra_Sel, AV66TFFuncoesAPFAtributos_Code, AV67TFFuncoesAPFAtributos_Code_Sel, AV70TFFuncoesAPFAtributos_Nome, AV71TFFuncoesAPFAtributos_Nome_Sel, AV74TFFuncoesAPFAtributos_Descricao, AV75TFFuncoesAPFAtributos_Descricao_Sel, AV37ddo_FuncaoAPF_CodigoTitleControlIdToReplace, AV41ddo_FuncaoAPF_NomeTitleControlIdToReplace, AV45ddo_FuncaoAPFAtributos_AtributosCodTitleControlIdToReplace, AV49ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace, AV53ddo_FuncaoAPFAtributos_FuncaoDadosCodTitleControlIdToReplace, AV57ddo_FuncaoAPFAtributos_AtrTabelaCodTitleControlIdToReplace, AV61ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace, AV64ddo_FuncoesAPFAtributos_RegraTitleControlIdToReplace, AV68ddo_FuncoesAPFAtributos_CodeTitleControlIdToReplace, AV72ddo_FuncoesAPFAtributos_NomeTitleControlIdToReplace, AV76ddo_FuncoesAPFAtributos_DescricaoTitleControlIdToReplace, AV84Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutFuncaoAPF_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutFuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutFuncaoAPF_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV8InOutFuncaoAPFAtributos_AtributosCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutFuncaoAPFAtributos_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8InOutFuncaoAPFAtributos_AtributosCod), 6, 0)));
                  AV9InOutFuncaoAPF_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9InOutFuncaoAPF_Nome", AV9InOutFuncaoAPF_Nome);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PA9N2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV84Pgmname = "PromptFuncoesAPFAtributos";
               context.Gx_err = 0;
               WS9N2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WE9N2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117385125");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptfuncoesapfatributos.aspx") + "?" + UrlEncode("" +AV7InOutFuncaoAPF_Codigo) + "," + UrlEncode("" +AV8InOutFuncaoAPFAtributos_AtributosCod) + "," + UrlEncode(StringUtil.RTrim(AV9InOutFuncaoAPF_Nome))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV15OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV16DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAOAPF_NOME1", AV18FuncaoAPF_Nome1);
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1", StringUtil.RTrim( AV19FuncaoAPFAtributos_AtributosNom1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV21DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAOAPF_NOME2", AV23FuncaoAPF_Nome2);
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2", StringUtil.RTrim( AV24FuncaoAPFAtributos_AtributosNom2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV26DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV27DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAOAPF_NOME3", AV28FuncaoAPF_Nome3);
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM3", StringUtil.RTrim( AV29FuncaoAPFAtributos_AtributosNom3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV20DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV25DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAOAPF_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35TFFuncaoAPF_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAOAPF_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36TFFuncaoAPF_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAOAPF_NOME", AV39TFFuncaoAPF_Nome);
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAOAPF_NOME_SEL", AV40TFFuncaoAPF_Nome_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV43TFFuncaoAPFAtributos_AtributosCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV44TFFuncaoAPFAtributos_AtributosCod_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM", StringUtil.RTrim( AV47TFFuncaoAPFAtributos_AtributosNom));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL", StringUtil.RTrim( AV48TFFuncaoAPFAtributos_AtributosNom_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV51TFFuncaoAPFAtributos_FuncaoDadosCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV55TFFuncaoAPFAtributos_AtrTabelaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV56TFFuncaoAPFAtributos_AtrTabelaCod_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM", StringUtil.RTrim( AV59TFFuncaoAPFAtributos_AtrTabelaNom));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL", StringUtil.RTrim( AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCOESAPFATRIBUTOS_REGRA_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV63TFFuncoesAPFAtributos_Regra_Sel), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCOESAPFATRIBUTOS_CODE", AV66TFFuncoesAPFAtributos_Code);
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCOESAPFATRIBUTOS_CODE_SEL", AV67TFFuncoesAPFAtributos_Code_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCOESAPFATRIBUTOS_NOME", StringUtil.RTrim( AV70TFFuncoesAPFAtributos_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCOESAPFATRIBUTOS_NOME_SEL", StringUtil.RTrim( AV71TFFuncoesAPFAtributos_Nome_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCOESAPFATRIBUTOS_DESCRICAO", AV74TFFuncoesAPFAtributos_Descricao);
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCOESAPFATRIBUTOS_DESCRICAO_SEL", AV75TFFuncoesAPFAtributos_Descricao_Sel);
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_83", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_83), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV79GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV80GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV77DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV77DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCAOAPF_CODIGOTITLEFILTERDATA", AV34FuncaoAPF_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCAOAPF_CODIGOTITLEFILTERDATA", AV34FuncaoAPF_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCAOAPF_NOMETITLEFILTERDATA", AV38FuncaoAPF_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCAOAPF_NOMETITLEFILTERDATA", AV38FuncaoAPF_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCAOAPFATRIBUTOS_ATRIBUTOSCODTITLEFILTERDATA", AV42FuncaoAPFAtributos_AtributosCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCAOAPFATRIBUTOS_ATRIBUTOSCODTITLEFILTERDATA", AV42FuncaoAPFAtributos_AtributosCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOMTITLEFILTERDATA", AV46FuncaoAPFAtributos_AtributosNomTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOMTITLEFILTERDATA", AV46FuncaoAPFAtributos_AtributosNomTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCAOAPFATRIBUTOS_FUNCAODADOSCODTITLEFILTERDATA", AV50FuncaoAPFAtributos_FuncaoDadosCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCAOAPFATRIBUTOS_FUNCAODADOSCODTITLEFILTERDATA", AV50FuncaoAPFAtributos_FuncaoDadosCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCAOAPFATRIBUTOS_ATRTABELACODTITLEFILTERDATA", AV54FuncaoAPFAtributos_AtrTabelaCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCAOAPFATRIBUTOS_ATRTABELACODTITLEFILTERDATA", AV54FuncaoAPFAtributos_AtrTabelaCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCAOAPFATRIBUTOS_ATRTABELANOMTITLEFILTERDATA", AV58FuncaoAPFAtributos_AtrTabelaNomTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCAOAPFATRIBUTOS_ATRTABELANOMTITLEFILTERDATA", AV58FuncaoAPFAtributos_AtrTabelaNomTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCOESAPFATRIBUTOS_REGRATITLEFILTERDATA", AV62FuncoesAPFAtributos_RegraTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCOESAPFATRIBUTOS_REGRATITLEFILTERDATA", AV62FuncoesAPFAtributos_RegraTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCOESAPFATRIBUTOS_CODETITLEFILTERDATA", AV65FuncoesAPFAtributos_CodeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCOESAPFATRIBUTOS_CODETITLEFILTERDATA", AV65FuncoesAPFAtributos_CodeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCOESAPFATRIBUTOS_NOMETITLEFILTERDATA", AV69FuncoesAPFAtributos_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCOESAPFATRIBUTOS_NOMETITLEFILTERDATA", AV69FuncoesAPFAtributos_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCOESAPFATRIBUTOS_DESCRICAOTITLEFILTERDATA", AV73FuncoesAPFAtributos_DescricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCOESAPFATRIBUTOS_DESCRICAOTITLEFILTERDATA", AV73FuncoesAPFAtributos_DescricaoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV84Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV11GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV11GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV31DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV30DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTFUNCAOAPF_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutFuncaoAPF_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8InOutFuncaoAPFAtributos_AtributosCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTFUNCAOAPF_NOME", AV9InOutFuncaoAPF_Nome);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_CODIGO_Caption", StringUtil.RTrim( Ddo_funcaoapf_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_CODIGO_Tooltip", StringUtil.RTrim( Ddo_funcaoapf_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_CODIGO_Cls", StringUtil.RTrim( Ddo_funcaoapf_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_funcaoapf_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_funcaoapf_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaoapf_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaoapf_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_funcaoapf_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaoapf_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_funcaoapf_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_funcaoapf_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_CODIGO_Filtertype", StringUtil.RTrim( Ddo_funcaoapf_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_funcaoapf_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_funcaoapf_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_CODIGO_Sortasc", StringUtil.RTrim( Ddo_funcaoapf_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_funcaoapf_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_funcaoapf_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_funcaoapf_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_funcaoapf_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_funcaoapf_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Caption", StringUtil.RTrim( Ddo_funcaoapf_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Tooltip", StringUtil.RTrim( Ddo_funcaoapf_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Cls", StringUtil.RTrim( Ddo_funcaoapf_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_funcaoapf_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_funcaoapf_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaoapf_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaoapf_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_funcaoapf_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaoapf_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Sortedstatus", StringUtil.RTrim( Ddo_funcaoapf_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Includefilter", StringUtil.BoolToStr( Ddo_funcaoapf_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Filtertype", StringUtil.RTrim( Ddo_funcaoapf_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_funcaoapf_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_funcaoapf_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Datalisttype", StringUtil.RTrim( Ddo_funcaoapf_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Datalistproc", StringUtil.RTrim( Ddo_funcaoapf_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_funcaoapf_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Sortasc", StringUtil.RTrim( Ddo_funcaoapf_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Sortdsc", StringUtil.RTrim( Ddo_funcaoapf_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Loadingdata", StringUtil.RTrim( Ddo_funcaoapf_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Cleanfilter", StringUtil.RTrim( Ddo_funcaoapf_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Noresultsfound", StringUtil.RTrim( Ddo_funcaoapf_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_funcaoapf_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_Caption", StringUtil.RTrim( Ddo_funcaoapfatributos_atributoscod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_Tooltip", StringUtil.RTrim( Ddo_funcaoapfatributos_atributoscod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_Cls", StringUtil.RTrim( Ddo_funcaoapfatributos_atributoscod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_Filteredtext_set", StringUtil.RTrim( Ddo_funcaoapfatributos_atributoscod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_Filteredtextto_set", StringUtil.RTrim( Ddo_funcaoapfatributos_atributoscod_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaoapfatributos_atributoscod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaoapfatributos_atributoscod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_Includesortasc", StringUtil.BoolToStr( Ddo_funcaoapfatributos_atributoscod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaoapfatributos_atributoscod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_Sortedstatus", StringUtil.RTrim( Ddo_funcaoapfatributos_atributoscod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_Includefilter", StringUtil.BoolToStr( Ddo_funcaoapfatributos_atributoscod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_Filtertype", StringUtil.RTrim( Ddo_funcaoapfatributos_atributoscod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_Filterisrange", StringUtil.BoolToStr( Ddo_funcaoapfatributos_atributoscod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_Includedatalist", StringUtil.BoolToStr( Ddo_funcaoapfatributos_atributoscod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_Sortasc", StringUtil.RTrim( Ddo_funcaoapfatributos_atributoscod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_Sortdsc", StringUtil.RTrim( Ddo_funcaoapfatributos_atributoscod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_Cleanfilter", StringUtil.RTrim( Ddo_funcaoapfatributos_atributoscod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_Rangefilterfrom", StringUtil.RTrim( Ddo_funcaoapfatributos_atributoscod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_Rangefilterto", StringUtil.RTrim( Ddo_funcaoapfatributos_atributoscod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_Searchbuttontext", StringUtil.RTrim( Ddo_funcaoapfatributos_atributoscod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Caption", StringUtil.RTrim( Ddo_funcaoapfatributos_atributosnom_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Tooltip", StringUtil.RTrim( Ddo_funcaoapfatributos_atributosnom_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Cls", StringUtil.RTrim( Ddo_funcaoapfatributos_atributosnom_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Filteredtext_set", StringUtil.RTrim( Ddo_funcaoapfatributos_atributosnom_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Selectedvalue_set", StringUtil.RTrim( Ddo_funcaoapfatributos_atributosnom_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaoapfatributos_atributosnom_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaoapfatributos_atributosnom_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Includesortasc", StringUtil.BoolToStr( Ddo_funcaoapfatributos_atributosnom_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaoapfatributos_atributosnom_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Sortedstatus", StringUtil.RTrim( Ddo_funcaoapfatributos_atributosnom_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Includefilter", StringUtil.BoolToStr( Ddo_funcaoapfatributos_atributosnom_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Filtertype", StringUtil.RTrim( Ddo_funcaoapfatributos_atributosnom_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Filterisrange", StringUtil.BoolToStr( Ddo_funcaoapfatributos_atributosnom_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Includedatalist", StringUtil.BoolToStr( Ddo_funcaoapfatributos_atributosnom_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Datalisttype", StringUtil.RTrim( Ddo_funcaoapfatributos_atributosnom_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Datalistproc", StringUtil.RTrim( Ddo_funcaoapfatributos_atributosnom_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_funcaoapfatributos_atributosnom_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Sortasc", StringUtil.RTrim( Ddo_funcaoapfatributos_atributosnom_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Sortdsc", StringUtil.RTrim( Ddo_funcaoapfatributos_atributosnom_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Loadingdata", StringUtil.RTrim( Ddo_funcaoapfatributos_atributosnom_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Cleanfilter", StringUtil.RTrim( Ddo_funcaoapfatributos_atributosnom_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Noresultsfound", StringUtil.RTrim( Ddo_funcaoapfatributos_atributosnom_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Searchbuttontext", StringUtil.RTrim( Ddo_funcaoapfatributos_atributosnom_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_Caption", StringUtil.RTrim( Ddo_funcaoapfatributos_funcaodadoscod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_Tooltip", StringUtil.RTrim( Ddo_funcaoapfatributos_funcaodadoscod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_Cls", StringUtil.RTrim( Ddo_funcaoapfatributos_funcaodadoscod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_Filteredtext_set", StringUtil.RTrim( Ddo_funcaoapfatributos_funcaodadoscod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_Filteredtextto_set", StringUtil.RTrim( Ddo_funcaoapfatributos_funcaodadoscod_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaoapfatributos_funcaodadoscod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaoapfatributos_funcaodadoscod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_Includesortasc", StringUtil.BoolToStr( Ddo_funcaoapfatributos_funcaodadoscod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaoapfatributos_funcaodadoscod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_Sortedstatus", StringUtil.RTrim( Ddo_funcaoapfatributos_funcaodadoscod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_Includefilter", StringUtil.BoolToStr( Ddo_funcaoapfatributos_funcaodadoscod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_Filtertype", StringUtil.RTrim( Ddo_funcaoapfatributos_funcaodadoscod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_Filterisrange", StringUtil.BoolToStr( Ddo_funcaoapfatributos_funcaodadoscod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_Includedatalist", StringUtil.BoolToStr( Ddo_funcaoapfatributos_funcaodadoscod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_Sortasc", StringUtil.RTrim( Ddo_funcaoapfatributos_funcaodadoscod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_Sortdsc", StringUtil.RTrim( Ddo_funcaoapfatributos_funcaodadoscod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_Cleanfilter", StringUtil.RTrim( Ddo_funcaoapfatributos_funcaodadoscod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_Rangefilterfrom", StringUtil.RTrim( Ddo_funcaoapfatributos_funcaodadoscod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_Rangefilterto", StringUtil.RTrim( Ddo_funcaoapfatributos_funcaodadoscod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_Searchbuttontext", StringUtil.RTrim( Ddo_funcaoapfatributos_funcaodadoscod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD_Caption", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelacod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD_Tooltip", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelacod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD_Cls", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelacod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD_Filteredtext_set", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelacod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD_Filteredtextto_set", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelacod_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelacod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelacod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD_Includesortasc", StringUtil.BoolToStr( Ddo_funcaoapfatributos_atrtabelacod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaoapfatributos_atrtabelacod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD_Sortedstatus", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelacod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD_Includefilter", StringUtil.BoolToStr( Ddo_funcaoapfatributos_atrtabelacod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD_Filtertype", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelacod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD_Filterisrange", StringUtil.BoolToStr( Ddo_funcaoapfatributos_atrtabelacod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD_Includedatalist", StringUtil.BoolToStr( Ddo_funcaoapfatributos_atrtabelacod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD_Sortasc", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelacod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD_Sortdsc", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelacod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD_Cleanfilter", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelacod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD_Rangefilterfrom", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelacod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD_Rangefilterto", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelacod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD_Searchbuttontext", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelacod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Caption", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelanom_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Tooltip", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelanom_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Cls", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelanom_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Filteredtext_set", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelanom_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Selectedvalue_set", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelanom_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelanom_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelanom_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Includesortasc", StringUtil.BoolToStr( Ddo_funcaoapfatributos_atrtabelanom_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaoapfatributos_atrtabelanom_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Sortedstatus", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelanom_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Includefilter", StringUtil.BoolToStr( Ddo_funcaoapfatributos_atrtabelanom_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Filtertype", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelanom_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Filterisrange", StringUtil.BoolToStr( Ddo_funcaoapfatributos_atrtabelanom_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Includedatalist", StringUtil.BoolToStr( Ddo_funcaoapfatributos_atrtabelanom_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Datalisttype", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelanom_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Datalistproc", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelanom_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_funcaoapfatributos_atrtabelanom_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Sortasc", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelanom_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Sortdsc", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelanom_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Loadingdata", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelanom_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Cleanfilter", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelanom_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Noresultsfound", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelanom_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Searchbuttontext", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelanom_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_REGRA_Caption", StringUtil.RTrim( Ddo_funcoesapfatributos_regra_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_REGRA_Tooltip", StringUtil.RTrim( Ddo_funcoesapfatributos_regra_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_REGRA_Cls", StringUtil.RTrim( Ddo_funcoesapfatributos_regra_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_REGRA_Selectedvalue_set", StringUtil.RTrim( Ddo_funcoesapfatributos_regra_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_REGRA_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcoesapfatributos_regra_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_REGRA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcoesapfatributos_regra_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_REGRA_Includesortasc", StringUtil.BoolToStr( Ddo_funcoesapfatributos_regra_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_REGRA_Includesortdsc", StringUtil.BoolToStr( Ddo_funcoesapfatributos_regra_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_REGRA_Sortedstatus", StringUtil.RTrim( Ddo_funcoesapfatributos_regra_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_REGRA_Includefilter", StringUtil.BoolToStr( Ddo_funcoesapfatributos_regra_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_REGRA_Includedatalist", StringUtil.BoolToStr( Ddo_funcoesapfatributos_regra_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_REGRA_Datalisttype", StringUtil.RTrim( Ddo_funcoesapfatributos_regra_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_REGRA_Datalistfixedvalues", StringUtil.RTrim( Ddo_funcoesapfatributos_regra_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_REGRA_Sortasc", StringUtil.RTrim( Ddo_funcoesapfatributos_regra_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_REGRA_Sortdsc", StringUtil.RTrim( Ddo_funcoesapfatributos_regra_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_REGRA_Cleanfilter", StringUtil.RTrim( Ddo_funcoesapfatributos_regra_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_REGRA_Searchbuttontext", StringUtil.RTrim( Ddo_funcoesapfatributos_regra_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_CODE_Caption", StringUtil.RTrim( Ddo_funcoesapfatributos_code_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_CODE_Tooltip", StringUtil.RTrim( Ddo_funcoesapfatributos_code_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_CODE_Cls", StringUtil.RTrim( Ddo_funcoesapfatributos_code_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_CODE_Filteredtext_set", StringUtil.RTrim( Ddo_funcoesapfatributos_code_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_CODE_Selectedvalue_set", StringUtil.RTrim( Ddo_funcoesapfatributos_code_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_CODE_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcoesapfatributos_code_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_CODE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcoesapfatributos_code_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_CODE_Includesortasc", StringUtil.BoolToStr( Ddo_funcoesapfatributos_code_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_CODE_Includesortdsc", StringUtil.BoolToStr( Ddo_funcoesapfatributos_code_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_CODE_Sortedstatus", StringUtil.RTrim( Ddo_funcoesapfatributos_code_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_CODE_Includefilter", StringUtil.BoolToStr( Ddo_funcoesapfatributos_code_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_CODE_Filtertype", StringUtil.RTrim( Ddo_funcoesapfatributos_code_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_CODE_Filterisrange", StringUtil.BoolToStr( Ddo_funcoesapfatributos_code_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_CODE_Includedatalist", StringUtil.BoolToStr( Ddo_funcoesapfatributos_code_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_CODE_Datalisttype", StringUtil.RTrim( Ddo_funcoesapfatributos_code_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_CODE_Datalistproc", StringUtil.RTrim( Ddo_funcoesapfatributos_code_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_CODE_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_funcoesapfatributos_code_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_CODE_Sortasc", StringUtil.RTrim( Ddo_funcoesapfatributos_code_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_CODE_Sortdsc", StringUtil.RTrim( Ddo_funcoesapfatributos_code_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_CODE_Loadingdata", StringUtil.RTrim( Ddo_funcoesapfatributos_code_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_CODE_Cleanfilter", StringUtil.RTrim( Ddo_funcoesapfatributos_code_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_CODE_Noresultsfound", StringUtil.RTrim( Ddo_funcoesapfatributos_code_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_CODE_Searchbuttontext", StringUtil.RTrim( Ddo_funcoesapfatributos_code_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_NOME_Caption", StringUtil.RTrim( Ddo_funcoesapfatributos_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_NOME_Tooltip", StringUtil.RTrim( Ddo_funcoesapfatributos_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_NOME_Cls", StringUtil.RTrim( Ddo_funcoesapfatributos_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_funcoesapfatributos_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_funcoesapfatributos_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcoesapfatributos_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcoesapfatributos_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_funcoesapfatributos_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_funcoesapfatributos_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_NOME_Sortedstatus", StringUtil.RTrim( Ddo_funcoesapfatributos_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_NOME_Includefilter", StringUtil.BoolToStr( Ddo_funcoesapfatributos_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_NOME_Filtertype", StringUtil.RTrim( Ddo_funcoesapfatributos_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_funcoesapfatributos_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_funcoesapfatributos_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_NOME_Datalisttype", StringUtil.RTrim( Ddo_funcoesapfatributos_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_NOME_Datalistproc", StringUtil.RTrim( Ddo_funcoesapfatributos_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_funcoesapfatributos_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_NOME_Sortasc", StringUtil.RTrim( Ddo_funcoesapfatributos_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_NOME_Sortdsc", StringUtil.RTrim( Ddo_funcoesapfatributos_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_NOME_Loadingdata", StringUtil.RTrim( Ddo_funcoesapfatributos_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_NOME_Cleanfilter", StringUtil.RTrim( Ddo_funcoesapfatributos_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_NOME_Noresultsfound", StringUtil.RTrim( Ddo_funcoesapfatributos_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_funcoesapfatributos_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Caption", StringUtil.RTrim( Ddo_funcoesapfatributos_descricao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_funcoesapfatributos_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Cls", StringUtil.RTrim( Ddo_funcoesapfatributos_descricao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_funcoesapfatributos_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_funcoesapfatributos_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcoesapfatributos_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcoesapfatributos_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_funcoesapfatributos_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_funcoesapfatributos_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_funcoesapfatributos_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_funcoesapfatributos_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_funcoesapfatributos_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_funcoesapfatributos_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_funcoesapfatributos_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_funcoesapfatributos_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_funcoesapfatributos_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_funcoesapfatributos_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_funcoesapfatributos_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_funcoesapfatributos_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_funcoesapfatributos_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_funcoesapfatributos_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_funcoesapfatributos_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_funcoesapfatributos_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_funcaoapf_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_funcaoapf_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_funcaoapf_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Activeeventkey", StringUtil.RTrim( Ddo_funcaoapf_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_funcaoapf_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_funcaoapf_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_Activeeventkey", StringUtil.RTrim( Ddo_funcaoapfatributos_atributoscod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_Filteredtext_get", StringUtil.RTrim( Ddo_funcaoapfatributos_atributoscod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_Filteredtextto_get", StringUtil.RTrim( Ddo_funcaoapfatributos_atributoscod_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Activeeventkey", StringUtil.RTrim( Ddo_funcaoapfatributos_atributosnom_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Filteredtext_get", StringUtil.RTrim( Ddo_funcaoapfatributos_atributosnom_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Selectedvalue_get", StringUtil.RTrim( Ddo_funcaoapfatributos_atributosnom_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_Activeeventkey", StringUtil.RTrim( Ddo_funcaoapfatributos_funcaodadoscod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_Filteredtext_get", StringUtil.RTrim( Ddo_funcaoapfatributos_funcaodadoscod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_Filteredtextto_get", StringUtil.RTrim( Ddo_funcaoapfatributos_funcaodadoscod_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD_Activeeventkey", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelacod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD_Filteredtext_get", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelacod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD_Filteredtextto_get", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelacod_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Activeeventkey", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelanom_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Filteredtext_get", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelanom_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Selectedvalue_get", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelanom_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_REGRA_Activeeventkey", StringUtil.RTrim( Ddo_funcoesapfatributos_regra_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_REGRA_Selectedvalue_get", StringUtil.RTrim( Ddo_funcoesapfatributos_regra_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_CODE_Activeeventkey", StringUtil.RTrim( Ddo_funcoesapfatributos_code_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_CODE_Filteredtext_get", StringUtil.RTrim( Ddo_funcoesapfatributos_code_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_CODE_Selectedvalue_get", StringUtil.RTrim( Ddo_funcoesapfatributos_code_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_NOME_Activeeventkey", StringUtil.RTrim( Ddo_funcoesapfatributos_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_funcoesapfatributos_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_funcoesapfatributos_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_funcoesapfatributos_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_funcoesapfatributos_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_funcoesapfatributos_descricao_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm9N2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptFuncoesAPFAtributos" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Atributos das Fun��es de APF" ;
      }

      protected void WB9N0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_9N2( true) ;
         }
         else
         {
            wb_table1_2_9N2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_9N2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV20DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(100, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,100);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV25DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(101, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,101);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaoapf_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35TFFuncaoAPF_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV35TFFuncaoAPF_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,102);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaoapf_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaoapf_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptFuncoesAPFAtributos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaoapf_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36TFFuncaoAPF_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV36TFFuncaoAPF_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,103);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaoapf_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaoapf_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptFuncoesAPFAtributos.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTffuncaoapf_nome_Internalname, AV39TFFuncaoAPF_Nome, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,104);\"", 0, edtavTffuncaoapf_nome_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_PromptFuncoesAPFAtributos.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTffuncaoapf_nome_sel_Internalname, AV40TFFuncaoAPF_Nome_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,105);\"", 0, edtavTffuncaoapf_nome_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_PromptFuncoesAPFAtributos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaoapfatributos_atributoscod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV43TFFuncaoAPFAtributos_AtributosCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV43TFFuncaoAPFAtributos_AtributosCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,106);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaoapfatributos_atributoscod_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaoapfatributos_atributoscod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptFuncoesAPFAtributos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaoapfatributos_atributoscod_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV44TFFuncaoAPFAtributos_AtributosCod_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV44TFFuncaoAPFAtributos_AtributosCod_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,107);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaoapfatributos_atributoscod_to_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaoapfatributos_atributoscod_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptFuncoesAPFAtributos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaoapfatributos_atributosnom_Internalname, StringUtil.RTrim( AV47TFFuncaoAPFAtributos_AtributosNom), StringUtil.RTrim( context.localUtil.Format( AV47TFFuncaoAPFAtributos_AtributosNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,108);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaoapfatributos_atributosnom_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaoapfatributos_atributosnom_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptFuncoesAPFAtributos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaoapfatributos_atributosnom_sel_Internalname, StringUtil.RTrim( AV48TFFuncaoAPFAtributos_AtributosNom_Sel), StringUtil.RTrim( context.localUtil.Format( AV48TFFuncaoAPFAtributos_AtributosNom_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,109);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaoapfatributos_atributosnom_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaoapfatributos_atributosnom_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptFuncoesAPFAtributos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaoapfatributos_funcaodadoscod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV51TFFuncaoAPFAtributos_FuncaoDadosCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV51TFFuncaoAPFAtributos_FuncaoDadosCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,110);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaoapfatributos_funcaodadoscod_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaoapfatributos_funcaodadoscod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptFuncoesAPFAtributos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaoapfatributos_funcaodadoscod_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,111);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaoapfatributos_funcaodadoscod_to_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaoapfatributos_funcaodadoscod_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptFuncoesAPFAtributos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaoapfatributos_atrtabelacod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV55TFFuncaoAPFAtributos_AtrTabelaCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV55TFFuncaoAPFAtributos_AtrTabelaCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,112);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaoapfatributos_atrtabelacod_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaoapfatributos_atrtabelacod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptFuncoesAPFAtributos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaoapfatributos_atrtabelacod_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV56TFFuncaoAPFAtributos_AtrTabelaCod_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV56TFFuncaoAPFAtributos_AtrTabelaCod_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,113);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaoapfatributos_atrtabelacod_to_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaoapfatributos_atrtabelacod_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptFuncoesAPFAtributos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaoapfatributos_atrtabelanom_Internalname, StringUtil.RTrim( AV59TFFuncaoAPFAtributos_AtrTabelaNom), StringUtil.RTrim( context.localUtil.Format( AV59TFFuncaoAPFAtributos_AtrTabelaNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,114);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaoapfatributos_atrtabelanom_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaoapfatributos_atrtabelanom_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptFuncoesAPFAtributos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaoapfatributos_atrtabelanom_sel_Internalname, StringUtil.RTrim( AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel), StringUtil.RTrim( context.localUtil.Format( AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,115);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaoapfatributos_atrtabelanom_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaoapfatributos_atrtabelanom_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptFuncoesAPFAtributos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncoesapfatributos_regra_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV63TFFuncoesAPFAtributos_Regra_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV63TFFuncoesAPFAtributos_Regra_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,116);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncoesapfatributos_regra_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncoesapfatributos_regra_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptFuncoesAPFAtributos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncoesapfatributos_code_Internalname, AV66TFFuncoesAPFAtributos_Code, StringUtil.RTrim( context.localUtil.Format( AV66TFFuncoesAPFAtributos_Code, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,117);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncoesapfatributos_code_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncoesapfatributos_code_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptFuncoesAPFAtributos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncoesapfatributos_code_sel_Internalname, AV67TFFuncoesAPFAtributos_Code_Sel, StringUtil.RTrim( context.localUtil.Format( AV67TFFuncoesAPFAtributos_Code_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,118);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncoesapfatributos_code_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncoesapfatributos_code_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptFuncoesAPFAtributos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncoesapfatributos_nome_Internalname, StringUtil.RTrim( AV70TFFuncoesAPFAtributos_Nome), StringUtil.RTrim( context.localUtil.Format( AV70TFFuncoesAPFAtributos_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,119);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncoesapfatributos_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncoesapfatributos_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptFuncoesAPFAtributos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 120,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncoesapfatributos_nome_sel_Internalname, StringUtil.RTrim( AV71TFFuncoesAPFAtributos_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV71TFFuncoesAPFAtributos_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,120);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncoesapfatributos_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncoesapfatributos_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptFuncoesAPFAtributos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 121,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncoesapfatributos_descricao_Internalname, AV74TFFuncoesAPFAtributos_Descricao, StringUtil.RTrim( context.localUtil.Format( AV74TFFuncoesAPFAtributos_Descricao, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,121);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncoesapfatributos_descricao_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncoesapfatributos_descricao_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptFuncoesAPFAtributos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 122,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncoesapfatributos_descricao_sel_Internalname, AV75TFFuncoesAPFAtributos_Descricao_Sel, StringUtil.RTrim( context.localUtil.Format( AV75TFFuncoesAPFAtributos_Descricao_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,122);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncoesapfatributos_descricao_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncoesapfatributos_descricao_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptFuncoesAPFAtributos.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCAOAPF_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 124,'',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaoapf_codigotitlecontrolidtoreplace_Internalname, AV37ddo_FuncaoAPF_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,124);\"", 0, edtavDdo_funcaoapf_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptFuncoesAPFAtributos.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCAOAPF_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 126,'',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaoapf_nometitlecontrolidtoreplace_Internalname, AV41ddo_FuncaoAPF_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,126);\"", 0, edtavDdo_funcaoapf_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptFuncoesAPFAtributos.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 128,'',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaoapfatributos_atributoscodtitlecontrolidtoreplace_Internalname, AV45ddo_FuncaoAPFAtributos_AtributosCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,128);\"", 0, edtavDdo_funcaoapfatributos_atributoscodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptFuncoesAPFAtributos.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 130,'',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaoapfatributos_atributosnomtitlecontrolidtoreplace_Internalname, AV49ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,130);\"", 0, edtavDdo_funcaoapfatributos_atributosnomtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptFuncoesAPFAtributos.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 132,'',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaoapfatributos_funcaodadoscodtitlecontrolidtoreplace_Internalname, AV53ddo_FuncaoAPFAtributos_FuncaoDadosCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,132);\"", 0, edtavDdo_funcaoapfatributos_funcaodadoscodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptFuncoesAPFAtributos.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELACODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 134,'',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaoapfatributos_atrtabelacodtitlecontrolidtoreplace_Internalname, AV57ddo_FuncaoAPFAtributos_AtrTabelaCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,134);\"", 0, edtavDdo_funcaoapfatributos_atrtabelacodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptFuncoesAPFAtributos.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 136,'',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaoapfatributos_atrtabelanomtitlecontrolidtoreplace_Internalname, AV61ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,136);\"", 0, edtavDdo_funcaoapfatributos_atrtabelanomtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptFuncoesAPFAtributos.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCOESAPFATRIBUTOS_REGRAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 138,'',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcoesapfatributos_regratitlecontrolidtoreplace_Internalname, AV64ddo_FuncoesAPFAtributos_RegraTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,138);\"", 0, edtavDdo_funcoesapfatributos_regratitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptFuncoesAPFAtributos.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCOESAPFATRIBUTOS_CODEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 140,'',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcoesapfatributos_codetitlecontrolidtoreplace_Internalname, AV68ddo_FuncoesAPFAtributos_CodeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,140);\"", 0, edtavDdo_funcoesapfatributos_codetitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptFuncoesAPFAtributos.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCOESAPFATRIBUTOS_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 142,'',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcoesapfatributos_nometitlecontrolidtoreplace_Internalname, AV72ddo_FuncoesAPFAtributos_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,142);\"", 0, edtavDdo_funcoesapfatributos_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptFuncoesAPFAtributos.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCOESAPFATRIBUTOS_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 144,'',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcoesapfatributos_descricaotitlecontrolidtoreplace_Internalname, AV76ddo_FuncoesAPFAtributos_DescricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,144);\"", 0, edtavDdo_funcoesapfatributos_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptFuncoesAPFAtributos.htm");
         }
         wbLoad = true;
      }

      protected void START9N2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Atributos das Fun��es de APF", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP9N0( ) ;
      }

      protected void WS9N2( )
      {
         START9N2( ) ;
         EVT9N2( ) ;
      }

      protected void EVT9N2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E119N2 */
                           E119N2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAOAPF_CODIGO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E129N2 */
                           E129N2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAOAPF_NOME.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E139N2 */
                           E139N2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E149N2 */
                           E149N2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E159N2 */
                           E159N2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E169N2 */
                           E169N2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E179N2 */
                           E179N2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E189N2 */
                           E189N2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCOESAPFATRIBUTOS_REGRA.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E199N2 */
                           E199N2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCOESAPFATRIBUTOS_CODE.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E209N2 */
                           E209N2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCOESAPFATRIBUTOS_NOME.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E219N2 */
                           E219N2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E229N2 */
                           E229N2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E239N2 */
                           E239N2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E249N2 */
                           E249N2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E259N2 */
                           E259N2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E269N2 */
                           E269N2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E279N2 */
                           E279N2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E289N2 */
                           E289N2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E299N2 */
                           E299N2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E309N2 */
                           E309N2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E319N2 */
                           E319N2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E329N2 */
                           E329N2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_83_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_83_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_83_idx), 4, 0)), 4, "0");
                           SubsflControlProps_832( ) ;
                           AV32Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV32Select)) ? AV83Select_GXI : context.convertURL( context.PathToRelativeUrl( AV32Select))));
                           A165FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( edtFuncaoAPF_Codigo_Internalname), ",", "."));
                           A166FuncaoAPF_Nome = cgiGet( edtFuncaoAPF_Nome_Internalname);
                           A364FuncaoAPFAtributos_AtributosCod = (int)(context.localUtil.CToN( cgiGet( edtFuncaoAPFAtributos_AtributosCod_Internalname), ",", "."));
                           A365FuncaoAPFAtributos_AtributosNom = StringUtil.Upper( cgiGet( edtFuncaoAPFAtributos_AtributosNom_Internalname));
                           n365FuncaoAPFAtributos_AtributosNom = false;
                           A378FuncaoAPFAtributos_FuncaoDadosCod = (int)(context.localUtil.CToN( cgiGet( edtFuncaoAPFAtributos_FuncaoDadosCod_Internalname), ",", "."));
                           n378FuncaoAPFAtributos_FuncaoDadosCod = false;
                           A366FuncaoAPFAtributos_AtrTabelaCod = (int)(context.localUtil.CToN( cgiGet( edtFuncaoAPFAtributos_AtrTabelaCod_Internalname), ",", "."));
                           n366FuncaoAPFAtributos_AtrTabelaCod = false;
                           A367FuncaoAPFAtributos_AtrTabelaNom = StringUtil.Upper( cgiGet( edtFuncaoAPFAtributos_AtrTabelaNom_Internalname));
                           n367FuncaoAPFAtributos_AtrTabelaNom = false;
                           A389FuncoesAPFAtributos_Regra = StringUtil.StrToBool( cgiGet( chkFuncoesAPFAtributos_Regra_Internalname));
                           n389FuncoesAPFAtributos_Regra = false;
                           A383FuncoesAPFAtributos_Code = cgiGet( edtFuncoesAPFAtributos_Code_Internalname);
                           n383FuncoesAPFAtributos_Code = false;
                           A384FuncoesAPFAtributos_Nome = StringUtil.Upper( cgiGet( edtFuncoesAPFAtributos_Nome_Internalname));
                           n384FuncoesAPFAtributos_Nome = false;
                           A385FuncoesAPFAtributos_Descricao = StringUtil.Upper( cgiGet( edtFuncoesAPFAtributos_Descricao_Internalname));
                           n385FuncoesAPFAtributos_Descricao = false;
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E339N2 */
                                 E339N2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E349N2 */
                                 E349N2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E359N2 */
                                 E359N2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV15OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV17DynamicFiltersOperator1 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Funcaoapf_nome1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_NOME1"), AV18FuncaoAPF_Nome1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Funcaoapfatributos_atributosnom1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1"), AV19FuncaoAPFAtributos_AtributosNom1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV22DynamicFiltersOperator2 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Funcaoapf_nome2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_NOME2"), AV23FuncaoAPF_Nome2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Funcaoapfatributos_atributosnom2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2"), AV24FuncaoAPFAtributos_AtributosNom2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV26DynamicFiltersSelector3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV27DynamicFiltersOperator3 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Funcaoapf_nome3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_NOME3"), AV28FuncaoAPF_Nome3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Funcaoapfatributos_atributosnom3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM3"), AV29FuncaoAPFAtributos_AtributosNom3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV25DynamicFiltersEnabled3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tffuncaoapf_codigo Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCAOAPF_CODIGO"), ",", ".") != Convert.ToDecimal( AV35TFFuncaoAPF_Codigo )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tffuncaoapf_codigo_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCAOAPF_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV36TFFuncaoAPF_Codigo_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tffuncaoapf_nome Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAOAPF_NOME"), AV39TFFuncaoAPF_Nome) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tffuncaoapf_nome_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAOAPF_NOME_SEL"), AV40TFFuncaoAPF_Nome_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tffuncaoapfatributos_atributoscod Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD"), ",", ".") != Convert.ToDecimal( AV43TFFuncaoAPFAtributos_AtributosCod )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tffuncaoapfatributos_atributoscod_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_TO"), ",", ".") != Convert.ToDecimal( AV44TFFuncaoAPFAtributos_AtributosCod_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tffuncaoapfatributos_atributosnom Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM"), AV47TFFuncaoAPFAtributos_AtributosNom) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tffuncaoapfatributos_atributosnom_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL"), AV48TFFuncaoAPFAtributos_AtributosNom_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tffuncaoapfatributos_funcaodadoscod Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD"), ",", ".") != Convert.ToDecimal( AV51TFFuncaoAPFAtributos_FuncaoDadosCod )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tffuncaoapfatributos_funcaodadoscod_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_TO"), ",", ".") != Convert.ToDecimal( AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tffuncaoapfatributos_atrtabelacod Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD"), ",", ".") != Convert.ToDecimal( AV55TFFuncaoAPFAtributos_AtrTabelaCod )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tffuncaoapfatributos_atrtabelacod_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD_TO"), ",", ".") != Convert.ToDecimal( AV56TFFuncaoAPFAtributos_AtrTabelaCod_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tffuncaoapfatributos_atrtabelanom Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM"), AV59TFFuncaoAPFAtributos_AtrTabelaNom) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tffuncaoapfatributos_atrtabelanom_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL"), AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tffuncoesapfatributos_regra_sel Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCOESAPFATRIBUTOS_REGRA_SEL"), ",", ".") != Convert.ToDecimal( AV63TFFuncoesAPFAtributos_Regra_Sel )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tffuncoesapfatributos_code Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCOESAPFATRIBUTOS_CODE"), AV66TFFuncoesAPFAtributos_Code) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tffuncoesapfatributos_code_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCOESAPFATRIBUTOS_CODE_SEL"), AV67TFFuncoesAPFAtributos_Code_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tffuncoesapfatributos_nome Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCOESAPFATRIBUTOS_NOME"), AV70TFFuncoesAPFAtributos_Nome) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tffuncoesapfatributos_nome_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCOESAPFATRIBUTOS_NOME_SEL"), AV71TFFuncoesAPFAtributos_Nome_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tffuncoesapfatributos_descricao Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCOESAPFATRIBUTOS_DESCRICAO"), AV74TFFuncoesAPFAtributos_Descricao) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tffuncoesapfatributos_descricao_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCOESAPFATRIBUTOS_DESCRICAO_SEL"), AV75TFFuncoesAPFAtributos_Descricao_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E369N2 */
                                       E369N2 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WE9N2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm9N2( ) ;
            }
         }
      }

      protected void PA9N2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("FUNCAOAPF_NOME", "Fun��o de Transa��o", 0);
            cmbavDynamicfiltersselector1.addItem("FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM", "Atributo", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("FUNCAOAPF_NOME", "Fun��o de Transa��o", 0);
            cmbavDynamicfiltersselector2.addItem("FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM", "Atributo", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("FUNCAOAPF_NOME", "Fun��o de Transa��o", 0);
            cmbavDynamicfiltersselector3.addItem("FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM", "Atributo", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV26DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV26DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV27DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
            }
            GXCCtl = "FUNCOESAPFATRIBUTOS_REGRA_" + sGXsfl_83_idx;
            chkFuncoesAPFAtributos_Regra.Name = GXCCtl;
            chkFuncoesAPFAtributos_Regra.WebTags = "";
            chkFuncoesAPFAtributos_Regra.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkFuncoesAPFAtributos_Regra_Internalname, "TitleCaption", chkFuncoesAPFAtributos_Regra.Caption);
            chkFuncoesAPFAtributos_Regra.CheckedValue = "false";
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_832( ) ;
         while ( nGXsfl_83_idx <= nRC_GXsfl_83 )
         {
            sendrow_832( ) ;
            nGXsfl_83_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_83_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_83_idx+1));
            sGXsfl_83_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_83_idx), 4, 0)), 4, "0");
            SubsflControlProps_832( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV14OrderedBy ,
                                       bool AV15OrderedDsc ,
                                       String AV16DynamicFiltersSelector1 ,
                                       short AV17DynamicFiltersOperator1 ,
                                       String AV18FuncaoAPF_Nome1 ,
                                       String AV19FuncaoAPFAtributos_AtributosNom1 ,
                                       String AV21DynamicFiltersSelector2 ,
                                       short AV22DynamicFiltersOperator2 ,
                                       String AV23FuncaoAPF_Nome2 ,
                                       String AV24FuncaoAPFAtributos_AtributosNom2 ,
                                       String AV26DynamicFiltersSelector3 ,
                                       short AV27DynamicFiltersOperator3 ,
                                       String AV28FuncaoAPF_Nome3 ,
                                       String AV29FuncaoAPFAtributos_AtributosNom3 ,
                                       bool AV20DynamicFiltersEnabled2 ,
                                       bool AV25DynamicFiltersEnabled3 ,
                                       int AV35TFFuncaoAPF_Codigo ,
                                       int AV36TFFuncaoAPF_Codigo_To ,
                                       String AV39TFFuncaoAPF_Nome ,
                                       String AV40TFFuncaoAPF_Nome_Sel ,
                                       int AV43TFFuncaoAPFAtributos_AtributosCod ,
                                       int AV44TFFuncaoAPFAtributos_AtributosCod_To ,
                                       String AV47TFFuncaoAPFAtributos_AtributosNom ,
                                       String AV48TFFuncaoAPFAtributos_AtributosNom_Sel ,
                                       int AV51TFFuncaoAPFAtributos_FuncaoDadosCod ,
                                       int AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To ,
                                       int AV55TFFuncaoAPFAtributos_AtrTabelaCod ,
                                       int AV56TFFuncaoAPFAtributos_AtrTabelaCod_To ,
                                       String AV59TFFuncaoAPFAtributos_AtrTabelaNom ,
                                       String AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel ,
                                       short AV63TFFuncoesAPFAtributos_Regra_Sel ,
                                       String AV66TFFuncoesAPFAtributos_Code ,
                                       String AV67TFFuncoesAPFAtributos_Code_Sel ,
                                       String AV70TFFuncoesAPFAtributos_Nome ,
                                       String AV71TFFuncoesAPFAtributos_Nome_Sel ,
                                       String AV74TFFuncoesAPFAtributos_Descricao ,
                                       String AV75TFFuncoesAPFAtributos_Descricao_Sel ,
                                       String AV37ddo_FuncaoAPF_CodigoTitleControlIdToReplace ,
                                       String AV41ddo_FuncaoAPF_NomeTitleControlIdToReplace ,
                                       String AV45ddo_FuncaoAPFAtributos_AtributosCodTitleControlIdToReplace ,
                                       String AV49ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace ,
                                       String AV53ddo_FuncaoAPFAtributos_FuncaoDadosCodTitleControlIdToReplace ,
                                       String AV57ddo_FuncaoAPFAtributos_AtrTabelaCodTitleControlIdToReplace ,
                                       String AV61ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace ,
                                       String AV64ddo_FuncoesAPFAtributos_RegraTitleControlIdToReplace ,
                                       String AV68ddo_FuncoesAPFAtributos_CodeTitleControlIdToReplace ,
                                       String AV72ddo_FuncoesAPFAtributos_NomeTitleControlIdToReplace ,
                                       String AV76ddo_FuncoesAPFAtributos_DescricaoTitleControlIdToReplace ,
                                       String AV84Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV11GridState ,
                                       bool AV31DynamicFiltersIgnoreFirst ,
                                       bool AV30DynamicFiltersRemoving )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF9N2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A364FuncaoAPFAtributos_AtributosCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A364FuncaoAPFAtributos_AtributosCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A378FuncaoAPFAtributos_FuncaoDadosCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A378FuncaoAPFAtributos_FuncaoDadosCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCOESAPFATRIBUTOS_REGRA", GetSecureSignedToken( "", A389FuncoesAPFAtributos_Regra));
         GxWebStd.gx_hidden_field( context, "FUNCOESAPFATRIBUTOS_REGRA", StringUtil.BoolToStr( A389FuncoesAPFAtributos_Regra));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCOESAPFATRIBUTOS_CODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A383FuncoesAPFAtributos_Code, ""))));
         GxWebStd.gx_hidden_field( context, "FUNCOESAPFATRIBUTOS_CODE", A383FuncoesAPFAtributos_Code);
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCOESAPFATRIBUTOS_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A384FuncoesAPFAtributos_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "FUNCOESAPFATRIBUTOS_NOME", StringUtil.RTrim( A384FuncoesAPFAtributos_Nome));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCOESAPFATRIBUTOS_DESCRICAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A385FuncoesAPFAtributos_Descricao, "@!"))));
         GxWebStd.gx_hidden_field( context, "FUNCOESAPFATRIBUTOS_DESCRICAO", A385FuncoesAPFAtributos_Descricao);
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV26DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV26DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV27DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF9N2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV84Pgmname = "PromptFuncoesAPFAtributos";
         context.Gx_err = 0;
      }

      protected void RF9N2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 83;
         /* Execute user event: E349N2 */
         E349N2 ();
         nGXsfl_83_idx = 1;
         sGXsfl_83_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_83_idx), 4, 0)), 4, "0");
         SubsflControlProps_832( ) ;
         nGXsfl_83_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_832( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV16DynamicFiltersSelector1 ,
                                                 AV17DynamicFiltersOperator1 ,
                                                 AV18FuncaoAPF_Nome1 ,
                                                 AV19FuncaoAPFAtributos_AtributosNom1 ,
                                                 AV20DynamicFiltersEnabled2 ,
                                                 AV21DynamicFiltersSelector2 ,
                                                 AV22DynamicFiltersOperator2 ,
                                                 AV23FuncaoAPF_Nome2 ,
                                                 AV24FuncaoAPFAtributos_AtributosNom2 ,
                                                 AV25DynamicFiltersEnabled3 ,
                                                 AV26DynamicFiltersSelector3 ,
                                                 AV27DynamicFiltersOperator3 ,
                                                 AV28FuncaoAPF_Nome3 ,
                                                 AV29FuncaoAPFAtributos_AtributosNom3 ,
                                                 AV35TFFuncaoAPF_Codigo ,
                                                 AV36TFFuncaoAPF_Codigo_To ,
                                                 AV40TFFuncaoAPF_Nome_Sel ,
                                                 AV39TFFuncaoAPF_Nome ,
                                                 AV43TFFuncaoAPFAtributos_AtributosCod ,
                                                 AV44TFFuncaoAPFAtributos_AtributosCod_To ,
                                                 AV48TFFuncaoAPFAtributos_AtributosNom_Sel ,
                                                 AV47TFFuncaoAPFAtributos_AtributosNom ,
                                                 AV51TFFuncaoAPFAtributos_FuncaoDadosCod ,
                                                 AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To ,
                                                 AV55TFFuncaoAPFAtributos_AtrTabelaCod ,
                                                 AV56TFFuncaoAPFAtributos_AtrTabelaCod_To ,
                                                 AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel ,
                                                 AV59TFFuncaoAPFAtributos_AtrTabelaNom ,
                                                 AV63TFFuncoesAPFAtributos_Regra_Sel ,
                                                 AV67TFFuncoesAPFAtributos_Code_Sel ,
                                                 AV66TFFuncoesAPFAtributos_Code ,
                                                 AV71TFFuncoesAPFAtributos_Nome_Sel ,
                                                 AV70TFFuncoesAPFAtributos_Nome ,
                                                 AV75TFFuncoesAPFAtributos_Descricao_Sel ,
                                                 AV74TFFuncoesAPFAtributos_Descricao ,
                                                 A166FuncaoAPF_Nome ,
                                                 A365FuncaoAPFAtributos_AtributosNom ,
                                                 A165FuncaoAPF_Codigo ,
                                                 A364FuncaoAPFAtributos_AtributosCod ,
                                                 A378FuncaoAPFAtributos_FuncaoDadosCod ,
                                                 A366FuncaoAPFAtributos_AtrTabelaCod ,
                                                 A367FuncaoAPFAtributos_AtrTabelaNom ,
                                                 A389FuncoesAPFAtributos_Regra ,
                                                 A383FuncoesAPFAtributos_Code ,
                                                 A384FuncoesAPFAtributos_Nome ,
                                                 A385FuncoesAPFAtributos_Descricao ,
                                                 AV14OrderedBy ,
                                                 AV15OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT,
                                                 TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV18FuncaoAPF_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV18FuncaoAPF_Nome1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18FuncaoAPF_Nome1", AV18FuncaoAPF_Nome1);
            lV18FuncaoAPF_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV18FuncaoAPF_Nome1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18FuncaoAPF_Nome1", AV18FuncaoAPF_Nome1);
            lV19FuncaoAPFAtributos_AtributosNom1 = StringUtil.PadR( StringUtil.RTrim( AV19FuncaoAPFAtributos_AtributosNom1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19FuncaoAPFAtributos_AtributosNom1", AV19FuncaoAPFAtributos_AtributosNom1);
            lV19FuncaoAPFAtributos_AtributosNom1 = StringUtil.PadR( StringUtil.RTrim( AV19FuncaoAPFAtributos_AtributosNom1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19FuncaoAPFAtributos_AtributosNom1", AV19FuncaoAPFAtributos_AtributosNom1);
            lV23FuncaoAPF_Nome2 = StringUtil.Concat( StringUtil.RTrim( AV23FuncaoAPF_Nome2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23FuncaoAPF_Nome2", AV23FuncaoAPF_Nome2);
            lV23FuncaoAPF_Nome2 = StringUtil.Concat( StringUtil.RTrim( AV23FuncaoAPF_Nome2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23FuncaoAPF_Nome2", AV23FuncaoAPF_Nome2);
            lV24FuncaoAPFAtributos_AtributosNom2 = StringUtil.PadR( StringUtil.RTrim( AV24FuncaoAPFAtributos_AtributosNom2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24FuncaoAPFAtributos_AtributosNom2", AV24FuncaoAPFAtributos_AtributosNom2);
            lV24FuncaoAPFAtributos_AtributosNom2 = StringUtil.PadR( StringUtil.RTrim( AV24FuncaoAPFAtributos_AtributosNom2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24FuncaoAPFAtributos_AtributosNom2", AV24FuncaoAPFAtributos_AtributosNom2);
            lV28FuncaoAPF_Nome3 = StringUtil.Concat( StringUtil.RTrim( AV28FuncaoAPF_Nome3), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28FuncaoAPF_Nome3", AV28FuncaoAPF_Nome3);
            lV28FuncaoAPF_Nome3 = StringUtil.Concat( StringUtil.RTrim( AV28FuncaoAPF_Nome3), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28FuncaoAPF_Nome3", AV28FuncaoAPF_Nome3);
            lV29FuncaoAPFAtributos_AtributosNom3 = StringUtil.PadR( StringUtil.RTrim( AV29FuncaoAPFAtributos_AtributosNom3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29FuncaoAPFAtributos_AtributosNom3", AV29FuncaoAPFAtributos_AtributosNom3);
            lV29FuncaoAPFAtributos_AtributosNom3 = StringUtil.PadR( StringUtil.RTrim( AV29FuncaoAPFAtributos_AtributosNom3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29FuncaoAPFAtributos_AtributosNom3", AV29FuncaoAPFAtributos_AtributosNom3);
            lV39TFFuncaoAPF_Nome = StringUtil.Concat( StringUtil.RTrim( AV39TFFuncaoAPF_Nome), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFFuncaoAPF_Nome", AV39TFFuncaoAPF_Nome);
            lV47TFFuncaoAPFAtributos_AtributosNom = StringUtil.PadR( StringUtil.RTrim( AV47TFFuncaoAPFAtributos_AtributosNom), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFFuncaoAPFAtributos_AtributosNom", AV47TFFuncaoAPFAtributos_AtributosNom);
            lV59TFFuncaoAPFAtributos_AtrTabelaNom = StringUtil.PadR( StringUtil.RTrim( AV59TFFuncaoAPFAtributos_AtrTabelaNom), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFFuncaoAPFAtributos_AtrTabelaNom", AV59TFFuncaoAPFAtributos_AtrTabelaNom);
            lV66TFFuncoesAPFAtributos_Code = StringUtil.Concat( StringUtil.RTrim( AV66TFFuncoesAPFAtributos_Code), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFFuncoesAPFAtributos_Code", AV66TFFuncoesAPFAtributos_Code);
            lV70TFFuncoesAPFAtributos_Nome = StringUtil.PadR( StringUtil.RTrim( AV70TFFuncoesAPFAtributos_Nome), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFFuncoesAPFAtributos_Nome", AV70TFFuncoesAPFAtributos_Nome);
            lV74TFFuncoesAPFAtributos_Descricao = StringUtil.Concat( StringUtil.RTrim( AV74TFFuncoesAPFAtributos_Descricao), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFFuncoesAPFAtributos_Descricao", AV74TFFuncoesAPFAtributos_Descricao);
            /* Using cursor H009N2 */
            pr_default.execute(0, new Object[] {lV18FuncaoAPF_Nome1, lV18FuncaoAPF_Nome1, lV19FuncaoAPFAtributos_AtributosNom1, lV19FuncaoAPFAtributos_AtributosNom1, lV23FuncaoAPF_Nome2, lV23FuncaoAPF_Nome2, lV24FuncaoAPFAtributos_AtributosNom2, lV24FuncaoAPFAtributos_AtributosNom2, lV28FuncaoAPF_Nome3, lV28FuncaoAPF_Nome3, lV29FuncaoAPFAtributos_AtributosNom3, lV29FuncaoAPFAtributos_AtributosNom3, AV35TFFuncaoAPF_Codigo, AV36TFFuncaoAPF_Codigo_To, lV39TFFuncaoAPF_Nome, AV40TFFuncaoAPF_Nome_Sel, AV43TFFuncaoAPFAtributos_AtributosCod, AV44TFFuncaoAPFAtributos_AtributosCod_To, lV47TFFuncaoAPFAtributos_AtributosNom, AV48TFFuncaoAPFAtributos_AtributosNom_Sel, AV51TFFuncaoAPFAtributos_FuncaoDadosCod, AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To, AV55TFFuncaoAPFAtributos_AtrTabelaCod, AV56TFFuncaoAPFAtributos_AtrTabelaCod_To, lV59TFFuncaoAPFAtributos_AtrTabelaNom, AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel, lV66TFFuncoesAPFAtributos_Code, AV67TFFuncoesAPFAtributos_Code_Sel, lV70TFFuncoesAPFAtributos_Nome, AV71TFFuncoesAPFAtributos_Nome_Sel, lV74TFFuncoesAPFAtributos_Descricao, AV75TFFuncoesAPFAtributos_Descricao_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_83_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A385FuncoesAPFAtributos_Descricao = H009N2_A385FuncoesAPFAtributos_Descricao[0];
               n385FuncoesAPFAtributos_Descricao = H009N2_n385FuncoesAPFAtributos_Descricao[0];
               A384FuncoesAPFAtributos_Nome = H009N2_A384FuncoesAPFAtributos_Nome[0];
               n384FuncoesAPFAtributos_Nome = H009N2_n384FuncoesAPFAtributos_Nome[0];
               A383FuncoesAPFAtributos_Code = H009N2_A383FuncoesAPFAtributos_Code[0];
               n383FuncoesAPFAtributos_Code = H009N2_n383FuncoesAPFAtributos_Code[0];
               A389FuncoesAPFAtributos_Regra = H009N2_A389FuncoesAPFAtributos_Regra[0];
               n389FuncoesAPFAtributos_Regra = H009N2_n389FuncoesAPFAtributos_Regra[0];
               A367FuncaoAPFAtributos_AtrTabelaNom = H009N2_A367FuncaoAPFAtributos_AtrTabelaNom[0];
               n367FuncaoAPFAtributos_AtrTabelaNom = H009N2_n367FuncaoAPFAtributos_AtrTabelaNom[0];
               A366FuncaoAPFAtributos_AtrTabelaCod = H009N2_A366FuncaoAPFAtributos_AtrTabelaCod[0];
               n366FuncaoAPFAtributos_AtrTabelaCod = H009N2_n366FuncaoAPFAtributos_AtrTabelaCod[0];
               A378FuncaoAPFAtributos_FuncaoDadosCod = H009N2_A378FuncaoAPFAtributos_FuncaoDadosCod[0];
               n378FuncaoAPFAtributos_FuncaoDadosCod = H009N2_n378FuncaoAPFAtributos_FuncaoDadosCod[0];
               A365FuncaoAPFAtributos_AtributosNom = H009N2_A365FuncaoAPFAtributos_AtributosNom[0];
               n365FuncaoAPFAtributos_AtributosNom = H009N2_n365FuncaoAPFAtributos_AtributosNom[0];
               A364FuncaoAPFAtributos_AtributosCod = H009N2_A364FuncaoAPFAtributos_AtributosCod[0];
               A166FuncaoAPF_Nome = H009N2_A166FuncaoAPF_Nome[0];
               A165FuncaoAPF_Codigo = H009N2_A165FuncaoAPF_Codigo[0];
               A366FuncaoAPFAtributos_AtrTabelaCod = H009N2_A366FuncaoAPFAtributos_AtrTabelaCod[0];
               n366FuncaoAPFAtributos_AtrTabelaCod = H009N2_n366FuncaoAPFAtributos_AtrTabelaCod[0];
               A365FuncaoAPFAtributos_AtributosNom = H009N2_A365FuncaoAPFAtributos_AtributosNom[0];
               n365FuncaoAPFAtributos_AtributosNom = H009N2_n365FuncaoAPFAtributos_AtributosNom[0];
               A367FuncaoAPFAtributos_AtrTabelaNom = H009N2_A367FuncaoAPFAtributos_AtrTabelaNom[0];
               n367FuncaoAPFAtributos_AtrTabelaNom = H009N2_n367FuncaoAPFAtributos_AtrTabelaNom[0];
               A166FuncaoAPF_Nome = H009N2_A166FuncaoAPF_Nome[0];
               /* Execute user event: E359N2 */
               E359N2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 83;
            WB9N0( ) ;
         }
         nGXsfl_83_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV16DynamicFiltersSelector1 ,
                                              AV17DynamicFiltersOperator1 ,
                                              AV18FuncaoAPF_Nome1 ,
                                              AV19FuncaoAPFAtributos_AtributosNom1 ,
                                              AV20DynamicFiltersEnabled2 ,
                                              AV21DynamicFiltersSelector2 ,
                                              AV22DynamicFiltersOperator2 ,
                                              AV23FuncaoAPF_Nome2 ,
                                              AV24FuncaoAPFAtributos_AtributosNom2 ,
                                              AV25DynamicFiltersEnabled3 ,
                                              AV26DynamicFiltersSelector3 ,
                                              AV27DynamicFiltersOperator3 ,
                                              AV28FuncaoAPF_Nome3 ,
                                              AV29FuncaoAPFAtributos_AtributosNom3 ,
                                              AV35TFFuncaoAPF_Codigo ,
                                              AV36TFFuncaoAPF_Codigo_To ,
                                              AV40TFFuncaoAPF_Nome_Sel ,
                                              AV39TFFuncaoAPF_Nome ,
                                              AV43TFFuncaoAPFAtributos_AtributosCod ,
                                              AV44TFFuncaoAPFAtributos_AtributosCod_To ,
                                              AV48TFFuncaoAPFAtributos_AtributosNom_Sel ,
                                              AV47TFFuncaoAPFAtributos_AtributosNom ,
                                              AV51TFFuncaoAPFAtributos_FuncaoDadosCod ,
                                              AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To ,
                                              AV55TFFuncaoAPFAtributos_AtrTabelaCod ,
                                              AV56TFFuncaoAPFAtributos_AtrTabelaCod_To ,
                                              AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel ,
                                              AV59TFFuncaoAPFAtributos_AtrTabelaNom ,
                                              AV63TFFuncoesAPFAtributos_Regra_Sel ,
                                              AV67TFFuncoesAPFAtributos_Code_Sel ,
                                              AV66TFFuncoesAPFAtributos_Code ,
                                              AV71TFFuncoesAPFAtributos_Nome_Sel ,
                                              AV70TFFuncoesAPFAtributos_Nome ,
                                              AV75TFFuncoesAPFAtributos_Descricao_Sel ,
                                              AV74TFFuncoesAPFAtributos_Descricao ,
                                              A166FuncaoAPF_Nome ,
                                              A365FuncaoAPFAtributos_AtributosNom ,
                                              A165FuncaoAPF_Codigo ,
                                              A364FuncaoAPFAtributos_AtributosCod ,
                                              A378FuncaoAPFAtributos_FuncaoDadosCod ,
                                              A366FuncaoAPFAtributos_AtrTabelaCod ,
                                              A367FuncaoAPFAtributos_AtrTabelaNom ,
                                              A389FuncoesAPFAtributos_Regra ,
                                              A383FuncoesAPFAtributos_Code ,
                                              A384FuncoesAPFAtributos_Nome ,
                                              A385FuncoesAPFAtributos_Descricao ,
                                              AV14OrderedBy ,
                                              AV15OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV18FuncaoAPF_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV18FuncaoAPF_Nome1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18FuncaoAPF_Nome1", AV18FuncaoAPF_Nome1);
         lV18FuncaoAPF_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV18FuncaoAPF_Nome1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18FuncaoAPF_Nome1", AV18FuncaoAPF_Nome1);
         lV19FuncaoAPFAtributos_AtributosNom1 = StringUtil.PadR( StringUtil.RTrim( AV19FuncaoAPFAtributos_AtributosNom1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19FuncaoAPFAtributos_AtributosNom1", AV19FuncaoAPFAtributos_AtributosNom1);
         lV19FuncaoAPFAtributos_AtributosNom1 = StringUtil.PadR( StringUtil.RTrim( AV19FuncaoAPFAtributos_AtributosNom1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19FuncaoAPFAtributos_AtributosNom1", AV19FuncaoAPFAtributos_AtributosNom1);
         lV23FuncaoAPF_Nome2 = StringUtil.Concat( StringUtil.RTrim( AV23FuncaoAPF_Nome2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23FuncaoAPF_Nome2", AV23FuncaoAPF_Nome2);
         lV23FuncaoAPF_Nome2 = StringUtil.Concat( StringUtil.RTrim( AV23FuncaoAPF_Nome2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23FuncaoAPF_Nome2", AV23FuncaoAPF_Nome2);
         lV24FuncaoAPFAtributos_AtributosNom2 = StringUtil.PadR( StringUtil.RTrim( AV24FuncaoAPFAtributos_AtributosNom2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24FuncaoAPFAtributos_AtributosNom2", AV24FuncaoAPFAtributos_AtributosNom2);
         lV24FuncaoAPFAtributos_AtributosNom2 = StringUtil.PadR( StringUtil.RTrim( AV24FuncaoAPFAtributos_AtributosNom2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24FuncaoAPFAtributos_AtributosNom2", AV24FuncaoAPFAtributos_AtributosNom2);
         lV28FuncaoAPF_Nome3 = StringUtil.Concat( StringUtil.RTrim( AV28FuncaoAPF_Nome3), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28FuncaoAPF_Nome3", AV28FuncaoAPF_Nome3);
         lV28FuncaoAPF_Nome3 = StringUtil.Concat( StringUtil.RTrim( AV28FuncaoAPF_Nome3), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28FuncaoAPF_Nome3", AV28FuncaoAPF_Nome3);
         lV29FuncaoAPFAtributos_AtributosNom3 = StringUtil.PadR( StringUtil.RTrim( AV29FuncaoAPFAtributos_AtributosNom3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29FuncaoAPFAtributos_AtributosNom3", AV29FuncaoAPFAtributos_AtributosNom3);
         lV29FuncaoAPFAtributos_AtributosNom3 = StringUtil.PadR( StringUtil.RTrim( AV29FuncaoAPFAtributos_AtributosNom3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29FuncaoAPFAtributos_AtributosNom3", AV29FuncaoAPFAtributos_AtributosNom3);
         lV39TFFuncaoAPF_Nome = StringUtil.Concat( StringUtil.RTrim( AV39TFFuncaoAPF_Nome), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFFuncaoAPF_Nome", AV39TFFuncaoAPF_Nome);
         lV47TFFuncaoAPFAtributos_AtributosNom = StringUtil.PadR( StringUtil.RTrim( AV47TFFuncaoAPFAtributos_AtributosNom), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFFuncaoAPFAtributos_AtributosNom", AV47TFFuncaoAPFAtributos_AtributosNom);
         lV59TFFuncaoAPFAtributos_AtrTabelaNom = StringUtil.PadR( StringUtil.RTrim( AV59TFFuncaoAPFAtributos_AtrTabelaNom), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFFuncaoAPFAtributos_AtrTabelaNom", AV59TFFuncaoAPFAtributos_AtrTabelaNom);
         lV66TFFuncoesAPFAtributos_Code = StringUtil.Concat( StringUtil.RTrim( AV66TFFuncoesAPFAtributos_Code), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFFuncoesAPFAtributos_Code", AV66TFFuncoesAPFAtributos_Code);
         lV70TFFuncoesAPFAtributos_Nome = StringUtil.PadR( StringUtil.RTrim( AV70TFFuncoesAPFAtributos_Nome), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFFuncoesAPFAtributos_Nome", AV70TFFuncoesAPFAtributos_Nome);
         lV74TFFuncoesAPFAtributos_Descricao = StringUtil.Concat( StringUtil.RTrim( AV74TFFuncoesAPFAtributos_Descricao), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFFuncoesAPFAtributos_Descricao", AV74TFFuncoesAPFAtributos_Descricao);
         /* Using cursor H009N3 */
         pr_default.execute(1, new Object[] {lV18FuncaoAPF_Nome1, lV18FuncaoAPF_Nome1, lV19FuncaoAPFAtributos_AtributosNom1, lV19FuncaoAPFAtributos_AtributosNom1, lV23FuncaoAPF_Nome2, lV23FuncaoAPF_Nome2, lV24FuncaoAPFAtributos_AtributosNom2, lV24FuncaoAPFAtributos_AtributosNom2, lV28FuncaoAPF_Nome3, lV28FuncaoAPF_Nome3, lV29FuncaoAPFAtributos_AtributosNom3, lV29FuncaoAPFAtributos_AtributosNom3, AV35TFFuncaoAPF_Codigo, AV36TFFuncaoAPF_Codigo_To, lV39TFFuncaoAPF_Nome, AV40TFFuncaoAPF_Nome_Sel, AV43TFFuncaoAPFAtributos_AtributosCod, AV44TFFuncaoAPFAtributos_AtributosCod_To, lV47TFFuncaoAPFAtributos_AtributosNom, AV48TFFuncaoAPFAtributos_AtributosNom_Sel, AV51TFFuncaoAPFAtributos_FuncaoDadosCod, AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To, AV55TFFuncaoAPFAtributos_AtrTabelaCod, AV56TFFuncaoAPFAtributos_AtrTabelaCod_To, lV59TFFuncaoAPFAtributos_AtrTabelaNom, AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel, lV66TFFuncoesAPFAtributos_Code, AV67TFFuncoesAPFAtributos_Code_Sel, lV70TFFuncoesAPFAtributos_Nome, AV71TFFuncoesAPFAtributos_Nome_Sel, lV74TFFuncoesAPFAtributos_Descricao, AV75TFFuncoesAPFAtributos_Descricao_Sel});
         GRID_nRecordCount = H009N3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18FuncaoAPF_Nome1, AV19FuncaoAPFAtributos_AtributosNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23FuncaoAPF_Nome2, AV24FuncaoAPFAtributos_AtributosNom2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28FuncaoAPF_Nome3, AV29FuncaoAPFAtributos_AtributosNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV35TFFuncaoAPF_Codigo, AV36TFFuncaoAPF_Codigo_To, AV39TFFuncaoAPF_Nome, AV40TFFuncaoAPF_Nome_Sel, AV43TFFuncaoAPFAtributos_AtributosCod, AV44TFFuncaoAPFAtributos_AtributosCod_To, AV47TFFuncaoAPFAtributos_AtributosNom, AV48TFFuncaoAPFAtributos_AtributosNom_Sel, AV51TFFuncaoAPFAtributos_FuncaoDadosCod, AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To, AV55TFFuncaoAPFAtributos_AtrTabelaCod, AV56TFFuncaoAPFAtributos_AtrTabelaCod_To, AV59TFFuncaoAPFAtributos_AtrTabelaNom, AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel, AV63TFFuncoesAPFAtributos_Regra_Sel, AV66TFFuncoesAPFAtributos_Code, AV67TFFuncoesAPFAtributos_Code_Sel, AV70TFFuncoesAPFAtributos_Nome, AV71TFFuncoesAPFAtributos_Nome_Sel, AV74TFFuncoesAPFAtributos_Descricao, AV75TFFuncoesAPFAtributos_Descricao_Sel, AV37ddo_FuncaoAPF_CodigoTitleControlIdToReplace, AV41ddo_FuncaoAPF_NomeTitleControlIdToReplace, AV45ddo_FuncaoAPFAtributos_AtributosCodTitleControlIdToReplace, AV49ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace, AV53ddo_FuncaoAPFAtributos_FuncaoDadosCodTitleControlIdToReplace, AV57ddo_FuncaoAPFAtributos_AtrTabelaCodTitleControlIdToReplace, AV61ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace, AV64ddo_FuncoesAPFAtributos_RegraTitleControlIdToReplace, AV68ddo_FuncoesAPFAtributos_CodeTitleControlIdToReplace, AV72ddo_FuncoesAPFAtributos_NomeTitleControlIdToReplace, AV76ddo_FuncoesAPFAtributos_DescricaoTitleControlIdToReplace, AV84Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18FuncaoAPF_Nome1, AV19FuncaoAPFAtributos_AtributosNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23FuncaoAPF_Nome2, AV24FuncaoAPFAtributos_AtributosNom2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28FuncaoAPF_Nome3, AV29FuncaoAPFAtributos_AtributosNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV35TFFuncaoAPF_Codigo, AV36TFFuncaoAPF_Codigo_To, AV39TFFuncaoAPF_Nome, AV40TFFuncaoAPF_Nome_Sel, AV43TFFuncaoAPFAtributos_AtributosCod, AV44TFFuncaoAPFAtributos_AtributosCod_To, AV47TFFuncaoAPFAtributos_AtributosNom, AV48TFFuncaoAPFAtributos_AtributosNom_Sel, AV51TFFuncaoAPFAtributos_FuncaoDadosCod, AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To, AV55TFFuncaoAPFAtributos_AtrTabelaCod, AV56TFFuncaoAPFAtributos_AtrTabelaCod_To, AV59TFFuncaoAPFAtributos_AtrTabelaNom, AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel, AV63TFFuncoesAPFAtributos_Regra_Sel, AV66TFFuncoesAPFAtributos_Code, AV67TFFuncoesAPFAtributos_Code_Sel, AV70TFFuncoesAPFAtributos_Nome, AV71TFFuncoesAPFAtributos_Nome_Sel, AV74TFFuncoesAPFAtributos_Descricao, AV75TFFuncoesAPFAtributos_Descricao_Sel, AV37ddo_FuncaoAPF_CodigoTitleControlIdToReplace, AV41ddo_FuncaoAPF_NomeTitleControlIdToReplace, AV45ddo_FuncaoAPFAtributos_AtributosCodTitleControlIdToReplace, AV49ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace, AV53ddo_FuncaoAPFAtributos_FuncaoDadosCodTitleControlIdToReplace, AV57ddo_FuncaoAPFAtributos_AtrTabelaCodTitleControlIdToReplace, AV61ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace, AV64ddo_FuncoesAPFAtributos_RegraTitleControlIdToReplace, AV68ddo_FuncoesAPFAtributos_CodeTitleControlIdToReplace, AV72ddo_FuncoesAPFAtributos_NomeTitleControlIdToReplace, AV76ddo_FuncoesAPFAtributos_DescricaoTitleControlIdToReplace, AV84Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18FuncaoAPF_Nome1, AV19FuncaoAPFAtributos_AtributosNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23FuncaoAPF_Nome2, AV24FuncaoAPFAtributos_AtributosNom2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28FuncaoAPF_Nome3, AV29FuncaoAPFAtributos_AtributosNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV35TFFuncaoAPF_Codigo, AV36TFFuncaoAPF_Codigo_To, AV39TFFuncaoAPF_Nome, AV40TFFuncaoAPF_Nome_Sel, AV43TFFuncaoAPFAtributos_AtributosCod, AV44TFFuncaoAPFAtributos_AtributosCod_To, AV47TFFuncaoAPFAtributos_AtributosNom, AV48TFFuncaoAPFAtributos_AtributosNom_Sel, AV51TFFuncaoAPFAtributos_FuncaoDadosCod, AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To, AV55TFFuncaoAPFAtributos_AtrTabelaCod, AV56TFFuncaoAPFAtributos_AtrTabelaCod_To, AV59TFFuncaoAPFAtributos_AtrTabelaNom, AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel, AV63TFFuncoesAPFAtributos_Regra_Sel, AV66TFFuncoesAPFAtributos_Code, AV67TFFuncoesAPFAtributos_Code_Sel, AV70TFFuncoesAPFAtributos_Nome, AV71TFFuncoesAPFAtributos_Nome_Sel, AV74TFFuncoesAPFAtributos_Descricao, AV75TFFuncoesAPFAtributos_Descricao_Sel, AV37ddo_FuncaoAPF_CodigoTitleControlIdToReplace, AV41ddo_FuncaoAPF_NomeTitleControlIdToReplace, AV45ddo_FuncaoAPFAtributos_AtributosCodTitleControlIdToReplace, AV49ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace, AV53ddo_FuncaoAPFAtributos_FuncaoDadosCodTitleControlIdToReplace, AV57ddo_FuncaoAPFAtributos_AtrTabelaCodTitleControlIdToReplace, AV61ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace, AV64ddo_FuncoesAPFAtributos_RegraTitleControlIdToReplace, AV68ddo_FuncoesAPFAtributos_CodeTitleControlIdToReplace, AV72ddo_FuncoesAPFAtributos_NomeTitleControlIdToReplace, AV76ddo_FuncoesAPFAtributos_DescricaoTitleControlIdToReplace, AV84Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18FuncaoAPF_Nome1, AV19FuncaoAPFAtributos_AtributosNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23FuncaoAPF_Nome2, AV24FuncaoAPFAtributos_AtributosNom2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28FuncaoAPF_Nome3, AV29FuncaoAPFAtributos_AtributosNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV35TFFuncaoAPF_Codigo, AV36TFFuncaoAPF_Codigo_To, AV39TFFuncaoAPF_Nome, AV40TFFuncaoAPF_Nome_Sel, AV43TFFuncaoAPFAtributos_AtributosCod, AV44TFFuncaoAPFAtributos_AtributosCod_To, AV47TFFuncaoAPFAtributos_AtributosNom, AV48TFFuncaoAPFAtributos_AtributosNom_Sel, AV51TFFuncaoAPFAtributos_FuncaoDadosCod, AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To, AV55TFFuncaoAPFAtributos_AtrTabelaCod, AV56TFFuncaoAPFAtributos_AtrTabelaCod_To, AV59TFFuncaoAPFAtributos_AtrTabelaNom, AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel, AV63TFFuncoesAPFAtributos_Regra_Sel, AV66TFFuncoesAPFAtributos_Code, AV67TFFuncoesAPFAtributos_Code_Sel, AV70TFFuncoesAPFAtributos_Nome, AV71TFFuncoesAPFAtributos_Nome_Sel, AV74TFFuncoesAPFAtributos_Descricao, AV75TFFuncoesAPFAtributos_Descricao_Sel, AV37ddo_FuncaoAPF_CodigoTitleControlIdToReplace, AV41ddo_FuncaoAPF_NomeTitleControlIdToReplace, AV45ddo_FuncaoAPFAtributos_AtributosCodTitleControlIdToReplace, AV49ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace, AV53ddo_FuncaoAPFAtributos_FuncaoDadosCodTitleControlIdToReplace, AV57ddo_FuncaoAPFAtributos_AtrTabelaCodTitleControlIdToReplace, AV61ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace, AV64ddo_FuncoesAPFAtributos_RegraTitleControlIdToReplace, AV68ddo_FuncoesAPFAtributos_CodeTitleControlIdToReplace, AV72ddo_FuncoesAPFAtributos_NomeTitleControlIdToReplace, AV76ddo_FuncoesAPFAtributos_DescricaoTitleControlIdToReplace, AV84Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18FuncaoAPF_Nome1, AV19FuncaoAPFAtributos_AtributosNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23FuncaoAPF_Nome2, AV24FuncaoAPFAtributos_AtributosNom2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28FuncaoAPF_Nome3, AV29FuncaoAPFAtributos_AtributosNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV35TFFuncaoAPF_Codigo, AV36TFFuncaoAPF_Codigo_To, AV39TFFuncaoAPF_Nome, AV40TFFuncaoAPF_Nome_Sel, AV43TFFuncaoAPFAtributos_AtributosCod, AV44TFFuncaoAPFAtributos_AtributosCod_To, AV47TFFuncaoAPFAtributos_AtributosNom, AV48TFFuncaoAPFAtributos_AtributosNom_Sel, AV51TFFuncaoAPFAtributos_FuncaoDadosCod, AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To, AV55TFFuncaoAPFAtributos_AtrTabelaCod, AV56TFFuncaoAPFAtributos_AtrTabelaCod_To, AV59TFFuncaoAPFAtributos_AtrTabelaNom, AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel, AV63TFFuncoesAPFAtributos_Regra_Sel, AV66TFFuncoesAPFAtributos_Code, AV67TFFuncoesAPFAtributos_Code_Sel, AV70TFFuncoesAPFAtributos_Nome, AV71TFFuncoesAPFAtributos_Nome_Sel, AV74TFFuncoesAPFAtributos_Descricao, AV75TFFuncoesAPFAtributos_Descricao_Sel, AV37ddo_FuncaoAPF_CodigoTitleControlIdToReplace, AV41ddo_FuncaoAPF_NomeTitleControlIdToReplace, AV45ddo_FuncaoAPFAtributos_AtributosCodTitleControlIdToReplace, AV49ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace, AV53ddo_FuncaoAPFAtributos_FuncaoDadosCodTitleControlIdToReplace, AV57ddo_FuncaoAPFAtributos_AtrTabelaCodTitleControlIdToReplace, AV61ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace, AV64ddo_FuncoesAPFAtributos_RegraTitleControlIdToReplace, AV68ddo_FuncoesAPFAtributos_CodeTitleControlIdToReplace, AV72ddo_FuncoesAPFAtributos_NomeTitleControlIdToReplace, AV76ddo_FuncoesAPFAtributos_DescricaoTitleControlIdToReplace, AV84Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving) ;
         }
         return (int)(0) ;
      }

      protected void STRUP9N0( )
      {
         /* Before Start, stand alone formulas. */
         AV84Pgmname = "PromptFuncoesAPFAtributos";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E339N2 */
         E339N2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV77DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCAOAPF_CODIGOTITLEFILTERDATA"), AV34FuncaoAPF_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCAOAPF_NOMETITLEFILTERDATA"), AV38FuncaoAPF_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCAOAPFATRIBUTOS_ATRIBUTOSCODTITLEFILTERDATA"), AV42FuncaoAPFAtributos_AtributosCodTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOMTITLEFILTERDATA"), AV46FuncaoAPFAtributos_AtributosNomTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCAOAPFATRIBUTOS_FUNCAODADOSCODTITLEFILTERDATA"), AV50FuncaoAPFAtributos_FuncaoDadosCodTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCAOAPFATRIBUTOS_ATRTABELACODTITLEFILTERDATA"), AV54FuncaoAPFAtributos_AtrTabelaCodTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCAOAPFATRIBUTOS_ATRTABELANOMTITLEFILTERDATA"), AV58FuncaoAPFAtributos_AtrTabelaNomTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCOESAPFATRIBUTOS_REGRATITLEFILTERDATA"), AV62FuncoesAPFAtributos_RegraTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCOESAPFATRIBUTOS_CODETITLEFILTERDATA"), AV65FuncoesAPFAtributos_CodeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCOESAPFATRIBUTOS_NOMETITLEFILTERDATA"), AV69FuncoesAPFAtributos_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCOESAPFATRIBUTOS_DESCRICAOTITLEFILTERDATA"), AV73FuncoesAPFAtributos_DescricaoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV14OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV16DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
            AV18FuncaoAPF_Nome1 = cgiGet( edtavFuncaoapf_nome1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18FuncaoAPF_Nome1", AV18FuncaoAPF_Nome1);
            AV19FuncaoAPFAtributos_AtributosNom1 = StringUtil.Upper( cgiGet( edtavFuncaoapfatributos_atributosnom1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19FuncaoAPFAtributos_AtributosNom1", AV19FuncaoAPFAtributos_AtributosNom1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV21DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
            AV23FuncaoAPF_Nome2 = cgiGet( edtavFuncaoapf_nome2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23FuncaoAPF_Nome2", AV23FuncaoAPF_Nome2);
            AV24FuncaoAPFAtributos_AtributosNom2 = StringUtil.Upper( cgiGet( edtavFuncaoapfatributos_atributosnom2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24FuncaoAPFAtributos_AtributosNom2", AV24FuncaoAPFAtributos_AtributosNom2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV26DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV27DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
            AV28FuncaoAPF_Nome3 = cgiGet( edtavFuncaoapf_nome3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28FuncaoAPF_Nome3", AV28FuncaoAPF_Nome3);
            AV29FuncaoAPFAtributos_AtributosNom3 = StringUtil.Upper( cgiGet( edtavFuncaoapfatributos_atributosnom3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29FuncaoAPFAtributos_AtributosNom3", AV29FuncaoAPFAtributos_AtributosNom3);
            AV20DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
            AV25DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaoapf_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaoapf_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAOAPF_CODIGO");
               GX_FocusControl = edtavTffuncaoapf_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV35TFFuncaoAPF_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFFuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFFuncaoAPF_Codigo), 6, 0)));
            }
            else
            {
               AV35TFFuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTffuncaoapf_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFFuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFFuncaoAPF_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaoapf_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaoapf_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAOAPF_CODIGO_TO");
               GX_FocusControl = edtavTffuncaoapf_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV36TFFuncaoAPF_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFFuncaoAPF_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFFuncaoAPF_Codigo_To), 6, 0)));
            }
            else
            {
               AV36TFFuncaoAPF_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTffuncaoapf_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFFuncaoAPF_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFFuncaoAPF_Codigo_To), 6, 0)));
            }
            AV39TFFuncaoAPF_Nome = cgiGet( edtavTffuncaoapf_nome_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFFuncaoAPF_Nome", AV39TFFuncaoAPF_Nome);
            AV40TFFuncaoAPF_Nome_Sel = cgiGet( edtavTffuncaoapf_nome_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFFuncaoAPF_Nome_Sel", AV40TFFuncaoAPF_Nome_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaoapfatributos_atributoscod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaoapfatributos_atributoscod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD");
               GX_FocusControl = edtavTffuncaoapfatributos_atributoscod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV43TFFuncaoAPFAtributos_AtributosCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFFuncaoAPFAtributos_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFFuncaoAPFAtributos_AtributosCod), 6, 0)));
            }
            else
            {
               AV43TFFuncaoAPFAtributos_AtributosCod = (int)(context.localUtil.CToN( cgiGet( edtavTffuncaoapfatributos_atributoscod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFFuncaoAPFAtributos_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFFuncaoAPFAtributos_AtributosCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaoapfatributos_atributoscod_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaoapfatributos_atributoscod_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_TO");
               GX_FocusControl = edtavTffuncaoapfatributos_atributoscod_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV44TFFuncaoAPFAtributos_AtributosCod_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFFuncaoAPFAtributos_AtributosCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFFuncaoAPFAtributos_AtributosCod_To), 6, 0)));
            }
            else
            {
               AV44TFFuncaoAPFAtributos_AtributosCod_To = (int)(context.localUtil.CToN( cgiGet( edtavTffuncaoapfatributos_atributoscod_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFFuncaoAPFAtributos_AtributosCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFFuncaoAPFAtributos_AtributosCod_To), 6, 0)));
            }
            AV47TFFuncaoAPFAtributos_AtributosNom = StringUtil.Upper( cgiGet( edtavTffuncaoapfatributos_atributosnom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFFuncaoAPFAtributos_AtributosNom", AV47TFFuncaoAPFAtributos_AtributosNom);
            AV48TFFuncaoAPFAtributos_AtributosNom_Sel = StringUtil.Upper( cgiGet( edtavTffuncaoapfatributos_atributosnom_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFFuncaoAPFAtributos_AtributosNom_Sel", AV48TFFuncaoAPFAtributos_AtributosNom_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaoapfatributos_funcaodadoscod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaoapfatributos_funcaodadoscod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD");
               GX_FocusControl = edtavTffuncaoapfatributos_funcaodadoscod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV51TFFuncaoAPFAtributos_FuncaoDadosCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFFuncaoAPFAtributos_FuncaoDadosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51TFFuncaoAPFAtributos_FuncaoDadosCod), 6, 0)));
            }
            else
            {
               AV51TFFuncaoAPFAtributos_FuncaoDadosCod = (int)(context.localUtil.CToN( cgiGet( edtavTffuncaoapfatributos_funcaodadoscod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFFuncaoAPFAtributos_FuncaoDadosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51TFFuncaoAPFAtributos_FuncaoDadosCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaoapfatributos_funcaodadoscod_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaoapfatributos_funcaodadoscod_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_TO");
               GX_FocusControl = edtavTffuncaoapfatributos_funcaodadoscod_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To), 6, 0)));
            }
            else
            {
               AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To = (int)(context.localUtil.CToN( cgiGet( edtavTffuncaoapfatributos_funcaodadoscod_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaoapfatributos_atrtabelacod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaoapfatributos_atrtabelacod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD");
               GX_FocusControl = edtavTffuncaoapfatributos_atrtabelacod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV55TFFuncaoAPFAtributos_AtrTabelaCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFFuncaoAPFAtributos_AtrTabelaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFFuncaoAPFAtributos_AtrTabelaCod), 6, 0)));
            }
            else
            {
               AV55TFFuncaoAPFAtributos_AtrTabelaCod = (int)(context.localUtil.CToN( cgiGet( edtavTffuncaoapfatributos_atrtabelacod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFFuncaoAPFAtributos_AtrTabelaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFFuncaoAPFAtributos_AtrTabelaCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaoapfatributos_atrtabelacod_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaoapfatributos_atrtabelacod_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD_TO");
               GX_FocusControl = edtavTffuncaoapfatributos_atrtabelacod_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV56TFFuncaoAPFAtributos_AtrTabelaCod_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFFuncaoAPFAtributos_AtrTabelaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56TFFuncaoAPFAtributos_AtrTabelaCod_To), 6, 0)));
            }
            else
            {
               AV56TFFuncaoAPFAtributos_AtrTabelaCod_To = (int)(context.localUtil.CToN( cgiGet( edtavTffuncaoapfatributos_atrtabelacod_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFFuncaoAPFAtributos_AtrTabelaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56TFFuncaoAPFAtributos_AtrTabelaCod_To), 6, 0)));
            }
            AV59TFFuncaoAPFAtributos_AtrTabelaNom = StringUtil.Upper( cgiGet( edtavTffuncaoapfatributos_atrtabelanom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFFuncaoAPFAtributos_AtrTabelaNom", AV59TFFuncaoAPFAtributos_AtrTabelaNom);
            AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel = StringUtil.Upper( cgiGet( edtavTffuncaoapfatributos_atrtabelanom_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel", AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncoesapfatributos_regra_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncoesapfatributos_regra_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCOESAPFATRIBUTOS_REGRA_SEL");
               GX_FocusControl = edtavTffuncoesapfatributos_regra_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV63TFFuncoesAPFAtributos_Regra_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFFuncoesAPFAtributos_Regra_Sel", StringUtil.Str( (decimal)(AV63TFFuncoesAPFAtributos_Regra_Sel), 1, 0));
            }
            else
            {
               AV63TFFuncoesAPFAtributos_Regra_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTffuncoesapfatributos_regra_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFFuncoesAPFAtributos_Regra_Sel", StringUtil.Str( (decimal)(AV63TFFuncoesAPFAtributos_Regra_Sel), 1, 0));
            }
            AV66TFFuncoesAPFAtributos_Code = cgiGet( edtavTffuncoesapfatributos_code_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFFuncoesAPFAtributos_Code", AV66TFFuncoesAPFAtributos_Code);
            AV67TFFuncoesAPFAtributos_Code_Sel = cgiGet( edtavTffuncoesapfatributos_code_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFFuncoesAPFAtributos_Code_Sel", AV67TFFuncoesAPFAtributos_Code_Sel);
            AV70TFFuncoesAPFAtributos_Nome = StringUtil.Upper( cgiGet( edtavTffuncoesapfatributos_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFFuncoesAPFAtributos_Nome", AV70TFFuncoesAPFAtributos_Nome);
            AV71TFFuncoesAPFAtributos_Nome_Sel = StringUtil.Upper( cgiGet( edtavTffuncoesapfatributos_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFFuncoesAPFAtributos_Nome_Sel", AV71TFFuncoesAPFAtributos_Nome_Sel);
            AV74TFFuncoesAPFAtributos_Descricao = StringUtil.Upper( cgiGet( edtavTffuncoesapfatributos_descricao_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFFuncoesAPFAtributos_Descricao", AV74TFFuncoesAPFAtributos_Descricao);
            AV75TFFuncoesAPFAtributos_Descricao_Sel = StringUtil.Upper( cgiGet( edtavTffuncoesapfatributos_descricao_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFFuncoesAPFAtributos_Descricao_Sel", AV75TFFuncoesAPFAtributos_Descricao_Sel);
            AV37ddo_FuncaoAPF_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_funcaoapf_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_FuncaoAPF_CodigoTitleControlIdToReplace", AV37ddo_FuncaoAPF_CodigoTitleControlIdToReplace);
            AV41ddo_FuncaoAPF_NomeTitleControlIdToReplace = cgiGet( edtavDdo_funcaoapf_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_FuncaoAPF_NomeTitleControlIdToReplace", AV41ddo_FuncaoAPF_NomeTitleControlIdToReplace);
            AV45ddo_FuncaoAPFAtributos_AtributosCodTitleControlIdToReplace = cgiGet( edtavDdo_funcaoapfatributos_atributoscodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_FuncaoAPFAtributos_AtributosCodTitleControlIdToReplace", AV45ddo_FuncaoAPFAtributos_AtributosCodTitleControlIdToReplace);
            AV49ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace = cgiGet( edtavDdo_funcaoapfatributos_atributosnomtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace", AV49ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace);
            AV53ddo_FuncaoAPFAtributos_FuncaoDadosCodTitleControlIdToReplace = cgiGet( edtavDdo_funcaoapfatributos_funcaodadoscodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_FuncaoAPFAtributos_FuncaoDadosCodTitleControlIdToReplace", AV53ddo_FuncaoAPFAtributos_FuncaoDadosCodTitleControlIdToReplace);
            AV57ddo_FuncaoAPFAtributos_AtrTabelaCodTitleControlIdToReplace = cgiGet( edtavDdo_funcaoapfatributos_atrtabelacodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_FuncaoAPFAtributos_AtrTabelaCodTitleControlIdToReplace", AV57ddo_FuncaoAPFAtributos_AtrTabelaCodTitleControlIdToReplace);
            AV61ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace = cgiGet( edtavDdo_funcaoapfatributos_atrtabelanomtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace", AV61ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace);
            AV64ddo_FuncoesAPFAtributos_RegraTitleControlIdToReplace = cgiGet( edtavDdo_funcoesapfatributos_regratitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64ddo_FuncoesAPFAtributos_RegraTitleControlIdToReplace", AV64ddo_FuncoesAPFAtributos_RegraTitleControlIdToReplace);
            AV68ddo_FuncoesAPFAtributos_CodeTitleControlIdToReplace = cgiGet( edtavDdo_funcoesapfatributos_codetitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ddo_FuncoesAPFAtributos_CodeTitleControlIdToReplace", AV68ddo_FuncoesAPFAtributos_CodeTitleControlIdToReplace);
            AV72ddo_FuncoesAPFAtributos_NomeTitleControlIdToReplace = cgiGet( edtavDdo_funcoesapfatributos_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72ddo_FuncoesAPFAtributos_NomeTitleControlIdToReplace", AV72ddo_FuncoesAPFAtributos_NomeTitleControlIdToReplace);
            AV76ddo_FuncoesAPFAtributos_DescricaoTitleControlIdToReplace = cgiGet( edtavDdo_funcoesapfatributos_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76ddo_FuncoesAPFAtributos_DescricaoTitleControlIdToReplace", AV76ddo_FuncoesAPFAtributos_DescricaoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_83 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_83"), ",", "."));
            AV79GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV80GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_funcaoapf_codigo_Caption = cgiGet( "DDO_FUNCAOAPF_CODIGO_Caption");
            Ddo_funcaoapf_codigo_Tooltip = cgiGet( "DDO_FUNCAOAPF_CODIGO_Tooltip");
            Ddo_funcaoapf_codigo_Cls = cgiGet( "DDO_FUNCAOAPF_CODIGO_Cls");
            Ddo_funcaoapf_codigo_Filteredtext_set = cgiGet( "DDO_FUNCAOAPF_CODIGO_Filteredtext_set");
            Ddo_funcaoapf_codigo_Filteredtextto_set = cgiGet( "DDO_FUNCAOAPF_CODIGO_Filteredtextto_set");
            Ddo_funcaoapf_codigo_Dropdownoptionstype = cgiGet( "DDO_FUNCAOAPF_CODIGO_Dropdownoptionstype");
            Ddo_funcaoapf_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCAOAPF_CODIGO_Titlecontrolidtoreplace");
            Ddo_funcaoapf_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_CODIGO_Includesortasc"));
            Ddo_funcaoapf_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_CODIGO_Includesortdsc"));
            Ddo_funcaoapf_codigo_Sortedstatus = cgiGet( "DDO_FUNCAOAPF_CODIGO_Sortedstatus");
            Ddo_funcaoapf_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_CODIGO_Includefilter"));
            Ddo_funcaoapf_codigo_Filtertype = cgiGet( "DDO_FUNCAOAPF_CODIGO_Filtertype");
            Ddo_funcaoapf_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_CODIGO_Filterisrange"));
            Ddo_funcaoapf_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_CODIGO_Includedatalist"));
            Ddo_funcaoapf_codigo_Sortasc = cgiGet( "DDO_FUNCAOAPF_CODIGO_Sortasc");
            Ddo_funcaoapf_codigo_Sortdsc = cgiGet( "DDO_FUNCAOAPF_CODIGO_Sortdsc");
            Ddo_funcaoapf_codigo_Cleanfilter = cgiGet( "DDO_FUNCAOAPF_CODIGO_Cleanfilter");
            Ddo_funcaoapf_codigo_Rangefilterfrom = cgiGet( "DDO_FUNCAOAPF_CODIGO_Rangefilterfrom");
            Ddo_funcaoapf_codigo_Rangefilterto = cgiGet( "DDO_FUNCAOAPF_CODIGO_Rangefilterto");
            Ddo_funcaoapf_codigo_Searchbuttontext = cgiGet( "DDO_FUNCAOAPF_CODIGO_Searchbuttontext");
            Ddo_funcaoapf_nome_Caption = cgiGet( "DDO_FUNCAOAPF_NOME_Caption");
            Ddo_funcaoapf_nome_Tooltip = cgiGet( "DDO_FUNCAOAPF_NOME_Tooltip");
            Ddo_funcaoapf_nome_Cls = cgiGet( "DDO_FUNCAOAPF_NOME_Cls");
            Ddo_funcaoapf_nome_Filteredtext_set = cgiGet( "DDO_FUNCAOAPF_NOME_Filteredtext_set");
            Ddo_funcaoapf_nome_Selectedvalue_set = cgiGet( "DDO_FUNCAOAPF_NOME_Selectedvalue_set");
            Ddo_funcaoapf_nome_Dropdownoptionstype = cgiGet( "DDO_FUNCAOAPF_NOME_Dropdownoptionstype");
            Ddo_funcaoapf_nome_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCAOAPF_NOME_Titlecontrolidtoreplace");
            Ddo_funcaoapf_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_NOME_Includesortasc"));
            Ddo_funcaoapf_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_NOME_Includesortdsc"));
            Ddo_funcaoapf_nome_Sortedstatus = cgiGet( "DDO_FUNCAOAPF_NOME_Sortedstatus");
            Ddo_funcaoapf_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_NOME_Includefilter"));
            Ddo_funcaoapf_nome_Filtertype = cgiGet( "DDO_FUNCAOAPF_NOME_Filtertype");
            Ddo_funcaoapf_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_NOME_Filterisrange"));
            Ddo_funcaoapf_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_NOME_Includedatalist"));
            Ddo_funcaoapf_nome_Datalisttype = cgiGet( "DDO_FUNCAOAPF_NOME_Datalisttype");
            Ddo_funcaoapf_nome_Datalistproc = cgiGet( "DDO_FUNCAOAPF_NOME_Datalistproc");
            Ddo_funcaoapf_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_FUNCAOAPF_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_funcaoapf_nome_Sortasc = cgiGet( "DDO_FUNCAOAPF_NOME_Sortasc");
            Ddo_funcaoapf_nome_Sortdsc = cgiGet( "DDO_FUNCAOAPF_NOME_Sortdsc");
            Ddo_funcaoapf_nome_Loadingdata = cgiGet( "DDO_FUNCAOAPF_NOME_Loadingdata");
            Ddo_funcaoapf_nome_Cleanfilter = cgiGet( "DDO_FUNCAOAPF_NOME_Cleanfilter");
            Ddo_funcaoapf_nome_Noresultsfound = cgiGet( "DDO_FUNCAOAPF_NOME_Noresultsfound");
            Ddo_funcaoapf_nome_Searchbuttontext = cgiGet( "DDO_FUNCAOAPF_NOME_Searchbuttontext");
            Ddo_funcaoapfatributos_atributoscod_Caption = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_Caption");
            Ddo_funcaoapfatributos_atributoscod_Tooltip = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_Tooltip");
            Ddo_funcaoapfatributos_atributoscod_Cls = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_Cls");
            Ddo_funcaoapfatributos_atributoscod_Filteredtext_set = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_Filteredtext_set");
            Ddo_funcaoapfatributos_atributoscod_Filteredtextto_set = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_Filteredtextto_set");
            Ddo_funcaoapfatributos_atributoscod_Dropdownoptionstype = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_Dropdownoptionstype");
            Ddo_funcaoapfatributos_atributoscod_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_Titlecontrolidtoreplace");
            Ddo_funcaoapfatributos_atributoscod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_Includesortasc"));
            Ddo_funcaoapfatributos_atributoscod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_Includesortdsc"));
            Ddo_funcaoapfatributos_atributoscod_Sortedstatus = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_Sortedstatus");
            Ddo_funcaoapfatributos_atributoscod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_Includefilter"));
            Ddo_funcaoapfatributos_atributoscod_Filtertype = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_Filtertype");
            Ddo_funcaoapfatributos_atributoscod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_Filterisrange"));
            Ddo_funcaoapfatributos_atributoscod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_Includedatalist"));
            Ddo_funcaoapfatributos_atributoscod_Sortasc = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_Sortasc");
            Ddo_funcaoapfatributos_atributoscod_Sortdsc = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_Sortdsc");
            Ddo_funcaoapfatributos_atributoscod_Cleanfilter = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_Cleanfilter");
            Ddo_funcaoapfatributos_atributoscod_Rangefilterfrom = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_Rangefilterfrom");
            Ddo_funcaoapfatributos_atributoscod_Rangefilterto = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_Rangefilterto");
            Ddo_funcaoapfatributos_atributoscod_Searchbuttontext = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_Searchbuttontext");
            Ddo_funcaoapfatributos_atributosnom_Caption = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Caption");
            Ddo_funcaoapfatributos_atributosnom_Tooltip = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Tooltip");
            Ddo_funcaoapfatributos_atributosnom_Cls = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Cls");
            Ddo_funcaoapfatributos_atributosnom_Filteredtext_set = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Filteredtext_set");
            Ddo_funcaoapfatributos_atributosnom_Selectedvalue_set = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Selectedvalue_set");
            Ddo_funcaoapfatributos_atributosnom_Dropdownoptionstype = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Dropdownoptionstype");
            Ddo_funcaoapfatributos_atributosnom_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Titlecontrolidtoreplace");
            Ddo_funcaoapfatributos_atributosnom_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Includesortasc"));
            Ddo_funcaoapfatributos_atributosnom_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Includesortdsc"));
            Ddo_funcaoapfatributos_atributosnom_Sortedstatus = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Sortedstatus");
            Ddo_funcaoapfatributos_atributosnom_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Includefilter"));
            Ddo_funcaoapfatributos_atributosnom_Filtertype = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Filtertype");
            Ddo_funcaoapfatributos_atributosnom_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Filterisrange"));
            Ddo_funcaoapfatributos_atributosnom_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Includedatalist"));
            Ddo_funcaoapfatributos_atributosnom_Datalisttype = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Datalisttype");
            Ddo_funcaoapfatributos_atributosnom_Datalistproc = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Datalistproc");
            Ddo_funcaoapfatributos_atributosnom_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_funcaoapfatributos_atributosnom_Sortasc = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Sortasc");
            Ddo_funcaoapfatributos_atributosnom_Sortdsc = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Sortdsc");
            Ddo_funcaoapfatributos_atributosnom_Loadingdata = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Loadingdata");
            Ddo_funcaoapfatributos_atributosnom_Cleanfilter = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Cleanfilter");
            Ddo_funcaoapfatributos_atributosnom_Noresultsfound = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Noresultsfound");
            Ddo_funcaoapfatributos_atributosnom_Searchbuttontext = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Searchbuttontext");
            Ddo_funcaoapfatributos_funcaodadoscod_Caption = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_Caption");
            Ddo_funcaoapfatributos_funcaodadoscod_Tooltip = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_Tooltip");
            Ddo_funcaoapfatributos_funcaodadoscod_Cls = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_Cls");
            Ddo_funcaoapfatributos_funcaodadoscod_Filteredtext_set = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_Filteredtext_set");
            Ddo_funcaoapfatributos_funcaodadoscod_Filteredtextto_set = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_Filteredtextto_set");
            Ddo_funcaoapfatributos_funcaodadoscod_Dropdownoptionstype = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_Dropdownoptionstype");
            Ddo_funcaoapfatributos_funcaodadoscod_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_Titlecontrolidtoreplace");
            Ddo_funcaoapfatributos_funcaodadoscod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_Includesortasc"));
            Ddo_funcaoapfatributos_funcaodadoscod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_Includesortdsc"));
            Ddo_funcaoapfatributos_funcaodadoscod_Sortedstatus = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_Sortedstatus");
            Ddo_funcaoapfatributos_funcaodadoscod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_Includefilter"));
            Ddo_funcaoapfatributos_funcaodadoscod_Filtertype = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_Filtertype");
            Ddo_funcaoapfatributos_funcaodadoscod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_Filterisrange"));
            Ddo_funcaoapfatributos_funcaodadoscod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_Includedatalist"));
            Ddo_funcaoapfatributos_funcaodadoscod_Sortasc = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_Sortasc");
            Ddo_funcaoapfatributos_funcaodadoscod_Sortdsc = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_Sortdsc");
            Ddo_funcaoapfatributos_funcaodadoscod_Cleanfilter = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_Cleanfilter");
            Ddo_funcaoapfatributos_funcaodadoscod_Rangefilterfrom = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_Rangefilterfrom");
            Ddo_funcaoapfatributos_funcaodadoscod_Rangefilterto = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_Rangefilterto");
            Ddo_funcaoapfatributos_funcaodadoscod_Searchbuttontext = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_Searchbuttontext");
            Ddo_funcaoapfatributos_atrtabelacod_Caption = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD_Caption");
            Ddo_funcaoapfatributos_atrtabelacod_Tooltip = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD_Tooltip");
            Ddo_funcaoapfatributos_atrtabelacod_Cls = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD_Cls");
            Ddo_funcaoapfatributos_atrtabelacod_Filteredtext_set = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD_Filteredtext_set");
            Ddo_funcaoapfatributos_atrtabelacod_Filteredtextto_set = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD_Filteredtextto_set");
            Ddo_funcaoapfatributos_atrtabelacod_Dropdownoptionstype = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD_Dropdownoptionstype");
            Ddo_funcaoapfatributos_atrtabelacod_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD_Titlecontrolidtoreplace");
            Ddo_funcaoapfatributos_atrtabelacod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD_Includesortasc"));
            Ddo_funcaoapfatributos_atrtabelacod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD_Includesortdsc"));
            Ddo_funcaoapfatributos_atrtabelacod_Sortedstatus = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD_Sortedstatus");
            Ddo_funcaoapfatributos_atrtabelacod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD_Includefilter"));
            Ddo_funcaoapfatributos_atrtabelacod_Filtertype = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD_Filtertype");
            Ddo_funcaoapfatributos_atrtabelacod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD_Filterisrange"));
            Ddo_funcaoapfatributos_atrtabelacod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD_Includedatalist"));
            Ddo_funcaoapfatributos_atrtabelacod_Sortasc = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD_Sortasc");
            Ddo_funcaoapfatributos_atrtabelacod_Sortdsc = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD_Sortdsc");
            Ddo_funcaoapfatributos_atrtabelacod_Cleanfilter = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD_Cleanfilter");
            Ddo_funcaoapfatributos_atrtabelacod_Rangefilterfrom = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD_Rangefilterfrom");
            Ddo_funcaoapfatributos_atrtabelacod_Rangefilterto = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD_Rangefilterto");
            Ddo_funcaoapfatributos_atrtabelacod_Searchbuttontext = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD_Searchbuttontext");
            Ddo_funcaoapfatributos_atrtabelanom_Caption = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Caption");
            Ddo_funcaoapfatributos_atrtabelanom_Tooltip = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Tooltip");
            Ddo_funcaoapfatributos_atrtabelanom_Cls = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Cls");
            Ddo_funcaoapfatributos_atrtabelanom_Filteredtext_set = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Filteredtext_set");
            Ddo_funcaoapfatributos_atrtabelanom_Selectedvalue_set = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Selectedvalue_set");
            Ddo_funcaoapfatributos_atrtabelanom_Dropdownoptionstype = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Dropdownoptionstype");
            Ddo_funcaoapfatributos_atrtabelanom_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Titlecontrolidtoreplace");
            Ddo_funcaoapfatributos_atrtabelanom_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Includesortasc"));
            Ddo_funcaoapfatributos_atrtabelanom_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Includesortdsc"));
            Ddo_funcaoapfatributos_atrtabelanom_Sortedstatus = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Sortedstatus");
            Ddo_funcaoapfatributos_atrtabelanom_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Includefilter"));
            Ddo_funcaoapfatributos_atrtabelanom_Filtertype = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Filtertype");
            Ddo_funcaoapfatributos_atrtabelanom_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Filterisrange"));
            Ddo_funcaoapfatributos_atrtabelanom_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Includedatalist"));
            Ddo_funcaoapfatributos_atrtabelanom_Datalisttype = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Datalisttype");
            Ddo_funcaoapfatributos_atrtabelanom_Datalistproc = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Datalistproc");
            Ddo_funcaoapfatributos_atrtabelanom_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_funcaoapfatributos_atrtabelanom_Sortasc = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Sortasc");
            Ddo_funcaoapfatributos_atrtabelanom_Sortdsc = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Sortdsc");
            Ddo_funcaoapfatributos_atrtabelanom_Loadingdata = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Loadingdata");
            Ddo_funcaoapfatributos_atrtabelanom_Cleanfilter = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Cleanfilter");
            Ddo_funcaoapfatributos_atrtabelanom_Noresultsfound = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Noresultsfound");
            Ddo_funcaoapfatributos_atrtabelanom_Searchbuttontext = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Searchbuttontext");
            Ddo_funcoesapfatributos_regra_Caption = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_REGRA_Caption");
            Ddo_funcoesapfatributos_regra_Tooltip = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_REGRA_Tooltip");
            Ddo_funcoesapfatributos_regra_Cls = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_REGRA_Cls");
            Ddo_funcoesapfatributos_regra_Selectedvalue_set = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_REGRA_Selectedvalue_set");
            Ddo_funcoesapfatributos_regra_Dropdownoptionstype = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_REGRA_Dropdownoptionstype");
            Ddo_funcoesapfatributos_regra_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_REGRA_Titlecontrolidtoreplace");
            Ddo_funcoesapfatributos_regra_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCOESAPFATRIBUTOS_REGRA_Includesortasc"));
            Ddo_funcoesapfatributos_regra_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCOESAPFATRIBUTOS_REGRA_Includesortdsc"));
            Ddo_funcoesapfatributos_regra_Sortedstatus = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_REGRA_Sortedstatus");
            Ddo_funcoesapfatributos_regra_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCOESAPFATRIBUTOS_REGRA_Includefilter"));
            Ddo_funcoesapfatributos_regra_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCOESAPFATRIBUTOS_REGRA_Includedatalist"));
            Ddo_funcoesapfatributos_regra_Datalisttype = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_REGRA_Datalisttype");
            Ddo_funcoesapfatributos_regra_Datalistfixedvalues = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_REGRA_Datalistfixedvalues");
            Ddo_funcoesapfatributos_regra_Sortasc = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_REGRA_Sortasc");
            Ddo_funcoesapfatributos_regra_Sortdsc = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_REGRA_Sortdsc");
            Ddo_funcoesapfatributos_regra_Cleanfilter = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_REGRA_Cleanfilter");
            Ddo_funcoesapfatributos_regra_Searchbuttontext = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_REGRA_Searchbuttontext");
            Ddo_funcoesapfatributos_code_Caption = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_CODE_Caption");
            Ddo_funcoesapfatributos_code_Tooltip = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_CODE_Tooltip");
            Ddo_funcoesapfatributos_code_Cls = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_CODE_Cls");
            Ddo_funcoesapfatributos_code_Filteredtext_set = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_CODE_Filteredtext_set");
            Ddo_funcoesapfatributos_code_Selectedvalue_set = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_CODE_Selectedvalue_set");
            Ddo_funcoesapfatributos_code_Dropdownoptionstype = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_CODE_Dropdownoptionstype");
            Ddo_funcoesapfatributos_code_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_CODE_Titlecontrolidtoreplace");
            Ddo_funcoesapfatributos_code_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCOESAPFATRIBUTOS_CODE_Includesortasc"));
            Ddo_funcoesapfatributos_code_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCOESAPFATRIBUTOS_CODE_Includesortdsc"));
            Ddo_funcoesapfatributos_code_Sortedstatus = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_CODE_Sortedstatus");
            Ddo_funcoesapfatributos_code_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCOESAPFATRIBUTOS_CODE_Includefilter"));
            Ddo_funcoesapfatributos_code_Filtertype = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_CODE_Filtertype");
            Ddo_funcoesapfatributos_code_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_FUNCOESAPFATRIBUTOS_CODE_Filterisrange"));
            Ddo_funcoesapfatributos_code_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCOESAPFATRIBUTOS_CODE_Includedatalist"));
            Ddo_funcoesapfatributos_code_Datalisttype = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_CODE_Datalisttype");
            Ddo_funcoesapfatributos_code_Datalistproc = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_CODE_Datalistproc");
            Ddo_funcoesapfatributos_code_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_FUNCOESAPFATRIBUTOS_CODE_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_funcoesapfatributos_code_Sortasc = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_CODE_Sortasc");
            Ddo_funcoesapfatributos_code_Sortdsc = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_CODE_Sortdsc");
            Ddo_funcoesapfatributos_code_Loadingdata = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_CODE_Loadingdata");
            Ddo_funcoesapfatributos_code_Cleanfilter = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_CODE_Cleanfilter");
            Ddo_funcoesapfatributos_code_Noresultsfound = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_CODE_Noresultsfound");
            Ddo_funcoesapfatributos_code_Searchbuttontext = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_CODE_Searchbuttontext");
            Ddo_funcoesapfatributos_nome_Caption = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_NOME_Caption");
            Ddo_funcoesapfatributos_nome_Tooltip = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_NOME_Tooltip");
            Ddo_funcoesapfatributos_nome_Cls = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_NOME_Cls");
            Ddo_funcoesapfatributos_nome_Filteredtext_set = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_NOME_Filteredtext_set");
            Ddo_funcoesapfatributos_nome_Selectedvalue_set = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_NOME_Selectedvalue_set");
            Ddo_funcoesapfatributos_nome_Dropdownoptionstype = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_NOME_Dropdownoptionstype");
            Ddo_funcoesapfatributos_nome_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_NOME_Titlecontrolidtoreplace");
            Ddo_funcoesapfatributos_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCOESAPFATRIBUTOS_NOME_Includesortasc"));
            Ddo_funcoesapfatributos_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCOESAPFATRIBUTOS_NOME_Includesortdsc"));
            Ddo_funcoesapfatributos_nome_Sortedstatus = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_NOME_Sortedstatus");
            Ddo_funcoesapfatributos_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCOESAPFATRIBUTOS_NOME_Includefilter"));
            Ddo_funcoesapfatributos_nome_Filtertype = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_NOME_Filtertype");
            Ddo_funcoesapfatributos_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_FUNCOESAPFATRIBUTOS_NOME_Filterisrange"));
            Ddo_funcoesapfatributos_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCOESAPFATRIBUTOS_NOME_Includedatalist"));
            Ddo_funcoesapfatributos_nome_Datalisttype = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_NOME_Datalisttype");
            Ddo_funcoesapfatributos_nome_Datalistproc = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_NOME_Datalistproc");
            Ddo_funcoesapfatributos_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_FUNCOESAPFATRIBUTOS_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_funcoesapfatributos_nome_Sortasc = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_NOME_Sortasc");
            Ddo_funcoesapfatributos_nome_Sortdsc = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_NOME_Sortdsc");
            Ddo_funcoesapfatributos_nome_Loadingdata = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_NOME_Loadingdata");
            Ddo_funcoesapfatributos_nome_Cleanfilter = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_NOME_Cleanfilter");
            Ddo_funcoesapfatributos_nome_Noresultsfound = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_NOME_Noresultsfound");
            Ddo_funcoesapfatributos_nome_Searchbuttontext = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_NOME_Searchbuttontext");
            Ddo_funcoesapfatributos_descricao_Caption = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Caption");
            Ddo_funcoesapfatributos_descricao_Tooltip = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Tooltip");
            Ddo_funcoesapfatributos_descricao_Cls = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Cls");
            Ddo_funcoesapfatributos_descricao_Filteredtext_set = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Filteredtext_set");
            Ddo_funcoesapfatributos_descricao_Selectedvalue_set = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Selectedvalue_set");
            Ddo_funcoesapfatributos_descricao_Dropdownoptionstype = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Dropdownoptionstype");
            Ddo_funcoesapfatributos_descricao_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_funcoesapfatributos_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Includesortasc"));
            Ddo_funcoesapfatributos_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Includesortdsc"));
            Ddo_funcoesapfatributos_descricao_Sortedstatus = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Sortedstatus");
            Ddo_funcoesapfatributos_descricao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Includefilter"));
            Ddo_funcoesapfatributos_descricao_Filtertype = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Filtertype");
            Ddo_funcoesapfatributos_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Filterisrange"));
            Ddo_funcoesapfatributos_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Includedatalist"));
            Ddo_funcoesapfatributos_descricao_Datalisttype = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Datalisttype");
            Ddo_funcoesapfatributos_descricao_Datalistproc = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Datalistproc");
            Ddo_funcoesapfatributos_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_funcoesapfatributos_descricao_Sortasc = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Sortasc");
            Ddo_funcoesapfatributos_descricao_Sortdsc = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Sortdsc");
            Ddo_funcoesapfatributos_descricao_Loadingdata = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Loadingdata");
            Ddo_funcoesapfatributos_descricao_Cleanfilter = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Cleanfilter");
            Ddo_funcoesapfatributos_descricao_Noresultsfound = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Noresultsfound");
            Ddo_funcoesapfatributos_descricao_Searchbuttontext = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_funcaoapf_codigo_Activeeventkey = cgiGet( "DDO_FUNCAOAPF_CODIGO_Activeeventkey");
            Ddo_funcaoapf_codigo_Filteredtext_get = cgiGet( "DDO_FUNCAOAPF_CODIGO_Filteredtext_get");
            Ddo_funcaoapf_codigo_Filteredtextto_get = cgiGet( "DDO_FUNCAOAPF_CODIGO_Filteredtextto_get");
            Ddo_funcaoapf_nome_Activeeventkey = cgiGet( "DDO_FUNCAOAPF_NOME_Activeeventkey");
            Ddo_funcaoapf_nome_Filteredtext_get = cgiGet( "DDO_FUNCAOAPF_NOME_Filteredtext_get");
            Ddo_funcaoapf_nome_Selectedvalue_get = cgiGet( "DDO_FUNCAOAPF_NOME_Selectedvalue_get");
            Ddo_funcaoapfatributos_atributoscod_Activeeventkey = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_Activeeventkey");
            Ddo_funcaoapfatributos_atributoscod_Filteredtext_get = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_Filteredtext_get");
            Ddo_funcaoapfatributos_atributoscod_Filteredtextto_get = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_Filteredtextto_get");
            Ddo_funcaoapfatributos_atributosnom_Activeeventkey = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Activeeventkey");
            Ddo_funcaoapfatributos_atributosnom_Filteredtext_get = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Filteredtext_get");
            Ddo_funcaoapfatributos_atributosnom_Selectedvalue_get = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Selectedvalue_get");
            Ddo_funcaoapfatributos_funcaodadoscod_Activeeventkey = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_Activeeventkey");
            Ddo_funcaoapfatributos_funcaodadoscod_Filteredtext_get = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_Filteredtext_get");
            Ddo_funcaoapfatributos_funcaodadoscod_Filteredtextto_get = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_Filteredtextto_get");
            Ddo_funcaoapfatributos_atrtabelacod_Activeeventkey = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD_Activeeventkey");
            Ddo_funcaoapfatributos_atrtabelacod_Filteredtext_get = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD_Filteredtext_get");
            Ddo_funcaoapfatributos_atrtabelacod_Filteredtextto_get = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD_Filteredtextto_get");
            Ddo_funcaoapfatributos_atrtabelanom_Activeeventkey = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Activeeventkey");
            Ddo_funcaoapfatributos_atrtabelanom_Filteredtext_get = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Filteredtext_get");
            Ddo_funcaoapfatributos_atrtabelanom_Selectedvalue_get = cgiGet( "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Selectedvalue_get");
            Ddo_funcoesapfatributos_regra_Activeeventkey = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_REGRA_Activeeventkey");
            Ddo_funcoesapfatributos_regra_Selectedvalue_get = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_REGRA_Selectedvalue_get");
            Ddo_funcoesapfatributos_code_Activeeventkey = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_CODE_Activeeventkey");
            Ddo_funcoesapfatributos_code_Filteredtext_get = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_CODE_Filteredtext_get");
            Ddo_funcoesapfatributos_code_Selectedvalue_get = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_CODE_Selectedvalue_get");
            Ddo_funcoesapfatributos_nome_Activeeventkey = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_NOME_Activeeventkey");
            Ddo_funcoesapfatributos_nome_Filteredtext_get = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_NOME_Filteredtext_get");
            Ddo_funcoesapfatributos_nome_Selectedvalue_get = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_NOME_Selectedvalue_get");
            Ddo_funcoesapfatributos_descricao_Activeeventkey = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Activeeventkey");
            Ddo_funcoesapfatributos_descricao_Filteredtext_get = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Filteredtext_get");
            Ddo_funcoesapfatributos_descricao_Selectedvalue_get = cgiGet( "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV15OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV17DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_NOME1"), AV18FuncaoAPF_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1"), AV19FuncaoAPFAtributos_AtributosNom1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV22DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_NOME2"), AV23FuncaoAPF_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2"), AV24FuncaoAPFAtributos_AtributosNom2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV26DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV27DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_NOME3"), AV28FuncaoAPF_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM3"), AV29FuncaoAPFAtributos_AtributosNom3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV25DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCAOAPF_CODIGO"), ",", ".") != Convert.ToDecimal( AV35TFFuncaoAPF_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCAOAPF_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV36TFFuncaoAPF_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAOAPF_NOME"), AV39TFFuncaoAPF_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAOAPF_NOME_SEL"), AV40TFFuncaoAPF_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD"), ",", ".") != Convert.ToDecimal( AV43TFFuncaoAPFAtributos_AtributosCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_TO"), ",", ".") != Convert.ToDecimal( AV44TFFuncaoAPFAtributos_AtributosCod_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM"), AV47TFFuncaoAPFAtributos_AtributosNom) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL"), AV48TFFuncaoAPFAtributos_AtributosNom_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD"), ",", ".") != Convert.ToDecimal( AV51TFFuncaoAPFAtributos_FuncaoDadosCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_TO"), ",", ".") != Convert.ToDecimal( AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD"), ",", ".") != Convert.ToDecimal( AV55TFFuncaoAPFAtributos_AtrTabelaCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD_TO"), ",", ".") != Convert.ToDecimal( AV56TFFuncaoAPFAtributos_AtrTabelaCod_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM"), AV59TFFuncaoAPFAtributos_AtrTabelaNom) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL"), AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCOESAPFATRIBUTOS_REGRA_SEL"), ",", ".") != Convert.ToDecimal( AV63TFFuncoesAPFAtributos_Regra_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCOESAPFATRIBUTOS_CODE"), AV66TFFuncoesAPFAtributos_Code) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCOESAPFATRIBUTOS_CODE_SEL"), AV67TFFuncoesAPFAtributos_Code_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCOESAPFATRIBUTOS_NOME"), AV70TFFuncoesAPFAtributos_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCOESAPFATRIBUTOS_NOME_SEL"), AV71TFFuncoesAPFAtributos_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCOESAPFATRIBUTOS_DESCRICAO"), AV74TFFuncoesAPFAtributos_Descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCOESAPFATRIBUTOS_DESCRICAO_SEL"), AV75TFFuncoesAPFAtributos_Descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E339N2 */
         E339N2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E339N2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV16DynamicFiltersSelector1 = "FUNCAOAPF_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV21DynamicFiltersSelector2 = "FUNCAOAPF_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersSelector3 = "FUNCAOAPF_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTffuncaoapf_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaoapf_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapf_codigo_Visible), 5, 0)));
         edtavTffuncaoapf_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaoapf_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapf_codigo_to_Visible), 5, 0)));
         edtavTffuncaoapf_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaoapf_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapf_nome_Visible), 5, 0)));
         edtavTffuncaoapf_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaoapf_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapf_nome_sel_Visible), 5, 0)));
         edtavTffuncaoapfatributos_atributoscod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaoapfatributos_atributoscod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapfatributos_atributoscod_Visible), 5, 0)));
         edtavTffuncaoapfatributos_atributoscod_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaoapfatributos_atributoscod_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapfatributos_atributoscod_to_Visible), 5, 0)));
         edtavTffuncaoapfatributos_atributosnom_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaoapfatributos_atributosnom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapfatributos_atributosnom_Visible), 5, 0)));
         edtavTffuncaoapfatributos_atributosnom_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaoapfatributos_atributosnom_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapfatributos_atributosnom_sel_Visible), 5, 0)));
         edtavTffuncaoapfatributos_funcaodadoscod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaoapfatributos_funcaodadoscod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapfatributos_funcaodadoscod_Visible), 5, 0)));
         edtavTffuncaoapfatributos_funcaodadoscod_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaoapfatributos_funcaodadoscod_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapfatributos_funcaodadoscod_to_Visible), 5, 0)));
         edtavTffuncaoapfatributos_atrtabelacod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaoapfatributos_atrtabelacod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapfatributos_atrtabelacod_Visible), 5, 0)));
         edtavTffuncaoapfatributos_atrtabelacod_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaoapfatributos_atrtabelacod_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapfatributos_atrtabelacod_to_Visible), 5, 0)));
         edtavTffuncaoapfatributos_atrtabelanom_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaoapfatributos_atrtabelanom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapfatributos_atrtabelanom_Visible), 5, 0)));
         edtavTffuncaoapfatributos_atrtabelanom_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaoapfatributos_atrtabelanom_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapfatributos_atrtabelanom_sel_Visible), 5, 0)));
         edtavTffuncoesapfatributos_regra_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncoesapfatributos_regra_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncoesapfatributos_regra_sel_Visible), 5, 0)));
         edtavTffuncoesapfatributos_code_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncoesapfatributos_code_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncoesapfatributos_code_Visible), 5, 0)));
         edtavTffuncoesapfatributos_code_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncoesapfatributos_code_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncoesapfatributos_code_sel_Visible), 5, 0)));
         edtavTffuncoesapfatributos_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncoesapfatributos_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncoesapfatributos_nome_Visible), 5, 0)));
         edtavTffuncoesapfatributos_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncoesapfatributos_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncoesapfatributos_nome_sel_Visible), 5, 0)));
         edtavTffuncoesapfatributos_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncoesapfatributos_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncoesapfatributos_descricao_Visible), 5, 0)));
         edtavTffuncoesapfatributos_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncoesapfatributos_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncoesapfatributos_descricao_sel_Visible), 5, 0)));
         Ddo_funcaoapf_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoAPF_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_codigo_Internalname, "TitleControlIdToReplace", Ddo_funcaoapf_codigo_Titlecontrolidtoreplace);
         AV37ddo_FuncaoAPF_CodigoTitleControlIdToReplace = Ddo_funcaoapf_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_FuncaoAPF_CodigoTitleControlIdToReplace", AV37ddo_FuncaoAPF_CodigoTitleControlIdToReplace);
         edtavDdo_funcaoapf_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcaoapf_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaoapf_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaoapf_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoAPF_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_nome_Internalname, "TitleControlIdToReplace", Ddo_funcaoapf_nome_Titlecontrolidtoreplace);
         AV41ddo_FuncaoAPF_NomeTitleControlIdToReplace = Ddo_funcaoapf_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_FuncaoAPF_NomeTitleControlIdToReplace", AV41ddo_FuncaoAPF_NomeTitleControlIdToReplace);
         edtavDdo_funcaoapf_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcaoapf_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaoapf_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaoapfatributos_atributoscod_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoAPFAtributos_AtributosCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfatributos_atributoscod_Internalname, "TitleControlIdToReplace", Ddo_funcaoapfatributos_atributoscod_Titlecontrolidtoreplace);
         AV45ddo_FuncaoAPFAtributos_AtributosCodTitleControlIdToReplace = Ddo_funcaoapfatributos_atributoscod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_FuncaoAPFAtributos_AtributosCodTitleControlIdToReplace", AV45ddo_FuncaoAPFAtributos_AtributosCodTitleControlIdToReplace);
         edtavDdo_funcaoapfatributos_atributoscodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcaoapfatributos_atributoscodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaoapfatributos_atributoscodtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaoapfatributos_atributosnom_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoAPFAtributos_AtributosNom";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfatributos_atributosnom_Internalname, "TitleControlIdToReplace", Ddo_funcaoapfatributos_atributosnom_Titlecontrolidtoreplace);
         AV49ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace = Ddo_funcaoapfatributos_atributosnom_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace", AV49ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace);
         edtavDdo_funcaoapfatributos_atributosnomtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcaoapfatributos_atributosnomtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaoapfatributos_atributosnomtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaoapfatributos_funcaodadoscod_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoAPFAtributos_FuncaoDadosCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfatributos_funcaodadoscod_Internalname, "TitleControlIdToReplace", Ddo_funcaoapfatributos_funcaodadoscod_Titlecontrolidtoreplace);
         AV53ddo_FuncaoAPFAtributos_FuncaoDadosCodTitleControlIdToReplace = Ddo_funcaoapfatributos_funcaodadoscod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_FuncaoAPFAtributos_FuncaoDadosCodTitleControlIdToReplace", AV53ddo_FuncaoAPFAtributos_FuncaoDadosCodTitleControlIdToReplace);
         edtavDdo_funcaoapfatributos_funcaodadoscodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcaoapfatributos_funcaodadoscodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaoapfatributos_funcaodadoscodtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaoapfatributos_atrtabelacod_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoAPFAtributos_AtrTabelaCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfatributos_atrtabelacod_Internalname, "TitleControlIdToReplace", Ddo_funcaoapfatributos_atrtabelacod_Titlecontrolidtoreplace);
         AV57ddo_FuncaoAPFAtributos_AtrTabelaCodTitleControlIdToReplace = Ddo_funcaoapfatributos_atrtabelacod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_FuncaoAPFAtributos_AtrTabelaCodTitleControlIdToReplace", AV57ddo_FuncaoAPFAtributos_AtrTabelaCodTitleControlIdToReplace);
         edtavDdo_funcaoapfatributos_atrtabelacodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcaoapfatributos_atrtabelacodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaoapfatributos_atrtabelacodtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaoapfatributos_atrtabelanom_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoAPFAtributos_AtrTabelaNom";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfatributos_atrtabelanom_Internalname, "TitleControlIdToReplace", Ddo_funcaoapfatributos_atrtabelanom_Titlecontrolidtoreplace);
         AV61ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace = Ddo_funcaoapfatributos_atrtabelanom_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace", AV61ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace);
         edtavDdo_funcaoapfatributos_atrtabelanomtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcaoapfatributos_atrtabelanomtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaoapfatributos_atrtabelanomtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcoesapfatributos_regra_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncoesAPFAtributos_Regra";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcoesapfatributos_regra_Internalname, "TitleControlIdToReplace", Ddo_funcoesapfatributos_regra_Titlecontrolidtoreplace);
         AV64ddo_FuncoesAPFAtributos_RegraTitleControlIdToReplace = Ddo_funcoesapfatributos_regra_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64ddo_FuncoesAPFAtributos_RegraTitleControlIdToReplace", AV64ddo_FuncoesAPFAtributos_RegraTitleControlIdToReplace);
         edtavDdo_funcoesapfatributos_regratitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcoesapfatributos_regratitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcoesapfatributos_regratitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcoesapfatributos_code_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncoesAPFAtributos_Code";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcoesapfatributos_code_Internalname, "TitleControlIdToReplace", Ddo_funcoesapfatributos_code_Titlecontrolidtoreplace);
         AV68ddo_FuncoesAPFAtributos_CodeTitleControlIdToReplace = Ddo_funcoesapfatributos_code_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ddo_FuncoesAPFAtributos_CodeTitleControlIdToReplace", AV68ddo_FuncoesAPFAtributos_CodeTitleControlIdToReplace);
         edtavDdo_funcoesapfatributos_codetitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcoesapfatributos_codetitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcoesapfatributos_codetitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcoesapfatributos_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncoesAPFAtributos_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcoesapfatributos_nome_Internalname, "TitleControlIdToReplace", Ddo_funcoesapfatributos_nome_Titlecontrolidtoreplace);
         AV72ddo_FuncoesAPFAtributos_NomeTitleControlIdToReplace = Ddo_funcoesapfatributos_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72ddo_FuncoesAPFAtributos_NomeTitleControlIdToReplace", AV72ddo_FuncoesAPFAtributos_NomeTitleControlIdToReplace);
         edtavDdo_funcoesapfatributos_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcoesapfatributos_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcoesapfatributos_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcoesapfatributos_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncoesAPFAtributos_Descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcoesapfatributos_descricao_Internalname, "TitleControlIdToReplace", Ddo_funcoesapfatributos_descricao_Titlecontrolidtoreplace);
         AV76ddo_FuncoesAPFAtributos_DescricaoTitleControlIdToReplace = Ddo_funcoesapfatributos_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76ddo_FuncoesAPFAtributos_DescricaoTitleControlIdToReplace", AV76ddo_FuncoesAPFAtributos_DescricaoTitleControlIdToReplace);
         edtavDdo_funcoesapfatributos_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcoesapfatributos_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcoesapfatributos_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Selecione Atributos das Fun��es de APF";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "de Transa��o", 0);
         cmbavOrderedby.addItem("2", "Fun��o de Transa��o", 0);
         cmbavOrderedby.addItem("3", "Atributo", 0);
         cmbavOrderedby.addItem("4", "Atributo", 0);
         cmbavOrderedby.addItem("5", "Dados", 0);
         cmbavOrderedby.addItem("6", "Tabela", 0);
         cmbavOrderedby.addItem("7", "Tabela", 0);
         cmbavOrderedby.addItem("8", "de Neg�cio", 0);
         cmbavOrderedby.addItem("9", "Codigo", 0);
         cmbavOrderedby.addItem("10", "Nome", 0);
         cmbavOrderedby.addItem("11", "Descri��o", 0);
         if ( AV14OrderedBy < 1 )
         {
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV77DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV77DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E349N2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV34FuncaoAPF_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38FuncaoAPF_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV42FuncaoAPFAtributos_AtributosCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV46FuncaoAPFAtributos_AtributosNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV50FuncaoAPFAtributos_FuncaoDadosCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV54FuncaoAPFAtributos_AtrTabelaCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV58FuncaoAPFAtributos_AtrTabelaNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV62FuncoesAPFAtributos_RegraTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV65FuncoesAPFAtributos_CodeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV69FuncoesAPFAtributos_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV73FuncoesAPFAtributos_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         cmbavDynamicfiltersoperator1.removeAllItems();
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         if ( AV20DynamicFiltersEnabled2 )
         {
            cmbavDynamicfiltersoperator2.removeAllItems();
            if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "FUNCAOAPF_NOME") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            if ( AV25DynamicFiltersEnabled3 )
            {
               cmbavDynamicfiltersoperator3.removeAllItems();
               if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "FUNCAOAPF_NOME") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
               else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
            }
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtFuncaoAPF_Codigo_Titleformat = 2;
         edtFuncaoAPF_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Fun��o de Transa��o", AV37ddo_FuncaoAPF_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPF_Codigo_Internalname, "Title", edtFuncaoAPF_Codigo_Title);
         edtFuncaoAPF_Nome_Titleformat = 2;
         edtFuncaoAPF_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Fun��o de Transa��o", AV41ddo_FuncaoAPF_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPF_Nome_Internalname, "Title", edtFuncaoAPF_Nome_Title);
         edtFuncaoAPFAtributos_AtributosCod_Titleformat = 2;
         edtFuncaoAPFAtributos_AtributosCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Atributo", AV45ddo_FuncaoAPFAtributos_AtributosCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFAtributos_AtributosCod_Internalname, "Title", edtFuncaoAPFAtributos_AtributosCod_Title);
         edtFuncaoAPFAtributos_AtributosNom_Titleformat = 2;
         edtFuncaoAPFAtributos_AtributosNom_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Atributo", AV49ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFAtributos_AtributosNom_Internalname, "Title", edtFuncaoAPFAtributos_AtributosNom_Title);
         edtFuncaoAPFAtributos_FuncaoDadosCod_Titleformat = 2;
         edtFuncaoAPFAtributos_FuncaoDadosCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Dados", AV53ddo_FuncaoAPFAtributos_FuncaoDadosCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFAtributos_FuncaoDadosCod_Internalname, "Title", edtFuncaoAPFAtributos_FuncaoDadosCod_Title);
         edtFuncaoAPFAtributos_AtrTabelaCod_Titleformat = 2;
         edtFuncaoAPFAtributos_AtrTabelaCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Tabela", AV57ddo_FuncaoAPFAtributos_AtrTabelaCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFAtributos_AtrTabelaCod_Internalname, "Title", edtFuncaoAPFAtributos_AtrTabelaCod_Title);
         edtFuncaoAPFAtributos_AtrTabelaNom_Titleformat = 2;
         edtFuncaoAPFAtributos_AtrTabelaNom_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Tabela", AV61ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFAtributos_AtrTabelaNom_Internalname, "Title", edtFuncaoAPFAtributos_AtrTabelaNom_Title);
         chkFuncoesAPFAtributos_Regra_Titleformat = 2;
         chkFuncoesAPFAtributos_Regra.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "de Neg�cio", AV64ddo_FuncoesAPFAtributos_RegraTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkFuncoesAPFAtributos_Regra_Internalname, "Title", chkFuncoesAPFAtributos_Regra.Title.Text);
         edtFuncoesAPFAtributos_Code_Titleformat = 2;
         edtFuncoesAPFAtributos_Code_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Codigo", AV68ddo_FuncoesAPFAtributos_CodeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncoesAPFAtributos_Code_Internalname, "Title", edtFuncoesAPFAtributos_Code_Title);
         edtFuncoesAPFAtributos_Nome_Titleformat = 2;
         edtFuncoesAPFAtributos_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV72ddo_FuncoesAPFAtributos_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncoesAPFAtributos_Nome_Internalname, "Title", edtFuncoesAPFAtributos_Nome_Title);
         edtFuncoesAPFAtributos_Descricao_Titleformat = 2;
         edtFuncoesAPFAtributos_Descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Descri��o", AV76ddo_FuncoesAPFAtributos_DescricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncoesAPFAtributos_Descricao_Internalname, "Title", edtFuncoesAPFAtributos_Descricao_Title);
         AV79GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV79GridCurrentPage), 10, 0)));
         AV80GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV80GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV34FuncaoAPF_CodigoTitleFilterData", AV34FuncaoAPF_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV38FuncaoAPF_NomeTitleFilterData", AV38FuncaoAPF_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV42FuncaoAPFAtributos_AtributosCodTitleFilterData", AV42FuncaoAPFAtributos_AtributosCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV46FuncaoAPFAtributos_AtributosNomTitleFilterData", AV46FuncaoAPFAtributos_AtributosNomTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV50FuncaoAPFAtributos_FuncaoDadosCodTitleFilterData", AV50FuncaoAPFAtributos_FuncaoDadosCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV54FuncaoAPFAtributos_AtrTabelaCodTitleFilterData", AV54FuncaoAPFAtributos_AtrTabelaCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV58FuncaoAPFAtributos_AtrTabelaNomTitleFilterData", AV58FuncaoAPFAtributos_AtrTabelaNomTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV62FuncoesAPFAtributos_RegraTitleFilterData", AV62FuncoesAPFAtributos_RegraTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV65FuncoesAPFAtributos_CodeTitleFilterData", AV65FuncoesAPFAtributos_CodeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV69FuncoesAPFAtributos_NomeTitleFilterData", AV69FuncoesAPFAtributos_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV73FuncoesAPFAtributos_DescricaoTitleFilterData", AV73FuncoesAPFAtributos_DescricaoTitleFilterData);
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11GridState", AV11GridState);
      }

      protected void E119N2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV78PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV78PageToGo) ;
         }
      }

      protected void E129N2( )
      {
         /* Ddo_funcaoapf_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaoapf_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_funcaoapf_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_codigo_Internalname, "SortedStatus", Ddo_funcaoapf_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapf_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_funcaoapf_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_codigo_Internalname, "SortedStatus", Ddo_funcaoapf_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapf_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV35TFFuncaoAPF_Codigo = (int)(NumberUtil.Val( Ddo_funcaoapf_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFFuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFFuncaoAPF_Codigo), 6, 0)));
            AV36TFFuncaoAPF_Codigo_To = (int)(NumberUtil.Val( Ddo_funcaoapf_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFFuncaoAPF_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFFuncaoAPF_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E139N2( )
      {
         /* Ddo_funcaoapf_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaoapf_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_funcaoapf_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_nome_Internalname, "SortedStatus", Ddo_funcaoapf_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapf_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_funcaoapf_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_nome_Internalname, "SortedStatus", Ddo_funcaoapf_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapf_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV39TFFuncaoAPF_Nome = Ddo_funcaoapf_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFFuncaoAPF_Nome", AV39TFFuncaoAPF_Nome);
            AV40TFFuncaoAPF_Nome_Sel = Ddo_funcaoapf_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFFuncaoAPF_Nome_Sel", AV40TFFuncaoAPF_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E149N2( )
      {
         /* Ddo_funcaoapfatributos_atributoscod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaoapfatributos_atributoscod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_funcaoapfatributos_atributoscod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfatributos_atributoscod_Internalname, "SortedStatus", Ddo_funcaoapfatributos_atributoscod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapfatributos_atributoscod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_funcaoapfatributos_atributoscod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfatributos_atributoscod_Internalname, "SortedStatus", Ddo_funcaoapfatributos_atributoscod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapfatributos_atributoscod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV43TFFuncaoAPFAtributos_AtributosCod = (int)(NumberUtil.Val( Ddo_funcaoapfatributos_atributoscod_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFFuncaoAPFAtributos_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFFuncaoAPFAtributos_AtributosCod), 6, 0)));
            AV44TFFuncaoAPFAtributos_AtributosCod_To = (int)(NumberUtil.Val( Ddo_funcaoapfatributos_atributoscod_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFFuncaoAPFAtributos_AtributosCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFFuncaoAPFAtributos_AtributosCod_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E159N2( )
      {
         /* Ddo_funcaoapfatributos_atributosnom_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaoapfatributos_atributosnom_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_funcaoapfatributos_atributosnom_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfatributos_atributosnom_Internalname, "SortedStatus", Ddo_funcaoapfatributos_atributosnom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapfatributos_atributosnom_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_funcaoapfatributos_atributosnom_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfatributos_atributosnom_Internalname, "SortedStatus", Ddo_funcaoapfatributos_atributosnom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapfatributos_atributosnom_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV47TFFuncaoAPFAtributos_AtributosNom = Ddo_funcaoapfatributos_atributosnom_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFFuncaoAPFAtributos_AtributosNom", AV47TFFuncaoAPFAtributos_AtributosNom);
            AV48TFFuncaoAPFAtributos_AtributosNom_Sel = Ddo_funcaoapfatributos_atributosnom_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFFuncaoAPFAtributos_AtributosNom_Sel", AV48TFFuncaoAPFAtributos_AtributosNom_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E169N2( )
      {
         /* Ddo_funcaoapfatributos_funcaodadoscod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaoapfatributos_funcaodadoscod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_funcaoapfatributos_funcaodadoscod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfatributos_funcaodadoscod_Internalname, "SortedStatus", Ddo_funcaoapfatributos_funcaodadoscod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapfatributos_funcaodadoscod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_funcaoapfatributos_funcaodadoscod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfatributos_funcaodadoscod_Internalname, "SortedStatus", Ddo_funcaoapfatributos_funcaodadoscod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapfatributos_funcaodadoscod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV51TFFuncaoAPFAtributos_FuncaoDadosCod = (int)(NumberUtil.Val( Ddo_funcaoapfatributos_funcaodadoscod_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFFuncaoAPFAtributos_FuncaoDadosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51TFFuncaoAPFAtributos_FuncaoDadosCod), 6, 0)));
            AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To = (int)(NumberUtil.Val( Ddo_funcaoapfatributos_funcaodadoscod_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E179N2( )
      {
         /* Ddo_funcaoapfatributos_atrtabelacod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaoapfatributos_atrtabelacod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_funcaoapfatributos_atrtabelacod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfatributos_atrtabelacod_Internalname, "SortedStatus", Ddo_funcaoapfatributos_atrtabelacod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapfatributos_atrtabelacod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_funcaoapfatributos_atrtabelacod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfatributos_atrtabelacod_Internalname, "SortedStatus", Ddo_funcaoapfatributos_atrtabelacod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapfatributos_atrtabelacod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV55TFFuncaoAPFAtributos_AtrTabelaCod = (int)(NumberUtil.Val( Ddo_funcaoapfatributos_atrtabelacod_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFFuncaoAPFAtributos_AtrTabelaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFFuncaoAPFAtributos_AtrTabelaCod), 6, 0)));
            AV56TFFuncaoAPFAtributos_AtrTabelaCod_To = (int)(NumberUtil.Val( Ddo_funcaoapfatributos_atrtabelacod_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFFuncaoAPFAtributos_AtrTabelaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56TFFuncaoAPFAtributos_AtrTabelaCod_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E189N2( )
      {
         /* Ddo_funcaoapfatributos_atrtabelanom_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaoapfatributos_atrtabelanom_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_funcaoapfatributos_atrtabelanom_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfatributos_atrtabelanom_Internalname, "SortedStatus", Ddo_funcaoapfatributos_atrtabelanom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapfatributos_atrtabelanom_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_funcaoapfatributos_atrtabelanom_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfatributos_atrtabelanom_Internalname, "SortedStatus", Ddo_funcaoapfatributos_atrtabelanom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapfatributos_atrtabelanom_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV59TFFuncaoAPFAtributos_AtrTabelaNom = Ddo_funcaoapfatributos_atrtabelanom_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFFuncaoAPFAtributos_AtrTabelaNom", AV59TFFuncaoAPFAtributos_AtrTabelaNom);
            AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel = Ddo_funcaoapfatributos_atrtabelanom_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel", AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E199N2( )
      {
         /* Ddo_funcoesapfatributos_regra_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcoesapfatributos_regra_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_funcoesapfatributos_regra_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcoesapfatributos_regra_Internalname, "SortedStatus", Ddo_funcoesapfatributos_regra_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcoesapfatributos_regra_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_funcoesapfatributos_regra_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcoesapfatributos_regra_Internalname, "SortedStatus", Ddo_funcoesapfatributos_regra_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcoesapfatributos_regra_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV63TFFuncoesAPFAtributos_Regra_Sel = (short)(NumberUtil.Val( Ddo_funcoesapfatributos_regra_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFFuncoesAPFAtributos_Regra_Sel", StringUtil.Str( (decimal)(AV63TFFuncoesAPFAtributos_Regra_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E209N2( )
      {
         /* Ddo_funcoesapfatributos_code_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcoesapfatributos_code_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 9;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_funcoesapfatributos_code_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcoesapfatributos_code_Internalname, "SortedStatus", Ddo_funcoesapfatributos_code_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcoesapfatributos_code_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 9;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_funcoesapfatributos_code_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcoesapfatributos_code_Internalname, "SortedStatus", Ddo_funcoesapfatributos_code_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcoesapfatributos_code_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV66TFFuncoesAPFAtributos_Code = Ddo_funcoesapfatributos_code_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFFuncoesAPFAtributos_Code", AV66TFFuncoesAPFAtributos_Code);
            AV67TFFuncoesAPFAtributos_Code_Sel = Ddo_funcoesapfatributos_code_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFFuncoesAPFAtributos_Code_Sel", AV67TFFuncoesAPFAtributos_Code_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E219N2( )
      {
         /* Ddo_funcoesapfatributos_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcoesapfatributos_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 10;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_funcoesapfatributos_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcoesapfatributos_nome_Internalname, "SortedStatus", Ddo_funcoesapfatributos_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcoesapfatributos_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 10;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_funcoesapfatributos_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcoesapfatributos_nome_Internalname, "SortedStatus", Ddo_funcoesapfatributos_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcoesapfatributos_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV70TFFuncoesAPFAtributos_Nome = Ddo_funcoesapfatributos_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFFuncoesAPFAtributos_Nome", AV70TFFuncoesAPFAtributos_Nome);
            AV71TFFuncoesAPFAtributos_Nome_Sel = Ddo_funcoesapfatributos_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFFuncoesAPFAtributos_Nome_Sel", AV71TFFuncoesAPFAtributos_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E229N2( )
      {
         /* Ddo_funcoesapfatributos_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcoesapfatributos_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 11;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_funcoesapfatributos_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcoesapfatributos_descricao_Internalname, "SortedStatus", Ddo_funcoesapfatributos_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcoesapfatributos_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 11;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_funcoesapfatributos_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcoesapfatributos_descricao_Internalname, "SortedStatus", Ddo_funcoesapfatributos_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcoesapfatributos_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV74TFFuncoesAPFAtributos_Descricao = Ddo_funcoesapfatributos_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFFuncoesAPFAtributos_Descricao", AV74TFFuncoesAPFAtributos_Descricao);
            AV75TFFuncoesAPFAtributos_Descricao_Sel = Ddo_funcoesapfatributos_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFFuncoesAPFAtributos_Descricao_Sel", AV75TFFuncoesAPFAtributos_Descricao_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E359N2( )
      {
         /* Grid_Load Routine */
         AV32Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV32Select);
         AV83Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 83;
         }
         sendrow_832( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_83_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(83, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E369N2 */
         E369N2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E369N2( )
      {
         /* Enter Routine */
         AV7InOutFuncaoAPF_Codigo = A165FuncaoAPF_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutFuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutFuncaoAPF_Codigo), 6, 0)));
         AV8InOutFuncaoAPFAtributos_AtributosCod = A364FuncaoAPFAtributos_AtributosCod;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutFuncaoAPFAtributos_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8InOutFuncaoAPFAtributos_AtributosCod), 6, 0)));
         AV9InOutFuncaoAPF_Nome = A166FuncaoAPF_Nome;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9InOutFuncaoAPF_Nome", AV9InOutFuncaoAPF_Nome);
         context.setWebReturnParms(new Object[] {(int)AV7InOutFuncaoAPF_Codigo,(int)AV8InOutFuncaoAPFAtributos_AtributosCod,(String)AV9InOutFuncaoAPF_Nome});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E239N2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E289N2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV20DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18FuncaoAPF_Nome1, AV19FuncaoAPFAtributos_AtributosNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23FuncaoAPF_Nome2, AV24FuncaoAPFAtributos_AtributosNom2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28FuncaoAPF_Nome3, AV29FuncaoAPFAtributos_AtributosNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV35TFFuncaoAPF_Codigo, AV36TFFuncaoAPF_Codigo_To, AV39TFFuncaoAPF_Nome, AV40TFFuncaoAPF_Nome_Sel, AV43TFFuncaoAPFAtributos_AtributosCod, AV44TFFuncaoAPFAtributos_AtributosCod_To, AV47TFFuncaoAPFAtributos_AtributosNom, AV48TFFuncaoAPFAtributos_AtributosNom_Sel, AV51TFFuncaoAPFAtributos_FuncaoDadosCod, AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To, AV55TFFuncaoAPFAtributos_AtrTabelaCod, AV56TFFuncaoAPFAtributos_AtrTabelaCod_To, AV59TFFuncaoAPFAtributos_AtrTabelaNom, AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel, AV63TFFuncoesAPFAtributos_Regra_Sel, AV66TFFuncoesAPFAtributos_Code, AV67TFFuncoesAPFAtributos_Code_Sel, AV70TFFuncoesAPFAtributos_Nome, AV71TFFuncoesAPFAtributos_Nome_Sel, AV74TFFuncoesAPFAtributos_Descricao, AV75TFFuncoesAPFAtributos_Descricao_Sel, AV37ddo_FuncaoAPF_CodigoTitleControlIdToReplace, AV41ddo_FuncaoAPF_NomeTitleControlIdToReplace, AV45ddo_FuncaoAPFAtributos_AtributosCodTitleControlIdToReplace, AV49ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace, AV53ddo_FuncaoAPFAtributos_FuncaoDadosCodTitleControlIdToReplace, AV57ddo_FuncaoAPFAtributos_AtrTabelaCodTitleControlIdToReplace, AV61ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace, AV64ddo_FuncoesAPFAtributos_RegraTitleControlIdToReplace, AV68ddo_FuncoesAPFAtributos_CodeTitleControlIdToReplace, AV72ddo_FuncoesAPFAtributos_NomeTitleControlIdToReplace, AV76ddo_FuncoesAPFAtributos_DescricaoTitleControlIdToReplace, AV84Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving) ;
      }

      protected void E249N2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV30DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         AV31DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31DynamicFiltersIgnoreFirst", AV31DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV30DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         AV31DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31DynamicFiltersIgnoreFirst", AV31DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18FuncaoAPF_Nome1, AV19FuncaoAPFAtributos_AtributosNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23FuncaoAPF_Nome2, AV24FuncaoAPFAtributos_AtributosNom2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28FuncaoAPF_Nome3, AV29FuncaoAPFAtributos_AtributosNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV35TFFuncaoAPF_Codigo, AV36TFFuncaoAPF_Codigo_To, AV39TFFuncaoAPF_Nome, AV40TFFuncaoAPF_Nome_Sel, AV43TFFuncaoAPFAtributos_AtributosCod, AV44TFFuncaoAPFAtributos_AtributosCod_To, AV47TFFuncaoAPFAtributos_AtributosNom, AV48TFFuncaoAPFAtributos_AtributosNom_Sel, AV51TFFuncaoAPFAtributos_FuncaoDadosCod, AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To, AV55TFFuncaoAPFAtributos_AtrTabelaCod, AV56TFFuncaoAPFAtributos_AtrTabelaCod_To, AV59TFFuncaoAPFAtributos_AtrTabelaNom, AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel, AV63TFFuncoesAPFAtributos_Regra_Sel, AV66TFFuncoesAPFAtributos_Code, AV67TFFuncoesAPFAtributos_Code_Sel, AV70TFFuncoesAPFAtributos_Nome, AV71TFFuncoesAPFAtributos_Nome_Sel, AV74TFFuncoesAPFAtributos_Descricao, AV75TFFuncoesAPFAtributos_Descricao_Sel, AV37ddo_FuncaoAPF_CodigoTitleControlIdToReplace, AV41ddo_FuncaoAPF_NomeTitleControlIdToReplace, AV45ddo_FuncaoAPFAtributos_AtributosCodTitleControlIdToReplace, AV49ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace, AV53ddo_FuncaoAPFAtributos_FuncaoDadosCodTitleControlIdToReplace, AV57ddo_FuncaoAPFAtributos_AtrTabelaCodTitleControlIdToReplace, AV61ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace, AV64ddo_FuncoesAPFAtributos_RegraTitleControlIdToReplace, AV68ddo_FuncoesAPFAtributos_CodeTitleControlIdToReplace, AV72ddo_FuncoesAPFAtributos_NomeTitleControlIdToReplace, AV76ddo_FuncoesAPFAtributos_DescricaoTitleControlIdToReplace, AV84Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E299N2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         AV17DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E309N2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV25DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18FuncaoAPF_Nome1, AV19FuncaoAPFAtributos_AtributosNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23FuncaoAPF_Nome2, AV24FuncaoAPFAtributos_AtributosNom2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28FuncaoAPF_Nome3, AV29FuncaoAPFAtributos_AtributosNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV35TFFuncaoAPF_Codigo, AV36TFFuncaoAPF_Codigo_To, AV39TFFuncaoAPF_Nome, AV40TFFuncaoAPF_Nome_Sel, AV43TFFuncaoAPFAtributos_AtributosCod, AV44TFFuncaoAPFAtributos_AtributosCod_To, AV47TFFuncaoAPFAtributos_AtributosNom, AV48TFFuncaoAPFAtributos_AtributosNom_Sel, AV51TFFuncaoAPFAtributos_FuncaoDadosCod, AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To, AV55TFFuncaoAPFAtributos_AtrTabelaCod, AV56TFFuncaoAPFAtributos_AtrTabelaCod_To, AV59TFFuncaoAPFAtributos_AtrTabelaNom, AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel, AV63TFFuncoesAPFAtributos_Regra_Sel, AV66TFFuncoesAPFAtributos_Code, AV67TFFuncoesAPFAtributos_Code_Sel, AV70TFFuncoesAPFAtributos_Nome, AV71TFFuncoesAPFAtributos_Nome_Sel, AV74TFFuncoesAPFAtributos_Descricao, AV75TFFuncoesAPFAtributos_Descricao_Sel, AV37ddo_FuncaoAPF_CodigoTitleControlIdToReplace, AV41ddo_FuncaoAPF_NomeTitleControlIdToReplace, AV45ddo_FuncaoAPFAtributos_AtributosCodTitleControlIdToReplace, AV49ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace, AV53ddo_FuncaoAPFAtributos_FuncaoDadosCodTitleControlIdToReplace, AV57ddo_FuncaoAPFAtributos_AtrTabelaCodTitleControlIdToReplace, AV61ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace, AV64ddo_FuncoesAPFAtributos_RegraTitleControlIdToReplace, AV68ddo_FuncoesAPFAtributos_CodeTitleControlIdToReplace, AV72ddo_FuncoesAPFAtributos_NomeTitleControlIdToReplace, AV76ddo_FuncoesAPFAtributos_DescricaoTitleControlIdToReplace, AV84Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving) ;
      }

      protected void E259N2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV30DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV30DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18FuncaoAPF_Nome1, AV19FuncaoAPFAtributos_AtributosNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23FuncaoAPF_Nome2, AV24FuncaoAPFAtributos_AtributosNom2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28FuncaoAPF_Nome3, AV29FuncaoAPFAtributos_AtributosNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV35TFFuncaoAPF_Codigo, AV36TFFuncaoAPF_Codigo_To, AV39TFFuncaoAPF_Nome, AV40TFFuncaoAPF_Nome_Sel, AV43TFFuncaoAPFAtributos_AtributosCod, AV44TFFuncaoAPFAtributos_AtributosCod_To, AV47TFFuncaoAPFAtributos_AtributosNom, AV48TFFuncaoAPFAtributos_AtributosNom_Sel, AV51TFFuncaoAPFAtributos_FuncaoDadosCod, AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To, AV55TFFuncaoAPFAtributos_AtrTabelaCod, AV56TFFuncaoAPFAtributos_AtrTabelaCod_To, AV59TFFuncaoAPFAtributos_AtrTabelaNom, AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel, AV63TFFuncoesAPFAtributos_Regra_Sel, AV66TFFuncoesAPFAtributos_Code, AV67TFFuncoesAPFAtributos_Code_Sel, AV70TFFuncoesAPFAtributos_Nome, AV71TFFuncoesAPFAtributos_Nome_Sel, AV74TFFuncoesAPFAtributos_Descricao, AV75TFFuncoesAPFAtributos_Descricao_Sel, AV37ddo_FuncaoAPF_CodigoTitleControlIdToReplace, AV41ddo_FuncaoAPF_NomeTitleControlIdToReplace, AV45ddo_FuncaoAPFAtributos_AtributosCodTitleControlIdToReplace, AV49ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace, AV53ddo_FuncaoAPFAtributos_FuncaoDadosCodTitleControlIdToReplace, AV57ddo_FuncaoAPFAtributos_AtrTabelaCodTitleControlIdToReplace, AV61ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace, AV64ddo_FuncoesAPFAtributos_RegraTitleControlIdToReplace, AV68ddo_FuncoesAPFAtributos_CodeTitleControlIdToReplace, AV72ddo_FuncoesAPFAtributos_NomeTitleControlIdToReplace, AV76ddo_FuncoesAPFAtributos_DescricaoTitleControlIdToReplace, AV84Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E319N2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         AV22DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E269N2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV30DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         AV25DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV30DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18FuncaoAPF_Nome1, AV19FuncaoAPFAtributos_AtributosNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23FuncaoAPF_Nome2, AV24FuncaoAPFAtributos_AtributosNom2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28FuncaoAPF_Nome3, AV29FuncaoAPFAtributos_AtributosNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV35TFFuncaoAPF_Codigo, AV36TFFuncaoAPF_Codigo_To, AV39TFFuncaoAPF_Nome, AV40TFFuncaoAPF_Nome_Sel, AV43TFFuncaoAPFAtributos_AtributosCod, AV44TFFuncaoAPFAtributos_AtributosCod_To, AV47TFFuncaoAPFAtributos_AtributosNom, AV48TFFuncaoAPFAtributos_AtributosNom_Sel, AV51TFFuncaoAPFAtributos_FuncaoDadosCod, AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To, AV55TFFuncaoAPFAtributos_AtrTabelaCod, AV56TFFuncaoAPFAtributos_AtrTabelaCod_To, AV59TFFuncaoAPFAtributos_AtrTabelaNom, AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel, AV63TFFuncoesAPFAtributos_Regra_Sel, AV66TFFuncoesAPFAtributos_Code, AV67TFFuncoesAPFAtributos_Code_Sel, AV70TFFuncoesAPFAtributos_Nome, AV71TFFuncoesAPFAtributos_Nome_Sel, AV74TFFuncoesAPFAtributos_Descricao, AV75TFFuncoesAPFAtributos_Descricao_Sel, AV37ddo_FuncaoAPF_CodigoTitleControlIdToReplace, AV41ddo_FuncaoAPF_NomeTitleControlIdToReplace, AV45ddo_FuncaoAPFAtributos_AtributosCodTitleControlIdToReplace, AV49ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace, AV53ddo_FuncaoAPFAtributos_FuncaoDadosCodTitleControlIdToReplace, AV57ddo_FuncaoAPFAtributos_AtrTabelaCodTitleControlIdToReplace, AV61ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace, AV64ddo_FuncoesAPFAtributos_RegraTitleControlIdToReplace, AV68ddo_FuncoesAPFAtributos_CodeTitleControlIdToReplace, AV72ddo_FuncoesAPFAtributos_NomeTitleControlIdToReplace, AV76ddo_FuncoesAPFAtributos_DescricaoTitleControlIdToReplace, AV84Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E329N2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         AV27DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E279N2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_funcaoapf_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_codigo_Internalname, "SortedStatus", Ddo_funcaoapf_codigo_Sortedstatus);
         Ddo_funcaoapf_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_nome_Internalname, "SortedStatus", Ddo_funcaoapf_nome_Sortedstatus);
         Ddo_funcaoapfatributos_atributoscod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfatributos_atributoscod_Internalname, "SortedStatus", Ddo_funcaoapfatributos_atributoscod_Sortedstatus);
         Ddo_funcaoapfatributos_atributosnom_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfatributos_atributosnom_Internalname, "SortedStatus", Ddo_funcaoapfatributos_atributosnom_Sortedstatus);
         Ddo_funcaoapfatributos_funcaodadoscod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfatributos_funcaodadoscod_Internalname, "SortedStatus", Ddo_funcaoapfatributos_funcaodadoscod_Sortedstatus);
         Ddo_funcaoapfatributos_atrtabelacod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfatributos_atrtabelacod_Internalname, "SortedStatus", Ddo_funcaoapfatributos_atrtabelacod_Sortedstatus);
         Ddo_funcaoapfatributos_atrtabelanom_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfatributos_atrtabelanom_Internalname, "SortedStatus", Ddo_funcaoapfatributos_atrtabelanom_Sortedstatus);
         Ddo_funcoesapfatributos_regra_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcoesapfatributos_regra_Internalname, "SortedStatus", Ddo_funcoesapfatributos_regra_Sortedstatus);
         Ddo_funcoesapfatributos_code_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcoesapfatributos_code_Internalname, "SortedStatus", Ddo_funcoesapfatributos_code_Sortedstatus);
         Ddo_funcoesapfatributos_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcoesapfatributos_nome_Internalname, "SortedStatus", Ddo_funcoesapfatributos_nome_Sortedstatus);
         Ddo_funcoesapfatributos_descricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcoesapfatributos_descricao_Internalname, "SortedStatus", Ddo_funcoesapfatributos_descricao_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV14OrderedBy == 2 )
         {
            Ddo_funcaoapf_codigo_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_codigo_Internalname, "SortedStatus", Ddo_funcaoapf_codigo_Sortedstatus);
         }
         else if ( AV14OrderedBy == 1 )
         {
            Ddo_funcaoapf_nome_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_nome_Internalname, "SortedStatus", Ddo_funcaoapf_nome_Sortedstatus);
         }
         else if ( AV14OrderedBy == 3 )
         {
            Ddo_funcaoapfatributos_atributoscod_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfatributos_atributoscod_Internalname, "SortedStatus", Ddo_funcaoapfatributos_atributoscod_Sortedstatus);
         }
         else if ( AV14OrderedBy == 4 )
         {
            Ddo_funcaoapfatributos_atributosnom_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfatributos_atributosnom_Internalname, "SortedStatus", Ddo_funcaoapfatributos_atributosnom_Sortedstatus);
         }
         else if ( AV14OrderedBy == 5 )
         {
            Ddo_funcaoapfatributos_funcaodadoscod_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfatributos_funcaodadoscod_Internalname, "SortedStatus", Ddo_funcaoapfatributos_funcaodadoscod_Sortedstatus);
         }
         else if ( AV14OrderedBy == 6 )
         {
            Ddo_funcaoapfatributos_atrtabelacod_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfatributos_atrtabelacod_Internalname, "SortedStatus", Ddo_funcaoapfatributos_atrtabelacod_Sortedstatus);
         }
         else if ( AV14OrderedBy == 7 )
         {
            Ddo_funcaoapfatributos_atrtabelanom_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfatributos_atrtabelanom_Internalname, "SortedStatus", Ddo_funcaoapfatributos_atrtabelanom_Sortedstatus);
         }
         else if ( AV14OrderedBy == 8 )
         {
            Ddo_funcoesapfatributos_regra_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcoesapfatributos_regra_Internalname, "SortedStatus", Ddo_funcoesapfatributos_regra_Sortedstatus);
         }
         else if ( AV14OrderedBy == 9 )
         {
            Ddo_funcoesapfatributos_code_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcoesapfatributos_code_Internalname, "SortedStatus", Ddo_funcoesapfatributos_code_Sortedstatus);
         }
         else if ( AV14OrderedBy == 10 )
         {
            Ddo_funcoesapfatributos_nome_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcoesapfatributos_nome_Internalname, "SortedStatus", Ddo_funcoesapfatributos_nome_Sortedstatus);
         }
         else if ( AV14OrderedBy == 11 )
         {
            Ddo_funcoesapfatributos_descricao_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcoesapfatributos_descricao_Internalname, "SortedStatus", Ddo_funcoesapfatributos_descricao_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavFuncaoapf_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome1_Visible), 5, 0)));
         edtavFuncaoapfatributos_atributosnom1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapfatributos_atributosnom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapfatributos_atributosnom1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 )
         {
            edtavFuncaoapf_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 )
         {
            edtavFuncaoapfatributos_atributosnom1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapfatributos_atributosnom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapfatributos_atributosnom1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavFuncaoapf_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome2_Visible), 5, 0)));
         edtavFuncaoapfatributos_atributosnom2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapfatributos_atributosnom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapfatributos_atributosnom2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "FUNCAOAPF_NOME") == 0 )
         {
            edtavFuncaoapf_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 )
         {
            edtavFuncaoapfatributos_atributosnom2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapfatributos_atributosnom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapfatributos_atributosnom2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavFuncaoapf_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome3_Visible), 5, 0)));
         edtavFuncaoapfatributos_atributosnom3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapfatributos_atributosnom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapfatributos_atributosnom3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "FUNCAOAPF_NOME") == 0 )
         {
            edtavFuncaoapf_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 )
         {
            edtavFuncaoapfatributos_atributosnom3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapfatributos_atributosnom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapfatributos_atributosnom3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         AV21DynamicFiltersSelector2 = "FUNCAOAPF_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         AV22DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         AV23FuncaoAPF_Nome2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23FuncaoAPF_Nome2", AV23FuncaoAPF_Nome2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV25DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
         AV26DynamicFiltersSelector3 = "FUNCAOAPF_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
         AV27DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
         AV28FuncaoAPF_Nome3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28FuncaoAPF_Nome3", AV28FuncaoAPF_Nome3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S202( )
      {
         /* 'CLEANFILTERS' Routine */
         AV35TFFuncaoAPF_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFFuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFFuncaoAPF_Codigo), 6, 0)));
         Ddo_funcaoapf_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_codigo_Internalname, "FilteredText_set", Ddo_funcaoapf_codigo_Filteredtext_set);
         AV36TFFuncaoAPF_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFFuncaoAPF_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFFuncaoAPF_Codigo_To), 6, 0)));
         Ddo_funcaoapf_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_codigo_Internalname, "FilteredTextTo_set", Ddo_funcaoapf_codigo_Filteredtextto_set);
         AV39TFFuncaoAPF_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFFuncaoAPF_Nome", AV39TFFuncaoAPF_Nome);
         Ddo_funcaoapf_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_nome_Internalname, "FilteredText_set", Ddo_funcaoapf_nome_Filteredtext_set);
         AV40TFFuncaoAPF_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFFuncaoAPF_Nome_Sel", AV40TFFuncaoAPF_Nome_Sel);
         Ddo_funcaoapf_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_nome_Internalname, "SelectedValue_set", Ddo_funcaoapf_nome_Selectedvalue_set);
         AV43TFFuncaoAPFAtributos_AtributosCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFFuncaoAPFAtributos_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFFuncaoAPFAtributos_AtributosCod), 6, 0)));
         Ddo_funcaoapfatributos_atributoscod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfatributos_atributoscod_Internalname, "FilteredText_set", Ddo_funcaoapfatributos_atributoscod_Filteredtext_set);
         AV44TFFuncaoAPFAtributos_AtributosCod_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFFuncaoAPFAtributos_AtributosCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFFuncaoAPFAtributos_AtributosCod_To), 6, 0)));
         Ddo_funcaoapfatributos_atributoscod_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfatributos_atributoscod_Internalname, "FilteredTextTo_set", Ddo_funcaoapfatributos_atributoscod_Filteredtextto_set);
         AV47TFFuncaoAPFAtributos_AtributosNom = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFFuncaoAPFAtributos_AtributosNom", AV47TFFuncaoAPFAtributos_AtributosNom);
         Ddo_funcaoapfatributos_atributosnom_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfatributos_atributosnom_Internalname, "FilteredText_set", Ddo_funcaoapfatributos_atributosnom_Filteredtext_set);
         AV48TFFuncaoAPFAtributos_AtributosNom_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFFuncaoAPFAtributos_AtributosNom_Sel", AV48TFFuncaoAPFAtributos_AtributosNom_Sel);
         Ddo_funcaoapfatributos_atributosnom_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfatributos_atributosnom_Internalname, "SelectedValue_set", Ddo_funcaoapfatributos_atributosnom_Selectedvalue_set);
         AV51TFFuncaoAPFAtributos_FuncaoDadosCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFFuncaoAPFAtributos_FuncaoDadosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51TFFuncaoAPFAtributos_FuncaoDadosCod), 6, 0)));
         Ddo_funcaoapfatributos_funcaodadoscod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfatributos_funcaodadoscod_Internalname, "FilteredText_set", Ddo_funcaoapfatributos_funcaodadoscod_Filteredtext_set);
         AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To), 6, 0)));
         Ddo_funcaoapfatributos_funcaodadoscod_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfatributos_funcaodadoscod_Internalname, "FilteredTextTo_set", Ddo_funcaoapfatributos_funcaodadoscod_Filteredtextto_set);
         AV55TFFuncaoAPFAtributos_AtrTabelaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFFuncaoAPFAtributos_AtrTabelaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFFuncaoAPFAtributos_AtrTabelaCod), 6, 0)));
         Ddo_funcaoapfatributos_atrtabelacod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfatributos_atrtabelacod_Internalname, "FilteredText_set", Ddo_funcaoapfatributos_atrtabelacod_Filteredtext_set);
         AV56TFFuncaoAPFAtributos_AtrTabelaCod_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFFuncaoAPFAtributos_AtrTabelaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56TFFuncaoAPFAtributos_AtrTabelaCod_To), 6, 0)));
         Ddo_funcaoapfatributos_atrtabelacod_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfatributos_atrtabelacod_Internalname, "FilteredTextTo_set", Ddo_funcaoapfatributos_atrtabelacod_Filteredtextto_set);
         AV59TFFuncaoAPFAtributos_AtrTabelaNom = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFFuncaoAPFAtributos_AtrTabelaNom", AV59TFFuncaoAPFAtributos_AtrTabelaNom);
         Ddo_funcaoapfatributos_atrtabelanom_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfatributos_atrtabelanom_Internalname, "FilteredText_set", Ddo_funcaoapfatributos_atrtabelanom_Filteredtext_set);
         AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel", AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel);
         Ddo_funcaoapfatributos_atrtabelanom_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapfatributos_atrtabelanom_Internalname, "SelectedValue_set", Ddo_funcaoapfatributos_atrtabelanom_Selectedvalue_set);
         AV63TFFuncoesAPFAtributos_Regra_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFFuncoesAPFAtributos_Regra_Sel", StringUtil.Str( (decimal)(AV63TFFuncoesAPFAtributos_Regra_Sel), 1, 0));
         Ddo_funcoesapfatributos_regra_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcoesapfatributos_regra_Internalname, "SelectedValue_set", Ddo_funcoesapfatributos_regra_Selectedvalue_set);
         AV66TFFuncoesAPFAtributos_Code = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFFuncoesAPFAtributos_Code", AV66TFFuncoesAPFAtributos_Code);
         Ddo_funcoesapfatributos_code_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcoesapfatributos_code_Internalname, "FilteredText_set", Ddo_funcoesapfatributos_code_Filteredtext_set);
         AV67TFFuncoesAPFAtributos_Code_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFFuncoesAPFAtributos_Code_Sel", AV67TFFuncoesAPFAtributos_Code_Sel);
         Ddo_funcoesapfatributos_code_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcoesapfatributos_code_Internalname, "SelectedValue_set", Ddo_funcoesapfatributos_code_Selectedvalue_set);
         AV70TFFuncoesAPFAtributos_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFFuncoesAPFAtributos_Nome", AV70TFFuncoesAPFAtributos_Nome);
         Ddo_funcoesapfatributos_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcoesapfatributos_nome_Internalname, "FilteredText_set", Ddo_funcoesapfatributos_nome_Filteredtext_set);
         AV71TFFuncoesAPFAtributos_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFFuncoesAPFAtributos_Nome_Sel", AV71TFFuncoesAPFAtributos_Nome_Sel);
         Ddo_funcoesapfatributos_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcoesapfatributos_nome_Internalname, "SelectedValue_set", Ddo_funcoesapfatributos_nome_Selectedvalue_set);
         AV74TFFuncoesAPFAtributos_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFFuncoesAPFAtributos_Descricao", AV74TFFuncoesAPFAtributos_Descricao);
         Ddo_funcoesapfatributos_descricao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcoesapfatributos_descricao_Internalname, "FilteredText_set", Ddo_funcoesapfatributos_descricao_Filteredtext_set);
         AV75TFFuncoesAPFAtributos_Descricao_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFFuncoesAPFAtributos_Descricao_Sel", AV75TFFuncoesAPFAtributos_Descricao_Sel);
         Ddo_funcoesapfatributos_descricao_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcoesapfatributos_descricao_Internalname, "SelectedValue_set", Ddo_funcoesapfatributos_descricao_Selectedvalue_set);
         AV16DynamicFiltersSelector1 = "FUNCAOAPF_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         AV17DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         AV18FuncaoAPF_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18FuncaoAPF_Nome1", AV18FuncaoAPF_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(1));
            AV16DynamicFiltersSelector1 = AV13GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 )
            {
               AV17DynamicFiltersOperator1 = AV13GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV18FuncaoAPF_Nome1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18FuncaoAPF_Nome1", AV18FuncaoAPF_Nome1);
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 )
            {
               AV17DynamicFiltersOperator1 = AV13GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV19FuncaoAPFAtributos_AtributosNom1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19FuncaoAPFAtributos_AtributosNom1", AV19FuncaoAPFAtributos_AtributosNom1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV20DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(2));
               AV21DynamicFiltersSelector2 = AV13GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "FUNCAOAPF_NOME") == 0 )
               {
                  AV22DynamicFiltersOperator2 = AV13GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
                  AV23FuncaoAPF_Nome2 = AV13GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23FuncaoAPF_Nome2", AV23FuncaoAPF_Nome2);
               }
               else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 )
               {
                  AV22DynamicFiltersOperator2 = AV13GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
                  AV24FuncaoAPFAtributos_AtributosNom2 = AV13GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24FuncaoAPFAtributos_AtributosNom2", AV24FuncaoAPFAtributos_AtributosNom2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV25DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
                  AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(3));
                  AV26DynamicFiltersSelector3 = AV13GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "FUNCAOAPF_NOME") == 0 )
                  {
                     AV27DynamicFiltersOperator3 = AV13GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
                     AV28FuncaoAPF_Nome3 = AV13GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28FuncaoAPF_Nome3", AV28FuncaoAPF_Nome3);
                  }
                  else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 )
                  {
                     AV27DynamicFiltersOperator3 = AV13GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
                     AV29FuncaoAPFAtributos_AtributosNom3 = AV13GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29FuncaoAPFAtributos_AtributosNom3", AV29FuncaoAPFAtributos_AtributosNom3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV30DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV11GridState.gxTpr_Orderedby = AV14OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV15OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (0==AV35TFFuncaoAPF_Codigo) && (0==AV36TFFuncaoAPF_Codigo_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPF_CODIGO";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV35TFFuncaoAPF_Codigo), 6, 0);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV36TFFuncaoAPF_Codigo_To), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFFuncaoAPF_Nome)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPF_NOME";
            AV12GridStateFilterValue.gxTpr_Value = AV39TFFuncaoAPF_Nome;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFFuncaoAPF_Nome_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPF_NOME_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV40TFFuncaoAPF_Nome_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV43TFFuncaoAPFAtributos_AtributosCod) && (0==AV44TFFuncaoAPFAtributos_AtributosCod_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV43TFFuncaoAPFAtributos_AtributosCod), 6, 0);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV44TFFuncaoAPFAtributos_AtributosCod_To), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47TFFuncaoAPFAtributos_AtributosNom)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM";
            AV12GridStateFilterValue.gxTpr_Value = AV47TFFuncaoAPFAtributos_AtributosNom;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TFFuncaoAPFAtributos_AtributosNom_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV48TFFuncaoAPFAtributos_AtributosNom_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV51TFFuncaoAPFAtributos_FuncaoDadosCod) && (0==AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV51TFFuncaoAPFAtributos_FuncaoDadosCod), 6, 0);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV55TFFuncaoAPFAtributos_AtrTabelaCod) && (0==AV56TFFuncaoAPFAtributos_AtrTabelaCod_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPFATRIBUTOS_ATRTABELACOD";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV55TFFuncaoAPFAtributos_AtrTabelaCod), 6, 0);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV56TFFuncaoAPFAtributos_AtrTabelaCod_To), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59TFFuncaoAPFAtributos_AtrTabelaNom)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPFATRIBUTOS_ATRTABELANOM";
            AV12GridStateFilterValue.gxTpr_Value = AV59TFFuncaoAPFAtributos_AtrTabelaNom;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV63TFFuncoesAPFAtributos_Regra_Sel) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFFUNCOESAPFATRIBUTOS_REGRA_SEL";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV63TFFuncoesAPFAtributos_Regra_Sel), 1, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66TFFuncoesAPFAtributos_Code)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFFUNCOESAPFATRIBUTOS_CODE";
            AV12GridStateFilterValue.gxTpr_Value = AV66TFFuncoesAPFAtributos_Code;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67TFFuncoesAPFAtributos_Code_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFFUNCOESAPFATRIBUTOS_CODE_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV67TFFuncoesAPFAtributos_Code_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70TFFuncoesAPFAtributos_Nome)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFFUNCOESAPFATRIBUTOS_NOME";
            AV12GridStateFilterValue.gxTpr_Value = AV70TFFuncoesAPFAtributos_Nome;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71TFFuncoesAPFAtributos_Nome_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFFUNCOESAPFATRIBUTOS_NOME_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV71TFFuncoesAPFAtributos_Nome_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74TFFuncoesAPFAtributos_Descricao)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFFUNCOESAPFATRIBUTOS_DESCRICAO";
            AV12GridStateFilterValue.gxTpr_Value = AV74TFFuncoesAPFAtributos_Descricao;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75TFFuncoesAPFAtributos_Descricao_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFFUNCOESAPFATRIBUTOS_DESCRICAO_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV75TFFuncoesAPFAtributos_Descricao_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV84Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV31DynamicFiltersIgnoreFirst )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV16DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV18FuncaoAPF_Nome1)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV18FuncaoAPF_Nome1;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV17DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV19FuncaoAPFAtributos_AtributosNom1)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV19FuncaoAPFAtributos_AtributosNom1;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV17DynamicFiltersOperator1;
            }
            if ( AV30DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV20DynamicFiltersEnabled2 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV21DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "FUNCAOAPF_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV23FuncaoAPF_Nome2)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV23FuncaoAPF_Nome2;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV24FuncaoAPFAtributos_AtributosNom2)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV24FuncaoAPFAtributos_AtributosNom2;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator2;
            }
            if ( AV30DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV25DynamicFiltersEnabled3 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV26DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "FUNCAOAPF_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV28FuncaoAPF_Nome3)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV28FuncaoAPF_Nome3;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV27DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV29FuncaoAPFAtributos_AtributosNom3)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV29FuncaoAPFAtributos_AtributosNom3;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV27DynamicFiltersOperator3;
            }
            if ( AV30DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_9N2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_9N2( true) ;
         }
         else
         {
            wb_table2_5_9N2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_9N2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_77_9N2( true) ;
         }
         else
         {
            wb_table3_77_9N2( false) ;
         }
         return  ;
      }

      protected void wb_table3_77_9N2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_9N2e( true) ;
         }
         else
         {
            wb_table1_2_9N2e( false) ;
         }
      }

      protected void wb_table3_77_9N2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_80_9N2( true) ;
         }
         else
         {
            wb_table4_80_9N2( false) ;
         }
         return  ;
      }

      protected void wb_table4_80_9N2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_77_9N2e( true) ;
         }
         else
         {
            wb_table3_77_9N2e( false) ;
         }
      }

      protected void wb_table4_80_9N2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"83\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoAPF_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoAPF_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoAPF_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoAPF_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoAPF_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoAPF_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoAPFAtributos_AtributosCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoAPFAtributos_AtributosCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoAPFAtributos_AtributosCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoAPFAtributos_AtributosNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoAPFAtributos_AtributosNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoAPFAtributos_AtributosNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoAPFAtributos_FuncaoDadosCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoAPFAtributos_FuncaoDadosCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoAPFAtributos_FuncaoDadosCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoAPFAtributos_AtrTabelaCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoAPFAtributos_AtrTabelaCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoAPFAtributos_AtrTabelaCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoAPFAtributos_AtrTabelaNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoAPFAtributos_AtrTabelaNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoAPFAtributos_AtrTabelaNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkFuncoesAPFAtributos_Regra_Titleformat == 0 )
               {
                  context.SendWebValue( chkFuncoesAPFAtributos_Regra.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkFuncoesAPFAtributos_Regra.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncoesAPFAtributos_Code_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncoesAPFAtributos_Code_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncoesAPFAtributos_Code_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncoesAPFAtributos_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncoesAPFAtributos_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncoesAPFAtributos_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncoesAPFAtributos_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncoesAPFAtributos_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncoesAPFAtributos_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV32Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoAPF_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoAPF_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A166FuncaoAPF_Nome);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoAPF_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoAPF_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A364FuncaoAPFAtributos_AtributosCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoAPFAtributos_AtributosCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoAPFAtributos_AtributosCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A365FuncaoAPFAtributos_AtributosNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoAPFAtributos_AtributosNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoAPFAtributos_AtributosNom_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A378FuncaoAPFAtributos_FuncaoDadosCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoAPFAtributos_FuncaoDadosCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoAPFAtributos_FuncaoDadosCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A366FuncaoAPFAtributos_AtrTabelaCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoAPFAtributos_AtrTabelaCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoAPFAtributos_AtrTabelaCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A367FuncaoAPFAtributos_AtrTabelaNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoAPFAtributos_AtrTabelaNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoAPFAtributos_AtrTabelaNom_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A389FuncoesAPFAtributos_Regra));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkFuncoesAPFAtributos_Regra.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkFuncoesAPFAtributos_Regra_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A383FuncoesAPFAtributos_Code);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncoesAPFAtributos_Code_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncoesAPFAtributos_Code_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A384FuncoesAPFAtributos_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncoesAPFAtributos_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncoesAPFAtributos_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A385FuncoesAPFAtributos_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncoesAPFAtributos_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncoesAPFAtributos_Descricao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 83 )
         {
            wbEnd = 0;
            nRC_GXsfl_83 = (short)(nGXsfl_83_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_80_9N2e( true) ;
         }
         else
         {
            wb_table4_80_9N2e( false) ;
         }
      }

      protected void wb_table2_5_9N2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptFuncoesAPFAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptFuncoesAPFAtributos.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV15OrderedDsc), StringUtil.BoolToStr( AV15OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptFuncoesAPFAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_14_9N2( true) ;
         }
         else
         {
            wb_table5_14_9N2( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_9N2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_9N2e( true) ;
         }
         else
         {
            wb_table2_5_9N2e( false) ;
         }
      }

      protected void wb_table5_14_9N2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptFuncoesAPFAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_19_9N2( true) ;
         }
         else
         {
            wb_table6_19_9N2( false) ;
         }
         return  ;
      }

      protected void wb_table6_19_9N2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptFuncoesAPFAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_9N2e( true) ;
         }
         else
         {
            wb_table5_14_9N2e( false) ;
         }
      }

      protected void wb_table6_19_9N2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptFuncoesAPFAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV16DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_PromptFuncoesAPFAtributos.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptFuncoesAPFAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_9N2( true) ;
         }
         else
         {
            wb_table7_28_9N2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_9N2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptFuncoesAPFAtributos.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptFuncoesAPFAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptFuncoesAPFAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV21DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"", "", true, "HLP_PromptFuncoesAPFAtributos.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptFuncoesAPFAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_46_9N2( true) ;
         }
         else
         {
            wb_table8_46_9N2( false) ;
         }
         return  ;
      }

      protected void wb_table8_46_9N2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptFuncoesAPFAtributos.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptFuncoesAPFAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptFuncoesAPFAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV26DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,60);\"", "", true, "HLP_PromptFuncoesAPFAtributos.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptFuncoesAPFAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_64_9N2( true) ;
         }
         else
         {
            wb_table9_64_9N2( false) ;
         }
         return  ;
      }

      protected void wb_table9_64_9N2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptFuncoesAPFAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_19_9N2e( true) ;
         }
         else
         {
            wb_table6_19_9N2e( false) ;
         }
      }

      protected void wb_table9_64_9N2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,67);\"", "", true, "HLP_PromptFuncoesAPFAtributos.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavFuncaoapf_nome3_Internalname, AV28FuncaoAPF_Nome3, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,69);\"", 0, edtavFuncaoapf_nome3_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_PromptFuncoesAPFAtributos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFuncaoapfatributos_atributosnom3_Internalname, StringUtil.RTrim( AV29FuncaoAPFAtributos_AtributosNom3), StringUtil.RTrim( context.localUtil.Format( AV29FuncaoAPFAtributos_AtributosNom3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,70);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFuncaoapfatributos_atributosnom3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavFuncaoapfatributos_atributosnom3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptFuncoesAPFAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_64_9N2e( true) ;
         }
         else
         {
            wb_table9_64_9N2e( false) ;
         }
      }

      protected void wb_table8_46_9N2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", "", true, "HLP_PromptFuncoesAPFAtributos.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavFuncaoapf_nome2_Internalname, AV23FuncaoAPF_Nome2, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,51);\"", 0, edtavFuncaoapf_nome2_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_PromptFuncoesAPFAtributos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFuncaoapfatributos_atributosnom2_Internalname, StringUtil.RTrim( AV24FuncaoAPFAtributos_AtributosNom2), StringUtil.RTrim( context.localUtil.Format( AV24FuncaoAPFAtributos_AtributosNom2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,52);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFuncaoapfatributos_atributosnom2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavFuncaoapfatributos_atributosnom2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptFuncoesAPFAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_46_9N2e( true) ;
         }
         else
         {
            wb_table8_46_9N2e( false) ;
         }
      }

      protected void wb_table7_28_9N2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_PromptFuncoesAPFAtributos.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavFuncaoapf_nome1_Internalname, AV18FuncaoAPF_Nome1, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", 0, edtavFuncaoapf_nome1_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_PromptFuncoesAPFAtributos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFuncaoapfatributos_atributosnom1_Internalname, StringUtil.RTrim( AV19FuncaoAPFAtributos_AtributosNom1), StringUtil.RTrim( context.localUtil.Format( AV19FuncaoAPFAtributos_AtributosNom1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,34);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFuncaoapfatributos_atributosnom1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavFuncaoapfatributos_atributosnom1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptFuncoesAPFAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_9N2e( true) ;
         }
         else
         {
            wb_table7_28_9N2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutFuncaoAPF_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutFuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutFuncaoAPF_Codigo), 6, 0)));
         AV8InOutFuncaoAPFAtributos_AtributosCod = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutFuncaoAPFAtributos_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8InOutFuncaoAPFAtributos_AtributosCod), 6, 0)));
         AV9InOutFuncaoAPF_Nome = (String)getParm(obj,2);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9InOutFuncaoAPF_Nome", AV9InOutFuncaoAPF_Nome);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA9N2( ) ;
         WS9N2( ) ;
         WE9N2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117391637");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptfuncoesapfatributos.js", "?20203117391637");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_832( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_83_idx;
         edtFuncaoAPF_Codigo_Internalname = "FUNCAOAPF_CODIGO_"+sGXsfl_83_idx;
         edtFuncaoAPF_Nome_Internalname = "FUNCAOAPF_NOME_"+sGXsfl_83_idx;
         edtFuncaoAPFAtributos_AtributosCod_Internalname = "FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_"+sGXsfl_83_idx;
         edtFuncaoAPFAtributos_AtributosNom_Internalname = "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_"+sGXsfl_83_idx;
         edtFuncaoAPFAtributos_FuncaoDadosCod_Internalname = "FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_"+sGXsfl_83_idx;
         edtFuncaoAPFAtributos_AtrTabelaCod_Internalname = "FUNCAOAPFATRIBUTOS_ATRTABELACOD_"+sGXsfl_83_idx;
         edtFuncaoAPFAtributos_AtrTabelaNom_Internalname = "FUNCAOAPFATRIBUTOS_ATRTABELANOM_"+sGXsfl_83_idx;
         chkFuncoesAPFAtributos_Regra_Internalname = "FUNCOESAPFATRIBUTOS_REGRA_"+sGXsfl_83_idx;
         edtFuncoesAPFAtributos_Code_Internalname = "FUNCOESAPFATRIBUTOS_CODE_"+sGXsfl_83_idx;
         edtFuncoesAPFAtributos_Nome_Internalname = "FUNCOESAPFATRIBUTOS_NOME_"+sGXsfl_83_idx;
         edtFuncoesAPFAtributos_Descricao_Internalname = "FUNCOESAPFATRIBUTOS_DESCRICAO_"+sGXsfl_83_idx;
      }

      protected void SubsflControlProps_fel_832( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_83_fel_idx;
         edtFuncaoAPF_Codigo_Internalname = "FUNCAOAPF_CODIGO_"+sGXsfl_83_fel_idx;
         edtFuncaoAPF_Nome_Internalname = "FUNCAOAPF_NOME_"+sGXsfl_83_fel_idx;
         edtFuncaoAPFAtributos_AtributosCod_Internalname = "FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_"+sGXsfl_83_fel_idx;
         edtFuncaoAPFAtributos_AtributosNom_Internalname = "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_"+sGXsfl_83_fel_idx;
         edtFuncaoAPFAtributos_FuncaoDadosCod_Internalname = "FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_"+sGXsfl_83_fel_idx;
         edtFuncaoAPFAtributos_AtrTabelaCod_Internalname = "FUNCAOAPFATRIBUTOS_ATRTABELACOD_"+sGXsfl_83_fel_idx;
         edtFuncaoAPFAtributos_AtrTabelaNom_Internalname = "FUNCAOAPFATRIBUTOS_ATRTABELANOM_"+sGXsfl_83_fel_idx;
         chkFuncoesAPFAtributos_Regra_Internalname = "FUNCOESAPFATRIBUTOS_REGRA_"+sGXsfl_83_fel_idx;
         edtFuncoesAPFAtributos_Code_Internalname = "FUNCOESAPFATRIBUTOS_CODE_"+sGXsfl_83_fel_idx;
         edtFuncoesAPFAtributos_Nome_Internalname = "FUNCOESAPFATRIBUTOS_NOME_"+sGXsfl_83_fel_idx;
         edtFuncoesAPFAtributos_Descricao_Internalname = "FUNCOESAPFATRIBUTOS_DESCRICAO_"+sGXsfl_83_fel_idx;
      }

      protected void sendrow_832( )
      {
         SubsflControlProps_832( ) ;
         WB9N0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_83_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_83_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_83_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 84,'',false,'',83)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV32Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV32Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV83Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV32Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV32Select)) ? AV83Select_GXI : context.PathToRelativeUrl( AV32Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_83_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV32Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPF_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPF_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)83,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPF_Nome_Internalname,(String)A166FuncaoAPF_Nome,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPF_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)200,(short)0,(short)0,(short)83,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPFAtributos_AtributosCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A364FuncaoAPFAtributos_AtributosCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A364FuncaoAPFAtributos_AtributosCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPFAtributos_AtributosCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)83,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPFAtributos_AtributosNom_Internalname,StringUtil.RTrim( A365FuncaoAPFAtributos_AtributosNom),StringUtil.RTrim( context.localUtil.Format( A365FuncaoAPFAtributos_AtributosNom, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPFAtributos_AtributosNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)83,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPFAtributos_FuncaoDadosCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A378FuncaoAPFAtributos_FuncaoDadosCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A378FuncaoAPFAtributos_FuncaoDadosCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPFAtributos_FuncaoDadosCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)83,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPFAtributos_AtrTabelaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A366FuncaoAPFAtributos_AtrTabelaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A366FuncaoAPFAtributos_AtrTabelaCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPFAtributos_AtrTabelaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)83,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPFAtributos_AtrTabelaNom_Internalname,StringUtil.RTrim( A367FuncaoAPFAtributos_AtrTabelaNom),StringUtil.RTrim( context.localUtil.Format( A367FuncaoAPFAtributos_AtrTabelaNom, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPFAtributos_AtrTabelaNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)83,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkFuncoesAPFAtributos_Regra_Internalname,StringUtil.BoolToStr( A389FuncoesAPFAtributos_Regra),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncoesAPFAtributos_Code_Internalname,(String)A383FuncoesAPFAtributos_Code,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncoesAPFAtributos_Code_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)83,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncoesAPFAtributos_Nome_Internalname,StringUtil.RTrim( A384FuncoesAPFAtributos_Nome),StringUtil.RTrim( context.localUtil.Format( A384FuncoesAPFAtributos_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncoesAPFAtributos_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)83,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncoesAPFAtributos_Descricao_Internalname,(String)A385FuncoesAPFAtributos_Descricao,StringUtil.RTrim( context.localUtil.Format( A385FuncoesAPFAtributos_Descricao, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncoesAPFAtributos_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)83,(short)1,(short)-1,(short)-1,(bool)true,(String)"Descricao",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_CODIGO"+"_"+sGXsfl_83_idx, GetSecureSignedToken( sGXsfl_83_idx, context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD"+"_"+sGXsfl_83_idx, GetSecureSignedToken( sGXsfl_83_idx, context.localUtil.Format( (decimal)(A364FuncaoAPFAtributos_AtributosCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD"+"_"+sGXsfl_83_idx, GetSecureSignedToken( sGXsfl_83_idx, context.localUtil.Format( (decimal)(A378FuncaoAPFAtributos_FuncaoDadosCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCOESAPFATRIBUTOS_REGRA"+"_"+sGXsfl_83_idx, GetSecureSignedToken( sGXsfl_83_idx, A389FuncoesAPFAtributos_Regra));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCOESAPFATRIBUTOS_CODE"+"_"+sGXsfl_83_idx, GetSecureSignedToken( sGXsfl_83_idx, StringUtil.RTrim( context.localUtil.Format( A383FuncoesAPFAtributos_Code, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCOESAPFATRIBUTOS_NOME"+"_"+sGXsfl_83_idx, GetSecureSignedToken( sGXsfl_83_idx, StringUtil.RTrim( context.localUtil.Format( A384FuncoesAPFAtributos_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCOESAPFATRIBUTOS_DESCRICAO"+"_"+sGXsfl_83_idx, GetSecureSignedToken( sGXsfl_83_idx, StringUtil.RTrim( context.localUtil.Format( A385FuncoesAPFAtributos_Descricao, "@!"))));
            GridContainer.AddRow(GridRow);
            nGXsfl_83_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_83_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_83_idx+1));
            sGXsfl_83_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_83_idx), 4, 0)), 4, "0");
            SubsflControlProps_832( ) ;
         }
         /* End function sendrow_832 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavFuncaoapf_nome1_Internalname = "vFUNCAOAPF_NOME1";
         edtavFuncaoapfatributos_atributosnom1_Internalname = "vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavFuncaoapf_nome2_Internalname = "vFUNCAOAPF_NOME2";
         edtavFuncaoapfatributos_atributosnom2_Internalname = "vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavFuncaoapf_nome3_Internalname = "vFUNCAOAPF_NOME3";
         edtavFuncaoapfatributos_atributosnom3_Internalname = "vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtFuncaoAPF_Codigo_Internalname = "FUNCAOAPF_CODIGO";
         edtFuncaoAPF_Nome_Internalname = "FUNCAOAPF_NOME";
         edtFuncaoAPFAtributos_AtributosCod_Internalname = "FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD";
         edtFuncaoAPFAtributos_AtributosNom_Internalname = "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM";
         edtFuncaoAPFAtributos_FuncaoDadosCod_Internalname = "FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD";
         edtFuncaoAPFAtributos_AtrTabelaCod_Internalname = "FUNCAOAPFATRIBUTOS_ATRTABELACOD";
         edtFuncaoAPFAtributos_AtrTabelaNom_Internalname = "FUNCAOAPFATRIBUTOS_ATRTABELANOM";
         chkFuncoesAPFAtributos_Regra_Internalname = "FUNCOESAPFATRIBUTOS_REGRA";
         edtFuncoesAPFAtributos_Code_Internalname = "FUNCOESAPFATRIBUTOS_CODE";
         edtFuncoesAPFAtributos_Nome_Internalname = "FUNCOESAPFATRIBUTOS_NOME";
         edtFuncoesAPFAtributos_Descricao_Internalname = "FUNCOESAPFATRIBUTOS_DESCRICAO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTffuncaoapf_codigo_Internalname = "vTFFUNCAOAPF_CODIGO";
         edtavTffuncaoapf_codigo_to_Internalname = "vTFFUNCAOAPF_CODIGO_TO";
         edtavTffuncaoapf_nome_Internalname = "vTFFUNCAOAPF_NOME";
         edtavTffuncaoapf_nome_sel_Internalname = "vTFFUNCAOAPF_NOME_SEL";
         edtavTffuncaoapfatributos_atributoscod_Internalname = "vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD";
         edtavTffuncaoapfatributos_atributoscod_to_Internalname = "vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_TO";
         edtavTffuncaoapfatributos_atributosnom_Internalname = "vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM";
         edtavTffuncaoapfatributos_atributosnom_sel_Internalname = "vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL";
         edtavTffuncaoapfatributos_funcaodadoscod_Internalname = "vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD";
         edtavTffuncaoapfatributos_funcaodadoscod_to_Internalname = "vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_TO";
         edtavTffuncaoapfatributos_atrtabelacod_Internalname = "vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD";
         edtavTffuncaoapfatributos_atrtabelacod_to_Internalname = "vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD_TO";
         edtavTffuncaoapfatributos_atrtabelanom_Internalname = "vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM";
         edtavTffuncaoapfatributos_atrtabelanom_sel_Internalname = "vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL";
         edtavTffuncoesapfatributos_regra_sel_Internalname = "vTFFUNCOESAPFATRIBUTOS_REGRA_SEL";
         edtavTffuncoesapfatributos_code_Internalname = "vTFFUNCOESAPFATRIBUTOS_CODE";
         edtavTffuncoesapfatributos_code_sel_Internalname = "vTFFUNCOESAPFATRIBUTOS_CODE_SEL";
         edtavTffuncoesapfatributos_nome_Internalname = "vTFFUNCOESAPFATRIBUTOS_NOME";
         edtavTffuncoesapfatributos_nome_sel_Internalname = "vTFFUNCOESAPFATRIBUTOS_NOME_SEL";
         edtavTffuncoesapfatributos_descricao_Internalname = "vTFFUNCOESAPFATRIBUTOS_DESCRICAO";
         edtavTffuncoesapfatributos_descricao_sel_Internalname = "vTFFUNCOESAPFATRIBUTOS_DESCRICAO_SEL";
         Ddo_funcaoapf_codigo_Internalname = "DDO_FUNCAOAPF_CODIGO";
         edtavDdo_funcaoapf_codigotitlecontrolidtoreplace_Internalname = "vDDO_FUNCAOAPF_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_funcaoapf_nome_Internalname = "DDO_FUNCAOAPF_NOME";
         edtavDdo_funcaoapf_nometitlecontrolidtoreplace_Internalname = "vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE";
         Ddo_funcaoapfatributos_atributoscod_Internalname = "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD";
         edtavDdo_funcaoapfatributos_atributoscodtitlecontrolidtoreplace_Internalname = "vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCODTITLECONTROLIDTOREPLACE";
         Ddo_funcaoapfatributos_atributosnom_Internalname = "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM";
         edtavDdo_funcaoapfatributos_atributosnomtitlecontrolidtoreplace_Internalname = "vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOMTITLECONTROLIDTOREPLACE";
         Ddo_funcaoapfatributos_funcaodadoscod_Internalname = "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD";
         edtavDdo_funcaoapfatributos_funcaodadoscodtitlecontrolidtoreplace_Internalname = "vDDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCODTITLECONTROLIDTOREPLACE";
         Ddo_funcaoapfatributos_atrtabelacod_Internalname = "DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD";
         edtavDdo_funcaoapfatributos_atrtabelacodtitlecontrolidtoreplace_Internalname = "vDDO_FUNCAOAPFATRIBUTOS_ATRTABELACODTITLECONTROLIDTOREPLACE";
         Ddo_funcaoapfatributos_atrtabelanom_Internalname = "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM";
         edtavDdo_funcaoapfatributos_atrtabelanomtitlecontrolidtoreplace_Internalname = "vDDO_FUNCAOAPFATRIBUTOS_ATRTABELANOMTITLECONTROLIDTOREPLACE";
         Ddo_funcoesapfatributos_regra_Internalname = "DDO_FUNCOESAPFATRIBUTOS_REGRA";
         edtavDdo_funcoesapfatributos_regratitlecontrolidtoreplace_Internalname = "vDDO_FUNCOESAPFATRIBUTOS_REGRATITLECONTROLIDTOREPLACE";
         Ddo_funcoesapfatributos_code_Internalname = "DDO_FUNCOESAPFATRIBUTOS_CODE";
         edtavDdo_funcoesapfatributos_codetitlecontrolidtoreplace_Internalname = "vDDO_FUNCOESAPFATRIBUTOS_CODETITLECONTROLIDTOREPLACE";
         Ddo_funcoesapfatributos_nome_Internalname = "DDO_FUNCOESAPFATRIBUTOS_NOME";
         edtavDdo_funcoesapfatributos_nometitlecontrolidtoreplace_Internalname = "vDDO_FUNCOESAPFATRIBUTOS_NOMETITLECONTROLIDTOREPLACE";
         Ddo_funcoesapfatributos_descricao_Internalname = "DDO_FUNCOESAPFATRIBUTOS_DESCRICAO";
         edtavDdo_funcoesapfatributos_descricaotitlecontrolidtoreplace_Internalname = "vDDO_FUNCOESAPFATRIBUTOS_DESCRICAOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtFuncoesAPFAtributos_Descricao_Jsonclick = "";
         edtFuncoesAPFAtributos_Nome_Jsonclick = "";
         edtFuncoesAPFAtributos_Code_Jsonclick = "";
         edtFuncaoAPFAtributos_AtrTabelaNom_Jsonclick = "";
         edtFuncaoAPFAtributos_AtrTabelaCod_Jsonclick = "";
         edtFuncaoAPFAtributos_FuncaoDadosCod_Jsonclick = "";
         edtFuncaoAPFAtributos_AtributosNom_Jsonclick = "";
         edtFuncaoAPFAtributos_AtributosCod_Jsonclick = "";
         edtFuncaoAPF_Nome_Jsonclick = "";
         edtFuncaoAPF_Codigo_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavFuncaoapfatributos_atributosnom1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavFuncaoapfatributos_atributosnom2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavFuncaoapfatributos_atributosnom3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         edtFuncoesAPFAtributos_Descricao_Titleformat = 0;
         edtFuncoesAPFAtributos_Nome_Titleformat = 0;
         edtFuncoesAPFAtributos_Code_Titleformat = 0;
         chkFuncoesAPFAtributos_Regra_Titleformat = 0;
         edtFuncaoAPFAtributos_AtrTabelaNom_Titleformat = 0;
         edtFuncaoAPFAtributos_AtrTabelaCod_Titleformat = 0;
         edtFuncaoAPFAtributos_FuncaoDadosCod_Titleformat = 0;
         edtFuncaoAPFAtributos_AtributosNom_Titleformat = 0;
         edtFuncaoAPFAtributos_AtributosCod_Titleformat = 0;
         edtFuncaoAPF_Nome_Titleformat = 0;
         edtFuncaoAPF_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavFuncaoapfatributos_atributosnom3_Visible = 1;
         edtavFuncaoapf_nome3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavFuncaoapfatributos_atributosnom2_Visible = 1;
         edtavFuncaoapf_nome2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavFuncaoapfatributos_atributosnom1_Visible = 1;
         edtavFuncaoapf_nome1_Visible = 1;
         edtFuncoesAPFAtributos_Descricao_Title = "Descri��o";
         edtFuncoesAPFAtributos_Nome_Title = "Nome";
         edtFuncoesAPFAtributos_Code_Title = "Codigo";
         chkFuncoesAPFAtributos_Regra.Title.Text = "de Neg�cio";
         edtFuncaoAPFAtributos_AtrTabelaNom_Title = "Tabela";
         edtFuncaoAPFAtributos_AtrTabelaCod_Title = "Tabela";
         edtFuncaoAPFAtributos_FuncaoDadosCod_Title = "Dados";
         edtFuncaoAPFAtributos_AtributosNom_Title = "Atributo";
         edtFuncaoAPFAtributos_AtributosCod_Title = "Atributo";
         edtFuncaoAPF_Nome_Title = "Fun��o de Transa��o";
         edtFuncaoAPF_Codigo_Title = "Fun��o de Transa��o";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkFuncoesAPFAtributos_Regra.Caption = "";
         edtavDdo_funcoesapfatributos_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcoesapfatributos_nometitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcoesapfatributos_codetitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcoesapfatributos_regratitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaoapfatributos_atrtabelanomtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaoapfatributos_atrtabelacodtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaoapfatributos_funcaodadoscodtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaoapfatributos_atributosnomtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaoapfatributos_atributoscodtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaoapf_nometitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaoapf_codigotitlecontrolidtoreplace_Visible = 1;
         edtavTffuncoesapfatributos_descricao_sel_Jsonclick = "";
         edtavTffuncoesapfatributos_descricao_sel_Visible = 1;
         edtavTffuncoesapfatributos_descricao_Jsonclick = "";
         edtavTffuncoesapfatributos_descricao_Visible = 1;
         edtavTffuncoesapfatributos_nome_sel_Jsonclick = "";
         edtavTffuncoesapfatributos_nome_sel_Visible = 1;
         edtavTffuncoesapfatributos_nome_Jsonclick = "";
         edtavTffuncoesapfatributos_nome_Visible = 1;
         edtavTffuncoesapfatributos_code_sel_Jsonclick = "";
         edtavTffuncoesapfatributos_code_sel_Visible = 1;
         edtavTffuncoesapfatributos_code_Jsonclick = "";
         edtavTffuncoesapfatributos_code_Visible = 1;
         edtavTffuncoesapfatributos_regra_sel_Jsonclick = "";
         edtavTffuncoesapfatributos_regra_sel_Visible = 1;
         edtavTffuncaoapfatributos_atrtabelanom_sel_Jsonclick = "";
         edtavTffuncaoapfatributos_atrtabelanom_sel_Visible = 1;
         edtavTffuncaoapfatributos_atrtabelanom_Jsonclick = "";
         edtavTffuncaoapfatributos_atrtabelanom_Visible = 1;
         edtavTffuncaoapfatributos_atrtabelacod_to_Jsonclick = "";
         edtavTffuncaoapfatributos_atrtabelacod_to_Visible = 1;
         edtavTffuncaoapfatributos_atrtabelacod_Jsonclick = "";
         edtavTffuncaoapfatributos_atrtabelacod_Visible = 1;
         edtavTffuncaoapfatributos_funcaodadoscod_to_Jsonclick = "";
         edtavTffuncaoapfatributos_funcaodadoscod_to_Visible = 1;
         edtavTffuncaoapfatributos_funcaodadoscod_Jsonclick = "";
         edtavTffuncaoapfatributos_funcaodadoscod_Visible = 1;
         edtavTffuncaoapfatributos_atributosnom_sel_Jsonclick = "";
         edtavTffuncaoapfatributos_atributosnom_sel_Visible = 1;
         edtavTffuncaoapfatributos_atributosnom_Jsonclick = "";
         edtavTffuncaoapfatributos_atributosnom_Visible = 1;
         edtavTffuncaoapfatributos_atributoscod_to_Jsonclick = "";
         edtavTffuncaoapfatributos_atributoscod_to_Visible = 1;
         edtavTffuncaoapfatributos_atributoscod_Jsonclick = "";
         edtavTffuncaoapfatributos_atributoscod_Visible = 1;
         edtavTffuncaoapf_nome_sel_Visible = 1;
         edtavTffuncaoapf_nome_Visible = 1;
         edtavTffuncaoapf_codigo_to_Jsonclick = "";
         edtavTffuncaoapf_codigo_to_Visible = 1;
         edtavTffuncaoapf_codigo_Jsonclick = "";
         edtavTffuncaoapf_codigo_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_funcoesapfatributos_descricao_Searchbuttontext = "Pesquisar";
         Ddo_funcoesapfatributos_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_funcoesapfatributos_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_funcoesapfatributos_descricao_Loadingdata = "Carregando dados...";
         Ddo_funcoesapfatributos_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_funcoesapfatributos_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_funcoesapfatributos_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_funcoesapfatributos_descricao_Datalistproc = "GetPromptFuncoesAPFAtributosFilterData";
         Ddo_funcoesapfatributos_descricao_Datalisttype = "Dynamic";
         Ddo_funcoesapfatributos_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcoesapfatributos_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_funcoesapfatributos_descricao_Filtertype = "Character";
         Ddo_funcoesapfatributos_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcoesapfatributos_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcoesapfatributos_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcoesapfatributos_descricao_Titlecontrolidtoreplace = "";
         Ddo_funcoesapfatributos_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcoesapfatributos_descricao_Cls = "ColumnSettings";
         Ddo_funcoesapfatributos_descricao_Tooltip = "Op��es";
         Ddo_funcoesapfatributos_descricao_Caption = "";
         Ddo_funcoesapfatributos_nome_Searchbuttontext = "Pesquisar";
         Ddo_funcoesapfatributos_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_funcoesapfatributos_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_funcoesapfatributos_nome_Loadingdata = "Carregando dados...";
         Ddo_funcoesapfatributos_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_funcoesapfatributos_nome_Sortasc = "Ordenar de A � Z";
         Ddo_funcoesapfatributos_nome_Datalistupdateminimumcharacters = 0;
         Ddo_funcoesapfatributos_nome_Datalistproc = "GetPromptFuncoesAPFAtributosFilterData";
         Ddo_funcoesapfatributos_nome_Datalisttype = "Dynamic";
         Ddo_funcoesapfatributos_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcoesapfatributos_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_funcoesapfatributos_nome_Filtertype = "Character";
         Ddo_funcoesapfatributos_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcoesapfatributos_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcoesapfatributos_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcoesapfatributos_nome_Titlecontrolidtoreplace = "";
         Ddo_funcoesapfatributos_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcoesapfatributos_nome_Cls = "ColumnSettings";
         Ddo_funcoesapfatributos_nome_Tooltip = "Op��es";
         Ddo_funcoesapfatributos_nome_Caption = "";
         Ddo_funcoesapfatributos_code_Searchbuttontext = "Pesquisar";
         Ddo_funcoesapfatributos_code_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_funcoesapfatributos_code_Cleanfilter = "Limpar pesquisa";
         Ddo_funcoesapfatributos_code_Loadingdata = "Carregando dados...";
         Ddo_funcoesapfatributos_code_Sortdsc = "Ordenar de Z � A";
         Ddo_funcoesapfatributos_code_Sortasc = "Ordenar de A � Z";
         Ddo_funcoesapfatributos_code_Datalistupdateminimumcharacters = 0;
         Ddo_funcoesapfatributos_code_Datalistproc = "GetPromptFuncoesAPFAtributosFilterData";
         Ddo_funcoesapfatributos_code_Datalisttype = "Dynamic";
         Ddo_funcoesapfatributos_code_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcoesapfatributos_code_Filterisrange = Convert.ToBoolean( 0);
         Ddo_funcoesapfatributos_code_Filtertype = "Character";
         Ddo_funcoesapfatributos_code_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcoesapfatributos_code_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcoesapfatributos_code_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcoesapfatributos_code_Titlecontrolidtoreplace = "";
         Ddo_funcoesapfatributos_code_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcoesapfatributos_code_Cls = "ColumnSettings";
         Ddo_funcoesapfatributos_code_Tooltip = "Op��es";
         Ddo_funcoesapfatributos_code_Caption = "";
         Ddo_funcoesapfatributos_regra_Searchbuttontext = "Pesquisar";
         Ddo_funcoesapfatributos_regra_Cleanfilter = "Limpar pesquisa";
         Ddo_funcoesapfatributos_regra_Sortdsc = "Ordenar de Z � A";
         Ddo_funcoesapfatributos_regra_Sortasc = "Ordenar de A � Z";
         Ddo_funcoesapfatributos_regra_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_funcoesapfatributos_regra_Datalisttype = "FixedValues";
         Ddo_funcoesapfatributos_regra_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcoesapfatributos_regra_Includefilter = Convert.ToBoolean( 0);
         Ddo_funcoesapfatributos_regra_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcoesapfatributos_regra_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcoesapfatributos_regra_Titlecontrolidtoreplace = "";
         Ddo_funcoesapfatributos_regra_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcoesapfatributos_regra_Cls = "ColumnSettings";
         Ddo_funcoesapfatributos_regra_Tooltip = "Op��es";
         Ddo_funcoesapfatributos_regra_Caption = "";
         Ddo_funcaoapfatributos_atrtabelanom_Searchbuttontext = "Pesquisar";
         Ddo_funcaoapfatributos_atrtabelanom_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_funcaoapfatributos_atrtabelanom_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaoapfatributos_atrtabelanom_Loadingdata = "Carregando dados...";
         Ddo_funcaoapfatributos_atrtabelanom_Sortdsc = "Ordenar de Z � A";
         Ddo_funcaoapfatributos_atrtabelanom_Sortasc = "Ordenar de A � Z";
         Ddo_funcaoapfatributos_atrtabelanom_Datalistupdateminimumcharacters = 0;
         Ddo_funcaoapfatributos_atrtabelanom_Datalistproc = "GetPromptFuncoesAPFAtributosFilterData";
         Ddo_funcaoapfatributos_atrtabelanom_Datalisttype = "Dynamic";
         Ddo_funcaoapfatributos_atrtabelanom_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcaoapfatributos_atrtabelanom_Filterisrange = Convert.ToBoolean( 0);
         Ddo_funcaoapfatributos_atrtabelanom_Filtertype = "Character";
         Ddo_funcaoapfatributos_atrtabelanom_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaoapfatributos_atrtabelanom_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcaoapfatributos_atrtabelanom_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcaoapfatributos_atrtabelanom_Titlecontrolidtoreplace = "";
         Ddo_funcaoapfatributos_atrtabelanom_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaoapfatributos_atrtabelanom_Cls = "ColumnSettings";
         Ddo_funcaoapfatributos_atrtabelanom_Tooltip = "Op��es";
         Ddo_funcaoapfatributos_atrtabelanom_Caption = "";
         Ddo_funcaoapfatributos_atrtabelacod_Searchbuttontext = "Pesquisar";
         Ddo_funcaoapfatributos_atrtabelacod_Rangefilterto = "At�";
         Ddo_funcaoapfatributos_atrtabelacod_Rangefilterfrom = "Desde";
         Ddo_funcaoapfatributos_atrtabelacod_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaoapfatributos_atrtabelacod_Sortdsc = "Ordenar de Z � A";
         Ddo_funcaoapfatributos_atrtabelacod_Sortasc = "Ordenar de A � Z";
         Ddo_funcaoapfatributos_atrtabelacod_Includedatalist = Convert.ToBoolean( 0);
         Ddo_funcaoapfatributos_atrtabelacod_Filterisrange = Convert.ToBoolean( -1);
         Ddo_funcaoapfatributos_atrtabelacod_Filtertype = "Numeric";
         Ddo_funcaoapfatributos_atrtabelacod_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaoapfatributos_atrtabelacod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcaoapfatributos_atrtabelacod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcaoapfatributos_atrtabelacod_Titlecontrolidtoreplace = "";
         Ddo_funcaoapfatributos_atrtabelacod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaoapfatributos_atrtabelacod_Cls = "ColumnSettings";
         Ddo_funcaoapfatributos_atrtabelacod_Tooltip = "Op��es";
         Ddo_funcaoapfatributos_atrtabelacod_Caption = "";
         Ddo_funcaoapfatributos_funcaodadoscod_Searchbuttontext = "Pesquisar";
         Ddo_funcaoapfatributos_funcaodadoscod_Rangefilterto = "At�";
         Ddo_funcaoapfatributos_funcaodadoscod_Rangefilterfrom = "Desde";
         Ddo_funcaoapfatributos_funcaodadoscod_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaoapfatributos_funcaodadoscod_Sortdsc = "Ordenar de Z � A";
         Ddo_funcaoapfatributos_funcaodadoscod_Sortasc = "Ordenar de A � Z";
         Ddo_funcaoapfatributos_funcaodadoscod_Includedatalist = Convert.ToBoolean( 0);
         Ddo_funcaoapfatributos_funcaodadoscod_Filterisrange = Convert.ToBoolean( -1);
         Ddo_funcaoapfatributos_funcaodadoscod_Filtertype = "Numeric";
         Ddo_funcaoapfatributos_funcaodadoscod_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaoapfatributos_funcaodadoscod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcaoapfatributos_funcaodadoscod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcaoapfatributos_funcaodadoscod_Titlecontrolidtoreplace = "";
         Ddo_funcaoapfatributos_funcaodadoscod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaoapfatributos_funcaodadoscod_Cls = "ColumnSettings";
         Ddo_funcaoapfatributos_funcaodadoscod_Tooltip = "Op��es";
         Ddo_funcaoapfatributos_funcaodadoscod_Caption = "";
         Ddo_funcaoapfatributos_atributosnom_Searchbuttontext = "Pesquisar";
         Ddo_funcaoapfatributos_atributosnom_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_funcaoapfatributos_atributosnom_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaoapfatributos_atributosnom_Loadingdata = "Carregando dados...";
         Ddo_funcaoapfatributos_atributosnom_Sortdsc = "Ordenar de Z � A";
         Ddo_funcaoapfatributos_atributosnom_Sortasc = "Ordenar de A � Z";
         Ddo_funcaoapfatributos_atributosnom_Datalistupdateminimumcharacters = 0;
         Ddo_funcaoapfatributos_atributosnom_Datalistproc = "GetPromptFuncoesAPFAtributosFilterData";
         Ddo_funcaoapfatributos_atributosnom_Datalisttype = "Dynamic";
         Ddo_funcaoapfatributos_atributosnom_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcaoapfatributos_atributosnom_Filterisrange = Convert.ToBoolean( 0);
         Ddo_funcaoapfatributos_atributosnom_Filtertype = "Character";
         Ddo_funcaoapfatributos_atributosnom_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaoapfatributos_atributosnom_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcaoapfatributos_atributosnom_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcaoapfatributos_atributosnom_Titlecontrolidtoreplace = "";
         Ddo_funcaoapfatributos_atributosnom_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaoapfatributos_atributosnom_Cls = "ColumnSettings";
         Ddo_funcaoapfatributos_atributosnom_Tooltip = "Op��es";
         Ddo_funcaoapfatributos_atributosnom_Caption = "";
         Ddo_funcaoapfatributos_atributoscod_Searchbuttontext = "Pesquisar";
         Ddo_funcaoapfatributos_atributoscod_Rangefilterto = "At�";
         Ddo_funcaoapfatributos_atributoscod_Rangefilterfrom = "Desde";
         Ddo_funcaoapfatributos_atributoscod_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaoapfatributos_atributoscod_Sortdsc = "Ordenar de Z � A";
         Ddo_funcaoapfatributos_atributoscod_Sortasc = "Ordenar de A � Z";
         Ddo_funcaoapfatributos_atributoscod_Includedatalist = Convert.ToBoolean( 0);
         Ddo_funcaoapfatributos_atributoscod_Filterisrange = Convert.ToBoolean( -1);
         Ddo_funcaoapfatributos_atributoscod_Filtertype = "Numeric";
         Ddo_funcaoapfatributos_atributoscod_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaoapfatributos_atributoscod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcaoapfatributos_atributoscod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcaoapfatributos_atributoscod_Titlecontrolidtoreplace = "";
         Ddo_funcaoapfatributos_atributoscod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaoapfatributos_atributoscod_Cls = "ColumnSettings";
         Ddo_funcaoapfatributos_atributoscod_Tooltip = "Op��es";
         Ddo_funcaoapfatributos_atributoscod_Caption = "";
         Ddo_funcaoapf_nome_Searchbuttontext = "Pesquisar";
         Ddo_funcaoapf_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_funcaoapf_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaoapf_nome_Loadingdata = "Carregando dados...";
         Ddo_funcaoapf_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_funcaoapf_nome_Sortasc = "Ordenar de A � Z";
         Ddo_funcaoapf_nome_Datalistupdateminimumcharacters = 0;
         Ddo_funcaoapf_nome_Datalistproc = "GetPromptFuncoesAPFAtributosFilterData";
         Ddo_funcaoapf_nome_Datalisttype = "Dynamic";
         Ddo_funcaoapf_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcaoapf_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_funcaoapf_nome_Filtertype = "Character";
         Ddo_funcaoapf_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaoapf_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcaoapf_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcaoapf_nome_Titlecontrolidtoreplace = "";
         Ddo_funcaoapf_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaoapf_nome_Cls = "ColumnSettings";
         Ddo_funcaoapf_nome_Tooltip = "Op��es";
         Ddo_funcaoapf_nome_Caption = "";
         Ddo_funcaoapf_codigo_Searchbuttontext = "Pesquisar";
         Ddo_funcaoapf_codigo_Rangefilterto = "At�";
         Ddo_funcaoapf_codigo_Rangefilterfrom = "Desde";
         Ddo_funcaoapf_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaoapf_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_funcaoapf_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_funcaoapf_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_funcaoapf_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_funcaoapf_codigo_Filtertype = "Numeric";
         Ddo_funcaoapf_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaoapf_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcaoapf_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcaoapf_codigo_Titlecontrolidtoreplace = "";
         Ddo_funcaoapf_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaoapf_codigo_Cls = "ColumnSettings";
         Ddo_funcaoapf_codigo_Tooltip = "Op��es";
         Ddo_funcaoapf_codigo_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Atributos das Fun��es de APF";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV37ddo_FuncaoAPF_CodigoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoAPFAtributos_AtributosCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoAPFAtributos_FuncaoDadosCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoAPFAtributos_AtrTabelaCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_FuncoesAPFAtributos_RegraTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_REGRATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_FuncoesAPFAtributos_CodeTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_CODETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_FuncoesAPFAtributos_NomeTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncoesAPFAtributos_DescricaoTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV35TFFuncaoAPF_Codigo',fld:'vTFFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFFuncaoAPF_Codigo_To',fld:'vTFFUNCAOAPF_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV40TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV43TFFuncaoAPFAtributos_AtributosCod',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFFuncaoAPFAtributos_AtributosCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFFuncaoAPFAtributos_AtributosNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',pic:'@!',nv:''},{av:'AV48TFFuncaoAPFAtributos_AtributosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL',pic:'@!',nv:''},{av:'AV51TFFuncaoAPFAtributos_FuncaoDadosCod',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFFuncaoAPFAtributos_AtrTabelaCod',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD',pic:'ZZZZZ9',nv:0},{av:'AV56TFFuncaoAPFAtributos_AtrTabelaCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV59TFFuncaoAPFAtributos_AtrTabelaNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM',pic:'@!',nv:''},{av:'AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL',pic:'@!',nv:''},{av:'AV63TFFuncoesAPFAtributos_Regra_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_REGRA_SEL',pic:'9',nv:0},{av:'AV66TFFuncoesAPFAtributos_Code',fld:'vTFFUNCOESAPFATRIBUTOS_CODE',pic:'',nv:''},{av:'AV67TFFuncoesAPFAtributos_Code_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_CODE_SEL',pic:'',nv:''},{av:'AV70TFFuncoesAPFAtributos_Nome',fld:'vTFFUNCOESAPFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV71TFFuncoesAPFAtributos_Nome_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_NOME_SEL',pic:'@!',nv:''},{av:'AV74TFFuncoesAPFAtributos_Descricao',fld:'vTFFUNCOESAPFATRIBUTOS_DESCRICAO',pic:'@!',nv:''},{av:'AV75TFFuncoesAPFAtributos_Descricao_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV18FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV19FuncaoAPFAtributos_AtributosNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',pic:'@!',nv:''},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV23FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV24FuncaoAPFAtributos_AtributosNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',pic:'@!',nv:''},{av:'AV28FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29FuncaoAPFAtributos_AtributosNom3',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM3',pic:'@!',nv:''}],oparms:[{av:'AV34FuncaoAPF_CodigoTitleFilterData',fld:'vFUNCAOAPF_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV38FuncaoAPF_NomeTitleFilterData',fld:'vFUNCAOAPF_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV42FuncaoAPFAtributos_AtributosCodTitleFilterData',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSCODTITLEFILTERDATA',pic:'',nv:null},{av:'AV46FuncaoAPFAtributos_AtributosNomTitleFilterData',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV50FuncaoAPFAtributos_FuncaoDadosCodTitleFilterData',fld:'vFUNCAOAPFATRIBUTOS_FUNCAODADOSCODTITLEFILTERDATA',pic:'',nv:null},{av:'AV54FuncaoAPFAtributos_AtrTabelaCodTitleFilterData',fld:'vFUNCAOAPFATRIBUTOS_ATRTABELACODTITLEFILTERDATA',pic:'',nv:null},{av:'AV58FuncaoAPFAtributos_AtrTabelaNomTitleFilterData',fld:'vFUNCAOAPFATRIBUTOS_ATRTABELANOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV62FuncoesAPFAtributos_RegraTitleFilterData',fld:'vFUNCOESAPFATRIBUTOS_REGRATITLEFILTERDATA',pic:'',nv:null},{av:'AV65FuncoesAPFAtributos_CodeTitleFilterData',fld:'vFUNCOESAPFATRIBUTOS_CODETITLEFILTERDATA',pic:'',nv:null},{av:'AV69FuncoesAPFAtributos_NomeTitleFilterData',fld:'vFUNCOESAPFATRIBUTOS_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV73FuncoesAPFAtributos_DescricaoTitleFilterData',fld:'vFUNCOESAPFATRIBUTOS_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtFuncaoAPF_Codigo_Titleformat',ctrl:'FUNCAOAPF_CODIGO',prop:'Titleformat'},{av:'edtFuncaoAPF_Codigo_Title',ctrl:'FUNCAOAPF_CODIGO',prop:'Title'},{av:'edtFuncaoAPF_Nome_Titleformat',ctrl:'FUNCAOAPF_NOME',prop:'Titleformat'},{av:'edtFuncaoAPF_Nome_Title',ctrl:'FUNCAOAPF_NOME',prop:'Title'},{av:'edtFuncaoAPFAtributos_AtributosCod_Titleformat',ctrl:'FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',prop:'Titleformat'},{av:'edtFuncaoAPFAtributos_AtributosCod_Title',ctrl:'FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',prop:'Title'},{av:'edtFuncaoAPFAtributos_AtributosNom_Titleformat',ctrl:'FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',prop:'Titleformat'},{av:'edtFuncaoAPFAtributos_AtributosNom_Title',ctrl:'FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',prop:'Title'},{av:'edtFuncaoAPFAtributos_FuncaoDadosCod_Titleformat',ctrl:'FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',prop:'Titleformat'},{av:'edtFuncaoAPFAtributos_FuncaoDadosCod_Title',ctrl:'FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',prop:'Title'},{av:'edtFuncaoAPFAtributos_AtrTabelaCod_Titleformat',ctrl:'FUNCAOAPFATRIBUTOS_ATRTABELACOD',prop:'Titleformat'},{av:'edtFuncaoAPFAtributos_AtrTabelaCod_Title',ctrl:'FUNCAOAPFATRIBUTOS_ATRTABELACOD',prop:'Title'},{av:'edtFuncaoAPFAtributos_AtrTabelaNom_Titleformat',ctrl:'FUNCAOAPFATRIBUTOS_ATRTABELANOM',prop:'Titleformat'},{av:'edtFuncaoAPFAtributos_AtrTabelaNom_Title',ctrl:'FUNCAOAPFATRIBUTOS_ATRTABELANOM',prop:'Title'},{av:'chkFuncoesAPFAtributos_Regra_Titleformat',ctrl:'FUNCOESAPFATRIBUTOS_REGRA',prop:'Titleformat'},{av:'chkFuncoesAPFAtributos_Regra.Title.Text',ctrl:'FUNCOESAPFATRIBUTOS_REGRA',prop:'Title'},{av:'edtFuncoesAPFAtributos_Code_Titleformat',ctrl:'FUNCOESAPFATRIBUTOS_CODE',prop:'Titleformat'},{av:'edtFuncoesAPFAtributos_Code_Title',ctrl:'FUNCOESAPFATRIBUTOS_CODE',prop:'Title'},{av:'edtFuncoesAPFAtributos_Nome_Titleformat',ctrl:'FUNCOESAPFATRIBUTOS_NOME',prop:'Titleformat'},{av:'edtFuncoesAPFAtributos_Nome_Title',ctrl:'FUNCOESAPFATRIBUTOS_NOME',prop:'Title'},{av:'edtFuncoesAPFAtributos_Descricao_Titleformat',ctrl:'FUNCOESAPFATRIBUTOS_DESCRICAO',prop:'Titleformat'},{av:'edtFuncoesAPFAtributos_Descricao_Title',ctrl:'FUNCOESAPFATRIBUTOS_DESCRICAO',prop:'Title'},{av:'AV79GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV80GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E119N2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV19FuncaoAPFAtributos_AtributosNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV24FuncaoAPFAtributos_AtributosNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV29FuncaoAPFAtributos_AtributosNom3',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFFuncaoAPF_Codigo',fld:'vTFFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFFuncaoAPF_Codigo_To',fld:'vTFFUNCAOAPF_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV40TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV43TFFuncaoAPFAtributos_AtributosCod',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFFuncaoAPFAtributos_AtributosCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFFuncaoAPFAtributos_AtributosNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',pic:'@!',nv:''},{av:'AV48TFFuncaoAPFAtributos_AtributosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL',pic:'@!',nv:''},{av:'AV51TFFuncaoAPFAtributos_FuncaoDadosCod',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFFuncaoAPFAtributos_AtrTabelaCod',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD',pic:'ZZZZZ9',nv:0},{av:'AV56TFFuncaoAPFAtributos_AtrTabelaCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV59TFFuncaoAPFAtributos_AtrTabelaNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM',pic:'@!',nv:''},{av:'AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL',pic:'@!',nv:''},{av:'AV63TFFuncoesAPFAtributos_Regra_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_REGRA_SEL',pic:'9',nv:0},{av:'AV66TFFuncoesAPFAtributos_Code',fld:'vTFFUNCOESAPFATRIBUTOS_CODE',pic:'',nv:''},{av:'AV67TFFuncoesAPFAtributos_Code_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_CODE_SEL',pic:'',nv:''},{av:'AV70TFFuncoesAPFAtributos_Nome',fld:'vTFFUNCOESAPFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV71TFFuncoesAPFAtributos_Nome_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_NOME_SEL',pic:'@!',nv:''},{av:'AV74TFFuncoesAPFAtributos_Descricao',fld:'vTFFUNCOESAPFATRIBUTOS_DESCRICAO',pic:'@!',nv:''},{av:'AV75TFFuncoesAPFAtributos_Descricao_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV37ddo_FuncaoAPF_CodigoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoAPFAtributos_AtributosCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoAPFAtributos_FuncaoDadosCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoAPFAtributos_AtrTabelaCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_FuncoesAPFAtributos_RegraTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_REGRATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_FuncoesAPFAtributos_CodeTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_CODETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_FuncoesAPFAtributos_NomeTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncoesAPFAtributos_DescricaoTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_FUNCAOAPF_CODIGO.ONOPTIONCLICKED","{handler:'E129N2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV19FuncaoAPFAtributos_AtributosNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV24FuncaoAPFAtributos_AtributosNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV29FuncaoAPFAtributos_AtributosNom3',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFFuncaoAPF_Codigo',fld:'vTFFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFFuncaoAPF_Codigo_To',fld:'vTFFUNCAOAPF_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV40TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV43TFFuncaoAPFAtributos_AtributosCod',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFFuncaoAPFAtributos_AtributosCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFFuncaoAPFAtributos_AtributosNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',pic:'@!',nv:''},{av:'AV48TFFuncaoAPFAtributos_AtributosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL',pic:'@!',nv:''},{av:'AV51TFFuncaoAPFAtributos_FuncaoDadosCod',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFFuncaoAPFAtributos_AtrTabelaCod',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD',pic:'ZZZZZ9',nv:0},{av:'AV56TFFuncaoAPFAtributos_AtrTabelaCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV59TFFuncaoAPFAtributos_AtrTabelaNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM',pic:'@!',nv:''},{av:'AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL',pic:'@!',nv:''},{av:'AV63TFFuncoesAPFAtributos_Regra_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_REGRA_SEL',pic:'9',nv:0},{av:'AV66TFFuncoesAPFAtributos_Code',fld:'vTFFUNCOESAPFATRIBUTOS_CODE',pic:'',nv:''},{av:'AV67TFFuncoesAPFAtributos_Code_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_CODE_SEL',pic:'',nv:''},{av:'AV70TFFuncoesAPFAtributos_Nome',fld:'vTFFUNCOESAPFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV71TFFuncoesAPFAtributos_Nome_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_NOME_SEL',pic:'@!',nv:''},{av:'AV74TFFuncoesAPFAtributos_Descricao',fld:'vTFFUNCOESAPFATRIBUTOS_DESCRICAO',pic:'@!',nv:''},{av:'AV75TFFuncoesAPFAtributos_Descricao_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV37ddo_FuncaoAPF_CodigoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoAPFAtributos_AtributosCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoAPFAtributos_FuncaoDadosCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoAPFAtributos_AtrTabelaCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_FuncoesAPFAtributos_RegraTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_REGRATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_FuncoesAPFAtributos_CodeTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_CODETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_FuncoesAPFAtributos_NomeTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncoesAPFAtributos_DescricaoTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_funcaoapf_codigo_Activeeventkey',ctrl:'DDO_FUNCAOAPF_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_funcaoapf_codigo_Filteredtext_get',ctrl:'DDO_FUNCAOAPF_CODIGO',prop:'FilteredText_get'},{av:'Ddo_funcaoapf_codigo_Filteredtextto_get',ctrl:'DDO_FUNCAOAPF_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcaoapf_codigo_Sortedstatus',ctrl:'DDO_FUNCAOAPF_CODIGO',prop:'SortedStatus'},{av:'AV35TFFuncaoAPF_Codigo',fld:'vTFFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFFuncaoAPF_Codigo_To',fld:'vTFFUNCAOAPF_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_funcaoapf_nome_Sortedstatus',ctrl:'DDO_FUNCAOAPF_NOME',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_atributoscod_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_atributosnom_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_funcaodadoscod_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_atrtabelacod_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_atrtabelanom_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM',prop:'SortedStatus'},{av:'Ddo_funcoesapfatributos_regra_Sortedstatus',ctrl:'DDO_FUNCOESAPFATRIBUTOS_REGRA',prop:'SortedStatus'},{av:'Ddo_funcoesapfatributos_code_Sortedstatus',ctrl:'DDO_FUNCOESAPFATRIBUTOS_CODE',prop:'SortedStatus'},{av:'Ddo_funcoesapfatributos_nome_Sortedstatus',ctrl:'DDO_FUNCOESAPFATRIBUTOS_NOME',prop:'SortedStatus'},{av:'Ddo_funcoesapfatributos_descricao_Sortedstatus',ctrl:'DDO_FUNCOESAPFATRIBUTOS_DESCRICAO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_FUNCAOAPF_NOME.ONOPTIONCLICKED","{handler:'E139N2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV19FuncaoAPFAtributos_AtributosNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV24FuncaoAPFAtributos_AtributosNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV29FuncaoAPFAtributos_AtributosNom3',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFFuncaoAPF_Codigo',fld:'vTFFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFFuncaoAPF_Codigo_To',fld:'vTFFUNCAOAPF_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV40TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV43TFFuncaoAPFAtributos_AtributosCod',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFFuncaoAPFAtributos_AtributosCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFFuncaoAPFAtributos_AtributosNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',pic:'@!',nv:''},{av:'AV48TFFuncaoAPFAtributos_AtributosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL',pic:'@!',nv:''},{av:'AV51TFFuncaoAPFAtributos_FuncaoDadosCod',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFFuncaoAPFAtributos_AtrTabelaCod',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD',pic:'ZZZZZ9',nv:0},{av:'AV56TFFuncaoAPFAtributos_AtrTabelaCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV59TFFuncaoAPFAtributos_AtrTabelaNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM',pic:'@!',nv:''},{av:'AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL',pic:'@!',nv:''},{av:'AV63TFFuncoesAPFAtributos_Regra_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_REGRA_SEL',pic:'9',nv:0},{av:'AV66TFFuncoesAPFAtributos_Code',fld:'vTFFUNCOESAPFATRIBUTOS_CODE',pic:'',nv:''},{av:'AV67TFFuncoesAPFAtributos_Code_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_CODE_SEL',pic:'',nv:''},{av:'AV70TFFuncoesAPFAtributos_Nome',fld:'vTFFUNCOESAPFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV71TFFuncoesAPFAtributos_Nome_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_NOME_SEL',pic:'@!',nv:''},{av:'AV74TFFuncoesAPFAtributos_Descricao',fld:'vTFFUNCOESAPFATRIBUTOS_DESCRICAO',pic:'@!',nv:''},{av:'AV75TFFuncoesAPFAtributos_Descricao_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV37ddo_FuncaoAPF_CodigoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoAPFAtributos_AtributosCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoAPFAtributos_FuncaoDadosCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoAPFAtributos_AtrTabelaCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_FuncoesAPFAtributos_RegraTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_REGRATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_FuncoesAPFAtributos_CodeTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_CODETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_FuncoesAPFAtributos_NomeTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncoesAPFAtributos_DescricaoTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_funcaoapf_nome_Activeeventkey',ctrl:'DDO_FUNCAOAPF_NOME',prop:'ActiveEventKey'},{av:'Ddo_funcaoapf_nome_Filteredtext_get',ctrl:'DDO_FUNCAOAPF_NOME',prop:'FilteredText_get'},{av:'Ddo_funcaoapf_nome_Selectedvalue_get',ctrl:'DDO_FUNCAOAPF_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcaoapf_nome_Sortedstatus',ctrl:'DDO_FUNCAOAPF_NOME',prop:'SortedStatus'},{av:'AV39TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV40TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'Ddo_funcaoapf_codigo_Sortedstatus',ctrl:'DDO_FUNCAOAPF_CODIGO',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_atributoscod_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_atributosnom_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_funcaodadoscod_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_atrtabelacod_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_atrtabelanom_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM',prop:'SortedStatus'},{av:'Ddo_funcoesapfatributos_regra_Sortedstatus',ctrl:'DDO_FUNCOESAPFATRIBUTOS_REGRA',prop:'SortedStatus'},{av:'Ddo_funcoesapfatributos_code_Sortedstatus',ctrl:'DDO_FUNCOESAPFATRIBUTOS_CODE',prop:'SortedStatus'},{av:'Ddo_funcoesapfatributos_nome_Sortedstatus',ctrl:'DDO_FUNCOESAPFATRIBUTOS_NOME',prop:'SortedStatus'},{av:'Ddo_funcoesapfatributos_descricao_Sortedstatus',ctrl:'DDO_FUNCOESAPFATRIBUTOS_DESCRICAO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD.ONOPTIONCLICKED","{handler:'E149N2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV19FuncaoAPFAtributos_AtributosNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV24FuncaoAPFAtributos_AtributosNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV29FuncaoAPFAtributos_AtributosNom3',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFFuncaoAPF_Codigo',fld:'vTFFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFFuncaoAPF_Codigo_To',fld:'vTFFUNCAOAPF_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV40TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV43TFFuncaoAPFAtributos_AtributosCod',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFFuncaoAPFAtributos_AtributosCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFFuncaoAPFAtributos_AtributosNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',pic:'@!',nv:''},{av:'AV48TFFuncaoAPFAtributos_AtributosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL',pic:'@!',nv:''},{av:'AV51TFFuncaoAPFAtributos_FuncaoDadosCod',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFFuncaoAPFAtributos_AtrTabelaCod',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD',pic:'ZZZZZ9',nv:0},{av:'AV56TFFuncaoAPFAtributos_AtrTabelaCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV59TFFuncaoAPFAtributos_AtrTabelaNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM',pic:'@!',nv:''},{av:'AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL',pic:'@!',nv:''},{av:'AV63TFFuncoesAPFAtributos_Regra_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_REGRA_SEL',pic:'9',nv:0},{av:'AV66TFFuncoesAPFAtributos_Code',fld:'vTFFUNCOESAPFATRIBUTOS_CODE',pic:'',nv:''},{av:'AV67TFFuncoesAPFAtributos_Code_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_CODE_SEL',pic:'',nv:''},{av:'AV70TFFuncoesAPFAtributos_Nome',fld:'vTFFUNCOESAPFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV71TFFuncoesAPFAtributos_Nome_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_NOME_SEL',pic:'@!',nv:''},{av:'AV74TFFuncoesAPFAtributos_Descricao',fld:'vTFFUNCOESAPFATRIBUTOS_DESCRICAO',pic:'@!',nv:''},{av:'AV75TFFuncoesAPFAtributos_Descricao_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV37ddo_FuncaoAPF_CodigoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoAPFAtributos_AtributosCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoAPFAtributos_FuncaoDadosCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoAPFAtributos_AtrTabelaCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_FuncoesAPFAtributos_RegraTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_REGRATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_FuncoesAPFAtributos_CodeTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_CODETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_FuncoesAPFAtributos_NomeTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncoesAPFAtributos_DescricaoTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_funcaoapfatributos_atributoscod_Activeeventkey',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',prop:'ActiveEventKey'},{av:'Ddo_funcaoapfatributos_atributoscod_Filteredtext_get',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',prop:'FilteredText_get'},{av:'Ddo_funcaoapfatributos_atributoscod_Filteredtextto_get',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcaoapfatributos_atributoscod_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',prop:'SortedStatus'},{av:'AV43TFFuncaoAPFAtributos_AtributosCod',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFFuncaoAPFAtributos_AtributosCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_funcaoapf_codigo_Sortedstatus',ctrl:'DDO_FUNCAOAPF_CODIGO',prop:'SortedStatus'},{av:'Ddo_funcaoapf_nome_Sortedstatus',ctrl:'DDO_FUNCAOAPF_NOME',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_atributosnom_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_funcaodadoscod_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_atrtabelacod_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_atrtabelanom_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM',prop:'SortedStatus'},{av:'Ddo_funcoesapfatributos_regra_Sortedstatus',ctrl:'DDO_FUNCOESAPFATRIBUTOS_REGRA',prop:'SortedStatus'},{av:'Ddo_funcoesapfatributos_code_Sortedstatus',ctrl:'DDO_FUNCOESAPFATRIBUTOS_CODE',prop:'SortedStatus'},{av:'Ddo_funcoesapfatributos_nome_Sortedstatus',ctrl:'DDO_FUNCOESAPFATRIBUTOS_NOME',prop:'SortedStatus'},{av:'Ddo_funcoesapfatributos_descricao_Sortedstatus',ctrl:'DDO_FUNCOESAPFATRIBUTOS_DESCRICAO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM.ONOPTIONCLICKED","{handler:'E159N2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV19FuncaoAPFAtributos_AtributosNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV24FuncaoAPFAtributos_AtributosNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV29FuncaoAPFAtributos_AtributosNom3',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFFuncaoAPF_Codigo',fld:'vTFFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFFuncaoAPF_Codigo_To',fld:'vTFFUNCAOAPF_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV40TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV43TFFuncaoAPFAtributos_AtributosCod',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFFuncaoAPFAtributos_AtributosCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFFuncaoAPFAtributos_AtributosNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',pic:'@!',nv:''},{av:'AV48TFFuncaoAPFAtributos_AtributosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL',pic:'@!',nv:''},{av:'AV51TFFuncaoAPFAtributos_FuncaoDadosCod',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFFuncaoAPFAtributos_AtrTabelaCod',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD',pic:'ZZZZZ9',nv:0},{av:'AV56TFFuncaoAPFAtributos_AtrTabelaCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV59TFFuncaoAPFAtributos_AtrTabelaNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM',pic:'@!',nv:''},{av:'AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL',pic:'@!',nv:''},{av:'AV63TFFuncoesAPFAtributos_Regra_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_REGRA_SEL',pic:'9',nv:0},{av:'AV66TFFuncoesAPFAtributos_Code',fld:'vTFFUNCOESAPFATRIBUTOS_CODE',pic:'',nv:''},{av:'AV67TFFuncoesAPFAtributos_Code_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_CODE_SEL',pic:'',nv:''},{av:'AV70TFFuncoesAPFAtributos_Nome',fld:'vTFFUNCOESAPFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV71TFFuncoesAPFAtributos_Nome_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_NOME_SEL',pic:'@!',nv:''},{av:'AV74TFFuncoesAPFAtributos_Descricao',fld:'vTFFUNCOESAPFATRIBUTOS_DESCRICAO',pic:'@!',nv:''},{av:'AV75TFFuncoesAPFAtributos_Descricao_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV37ddo_FuncaoAPF_CodigoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoAPFAtributos_AtributosCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoAPFAtributos_FuncaoDadosCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoAPFAtributos_AtrTabelaCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_FuncoesAPFAtributos_RegraTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_REGRATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_FuncoesAPFAtributos_CodeTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_CODETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_FuncoesAPFAtributos_NomeTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncoesAPFAtributos_DescricaoTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_funcaoapfatributos_atributosnom_Activeeventkey',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',prop:'ActiveEventKey'},{av:'Ddo_funcaoapfatributos_atributosnom_Filteredtext_get',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',prop:'FilteredText_get'},{av:'Ddo_funcaoapfatributos_atributosnom_Selectedvalue_get',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcaoapfatributos_atributosnom_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',prop:'SortedStatus'},{av:'AV47TFFuncaoAPFAtributos_AtributosNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',pic:'@!',nv:''},{av:'AV48TFFuncaoAPFAtributos_AtributosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL',pic:'@!',nv:''},{av:'Ddo_funcaoapf_codigo_Sortedstatus',ctrl:'DDO_FUNCAOAPF_CODIGO',prop:'SortedStatus'},{av:'Ddo_funcaoapf_nome_Sortedstatus',ctrl:'DDO_FUNCAOAPF_NOME',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_atributoscod_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_funcaodadoscod_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_atrtabelacod_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_atrtabelanom_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM',prop:'SortedStatus'},{av:'Ddo_funcoesapfatributos_regra_Sortedstatus',ctrl:'DDO_FUNCOESAPFATRIBUTOS_REGRA',prop:'SortedStatus'},{av:'Ddo_funcoesapfatributos_code_Sortedstatus',ctrl:'DDO_FUNCOESAPFATRIBUTOS_CODE',prop:'SortedStatus'},{av:'Ddo_funcoesapfatributos_nome_Sortedstatus',ctrl:'DDO_FUNCOESAPFATRIBUTOS_NOME',prop:'SortedStatus'},{av:'Ddo_funcoesapfatributos_descricao_Sortedstatus',ctrl:'DDO_FUNCOESAPFATRIBUTOS_DESCRICAO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD.ONOPTIONCLICKED","{handler:'E169N2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV19FuncaoAPFAtributos_AtributosNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV24FuncaoAPFAtributos_AtributosNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV29FuncaoAPFAtributos_AtributosNom3',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFFuncaoAPF_Codigo',fld:'vTFFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFFuncaoAPF_Codigo_To',fld:'vTFFUNCAOAPF_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV40TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV43TFFuncaoAPFAtributos_AtributosCod',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFFuncaoAPFAtributos_AtributosCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFFuncaoAPFAtributos_AtributosNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',pic:'@!',nv:''},{av:'AV48TFFuncaoAPFAtributos_AtributosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL',pic:'@!',nv:''},{av:'AV51TFFuncaoAPFAtributos_FuncaoDadosCod',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFFuncaoAPFAtributos_AtrTabelaCod',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD',pic:'ZZZZZ9',nv:0},{av:'AV56TFFuncaoAPFAtributos_AtrTabelaCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV59TFFuncaoAPFAtributos_AtrTabelaNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM',pic:'@!',nv:''},{av:'AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL',pic:'@!',nv:''},{av:'AV63TFFuncoesAPFAtributos_Regra_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_REGRA_SEL',pic:'9',nv:0},{av:'AV66TFFuncoesAPFAtributos_Code',fld:'vTFFUNCOESAPFATRIBUTOS_CODE',pic:'',nv:''},{av:'AV67TFFuncoesAPFAtributos_Code_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_CODE_SEL',pic:'',nv:''},{av:'AV70TFFuncoesAPFAtributos_Nome',fld:'vTFFUNCOESAPFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV71TFFuncoesAPFAtributos_Nome_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_NOME_SEL',pic:'@!',nv:''},{av:'AV74TFFuncoesAPFAtributos_Descricao',fld:'vTFFUNCOESAPFATRIBUTOS_DESCRICAO',pic:'@!',nv:''},{av:'AV75TFFuncoesAPFAtributos_Descricao_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV37ddo_FuncaoAPF_CodigoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoAPFAtributos_AtributosCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoAPFAtributos_FuncaoDadosCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoAPFAtributos_AtrTabelaCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_FuncoesAPFAtributos_RegraTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_REGRATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_FuncoesAPFAtributos_CodeTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_CODETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_FuncoesAPFAtributos_NomeTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncoesAPFAtributos_DescricaoTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_funcaoapfatributos_funcaodadoscod_Activeeventkey',ctrl:'DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',prop:'ActiveEventKey'},{av:'Ddo_funcaoapfatributos_funcaodadoscod_Filteredtext_get',ctrl:'DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',prop:'FilteredText_get'},{av:'Ddo_funcaoapfatributos_funcaodadoscod_Filteredtextto_get',ctrl:'DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcaoapfatributos_funcaodadoscod_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',prop:'SortedStatus'},{av:'AV51TFFuncaoAPFAtributos_FuncaoDadosCod',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_funcaoapf_codigo_Sortedstatus',ctrl:'DDO_FUNCAOAPF_CODIGO',prop:'SortedStatus'},{av:'Ddo_funcaoapf_nome_Sortedstatus',ctrl:'DDO_FUNCAOAPF_NOME',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_atributoscod_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_atributosnom_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_atrtabelacod_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_atrtabelanom_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM',prop:'SortedStatus'},{av:'Ddo_funcoesapfatributos_regra_Sortedstatus',ctrl:'DDO_FUNCOESAPFATRIBUTOS_REGRA',prop:'SortedStatus'},{av:'Ddo_funcoesapfatributos_code_Sortedstatus',ctrl:'DDO_FUNCOESAPFATRIBUTOS_CODE',prop:'SortedStatus'},{av:'Ddo_funcoesapfatributos_nome_Sortedstatus',ctrl:'DDO_FUNCOESAPFATRIBUTOS_NOME',prop:'SortedStatus'},{av:'Ddo_funcoesapfatributos_descricao_Sortedstatus',ctrl:'DDO_FUNCOESAPFATRIBUTOS_DESCRICAO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD.ONOPTIONCLICKED","{handler:'E179N2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV19FuncaoAPFAtributos_AtributosNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV24FuncaoAPFAtributos_AtributosNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV29FuncaoAPFAtributos_AtributosNom3',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFFuncaoAPF_Codigo',fld:'vTFFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFFuncaoAPF_Codigo_To',fld:'vTFFUNCAOAPF_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV40TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV43TFFuncaoAPFAtributos_AtributosCod',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFFuncaoAPFAtributos_AtributosCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFFuncaoAPFAtributos_AtributosNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',pic:'@!',nv:''},{av:'AV48TFFuncaoAPFAtributos_AtributosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL',pic:'@!',nv:''},{av:'AV51TFFuncaoAPFAtributos_FuncaoDadosCod',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFFuncaoAPFAtributos_AtrTabelaCod',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD',pic:'ZZZZZ9',nv:0},{av:'AV56TFFuncaoAPFAtributos_AtrTabelaCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV59TFFuncaoAPFAtributos_AtrTabelaNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM',pic:'@!',nv:''},{av:'AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL',pic:'@!',nv:''},{av:'AV63TFFuncoesAPFAtributos_Regra_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_REGRA_SEL',pic:'9',nv:0},{av:'AV66TFFuncoesAPFAtributos_Code',fld:'vTFFUNCOESAPFATRIBUTOS_CODE',pic:'',nv:''},{av:'AV67TFFuncoesAPFAtributos_Code_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_CODE_SEL',pic:'',nv:''},{av:'AV70TFFuncoesAPFAtributos_Nome',fld:'vTFFUNCOESAPFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV71TFFuncoesAPFAtributos_Nome_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_NOME_SEL',pic:'@!',nv:''},{av:'AV74TFFuncoesAPFAtributos_Descricao',fld:'vTFFUNCOESAPFATRIBUTOS_DESCRICAO',pic:'@!',nv:''},{av:'AV75TFFuncoesAPFAtributos_Descricao_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV37ddo_FuncaoAPF_CodigoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoAPFAtributos_AtributosCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoAPFAtributos_FuncaoDadosCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoAPFAtributos_AtrTabelaCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_FuncoesAPFAtributos_RegraTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_REGRATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_FuncoesAPFAtributos_CodeTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_CODETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_FuncoesAPFAtributos_NomeTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncoesAPFAtributos_DescricaoTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_funcaoapfatributos_atrtabelacod_Activeeventkey',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD',prop:'ActiveEventKey'},{av:'Ddo_funcaoapfatributos_atrtabelacod_Filteredtext_get',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD',prop:'FilteredText_get'},{av:'Ddo_funcaoapfatributos_atrtabelacod_Filteredtextto_get',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcaoapfatributos_atrtabelacod_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD',prop:'SortedStatus'},{av:'AV55TFFuncaoAPFAtributos_AtrTabelaCod',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD',pic:'ZZZZZ9',nv:0},{av:'AV56TFFuncaoAPFAtributos_AtrTabelaCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_funcaoapf_codigo_Sortedstatus',ctrl:'DDO_FUNCAOAPF_CODIGO',prop:'SortedStatus'},{av:'Ddo_funcaoapf_nome_Sortedstatus',ctrl:'DDO_FUNCAOAPF_NOME',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_atributoscod_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_atributosnom_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_funcaodadoscod_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_atrtabelanom_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM',prop:'SortedStatus'},{av:'Ddo_funcoesapfatributos_regra_Sortedstatus',ctrl:'DDO_FUNCOESAPFATRIBUTOS_REGRA',prop:'SortedStatus'},{av:'Ddo_funcoesapfatributos_code_Sortedstatus',ctrl:'DDO_FUNCOESAPFATRIBUTOS_CODE',prop:'SortedStatus'},{av:'Ddo_funcoesapfatributos_nome_Sortedstatus',ctrl:'DDO_FUNCOESAPFATRIBUTOS_NOME',prop:'SortedStatus'},{av:'Ddo_funcoesapfatributos_descricao_Sortedstatus',ctrl:'DDO_FUNCOESAPFATRIBUTOS_DESCRICAO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM.ONOPTIONCLICKED","{handler:'E189N2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV19FuncaoAPFAtributos_AtributosNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV24FuncaoAPFAtributos_AtributosNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV29FuncaoAPFAtributos_AtributosNom3',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFFuncaoAPF_Codigo',fld:'vTFFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFFuncaoAPF_Codigo_To',fld:'vTFFUNCAOAPF_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV40TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV43TFFuncaoAPFAtributos_AtributosCod',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFFuncaoAPFAtributos_AtributosCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFFuncaoAPFAtributos_AtributosNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',pic:'@!',nv:''},{av:'AV48TFFuncaoAPFAtributos_AtributosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL',pic:'@!',nv:''},{av:'AV51TFFuncaoAPFAtributos_FuncaoDadosCod',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFFuncaoAPFAtributos_AtrTabelaCod',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD',pic:'ZZZZZ9',nv:0},{av:'AV56TFFuncaoAPFAtributos_AtrTabelaCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV59TFFuncaoAPFAtributos_AtrTabelaNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM',pic:'@!',nv:''},{av:'AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL',pic:'@!',nv:''},{av:'AV63TFFuncoesAPFAtributos_Regra_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_REGRA_SEL',pic:'9',nv:0},{av:'AV66TFFuncoesAPFAtributos_Code',fld:'vTFFUNCOESAPFATRIBUTOS_CODE',pic:'',nv:''},{av:'AV67TFFuncoesAPFAtributos_Code_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_CODE_SEL',pic:'',nv:''},{av:'AV70TFFuncoesAPFAtributos_Nome',fld:'vTFFUNCOESAPFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV71TFFuncoesAPFAtributos_Nome_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_NOME_SEL',pic:'@!',nv:''},{av:'AV74TFFuncoesAPFAtributos_Descricao',fld:'vTFFUNCOESAPFATRIBUTOS_DESCRICAO',pic:'@!',nv:''},{av:'AV75TFFuncoesAPFAtributos_Descricao_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV37ddo_FuncaoAPF_CodigoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoAPFAtributos_AtributosCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoAPFAtributos_FuncaoDadosCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoAPFAtributos_AtrTabelaCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_FuncoesAPFAtributos_RegraTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_REGRATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_FuncoesAPFAtributos_CodeTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_CODETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_FuncoesAPFAtributos_NomeTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncoesAPFAtributos_DescricaoTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_funcaoapfatributos_atrtabelanom_Activeeventkey',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM',prop:'ActiveEventKey'},{av:'Ddo_funcaoapfatributos_atrtabelanom_Filteredtext_get',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM',prop:'FilteredText_get'},{av:'Ddo_funcaoapfatributos_atrtabelanom_Selectedvalue_get',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcaoapfatributos_atrtabelanom_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM',prop:'SortedStatus'},{av:'AV59TFFuncaoAPFAtributos_AtrTabelaNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM',pic:'@!',nv:''},{av:'AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL',pic:'@!',nv:''},{av:'Ddo_funcaoapf_codigo_Sortedstatus',ctrl:'DDO_FUNCAOAPF_CODIGO',prop:'SortedStatus'},{av:'Ddo_funcaoapf_nome_Sortedstatus',ctrl:'DDO_FUNCAOAPF_NOME',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_atributoscod_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_atributosnom_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_funcaodadoscod_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_atrtabelacod_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD',prop:'SortedStatus'},{av:'Ddo_funcoesapfatributos_regra_Sortedstatus',ctrl:'DDO_FUNCOESAPFATRIBUTOS_REGRA',prop:'SortedStatus'},{av:'Ddo_funcoesapfatributos_code_Sortedstatus',ctrl:'DDO_FUNCOESAPFATRIBUTOS_CODE',prop:'SortedStatus'},{av:'Ddo_funcoesapfatributos_nome_Sortedstatus',ctrl:'DDO_FUNCOESAPFATRIBUTOS_NOME',prop:'SortedStatus'},{av:'Ddo_funcoesapfatributos_descricao_Sortedstatus',ctrl:'DDO_FUNCOESAPFATRIBUTOS_DESCRICAO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_FUNCOESAPFATRIBUTOS_REGRA.ONOPTIONCLICKED","{handler:'E199N2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV19FuncaoAPFAtributos_AtributosNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV24FuncaoAPFAtributos_AtributosNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV29FuncaoAPFAtributos_AtributosNom3',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFFuncaoAPF_Codigo',fld:'vTFFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFFuncaoAPF_Codigo_To',fld:'vTFFUNCAOAPF_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV40TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV43TFFuncaoAPFAtributos_AtributosCod',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFFuncaoAPFAtributos_AtributosCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFFuncaoAPFAtributos_AtributosNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',pic:'@!',nv:''},{av:'AV48TFFuncaoAPFAtributos_AtributosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL',pic:'@!',nv:''},{av:'AV51TFFuncaoAPFAtributos_FuncaoDadosCod',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFFuncaoAPFAtributos_AtrTabelaCod',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD',pic:'ZZZZZ9',nv:0},{av:'AV56TFFuncaoAPFAtributos_AtrTabelaCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV59TFFuncaoAPFAtributos_AtrTabelaNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM',pic:'@!',nv:''},{av:'AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL',pic:'@!',nv:''},{av:'AV63TFFuncoesAPFAtributos_Regra_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_REGRA_SEL',pic:'9',nv:0},{av:'AV66TFFuncoesAPFAtributos_Code',fld:'vTFFUNCOESAPFATRIBUTOS_CODE',pic:'',nv:''},{av:'AV67TFFuncoesAPFAtributos_Code_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_CODE_SEL',pic:'',nv:''},{av:'AV70TFFuncoesAPFAtributos_Nome',fld:'vTFFUNCOESAPFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV71TFFuncoesAPFAtributos_Nome_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_NOME_SEL',pic:'@!',nv:''},{av:'AV74TFFuncoesAPFAtributos_Descricao',fld:'vTFFUNCOESAPFATRIBUTOS_DESCRICAO',pic:'@!',nv:''},{av:'AV75TFFuncoesAPFAtributos_Descricao_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV37ddo_FuncaoAPF_CodigoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoAPFAtributos_AtributosCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoAPFAtributos_FuncaoDadosCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoAPFAtributos_AtrTabelaCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_FuncoesAPFAtributos_RegraTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_REGRATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_FuncoesAPFAtributos_CodeTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_CODETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_FuncoesAPFAtributos_NomeTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncoesAPFAtributos_DescricaoTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_funcoesapfatributos_regra_Activeeventkey',ctrl:'DDO_FUNCOESAPFATRIBUTOS_REGRA',prop:'ActiveEventKey'},{av:'Ddo_funcoesapfatributos_regra_Selectedvalue_get',ctrl:'DDO_FUNCOESAPFATRIBUTOS_REGRA',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcoesapfatributos_regra_Sortedstatus',ctrl:'DDO_FUNCOESAPFATRIBUTOS_REGRA',prop:'SortedStatus'},{av:'AV63TFFuncoesAPFAtributos_Regra_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_REGRA_SEL',pic:'9',nv:0},{av:'Ddo_funcaoapf_codigo_Sortedstatus',ctrl:'DDO_FUNCAOAPF_CODIGO',prop:'SortedStatus'},{av:'Ddo_funcaoapf_nome_Sortedstatus',ctrl:'DDO_FUNCAOAPF_NOME',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_atributoscod_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_atributosnom_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_funcaodadoscod_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_atrtabelacod_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_atrtabelanom_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM',prop:'SortedStatus'},{av:'Ddo_funcoesapfatributos_code_Sortedstatus',ctrl:'DDO_FUNCOESAPFATRIBUTOS_CODE',prop:'SortedStatus'},{av:'Ddo_funcoesapfatributos_nome_Sortedstatus',ctrl:'DDO_FUNCOESAPFATRIBUTOS_NOME',prop:'SortedStatus'},{av:'Ddo_funcoesapfatributos_descricao_Sortedstatus',ctrl:'DDO_FUNCOESAPFATRIBUTOS_DESCRICAO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_FUNCOESAPFATRIBUTOS_CODE.ONOPTIONCLICKED","{handler:'E209N2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV19FuncaoAPFAtributos_AtributosNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV24FuncaoAPFAtributos_AtributosNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV29FuncaoAPFAtributos_AtributosNom3',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFFuncaoAPF_Codigo',fld:'vTFFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFFuncaoAPF_Codigo_To',fld:'vTFFUNCAOAPF_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV40TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV43TFFuncaoAPFAtributos_AtributosCod',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFFuncaoAPFAtributos_AtributosCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFFuncaoAPFAtributos_AtributosNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',pic:'@!',nv:''},{av:'AV48TFFuncaoAPFAtributos_AtributosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL',pic:'@!',nv:''},{av:'AV51TFFuncaoAPFAtributos_FuncaoDadosCod',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFFuncaoAPFAtributos_AtrTabelaCod',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD',pic:'ZZZZZ9',nv:0},{av:'AV56TFFuncaoAPFAtributos_AtrTabelaCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV59TFFuncaoAPFAtributos_AtrTabelaNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM',pic:'@!',nv:''},{av:'AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL',pic:'@!',nv:''},{av:'AV63TFFuncoesAPFAtributos_Regra_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_REGRA_SEL',pic:'9',nv:0},{av:'AV66TFFuncoesAPFAtributos_Code',fld:'vTFFUNCOESAPFATRIBUTOS_CODE',pic:'',nv:''},{av:'AV67TFFuncoesAPFAtributos_Code_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_CODE_SEL',pic:'',nv:''},{av:'AV70TFFuncoesAPFAtributos_Nome',fld:'vTFFUNCOESAPFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV71TFFuncoesAPFAtributos_Nome_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_NOME_SEL',pic:'@!',nv:''},{av:'AV74TFFuncoesAPFAtributos_Descricao',fld:'vTFFUNCOESAPFATRIBUTOS_DESCRICAO',pic:'@!',nv:''},{av:'AV75TFFuncoesAPFAtributos_Descricao_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV37ddo_FuncaoAPF_CodigoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoAPFAtributos_AtributosCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoAPFAtributos_FuncaoDadosCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoAPFAtributos_AtrTabelaCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_FuncoesAPFAtributos_RegraTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_REGRATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_FuncoesAPFAtributos_CodeTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_CODETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_FuncoesAPFAtributos_NomeTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncoesAPFAtributos_DescricaoTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_funcoesapfatributos_code_Activeeventkey',ctrl:'DDO_FUNCOESAPFATRIBUTOS_CODE',prop:'ActiveEventKey'},{av:'Ddo_funcoesapfatributos_code_Filteredtext_get',ctrl:'DDO_FUNCOESAPFATRIBUTOS_CODE',prop:'FilteredText_get'},{av:'Ddo_funcoesapfatributos_code_Selectedvalue_get',ctrl:'DDO_FUNCOESAPFATRIBUTOS_CODE',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcoesapfatributos_code_Sortedstatus',ctrl:'DDO_FUNCOESAPFATRIBUTOS_CODE',prop:'SortedStatus'},{av:'AV66TFFuncoesAPFAtributos_Code',fld:'vTFFUNCOESAPFATRIBUTOS_CODE',pic:'',nv:''},{av:'AV67TFFuncoesAPFAtributos_Code_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_CODE_SEL',pic:'',nv:''},{av:'Ddo_funcaoapf_codigo_Sortedstatus',ctrl:'DDO_FUNCAOAPF_CODIGO',prop:'SortedStatus'},{av:'Ddo_funcaoapf_nome_Sortedstatus',ctrl:'DDO_FUNCAOAPF_NOME',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_atributoscod_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_atributosnom_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_funcaodadoscod_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_atrtabelacod_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_atrtabelanom_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM',prop:'SortedStatus'},{av:'Ddo_funcoesapfatributos_regra_Sortedstatus',ctrl:'DDO_FUNCOESAPFATRIBUTOS_REGRA',prop:'SortedStatus'},{av:'Ddo_funcoesapfatributos_nome_Sortedstatus',ctrl:'DDO_FUNCOESAPFATRIBUTOS_NOME',prop:'SortedStatus'},{av:'Ddo_funcoesapfatributos_descricao_Sortedstatus',ctrl:'DDO_FUNCOESAPFATRIBUTOS_DESCRICAO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_FUNCOESAPFATRIBUTOS_NOME.ONOPTIONCLICKED","{handler:'E219N2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV19FuncaoAPFAtributos_AtributosNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV24FuncaoAPFAtributos_AtributosNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV29FuncaoAPFAtributos_AtributosNom3',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFFuncaoAPF_Codigo',fld:'vTFFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFFuncaoAPF_Codigo_To',fld:'vTFFUNCAOAPF_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV40TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV43TFFuncaoAPFAtributos_AtributosCod',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFFuncaoAPFAtributos_AtributosCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFFuncaoAPFAtributos_AtributosNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',pic:'@!',nv:''},{av:'AV48TFFuncaoAPFAtributos_AtributosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL',pic:'@!',nv:''},{av:'AV51TFFuncaoAPFAtributos_FuncaoDadosCod',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFFuncaoAPFAtributos_AtrTabelaCod',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD',pic:'ZZZZZ9',nv:0},{av:'AV56TFFuncaoAPFAtributos_AtrTabelaCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV59TFFuncaoAPFAtributos_AtrTabelaNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM',pic:'@!',nv:''},{av:'AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL',pic:'@!',nv:''},{av:'AV63TFFuncoesAPFAtributos_Regra_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_REGRA_SEL',pic:'9',nv:0},{av:'AV66TFFuncoesAPFAtributos_Code',fld:'vTFFUNCOESAPFATRIBUTOS_CODE',pic:'',nv:''},{av:'AV67TFFuncoesAPFAtributos_Code_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_CODE_SEL',pic:'',nv:''},{av:'AV70TFFuncoesAPFAtributos_Nome',fld:'vTFFUNCOESAPFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV71TFFuncoesAPFAtributos_Nome_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_NOME_SEL',pic:'@!',nv:''},{av:'AV74TFFuncoesAPFAtributos_Descricao',fld:'vTFFUNCOESAPFATRIBUTOS_DESCRICAO',pic:'@!',nv:''},{av:'AV75TFFuncoesAPFAtributos_Descricao_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV37ddo_FuncaoAPF_CodigoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoAPFAtributos_AtributosCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoAPFAtributos_FuncaoDadosCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoAPFAtributos_AtrTabelaCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_FuncoesAPFAtributos_RegraTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_REGRATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_FuncoesAPFAtributos_CodeTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_CODETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_FuncoesAPFAtributos_NomeTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncoesAPFAtributos_DescricaoTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_funcoesapfatributos_nome_Activeeventkey',ctrl:'DDO_FUNCOESAPFATRIBUTOS_NOME',prop:'ActiveEventKey'},{av:'Ddo_funcoesapfatributos_nome_Filteredtext_get',ctrl:'DDO_FUNCOESAPFATRIBUTOS_NOME',prop:'FilteredText_get'},{av:'Ddo_funcoesapfatributos_nome_Selectedvalue_get',ctrl:'DDO_FUNCOESAPFATRIBUTOS_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcoesapfatributos_nome_Sortedstatus',ctrl:'DDO_FUNCOESAPFATRIBUTOS_NOME',prop:'SortedStatus'},{av:'AV70TFFuncoesAPFAtributos_Nome',fld:'vTFFUNCOESAPFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV71TFFuncoesAPFAtributos_Nome_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_funcaoapf_codigo_Sortedstatus',ctrl:'DDO_FUNCAOAPF_CODIGO',prop:'SortedStatus'},{av:'Ddo_funcaoapf_nome_Sortedstatus',ctrl:'DDO_FUNCAOAPF_NOME',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_atributoscod_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_atributosnom_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_funcaodadoscod_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_atrtabelacod_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_atrtabelanom_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM',prop:'SortedStatus'},{av:'Ddo_funcoesapfatributos_regra_Sortedstatus',ctrl:'DDO_FUNCOESAPFATRIBUTOS_REGRA',prop:'SortedStatus'},{av:'Ddo_funcoesapfatributos_code_Sortedstatus',ctrl:'DDO_FUNCOESAPFATRIBUTOS_CODE',prop:'SortedStatus'},{av:'Ddo_funcoesapfatributos_descricao_Sortedstatus',ctrl:'DDO_FUNCOESAPFATRIBUTOS_DESCRICAO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_FUNCOESAPFATRIBUTOS_DESCRICAO.ONOPTIONCLICKED","{handler:'E229N2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV19FuncaoAPFAtributos_AtributosNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV24FuncaoAPFAtributos_AtributosNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV29FuncaoAPFAtributos_AtributosNom3',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFFuncaoAPF_Codigo',fld:'vTFFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFFuncaoAPF_Codigo_To',fld:'vTFFUNCAOAPF_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV40TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV43TFFuncaoAPFAtributos_AtributosCod',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFFuncaoAPFAtributos_AtributosCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFFuncaoAPFAtributos_AtributosNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',pic:'@!',nv:''},{av:'AV48TFFuncaoAPFAtributos_AtributosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL',pic:'@!',nv:''},{av:'AV51TFFuncaoAPFAtributos_FuncaoDadosCod',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFFuncaoAPFAtributos_AtrTabelaCod',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD',pic:'ZZZZZ9',nv:0},{av:'AV56TFFuncaoAPFAtributos_AtrTabelaCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV59TFFuncaoAPFAtributos_AtrTabelaNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM',pic:'@!',nv:''},{av:'AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL',pic:'@!',nv:''},{av:'AV63TFFuncoesAPFAtributos_Regra_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_REGRA_SEL',pic:'9',nv:0},{av:'AV66TFFuncoesAPFAtributos_Code',fld:'vTFFUNCOESAPFATRIBUTOS_CODE',pic:'',nv:''},{av:'AV67TFFuncoesAPFAtributos_Code_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_CODE_SEL',pic:'',nv:''},{av:'AV70TFFuncoesAPFAtributos_Nome',fld:'vTFFUNCOESAPFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV71TFFuncoesAPFAtributos_Nome_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_NOME_SEL',pic:'@!',nv:''},{av:'AV74TFFuncoesAPFAtributos_Descricao',fld:'vTFFUNCOESAPFATRIBUTOS_DESCRICAO',pic:'@!',nv:''},{av:'AV75TFFuncoesAPFAtributos_Descricao_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV37ddo_FuncaoAPF_CodigoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoAPFAtributos_AtributosCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoAPFAtributos_FuncaoDadosCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoAPFAtributos_AtrTabelaCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_FuncoesAPFAtributos_RegraTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_REGRATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_FuncoesAPFAtributos_CodeTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_CODETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_FuncoesAPFAtributos_NomeTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncoesAPFAtributos_DescricaoTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_funcoesapfatributos_descricao_Activeeventkey',ctrl:'DDO_FUNCOESAPFATRIBUTOS_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_funcoesapfatributos_descricao_Filteredtext_get',ctrl:'DDO_FUNCOESAPFATRIBUTOS_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_funcoesapfatributos_descricao_Selectedvalue_get',ctrl:'DDO_FUNCOESAPFATRIBUTOS_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcoesapfatributos_descricao_Sortedstatus',ctrl:'DDO_FUNCOESAPFATRIBUTOS_DESCRICAO',prop:'SortedStatus'},{av:'AV74TFFuncoesAPFAtributos_Descricao',fld:'vTFFUNCOESAPFATRIBUTOS_DESCRICAO',pic:'@!',nv:''},{av:'AV75TFFuncoesAPFAtributos_Descricao_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_DESCRICAO_SEL',pic:'@!',nv:''},{av:'Ddo_funcaoapf_codigo_Sortedstatus',ctrl:'DDO_FUNCAOAPF_CODIGO',prop:'SortedStatus'},{av:'Ddo_funcaoapf_nome_Sortedstatus',ctrl:'DDO_FUNCAOAPF_NOME',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_atributoscod_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_atributosnom_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_funcaodadoscod_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_atrtabelacod_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_atrtabelanom_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM',prop:'SortedStatus'},{av:'Ddo_funcoesapfatributos_regra_Sortedstatus',ctrl:'DDO_FUNCOESAPFATRIBUTOS_REGRA',prop:'SortedStatus'},{av:'Ddo_funcoesapfatributos_code_Sortedstatus',ctrl:'DDO_FUNCOESAPFATRIBUTOS_CODE',prop:'SortedStatus'},{av:'Ddo_funcoesapfatributos_nome_Sortedstatus',ctrl:'DDO_FUNCOESAPFATRIBUTOS_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E359N2',iparms:[],oparms:[{av:'AV32Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E369N2',iparms:[{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A364FuncaoAPFAtributos_AtributosCod',fld:'FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A166FuncaoAPF_Nome',fld:'FUNCAOAPF_NOME',pic:'',nv:''}],oparms:[{av:'AV7InOutFuncaoAPF_Codigo',fld:'vINOUTFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8InOutFuncaoAPFAtributos_AtributosCod',fld:'vINOUTFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV9InOutFuncaoAPF_Nome',fld:'vINOUTFUNCAOAPF_NOME',pic:'',nv:''}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E239N2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV19FuncaoAPFAtributos_AtributosNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV24FuncaoAPFAtributos_AtributosNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV29FuncaoAPFAtributos_AtributosNom3',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFFuncaoAPF_Codigo',fld:'vTFFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFFuncaoAPF_Codigo_To',fld:'vTFFUNCAOAPF_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV40TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV43TFFuncaoAPFAtributos_AtributosCod',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFFuncaoAPFAtributos_AtributosCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFFuncaoAPFAtributos_AtributosNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',pic:'@!',nv:''},{av:'AV48TFFuncaoAPFAtributos_AtributosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL',pic:'@!',nv:''},{av:'AV51TFFuncaoAPFAtributos_FuncaoDadosCod',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFFuncaoAPFAtributos_AtrTabelaCod',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD',pic:'ZZZZZ9',nv:0},{av:'AV56TFFuncaoAPFAtributos_AtrTabelaCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV59TFFuncaoAPFAtributos_AtrTabelaNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM',pic:'@!',nv:''},{av:'AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL',pic:'@!',nv:''},{av:'AV63TFFuncoesAPFAtributos_Regra_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_REGRA_SEL',pic:'9',nv:0},{av:'AV66TFFuncoesAPFAtributos_Code',fld:'vTFFUNCOESAPFATRIBUTOS_CODE',pic:'',nv:''},{av:'AV67TFFuncoesAPFAtributos_Code_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_CODE_SEL',pic:'',nv:''},{av:'AV70TFFuncoesAPFAtributos_Nome',fld:'vTFFUNCOESAPFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV71TFFuncoesAPFAtributos_Nome_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_NOME_SEL',pic:'@!',nv:''},{av:'AV74TFFuncoesAPFAtributos_Descricao',fld:'vTFFUNCOESAPFATRIBUTOS_DESCRICAO',pic:'@!',nv:''},{av:'AV75TFFuncoesAPFAtributos_Descricao_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV37ddo_FuncaoAPF_CodigoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoAPFAtributos_AtributosCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoAPFAtributos_FuncaoDadosCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoAPFAtributos_AtrTabelaCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_FuncoesAPFAtributos_RegraTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_REGRATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_FuncoesAPFAtributos_CodeTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_CODETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_FuncoesAPFAtributos_NomeTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncoesAPFAtributos_DescricaoTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E289N2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV19FuncaoAPFAtributos_AtributosNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV24FuncaoAPFAtributos_AtributosNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV29FuncaoAPFAtributos_AtributosNom3',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFFuncaoAPF_Codigo',fld:'vTFFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFFuncaoAPF_Codigo_To',fld:'vTFFUNCAOAPF_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV40TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV43TFFuncaoAPFAtributos_AtributosCod',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFFuncaoAPFAtributos_AtributosCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFFuncaoAPFAtributos_AtributosNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',pic:'@!',nv:''},{av:'AV48TFFuncaoAPFAtributos_AtributosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL',pic:'@!',nv:''},{av:'AV51TFFuncaoAPFAtributos_FuncaoDadosCod',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFFuncaoAPFAtributos_AtrTabelaCod',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD',pic:'ZZZZZ9',nv:0},{av:'AV56TFFuncaoAPFAtributos_AtrTabelaCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV59TFFuncaoAPFAtributos_AtrTabelaNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM',pic:'@!',nv:''},{av:'AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL',pic:'@!',nv:''},{av:'AV63TFFuncoesAPFAtributos_Regra_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_REGRA_SEL',pic:'9',nv:0},{av:'AV66TFFuncoesAPFAtributos_Code',fld:'vTFFUNCOESAPFATRIBUTOS_CODE',pic:'',nv:''},{av:'AV67TFFuncoesAPFAtributos_Code_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_CODE_SEL',pic:'',nv:''},{av:'AV70TFFuncoesAPFAtributos_Nome',fld:'vTFFUNCOESAPFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV71TFFuncoesAPFAtributos_Nome_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_NOME_SEL',pic:'@!',nv:''},{av:'AV74TFFuncoesAPFAtributos_Descricao',fld:'vTFFUNCOESAPFATRIBUTOS_DESCRICAO',pic:'@!',nv:''},{av:'AV75TFFuncoesAPFAtributos_Descricao_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV37ddo_FuncaoAPF_CodigoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoAPFAtributos_AtributosCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoAPFAtributos_FuncaoDadosCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoAPFAtributos_AtrTabelaCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_FuncoesAPFAtributos_RegraTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_REGRATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_FuncoesAPFAtributos_CodeTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_CODETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_FuncoesAPFAtributos_NomeTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncoesAPFAtributos_DescricaoTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E249N2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV19FuncaoAPFAtributos_AtributosNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV24FuncaoAPFAtributos_AtributosNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV29FuncaoAPFAtributos_AtributosNom3',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFFuncaoAPF_Codigo',fld:'vTFFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFFuncaoAPF_Codigo_To',fld:'vTFFUNCAOAPF_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV40TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV43TFFuncaoAPFAtributos_AtributosCod',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFFuncaoAPFAtributos_AtributosCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFFuncaoAPFAtributos_AtributosNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',pic:'@!',nv:''},{av:'AV48TFFuncaoAPFAtributos_AtributosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL',pic:'@!',nv:''},{av:'AV51TFFuncaoAPFAtributos_FuncaoDadosCod',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFFuncaoAPFAtributos_AtrTabelaCod',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD',pic:'ZZZZZ9',nv:0},{av:'AV56TFFuncaoAPFAtributos_AtrTabelaCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV59TFFuncaoAPFAtributos_AtrTabelaNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM',pic:'@!',nv:''},{av:'AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL',pic:'@!',nv:''},{av:'AV63TFFuncoesAPFAtributos_Regra_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_REGRA_SEL',pic:'9',nv:0},{av:'AV66TFFuncoesAPFAtributos_Code',fld:'vTFFUNCOESAPFATRIBUTOS_CODE',pic:'',nv:''},{av:'AV67TFFuncoesAPFAtributos_Code_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_CODE_SEL',pic:'',nv:''},{av:'AV70TFFuncoesAPFAtributos_Nome',fld:'vTFFUNCOESAPFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV71TFFuncoesAPFAtributos_Nome_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_NOME_SEL',pic:'@!',nv:''},{av:'AV74TFFuncoesAPFAtributos_Descricao',fld:'vTFFUNCOESAPFATRIBUTOS_DESCRICAO',pic:'@!',nv:''},{av:'AV75TFFuncoesAPFAtributos_Descricao_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV37ddo_FuncaoAPF_CodigoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoAPFAtributos_AtributosCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoAPFAtributos_FuncaoDadosCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoAPFAtributos_AtrTabelaCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_FuncoesAPFAtributos_RegraTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_REGRATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_FuncoesAPFAtributos_CodeTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_CODETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_FuncoesAPFAtributos_NomeTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncoesAPFAtributos_DescricaoTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV19FuncaoAPFAtributos_AtributosNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24FuncaoAPFAtributos_AtributosNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',pic:'@!',nv:''},{av:'AV29FuncaoAPFAtributos_AtributosNom3',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM3',pic:'@!',nv:''},{av:'edtavFuncaoapf_nome2_Visible',ctrl:'vFUNCAOAPF_NOME2',prop:'Visible'},{av:'edtavFuncaoapfatributos_atributosnom2_Visible',ctrl:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavFuncaoapf_nome3_Visible',ctrl:'vFUNCAOAPF_NOME3',prop:'Visible'},{av:'edtavFuncaoapfatributos_atributosnom3_Visible',ctrl:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavFuncaoapf_nome1_Visible',ctrl:'vFUNCAOAPF_NOME1',prop:'Visible'},{av:'edtavFuncaoapfatributos_atributosnom1_Visible',ctrl:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E299N2',iparms:[{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'edtavFuncaoapf_nome1_Visible',ctrl:'vFUNCAOAPF_NOME1',prop:'Visible'},{av:'edtavFuncaoapfatributos_atributosnom1_Visible',ctrl:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E309N2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV19FuncaoAPFAtributos_AtributosNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV24FuncaoAPFAtributos_AtributosNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV29FuncaoAPFAtributos_AtributosNom3',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFFuncaoAPF_Codigo',fld:'vTFFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFFuncaoAPF_Codigo_To',fld:'vTFFUNCAOAPF_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV40TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV43TFFuncaoAPFAtributos_AtributosCod',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFFuncaoAPFAtributos_AtributosCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFFuncaoAPFAtributos_AtributosNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',pic:'@!',nv:''},{av:'AV48TFFuncaoAPFAtributos_AtributosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL',pic:'@!',nv:''},{av:'AV51TFFuncaoAPFAtributos_FuncaoDadosCod',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFFuncaoAPFAtributos_AtrTabelaCod',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD',pic:'ZZZZZ9',nv:0},{av:'AV56TFFuncaoAPFAtributos_AtrTabelaCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV59TFFuncaoAPFAtributos_AtrTabelaNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM',pic:'@!',nv:''},{av:'AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL',pic:'@!',nv:''},{av:'AV63TFFuncoesAPFAtributos_Regra_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_REGRA_SEL',pic:'9',nv:0},{av:'AV66TFFuncoesAPFAtributos_Code',fld:'vTFFUNCOESAPFATRIBUTOS_CODE',pic:'',nv:''},{av:'AV67TFFuncoesAPFAtributos_Code_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_CODE_SEL',pic:'',nv:''},{av:'AV70TFFuncoesAPFAtributos_Nome',fld:'vTFFUNCOESAPFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV71TFFuncoesAPFAtributos_Nome_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_NOME_SEL',pic:'@!',nv:''},{av:'AV74TFFuncoesAPFAtributos_Descricao',fld:'vTFFUNCOESAPFATRIBUTOS_DESCRICAO',pic:'@!',nv:''},{av:'AV75TFFuncoesAPFAtributos_Descricao_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV37ddo_FuncaoAPF_CodigoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoAPFAtributos_AtributosCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoAPFAtributos_FuncaoDadosCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoAPFAtributos_AtrTabelaCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_FuncoesAPFAtributos_RegraTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_REGRATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_FuncoesAPFAtributos_CodeTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_CODETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_FuncoesAPFAtributos_NomeTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncoesAPFAtributos_DescricaoTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E259N2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV19FuncaoAPFAtributos_AtributosNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV24FuncaoAPFAtributos_AtributosNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV29FuncaoAPFAtributos_AtributosNom3',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFFuncaoAPF_Codigo',fld:'vTFFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFFuncaoAPF_Codigo_To',fld:'vTFFUNCAOAPF_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV40TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV43TFFuncaoAPFAtributos_AtributosCod',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFFuncaoAPFAtributos_AtributosCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFFuncaoAPFAtributos_AtributosNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',pic:'@!',nv:''},{av:'AV48TFFuncaoAPFAtributos_AtributosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL',pic:'@!',nv:''},{av:'AV51TFFuncaoAPFAtributos_FuncaoDadosCod',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFFuncaoAPFAtributos_AtrTabelaCod',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD',pic:'ZZZZZ9',nv:0},{av:'AV56TFFuncaoAPFAtributos_AtrTabelaCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV59TFFuncaoAPFAtributos_AtrTabelaNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM',pic:'@!',nv:''},{av:'AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL',pic:'@!',nv:''},{av:'AV63TFFuncoesAPFAtributos_Regra_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_REGRA_SEL',pic:'9',nv:0},{av:'AV66TFFuncoesAPFAtributos_Code',fld:'vTFFUNCOESAPFATRIBUTOS_CODE',pic:'',nv:''},{av:'AV67TFFuncoesAPFAtributos_Code_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_CODE_SEL',pic:'',nv:''},{av:'AV70TFFuncoesAPFAtributos_Nome',fld:'vTFFUNCOESAPFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV71TFFuncoesAPFAtributos_Nome_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_NOME_SEL',pic:'@!',nv:''},{av:'AV74TFFuncoesAPFAtributos_Descricao',fld:'vTFFUNCOESAPFATRIBUTOS_DESCRICAO',pic:'@!',nv:''},{av:'AV75TFFuncoesAPFAtributos_Descricao_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV37ddo_FuncaoAPF_CodigoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoAPFAtributos_AtributosCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoAPFAtributos_FuncaoDadosCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoAPFAtributos_AtrTabelaCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_FuncoesAPFAtributos_RegraTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_REGRATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_FuncoesAPFAtributos_CodeTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_CODETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_FuncoesAPFAtributos_NomeTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncoesAPFAtributos_DescricaoTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV19FuncaoAPFAtributos_AtributosNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24FuncaoAPFAtributos_AtributosNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',pic:'@!',nv:''},{av:'AV29FuncaoAPFAtributos_AtributosNom3',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM3',pic:'@!',nv:''},{av:'edtavFuncaoapf_nome2_Visible',ctrl:'vFUNCAOAPF_NOME2',prop:'Visible'},{av:'edtavFuncaoapfatributos_atributosnom2_Visible',ctrl:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavFuncaoapf_nome3_Visible',ctrl:'vFUNCAOAPF_NOME3',prop:'Visible'},{av:'edtavFuncaoapfatributos_atributosnom3_Visible',ctrl:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavFuncaoapf_nome1_Visible',ctrl:'vFUNCAOAPF_NOME1',prop:'Visible'},{av:'edtavFuncaoapfatributos_atributosnom1_Visible',ctrl:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E319N2',iparms:[{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'edtavFuncaoapf_nome2_Visible',ctrl:'vFUNCAOAPF_NOME2',prop:'Visible'},{av:'edtavFuncaoapfatributos_atributosnom2_Visible',ctrl:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E269N2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV19FuncaoAPFAtributos_AtributosNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV24FuncaoAPFAtributos_AtributosNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV29FuncaoAPFAtributos_AtributosNom3',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFFuncaoAPF_Codigo',fld:'vTFFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFFuncaoAPF_Codigo_To',fld:'vTFFUNCAOAPF_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV40TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV43TFFuncaoAPFAtributos_AtributosCod',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFFuncaoAPFAtributos_AtributosCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFFuncaoAPFAtributos_AtributosNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',pic:'@!',nv:''},{av:'AV48TFFuncaoAPFAtributos_AtributosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL',pic:'@!',nv:''},{av:'AV51TFFuncaoAPFAtributos_FuncaoDadosCod',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFFuncaoAPFAtributos_AtrTabelaCod',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD',pic:'ZZZZZ9',nv:0},{av:'AV56TFFuncaoAPFAtributos_AtrTabelaCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV59TFFuncaoAPFAtributos_AtrTabelaNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM',pic:'@!',nv:''},{av:'AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL',pic:'@!',nv:''},{av:'AV63TFFuncoesAPFAtributos_Regra_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_REGRA_SEL',pic:'9',nv:0},{av:'AV66TFFuncoesAPFAtributos_Code',fld:'vTFFUNCOESAPFATRIBUTOS_CODE',pic:'',nv:''},{av:'AV67TFFuncoesAPFAtributos_Code_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_CODE_SEL',pic:'',nv:''},{av:'AV70TFFuncoesAPFAtributos_Nome',fld:'vTFFUNCOESAPFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV71TFFuncoesAPFAtributos_Nome_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_NOME_SEL',pic:'@!',nv:''},{av:'AV74TFFuncoesAPFAtributos_Descricao',fld:'vTFFUNCOESAPFATRIBUTOS_DESCRICAO',pic:'@!',nv:''},{av:'AV75TFFuncoesAPFAtributos_Descricao_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV37ddo_FuncaoAPF_CodigoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoAPFAtributos_AtributosCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoAPFAtributos_FuncaoDadosCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoAPFAtributos_AtrTabelaCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_FuncoesAPFAtributos_RegraTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_REGRATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_FuncoesAPFAtributos_CodeTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_CODETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_FuncoesAPFAtributos_NomeTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncoesAPFAtributos_DescricaoTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV19FuncaoAPFAtributos_AtributosNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24FuncaoAPFAtributos_AtributosNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',pic:'@!',nv:''},{av:'AV29FuncaoAPFAtributos_AtributosNom3',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM3',pic:'@!',nv:''},{av:'edtavFuncaoapf_nome2_Visible',ctrl:'vFUNCAOAPF_NOME2',prop:'Visible'},{av:'edtavFuncaoapfatributos_atributosnom2_Visible',ctrl:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavFuncaoapf_nome3_Visible',ctrl:'vFUNCAOAPF_NOME3',prop:'Visible'},{av:'edtavFuncaoapfatributos_atributosnom3_Visible',ctrl:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavFuncaoapf_nome1_Visible',ctrl:'vFUNCAOAPF_NOME1',prop:'Visible'},{av:'edtavFuncaoapfatributos_atributosnom1_Visible',ctrl:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E329N2',iparms:[{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtavFuncaoapf_nome3_Visible',ctrl:'vFUNCAOAPF_NOME3',prop:'Visible'},{av:'edtavFuncaoapfatributos_atributosnom3_Visible',ctrl:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E279N2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV19FuncaoAPFAtributos_AtributosNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV24FuncaoAPFAtributos_AtributosNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV29FuncaoAPFAtributos_AtributosNom3',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFFuncaoAPF_Codigo',fld:'vTFFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFFuncaoAPF_Codigo_To',fld:'vTFFUNCAOAPF_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV40TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV43TFFuncaoAPFAtributos_AtributosCod',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFFuncaoAPFAtributos_AtributosCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFFuncaoAPFAtributos_AtributosNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',pic:'@!',nv:''},{av:'AV48TFFuncaoAPFAtributos_AtributosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL',pic:'@!',nv:''},{av:'AV51TFFuncaoAPFAtributos_FuncaoDadosCod',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV55TFFuncaoAPFAtributos_AtrTabelaCod',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD',pic:'ZZZZZ9',nv:0},{av:'AV56TFFuncaoAPFAtributos_AtrTabelaCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV59TFFuncaoAPFAtributos_AtrTabelaNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM',pic:'@!',nv:''},{av:'AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL',pic:'@!',nv:''},{av:'AV63TFFuncoesAPFAtributos_Regra_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_REGRA_SEL',pic:'9',nv:0},{av:'AV66TFFuncoesAPFAtributos_Code',fld:'vTFFUNCOESAPFATRIBUTOS_CODE',pic:'',nv:''},{av:'AV67TFFuncoesAPFAtributos_Code_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_CODE_SEL',pic:'',nv:''},{av:'AV70TFFuncoesAPFAtributos_Nome',fld:'vTFFUNCOESAPFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV71TFFuncoesAPFAtributos_Nome_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_NOME_SEL',pic:'@!',nv:''},{av:'AV74TFFuncoesAPFAtributos_Descricao',fld:'vTFFUNCOESAPFATRIBUTOS_DESCRICAO',pic:'@!',nv:''},{av:'AV75TFFuncoesAPFAtributos_Descricao_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV37ddo_FuncaoAPF_CodigoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoAPFAtributos_AtributosCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoAPFAtributos_FuncaoDadosCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoAPFAtributos_AtrTabelaCodTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64ddo_FuncoesAPFAtributos_RegraTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_REGRATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_FuncoesAPFAtributos_CodeTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_CODETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_FuncoesAPFAtributos_NomeTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncoesAPFAtributos_DescricaoTitleControlIdToReplace',fld:'vDDO_FUNCOESAPFATRIBUTOS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV35TFFuncaoAPF_Codigo',fld:'vTFFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_funcaoapf_codigo_Filteredtext_set',ctrl:'DDO_FUNCAOAPF_CODIGO',prop:'FilteredText_set'},{av:'AV36TFFuncaoAPF_Codigo_To',fld:'vTFFUNCAOAPF_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_funcaoapf_codigo_Filteredtextto_set',ctrl:'DDO_FUNCAOAPF_CODIGO',prop:'FilteredTextTo_set'},{av:'AV39TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'Ddo_funcaoapf_nome_Filteredtext_set',ctrl:'DDO_FUNCAOAPF_NOME',prop:'FilteredText_set'},{av:'AV40TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'Ddo_funcaoapf_nome_Selectedvalue_set',ctrl:'DDO_FUNCAOAPF_NOME',prop:'SelectedValue_set'},{av:'AV43TFFuncaoAPFAtributos_AtributosCod',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_funcaoapfatributos_atributoscod_Filteredtext_set',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',prop:'FilteredText_set'},{av:'AV44TFFuncaoAPFAtributos_AtributosCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_funcaoapfatributos_atributoscod_Filteredtextto_set',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',prop:'FilteredTextTo_set'},{av:'AV47TFFuncaoAPFAtributos_AtributosNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',pic:'@!',nv:''},{av:'Ddo_funcaoapfatributos_atributosnom_Filteredtext_set',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',prop:'FilteredText_set'},{av:'AV48TFFuncaoAPFAtributos_AtributosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL',pic:'@!',nv:''},{av:'Ddo_funcaoapfatributos_atributosnom_Selectedvalue_set',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',prop:'SelectedValue_set'},{av:'AV51TFFuncaoAPFAtributos_FuncaoDadosCod',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_funcaoapfatributos_funcaodadoscod_Filteredtext_set',ctrl:'DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',prop:'FilteredText_set'},{av:'AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_funcaoapfatributos_funcaodadoscod_Filteredtextto_set',ctrl:'DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',prop:'FilteredTextTo_set'},{av:'AV55TFFuncaoAPFAtributos_AtrTabelaCod',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_funcaoapfatributos_atrtabelacod_Filteredtext_set',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD',prop:'FilteredText_set'},{av:'AV56TFFuncaoAPFAtributos_AtrTabelaCod_To',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELACOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_funcaoapfatributos_atrtabelacod_Filteredtextto_set',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRTABELACOD',prop:'FilteredTextTo_set'},{av:'AV59TFFuncaoAPFAtributos_AtrTabelaNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM',pic:'@!',nv:''},{av:'Ddo_funcaoapfatributos_atrtabelanom_Filteredtext_set',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM',prop:'FilteredText_set'},{av:'AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL',pic:'@!',nv:''},{av:'Ddo_funcaoapfatributos_atrtabelanom_Selectedvalue_set',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM',prop:'SelectedValue_set'},{av:'AV63TFFuncoesAPFAtributos_Regra_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_REGRA_SEL',pic:'9',nv:0},{av:'Ddo_funcoesapfatributos_regra_Selectedvalue_set',ctrl:'DDO_FUNCOESAPFATRIBUTOS_REGRA',prop:'SelectedValue_set'},{av:'AV66TFFuncoesAPFAtributos_Code',fld:'vTFFUNCOESAPFATRIBUTOS_CODE',pic:'',nv:''},{av:'Ddo_funcoesapfatributos_code_Filteredtext_set',ctrl:'DDO_FUNCOESAPFATRIBUTOS_CODE',prop:'FilteredText_set'},{av:'AV67TFFuncoesAPFAtributos_Code_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_CODE_SEL',pic:'',nv:''},{av:'Ddo_funcoesapfatributos_code_Selectedvalue_set',ctrl:'DDO_FUNCOESAPFATRIBUTOS_CODE',prop:'SelectedValue_set'},{av:'AV70TFFuncoesAPFAtributos_Nome',fld:'vTFFUNCOESAPFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'Ddo_funcoesapfatributos_nome_Filteredtext_set',ctrl:'DDO_FUNCOESAPFATRIBUTOS_NOME',prop:'FilteredText_set'},{av:'AV71TFFuncoesAPFAtributos_Nome_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_funcoesapfatributos_nome_Selectedvalue_set',ctrl:'DDO_FUNCOESAPFATRIBUTOS_NOME',prop:'SelectedValue_set'},{av:'AV74TFFuncoesAPFAtributos_Descricao',fld:'vTFFUNCOESAPFATRIBUTOS_DESCRICAO',pic:'@!',nv:''},{av:'Ddo_funcoesapfatributos_descricao_Filteredtext_set',ctrl:'DDO_FUNCOESAPFATRIBUTOS_DESCRICAO',prop:'FilteredText_set'},{av:'AV75TFFuncoesAPFAtributos_Descricao_Sel',fld:'vTFFUNCOESAPFATRIBUTOS_DESCRICAO_SEL',pic:'@!',nv:''},{av:'Ddo_funcoesapfatributos_descricao_Selectedvalue_set',ctrl:'DDO_FUNCOESAPFATRIBUTOS_DESCRICAO',prop:'SelectedValue_set'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavFuncaoapf_nome1_Visible',ctrl:'vFUNCAOAPF_NOME1',prop:'Visible'},{av:'edtavFuncaoapfatributos_atributosnom1_Visible',ctrl:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV19FuncaoAPFAtributos_AtributosNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24FuncaoAPFAtributos_AtributosNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',pic:'@!',nv:''},{av:'AV29FuncaoAPFAtributos_AtributosNom3',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM3',pic:'@!',nv:''},{av:'edtavFuncaoapf_nome2_Visible',ctrl:'vFUNCAOAPF_NOME2',prop:'Visible'},{av:'edtavFuncaoapfatributos_atributosnom2_Visible',ctrl:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavFuncaoapf_nome3_Visible',ctrl:'vFUNCAOAPF_NOME3',prop:'Visible'},{av:'edtavFuncaoapfatributos_atributosnom3_Visible',ctrl:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV9InOutFuncaoAPF_Nome = "";
         Gridpaginationbar_Selectedpage = "";
         Ddo_funcaoapf_codigo_Activeeventkey = "";
         Ddo_funcaoapf_codigo_Filteredtext_get = "";
         Ddo_funcaoapf_codigo_Filteredtextto_get = "";
         Ddo_funcaoapf_nome_Activeeventkey = "";
         Ddo_funcaoapf_nome_Filteredtext_get = "";
         Ddo_funcaoapf_nome_Selectedvalue_get = "";
         Ddo_funcaoapfatributos_atributoscod_Activeeventkey = "";
         Ddo_funcaoapfatributos_atributoscod_Filteredtext_get = "";
         Ddo_funcaoapfatributos_atributoscod_Filteredtextto_get = "";
         Ddo_funcaoapfatributos_atributosnom_Activeeventkey = "";
         Ddo_funcaoapfatributos_atributosnom_Filteredtext_get = "";
         Ddo_funcaoapfatributos_atributosnom_Selectedvalue_get = "";
         Ddo_funcaoapfatributos_funcaodadoscod_Activeeventkey = "";
         Ddo_funcaoapfatributos_funcaodadoscod_Filteredtext_get = "";
         Ddo_funcaoapfatributos_funcaodadoscod_Filteredtextto_get = "";
         Ddo_funcaoapfatributos_atrtabelacod_Activeeventkey = "";
         Ddo_funcaoapfatributos_atrtabelacod_Filteredtext_get = "";
         Ddo_funcaoapfatributos_atrtabelacod_Filteredtextto_get = "";
         Ddo_funcaoapfatributos_atrtabelanom_Activeeventkey = "";
         Ddo_funcaoapfatributos_atrtabelanom_Filteredtext_get = "";
         Ddo_funcaoapfatributos_atrtabelanom_Selectedvalue_get = "";
         Ddo_funcoesapfatributos_regra_Activeeventkey = "";
         Ddo_funcoesapfatributos_regra_Selectedvalue_get = "";
         Ddo_funcoesapfatributos_code_Activeeventkey = "";
         Ddo_funcoesapfatributos_code_Filteredtext_get = "";
         Ddo_funcoesapfatributos_code_Selectedvalue_get = "";
         Ddo_funcoesapfatributos_nome_Activeeventkey = "";
         Ddo_funcoesapfatributos_nome_Filteredtext_get = "";
         Ddo_funcoesapfatributos_nome_Selectedvalue_get = "";
         Ddo_funcoesapfatributos_descricao_Activeeventkey = "";
         Ddo_funcoesapfatributos_descricao_Filteredtext_get = "";
         Ddo_funcoesapfatributos_descricao_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV16DynamicFiltersSelector1 = "";
         AV18FuncaoAPF_Nome1 = "";
         AV19FuncaoAPFAtributos_AtributosNom1 = "";
         AV21DynamicFiltersSelector2 = "";
         AV23FuncaoAPF_Nome2 = "";
         AV24FuncaoAPFAtributos_AtributosNom2 = "";
         AV26DynamicFiltersSelector3 = "";
         AV28FuncaoAPF_Nome3 = "";
         AV29FuncaoAPFAtributos_AtributosNom3 = "";
         AV39TFFuncaoAPF_Nome = "";
         AV40TFFuncaoAPF_Nome_Sel = "";
         AV47TFFuncaoAPFAtributos_AtributosNom = "";
         AV48TFFuncaoAPFAtributos_AtributosNom_Sel = "";
         AV59TFFuncaoAPFAtributos_AtrTabelaNom = "";
         AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel = "";
         AV66TFFuncoesAPFAtributos_Code = "";
         AV67TFFuncoesAPFAtributos_Code_Sel = "";
         AV70TFFuncoesAPFAtributos_Nome = "";
         AV71TFFuncoesAPFAtributos_Nome_Sel = "";
         AV74TFFuncoesAPFAtributos_Descricao = "";
         AV75TFFuncoesAPFAtributos_Descricao_Sel = "";
         AV37ddo_FuncaoAPF_CodigoTitleControlIdToReplace = "";
         AV41ddo_FuncaoAPF_NomeTitleControlIdToReplace = "";
         AV45ddo_FuncaoAPFAtributos_AtributosCodTitleControlIdToReplace = "";
         AV49ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace = "";
         AV53ddo_FuncaoAPFAtributos_FuncaoDadosCodTitleControlIdToReplace = "";
         AV57ddo_FuncaoAPFAtributos_AtrTabelaCodTitleControlIdToReplace = "";
         AV61ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace = "";
         AV64ddo_FuncoesAPFAtributos_RegraTitleControlIdToReplace = "";
         AV68ddo_FuncoesAPFAtributos_CodeTitleControlIdToReplace = "";
         AV72ddo_FuncoesAPFAtributos_NomeTitleControlIdToReplace = "";
         AV76ddo_FuncoesAPFAtributos_DescricaoTitleControlIdToReplace = "";
         AV84Pgmname = "";
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV77DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV34FuncaoAPF_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38FuncaoAPF_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV42FuncaoAPFAtributos_AtributosCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV46FuncaoAPFAtributos_AtributosNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV50FuncaoAPFAtributos_FuncaoDadosCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV54FuncaoAPFAtributos_AtrTabelaCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV58FuncaoAPFAtributos_AtrTabelaNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV62FuncoesAPFAtributos_RegraTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV65FuncoesAPFAtributos_CodeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV69FuncoesAPFAtributos_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV73FuncoesAPFAtributos_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_funcaoapf_codigo_Filteredtext_set = "";
         Ddo_funcaoapf_codigo_Filteredtextto_set = "";
         Ddo_funcaoapf_codigo_Sortedstatus = "";
         Ddo_funcaoapf_nome_Filteredtext_set = "";
         Ddo_funcaoapf_nome_Selectedvalue_set = "";
         Ddo_funcaoapf_nome_Sortedstatus = "";
         Ddo_funcaoapfatributos_atributoscod_Filteredtext_set = "";
         Ddo_funcaoapfatributos_atributoscod_Filteredtextto_set = "";
         Ddo_funcaoapfatributos_atributoscod_Sortedstatus = "";
         Ddo_funcaoapfatributos_atributosnom_Filteredtext_set = "";
         Ddo_funcaoapfatributos_atributosnom_Selectedvalue_set = "";
         Ddo_funcaoapfatributos_atributosnom_Sortedstatus = "";
         Ddo_funcaoapfatributos_funcaodadoscod_Filteredtext_set = "";
         Ddo_funcaoapfatributos_funcaodadoscod_Filteredtextto_set = "";
         Ddo_funcaoapfatributos_funcaodadoscod_Sortedstatus = "";
         Ddo_funcaoapfatributos_atrtabelacod_Filteredtext_set = "";
         Ddo_funcaoapfatributos_atrtabelacod_Filteredtextto_set = "";
         Ddo_funcaoapfatributos_atrtabelacod_Sortedstatus = "";
         Ddo_funcaoapfatributos_atrtabelanom_Filteredtext_set = "";
         Ddo_funcaoapfatributos_atrtabelanom_Selectedvalue_set = "";
         Ddo_funcaoapfatributos_atrtabelanom_Sortedstatus = "";
         Ddo_funcoesapfatributos_regra_Selectedvalue_set = "";
         Ddo_funcoesapfatributos_regra_Sortedstatus = "";
         Ddo_funcoesapfatributos_code_Filteredtext_set = "";
         Ddo_funcoesapfatributos_code_Selectedvalue_set = "";
         Ddo_funcoesapfatributos_code_Sortedstatus = "";
         Ddo_funcoesapfatributos_nome_Filteredtext_set = "";
         Ddo_funcoesapfatributos_nome_Selectedvalue_set = "";
         Ddo_funcoesapfatributos_nome_Sortedstatus = "";
         Ddo_funcoesapfatributos_descricao_Filteredtext_set = "";
         Ddo_funcoesapfatributos_descricao_Selectedvalue_set = "";
         Ddo_funcoesapfatributos_descricao_Sortedstatus = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV32Select = "";
         AV83Select_GXI = "";
         A166FuncaoAPF_Nome = "";
         A365FuncaoAPFAtributos_AtributosNom = "";
         A367FuncaoAPFAtributos_AtrTabelaNom = "";
         A383FuncoesAPFAtributos_Code = "";
         A384FuncoesAPFAtributos_Nome = "";
         A385FuncoesAPFAtributos_Descricao = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV18FuncaoAPF_Nome1 = "";
         lV19FuncaoAPFAtributos_AtributosNom1 = "";
         lV23FuncaoAPF_Nome2 = "";
         lV24FuncaoAPFAtributos_AtributosNom2 = "";
         lV28FuncaoAPF_Nome3 = "";
         lV29FuncaoAPFAtributos_AtributosNom3 = "";
         lV39TFFuncaoAPF_Nome = "";
         lV47TFFuncaoAPFAtributos_AtributosNom = "";
         lV59TFFuncaoAPFAtributos_AtrTabelaNom = "";
         lV66TFFuncoesAPFAtributos_Code = "";
         lV70TFFuncoesAPFAtributos_Nome = "";
         lV74TFFuncoesAPFAtributos_Descricao = "";
         H009N2_A385FuncoesAPFAtributos_Descricao = new String[] {""} ;
         H009N2_n385FuncoesAPFAtributos_Descricao = new bool[] {false} ;
         H009N2_A384FuncoesAPFAtributos_Nome = new String[] {""} ;
         H009N2_n384FuncoesAPFAtributos_Nome = new bool[] {false} ;
         H009N2_A383FuncoesAPFAtributos_Code = new String[] {""} ;
         H009N2_n383FuncoesAPFAtributos_Code = new bool[] {false} ;
         H009N2_A389FuncoesAPFAtributos_Regra = new bool[] {false} ;
         H009N2_n389FuncoesAPFAtributos_Regra = new bool[] {false} ;
         H009N2_A367FuncaoAPFAtributos_AtrTabelaNom = new String[] {""} ;
         H009N2_n367FuncaoAPFAtributos_AtrTabelaNom = new bool[] {false} ;
         H009N2_A366FuncaoAPFAtributos_AtrTabelaCod = new int[1] ;
         H009N2_n366FuncaoAPFAtributos_AtrTabelaCod = new bool[] {false} ;
         H009N2_A378FuncaoAPFAtributos_FuncaoDadosCod = new int[1] ;
         H009N2_n378FuncaoAPFAtributos_FuncaoDadosCod = new bool[] {false} ;
         H009N2_A365FuncaoAPFAtributos_AtributosNom = new String[] {""} ;
         H009N2_n365FuncaoAPFAtributos_AtributosNom = new bool[] {false} ;
         H009N2_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         H009N2_A166FuncaoAPF_Nome = new String[] {""} ;
         H009N2_A165FuncaoAPF_Codigo = new int[1] ;
         H009N3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptfuncoesapfatributos__default(),
            new Object[][] {
                new Object[] {
               H009N2_A385FuncoesAPFAtributos_Descricao, H009N2_n385FuncoesAPFAtributos_Descricao, H009N2_A384FuncoesAPFAtributos_Nome, H009N2_n384FuncoesAPFAtributos_Nome, H009N2_A383FuncoesAPFAtributos_Code, H009N2_n383FuncoesAPFAtributos_Code, H009N2_A389FuncoesAPFAtributos_Regra, H009N2_n389FuncoesAPFAtributos_Regra, H009N2_A367FuncaoAPFAtributos_AtrTabelaNom, H009N2_n367FuncaoAPFAtributos_AtrTabelaNom,
               H009N2_A366FuncaoAPFAtributos_AtrTabelaCod, H009N2_n366FuncaoAPFAtributos_AtrTabelaCod, H009N2_A378FuncaoAPFAtributos_FuncaoDadosCod, H009N2_n378FuncaoAPFAtributos_FuncaoDadosCod, H009N2_A365FuncaoAPFAtributos_AtributosNom, H009N2_n365FuncaoAPFAtributos_AtributosNom, H009N2_A364FuncaoAPFAtributos_AtributosCod, H009N2_A166FuncaoAPF_Nome, H009N2_A165FuncaoAPF_Codigo
               }
               , new Object[] {
               H009N3_AGRID_nRecordCount
               }
            }
         );
         AV84Pgmname = "PromptFuncoesAPFAtributos";
         /* GeneXus formulas. */
         AV84Pgmname = "PromptFuncoesAPFAtributos";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_83 ;
      private short nGXsfl_83_idx=1 ;
      private short AV14OrderedBy ;
      private short AV17DynamicFiltersOperator1 ;
      private short AV22DynamicFiltersOperator2 ;
      private short AV27DynamicFiltersOperator3 ;
      private short AV63TFFuncoesAPFAtributos_Regra_Sel ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_83_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtFuncaoAPF_Codigo_Titleformat ;
      private short edtFuncaoAPF_Nome_Titleformat ;
      private short edtFuncaoAPFAtributos_AtributosCod_Titleformat ;
      private short edtFuncaoAPFAtributos_AtributosNom_Titleformat ;
      private short edtFuncaoAPFAtributos_FuncaoDadosCod_Titleformat ;
      private short edtFuncaoAPFAtributos_AtrTabelaCod_Titleformat ;
      private short edtFuncaoAPFAtributos_AtrTabelaNom_Titleformat ;
      private short chkFuncoesAPFAtributos_Regra_Titleformat ;
      private short edtFuncoesAPFAtributos_Code_Titleformat ;
      private short edtFuncoesAPFAtributos_Nome_Titleformat ;
      private short edtFuncoesAPFAtributos_Descricao_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutFuncaoAPF_Codigo ;
      private int AV8InOutFuncaoAPFAtributos_AtributosCod ;
      private int wcpOAV7InOutFuncaoAPF_Codigo ;
      private int wcpOAV8InOutFuncaoAPFAtributos_AtributosCod ;
      private int subGrid_Rows ;
      private int AV35TFFuncaoAPF_Codigo ;
      private int AV36TFFuncaoAPF_Codigo_To ;
      private int AV43TFFuncaoAPFAtributos_AtributosCod ;
      private int AV44TFFuncaoAPFAtributos_AtributosCod_To ;
      private int AV51TFFuncaoAPFAtributos_FuncaoDadosCod ;
      private int AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To ;
      private int AV55TFFuncaoAPFAtributos_AtrTabelaCod ;
      private int AV56TFFuncaoAPFAtributos_AtrTabelaCod_To ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_funcaoapf_nome_Datalistupdateminimumcharacters ;
      private int Ddo_funcaoapfatributos_atributosnom_Datalistupdateminimumcharacters ;
      private int Ddo_funcaoapfatributos_atrtabelanom_Datalistupdateminimumcharacters ;
      private int Ddo_funcoesapfatributos_code_Datalistupdateminimumcharacters ;
      private int Ddo_funcoesapfatributos_nome_Datalistupdateminimumcharacters ;
      private int Ddo_funcoesapfatributos_descricao_Datalistupdateminimumcharacters ;
      private int edtavTffuncaoapf_codigo_Visible ;
      private int edtavTffuncaoapf_codigo_to_Visible ;
      private int edtavTffuncaoapf_nome_Visible ;
      private int edtavTffuncaoapf_nome_sel_Visible ;
      private int edtavTffuncaoapfatributos_atributoscod_Visible ;
      private int edtavTffuncaoapfatributos_atributoscod_to_Visible ;
      private int edtavTffuncaoapfatributos_atributosnom_Visible ;
      private int edtavTffuncaoapfatributos_atributosnom_sel_Visible ;
      private int edtavTffuncaoapfatributos_funcaodadoscod_Visible ;
      private int edtavTffuncaoapfatributos_funcaodadoscod_to_Visible ;
      private int edtavTffuncaoapfatributos_atrtabelacod_Visible ;
      private int edtavTffuncaoapfatributos_atrtabelacod_to_Visible ;
      private int edtavTffuncaoapfatributos_atrtabelanom_Visible ;
      private int edtavTffuncaoapfatributos_atrtabelanom_sel_Visible ;
      private int edtavTffuncoesapfatributos_regra_sel_Visible ;
      private int edtavTffuncoesapfatributos_code_Visible ;
      private int edtavTffuncoesapfatributos_code_sel_Visible ;
      private int edtavTffuncoesapfatributos_nome_Visible ;
      private int edtavTffuncoesapfatributos_nome_sel_Visible ;
      private int edtavTffuncoesapfatributos_descricao_Visible ;
      private int edtavTffuncoesapfatributos_descricao_sel_Visible ;
      private int edtavDdo_funcaoapf_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaoapf_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaoapfatributos_atributoscodtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaoapfatributos_atributosnomtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaoapfatributos_funcaodadoscodtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaoapfatributos_atrtabelacodtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaoapfatributos_atrtabelanomtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcoesapfatributos_regratitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcoesapfatributos_codetitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcoesapfatributos_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcoesapfatributos_descricaotitlecontrolidtoreplace_Visible ;
      private int A165FuncaoAPF_Codigo ;
      private int A364FuncaoAPFAtributos_AtributosCod ;
      private int A378FuncaoAPFAtributos_FuncaoDadosCod ;
      private int A366FuncaoAPFAtributos_AtrTabelaCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV78PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavFuncaoapf_nome1_Visible ;
      private int edtavFuncaoapfatributos_atributosnom1_Visible ;
      private int edtavFuncaoapf_nome2_Visible ;
      private int edtavFuncaoapfatributos_atributosnom2_Visible ;
      private int edtavFuncaoapf_nome3_Visible ;
      private int edtavFuncaoapfatributos_atributosnom3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV79GridCurrentPage ;
      private long AV80GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_funcaoapf_codigo_Activeeventkey ;
      private String Ddo_funcaoapf_codigo_Filteredtext_get ;
      private String Ddo_funcaoapf_codigo_Filteredtextto_get ;
      private String Ddo_funcaoapf_nome_Activeeventkey ;
      private String Ddo_funcaoapf_nome_Filteredtext_get ;
      private String Ddo_funcaoapf_nome_Selectedvalue_get ;
      private String Ddo_funcaoapfatributos_atributoscod_Activeeventkey ;
      private String Ddo_funcaoapfatributos_atributoscod_Filteredtext_get ;
      private String Ddo_funcaoapfatributos_atributoscod_Filteredtextto_get ;
      private String Ddo_funcaoapfatributos_atributosnom_Activeeventkey ;
      private String Ddo_funcaoapfatributos_atributosnom_Filteredtext_get ;
      private String Ddo_funcaoapfatributos_atributosnom_Selectedvalue_get ;
      private String Ddo_funcaoapfatributos_funcaodadoscod_Activeeventkey ;
      private String Ddo_funcaoapfatributos_funcaodadoscod_Filteredtext_get ;
      private String Ddo_funcaoapfatributos_funcaodadoscod_Filteredtextto_get ;
      private String Ddo_funcaoapfatributos_atrtabelacod_Activeeventkey ;
      private String Ddo_funcaoapfatributos_atrtabelacod_Filteredtext_get ;
      private String Ddo_funcaoapfatributos_atrtabelacod_Filteredtextto_get ;
      private String Ddo_funcaoapfatributos_atrtabelanom_Activeeventkey ;
      private String Ddo_funcaoapfatributos_atrtabelanom_Filteredtext_get ;
      private String Ddo_funcaoapfatributos_atrtabelanom_Selectedvalue_get ;
      private String Ddo_funcoesapfatributos_regra_Activeeventkey ;
      private String Ddo_funcoesapfatributos_regra_Selectedvalue_get ;
      private String Ddo_funcoesapfatributos_code_Activeeventkey ;
      private String Ddo_funcoesapfatributos_code_Filteredtext_get ;
      private String Ddo_funcoesapfatributos_code_Selectedvalue_get ;
      private String Ddo_funcoesapfatributos_nome_Activeeventkey ;
      private String Ddo_funcoesapfatributos_nome_Filteredtext_get ;
      private String Ddo_funcoesapfatributos_nome_Selectedvalue_get ;
      private String Ddo_funcoesapfatributos_descricao_Activeeventkey ;
      private String Ddo_funcoesapfatributos_descricao_Filteredtext_get ;
      private String Ddo_funcoesapfatributos_descricao_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_83_idx="0001" ;
      private String AV19FuncaoAPFAtributos_AtributosNom1 ;
      private String AV24FuncaoAPFAtributos_AtributosNom2 ;
      private String AV29FuncaoAPFAtributos_AtributosNom3 ;
      private String AV47TFFuncaoAPFAtributos_AtributosNom ;
      private String AV48TFFuncaoAPFAtributos_AtributosNom_Sel ;
      private String AV59TFFuncaoAPFAtributos_AtrTabelaNom ;
      private String AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel ;
      private String AV70TFFuncoesAPFAtributos_Nome ;
      private String AV71TFFuncoesAPFAtributos_Nome_Sel ;
      private String AV84Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_funcaoapf_codigo_Caption ;
      private String Ddo_funcaoapf_codigo_Tooltip ;
      private String Ddo_funcaoapf_codigo_Cls ;
      private String Ddo_funcaoapf_codigo_Filteredtext_set ;
      private String Ddo_funcaoapf_codigo_Filteredtextto_set ;
      private String Ddo_funcaoapf_codigo_Dropdownoptionstype ;
      private String Ddo_funcaoapf_codigo_Titlecontrolidtoreplace ;
      private String Ddo_funcaoapf_codigo_Sortedstatus ;
      private String Ddo_funcaoapf_codigo_Filtertype ;
      private String Ddo_funcaoapf_codigo_Sortasc ;
      private String Ddo_funcaoapf_codigo_Sortdsc ;
      private String Ddo_funcaoapf_codigo_Cleanfilter ;
      private String Ddo_funcaoapf_codigo_Rangefilterfrom ;
      private String Ddo_funcaoapf_codigo_Rangefilterto ;
      private String Ddo_funcaoapf_codigo_Searchbuttontext ;
      private String Ddo_funcaoapf_nome_Caption ;
      private String Ddo_funcaoapf_nome_Tooltip ;
      private String Ddo_funcaoapf_nome_Cls ;
      private String Ddo_funcaoapf_nome_Filteredtext_set ;
      private String Ddo_funcaoapf_nome_Selectedvalue_set ;
      private String Ddo_funcaoapf_nome_Dropdownoptionstype ;
      private String Ddo_funcaoapf_nome_Titlecontrolidtoreplace ;
      private String Ddo_funcaoapf_nome_Sortedstatus ;
      private String Ddo_funcaoapf_nome_Filtertype ;
      private String Ddo_funcaoapf_nome_Datalisttype ;
      private String Ddo_funcaoapf_nome_Datalistproc ;
      private String Ddo_funcaoapf_nome_Sortasc ;
      private String Ddo_funcaoapf_nome_Sortdsc ;
      private String Ddo_funcaoapf_nome_Loadingdata ;
      private String Ddo_funcaoapf_nome_Cleanfilter ;
      private String Ddo_funcaoapf_nome_Noresultsfound ;
      private String Ddo_funcaoapf_nome_Searchbuttontext ;
      private String Ddo_funcaoapfatributos_atributoscod_Caption ;
      private String Ddo_funcaoapfatributos_atributoscod_Tooltip ;
      private String Ddo_funcaoapfatributos_atributoscod_Cls ;
      private String Ddo_funcaoapfatributos_atributoscod_Filteredtext_set ;
      private String Ddo_funcaoapfatributos_atributoscod_Filteredtextto_set ;
      private String Ddo_funcaoapfatributos_atributoscod_Dropdownoptionstype ;
      private String Ddo_funcaoapfatributos_atributoscod_Titlecontrolidtoreplace ;
      private String Ddo_funcaoapfatributos_atributoscod_Sortedstatus ;
      private String Ddo_funcaoapfatributos_atributoscod_Filtertype ;
      private String Ddo_funcaoapfatributos_atributoscod_Sortasc ;
      private String Ddo_funcaoapfatributos_atributoscod_Sortdsc ;
      private String Ddo_funcaoapfatributos_atributoscod_Cleanfilter ;
      private String Ddo_funcaoapfatributos_atributoscod_Rangefilterfrom ;
      private String Ddo_funcaoapfatributos_atributoscod_Rangefilterto ;
      private String Ddo_funcaoapfatributos_atributoscod_Searchbuttontext ;
      private String Ddo_funcaoapfatributos_atributosnom_Caption ;
      private String Ddo_funcaoapfatributos_atributosnom_Tooltip ;
      private String Ddo_funcaoapfatributos_atributosnom_Cls ;
      private String Ddo_funcaoapfatributos_atributosnom_Filteredtext_set ;
      private String Ddo_funcaoapfatributos_atributosnom_Selectedvalue_set ;
      private String Ddo_funcaoapfatributos_atributosnom_Dropdownoptionstype ;
      private String Ddo_funcaoapfatributos_atributosnom_Titlecontrolidtoreplace ;
      private String Ddo_funcaoapfatributos_atributosnom_Sortedstatus ;
      private String Ddo_funcaoapfatributos_atributosnom_Filtertype ;
      private String Ddo_funcaoapfatributos_atributosnom_Datalisttype ;
      private String Ddo_funcaoapfatributos_atributosnom_Datalistproc ;
      private String Ddo_funcaoapfatributos_atributosnom_Sortasc ;
      private String Ddo_funcaoapfatributos_atributosnom_Sortdsc ;
      private String Ddo_funcaoapfatributos_atributosnom_Loadingdata ;
      private String Ddo_funcaoapfatributos_atributosnom_Cleanfilter ;
      private String Ddo_funcaoapfatributos_atributosnom_Noresultsfound ;
      private String Ddo_funcaoapfatributos_atributosnom_Searchbuttontext ;
      private String Ddo_funcaoapfatributos_funcaodadoscod_Caption ;
      private String Ddo_funcaoapfatributos_funcaodadoscod_Tooltip ;
      private String Ddo_funcaoapfatributos_funcaodadoscod_Cls ;
      private String Ddo_funcaoapfatributos_funcaodadoscod_Filteredtext_set ;
      private String Ddo_funcaoapfatributos_funcaodadoscod_Filteredtextto_set ;
      private String Ddo_funcaoapfatributos_funcaodadoscod_Dropdownoptionstype ;
      private String Ddo_funcaoapfatributos_funcaodadoscod_Titlecontrolidtoreplace ;
      private String Ddo_funcaoapfatributos_funcaodadoscod_Sortedstatus ;
      private String Ddo_funcaoapfatributos_funcaodadoscod_Filtertype ;
      private String Ddo_funcaoapfatributos_funcaodadoscod_Sortasc ;
      private String Ddo_funcaoapfatributos_funcaodadoscod_Sortdsc ;
      private String Ddo_funcaoapfatributos_funcaodadoscod_Cleanfilter ;
      private String Ddo_funcaoapfatributos_funcaodadoscod_Rangefilterfrom ;
      private String Ddo_funcaoapfatributos_funcaodadoscod_Rangefilterto ;
      private String Ddo_funcaoapfatributos_funcaodadoscod_Searchbuttontext ;
      private String Ddo_funcaoapfatributos_atrtabelacod_Caption ;
      private String Ddo_funcaoapfatributos_atrtabelacod_Tooltip ;
      private String Ddo_funcaoapfatributos_atrtabelacod_Cls ;
      private String Ddo_funcaoapfatributos_atrtabelacod_Filteredtext_set ;
      private String Ddo_funcaoapfatributos_atrtabelacod_Filteredtextto_set ;
      private String Ddo_funcaoapfatributos_atrtabelacod_Dropdownoptionstype ;
      private String Ddo_funcaoapfatributos_atrtabelacod_Titlecontrolidtoreplace ;
      private String Ddo_funcaoapfatributos_atrtabelacod_Sortedstatus ;
      private String Ddo_funcaoapfatributos_atrtabelacod_Filtertype ;
      private String Ddo_funcaoapfatributos_atrtabelacod_Sortasc ;
      private String Ddo_funcaoapfatributos_atrtabelacod_Sortdsc ;
      private String Ddo_funcaoapfatributos_atrtabelacod_Cleanfilter ;
      private String Ddo_funcaoapfatributos_atrtabelacod_Rangefilterfrom ;
      private String Ddo_funcaoapfatributos_atrtabelacod_Rangefilterto ;
      private String Ddo_funcaoapfatributos_atrtabelacod_Searchbuttontext ;
      private String Ddo_funcaoapfatributos_atrtabelanom_Caption ;
      private String Ddo_funcaoapfatributos_atrtabelanom_Tooltip ;
      private String Ddo_funcaoapfatributos_atrtabelanom_Cls ;
      private String Ddo_funcaoapfatributos_atrtabelanom_Filteredtext_set ;
      private String Ddo_funcaoapfatributos_atrtabelanom_Selectedvalue_set ;
      private String Ddo_funcaoapfatributos_atrtabelanom_Dropdownoptionstype ;
      private String Ddo_funcaoapfatributos_atrtabelanom_Titlecontrolidtoreplace ;
      private String Ddo_funcaoapfatributos_atrtabelanom_Sortedstatus ;
      private String Ddo_funcaoapfatributos_atrtabelanom_Filtertype ;
      private String Ddo_funcaoapfatributos_atrtabelanom_Datalisttype ;
      private String Ddo_funcaoapfatributos_atrtabelanom_Datalistproc ;
      private String Ddo_funcaoapfatributos_atrtabelanom_Sortasc ;
      private String Ddo_funcaoapfatributos_atrtabelanom_Sortdsc ;
      private String Ddo_funcaoapfatributos_atrtabelanom_Loadingdata ;
      private String Ddo_funcaoapfatributos_atrtabelanom_Cleanfilter ;
      private String Ddo_funcaoapfatributos_atrtabelanom_Noresultsfound ;
      private String Ddo_funcaoapfatributos_atrtabelanom_Searchbuttontext ;
      private String Ddo_funcoesapfatributos_regra_Caption ;
      private String Ddo_funcoesapfatributos_regra_Tooltip ;
      private String Ddo_funcoesapfatributos_regra_Cls ;
      private String Ddo_funcoesapfatributos_regra_Selectedvalue_set ;
      private String Ddo_funcoesapfatributos_regra_Dropdownoptionstype ;
      private String Ddo_funcoesapfatributos_regra_Titlecontrolidtoreplace ;
      private String Ddo_funcoesapfatributos_regra_Sortedstatus ;
      private String Ddo_funcoesapfatributos_regra_Datalisttype ;
      private String Ddo_funcoesapfatributos_regra_Datalistfixedvalues ;
      private String Ddo_funcoesapfatributos_regra_Sortasc ;
      private String Ddo_funcoesapfatributos_regra_Sortdsc ;
      private String Ddo_funcoesapfatributos_regra_Cleanfilter ;
      private String Ddo_funcoesapfatributos_regra_Searchbuttontext ;
      private String Ddo_funcoesapfatributos_code_Caption ;
      private String Ddo_funcoesapfatributos_code_Tooltip ;
      private String Ddo_funcoesapfatributos_code_Cls ;
      private String Ddo_funcoesapfatributos_code_Filteredtext_set ;
      private String Ddo_funcoesapfatributos_code_Selectedvalue_set ;
      private String Ddo_funcoesapfatributos_code_Dropdownoptionstype ;
      private String Ddo_funcoesapfatributos_code_Titlecontrolidtoreplace ;
      private String Ddo_funcoesapfatributos_code_Sortedstatus ;
      private String Ddo_funcoesapfatributos_code_Filtertype ;
      private String Ddo_funcoesapfatributos_code_Datalisttype ;
      private String Ddo_funcoesapfatributos_code_Datalistproc ;
      private String Ddo_funcoesapfatributos_code_Sortasc ;
      private String Ddo_funcoesapfatributos_code_Sortdsc ;
      private String Ddo_funcoesapfatributos_code_Loadingdata ;
      private String Ddo_funcoesapfatributos_code_Cleanfilter ;
      private String Ddo_funcoesapfatributos_code_Noresultsfound ;
      private String Ddo_funcoesapfatributos_code_Searchbuttontext ;
      private String Ddo_funcoesapfatributos_nome_Caption ;
      private String Ddo_funcoesapfatributos_nome_Tooltip ;
      private String Ddo_funcoesapfatributos_nome_Cls ;
      private String Ddo_funcoesapfatributos_nome_Filteredtext_set ;
      private String Ddo_funcoesapfatributos_nome_Selectedvalue_set ;
      private String Ddo_funcoesapfatributos_nome_Dropdownoptionstype ;
      private String Ddo_funcoesapfatributos_nome_Titlecontrolidtoreplace ;
      private String Ddo_funcoesapfatributos_nome_Sortedstatus ;
      private String Ddo_funcoesapfatributos_nome_Filtertype ;
      private String Ddo_funcoesapfatributos_nome_Datalisttype ;
      private String Ddo_funcoesapfatributos_nome_Datalistproc ;
      private String Ddo_funcoesapfatributos_nome_Sortasc ;
      private String Ddo_funcoesapfatributos_nome_Sortdsc ;
      private String Ddo_funcoesapfatributos_nome_Loadingdata ;
      private String Ddo_funcoesapfatributos_nome_Cleanfilter ;
      private String Ddo_funcoesapfatributos_nome_Noresultsfound ;
      private String Ddo_funcoesapfatributos_nome_Searchbuttontext ;
      private String Ddo_funcoesapfatributos_descricao_Caption ;
      private String Ddo_funcoesapfatributos_descricao_Tooltip ;
      private String Ddo_funcoesapfatributos_descricao_Cls ;
      private String Ddo_funcoesapfatributos_descricao_Filteredtext_set ;
      private String Ddo_funcoesapfatributos_descricao_Selectedvalue_set ;
      private String Ddo_funcoesapfatributos_descricao_Dropdownoptionstype ;
      private String Ddo_funcoesapfatributos_descricao_Titlecontrolidtoreplace ;
      private String Ddo_funcoesapfatributos_descricao_Sortedstatus ;
      private String Ddo_funcoesapfatributos_descricao_Filtertype ;
      private String Ddo_funcoesapfatributos_descricao_Datalisttype ;
      private String Ddo_funcoesapfatributos_descricao_Datalistproc ;
      private String Ddo_funcoesapfatributos_descricao_Sortasc ;
      private String Ddo_funcoesapfatributos_descricao_Sortdsc ;
      private String Ddo_funcoesapfatributos_descricao_Loadingdata ;
      private String Ddo_funcoesapfatributos_descricao_Cleanfilter ;
      private String Ddo_funcoesapfatributos_descricao_Noresultsfound ;
      private String Ddo_funcoesapfatributos_descricao_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTffuncaoapf_codigo_Internalname ;
      private String edtavTffuncaoapf_codigo_Jsonclick ;
      private String edtavTffuncaoapf_codigo_to_Internalname ;
      private String edtavTffuncaoapf_codigo_to_Jsonclick ;
      private String edtavTffuncaoapf_nome_Internalname ;
      private String edtavTffuncaoapf_nome_sel_Internalname ;
      private String edtavTffuncaoapfatributos_atributoscod_Internalname ;
      private String edtavTffuncaoapfatributos_atributoscod_Jsonclick ;
      private String edtavTffuncaoapfatributos_atributoscod_to_Internalname ;
      private String edtavTffuncaoapfatributos_atributoscod_to_Jsonclick ;
      private String edtavTffuncaoapfatributos_atributosnom_Internalname ;
      private String edtavTffuncaoapfatributos_atributosnom_Jsonclick ;
      private String edtavTffuncaoapfatributos_atributosnom_sel_Internalname ;
      private String edtavTffuncaoapfatributos_atributosnom_sel_Jsonclick ;
      private String edtavTffuncaoapfatributos_funcaodadoscod_Internalname ;
      private String edtavTffuncaoapfatributos_funcaodadoscod_Jsonclick ;
      private String edtavTffuncaoapfatributos_funcaodadoscod_to_Internalname ;
      private String edtavTffuncaoapfatributos_funcaodadoscod_to_Jsonclick ;
      private String edtavTffuncaoapfatributos_atrtabelacod_Internalname ;
      private String edtavTffuncaoapfatributos_atrtabelacod_Jsonclick ;
      private String edtavTffuncaoapfatributos_atrtabelacod_to_Internalname ;
      private String edtavTffuncaoapfatributos_atrtabelacod_to_Jsonclick ;
      private String edtavTffuncaoapfatributos_atrtabelanom_Internalname ;
      private String edtavTffuncaoapfatributos_atrtabelanom_Jsonclick ;
      private String edtavTffuncaoapfatributos_atrtabelanom_sel_Internalname ;
      private String edtavTffuncaoapfatributos_atrtabelanom_sel_Jsonclick ;
      private String edtavTffuncoesapfatributos_regra_sel_Internalname ;
      private String edtavTffuncoesapfatributos_regra_sel_Jsonclick ;
      private String edtavTffuncoesapfatributos_code_Internalname ;
      private String edtavTffuncoesapfatributos_code_Jsonclick ;
      private String edtavTffuncoesapfatributos_code_sel_Internalname ;
      private String edtavTffuncoesapfatributos_code_sel_Jsonclick ;
      private String edtavTffuncoesapfatributos_nome_Internalname ;
      private String edtavTffuncoesapfatributos_nome_Jsonclick ;
      private String edtavTffuncoesapfatributos_nome_sel_Internalname ;
      private String edtavTffuncoesapfatributos_nome_sel_Jsonclick ;
      private String edtavTffuncoesapfatributos_descricao_Internalname ;
      private String edtavTffuncoesapfatributos_descricao_Jsonclick ;
      private String edtavTffuncoesapfatributos_descricao_sel_Internalname ;
      private String edtavTffuncoesapfatributos_descricao_sel_Jsonclick ;
      private String edtavDdo_funcaoapf_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaoapf_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaoapfatributos_atributoscodtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaoapfatributos_atributosnomtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaoapfatributos_funcaodadoscodtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaoapfatributos_atrtabelacodtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaoapfatributos_atrtabelanomtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcoesapfatributos_regratitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcoesapfatributos_codetitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcoesapfatributos_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcoesapfatributos_descricaotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtFuncaoAPF_Codigo_Internalname ;
      private String edtFuncaoAPF_Nome_Internalname ;
      private String edtFuncaoAPFAtributos_AtributosCod_Internalname ;
      private String A365FuncaoAPFAtributos_AtributosNom ;
      private String edtFuncaoAPFAtributos_AtributosNom_Internalname ;
      private String edtFuncaoAPFAtributos_FuncaoDadosCod_Internalname ;
      private String edtFuncaoAPFAtributos_AtrTabelaCod_Internalname ;
      private String A367FuncaoAPFAtributos_AtrTabelaNom ;
      private String edtFuncaoAPFAtributos_AtrTabelaNom_Internalname ;
      private String chkFuncoesAPFAtributos_Regra_Internalname ;
      private String edtFuncoesAPFAtributos_Code_Internalname ;
      private String A384FuncoesAPFAtributos_Nome ;
      private String edtFuncoesAPFAtributos_Nome_Internalname ;
      private String edtFuncoesAPFAtributos_Descricao_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV19FuncaoAPFAtributos_AtributosNom1 ;
      private String lV24FuncaoAPFAtributos_AtributosNom2 ;
      private String lV29FuncaoAPFAtributos_AtributosNom3 ;
      private String lV47TFFuncaoAPFAtributos_AtributosNom ;
      private String lV59TFFuncaoAPFAtributos_AtrTabelaNom ;
      private String lV70TFFuncoesAPFAtributos_Nome ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavFuncaoapf_nome1_Internalname ;
      private String edtavFuncaoapfatributos_atributosnom1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavFuncaoapf_nome2_Internalname ;
      private String edtavFuncaoapfatributos_atributosnom2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavFuncaoapf_nome3_Internalname ;
      private String edtavFuncaoapfatributos_atributosnom3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_funcaoapf_codigo_Internalname ;
      private String Ddo_funcaoapf_nome_Internalname ;
      private String Ddo_funcaoapfatributos_atributoscod_Internalname ;
      private String Ddo_funcaoapfatributos_atributosnom_Internalname ;
      private String Ddo_funcaoapfatributos_funcaodadoscod_Internalname ;
      private String Ddo_funcaoapfatributos_atrtabelacod_Internalname ;
      private String Ddo_funcaoapfatributos_atrtabelanom_Internalname ;
      private String Ddo_funcoesapfatributos_regra_Internalname ;
      private String Ddo_funcoesapfatributos_code_Internalname ;
      private String Ddo_funcoesapfatributos_nome_Internalname ;
      private String Ddo_funcoesapfatributos_descricao_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtFuncaoAPF_Codigo_Title ;
      private String edtFuncaoAPF_Nome_Title ;
      private String edtFuncaoAPFAtributos_AtributosCod_Title ;
      private String edtFuncaoAPFAtributos_AtributosNom_Title ;
      private String edtFuncaoAPFAtributos_FuncaoDadosCod_Title ;
      private String edtFuncaoAPFAtributos_AtrTabelaCod_Title ;
      private String edtFuncaoAPFAtributos_AtrTabelaNom_Title ;
      private String edtFuncoesAPFAtributos_Code_Title ;
      private String edtFuncoesAPFAtributos_Nome_Title ;
      private String edtFuncoesAPFAtributos_Descricao_Title ;
      private String edtavSelect_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavFuncaoapfatributos_atributosnom3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavFuncaoapfatributos_atributosnom2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavFuncaoapfatributos_atributosnom1_Jsonclick ;
      private String sGXsfl_83_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtFuncaoAPF_Codigo_Jsonclick ;
      private String edtFuncaoAPF_Nome_Jsonclick ;
      private String edtFuncaoAPFAtributos_AtributosCod_Jsonclick ;
      private String edtFuncaoAPFAtributos_AtributosNom_Jsonclick ;
      private String edtFuncaoAPFAtributos_FuncaoDadosCod_Jsonclick ;
      private String edtFuncaoAPFAtributos_AtrTabelaCod_Jsonclick ;
      private String edtFuncaoAPFAtributos_AtrTabelaNom_Jsonclick ;
      private String edtFuncoesAPFAtributos_Code_Jsonclick ;
      private String edtFuncoesAPFAtributos_Nome_Jsonclick ;
      private String edtFuncoesAPFAtributos_Descricao_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV15OrderedDsc ;
      private bool AV20DynamicFiltersEnabled2 ;
      private bool AV25DynamicFiltersEnabled3 ;
      private bool AV31DynamicFiltersIgnoreFirst ;
      private bool AV30DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_funcaoapf_codigo_Includesortasc ;
      private bool Ddo_funcaoapf_codigo_Includesortdsc ;
      private bool Ddo_funcaoapf_codigo_Includefilter ;
      private bool Ddo_funcaoapf_codigo_Filterisrange ;
      private bool Ddo_funcaoapf_codigo_Includedatalist ;
      private bool Ddo_funcaoapf_nome_Includesortasc ;
      private bool Ddo_funcaoapf_nome_Includesortdsc ;
      private bool Ddo_funcaoapf_nome_Includefilter ;
      private bool Ddo_funcaoapf_nome_Filterisrange ;
      private bool Ddo_funcaoapf_nome_Includedatalist ;
      private bool Ddo_funcaoapfatributos_atributoscod_Includesortasc ;
      private bool Ddo_funcaoapfatributos_atributoscod_Includesortdsc ;
      private bool Ddo_funcaoapfatributos_atributoscod_Includefilter ;
      private bool Ddo_funcaoapfatributos_atributoscod_Filterisrange ;
      private bool Ddo_funcaoapfatributos_atributoscod_Includedatalist ;
      private bool Ddo_funcaoapfatributos_atributosnom_Includesortasc ;
      private bool Ddo_funcaoapfatributos_atributosnom_Includesortdsc ;
      private bool Ddo_funcaoapfatributos_atributosnom_Includefilter ;
      private bool Ddo_funcaoapfatributos_atributosnom_Filterisrange ;
      private bool Ddo_funcaoapfatributos_atributosnom_Includedatalist ;
      private bool Ddo_funcaoapfatributos_funcaodadoscod_Includesortasc ;
      private bool Ddo_funcaoapfatributos_funcaodadoscod_Includesortdsc ;
      private bool Ddo_funcaoapfatributos_funcaodadoscod_Includefilter ;
      private bool Ddo_funcaoapfatributos_funcaodadoscod_Filterisrange ;
      private bool Ddo_funcaoapfatributos_funcaodadoscod_Includedatalist ;
      private bool Ddo_funcaoapfatributos_atrtabelacod_Includesortasc ;
      private bool Ddo_funcaoapfatributos_atrtabelacod_Includesortdsc ;
      private bool Ddo_funcaoapfatributos_atrtabelacod_Includefilter ;
      private bool Ddo_funcaoapfatributos_atrtabelacod_Filterisrange ;
      private bool Ddo_funcaoapfatributos_atrtabelacod_Includedatalist ;
      private bool Ddo_funcaoapfatributos_atrtabelanom_Includesortasc ;
      private bool Ddo_funcaoapfatributos_atrtabelanom_Includesortdsc ;
      private bool Ddo_funcaoapfatributos_atrtabelanom_Includefilter ;
      private bool Ddo_funcaoapfatributos_atrtabelanom_Filterisrange ;
      private bool Ddo_funcaoapfatributos_atrtabelanom_Includedatalist ;
      private bool Ddo_funcoesapfatributos_regra_Includesortasc ;
      private bool Ddo_funcoesapfatributos_regra_Includesortdsc ;
      private bool Ddo_funcoesapfatributos_regra_Includefilter ;
      private bool Ddo_funcoesapfatributos_regra_Includedatalist ;
      private bool Ddo_funcoesapfatributos_code_Includesortasc ;
      private bool Ddo_funcoesapfatributos_code_Includesortdsc ;
      private bool Ddo_funcoesapfatributos_code_Includefilter ;
      private bool Ddo_funcoesapfatributos_code_Filterisrange ;
      private bool Ddo_funcoesapfatributos_code_Includedatalist ;
      private bool Ddo_funcoesapfatributos_nome_Includesortasc ;
      private bool Ddo_funcoesapfatributos_nome_Includesortdsc ;
      private bool Ddo_funcoesapfatributos_nome_Includefilter ;
      private bool Ddo_funcoesapfatributos_nome_Filterisrange ;
      private bool Ddo_funcoesapfatributos_nome_Includedatalist ;
      private bool Ddo_funcoesapfatributos_descricao_Includesortasc ;
      private bool Ddo_funcoesapfatributos_descricao_Includesortdsc ;
      private bool Ddo_funcoesapfatributos_descricao_Includefilter ;
      private bool Ddo_funcoesapfatributos_descricao_Filterisrange ;
      private bool Ddo_funcoesapfatributos_descricao_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n365FuncaoAPFAtributos_AtributosNom ;
      private bool n378FuncaoAPFAtributos_FuncaoDadosCod ;
      private bool n366FuncaoAPFAtributos_AtrTabelaCod ;
      private bool n367FuncaoAPFAtributos_AtrTabelaNom ;
      private bool A389FuncoesAPFAtributos_Regra ;
      private bool n389FuncoesAPFAtributos_Regra ;
      private bool n383FuncoesAPFAtributos_Code ;
      private bool n384FuncoesAPFAtributos_Nome ;
      private bool n385FuncoesAPFAtributos_Descricao ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV32Select_IsBlob ;
      private String AV9InOutFuncaoAPF_Nome ;
      private String wcpOAV9InOutFuncaoAPF_Nome ;
      private String AV16DynamicFiltersSelector1 ;
      private String AV18FuncaoAPF_Nome1 ;
      private String AV21DynamicFiltersSelector2 ;
      private String AV23FuncaoAPF_Nome2 ;
      private String AV26DynamicFiltersSelector3 ;
      private String AV28FuncaoAPF_Nome3 ;
      private String AV39TFFuncaoAPF_Nome ;
      private String AV40TFFuncaoAPF_Nome_Sel ;
      private String AV66TFFuncoesAPFAtributos_Code ;
      private String AV67TFFuncoesAPFAtributos_Code_Sel ;
      private String AV74TFFuncoesAPFAtributos_Descricao ;
      private String AV75TFFuncoesAPFAtributos_Descricao_Sel ;
      private String AV37ddo_FuncaoAPF_CodigoTitleControlIdToReplace ;
      private String AV41ddo_FuncaoAPF_NomeTitleControlIdToReplace ;
      private String AV45ddo_FuncaoAPFAtributos_AtributosCodTitleControlIdToReplace ;
      private String AV49ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace ;
      private String AV53ddo_FuncaoAPFAtributos_FuncaoDadosCodTitleControlIdToReplace ;
      private String AV57ddo_FuncaoAPFAtributos_AtrTabelaCodTitleControlIdToReplace ;
      private String AV61ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace ;
      private String AV64ddo_FuncoesAPFAtributos_RegraTitleControlIdToReplace ;
      private String AV68ddo_FuncoesAPFAtributos_CodeTitleControlIdToReplace ;
      private String AV72ddo_FuncoesAPFAtributos_NomeTitleControlIdToReplace ;
      private String AV76ddo_FuncoesAPFAtributos_DescricaoTitleControlIdToReplace ;
      private String AV83Select_GXI ;
      private String A166FuncaoAPF_Nome ;
      private String A383FuncoesAPFAtributos_Code ;
      private String A385FuncoesAPFAtributos_Descricao ;
      private String lV18FuncaoAPF_Nome1 ;
      private String lV23FuncaoAPF_Nome2 ;
      private String lV28FuncaoAPF_Nome3 ;
      private String lV39TFFuncaoAPF_Nome ;
      private String lV66TFFuncoesAPFAtributos_Code ;
      private String lV74TFFuncoesAPFAtributos_Descricao ;
      private String AV32Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutFuncaoAPF_Codigo ;
      private int aP1_InOutFuncaoAPFAtributos_AtributosCod ;
      private String aP2_InOutFuncaoAPF_Nome ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCheckbox chkFuncoesAPFAtributos_Regra ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private String[] H009N2_A385FuncoesAPFAtributos_Descricao ;
      private bool[] H009N2_n385FuncoesAPFAtributos_Descricao ;
      private String[] H009N2_A384FuncoesAPFAtributos_Nome ;
      private bool[] H009N2_n384FuncoesAPFAtributos_Nome ;
      private String[] H009N2_A383FuncoesAPFAtributos_Code ;
      private bool[] H009N2_n383FuncoesAPFAtributos_Code ;
      private bool[] H009N2_A389FuncoesAPFAtributos_Regra ;
      private bool[] H009N2_n389FuncoesAPFAtributos_Regra ;
      private String[] H009N2_A367FuncaoAPFAtributos_AtrTabelaNom ;
      private bool[] H009N2_n367FuncaoAPFAtributos_AtrTabelaNom ;
      private int[] H009N2_A366FuncaoAPFAtributos_AtrTabelaCod ;
      private bool[] H009N2_n366FuncaoAPFAtributos_AtrTabelaCod ;
      private int[] H009N2_A378FuncaoAPFAtributos_FuncaoDadosCod ;
      private bool[] H009N2_n378FuncaoAPFAtributos_FuncaoDadosCod ;
      private String[] H009N2_A365FuncaoAPFAtributos_AtributosNom ;
      private bool[] H009N2_n365FuncaoAPFAtributos_AtributosNom ;
      private int[] H009N2_A364FuncaoAPFAtributos_AtributosCod ;
      private String[] H009N2_A166FuncaoAPF_Nome ;
      private int[] H009N2_A165FuncaoAPF_Codigo ;
      private long[] H009N3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV34FuncaoAPF_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV38FuncaoAPF_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV42FuncaoAPFAtributos_AtributosCodTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV46FuncaoAPFAtributos_AtributosNomTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV50FuncaoAPFAtributos_FuncaoDadosCodTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV54FuncaoAPFAtributos_AtrTabelaCodTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV58FuncaoAPFAtributos_AtrTabelaNomTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV62FuncoesAPFAtributos_RegraTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV65FuncoesAPFAtributos_CodeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV69FuncoesAPFAtributos_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV73FuncoesAPFAtributos_DescricaoTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV13GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV77DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class promptfuncoesapfatributos__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H009N2( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             short AV17DynamicFiltersOperator1 ,
                                             String AV18FuncaoAPF_Nome1 ,
                                             String AV19FuncaoAPFAtributos_AtributosNom1 ,
                                             bool AV20DynamicFiltersEnabled2 ,
                                             String AV21DynamicFiltersSelector2 ,
                                             short AV22DynamicFiltersOperator2 ,
                                             String AV23FuncaoAPF_Nome2 ,
                                             String AV24FuncaoAPFAtributos_AtributosNom2 ,
                                             bool AV25DynamicFiltersEnabled3 ,
                                             String AV26DynamicFiltersSelector3 ,
                                             short AV27DynamicFiltersOperator3 ,
                                             String AV28FuncaoAPF_Nome3 ,
                                             String AV29FuncaoAPFAtributos_AtributosNom3 ,
                                             int AV35TFFuncaoAPF_Codigo ,
                                             int AV36TFFuncaoAPF_Codigo_To ,
                                             String AV40TFFuncaoAPF_Nome_Sel ,
                                             String AV39TFFuncaoAPF_Nome ,
                                             int AV43TFFuncaoAPFAtributos_AtributosCod ,
                                             int AV44TFFuncaoAPFAtributos_AtributosCod_To ,
                                             String AV48TFFuncaoAPFAtributos_AtributosNom_Sel ,
                                             String AV47TFFuncaoAPFAtributos_AtributosNom ,
                                             int AV51TFFuncaoAPFAtributos_FuncaoDadosCod ,
                                             int AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To ,
                                             int AV55TFFuncaoAPFAtributos_AtrTabelaCod ,
                                             int AV56TFFuncaoAPFAtributos_AtrTabelaCod_To ,
                                             String AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel ,
                                             String AV59TFFuncaoAPFAtributos_AtrTabelaNom ,
                                             short AV63TFFuncoesAPFAtributos_Regra_Sel ,
                                             String AV67TFFuncoesAPFAtributos_Code_Sel ,
                                             String AV66TFFuncoesAPFAtributos_Code ,
                                             String AV71TFFuncoesAPFAtributos_Nome_Sel ,
                                             String AV70TFFuncoesAPFAtributos_Nome ,
                                             String AV75TFFuncoesAPFAtributos_Descricao_Sel ,
                                             String AV74TFFuncoesAPFAtributos_Descricao ,
                                             String A166FuncaoAPF_Nome ,
                                             String A365FuncaoAPFAtributos_AtributosNom ,
                                             int A165FuncaoAPF_Codigo ,
                                             int A364FuncaoAPFAtributos_AtributosCod ,
                                             int A378FuncaoAPFAtributos_FuncaoDadosCod ,
                                             int A366FuncaoAPFAtributos_AtrTabelaCod ,
                                             String A367FuncaoAPFAtributos_AtrTabelaNom ,
                                             bool A389FuncoesAPFAtributos_Regra ,
                                             String A383FuncoesAPFAtributos_Code ,
                                             String A384FuncoesAPFAtributos_Nome ,
                                             String A385FuncoesAPFAtributos_Descricao ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [37] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[FuncoesAPFAtributos_Descricao], T1.[FuncoesAPFAtributos_Nome], T1.[FuncoesAPFAtributos_Code], T1.[FuncoesAPFAtributos_Regra], T3.[Tabela_Nome] AS FuncaoAPFAtributos_AtrTabelaNom, T2.[Atributos_TabelaCod] AS FuncaoAPFAtributos_AtrTabelaCod, T1.[FuncaoAPFAtributos_FuncaoDadosCod], T2.[Atributos_Nome] AS FuncaoAPFAtributos_AtributosNom, T1.[FuncaoAPFAtributos_AtributosCod] AS FuncaoAPFAtributos_AtributosCod, T4.[FuncaoAPF_Nome], T1.[FuncaoAPF_Codigo]";
         sFromString = " FROM ((([FuncoesAPFAtributos] T1 WITH (NOLOCK) INNER JOIN [Atributos] T2 WITH (NOLOCK) ON T2.[Atributos_Codigo] = T1.[FuncaoAPFAtributos_AtributosCod]) LEFT JOIN [Tabela] T3 WITH (NOLOCK) ON T3.[Tabela_Codigo] = T2.[Atributos_TabelaCod]) INNER JOIN [FuncoesAPF] T4 WITH (NOLOCK) ON T4.[FuncaoAPF_Codigo] = T1.[FuncaoAPF_Codigo])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18FuncaoAPF_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like @lV18FuncaoAPF_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like @lV18FuncaoAPF_Nome1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18FuncaoAPF_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like '%' + @lV18FuncaoAPF_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like '%' + @lV18FuncaoAPF_Nome1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19FuncaoAPFAtributos_AtributosNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] like @lV19FuncaoAPFAtributos_AtributosNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] like @lV19FuncaoAPFAtributos_AtributosNom1)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19FuncaoAPFAtributos_AtributosNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] like '%' + @lV19FuncaoAPFAtributos_AtributosNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] like '%' + @lV19FuncaoAPFAtributos_AtributosNom1)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "FUNCAOAPF_NOME") == 0 ) && ( AV22DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23FuncaoAPF_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like @lV23FuncaoAPF_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like @lV23FuncaoAPF_Nome2)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "FUNCAOAPF_NOME") == 0 ) && ( AV22DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23FuncaoAPF_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like '%' + @lV23FuncaoAPF_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like '%' + @lV23FuncaoAPF_Nome2)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV22DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24FuncaoAPFAtributos_AtributosNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] like @lV24FuncaoAPFAtributos_AtributosNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] like @lV24FuncaoAPFAtributos_AtributosNom2)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV22DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24FuncaoAPFAtributos_AtributosNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] like '%' + @lV24FuncaoAPFAtributos_AtributosNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] like '%' + @lV24FuncaoAPFAtributos_AtributosNom2)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "FUNCAOAPF_NOME") == 0 ) && ( AV27DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28FuncaoAPF_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like @lV28FuncaoAPF_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like @lV28FuncaoAPF_Nome3)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "FUNCAOAPF_NOME") == 0 ) && ( AV27DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28FuncaoAPF_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like '%' + @lV28FuncaoAPF_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like '%' + @lV28FuncaoAPF_Nome3)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV27DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29FuncaoAPFAtributos_AtributosNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] like @lV29FuncaoAPFAtributos_AtributosNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] like @lV29FuncaoAPFAtributos_AtributosNom3)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV27DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29FuncaoAPFAtributos_AtributosNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] like '%' + @lV29FuncaoAPFAtributos_AtributosNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] like '%' + @lV29FuncaoAPFAtributos_AtributosNom3)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( ! (0==AV35TFFuncaoAPF_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Codigo] >= @AV35TFFuncaoAPF_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Codigo] >= @AV35TFFuncaoAPF_Codigo)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( ! (0==AV36TFFuncaoAPF_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Codigo] <= @AV36TFFuncaoAPF_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Codigo] <= @AV36TFFuncaoAPF_Codigo_To)";
            }
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV40TFFuncaoAPF_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFFuncaoAPF_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] like @lV39TFFuncaoAPF_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] like @lV39TFFuncaoAPF_Nome)";
            }
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFFuncaoAPF_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[FuncaoAPF_Nome] = @AV40TFFuncaoAPF_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[FuncaoAPF_Nome] = @AV40TFFuncaoAPF_Nome_Sel)";
            }
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( ! (0==AV43TFFuncaoAPFAtributos_AtributosCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFAtributos_AtributosCod] >= @AV43TFFuncaoAPFAtributos_AtributosCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFAtributos_AtributosCod] >= @AV43TFFuncaoAPFAtributos_AtributosCod)";
            }
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( ! (0==AV44TFFuncaoAPFAtributos_AtributosCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFAtributos_AtributosCod] <= @AV44TFFuncaoAPFAtributos_AtributosCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFAtributos_AtributosCod] <= @AV44TFFuncaoAPFAtributos_AtributosCod_To)";
            }
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV48TFFuncaoAPFAtributos_AtributosNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47TFFuncaoAPFAtributos_AtributosNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] like @lV47TFFuncaoAPFAtributos_AtributosNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] like @lV47TFFuncaoAPFAtributos_AtributosNom)";
            }
         }
         else
         {
            GXv_int2[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TFFuncaoAPFAtributos_AtributosNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_Nome] = @AV48TFFuncaoAPFAtributos_AtributosNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_Nome] = @AV48TFFuncaoAPFAtributos_AtributosNom_Sel)";
            }
         }
         else
         {
            GXv_int2[19] = 1;
         }
         if ( ! (0==AV51TFFuncaoAPFAtributos_FuncaoDadosCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFAtributos_FuncaoDadosCod] >= @AV51TFFuncaoAPFAtributos_FuncaoDadosCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFAtributos_FuncaoDadosCod] >= @AV51TFFuncaoAPFAtributos_FuncaoDadosCod)";
            }
         }
         else
         {
            GXv_int2[20] = 1;
         }
         if ( ! (0==AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFAtributos_FuncaoDadosCod] <= @AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFAtributos_FuncaoDadosCod] <= @AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To)";
            }
         }
         else
         {
            GXv_int2[21] = 1;
         }
         if ( ! (0==AV55TFFuncaoAPFAtributos_AtrTabelaCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_TabelaCod] >= @AV55TFFuncaoAPFAtributos_AtrTabelaCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_TabelaCod] >= @AV55TFFuncaoAPFAtributos_AtrTabelaCod)";
            }
         }
         else
         {
            GXv_int2[22] = 1;
         }
         if ( ! (0==AV56TFFuncaoAPFAtributos_AtrTabelaCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Atributos_TabelaCod] <= @AV56TFFuncaoAPFAtributos_AtrTabelaCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Atributos_TabelaCod] <= @AV56TFFuncaoAPFAtributos_AtrTabelaCod_To)";
            }
         }
         else
         {
            GXv_int2[23] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59TFFuncaoAPFAtributos_AtrTabelaNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Tabela_Nome] like @lV59TFFuncaoAPFAtributos_AtrTabelaNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Tabela_Nome] like @lV59TFFuncaoAPFAtributos_AtrTabelaNom)";
            }
         }
         else
         {
            GXv_int2[24] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Tabela_Nome] = @AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Tabela_Nome] = @AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel)";
            }
         }
         else
         {
            GXv_int2[25] = 1;
         }
         if ( AV63TFFuncoesAPFAtributos_Regra_Sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Regra] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Regra] = 1)";
            }
         }
         if ( AV63TFFuncoesAPFAtributos_Regra_Sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Regra] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Regra] = 0)";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV67TFFuncoesAPFAtributos_Code_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66TFFuncoesAPFAtributos_Code)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Code] like @lV66TFFuncoesAPFAtributos_Code)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Code] like @lV66TFFuncoesAPFAtributos_Code)";
            }
         }
         else
         {
            GXv_int2[26] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67TFFuncoesAPFAtributos_Code_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Code] = @AV67TFFuncoesAPFAtributos_Code_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Code] = @AV67TFFuncoesAPFAtributos_Code_Sel)";
            }
         }
         else
         {
            GXv_int2[27] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV71TFFuncoesAPFAtributos_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70TFFuncoesAPFAtributos_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Nome] like @lV70TFFuncoesAPFAtributos_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Nome] like @lV70TFFuncoesAPFAtributos_Nome)";
            }
         }
         else
         {
            GXv_int2[28] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71TFFuncoesAPFAtributos_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Nome] = @AV71TFFuncoesAPFAtributos_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Nome] = @AV71TFFuncoesAPFAtributos_Nome_Sel)";
            }
         }
         else
         {
            GXv_int2[29] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV75TFFuncoesAPFAtributos_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74TFFuncoesAPFAtributos_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Descricao] like @lV74TFFuncoesAPFAtributos_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Descricao] like @lV74TFFuncoesAPFAtributos_Descricao)";
            }
         }
         else
         {
            GXv_int2[30] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75TFFuncoesAPFAtributos_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Descricao] = @AV75TFFuncoesAPFAtributos_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Descricao] = @AV75TFFuncoesAPFAtributos_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int2[31] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T4.[FuncaoAPF_Nome]";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T4.[FuncaoAPF_Nome] DESC";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[FuncaoAPF_Codigo]";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[FuncaoAPF_Codigo] DESC";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[FuncaoAPFAtributos_AtributosCod]";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[FuncaoAPFAtributos_AtributosCod] DESC";
         }
         else if ( ( AV14OrderedBy == 4 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Atributos_Nome]";
         }
         else if ( ( AV14OrderedBy == 4 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Atributos_Nome] DESC";
         }
         else if ( ( AV14OrderedBy == 5 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[FuncaoAPFAtributos_FuncaoDadosCod]";
         }
         else if ( ( AV14OrderedBy == 5 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[FuncaoAPFAtributos_FuncaoDadosCod] DESC";
         }
         else if ( ( AV14OrderedBy == 6 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Atributos_TabelaCod]";
         }
         else if ( ( AV14OrderedBy == 6 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Atributos_TabelaCod] DESC";
         }
         else if ( ( AV14OrderedBy == 7 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Tabela_Nome]";
         }
         else if ( ( AV14OrderedBy == 7 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Tabela_Nome] DESC";
         }
         else if ( ( AV14OrderedBy == 8 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[FuncoesAPFAtributos_Regra]";
         }
         else if ( ( AV14OrderedBy == 8 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[FuncoesAPFAtributos_Regra] DESC";
         }
         else if ( ( AV14OrderedBy == 9 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[FuncoesAPFAtributos_Code]";
         }
         else if ( ( AV14OrderedBy == 9 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[FuncoesAPFAtributos_Code] DESC";
         }
         else if ( ( AV14OrderedBy == 10 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[FuncoesAPFAtributos_Nome]";
         }
         else if ( ( AV14OrderedBy == 10 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[FuncoesAPFAtributos_Nome] DESC";
         }
         else if ( ( AV14OrderedBy == 11 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[FuncoesAPFAtributos_Descricao]";
         }
         else if ( ( AV14OrderedBy == 11 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[FuncoesAPFAtributos_Descricao] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[FuncaoAPF_Codigo], T1.[FuncaoAPFAtributos_AtributosCod]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H009N3( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             short AV17DynamicFiltersOperator1 ,
                                             String AV18FuncaoAPF_Nome1 ,
                                             String AV19FuncaoAPFAtributos_AtributosNom1 ,
                                             bool AV20DynamicFiltersEnabled2 ,
                                             String AV21DynamicFiltersSelector2 ,
                                             short AV22DynamicFiltersOperator2 ,
                                             String AV23FuncaoAPF_Nome2 ,
                                             String AV24FuncaoAPFAtributos_AtributosNom2 ,
                                             bool AV25DynamicFiltersEnabled3 ,
                                             String AV26DynamicFiltersSelector3 ,
                                             short AV27DynamicFiltersOperator3 ,
                                             String AV28FuncaoAPF_Nome3 ,
                                             String AV29FuncaoAPFAtributos_AtributosNom3 ,
                                             int AV35TFFuncaoAPF_Codigo ,
                                             int AV36TFFuncaoAPF_Codigo_To ,
                                             String AV40TFFuncaoAPF_Nome_Sel ,
                                             String AV39TFFuncaoAPF_Nome ,
                                             int AV43TFFuncaoAPFAtributos_AtributosCod ,
                                             int AV44TFFuncaoAPFAtributos_AtributosCod_To ,
                                             String AV48TFFuncaoAPFAtributos_AtributosNom_Sel ,
                                             String AV47TFFuncaoAPFAtributos_AtributosNom ,
                                             int AV51TFFuncaoAPFAtributos_FuncaoDadosCod ,
                                             int AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To ,
                                             int AV55TFFuncaoAPFAtributos_AtrTabelaCod ,
                                             int AV56TFFuncaoAPFAtributos_AtrTabelaCod_To ,
                                             String AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel ,
                                             String AV59TFFuncaoAPFAtributos_AtrTabelaNom ,
                                             short AV63TFFuncoesAPFAtributos_Regra_Sel ,
                                             String AV67TFFuncoesAPFAtributos_Code_Sel ,
                                             String AV66TFFuncoesAPFAtributos_Code ,
                                             String AV71TFFuncoesAPFAtributos_Nome_Sel ,
                                             String AV70TFFuncoesAPFAtributos_Nome ,
                                             String AV75TFFuncoesAPFAtributos_Descricao_Sel ,
                                             String AV74TFFuncoesAPFAtributos_Descricao ,
                                             String A166FuncaoAPF_Nome ,
                                             String A365FuncaoAPFAtributos_AtributosNom ,
                                             int A165FuncaoAPF_Codigo ,
                                             int A364FuncaoAPFAtributos_AtributosCod ,
                                             int A378FuncaoAPFAtributos_FuncaoDadosCod ,
                                             int A366FuncaoAPFAtributos_AtrTabelaCod ,
                                             String A367FuncaoAPFAtributos_AtrTabelaNom ,
                                             bool A389FuncoesAPFAtributos_Regra ,
                                             String A383FuncoesAPFAtributos_Code ,
                                             String A384FuncoesAPFAtributos_Nome ,
                                             String A385FuncoesAPFAtributos_Descricao ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [32] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ((([FuncoesAPFAtributos] T1 WITH (NOLOCK) INNER JOIN [Atributos] T3 WITH (NOLOCK) ON T3.[Atributos_Codigo] = T1.[FuncaoAPFAtributos_AtributosCod]) LEFT JOIN [Tabela] T4 WITH (NOLOCK) ON T4.[Tabela_Codigo] = T3.[Atributos_TabelaCod]) INNER JOIN [FuncoesAPF] T2 WITH (NOLOCK) ON T2.[FuncaoAPF_Codigo] = T1.[FuncaoAPF_Codigo])";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18FuncaoAPF_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[FuncaoAPF_Nome] like @lV18FuncaoAPF_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[FuncaoAPF_Nome] like @lV18FuncaoAPF_Nome1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18FuncaoAPF_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[FuncaoAPF_Nome] like '%' + @lV18FuncaoAPF_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[FuncaoAPF_Nome] like '%' + @lV18FuncaoAPF_Nome1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19FuncaoAPFAtributos_AtributosNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Atributos_Nome] like @lV19FuncaoAPFAtributos_AtributosNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Atributos_Nome] like @lV19FuncaoAPFAtributos_AtributosNom1)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19FuncaoAPFAtributos_AtributosNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Atributos_Nome] like '%' + @lV19FuncaoAPFAtributos_AtributosNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Atributos_Nome] like '%' + @lV19FuncaoAPFAtributos_AtributosNom1)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "FUNCAOAPF_NOME") == 0 ) && ( AV22DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23FuncaoAPF_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[FuncaoAPF_Nome] like @lV23FuncaoAPF_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[FuncaoAPF_Nome] like @lV23FuncaoAPF_Nome2)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "FUNCAOAPF_NOME") == 0 ) && ( AV22DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23FuncaoAPF_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[FuncaoAPF_Nome] like '%' + @lV23FuncaoAPF_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[FuncaoAPF_Nome] like '%' + @lV23FuncaoAPF_Nome2)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV22DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24FuncaoAPFAtributos_AtributosNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Atributos_Nome] like @lV24FuncaoAPFAtributos_AtributosNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Atributos_Nome] like @lV24FuncaoAPFAtributos_AtributosNom2)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV22DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24FuncaoAPFAtributos_AtributosNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Atributos_Nome] like '%' + @lV24FuncaoAPFAtributos_AtributosNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Atributos_Nome] like '%' + @lV24FuncaoAPFAtributos_AtributosNom2)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "FUNCAOAPF_NOME") == 0 ) && ( AV27DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28FuncaoAPF_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[FuncaoAPF_Nome] like @lV28FuncaoAPF_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[FuncaoAPF_Nome] like @lV28FuncaoAPF_Nome3)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "FUNCAOAPF_NOME") == 0 ) && ( AV27DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28FuncaoAPF_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[FuncaoAPF_Nome] like '%' + @lV28FuncaoAPF_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[FuncaoAPF_Nome] like '%' + @lV28FuncaoAPF_Nome3)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV27DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29FuncaoAPFAtributos_AtributosNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Atributos_Nome] like @lV29FuncaoAPFAtributos_AtributosNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Atributos_Nome] like @lV29FuncaoAPFAtributos_AtributosNom3)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( AV27DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29FuncaoAPFAtributos_AtributosNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Atributos_Nome] like '%' + @lV29FuncaoAPFAtributos_AtributosNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Atributos_Nome] like '%' + @lV29FuncaoAPFAtributos_AtributosNom3)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( ! (0==AV35TFFuncaoAPF_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Codigo] >= @AV35TFFuncaoAPF_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Codigo] >= @AV35TFFuncaoAPF_Codigo)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( ! (0==AV36TFFuncaoAPF_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Codigo] <= @AV36TFFuncaoAPF_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Codigo] <= @AV36TFFuncaoAPF_Codigo_To)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV40TFFuncaoAPF_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFFuncaoAPF_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[FuncaoAPF_Nome] like @lV39TFFuncaoAPF_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[FuncaoAPF_Nome] like @lV39TFFuncaoAPF_Nome)";
            }
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFFuncaoAPF_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[FuncaoAPF_Nome] = @AV40TFFuncaoAPF_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[FuncaoAPF_Nome] = @AV40TFFuncaoAPF_Nome_Sel)";
            }
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( ! (0==AV43TFFuncaoAPFAtributos_AtributosCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFAtributos_AtributosCod] >= @AV43TFFuncaoAPFAtributos_AtributosCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFAtributos_AtributosCod] >= @AV43TFFuncaoAPFAtributos_AtributosCod)";
            }
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( ! (0==AV44TFFuncaoAPFAtributos_AtributosCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFAtributos_AtributosCod] <= @AV44TFFuncaoAPFAtributos_AtributosCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFAtributos_AtributosCod] <= @AV44TFFuncaoAPFAtributos_AtributosCod_To)";
            }
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV48TFFuncaoAPFAtributos_AtributosNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47TFFuncaoAPFAtributos_AtributosNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Atributos_Nome] like @lV47TFFuncaoAPFAtributos_AtributosNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Atributos_Nome] like @lV47TFFuncaoAPFAtributos_AtributosNom)";
            }
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TFFuncaoAPFAtributos_AtributosNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Atributos_Nome] = @AV48TFFuncaoAPFAtributos_AtributosNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Atributos_Nome] = @AV48TFFuncaoAPFAtributos_AtributosNom_Sel)";
            }
         }
         else
         {
            GXv_int4[19] = 1;
         }
         if ( ! (0==AV51TFFuncaoAPFAtributos_FuncaoDadosCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFAtributos_FuncaoDadosCod] >= @AV51TFFuncaoAPFAtributos_FuncaoDadosCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFAtributos_FuncaoDadosCod] >= @AV51TFFuncaoAPFAtributos_FuncaoDadosCod)";
            }
         }
         else
         {
            GXv_int4[20] = 1;
         }
         if ( ! (0==AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPFAtributos_FuncaoDadosCod] <= @AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPFAtributos_FuncaoDadosCod] <= @AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To)";
            }
         }
         else
         {
            GXv_int4[21] = 1;
         }
         if ( ! (0==AV55TFFuncaoAPFAtributos_AtrTabelaCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Atributos_TabelaCod] >= @AV55TFFuncaoAPFAtributos_AtrTabelaCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Atributos_TabelaCod] >= @AV55TFFuncaoAPFAtributos_AtrTabelaCod)";
            }
         }
         else
         {
            GXv_int4[22] = 1;
         }
         if ( ! (0==AV56TFFuncaoAPFAtributos_AtrTabelaCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Atributos_TabelaCod] <= @AV56TFFuncaoAPFAtributos_AtrTabelaCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Atributos_TabelaCod] <= @AV56TFFuncaoAPFAtributos_AtrTabelaCod_To)";
            }
         }
         else
         {
            GXv_int4[23] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59TFFuncaoAPFAtributos_AtrTabelaNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Tabela_Nome] like @lV59TFFuncaoAPFAtributos_AtrTabelaNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Tabela_Nome] like @lV59TFFuncaoAPFAtributos_AtrTabelaNom)";
            }
         }
         else
         {
            GXv_int4[24] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Tabela_Nome] = @AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Tabela_Nome] = @AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel)";
            }
         }
         else
         {
            GXv_int4[25] = 1;
         }
         if ( AV63TFFuncoesAPFAtributos_Regra_Sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Regra] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Regra] = 1)";
            }
         }
         if ( AV63TFFuncoesAPFAtributos_Regra_Sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Regra] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Regra] = 0)";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV67TFFuncoesAPFAtributos_Code_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66TFFuncoesAPFAtributos_Code)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Code] like @lV66TFFuncoesAPFAtributos_Code)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Code] like @lV66TFFuncoesAPFAtributos_Code)";
            }
         }
         else
         {
            GXv_int4[26] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67TFFuncoesAPFAtributos_Code_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Code] = @AV67TFFuncoesAPFAtributos_Code_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Code] = @AV67TFFuncoesAPFAtributos_Code_Sel)";
            }
         }
         else
         {
            GXv_int4[27] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV71TFFuncoesAPFAtributos_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70TFFuncoesAPFAtributos_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Nome] like @lV70TFFuncoesAPFAtributos_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Nome] like @lV70TFFuncoesAPFAtributos_Nome)";
            }
         }
         else
         {
            GXv_int4[28] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71TFFuncoesAPFAtributos_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Nome] = @AV71TFFuncoesAPFAtributos_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Nome] = @AV71TFFuncoesAPFAtributos_Nome_Sel)";
            }
         }
         else
         {
            GXv_int4[29] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV75TFFuncoesAPFAtributos_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74TFFuncoesAPFAtributos_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Descricao] like @lV74TFFuncoesAPFAtributos_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Descricao] like @lV74TFFuncoesAPFAtributos_Descricao)";
            }
         }
         else
         {
            GXv_int4[30] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75TFFuncoesAPFAtributos_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncoesAPFAtributos_Descricao] = @AV75TFFuncoesAPFAtributos_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncoesAPFAtributos_Descricao] = @AV75TFFuncoesAPFAtributos_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int4[31] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 4 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 4 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 5 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 5 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 6 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 6 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 7 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 7 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 8 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 8 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 9 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 9 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 10 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 10 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 11 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 11 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H009N2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] , (int)dynConstraints[23] , (int)dynConstraints[24] , (int)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (short)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (int)dynConstraints[37] , (int)dynConstraints[38] , (int)dynConstraints[39] , (int)dynConstraints[40] , (String)dynConstraints[41] , (bool)dynConstraints[42] , (String)dynConstraints[43] , (String)dynConstraints[44] , (String)dynConstraints[45] , (short)dynConstraints[46] , (bool)dynConstraints[47] );
               case 1 :
                     return conditional_H009N3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] , (int)dynConstraints[23] , (int)dynConstraints[24] , (int)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (short)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (int)dynConstraints[37] , (int)dynConstraints[38] , (int)dynConstraints[39] , (int)dynConstraints[40] , (String)dynConstraints[41] , (bool)dynConstraints[42] , (String)dynConstraints[43] , (String)dynConstraints[44] , (String)dynConstraints[45] , (short)dynConstraints[46] , (bool)dynConstraints[47] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH009N2 ;
          prmH009N2 = new Object[] {
          new Object[] {"@lV18FuncaoAPF_Nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV18FuncaoAPF_Nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV19FuncaoAPFAtributos_AtributosNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV19FuncaoAPFAtributos_AtributosNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV23FuncaoAPF_Nome2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV23FuncaoAPF_Nome2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV24FuncaoAPFAtributos_AtributosNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV24FuncaoAPFAtributos_AtributosNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV28FuncaoAPF_Nome3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV28FuncaoAPF_Nome3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV29FuncaoAPFAtributos_AtributosNom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV29FuncaoAPFAtributos_AtributosNom3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV35TFFuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV36TFFuncaoAPF_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV39TFFuncaoAPF_Nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV40TFFuncaoAPF_Nome_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV43TFFuncaoAPFAtributos_AtributosCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV44TFFuncaoAPFAtributos_AtributosCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV47TFFuncaoAPFAtributos_AtributosNom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV48TFFuncaoAPFAtributos_AtributosNom_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV51TFFuncaoAPFAtributos_FuncaoDadosCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV55TFFuncaoAPFAtributos_AtrTabelaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV56TFFuncaoAPFAtributos_AtrTabelaCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV59TFFuncaoAPFAtributos_AtrTabelaNom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV66TFFuncoesAPFAtributos_Code",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV67TFFuncoesAPFAtributos_Code_Sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV70TFFuncoesAPFAtributos_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV71TFFuncoesAPFAtributos_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV74TFFuncoesAPFAtributos_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV75TFFuncoesAPFAtributos_Descricao_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH009N3 ;
          prmH009N3 = new Object[] {
          new Object[] {"@lV18FuncaoAPF_Nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV18FuncaoAPF_Nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV19FuncaoAPFAtributos_AtributosNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV19FuncaoAPFAtributos_AtributosNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV23FuncaoAPF_Nome2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV23FuncaoAPF_Nome2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV24FuncaoAPFAtributos_AtributosNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV24FuncaoAPFAtributos_AtributosNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV28FuncaoAPF_Nome3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV28FuncaoAPF_Nome3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV29FuncaoAPFAtributos_AtributosNom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV29FuncaoAPFAtributos_AtributosNom3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV35TFFuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV36TFFuncaoAPF_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV39TFFuncaoAPF_Nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV40TFFuncaoAPF_Nome_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV43TFFuncaoAPFAtributos_AtributosCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV44TFFuncaoAPFAtributos_AtributosCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV47TFFuncaoAPFAtributos_AtributosNom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV48TFFuncaoAPFAtributos_AtributosNom_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV51TFFuncaoAPFAtributos_FuncaoDadosCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV52TFFuncaoAPFAtributos_FuncaoDadosCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV55TFFuncaoAPFAtributos_AtrTabelaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV56TFFuncaoAPFAtributos_AtrTabelaCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV59TFFuncaoAPFAtributos_AtrTabelaNom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV60TFFuncaoAPFAtributos_AtrTabelaNom_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV66TFFuncoesAPFAtributos_Code",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV67TFFuncoesAPFAtributos_Code_Sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV70TFFuncoesAPFAtributos_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV71TFFuncoesAPFAtributos_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV74TFFuncoesAPFAtributos_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV75TFFuncoesAPFAtributos_Descricao_Sel",SqlDbType.VarChar,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H009N2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009N2,11,0,true,false )
             ,new CursorDef("H009N3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009N3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((bool[]) buf[6])[0] = rslt.getBool(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((int[]) buf[12])[0] = rslt.getInt(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((String[]) buf[14])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((int[]) buf[16])[0] = rslt.getInt(9) ;
                ((String[]) buf[17])[0] = rslt.getVarchar(10) ;
                ((int[]) buf[18])[0] = rslt.getInt(11) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[49]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[50]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[53]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[54]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[57]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[58]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[59]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[60]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[65]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[66]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[67]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[68]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[69]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[70]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[71]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[72]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[73]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[48]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[49]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[52]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[53]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[54]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[55]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[58]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                return;
       }
    }

 }

}
