/*
               File: PromptContagemResultadoEvidencia
        Description: Selecione Contagem Resultado Evidencias
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:43:57.48
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptcontagemresultadoevidencia : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptcontagemresultadoevidencia( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptcontagemresultadoevidencia( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutContagemResultadoEvidencia_Codigo ,
                           ref String aP1_InOutContagemResultadoEvidencia_NomeArq )
      {
         this.AV8InOutContagemResultadoEvidencia_Codigo = aP0_InOutContagemResultadoEvidencia_Codigo;
         this.AV31InOutContagemResultadoEvidencia_NomeArq = aP1_InOutContagemResultadoEvidencia_NomeArq;
         executePrivate();
         aP0_InOutContagemResultadoEvidencia_Codigo=this.AV8InOutContagemResultadoEvidencia_Codigo;
         aP1_InOutContagemResultadoEvidencia_NomeArq=this.AV31InOutContagemResultadoEvidencia_NomeArq;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_65 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_65_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_65_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV14OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
               AV15OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
               AV16DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
               AV32ContagemResultadoEvidencia_NomeArq1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ContagemResultadoEvidencia_NomeArq1", AV32ContagemResultadoEvidencia_NomeArq1);
               AV20DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               AV33ContagemResultadoEvidencia_NomeArq2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContagemResultadoEvidencia_NomeArq2", AV33ContagemResultadoEvidencia_NomeArq2);
               AV24DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
               AV34ContagemResultadoEvidencia_NomeArq3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ContagemResultadoEvidencia_NomeArq3", AV34ContagemResultadoEvidencia_NomeArq3);
               AV19DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV23DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
               AV36TFContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFContagemResultado_Codigo), 6, 0)));
               AV37TFContagemResultado_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFContagemResultado_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37TFContagemResultado_Codigo_To), 6, 0)));
               AV40TFContagemResultadoEvidencia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContagemResultadoEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40TFContagemResultadoEvidencia_Codigo), 6, 0)));
               AV41TFContagemResultadoEvidencia_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFContagemResultadoEvidencia_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41TFContagemResultadoEvidencia_Codigo_To), 6, 0)));
               AV44TFContagemResultadoEvidencia_NomeArq = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContagemResultadoEvidencia_NomeArq", AV44TFContagemResultadoEvidencia_NomeArq);
               AV45TFContagemResultadoEvidencia_NomeArq_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContagemResultadoEvidencia_NomeArq_Sel", AV45TFContagemResultadoEvidencia_NomeArq_Sel);
               AV48TFContagemResultadoEvidencia_TipoArq = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFContagemResultadoEvidencia_TipoArq", AV48TFContagemResultadoEvidencia_TipoArq);
               AV49TFContagemResultadoEvidencia_TipoArq_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContagemResultadoEvidencia_TipoArq_Sel", AV49TFContagemResultadoEvidencia_TipoArq_Sel);
               AV52TFContagemResultadoEvidencia_Data = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFContagemResultadoEvidencia_Data", context.localUtil.TToC( AV52TFContagemResultadoEvidencia_Data, 8, 5, 0, 3, "/", ":", " "));
               AV53TFContagemResultadoEvidencia_Data_To = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContagemResultadoEvidencia_Data_To", context.localUtil.TToC( AV53TFContagemResultadoEvidencia_Data_To, 8, 5, 0, 3, "/", ":", " "));
               AV38ddo_ContagemResultado_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ddo_ContagemResultado_CodigoTitleControlIdToReplace", AV38ddo_ContagemResultado_CodigoTitleControlIdToReplace);
               AV42ddo_ContagemResultadoEvidencia_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ddo_ContagemResultadoEvidencia_CodigoTitleControlIdToReplace", AV42ddo_ContagemResultadoEvidencia_CodigoTitleControlIdToReplace);
               AV46ddo_ContagemResultadoEvidencia_NomeArqTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ddo_ContagemResultadoEvidencia_NomeArqTitleControlIdToReplace", AV46ddo_ContagemResultadoEvidencia_NomeArqTitleControlIdToReplace);
               AV50ddo_ContagemResultadoEvidencia_TipoArqTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ddo_ContagemResultadoEvidencia_TipoArqTitleControlIdToReplace", AV50ddo_ContagemResultadoEvidencia_TipoArqTitleControlIdToReplace);
               AV56ddo_ContagemResultadoEvidencia_DataTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ddo_ContagemResultadoEvidencia_DataTitleControlIdToReplace", AV56ddo_ContagemResultadoEvidencia_DataTitleControlIdToReplace);
               AV64Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV11GridState);
               AV28DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersIgnoreFirst", AV28DynamicFiltersIgnoreFirst);
               AV27DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV32ContagemResultadoEvidencia_NomeArq1, AV20DynamicFiltersSelector2, AV33ContagemResultadoEvidencia_NomeArq2, AV24DynamicFiltersSelector3, AV34ContagemResultadoEvidencia_NomeArq3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV36TFContagemResultado_Codigo, AV37TFContagemResultado_Codigo_To, AV40TFContagemResultadoEvidencia_Codigo, AV41TFContagemResultadoEvidencia_Codigo_To, AV44TFContagemResultadoEvidencia_NomeArq, AV45TFContagemResultadoEvidencia_NomeArq_Sel, AV48TFContagemResultadoEvidencia_TipoArq, AV49TFContagemResultadoEvidencia_TipoArq_Sel, AV52TFContagemResultadoEvidencia_Data, AV53TFContagemResultadoEvidencia_Data_To, AV38ddo_ContagemResultado_CodigoTitleControlIdToReplace, AV42ddo_ContagemResultadoEvidencia_CodigoTitleControlIdToReplace, AV46ddo_ContagemResultadoEvidencia_NomeArqTitleControlIdToReplace, AV50ddo_ContagemResultadoEvidencia_TipoArqTitleControlIdToReplace, AV56ddo_ContagemResultadoEvidencia_DataTitleControlIdToReplace, AV64Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV8InOutContagemResultadoEvidencia_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContagemResultadoEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8InOutContagemResultadoEvidencia_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV31InOutContagemResultadoEvidencia_NomeArq = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31InOutContagemResultadoEvidencia_NomeArq", AV31InOutContagemResultadoEvidencia_NomeArq);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PABY2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV64Pgmname = "PromptContagemResultadoEvidencia";
               context.Gx_err = 0;
               WSBY2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEBY2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117435779");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptcontagemresultadoevidencia.aspx") + "?" + UrlEncode("" +AV8InOutContagemResultadoEvidencia_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV31InOutContagemResultadoEvidencia_NomeArq))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV15OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV16DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ1", StringUtil.RTrim( AV32ContagemResultadoEvidencia_NomeArq1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV20DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ2", StringUtil.RTrim( AV33ContagemResultadoEvidencia_NomeArq2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV24DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ3", StringUtil.RTrim( AV34ContagemResultadoEvidencia_NomeArq3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV19DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV23DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36TFContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADO_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV37TFContagemResultado_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADOEVIDENCIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV40TFContagemResultadoEvidencia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADOEVIDENCIA_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV41TFContagemResultadoEvidencia_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADOEVIDENCIA_NOMEARQ", StringUtil.RTrim( AV44TFContagemResultadoEvidencia_NomeArq));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADOEVIDENCIA_NOMEARQ_SEL", StringUtil.RTrim( AV45TFContagemResultadoEvidencia_NomeArq_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADOEVIDENCIA_TIPOARQ", StringUtil.RTrim( AV48TFContagemResultadoEvidencia_TipoArq));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADOEVIDENCIA_TIPOARQ_SEL", StringUtil.RTrim( AV49TFContagemResultadoEvidencia_TipoArq_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADOEVIDENCIA_DATA", context.localUtil.TToC( AV52TFContagemResultadoEvidencia_Data, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADOEVIDENCIA_DATA_TO", context.localUtil.TToC( AV53TFContagemResultadoEvidencia_Data_To, 10, 8, 0, 3, "/", ":", " "));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_65", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_65), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV59GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV60GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV57DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV57DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADO_CODIGOTITLEFILTERDATA", AV35ContagemResultado_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADO_CODIGOTITLEFILTERDATA", AV35ContagemResultado_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADOEVIDENCIA_CODIGOTITLEFILTERDATA", AV39ContagemResultadoEvidencia_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADOEVIDENCIA_CODIGOTITLEFILTERDATA", AV39ContagemResultadoEvidencia_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADOEVIDENCIA_NOMEARQTITLEFILTERDATA", AV43ContagemResultadoEvidencia_NomeArqTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADOEVIDENCIA_NOMEARQTITLEFILTERDATA", AV43ContagemResultadoEvidencia_NomeArqTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADOEVIDENCIA_TIPOARQTITLEFILTERDATA", AV47ContagemResultadoEvidencia_TipoArqTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADOEVIDENCIA_TIPOARQTITLEFILTERDATA", AV47ContagemResultadoEvidencia_TipoArqTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADOEVIDENCIA_DATATITLEFILTERDATA", AV51ContagemResultadoEvidencia_DataTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADOEVIDENCIA_DATATITLEFILTERDATA", AV51ContagemResultadoEvidencia_DataTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV64Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV11GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV11GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV28DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV27DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTCONTAGEMRESULTADOEVIDENCIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8InOutContagemResultadoEvidencia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTCONTAGEMRESULTADOEVIDENCIA_NOMEARQ", StringUtil.RTrim( AV31InOutContagemResultadoEvidencia_NomeArq));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CODIGO_Caption", StringUtil.RTrim( Ddo_contagemresultado_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CODIGO_Tooltip", StringUtil.RTrim( Ddo_contagemresultado_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CODIGO_Cls", StringUtil.RTrim( Ddo_contagemresultado_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultado_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_contagemresultado_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultado_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultado_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultado_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultado_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_contagemresultado_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultado_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CODIGO_Filtertype", StringUtil.RTrim( Ddo_contagemresultado_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultado_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultado_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CODIGO_Sortasc", StringUtil.RTrim( Ddo_contagemresultado_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_contagemresultado_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultado_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_contagemresultado_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_contagemresultado_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultado_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO_Caption", StringUtil.RTrim( Ddo_contagemresultadoevidencia_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO_Tooltip", StringUtil.RTrim( Ddo_contagemresultadoevidencia_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO_Cls", StringUtil.RTrim( Ddo_contagemresultadoevidencia_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultadoevidencia_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_contagemresultadoevidencia_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultadoevidencia_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultadoevidencia_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultadoevidencia_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultadoevidencia_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_contagemresultadoevidencia_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultadoevidencia_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO_Filtertype", StringUtil.RTrim( Ddo_contagemresultadoevidencia_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultadoevidencia_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultadoevidencia_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO_Sortasc", StringUtil.RTrim( Ddo_contagemresultadoevidencia_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_contagemresultadoevidencia_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultadoevidencia_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_contagemresultadoevidencia_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_contagemresultadoevidencia_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultadoevidencia_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Caption", StringUtil.RTrim( Ddo_contagemresultadoevidencia_nomearq_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Tooltip", StringUtil.RTrim( Ddo_contagemresultadoevidencia_nomearq_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Cls", StringUtil.RTrim( Ddo_contagemresultadoevidencia_nomearq_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultadoevidencia_nomearq_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Selectedvalue_set", StringUtil.RTrim( Ddo_contagemresultadoevidencia_nomearq_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultadoevidencia_nomearq_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultadoevidencia_nomearq_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultadoevidencia_nomearq_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultadoevidencia_nomearq_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Sortedstatus", StringUtil.RTrim( Ddo_contagemresultadoevidencia_nomearq_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultadoevidencia_nomearq_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Filtertype", StringUtil.RTrim( Ddo_contagemresultadoevidencia_nomearq_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultadoevidencia_nomearq_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultadoevidencia_nomearq_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Datalisttype", StringUtil.RTrim( Ddo_contagemresultadoevidencia_nomearq_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Datalistproc", StringUtil.RTrim( Ddo_contagemresultadoevidencia_nomearq_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contagemresultadoevidencia_nomearq_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Sortasc", StringUtil.RTrim( Ddo_contagemresultadoevidencia_nomearq_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Sortdsc", StringUtil.RTrim( Ddo_contagemresultadoevidencia_nomearq_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Loadingdata", StringUtil.RTrim( Ddo_contagemresultadoevidencia_nomearq_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultadoevidencia_nomearq_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Noresultsfound", StringUtil.RTrim( Ddo_contagemresultadoevidencia_nomearq_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultadoevidencia_nomearq_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Caption", StringUtil.RTrim( Ddo_contagemresultadoevidencia_tipoarq_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Tooltip", StringUtil.RTrim( Ddo_contagemresultadoevidencia_tipoarq_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Cls", StringUtil.RTrim( Ddo_contagemresultadoevidencia_tipoarq_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultadoevidencia_tipoarq_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Selectedvalue_set", StringUtil.RTrim( Ddo_contagemresultadoevidencia_tipoarq_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultadoevidencia_tipoarq_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultadoevidencia_tipoarq_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultadoevidencia_tipoarq_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultadoevidencia_tipoarq_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Sortedstatus", StringUtil.RTrim( Ddo_contagemresultadoevidencia_tipoarq_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultadoevidencia_tipoarq_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Filtertype", StringUtil.RTrim( Ddo_contagemresultadoevidencia_tipoarq_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultadoevidencia_tipoarq_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultadoevidencia_tipoarq_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Datalisttype", StringUtil.RTrim( Ddo_contagemresultadoevidencia_tipoarq_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Datalistproc", StringUtil.RTrim( Ddo_contagemresultadoevidencia_tipoarq_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contagemresultadoevidencia_tipoarq_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Sortasc", StringUtil.RTrim( Ddo_contagemresultadoevidencia_tipoarq_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Sortdsc", StringUtil.RTrim( Ddo_contagemresultadoevidencia_tipoarq_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Loadingdata", StringUtil.RTrim( Ddo_contagemresultadoevidencia_tipoarq_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultadoevidencia_tipoarq_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Noresultsfound", StringUtil.RTrim( Ddo_contagemresultadoevidencia_tipoarq_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultadoevidencia_tipoarq_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA_Caption", StringUtil.RTrim( Ddo_contagemresultadoevidencia_data_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA_Tooltip", StringUtil.RTrim( Ddo_contagemresultadoevidencia_data_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA_Cls", StringUtil.RTrim( Ddo_contagemresultadoevidencia_data_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultadoevidencia_data_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA_Filteredtextto_set", StringUtil.RTrim( Ddo_contagemresultadoevidencia_data_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultadoevidencia_data_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultadoevidencia_data_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultadoevidencia_data_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultadoevidencia_data_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA_Sortedstatus", StringUtil.RTrim( Ddo_contagemresultadoevidencia_data_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultadoevidencia_data_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA_Filtertype", StringUtil.RTrim( Ddo_contagemresultadoevidencia_data_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultadoevidencia_data_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultadoevidencia_data_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA_Sortasc", StringUtil.RTrim( Ddo_contagemresultadoevidencia_data_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA_Sortdsc", StringUtil.RTrim( Ddo_contagemresultadoevidencia_data_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultadoevidencia_data_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA_Rangefilterfrom", StringUtil.RTrim( Ddo_contagemresultadoevidencia_data_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA_Rangefilterto", StringUtil.RTrim( Ddo_contagemresultadoevidencia_data_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultadoevidencia_data_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultado_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultado_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADO_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_contagemresultado_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultadoevidencia_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultadoevidencia_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_contagemresultadoevidencia_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultadoevidencia_nomearq_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultadoevidencia_nomearq_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Selectedvalue_get", StringUtil.RTrim( Ddo_contagemresultadoevidencia_nomearq_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultadoevidencia_tipoarq_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultadoevidencia_tipoarq_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Selectedvalue_get", StringUtil.RTrim( Ddo_contagemresultadoevidencia_tipoarq_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultadoevidencia_data_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultadoevidencia_data_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA_Filteredtextto_get", StringUtil.RTrim( Ddo_contagemresultadoevidencia_data_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormBY2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptContagemResultadoEvidencia" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Contagem Resultado Evidencias" ;
      }

      protected void WBBY0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_BY2( true) ;
         }
         else
         {
            wb_table1_2_BY2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_BY2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'" + sGXsfl_65_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV19DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(76, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,76);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'',false,'" + sGXsfl_65_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV23DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(77, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,77);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'" + sGXsfl_65_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36TFContagemResultado_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV36TFContagemResultado_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,78);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoEvidencia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'" + sGXsfl_65_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultado_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV37TFContagemResultado_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV37TFContagemResultado_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,79);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultado_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultado_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoEvidencia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'',false,'" + sGXsfl_65_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadoevidencia_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV40TFContagemResultadoEvidencia_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV40TFContagemResultadoEvidencia_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,80);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadoevidencia_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadoevidencia_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoEvidencia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'" + sGXsfl_65_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadoevidencia_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV41TFContagemResultadoEvidencia_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV41TFContagemResultadoEvidencia_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,81);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadoevidencia_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadoevidencia_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoEvidencia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'',false,'" + sGXsfl_65_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadoevidencia_nomearq_Internalname, StringUtil.RTrim( AV44TFContagemResultadoEvidencia_NomeArq), StringUtil.RTrim( context.localUtil.Format( AV44TFContagemResultadoEvidencia_NomeArq, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,82);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadoevidencia_nomearq_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadoevidencia_nomearq_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContagemResultadoEvidencia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'',false,'" + sGXsfl_65_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadoevidencia_nomearq_sel_Internalname, StringUtil.RTrim( AV45TFContagemResultadoEvidencia_NomeArq_Sel), StringUtil.RTrim( context.localUtil.Format( AV45TFContagemResultadoEvidencia_NomeArq_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,83);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadoevidencia_nomearq_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadoevidencia_nomearq_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContagemResultadoEvidencia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'" + sGXsfl_65_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadoevidencia_tipoarq_Internalname, StringUtil.RTrim( AV48TFContagemResultadoEvidencia_TipoArq), StringUtil.RTrim( context.localUtil.Format( AV48TFContagemResultadoEvidencia_TipoArq, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,84);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadoevidencia_tipoarq_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadoevidencia_tipoarq_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContagemResultadoEvidencia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 85,'',false,'" + sGXsfl_65_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadoevidencia_tipoarq_sel_Internalname, StringUtil.RTrim( AV49TFContagemResultadoEvidencia_TipoArq_Sel), StringUtil.RTrim( context.localUtil.Format( AV49TFContagemResultadoEvidencia_TipoArq_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,85);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadoevidencia_tipoarq_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadoevidencia_tipoarq_sel_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContagemResultadoEvidencia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 86,'',false,'" + sGXsfl_65_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontagemresultadoevidencia_data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadoevidencia_data_Internalname, context.localUtil.TToC( AV52TFContagemResultadoEvidencia_Data, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV52TFContagemResultadoEvidencia_Data, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,86);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadoevidencia_data_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadoevidencia_data_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoEvidencia.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontagemresultadoevidencia_data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontagemresultadoevidencia_data_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoEvidencia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 87,'',false,'" + sGXsfl_65_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontagemresultadoevidencia_data_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadoevidencia_data_to_Internalname, context.localUtil.TToC( AV53TFContagemResultadoEvidencia_Data_To, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV53TFContagemResultadoEvidencia_Data_To, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,87);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadoevidencia_data_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadoevidencia_data_to_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoEvidencia.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontagemresultadoevidencia_data_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontagemresultadoevidencia_data_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoEvidencia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_contagemresultadoevidencia_dataauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'',false,'" + sGXsfl_65_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contagemresultadoevidencia_dataauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contagemresultadoevidencia_dataauxdate_Internalname, context.localUtil.Format(AV54DDO_ContagemResultadoEvidencia_DataAuxDate, "99/99/99"), context.localUtil.Format( AV54DDO_ContagemResultadoEvidencia_DataAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,89);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contagemresultadoevidencia_dataauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoEvidencia.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contagemresultadoevidencia_dataauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoEvidencia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 90,'',false,'" + sGXsfl_65_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contagemresultadoevidencia_dataauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contagemresultadoevidencia_dataauxdateto_Internalname, context.localUtil.Format(AV55DDO_ContagemResultadoEvidencia_DataAuxDateTo, "99/99/99"), context.localUtil.Format( AV55DDO_ContagemResultadoEvidencia_DataAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,90);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contagemresultadoevidencia_dataauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoEvidencia.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contagemresultadoevidencia_dataauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoEvidencia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMRESULTADO_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'',false,'" + sGXsfl_65_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultado_codigotitlecontrolidtoreplace_Internalname, AV38ddo_ContagemResultado_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,92);\"", 0, edtavDdo_contagemresultado_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContagemResultadoEvidencia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'" + sGXsfl_65_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultadoevidencia_codigotitlecontrolidtoreplace_Internalname, AV42ddo_ContagemResultadoEvidencia_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,94);\"", 0, edtavDdo_contagemresultadoevidencia_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContagemResultadoEvidencia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'" + sGXsfl_65_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultadoevidencia_nomearqtitlecontrolidtoreplace_Internalname, AV46ddo_ContagemResultadoEvidencia_NomeArqTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,96);\"", 0, edtavDdo_contagemresultadoevidencia_nomearqtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContagemResultadoEvidencia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'',false,'" + sGXsfl_65_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultadoevidencia_tipoarqtitlecontrolidtoreplace_Internalname, AV50ddo_ContagemResultadoEvidencia_TipoArqTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,98);\"", 0, edtavDdo_contagemresultadoevidencia_tipoarqtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContagemResultadoEvidencia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMRESULTADOEVIDENCIA_DATAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'',false,'" + sGXsfl_65_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultadoevidencia_datatitlecontrolidtoreplace_Internalname, AV56ddo_ContagemResultadoEvidencia_DataTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,100);\"", 0, edtavDdo_contagemresultadoevidencia_datatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContagemResultadoEvidencia.htm");
         }
         wbLoad = true;
      }

      protected void STARTBY2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Contagem Resultado Evidencias", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPBY0( ) ;
      }

      protected void WSBY2( )
      {
         STARTBY2( ) ;
         EVTBY2( ) ;
      }

      protected void EVTBY2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11BY2 */
                           E11BY2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADO_CODIGO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12BY2 */
                           E12BY2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13BY2 */
                           E13BY2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14BY2 */
                           E14BY2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15BY2 */
                           E15BY2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16BY2 */
                           E16BY2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17BY2 */
                           E17BY2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18BY2 */
                           E18BY2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19BY2 */
                           E19BY2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E20BY2 */
                           E20BY2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E21BY2 */
                           E21BY2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E22BY2 */
                           E22BY2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E23BY2 */
                           E23BY2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E24BY2 */
                           E24BY2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E25BY2 */
                           E25BY2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E26BY2 */
                           E26BY2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_65_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_65_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_65_idx), 4, 0)), 4, "0");
                           SubsflControlProps_652( ) ;
                           AV29Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29Select)) ? AV63Select_GXI : context.convertURL( context.PathToRelativeUrl( AV29Select))));
                           A456ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagemResultado_Codigo_Internalname), ",", "."));
                           A586ContagemResultadoEvidencia_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoEvidencia_Codigo_Internalname), ",", "."));
                           A589ContagemResultadoEvidencia_NomeArq = cgiGet( edtContagemResultadoEvidencia_NomeArq_Internalname);
                           n589ContagemResultadoEvidencia_NomeArq = false;
                           A590ContagemResultadoEvidencia_TipoArq = cgiGet( edtContagemResultadoEvidencia_TipoArq_Internalname);
                           n590ContagemResultadoEvidencia_TipoArq = false;
                           A591ContagemResultadoEvidencia_Data = context.localUtil.CToT( cgiGet( edtContagemResultadoEvidencia_Data_Internalname), 0);
                           n591ContagemResultadoEvidencia_Data = false;
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E27BY2 */
                                 E27BY2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E28BY2 */
                                 E28BY2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E29BY2 */
                                 E29BY2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV15OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemresultadoevidencia_nomearq1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ1"), AV32ContagemResultadoEvidencia_NomeArq1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemresultadoevidencia_nomearq2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ2"), AV33ContagemResultadoEvidencia_NomeArq2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV24DynamicFiltersSelector3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemresultadoevidencia_nomearq3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ3"), AV34ContagemResultadoEvidencia_NomeArq3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV23DynamicFiltersEnabled3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultado_codigo Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADO_CODIGO"), ",", ".") != Convert.ToDecimal( AV36TFContagemResultado_Codigo )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultado_codigo_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV37TFContagemResultado_Codigo_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultadoevidencia_codigo Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADOEVIDENCIA_CODIGO"), ",", ".") != Convert.ToDecimal( AV40TFContagemResultadoEvidencia_Codigo )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultadoevidencia_codigo_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADOEVIDENCIA_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV41TFContagemResultadoEvidencia_Codigo_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultadoevidencia_nomearq Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADOEVIDENCIA_NOMEARQ"), AV44TFContagemResultadoEvidencia_NomeArq) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultadoevidencia_nomearq_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADOEVIDENCIA_NOMEARQ_SEL"), AV45TFContagemResultadoEvidencia_NomeArq_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultadoevidencia_tipoarq Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADOEVIDENCIA_TIPOARQ"), AV48TFContagemResultadoEvidencia_TipoArq) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultadoevidencia_tipoarq_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADOEVIDENCIA_TIPOARQ_SEL"), AV49TFContagemResultadoEvidencia_TipoArq_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultadoevidencia_data Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADOEVIDENCIA_DATA"), 0) != AV52TFContagemResultadoEvidencia_Data )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultadoevidencia_data_to Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADOEVIDENCIA_DATA_TO"), 0) != AV53TFContagemResultadoEvidencia_Data_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E30BY2 */
                                       E30BY2 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEBY2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormBY2( ) ;
            }
         }
      }

      protected void PABY2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CONTAGEMRESULTADOEVIDENCIA_NOMEARQ", "Nome do Arquivo", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CONTAGEMRESULTADOEVIDENCIA_NOMEARQ", "Nome do Arquivo", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("CONTAGEMRESULTADOEVIDENCIA_NOMEARQ", "Nome do Arquivo", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV24DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV24DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_652( ) ;
         while ( nGXsfl_65_idx <= nRC_GXsfl_65 )
         {
            sendrow_652( ) ;
            nGXsfl_65_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_65_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_65_idx+1));
            sGXsfl_65_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_65_idx), 4, 0)), 4, "0");
            SubsflControlProps_652( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV14OrderedBy ,
                                       bool AV15OrderedDsc ,
                                       String AV16DynamicFiltersSelector1 ,
                                       String AV32ContagemResultadoEvidencia_NomeArq1 ,
                                       String AV20DynamicFiltersSelector2 ,
                                       String AV33ContagemResultadoEvidencia_NomeArq2 ,
                                       String AV24DynamicFiltersSelector3 ,
                                       String AV34ContagemResultadoEvidencia_NomeArq3 ,
                                       bool AV19DynamicFiltersEnabled2 ,
                                       bool AV23DynamicFiltersEnabled3 ,
                                       int AV36TFContagemResultado_Codigo ,
                                       int AV37TFContagemResultado_Codigo_To ,
                                       int AV40TFContagemResultadoEvidencia_Codigo ,
                                       int AV41TFContagemResultadoEvidencia_Codigo_To ,
                                       String AV44TFContagemResultadoEvidencia_NomeArq ,
                                       String AV45TFContagemResultadoEvidencia_NomeArq_Sel ,
                                       String AV48TFContagemResultadoEvidencia_TipoArq ,
                                       String AV49TFContagemResultadoEvidencia_TipoArq_Sel ,
                                       DateTime AV52TFContagemResultadoEvidencia_Data ,
                                       DateTime AV53TFContagemResultadoEvidencia_Data_To ,
                                       String AV38ddo_ContagemResultado_CodigoTitleControlIdToReplace ,
                                       String AV42ddo_ContagemResultadoEvidencia_CodigoTitleControlIdToReplace ,
                                       String AV46ddo_ContagemResultadoEvidencia_NomeArqTitleControlIdToReplace ,
                                       String AV50ddo_ContagemResultadoEvidencia_TipoArqTitleControlIdToReplace ,
                                       String AV56ddo_ContagemResultadoEvidencia_DataTitleControlIdToReplace ,
                                       String AV64Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV11GridState ,
                                       bool AV28DynamicFiltersIgnoreFirst ,
                                       bool AV27DynamicFiltersRemoving )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFBY2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADOEVIDENCIA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A586ContagemResultadoEvidencia_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOEVIDENCIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A586ContagemResultadoEvidencia_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A589ContagemResultadoEvidencia_NomeArq, ""))));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ", StringUtil.RTrim( A589ContagemResultadoEvidencia_NomeArq));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A590ContagemResultadoEvidencia_TipoArq, ""))));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOEVIDENCIA_TIPOARQ", StringUtil.RTrim( A590ContagemResultadoEvidencia_TipoArq));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADOEVIDENCIA_DATA", GetSecureSignedToken( "", context.localUtil.Format( A591ContagemResultadoEvidencia_Data, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOEVIDENCIA_DATA", context.localUtil.TToC( A591ContagemResultadoEvidencia_Data, 10, 8, 0, 3, "/", ":", " "));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV24DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV24DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFBY2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV64Pgmname = "PromptContagemResultadoEvidencia";
         context.Gx_err = 0;
      }

      protected void RFBY2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 65;
         /* Execute user event: E28BY2 */
         E28BY2 ();
         nGXsfl_65_idx = 1;
         sGXsfl_65_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_65_idx), 4, 0)), 4, "0");
         SubsflControlProps_652( ) ;
         nGXsfl_65_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_652( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV16DynamicFiltersSelector1 ,
                                                 AV32ContagemResultadoEvidencia_NomeArq1 ,
                                                 AV19DynamicFiltersEnabled2 ,
                                                 AV20DynamicFiltersSelector2 ,
                                                 AV33ContagemResultadoEvidencia_NomeArq2 ,
                                                 AV23DynamicFiltersEnabled3 ,
                                                 AV24DynamicFiltersSelector3 ,
                                                 AV34ContagemResultadoEvidencia_NomeArq3 ,
                                                 AV36TFContagemResultado_Codigo ,
                                                 AV37TFContagemResultado_Codigo_To ,
                                                 AV40TFContagemResultadoEvidencia_Codigo ,
                                                 AV41TFContagemResultadoEvidencia_Codigo_To ,
                                                 AV45TFContagemResultadoEvidencia_NomeArq_Sel ,
                                                 AV44TFContagemResultadoEvidencia_NomeArq ,
                                                 AV49TFContagemResultadoEvidencia_TipoArq_Sel ,
                                                 AV48TFContagemResultadoEvidencia_TipoArq ,
                                                 AV52TFContagemResultadoEvidencia_Data ,
                                                 AV53TFContagemResultadoEvidencia_Data_To ,
                                                 A589ContagemResultadoEvidencia_NomeArq ,
                                                 A456ContagemResultado_Codigo ,
                                                 A586ContagemResultadoEvidencia_Codigo ,
                                                 A590ContagemResultadoEvidencia_TipoArq ,
                                                 A591ContagemResultadoEvidencia_Data ,
                                                 AV14OrderedBy ,
                                                 AV15OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV32ContagemResultadoEvidencia_NomeArq1 = StringUtil.PadR( StringUtil.RTrim( AV32ContagemResultadoEvidencia_NomeArq1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ContagemResultadoEvidencia_NomeArq1", AV32ContagemResultadoEvidencia_NomeArq1);
            lV33ContagemResultadoEvidencia_NomeArq2 = StringUtil.PadR( StringUtil.RTrim( AV33ContagemResultadoEvidencia_NomeArq2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContagemResultadoEvidencia_NomeArq2", AV33ContagemResultadoEvidencia_NomeArq2);
            lV34ContagemResultadoEvidencia_NomeArq3 = StringUtil.PadR( StringUtil.RTrim( AV34ContagemResultadoEvidencia_NomeArq3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ContagemResultadoEvidencia_NomeArq3", AV34ContagemResultadoEvidencia_NomeArq3);
            lV44TFContagemResultadoEvidencia_NomeArq = StringUtil.PadR( StringUtil.RTrim( AV44TFContagemResultadoEvidencia_NomeArq), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContagemResultadoEvidencia_NomeArq", AV44TFContagemResultadoEvidencia_NomeArq);
            lV48TFContagemResultadoEvidencia_TipoArq = StringUtil.PadR( StringUtil.RTrim( AV48TFContagemResultadoEvidencia_TipoArq), 10, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFContagemResultadoEvidencia_TipoArq", AV48TFContagemResultadoEvidencia_TipoArq);
            /* Using cursor H00BY2 */
            pr_default.execute(0, new Object[] {lV32ContagemResultadoEvidencia_NomeArq1, lV33ContagemResultadoEvidencia_NomeArq2, lV34ContagemResultadoEvidencia_NomeArq3, AV36TFContagemResultado_Codigo, AV37TFContagemResultado_Codigo_To, AV40TFContagemResultadoEvidencia_Codigo, AV41TFContagemResultadoEvidencia_Codigo_To, lV44TFContagemResultadoEvidencia_NomeArq, AV45TFContagemResultadoEvidencia_NomeArq_Sel, lV48TFContagemResultadoEvidencia_TipoArq, AV49TFContagemResultadoEvidencia_TipoArq_Sel, AV52TFContagemResultadoEvidencia_Data, AV53TFContagemResultadoEvidencia_Data_To, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_65_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A591ContagemResultadoEvidencia_Data = H00BY2_A591ContagemResultadoEvidencia_Data[0];
               n591ContagemResultadoEvidencia_Data = H00BY2_n591ContagemResultadoEvidencia_Data[0];
               A590ContagemResultadoEvidencia_TipoArq = H00BY2_A590ContagemResultadoEvidencia_TipoArq[0];
               n590ContagemResultadoEvidencia_TipoArq = H00BY2_n590ContagemResultadoEvidencia_TipoArq[0];
               A589ContagemResultadoEvidencia_NomeArq = H00BY2_A589ContagemResultadoEvidencia_NomeArq[0];
               n589ContagemResultadoEvidencia_NomeArq = H00BY2_n589ContagemResultadoEvidencia_NomeArq[0];
               A586ContagemResultadoEvidencia_Codigo = H00BY2_A586ContagemResultadoEvidencia_Codigo[0];
               A456ContagemResultado_Codigo = H00BY2_A456ContagemResultado_Codigo[0];
               /* Execute user event: E29BY2 */
               E29BY2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 65;
            WBBY0( ) ;
         }
         nGXsfl_65_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV16DynamicFiltersSelector1 ,
                                              AV32ContagemResultadoEvidencia_NomeArq1 ,
                                              AV19DynamicFiltersEnabled2 ,
                                              AV20DynamicFiltersSelector2 ,
                                              AV33ContagemResultadoEvidencia_NomeArq2 ,
                                              AV23DynamicFiltersEnabled3 ,
                                              AV24DynamicFiltersSelector3 ,
                                              AV34ContagemResultadoEvidencia_NomeArq3 ,
                                              AV36TFContagemResultado_Codigo ,
                                              AV37TFContagemResultado_Codigo_To ,
                                              AV40TFContagemResultadoEvidencia_Codigo ,
                                              AV41TFContagemResultadoEvidencia_Codigo_To ,
                                              AV45TFContagemResultadoEvidencia_NomeArq_Sel ,
                                              AV44TFContagemResultadoEvidencia_NomeArq ,
                                              AV49TFContagemResultadoEvidencia_TipoArq_Sel ,
                                              AV48TFContagemResultadoEvidencia_TipoArq ,
                                              AV52TFContagemResultadoEvidencia_Data ,
                                              AV53TFContagemResultadoEvidencia_Data_To ,
                                              A589ContagemResultadoEvidencia_NomeArq ,
                                              A456ContagemResultado_Codigo ,
                                              A586ContagemResultadoEvidencia_Codigo ,
                                              A590ContagemResultadoEvidencia_TipoArq ,
                                              A591ContagemResultadoEvidencia_Data ,
                                              AV14OrderedBy ,
                                              AV15OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV32ContagemResultadoEvidencia_NomeArq1 = StringUtil.PadR( StringUtil.RTrim( AV32ContagemResultadoEvidencia_NomeArq1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ContagemResultadoEvidencia_NomeArq1", AV32ContagemResultadoEvidencia_NomeArq1);
         lV33ContagemResultadoEvidencia_NomeArq2 = StringUtil.PadR( StringUtil.RTrim( AV33ContagemResultadoEvidencia_NomeArq2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContagemResultadoEvidencia_NomeArq2", AV33ContagemResultadoEvidencia_NomeArq2);
         lV34ContagemResultadoEvidencia_NomeArq3 = StringUtil.PadR( StringUtil.RTrim( AV34ContagemResultadoEvidencia_NomeArq3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ContagemResultadoEvidencia_NomeArq3", AV34ContagemResultadoEvidencia_NomeArq3);
         lV44TFContagemResultadoEvidencia_NomeArq = StringUtil.PadR( StringUtil.RTrim( AV44TFContagemResultadoEvidencia_NomeArq), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContagemResultadoEvidencia_NomeArq", AV44TFContagemResultadoEvidencia_NomeArq);
         lV48TFContagemResultadoEvidencia_TipoArq = StringUtil.PadR( StringUtil.RTrim( AV48TFContagemResultadoEvidencia_TipoArq), 10, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFContagemResultadoEvidencia_TipoArq", AV48TFContagemResultadoEvidencia_TipoArq);
         /* Using cursor H00BY3 */
         pr_default.execute(1, new Object[] {lV32ContagemResultadoEvidencia_NomeArq1, lV33ContagemResultadoEvidencia_NomeArq2, lV34ContagemResultadoEvidencia_NomeArq3, AV36TFContagemResultado_Codigo, AV37TFContagemResultado_Codigo_To, AV40TFContagemResultadoEvidencia_Codigo, AV41TFContagemResultadoEvidencia_Codigo_To, lV44TFContagemResultadoEvidencia_NomeArq, AV45TFContagemResultadoEvidencia_NomeArq_Sel, lV48TFContagemResultadoEvidencia_TipoArq, AV49TFContagemResultadoEvidencia_TipoArq_Sel, AV52TFContagemResultadoEvidencia_Data, AV53TFContagemResultadoEvidencia_Data_To});
         GRID_nRecordCount = H00BY3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV32ContagemResultadoEvidencia_NomeArq1, AV20DynamicFiltersSelector2, AV33ContagemResultadoEvidencia_NomeArq2, AV24DynamicFiltersSelector3, AV34ContagemResultadoEvidencia_NomeArq3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV36TFContagemResultado_Codigo, AV37TFContagemResultado_Codigo_To, AV40TFContagemResultadoEvidencia_Codigo, AV41TFContagemResultadoEvidencia_Codigo_To, AV44TFContagemResultadoEvidencia_NomeArq, AV45TFContagemResultadoEvidencia_NomeArq_Sel, AV48TFContagemResultadoEvidencia_TipoArq, AV49TFContagemResultadoEvidencia_TipoArq_Sel, AV52TFContagemResultadoEvidencia_Data, AV53TFContagemResultadoEvidencia_Data_To, AV38ddo_ContagemResultado_CodigoTitleControlIdToReplace, AV42ddo_ContagemResultadoEvidencia_CodigoTitleControlIdToReplace, AV46ddo_ContagemResultadoEvidencia_NomeArqTitleControlIdToReplace, AV50ddo_ContagemResultadoEvidencia_TipoArqTitleControlIdToReplace, AV56ddo_ContagemResultadoEvidencia_DataTitleControlIdToReplace, AV64Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV32ContagemResultadoEvidencia_NomeArq1, AV20DynamicFiltersSelector2, AV33ContagemResultadoEvidencia_NomeArq2, AV24DynamicFiltersSelector3, AV34ContagemResultadoEvidencia_NomeArq3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV36TFContagemResultado_Codigo, AV37TFContagemResultado_Codigo_To, AV40TFContagemResultadoEvidencia_Codigo, AV41TFContagemResultadoEvidencia_Codigo_To, AV44TFContagemResultadoEvidencia_NomeArq, AV45TFContagemResultadoEvidencia_NomeArq_Sel, AV48TFContagemResultadoEvidencia_TipoArq, AV49TFContagemResultadoEvidencia_TipoArq_Sel, AV52TFContagemResultadoEvidencia_Data, AV53TFContagemResultadoEvidencia_Data_To, AV38ddo_ContagemResultado_CodigoTitleControlIdToReplace, AV42ddo_ContagemResultadoEvidencia_CodigoTitleControlIdToReplace, AV46ddo_ContagemResultadoEvidencia_NomeArqTitleControlIdToReplace, AV50ddo_ContagemResultadoEvidencia_TipoArqTitleControlIdToReplace, AV56ddo_ContagemResultadoEvidencia_DataTitleControlIdToReplace, AV64Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV32ContagemResultadoEvidencia_NomeArq1, AV20DynamicFiltersSelector2, AV33ContagemResultadoEvidencia_NomeArq2, AV24DynamicFiltersSelector3, AV34ContagemResultadoEvidencia_NomeArq3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV36TFContagemResultado_Codigo, AV37TFContagemResultado_Codigo_To, AV40TFContagemResultadoEvidencia_Codigo, AV41TFContagemResultadoEvidencia_Codigo_To, AV44TFContagemResultadoEvidencia_NomeArq, AV45TFContagemResultadoEvidencia_NomeArq_Sel, AV48TFContagemResultadoEvidencia_TipoArq, AV49TFContagemResultadoEvidencia_TipoArq_Sel, AV52TFContagemResultadoEvidencia_Data, AV53TFContagemResultadoEvidencia_Data_To, AV38ddo_ContagemResultado_CodigoTitleControlIdToReplace, AV42ddo_ContagemResultadoEvidencia_CodigoTitleControlIdToReplace, AV46ddo_ContagemResultadoEvidencia_NomeArqTitleControlIdToReplace, AV50ddo_ContagemResultadoEvidencia_TipoArqTitleControlIdToReplace, AV56ddo_ContagemResultadoEvidencia_DataTitleControlIdToReplace, AV64Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV32ContagemResultadoEvidencia_NomeArq1, AV20DynamicFiltersSelector2, AV33ContagemResultadoEvidencia_NomeArq2, AV24DynamicFiltersSelector3, AV34ContagemResultadoEvidencia_NomeArq3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV36TFContagemResultado_Codigo, AV37TFContagemResultado_Codigo_To, AV40TFContagemResultadoEvidencia_Codigo, AV41TFContagemResultadoEvidencia_Codigo_To, AV44TFContagemResultadoEvidencia_NomeArq, AV45TFContagemResultadoEvidencia_NomeArq_Sel, AV48TFContagemResultadoEvidencia_TipoArq, AV49TFContagemResultadoEvidencia_TipoArq_Sel, AV52TFContagemResultadoEvidencia_Data, AV53TFContagemResultadoEvidencia_Data_To, AV38ddo_ContagemResultado_CodigoTitleControlIdToReplace, AV42ddo_ContagemResultadoEvidencia_CodigoTitleControlIdToReplace, AV46ddo_ContagemResultadoEvidencia_NomeArqTitleControlIdToReplace, AV50ddo_ContagemResultadoEvidencia_TipoArqTitleControlIdToReplace, AV56ddo_ContagemResultadoEvidencia_DataTitleControlIdToReplace, AV64Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV32ContagemResultadoEvidencia_NomeArq1, AV20DynamicFiltersSelector2, AV33ContagemResultadoEvidencia_NomeArq2, AV24DynamicFiltersSelector3, AV34ContagemResultadoEvidencia_NomeArq3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV36TFContagemResultado_Codigo, AV37TFContagemResultado_Codigo_To, AV40TFContagemResultadoEvidencia_Codigo, AV41TFContagemResultadoEvidencia_Codigo_To, AV44TFContagemResultadoEvidencia_NomeArq, AV45TFContagemResultadoEvidencia_NomeArq_Sel, AV48TFContagemResultadoEvidencia_TipoArq, AV49TFContagemResultadoEvidencia_TipoArq_Sel, AV52TFContagemResultadoEvidencia_Data, AV53TFContagemResultadoEvidencia_Data_To, AV38ddo_ContagemResultado_CodigoTitleControlIdToReplace, AV42ddo_ContagemResultadoEvidencia_CodigoTitleControlIdToReplace, AV46ddo_ContagemResultadoEvidencia_NomeArqTitleControlIdToReplace, AV50ddo_ContagemResultadoEvidencia_TipoArqTitleControlIdToReplace, AV56ddo_ContagemResultadoEvidencia_DataTitleControlIdToReplace, AV64Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving) ;
         }
         return (int)(0) ;
      }

      protected void STRUPBY0( )
      {
         /* Before Start, stand alone formulas. */
         AV64Pgmname = "PromptContagemResultadoEvidencia";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E27BY2 */
         E27BY2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV57DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMRESULTADO_CODIGOTITLEFILTERDATA"), AV35ContagemResultado_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMRESULTADOEVIDENCIA_CODIGOTITLEFILTERDATA"), AV39ContagemResultadoEvidencia_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMRESULTADOEVIDENCIA_NOMEARQTITLEFILTERDATA"), AV43ContagemResultadoEvidencia_NomeArqTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMRESULTADOEVIDENCIA_TIPOARQTITLEFILTERDATA"), AV47ContagemResultadoEvidencia_TipoArqTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMRESULTADOEVIDENCIA_DATATITLEFILTERDATA"), AV51ContagemResultadoEvidencia_DataTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV14OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV16DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            AV32ContagemResultadoEvidencia_NomeArq1 = cgiGet( edtavContagemresultadoevidencia_nomearq1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ContagemResultadoEvidencia_NomeArq1", AV32ContagemResultadoEvidencia_NomeArq1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV20DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            AV33ContagemResultadoEvidencia_NomeArq2 = cgiGet( edtavContagemresultadoevidencia_nomearq2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContagemResultadoEvidencia_NomeArq2", AV33ContagemResultadoEvidencia_NomeArq2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV24DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
            AV34ContagemResultadoEvidencia_NomeArq3 = cgiGet( edtavContagemresultadoevidencia_nomearq3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ContagemResultadoEvidencia_NomeArq3", AV34ContagemResultadoEvidencia_NomeArq3);
            AV19DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
            AV23DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMRESULTADO_CODIGO");
               GX_FocusControl = edtavTfcontagemresultado_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV36TFContagemResultado_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFContagemResultado_Codigo), 6, 0)));
            }
            else
            {
               AV36TFContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFContagemResultado_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMRESULTADO_CODIGO_TO");
               GX_FocusControl = edtavTfcontagemresultado_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV37TFContagemResultado_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFContagemResultado_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37TFContagemResultado_Codigo_To), 6, 0)));
            }
            else
            {
               AV37TFContagemResultado_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontagemresultado_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFContagemResultado_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37TFContagemResultado_Codigo_To), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultadoevidencia_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultadoevidencia_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMRESULTADOEVIDENCIA_CODIGO");
               GX_FocusControl = edtavTfcontagemresultadoevidencia_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV40TFContagemResultadoEvidencia_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContagemResultadoEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40TFContagemResultadoEvidencia_Codigo), 6, 0)));
            }
            else
            {
               AV40TFContagemResultadoEvidencia_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfcontagemresultadoevidencia_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContagemResultadoEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40TFContagemResultadoEvidencia_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultadoevidencia_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultadoevidencia_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMRESULTADOEVIDENCIA_CODIGO_TO");
               GX_FocusControl = edtavTfcontagemresultadoevidencia_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV41TFContagemResultadoEvidencia_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFContagemResultadoEvidencia_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41TFContagemResultadoEvidencia_Codigo_To), 6, 0)));
            }
            else
            {
               AV41TFContagemResultadoEvidencia_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontagemresultadoevidencia_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFContagemResultadoEvidencia_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41TFContagemResultadoEvidencia_Codigo_To), 6, 0)));
            }
            AV44TFContagemResultadoEvidencia_NomeArq = cgiGet( edtavTfcontagemresultadoevidencia_nomearq_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContagemResultadoEvidencia_NomeArq", AV44TFContagemResultadoEvidencia_NomeArq);
            AV45TFContagemResultadoEvidencia_NomeArq_Sel = cgiGet( edtavTfcontagemresultadoevidencia_nomearq_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContagemResultadoEvidencia_NomeArq_Sel", AV45TFContagemResultadoEvidencia_NomeArq_Sel);
            AV48TFContagemResultadoEvidencia_TipoArq = cgiGet( edtavTfcontagemresultadoevidencia_tipoarq_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFContagemResultadoEvidencia_TipoArq", AV48TFContagemResultadoEvidencia_TipoArq);
            AV49TFContagemResultadoEvidencia_TipoArq_Sel = cgiGet( edtavTfcontagemresultadoevidencia_tipoarq_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContagemResultadoEvidencia_TipoArq_Sel", AV49TFContagemResultadoEvidencia_TipoArq_Sel);
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontagemresultadoevidencia_data_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFContagem Resultado Evidencia_Data"}), 1, "vTFCONTAGEMRESULTADOEVIDENCIA_DATA");
               GX_FocusControl = edtavTfcontagemresultadoevidencia_data_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV52TFContagemResultadoEvidencia_Data = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFContagemResultadoEvidencia_Data", context.localUtil.TToC( AV52TFContagemResultadoEvidencia_Data, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV52TFContagemResultadoEvidencia_Data = context.localUtil.CToT( cgiGet( edtavTfcontagemresultadoevidencia_data_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFContagemResultadoEvidencia_Data", context.localUtil.TToC( AV52TFContagemResultadoEvidencia_Data, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontagemresultadoevidencia_data_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFContagem Resultado Evidencia_Data_To"}), 1, "vTFCONTAGEMRESULTADOEVIDENCIA_DATA_TO");
               GX_FocusControl = edtavTfcontagemresultadoevidencia_data_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV53TFContagemResultadoEvidencia_Data_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContagemResultadoEvidencia_Data_To", context.localUtil.TToC( AV53TFContagemResultadoEvidencia_Data_To, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV53TFContagemResultadoEvidencia_Data_To = context.localUtil.CToT( cgiGet( edtavTfcontagemresultadoevidencia_data_to_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContagemResultadoEvidencia_Data_To", context.localUtil.TToC( AV53TFContagemResultadoEvidencia_Data_To, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contagemresultadoevidencia_dataauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contagem Resultado Evidencia_Data Aux Date"}), 1, "vDDO_CONTAGEMRESULTADOEVIDENCIA_DATAAUXDATE");
               GX_FocusControl = edtavDdo_contagemresultadoevidencia_dataauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV54DDO_ContagemResultadoEvidencia_DataAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54DDO_ContagemResultadoEvidencia_DataAuxDate", context.localUtil.Format(AV54DDO_ContagemResultadoEvidencia_DataAuxDate, "99/99/99"));
            }
            else
            {
               AV54DDO_ContagemResultadoEvidencia_DataAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contagemresultadoevidencia_dataauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54DDO_ContagemResultadoEvidencia_DataAuxDate", context.localUtil.Format(AV54DDO_ContagemResultadoEvidencia_DataAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contagemresultadoevidencia_dataauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contagem Resultado Evidencia_Data Aux Date To"}), 1, "vDDO_CONTAGEMRESULTADOEVIDENCIA_DATAAUXDATETO");
               GX_FocusControl = edtavDdo_contagemresultadoevidencia_dataauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV55DDO_ContagemResultadoEvidencia_DataAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55DDO_ContagemResultadoEvidencia_DataAuxDateTo", context.localUtil.Format(AV55DDO_ContagemResultadoEvidencia_DataAuxDateTo, "99/99/99"));
            }
            else
            {
               AV55DDO_ContagemResultadoEvidencia_DataAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contagemresultadoevidencia_dataauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55DDO_ContagemResultadoEvidencia_DataAuxDateTo", context.localUtil.Format(AV55DDO_ContagemResultadoEvidencia_DataAuxDateTo, "99/99/99"));
            }
            AV38ddo_ContagemResultado_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultado_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ddo_ContagemResultado_CodigoTitleControlIdToReplace", AV38ddo_ContagemResultado_CodigoTitleControlIdToReplace);
            AV42ddo_ContagemResultadoEvidencia_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultadoevidencia_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ddo_ContagemResultadoEvidencia_CodigoTitleControlIdToReplace", AV42ddo_ContagemResultadoEvidencia_CodigoTitleControlIdToReplace);
            AV46ddo_ContagemResultadoEvidencia_NomeArqTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultadoevidencia_nomearqtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ddo_ContagemResultadoEvidencia_NomeArqTitleControlIdToReplace", AV46ddo_ContagemResultadoEvidencia_NomeArqTitleControlIdToReplace);
            AV50ddo_ContagemResultadoEvidencia_TipoArqTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultadoevidencia_tipoarqtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ddo_ContagemResultadoEvidencia_TipoArqTitleControlIdToReplace", AV50ddo_ContagemResultadoEvidencia_TipoArqTitleControlIdToReplace);
            AV56ddo_ContagemResultadoEvidencia_DataTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultadoevidencia_datatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ddo_ContagemResultadoEvidencia_DataTitleControlIdToReplace", AV56ddo_ContagemResultadoEvidencia_DataTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_65 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_65"), ",", "."));
            AV59GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV60GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_contagemresultado_codigo_Caption = cgiGet( "DDO_CONTAGEMRESULTADO_CODIGO_Caption");
            Ddo_contagemresultado_codigo_Tooltip = cgiGet( "DDO_CONTAGEMRESULTADO_CODIGO_Tooltip");
            Ddo_contagemresultado_codigo_Cls = cgiGet( "DDO_CONTAGEMRESULTADO_CODIGO_Cls");
            Ddo_contagemresultado_codigo_Filteredtext_set = cgiGet( "DDO_CONTAGEMRESULTADO_CODIGO_Filteredtext_set");
            Ddo_contagemresultado_codigo_Filteredtextto_set = cgiGet( "DDO_CONTAGEMRESULTADO_CODIGO_Filteredtextto_set");
            Ddo_contagemresultado_codigo_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMRESULTADO_CODIGO_Dropdownoptionstype");
            Ddo_contagemresultado_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMRESULTADO_CODIGO_Titlecontrolidtoreplace");
            Ddo_contagemresultado_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_CODIGO_Includesortasc"));
            Ddo_contagemresultado_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_CODIGO_Includesortdsc"));
            Ddo_contagemresultado_codigo_Sortedstatus = cgiGet( "DDO_CONTAGEMRESULTADO_CODIGO_Sortedstatus");
            Ddo_contagemresultado_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_CODIGO_Includefilter"));
            Ddo_contagemresultado_codigo_Filtertype = cgiGet( "DDO_CONTAGEMRESULTADO_CODIGO_Filtertype");
            Ddo_contagemresultado_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_CODIGO_Filterisrange"));
            Ddo_contagemresultado_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADO_CODIGO_Includedatalist"));
            Ddo_contagemresultado_codigo_Sortasc = cgiGet( "DDO_CONTAGEMRESULTADO_CODIGO_Sortasc");
            Ddo_contagemresultado_codigo_Sortdsc = cgiGet( "DDO_CONTAGEMRESULTADO_CODIGO_Sortdsc");
            Ddo_contagemresultado_codigo_Cleanfilter = cgiGet( "DDO_CONTAGEMRESULTADO_CODIGO_Cleanfilter");
            Ddo_contagemresultado_codigo_Rangefilterfrom = cgiGet( "DDO_CONTAGEMRESULTADO_CODIGO_Rangefilterfrom");
            Ddo_contagemresultado_codigo_Rangefilterto = cgiGet( "DDO_CONTAGEMRESULTADO_CODIGO_Rangefilterto");
            Ddo_contagemresultado_codigo_Searchbuttontext = cgiGet( "DDO_CONTAGEMRESULTADO_CODIGO_Searchbuttontext");
            Ddo_contagemresultadoevidencia_codigo_Caption = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO_Caption");
            Ddo_contagemresultadoevidencia_codigo_Tooltip = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO_Tooltip");
            Ddo_contagemresultadoevidencia_codigo_Cls = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO_Cls");
            Ddo_contagemresultadoevidencia_codigo_Filteredtext_set = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO_Filteredtext_set");
            Ddo_contagemresultadoevidencia_codigo_Filteredtextto_set = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO_Filteredtextto_set");
            Ddo_contagemresultadoevidencia_codigo_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO_Dropdownoptionstype");
            Ddo_contagemresultadoevidencia_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO_Titlecontrolidtoreplace");
            Ddo_contagemresultadoevidencia_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO_Includesortasc"));
            Ddo_contagemresultadoevidencia_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO_Includesortdsc"));
            Ddo_contagemresultadoevidencia_codigo_Sortedstatus = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO_Sortedstatus");
            Ddo_contagemresultadoevidencia_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO_Includefilter"));
            Ddo_contagemresultadoevidencia_codigo_Filtertype = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO_Filtertype");
            Ddo_contagemresultadoevidencia_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO_Filterisrange"));
            Ddo_contagemresultadoevidencia_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO_Includedatalist"));
            Ddo_contagemresultadoevidencia_codigo_Sortasc = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO_Sortasc");
            Ddo_contagemresultadoevidencia_codigo_Sortdsc = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO_Sortdsc");
            Ddo_contagemresultadoevidencia_codigo_Cleanfilter = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO_Cleanfilter");
            Ddo_contagemresultadoevidencia_codigo_Rangefilterfrom = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO_Rangefilterfrom");
            Ddo_contagemresultadoevidencia_codigo_Rangefilterto = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO_Rangefilterto");
            Ddo_contagemresultadoevidencia_codigo_Searchbuttontext = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO_Searchbuttontext");
            Ddo_contagemresultadoevidencia_nomearq_Caption = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Caption");
            Ddo_contagemresultadoevidencia_nomearq_Tooltip = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Tooltip");
            Ddo_contagemresultadoevidencia_nomearq_Cls = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Cls");
            Ddo_contagemresultadoevidencia_nomearq_Filteredtext_set = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Filteredtext_set");
            Ddo_contagemresultadoevidencia_nomearq_Selectedvalue_set = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Selectedvalue_set");
            Ddo_contagemresultadoevidencia_nomearq_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Dropdownoptionstype");
            Ddo_contagemresultadoevidencia_nomearq_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Titlecontrolidtoreplace");
            Ddo_contagemresultadoevidencia_nomearq_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Includesortasc"));
            Ddo_contagemresultadoevidencia_nomearq_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Includesortdsc"));
            Ddo_contagemresultadoevidencia_nomearq_Sortedstatus = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Sortedstatus");
            Ddo_contagemresultadoevidencia_nomearq_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Includefilter"));
            Ddo_contagemresultadoevidencia_nomearq_Filtertype = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Filtertype");
            Ddo_contagemresultadoevidencia_nomearq_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Filterisrange"));
            Ddo_contagemresultadoevidencia_nomearq_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Includedatalist"));
            Ddo_contagemresultadoevidencia_nomearq_Datalisttype = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Datalisttype");
            Ddo_contagemresultadoevidencia_nomearq_Datalistproc = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Datalistproc");
            Ddo_contagemresultadoevidencia_nomearq_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contagemresultadoevidencia_nomearq_Sortasc = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Sortasc");
            Ddo_contagemresultadoevidencia_nomearq_Sortdsc = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Sortdsc");
            Ddo_contagemresultadoevidencia_nomearq_Loadingdata = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Loadingdata");
            Ddo_contagemresultadoevidencia_nomearq_Cleanfilter = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Cleanfilter");
            Ddo_contagemresultadoevidencia_nomearq_Noresultsfound = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Noresultsfound");
            Ddo_contagemresultadoevidencia_nomearq_Searchbuttontext = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Searchbuttontext");
            Ddo_contagemresultadoevidencia_tipoarq_Caption = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Caption");
            Ddo_contagemresultadoevidencia_tipoarq_Tooltip = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Tooltip");
            Ddo_contagemresultadoevidencia_tipoarq_Cls = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Cls");
            Ddo_contagemresultadoevidencia_tipoarq_Filteredtext_set = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Filteredtext_set");
            Ddo_contagemresultadoevidencia_tipoarq_Selectedvalue_set = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Selectedvalue_set");
            Ddo_contagemresultadoevidencia_tipoarq_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Dropdownoptionstype");
            Ddo_contagemresultadoevidencia_tipoarq_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Titlecontrolidtoreplace");
            Ddo_contagemresultadoevidencia_tipoarq_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Includesortasc"));
            Ddo_contagemresultadoevidencia_tipoarq_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Includesortdsc"));
            Ddo_contagemresultadoevidencia_tipoarq_Sortedstatus = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Sortedstatus");
            Ddo_contagemresultadoevidencia_tipoarq_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Includefilter"));
            Ddo_contagemresultadoevidencia_tipoarq_Filtertype = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Filtertype");
            Ddo_contagemresultadoevidencia_tipoarq_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Filterisrange"));
            Ddo_contagemresultadoevidencia_tipoarq_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Includedatalist"));
            Ddo_contagemresultadoevidencia_tipoarq_Datalisttype = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Datalisttype");
            Ddo_contagemresultadoevidencia_tipoarq_Datalistproc = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Datalistproc");
            Ddo_contagemresultadoevidencia_tipoarq_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contagemresultadoevidencia_tipoarq_Sortasc = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Sortasc");
            Ddo_contagemresultadoevidencia_tipoarq_Sortdsc = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Sortdsc");
            Ddo_contagemresultadoevidencia_tipoarq_Loadingdata = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Loadingdata");
            Ddo_contagemresultadoevidencia_tipoarq_Cleanfilter = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Cleanfilter");
            Ddo_contagemresultadoevidencia_tipoarq_Noresultsfound = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Noresultsfound");
            Ddo_contagemresultadoevidencia_tipoarq_Searchbuttontext = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Searchbuttontext");
            Ddo_contagemresultadoevidencia_data_Caption = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA_Caption");
            Ddo_contagemresultadoevidencia_data_Tooltip = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA_Tooltip");
            Ddo_contagemresultadoevidencia_data_Cls = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA_Cls");
            Ddo_contagemresultadoevidencia_data_Filteredtext_set = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA_Filteredtext_set");
            Ddo_contagemresultadoevidencia_data_Filteredtextto_set = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA_Filteredtextto_set");
            Ddo_contagemresultadoevidencia_data_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA_Dropdownoptionstype");
            Ddo_contagemresultadoevidencia_data_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA_Titlecontrolidtoreplace");
            Ddo_contagemresultadoevidencia_data_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA_Includesortasc"));
            Ddo_contagemresultadoevidencia_data_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA_Includesortdsc"));
            Ddo_contagemresultadoevidencia_data_Sortedstatus = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA_Sortedstatus");
            Ddo_contagemresultadoevidencia_data_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA_Includefilter"));
            Ddo_contagemresultadoevidencia_data_Filtertype = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA_Filtertype");
            Ddo_contagemresultadoevidencia_data_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA_Filterisrange"));
            Ddo_contagemresultadoevidencia_data_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA_Includedatalist"));
            Ddo_contagemresultadoevidencia_data_Sortasc = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA_Sortasc");
            Ddo_contagemresultadoevidencia_data_Sortdsc = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA_Sortdsc");
            Ddo_contagemresultadoevidencia_data_Cleanfilter = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA_Cleanfilter");
            Ddo_contagemresultadoevidencia_data_Rangefilterfrom = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA_Rangefilterfrom");
            Ddo_contagemresultadoevidencia_data_Rangefilterto = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA_Rangefilterto");
            Ddo_contagemresultadoevidencia_data_Searchbuttontext = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contagemresultado_codigo_Activeeventkey = cgiGet( "DDO_CONTAGEMRESULTADO_CODIGO_Activeeventkey");
            Ddo_contagemresultado_codigo_Filteredtext_get = cgiGet( "DDO_CONTAGEMRESULTADO_CODIGO_Filteredtext_get");
            Ddo_contagemresultado_codigo_Filteredtextto_get = cgiGet( "DDO_CONTAGEMRESULTADO_CODIGO_Filteredtextto_get");
            Ddo_contagemresultadoevidencia_codigo_Activeeventkey = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO_Activeeventkey");
            Ddo_contagemresultadoevidencia_codigo_Filteredtext_get = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO_Filteredtext_get");
            Ddo_contagemresultadoevidencia_codigo_Filteredtextto_get = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO_Filteredtextto_get");
            Ddo_contagemresultadoevidencia_nomearq_Activeeventkey = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Activeeventkey");
            Ddo_contagemresultadoevidencia_nomearq_Filteredtext_get = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Filteredtext_get");
            Ddo_contagemresultadoevidencia_nomearq_Selectedvalue_get = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_Selectedvalue_get");
            Ddo_contagemresultadoevidencia_tipoarq_Activeeventkey = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Activeeventkey");
            Ddo_contagemresultadoevidencia_tipoarq_Filteredtext_get = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Filteredtext_get");
            Ddo_contagemresultadoevidencia_tipoarq_Selectedvalue_get = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_Selectedvalue_get");
            Ddo_contagemresultadoevidencia_data_Activeeventkey = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA_Activeeventkey");
            Ddo_contagemresultadoevidencia_data_Filteredtext_get = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA_Filteredtext_get");
            Ddo_contagemresultadoevidencia_data_Filteredtextto_get = cgiGet( "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV15OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ1"), AV32ContagemResultadoEvidencia_NomeArq1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ2"), AV33ContagemResultadoEvidencia_NomeArq2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV24DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ3"), AV34ContagemResultadoEvidencia_NomeArq3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV23DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADO_CODIGO"), ",", ".") != Convert.ToDecimal( AV36TFContagemResultado_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV37TFContagemResultado_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADOEVIDENCIA_CODIGO"), ",", ".") != Convert.ToDecimal( AV40TFContagemResultadoEvidencia_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADOEVIDENCIA_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV41TFContagemResultadoEvidencia_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADOEVIDENCIA_NOMEARQ"), AV44TFContagemResultadoEvidencia_NomeArq) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADOEVIDENCIA_NOMEARQ_SEL"), AV45TFContagemResultadoEvidencia_NomeArq_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADOEVIDENCIA_TIPOARQ"), AV48TFContagemResultadoEvidencia_TipoArq) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTAGEMRESULTADOEVIDENCIA_TIPOARQ_SEL"), AV49TFContagemResultadoEvidencia_TipoArq_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADOEVIDENCIA_DATA"), 0) != AV52TFContagemResultadoEvidencia_Data )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADOEVIDENCIA_DATA_TO"), 0) != AV53TFContagemResultadoEvidencia_Data_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E27BY2 */
         E27BY2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E27BY2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV16DynamicFiltersSelector1 = "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV20DynamicFiltersSelector2 = "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV24DynamicFiltersSelector3 = "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfcontagemresultado_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_codigo_Visible), 5, 0)));
         edtavTfcontagemresultado_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultado_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultado_codigo_to_Visible), 5, 0)));
         edtavTfcontagemresultadoevidencia_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultadoevidencia_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadoevidencia_codigo_Visible), 5, 0)));
         edtavTfcontagemresultadoevidencia_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultadoevidencia_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadoevidencia_codigo_to_Visible), 5, 0)));
         edtavTfcontagemresultadoevidencia_nomearq_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultadoevidencia_nomearq_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadoevidencia_nomearq_Visible), 5, 0)));
         edtavTfcontagemresultadoevidencia_nomearq_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultadoevidencia_nomearq_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadoevidencia_nomearq_sel_Visible), 5, 0)));
         edtavTfcontagemresultadoevidencia_tipoarq_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultadoevidencia_tipoarq_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadoevidencia_tipoarq_Visible), 5, 0)));
         edtavTfcontagemresultadoevidencia_tipoarq_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultadoevidencia_tipoarq_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadoevidencia_tipoarq_sel_Visible), 5, 0)));
         edtavTfcontagemresultadoevidencia_data_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultadoevidencia_data_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadoevidencia_data_Visible), 5, 0)));
         edtavTfcontagemresultadoevidencia_data_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultadoevidencia_data_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadoevidencia_data_to_Visible), 5, 0)));
         Ddo_contagemresultado_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultado_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_codigo_Internalname, "TitleControlIdToReplace", Ddo_contagemresultado_codigo_Titlecontrolidtoreplace);
         AV38ddo_ContagemResultado_CodigoTitleControlIdToReplace = Ddo_contagemresultado_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ddo_ContagemResultado_CodigoTitleControlIdToReplace", AV38ddo_ContagemResultado_CodigoTitleControlIdToReplace);
         edtavDdo_contagemresultado_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemresultado_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultado_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultadoevidencia_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultadoEvidencia_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoevidencia_codigo_Internalname, "TitleControlIdToReplace", Ddo_contagemresultadoevidencia_codigo_Titlecontrolidtoreplace);
         AV42ddo_ContagemResultadoEvidencia_CodigoTitleControlIdToReplace = Ddo_contagemresultadoevidencia_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ddo_ContagemResultadoEvidencia_CodigoTitleControlIdToReplace", AV42ddo_ContagemResultadoEvidencia_CodigoTitleControlIdToReplace);
         edtavDdo_contagemresultadoevidencia_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemresultadoevidencia_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultadoevidencia_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultadoevidencia_nomearq_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultadoEvidencia_NomeArq";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoevidencia_nomearq_Internalname, "TitleControlIdToReplace", Ddo_contagemresultadoevidencia_nomearq_Titlecontrolidtoreplace);
         AV46ddo_ContagemResultadoEvidencia_NomeArqTitleControlIdToReplace = Ddo_contagemresultadoevidencia_nomearq_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ddo_ContagemResultadoEvidencia_NomeArqTitleControlIdToReplace", AV46ddo_ContagemResultadoEvidencia_NomeArqTitleControlIdToReplace);
         edtavDdo_contagemresultadoevidencia_nomearqtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemresultadoevidencia_nomearqtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultadoevidencia_nomearqtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultadoevidencia_tipoarq_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultadoEvidencia_TipoArq";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoevidencia_tipoarq_Internalname, "TitleControlIdToReplace", Ddo_contagemresultadoevidencia_tipoarq_Titlecontrolidtoreplace);
         AV50ddo_ContagemResultadoEvidencia_TipoArqTitleControlIdToReplace = Ddo_contagemresultadoevidencia_tipoarq_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ddo_ContagemResultadoEvidencia_TipoArqTitleControlIdToReplace", AV50ddo_ContagemResultadoEvidencia_TipoArqTitleControlIdToReplace);
         edtavDdo_contagemresultadoevidencia_tipoarqtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemresultadoevidencia_tipoarqtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultadoevidencia_tipoarqtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultadoevidencia_data_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultadoEvidencia_Data";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoevidencia_data_Internalname, "TitleControlIdToReplace", Ddo_contagemresultadoevidencia_data_Titlecontrolidtoreplace);
         AV56ddo_ContagemResultadoEvidencia_DataTitleControlIdToReplace = Ddo_contagemresultadoevidencia_data_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ddo_ContagemResultadoEvidencia_DataTitleControlIdToReplace", AV56ddo_ContagemResultadoEvidencia_DataTitleControlIdToReplace);
         edtavDdo_contagemresultadoevidencia_datatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemresultadoevidencia_datatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultadoevidencia_datatitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Selecione Contagem Resultado Evidencias";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Nome do Arquivo", 0);
         cmbavOrderedby.addItem("2", "Contagem Resultado_Codigo", 0);
         cmbavOrderedby.addItem("3", "Evidencia", 0);
         cmbavOrderedby.addItem("4", "Tipo", 0);
         cmbavOrderedby.addItem("5", "Upload", 0);
         if ( AV14OrderedBy < 1 )
         {
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV57DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV57DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E28BY2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV35ContagemResultado_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV39ContagemResultadoEvidencia_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV43ContagemResultadoEvidencia_NomeArqTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV47ContagemResultadoEvidencia_TipoArqTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV51ContagemResultadoEvidencia_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContagemResultado_Codigo_Titleformat = 2;
         edtContagemResultado_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Contagem Resultado_Codigo", AV38ddo_ContagemResultado_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_Codigo_Internalname, "Title", edtContagemResultado_Codigo_Title);
         edtContagemResultadoEvidencia_Codigo_Titleformat = 2;
         edtContagemResultadoEvidencia_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Evidencia", AV42ddo_ContagemResultadoEvidencia_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoEvidencia_Codigo_Internalname, "Title", edtContagemResultadoEvidencia_Codigo_Title);
         edtContagemResultadoEvidencia_NomeArq_Titleformat = 2;
         edtContagemResultadoEvidencia_NomeArq_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "do Arquivo", AV46ddo_ContagemResultadoEvidencia_NomeArqTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoEvidencia_NomeArq_Internalname, "Title", edtContagemResultadoEvidencia_NomeArq_Title);
         edtContagemResultadoEvidencia_TipoArq_Titleformat = 2;
         edtContagemResultadoEvidencia_TipoArq_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Tipo", AV50ddo_ContagemResultadoEvidencia_TipoArqTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoEvidencia_TipoArq_Internalname, "Title", edtContagemResultadoEvidencia_TipoArq_Title);
         edtContagemResultadoEvidencia_Data_Titleformat = 2;
         edtContagemResultadoEvidencia_Data_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Upload", AV56ddo_ContagemResultadoEvidencia_DataTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoEvidencia_Data_Internalname, "Title", edtContagemResultadoEvidencia_Data_Title);
         AV59GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59GridCurrentPage), 10, 0)));
         AV60GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV35ContagemResultado_CodigoTitleFilterData", AV35ContagemResultado_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV39ContagemResultadoEvidencia_CodigoTitleFilterData", AV39ContagemResultadoEvidencia_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV43ContagemResultadoEvidencia_NomeArqTitleFilterData", AV43ContagemResultadoEvidencia_NomeArqTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV47ContagemResultadoEvidencia_TipoArqTitleFilterData", AV47ContagemResultadoEvidencia_TipoArqTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV51ContagemResultadoEvidencia_DataTitleFilterData", AV51ContagemResultadoEvidencia_DataTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11GridState", AV11GridState);
      }

      protected void E11BY2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV58PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV58PageToGo) ;
         }
      }

      protected void E12BY2( )
      {
         /* Ddo_contagemresultado_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultado_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contagemresultado_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_codigo_Internalname, "SortedStatus", Ddo_contagemresultado_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultado_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contagemresultado_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_codigo_Internalname, "SortedStatus", Ddo_contagemresultado_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultado_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV36TFContagemResultado_Codigo = (int)(NumberUtil.Val( Ddo_contagemresultado_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFContagemResultado_Codigo), 6, 0)));
            AV37TFContagemResultado_Codigo_To = (int)(NumberUtil.Val( Ddo_contagemresultado_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFContagemResultado_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37TFContagemResultado_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13BY2( )
      {
         /* Ddo_contagemresultadoevidencia_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultadoevidencia_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contagemresultadoevidencia_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoevidencia_codigo_Internalname, "SortedStatus", Ddo_contagemresultadoevidencia_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultadoevidencia_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contagemresultadoevidencia_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoevidencia_codigo_Internalname, "SortedStatus", Ddo_contagemresultadoevidencia_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultadoevidencia_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV40TFContagemResultadoEvidencia_Codigo = (int)(NumberUtil.Val( Ddo_contagemresultadoevidencia_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContagemResultadoEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40TFContagemResultadoEvidencia_Codigo), 6, 0)));
            AV41TFContagemResultadoEvidencia_Codigo_To = (int)(NumberUtil.Val( Ddo_contagemresultadoevidencia_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFContagemResultadoEvidencia_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41TFContagemResultadoEvidencia_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14BY2( )
      {
         /* Ddo_contagemresultadoevidencia_nomearq_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultadoevidencia_nomearq_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contagemresultadoevidencia_nomearq_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoevidencia_nomearq_Internalname, "SortedStatus", Ddo_contagemresultadoevidencia_nomearq_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultadoevidencia_nomearq_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contagemresultadoevidencia_nomearq_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoevidencia_nomearq_Internalname, "SortedStatus", Ddo_contagemresultadoevidencia_nomearq_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultadoevidencia_nomearq_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV44TFContagemResultadoEvidencia_NomeArq = Ddo_contagemresultadoevidencia_nomearq_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContagemResultadoEvidencia_NomeArq", AV44TFContagemResultadoEvidencia_NomeArq);
            AV45TFContagemResultadoEvidencia_NomeArq_Sel = Ddo_contagemresultadoevidencia_nomearq_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContagemResultadoEvidencia_NomeArq_Sel", AV45TFContagemResultadoEvidencia_NomeArq_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E15BY2( )
      {
         /* Ddo_contagemresultadoevidencia_tipoarq_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultadoevidencia_tipoarq_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contagemresultadoevidencia_tipoarq_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoevidencia_tipoarq_Internalname, "SortedStatus", Ddo_contagemresultadoevidencia_tipoarq_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultadoevidencia_tipoarq_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contagemresultadoevidencia_tipoarq_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoevidencia_tipoarq_Internalname, "SortedStatus", Ddo_contagemresultadoevidencia_tipoarq_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultadoevidencia_tipoarq_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV48TFContagemResultadoEvidencia_TipoArq = Ddo_contagemresultadoevidencia_tipoarq_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFContagemResultadoEvidencia_TipoArq", AV48TFContagemResultadoEvidencia_TipoArq);
            AV49TFContagemResultadoEvidencia_TipoArq_Sel = Ddo_contagemresultadoevidencia_tipoarq_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContagemResultadoEvidencia_TipoArq_Sel", AV49TFContagemResultadoEvidencia_TipoArq_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E16BY2( )
      {
         /* Ddo_contagemresultadoevidencia_data_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultadoevidencia_data_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contagemresultadoevidencia_data_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoevidencia_data_Internalname, "SortedStatus", Ddo_contagemresultadoevidencia_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultadoevidencia_data_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contagemresultadoevidencia_data_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoevidencia_data_Internalname, "SortedStatus", Ddo_contagemresultadoevidencia_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultadoevidencia_data_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV52TFContagemResultadoEvidencia_Data = context.localUtil.CToT( Ddo_contagemresultadoevidencia_data_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFContagemResultadoEvidencia_Data", context.localUtil.TToC( AV52TFContagemResultadoEvidencia_Data, 8, 5, 0, 3, "/", ":", " "));
            AV53TFContagemResultadoEvidencia_Data_To = context.localUtil.CToT( Ddo_contagemresultadoevidencia_data_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContagemResultadoEvidencia_Data_To", context.localUtil.TToC( AV53TFContagemResultadoEvidencia_Data_To, 8, 5, 0, 3, "/", ":", " "));
            if ( ! (DateTime.MinValue==AV53TFContagemResultadoEvidencia_Data_To) )
            {
               AV53TFContagemResultadoEvidencia_Data_To = context.localUtil.YMDHMSToT( (short)(DateTimeUtil.Year( AV53TFContagemResultadoEvidencia_Data_To)), (short)(DateTimeUtil.Month( AV53TFContagemResultadoEvidencia_Data_To)), (short)(DateTimeUtil.Day( AV53TFContagemResultadoEvidencia_Data_To)), 23, 59, 59);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContagemResultadoEvidencia_Data_To", context.localUtil.TToC( AV53TFContagemResultadoEvidencia_Data_To, 8, 5, 0, 3, "/", ":", " "));
            }
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E29BY2( )
      {
         /* Grid_Load Routine */
         AV29Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV29Select);
         AV63Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 65;
         }
         sendrow_652( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_65_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(65, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E30BY2 */
         E30BY2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E30BY2( )
      {
         /* Enter Routine */
         AV8InOutContagemResultadoEvidencia_Codigo = A586ContagemResultadoEvidencia_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContagemResultadoEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8InOutContagemResultadoEvidencia_Codigo), 6, 0)));
         AV31InOutContagemResultadoEvidencia_NomeArq = A589ContagemResultadoEvidencia_NomeArq;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31InOutContagemResultadoEvidencia_NomeArq", AV31InOutContagemResultadoEvidencia_NomeArq);
         context.setWebReturnParms(new Object[] {(int)AV8InOutContagemResultadoEvidencia_Codigo,(String)AV31InOutContagemResultadoEvidencia_NomeArq});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E17BY2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E22BY2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV19DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E18BY2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV27DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         AV28DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersIgnoreFirst", AV28DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV27DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         AV28DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersIgnoreFirst", AV28DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV32ContagemResultadoEvidencia_NomeArq1, AV20DynamicFiltersSelector2, AV33ContagemResultadoEvidencia_NomeArq2, AV24DynamicFiltersSelector3, AV34ContagemResultadoEvidencia_NomeArq3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV36TFContagemResultado_Codigo, AV37TFContagemResultado_Codigo_To, AV40TFContagemResultadoEvidencia_Codigo, AV41TFContagemResultadoEvidencia_Codigo_To, AV44TFContagemResultadoEvidencia_NomeArq, AV45TFContagemResultadoEvidencia_NomeArq_Sel, AV48TFContagemResultadoEvidencia_TipoArq, AV49TFContagemResultadoEvidencia_TipoArq_Sel, AV52TFContagemResultadoEvidencia_Data, AV53TFContagemResultadoEvidencia_Data_To, AV38ddo_ContagemResultado_CodigoTitleControlIdToReplace, AV42ddo_ContagemResultadoEvidencia_CodigoTitleControlIdToReplace, AV46ddo_ContagemResultadoEvidencia_NomeArqTitleControlIdToReplace, AV50ddo_ContagemResultadoEvidencia_TipoArqTitleControlIdToReplace, AV56ddo_ContagemResultadoEvidencia_DataTitleControlIdToReplace, AV64Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E23BY2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E24BY2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV23DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E19BY2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV27DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV27DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV32ContagemResultadoEvidencia_NomeArq1, AV20DynamicFiltersSelector2, AV33ContagemResultadoEvidencia_NomeArq2, AV24DynamicFiltersSelector3, AV34ContagemResultadoEvidencia_NomeArq3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV36TFContagemResultado_Codigo, AV37TFContagemResultado_Codigo_To, AV40TFContagemResultadoEvidencia_Codigo, AV41TFContagemResultadoEvidencia_Codigo_To, AV44TFContagemResultadoEvidencia_NomeArq, AV45TFContagemResultadoEvidencia_NomeArq_Sel, AV48TFContagemResultadoEvidencia_TipoArq, AV49TFContagemResultadoEvidencia_TipoArq_Sel, AV52TFContagemResultadoEvidencia_Data, AV53TFContagemResultadoEvidencia_Data_To, AV38ddo_ContagemResultado_CodigoTitleControlIdToReplace, AV42ddo_ContagemResultadoEvidencia_CodigoTitleControlIdToReplace, AV46ddo_ContagemResultadoEvidencia_NomeArqTitleControlIdToReplace, AV50ddo_ContagemResultadoEvidencia_TipoArqTitleControlIdToReplace, AV56ddo_ContagemResultadoEvidencia_DataTitleControlIdToReplace, AV64Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E25BY2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E20BY2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV27DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         AV23DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV27DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersRemoving", AV27DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV32ContagemResultadoEvidencia_NomeArq1, AV20DynamicFiltersSelector2, AV33ContagemResultadoEvidencia_NomeArq2, AV24DynamicFiltersSelector3, AV34ContagemResultadoEvidencia_NomeArq3, AV19DynamicFiltersEnabled2, AV23DynamicFiltersEnabled3, AV36TFContagemResultado_Codigo, AV37TFContagemResultado_Codigo_To, AV40TFContagemResultadoEvidencia_Codigo, AV41TFContagemResultadoEvidencia_Codigo_To, AV44TFContagemResultadoEvidencia_NomeArq, AV45TFContagemResultadoEvidencia_NomeArq_Sel, AV48TFContagemResultadoEvidencia_TipoArq, AV49TFContagemResultadoEvidencia_TipoArq_Sel, AV52TFContagemResultadoEvidencia_Data, AV53TFContagemResultadoEvidencia_Data_To, AV38ddo_ContagemResultado_CodigoTitleControlIdToReplace, AV42ddo_ContagemResultadoEvidencia_CodigoTitleControlIdToReplace, AV46ddo_ContagemResultadoEvidencia_NomeArqTitleControlIdToReplace, AV50ddo_ContagemResultadoEvidencia_TipoArqTitleControlIdToReplace, AV56ddo_ContagemResultadoEvidencia_DataTitleControlIdToReplace, AV64Pgmname, AV11GridState, AV28DynamicFiltersIgnoreFirst, AV27DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E26BY2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E21BY2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contagemresultado_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_codigo_Internalname, "SortedStatus", Ddo_contagemresultado_codigo_Sortedstatus);
         Ddo_contagemresultadoevidencia_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoevidencia_codigo_Internalname, "SortedStatus", Ddo_contagemresultadoevidencia_codigo_Sortedstatus);
         Ddo_contagemresultadoevidencia_nomearq_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoevidencia_nomearq_Internalname, "SortedStatus", Ddo_contagemresultadoevidencia_nomearq_Sortedstatus);
         Ddo_contagemresultadoevidencia_tipoarq_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoevidencia_tipoarq_Internalname, "SortedStatus", Ddo_contagemresultadoevidencia_tipoarq_Sortedstatus);
         Ddo_contagemresultadoevidencia_data_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoevidencia_data_Internalname, "SortedStatus", Ddo_contagemresultadoevidencia_data_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV14OrderedBy == 2 )
         {
            Ddo_contagemresultado_codigo_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_codigo_Internalname, "SortedStatus", Ddo_contagemresultado_codigo_Sortedstatus);
         }
         else if ( AV14OrderedBy == 3 )
         {
            Ddo_contagemresultadoevidencia_codigo_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoevidencia_codigo_Internalname, "SortedStatus", Ddo_contagemresultadoevidencia_codigo_Sortedstatus);
         }
         else if ( AV14OrderedBy == 1 )
         {
            Ddo_contagemresultadoevidencia_nomearq_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoevidencia_nomearq_Internalname, "SortedStatus", Ddo_contagemresultadoevidencia_nomearq_Sortedstatus);
         }
         else if ( AV14OrderedBy == 4 )
         {
            Ddo_contagemresultadoevidencia_tipoarq_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoevidencia_tipoarq_Internalname, "SortedStatus", Ddo_contagemresultadoevidencia_tipoarq_Sortedstatus);
         }
         else if ( AV14OrderedBy == 5 )
         {
            Ddo_contagemresultadoevidencia_data_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoevidencia_data_Internalname, "SortedStatus", Ddo_contagemresultadoevidencia_data_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavContagemresultadoevidencia_nomearq1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultadoevidencia_nomearq1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadoevidencia_nomearq1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ") == 0 )
         {
            edtavContagemresultadoevidencia_nomearq1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultadoevidencia_nomearq1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadoevidencia_nomearq1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavContagemresultadoevidencia_nomearq2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultadoevidencia_nomearq2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadoevidencia_nomearq2_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ") == 0 )
         {
            edtavContagemresultadoevidencia_nomearq2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultadoevidencia_nomearq2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadoevidencia_nomearq2_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavContagemresultadoevidencia_nomearq3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultadoevidencia_nomearq3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadoevidencia_nomearq3_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ") == 0 )
         {
            edtavContagemresultadoevidencia_nomearq3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultadoevidencia_nomearq3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultadoevidencia_nomearq3_Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         AV20DynamicFiltersSelector2 = "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         AV33ContagemResultadoEvidencia_NomeArq2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContagemResultadoEvidencia_NomeArq2", AV33ContagemResultadoEvidencia_NomeArq2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
         AV24DynamicFiltersSelector3 = "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
         AV34ContagemResultadoEvidencia_NomeArq3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ContagemResultadoEvidencia_NomeArq3", AV34ContagemResultadoEvidencia_NomeArq3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S202( )
      {
         /* 'CLEANFILTERS' Routine */
         AV36TFContagemResultado_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFContagemResultado_Codigo), 6, 0)));
         Ddo_contagemresultado_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_codigo_Internalname, "FilteredText_set", Ddo_contagemresultado_codigo_Filteredtext_set);
         AV37TFContagemResultado_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFContagemResultado_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37TFContagemResultado_Codigo_To), 6, 0)));
         Ddo_contagemresultado_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultado_codigo_Internalname, "FilteredTextTo_set", Ddo_contagemresultado_codigo_Filteredtextto_set);
         AV40TFContagemResultadoEvidencia_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContagemResultadoEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40TFContagemResultadoEvidencia_Codigo), 6, 0)));
         Ddo_contagemresultadoevidencia_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoevidencia_codigo_Internalname, "FilteredText_set", Ddo_contagemresultadoevidencia_codigo_Filteredtext_set);
         AV41TFContagemResultadoEvidencia_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFContagemResultadoEvidencia_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41TFContagemResultadoEvidencia_Codigo_To), 6, 0)));
         Ddo_contagemresultadoevidencia_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoevidencia_codigo_Internalname, "FilteredTextTo_set", Ddo_contagemresultadoevidencia_codigo_Filteredtextto_set);
         AV44TFContagemResultadoEvidencia_NomeArq = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContagemResultadoEvidencia_NomeArq", AV44TFContagemResultadoEvidencia_NomeArq);
         Ddo_contagemresultadoevidencia_nomearq_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoevidencia_nomearq_Internalname, "FilteredText_set", Ddo_contagemresultadoevidencia_nomearq_Filteredtext_set);
         AV45TFContagemResultadoEvidencia_NomeArq_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContagemResultadoEvidencia_NomeArq_Sel", AV45TFContagemResultadoEvidencia_NomeArq_Sel);
         Ddo_contagemresultadoevidencia_nomearq_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoevidencia_nomearq_Internalname, "SelectedValue_set", Ddo_contagemresultadoevidencia_nomearq_Selectedvalue_set);
         AV48TFContagemResultadoEvidencia_TipoArq = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFContagemResultadoEvidencia_TipoArq", AV48TFContagemResultadoEvidencia_TipoArq);
         Ddo_contagemresultadoevidencia_tipoarq_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoevidencia_tipoarq_Internalname, "FilteredText_set", Ddo_contagemresultadoevidencia_tipoarq_Filteredtext_set);
         AV49TFContagemResultadoEvidencia_TipoArq_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContagemResultadoEvidencia_TipoArq_Sel", AV49TFContagemResultadoEvidencia_TipoArq_Sel);
         Ddo_contagemresultadoevidencia_tipoarq_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoevidencia_tipoarq_Internalname, "SelectedValue_set", Ddo_contagemresultadoevidencia_tipoarq_Selectedvalue_set);
         AV52TFContagemResultadoEvidencia_Data = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFContagemResultadoEvidencia_Data", context.localUtil.TToC( AV52TFContagemResultadoEvidencia_Data, 8, 5, 0, 3, "/", ":", " "));
         Ddo_contagemresultadoevidencia_data_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoevidencia_data_Internalname, "FilteredText_set", Ddo_contagemresultadoevidencia_data_Filteredtext_set);
         AV53TFContagemResultadoEvidencia_Data_To = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContagemResultadoEvidencia_Data_To", context.localUtil.TToC( AV53TFContagemResultadoEvidencia_Data_To, 8, 5, 0, 3, "/", ":", " "));
         Ddo_contagemresultadoevidencia_data_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoevidencia_data_Internalname, "FilteredTextTo_set", Ddo_contagemresultadoevidencia_data_Filteredtextto_set);
         AV16DynamicFiltersSelector1 = "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         AV32ContagemResultadoEvidencia_NomeArq1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ContagemResultadoEvidencia_NomeArq1", AV32ContagemResultadoEvidencia_NomeArq1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(1));
            AV16DynamicFiltersSelector1 = AV13GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ") == 0 )
            {
               AV32ContagemResultadoEvidencia_NomeArq1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ContagemResultadoEvidencia_NomeArq1", AV32ContagemResultadoEvidencia_NomeArq1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV19DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(2));
               AV20DynamicFiltersSelector2 = AV13GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ") == 0 )
               {
                  AV33ContagemResultadoEvidencia_NomeArq2 = AV13GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContagemResultadoEvidencia_NomeArq2", AV33ContagemResultadoEvidencia_NomeArq2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV23DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersEnabled3", AV23DynamicFiltersEnabled3);
                  AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(3));
                  AV24DynamicFiltersSelector3 = AV13GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersSelector3", AV24DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ") == 0 )
                  {
                     AV34ContagemResultadoEvidencia_NomeArq3 = AV13GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ContagemResultadoEvidencia_NomeArq3", AV34ContagemResultadoEvidencia_NomeArq3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV27DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV11GridState.gxTpr_Orderedby = AV14OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV15OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (0==AV36TFContagemResultado_Codigo) && (0==AV37TFContagemResultado_Codigo_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADO_CODIGO";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV36TFContagemResultado_Codigo), 6, 0);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV37TFContagemResultado_Codigo_To), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV40TFContagemResultadoEvidencia_Codigo) && (0==AV41TFContagemResultadoEvidencia_Codigo_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADOEVIDENCIA_CODIGO";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV40TFContagemResultadoEvidencia_Codigo), 6, 0);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV41TFContagemResultadoEvidencia_Codigo_To), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TFContagemResultadoEvidencia_NomeArq)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADOEVIDENCIA_NOMEARQ";
            AV12GridStateFilterValue.gxTpr_Value = AV44TFContagemResultadoEvidencia_NomeArq;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45TFContagemResultadoEvidencia_NomeArq_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADOEVIDENCIA_NOMEARQ_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV45TFContagemResultadoEvidencia_NomeArq_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TFContagemResultadoEvidencia_TipoArq)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADOEVIDENCIA_TIPOARQ";
            AV12GridStateFilterValue.gxTpr_Value = AV48TFContagemResultadoEvidencia_TipoArq;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49TFContagemResultadoEvidencia_TipoArq_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADOEVIDENCIA_TIPOARQ_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV49TFContagemResultadoEvidencia_TipoArq_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV52TFContagemResultadoEvidencia_Data) && (DateTime.MinValue==AV53TFContagemResultadoEvidencia_Data_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTAGEMRESULTADOEVIDENCIA_DATA";
            AV12GridStateFilterValue.gxTpr_Value = context.localUtil.TToC( AV52TFContagemResultadoEvidencia_Data, 8, 5, 0, 3, "/", ":", " ");
            AV12GridStateFilterValue.gxTpr_Valueto = context.localUtil.TToC( AV53TFContagemResultadoEvidencia_Data_To, 8, 5, 0, 3, "/", ":", " ");
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV64Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV28DynamicFiltersIgnoreFirst )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV16DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV32ContagemResultadoEvidencia_NomeArq1)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV32ContagemResultadoEvidencia_NomeArq1;
            }
            if ( AV27DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV19DynamicFiltersEnabled2 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV20DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV33ContagemResultadoEvidencia_NomeArq2)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV33ContagemResultadoEvidencia_NomeArq2;
            }
            if ( AV27DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV23DynamicFiltersEnabled3 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV24DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV34ContagemResultadoEvidencia_NomeArq3)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV34ContagemResultadoEvidencia_NomeArq3;
            }
            if ( AV27DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_BY2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_BY2( true) ;
         }
         else
         {
            wb_table2_5_BY2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_BY2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_59_BY2( true) ;
         }
         else
         {
            wb_table3_59_BY2( false) ;
         }
         return  ;
      }

      protected void wb_table3_59_BY2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_BY2e( true) ;
         }
         else
         {
            wb_table1_2_BY2e( false) ;
         }
      }

      protected void wb_table3_59_BY2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_62_BY2( true) ;
         }
         else
         {
            wb_table4_62_BY2( false) ;
         }
         return  ;
      }

      protected void wb_table4_62_BY2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_59_BY2e( true) ;
         }
         else
         {
            wb_table3_59_BY2e( false) ;
         }
      }

      protected void wb_table4_62_BY2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"65\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultado_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultado_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultado_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoEvidencia_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoEvidencia_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoEvidencia_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoEvidencia_NomeArq_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoEvidencia_NomeArq_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoEvidencia_NomeArq_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoEvidencia_TipoArq_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoEvidencia_TipoArq_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoEvidencia_TipoArq_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoEvidencia_Data_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoEvidencia_Data_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoEvidencia_Data_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV29Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultado_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A586ContagemResultadoEvidencia_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoEvidencia_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoEvidencia_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A589ContagemResultadoEvidencia_NomeArq));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoEvidencia_NomeArq_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoEvidencia_NomeArq_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A590ContagemResultadoEvidencia_TipoArq));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoEvidencia_TipoArq_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoEvidencia_TipoArq_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( A591ContagemResultadoEvidencia_Data, 10, 8, 0, 3, "/", ":", " "));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoEvidencia_Data_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoEvidencia_Data_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 65 )
         {
            wbEnd = 0;
            nRC_GXsfl_65 = (short)(nGXsfl_65_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_62_BY2e( true) ;
         }
         else
         {
            wb_table4_62_BY2e( false) ;
         }
      }

      protected void wb_table2_5_BY2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_65_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptContagemResultadoEvidencia.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_65_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV15OrderedDsc), StringUtil.BoolToStr( AV15OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptContagemResultadoEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_14_BY2( true) ;
         }
         else
         {
            wb_table5_14_BY2( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_BY2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_BY2e( true) ;
         }
         else
         {
            wb_table2_5_BY2e( false) ;
         }
      }

      protected void wb_table5_14_BY2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemResultadoEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_19_BY2( true) ;
         }
         else
         {
            wb_table6_19_BY2( false) ;
         }
         return  ;
      }

      protected void wb_table6_19_BY2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptContagemResultadoEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_BY2e( true) ;
         }
         else
         {
            wb_table5_14_BY2e( false) ;
         }
      }

      protected void wb_table6_19_BY2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'" + sGXsfl_65_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV16DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_PromptContagemResultadoEvidencia.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'" + sGXsfl_65_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadoevidencia_nomearq1_Internalname, StringUtil.RTrim( AV32ContagemResultadoEvidencia_NomeArq1), StringUtil.RTrim( context.localUtil.Format( AV32ContagemResultadoEvidencia_NomeArq1, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadoevidencia_nomearq1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultadoevidencia_nomearq1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContagemResultadoEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemResultadoEvidencia.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemResultadoEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'" + sGXsfl_65_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV20DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,36);\"", "", true, "HLP_PromptContagemResultadoEvidencia.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_65_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadoevidencia_nomearq2_Internalname, StringUtil.RTrim( AV33ContagemResultadoEvidencia_NomeArq2), StringUtil.RTrim( context.localUtil.Format( AV33ContagemResultadoEvidencia_NomeArq2, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadoevidencia_nomearq2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultadoevidencia_nomearq2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContagemResultadoEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemResultadoEvidencia.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemResultadoEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'" + sGXsfl_65_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV24DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,48);\"", "", true, "HLP_PromptContagemResultadoEvidencia.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'" + sGXsfl_65_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadoevidencia_nomearq3_Internalname, StringUtil.RTrim( AV34ContagemResultadoEvidencia_NomeArq3), StringUtil.RTrim( context.localUtil.Format( AV34ContagemResultadoEvidencia_NomeArq3, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,52);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadoevidencia_nomearq3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultadoevidencia_nomearq3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContagemResultadoEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemResultadoEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_19_BY2e( true) ;
         }
         else
         {
            wb_table6_19_BY2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV8InOutContagemResultadoEvidencia_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContagemResultadoEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8InOutContagemResultadoEvidencia_Codigo), 6, 0)));
         AV31InOutContagemResultadoEvidencia_NomeArq = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31InOutContagemResultadoEvidencia_NomeArq", AV31InOutContagemResultadoEvidencia_NomeArq);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PABY2( ) ;
         WSBY2( ) ;
         WEBY2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311744459");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptcontagemresultadoevidencia.js", "?2020311744459");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_652( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_65_idx;
         edtContagemResultado_Codigo_Internalname = "CONTAGEMRESULTADO_CODIGO_"+sGXsfl_65_idx;
         edtContagemResultadoEvidencia_Codigo_Internalname = "CONTAGEMRESULTADOEVIDENCIA_CODIGO_"+sGXsfl_65_idx;
         edtContagemResultadoEvidencia_NomeArq_Internalname = "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_"+sGXsfl_65_idx;
         edtContagemResultadoEvidencia_TipoArq_Internalname = "CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_"+sGXsfl_65_idx;
         edtContagemResultadoEvidencia_Data_Internalname = "CONTAGEMRESULTADOEVIDENCIA_DATA_"+sGXsfl_65_idx;
      }

      protected void SubsflControlProps_fel_652( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_65_fel_idx;
         edtContagemResultado_Codigo_Internalname = "CONTAGEMRESULTADO_CODIGO_"+sGXsfl_65_fel_idx;
         edtContagemResultadoEvidencia_Codigo_Internalname = "CONTAGEMRESULTADOEVIDENCIA_CODIGO_"+sGXsfl_65_fel_idx;
         edtContagemResultadoEvidencia_NomeArq_Internalname = "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_"+sGXsfl_65_fel_idx;
         edtContagemResultadoEvidencia_TipoArq_Internalname = "CONTAGEMRESULTADOEVIDENCIA_TIPOARQ_"+sGXsfl_65_fel_idx;
         edtContagemResultadoEvidencia_Data_Internalname = "CONTAGEMRESULTADOEVIDENCIA_DATA_"+sGXsfl_65_fel_idx;
      }

      protected void sendrow_652( )
      {
         SubsflControlProps_652( ) ;
         WBBY0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_65_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_65_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_65_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 66,'',false,'',65)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV29Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV29Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV63Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV29Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV29Select)) ? AV63Select_GXI : context.PathToRelativeUrl( AV29Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_65_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV29Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)65,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoEvidencia_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A586ContagemResultadoEvidencia_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A586ContagemResultadoEvidencia_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoEvidencia_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)65,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoEvidencia_NomeArq_Internalname,StringUtil.RTrim( A589ContagemResultadoEvidencia_NomeArq),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoEvidencia_NomeArq_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)65,(short)1,(short)-1,(short)-1,(bool)true,(String)"NomeArq",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoEvidencia_TipoArq_Internalname,StringUtil.RTrim( A590ContagemResultadoEvidencia_TipoArq),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoEvidencia_TipoArq_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)10,(short)0,(short)0,(short)65,(short)1,(short)-1,(short)-1,(bool)true,(String)"TipoArq",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoEvidencia_Data_Internalname,context.localUtil.TToC( A591ContagemResultadoEvidencia_Data, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A591ContagemResultadoEvidencia_Data, "99/99/99 99:99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoEvidencia_Data_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)65,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_CODIGO"+"_"+sGXsfl_65_idx, GetSecureSignedToken( sGXsfl_65_idx, context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADOEVIDENCIA_CODIGO"+"_"+sGXsfl_65_idx, GetSecureSignedToken( sGXsfl_65_idx, context.localUtil.Format( (decimal)(A586ContagemResultadoEvidencia_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ"+"_"+sGXsfl_65_idx, GetSecureSignedToken( sGXsfl_65_idx, StringUtil.RTrim( context.localUtil.Format( A589ContagemResultadoEvidencia_NomeArq, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ"+"_"+sGXsfl_65_idx, GetSecureSignedToken( sGXsfl_65_idx, StringUtil.RTrim( context.localUtil.Format( A590ContagemResultadoEvidencia_TipoArq, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADOEVIDENCIA_DATA"+"_"+sGXsfl_65_idx, GetSecureSignedToken( sGXsfl_65_idx, context.localUtil.Format( A591ContagemResultadoEvidencia_Data, "99/99/99 99:99")));
            GridContainer.AddRow(GridRow);
            nGXsfl_65_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_65_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_65_idx+1));
            sGXsfl_65_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_65_idx), 4, 0)), 4, "0");
            SubsflControlProps_652( ) ;
         }
         /* End function sendrow_652 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavContagemresultadoevidencia_nomearq1_Internalname = "vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavContagemresultadoevidencia_nomearq2_Internalname = "vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         edtavContagemresultadoevidencia_nomearq3_Internalname = "vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtContagemResultado_Codigo_Internalname = "CONTAGEMRESULTADO_CODIGO";
         edtContagemResultadoEvidencia_Codigo_Internalname = "CONTAGEMRESULTADOEVIDENCIA_CODIGO";
         edtContagemResultadoEvidencia_NomeArq_Internalname = "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ";
         edtContagemResultadoEvidencia_TipoArq_Internalname = "CONTAGEMRESULTADOEVIDENCIA_TIPOARQ";
         edtContagemResultadoEvidencia_Data_Internalname = "CONTAGEMRESULTADOEVIDENCIA_DATA";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfcontagemresultado_codigo_Internalname = "vTFCONTAGEMRESULTADO_CODIGO";
         edtavTfcontagemresultado_codigo_to_Internalname = "vTFCONTAGEMRESULTADO_CODIGO_TO";
         edtavTfcontagemresultadoevidencia_codigo_Internalname = "vTFCONTAGEMRESULTADOEVIDENCIA_CODIGO";
         edtavTfcontagemresultadoevidencia_codigo_to_Internalname = "vTFCONTAGEMRESULTADOEVIDENCIA_CODIGO_TO";
         edtavTfcontagemresultadoevidencia_nomearq_Internalname = "vTFCONTAGEMRESULTADOEVIDENCIA_NOMEARQ";
         edtavTfcontagemresultadoevidencia_nomearq_sel_Internalname = "vTFCONTAGEMRESULTADOEVIDENCIA_NOMEARQ_SEL";
         edtavTfcontagemresultadoevidencia_tipoarq_Internalname = "vTFCONTAGEMRESULTADOEVIDENCIA_TIPOARQ";
         edtavTfcontagemresultadoevidencia_tipoarq_sel_Internalname = "vTFCONTAGEMRESULTADOEVIDENCIA_TIPOARQ_SEL";
         edtavTfcontagemresultadoevidencia_data_Internalname = "vTFCONTAGEMRESULTADOEVIDENCIA_DATA";
         edtavTfcontagemresultadoevidencia_data_to_Internalname = "vTFCONTAGEMRESULTADOEVIDENCIA_DATA_TO";
         edtavDdo_contagemresultadoevidencia_dataauxdate_Internalname = "vDDO_CONTAGEMRESULTADOEVIDENCIA_DATAAUXDATE";
         edtavDdo_contagemresultadoevidencia_dataauxdateto_Internalname = "vDDO_CONTAGEMRESULTADOEVIDENCIA_DATAAUXDATETO";
         divDdo_contagemresultadoevidencia_dataauxdates_Internalname = "DDO_CONTAGEMRESULTADOEVIDENCIA_DATAAUXDATES";
         Ddo_contagemresultado_codigo_Internalname = "DDO_CONTAGEMRESULTADO_CODIGO";
         edtavDdo_contagemresultado_codigotitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMRESULTADO_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_contagemresultadoevidencia_codigo_Internalname = "DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO";
         edtavDdo_contagemresultadoevidencia_codigotitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMRESULTADOEVIDENCIA_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_contagemresultadoevidencia_nomearq_Internalname = "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ";
         edtavDdo_contagemresultadoevidencia_nomearqtitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQTITLECONTROLIDTOREPLACE";
         Ddo_contagemresultadoevidencia_tipoarq_Internalname = "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ";
         edtavDdo_contagemresultadoevidencia_tipoarqtitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQTITLECONTROLIDTOREPLACE";
         Ddo_contagemresultadoevidencia_data_Internalname = "DDO_CONTAGEMRESULTADOEVIDENCIA_DATA";
         edtavDdo_contagemresultadoevidencia_datatitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMRESULTADOEVIDENCIA_DATATITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtContagemResultadoEvidencia_Data_Jsonclick = "";
         edtContagemResultadoEvidencia_TipoArq_Jsonclick = "";
         edtContagemResultadoEvidencia_NomeArq_Jsonclick = "";
         edtContagemResultadoEvidencia_Codigo_Jsonclick = "";
         edtContagemResultado_Codigo_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavContagemresultadoevidencia_nomearq3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         edtavContagemresultadoevidencia_nomearq2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         edtavContagemresultadoevidencia_nomearq1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         edtContagemResultadoEvidencia_Data_Titleformat = 0;
         edtContagemResultadoEvidencia_TipoArq_Titleformat = 0;
         edtContagemResultadoEvidencia_NomeArq_Titleformat = 0;
         edtContagemResultadoEvidencia_Codigo_Titleformat = 0;
         edtContagemResultado_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavContagemresultadoevidencia_nomearq3_Visible = 1;
         edtavContagemresultadoevidencia_nomearq2_Visible = 1;
         edtavContagemresultadoevidencia_nomearq1_Visible = 1;
         edtContagemResultadoEvidencia_Data_Title = "Upload";
         edtContagemResultadoEvidencia_TipoArq_Title = "Tipo";
         edtContagemResultadoEvidencia_NomeArq_Title = "do Arquivo";
         edtContagemResultadoEvidencia_Codigo_Title = "Evidencia";
         edtContagemResultado_Codigo_Title = "Contagem Resultado_Codigo";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_contagemresultadoevidencia_datatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultadoevidencia_tipoarqtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultadoevidencia_nomearqtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultadoevidencia_codigotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultado_codigotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultadoevidencia_dataauxdateto_Jsonclick = "";
         edtavDdo_contagemresultadoevidencia_dataauxdate_Jsonclick = "";
         edtavTfcontagemresultadoevidencia_data_to_Jsonclick = "";
         edtavTfcontagemresultadoevidencia_data_to_Visible = 1;
         edtavTfcontagemresultadoevidencia_data_Jsonclick = "";
         edtavTfcontagemresultadoevidencia_data_Visible = 1;
         edtavTfcontagemresultadoevidencia_tipoarq_sel_Jsonclick = "";
         edtavTfcontagemresultadoevidencia_tipoarq_sel_Visible = 1;
         edtavTfcontagemresultadoevidencia_tipoarq_Jsonclick = "";
         edtavTfcontagemresultadoevidencia_tipoarq_Visible = 1;
         edtavTfcontagemresultadoevidencia_nomearq_sel_Jsonclick = "";
         edtavTfcontagemresultadoevidencia_nomearq_sel_Visible = 1;
         edtavTfcontagemresultadoevidencia_nomearq_Jsonclick = "";
         edtavTfcontagemresultadoevidencia_nomearq_Visible = 1;
         edtavTfcontagemresultadoevidencia_codigo_to_Jsonclick = "";
         edtavTfcontagemresultadoevidencia_codigo_to_Visible = 1;
         edtavTfcontagemresultadoevidencia_codigo_Jsonclick = "";
         edtavTfcontagemresultadoevidencia_codigo_Visible = 1;
         edtavTfcontagemresultado_codigo_to_Jsonclick = "";
         edtavTfcontagemresultado_codigo_to_Visible = 1;
         edtavTfcontagemresultado_codigo_Jsonclick = "";
         edtavTfcontagemresultado_codigo_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_contagemresultadoevidencia_data_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultadoevidencia_data_Rangefilterto = "At�";
         Ddo_contagemresultadoevidencia_data_Rangefilterfrom = "Desde";
         Ddo_contagemresultadoevidencia_data_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultadoevidencia_data_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemresultadoevidencia_data_Sortasc = "Ordenar de A � Z";
         Ddo_contagemresultadoevidencia_data_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contagemresultadoevidencia_data_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contagemresultadoevidencia_data_Filtertype = "Date";
         Ddo_contagemresultadoevidencia_data_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultadoevidencia_data_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemresultadoevidencia_data_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemresultadoevidencia_data_Titlecontrolidtoreplace = "";
         Ddo_contagemresultadoevidencia_data_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultadoevidencia_data_Cls = "ColumnSettings";
         Ddo_contagemresultadoevidencia_data_Tooltip = "Op��es";
         Ddo_contagemresultadoevidencia_data_Caption = "";
         Ddo_contagemresultadoevidencia_tipoarq_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultadoevidencia_tipoarq_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contagemresultadoevidencia_tipoarq_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultadoevidencia_tipoarq_Loadingdata = "Carregando dados...";
         Ddo_contagemresultadoevidencia_tipoarq_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemresultadoevidencia_tipoarq_Sortasc = "Ordenar de A � Z";
         Ddo_contagemresultadoevidencia_tipoarq_Datalistupdateminimumcharacters = 0;
         Ddo_contagemresultadoevidencia_tipoarq_Datalistproc = "GetPromptContagemResultadoEvidenciaFilterData";
         Ddo_contagemresultadoevidencia_tipoarq_Datalisttype = "Dynamic";
         Ddo_contagemresultadoevidencia_tipoarq_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contagemresultadoevidencia_tipoarq_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contagemresultadoevidencia_tipoarq_Filtertype = "Character";
         Ddo_contagemresultadoevidencia_tipoarq_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultadoevidencia_tipoarq_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemresultadoevidencia_tipoarq_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemresultadoevidencia_tipoarq_Titlecontrolidtoreplace = "";
         Ddo_contagemresultadoevidencia_tipoarq_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultadoevidencia_tipoarq_Cls = "ColumnSettings";
         Ddo_contagemresultadoevidencia_tipoarq_Tooltip = "Op��es";
         Ddo_contagemresultadoevidencia_tipoarq_Caption = "";
         Ddo_contagemresultadoevidencia_nomearq_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultadoevidencia_nomearq_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contagemresultadoevidencia_nomearq_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultadoevidencia_nomearq_Loadingdata = "Carregando dados...";
         Ddo_contagemresultadoevidencia_nomearq_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemresultadoevidencia_nomearq_Sortasc = "Ordenar de A � Z";
         Ddo_contagemresultadoevidencia_nomearq_Datalistupdateminimumcharacters = 0;
         Ddo_contagemresultadoevidencia_nomearq_Datalistproc = "GetPromptContagemResultadoEvidenciaFilterData";
         Ddo_contagemresultadoevidencia_nomearq_Datalisttype = "Dynamic";
         Ddo_contagemresultadoevidencia_nomearq_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contagemresultadoevidencia_nomearq_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contagemresultadoevidencia_nomearq_Filtertype = "Character";
         Ddo_contagemresultadoevidencia_nomearq_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultadoevidencia_nomearq_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemresultadoevidencia_nomearq_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemresultadoevidencia_nomearq_Titlecontrolidtoreplace = "";
         Ddo_contagemresultadoevidencia_nomearq_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultadoevidencia_nomearq_Cls = "ColumnSettings";
         Ddo_contagemresultadoevidencia_nomearq_Tooltip = "Op��es";
         Ddo_contagemresultadoevidencia_nomearq_Caption = "";
         Ddo_contagemresultadoevidencia_codigo_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultadoevidencia_codigo_Rangefilterto = "At�";
         Ddo_contagemresultadoevidencia_codigo_Rangefilterfrom = "Desde";
         Ddo_contagemresultadoevidencia_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultadoevidencia_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemresultadoevidencia_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_contagemresultadoevidencia_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contagemresultadoevidencia_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contagemresultadoevidencia_codigo_Filtertype = "Numeric";
         Ddo_contagemresultadoevidencia_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultadoevidencia_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemresultadoevidencia_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemresultadoevidencia_codigo_Titlecontrolidtoreplace = "";
         Ddo_contagemresultadoevidencia_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultadoevidencia_codigo_Cls = "ColumnSettings";
         Ddo_contagemresultadoevidencia_codigo_Tooltip = "Op��es";
         Ddo_contagemresultadoevidencia_codigo_Caption = "";
         Ddo_contagemresultado_codigo_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultado_codigo_Rangefilterto = "At�";
         Ddo_contagemresultado_codigo_Rangefilterfrom = "Desde";
         Ddo_contagemresultado_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultado_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemresultado_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_contagemresultado_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contagemresultado_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contagemresultado_codigo_Filtertype = "Numeric";
         Ddo_contagemresultado_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultado_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemresultado_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemresultado_codigo_Titlecontrolidtoreplace = "";
         Ddo_contagemresultado_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultado_codigo_Cls = "ColumnSettings";
         Ddo_contagemresultado_codigo_Tooltip = "Op��es";
         Ddo_contagemresultado_codigo_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Contagem Resultado Evidencias";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV38ddo_ContagemResultado_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_ContagemResultadoEvidencia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContagemResultadoEvidencia_NomeArqTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_ContagemResultadoEvidencia_TipoArqTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContagemResultadoEvidencia_DataTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV36TFContagemResultado_Codigo',fld:'vTFCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV37TFContagemResultado_Codigo_To',fld:'vTFCONTAGEMRESULTADO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV40TFContagemResultadoEvidencia_Codigo',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV41TFContagemResultadoEvidencia_Codigo_To',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContagemResultadoEvidencia_NomeArq',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'AV45TFContagemResultadoEvidencia_NomeArq_Sel',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_NOMEARQ_SEL',pic:'',nv:''},{av:'AV48TFContagemResultadoEvidencia_TipoArq',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_TIPOARQ',pic:'',nv:''},{av:'AV49TFContagemResultadoEvidencia_TipoArq_Sel',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_TIPOARQ_SEL',pic:'',nv:''},{av:'AV52TFContagemResultadoEvidencia_Data',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV53TFContagemResultadoEvidencia_Data_To',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV64Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32ContagemResultadoEvidencia_NomeArq1',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ1',pic:'',nv:''},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33ContagemResultadoEvidencia_NomeArq2',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ2',pic:'',nv:''},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34ContagemResultadoEvidencia_NomeArq3',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ3',pic:'',nv:''}],oparms:[{av:'AV35ContagemResultado_CodigoTitleFilterData',fld:'vCONTAGEMRESULTADO_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV39ContagemResultadoEvidencia_CodigoTitleFilterData',fld:'vCONTAGEMRESULTADOEVIDENCIA_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV43ContagemResultadoEvidencia_NomeArqTitleFilterData',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQTITLEFILTERDATA',pic:'',nv:null},{av:'AV47ContagemResultadoEvidencia_TipoArqTitleFilterData',fld:'vCONTAGEMRESULTADOEVIDENCIA_TIPOARQTITLEFILTERDATA',pic:'',nv:null},{av:'AV51ContagemResultadoEvidencia_DataTitleFilterData',fld:'vCONTAGEMRESULTADOEVIDENCIA_DATATITLEFILTERDATA',pic:'',nv:null},{av:'edtContagemResultado_Codigo_Titleformat',ctrl:'CONTAGEMRESULTADO_CODIGO',prop:'Titleformat'},{av:'edtContagemResultado_Codigo_Title',ctrl:'CONTAGEMRESULTADO_CODIGO',prop:'Title'},{av:'edtContagemResultadoEvidencia_Codigo_Titleformat',ctrl:'CONTAGEMRESULTADOEVIDENCIA_CODIGO',prop:'Titleformat'},{av:'edtContagemResultadoEvidencia_Codigo_Title',ctrl:'CONTAGEMRESULTADOEVIDENCIA_CODIGO',prop:'Title'},{av:'edtContagemResultadoEvidencia_NomeArq_Titleformat',ctrl:'CONTAGEMRESULTADOEVIDENCIA_NOMEARQ',prop:'Titleformat'},{av:'edtContagemResultadoEvidencia_NomeArq_Title',ctrl:'CONTAGEMRESULTADOEVIDENCIA_NOMEARQ',prop:'Title'},{av:'edtContagemResultadoEvidencia_TipoArq_Titleformat',ctrl:'CONTAGEMRESULTADOEVIDENCIA_TIPOARQ',prop:'Titleformat'},{av:'edtContagemResultadoEvidencia_TipoArq_Title',ctrl:'CONTAGEMRESULTADOEVIDENCIA_TIPOARQ',prop:'Title'},{av:'edtContagemResultadoEvidencia_Data_Titleformat',ctrl:'CONTAGEMRESULTADOEVIDENCIA_DATA',prop:'Titleformat'},{av:'edtContagemResultadoEvidencia_Data_Title',ctrl:'CONTAGEMRESULTADOEVIDENCIA_DATA',prop:'Title'},{av:'AV59GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV60GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11BY2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32ContagemResultadoEvidencia_NomeArq1',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33ContagemResultadoEvidencia_NomeArq2',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ2',pic:'',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34ContagemResultadoEvidencia_NomeArq3',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ3',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV36TFContagemResultado_Codigo',fld:'vTFCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV37TFContagemResultado_Codigo_To',fld:'vTFCONTAGEMRESULTADO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV40TFContagemResultadoEvidencia_Codigo',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV41TFContagemResultadoEvidencia_Codigo_To',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContagemResultadoEvidencia_NomeArq',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'AV45TFContagemResultadoEvidencia_NomeArq_Sel',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_NOMEARQ_SEL',pic:'',nv:''},{av:'AV48TFContagemResultadoEvidencia_TipoArq',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_TIPOARQ',pic:'',nv:''},{av:'AV49TFContagemResultadoEvidencia_TipoArq_Sel',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_TIPOARQ_SEL',pic:'',nv:''},{av:'AV52TFContagemResultadoEvidencia_Data',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV53TFContagemResultadoEvidencia_Data_To',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV38ddo_ContagemResultado_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_ContagemResultadoEvidencia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContagemResultadoEvidencia_NomeArqTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_ContagemResultadoEvidencia_TipoArqTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContagemResultadoEvidencia_DataTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTAGEMRESULTADO_CODIGO.ONOPTIONCLICKED","{handler:'E12BY2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32ContagemResultadoEvidencia_NomeArq1',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33ContagemResultadoEvidencia_NomeArq2',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ2',pic:'',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34ContagemResultadoEvidencia_NomeArq3',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ3',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV36TFContagemResultado_Codigo',fld:'vTFCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV37TFContagemResultado_Codigo_To',fld:'vTFCONTAGEMRESULTADO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV40TFContagemResultadoEvidencia_Codigo',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV41TFContagemResultadoEvidencia_Codigo_To',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContagemResultadoEvidencia_NomeArq',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'AV45TFContagemResultadoEvidencia_NomeArq_Sel',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_NOMEARQ_SEL',pic:'',nv:''},{av:'AV48TFContagemResultadoEvidencia_TipoArq',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_TIPOARQ',pic:'',nv:''},{av:'AV49TFContagemResultadoEvidencia_TipoArq_Sel',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_TIPOARQ_SEL',pic:'',nv:''},{av:'AV52TFContagemResultadoEvidencia_Data',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV53TFContagemResultadoEvidencia_Data_To',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV38ddo_ContagemResultado_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_ContagemResultadoEvidencia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContagemResultadoEvidencia_NomeArqTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_ContagemResultadoEvidencia_TipoArqTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContagemResultadoEvidencia_DataTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contagemresultado_codigo_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADO_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_contagemresultado_codigo_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADO_CODIGO',prop:'FilteredText_get'},{av:'Ddo_contagemresultado_codigo_Filteredtextto_get',ctrl:'DDO_CONTAGEMRESULTADO_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemresultado_codigo_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_CODIGO',prop:'SortedStatus'},{av:'AV36TFContagemResultado_Codigo',fld:'vTFCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV37TFContagemResultado_Codigo_To',fld:'vTFCONTAGEMRESULTADO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contagemresultadoevidencia_codigo_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contagemresultadoevidencia_nomearq_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ',prop:'SortedStatus'},{av:'Ddo_contagemresultadoevidencia_tipoarq_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ',prop:'SortedStatus'},{av:'Ddo_contagemresultadoevidencia_data_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEVIDENCIA_DATA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO.ONOPTIONCLICKED","{handler:'E13BY2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32ContagemResultadoEvidencia_NomeArq1',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33ContagemResultadoEvidencia_NomeArq2',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ2',pic:'',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34ContagemResultadoEvidencia_NomeArq3',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ3',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV36TFContagemResultado_Codigo',fld:'vTFCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV37TFContagemResultado_Codigo_To',fld:'vTFCONTAGEMRESULTADO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV40TFContagemResultadoEvidencia_Codigo',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV41TFContagemResultadoEvidencia_Codigo_To',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContagemResultadoEvidencia_NomeArq',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'AV45TFContagemResultadoEvidencia_NomeArq_Sel',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_NOMEARQ_SEL',pic:'',nv:''},{av:'AV48TFContagemResultadoEvidencia_TipoArq',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_TIPOARQ',pic:'',nv:''},{av:'AV49TFContagemResultadoEvidencia_TipoArq_Sel',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_TIPOARQ_SEL',pic:'',nv:''},{av:'AV52TFContagemResultadoEvidencia_Data',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV53TFContagemResultadoEvidencia_Data_To',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV38ddo_ContagemResultado_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_ContagemResultadoEvidencia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContagemResultadoEvidencia_NomeArqTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_ContagemResultadoEvidencia_TipoArqTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContagemResultadoEvidencia_DataTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contagemresultadoevidencia_codigo_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_contagemresultadoevidencia_codigo_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO',prop:'FilteredText_get'},{av:'Ddo_contagemresultadoevidencia_codigo_Filteredtextto_get',ctrl:'DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemresultadoevidencia_codigo_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO',prop:'SortedStatus'},{av:'AV40TFContagemResultadoEvidencia_Codigo',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV41TFContagemResultadoEvidencia_Codigo_To',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contagemresultado_codigo_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contagemresultadoevidencia_nomearq_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ',prop:'SortedStatus'},{av:'Ddo_contagemresultadoevidencia_tipoarq_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ',prop:'SortedStatus'},{av:'Ddo_contagemresultadoevidencia_data_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEVIDENCIA_DATA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ.ONOPTIONCLICKED","{handler:'E14BY2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32ContagemResultadoEvidencia_NomeArq1',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33ContagemResultadoEvidencia_NomeArq2',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ2',pic:'',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34ContagemResultadoEvidencia_NomeArq3',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ3',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV36TFContagemResultado_Codigo',fld:'vTFCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV37TFContagemResultado_Codigo_To',fld:'vTFCONTAGEMRESULTADO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV40TFContagemResultadoEvidencia_Codigo',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV41TFContagemResultadoEvidencia_Codigo_To',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContagemResultadoEvidencia_NomeArq',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'AV45TFContagemResultadoEvidencia_NomeArq_Sel',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_NOMEARQ_SEL',pic:'',nv:''},{av:'AV48TFContagemResultadoEvidencia_TipoArq',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_TIPOARQ',pic:'',nv:''},{av:'AV49TFContagemResultadoEvidencia_TipoArq_Sel',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_TIPOARQ_SEL',pic:'',nv:''},{av:'AV52TFContagemResultadoEvidencia_Data',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV53TFContagemResultadoEvidencia_Data_To',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV38ddo_ContagemResultado_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_ContagemResultadoEvidencia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContagemResultadoEvidencia_NomeArqTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_ContagemResultadoEvidencia_TipoArqTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContagemResultadoEvidencia_DataTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contagemresultadoevidencia_nomearq_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ',prop:'ActiveEventKey'},{av:'Ddo_contagemresultadoevidencia_nomearq_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ',prop:'FilteredText_get'},{av:'Ddo_contagemresultadoevidencia_nomearq_Selectedvalue_get',ctrl:'DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemresultadoevidencia_nomearq_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ',prop:'SortedStatus'},{av:'AV44TFContagemResultadoEvidencia_NomeArq',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'AV45TFContagemResultadoEvidencia_NomeArq_Sel',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_NOMEARQ_SEL',pic:'',nv:''},{av:'Ddo_contagemresultado_codigo_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contagemresultadoevidencia_codigo_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contagemresultadoevidencia_tipoarq_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ',prop:'SortedStatus'},{av:'Ddo_contagemresultadoevidencia_data_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEVIDENCIA_DATA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ.ONOPTIONCLICKED","{handler:'E15BY2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32ContagemResultadoEvidencia_NomeArq1',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33ContagemResultadoEvidencia_NomeArq2',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ2',pic:'',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34ContagemResultadoEvidencia_NomeArq3',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ3',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV36TFContagemResultado_Codigo',fld:'vTFCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV37TFContagemResultado_Codigo_To',fld:'vTFCONTAGEMRESULTADO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV40TFContagemResultadoEvidencia_Codigo',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV41TFContagemResultadoEvidencia_Codigo_To',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContagemResultadoEvidencia_NomeArq',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'AV45TFContagemResultadoEvidencia_NomeArq_Sel',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_NOMEARQ_SEL',pic:'',nv:''},{av:'AV48TFContagemResultadoEvidencia_TipoArq',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_TIPOARQ',pic:'',nv:''},{av:'AV49TFContagemResultadoEvidencia_TipoArq_Sel',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_TIPOARQ_SEL',pic:'',nv:''},{av:'AV52TFContagemResultadoEvidencia_Data',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV53TFContagemResultadoEvidencia_Data_To',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV38ddo_ContagemResultado_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_ContagemResultadoEvidencia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContagemResultadoEvidencia_NomeArqTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_ContagemResultadoEvidencia_TipoArqTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContagemResultadoEvidencia_DataTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contagemresultadoevidencia_tipoarq_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ',prop:'ActiveEventKey'},{av:'Ddo_contagemresultadoevidencia_tipoarq_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ',prop:'FilteredText_get'},{av:'Ddo_contagemresultadoevidencia_tipoarq_Selectedvalue_get',ctrl:'DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemresultadoevidencia_tipoarq_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ',prop:'SortedStatus'},{av:'AV48TFContagemResultadoEvidencia_TipoArq',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_TIPOARQ',pic:'',nv:''},{av:'AV49TFContagemResultadoEvidencia_TipoArq_Sel',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_TIPOARQ_SEL',pic:'',nv:''},{av:'Ddo_contagemresultado_codigo_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contagemresultadoevidencia_codigo_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contagemresultadoevidencia_nomearq_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ',prop:'SortedStatus'},{av:'Ddo_contagemresultadoevidencia_data_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEVIDENCIA_DATA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADOEVIDENCIA_DATA.ONOPTIONCLICKED","{handler:'E16BY2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32ContagemResultadoEvidencia_NomeArq1',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33ContagemResultadoEvidencia_NomeArq2',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ2',pic:'',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34ContagemResultadoEvidencia_NomeArq3',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ3',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV36TFContagemResultado_Codigo',fld:'vTFCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV37TFContagemResultado_Codigo_To',fld:'vTFCONTAGEMRESULTADO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV40TFContagemResultadoEvidencia_Codigo',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV41TFContagemResultadoEvidencia_Codigo_To',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContagemResultadoEvidencia_NomeArq',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'AV45TFContagemResultadoEvidencia_NomeArq_Sel',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_NOMEARQ_SEL',pic:'',nv:''},{av:'AV48TFContagemResultadoEvidencia_TipoArq',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_TIPOARQ',pic:'',nv:''},{av:'AV49TFContagemResultadoEvidencia_TipoArq_Sel',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_TIPOARQ_SEL',pic:'',nv:''},{av:'AV52TFContagemResultadoEvidencia_Data',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV53TFContagemResultadoEvidencia_Data_To',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV38ddo_ContagemResultado_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_ContagemResultadoEvidencia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContagemResultadoEvidencia_NomeArqTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_ContagemResultadoEvidencia_TipoArqTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContagemResultadoEvidencia_DataTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contagemresultadoevidencia_data_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADOEVIDENCIA_DATA',prop:'ActiveEventKey'},{av:'Ddo_contagemresultadoevidencia_data_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADOEVIDENCIA_DATA',prop:'FilteredText_get'},{av:'Ddo_contagemresultadoevidencia_data_Filteredtextto_get',ctrl:'DDO_CONTAGEMRESULTADOEVIDENCIA_DATA',prop:'FilteredTextTo_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemresultadoevidencia_data_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEVIDENCIA_DATA',prop:'SortedStatus'},{av:'AV52TFContagemResultadoEvidencia_Data',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV53TFContagemResultadoEvidencia_Data_To',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_contagemresultado_codigo_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contagemresultadoevidencia_codigo_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO',prop:'SortedStatus'},{av:'Ddo_contagemresultadoevidencia_nomearq_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ',prop:'SortedStatus'},{av:'Ddo_contagemresultadoevidencia_tipoarq_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E29BY2',iparms:[],oparms:[{av:'AV29Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E30BY2',iparms:[{av:'A586ContagemResultadoEvidencia_Codigo',fld:'CONTAGEMRESULTADOEVIDENCIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A589ContagemResultadoEvidencia_NomeArq',fld:'CONTAGEMRESULTADOEVIDENCIA_NOMEARQ',pic:'',hsh:true,nv:''}],oparms:[{av:'AV8InOutContagemResultadoEvidencia_Codigo',fld:'vINOUTCONTAGEMRESULTADOEVIDENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV31InOutContagemResultadoEvidencia_NomeArq',fld:'vINOUTCONTAGEMRESULTADOEVIDENCIA_NOMEARQ',pic:'',nv:''}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E17BY2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32ContagemResultadoEvidencia_NomeArq1',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33ContagemResultadoEvidencia_NomeArq2',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ2',pic:'',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34ContagemResultadoEvidencia_NomeArq3',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ3',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV36TFContagemResultado_Codigo',fld:'vTFCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV37TFContagemResultado_Codigo_To',fld:'vTFCONTAGEMRESULTADO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV40TFContagemResultadoEvidencia_Codigo',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV41TFContagemResultadoEvidencia_Codigo_To',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContagemResultadoEvidencia_NomeArq',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'AV45TFContagemResultadoEvidencia_NomeArq_Sel',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_NOMEARQ_SEL',pic:'',nv:''},{av:'AV48TFContagemResultadoEvidencia_TipoArq',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_TIPOARQ',pic:'',nv:''},{av:'AV49TFContagemResultadoEvidencia_TipoArq_Sel',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_TIPOARQ_SEL',pic:'',nv:''},{av:'AV52TFContagemResultadoEvidencia_Data',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV53TFContagemResultadoEvidencia_Data_To',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV38ddo_ContagemResultado_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_ContagemResultadoEvidencia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContagemResultadoEvidencia_NomeArqTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_ContagemResultadoEvidencia_TipoArqTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContagemResultadoEvidencia_DataTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E22BY2',iparms:[],oparms:[{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E18BY2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32ContagemResultadoEvidencia_NomeArq1',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33ContagemResultadoEvidencia_NomeArq2',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ2',pic:'',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34ContagemResultadoEvidencia_NomeArq3',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ3',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV36TFContagemResultado_Codigo',fld:'vTFCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV37TFContagemResultado_Codigo_To',fld:'vTFCONTAGEMRESULTADO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV40TFContagemResultadoEvidencia_Codigo',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV41TFContagemResultadoEvidencia_Codigo_To',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContagemResultadoEvidencia_NomeArq',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'AV45TFContagemResultadoEvidencia_NomeArq_Sel',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_NOMEARQ_SEL',pic:'',nv:''},{av:'AV48TFContagemResultadoEvidencia_TipoArq',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_TIPOARQ',pic:'',nv:''},{av:'AV49TFContagemResultadoEvidencia_TipoArq_Sel',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_TIPOARQ_SEL',pic:'',nv:''},{av:'AV52TFContagemResultadoEvidencia_Data',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV53TFContagemResultadoEvidencia_Data_To',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV38ddo_ContagemResultado_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_ContagemResultadoEvidencia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContagemResultadoEvidencia_NomeArqTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_ContagemResultadoEvidencia_TipoArqTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContagemResultadoEvidencia_DataTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33ContagemResultadoEvidencia_NomeArq2',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ2',pic:'',nv:''},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34ContagemResultadoEvidencia_NomeArq3',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32ContagemResultadoEvidencia_NomeArq1',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavContagemresultadoevidencia_nomearq2_Visible',ctrl:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ2',prop:'Visible'},{av:'edtavContagemresultadoevidencia_nomearq3_Visible',ctrl:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ3',prop:'Visible'},{av:'edtavContagemresultadoevidencia_nomearq1_Visible',ctrl:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E23BY2',iparms:[{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavContagemresultadoevidencia_nomearq1_Visible',ctrl:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ1',prop:'Visible'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E24BY2',iparms:[],oparms:[{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E19BY2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32ContagemResultadoEvidencia_NomeArq1',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33ContagemResultadoEvidencia_NomeArq2',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ2',pic:'',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34ContagemResultadoEvidencia_NomeArq3',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ3',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV36TFContagemResultado_Codigo',fld:'vTFCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV37TFContagemResultado_Codigo_To',fld:'vTFCONTAGEMRESULTADO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV40TFContagemResultadoEvidencia_Codigo',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV41TFContagemResultadoEvidencia_Codigo_To',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContagemResultadoEvidencia_NomeArq',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'AV45TFContagemResultadoEvidencia_NomeArq_Sel',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_NOMEARQ_SEL',pic:'',nv:''},{av:'AV48TFContagemResultadoEvidencia_TipoArq',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_TIPOARQ',pic:'',nv:''},{av:'AV49TFContagemResultadoEvidencia_TipoArq_Sel',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_TIPOARQ_SEL',pic:'',nv:''},{av:'AV52TFContagemResultadoEvidencia_Data',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV53TFContagemResultadoEvidencia_Data_To',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV38ddo_ContagemResultado_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_ContagemResultadoEvidencia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContagemResultadoEvidencia_NomeArqTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_ContagemResultadoEvidencia_TipoArqTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContagemResultadoEvidencia_DataTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33ContagemResultadoEvidencia_NomeArq2',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ2',pic:'',nv:''},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34ContagemResultadoEvidencia_NomeArq3',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32ContagemResultadoEvidencia_NomeArq1',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavContagemresultadoevidencia_nomearq2_Visible',ctrl:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ2',prop:'Visible'},{av:'edtavContagemresultadoevidencia_nomearq3_Visible',ctrl:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ3',prop:'Visible'},{av:'edtavContagemresultadoevidencia_nomearq1_Visible',ctrl:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E25BY2',iparms:[{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavContagemresultadoevidencia_nomearq2_Visible',ctrl:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E20BY2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32ContagemResultadoEvidencia_NomeArq1',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33ContagemResultadoEvidencia_NomeArq2',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ2',pic:'',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34ContagemResultadoEvidencia_NomeArq3',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ3',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV36TFContagemResultado_Codigo',fld:'vTFCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV37TFContagemResultado_Codigo_To',fld:'vTFCONTAGEMRESULTADO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV40TFContagemResultadoEvidencia_Codigo',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV41TFContagemResultadoEvidencia_Codigo_To',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContagemResultadoEvidencia_NomeArq',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'AV45TFContagemResultadoEvidencia_NomeArq_Sel',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_NOMEARQ_SEL',pic:'',nv:''},{av:'AV48TFContagemResultadoEvidencia_TipoArq',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_TIPOARQ',pic:'',nv:''},{av:'AV49TFContagemResultadoEvidencia_TipoArq_Sel',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_TIPOARQ_SEL',pic:'',nv:''},{av:'AV52TFContagemResultadoEvidencia_Data',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV53TFContagemResultadoEvidencia_Data_To',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV38ddo_ContagemResultado_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_ContagemResultadoEvidencia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContagemResultadoEvidencia_NomeArqTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_ContagemResultadoEvidencia_TipoArqTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContagemResultadoEvidencia_DataTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33ContagemResultadoEvidencia_NomeArq2',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ2',pic:'',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34ContagemResultadoEvidencia_NomeArq3',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32ContagemResultadoEvidencia_NomeArq1',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavContagemresultadoevidencia_nomearq2_Visible',ctrl:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ2',prop:'Visible'},{av:'edtavContagemresultadoevidencia_nomearq3_Visible',ctrl:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ3',prop:'Visible'},{av:'edtavContagemresultadoevidencia_nomearq1_Visible',ctrl:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E26BY2',iparms:[{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavContagemresultadoevidencia_nomearq3_Visible',ctrl:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ3',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E21BY2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32ContagemResultadoEvidencia_NomeArq1',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ1',pic:'',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33ContagemResultadoEvidencia_NomeArq2',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ2',pic:'',nv:''},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34ContagemResultadoEvidencia_NomeArq3',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ3',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV36TFContagemResultado_Codigo',fld:'vTFCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV37TFContagemResultado_Codigo_To',fld:'vTFCONTAGEMRESULTADO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV40TFContagemResultadoEvidencia_Codigo',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV41TFContagemResultadoEvidencia_Codigo_To',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV44TFContagemResultadoEvidencia_NomeArq',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'AV45TFContagemResultadoEvidencia_NomeArq_Sel',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_NOMEARQ_SEL',pic:'',nv:''},{av:'AV48TFContagemResultadoEvidencia_TipoArq',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_TIPOARQ',pic:'',nv:''},{av:'AV49TFContagemResultadoEvidencia_TipoArq_Sel',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_TIPOARQ_SEL',pic:'',nv:''},{av:'AV52TFContagemResultadoEvidencia_Data',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV53TFContagemResultadoEvidencia_Data_To',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV38ddo_ContagemResultado_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_ContagemResultadoEvidencia_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContagemResultadoEvidencia_NomeArqTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_ContagemResultadoEvidencia_TipoArqTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_ContagemResultadoEvidencia_DataTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEVIDENCIA_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV64Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV28DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV27DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV36TFContagemResultado_Codigo',fld:'vTFCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contagemresultado_codigo_Filteredtext_set',ctrl:'DDO_CONTAGEMRESULTADO_CODIGO',prop:'FilteredText_set'},{av:'AV37TFContagemResultado_Codigo_To',fld:'vTFCONTAGEMRESULTADO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contagemresultado_codigo_Filteredtextto_set',ctrl:'DDO_CONTAGEMRESULTADO_CODIGO',prop:'FilteredTextTo_set'},{av:'AV40TFContagemResultadoEvidencia_Codigo',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contagemresultadoevidencia_codigo_Filteredtext_set',ctrl:'DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO',prop:'FilteredText_set'},{av:'AV41TFContagemResultadoEvidencia_Codigo_To',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contagemresultadoevidencia_codigo_Filteredtextto_set',ctrl:'DDO_CONTAGEMRESULTADOEVIDENCIA_CODIGO',prop:'FilteredTextTo_set'},{av:'AV44TFContagemResultadoEvidencia_NomeArq',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'Ddo_contagemresultadoevidencia_nomearq_Filteredtext_set',ctrl:'DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ',prop:'FilteredText_set'},{av:'AV45TFContagemResultadoEvidencia_NomeArq_Sel',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_NOMEARQ_SEL',pic:'',nv:''},{av:'Ddo_contagemresultadoevidencia_nomearq_Selectedvalue_set',ctrl:'DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ',prop:'SelectedValue_set'},{av:'AV48TFContagemResultadoEvidencia_TipoArq',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_TIPOARQ',pic:'',nv:''},{av:'Ddo_contagemresultadoevidencia_tipoarq_Filteredtext_set',ctrl:'DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ',prop:'FilteredText_set'},{av:'AV49TFContagemResultadoEvidencia_TipoArq_Sel',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_TIPOARQ_SEL',pic:'',nv:''},{av:'Ddo_contagemresultadoevidencia_tipoarq_Selectedvalue_set',ctrl:'DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ',prop:'SelectedValue_set'},{av:'AV52TFContagemResultadoEvidencia_Data',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_DATA',pic:'99/99/99 99:99',nv:''},{av:'Ddo_contagemresultadoevidencia_data_Filteredtext_set',ctrl:'DDO_CONTAGEMRESULTADOEVIDENCIA_DATA',prop:'FilteredText_set'},{av:'AV53TFContagemResultadoEvidencia_Data_To',fld:'vTFCONTAGEMRESULTADOEVIDENCIA_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_contagemresultadoevidencia_data_Filteredtextto_set',ctrl:'DDO_CONTAGEMRESULTADOEVIDENCIA_DATA',prop:'FilteredTextTo_set'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV32ContagemResultadoEvidencia_NomeArq1',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ1',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavContagemresultadoevidencia_nomearq1_Visible',ctrl:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ1',prop:'Visible'},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33ContagemResultadoEvidencia_NomeArq2',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ2',pic:'',nv:''},{av:'AV23DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV24DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV34ContagemResultadoEvidencia_NomeArq3',fld:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavContagemresultadoevidencia_nomearq2_Visible',ctrl:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ2',prop:'Visible'},{av:'edtavContagemresultadoevidencia_nomearq3_Visible',ctrl:'vCONTAGEMRESULTADOEVIDENCIA_NOMEARQ3',prop:'Visible'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV31InOutContagemResultadoEvidencia_NomeArq = "";
         Gridpaginationbar_Selectedpage = "";
         Ddo_contagemresultado_codigo_Activeeventkey = "";
         Ddo_contagemresultado_codigo_Filteredtext_get = "";
         Ddo_contagemresultado_codigo_Filteredtextto_get = "";
         Ddo_contagemresultadoevidencia_codigo_Activeeventkey = "";
         Ddo_contagemresultadoevidencia_codigo_Filteredtext_get = "";
         Ddo_contagemresultadoevidencia_codigo_Filteredtextto_get = "";
         Ddo_contagemresultadoevidencia_nomearq_Activeeventkey = "";
         Ddo_contagemresultadoevidencia_nomearq_Filteredtext_get = "";
         Ddo_contagemresultadoevidencia_nomearq_Selectedvalue_get = "";
         Ddo_contagemresultadoevidencia_tipoarq_Activeeventkey = "";
         Ddo_contagemresultadoevidencia_tipoarq_Filteredtext_get = "";
         Ddo_contagemresultadoevidencia_tipoarq_Selectedvalue_get = "";
         Ddo_contagemresultadoevidencia_data_Activeeventkey = "";
         Ddo_contagemresultadoevidencia_data_Filteredtext_get = "";
         Ddo_contagemresultadoevidencia_data_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV16DynamicFiltersSelector1 = "";
         AV32ContagemResultadoEvidencia_NomeArq1 = "";
         AV20DynamicFiltersSelector2 = "";
         AV33ContagemResultadoEvidencia_NomeArq2 = "";
         AV24DynamicFiltersSelector3 = "";
         AV34ContagemResultadoEvidencia_NomeArq3 = "";
         AV44TFContagemResultadoEvidencia_NomeArq = "";
         AV45TFContagemResultadoEvidencia_NomeArq_Sel = "";
         AV48TFContagemResultadoEvidencia_TipoArq = "";
         AV49TFContagemResultadoEvidencia_TipoArq_Sel = "";
         AV52TFContagemResultadoEvidencia_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         AV53TFContagemResultadoEvidencia_Data_To = DateTimeUtil.ServerNow( context, "DEFAULT");
         AV38ddo_ContagemResultado_CodigoTitleControlIdToReplace = "";
         AV42ddo_ContagemResultadoEvidencia_CodigoTitleControlIdToReplace = "";
         AV46ddo_ContagemResultadoEvidencia_NomeArqTitleControlIdToReplace = "";
         AV50ddo_ContagemResultadoEvidencia_TipoArqTitleControlIdToReplace = "";
         AV56ddo_ContagemResultadoEvidencia_DataTitleControlIdToReplace = "";
         AV64Pgmname = "";
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV57DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV35ContagemResultado_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV39ContagemResultadoEvidencia_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV43ContagemResultadoEvidencia_NomeArqTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV47ContagemResultadoEvidencia_TipoArqTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV51ContagemResultadoEvidencia_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_contagemresultado_codigo_Filteredtext_set = "";
         Ddo_contagemresultado_codigo_Filteredtextto_set = "";
         Ddo_contagemresultado_codigo_Sortedstatus = "";
         Ddo_contagemresultadoevidencia_codigo_Filteredtext_set = "";
         Ddo_contagemresultadoevidencia_codigo_Filteredtextto_set = "";
         Ddo_contagemresultadoevidencia_codigo_Sortedstatus = "";
         Ddo_contagemresultadoevidencia_nomearq_Filteredtext_set = "";
         Ddo_contagemresultadoevidencia_nomearq_Selectedvalue_set = "";
         Ddo_contagemresultadoevidencia_nomearq_Sortedstatus = "";
         Ddo_contagemresultadoevidencia_tipoarq_Filteredtext_set = "";
         Ddo_contagemresultadoevidencia_tipoarq_Selectedvalue_set = "";
         Ddo_contagemresultadoevidencia_tipoarq_Sortedstatus = "";
         Ddo_contagemresultadoevidencia_data_Filteredtext_set = "";
         Ddo_contagemresultadoevidencia_data_Filteredtextto_set = "";
         Ddo_contagemresultadoevidencia_data_Sortedstatus = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV54DDO_ContagemResultadoEvidencia_DataAuxDate = DateTime.MinValue;
         AV55DDO_ContagemResultadoEvidencia_DataAuxDateTo = DateTime.MinValue;
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV29Select = "";
         AV63Select_GXI = "";
         A589ContagemResultadoEvidencia_NomeArq = "";
         A590ContagemResultadoEvidencia_TipoArq = "";
         A591ContagemResultadoEvidencia_Data = (DateTime)(DateTime.MinValue);
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV32ContagemResultadoEvidencia_NomeArq1 = "";
         lV33ContagemResultadoEvidencia_NomeArq2 = "";
         lV34ContagemResultadoEvidencia_NomeArq3 = "";
         lV44TFContagemResultadoEvidencia_NomeArq = "";
         lV48TFContagemResultadoEvidencia_TipoArq = "";
         H00BY2_A591ContagemResultadoEvidencia_Data = new DateTime[] {DateTime.MinValue} ;
         H00BY2_n591ContagemResultadoEvidencia_Data = new bool[] {false} ;
         H00BY2_A590ContagemResultadoEvidencia_TipoArq = new String[] {""} ;
         H00BY2_n590ContagemResultadoEvidencia_TipoArq = new bool[] {false} ;
         H00BY2_A589ContagemResultadoEvidencia_NomeArq = new String[] {""} ;
         H00BY2_n589ContagemResultadoEvidencia_NomeArq = new bool[] {false} ;
         H00BY2_A586ContagemResultadoEvidencia_Codigo = new int[1] ;
         H00BY2_A456ContagemResultado_Codigo = new int[1] ;
         H00BY3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptcontagemresultadoevidencia__default(),
            new Object[][] {
                new Object[] {
               H00BY2_A591ContagemResultadoEvidencia_Data, H00BY2_n591ContagemResultadoEvidencia_Data, H00BY2_A590ContagemResultadoEvidencia_TipoArq, H00BY2_n590ContagemResultadoEvidencia_TipoArq, H00BY2_A589ContagemResultadoEvidencia_NomeArq, H00BY2_n589ContagemResultadoEvidencia_NomeArq, H00BY2_A586ContagemResultadoEvidencia_Codigo, H00BY2_A456ContagemResultado_Codigo
               }
               , new Object[] {
               H00BY3_AGRID_nRecordCount
               }
            }
         );
         AV64Pgmname = "PromptContagemResultadoEvidencia";
         /* GeneXus formulas. */
         AV64Pgmname = "PromptContagemResultadoEvidencia";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_65 ;
      private short nGXsfl_65_idx=1 ;
      private short AV14OrderedBy ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_65_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContagemResultado_Codigo_Titleformat ;
      private short edtContagemResultadoEvidencia_Codigo_Titleformat ;
      private short edtContagemResultadoEvidencia_NomeArq_Titleformat ;
      private short edtContagemResultadoEvidencia_TipoArq_Titleformat ;
      private short edtContagemResultadoEvidencia_Data_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV8InOutContagemResultadoEvidencia_Codigo ;
      private int wcpOAV8InOutContagemResultadoEvidencia_Codigo ;
      private int subGrid_Rows ;
      private int AV36TFContagemResultado_Codigo ;
      private int AV37TFContagemResultado_Codigo_To ;
      private int AV40TFContagemResultadoEvidencia_Codigo ;
      private int AV41TFContagemResultadoEvidencia_Codigo_To ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_contagemresultadoevidencia_nomearq_Datalistupdateminimumcharacters ;
      private int Ddo_contagemresultadoevidencia_tipoarq_Datalistupdateminimumcharacters ;
      private int edtavTfcontagemresultado_codigo_Visible ;
      private int edtavTfcontagemresultado_codigo_to_Visible ;
      private int edtavTfcontagemresultadoevidencia_codigo_Visible ;
      private int edtavTfcontagemresultadoevidencia_codigo_to_Visible ;
      private int edtavTfcontagemresultadoevidencia_nomearq_Visible ;
      private int edtavTfcontagemresultadoevidencia_nomearq_sel_Visible ;
      private int edtavTfcontagemresultadoevidencia_tipoarq_Visible ;
      private int edtavTfcontagemresultadoevidencia_tipoarq_sel_Visible ;
      private int edtavTfcontagemresultadoevidencia_data_Visible ;
      private int edtavTfcontagemresultadoevidencia_data_to_Visible ;
      private int edtavDdo_contagemresultado_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultadoevidencia_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultadoevidencia_nomearqtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultadoevidencia_tipoarqtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultadoevidencia_datatitlecontrolidtoreplace_Visible ;
      private int A456ContagemResultado_Codigo ;
      private int A586ContagemResultadoEvidencia_Codigo ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV58PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavContagemresultadoevidencia_nomearq1_Visible ;
      private int edtavContagemresultadoevidencia_nomearq2_Visible ;
      private int edtavContagemresultadoevidencia_nomearq3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV59GridCurrentPage ;
      private long AV60GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String AV31InOutContagemResultadoEvidencia_NomeArq ;
      private String wcpOAV31InOutContagemResultadoEvidencia_NomeArq ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contagemresultado_codigo_Activeeventkey ;
      private String Ddo_contagemresultado_codigo_Filteredtext_get ;
      private String Ddo_contagemresultado_codigo_Filteredtextto_get ;
      private String Ddo_contagemresultadoevidencia_codigo_Activeeventkey ;
      private String Ddo_contagemresultadoevidencia_codigo_Filteredtext_get ;
      private String Ddo_contagemresultadoevidencia_codigo_Filteredtextto_get ;
      private String Ddo_contagemresultadoevidencia_nomearq_Activeeventkey ;
      private String Ddo_contagemresultadoevidencia_nomearq_Filteredtext_get ;
      private String Ddo_contagemresultadoevidencia_nomearq_Selectedvalue_get ;
      private String Ddo_contagemresultadoevidencia_tipoarq_Activeeventkey ;
      private String Ddo_contagemresultadoevidencia_tipoarq_Filteredtext_get ;
      private String Ddo_contagemresultadoevidencia_tipoarq_Selectedvalue_get ;
      private String Ddo_contagemresultadoevidencia_data_Activeeventkey ;
      private String Ddo_contagemresultadoevidencia_data_Filteredtext_get ;
      private String Ddo_contagemresultadoevidencia_data_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_65_idx="0001" ;
      private String AV32ContagemResultadoEvidencia_NomeArq1 ;
      private String AV33ContagemResultadoEvidencia_NomeArq2 ;
      private String AV34ContagemResultadoEvidencia_NomeArq3 ;
      private String AV44TFContagemResultadoEvidencia_NomeArq ;
      private String AV45TFContagemResultadoEvidencia_NomeArq_Sel ;
      private String AV48TFContagemResultadoEvidencia_TipoArq ;
      private String AV49TFContagemResultadoEvidencia_TipoArq_Sel ;
      private String AV64Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_contagemresultado_codigo_Caption ;
      private String Ddo_contagemresultado_codigo_Tooltip ;
      private String Ddo_contagemresultado_codigo_Cls ;
      private String Ddo_contagemresultado_codigo_Filteredtext_set ;
      private String Ddo_contagemresultado_codigo_Filteredtextto_set ;
      private String Ddo_contagemresultado_codigo_Dropdownoptionstype ;
      private String Ddo_contagemresultado_codigo_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultado_codigo_Sortedstatus ;
      private String Ddo_contagemresultado_codigo_Filtertype ;
      private String Ddo_contagemresultado_codigo_Sortasc ;
      private String Ddo_contagemresultado_codigo_Sortdsc ;
      private String Ddo_contagemresultado_codigo_Cleanfilter ;
      private String Ddo_contagemresultado_codigo_Rangefilterfrom ;
      private String Ddo_contagemresultado_codigo_Rangefilterto ;
      private String Ddo_contagemresultado_codigo_Searchbuttontext ;
      private String Ddo_contagemresultadoevidencia_codigo_Caption ;
      private String Ddo_contagemresultadoevidencia_codigo_Tooltip ;
      private String Ddo_contagemresultadoevidencia_codigo_Cls ;
      private String Ddo_contagemresultadoevidencia_codigo_Filteredtext_set ;
      private String Ddo_contagemresultadoevidencia_codigo_Filteredtextto_set ;
      private String Ddo_contagemresultadoevidencia_codigo_Dropdownoptionstype ;
      private String Ddo_contagemresultadoevidencia_codigo_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultadoevidencia_codigo_Sortedstatus ;
      private String Ddo_contagemresultadoevidencia_codigo_Filtertype ;
      private String Ddo_contagemresultadoevidencia_codigo_Sortasc ;
      private String Ddo_contagemresultadoevidencia_codigo_Sortdsc ;
      private String Ddo_contagemresultadoevidencia_codigo_Cleanfilter ;
      private String Ddo_contagemresultadoevidencia_codigo_Rangefilterfrom ;
      private String Ddo_contagemresultadoevidencia_codigo_Rangefilterto ;
      private String Ddo_contagemresultadoevidencia_codigo_Searchbuttontext ;
      private String Ddo_contagemresultadoevidencia_nomearq_Caption ;
      private String Ddo_contagemresultadoevidencia_nomearq_Tooltip ;
      private String Ddo_contagemresultadoevidencia_nomearq_Cls ;
      private String Ddo_contagemresultadoevidencia_nomearq_Filteredtext_set ;
      private String Ddo_contagemresultadoevidencia_nomearq_Selectedvalue_set ;
      private String Ddo_contagemresultadoevidencia_nomearq_Dropdownoptionstype ;
      private String Ddo_contagemresultadoevidencia_nomearq_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultadoevidencia_nomearq_Sortedstatus ;
      private String Ddo_contagemresultadoevidencia_nomearq_Filtertype ;
      private String Ddo_contagemresultadoevidencia_nomearq_Datalisttype ;
      private String Ddo_contagemresultadoevidencia_nomearq_Datalistproc ;
      private String Ddo_contagemresultadoevidencia_nomearq_Sortasc ;
      private String Ddo_contagemresultadoevidencia_nomearq_Sortdsc ;
      private String Ddo_contagemresultadoevidencia_nomearq_Loadingdata ;
      private String Ddo_contagemresultadoevidencia_nomearq_Cleanfilter ;
      private String Ddo_contagemresultadoevidencia_nomearq_Noresultsfound ;
      private String Ddo_contagemresultadoevidencia_nomearq_Searchbuttontext ;
      private String Ddo_contagemresultadoevidencia_tipoarq_Caption ;
      private String Ddo_contagemresultadoevidencia_tipoarq_Tooltip ;
      private String Ddo_contagemresultadoevidencia_tipoarq_Cls ;
      private String Ddo_contagemresultadoevidencia_tipoarq_Filteredtext_set ;
      private String Ddo_contagemresultadoevidencia_tipoarq_Selectedvalue_set ;
      private String Ddo_contagemresultadoevidencia_tipoarq_Dropdownoptionstype ;
      private String Ddo_contagemresultadoevidencia_tipoarq_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultadoevidencia_tipoarq_Sortedstatus ;
      private String Ddo_contagemresultadoevidencia_tipoarq_Filtertype ;
      private String Ddo_contagemresultadoevidencia_tipoarq_Datalisttype ;
      private String Ddo_contagemresultadoevidencia_tipoarq_Datalistproc ;
      private String Ddo_contagemresultadoevidencia_tipoarq_Sortasc ;
      private String Ddo_contagemresultadoevidencia_tipoarq_Sortdsc ;
      private String Ddo_contagemresultadoevidencia_tipoarq_Loadingdata ;
      private String Ddo_contagemresultadoevidencia_tipoarq_Cleanfilter ;
      private String Ddo_contagemresultadoevidencia_tipoarq_Noresultsfound ;
      private String Ddo_contagemresultadoevidencia_tipoarq_Searchbuttontext ;
      private String Ddo_contagemresultadoevidencia_data_Caption ;
      private String Ddo_contagemresultadoevidencia_data_Tooltip ;
      private String Ddo_contagemresultadoevidencia_data_Cls ;
      private String Ddo_contagemresultadoevidencia_data_Filteredtext_set ;
      private String Ddo_contagemresultadoevidencia_data_Filteredtextto_set ;
      private String Ddo_contagemresultadoevidencia_data_Dropdownoptionstype ;
      private String Ddo_contagemresultadoevidencia_data_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultadoevidencia_data_Sortedstatus ;
      private String Ddo_contagemresultadoevidencia_data_Filtertype ;
      private String Ddo_contagemresultadoevidencia_data_Sortasc ;
      private String Ddo_contagemresultadoevidencia_data_Sortdsc ;
      private String Ddo_contagemresultadoevidencia_data_Cleanfilter ;
      private String Ddo_contagemresultadoevidencia_data_Rangefilterfrom ;
      private String Ddo_contagemresultadoevidencia_data_Rangefilterto ;
      private String Ddo_contagemresultadoevidencia_data_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfcontagemresultado_codigo_Internalname ;
      private String edtavTfcontagemresultado_codigo_Jsonclick ;
      private String edtavTfcontagemresultado_codigo_to_Internalname ;
      private String edtavTfcontagemresultado_codigo_to_Jsonclick ;
      private String edtavTfcontagemresultadoevidencia_codigo_Internalname ;
      private String edtavTfcontagemresultadoevidencia_codigo_Jsonclick ;
      private String edtavTfcontagemresultadoevidencia_codigo_to_Internalname ;
      private String edtavTfcontagemresultadoevidencia_codigo_to_Jsonclick ;
      private String edtavTfcontagemresultadoevidencia_nomearq_Internalname ;
      private String edtavTfcontagemresultadoevidencia_nomearq_Jsonclick ;
      private String edtavTfcontagemresultadoevidencia_nomearq_sel_Internalname ;
      private String edtavTfcontagemresultadoevidencia_nomearq_sel_Jsonclick ;
      private String edtavTfcontagemresultadoevidencia_tipoarq_Internalname ;
      private String edtavTfcontagemresultadoevidencia_tipoarq_Jsonclick ;
      private String edtavTfcontagemresultadoevidencia_tipoarq_sel_Internalname ;
      private String edtavTfcontagemresultadoevidencia_tipoarq_sel_Jsonclick ;
      private String edtavTfcontagemresultadoevidencia_data_Internalname ;
      private String edtavTfcontagemresultadoevidencia_data_Jsonclick ;
      private String edtavTfcontagemresultadoevidencia_data_to_Internalname ;
      private String edtavTfcontagemresultadoevidencia_data_to_Jsonclick ;
      private String divDdo_contagemresultadoevidencia_dataauxdates_Internalname ;
      private String edtavDdo_contagemresultadoevidencia_dataauxdate_Internalname ;
      private String edtavDdo_contagemresultadoevidencia_dataauxdate_Jsonclick ;
      private String edtavDdo_contagemresultadoevidencia_dataauxdateto_Internalname ;
      private String edtavDdo_contagemresultadoevidencia_dataauxdateto_Jsonclick ;
      private String edtavDdo_contagemresultado_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultadoevidencia_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultadoevidencia_nomearqtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultadoevidencia_tipoarqtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultadoevidencia_datatitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtContagemResultado_Codigo_Internalname ;
      private String edtContagemResultadoEvidencia_Codigo_Internalname ;
      private String A589ContagemResultadoEvidencia_NomeArq ;
      private String edtContagemResultadoEvidencia_NomeArq_Internalname ;
      private String A590ContagemResultadoEvidencia_TipoArq ;
      private String edtContagemResultadoEvidencia_TipoArq_Internalname ;
      private String edtContagemResultadoEvidencia_Data_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV32ContagemResultadoEvidencia_NomeArq1 ;
      private String lV33ContagemResultadoEvidencia_NomeArq2 ;
      private String lV34ContagemResultadoEvidencia_NomeArq3 ;
      private String lV44TFContagemResultadoEvidencia_NomeArq ;
      private String lV48TFContagemResultadoEvidencia_TipoArq ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavContagemresultadoevidencia_nomearq1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavContagemresultadoevidencia_nomearq2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavContagemresultadoevidencia_nomearq3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_contagemresultado_codigo_Internalname ;
      private String Ddo_contagemresultadoevidencia_codigo_Internalname ;
      private String Ddo_contagemresultadoevidencia_nomearq_Internalname ;
      private String Ddo_contagemresultadoevidencia_tipoarq_Internalname ;
      private String Ddo_contagemresultadoevidencia_data_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtContagemResultado_Codigo_Title ;
      private String edtContagemResultadoEvidencia_Codigo_Title ;
      private String edtContagemResultadoEvidencia_NomeArq_Title ;
      private String edtContagemResultadoEvidencia_TipoArq_Title ;
      private String edtContagemResultadoEvidencia_Data_Title ;
      private String edtavSelect_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavContagemresultadoevidencia_nomearq1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String edtavContagemresultadoevidencia_nomearq2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavContagemresultadoevidencia_nomearq3_Jsonclick ;
      private String sGXsfl_65_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtContagemResultado_Codigo_Jsonclick ;
      private String edtContagemResultadoEvidencia_Codigo_Jsonclick ;
      private String edtContagemResultadoEvidencia_NomeArq_Jsonclick ;
      private String edtContagemResultadoEvidencia_TipoArq_Jsonclick ;
      private String edtContagemResultadoEvidencia_Data_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV52TFContagemResultadoEvidencia_Data ;
      private DateTime AV53TFContagemResultadoEvidencia_Data_To ;
      private DateTime A591ContagemResultadoEvidencia_Data ;
      private DateTime AV54DDO_ContagemResultadoEvidencia_DataAuxDate ;
      private DateTime AV55DDO_ContagemResultadoEvidencia_DataAuxDateTo ;
      private bool entryPointCalled ;
      private bool AV15OrderedDsc ;
      private bool AV19DynamicFiltersEnabled2 ;
      private bool AV23DynamicFiltersEnabled3 ;
      private bool AV28DynamicFiltersIgnoreFirst ;
      private bool AV27DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contagemresultado_codigo_Includesortasc ;
      private bool Ddo_contagemresultado_codigo_Includesortdsc ;
      private bool Ddo_contagemresultado_codigo_Includefilter ;
      private bool Ddo_contagemresultado_codigo_Filterisrange ;
      private bool Ddo_contagemresultado_codigo_Includedatalist ;
      private bool Ddo_contagemresultadoevidencia_codigo_Includesortasc ;
      private bool Ddo_contagemresultadoevidencia_codigo_Includesortdsc ;
      private bool Ddo_contagemresultadoevidencia_codigo_Includefilter ;
      private bool Ddo_contagemresultadoevidencia_codigo_Filterisrange ;
      private bool Ddo_contagemresultadoevidencia_codigo_Includedatalist ;
      private bool Ddo_contagemresultadoevidencia_nomearq_Includesortasc ;
      private bool Ddo_contagemresultadoevidencia_nomearq_Includesortdsc ;
      private bool Ddo_contagemresultadoevidencia_nomearq_Includefilter ;
      private bool Ddo_contagemresultadoevidencia_nomearq_Filterisrange ;
      private bool Ddo_contagemresultadoevidencia_nomearq_Includedatalist ;
      private bool Ddo_contagemresultadoevidencia_tipoarq_Includesortasc ;
      private bool Ddo_contagemresultadoevidencia_tipoarq_Includesortdsc ;
      private bool Ddo_contagemresultadoevidencia_tipoarq_Includefilter ;
      private bool Ddo_contagemresultadoevidencia_tipoarq_Filterisrange ;
      private bool Ddo_contagemresultadoevidencia_tipoarq_Includedatalist ;
      private bool Ddo_contagemresultadoevidencia_data_Includesortasc ;
      private bool Ddo_contagemresultadoevidencia_data_Includesortdsc ;
      private bool Ddo_contagemresultadoevidencia_data_Includefilter ;
      private bool Ddo_contagemresultadoevidencia_data_Filterisrange ;
      private bool Ddo_contagemresultadoevidencia_data_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n589ContagemResultadoEvidencia_NomeArq ;
      private bool n590ContagemResultadoEvidencia_TipoArq ;
      private bool n591ContagemResultadoEvidencia_Data ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV29Select_IsBlob ;
      private String AV16DynamicFiltersSelector1 ;
      private String AV20DynamicFiltersSelector2 ;
      private String AV24DynamicFiltersSelector3 ;
      private String AV38ddo_ContagemResultado_CodigoTitleControlIdToReplace ;
      private String AV42ddo_ContagemResultadoEvidencia_CodigoTitleControlIdToReplace ;
      private String AV46ddo_ContagemResultadoEvidencia_NomeArqTitleControlIdToReplace ;
      private String AV50ddo_ContagemResultadoEvidencia_TipoArqTitleControlIdToReplace ;
      private String AV56ddo_ContagemResultadoEvidencia_DataTitleControlIdToReplace ;
      private String AV63Select_GXI ;
      private String AV29Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutContagemResultadoEvidencia_Codigo ;
      private String aP1_InOutContagemResultadoEvidencia_NomeArq ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private DateTime[] H00BY2_A591ContagemResultadoEvidencia_Data ;
      private bool[] H00BY2_n591ContagemResultadoEvidencia_Data ;
      private String[] H00BY2_A590ContagemResultadoEvidencia_TipoArq ;
      private bool[] H00BY2_n590ContagemResultadoEvidencia_TipoArq ;
      private String[] H00BY2_A589ContagemResultadoEvidencia_NomeArq ;
      private bool[] H00BY2_n589ContagemResultadoEvidencia_NomeArq ;
      private int[] H00BY2_A586ContagemResultadoEvidencia_Codigo ;
      private int[] H00BY2_A456ContagemResultado_Codigo ;
      private long[] H00BY3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV35ContagemResultado_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV39ContagemResultadoEvidencia_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV43ContagemResultadoEvidencia_NomeArqTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV47ContagemResultadoEvidencia_TipoArqTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV51ContagemResultadoEvidencia_DataTitleFilterData ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV13GridStateDynamicFilter ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV57DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class promptcontagemresultadoevidencia__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00BY2( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             String AV32ContagemResultadoEvidencia_NomeArq1 ,
                                             bool AV19DynamicFiltersEnabled2 ,
                                             String AV20DynamicFiltersSelector2 ,
                                             String AV33ContagemResultadoEvidencia_NomeArq2 ,
                                             bool AV23DynamicFiltersEnabled3 ,
                                             String AV24DynamicFiltersSelector3 ,
                                             String AV34ContagemResultadoEvidencia_NomeArq3 ,
                                             int AV36TFContagemResultado_Codigo ,
                                             int AV37TFContagemResultado_Codigo_To ,
                                             int AV40TFContagemResultadoEvidencia_Codigo ,
                                             int AV41TFContagemResultadoEvidencia_Codigo_To ,
                                             String AV45TFContagemResultadoEvidencia_NomeArq_Sel ,
                                             String AV44TFContagemResultadoEvidencia_NomeArq ,
                                             String AV49TFContagemResultadoEvidencia_TipoArq_Sel ,
                                             String AV48TFContagemResultadoEvidencia_TipoArq ,
                                             DateTime AV52TFContagemResultadoEvidencia_Data ,
                                             DateTime AV53TFContagemResultadoEvidencia_Data_To ,
                                             String A589ContagemResultadoEvidencia_NomeArq ,
                                             int A456ContagemResultado_Codigo ,
                                             int A586ContagemResultadoEvidencia_Codigo ,
                                             String A590ContagemResultadoEvidencia_TipoArq ,
                                             DateTime A591ContagemResultadoEvidencia_Data ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [18] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [ContagemResultadoEvidencia_Data], [ContagemResultadoEvidencia_TipoArq], [ContagemResultadoEvidencia_NomeArq], [ContagemResultadoEvidencia_Codigo], [ContagemResultado_Codigo]";
         sFromString = " FROM [ContagemResultadoEvidencia] WITH (NOLOCK)";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32ContagemResultadoEvidencia_NomeArq1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_NomeArq] like '%' + @lV32ContagemResultadoEvidencia_NomeArq1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_NomeArq] like '%' + @lV32ContagemResultadoEvidencia_NomeArq1 + '%')";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33ContagemResultadoEvidencia_NomeArq2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_NomeArq] like '%' + @lV33ContagemResultadoEvidencia_NomeArq2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_NomeArq] like '%' + @lV33ContagemResultadoEvidencia_NomeArq2 + '%')";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34ContagemResultadoEvidencia_NomeArq3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_NomeArq] like '%' + @lV34ContagemResultadoEvidencia_NomeArq3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_NomeArq] like '%' + @lV34ContagemResultadoEvidencia_NomeArq3 + '%')";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ! (0==AV36TFContagemResultado_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_Codigo] >= @AV36TFContagemResultado_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_Codigo] >= @AV36TFContagemResultado_Codigo)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ! (0==AV37TFContagemResultado_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_Codigo] <= @AV37TFContagemResultado_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_Codigo] <= @AV37TFContagemResultado_Codigo_To)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ! (0==AV40TFContagemResultadoEvidencia_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_Codigo] >= @AV40TFContagemResultadoEvidencia_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_Codigo] >= @AV40TFContagemResultadoEvidencia_Codigo)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! (0==AV41TFContagemResultadoEvidencia_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_Codigo] <= @AV41TFContagemResultadoEvidencia_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_Codigo] <= @AV41TFContagemResultadoEvidencia_Codigo_To)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV45TFContagemResultadoEvidencia_NomeArq_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TFContagemResultadoEvidencia_NomeArq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_NomeArq] like @lV44TFContagemResultadoEvidencia_NomeArq)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_NomeArq] like @lV44TFContagemResultadoEvidencia_NomeArq)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45TFContagemResultadoEvidencia_NomeArq_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_NomeArq] = @AV45TFContagemResultadoEvidencia_NomeArq_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_NomeArq] = @AV45TFContagemResultadoEvidencia_NomeArq_Sel)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV49TFContagemResultadoEvidencia_TipoArq_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TFContagemResultadoEvidencia_TipoArq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_TipoArq] like @lV48TFContagemResultadoEvidencia_TipoArq)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_TipoArq] like @lV48TFContagemResultadoEvidencia_TipoArq)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49TFContagemResultadoEvidencia_TipoArq_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_TipoArq] = @AV49TFContagemResultadoEvidencia_TipoArq_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_TipoArq] = @AV49TFContagemResultadoEvidencia_TipoArq_Sel)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ! (DateTime.MinValue==AV52TFContagemResultadoEvidencia_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_Data] >= @AV52TFContagemResultadoEvidencia_Data)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_Data] >= @AV52TFContagemResultadoEvidencia_Data)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( ! (DateTime.MinValue==AV53TFContagemResultadoEvidencia_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_Data] <= @AV53TFContagemResultadoEvidencia_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_Data] <= @AV53TFContagemResultadoEvidencia_Data_To)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultadoEvidencia_NomeArq]";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultadoEvidencia_NomeArq] DESC";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultado_Codigo]";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultado_Codigo] DESC";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultadoEvidencia_Codigo]";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultadoEvidencia_Codigo] DESC";
         }
         else if ( ( AV14OrderedBy == 4 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultadoEvidencia_TipoArq]";
         }
         else if ( ( AV14OrderedBy == 4 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultadoEvidencia_TipoArq] DESC";
         }
         else if ( ( AV14OrderedBy == 5 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultadoEvidencia_Data]";
         }
         else if ( ( AV14OrderedBy == 5 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultadoEvidencia_Data] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultadoEvidencia_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00BY3( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             String AV32ContagemResultadoEvidencia_NomeArq1 ,
                                             bool AV19DynamicFiltersEnabled2 ,
                                             String AV20DynamicFiltersSelector2 ,
                                             String AV33ContagemResultadoEvidencia_NomeArq2 ,
                                             bool AV23DynamicFiltersEnabled3 ,
                                             String AV24DynamicFiltersSelector3 ,
                                             String AV34ContagemResultadoEvidencia_NomeArq3 ,
                                             int AV36TFContagemResultado_Codigo ,
                                             int AV37TFContagemResultado_Codigo_To ,
                                             int AV40TFContagemResultadoEvidencia_Codigo ,
                                             int AV41TFContagemResultadoEvidencia_Codigo_To ,
                                             String AV45TFContagemResultadoEvidencia_NomeArq_Sel ,
                                             String AV44TFContagemResultadoEvidencia_NomeArq ,
                                             String AV49TFContagemResultadoEvidencia_TipoArq_Sel ,
                                             String AV48TFContagemResultadoEvidencia_TipoArq ,
                                             DateTime AV52TFContagemResultadoEvidencia_Data ,
                                             DateTime AV53TFContagemResultadoEvidencia_Data_To ,
                                             String A589ContagemResultadoEvidencia_NomeArq ,
                                             int A456ContagemResultado_Codigo ,
                                             int A586ContagemResultadoEvidencia_Codigo ,
                                             String A590ContagemResultadoEvidencia_TipoArq ,
                                             DateTime A591ContagemResultadoEvidencia_Data ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [13] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [ContagemResultadoEvidencia] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32ContagemResultadoEvidencia_NomeArq1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_NomeArq] like '%' + @lV32ContagemResultadoEvidencia_NomeArq1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_NomeArq] like '%' + @lV32ContagemResultadoEvidencia_NomeArq1 + '%')";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( AV19DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33ContagemResultadoEvidencia_NomeArq2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_NomeArq] like '%' + @lV33ContagemResultadoEvidencia_NomeArq2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_NomeArq] like '%' + @lV33ContagemResultadoEvidencia_NomeArq2 + '%')";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( AV23DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV24DynamicFiltersSelector3, "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34ContagemResultadoEvidencia_NomeArq3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_NomeArq] like '%' + @lV34ContagemResultadoEvidencia_NomeArq3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_NomeArq] like '%' + @lV34ContagemResultadoEvidencia_NomeArq3 + '%')";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ! (0==AV36TFContagemResultado_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_Codigo] >= @AV36TFContagemResultado_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_Codigo] >= @AV36TFContagemResultado_Codigo)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ! (0==AV37TFContagemResultado_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_Codigo] <= @AV37TFContagemResultado_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_Codigo] <= @AV37TFContagemResultado_Codigo_To)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ! (0==AV40TFContagemResultadoEvidencia_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_Codigo] >= @AV40TFContagemResultadoEvidencia_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_Codigo] >= @AV40TFContagemResultadoEvidencia_Codigo)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! (0==AV41TFContagemResultadoEvidencia_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_Codigo] <= @AV41TFContagemResultadoEvidencia_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_Codigo] <= @AV41TFContagemResultadoEvidencia_Codigo_To)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV45TFContagemResultadoEvidencia_NomeArq_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TFContagemResultadoEvidencia_NomeArq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_NomeArq] like @lV44TFContagemResultadoEvidencia_NomeArq)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_NomeArq] like @lV44TFContagemResultadoEvidencia_NomeArq)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45TFContagemResultadoEvidencia_NomeArq_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_NomeArq] = @AV45TFContagemResultadoEvidencia_NomeArq_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_NomeArq] = @AV45TFContagemResultadoEvidencia_NomeArq_Sel)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV49TFContagemResultadoEvidencia_TipoArq_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TFContagemResultadoEvidencia_TipoArq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_TipoArq] like @lV48TFContagemResultadoEvidencia_TipoArq)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_TipoArq] like @lV48TFContagemResultadoEvidencia_TipoArq)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49TFContagemResultadoEvidencia_TipoArq_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_TipoArq] = @AV49TFContagemResultadoEvidencia_TipoArq_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_TipoArq] = @AV49TFContagemResultadoEvidencia_TipoArq_Sel)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! (DateTime.MinValue==AV52TFContagemResultadoEvidencia_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_Data] >= @AV52TFContagemResultadoEvidencia_Data)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_Data] >= @AV52TFContagemResultadoEvidencia_Data)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( ! (DateTime.MinValue==AV53TFContagemResultadoEvidencia_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_Data] <= @AV53TFContagemResultadoEvidencia_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_Data] <= @AV53TFContagemResultadoEvidencia_Data_To)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 4 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 4 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 5 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 5 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00BY2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (DateTime)dynConstraints[22] , (short)dynConstraints[23] , (bool)dynConstraints[24] );
               case 1 :
                     return conditional_H00BY3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (DateTime)dynConstraints[22] , (short)dynConstraints[23] , (bool)dynConstraints[24] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00BY2 ;
          prmH00BY2 = new Object[] {
          new Object[] {"@lV32ContagemResultadoEvidencia_NomeArq1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV33ContagemResultadoEvidencia_NomeArq2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV34ContagemResultadoEvidencia_NomeArq3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV36TFContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV37TFContagemResultado_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV40TFContagemResultadoEvidencia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV41TFContagemResultadoEvidencia_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV44TFContagemResultadoEvidencia_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@AV45TFContagemResultadoEvidencia_NomeArq_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV48TFContagemResultadoEvidencia_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@AV49TFContagemResultadoEvidencia_TipoArq_Sel",SqlDbType.Char,10,0} ,
          new Object[] {"@AV52TFContagemResultadoEvidencia_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV53TFContagemResultadoEvidencia_Data_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00BY3 ;
          prmH00BY3 = new Object[] {
          new Object[] {"@lV32ContagemResultadoEvidencia_NomeArq1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV33ContagemResultadoEvidencia_NomeArq2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV34ContagemResultadoEvidencia_NomeArq3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV36TFContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV37TFContagemResultado_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV40TFContagemResultadoEvidencia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV41TFContagemResultadoEvidencia_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV44TFContagemResultadoEvidencia_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@AV45TFContagemResultadoEvidencia_NomeArq_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV48TFContagemResultadoEvidencia_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@AV49TFContagemResultadoEvidencia_TipoArq_Sel",SqlDbType.Char,10,0} ,
          new Object[] {"@AV52TFContagemResultadoEvidencia_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV53TFContagemResultadoEvidencia_Data_To",SqlDbType.DateTime,8,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00BY2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BY2,11,0,true,false )
             ,new CursorDef("H00BY3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BY3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDateTime(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 10) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[29]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[30]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[18]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[24]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[25]);
                }
                return;
       }
    }

 }

}
