/*
               File: ContratoServicosUnidConversaoGeneral
        Description: Contrato Servicos Unid Conversao General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:29:38.48
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratoservicosunidconversaogeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contratoservicosunidconversaogeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contratoservicosunidconversaogeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratoServicos_Codigo ,
                           int aP1_ContratoServicosUnidConversao_Codigo )
      {
         this.A160ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         this.A2110ContratoServicosUnidConversao_Codigo = aP1_ContratoServicosUnidConversao_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A160ContratoServicos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
                  A2110ContratoServicosUnidConversao_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2110ContratoServicosUnidConversao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2110ContratoServicosUnidConversao_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A160ContratoServicos_Codigo,(int)A2110ContratoServicosUnidConversao_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PASQ2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV17Pgmname = "ContratoServicosUnidConversaoGeneral";
               context.Gx_err = 0;
               /* Using cursor H00SQ2 */
               pr_default.execute(0, new Object[] {A2110ContratoServicosUnidConversao_Codigo});
               A2112ContratoServicosUnidConversao_Sigla = H00SQ2_A2112ContratoServicosUnidConversao_Sigla[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2112ContratoServicosUnidConversao_Sigla", A2112ContratoServicosUnidConversao_Sigla);
               n2112ContratoServicosUnidConversao_Sigla = H00SQ2_n2112ContratoServicosUnidConversao_Sigla[0];
               A2111ContratoServicosUnidConversao_Nome = H00SQ2_A2111ContratoServicosUnidConversao_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2111ContratoServicosUnidConversao_Nome", A2111ContratoServicosUnidConversao_Nome);
               n2111ContratoServicosUnidConversao_Nome = H00SQ2_n2111ContratoServicosUnidConversao_Nome[0];
               pr_default.close(0);
               WSSQ2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contrato Servicos Unid Conversao General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203120293854");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratoservicosunidconversaogeneral.aspx") + "?" + UrlEncode("" +A160ContratoServicos_Codigo) + "," + UrlEncode("" +A2110ContratoServicosUnidConversao_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA160ContratoServicos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA2110ContratoServicosUnidConversao_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA2110ContratoServicosUnidConversao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTRATOSERVICOS_UNIDADECONTRATADA", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14ContratoServicos_UnidadeContratada), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSUNIDCONVERSAO_CONVERSAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A2114ContratoServicosUnidConversao_Conversao, "ZZ,ZZZ,ZZ9.999")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormSQ2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contratoservicosunidconversaogeneral.js", "?20203120293855");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContratoServicosUnidConversaoGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contrato Servicos Unid Conversao General" ;
      }

      protected void WBSQ0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contratoservicosunidconversaogeneral.aspx");
            }
            wb_table1_2_SQ2( true) ;
         }
         else
         {
            wb_table1_2_SQ2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_SQ2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicos_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A160ContratoServicos_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A160ContratoServicos_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicos_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtContratoServicos_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoServicosUnidConversaoGeneral.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicosUnidConversao_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A2110ContratoServicosUnidConversao_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A2110ContratoServicosUnidConversao_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosUnidConversao_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtContratoServicosUnidConversao_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoServicosUnidConversaoGeneral.htm");
         }
         wbLoad = true;
      }

      protected void STARTSQ2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contrato Servicos Unid Conversao General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPSQ0( ) ;
            }
         }
      }

      protected void WSSQ2( )
      {
         STARTSQ2( ) ;
         EVTSQ2( ) ;
      }

      protected void EVTSQ2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPSQ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPSQ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11SQ2 */
                                    E11SQ2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPSQ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12SQ2 */
                                    E12SQ2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPSQ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13SQ2 */
                                    E13SQ2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPSQ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14SQ2 */
                                    E14SQ2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPSQ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPSQ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WESQ2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormSQ2( ) ;
            }
         }
      }

      protected void PASQ2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFSQ2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV17Pgmname = "ContratoServicosUnidConversaoGeneral";
         context.Gx_err = 0;
      }

      protected void RFSQ2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00SQ3 */
            pr_default.execute(1, new Object[] {A160ContratoServicos_Codigo, A2110ContratoServicosUnidConversao_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A2114ContratoServicosUnidConversao_Conversao = H00SQ3_A2114ContratoServicosUnidConversao_Conversao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2114ContratoServicosUnidConversao_Conversao", StringUtil.LTrim( StringUtil.Str( A2114ContratoServicosUnidConversao_Conversao, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSUNIDCONVERSAO_CONVERSAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A2114ContratoServicosUnidConversao_Conversao, "ZZ,ZZZ,ZZ9.999")));
               /* Execute user event: E12SQ2 */
               E12SQ2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(1);
            WBSQ0( ) ;
         }
      }

      protected void STRUPSQ0( )
      {
         /* Before Start, stand alone formulas. */
         AV17Pgmname = "ContratoServicosUnidConversaoGeneral";
         context.Gx_err = 0;
         /* Using cursor H00SQ4 */
         pr_default.execute(2, new Object[] {A2110ContratoServicosUnidConversao_Codigo});
         A2112ContratoServicosUnidConversao_Sigla = H00SQ4_A2112ContratoServicosUnidConversao_Sigla[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2112ContratoServicosUnidConversao_Sigla", A2112ContratoServicosUnidConversao_Sigla);
         n2112ContratoServicosUnidConversao_Sigla = H00SQ4_n2112ContratoServicosUnidConversao_Sigla[0];
         A2111ContratoServicosUnidConversao_Nome = H00SQ4_A2111ContratoServicosUnidConversao_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2111ContratoServicosUnidConversao_Nome", A2111ContratoServicosUnidConversao_Nome);
         n2111ContratoServicosUnidConversao_Nome = H00SQ4_n2111ContratoServicosUnidConversao_Nome[0];
         pr_default.close(2);
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11SQ2 */
         E11SQ2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A2111ContratoServicosUnidConversao_Nome = StringUtil.Upper( cgiGet( edtContratoServicosUnidConversao_Nome_Internalname));
            n2111ContratoServicosUnidConversao_Nome = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2111ContratoServicosUnidConversao_Nome", A2111ContratoServicosUnidConversao_Nome);
            A2112ContratoServicosUnidConversao_Sigla = StringUtil.Upper( cgiGet( edtContratoServicosUnidConversao_Sigla_Internalname));
            n2112ContratoServicosUnidConversao_Sigla = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2112ContratoServicosUnidConversao_Sigla", A2112ContratoServicosUnidConversao_Sigla);
            A2114ContratoServicosUnidConversao_Conversao = context.localUtil.CToN( cgiGet( edtContratoServicosUnidConversao_Conversao_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2114ContratoServicosUnidConversao_Conversao", StringUtil.LTrim( StringUtil.Str( A2114ContratoServicosUnidConversao_Conversao, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATOSERVICOSUNIDCONVERSAO_CONVERSAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A2114ContratoServicosUnidConversao_Conversao, "ZZ,ZZZ,ZZ9.999")));
            /* Read saved values. */
            wcpOA160ContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA160ContratoServicos_Codigo"), ",", "."));
            wcpOA2110ContratoServicosUnidConversao_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA2110ContratoServicosUnidConversao_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11SQ2 */
         E11SQ2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11SQ2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E12SQ2( )
      {
         /* Load Routine */
         edtContratoServicosUnidConversao_Nome_Link = formatLink("viewunidademedicao.aspx") + "?" + UrlEncode("" +A2110ContratoServicosUnidConversao_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoServicosUnidConversao_Nome_Internalname, "Link", edtContratoServicosUnidConversao_Nome_Link);
         edtContratoServicos_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoServicos_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicos_Codigo_Visible), 5, 0)));
         edtContratoServicosUnidConversao_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoServicosUnidConversao_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosUnidConversao_Codigo_Visible), 5, 0)));
         if ( ! ( ( AV6WWPContext.gxTpr_Update ) ) )
         {
            bttBtnupdate_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Enabled), 5, 0)));
         }
         if ( ! ( ( AV6WWPContext.gxTpr_Delete ) ) )
         {
            bttBtndelete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Enabled), 5, 0)));
         }
      }

      protected void E13SQ2( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("contratoservicosunidconversao.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A160ContratoServicos_Codigo) + "," + UrlEncode("" +A2110ContratoServicosUnidConversao_Codigo) + "," + UrlEncode("" +AV13Contrato_Codigo) + "," + UrlEncode("" +AV14ContratoServicos_UnidadeContratada);
         context.wjLocDisableFrm = 1;
      }

      protected void E14SQ2( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("contratoservicosunidconversao.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A160ContratoServicos_Codigo) + "," + UrlEncode("" +A2110ContratoServicosUnidConversao_Codigo) + "," + UrlEncode("" +AV13Contrato_Codigo) + "," + UrlEncode("" +AV14ContratoServicos_UnidadeContratada);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV17Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = false;
         AV9TrnContext.gxTpr_Callerurl = AV12HTTPRequest.ScriptName+"?"+AV12HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "ContratoServicosUnidConversao";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "ContratoServicos_Codigo";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7ContratoServicos_Codigo), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "ContratoServicosUnidConversao_Codigo";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV8ContratoServicosUnidConversao_Codigo), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV11Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_SQ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_SQ2( true) ;
         }
         else
         {
            wb_table2_8_SQ2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_SQ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_26_SQ2( true) ;
         }
         else
         {
            wb_table3_26_SQ2( false) ;
         }
         return  ;
      }

      protected void wb_table3_26_SQ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_SQ2e( true) ;
         }
         else
         {
            wb_table1_2_SQ2e( false) ;
         }
      }

      protected void wb_table3_26_SQ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, 1, bttBtnupdate_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicosUnidConversaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, 1, bttBtndelete_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicosUnidConversaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_26_SQ2e( true) ;
         }
         else
         {
            wb_table3_26_SQ2e( false) ;
         }
      }

      protected void wb_table2_8_SQ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosunidconversao_nome_Internalname, "Nome", "", "", lblTextblockcontratoservicosunidconversao_nome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosUnidConversaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicosUnidConversao_Nome_Internalname, StringUtil.RTrim( A2111ContratoServicosUnidConversao_Nome), StringUtil.RTrim( context.localUtil.Format( A2111ContratoServicosUnidConversao_Nome, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtContratoServicosUnidConversao_Nome_Link, "", "", "", edtContratoServicosUnidConversao_Nome_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ContratoServicosUnidConversaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosunidconversao_sigla_Internalname, "Sigla", "", "", lblTextblockcontratoservicosunidconversao_sigla_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosUnidConversaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicosUnidConversao_Sigla_Internalname, StringUtil.RTrim( A2112ContratoServicosUnidConversao_Sigla), StringUtil.RTrim( context.localUtil.Format( A2112ContratoServicosUnidConversao_Sigla, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosUnidConversao_Sigla_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Sigla", "left", true, "HLP_ContratoServicosUnidConversaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosunidconversao_conversao_Internalname, "Convers�o", "", "", lblTextblockcontratoservicosunidconversao_conversao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosUnidConversaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicosUnidConversao_Conversao_Internalname, StringUtil.LTrim( StringUtil.NToC( A2114ContratoServicosUnidConversao_Conversao, 14, 5, ",", "")), context.localUtil.Format( A2114ContratoServicosUnidConversao_Conversao, "ZZ,ZZZ,ZZ9.999"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosUnidConversao_Conversao_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_ContratoServicosUnidConversaoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_SQ2e( true) ;
         }
         else
         {
            wb_table2_8_SQ2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A160ContratoServicos_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
         A2110ContratoServicosUnidConversao_Codigo = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2110ContratoServicosUnidConversao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2110ContratoServicosUnidConversao_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PASQ2( ) ;
         WSSQ2( ) ;
         WESQ2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA160ContratoServicos_Codigo = (String)((String)getParm(obj,0));
         sCtrlA2110ContratoServicosUnidConversao_Codigo = (String)((String)getParm(obj,1));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PASQ2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contratoservicosunidconversaogeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PASQ2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A160ContratoServicos_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
            A2110ContratoServicosUnidConversao_Codigo = Convert.ToInt32(getParm(obj,3));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2110ContratoServicosUnidConversao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2110ContratoServicosUnidConversao_Codigo), 6, 0)));
         }
         wcpOA160ContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA160ContratoServicos_Codigo"), ",", "."));
         wcpOA2110ContratoServicosUnidConversao_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA2110ContratoServicosUnidConversao_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A160ContratoServicos_Codigo != wcpOA160ContratoServicos_Codigo ) || ( A2110ContratoServicosUnidConversao_Codigo != wcpOA2110ContratoServicosUnidConversao_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA160ContratoServicos_Codigo = A160ContratoServicos_Codigo;
         wcpOA2110ContratoServicosUnidConversao_Codigo = A2110ContratoServicosUnidConversao_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA160ContratoServicos_Codigo = cgiGet( sPrefix+"A160ContratoServicos_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA160ContratoServicos_Codigo) > 0 )
         {
            A160ContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA160ContratoServicos_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A160ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A160ContratoServicos_Codigo), 6, 0)));
         }
         else
         {
            A160ContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A160ContratoServicos_Codigo_PARM"), ",", "."));
         }
         sCtrlA2110ContratoServicosUnidConversao_Codigo = cgiGet( sPrefix+"A2110ContratoServicosUnidConversao_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA2110ContratoServicosUnidConversao_Codigo) > 0 )
         {
            A2110ContratoServicosUnidConversao_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA2110ContratoServicosUnidConversao_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2110ContratoServicosUnidConversao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2110ContratoServicosUnidConversao_Codigo), 6, 0)));
         }
         else
         {
            A2110ContratoServicosUnidConversao_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A2110ContratoServicosUnidConversao_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PASQ2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSSQ2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSSQ2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A160ContratoServicos_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A160ContratoServicos_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA160ContratoServicos_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A160ContratoServicos_Codigo_CTRL", StringUtil.RTrim( sCtrlA160ContratoServicos_Codigo));
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"A2110ContratoServicosUnidConversao_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2110ContratoServicosUnidConversao_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA2110ContratoServicosUnidConversao_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A2110ContratoServicosUnidConversao_Codigo_CTRL", StringUtil.RTrim( sCtrlA2110ContratoServicosUnidConversao_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WESQ2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203120293875");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("contratoservicosunidconversaogeneral.js", "?20203120293875");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontratoservicosunidconversao_nome_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOSUNIDCONVERSAO_NOME";
         edtContratoServicosUnidConversao_Nome_Internalname = sPrefix+"CONTRATOSERVICOSUNIDCONVERSAO_NOME";
         lblTextblockcontratoservicosunidconversao_sigla_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOSUNIDCONVERSAO_SIGLA";
         edtContratoServicosUnidConversao_Sigla_Internalname = sPrefix+"CONTRATOSERVICOSUNIDCONVERSAO_SIGLA";
         lblTextblockcontratoservicosunidconversao_conversao_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOSUNIDCONVERSAO_CONVERSAO";
         edtContratoServicosUnidConversao_Conversao_Internalname = sPrefix+"CONTRATOSERVICOSUNIDCONVERSAO_CONVERSAO";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         edtContratoServicos_Codigo_Internalname = sPrefix+"CONTRATOSERVICOS_CODIGO";
         edtContratoServicosUnidConversao_Codigo_Internalname = sPrefix+"CONTRATOSERVICOSUNIDCONVERSAO_CODIGO";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtContratoServicosUnidConversao_Conversao_Jsonclick = "";
         edtContratoServicosUnidConversao_Sigla_Jsonclick = "";
         edtContratoServicosUnidConversao_Nome_Jsonclick = "";
         bttBtndelete_Enabled = 1;
         bttBtnupdate_Enabled = 1;
         edtContratoServicosUnidConversao_Nome_Link = "";
         edtContratoServicosUnidConversao_Codigo_Jsonclick = "";
         edtContratoServicosUnidConversao_Codigo_Visible = 1;
         edtContratoServicos_Codigo_Jsonclick = "";
         edtContratoServicos_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13SQ2',iparms:[{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A2110ContratoServicosUnidConversao_Codigo',fld:'CONTRATOSERVICOSUNIDCONVERSAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV13Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV14ContratoServicos_UnidadeContratada',fld:'vCONTRATOSERVICOS_UNIDADECONTRATADA',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV14ContratoServicos_UnidadeContratada',fld:'vCONTRATOSERVICOS_UNIDADECONTRATADA',pic:'ZZZZZ9',nv:0},{av:'AV13Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DODELETE'","{handler:'E14SQ2',iparms:[{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A2110ContratoServicosUnidConversao_Codigo',fld:'CONTRATOSERVICOSUNIDCONVERSAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV13Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV14ContratoServicos_UnidadeContratada',fld:'vCONTRATOSERVICOS_UNIDADECONTRATADA',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV14ContratoServicos_UnidadeContratada',fld:'vCONTRATOSERVICOS_UNIDADECONTRATADA',pic:'ZZZZZ9',nv:0},{av:'AV13Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV17Pgmname = "";
         scmdbuf = "";
         H00SQ2_A2112ContratoServicosUnidConversao_Sigla = new String[] {""} ;
         H00SQ2_n2112ContratoServicosUnidConversao_Sigla = new bool[] {false} ;
         H00SQ2_A2111ContratoServicosUnidConversao_Nome = new String[] {""} ;
         H00SQ2_n2111ContratoServicosUnidConversao_Nome = new bool[] {false} ;
         A2112ContratoServicosUnidConversao_Sigla = "";
         A2111ContratoServicosUnidConversao_Nome = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         H00SQ3_A160ContratoServicos_Codigo = new int[1] ;
         H00SQ3_A2110ContratoServicosUnidConversao_Codigo = new int[1] ;
         H00SQ3_A2114ContratoServicosUnidConversao_Conversao = new decimal[1] ;
         H00SQ3_A2112ContratoServicosUnidConversao_Sigla = new String[] {""} ;
         H00SQ3_n2112ContratoServicosUnidConversao_Sigla = new bool[] {false} ;
         H00SQ3_A2111ContratoServicosUnidConversao_Nome = new String[] {""} ;
         H00SQ3_n2111ContratoServicosUnidConversao_Nome = new bool[] {false} ;
         H00SQ4_A2112ContratoServicosUnidConversao_Sigla = new String[] {""} ;
         H00SQ4_n2112ContratoServicosUnidConversao_Sigla = new bool[] {false} ;
         H00SQ4_A2111ContratoServicosUnidConversao_Nome = new String[] {""} ;
         H00SQ4_n2111ContratoServicosUnidConversao_Nome = new bool[] {false} ;
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV12HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV11Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         lblTextblockcontratoservicosunidconversao_nome_Jsonclick = "";
         lblTextblockcontratoservicosunidconversao_sigla_Jsonclick = "";
         lblTextblockcontratoservicosunidconversao_conversao_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA160ContratoServicos_Codigo = "";
         sCtrlA2110ContratoServicosUnidConversao_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratoservicosunidconversaogeneral__default(),
            new Object[][] {
                new Object[] {
               H00SQ2_A2112ContratoServicosUnidConversao_Sigla, H00SQ2_n2112ContratoServicosUnidConversao_Sigla, H00SQ2_A2111ContratoServicosUnidConversao_Nome, H00SQ2_n2111ContratoServicosUnidConversao_Nome
               }
               , new Object[] {
               H00SQ3_A160ContratoServicos_Codigo, H00SQ3_A2110ContratoServicosUnidConversao_Codigo, H00SQ3_A2114ContratoServicosUnidConversao_Conversao, H00SQ3_A2112ContratoServicosUnidConversao_Sigla, H00SQ3_n2112ContratoServicosUnidConversao_Sigla, H00SQ3_A2111ContratoServicosUnidConversao_Nome, H00SQ3_n2111ContratoServicosUnidConversao_Nome
               }
               , new Object[] {
               H00SQ4_A2112ContratoServicosUnidConversao_Sigla, H00SQ4_n2112ContratoServicosUnidConversao_Sigla, H00SQ4_A2111ContratoServicosUnidConversao_Nome, H00SQ4_n2111ContratoServicosUnidConversao_Nome
               }
            }
         );
         AV17Pgmname = "ContratoServicosUnidConversaoGeneral";
         /* GeneXus formulas. */
         AV17Pgmname = "ContratoServicosUnidConversaoGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A160ContratoServicos_Codigo ;
      private int A2110ContratoServicosUnidConversao_Codigo ;
      private int wcpOA160ContratoServicos_Codigo ;
      private int wcpOA2110ContratoServicosUnidConversao_Codigo ;
      private int AV13Contrato_Codigo ;
      private int AV14ContratoServicos_UnidadeContratada ;
      private int edtContratoServicos_Codigo_Visible ;
      private int edtContratoServicosUnidConversao_Codigo_Visible ;
      private int bttBtnupdate_Enabled ;
      private int bttBtndelete_Enabled ;
      private int AV7ContratoServicos_Codigo ;
      private int AV8ContratoServicosUnidConversao_Codigo ;
      private int idxLst ;
      private decimal A2114ContratoServicosUnidConversao_Conversao ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV17Pgmname ;
      private String scmdbuf ;
      private String A2112ContratoServicosUnidConversao_Sigla ;
      private String A2111ContratoServicosUnidConversao_Nome ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String edtContratoServicos_Codigo_Internalname ;
      private String edtContratoServicos_Codigo_Jsonclick ;
      private String edtContratoServicosUnidConversao_Codigo_Internalname ;
      private String edtContratoServicosUnidConversao_Codigo_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtContratoServicosUnidConversao_Nome_Internalname ;
      private String edtContratoServicosUnidConversao_Sigla_Internalname ;
      private String edtContratoServicosUnidConversao_Conversao_Internalname ;
      private String edtContratoServicosUnidConversao_Nome_Link ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockcontratoservicosunidconversao_nome_Internalname ;
      private String lblTextblockcontratoservicosunidconversao_nome_Jsonclick ;
      private String edtContratoServicosUnidConversao_Nome_Jsonclick ;
      private String lblTextblockcontratoservicosunidconversao_sigla_Internalname ;
      private String lblTextblockcontratoservicosunidconversao_sigla_Jsonclick ;
      private String edtContratoServicosUnidConversao_Sigla_Jsonclick ;
      private String lblTextblockcontratoservicosunidconversao_conversao_Internalname ;
      private String lblTextblockcontratoservicosunidconversao_conversao_Jsonclick ;
      private String edtContratoServicosUnidConversao_Conversao_Jsonclick ;
      private String sCtrlA160ContratoServicos_Codigo ;
      private String sCtrlA2110ContratoServicosUnidConversao_Codigo ;
      private bool entryPointCalled ;
      private bool n2112ContratoServicosUnidConversao_Sigla ;
      private bool n2111ContratoServicosUnidConversao_Nome ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] H00SQ2_A2112ContratoServicosUnidConversao_Sigla ;
      private bool[] H00SQ2_n2112ContratoServicosUnidConversao_Sigla ;
      private String[] H00SQ2_A2111ContratoServicosUnidConversao_Nome ;
      private bool[] H00SQ2_n2111ContratoServicosUnidConversao_Nome ;
      private int[] H00SQ3_A160ContratoServicos_Codigo ;
      private int[] H00SQ3_A2110ContratoServicosUnidConversao_Codigo ;
      private decimal[] H00SQ3_A2114ContratoServicosUnidConversao_Conversao ;
      private String[] H00SQ3_A2112ContratoServicosUnidConversao_Sigla ;
      private bool[] H00SQ3_n2112ContratoServicosUnidConversao_Sigla ;
      private String[] H00SQ3_A2111ContratoServicosUnidConversao_Nome ;
      private bool[] H00SQ3_n2111ContratoServicosUnidConversao_Nome ;
      private String[] H00SQ4_A2112ContratoServicosUnidConversao_Sigla ;
      private bool[] H00SQ4_n2112ContratoServicosUnidConversao_Sigla ;
      private String[] H00SQ4_A2111ContratoServicosUnidConversao_Nome ;
      private bool[] H00SQ4_n2111ContratoServicosUnidConversao_Nome ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV12HTTPRequest ;
      private IGxSession AV11Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
   }

   public class contratoservicosunidconversaogeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00SQ2 ;
          prmH00SQ2 = new Object[] {
          new Object[] {"@ContratoServicosUnidConversao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00SQ3 ;
          prmH00SQ3 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosUnidConversao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00SQ4 ;
          prmH00SQ4 = new Object[] {
          new Object[] {"@ContratoServicosUnidConversao_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00SQ2", "SELECT [UnidadeMedicao_Sigla] AS ContratoServicosUnidConversao_Sigla, [UnidadeMedicao_Nome] AS ContratoServicosUnidConversao_Nome FROM [UnidadeMedicao] WITH (NOLOCK) WHERE [UnidadeMedicao_Codigo] = @ContratoServicosUnidConversao_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00SQ2,1,0,true,true )
             ,new CursorDef("H00SQ3", "SELECT T1.[ContratoServicos_Codigo], T1.[ContratoServicosUnidConversao_Codigo] AS ContratoServicosUnidConversao_Codigo, T1.[ContratoServicosUnidConversao_Conversao], T2.[UnidadeMedicao_Sigla] AS ContratoServicosUnidConversao_Sigla, T2.[UnidadeMedicao_Nome] AS ContratoServicosUnidConversao_Nome FROM ([ContratoServicosUnidConversao] T1 WITH (NOLOCK) INNER JOIN [UnidadeMedicao] T2 WITH (NOLOCK) ON T2.[UnidadeMedicao_Codigo] = T1.[ContratoServicosUnidConversao_Codigo]) WHERE T1.[ContratoServicos_Codigo] = @ContratoServicos_Codigo and T1.[ContratoServicosUnidConversao_Codigo] = @ContratoServicosUnidConversao_Codigo ORDER BY T1.[ContratoServicos_Codigo], T1.[ContratoServicosUnidConversao_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00SQ3,1,0,true,true )
             ,new CursorDef("H00SQ4", "SELECT [UnidadeMedicao_Sigla] AS ContratoServicosUnidConversao_Sigla, [UnidadeMedicao_Nome] AS ContratoServicosUnidConversao_Nome FROM [UnidadeMedicao] WITH (NOLOCK) WHERE [UnidadeMedicao_Codigo] = @ContratoServicosUnidConversao_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00SQ4,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
