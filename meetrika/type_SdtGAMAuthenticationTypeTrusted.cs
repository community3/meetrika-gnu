/*
               File: type_SdtGAMAuthenticationTypeTrusted
        Description: GAMAuthenticationTypeTrusted
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:36:43.79
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtGAMAuthenticationTypeTrusted : GxUserType, IGxExternalObject
   {
      public SdtGAMAuthenticationTypeTrusted( )
      {
         initialize();
      }

      public SdtGAMAuthenticationTypeTrusted( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public String tostring( )
      {
         String returntostring ;
         if ( GAMAuthenticationTypeTrusted_externalReference == null )
         {
            GAMAuthenticationTypeTrusted_externalReference = new Artech.Security.GAMAuthenticationTypeTrusted(context);
         }
         returntostring = "";
         returntostring = (String)(GAMAuthenticationTypeTrusted_externalReference.ToString());
         return returntostring ;
      }

      public String gxTpr_Name
      {
         get {
            if ( GAMAuthenticationTypeTrusted_externalReference == null )
            {
               GAMAuthenticationTypeTrusted_externalReference = new Artech.Security.GAMAuthenticationTypeTrusted(context);
            }
            return GAMAuthenticationTypeTrusted_externalReference.Name ;
         }

         set {
            if ( GAMAuthenticationTypeTrusted_externalReference == null )
            {
               GAMAuthenticationTypeTrusted_externalReference = new Artech.Security.GAMAuthenticationTypeTrusted(context);
            }
            GAMAuthenticationTypeTrusted_externalReference.Name = value;
         }

      }

      public bool gxTpr_Isenable
      {
         get {
            if ( GAMAuthenticationTypeTrusted_externalReference == null )
            {
               GAMAuthenticationTypeTrusted_externalReference = new Artech.Security.GAMAuthenticationTypeTrusted(context);
            }
            return GAMAuthenticationTypeTrusted_externalReference.IsEnable ;
         }

         set {
            if ( GAMAuthenticationTypeTrusted_externalReference == null )
            {
               GAMAuthenticationTypeTrusted_externalReference = new Artech.Security.GAMAuthenticationTypeTrusted(context);
            }
            GAMAuthenticationTypeTrusted_externalReference.IsEnable = value;
         }

      }

      public String gxTpr_Description
      {
         get {
            if ( GAMAuthenticationTypeTrusted_externalReference == null )
            {
               GAMAuthenticationTypeTrusted_externalReference = new Artech.Security.GAMAuthenticationTypeTrusted(context);
            }
            return GAMAuthenticationTypeTrusted_externalReference.Description ;
         }

         set {
            if ( GAMAuthenticationTypeTrusted_externalReference == null )
            {
               GAMAuthenticationTypeTrusted_externalReference = new Artech.Security.GAMAuthenticationTypeTrusted(context);
            }
            GAMAuthenticationTypeTrusted_externalReference.Description = value;
         }

      }

      public String gxTpr_Impersonate
      {
         get {
            if ( GAMAuthenticationTypeTrusted_externalReference == null )
            {
               GAMAuthenticationTypeTrusted_externalReference = new Artech.Security.GAMAuthenticationTypeTrusted(context);
            }
            return GAMAuthenticationTypeTrusted_externalReference.Impersonate ;
         }

         set {
            if ( GAMAuthenticationTypeTrusted_externalReference == null )
            {
               GAMAuthenticationTypeTrusted_externalReference = new Artech.Security.GAMAuthenticationTypeTrusted(context);
            }
            GAMAuthenticationTypeTrusted_externalReference.Impersonate = value;
         }

      }

      public DateTime gxTpr_Datecreated
      {
         get {
            if ( GAMAuthenticationTypeTrusted_externalReference == null )
            {
               GAMAuthenticationTypeTrusted_externalReference = new Artech.Security.GAMAuthenticationTypeTrusted(context);
            }
            return GAMAuthenticationTypeTrusted_externalReference.DateCreated ;
         }

         set {
            if ( GAMAuthenticationTypeTrusted_externalReference == null )
            {
               GAMAuthenticationTypeTrusted_externalReference = new Artech.Security.GAMAuthenticationTypeTrusted(context);
            }
            GAMAuthenticationTypeTrusted_externalReference.DateCreated = value;
         }

      }

      public String gxTpr_Usercreated
      {
         get {
            if ( GAMAuthenticationTypeTrusted_externalReference == null )
            {
               GAMAuthenticationTypeTrusted_externalReference = new Artech.Security.GAMAuthenticationTypeTrusted(context);
            }
            return GAMAuthenticationTypeTrusted_externalReference.UserCreated ;
         }

         set {
            if ( GAMAuthenticationTypeTrusted_externalReference == null )
            {
               GAMAuthenticationTypeTrusted_externalReference = new Artech.Security.GAMAuthenticationTypeTrusted(context);
            }
            GAMAuthenticationTypeTrusted_externalReference.UserCreated = value;
         }

      }

      public DateTime gxTpr_Dateupdated
      {
         get {
            if ( GAMAuthenticationTypeTrusted_externalReference == null )
            {
               GAMAuthenticationTypeTrusted_externalReference = new Artech.Security.GAMAuthenticationTypeTrusted(context);
            }
            return GAMAuthenticationTypeTrusted_externalReference.DateUpdated ;
         }

         set {
            if ( GAMAuthenticationTypeTrusted_externalReference == null )
            {
               GAMAuthenticationTypeTrusted_externalReference = new Artech.Security.GAMAuthenticationTypeTrusted(context);
            }
            GAMAuthenticationTypeTrusted_externalReference.DateUpdated = value;
         }

      }

      public String gxTpr_Userupdated
      {
         get {
            if ( GAMAuthenticationTypeTrusted_externalReference == null )
            {
               GAMAuthenticationTypeTrusted_externalReference = new Artech.Security.GAMAuthenticationTypeTrusted(context);
            }
            return GAMAuthenticationTypeTrusted_externalReference.UserUpdated ;
         }

         set {
            if ( GAMAuthenticationTypeTrusted_externalReference == null )
            {
               GAMAuthenticationTypeTrusted_externalReference = new Artech.Security.GAMAuthenticationTypeTrusted(context);
            }
            GAMAuthenticationTypeTrusted_externalReference.UserUpdated = value;
         }

      }

      public IGxCollection gxTpr_Descriptions
      {
         get {
            if ( GAMAuthenticationTypeTrusted_externalReference == null )
            {
               GAMAuthenticationTypeTrusted_externalReference = new Artech.Security.GAMAuthenticationTypeTrusted(context);
            }
            IGxCollection intValue ;
            intValue = new GxExternalCollection( context, "SdtGAMDescription", "GeneXus.Programs");
            System.Collections.Generic.List<Artech.Security.GAMDescription> externalParm0 ;
            externalParm0 = GAMAuthenticationTypeTrusted_externalReference.Descriptions;
            intValue.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMDescription>), externalParm0);
            return intValue ;
         }

         set {
            if ( GAMAuthenticationTypeTrusted_externalReference == null )
            {
               GAMAuthenticationTypeTrusted_externalReference = new Artech.Security.GAMAuthenticationTypeTrusted(context);
            }
            IGxCollection intValue ;
            System.Collections.Generic.List<Artech.Security.GAMDescription> externalParm1 ;
            intValue = value;
            externalParm1 = (System.Collections.Generic.List<Artech.Security.GAMDescription>)CollectionUtils.ConvertToExternal( typeof(System.Collections.Generic.List<Artech.Security.GAMDescription>), intValue.ExternalInstance);
            GAMAuthenticationTypeTrusted_externalReference.Descriptions = externalParm1;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( GAMAuthenticationTypeTrusted_externalReference == null )
            {
               GAMAuthenticationTypeTrusted_externalReference = new Artech.Security.GAMAuthenticationTypeTrusted(context);
            }
            return GAMAuthenticationTypeTrusted_externalReference ;
         }

         set {
            GAMAuthenticationTypeTrusted_externalReference = (Artech.Security.GAMAuthenticationTypeTrusted)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected Artech.Security.GAMAuthenticationTypeTrusted GAMAuthenticationTypeTrusted_externalReference=null ;
   }

}
