/*
               File: REL_ConferenciaMS
        Description: Stub for REL_ConferenciaMS
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:14:5.23
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class rel_conferenciams : GXProcedure
   {
      public rel_conferenciams( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public rel_conferenciams( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Arquivo ,
                           String aP1_Arquivo2 ,
                           String aP2_Aba ,
                           String aP3_Opcao ,
                           short aP4_ColDmn ,
                           short aP5_ColPB ,
                           short aP6_ColPL ,
                           short aP7_PraLinha ,
                           String aP8_Acao ,
                           int aP9_Contratada ,
                           String aP10_FileName ,
                           short aP11_ColPBFM ,
                           short aP12_ColPLFM ,
                           ref String aP13_DemandaFM )
      {
         this.AV2Arquivo = aP0_Arquivo;
         this.AV3Arquivo2 = aP1_Arquivo2;
         this.AV4Aba = aP2_Aba;
         this.AV5Opcao = aP3_Opcao;
         this.AV6ColDmn = aP4_ColDmn;
         this.AV7ColPB = aP5_ColPB;
         this.AV8ColPL = aP6_ColPL;
         this.AV9PraLinha = aP7_PraLinha;
         this.AV10Acao = aP8_Acao;
         this.AV11Contratada = aP9_Contratada;
         this.AV12FileName = aP10_FileName;
         this.AV13ColPBFM = aP11_ColPBFM;
         this.AV14ColPLFM = aP12_ColPLFM;
         this.AV15DemandaFM = aP13_DemandaFM;
         initialize();
         executePrivate();
         aP13_DemandaFM=this.AV15DemandaFM;
      }

      public String executeUdp( String aP0_Arquivo ,
                                String aP1_Arquivo2 ,
                                String aP2_Aba ,
                                String aP3_Opcao ,
                                short aP4_ColDmn ,
                                short aP5_ColPB ,
                                short aP6_ColPL ,
                                short aP7_PraLinha ,
                                String aP8_Acao ,
                                int aP9_Contratada ,
                                String aP10_FileName ,
                                short aP11_ColPBFM ,
                                short aP12_ColPLFM )
      {
         this.AV2Arquivo = aP0_Arquivo;
         this.AV3Arquivo2 = aP1_Arquivo2;
         this.AV4Aba = aP2_Aba;
         this.AV5Opcao = aP3_Opcao;
         this.AV6ColDmn = aP4_ColDmn;
         this.AV7ColPB = aP5_ColPB;
         this.AV8ColPL = aP6_ColPL;
         this.AV9PraLinha = aP7_PraLinha;
         this.AV10Acao = aP8_Acao;
         this.AV11Contratada = aP9_Contratada;
         this.AV12FileName = aP10_FileName;
         this.AV13ColPBFM = aP11_ColPBFM;
         this.AV14ColPLFM = aP12_ColPLFM;
         this.AV15DemandaFM = aP13_DemandaFM;
         initialize();
         executePrivate();
         aP13_DemandaFM=this.AV15DemandaFM;
         return AV15DemandaFM ;
      }

      public void executeSubmit( String aP0_Arquivo ,
                                 String aP1_Arquivo2 ,
                                 String aP2_Aba ,
                                 String aP3_Opcao ,
                                 short aP4_ColDmn ,
                                 short aP5_ColPB ,
                                 short aP6_ColPL ,
                                 short aP7_PraLinha ,
                                 String aP8_Acao ,
                                 int aP9_Contratada ,
                                 String aP10_FileName ,
                                 short aP11_ColPBFM ,
                                 short aP12_ColPLFM ,
                                 ref String aP13_DemandaFM )
      {
         rel_conferenciams objrel_conferenciams;
         objrel_conferenciams = new rel_conferenciams();
         objrel_conferenciams.AV2Arquivo = aP0_Arquivo;
         objrel_conferenciams.AV3Arquivo2 = aP1_Arquivo2;
         objrel_conferenciams.AV4Aba = aP2_Aba;
         objrel_conferenciams.AV5Opcao = aP3_Opcao;
         objrel_conferenciams.AV6ColDmn = aP4_ColDmn;
         objrel_conferenciams.AV7ColPB = aP5_ColPB;
         objrel_conferenciams.AV8ColPL = aP6_ColPL;
         objrel_conferenciams.AV9PraLinha = aP7_PraLinha;
         objrel_conferenciams.AV10Acao = aP8_Acao;
         objrel_conferenciams.AV11Contratada = aP9_Contratada;
         objrel_conferenciams.AV12FileName = aP10_FileName;
         objrel_conferenciams.AV13ColPBFM = aP11_ColPBFM;
         objrel_conferenciams.AV14ColPLFM = aP12_ColPLFM;
         objrel_conferenciams.AV15DemandaFM = aP13_DemandaFM;
         objrel_conferenciams.context.SetSubmitInitialConfig(context);
         objrel_conferenciams.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objrel_conferenciams);
         aP13_DemandaFM=this.AV15DemandaFM;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((rel_conferenciams)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         args = new Object[] {(String)AV2Arquivo,(String)AV3Arquivo2,(String)AV4Aba,(String)AV5Opcao,(short)AV6ColDmn,(short)AV7ColPB,(short)AV8ColPL,(short)AV9PraLinha,(String)AV10Acao,(int)AV11Contratada,(String)AV12FileName,(short)AV13ColPBFM,(short)AV14ColPLFM,(String)AV15DemandaFM} ;
         ClassLoader.Execute("arel_conferenciams","GeneXus.Programs.arel_conferenciams", new Object[] {context }, "execute", args);
         if ( ( args != null ) && ( args.Length == 14 ) )
         {
            AV15DemandaFM = (String)(args[13]) ;
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV6ColDmn ;
      private short AV7ColPB ;
      private short AV8ColPL ;
      private short AV9PraLinha ;
      private short AV13ColPBFM ;
      private short AV14ColPLFM ;
      private int AV11Contratada ;
      private String AV2Arquivo ;
      private String AV3Arquivo2 ;
      private String AV4Aba ;
      private String AV5Opcao ;
      private String AV10Acao ;
      private String AV12FileName ;
      private String AV15DemandaFM ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP13_DemandaFM ;
      private Object[] args ;
   }

}
