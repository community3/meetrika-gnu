/*
               File: ExportWWContagemResultadoContagens
        Description: Export WWContagem Resultado Contagens
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:14:27.5
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Office;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class exportwwcontagemresultadocontagens : GXProcedure
   {
      public exportwwcontagemresultadocontagens( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public exportwwcontagemresultadocontagens( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_TFContratada_AreaTrabalhoDes ,
                           String aP1_TFContratada_AreaTrabalhoDes_Sel ,
                           DateTime aP2_TFContagemResultado_DataDmn ,
                           DateTime aP3_TFContagemResultado_DataDmn_To ,
                           DateTime aP4_TFContagemResultado_DataCnt ,
                           DateTime aP5_TFContagemResultado_DataCnt_To ,
                           String aP6_TFContagemResultado_HoraCnt ,
                           String aP7_TFContagemResultado_HoraCnt_Sel ,
                           String aP8_TFContagemResultado_OsFsOsFm ,
                           String aP9_TFContagemResultado_OsFsOsFm_Sel ,
                           String aP10_TFContagemrResultado_SistemaSigla ,
                           String aP11_TFContagemrResultado_SistemaSigla_Sel ,
                           String aP12_TFContagemResultado_ContadorFMNom ,
                           String aP13_TFContagemResultado_ContadorFMNom_Sel ,
                           String aP14_TFContagemResultado_StatusCnt_SelsJson ,
                           short aP15_TFContagemResultadoContagens_Esforco ,
                           short aP16_TFContagemResultadoContagens_Esforco_To ,
                           decimal aP17_TFContagemResultado_PFBFS ,
                           decimal aP18_TFContagemResultado_PFBFS_To ,
                           decimal aP19_TFContagemResultado_PFLFS ,
                           decimal aP20_TFContagemResultado_PFLFS_To ,
                           decimal aP21_TFContagemResultado_PFBFM ,
                           decimal aP22_TFContagemResultado_PFBFM_To ,
                           decimal aP23_TFContagemResultado_PFLFM ,
                           decimal aP24_TFContagemResultado_PFLFM_To ,
                           short aP25_OrderedBy ,
                           bool aP26_OrderedDsc ,
                           String aP27_GridStateXML ,
                           out String aP28_Filename ,
                           out String aP29_ErrorMessage )
      {
         this.AV76TFContratada_AreaTrabalhoDes = aP0_TFContratada_AreaTrabalhoDes;
         this.AV77TFContratada_AreaTrabalhoDes_Sel = aP1_TFContratada_AreaTrabalhoDes_Sel;
         this.AV78TFContagemResultado_DataDmn = aP2_TFContagemResultado_DataDmn;
         this.AV79TFContagemResultado_DataDmn_To = aP3_TFContagemResultado_DataDmn_To;
         this.AV80TFContagemResultado_DataCnt = aP4_TFContagemResultado_DataCnt;
         this.AV81TFContagemResultado_DataCnt_To = aP5_TFContagemResultado_DataCnt_To;
         this.AV82TFContagemResultado_HoraCnt = aP6_TFContagemResultado_HoraCnt;
         this.AV83TFContagemResultado_HoraCnt_Sel = aP7_TFContagemResultado_HoraCnt_Sel;
         this.AV84TFContagemResultado_OsFsOsFm = aP8_TFContagemResultado_OsFsOsFm;
         this.AV85TFContagemResultado_OsFsOsFm_Sel = aP9_TFContagemResultado_OsFsOsFm_Sel;
         this.AV86TFContagemrResultado_SistemaSigla = aP10_TFContagemrResultado_SistemaSigla;
         this.AV87TFContagemrResultado_SistemaSigla_Sel = aP11_TFContagemrResultado_SistemaSigla_Sel;
         this.AV88TFContagemResultado_ContadorFMNom = aP12_TFContagemResultado_ContadorFMNom;
         this.AV89TFContagemResultado_ContadorFMNom_Sel = aP13_TFContagemResultado_ContadorFMNom_Sel;
         this.AV90TFContagemResultado_StatusCnt_SelsJson = aP14_TFContagemResultado_StatusCnt_SelsJson;
         this.AV93TFContagemResultadoContagens_Esforco = aP15_TFContagemResultadoContagens_Esforco;
         this.AV94TFContagemResultadoContagens_Esforco_To = aP16_TFContagemResultadoContagens_Esforco_To;
         this.AV95TFContagemResultado_PFBFS = aP17_TFContagemResultado_PFBFS;
         this.AV96TFContagemResultado_PFBFS_To = aP18_TFContagemResultado_PFBFS_To;
         this.AV97TFContagemResultado_PFLFS = aP19_TFContagemResultado_PFLFS;
         this.AV98TFContagemResultado_PFLFS_To = aP20_TFContagemResultado_PFLFS_To;
         this.AV99TFContagemResultado_PFBFM = aP21_TFContagemResultado_PFBFM;
         this.AV100TFContagemResultado_PFBFM_To = aP22_TFContagemResultado_PFBFM_To;
         this.AV101TFContagemResultado_PFLFM = aP23_TFContagemResultado_PFLFM;
         this.AV102TFContagemResultado_PFLFM_To = aP24_TFContagemResultado_PFLFM_To;
         this.AV16OrderedBy = aP25_OrderedBy;
         this.AV17OrderedDsc = aP26_OrderedDsc;
         this.AV44GridStateXML = aP27_GridStateXML;
         this.AV11Filename = "" ;
         this.AV12ErrorMessage = "" ;
         initialize();
         executePrivate();
         aP28_Filename=this.AV11Filename;
         aP29_ErrorMessage=this.AV12ErrorMessage;
      }

      public String executeUdp( String aP0_TFContratada_AreaTrabalhoDes ,
                                String aP1_TFContratada_AreaTrabalhoDes_Sel ,
                                DateTime aP2_TFContagemResultado_DataDmn ,
                                DateTime aP3_TFContagemResultado_DataDmn_To ,
                                DateTime aP4_TFContagemResultado_DataCnt ,
                                DateTime aP5_TFContagemResultado_DataCnt_To ,
                                String aP6_TFContagemResultado_HoraCnt ,
                                String aP7_TFContagemResultado_HoraCnt_Sel ,
                                String aP8_TFContagemResultado_OsFsOsFm ,
                                String aP9_TFContagemResultado_OsFsOsFm_Sel ,
                                String aP10_TFContagemrResultado_SistemaSigla ,
                                String aP11_TFContagemrResultado_SistemaSigla_Sel ,
                                String aP12_TFContagemResultado_ContadorFMNom ,
                                String aP13_TFContagemResultado_ContadorFMNom_Sel ,
                                String aP14_TFContagemResultado_StatusCnt_SelsJson ,
                                short aP15_TFContagemResultadoContagens_Esforco ,
                                short aP16_TFContagemResultadoContagens_Esforco_To ,
                                decimal aP17_TFContagemResultado_PFBFS ,
                                decimal aP18_TFContagemResultado_PFBFS_To ,
                                decimal aP19_TFContagemResultado_PFLFS ,
                                decimal aP20_TFContagemResultado_PFLFS_To ,
                                decimal aP21_TFContagemResultado_PFBFM ,
                                decimal aP22_TFContagemResultado_PFBFM_To ,
                                decimal aP23_TFContagemResultado_PFLFM ,
                                decimal aP24_TFContagemResultado_PFLFM_To ,
                                short aP25_OrderedBy ,
                                bool aP26_OrderedDsc ,
                                String aP27_GridStateXML ,
                                out String aP28_Filename )
      {
         this.AV76TFContratada_AreaTrabalhoDes = aP0_TFContratada_AreaTrabalhoDes;
         this.AV77TFContratada_AreaTrabalhoDes_Sel = aP1_TFContratada_AreaTrabalhoDes_Sel;
         this.AV78TFContagemResultado_DataDmn = aP2_TFContagemResultado_DataDmn;
         this.AV79TFContagemResultado_DataDmn_To = aP3_TFContagemResultado_DataDmn_To;
         this.AV80TFContagemResultado_DataCnt = aP4_TFContagemResultado_DataCnt;
         this.AV81TFContagemResultado_DataCnt_To = aP5_TFContagemResultado_DataCnt_To;
         this.AV82TFContagemResultado_HoraCnt = aP6_TFContagemResultado_HoraCnt;
         this.AV83TFContagemResultado_HoraCnt_Sel = aP7_TFContagemResultado_HoraCnt_Sel;
         this.AV84TFContagemResultado_OsFsOsFm = aP8_TFContagemResultado_OsFsOsFm;
         this.AV85TFContagemResultado_OsFsOsFm_Sel = aP9_TFContagemResultado_OsFsOsFm_Sel;
         this.AV86TFContagemrResultado_SistemaSigla = aP10_TFContagemrResultado_SistemaSigla;
         this.AV87TFContagemrResultado_SistemaSigla_Sel = aP11_TFContagemrResultado_SistemaSigla_Sel;
         this.AV88TFContagemResultado_ContadorFMNom = aP12_TFContagemResultado_ContadorFMNom;
         this.AV89TFContagemResultado_ContadorFMNom_Sel = aP13_TFContagemResultado_ContadorFMNom_Sel;
         this.AV90TFContagemResultado_StatusCnt_SelsJson = aP14_TFContagemResultado_StatusCnt_SelsJson;
         this.AV93TFContagemResultadoContagens_Esforco = aP15_TFContagemResultadoContagens_Esforco;
         this.AV94TFContagemResultadoContagens_Esforco_To = aP16_TFContagemResultadoContagens_Esforco_To;
         this.AV95TFContagemResultado_PFBFS = aP17_TFContagemResultado_PFBFS;
         this.AV96TFContagemResultado_PFBFS_To = aP18_TFContagemResultado_PFBFS_To;
         this.AV97TFContagemResultado_PFLFS = aP19_TFContagemResultado_PFLFS;
         this.AV98TFContagemResultado_PFLFS_To = aP20_TFContagemResultado_PFLFS_To;
         this.AV99TFContagemResultado_PFBFM = aP21_TFContagemResultado_PFBFM;
         this.AV100TFContagemResultado_PFBFM_To = aP22_TFContagemResultado_PFBFM_To;
         this.AV101TFContagemResultado_PFLFM = aP23_TFContagemResultado_PFLFM;
         this.AV102TFContagemResultado_PFLFM_To = aP24_TFContagemResultado_PFLFM_To;
         this.AV16OrderedBy = aP25_OrderedBy;
         this.AV17OrderedDsc = aP26_OrderedDsc;
         this.AV44GridStateXML = aP27_GridStateXML;
         this.AV11Filename = "" ;
         this.AV12ErrorMessage = "" ;
         initialize();
         executePrivate();
         aP28_Filename=this.AV11Filename;
         aP29_ErrorMessage=this.AV12ErrorMessage;
         return AV12ErrorMessage ;
      }

      public void executeSubmit( String aP0_TFContratada_AreaTrabalhoDes ,
                                 String aP1_TFContratada_AreaTrabalhoDes_Sel ,
                                 DateTime aP2_TFContagemResultado_DataDmn ,
                                 DateTime aP3_TFContagemResultado_DataDmn_To ,
                                 DateTime aP4_TFContagemResultado_DataCnt ,
                                 DateTime aP5_TFContagemResultado_DataCnt_To ,
                                 String aP6_TFContagemResultado_HoraCnt ,
                                 String aP7_TFContagemResultado_HoraCnt_Sel ,
                                 String aP8_TFContagemResultado_OsFsOsFm ,
                                 String aP9_TFContagemResultado_OsFsOsFm_Sel ,
                                 String aP10_TFContagemrResultado_SistemaSigla ,
                                 String aP11_TFContagemrResultado_SistemaSigla_Sel ,
                                 String aP12_TFContagemResultado_ContadorFMNom ,
                                 String aP13_TFContagemResultado_ContadorFMNom_Sel ,
                                 String aP14_TFContagemResultado_StatusCnt_SelsJson ,
                                 short aP15_TFContagemResultadoContagens_Esforco ,
                                 short aP16_TFContagemResultadoContagens_Esforco_To ,
                                 decimal aP17_TFContagemResultado_PFBFS ,
                                 decimal aP18_TFContagemResultado_PFBFS_To ,
                                 decimal aP19_TFContagemResultado_PFLFS ,
                                 decimal aP20_TFContagemResultado_PFLFS_To ,
                                 decimal aP21_TFContagemResultado_PFBFM ,
                                 decimal aP22_TFContagemResultado_PFBFM_To ,
                                 decimal aP23_TFContagemResultado_PFLFM ,
                                 decimal aP24_TFContagemResultado_PFLFM_To ,
                                 short aP25_OrderedBy ,
                                 bool aP26_OrderedDsc ,
                                 String aP27_GridStateXML ,
                                 out String aP28_Filename ,
                                 out String aP29_ErrorMessage )
      {
         exportwwcontagemresultadocontagens objexportwwcontagemresultadocontagens;
         objexportwwcontagemresultadocontagens = new exportwwcontagemresultadocontagens();
         objexportwwcontagemresultadocontagens.AV76TFContratada_AreaTrabalhoDes = aP0_TFContratada_AreaTrabalhoDes;
         objexportwwcontagemresultadocontagens.AV77TFContratada_AreaTrabalhoDes_Sel = aP1_TFContratada_AreaTrabalhoDes_Sel;
         objexportwwcontagemresultadocontagens.AV78TFContagemResultado_DataDmn = aP2_TFContagemResultado_DataDmn;
         objexportwwcontagemresultadocontagens.AV79TFContagemResultado_DataDmn_To = aP3_TFContagemResultado_DataDmn_To;
         objexportwwcontagemresultadocontagens.AV80TFContagemResultado_DataCnt = aP4_TFContagemResultado_DataCnt;
         objexportwwcontagemresultadocontagens.AV81TFContagemResultado_DataCnt_To = aP5_TFContagemResultado_DataCnt_To;
         objexportwwcontagemresultadocontagens.AV82TFContagemResultado_HoraCnt = aP6_TFContagemResultado_HoraCnt;
         objexportwwcontagemresultadocontagens.AV83TFContagemResultado_HoraCnt_Sel = aP7_TFContagemResultado_HoraCnt_Sel;
         objexportwwcontagemresultadocontagens.AV84TFContagemResultado_OsFsOsFm = aP8_TFContagemResultado_OsFsOsFm;
         objexportwwcontagemresultadocontagens.AV85TFContagemResultado_OsFsOsFm_Sel = aP9_TFContagemResultado_OsFsOsFm_Sel;
         objexportwwcontagemresultadocontagens.AV86TFContagemrResultado_SistemaSigla = aP10_TFContagemrResultado_SistemaSigla;
         objexportwwcontagemresultadocontagens.AV87TFContagemrResultado_SistemaSigla_Sel = aP11_TFContagemrResultado_SistemaSigla_Sel;
         objexportwwcontagemresultadocontagens.AV88TFContagemResultado_ContadorFMNom = aP12_TFContagemResultado_ContadorFMNom;
         objexportwwcontagemresultadocontagens.AV89TFContagemResultado_ContadorFMNom_Sel = aP13_TFContagemResultado_ContadorFMNom_Sel;
         objexportwwcontagemresultadocontagens.AV90TFContagemResultado_StatusCnt_SelsJson = aP14_TFContagemResultado_StatusCnt_SelsJson;
         objexportwwcontagemresultadocontagens.AV93TFContagemResultadoContagens_Esforco = aP15_TFContagemResultadoContagens_Esforco;
         objexportwwcontagemresultadocontagens.AV94TFContagemResultadoContagens_Esforco_To = aP16_TFContagemResultadoContagens_Esforco_To;
         objexportwwcontagemresultadocontagens.AV95TFContagemResultado_PFBFS = aP17_TFContagemResultado_PFBFS;
         objexportwwcontagemresultadocontagens.AV96TFContagemResultado_PFBFS_To = aP18_TFContagemResultado_PFBFS_To;
         objexportwwcontagemresultadocontagens.AV97TFContagemResultado_PFLFS = aP19_TFContagemResultado_PFLFS;
         objexportwwcontagemresultadocontagens.AV98TFContagemResultado_PFLFS_To = aP20_TFContagemResultado_PFLFS_To;
         objexportwwcontagemresultadocontagens.AV99TFContagemResultado_PFBFM = aP21_TFContagemResultado_PFBFM;
         objexportwwcontagemresultadocontagens.AV100TFContagemResultado_PFBFM_To = aP22_TFContagemResultado_PFBFM_To;
         objexportwwcontagemresultadocontagens.AV101TFContagemResultado_PFLFM = aP23_TFContagemResultado_PFLFM;
         objexportwwcontagemresultadocontagens.AV102TFContagemResultado_PFLFM_To = aP24_TFContagemResultado_PFLFM_To;
         objexportwwcontagemresultadocontagens.AV16OrderedBy = aP25_OrderedBy;
         objexportwwcontagemresultadocontagens.AV17OrderedDsc = aP26_OrderedDsc;
         objexportwwcontagemresultadocontagens.AV44GridStateXML = aP27_GridStateXML;
         objexportwwcontagemresultadocontagens.AV11Filename = "" ;
         objexportwwcontagemresultadocontagens.AV12ErrorMessage = "" ;
         objexportwwcontagemresultadocontagens.context.SetSubmitInitialConfig(context);
         objexportwwcontagemresultadocontagens.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objexportwwcontagemresultadocontagens);
         aP28_Filename=this.AV11Filename;
         aP29_ErrorMessage=this.AV12ErrorMessage;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((exportwwcontagemresultadocontagens)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV55Flag = (short)(NumberUtil.Val( AV54Websession.Get("TodasAsAreas"), "."));
         AV66Contratadas.FromXml(AV54Websession.Get("Contratadas"), "Collection");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'OPENDOCUMENT' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV13CellRow = 1;
         AV14FirstColumn = 1;
         /* Execute user subroutine: 'WRITEMAINTITLE' */
         S131 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         /* Execute user subroutine: 'WRITEFILTERS' */
         S141 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         /* Execute user subroutine: 'WRITECOLUMNTITLES' */
         S151 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         /* Execute user subroutine: 'WRITEDATA' */
         S161 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         /* Execute user subroutine: 'CLOSEDOCUMENT' */
         S191 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'OPENDOCUMENT' Routine */
         AV67Usuario_EhGestor = AV9WWPContext.gxTpr_Userehgestor;
         if ( false )
         {
            AV15Random = (int)(NumberUtil.Random( )*10000);
            AV11Filename = "ExportWWContagemResultadoContagens-" + StringUtil.Trim( StringUtil.Str( (decimal)(AV15Random), 8, 0)) + ".xlsx";
            AV10ExcelDocument.Open(AV11Filename);
            /* Execute user subroutine: 'CHECKSTATUS' */
            S121 ();
            if (returnInSub) return;
            AV10ExcelDocument.Clear();
         }
         AV15Random = (int)(NumberUtil.Random( )*10000);
         AV11Filename = "PublicTempStorage\\ExportWWContagemResultadoContagens-" + StringUtil.Trim( StringUtil.Str( (decimal)(AV15Random), 8, 0)) + ".xlsx";
         AV10ExcelDocument.Open(AV11Filename);
         /* Execute user subroutine: 'CHECKSTATUS' */
         S121 ();
         if (returnInSub) return;
         AV10ExcelDocument.Clear();
      }

      protected void S131( )
      {
         /* 'WRITEMAINTITLE' Routine */
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Contagem Resultado Contagens";
         AV13CellRow = (int)(AV13CellRow+2);
      }

      protected void S141( )
      {
         /* 'WRITEFILTERS' Routine */
         AV45GridState.gxTpr_Dynamicfilters.FromXml(AV44GridStateXML, "");
         if ( AV45GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV46GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV45GridState.gxTpr_Dynamicfilters.Item(1));
            AV18DynamicFiltersSelector1 = AV46GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV18DynamicFiltersSelector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
            {
               AV71DynamicFiltersOperator1 = AV46GridStateDynamicFilter.gxTpr_Operator;
               AV19ContagemResultado_OsFsOsFm1 = AV46GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19ContagemResultado_OsFsOsFm1)) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  if ( AV71DynamicFiltersOperator1 == 0 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Igual)";
                  }
                  else if ( AV71DynamicFiltersOperator1 == 1 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Come�a com)";
                  }
                  else if ( AV71DynamicFiltersOperator1 == 2 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Cont�m)";
                  }
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV19ContagemResultado_OsFsOsFm1;
               }
            }
            else if ( StringUtil.StrCmp(AV18DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATACNT") == 0 )
            {
               AV20ContagemResultado_DataCnt1 = context.localUtil.CToD( AV46GridStateDynamicFilter.gxTpr_Value, 2);
               AV21ContagemResultado_DataCnt_To1 = context.localUtil.CToD( AV46GridStateDynamicFilter.gxTpr_Valueto, 2);
               if ( ! (DateTime.MinValue==AV20ContagemResultado_DataCnt1) || ! (DateTime.MinValue==AV21ContagemResultado_DataCnt_To1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Contagem";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  GXt_dtime1 = DateTimeUtil.ResetTime( AV20ContagemResultado_DataCnt1 ) ;
                  AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                  GXt_dtime1 = DateTimeUtil.ResetTime( AV21ContagemResultado_DataCnt_To1 ) ;
                  AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
               }
            }
            else if ( StringUtil.StrCmp(AV18DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 )
            {
               AV22ContagemResultado_DataDmn1 = context.localUtil.CToD( AV46GridStateDynamicFilter.gxTpr_Value, 2);
               AV23ContagemResultado_DataDmn_To1 = context.localUtil.CToD( AV46GridStateDynamicFilter.gxTpr_Valueto, 2);
               if ( ! (DateTime.MinValue==AV22ContagemResultado_DataDmn1) || ! (DateTime.MinValue==AV23ContagemResultado_DataDmn_To1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Demanda";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  GXt_dtime1 = DateTimeUtil.ResetTime( AV22ContagemResultado_DataDmn1 ) ;
                  AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                  GXt_dtime1 = DateTimeUtil.ResetTime( AV23ContagemResultado_DataDmn_To1 ) ;
                  AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
               }
            }
            else if ( StringUtil.StrCmp(AV18DynamicFiltersSelector1, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
            {
               AV68ContagemResultado_ContadorFM1 = (int)(NumberUtil.Val( AV46GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV68ContagemResultado_ContadorFM1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Usu�rio da Prestadora";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV68ContagemResultado_ContadorFM1;
               }
            }
            else if ( StringUtil.StrCmp(AV18DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSCNT") == 0 )
            {
               AV25ContagemResultado_StatusCnt1 = (short)(NumberUtil.Val( AV46GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV25ContagemResultado_StatusCnt1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Status Contagem";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  if ( ! (0==AV25ContagemResultado_StatusCnt1) )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = gxdomainstatuscontagem.getDescription(context,AV25ContagemResultado_StatusCnt1);
                  }
               }
            }
            else if ( StringUtil.StrCmp(AV18DynamicFiltersSelector1, "CONTAGEMRESULTADO_BASELINE") == 0 )
            {
               AV57ContagemResultado_Baseline1 = AV46GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57ContagemResultado_Baseline1)) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Baseline";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "";
                  if ( StringUtil.StrCmp(StringUtil.Trim( AV57ContagemResultado_Baseline1), "S") == 0 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Sim";
                  }
                  else if ( StringUtil.StrCmp(StringUtil.Trim( AV57ContagemResultado_Baseline1), "N") == 0 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "N�o";
                  }
               }
            }
            if ( AV45GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV26DynamicFiltersEnabled2 = true;
               AV46GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV45GridState.gxTpr_Dynamicfilters.Item(2));
               AV27DynamicFiltersSelector2 = AV46GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
               {
                  AV72DynamicFiltersOperator2 = AV46GridStateDynamicFilter.gxTpr_Operator;
                  AV28ContagemResultado_OsFsOsFm2 = AV46GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28ContagemResultado_OsFsOsFm2)) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     if ( AV72DynamicFiltersOperator2 == 0 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Igual)";
                     }
                     else if ( AV72DynamicFiltersOperator2 == 1 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Come�a com)";
                     }
                     else if ( AV72DynamicFiltersOperator2 == 2 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Cont�m)";
                     }
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV28ContagemResultado_OsFsOsFm2;
                  }
               }
               else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATACNT") == 0 )
               {
                  AV29ContagemResultado_DataCnt2 = context.localUtil.CToD( AV46GridStateDynamicFilter.gxTpr_Value, 2);
                  AV30ContagemResultado_DataCnt_To2 = context.localUtil.CToD( AV46GridStateDynamicFilter.gxTpr_Valueto, 2);
                  if ( ! (DateTime.MinValue==AV29ContagemResultado_DataCnt2) || ! (DateTime.MinValue==AV30ContagemResultado_DataCnt_To2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Contagem";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     GXt_dtime1 = DateTimeUtil.ResetTime( AV29ContagemResultado_DataCnt2 ) ;
                     AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                     GXt_dtime1 = DateTimeUtil.ResetTime( AV30ContagemResultado_DataCnt_To2 ) ;
                     AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
                  }
               }
               else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 )
               {
                  AV31ContagemResultado_DataDmn2 = context.localUtil.CToD( AV46GridStateDynamicFilter.gxTpr_Value, 2);
                  AV32ContagemResultado_DataDmn_To2 = context.localUtil.CToD( AV46GridStateDynamicFilter.gxTpr_Valueto, 2);
                  if ( ! (DateTime.MinValue==AV31ContagemResultado_DataDmn2) || ! (DateTime.MinValue==AV32ContagemResultado_DataDmn_To2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Demanda";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     GXt_dtime1 = DateTimeUtil.ResetTime( AV31ContagemResultado_DataDmn2 ) ;
                     AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                     GXt_dtime1 = DateTimeUtil.ResetTime( AV32ContagemResultado_DataDmn_To2 ) ;
                     AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
                  }
               }
               else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
               {
                  AV69ContagemResultado_ContadorFM2 = (int)(NumberUtil.Val( AV46GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV69ContagemResultado_ContadorFM2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Usu�rio da Prestadora";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV69ContagemResultado_ContadorFM2;
                  }
               }
               else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSCNT") == 0 )
               {
                  AV34ContagemResultado_StatusCnt2 = (short)(NumberUtil.Val( AV46GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV34ContagemResultado_StatusCnt2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Status Contagem";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     if ( ! (0==AV34ContagemResultado_StatusCnt2) )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = gxdomainstatuscontagem.getDescription(context,AV34ContagemResultado_StatusCnt2);
                     }
                  }
               }
               else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_BASELINE") == 0 )
               {
                  AV58ContagemResultado_Baseline2 = AV46GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58ContagemResultado_Baseline2)) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Baseline";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "";
                     if ( StringUtil.StrCmp(StringUtil.Trim( AV58ContagemResultado_Baseline2), "S") == 0 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Sim";
                     }
                     else if ( StringUtil.StrCmp(StringUtil.Trim( AV58ContagemResultado_Baseline2), "N") == 0 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "N�o";
                     }
                  }
               }
               if ( AV45GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV35DynamicFiltersEnabled3 = true;
                  AV46GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV45GridState.gxTpr_Dynamicfilters.Item(3));
                  AV36DynamicFiltersSelector3 = AV46GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV36DynamicFiltersSelector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
                  {
                     AV73DynamicFiltersOperator3 = AV46GridStateDynamicFilter.gxTpr_Operator;
                     AV37ContagemResultado_OsFsOsFm3 = AV46GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37ContagemResultado_OsFsOsFm3)) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        if ( AV73DynamicFiltersOperator3 == 0 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Igual)";
                        }
                        else if ( AV73DynamicFiltersOperator3 == 1 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Come�a com)";
                        }
                        else if ( AV73DynamicFiltersOperator3 == 2 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Cont�m)";
                        }
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV37ContagemResultado_OsFsOsFm3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV36DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATACNT") == 0 )
                  {
                     AV38ContagemResultado_DataCnt3 = context.localUtil.CToD( AV46GridStateDynamicFilter.gxTpr_Value, 2);
                     AV39ContagemResultado_DataCnt_To3 = context.localUtil.CToD( AV46GridStateDynamicFilter.gxTpr_Valueto, 2);
                     if ( ! (DateTime.MinValue==AV38ContagemResultado_DataCnt3) || ! (DateTime.MinValue==AV39ContagemResultado_DataCnt_To3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Contagem";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        GXt_dtime1 = DateTimeUtil.ResetTime( AV38ContagemResultado_DataCnt3 ) ;
                        AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                        GXt_dtime1 = DateTimeUtil.ResetTime( AV39ContagemResultado_DataCnt_To3 ) ;
                        AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV36DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 )
                  {
                     AV40ContagemResultado_DataDmn3 = context.localUtil.CToD( AV46GridStateDynamicFilter.gxTpr_Value, 2);
                     AV41ContagemResultado_DataDmn_To3 = context.localUtil.CToD( AV46GridStateDynamicFilter.gxTpr_Valueto, 2);
                     if ( ! (DateTime.MinValue==AV40ContagemResultado_DataDmn3) || ! (DateTime.MinValue==AV41ContagemResultado_DataDmn_To3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Demanda";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        GXt_dtime1 = DateTimeUtil.ResetTime( AV40ContagemResultado_DataDmn3 ) ;
                        AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                        GXt_dtime1 = DateTimeUtil.ResetTime( AV41ContagemResultado_DataDmn_To3 ) ;
                        AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV36DynamicFiltersSelector3, "CONTAGEMRESULTADO_CONTADORFM") == 0 )
                  {
                     AV70ContagemResultado_ContadorFM3 = (int)(NumberUtil.Val( AV46GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV70ContagemResultado_ContadorFM3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Usu�rio da Prestadora";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV70ContagemResultado_ContadorFM3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV36DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSCNT") == 0 )
                  {
                     AV43ContagemResultado_StatusCnt3 = (short)(NumberUtil.Val( AV46GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV43ContagemResultado_StatusCnt3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Status Contagem";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        if ( ! (0==AV43ContagemResultado_StatusCnt3) )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = gxdomainstatuscontagem.getDescription(context,AV43ContagemResultado_StatusCnt3);
                        }
                     }
                  }
                  else if ( StringUtil.StrCmp(AV36DynamicFiltersSelector3, "CONTAGEMRESULTADO_BASELINE") == 0 )
                  {
                     AV59ContagemResultado_Baseline3 = AV46GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59ContagemResultado_Baseline3)) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Baseline";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "";
                        if ( StringUtil.StrCmp(StringUtil.Trim( AV59ContagemResultado_Baseline3), "S") == 0 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Sim";
                        }
                        else if ( StringUtil.StrCmp(StringUtil.Trim( AV59ContagemResultado_Baseline3), "N") == 0 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "N�o";
                        }
                     }
                  }
               }
            }
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV77TFContratada_AreaTrabalhoDes_Sel)) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "�rea";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV77TFContratada_AreaTrabalhoDes_Sel;
         }
         else
         {
            if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV76TFContratada_AreaTrabalhoDes)) ) )
            {
               AV13CellRow = (int)(AV13CellRow+1);
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "�rea";
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV76TFContratada_AreaTrabalhoDes;
            }
         }
         if ( ! ( (DateTime.MinValue==AV78TFContagemResultado_DataDmn) && (DateTime.MinValue==AV79TFContagemResultado_DataDmn_To) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Demanda";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            GXt_dtime1 = DateTimeUtil.ResetTime( AV78TFContagemResultado_DataDmn ) ;
            AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "At�";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
            GXt_dtime1 = DateTimeUtil.ResetTime( AV79TFContagemResultado_DataDmn_To ) ;
            AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
         }
         if ( ! ( (DateTime.MinValue==AV80TFContagemResultado_DataCnt) && (DateTime.MinValue==AV81TFContagemResultado_DataCnt_To) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Contagem";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            GXt_dtime1 = DateTimeUtil.ResetTime( AV80TFContagemResultado_DataCnt ) ;
            AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "At�";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
            GXt_dtime1 = DateTimeUtil.ResetTime( AV81TFContagemResultado_DataCnt_To ) ;
            AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV83TFContagemResultado_HoraCnt_Sel)) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV83TFContagemResultado_HoraCnt_Sel;
         }
         else
         {
            if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV82TFContagemResultado_HoraCnt)) ) )
            {
               AV13CellRow = (int)(AV13CellRow+1);
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "";
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV82TFContagemResultado_HoraCnt;
            }
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV85TFContagemResultado_OsFsOsFm_Sel)) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS Ref|OS";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV85TFContagemResultado_OsFsOsFm_Sel;
         }
         else
         {
            if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV84TFContagemResultado_OsFsOsFm)) ) )
            {
               AV13CellRow = (int)(AV13CellRow+1);
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS Ref|OS";
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV84TFContagemResultado_OsFsOsFm;
            }
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV87TFContagemrResultado_SistemaSigla_Sel)) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Sistema";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV87TFContagemrResultado_SistemaSigla_Sel;
         }
         else
         {
            if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV86TFContagemrResultado_SistemaSigla)) ) )
            {
               AV13CellRow = (int)(AV13CellRow+1);
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Sistema";
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV86TFContagemrResultado_SistemaSigla;
            }
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV89TFContagemResultado_ContadorFMNom_Sel)) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Contador FM";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV89TFContagemResultado_ContadorFMNom_Sel;
         }
         else
         {
            if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV88TFContagemResultado_ContadorFMNom)) ) )
            {
               AV13CellRow = (int)(AV13CellRow+1);
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Contador FM";
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV88TFContagemResultado_ContadorFMNom;
            }
         }
         AV91TFContagemResultado_StatusCnt_Sels.FromJSonString(AV90TFContagemResultado_StatusCnt_SelsJson);
         if ( ! ( ( AV91TFContagemResultado_StatusCnt_Sels.Count == 0 ) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Status";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV103i = 1;
            AV106GXV1 = 1;
            while ( AV106GXV1 <= AV91TFContagemResultado_StatusCnt_Sels.Count )
            {
               AV92TFContagemResultado_StatusCnt_Sel = (short)(AV91TFContagemResultado_StatusCnt_Sels.GetNumeric(AV106GXV1));
               if ( AV103i == 1 )
               {
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "";
               }
               else
               {
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text+", ";
               }
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text+gxdomainstatuscontagem.getDescription(context,AV92TFContagemResultado_StatusCnt_Sel);
               AV103i = (long)(AV103i+1);
               AV106GXV1 = (int)(AV106GXV1+1);
            }
         }
         if ( ! ( (0==AV93TFContagemResultadoContagens_Esforco) && (0==AV94TFContagemResultadoContagens_Esforco_To) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Esfor�o";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV93TFContagemResultadoContagens_Esforco;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "At�";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Number = AV94TFContagemResultadoContagens_Esforco_To;
         }
         if ( ! ( (Convert.ToDecimal(0)==AV95TFContagemResultado_PFBFS) && (Convert.ToDecimal(0)==AV96TFContagemResultado_PFBFS_To) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "PFB FS";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = (double)(AV95TFContagemResultado_PFBFS);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "At�";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Number = (double)(AV96TFContagemResultado_PFBFS_To);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV97TFContagemResultado_PFLFS) && (Convert.ToDecimal(0)==AV98TFContagemResultado_PFLFS_To) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "PFL FS";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = (double)(AV97TFContagemResultado_PFLFS);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "At�";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Number = (double)(AV98TFContagemResultado_PFLFS_To);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV99TFContagemResultado_PFBFM) && (Convert.ToDecimal(0)==AV100TFContagemResultado_PFBFM_To) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "PFB FM";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = (double)(AV99TFContagemResultado_PFBFM);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "At�";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Number = (double)(AV100TFContagemResultado_PFBFM_To);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV101TFContagemResultado_PFLFM) && (Convert.ToDecimal(0)==AV102TFContagemResultado_PFLFM_To) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "PFL FM";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = (double)(AV101TFContagemResultado_PFLFM);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "At�";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Number = (double)(AV102TFContagemResultado_PFLFM_To);
         }
         AV13CellRow = (int)(AV13CellRow+2);
      }

      protected void S151( )
      {
         /* 'WRITECOLUMNTITLES' Routine */
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Text = "�rea";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Demanda";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "Contagem";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Text = "";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+4, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+4, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+4, 1, 1).Text = "OS Ref|OS";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+5, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+5, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+5, 1, 1).Text = "Sistema";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+6, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+6, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+6, 1, 1).Text = "Contador FM";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+7, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+7, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+7, 1, 1).Text = "Status";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+8, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+8, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+8, 1, 1).Text = "Esfor�o";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+9, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+9, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+9, 1, 1).Text = "PFB FS";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+10, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+10, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+10, 1, 1).Text = "PFL FS";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+11, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+11, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+11, 1, 1).Text = "PFB FM";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+12, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+12, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+12, 1, 1).Text = "PFL FM";
      }

      protected void S161( )
      {
         /* 'WRITEDATA' Routine */
         AV56PrimeiraLinha = AV13CellRow;
         AV108WWContagemResultadoContagensDS_1_Dynamicfiltersselector1 = AV18DynamicFiltersSelector1;
         AV109WWContagemResultadoContagensDS_2_Dynamicfiltersoperator1 = AV71DynamicFiltersOperator1;
         AV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 = AV19ContagemResultado_OsFsOsFm1;
         AV111WWContagemResultadoContagensDS_4_Contagemresultado_datacnt1 = AV20ContagemResultado_DataCnt1;
         AV112WWContagemResultadoContagensDS_5_Contagemresultado_datacnt_to1 = AV21ContagemResultado_DataCnt_To1;
         AV113WWContagemResultadoContagensDS_6_Contagemresultado_datadmn1 = AV22ContagemResultado_DataDmn1;
         AV114WWContagemResultadoContagensDS_7_Contagemresultado_datadmn_to1 = AV23ContagemResultado_DataDmn_To1;
         AV115WWContagemResultadoContagensDS_8_Contagemresultado_contadorfm1 = AV68ContagemResultado_ContadorFM1;
         AV116WWContagemResultadoContagensDS_9_Contagemresultado_statuscnt1 = AV25ContagemResultado_StatusCnt1;
         AV117WWContagemResultadoContagensDS_10_Contagemresultado_baseline1 = AV57ContagemResultado_Baseline1;
         AV118WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 = AV26DynamicFiltersEnabled2;
         AV119WWContagemResultadoContagensDS_12_Dynamicfiltersselector2 = AV27DynamicFiltersSelector2;
         AV120WWContagemResultadoContagensDS_13_Dynamicfiltersoperator2 = AV72DynamicFiltersOperator2;
         AV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 = AV28ContagemResultado_OsFsOsFm2;
         AV122WWContagemResultadoContagensDS_15_Contagemresultado_datacnt2 = AV29ContagemResultado_DataCnt2;
         AV123WWContagemResultadoContagensDS_16_Contagemresultado_datacnt_to2 = AV30ContagemResultado_DataCnt_To2;
         AV124WWContagemResultadoContagensDS_17_Contagemresultado_datadmn2 = AV31ContagemResultado_DataDmn2;
         AV125WWContagemResultadoContagensDS_18_Contagemresultado_datadmn_to2 = AV32ContagemResultado_DataDmn_To2;
         AV126WWContagemResultadoContagensDS_19_Contagemresultado_contadorfm2 = AV69ContagemResultado_ContadorFM2;
         AV127WWContagemResultadoContagensDS_20_Contagemresultado_statuscnt2 = AV34ContagemResultado_StatusCnt2;
         AV128WWContagemResultadoContagensDS_21_Contagemresultado_baseline2 = AV58ContagemResultado_Baseline2;
         AV129WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 = AV35DynamicFiltersEnabled3;
         AV130WWContagemResultadoContagensDS_23_Dynamicfiltersselector3 = AV36DynamicFiltersSelector3;
         AV131WWContagemResultadoContagensDS_24_Dynamicfiltersoperator3 = AV73DynamicFiltersOperator3;
         AV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 = AV37ContagemResultado_OsFsOsFm3;
         AV133WWContagemResultadoContagensDS_26_Contagemresultado_datacnt3 = AV38ContagemResultado_DataCnt3;
         AV134WWContagemResultadoContagensDS_27_Contagemresultado_datacnt_to3 = AV39ContagemResultado_DataCnt_To3;
         AV135WWContagemResultadoContagensDS_28_Contagemresultado_datadmn3 = AV40ContagemResultado_DataDmn3;
         AV136WWContagemResultadoContagensDS_29_Contagemresultado_datadmn_to3 = AV41ContagemResultado_DataDmn_To3;
         AV137WWContagemResultadoContagensDS_30_Contagemresultado_contadorfm3 = AV70ContagemResultado_ContadorFM3;
         AV138WWContagemResultadoContagensDS_31_Contagemresultado_statuscnt3 = AV43ContagemResultado_StatusCnt3;
         AV139WWContagemResultadoContagensDS_32_Contagemresultado_baseline3 = AV59ContagemResultado_Baseline3;
         AV140WWContagemResultadoContagensDS_33_Tfcontratada_areatrabalhodes = AV76TFContratada_AreaTrabalhoDes;
         AV141WWContagemResultadoContagensDS_34_Tfcontratada_areatrabalhodes_sel = AV77TFContratada_AreaTrabalhoDes_Sel;
         AV142WWContagemResultadoContagensDS_35_Tfcontagemresultado_datadmn = AV78TFContagemResultado_DataDmn;
         AV143WWContagemResultadoContagensDS_36_Tfcontagemresultado_datadmn_to = AV79TFContagemResultado_DataDmn_To;
         AV144WWContagemResultadoContagensDS_37_Tfcontagemresultado_datacnt = AV80TFContagemResultado_DataCnt;
         AV145WWContagemResultadoContagensDS_38_Tfcontagemresultado_datacnt_to = AV81TFContagemResultado_DataCnt_To;
         AV146WWContagemResultadoContagensDS_39_Tfcontagemresultado_horacnt = AV82TFContagemResultado_HoraCnt;
         AV147WWContagemResultadoContagensDS_40_Tfcontagemresultado_horacnt_sel = AV83TFContagemResultado_HoraCnt_Sel;
         AV148WWContagemResultadoContagensDS_41_Tfcontagemresultado_osfsosfm = AV84TFContagemResultado_OsFsOsFm;
         AV149WWContagemResultadoContagensDS_42_Tfcontagemresultado_osfsosfm_sel = AV85TFContagemResultado_OsFsOsFm_Sel;
         AV150WWContagemResultadoContagensDS_43_Tfcontagemrresultado_sistemasigla = AV86TFContagemrResultado_SistemaSigla;
         AV151WWContagemResultadoContagensDS_44_Tfcontagemrresultado_sistemasigla_sel = AV87TFContagemrResultado_SistemaSigla_Sel;
         AV152WWContagemResultadoContagensDS_45_Tfcontagemresultado_contadorfmnom = AV88TFContagemResultado_ContadorFMNom;
         AV153WWContagemResultadoContagensDS_46_Tfcontagemresultado_contadorfmnom_sel = AV89TFContagemResultado_ContadorFMNom_Sel;
         AV154WWContagemResultadoContagensDS_47_Tfcontagemresultado_statuscnt_sels = AV91TFContagemResultado_StatusCnt_Sels;
         AV155WWContagemResultadoContagensDS_48_Tfcontagemresultadocontagens_esforco = AV93TFContagemResultadoContagens_Esforco;
         AV156WWContagemResultadoContagensDS_49_Tfcontagemresultadocontagens_esforco_to = AV94TFContagemResultadoContagens_Esforco_To;
         AV157WWContagemResultadoContagensDS_50_Tfcontagemresultado_pfbfs = AV95TFContagemResultado_PFBFS;
         AV158WWContagemResultadoContagensDS_51_Tfcontagemresultado_pfbfs_to = AV96TFContagemResultado_PFBFS_To;
         AV159WWContagemResultadoContagensDS_52_Tfcontagemresultado_pflfs = AV97TFContagemResultado_PFLFS;
         AV160WWContagemResultadoContagensDS_53_Tfcontagemresultado_pflfs_to = AV98TFContagemResultado_PFLFS_To;
         AV161WWContagemResultadoContagensDS_54_Tfcontagemresultado_pfbfm = AV99TFContagemResultado_PFBFM;
         AV162WWContagemResultadoContagensDS_55_Tfcontagemresultado_pfbfm_to = AV100TFContagemResultado_PFBFM_To;
         AV163WWContagemResultadoContagensDS_56_Tfcontagemresultado_pflfm = AV101TFContagemResultado_PFLFM;
         AV164WWContagemResultadoContagensDS_57_Tfcontagemresultado_pflfm_to = AV102TFContagemResultado_PFLFM_To;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A483ContagemResultado_StatusCnt ,
                                              AV154WWContagemResultadoContagensDS_47_Tfcontagemresultado_statuscnt_sels ,
                                              A490ContagemResultado_ContratadaCod ,
                                              AV66Contratadas ,
                                              AV108WWContagemResultadoContagensDS_1_Dynamicfiltersselector1 ,
                                              AV109WWContagemResultadoContagensDS_2_Dynamicfiltersoperator1 ,
                                              AV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 ,
                                              AV111WWContagemResultadoContagensDS_4_Contagemresultado_datacnt1 ,
                                              AV112WWContagemResultadoContagensDS_5_Contagemresultado_datacnt_to1 ,
                                              AV113WWContagemResultadoContagensDS_6_Contagemresultado_datadmn1 ,
                                              AV114WWContagemResultadoContagensDS_7_Contagemresultado_datadmn_to1 ,
                                              AV116WWContagemResultadoContagensDS_9_Contagemresultado_statuscnt1 ,
                                              AV117WWContagemResultadoContagensDS_10_Contagemresultado_baseline1 ,
                                              AV118WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 ,
                                              AV119WWContagemResultadoContagensDS_12_Dynamicfiltersselector2 ,
                                              AV120WWContagemResultadoContagensDS_13_Dynamicfiltersoperator2 ,
                                              AV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 ,
                                              AV122WWContagemResultadoContagensDS_15_Contagemresultado_datacnt2 ,
                                              AV123WWContagemResultadoContagensDS_16_Contagemresultado_datacnt_to2 ,
                                              AV124WWContagemResultadoContagensDS_17_Contagemresultado_datadmn2 ,
                                              AV125WWContagemResultadoContagensDS_18_Contagemresultado_datadmn_to2 ,
                                              AV127WWContagemResultadoContagensDS_20_Contagemresultado_statuscnt2 ,
                                              AV128WWContagemResultadoContagensDS_21_Contagemresultado_baseline2 ,
                                              AV129WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 ,
                                              AV130WWContagemResultadoContagensDS_23_Dynamicfiltersselector3 ,
                                              AV131WWContagemResultadoContagensDS_24_Dynamicfiltersoperator3 ,
                                              AV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 ,
                                              AV133WWContagemResultadoContagensDS_26_Contagemresultado_datacnt3 ,
                                              AV134WWContagemResultadoContagensDS_27_Contagemresultado_datacnt_to3 ,
                                              AV135WWContagemResultadoContagensDS_28_Contagemresultado_datadmn3 ,
                                              AV136WWContagemResultadoContagensDS_29_Contagemresultado_datadmn_to3 ,
                                              AV138WWContagemResultadoContagensDS_31_Contagemresultado_statuscnt3 ,
                                              AV139WWContagemResultadoContagensDS_32_Contagemresultado_baseline3 ,
                                              AV141WWContagemResultadoContagensDS_34_Tfcontratada_areatrabalhodes_sel ,
                                              AV140WWContagemResultadoContagensDS_33_Tfcontratada_areatrabalhodes ,
                                              AV142WWContagemResultadoContagensDS_35_Tfcontagemresultado_datadmn ,
                                              AV143WWContagemResultadoContagensDS_36_Tfcontagemresultado_datadmn_to ,
                                              AV144WWContagemResultadoContagensDS_37_Tfcontagemresultado_datacnt ,
                                              AV145WWContagemResultadoContagensDS_38_Tfcontagemresultado_datacnt_to ,
                                              AV147WWContagemResultadoContagensDS_40_Tfcontagemresultado_horacnt_sel ,
                                              AV146WWContagemResultadoContagensDS_39_Tfcontagemresultado_horacnt ,
                                              AV149WWContagemResultadoContagensDS_42_Tfcontagemresultado_osfsosfm_sel ,
                                              AV148WWContagemResultadoContagensDS_41_Tfcontagemresultado_osfsosfm ,
                                              AV151WWContagemResultadoContagensDS_44_Tfcontagemrresultado_sistemasigla_sel ,
                                              AV150WWContagemResultadoContagensDS_43_Tfcontagemrresultado_sistemasigla ,
                                              AV153WWContagemResultadoContagensDS_46_Tfcontagemresultado_contadorfmnom_sel ,
                                              AV152WWContagemResultadoContagensDS_45_Tfcontagemresultado_contadorfmnom ,
                                              AV154WWContagemResultadoContagensDS_47_Tfcontagemresultado_statuscnt_sels.Count ,
                                              AV155WWContagemResultadoContagensDS_48_Tfcontagemresultadocontagens_esforco ,
                                              AV156WWContagemResultadoContagensDS_49_Tfcontagemresultadocontagens_esforco_to ,
                                              AV157WWContagemResultadoContagensDS_50_Tfcontagemresultado_pfbfs ,
                                              AV158WWContagemResultadoContagensDS_51_Tfcontagemresultado_pfbfs_to ,
                                              AV159WWContagemResultadoContagensDS_52_Tfcontagemresultado_pflfs ,
                                              AV160WWContagemResultadoContagensDS_53_Tfcontagemresultado_pflfs_to ,
                                              AV161WWContagemResultadoContagensDS_54_Tfcontagemresultado_pfbfm ,
                                              AV162WWContagemResultadoContagensDS_55_Tfcontagemresultado_pfbfm_to ,
                                              AV163WWContagemResultadoContagensDS_56_Tfcontagemresultado_pflfm ,
                                              AV164WWContagemResultadoContagensDS_57_Tfcontagemresultado_pflfm_to ,
                                              AV67Usuario_EhGestor ,
                                              AV9WWPContext.gxTpr_Userehfinanceiro ,
                                              AV9WWPContext.gxTpr_Userehcontratante ,
                                              AV55Flag ,
                                              AV9WWPContext.gxTpr_Contratada_codigo ,
                                              AV45GridState.gxTpr_Dynamicfilters.Count ,
                                              A457ContagemResultado_Demanda ,
                                              A493ContagemResultado_DemandaFM ,
                                              A473ContagemResultado_DataCnt ,
                                              A471ContagemResultado_DataDmn ,
                                              A598ContagemResultado_Baseline ,
                                              A53Contratada_AreaTrabalhoDes ,
                                              A511ContagemResultado_HoraCnt ,
                                              A509ContagemrResultado_SistemaSigla ,
                                              A474ContagemResultado_ContadorFMNom ,
                                              A482ContagemResultadoContagens_Esforco ,
                                              A458ContagemResultado_PFBFS ,
                                              A459ContagemResultado_PFLFS ,
                                              A460ContagemResultado_PFBFM ,
                                              A461ContagemResultado_PFLFM ,
                                              A470ContagemResultado_ContadorFMCod ,
                                              AV9WWPContext.gxTpr_Userid ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo ,
                                              Gx_date ,
                                              AV16OrderedBy ,
                                              AV17OrderedDsc ,
                                              AV115WWContagemResultadoContagensDS_8_Contagemresultado_contadorfm1 ,
                                              A584ContagemResultado_ContadorFM ,
                                              A508ContagemResultado_Owner ,
                                              AV126WWContagemResultadoContagensDS_19_Contagemresultado_contadorfm2 ,
                                              AV137WWContagemResultadoContagensDS_30_Contagemresultado_contadorfm3 },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN,
                                              TypeConstants.SHORT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.DECIMAL,
                                              TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT
                                              }
         });
         lV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1), "%", "");
         lV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1), "%", "");
         lV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1), "%", "");
         lV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1), "%", "");
         lV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2), "%", "");
         lV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2), "%", "");
         lV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2), "%", "");
         lV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2), "%", "");
         lV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3), "%", "");
         lV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3), "%", "");
         lV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3), "%", "");
         lV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3), "%", "");
         lV140WWContagemResultadoContagensDS_33_Tfcontratada_areatrabalhodes = StringUtil.Concat( StringUtil.RTrim( AV140WWContagemResultadoContagensDS_33_Tfcontratada_areatrabalhodes), "%", "");
         lV146WWContagemResultadoContagensDS_39_Tfcontagemresultado_horacnt = StringUtil.PadR( StringUtil.RTrim( AV146WWContagemResultadoContagensDS_39_Tfcontagemresultado_horacnt), 5, "%");
         lV148WWContagemResultadoContagensDS_41_Tfcontagemresultado_osfsosfm = StringUtil.Concat( StringUtil.RTrim( AV148WWContagemResultadoContagensDS_41_Tfcontagemresultado_osfsosfm), "%", "");
         lV150WWContagemResultadoContagensDS_43_Tfcontagemrresultado_sistemasigla = StringUtil.PadR( StringUtil.RTrim( AV150WWContagemResultadoContagensDS_43_Tfcontagemrresultado_sistemasigla), 25, "%");
         lV152WWContagemResultadoContagensDS_45_Tfcontagemresultado_contadorfmnom = StringUtil.PadR( StringUtil.RTrim( AV152WWContagemResultadoContagensDS_45_Tfcontagemresultado_contadorfmnom), 100, "%");
         /* Using cursor P003X3 */
         pr_default.execute(0, new Object[] {AV108WWContagemResultadoContagensDS_1_Dynamicfiltersselector1, AV115WWContagemResultadoContagensDS_8_Contagemresultado_contadorfm1, AV115WWContagemResultadoContagensDS_8_Contagemresultado_contadorfm1, AV115WWContagemResultadoContagensDS_8_Contagemresultado_contadorfm1, AV118WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2, AV119WWContagemResultadoContagensDS_12_Dynamicfiltersselector2, AV126WWContagemResultadoContagensDS_19_Contagemresultado_contadorfm2, AV126WWContagemResultadoContagensDS_19_Contagemresultado_contadorfm2, AV126WWContagemResultadoContagensDS_19_Contagemresultado_contadorfm2, AV129WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3, AV130WWContagemResultadoContagensDS_23_Dynamicfiltersselector3, AV137WWContagemResultadoContagensDS_30_Contagemresultado_contadorfm3, AV137WWContagemResultadoContagensDS_30_Contagemresultado_contadorfm3, AV137WWContagemResultadoContagensDS_30_Contagemresultado_contadorfm3, AV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1, AV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1, lV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1, lV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1, lV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1, lV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1, AV111WWContagemResultadoContagensDS_4_Contagemresultado_datacnt1, AV112WWContagemResultadoContagensDS_5_Contagemresultado_datacnt_to1, AV113WWContagemResultadoContagensDS_6_Contagemresultado_datadmn1, AV114WWContagemResultadoContagensDS_7_Contagemresultado_datadmn_to1, AV116WWContagemResultadoContagensDS_9_Contagemresultado_statuscnt1, AV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2, AV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2, lV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2, lV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2, lV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2, lV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2, AV122WWContagemResultadoContagensDS_15_Contagemresultado_datacnt2, AV123WWContagemResultadoContagensDS_16_Contagemresultado_datacnt_to2, AV124WWContagemResultadoContagensDS_17_Contagemresultado_datadmn2, AV125WWContagemResultadoContagensDS_18_Contagemresultado_datadmn_to2, AV127WWContagemResultadoContagensDS_20_Contagemresultado_statuscnt2, AV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3, AV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3, lV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3, lV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3, lV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3, lV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3, AV133WWContagemResultadoContagensDS_26_Contagemresultado_datacnt3, AV134WWContagemResultadoContagensDS_27_Contagemresultado_datacnt_to3, AV135WWContagemResultadoContagensDS_28_Contagemresultado_datadmn3, AV136WWContagemResultadoContagensDS_29_Contagemresultado_datadmn_to3, AV138WWContagemResultadoContagensDS_31_Contagemresultado_statuscnt3, lV140WWContagemResultadoContagensDS_33_Tfcontratada_areatrabalhodes, AV141WWContagemResultadoContagensDS_34_Tfcontratada_areatrabalhodes_sel, AV142WWContagemResultadoContagensDS_35_Tfcontagemresultado_datadmn, AV143WWContagemResultadoContagensDS_36_Tfcontagemresultado_datadmn_to, AV144WWContagemResultadoContagensDS_37_Tfcontagemresultado_datacnt, AV145WWContagemResultadoContagensDS_38_Tfcontagemresultado_datacnt_to, lV146WWContagemResultadoContagensDS_39_Tfcontagemresultado_horacnt, AV147WWContagemResultadoContagensDS_40_Tfcontagemresultado_horacnt_sel, lV148WWContagemResultadoContagensDS_41_Tfcontagemresultado_osfsosfm, AV149WWContagemResultadoContagensDS_42_Tfcontagemresultado_osfsosfm_sel, lV150WWContagemResultadoContagensDS_43_Tfcontagemrresultado_sistemasigla, AV151WWContagemResultadoContagensDS_44_Tfcontagemrresultado_sistemasigla_sel, lV152WWContagemResultadoContagensDS_45_Tfcontagemresultado_contadorfmnom, AV153WWContagemResultadoContagensDS_46_Tfcontagemresultado_contadorfmnom_sel, AV155WWContagemResultadoContagensDS_48_Tfcontagemresultadocontagens_esforco, AV156WWContagemResultadoContagensDS_49_Tfcontagemresultadocontagens_esforco_to, AV157WWContagemResultadoContagensDS_50_Tfcontagemresultado_pfbfs, AV158WWContagemResultadoContagensDS_51_Tfcontagemresultado_pfbfs_to, AV159WWContagemResultadoContagensDS_52_Tfcontagemresultado_pflfs, AV160WWContagemResultadoContagensDS_53_Tfcontagemresultado_pflfs_to, AV161WWContagemResultadoContagensDS_54_Tfcontagemresultado_pfbfm, AV162WWContagemResultadoContagensDS_55_Tfcontagemresultado_pfbfm_to, AV163WWContagemResultadoContagensDS_56_Tfcontagemresultado_pflfm, AV164WWContagemResultadoContagensDS_57_Tfcontagemresultado_pflfm_to, AV9WWPContext.gxTpr_Userid, AV9WWPContext.gxTpr_Areatrabalho_codigo, Gx_date});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A489ContagemResultado_SistemaCod = P003X3_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P003X3_n489ContagemResultado_SistemaCod[0];
            A479ContagemResultado_CrFMPessoaCod = P003X3_A479ContagemResultado_CrFMPessoaCod[0];
            n479ContagemResultado_CrFMPessoaCod = P003X3_n479ContagemResultado_CrFMPessoaCod[0];
            A490ContagemResultado_ContratadaCod = P003X3_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P003X3_n490ContagemResultado_ContratadaCod[0];
            A52Contratada_AreaTrabalhoCod = P003X3_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P003X3_n52Contratada_AreaTrabalhoCod[0];
            A470ContagemResultado_ContadorFMCod = P003X3_A470ContagemResultado_ContadorFMCod[0];
            A461ContagemResultado_PFLFM = P003X3_A461ContagemResultado_PFLFM[0];
            n461ContagemResultado_PFLFM = P003X3_n461ContagemResultado_PFLFM[0];
            A460ContagemResultado_PFBFM = P003X3_A460ContagemResultado_PFBFM[0];
            n460ContagemResultado_PFBFM = P003X3_n460ContagemResultado_PFBFM[0];
            A459ContagemResultado_PFLFS = P003X3_A459ContagemResultado_PFLFS[0];
            n459ContagemResultado_PFLFS = P003X3_n459ContagemResultado_PFLFS[0];
            A458ContagemResultado_PFBFS = P003X3_A458ContagemResultado_PFBFS[0];
            n458ContagemResultado_PFBFS = P003X3_n458ContagemResultado_PFBFS[0];
            A482ContagemResultadoContagens_Esforco = P003X3_A482ContagemResultadoContagens_Esforco[0];
            A474ContagemResultado_ContadorFMNom = P003X3_A474ContagemResultado_ContadorFMNom[0];
            n474ContagemResultado_ContadorFMNom = P003X3_n474ContagemResultado_ContadorFMNom[0];
            A509ContagemrResultado_SistemaSigla = P003X3_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P003X3_n509ContagemrResultado_SistemaSigla[0];
            A511ContagemResultado_HoraCnt = P003X3_A511ContagemResultado_HoraCnt[0];
            A53Contratada_AreaTrabalhoDes = P003X3_A53Contratada_AreaTrabalhoDes[0];
            n53Contratada_AreaTrabalhoDes = P003X3_n53Contratada_AreaTrabalhoDes[0];
            A598ContagemResultado_Baseline = P003X3_A598ContagemResultado_Baseline[0];
            n598ContagemResultado_Baseline = P003X3_n598ContagemResultado_Baseline[0];
            A483ContagemResultado_StatusCnt = P003X3_A483ContagemResultado_StatusCnt[0];
            A508ContagemResultado_Owner = P003X3_A508ContagemResultado_Owner[0];
            A471ContagemResultado_DataDmn = P003X3_A471ContagemResultado_DataDmn[0];
            A473ContagemResultado_DataCnt = P003X3_A473ContagemResultado_DataCnt[0];
            A456ContagemResultado_Codigo = P003X3_A456ContagemResultado_Codigo[0];
            A584ContagemResultado_ContadorFM = P003X3_A584ContagemResultado_ContadorFM[0];
            n584ContagemResultado_ContadorFM = P003X3_n584ContagemResultado_ContadorFM[0];
            A493ContagemResultado_DemandaFM = P003X3_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P003X3_n493ContagemResultado_DemandaFM[0];
            A457ContagemResultado_Demanda = P003X3_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P003X3_n457ContagemResultado_Demanda[0];
            A479ContagemResultado_CrFMPessoaCod = P003X3_A479ContagemResultado_CrFMPessoaCod[0];
            n479ContagemResultado_CrFMPessoaCod = P003X3_n479ContagemResultado_CrFMPessoaCod[0];
            A474ContagemResultado_ContadorFMNom = P003X3_A474ContagemResultado_ContadorFMNom[0];
            n474ContagemResultado_ContadorFMNom = P003X3_n474ContagemResultado_ContadorFMNom[0];
            A489ContagemResultado_SistemaCod = P003X3_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P003X3_n489ContagemResultado_SistemaCod[0];
            A490ContagemResultado_ContratadaCod = P003X3_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P003X3_n490ContagemResultado_ContratadaCod[0];
            A598ContagemResultado_Baseline = P003X3_A598ContagemResultado_Baseline[0];
            n598ContagemResultado_Baseline = P003X3_n598ContagemResultado_Baseline[0];
            A508ContagemResultado_Owner = P003X3_A508ContagemResultado_Owner[0];
            A471ContagemResultado_DataDmn = P003X3_A471ContagemResultado_DataDmn[0];
            A493ContagemResultado_DemandaFM = P003X3_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P003X3_n493ContagemResultado_DemandaFM[0];
            A457ContagemResultado_Demanda = P003X3_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P003X3_n457ContagemResultado_Demanda[0];
            A509ContagemrResultado_SistemaSigla = P003X3_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P003X3_n509ContagemrResultado_SistemaSigla[0];
            A52Contratada_AreaTrabalhoCod = P003X3_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P003X3_n52Contratada_AreaTrabalhoCod[0];
            A53Contratada_AreaTrabalhoDes = P003X3_A53Contratada_AreaTrabalhoDes[0];
            n53Contratada_AreaTrabalhoDes = P003X3_n53Contratada_AreaTrabalhoDes[0];
            A584ContagemResultado_ContadorFM = P003X3_A584ContagemResultado_ContadorFM[0];
            n584ContagemResultado_ContadorFM = P003X3_n584ContagemResultado_ContadorFM[0];
            A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
            AV13CellRow = (int)(AV13CellRow+1);
            /* Execute user subroutine: 'BEFOREWRITELINE' */
            S172 ();
            if ( returnInSub )
            {
               pr_default.close(0);
               returnInSub = true;
               if (true) return;
            }
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Text = A53Contratada_AreaTrabalhoDes;
            GXt_dtime1 = DateTimeUtil.ResetTime( A471ContagemResultado_DataDmn ) ;
            AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
            GXt_dtime1 = DateTimeUtil.ResetTime( A473ContagemResultado_DataCnt ) ;
            AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Date = GXt_dtime1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Text = A511ContagemResultado_HoraCnt;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+4, 1, 1).Text = A501ContagemResultado_OsFsOsFm;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+5, 1, 1).Text = A509ContagemrResultado_SistemaSigla;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+6, 1, 1).Text = A474ContagemResultado_ContadorFMNom;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+7, 1, 1).Text = gxdomainstatuscontagem.getDescription(context,A483ContagemResultado_StatusCnt);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+8, 1, 1).Number = A482ContagemResultadoContagens_Esforco;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+9, 1, 1).Number = (double)(A458ContagemResultado_PFBFS);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+10, 1, 1).Number = (double)(A459ContagemResultado_PFLFS);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+11, 1, 1).Number = (double)(A460ContagemResultado_PFBFM);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+12, 1, 1).Number = (double)(A461ContagemResultado_PFLFM);
            /* Execute user subroutine: 'AFTERWRITELINE' */
            S182 ();
            if ( returnInSub )
            {
               pr_default.close(0);
               returnInSub = true;
               if (true) return;
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
         AV108WWContagemResultadoContagensDS_1_Dynamicfiltersselector1 = AV18DynamicFiltersSelector1;
         AV109WWContagemResultadoContagensDS_2_Dynamicfiltersoperator1 = AV71DynamicFiltersOperator1;
         AV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 = AV19ContagemResultado_OsFsOsFm1;
         AV111WWContagemResultadoContagensDS_4_Contagemresultado_datacnt1 = AV20ContagemResultado_DataCnt1;
         AV112WWContagemResultadoContagensDS_5_Contagemresultado_datacnt_to1 = AV21ContagemResultado_DataCnt_To1;
         AV113WWContagemResultadoContagensDS_6_Contagemresultado_datadmn1 = AV22ContagemResultado_DataDmn1;
         AV114WWContagemResultadoContagensDS_7_Contagemresultado_datadmn_to1 = AV23ContagemResultado_DataDmn_To1;
         AV115WWContagemResultadoContagensDS_8_Contagemresultado_contadorfm1 = AV68ContagemResultado_ContadorFM1;
         AV116WWContagemResultadoContagensDS_9_Contagemresultado_statuscnt1 = AV25ContagemResultado_StatusCnt1;
         AV117WWContagemResultadoContagensDS_10_Contagemresultado_baseline1 = AV57ContagemResultado_Baseline1;
         AV118WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 = AV26DynamicFiltersEnabled2;
         AV119WWContagemResultadoContagensDS_12_Dynamicfiltersselector2 = AV27DynamicFiltersSelector2;
         AV120WWContagemResultadoContagensDS_13_Dynamicfiltersoperator2 = AV72DynamicFiltersOperator2;
         AV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 = AV28ContagemResultado_OsFsOsFm2;
         AV122WWContagemResultadoContagensDS_15_Contagemresultado_datacnt2 = AV29ContagemResultado_DataCnt2;
         AV123WWContagemResultadoContagensDS_16_Contagemresultado_datacnt_to2 = AV30ContagemResultado_DataCnt_To2;
         AV124WWContagemResultadoContagensDS_17_Contagemresultado_datadmn2 = AV31ContagemResultado_DataDmn2;
         AV125WWContagemResultadoContagensDS_18_Contagemresultado_datadmn_to2 = AV32ContagemResultado_DataDmn_To2;
         AV126WWContagemResultadoContagensDS_19_Contagemresultado_contadorfm2 = AV69ContagemResultado_ContadorFM2;
         AV127WWContagemResultadoContagensDS_20_Contagemresultado_statuscnt2 = AV34ContagemResultado_StatusCnt2;
         AV128WWContagemResultadoContagensDS_21_Contagemresultado_baseline2 = AV58ContagemResultado_Baseline2;
         AV129WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 = AV35DynamicFiltersEnabled3;
         AV130WWContagemResultadoContagensDS_23_Dynamicfiltersselector3 = AV36DynamicFiltersSelector3;
         AV131WWContagemResultadoContagensDS_24_Dynamicfiltersoperator3 = AV73DynamicFiltersOperator3;
         AV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 = AV37ContagemResultado_OsFsOsFm3;
         AV133WWContagemResultadoContagensDS_26_Contagemresultado_datacnt3 = AV38ContagemResultado_DataCnt3;
         AV134WWContagemResultadoContagensDS_27_Contagemresultado_datacnt_to3 = AV39ContagemResultado_DataCnt_To3;
         AV135WWContagemResultadoContagensDS_28_Contagemresultado_datadmn3 = AV40ContagemResultado_DataDmn3;
         AV136WWContagemResultadoContagensDS_29_Contagemresultado_datadmn_to3 = AV41ContagemResultado_DataDmn_To3;
         AV137WWContagemResultadoContagensDS_30_Contagemresultado_contadorfm3 = AV70ContagemResultado_ContadorFM3;
         AV138WWContagemResultadoContagensDS_31_Contagemresultado_statuscnt3 = AV43ContagemResultado_StatusCnt3;
         AV139WWContagemResultadoContagensDS_32_Contagemresultado_baseline3 = AV59ContagemResultado_Baseline3;
         AV140WWContagemResultadoContagensDS_33_Tfcontratada_areatrabalhodes = AV76TFContratada_AreaTrabalhoDes;
         AV141WWContagemResultadoContagensDS_34_Tfcontratada_areatrabalhodes_sel = AV77TFContratada_AreaTrabalhoDes_Sel;
         AV142WWContagemResultadoContagensDS_35_Tfcontagemresultado_datadmn = AV78TFContagemResultado_DataDmn;
         AV143WWContagemResultadoContagensDS_36_Tfcontagemresultado_datadmn_to = AV79TFContagemResultado_DataDmn_To;
         AV144WWContagemResultadoContagensDS_37_Tfcontagemresultado_datacnt = AV80TFContagemResultado_DataCnt;
         AV145WWContagemResultadoContagensDS_38_Tfcontagemresultado_datacnt_to = AV81TFContagemResultado_DataCnt_To;
         AV146WWContagemResultadoContagensDS_39_Tfcontagemresultado_horacnt = AV82TFContagemResultado_HoraCnt;
         AV147WWContagemResultadoContagensDS_40_Tfcontagemresultado_horacnt_sel = AV83TFContagemResultado_HoraCnt_Sel;
         AV148WWContagemResultadoContagensDS_41_Tfcontagemresultado_osfsosfm = AV84TFContagemResultado_OsFsOsFm;
         AV149WWContagemResultadoContagensDS_42_Tfcontagemresultado_osfsosfm_sel = AV85TFContagemResultado_OsFsOsFm_Sel;
         AV150WWContagemResultadoContagensDS_43_Tfcontagemrresultado_sistemasigla = AV86TFContagemrResultado_SistemaSigla;
         AV151WWContagemResultadoContagensDS_44_Tfcontagemrresultado_sistemasigla_sel = AV87TFContagemrResultado_SistemaSigla_Sel;
         AV152WWContagemResultadoContagensDS_45_Tfcontagemresultado_contadorfmnom = AV88TFContagemResultado_ContadorFMNom;
         AV153WWContagemResultadoContagensDS_46_Tfcontagemresultado_contadorfmnom_sel = AV89TFContagemResultado_ContadorFMNom_Sel;
         AV154WWContagemResultadoContagensDS_47_Tfcontagemresultado_statuscnt_sels = AV91TFContagemResultado_StatusCnt_Sels;
         AV155WWContagemResultadoContagensDS_48_Tfcontagemresultadocontagens_esforco = AV93TFContagemResultadoContagens_Esforco;
         AV156WWContagemResultadoContagensDS_49_Tfcontagemresultadocontagens_esforco_to = AV94TFContagemResultadoContagens_Esforco_To;
         AV157WWContagemResultadoContagensDS_50_Tfcontagemresultado_pfbfs = AV95TFContagemResultado_PFBFS;
         AV158WWContagemResultadoContagensDS_51_Tfcontagemresultado_pfbfs_to = AV96TFContagemResultado_PFBFS_To;
         AV159WWContagemResultadoContagensDS_52_Tfcontagemresultado_pflfs = AV97TFContagemResultado_PFLFS;
         AV160WWContagemResultadoContagensDS_53_Tfcontagemresultado_pflfs_to = AV98TFContagemResultado_PFLFS_To;
         AV161WWContagemResultadoContagensDS_54_Tfcontagemresultado_pfbfm = AV99TFContagemResultado_PFBFM;
         AV162WWContagemResultadoContagensDS_55_Tfcontagemresultado_pfbfm_to = AV100TFContagemResultado_PFBFM_To;
         AV163WWContagemResultadoContagensDS_56_Tfcontagemresultado_pflfm = AV101TFContagemResultado_PFLFM;
         AV164WWContagemResultadoContagensDS_57_Tfcontagemresultado_pflfm_to = AV102TFContagemResultado_PFLFM_To;
         /* Optimized group. */
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A483ContagemResultado_StatusCnt ,
                                              AV154WWContagemResultadoContagensDS_47_Tfcontagemresultado_statuscnt_sels ,
                                              AV108WWContagemResultadoContagensDS_1_Dynamicfiltersselector1 ,
                                              AV109WWContagemResultadoContagensDS_2_Dynamicfiltersoperator1 ,
                                              AV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 ,
                                              AV111WWContagemResultadoContagensDS_4_Contagemresultado_datacnt1 ,
                                              AV112WWContagemResultadoContagensDS_5_Contagemresultado_datacnt_to1 ,
                                              AV113WWContagemResultadoContagensDS_6_Contagemresultado_datadmn1 ,
                                              AV114WWContagemResultadoContagensDS_7_Contagemresultado_datadmn_to1 ,
                                              AV116WWContagemResultadoContagensDS_9_Contagemresultado_statuscnt1 ,
                                              AV117WWContagemResultadoContagensDS_10_Contagemresultado_baseline1 ,
                                              AV118WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 ,
                                              AV119WWContagemResultadoContagensDS_12_Dynamicfiltersselector2 ,
                                              AV120WWContagemResultadoContagensDS_13_Dynamicfiltersoperator2 ,
                                              AV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 ,
                                              AV122WWContagemResultadoContagensDS_15_Contagemresultado_datacnt2 ,
                                              AV123WWContagemResultadoContagensDS_16_Contagemresultado_datacnt_to2 ,
                                              AV124WWContagemResultadoContagensDS_17_Contagemresultado_datadmn2 ,
                                              AV125WWContagemResultadoContagensDS_18_Contagemresultado_datadmn_to2 ,
                                              AV127WWContagemResultadoContagensDS_20_Contagemresultado_statuscnt2 ,
                                              AV128WWContagemResultadoContagensDS_21_Contagemresultado_baseline2 ,
                                              AV129WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 ,
                                              AV130WWContagemResultadoContagensDS_23_Dynamicfiltersselector3 ,
                                              AV131WWContagemResultadoContagensDS_24_Dynamicfiltersoperator3 ,
                                              AV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 ,
                                              AV133WWContagemResultadoContagensDS_26_Contagemresultado_datacnt3 ,
                                              AV134WWContagemResultadoContagensDS_27_Contagemresultado_datacnt_to3 ,
                                              AV135WWContagemResultadoContagensDS_28_Contagemresultado_datadmn3 ,
                                              AV136WWContagemResultadoContagensDS_29_Contagemresultado_datadmn_to3 ,
                                              AV138WWContagemResultadoContagensDS_31_Contagemresultado_statuscnt3 ,
                                              AV139WWContagemResultadoContagensDS_32_Contagemresultado_baseline3 ,
                                              AV141WWContagemResultadoContagensDS_34_Tfcontratada_areatrabalhodes_sel ,
                                              AV140WWContagemResultadoContagensDS_33_Tfcontratada_areatrabalhodes ,
                                              AV142WWContagemResultadoContagensDS_35_Tfcontagemresultado_datadmn ,
                                              AV143WWContagemResultadoContagensDS_36_Tfcontagemresultado_datadmn_to ,
                                              AV144WWContagemResultadoContagensDS_37_Tfcontagemresultado_datacnt ,
                                              AV145WWContagemResultadoContagensDS_38_Tfcontagemresultado_datacnt_to ,
                                              AV147WWContagemResultadoContagensDS_40_Tfcontagemresultado_horacnt_sel ,
                                              AV146WWContagemResultadoContagensDS_39_Tfcontagemresultado_horacnt ,
                                              AV149WWContagemResultadoContagensDS_42_Tfcontagemresultado_osfsosfm_sel ,
                                              AV148WWContagemResultadoContagensDS_41_Tfcontagemresultado_osfsosfm ,
                                              AV151WWContagemResultadoContagensDS_44_Tfcontagemrresultado_sistemasigla_sel ,
                                              AV150WWContagemResultadoContagensDS_43_Tfcontagemrresultado_sistemasigla ,
                                              AV153WWContagemResultadoContagensDS_46_Tfcontagemresultado_contadorfmnom_sel ,
                                              AV152WWContagemResultadoContagensDS_45_Tfcontagemresultado_contadorfmnom ,
                                              AV154WWContagemResultadoContagensDS_47_Tfcontagemresultado_statuscnt_sels.Count ,
                                              AV155WWContagemResultadoContagensDS_48_Tfcontagemresultadocontagens_esforco ,
                                              AV156WWContagemResultadoContagensDS_49_Tfcontagemresultadocontagens_esforco_to ,
                                              AV157WWContagemResultadoContagensDS_50_Tfcontagemresultado_pfbfs ,
                                              AV158WWContagemResultadoContagensDS_51_Tfcontagemresultado_pfbfs_to ,
                                              AV159WWContagemResultadoContagensDS_52_Tfcontagemresultado_pflfs ,
                                              AV160WWContagemResultadoContagensDS_53_Tfcontagemresultado_pflfs_to ,
                                              AV161WWContagemResultadoContagensDS_54_Tfcontagemresultado_pfbfm ,
                                              AV162WWContagemResultadoContagensDS_55_Tfcontagemresultado_pfbfm_to ,
                                              AV163WWContagemResultadoContagensDS_56_Tfcontagemresultado_pflfm ,
                                              AV164WWContagemResultadoContagensDS_57_Tfcontagemresultado_pflfm_to ,
                                              AV9WWPContext.gxTpr_Userehadministradorgam ,
                                              AV9WWPContext.gxTpr_Userehfinanceiro ,
                                              AV55Flag ,
                                              AV45GridState.gxTpr_Dynamicfilters.Count ,
                                              A457ContagemResultado_Demanda ,
                                              A493ContagemResultado_DemandaFM ,
                                              A473ContagemResultado_DataCnt ,
                                              A471ContagemResultado_DataDmn ,
                                              A598ContagemResultado_Baseline ,
                                              A53Contratada_AreaTrabalhoDes ,
                                              A511ContagemResultado_HoraCnt ,
                                              A509ContagemrResultado_SistemaSigla ,
                                              A474ContagemResultado_ContadorFMNom ,
                                              c482ContagemResultadoContagens_Esforco ,
                                              c458ContagemResultado_PFBFS ,
                                              c459ContagemResultado_PFLFS ,
                                              c460ContagemResultado_PFBFM ,
                                              c461ContagemResultado_PFLFM ,
                                              A470ContagemResultado_ContadorFMCod ,
                                              AV9WWPContext.gxTpr_Userid ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo ,
                                              Gx_date },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL,
                                              TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.DATE
                                              }
         });
         lV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1), "%", "");
         lV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1), "%", "");
         lV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1), "%", "");
         lV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1), "%", "");
         lV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2), "%", "");
         lV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2), "%", "");
         lV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2), "%", "");
         lV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2), "%", "");
         lV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3), "%", "");
         lV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3), "%", "");
         lV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3), "%", "");
         lV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3), "%", "");
         lV140WWContagemResultadoContagensDS_33_Tfcontratada_areatrabalhodes = StringUtil.Concat( StringUtil.RTrim( AV140WWContagemResultadoContagensDS_33_Tfcontratada_areatrabalhodes), "%", "");
         lV146WWContagemResultadoContagensDS_39_Tfcontagemresultado_horacnt = StringUtil.PadR( StringUtil.RTrim( AV146WWContagemResultadoContagensDS_39_Tfcontagemresultado_horacnt), 5, "%");
         lV148WWContagemResultadoContagensDS_41_Tfcontagemresultado_osfsosfm = StringUtil.Concat( StringUtil.RTrim( AV148WWContagemResultadoContagensDS_41_Tfcontagemresultado_osfsosfm), "%", "");
         lV150WWContagemResultadoContagensDS_43_Tfcontagemrresultado_sistemasigla = StringUtil.PadR( StringUtil.RTrim( AV150WWContagemResultadoContagensDS_43_Tfcontagemrresultado_sistemasigla), 25, "%");
         lV152WWContagemResultadoContagensDS_45_Tfcontagemresultado_contadorfmnom = StringUtil.PadR( StringUtil.RTrim( AV152WWContagemResultadoContagensDS_45_Tfcontagemresultado_contadorfmnom), 100, "%");
         /* Using cursor P003X5 */
         pr_default.execute(1, new Object[] {AV108WWContagemResultadoContagensDS_1_Dynamicfiltersselector1, AV115WWContagemResultadoContagensDS_8_Contagemresultado_contadorfm1, AV115WWContagemResultadoContagensDS_8_Contagemresultado_contadorfm1, AV115WWContagemResultadoContagensDS_8_Contagemresultado_contadorfm1, AV118WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2, AV119WWContagemResultadoContagensDS_12_Dynamicfiltersselector2, AV126WWContagemResultadoContagensDS_19_Contagemresultado_contadorfm2, AV126WWContagemResultadoContagensDS_19_Contagemresultado_contadorfm2, AV126WWContagemResultadoContagensDS_19_Contagemresultado_contadorfm2, AV129WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3, AV130WWContagemResultadoContagensDS_23_Dynamicfiltersselector3, AV137WWContagemResultadoContagensDS_30_Contagemresultado_contadorfm3, AV137WWContagemResultadoContagensDS_30_Contagemresultado_contadorfm3, AV137WWContagemResultadoContagensDS_30_Contagemresultado_contadorfm3, AV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1, AV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1, lV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1, lV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1, lV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1, lV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1, AV111WWContagemResultadoContagensDS_4_Contagemresultado_datacnt1, AV112WWContagemResultadoContagensDS_5_Contagemresultado_datacnt_to1, AV113WWContagemResultadoContagensDS_6_Contagemresultado_datadmn1, AV114WWContagemResultadoContagensDS_7_Contagemresultado_datadmn_to1, AV116WWContagemResultadoContagensDS_9_Contagemresultado_statuscnt1, AV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2, AV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2, lV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2, lV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2, lV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2, lV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2, AV122WWContagemResultadoContagensDS_15_Contagemresultado_datacnt2, AV123WWContagemResultadoContagensDS_16_Contagemresultado_datacnt_to2, AV124WWContagemResultadoContagensDS_17_Contagemresultado_datadmn2, AV125WWContagemResultadoContagensDS_18_Contagemresultado_datadmn_to2, AV127WWContagemResultadoContagensDS_20_Contagemresultado_statuscnt2, AV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3, AV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3, lV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3, lV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3, lV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3, lV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3, AV133WWContagemResultadoContagensDS_26_Contagemresultado_datacnt3, AV134WWContagemResultadoContagensDS_27_Contagemresultado_datacnt_to3, AV135WWContagemResultadoContagensDS_28_Contagemresultado_datadmn3, AV136WWContagemResultadoContagensDS_29_Contagemresultado_datadmn_to3, AV138WWContagemResultadoContagensDS_31_Contagemresultado_statuscnt3, lV140WWContagemResultadoContagensDS_33_Tfcontratada_areatrabalhodes, AV141WWContagemResultadoContagensDS_34_Tfcontratada_areatrabalhodes_sel, AV142WWContagemResultadoContagensDS_35_Tfcontagemresultado_datadmn, AV143WWContagemResultadoContagensDS_36_Tfcontagemresultado_datadmn_to, AV144WWContagemResultadoContagensDS_37_Tfcontagemresultado_datacnt, AV145WWContagemResultadoContagensDS_38_Tfcontagemresultado_datacnt_to, lV146WWContagemResultadoContagensDS_39_Tfcontagemresultado_horacnt, AV147WWContagemResultadoContagensDS_40_Tfcontagemresultado_horacnt_sel, lV148WWContagemResultadoContagensDS_41_Tfcontagemresultado_osfsosfm, AV149WWContagemResultadoContagensDS_42_Tfcontagemresultado_osfsosfm_sel, lV150WWContagemResultadoContagensDS_43_Tfcontagemrresultado_sistemasigla, AV151WWContagemResultadoContagensDS_44_Tfcontagemrresultado_sistemasigla_sel, lV152WWContagemResultadoContagensDS_45_Tfcontagemresultado_contadorfmnom, AV153WWContagemResultadoContagensDS_46_Tfcontagemresultado_contadorfmnom_sel, AV155WWContagemResultadoContagensDS_48_Tfcontagemresultadocontagens_esforco, AV156WWContagemResultadoContagensDS_49_Tfcontagemresultadocontagens_esforco_to, AV157WWContagemResultadoContagensDS_50_Tfcontagemresultado_pfbfs, AV158WWContagemResultadoContagensDS_51_Tfcontagemresultado_pfbfs_to, AV159WWContagemResultadoContagensDS_52_Tfcontagemresultado_pflfs, AV160WWContagemResultadoContagensDS_53_Tfcontagemresultado_pflfs_to, AV161WWContagemResultadoContagensDS_54_Tfcontagemresultado_pfbfm, AV162WWContagemResultadoContagensDS_55_Tfcontagemresultado_pfbfm_to, AV163WWContagemResultadoContagensDS_56_Tfcontagemresultado_pflfm, AV164WWContagemResultadoContagensDS_57_Tfcontagemresultado_pflfm_to, AV9WWPContext.gxTpr_Userid, AV9WWPContext.gxTpr_Areatrabalho_codigo, Gx_date});
         c482ContagemResultadoContagens_Esforco = P003X5_A482ContagemResultadoContagens_Esforco[0];
         c458ContagemResultado_PFBFS = P003X5_A458ContagemResultado_PFBFS[0];
         n458ContagemResultado_PFBFS = P003X5_n458ContagemResultado_PFBFS[0];
         c459ContagemResultado_PFLFS = P003X5_A459ContagemResultado_PFLFS[0];
         n459ContagemResultado_PFLFS = P003X5_n459ContagemResultado_PFLFS[0];
         c460ContagemResultado_PFBFM = P003X5_A460ContagemResultado_PFBFM[0];
         n460ContagemResultado_PFBFM = P003X5_n460ContagemResultado_PFBFM[0];
         c461ContagemResultado_PFLFM = P003X5_A461ContagemResultado_PFLFM[0];
         n461ContagemResultado_PFLFM = P003X5_n461ContagemResultado_PFLFM[0];
         pr_default.close(1);
         AV49ContagemResultadoContagens_Esforco = (short)(AV49ContagemResultadoContagens_Esforco+c482ContagemResultadoContagens_Esforco);
         AV50ContagemResultado_PFBFS = (decimal)(AV50ContagemResultado_PFBFS+c458ContagemResultado_PFBFS);
         AV51ContagemResultado_PFLFS = (decimal)(AV51ContagemResultado_PFLFS+c459ContagemResultado_PFLFS);
         AV52ContagemResultado_PFBFM = (decimal)(AV52ContagemResultado_PFBFM+c460ContagemResultado_PFBFM);
         AV53ContagemResultado_PFLFM = (decimal)(AV53ContagemResultado_PFLFM+c461ContagemResultado_PFLFM);
         /* End optimized group. */
         AV47Qtde = (short)(AV13CellRow-AV56PrimeiraLinha);
         AV48String = "Qtde: " + StringUtil.Trim( StringUtil.Str( (decimal)(AV47Qtde), 4, 0));
         AV10ExcelDocument.get_Cells(AV13CellRow+1, 8, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow+1, 9, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow+1, 10, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow+1, 11, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow+1, 12, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow+1, 13, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow+1, 8, 1, 1).Text = AV48String;
         AV10ExcelDocument.get_Cells(AV13CellRow+1, 9, 1, 1).Number = AV49ContagemResultadoContagens_Esforco;
         AV10ExcelDocument.get_Cells(AV13CellRow+1, 10, 1, 1).Number = (double)(AV50ContagemResultado_PFBFS);
         AV10ExcelDocument.get_Cells(AV13CellRow+1, 11, 1, 1).Number = (double)(AV51ContagemResultado_PFLFS);
         AV10ExcelDocument.get_Cells(AV13CellRow+1, 12, 1, 1).Number = (double)(AV52ContagemResultado_PFBFM);
         AV10ExcelDocument.get_Cells(AV13CellRow+1, 13, 1, 1).Number = (double)(AV53ContagemResultado_PFLFM);
      }

      protected void S191( )
      {
         /* 'CLOSEDOCUMENT' Routine */
         AV10ExcelDocument.Save();
         /* Execute user subroutine: 'CHECKSTATUS' */
         S121 ();
         if (returnInSub) return;
         AV10ExcelDocument.Close();
      }

      protected void S121( )
      {
         /* 'CHECKSTATUS' Routine */
         if ( AV10ExcelDocument.ErrCode != 0 )
         {
            AV11Filename = "";
            AV12ErrorMessage = AV10ExcelDocument.ErrDescription;
            AV10ExcelDocument.Close();
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S172( )
      {
         /* 'BEFOREWRITELINE' Routine */
      }

      protected void S182( )
      {
         /* 'AFTERWRITELINE' Routine */
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV54Websession = context.GetSession();
         AV66Contratadas = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV67Usuario_EhGestor = false;
         AV10ExcelDocument = new ExcelDocumentI();
         AV45GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV46GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV18DynamicFiltersSelector1 = "";
         AV19ContagemResultado_OsFsOsFm1 = "";
         AV20ContagemResultado_DataCnt1 = DateTime.MinValue;
         AV21ContagemResultado_DataCnt_To1 = DateTime.MinValue;
         AV22ContagemResultado_DataDmn1 = DateTime.MinValue;
         AV23ContagemResultado_DataDmn_To1 = DateTime.MinValue;
         AV57ContagemResultado_Baseline1 = "";
         AV27DynamicFiltersSelector2 = "";
         AV28ContagemResultado_OsFsOsFm2 = "";
         AV29ContagemResultado_DataCnt2 = DateTime.MinValue;
         AV30ContagemResultado_DataCnt_To2 = DateTime.MinValue;
         AV31ContagemResultado_DataDmn2 = DateTime.MinValue;
         AV32ContagemResultado_DataDmn_To2 = DateTime.MinValue;
         AV58ContagemResultado_Baseline2 = "";
         AV36DynamicFiltersSelector3 = "";
         AV37ContagemResultado_OsFsOsFm3 = "";
         AV38ContagemResultado_DataCnt3 = DateTime.MinValue;
         AV39ContagemResultado_DataCnt_To3 = DateTime.MinValue;
         AV40ContagemResultado_DataDmn3 = DateTime.MinValue;
         AV41ContagemResultado_DataDmn_To3 = DateTime.MinValue;
         AV59ContagemResultado_Baseline3 = "";
         AV91TFContagemResultado_StatusCnt_Sels = new GxSimpleCollection();
         AV108WWContagemResultadoContagensDS_1_Dynamicfiltersselector1 = "";
         AV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 = "";
         AV111WWContagemResultadoContagensDS_4_Contagemresultado_datacnt1 = DateTime.MinValue;
         AV112WWContagemResultadoContagensDS_5_Contagemresultado_datacnt_to1 = DateTime.MinValue;
         AV113WWContagemResultadoContagensDS_6_Contagemresultado_datadmn1 = DateTime.MinValue;
         AV114WWContagemResultadoContagensDS_7_Contagemresultado_datadmn_to1 = DateTime.MinValue;
         AV117WWContagemResultadoContagensDS_10_Contagemresultado_baseline1 = "";
         AV119WWContagemResultadoContagensDS_12_Dynamicfiltersselector2 = "";
         AV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 = "";
         AV122WWContagemResultadoContagensDS_15_Contagemresultado_datacnt2 = DateTime.MinValue;
         AV123WWContagemResultadoContagensDS_16_Contagemresultado_datacnt_to2 = DateTime.MinValue;
         AV124WWContagemResultadoContagensDS_17_Contagemresultado_datadmn2 = DateTime.MinValue;
         AV125WWContagemResultadoContagensDS_18_Contagemresultado_datadmn_to2 = DateTime.MinValue;
         AV128WWContagemResultadoContagensDS_21_Contagemresultado_baseline2 = "";
         AV130WWContagemResultadoContagensDS_23_Dynamicfiltersselector3 = "";
         AV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 = "";
         AV133WWContagemResultadoContagensDS_26_Contagemresultado_datacnt3 = DateTime.MinValue;
         AV134WWContagemResultadoContagensDS_27_Contagemresultado_datacnt_to3 = DateTime.MinValue;
         AV135WWContagemResultadoContagensDS_28_Contagemresultado_datadmn3 = DateTime.MinValue;
         AV136WWContagemResultadoContagensDS_29_Contagemresultado_datadmn_to3 = DateTime.MinValue;
         AV139WWContagemResultadoContagensDS_32_Contagemresultado_baseline3 = "";
         AV140WWContagemResultadoContagensDS_33_Tfcontratada_areatrabalhodes = "";
         AV141WWContagemResultadoContagensDS_34_Tfcontratada_areatrabalhodes_sel = "";
         AV142WWContagemResultadoContagensDS_35_Tfcontagemresultado_datadmn = DateTime.MinValue;
         AV143WWContagemResultadoContagensDS_36_Tfcontagemresultado_datadmn_to = DateTime.MinValue;
         AV144WWContagemResultadoContagensDS_37_Tfcontagemresultado_datacnt = DateTime.MinValue;
         AV145WWContagemResultadoContagensDS_38_Tfcontagemresultado_datacnt_to = DateTime.MinValue;
         AV146WWContagemResultadoContagensDS_39_Tfcontagemresultado_horacnt = "";
         AV147WWContagemResultadoContagensDS_40_Tfcontagemresultado_horacnt_sel = "";
         AV148WWContagemResultadoContagensDS_41_Tfcontagemresultado_osfsosfm = "";
         AV149WWContagemResultadoContagensDS_42_Tfcontagemresultado_osfsosfm_sel = "";
         AV150WWContagemResultadoContagensDS_43_Tfcontagemrresultado_sistemasigla = "";
         AV151WWContagemResultadoContagensDS_44_Tfcontagemrresultado_sistemasigla_sel = "";
         AV152WWContagemResultadoContagensDS_45_Tfcontagemresultado_contadorfmnom = "";
         AV153WWContagemResultadoContagensDS_46_Tfcontagemresultado_contadorfmnom_sel = "";
         AV154WWContagemResultadoContagensDS_47_Tfcontagemresultado_statuscnt_sels = new GxSimpleCollection();
         scmdbuf = "";
         lV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 = "";
         lV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 = "";
         lV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 = "";
         lV140WWContagemResultadoContagensDS_33_Tfcontratada_areatrabalhodes = "";
         lV146WWContagemResultadoContagensDS_39_Tfcontagemresultado_horacnt = "";
         lV148WWContagemResultadoContagensDS_41_Tfcontagemresultado_osfsosfm = "";
         lV150WWContagemResultadoContagensDS_43_Tfcontagemrresultado_sistemasigla = "";
         lV152WWContagemResultadoContagensDS_45_Tfcontagemresultado_contadorfmnom = "";
         A457ContagemResultado_Demanda = "";
         A493ContagemResultado_DemandaFM = "";
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A53Contratada_AreaTrabalhoDes = "";
         A511ContagemResultado_HoraCnt = "";
         A509ContagemrResultado_SistemaSigla = "";
         A474ContagemResultado_ContadorFMNom = "";
         Gx_date = DateTime.MinValue;
         P003X3_A489ContagemResultado_SistemaCod = new int[1] ;
         P003X3_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P003X3_A479ContagemResultado_CrFMPessoaCod = new int[1] ;
         P003X3_n479ContagemResultado_CrFMPessoaCod = new bool[] {false} ;
         P003X3_A490ContagemResultado_ContratadaCod = new int[1] ;
         P003X3_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P003X3_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P003X3_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P003X3_A470ContagemResultado_ContadorFMCod = new int[1] ;
         P003X3_A461ContagemResultado_PFLFM = new decimal[1] ;
         P003X3_n461ContagemResultado_PFLFM = new bool[] {false} ;
         P003X3_A460ContagemResultado_PFBFM = new decimal[1] ;
         P003X3_n460ContagemResultado_PFBFM = new bool[] {false} ;
         P003X3_A459ContagemResultado_PFLFS = new decimal[1] ;
         P003X3_n459ContagemResultado_PFLFS = new bool[] {false} ;
         P003X3_A458ContagemResultado_PFBFS = new decimal[1] ;
         P003X3_n458ContagemResultado_PFBFS = new bool[] {false} ;
         P003X3_A482ContagemResultadoContagens_Esforco = new short[1] ;
         P003X3_A474ContagemResultado_ContadorFMNom = new String[] {""} ;
         P003X3_n474ContagemResultado_ContadorFMNom = new bool[] {false} ;
         P003X3_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P003X3_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P003X3_A511ContagemResultado_HoraCnt = new String[] {""} ;
         P003X3_A53Contratada_AreaTrabalhoDes = new String[] {""} ;
         P003X3_n53Contratada_AreaTrabalhoDes = new bool[] {false} ;
         P003X3_A598ContagemResultado_Baseline = new bool[] {false} ;
         P003X3_n598ContagemResultado_Baseline = new bool[] {false} ;
         P003X3_A483ContagemResultado_StatusCnt = new short[1] ;
         P003X3_A508ContagemResultado_Owner = new int[1] ;
         P003X3_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P003X3_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P003X3_A456ContagemResultado_Codigo = new int[1] ;
         P003X3_A584ContagemResultado_ContadorFM = new int[1] ;
         P003X3_n584ContagemResultado_ContadorFM = new bool[] {false} ;
         P003X3_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P003X3_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P003X3_A457ContagemResultado_Demanda = new String[] {""} ;
         P003X3_n457ContagemResultado_Demanda = new bool[] {false} ;
         A501ContagemResultado_OsFsOsFm = "";
         GXt_dtime1 = (DateTime)(DateTime.MinValue);
         P003X5_A482ContagemResultadoContagens_Esforco = new short[1] ;
         P003X5_A458ContagemResultado_PFBFS = new decimal[1] ;
         P003X5_n458ContagemResultado_PFBFS = new bool[] {false} ;
         P003X5_A459ContagemResultado_PFLFS = new decimal[1] ;
         P003X5_n459ContagemResultado_PFLFS = new bool[] {false} ;
         P003X5_A460ContagemResultado_PFBFM = new decimal[1] ;
         P003X5_n460ContagemResultado_PFBFM = new bool[] {false} ;
         P003X5_A461ContagemResultado_PFLFM = new decimal[1] ;
         P003X5_n461ContagemResultado_PFLFM = new bool[] {false} ;
         AV49ContagemResultadoContagens_Esforco = 0;
         AV48String = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.exportwwcontagemresultadocontagens__default(),
            new Object[][] {
                new Object[] {
               P003X3_A489ContagemResultado_SistemaCod, P003X3_n489ContagemResultado_SistemaCod, P003X3_A479ContagemResultado_CrFMPessoaCod, P003X3_n479ContagemResultado_CrFMPessoaCod, P003X3_A490ContagemResultado_ContratadaCod, P003X3_n490ContagemResultado_ContratadaCod, P003X3_A52Contratada_AreaTrabalhoCod, P003X3_n52Contratada_AreaTrabalhoCod, P003X3_A470ContagemResultado_ContadorFMCod, P003X3_A461ContagemResultado_PFLFM,
               P003X3_n461ContagemResultado_PFLFM, P003X3_A460ContagemResultado_PFBFM, P003X3_n460ContagemResultado_PFBFM, P003X3_A459ContagemResultado_PFLFS, P003X3_n459ContagemResultado_PFLFS, P003X3_A458ContagemResultado_PFBFS, P003X3_n458ContagemResultado_PFBFS, P003X3_A482ContagemResultadoContagens_Esforco, P003X3_A474ContagemResultado_ContadorFMNom, P003X3_n474ContagemResultado_ContadorFMNom,
               P003X3_A509ContagemrResultado_SistemaSigla, P003X3_n509ContagemrResultado_SistemaSigla, P003X3_A511ContagemResultado_HoraCnt, P003X3_A53Contratada_AreaTrabalhoDes, P003X3_n53Contratada_AreaTrabalhoDes, P003X3_A598ContagemResultado_Baseline, P003X3_n598ContagemResultado_Baseline, P003X3_A483ContagemResultado_StatusCnt, P003X3_A508ContagemResultado_Owner, P003X3_A471ContagemResultado_DataDmn,
               P003X3_A473ContagemResultado_DataCnt, P003X3_A456ContagemResultado_Codigo, P003X3_A584ContagemResultado_ContadorFM, P003X3_n584ContagemResultado_ContadorFM, P003X3_A493ContagemResultado_DemandaFM, P003X3_n493ContagemResultado_DemandaFM, P003X3_A457ContagemResultado_Demanda, P003X3_n457ContagemResultado_Demanda
               }
               , new Object[] {
               P003X5_A482ContagemResultadoContagens_Esforco, P003X5_A458ContagemResultado_PFBFS, P003X5_n458ContagemResultado_PFBFS, P003X5_A459ContagemResultado_PFLFS, P003X5_n459ContagemResultado_PFLFS, P003X5_A460ContagemResultado_PFBFM, P003X5_n460ContagemResultado_PFBFM, P003X5_A461ContagemResultado_PFLFM, P003X5_n461ContagemResultado_PFLFM
               }
            }
         );
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
      }

      private short AV93TFContagemResultadoContagens_Esforco ;
      private short AV94TFContagemResultadoContagens_Esforco_To ;
      private short AV16OrderedBy ;
      private short AV55Flag ;
      private short AV71DynamicFiltersOperator1 ;
      private short AV25ContagemResultado_StatusCnt1 ;
      private short AV72DynamicFiltersOperator2 ;
      private short AV34ContagemResultado_StatusCnt2 ;
      private short AV73DynamicFiltersOperator3 ;
      private short AV43ContagemResultado_StatusCnt3 ;
      private short AV92TFContagemResultado_StatusCnt_Sel ;
      private short AV109WWContagemResultadoContagensDS_2_Dynamicfiltersoperator1 ;
      private short AV116WWContagemResultadoContagensDS_9_Contagemresultado_statuscnt1 ;
      private short AV120WWContagemResultadoContagensDS_13_Dynamicfiltersoperator2 ;
      private short AV127WWContagemResultadoContagensDS_20_Contagemresultado_statuscnt2 ;
      private short AV131WWContagemResultadoContagensDS_24_Dynamicfiltersoperator3 ;
      private short AV138WWContagemResultadoContagensDS_31_Contagemresultado_statuscnt3 ;
      private short AV155WWContagemResultadoContagensDS_48_Tfcontagemresultadocontagens_esforco ;
      private short AV156WWContagemResultadoContagensDS_49_Tfcontagemresultadocontagens_esforco_to ;
      private short AV9WWPContext_gxTpr_Userid ;
      private short A483ContagemResultado_StatusCnt ;
      private short A482ContagemResultadoContagens_Esforco ;
      private short c482ContagemResultadoContagens_Esforco ;
      private short AV49ContagemResultadoContagens_Esforco ;
      private short AV47Qtde ;
      private int AV13CellRow ;
      private int AV14FirstColumn ;
      private int AV15Random ;
      private int AV68ContagemResultado_ContadorFM1 ;
      private int AV69ContagemResultado_ContadorFM2 ;
      private int AV70ContagemResultado_ContadorFM3 ;
      private int AV106GXV1 ;
      private int AV56PrimeiraLinha ;
      private int AV115WWContagemResultadoContagensDS_8_Contagemresultado_contadorfm1 ;
      private int AV126WWContagemResultadoContagensDS_19_Contagemresultado_contadorfm2 ;
      private int AV137WWContagemResultadoContagensDS_30_Contagemresultado_contadorfm3 ;
      private int AV154WWContagemResultadoContagensDS_47_Tfcontagemresultado_statuscnt_sels_Count ;
      private int AV9WWPContext_gxTpr_Areatrabalho_codigo ;
      private int AV9WWPContext_gxTpr_Contratada_codigo ;
      private int AV45GridState_gxTpr_Dynamicfilters_Count ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A470ContagemResultado_ContadorFMCod ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A584ContagemResultado_ContadorFM ;
      private int A508ContagemResultado_Owner ;
      private int A489ContagemResultado_SistemaCod ;
      private int A479ContagemResultado_CrFMPessoaCod ;
      private int A456ContagemResultado_Codigo ;
      private long AV103i ;
      private decimal AV95TFContagemResultado_PFBFS ;
      private decimal AV96TFContagemResultado_PFBFS_To ;
      private decimal AV97TFContagemResultado_PFLFS ;
      private decimal AV98TFContagemResultado_PFLFS_To ;
      private decimal AV99TFContagemResultado_PFBFM ;
      private decimal AV100TFContagemResultado_PFBFM_To ;
      private decimal AV101TFContagemResultado_PFLFM ;
      private decimal AV102TFContagemResultado_PFLFM_To ;
      private decimal AV157WWContagemResultadoContagensDS_50_Tfcontagemresultado_pfbfs ;
      private decimal AV158WWContagemResultadoContagensDS_51_Tfcontagemresultado_pfbfs_to ;
      private decimal AV159WWContagemResultadoContagensDS_52_Tfcontagemresultado_pflfs ;
      private decimal AV160WWContagemResultadoContagensDS_53_Tfcontagemresultado_pflfs_to ;
      private decimal AV161WWContagemResultadoContagensDS_54_Tfcontagemresultado_pfbfm ;
      private decimal AV162WWContagemResultadoContagensDS_55_Tfcontagemresultado_pfbfm_to ;
      private decimal AV163WWContagemResultadoContagensDS_56_Tfcontagemresultado_pflfm ;
      private decimal AV164WWContagemResultadoContagensDS_57_Tfcontagemresultado_pflfm_to ;
      private decimal A458ContagemResultado_PFBFS ;
      private decimal A459ContagemResultado_PFLFS ;
      private decimal A460ContagemResultado_PFBFM ;
      private decimal A461ContagemResultado_PFLFM ;
      private decimal c458ContagemResultado_PFBFS ;
      private decimal c459ContagemResultado_PFLFS ;
      private decimal c460ContagemResultado_PFBFM ;
      private decimal c461ContagemResultado_PFLFM ;
      private decimal AV50ContagemResultado_PFBFS ;
      private decimal AV51ContagemResultado_PFLFS ;
      private decimal AV52ContagemResultado_PFBFM ;
      private decimal AV53ContagemResultado_PFLFM ;
      private String AV82TFContagemResultado_HoraCnt ;
      private String AV83TFContagemResultado_HoraCnt_Sel ;
      private String AV86TFContagemrResultado_SistemaSigla ;
      private String AV87TFContagemrResultado_SistemaSigla_Sel ;
      private String AV88TFContagemResultado_ContadorFMNom ;
      private String AV89TFContagemResultado_ContadorFMNom_Sel ;
      private String AV57ContagemResultado_Baseline1 ;
      private String AV58ContagemResultado_Baseline2 ;
      private String AV59ContagemResultado_Baseline3 ;
      private String AV117WWContagemResultadoContagensDS_10_Contagemresultado_baseline1 ;
      private String AV128WWContagemResultadoContagensDS_21_Contagemresultado_baseline2 ;
      private String AV139WWContagemResultadoContagensDS_32_Contagemresultado_baseline3 ;
      private String AV146WWContagemResultadoContagensDS_39_Tfcontagemresultado_horacnt ;
      private String AV147WWContagemResultadoContagensDS_40_Tfcontagemresultado_horacnt_sel ;
      private String AV150WWContagemResultadoContagensDS_43_Tfcontagemrresultado_sistemasigla ;
      private String AV151WWContagemResultadoContagensDS_44_Tfcontagemrresultado_sistemasigla_sel ;
      private String AV152WWContagemResultadoContagensDS_45_Tfcontagemresultado_contadorfmnom ;
      private String AV153WWContagemResultadoContagensDS_46_Tfcontagemresultado_contadorfmnom_sel ;
      private String scmdbuf ;
      private String lV146WWContagemResultadoContagensDS_39_Tfcontagemresultado_horacnt ;
      private String lV150WWContagemResultadoContagensDS_43_Tfcontagemrresultado_sistemasigla ;
      private String lV152WWContagemResultadoContagensDS_45_Tfcontagemresultado_contadorfmnom ;
      private String A511ContagemResultado_HoraCnt ;
      private String A509ContagemrResultado_SistemaSigla ;
      private String A474ContagemResultado_ContadorFMNom ;
      private String AV48String ;
      private DateTime GXt_dtime1 ;
      private DateTime AV78TFContagemResultado_DataDmn ;
      private DateTime AV79TFContagemResultado_DataDmn_To ;
      private DateTime AV80TFContagemResultado_DataCnt ;
      private DateTime AV81TFContagemResultado_DataCnt_To ;
      private DateTime AV20ContagemResultado_DataCnt1 ;
      private DateTime AV21ContagemResultado_DataCnt_To1 ;
      private DateTime AV22ContagemResultado_DataDmn1 ;
      private DateTime AV23ContagemResultado_DataDmn_To1 ;
      private DateTime AV29ContagemResultado_DataCnt2 ;
      private DateTime AV30ContagemResultado_DataCnt_To2 ;
      private DateTime AV31ContagemResultado_DataDmn2 ;
      private DateTime AV32ContagemResultado_DataDmn_To2 ;
      private DateTime AV38ContagemResultado_DataCnt3 ;
      private DateTime AV39ContagemResultado_DataCnt_To3 ;
      private DateTime AV40ContagemResultado_DataDmn3 ;
      private DateTime AV41ContagemResultado_DataDmn_To3 ;
      private DateTime AV111WWContagemResultadoContagensDS_4_Contagemresultado_datacnt1 ;
      private DateTime AV112WWContagemResultadoContagensDS_5_Contagemresultado_datacnt_to1 ;
      private DateTime AV113WWContagemResultadoContagensDS_6_Contagemresultado_datadmn1 ;
      private DateTime AV114WWContagemResultadoContagensDS_7_Contagemresultado_datadmn_to1 ;
      private DateTime AV122WWContagemResultadoContagensDS_15_Contagemresultado_datacnt2 ;
      private DateTime AV123WWContagemResultadoContagensDS_16_Contagemresultado_datacnt_to2 ;
      private DateTime AV124WWContagemResultadoContagensDS_17_Contagemresultado_datadmn2 ;
      private DateTime AV125WWContagemResultadoContagensDS_18_Contagemresultado_datadmn_to2 ;
      private DateTime AV133WWContagemResultadoContagensDS_26_Contagemresultado_datacnt3 ;
      private DateTime AV134WWContagemResultadoContagensDS_27_Contagemresultado_datacnt_to3 ;
      private DateTime AV135WWContagemResultadoContagensDS_28_Contagemresultado_datadmn3 ;
      private DateTime AV136WWContagemResultadoContagensDS_29_Contagemresultado_datadmn_to3 ;
      private DateTime AV142WWContagemResultadoContagensDS_35_Tfcontagemresultado_datadmn ;
      private DateTime AV143WWContagemResultadoContagensDS_36_Tfcontagemresultado_datadmn_to ;
      private DateTime AV144WWContagemResultadoContagensDS_37_Tfcontagemresultado_datacnt ;
      private DateTime AV145WWContagemResultadoContagensDS_38_Tfcontagemresultado_datacnt_to ;
      private DateTime A473ContagemResultado_DataCnt ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime Gx_date ;
      private bool AV17OrderedDsc ;
      private bool returnInSub ;
      private bool AV67Usuario_EhGestor ;
      private bool AV26DynamicFiltersEnabled2 ;
      private bool AV35DynamicFiltersEnabled3 ;
      private bool AV118WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 ;
      private bool AV129WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 ;
      private bool AV9WWPContext_gxTpr_Userehfinanceiro ;
      private bool AV9WWPContext_gxTpr_Userehcontratante ;
      private bool A598ContagemResultado_Baseline ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n479ContagemResultado_CrFMPessoaCod ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n461ContagemResultado_PFLFM ;
      private bool n460ContagemResultado_PFBFM ;
      private bool n459ContagemResultado_PFLFS ;
      private bool n458ContagemResultado_PFBFS ;
      private bool n474ContagemResultado_ContadorFMNom ;
      private bool n509ContagemrResultado_SistemaSigla ;
      private bool n53Contratada_AreaTrabalhoDes ;
      private bool n598ContagemResultado_Baseline ;
      private bool n584ContagemResultado_ContadorFM ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n457ContagemResultado_Demanda ;
      private bool AV9WWPContext_gxTpr_Userehadministradorgam ;
      private String AV90TFContagemResultado_StatusCnt_SelsJson ;
      private String AV44GridStateXML ;
      private String AV76TFContratada_AreaTrabalhoDes ;
      private String AV77TFContratada_AreaTrabalhoDes_Sel ;
      private String AV84TFContagemResultado_OsFsOsFm ;
      private String AV85TFContagemResultado_OsFsOsFm_Sel ;
      private String AV12ErrorMessage ;
      private String AV11Filename ;
      private String AV18DynamicFiltersSelector1 ;
      private String AV19ContagemResultado_OsFsOsFm1 ;
      private String AV27DynamicFiltersSelector2 ;
      private String AV28ContagemResultado_OsFsOsFm2 ;
      private String AV36DynamicFiltersSelector3 ;
      private String AV37ContagemResultado_OsFsOsFm3 ;
      private String AV108WWContagemResultadoContagensDS_1_Dynamicfiltersselector1 ;
      private String AV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 ;
      private String AV119WWContagemResultadoContagensDS_12_Dynamicfiltersselector2 ;
      private String AV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 ;
      private String AV130WWContagemResultadoContagensDS_23_Dynamicfiltersselector3 ;
      private String AV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 ;
      private String AV140WWContagemResultadoContagensDS_33_Tfcontratada_areatrabalhodes ;
      private String AV141WWContagemResultadoContagensDS_34_Tfcontratada_areatrabalhodes_sel ;
      private String AV148WWContagemResultadoContagensDS_41_Tfcontagemresultado_osfsosfm ;
      private String AV149WWContagemResultadoContagensDS_42_Tfcontagemresultado_osfsosfm_sel ;
      private String lV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 ;
      private String lV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 ;
      private String lV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 ;
      private String lV140WWContagemResultadoContagensDS_33_Tfcontratada_areatrabalhodes ;
      private String lV148WWContagemResultadoContagensDS_41_Tfcontagemresultado_osfsosfm ;
      private String A457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String A53Contratada_AreaTrabalhoDes ;
      private String A501ContagemResultado_OsFsOsFm ;
      private IGxSession AV54Websession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P003X3_A489ContagemResultado_SistemaCod ;
      private bool[] P003X3_n489ContagemResultado_SistemaCod ;
      private int[] P003X3_A479ContagemResultado_CrFMPessoaCod ;
      private bool[] P003X3_n479ContagemResultado_CrFMPessoaCod ;
      private int[] P003X3_A490ContagemResultado_ContratadaCod ;
      private bool[] P003X3_n490ContagemResultado_ContratadaCod ;
      private int[] P003X3_A52Contratada_AreaTrabalhoCod ;
      private bool[] P003X3_n52Contratada_AreaTrabalhoCod ;
      private int[] P003X3_A470ContagemResultado_ContadorFMCod ;
      private decimal[] P003X3_A461ContagemResultado_PFLFM ;
      private bool[] P003X3_n461ContagemResultado_PFLFM ;
      private decimal[] P003X3_A460ContagemResultado_PFBFM ;
      private bool[] P003X3_n460ContagemResultado_PFBFM ;
      private decimal[] P003X3_A459ContagemResultado_PFLFS ;
      private bool[] P003X3_n459ContagemResultado_PFLFS ;
      private decimal[] P003X3_A458ContagemResultado_PFBFS ;
      private bool[] P003X3_n458ContagemResultado_PFBFS ;
      private short[] P003X3_A482ContagemResultadoContagens_Esforco ;
      private String[] P003X3_A474ContagemResultado_ContadorFMNom ;
      private bool[] P003X3_n474ContagemResultado_ContadorFMNom ;
      private String[] P003X3_A509ContagemrResultado_SistemaSigla ;
      private bool[] P003X3_n509ContagemrResultado_SistemaSigla ;
      private String[] P003X3_A511ContagemResultado_HoraCnt ;
      private String[] P003X3_A53Contratada_AreaTrabalhoDes ;
      private bool[] P003X3_n53Contratada_AreaTrabalhoDes ;
      private bool[] P003X3_A598ContagemResultado_Baseline ;
      private bool[] P003X3_n598ContagemResultado_Baseline ;
      private short[] P003X3_A483ContagemResultado_StatusCnt ;
      private int[] P003X3_A508ContagemResultado_Owner ;
      private DateTime[] P003X3_A471ContagemResultado_DataDmn ;
      private DateTime[] P003X3_A473ContagemResultado_DataCnt ;
      private int[] P003X3_A456ContagemResultado_Codigo ;
      private int[] P003X3_A584ContagemResultado_ContadorFM ;
      private bool[] P003X3_n584ContagemResultado_ContadorFM ;
      private String[] P003X3_A493ContagemResultado_DemandaFM ;
      private bool[] P003X3_n493ContagemResultado_DemandaFM ;
      private String[] P003X3_A457ContagemResultado_Demanda ;
      private bool[] P003X3_n457ContagemResultado_Demanda ;
      private short[] P003X5_A482ContagemResultadoContagens_Esforco ;
      private decimal[] P003X5_A458ContagemResultado_PFBFS ;
      private bool[] P003X5_n458ContagemResultado_PFBFS ;
      private decimal[] P003X5_A459ContagemResultado_PFLFS ;
      private bool[] P003X5_n459ContagemResultado_PFLFS ;
      private decimal[] P003X5_A460ContagemResultado_PFBFM ;
      private bool[] P003X5_n460ContagemResultado_PFBFM ;
      private decimal[] P003X5_A461ContagemResultado_PFLFM ;
      private bool[] P003X5_n461ContagemResultado_PFLFM ;
      private String aP28_Filename ;
      private String aP29_ErrorMessage ;
      private ExcelDocumentI AV10ExcelDocument ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV66Contratadas ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV91TFContagemResultado_StatusCnt_Sels ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV154WWContagemResultadoContagensDS_47_Tfcontagemresultado_statuscnt_sels ;
      private wwpbaseobjects.SdtWWPGridState AV45GridState ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV46GridStateDynamicFilter ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
   }

   public class exportwwcontagemresultadocontagens__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P003X3( IGxContext context ,
                                             short A483ContagemResultado_StatusCnt ,
                                             IGxCollection AV154WWContagemResultadoContagensDS_47_Tfcontagemresultado_statuscnt_sels ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             IGxCollection AV66Contratadas ,
                                             String AV108WWContagemResultadoContagensDS_1_Dynamicfiltersselector1 ,
                                             short AV109WWContagemResultadoContagensDS_2_Dynamicfiltersoperator1 ,
                                             String AV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 ,
                                             DateTime AV111WWContagemResultadoContagensDS_4_Contagemresultado_datacnt1 ,
                                             DateTime AV112WWContagemResultadoContagensDS_5_Contagemresultado_datacnt_to1 ,
                                             DateTime AV113WWContagemResultadoContagensDS_6_Contagemresultado_datadmn1 ,
                                             DateTime AV114WWContagemResultadoContagensDS_7_Contagemresultado_datadmn_to1 ,
                                             short AV116WWContagemResultadoContagensDS_9_Contagemresultado_statuscnt1 ,
                                             String AV117WWContagemResultadoContagensDS_10_Contagemresultado_baseline1 ,
                                             bool AV118WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 ,
                                             String AV119WWContagemResultadoContagensDS_12_Dynamicfiltersselector2 ,
                                             short AV120WWContagemResultadoContagensDS_13_Dynamicfiltersoperator2 ,
                                             String AV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 ,
                                             DateTime AV122WWContagemResultadoContagensDS_15_Contagemresultado_datacnt2 ,
                                             DateTime AV123WWContagemResultadoContagensDS_16_Contagemresultado_datacnt_to2 ,
                                             DateTime AV124WWContagemResultadoContagensDS_17_Contagemresultado_datadmn2 ,
                                             DateTime AV125WWContagemResultadoContagensDS_18_Contagemresultado_datadmn_to2 ,
                                             short AV127WWContagemResultadoContagensDS_20_Contagemresultado_statuscnt2 ,
                                             String AV128WWContagemResultadoContagensDS_21_Contagemresultado_baseline2 ,
                                             bool AV129WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 ,
                                             String AV130WWContagemResultadoContagensDS_23_Dynamicfiltersselector3 ,
                                             short AV131WWContagemResultadoContagensDS_24_Dynamicfiltersoperator3 ,
                                             String AV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 ,
                                             DateTime AV133WWContagemResultadoContagensDS_26_Contagemresultado_datacnt3 ,
                                             DateTime AV134WWContagemResultadoContagensDS_27_Contagemresultado_datacnt_to3 ,
                                             DateTime AV135WWContagemResultadoContagensDS_28_Contagemresultado_datadmn3 ,
                                             DateTime AV136WWContagemResultadoContagensDS_29_Contagemresultado_datadmn_to3 ,
                                             short AV138WWContagemResultadoContagensDS_31_Contagemresultado_statuscnt3 ,
                                             String AV139WWContagemResultadoContagensDS_32_Contagemresultado_baseline3 ,
                                             String AV141WWContagemResultadoContagensDS_34_Tfcontratada_areatrabalhodes_sel ,
                                             String AV140WWContagemResultadoContagensDS_33_Tfcontratada_areatrabalhodes ,
                                             DateTime AV142WWContagemResultadoContagensDS_35_Tfcontagemresultado_datadmn ,
                                             DateTime AV143WWContagemResultadoContagensDS_36_Tfcontagemresultado_datadmn_to ,
                                             DateTime AV144WWContagemResultadoContagensDS_37_Tfcontagemresultado_datacnt ,
                                             DateTime AV145WWContagemResultadoContagensDS_38_Tfcontagemresultado_datacnt_to ,
                                             String AV147WWContagemResultadoContagensDS_40_Tfcontagemresultado_horacnt_sel ,
                                             String AV146WWContagemResultadoContagensDS_39_Tfcontagemresultado_horacnt ,
                                             String AV149WWContagemResultadoContagensDS_42_Tfcontagemresultado_osfsosfm_sel ,
                                             String AV148WWContagemResultadoContagensDS_41_Tfcontagemresultado_osfsosfm ,
                                             String AV151WWContagemResultadoContagensDS_44_Tfcontagemrresultado_sistemasigla_sel ,
                                             String AV150WWContagemResultadoContagensDS_43_Tfcontagemrresultado_sistemasigla ,
                                             String AV153WWContagemResultadoContagensDS_46_Tfcontagemresultado_contadorfmnom_sel ,
                                             String AV152WWContagemResultadoContagensDS_45_Tfcontagemresultado_contadorfmnom ,
                                             int AV154WWContagemResultadoContagensDS_47_Tfcontagemresultado_statuscnt_sels_Count ,
                                             short AV155WWContagemResultadoContagensDS_48_Tfcontagemresultadocontagens_esforco ,
                                             short AV156WWContagemResultadoContagensDS_49_Tfcontagemresultadocontagens_esforco_to ,
                                             decimal AV157WWContagemResultadoContagensDS_50_Tfcontagemresultado_pfbfs ,
                                             decimal AV158WWContagemResultadoContagensDS_51_Tfcontagemresultado_pfbfs_to ,
                                             decimal AV159WWContagemResultadoContagensDS_52_Tfcontagemresultado_pflfs ,
                                             decimal AV160WWContagemResultadoContagensDS_53_Tfcontagemresultado_pflfs_to ,
                                             decimal AV161WWContagemResultadoContagensDS_54_Tfcontagemresultado_pfbfm ,
                                             decimal AV162WWContagemResultadoContagensDS_55_Tfcontagemresultado_pfbfm_to ,
                                             decimal AV163WWContagemResultadoContagensDS_56_Tfcontagemresultado_pflfm ,
                                             decimal AV164WWContagemResultadoContagensDS_57_Tfcontagemresultado_pflfm_to ,
                                             bool AV67Usuario_EhGestor ,
                                             bool AV9WWPContext_gxTpr_Userehfinanceiro ,
                                             bool AV9WWPContext_gxTpr_Userehcontratante ,
                                             short AV55Flag ,
                                             int AV9WWPContext_gxTpr_Contratada_codigo ,
                                             int AV45GridState_gxTpr_Dynamicfilters_Count ,
                                             String A457ContagemResultado_Demanda ,
                                             String A493ContagemResultado_DemandaFM ,
                                             DateTime A473ContagemResultado_DataCnt ,
                                             DateTime A471ContagemResultado_DataDmn ,
                                             bool A598ContagemResultado_Baseline ,
                                             String A53Contratada_AreaTrabalhoDes ,
                                             String A511ContagemResultado_HoraCnt ,
                                             String A509ContagemrResultado_SistemaSigla ,
                                             String A474ContagemResultado_ContadorFMNom ,
                                             short A482ContagemResultadoContagens_Esforco ,
                                             decimal A458ContagemResultado_PFBFS ,
                                             decimal A459ContagemResultado_PFLFS ,
                                             decimal A460ContagemResultado_PFBFM ,
                                             decimal A461ContagemResultado_PFLFM ,
                                             int A470ContagemResultado_ContadorFMCod ,
                                             short AV9WWPContext_gxTpr_Userid ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo ,
                                             DateTime Gx_date ,
                                             short AV16OrderedBy ,
                                             bool AV17OrderedDsc ,
                                             int AV115WWContagemResultadoContagensDS_8_Contagemresultado_contadorfm1 ,
                                             int A584ContagemResultado_ContadorFM ,
                                             int A508ContagemResultado_Owner ,
                                             int AV126WWContagemResultadoContagensDS_19_Contagemresultado_contadorfm2 ,
                                             int AV137WWContagemResultadoContagensDS_30_Contagemresultado_contadorfm3 )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [74] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         scmdbuf = "SELECT T4.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T2.[Usuario_PessoaCod] AS ContagemResultado_CrFMPessoaCod, T4.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T6.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T1.[ContagemResultado_ContadorFMCod] AS ContagemResultado_ContadorFMCod, T1.[ContagemResultado_PFLFM], T1.[ContagemResultado_PFBFM], T1.[ContagemResultado_PFLFS], T1.[ContagemResultado_PFBFS], T1.[ContagemResultadoContagens_Esforco], T3.[Pessoa_Nome] AS ContagemResultado_ContadorFMNom, T5.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T1.[ContagemResultado_HoraCnt], T7.[AreaTrabalho_Descricao] AS Contratada_AreaTrabalhoDes, T4.[ContagemResultado_Baseline], T1.[ContagemResultado_StatusCnt], T4.[ContagemResultado_Owner], T4.[ContagemResultado_DataDmn], T1.[ContagemResultado_DataCnt], T1.[ContagemResultado_Codigo], COALESCE( T8.[ContagemResultado_ContadorFM], 0) AS ContagemResultado_ContadorFM, T4.[ContagemResultado_DemandaFM], T4.[ContagemResultado_Demanda] FROM ((((((([ContagemResultadoContagens] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContagemResultado_ContadorFMCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) INNER JOIN [ContagemResultado] T4 WITH (NOLOCK) ON T4.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Sistema] T5 WITH (NOLOCK) ON T5.[Sistema_Codigo] = T4.[ContagemResultado_SistemaCod]) LEFT JOIN [Contratada] T6 WITH (NOLOCK) ON T6.[Contratada_Codigo] = T4.[ContagemResultado_ContratadaCod]) LEFT JOIN [AreaTrabalho] T7 WITH (NOLOCK) ON T7.[AreaTrabalho_Codigo] = T6.[Contratada_AreaTrabalhoCod]) LEFT JOIN (SELECT MIN([ContagemResultado_ContadorFMCod]) AS ContagemResultado_ContadorFM, [ContagemResultado_Codigo]";
         scmdbuf = scmdbuf + " FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T8 ON T8.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo])";
         scmdbuf = scmdbuf + " WHERE (Not ( @AV108WWContagemResultadoContagensDS_1_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV115WWContagemResultadoContagensDS_8_Contagemresultado_contadorfm1 = convert(int, 0)))) or ( COALESCE( T8.[ContagemResultado_ContadorFM], 0) = @AV115WWContagemResultadoContagensDS_8_Contagemresultado_contadorfm1 or ( (COALESCE( T8.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T4.[ContagemResultado_Owner] = @AV115WWContagemResultadoContagensDS_8_Contagemresultado_contadorfm1)))";
         scmdbuf = scmdbuf + " and (Not ( @AV118WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 = 1 and @AV119WWContagemResultadoContagensDS_12_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV126WWContagemResultadoContagensDS_19_Contagemresultado_contadorfm2 = convert(int, 0)))) or ( COALESCE( T8.[ContagemResultado_ContadorFM], 0) = @AV126WWContagemResultadoContagensDS_19_Contagemresultado_contadorfm2 or ( (COALESCE( T8.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T4.[ContagemResultado_Owner] = @AV126WWContagemResultadoContagensDS_19_Contagemresultado_contadorfm2)))";
         scmdbuf = scmdbuf + " and (Not ( @AV129WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 = 1 and @AV130WWContagemResultadoContagensDS_23_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV137WWContagemResultadoContagensDS_30_Contagemresultado_contadorfm3 = convert(int, 0)))) or ( COALESCE( T8.[ContagemResultado_ContadorFM], 0) = @AV137WWContagemResultadoContagensDS_30_Contagemresultado_contadorfm3 or ( (COALESCE( T8.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T4.[ContagemResultado_Owner] = @AV137WWContagemResultadoContagensDS_30_Contagemresultado_contadorfm3)))";
         if ( ( StringUtil.StrCmp(AV108WWContagemResultadoContagensDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV109WWContagemResultadoContagensDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_Demanda] = @AV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 or T4.[ContagemResultado_DemandaFM] = @AV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int2[14] = 1;
            GXv_int2[15] = 1;
         }
         if ( ( StringUtil.StrCmp(AV108WWContagemResultadoContagensDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV109WWContagemResultadoContagensDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_Demanda] like @lV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 or T4.[ContagemResultado_DemandaFM] like @lV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int2[16] = 1;
            GXv_int2[17] = 1;
         }
         if ( ( StringUtil.StrCmp(AV108WWContagemResultadoContagensDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV109WWContagemResultadoContagensDS_2_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_Demanda] like '%' + @lV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 or T4.[ContagemResultado_DemandaFM] like '%' + @lV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int2[18] = 1;
            GXv_int2[19] = 1;
         }
         if ( ( StringUtil.StrCmp(AV108WWContagemResultadoContagensDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATACNT") == 0 ) && ( ! (DateTime.MinValue==AV111WWContagemResultadoContagensDS_4_Contagemresultado_datacnt1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataCnt] >= @AV111WWContagemResultadoContagensDS_4_Contagemresultado_datacnt1)";
         }
         else
         {
            GXv_int2[20] = 1;
         }
         if ( ( StringUtil.StrCmp(AV108WWContagemResultadoContagensDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATACNT") == 0 ) && ( ! (DateTime.MinValue==AV112WWContagemResultadoContagensDS_5_Contagemresultado_datacnt_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataCnt] <= @AV112WWContagemResultadoContagensDS_5_Contagemresultado_datacnt_to1)";
         }
         else
         {
            GXv_int2[21] = 1;
         }
         if ( ( StringUtil.StrCmp(AV108WWContagemResultadoContagensDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV113WWContagemResultadoContagensDS_6_Contagemresultado_datadmn1) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_DataDmn] >= @AV113WWContagemResultadoContagensDS_6_Contagemresultado_datadmn1)";
         }
         else
         {
            GXv_int2[22] = 1;
         }
         if ( ( StringUtil.StrCmp(AV108WWContagemResultadoContagensDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV114WWContagemResultadoContagensDS_7_Contagemresultado_datadmn_to1) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_DataDmn] <= @AV114WWContagemResultadoContagensDS_7_Contagemresultado_datadmn_to1)";
         }
         else
         {
            GXv_int2[23] = 1;
         }
         if ( ( StringUtil.StrCmp(AV108WWContagemResultadoContagensDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSCNT") == 0 ) && ( ! (0==AV116WWContagemResultadoContagensDS_9_Contagemresultado_statuscnt1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusCnt] = @AV116WWContagemResultadoContagensDS_9_Contagemresultado_statuscnt1)";
         }
         else
         {
            GXv_int2[24] = 1;
         }
         if ( ( StringUtil.StrCmp(AV108WWContagemResultadoContagensDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV117WWContagemResultadoContagensDS_10_Contagemresultado_baseline1, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_Baseline] = 1)";
         }
         if ( ( StringUtil.StrCmp(AV108WWContagemResultadoContagensDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV117WWContagemResultadoContagensDS_10_Contagemresultado_baseline1, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T4.[ContagemResultado_Baseline] = 1 or T4.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV118WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV119WWContagemResultadoContagensDS_12_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV120WWContagemResultadoContagensDS_13_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_Demanda] = @AV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 or T4.[ContagemResultado_DemandaFM] = @AV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int2[25] = 1;
            GXv_int2[26] = 1;
         }
         if ( AV118WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV119WWContagemResultadoContagensDS_12_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV120WWContagemResultadoContagensDS_13_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_Demanda] like @lV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 or T4.[ContagemResultado_DemandaFM] like @lV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int2[27] = 1;
            GXv_int2[28] = 1;
         }
         if ( AV118WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV119WWContagemResultadoContagensDS_12_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV120WWContagemResultadoContagensDS_13_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_Demanda] like '%' + @lV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 or T4.[ContagemResultado_DemandaFM] like '%' + @lV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int2[29] = 1;
            GXv_int2[30] = 1;
         }
         if ( AV118WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV119WWContagemResultadoContagensDS_12_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATACNT") == 0 ) && ( ! (DateTime.MinValue==AV122WWContagemResultadoContagensDS_15_Contagemresultado_datacnt2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataCnt] >= @AV122WWContagemResultadoContagensDS_15_Contagemresultado_datacnt2)";
         }
         else
         {
            GXv_int2[31] = 1;
         }
         if ( AV118WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV119WWContagemResultadoContagensDS_12_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATACNT") == 0 ) && ( ! (DateTime.MinValue==AV123WWContagemResultadoContagensDS_16_Contagemresultado_datacnt_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataCnt] <= @AV123WWContagemResultadoContagensDS_16_Contagemresultado_datacnt_to2)";
         }
         else
         {
            GXv_int2[32] = 1;
         }
         if ( AV118WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV119WWContagemResultadoContagensDS_12_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV124WWContagemResultadoContagensDS_17_Contagemresultado_datadmn2) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_DataDmn] >= @AV124WWContagemResultadoContagensDS_17_Contagemresultado_datadmn2)";
         }
         else
         {
            GXv_int2[33] = 1;
         }
         if ( AV118WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV119WWContagemResultadoContagensDS_12_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV125WWContagemResultadoContagensDS_18_Contagemresultado_datadmn_to2) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_DataDmn] <= @AV125WWContagemResultadoContagensDS_18_Contagemresultado_datadmn_to2)";
         }
         else
         {
            GXv_int2[34] = 1;
         }
         if ( AV118WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV119WWContagemResultadoContagensDS_12_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSCNT") == 0 ) && ( ! (0==AV127WWContagemResultadoContagensDS_20_Contagemresultado_statuscnt2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusCnt] = @AV127WWContagemResultadoContagensDS_20_Contagemresultado_statuscnt2)";
         }
         else
         {
            GXv_int2[35] = 1;
         }
         if ( AV118WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV119WWContagemResultadoContagensDS_12_Dynamicfiltersselector2, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV128WWContagemResultadoContagensDS_21_Contagemresultado_baseline2, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV118WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV119WWContagemResultadoContagensDS_12_Dynamicfiltersselector2, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV128WWContagemResultadoContagensDS_21_Contagemresultado_baseline2, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T4.[ContagemResultado_Baseline] = 1 or T4.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV129WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV130WWContagemResultadoContagensDS_23_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV131WWContagemResultadoContagensDS_24_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_Demanda] = @AV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 or T4.[ContagemResultado_DemandaFM] = @AV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int2[36] = 1;
            GXv_int2[37] = 1;
         }
         if ( AV129WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV130WWContagemResultadoContagensDS_23_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV131WWContagemResultadoContagensDS_24_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_Demanda] like @lV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 or T4.[ContagemResultado_DemandaFM] like @lV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int2[38] = 1;
            GXv_int2[39] = 1;
         }
         if ( AV129WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV130WWContagemResultadoContagensDS_23_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV131WWContagemResultadoContagensDS_24_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_Demanda] like '%' + @lV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 or T4.[ContagemResultado_DemandaFM] like '%' + @lV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int2[40] = 1;
            GXv_int2[41] = 1;
         }
         if ( AV129WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV130WWContagemResultadoContagensDS_23_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATACNT") == 0 ) && ( ! (DateTime.MinValue==AV133WWContagemResultadoContagensDS_26_Contagemresultado_datacnt3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataCnt] >= @AV133WWContagemResultadoContagensDS_26_Contagemresultado_datacnt3)";
         }
         else
         {
            GXv_int2[42] = 1;
         }
         if ( AV129WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV130WWContagemResultadoContagensDS_23_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATACNT") == 0 ) && ( ! (DateTime.MinValue==AV134WWContagemResultadoContagensDS_27_Contagemresultado_datacnt_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataCnt] <= @AV134WWContagemResultadoContagensDS_27_Contagemresultado_datacnt_to3)";
         }
         else
         {
            GXv_int2[43] = 1;
         }
         if ( AV129WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV130WWContagemResultadoContagensDS_23_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV135WWContagemResultadoContagensDS_28_Contagemresultado_datadmn3) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_DataDmn] >= @AV135WWContagemResultadoContagensDS_28_Contagemresultado_datadmn3)";
         }
         else
         {
            GXv_int2[44] = 1;
         }
         if ( AV129WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV130WWContagemResultadoContagensDS_23_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV136WWContagemResultadoContagensDS_29_Contagemresultado_datadmn_to3) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_DataDmn] <= @AV136WWContagemResultadoContagensDS_29_Contagemresultado_datadmn_to3)";
         }
         else
         {
            GXv_int2[45] = 1;
         }
         if ( AV129WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV130WWContagemResultadoContagensDS_23_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSCNT") == 0 ) && ( ! (0==AV138WWContagemResultadoContagensDS_31_Contagemresultado_statuscnt3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusCnt] = @AV138WWContagemResultadoContagensDS_31_Contagemresultado_statuscnt3)";
         }
         else
         {
            GXv_int2[46] = 1;
         }
         if ( AV129WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV130WWContagemResultadoContagensDS_23_Dynamicfiltersselector3, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV139WWContagemResultadoContagensDS_32_Contagemresultado_baseline3, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV129WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV130WWContagemResultadoContagensDS_23_Dynamicfiltersselector3, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV139WWContagemResultadoContagensDS_32_Contagemresultado_baseline3, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T4.[ContagemResultado_Baseline] = 1 or T4.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV141WWContagemResultadoContagensDS_34_Tfcontratada_areatrabalhodes_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV140WWContagemResultadoContagensDS_33_Tfcontratada_areatrabalhodes)) ) )
         {
            sWhereString = sWhereString + " and (T7.[AreaTrabalho_Descricao] like @lV140WWContagemResultadoContagensDS_33_Tfcontratada_areatrabalhodes)";
         }
         else
         {
            GXv_int2[47] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV141WWContagemResultadoContagensDS_34_Tfcontratada_areatrabalhodes_sel)) )
         {
            sWhereString = sWhereString + " and (T7.[AreaTrabalho_Descricao] = @AV141WWContagemResultadoContagensDS_34_Tfcontratada_areatrabalhodes_sel)";
         }
         else
         {
            GXv_int2[48] = 1;
         }
         if ( ! (DateTime.MinValue==AV142WWContagemResultadoContagensDS_35_Tfcontagemresultado_datadmn) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_DataDmn] >= @AV142WWContagemResultadoContagensDS_35_Tfcontagemresultado_datadmn)";
         }
         else
         {
            GXv_int2[49] = 1;
         }
         if ( ! (DateTime.MinValue==AV143WWContagemResultadoContagensDS_36_Tfcontagemresultado_datadmn_to) )
         {
            sWhereString = sWhereString + " and (T4.[ContagemResultado_DataDmn] <= @AV143WWContagemResultadoContagensDS_36_Tfcontagemresultado_datadmn_to)";
         }
         else
         {
            GXv_int2[50] = 1;
         }
         if ( ! (DateTime.MinValue==AV144WWContagemResultadoContagensDS_37_Tfcontagemresultado_datacnt) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataCnt] >= @AV144WWContagemResultadoContagensDS_37_Tfcontagemresultado_datacnt)";
         }
         else
         {
            GXv_int2[51] = 1;
         }
         if ( ! (DateTime.MinValue==AV145WWContagemResultadoContagensDS_38_Tfcontagemresultado_datacnt_to) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataCnt] <= @AV145WWContagemResultadoContagensDS_38_Tfcontagemresultado_datacnt_to)";
         }
         else
         {
            GXv_int2[52] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV147WWContagemResultadoContagensDS_40_Tfcontagemresultado_horacnt_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV146WWContagemResultadoContagensDS_39_Tfcontagemresultado_horacnt)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_HoraCnt] like @lV146WWContagemResultadoContagensDS_39_Tfcontagemresultado_horacnt)";
         }
         else
         {
            GXv_int2[53] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV147WWContagemResultadoContagensDS_40_Tfcontagemresultado_horacnt_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_HoraCnt] = @AV147WWContagemResultadoContagensDS_40_Tfcontagemresultado_horacnt_sel)";
         }
         else
         {
            GXv_int2[54] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV149WWContagemResultadoContagensDS_42_Tfcontagemresultado_osfsosfm_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV148WWContagemResultadoContagensDS_41_Tfcontagemresultado_osfsosfm)) ) )
         {
            sWhereString = sWhereString + " and (( RTRIM(LTRIM(T4.[ContagemResultado_Demanda])) + CASE  WHEN (T4.[ContagemResultado_DemandaFM] = '') THEN '' ELSE '|' + RTRIM(LTRIM(T4.[ContagemResultado_DemandaFM])) END) like @lV148WWContagemResultadoContagensDS_41_Tfcontagemresultado_osfsosfm)";
         }
         else
         {
            GXv_int2[55] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV149WWContagemResultadoContagensDS_42_Tfcontagemresultado_osfsosfm_sel)) )
         {
            sWhereString = sWhereString + " and (( RTRIM(LTRIM(T4.[ContagemResultado_Demanda])) + CASE  WHEN (T4.[ContagemResultado_DemandaFM] = '') THEN '' ELSE '|' + RTRIM(LTRIM(T4.[ContagemResultado_DemandaFM])) END) = @AV149WWContagemResultadoContagensDS_42_Tfcontagemresultado_osfsosfm_sel)";
         }
         else
         {
            GXv_int2[56] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV151WWContagemResultadoContagensDS_44_Tfcontagemrresultado_sistemasigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV150WWContagemResultadoContagensDS_43_Tfcontagemrresultado_sistemasigla)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Sistema_Sigla] like @lV150WWContagemResultadoContagensDS_43_Tfcontagemrresultado_sistemasigla)";
         }
         else
         {
            GXv_int2[57] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV151WWContagemResultadoContagensDS_44_Tfcontagemrresultado_sistemasigla_sel)) )
         {
            sWhereString = sWhereString + " and (T5.[Sistema_Sigla] = @AV151WWContagemResultadoContagensDS_44_Tfcontagemrresultado_sistemasigla_sel)";
         }
         else
         {
            GXv_int2[58] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV153WWContagemResultadoContagensDS_46_Tfcontagemresultado_contadorfmnom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV152WWContagemResultadoContagensDS_45_Tfcontagemresultado_contadorfmnom)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV152WWContagemResultadoContagensDS_45_Tfcontagemresultado_contadorfmnom)";
         }
         else
         {
            GXv_int2[59] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV153WWContagemResultadoContagensDS_46_Tfcontagemresultado_contadorfmnom_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV153WWContagemResultadoContagensDS_46_Tfcontagemresultado_contadorfmnom_sel)";
         }
         else
         {
            GXv_int2[60] = 1;
         }
         if ( AV154WWContagemResultadoContagensDS_47_Tfcontagemresultado_statuscnt_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV154WWContagemResultadoContagensDS_47_Tfcontagemresultado_statuscnt_sels, "T1.[ContagemResultado_StatusCnt] IN (", ")") + ")";
         }
         if ( ! (0==AV155WWContagemResultadoContagensDS_48_Tfcontagemresultadocontagens_esforco) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultadoContagens_Esforco] >= @AV155WWContagemResultadoContagensDS_48_Tfcontagemresultadocontagens_esforco)";
         }
         else
         {
            GXv_int2[61] = 1;
         }
         if ( ! (0==AV156WWContagemResultadoContagensDS_49_Tfcontagemresultadocontagens_esforco_to) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultadoContagens_Esforco] <= @AV156WWContagemResultadoContagensDS_49_Tfcontagemresultadocontagens_esforco_to)";
         }
         else
         {
            GXv_int2[62] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV157WWContagemResultadoContagensDS_50_Tfcontagemresultado_pfbfs) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_PFBFS] >= @AV157WWContagemResultadoContagensDS_50_Tfcontagemresultado_pfbfs)";
         }
         else
         {
            GXv_int2[63] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV158WWContagemResultadoContagensDS_51_Tfcontagemresultado_pfbfs_to) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_PFBFS] <= @AV158WWContagemResultadoContagensDS_51_Tfcontagemresultado_pfbfs_to)";
         }
         else
         {
            GXv_int2[64] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV159WWContagemResultadoContagensDS_52_Tfcontagemresultado_pflfs) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_PFLFS] >= @AV159WWContagemResultadoContagensDS_52_Tfcontagemresultado_pflfs)";
         }
         else
         {
            GXv_int2[65] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV160WWContagemResultadoContagensDS_53_Tfcontagemresultado_pflfs_to) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_PFLFS] <= @AV160WWContagemResultadoContagensDS_53_Tfcontagemresultado_pflfs_to)";
         }
         else
         {
            GXv_int2[66] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV161WWContagemResultadoContagensDS_54_Tfcontagemresultado_pfbfm) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_PFBFM] >= @AV161WWContagemResultadoContagensDS_54_Tfcontagemresultado_pfbfm)";
         }
         else
         {
            GXv_int2[67] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV162WWContagemResultadoContagensDS_55_Tfcontagemresultado_pfbfm_to) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_PFBFM] <= @AV162WWContagemResultadoContagensDS_55_Tfcontagemresultado_pfbfm_to)";
         }
         else
         {
            GXv_int2[68] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV163WWContagemResultadoContagensDS_56_Tfcontagemresultado_pflfm) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_PFLFM] >= @AV163WWContagemResultadoContagensDS_56_Tfcontagemresultado_pflfm)";
         }
         else
         {
            GXv_int2[69] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV164WWContagemResultadoContagensDS_57_Tfcontagemresultado_pflfm_to) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_PFLFM] <= @AV164WWContagemResultadoContagensDS_57_Tfcontagemresultado_pflfm_to)";
         }
         else
         {
            GXv_int2[70] = 1;
         }
         if ( ! ( AV67Usuario_EhGestor || AV9WWPContext_gxTpr_Userehfinanceiro || AV9WWPContext_gxTpr_Userehcontratante ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContadorFMCod] = @AV9WWPContext__Userid)";
         }
         else
         {
            GXv_int2[71] = 1;
         }
         if ( AV55Flag != 1 )
         {
            sWhereString = sWhereString + " and (T6.[Contratada_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         }
         else
         {
            GXv_int2[72] = 1;
         }
         if ( ! (0==AV9WWPContext_gxTpr_Contratada_codigo) && ( AV55Flag == 1 ) )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV66Contratadas, "T4.[ContagemResultado_ContratadaCod] IN (", ")") + ")";
         }
         if ( AV45GridState_gxTpr_Dynamicfilters_Count < 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataCnt] = @Gx_date)";
         }
         else
         {
            GXv_int2[73] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( AV16OrderedBy == 1 )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Codigo] DESC, T1.[ContagemResultado_DataCnt] DESC, T1.[ContagemResultado_HoraCnt] DESC";
         }
         else if ( AV16OrderedBy == 2 )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_DataCnt] DESC";
         }
         else if ( ( AV16OrderedBy == 3 ) && ! AV17OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T7.[AreaTrabalho_Descricao]";
         }
         else if ( ( AV16OrderedBy == 3 ) && ( AV17OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T7.[AreaTrabalho_Descricao] DESC";
         }
         else if ( ( AV16OrderedBy == 4 ) && ! AV17OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T4.[ContagemResultado_DataDmn]";
         }
         else if ( ( AV16OrderedBy == 4 ) && ( AV17OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T4.[ContagemResultado_DataDmn] DESC";
         }
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_P003X5( IGxContext context ,
                                             short A483ContagemResultado_StatusCnt ,
                                             IGxCollection AV154WWContagemResultadoContagensDS_47_Tfcontagemresultado_statuscnt_sels ,
                                             String AV108WWContagemResultadoContagensDS_1_Dynamicfiltersselector1 ,
                                             short AV109WWContagemResultadoContagensDS_2_Dynamicfiltersoperator1 ,
                                             String AV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 ,
                                             DateTime AV111WWContagemResultadoContagensDS_4_Contagemresultado_datacnt1 ,
                                             DateTime AV112WWContagemResultadoContagensDS_5_Contagemresultado_datacnt_to1 ,
                                             DateTime AV113WWContagemResultadoContagensDS_6_Contagemresultado_datadmn1 ,
                                             DateTime AV114WWContagemResultadoContagensDS_7_Contagemresultado_datadmn_to1 ,
                                             short AV116WWContagemResultadoContagensDS_9_Contagemresultado_statuscnt1 ,
                                             String AV117WWContagemResultadoContagensDS_10_Contagemresultado_baseline1 ,
                                             bool AV118WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 ,
                                             String AV119WWContagemResultadoContagensDS_12_Dynamicfiltersselector2 ,
                                             short AV120WWContagemResultadoContagensDS_13_Dynamicfiltersoperator2 ,
                                             String AV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 ,
                                             DateTime AV122WWContagemResultadoContagensDS_15_Contagemresultado_datacnt2 ,
                                             DateTime AV123WWContagemResultadoContagensDS_16_Contagemresultado_datacnt_to2 ,
                                             DateTime AV124WWContagemResultadoContagensDS_17_Contagemresultado_datadmn2 ,
                                             DateTime AV125WWContagemResultadoContagensDS_18_Contagemresultado_datadmn_to2 ,
                                             short AV127WWContagemResultadoContagensDS_20_Contagemresultado_statuscnt2 ,
                                             String AV128WWContagemResultadoContagensDS_21_Contagemresultado_baseline2 ,
                                             bool AV129WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 ,
                                             String AV130WWContagemResultadoContagensDS_23_Dynamicfiltersselector3 ,
                                             short AV131WWContagemResultadoContagensDS_24_Dynamicfiltersoperator3 ,
                                             String AV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 ,
                                             DateTime AV133WWContagemResultadoContagensDS_26_Contagemresultado_datacnt3 ,
                                             DateTime AV134WWContagemResultadoContagensDS_27_Contagemresultado_datacnt_to3 ,
                                             DateTime AV135WWContagemResultadoContagensDS_28_Contagemresultado_datadmn3 ,
                                             DateTime AV136WWContagemResultadoContagensDS_29_Contagemresultado_datadmn_to3 ,
                                             short AV138WWContagemResultadoContagensDS_31_Contagemresultado_statuscnt3 ,
                                             String AV139WWContagemResultadoContagensDS_32_Contagemresultado_baseline3 ,
                                             String AV141WWContagemResultadoContagensDS_34_Tfcontratada_areatrabalhodes_sel ,
                                             String AV140WWContagemResultadoContagensDS_33_Tfcontratada_areatrabalhodes ,
                                             DateTime AV142WWContagemResultadoContagensDS_35_Tfcontagemresultado_datadmn ,
                                             DateTime AV143WWContagemResultadoContagensDS_36_Tfcontagemresultado_datadmn_to ,
                                             DateTime AV144WWContagemResultadoContagensDS_37_Tfcontagemresultado_datacnt ,
                                             DateTime AV145WWContagemResultadoContagensDS_38_Tfcontagemresultado_datacnt_to ,
                                             String AV147WWContagemResultadoContagensDS_40_Tfcontagemresultado_horacnt_sel ,
                                             String AV146WWContagemResultadoContagensDS_39_Tfcontagemresultado_horacnt ,
                                             String AV149WWContagemResultadoContagensDS_42_Tfcontagemresultado_osfsosfm_sel ,
                                             String AV148WWContagemResultadoContagensDS_41_Tfcontagemresultado_osfsosfm ,
                                             String AV151WWContagemResultadoContagensDS_44_Tfcontagemrresultado_sistemasigla_sel ,
                                             String AV150WWContagemResultadoContagensDS_43_Tfcontagemrresultado_sistemasigla ,
                                             String AV153WWContagemResultadoContagensDS_46_Tfcontagemresultado_contadorfmnom_sel ,
                                             String AV152WWContagemResultadoContagensDS_45_Tfcontagemresultado_contadorfmnom ,
                                             int AV154WWContagemResultadoContagensDS_47_Tfcontagemresultado_statuscnt_sels_Count ,
                                             short AV155WWContagemResultadoContagensDS_48_Tfcontagemresultadocontagens_esforco ,
                                             short AV156WWContagemResultadoContagensDS_49_Tfcontagemresultadocontagens_esforco_to ,
                                             decimal AV157WWContagemResultadoContagensDS_50_Tfcontagemresultado_pfbfs ,
                                             decimal AV158WWContagemResultadoContagensDS_51_Tfcontagemresultado_pfbfs_to ,
                                             decimal AV159WWContagemResultadoContagensDS_52_Tfcontagemresultado_pflfs ,
                                             decimal AV160WWContagemResultadoContagensDS_53_Tfcontagemresultado_pflfs_to ,
                                             decimal AV161WWContagemResultadoContagensDS_54_Tfcontagemresultado_pfbfm ,
                                             decimal AV162WWContagemResultadoContagensDS_55_Tfcontagemresultado_pfbfm_to ,
                                             decimal AV163WWContagemResultadoContagensDS_56_Tfcontagemresultado_pflfm ,
                                             decimal AV164WWContagemResultadoContagensDS_57_Tfcontagemresultado_pflfm_to ,
                                             bool AV9WWPContext_gxTpr_Userehadministradorgam ,
                                             bool AV9WWPContext_gxTpr_Userehfinanceiro ,
                                             short AV55Flag ,
                                             int AV45GridState_gxTpr_Dynamicfilters_Count ,
                                             String A457ContagemResultado_Demanda ,
                                             String A493ContagemResultado_DemandaFM ,
                                             DateTime A473ContagemResultado_DataCnt ,
                                             DateTime A471ContagemResultado_DataDmn ,
                                             bool A598ContagemResultado_Baseline ,
                                             String A53Contratada_AreaTrabalhoDes ,
                                             String A511ContagemResultado_HoraCnt ,
                                             String A509ContagemrResultado_SistemaSigla ,
                                             String A474ContagemResultado_ContadorFMNom ,
                                             short A482ContagemResultadoContagens_Esforco ,
                                             decimal A458ContagemResultado_PFBFS ,
                                             decimal A459ContagemResultado_PFLFS ,
                                             decimal A460ContagemResultado_PFBFM ,
                                             decimal A461ContagemResultado_PFLFM ,
                                             int A470ContagemResultado_ContadorFMCod ,
                                             short AV9WWPContext_gxTpr_Userid ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo ,
                                             DateTime Gx_date )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [74] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT SUM(T1.[ContagemResultadoContagens_Esforco]), SUM(T1.[ContagemResultado_PFBFS]), SUM(T1.[ContagemResultado_PFLFS]), SUM(T1.[ContagemResultado_PFBFM]), SUM(T1.[ContagemResultado_PFLFM]) FROM ((((((([ContagemResultadoContagens] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[ContagemResultado_ContratadaCod]) LEFT JOIN [AreaTrabalho] T4 WITH (NOLOCK) ON T4.[AreaTrabalho_Codigo] = T3.[Contratada_AreaTrabalhoCod]) LEFT JOIN [Sistema] T5 WITH (NOLOCK) ON T5.[Sistema_Codigo] = T2.[ContagemResultado_SistemaCod]) LEFT JOIN (SELECT MIN([ContagemResultado_ContadorFMCod]) AS ContagemResultado_ContadorFM, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T6 ON T6.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) INNER JOIN [Usuario] T7 WITH (NOLOCK) ON T7.[Usuario_Codigo] = T1.[ContagemResultado_ContadorFMCod]) LEFT JOIN [Pessoa] T8 WITH (NOLOCK) ON T8.[Pessoa_Codigo] = T7.[Usuario_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (Not ( @AV108WWContagemResultadoContagensDS_1_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV115WWContagemResultadoContagensDS_8_Contagemresultado_contadorfm1 = convert(int, 0)))) or ( COALESCE( T6.[ContagemResultado_ContadorFM], 0) = @AV115WWContagemResultadoContagensDS_8_Contagemresultado_contadorfm1 or ( (COALESCE( T6.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T2.[ContagemResultado_Owner] = @AV115WWContagemResultadoContagensDS_8_Contagemresultado_contadorfm1)))";
         scmdbuf = scmdbuf + " and (Not ( @AV118WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 = 1 and @AV119WWContagemResultadoContagensDS_12_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV126WWContagemResultadoContagensDS_19_Contagemresultado_contadorfm2 = convert(int, 0)))) or ( COALESCE( T6.[ContagemResultado_ContadorFM], 0) = @AV126WWContagemResultadoContagensDS_19_Contagemresultado_contadorfm2 or ( (COALESCE( T6.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T2.[ContagemResultado_Owner] = @AV126WWContagemResultadoContagensDS_19_Contagemresultado_contadorfm2)))";
         scmdbuf = scmdbuf + " and (Not ( @AV129WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 = 1 and @AV130WWContagemResultadoContagensDS_23_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_CONTADORFM' and ( Not (@AV137WWContagemResultadoContagensDS_30_Contagemresultado_contadorfm3 = convert(int, 0)))) or ( COALESCE( T6.[ContagemResultado_ContadorFM], 0) = @AV137WWContagemResultadoContagensDS_30_Contagemresultado_contadorfm3 or ( (COALESCE( T6.[ContagemResultado_ContadorFM], 0) = convert(int, 0)) and T2.[ContagemResultado_Owner] = @AV137WWContagemResultadoContagensDS_30_Contagemresultado_contadorfm3)))";
         if ( ( StringUtil.StrCmp(AV108WWContagemResultadoContagensDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV109WWContagemResultadoContagensDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 or T2.[ContagemResultado_DemandaFM] = @AV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int4[14] = 1;
            GXv_int4[15] = 1;
         }
         if ( ( StringUtil.StrCmp(AV108WWContagemResultadoContagensDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV109WWContagemResultadoContagensDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 or T2.[ContagemResultado_DemandaFM] like @lV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int4[16] = 1;
            GXv_int4[17] = 1;
         }
         if ( ( StringUtil.StrCmp(AV108WWContagemResultadoContagensDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV109WWContagemResultadoContagensDS_2_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like '%' + @lV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1 or T2.[ContagemResultado_DemandaFM] like '%' + @lV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int4[18] = 1;
            GXv_int4[19] = 1;
         }
         if ( ( StringUtil.StrCmp(AV108WWContagemResultadoContagensDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATACNT") == 0 ) && ( ! (DateTime.MinValue==AV111WWContagemResultadoContagensDS_4_Contagemresultado_datacnt1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataCnt] >= @AV111WWContagemResultadoContagensDS_4_Contagemresultado_datacnt1)";
         }
         else
         {
            GXv_int4[20] = 1;
         }
         if ( ( StringUtil.StrCmp(AV108WWContagemResultadoContagensDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATACNT") == 0 ) && ( ! (DateTime.MinValue==AV112WWContagemResultadoContagensDS_5_Contagemresultado_datacnt_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataCnt] <= @AV112WWContagemResultadoContagensDS_5_Contagemresultado_datacnt_to1)";
         }
         else
         {
            GXv_int4[21] = 1;
         }
         if ( ( StringUtil.StrCmp(AV108WWContagemResultadoContagensDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV113WWContagemResultadoContagensDS_6_Contagemresultado_datadmn1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] >= @AV113WWContagemResultadoContagensDS_6_Contagemresultado_datadmn1)";
         }
         else
         {
            GXv_int4[22] = 1;
         }
         if ( ( StringUtil.StrCmp(AV108WWContagemResultadoContagensDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV114WWContagemResultadoContagensDS_7_Contagemresultado_datadmn_to1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] <= @AV114WWContagemResultadoContagensDS_7_Contagemresultado_datadmn_to1)";
         }
         else
         {
            GXv_int4[23] = 1;
         }
         if ( ( StringUtil.StrCmp(AV108WWContagemResultadoContagensDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSCNT") == 0 ) && ( ! (0==AV116WWContagemResultadoContagensDS_9_Contagemresultado_statuscnt1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusCnt] = @AV116WWContagemResultadoContagensDS_9_Contagemresultado_statuscnt1)";
         }
         else
         {
            GXv_int4[24] = 1;
         }
         if ( ( StringUtil.StrCmp(AV108WWContagemResultadoContagensDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV117WWContagemResultadoContagensDS_10_Contagemresultado_baseline1, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Baseline] = 1)";
         }
         if ( ( StringUtil.StrCmp(AV108WWContagemResultadoContagensDS_1_Dynamicfiltersselector1, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV117WWContagemResultadoContagensDS_10_Contagemresultado_baseline1, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T2.[ContagemResultado_Baseline] = 1 or T2.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV118WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV119WWContagemResultadoContagensDS_12_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV120WWContagemResultadoContagensDS_13_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 or T2.[ContagemResultado_DemandaFM] = @AV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int4[25] = 1;
            GXv_int4[26] = 1;
         }
         if ( AV118WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV119WWContagemResultadoContagensDS_12_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV120WWContagemResultadoContagensDS_13_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 or T2.[ContagemResultado_DemandaFM] like @lV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int4[27] = 1;
            GXv_int4[28] = 1;
         }
         if ( AV118WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV119WWContagemResultadoContagensDS_12_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV120WWContagemResultadoContagensDS_13_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like '%' + @lV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2 or T2.[ContagemResultado_DemandaFM] like '%' + @lV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int4[29] = 1;
            GXv_int4[30] = 1;
         }
         if ( AV118WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV119WWContagemResultadoContagensDS_12_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATACNT") == 0 ) && ( ! (DateTime.MinValue==AV122WWContagemResultadoContagensDS_15_Contagemresultado_datacnt2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataCnt] >= @AV122WWContagemResultadoContagensDS_15_Contagemresultado_datacnt2)";
         }
         else
         {
            GXv_int4[31] = 1;
         }
         if ( AV118WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV119WWContagemResultadoContagensDS_12_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATACNT") == 0 ) && ( ! (DateTime.MinValue==AV123WWContagemResultadoContagensDS_16_Contagemresultado_datacnt_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataCnt] <= @AV123WWContagemResultadoContagensDS_16_Contagemresultado_datacnt_to2)";
         }
         else
         {
            GXv_int4[32] = 1;
         }
         if ( AV118WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV119WWContagemResultadoContagensDS_12_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV124WWContagemResultadoContagensDS_17_Contagemresultado_datadmn2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] >= @AV124WWContagemResultadoContagensDS_17_Contagemresultado_datadmn2)";
         }
         else
         {
            GXv_int4[33] = 1;
         }
         if ( AV118WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV119WWContagemResultadoContagensDS_12_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV125WWContagemResultadoContagensDS_18_Contagemresultado_datadmn_to2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] <= @AV125WWContagemResultadoContagensDS_18_Contagemresultado_datadmn_to2)";
         }
         else
         {
            GXv_int4[34] = 1;
         }
         if ( AV118WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV119WWContagemResultadoContagensDS_12_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSCNT") == 0 ) && ( ! (0==AV127WWContagemResultadoContagensDS_20_Contagemresultado_statuscnt2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusCnt] = @AV127WWContagemResultadoContagensDS_20_Contagemresultado_statuscnt2)";
         }
         else
         {
            GXv_int4[35] = 1;
         }
         if ( AV118WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV119WWContagemResultadoContagensDS_12_Dynamicfiltersselector2, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV128WWContagemResultadoContagensDS_21_Contagemresultado_baseline2, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV118WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV119WWContagemResultadoContagensDS_12_Dynamicfiltersselector2, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV128WWContagemResultadoContagensDS_21_Contagemresultado_baseline2, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T2.[ContagemResultado_Baseline] = 1 or T2.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV129WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV130WWContagemResultadoContagensDS_23_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV131WWContagemResultadoContagensDS_24_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 or T2.[ContagemResultado_DemandaFM] = @AV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int4[36] = 1;
            GXv_int4[37] = 1;
         }
         if ( AV129WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV130WWContagemResultadoContagensDS_23_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV131WWContagemResultadoContagensDS_24_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 or T2.[ContagemResultado_DemandaFM] like @lV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int4[38] = 1;
            GXv_int4[39] = 1;
         }
         if ( AV129WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV130WWContagemResultadoContagensDS_23_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV131WWContagemResultadoContagensDS_24_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like '%' + @lV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3 or T2.[ContagemResultado_DemandaFM] like '%' + @lV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int4[40] = 1;
            GXv_int4[41] = 1;
         }
         if ( AV129WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV130WWContagemResultadoContagensDS_23_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATACNT") == 0 ) && ( ! (DateTime.MinValue==AV133WWContagemResultadoContagensDS_26_Contagemresultado_datacnt3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataCnt] >= @AV133WWContagemResultadoContagensDS_26_Contagemresultado_datacnt3)";
         }
         else
         {
            GXv_int4[42] = 1;
         }
         if ( AV129WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV130WWContagemResultadoContagensDS_23_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATACNT") == 0 ) && ( ! (DateTime.MinValue==AV134WWContagemResultadoContagensDS_27_Contagemresultado_datacnt_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataCnt] <= @AV134WWContagemResultadoContagensDS_27_Contagemresultado_datacnt_to3)";
         }
         else
         {
            GXv_int4[43] = 1;
         }
         if ( AV129WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV130WWContagemResultadoContagensDS_23_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV135WWContagemResultadoContagensDS_28_Contagemresultado_datadmn3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] >= @AV135WWContagemResultadoContagensDS_28_Contagemresultado_datadmn3)";
         }
         else
         {
            GXv_int4[44] = 1;
         }
         if ( AV129WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV130WWContagemResultadoContagensDS_23_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV136WWContagemResultadoContagensDS_29_Contagemresultado_datadmn_to3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] <= @AV136WWContagemResultadoContagensDS_29_Contagemresultado_datadmn_to3)";
         }
         else
         {
            GXv_int4[45] = 1;
         }
         if ( AV129WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV130WWContagemResultadoContagensDS_23_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSCNT") == 0 ) && ( ! (0==AV138WWContagemResultadoContagensDS_31_Contagemresultado_statuscnt3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusCnt] = @AV138WWContagemResultadoContagensDS_31_Contagemresultado_statuscnt3)";
         }
         else
         {
            GXv_int4[46] = 1;
         }
         if ( AV129WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV130WWContagemResultadoContagensDS_23_Dynamicfiltersselector3, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV139WWContagemResultadoContagensDS_32_Contagemresultado_baseline3, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV129WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV130WWContagemResultadoContagensDS_23_Dynamicfiltersselector3, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV139WWContagemResultadoContagensDS_32_Contagemresultado_baseline3, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T2.[ContagemResultado_Baseline] = 1 or T2.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV141WWContagemResultadoContagensDS_34_Tfcontratada_areatrabalhodes_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV140WWContagemResultadoContagensDS_33_Tfcontratada_areatrabalhodes)) ) )
         {
            sWhereString = sWhereString + " and (T4.[AreaTrabalho_Descricao] like @lV140WWContagemResultadoContagensDS_33_Tfcontratada_areatrabalhodes)";
         }
         else
         {
            GXv_int4[47] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV141WWContagemResultadoContagensDS_34_Tfcontratada_areatrabalhodes_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[AreaTrabalho_Descricao] = @AV141WWContagemResultadoContagensDS_34_Tfcontratada_areatrabalhodes_sel)";
         }
         else
         {
            GXv_int4[48] = 1;
         }
         if ( ! (DateTime.MinValue==AV142WWContagemResultadoContagensDS_35_Tfcontagemresultado_datadmn) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] >= @AV142WWContagemResultadoContagensDS_35_Tfcontagemresultado_datadmn)";
         }
         else
         {
            GXv_int4[49] = 1;
         }
         if ( ! (DateTime.MinValue==AV143WWContagemResultadoContagensDS_36_Tfcontagemresultado_datadmn_to) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] <= @AV143WWContagemResultadoContagensDS_36_Tfcontagemresultado_datadmn_to)";
         }
         else
         {
            GXv_int4[50] = 1;
         }
         if ( ! (DateTime.MinValue==AV144WWContagemResultadoContagensDS_37_Tfcontagemresultado_datacnt) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataCnt] >= @AV144WWContagemResultadoContagensDS_37_Tfcontagemresultado_datacnt)";
         }
         else
         {
            GXv_int4[51] = 1;
         }
         if ( ! (DateTime.MinValue==AV145WWContagemResultadoContagensDS_38_Tfcontagemresultado_datacnt_to) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataCnt] <= @AV145WWContagemResultadoContagensDS_38_Tfcontagemresultado_datacnt_to)";
         }
         else
         {
            GXv_int4[52] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV147WWContagemResultadoContagensDS_40_Tfcontagemresultado_horacnt_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV146WWContagemResultadoContagensDS_39_Tfcontagemresultado_horacnt)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_HoraCnt] like @lV146WWContagemResultadoContagensDS_39_Tfcontagemresultado_horacnt)";
         }
         else
         {
            GXv_int4[53] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV147WWContagemResultadoContagensDS_40_Tfcontagemresultado_horacnt_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_HoraCnt] = @AV147WWContagemResultadoContagensDS_40_Tfcontagemresultado_horacnt_sel)";
         }
         else
         {
            GXv_int4[54] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV149WWContagemResultadoContagensDS_42_Tfcontagemresultado_osfsosfm_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV148WWContagemResultadoContagensDS_41_Tfcontagemresultado_osfsosfm)) ) )
         {
            sWhereString = sWhereString + " and (( RTRIM(LTRIM(T2.[ContagemResultado_Demanda])) + CASE  WHEN (T2.[ContagemResultado_DemandaFM] = '') THEN '' ELSE '|' + RTRIM(LTRIM(T2.[ContagemResultado_DemandaFM])) END) like @lV148WWContagemResultadoContagensDS_41_Tfcontagemresultado_osfsosfm)";
         }
         else
         {
            GXv_int4[55] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV149WWContagemResultadoContagensDS_42_Tfcontagemresultado_osfsosfm_sel)) )
         {
            sWhereString = sWhereString + " and (( RTRIM(LTRIM(T2.[ContagemResultado_Demanda])) + CASE  WHEN (T2.[ContagemResultado_DemandaFM] = '') THEN '' ELSE '|' + RTRIM(LTRIM(T2.[ContagemResultado_DemandaFM])) END) = @AV149WWContagemResultadoContagensDS_42_Tfcontagemresultado_osfsosfm_sel)";
         }
         else
         {
            GXv_int4[56] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV151WWContagemResultadoContagensDS_44_Tfcontagemrresultado_sistemasigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV150WWContagemResultadoContagensDS_43_Tfcontagemrresultado_sistemasigla)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Sistema_Sigla] like @lV150WWContagemResultadoContagensDS_43_Tfcontagemrresultado_sistemasigla)";
         }
         else
         {
            GXv_int4[57] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV151WWContagemResultadoContagensDS_44_Tfcontagemrresultado_sistemasigla_sel)) )
         {
            sWhereString = sWhereString + " and (T5.[Sistema_Sigla] = @AV151WWContagemResultadoContagensDS_44_Tfcontagemrresultado_sistemasigla_sel)";
         }
         else
         {
            GXv_int4[58] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV153WWContagemResultadoContagensDS_46_Tfcontagemresultado_contadorfmnom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV152WWContagemResultadoContagensDS_45_Tfcontagemresultado_contadorfmnom)) ) )
         {
            sWhereString = sWhereString + " and (T8.[Pessoa_Nome] like @lV152WWContagemResultadoContagensDS_45_Tfcontagemresultado_contadorfmnom)";
         }
         else
         {
            GXv_int4[59] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV153WWContagemResultadoContagensDS_46_Tfcontagemresultado_contadorfmnom_sel)) )
         {
            sWhereString = sWhereString + " and (T8.[Pessoa_Nome] = @AV153WWContagemResultadoContagensDS_46_Tfcontagemresultado_contadorfmnom_sel)";
         }
         else
         {
            GXv_int4[60] = 1;
         }
         if ( AV154WWContagemResultadoContagensDS_47_Tfcontagemresultado_statuscnt_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV154WWContagemResultadoContagensDS_47_Tfcontagemresultado_statuscnt_sels, "T1.[ContagemResultado_StatusCnt] IN (", ")") + ")";
         }
         if ( ! (0==AV155WWContagemResultadoContagensDS_48_Tfcontagemresultadocontagens_esforco) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultadoContagens_Esforco] >= @AV155WWContagemResultadoContagensDS_48_Tfcontagemresultadocontagens_esforco)";
         }
         else
         {
            GXv_int4[61] = 1;
         }
         if ( ! (0==AV156WWContagemResultadoContagensDS_49_Tfcontagemresultadocontagens_esforco_to) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultadoContagens_Esforco] <= @AV156WWContagemResultadoContagensDS_49_Tfcontagemresultadocontagens_esforco_to)";
         }
         else
         {
            GXv_int4[62] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV157WWContagemResultadoContagensDS_50_Tfcontagemresultado_pfbfs) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_PFBFS] >= @AV157WWContagemResultadoContagensDS_50_Tfcontagemresultado_pfbfs)";
         }
         else
         {
            GXv_int4[63] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV158WWContagemResultadoContagensDS_51_Tfcontagemresultado_pfbfs_to) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_PFBFS] <= @AV158WWContagemResultadoContagensDS_51_Tfcontagemresultado_pfbfs_to)";
         }
         else
         {
            GXv_int4[64] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV159WWContagemResultadoContagensDS_52_Tfcontagemresultado_pflfs) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_PFLFS] >= @AV159WWContagemResultadoContagensDS_52_Tfcontagemresultado_pflfs)";
         }
         else
         {
            GXv_int4[65] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV160WWContagemResultadoContagensDS_53_Tfcontagemresultado_pflfs_to) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_PFLFS] <= @AV160WWContagemResultadoContagensDS_53_Tfcontagemresultado_pflfs_to)";
         }
         else
         {
            GXv_int4[66] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV161WWContagemResultadoContagensDS_54_Tfcontagemresultado_pfbfm) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_PFBFM] >= @AV161WWContagemResultadoContagensDS_54_Tfcontagemresultado_pfbfm)";
         }
         else
         {
            GXv_int4[67] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV162WWContagemResultadoContagensDS_55_Tfcontagemresultado_pfbfm_to) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_PFBFM] <= @AV162WWContagemResultadoContagensDS_55_Tfcontagemresultado_pfbfm_to)";
         }
         else
         {
            GXv_int4[68] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV163WWContagemResultadoContagensDS_56_Tfcontagemresultado_pflfm) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_PFLFM] >= @AV163WWContagemResultadoContagensDS_56_Tfcontagemresultado_pflfm)";
         }
         else
         {
            GXv_int4[69] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV164WWContagemResultadoContagensDS_57_Tfcontagemresultado_pflfm_to) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_PFLFM] <= @AV164WWContagemResultadoContagensDS_57_Tfcontagemresultado_pflfm_to)";
         }
         else
         {
            GXv_int4[70] = 1;
         }
         if ( ! AV9WWPContext_gxTpr_Userehadministradorgam && ! AV9WWPContext_gxTpr_Userehfinanceiro )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContadorFMCod] = @AV9WWPContext__Userid)";
         }
         else
         {
            GXv_int4[71] = 1;
         }
         if ( AV55Flag == 0 )
         {
            sWhereString = sWhereString + " and (T3.[Contratada_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         }
         else
         {
            GXv_int4[72] = 1;
         }
         if ( AV45GridState_gxTpr_Dynamicfilters_Count < 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataCnt] = @Gx_date)";
         }
         else
         {
            GXv_int4[73] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + "";
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P003X3(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (bool)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (String)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (DateTime)dynConstraints[19] , (DateTime)dynConstraints[20] , (short)dynConstraints[21] , (String)dynConstraints[22] , (bool)dynConstraints[23] , (String)dynConstraints[24] , (short)dynConstraints[25] , (String)dynConstraints[26] , (DateTime)dynConstraints[27] , (DateTime)dynConstraints[28] , (DateTime)dynConstraints[29] , (DateTime)dynConstraints[30] , (short)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (DateTime)dynConstraints[35] , (DateTime)dynConstraints[36] , (DateTime)dynConstraints[37] , (DateTime)dynConstraints[38] , (String)dynConstraints[39] , (String)dynConstraints[40] , (String)dynConstraints[41] , (String)dynConstraints[42] , (String)dynConstraints[43] , (String)dynConstraints[44] , (String)dynConstraints[45] , (String)dynConstraints[46] , (int)dynConstraints[47] , (short)dynConstraints[48] , (short)dynConstraints[49] , (decimal)dynConstraints[50] , (decimal)dynConstraints[51] , (decimal)dynConstraints[52] , (decimal)dynConstraints[53] , (decimal)dynConstraints[54] , (decimal)dynConstraints[55] , (decimal)dynConstraints[56] , (decimal)dynConstraints[57] , (bool)dynConstraints[58] , (bool)dynConstraints[59] , (bool)dynConstraints[60] , (short)dynConstraints[61] , (int)dynConstraints[62] , (int)dynConstraints[63] , (String)dynConstraints[64] , (String)dynConstraints[65] , (DateTime)dynConstraints[66] , (DateTime)dynConstraints[67] , (bool)dynConstraints[68] , (String)dynConstraints[69] , (String)dynConstraints[70] , (String)dynConstraints[71] , (String)dynConstraints[72] , (short)dynConstraints[73] , (decimal)dynConstraints[74] , (decimal)dynConstraints[75] , (decimal)dynConstraints[76] , (decimal)dynConstraints[77] , (int)dynConstraints[78] , (short)dynConstraints[79] , (int)dynConstraints[80] , (int)dynConstraints[81] , (DateTime)dynConstraints[82] , (short)dynConstraints[83] , (bool)dynConstraints[84] , (int)dynConstraints[85] , (int)dynConstraints[86] , (int)dynConstraints[87] , (int)dynConstraints[88] , (int)dynConstraints[89] );
               case 1 :
                     return conditional_P003X5(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (short)dynConstraints[19] , (String)dynConstraints[20] , (bool)dynConstraints[21] , (String)dynConstraints[22] , (short)dynConstraints[23] , (String)dynConstraints[24] , (DateTime)dynConstraints[25] , (DateTime)dynConstraints[26] , (DateTime)dynConstraints[27] , (DateTime)dynConstraints[28] , (short)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (DateTime)dynConstraints[33] , (DateTime)dynConstraints[34] , (DateTime)dynConstraints[35] , (DateTime)dynConstraints[36] , (String)dynConstraints[37] , (String)dynConstraints[38] , (String)dynConstraints[39] , (String)dynConstraints[40] , (String)dynConstraints[41] , (String)dynConstraints[42] , (String)dynConstraints[43] , (String)dynConstraints[44] , (int)dynConstraints[45] , (short)dynConstraints[46] , (short)dynConstraints[47] , (decimal)dynConstraints[48] , (decimal)dynConstraints[49] , (decimal)dynConstraints[50] , (decimal)dynConstraints[51] , (decimal)dynConstraints[52] , (decimal)dynConstraints[53] , (decimal)dynConstraints[54] , (decimal)dynConstraints[55] , (bool)dynConstraints[56] , (bool)dynConstraints[57] , (short)dynConstraints[58] , (int)dynConstraints[59] , (String)dynConstraints[60] , (String)dynConstraints[61] , (DateTime)dynConstraints[62] , (DateTime)dynConstraints[63] , (bool)dynConstraints[64] , (String)dynConstraints[65] , (String)dynConstraints[66] , (String)dynConstraints[67] , (String)dynConstraints[68] , (short)dynConstraints[69] , (decimal)dynConstraints[70] , (decimal)dynConstraints[71] , (decimal)dynConstraints[72] , (decimal)dynConstraints[73] , (int)dynConstraints[74] , (short)dynConstraints[75] , (int)dynConstraints[76] , (int)dynConstraints[77] , (DateTime)dynConstraints[78] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP003X3 ;
          prmP003X3 = new Object[] {
          new Object[] {"@AV108WWContagemResultadoContagensDS_1_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV115WWContagemResultadoContagensDS_8_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV115WWContagemResultadoContagensDS_8_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV115WWContagemResultadoContagensDS_8_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV118WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV119WWContagemResultadoContagensDS_12_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV126WWContagemResultadoContagensDS_19_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV126WWContagemResultadoContagensDS_19_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV126WWContagemResultadoContagensDS_19_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV129WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV130WWContagemResultadoContagensDS_23_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV137WWContagemResultadoContagensDS_30_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV137WWContagemResultadoContagensDS_30_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV137WWContagemResultadoContagensDS_30_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV111WWContagemResultadoContagensDS_4_Contagemresultado_datacnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV112WWContagemResultadoContagensDS_5_Contagemresultado_datacnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV113WWContagemResultadoContagensDS_6_Contagemresultado_datadmn1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV114WWContagemResultadoContagensDS_7_Contagemresultado_datadmn_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV116WWContagemResultadoContagensDS_9_Contagemresultado_statuscnt1",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@AV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV122WWContagemResultadoContagensDS_15_Contagemresultado_datacnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV123WWContagemResultadoContagensDS_16_Contagemresultado_datacnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV124WWContagemResultadoContagensDS_17_Contagemresultado_datadmn2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV125WWContagemResultadoContagensDS_18_Contagemresultado_datadmn_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV127WWContagemResultadoContagensDS_20_Contagemresultado_statuscnt2",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@AV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV133WWContagemResultadoContagensDS_26_Contagemresultado_datacnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV134WWContagemResultadoContagensDS_27_Contagemresultado_datacnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV135WWContagemResultadoContagensDS_28_Contagemresultado_datadmn3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV136WWContagemResultadoContagensDS_29_Contagemresultado_datadmn_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV138WWContagemResultadoContagensDS_31_Contagemresultado_statuscnt3",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@lV140WWContagemResultadoContagensDS_33_Tfcontratada_areatrabalhodes",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV141WWContagemResultadoContagensDS_34_Tfcontratada_areatrabalhodes_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV142WWContagemResultadoContagensDS_35_Tfcontagemresultado_datadmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV143WWContagemResultadoContagensDS_36_Tfcontagemresultado_datadmn_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV144WWContagemResultadoContagensDS_37_Tfcontagemresultado_datacnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV145WWContagemResultadoContagensDS_38_Tfcontagemresultado_datacnt_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV146WWContagemResultadoContagensDS_39_Tfcontagemresultado_horacnt",SqlDbType.Char,5,0} ,
          new Object[] {"@AV147WWContagemResultadoContagensDS_40_Tfcontagemresultado_horacnt_sel",SqlDbType.Char,5,0} ,
          new Object[] {"@lV148WWContagemResultadoContagensDS_41_Tfcontagemresultado_osfsosfm",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV149WWContagemResultadoContagensDS_42_Tfcontagemresultado_osfsosfm_sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV150WWContagemResultadoContagensDS_43_Tfcontagemrresultado_sistemasigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV151WWContagemResultadoContagensDS_44_Tfcontagemrresultado_sistemasigla_sel",SqlDbType.Char,25,0} ,
          new Object[] {"@lV152WWContagemResultadoContagensDS_45_Tfcontagemresultado_contadorfmnom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV153WWContagemResultadoContagensDS_46_Tfcontagemresultado_contadorfmnom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@AV155WWContagemResultadoContagensDS_48_Tfcontagemresultadocontagens_esforco",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV156WWContagemResultadoContagensDS_49_Tfcontagemresultadocontagens_esforco_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV157WWContagemResultadoContagensDS_50_Tfcontagemresultado_pfbfs",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV158WWContagemResultadoContagensDS_51_Tfcontagemresultado_pfbfs_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV159WWContagemResultadoContagensDS_52_Tfcontagemresultado_pflfs",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV160WWContagemResultadoContagensDS_53_Tfcontagemresultado_pflfs_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV161WWContagemResultadoContagensDS_54_Tfcontagemresultado_pfbfm",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV162WWContagemResultadoContagensDS_55_Tfcontagemresultado_pfbfm_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV163WWContagemResultadoContagensDS_56_Tfcontagemresultado_pflfm",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV164WWContagemResultadoContagensDS_57_Tfcontagemresultado_pflfm_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV9WWPContext__Userid",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Gx_date",SqlDbType.DateTime,8,0}
          } ;
          Object[] prmP003X5 ;
          prmP003X5 = new Object[] {
          new Object[] {"@AV108WWContagemResultadoContagensDS_1_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV115WWContagemResultadoContagensDS_8_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV115WWContagemResultadoContagensDS_8_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV115WWContagemResultadoContagensDS_8_Contagemresultado_contadorfm1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV118WWContagemResultadoContagensDS_11_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV119WWContagemResultadoContagensDS_12_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV126WWContagemResultadoContagensDS_19_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV126WWContagemResultadoContagensDS_19_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV126WWContagemResultadoContagensDS_19_Contagemresultado_contadorfm2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV129WWContagemResultadoContagensDS_22_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV130WWContagemResultadoContagensDS_23_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV137WWContagemResultadoContagensDS_30_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV137WWContagemResultadoContagensDS_30_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV137WWContagemResultadoContagensDS_30_Contagemresultado_contadorfm3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV110WWContagemResultadoContagensDS_3_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV111WWContagemResultadoContagensDS_4_Contagemresultado_datacnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV112WWContagemResultadoContagensDS_5_Contagemresultado_datacnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV113WWContagemResultadoContagensDS_6_Contagemresultado_datadmn1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV114WWContagemResultadoContagensDS_7_Contagemresultado_datadmn_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV116WWContagemResultadoContagensDS_9_Contagemresultado_statuscnt1",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@AV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV121WWContagemResultadoContagensDS_14_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV122WWContagemResultadoContagensDS_15_Contagemresultado_datacnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV123WWContagemResultadoContagensDS_16_Contagemresultado_datacnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV124WWContagemResultadoContagensDS_17_Contagemresultado_datadmn2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV125WWContagemResultadoContagensDS_18_Contagemresultado_datadmn_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV127WWContagemResultadoContagensDS_20_Contagemresultado_statuscnt2",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@AV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV132WWContagemResultadoContagensDS_25_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV133WWContagemResultadoContagensDS_26_Contagemresultado_datacnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV134WWContagemResultadoContagensDS_27_Contagemresultado_datacnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV135WWContagemResultadoContagensDS_28_Contagemresultado_datadmn3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV136WWContagemResultadoContagensDS_29_Contagemresultado_datadmn_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV138WWContagemResultadoContagensDS_31_Contagemresultado_statuscnt3",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@lV140WWContagemResultadoContagensDS_33_Tfcontratada_areatrabalhodes",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV141WWContagemResultadoContagensDS_34_Tfcontratada_areatrabalhodes_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV142WWContagemResultadoContagensDS_35_Tfcontagemresultado_datadmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV143WWContagemResultadoContagensDS_36_Tfcontagemresultado_datadmn_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV144WWContagemResultadoContagensDS_37_Tfcontagemresultado_datacnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV145WWContagemResultadoContagensDS_38_Tfcontagemresultado_datacnt_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV146WWContagemResultadoContagensDS_39_Tfcontagemresultado_horacnt",SqlDbType.Char,5,0} ,
          new Object[] {"@AV147WWContagemResultadoContagensDS_40_Tfcontagemresultado_horacnt_sel",SqlDbType.Char,5,0} ,
          new Object[] {"@lV148WWContagemResultadoContagensDS_41_Tfcontagemresultado_osfsosfm",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV149WWContagemResultadoContagensDS_42_Tfcontagemresultado_osfsosfm_sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV150WWContagemResultadoContagensDS_43_Tfcontagemrresultado_sistemasigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV151WWContagemResultadoContagensDS_44_Tfcontagemrresultado_sistemasigla_sel",SqlDbType.Char,25,0} ,
          new Object[] {"@lV152WWContagemResultadoContagensDS_45_Tfcontagemresultado_contadorfmnom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV153WWContagemResultadoContagensDS_46_Tfcontagemresultado_contadorfmnom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@AV155WWContagemResultadoContagensDS_48_Tfcontagemresultadocontagens_esforco",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV156WWContagemResultadoContagensDS_49_Tfcontagemresultadocontagens_esforco_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV157WWContagemResultadoContagensDS_50_Tfcontagemresultado_pfbfs",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV158WWContagemResultadoContagensDS_51_Tfcontagemresultado_pfbfs_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV159WWContagemResultadoContagensDS_52_Tfcontagemresultado_pflfs",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV160WWContagemResultadoContagensDS_53_Tfcontagemresultado_pflfs_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV161WWContagemResultadoContagensDS_54_Tfcontagemresultado_pfbfm",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV162WWContagemResultadoContagensDS_55_Tfcontagemresultado_pfbfm_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV163WWContagemResultadoContagensDS_56_Tfcontagemresultado_pflfm",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV164WWContagemResultadoContagensDS_57_Tfcontagemresultado_pflfm_to",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV9WWPContext__Userid",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Gx_date",SqlDbType.DateTime,8,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P003X3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003X3,100,0,true,false )
             ,new CursorDef("P003X5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003X5,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((decimal[]) buf[9])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((decimal[]) buf[11])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((decimal[]) buf[13])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((decimal[]) buf[15])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((short[]) buf[17])[0] = rslt.getShort(10) ;
                ((String[]) buf[18])[0] = rslt.getString(11, 100) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((String[]) buf[20])[0] = rslt.getString(12, 25) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((String[]) buf[22])[0] = rslt.getString(13, 5) ;
                ((String[]) buf[23])[0] = rslt.getVarchar(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((bool[]) buf[25])[0] = rslt.getBool(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((short[]) buf[27])[0] = rslt.getShort(16) ;
                ((int[]) buf[28])[0] = rslt.getInt(17) ;
                ((DateTime[]) buf[29])[0] = rslt.getGXDate(18) ;
                ((DateTime[]) buf[30])[0] = rslt.getGXDate(19) ;
                ((int[]) buf[31])[0] = rslt.getInt(20) ;
                ((int[]) buf[32])[0] = rslt.getInt(21) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(21);
                ((String[]) buf[34])[0] = rslt.getVarchar(22) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(22);
                ((String[]) buf[36])[0] = rslt.getVarchar(23) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(23);
                return;
             case 1 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((decimal[]) buf[5])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((decimal[]) buf[7])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[74]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[75]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[76]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[77]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[78]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[79]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[80]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[81]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[82]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[83]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[84]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[85]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[86]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[87]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[88]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[89]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[90]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[91]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[92]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[93]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[94]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[95]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[96]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[97]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[98]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[99]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[100]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[101]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[102]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[103]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[104]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[105]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[106]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[107]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[108]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[109]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[110]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[111]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[112]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[113]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[114]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[115]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[116]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[117]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[118]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[119]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[120]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[121]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[122]);
                }
                if ( (short)parms[49] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[123]);
                }
                if ( (short)parms[50] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[124]);
                }
                if ( (short)parms[51] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[125]);
                }
                if ( (short)parms[52] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[126]);
                }
                if ( (short)parms[53] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[127]);
                }
                if ( (short)parms[54] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[128]);
                }
                if ( (short)parms[55] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[129]);
                }
                if ( (short)parms[56] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[130]);
                }
                if ( (short)parms[57] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[131]);
                }
                if ( (short)parms[58] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[132]);
                }
                if ( (short)parms[59] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[133]);
                }
                if ( (short)parms[60] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[134]);
                }
                if ( (short)parms[61] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[135]);
                }
                if ( (short)parms[62] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[136]);
                }
                if ( (short)parms[63] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[137]);
                }
                if ( (short)parms[64] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[138]);
                }
                if ( (short)parms[65] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[139]);
                }
                if ( (short)parms[66] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[140]);
                }
                if ( (short)parms[67] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[141]);
                }
                if ( (short)parms[68] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[142]);
                }
                if ( (short)parms[69] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[143]);
                }
                if ( (short)parms[70] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[144]);
                }
                if ( (short)parms[71] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[145]);
                }
                if ( (short)parms[72] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[146]);
                }
                if ( (short)parms[73] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[147]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[74]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[75]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[76]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[77]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[78]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[79]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[80]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[81]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[82]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[83]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[84]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[85]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[86]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[87]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[88]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[89]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[90]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[91]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[92]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[93]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[94]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[95]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[96]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[97]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[98]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[99]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[100]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[101]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[102]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[103]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[104]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[105]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[106]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[107]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[108]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[109]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[110]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[111]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[112]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[113]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[114]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[115]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[116]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[117]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[118]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[119]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[120]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[121]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[122]);
                }
                if ( (short)parms[49] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[123]);
                }
                if ( (short)parms[50] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[124]);
                }
                if ( (short)parms[51] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[125]);
                }
                if ( (short)parms[52] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[126]);
                }
                if ( (short)parms[53] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[127]);
                }
                if ( (short)parms[54] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[128]);
                }
                if ( (short)parms[55] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[129]);
                }
                if ( (short)parms[56] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[130]);
                }
                if ( (short)parms[57] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[131]);
                }
                if ( (short)parms[58] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[132]);
                }
                if ( (short)parms[59] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[133]);
                }
                if ( (short)parms[60] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[134]);
                }
                if ( (short)parms[61] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[135]);
                }
                if ( (short)parms[62] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[136]);
                }
                if ( (short)parms[63] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[137]);
                }
                if ( (short)parms[64] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[138]);
                }
                if ( (short)parms[65] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[139]);
                }
                if ( (short)parms[66] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[140]);
                }
                if ( (short)parms[67] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[141]);
                }
                if ( (short)parms[68] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[142]);
                }
                if ( (short)parms[69] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[143]);
                }
                if ( (short)parms[70] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[144]);
                }
                if ( (short)parms[71] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[145]);
                }
                if ( (short)parms[72] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[146]);
                }
                if ( (short)parms[73] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[147]);
                }
                return;
       }
    }

 }

}
