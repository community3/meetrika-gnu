/*
               File: type_SdtSDT_WSConsultaDemanda
        Description: SDT_WSConsultaDemanda
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 7/10/2019 23:50:0.5
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "ConsultarDemanda" )]
   [XmlType(TypeName =  "ConsultarDemanda" , Namespace = "ConsultarDemanda" )]
   [Serializable]
   public class SdtSDT_WSConsultaDemanda : GxUserType
   {
      public SdtSDT_WSConsultaDemanda( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_WSConsultaDemanda_Username = "";
         gxTv_SdtSDT_WSConsultaDemanda_Userpassword = "";
      }

      public SdtSDT_WSConsultaDemanda( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
            mapper[ "UserName" ] =  "Username" ;
            mapper[ "UserPassword" ] =  "Userpassword" ;
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_WSConsultaDemanda deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "ConsultarDemanda" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_WSConsultaDemanda)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_WSConsultaDemanda obj ;
         obj = this;
         obj.gxTpr_Username = deserialized.gxTpr_Username;
         obj.gxTpr_Userpassword = deserialized.gxTpr_Userpassword;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "UserName") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "UserName") == 0 ) )
               {
                  gxTv_SdtSDT_WSConsultaDemanda_Username = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "UserPassword") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "UserPassword") == 0 ) )
               {
                  gxTv_SdtSDT_WSConsultaDemanda_Userpassword = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "ConsultarDemanda";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "ConsultarDemanda";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("UserName", StringUtil.RTrim( gxTv_SdtSDT_WSConsultaDemanda_Username));
         if ( StringUtil.StrCmp(sNameSpace, "UserName") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "UserName");
         }
         oWriter.WriteElement("UserPassword", StringUtil.RTrim( gxTv_SdtSDT_WSConsultaDemanda_Userpassword));
         if ( StringUtil.StrCmp(sNameSpace, "UserPassword") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "UserPassword");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("UserName", gxTv_SdtSDT_WSConsultaDemanda_Username, false);
         AddObjectProperty("UserPassword", gxTv_SdtSDT_WSConsultaDemanda_Userpassword, false);
         return  ;
      }

      [  SoapElement( ElementName = "UserName" )]
      [  XmlElement( ElementName = "UserName" , Namespace = "UserName"  )]
      public String gxTpr_Username
      {
         get {
            return gxTv_SdtSDT_WSConsultaDemanda_Username ;
         }

         set {
            gxTv_SdtSDT_WSConsultaDemanda_Username = (String)(value);
         }

      }

      [  SoapElement( ElementName = "UserPassword" )]
      [  XmlElement( ElementName = "UserPassword" , Namespace = "UserPassword"  )]
      public String gxTpr_Userpassword
      {
         get {
            return gxTv_SdtSDT_WSConsultaDemanda_Userpassword ;
         }

         set {
            gxTv_SdtSDT_WSConsultaDemanda_Userpassword = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtSDT_WSConsultaDemanda_Username = "";
         gxTv_SdtSDT_WSConsultaDemanda_Userpassword = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String gxTv_SdtSDT_WSConsultaDemanda_Userpassword ;
      protected String sTagName ;
      protected String gxTv_SdtSDT_WSConsultaDemanda_Username ;
   }

   [DataContract(Name = @"SDT_WSConsultaDemanda", Namespace = "ConsultarDemanda")]
   public class SdtSDT_WSConsultaDemanda_RESTInterface : GxGenericCollectionItem<SdtSDT_WSConsultaDemanda>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_WSConsultaDemanda_RESTInterface( ) : base()
      {
      }

      public SdtSDT_WSConsultaDemanda_RESTInterface( SdtSDT_WSConsultaDemanda psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "UserName" , Order = 0 )]
      public String gxTpr_Username
      {
         get {
            return sdt.gxTpr_Username ;
         }

         set {
            sdt.gxTpr_Username = (String)(value);
         }

      }

      [DataMember( Name = "UserPassword" , Order = 1 )]
      public String gxTpr_Userpassword
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Userpassword) ;
         }

         set {
            sdt.gxTpr_Userpassword = (String)(value);
         }

      }

      public SdtSDT_WSConsultaDemanda sdt
      {
         get {
            return (SdtSDT_WSConsultaDemanda)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_WSConsultaDemanda() ;
         }
      }

   }

}
