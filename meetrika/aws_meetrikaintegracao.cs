/*
               File: WS_MeetrikaIntegracao
        Description: Web Services Meetrika
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:9:24.81
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class aws_meetrikaintegracao : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      public override void webExecute( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize();
         if ( ! context.isAjaxRequest( ) )
         {
            GXSoapHTTPResponse.AppendHeader("Content-type", "text/xml;charset=utf-8");
         }
         if ( StringUtil.StrCmp(StringUtil.Lower( GXSoapHTTPRequest.Method), "get") == 0 )
         {
            if ( StringUtil.StrCmp(StringUtil.Lower( GXSoapHTTPRequest.QueryString), "wsdl") == 0 )
            {
               GXSoapXMLWriter.OpenResponse(GXSoapHTTPResponse);
               GXSoapXMLWriter.WriteStartDocument("utf-8", 0);
               GXSoapXMLWriter.WriteStartElement("definitions");
               GXSoapXMLWriter.WriteAttribute("name", "WS_MeetrikaIntegracao");
               GXSoapXMLWriter.WriteAttribute("targetNamespace", "MeetrikaIntegracao");
               GXSoapXMLWriter.WriteAttribute("xmlns:wsdlns", "MeetrikaIntegracao");
               GXSoapXMLWriter.WriteAttribute("xmlns:soap", "http://schemas.xmlsoap.org/wsdl/soap/");
               GXSoapXMLWriter.WriteAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
               GXSoapXMLWriter.WriteAttribute("xmlns", "http://schemas.xmlsoap.org/wsdl/");
               GXSoapXMLWriter.WriteAttribute("xmlns:tns1", "GxEv3Up14_Meetrika");
               GXSoapXMLWriter.WriteAttribute("xmlns:tns", "MeetrikaIntegracao");
               GXSoapXMLWriter.WriteStartElement("types");
               GXSoapXMLWriter.WriteStartElement("schema");
               GXSoapXMLWriter.WriteAttribute("targetNamespace", "GxEv3Up14_Meetrika");
               GXSoapXMLWriter.WriteAttribute("xmlns", "http://www.w3.org/2001/XMLSchema");
               GXSoapXMLWriter.WriteAttribute("xmlns:SOAP-ENC", "http://schemas.xmlsoap.org/soap/encoding/");
               GXSoapXMLWriter.WriteAttribute("elementFormDefault", "qualified");
               GXSoapXMLWriter.WriteElement("import", "");
               GXSoapXMLWriter.WriteAttribute("namespace", "MeetrikaIntegracao");
               GXSoapXMLWriter.WriteStartElement("complexType");
               GXSoapXMLWriter.WriteAttribute("name", "autenticacao");
               GXSoapXMLWriter.WriteStartElement("sequence");
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "usuario");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:string");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "senha");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:string");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("complexType");
               GXSoapXMLWriter.WriteAttribute("name", "SDT_WsFiltros");
               GXSoapXMLWriter.WriteStartElement("sequence");
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "Status");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:string");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "AreaTrabalhoCodigo");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:int");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "AreaTrabalhoNome");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:string");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("schema");
               GXSoapXMLWriter.WriteAttribute("targetNamespace", "MeetrikaIntegracao");
               GXSoapXMLWriter.WriteAttribute("xmlns", "http://www.w3.org/2001/XMLSchema");
               GXSoapXMLWriter.WriteAttribute("xmlns:SOAP-ENC", "http://schemas.xmlsoap.org/soap/encoding/");
               GXSoapXMLWriter.WriteAttribute("elementFormDefault", "qualified");
               GXSoapXMLWriter.WriteElement("import", "");
               GXSoapXMLWriter.WriteAttribute("namespace", "GxEv3Up14_Meetrika");
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "WS_MeetrikaIntegracao.CONSULTARDEMANDAS");
               GXSoapXMLWriter.WriteStartElement("complexType");
               GXSoapXMLWriter.WriteStartElement("sequence");
               GXSoapXMLWriter.WriteElement("element", "");
               GXSoapXMLWriter.WriteAttribute("minOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("maxOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("name", "Sdt_wsautenticacao");
               GXSoapXMLWriter.WriteAttribute("type", "tns1:autenticacao");
               GXSoapXMLWriter.WriteElement("element", "");
               GXSoapXMLWriter.WriteAttribute("minOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("maxOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("name", "Sdt_wsfiltros");
               GXSoapXMLWriter.WriteAttribute("type", "tns1:SDT_WsFiltros");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "WS_MeetrikaIntegracao.CONSULTARDEMANDASResponse");
               GXSoapXMLWriter.WriteStartElement("complexType");
               GXSoapXMLWriter.WriteStartElement("sequence");
               GXSoapXMLWriter.WriteElement("element", "");
               GXSoapXMLWriter.WriteAttribute("minOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("maxOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("name", "Retorno");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:string");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "WS_MeetrikaIntegracao.CONSULTARAUTORIZACAO");
               GXSoapXMLWriter.WriteStartElement("complexType");
               GXSoapXMLWriter.WriteStartElement("sequence");
               GXSoapXMLWriter.WriteElement("element", "");
               GXSoapXMLWriter.WriteAttribute("minOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("maxOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("name", "Sdt_wsautenticacao");
               GXSoapXMLWriter.WriteAttribute("type", "tns1:autenticacao");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("element");
               GXSoapXMLWriter.WriteAttribute("name", "WS_MeetrikaIntegracao.CONSULTARAUTORIZACAOResponse");
               GXSoapXMLWriter.WriteStartElement("complexType");
               GXSoapXMLWriter.WriteStartElement("sequence");
               GXSoapXMLWriter.WriteElement("element", "");
               GXSoapXMLWriter.WriteAttribute("minOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("maxOccurs", "1");
               GXSoapXMLWriter.WriteAttribute("name", "Retorno");
               GXSoapXMLWriter.WriteAttribute("type", "xsd:string");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("message");
               GXSoapXMLWriter.WriteAttribute("name", "WS_MeetrikaIntegracao.CONSULTARDEMANDASSoapIn");
               GXSoapXMLWriter.WriteElement("part", "");
               GXSoapXMLWriter.WriteAttribute("name", "parameters");
               GXSoapXMLWriter.WriteAttribute("element", "tns:WS_MeetrikaIntegracao.CONSULTARDEMANDAS");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("message");
               GXSoapXMLWriter.WriteAttribute("name", "WS_MeetrikaIntegracao.CONSULTARDEMANDASSoapOut");
               GXSoapXMLWriter.WriteElement("part", "");
               GXSoapXMLWriter.WriteAttribute("name", "parameters");
               GXSoapXMLWriter.WriteAttribute("element", "tns:WS_MeetrikaIntegracao.CONSULTARDEMANDASResponse");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("message");
               GXSoapXMLWriter.WriteAttribute("name", "WS_MeetrikaIntegracao.CONSULTARAUTORIZACAOSoapIn");
               GXSoapXMLWriter.WriteElement("part", "");
               GXSoapXMLWriter.WriteAttribute("name", "parameters");
               GXSoapXMLWriter.WriteAttribute("element", "tns:WS_MeetrikaIntegracao.CONSULTARAUTORIZACAO");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("message");
               GXSoapXMLWriter.WriteAttribute("name", "WS_MeetrikaIntegracao.CONSULTARAUTORIZACAOSoapOut");
               GXSoapXMLWriter.WriteElement("part", "");
               GXSoapXMLWriter.WriteAttribute("name", "parameters");
               GXSoapXMLWriter.WriteAttribute("element", "tns:WS_MeetrikaIntegracao.CONSULTARAUTORIZACAOResponse");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("portType");
               GXSoapXMLWriter.WriteAttribute("name", "WS_MeetrikaIntegracaoSoapPort");
               GXSoapXMLWriter.WriteStartElement("operation");
               GXSoapXMLWriter.WriteAttribute("name", "CONSULTARDEMANDAS");
               GXSoapXMLWriter.WriteElement("input", "");
               GXSoapXMLWriter.WriteAttribute("message", "wsdlns:"+"WS_MeetrikaIntegracao.CONSULTARDEMANDASSoapIn");
               GXSoapXMLWriter.WriteElement("output", "");
               GXSoapXMLWriter.WriteAttribute("message", "wsdlns:"+"WS_MeetrikaIntegracao.CONSULTARDEMANDASSoapOut");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("operation");
               GXSoapXMLWriter.WriteAttribute("name", "CONSULTARAUTORIZACAO");
               GXSoapXMLWriter.WriteElement("input", "");
               GXSoapXMLWriter.WriteAttribute("message", "wsdlns:"+"WS_MeetrikaIntegracao.CONSULTARAUTORIZACAOSoapIn");
               GXSoapXMLWriter.WriteElement("output", "");
               GXSoapXMLWriter.WriteAttribute("message", "wsdlns:"+"WS_MeetrikaIntegracao.CONSULTARAUTORIZACAOSoapOut");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("binding");
               GXSoapXMLWriter.WriteAttribute("name", "WS_MeetrikaIntegracaoSoapBinding");
               GXSoapXMLWriter.WriteAttribute("type", "wsdlns:"+"WS_MeetrikaIntegracaoSoapPort");
               GXSoapXMLWriter.WriteElement("soap:binding", "");
               GXSoapXMLWriter.WriteAttribute("style", "document");
               GXSoapXMLWriter.WriteAttribute("transport", "http://schemas.xmlsoap.org/soap/http");
               GXSoapXMLWriter.WriteStartElement("operation");
               GXSoapXMLWriter.WriteAttribute("name", "CONSULTARDEMANDAS");
               GXSoapXMLWriter.WriteElement("soap:operation", "");
               GXSoapXMLWriter.WriteAttribute("soapAction", "MeetrikaIntegracaoaction/"+"AWS_MEETRIKAINTEGRACAO.CONSULTARDEMANDAS");
               GXSoapXMLWriter.WriteStartElement("input");
               GXSoapXMLWriter.WriteElement("soap:body", "");
               GXSoapXMLWriter.WriteAttribute("use", "literal");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("output");
               GXSoapXMLWriter.WriteElement("soap:body", "");
               GXSoapXMLWriter.WriteAttribute("use", "literal");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("operation");
               GXSoapXMLWriter.WriteAttribute("name", "CONSULTARAUTORIZACAO");
               GXSoapXMLWriter.WriteElement("soap:operation", "");
               GXSoapXMLWriter.WriteAttribute("soapAction", "MeetrikaIntegracaoaction/"+"AWS_MEETRIKAINTEGRACAO.CONSULTARAUTORIZACAO");
               GXSoapXMLWriter.WriteStartElement("input");
               GXSoapXMLWriter.WriteElement("soap:body", "");
               GXSoapXMLWriter.WriteAttribute("use", "literal");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("output");
               GXSoapXMLWriter.WriteElement("soap:body", "");
               GXSoapXMLWriter.WriteAttribute("use", "literal");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteStartElement("service");
               GXSoapXMLWriter.WriteAttribute("name", "WS_MeetrikaIntegracao");
               GXSoapXMLWriter.WriteStartElement("port");
               GXSoapXMLWriter.WriteAttribute("name", "WS_MeetrikaIntegracaoSoapPort");
               GXSoapXMLWriter.WriteAttribute("binding", "wsdlns:"+"WS_MeetrikaIntegracaoSoapBinding");
               GXSoapXMLWriter.WriteElement("soap:address", "");
               GXSoapXMLWriter.WriteAttribute("location", "http://"+context.GetServerName( )+((context.GetServerPort( )>0)&&(context.GetServerPort( )!=80)&&(context.GetServerPort( )!=443) ? ":"+StringUtil.LTrim( StringUtil.Str( (decimal)(context.GetServerPort( )), 6, 0)) : "")+context.GetScriptPath( )+"aws_meetrikaintegracao.aspx");
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.WriteEndElement();
               GXSoapXMLWriter.Close();
               return  ;
            }
            else
            {
               currSoapErr = (short)(-20000);
               currSoapErrmsg = "No SOAP request found. Call " + "http://" + context.GetServerName( ) + ((context.GetServerPort( )>0)&&(context.GetServerPort( )!=80)&&(context.GetServerPort( )!=443) ? ":"+StringUtil.LTrim( StringUtil.Str( (decimal)(context.GetServerPort( )), 6, 0)) : "") + context.GetScriptPath( ) + "aws_meetrikaintegracao.aspx" + "?wsdl to get the WSDL.";
            }
         }
         if ( currSoapErr == 0 )
         {
            GXSoapXMLReader.OpenRequest(GXSoapHTTPRequest);
            GXSoapXMLReader.IgnoreComments = 1;
            GXSoapError = GXSoapXMLReader.Read();
            while ( GXSoapError > 0 )
            {
               if ( StringUtil.StringSearch( GXSoapXMLReader.Name, "Body", 1) > 0 )
               {
                  if (true) break;
               }
               GXSoapError = GXSoapXMLReader.Read();
            }
            if ( GXSoapError > 0 )
            {
               GXSoapError = GXSoapXMLReader.Read();
               if ( GXSoapError > 0 )
               {
                  currMethod = GXSoapXMLReader.Name;
                  if ( StringUtil.StringSearch( currMethod+"&", "CONSULTARDEMANDAS&", 1) > 0 )
                  {
                     if ( currSoapErr == 0 )
                     {
                        AV13SDT_WSAutenticacao = new SdtSDT_WSAutenticacao(context);
                        AV12SDT_WsFiltros = new SdtSDT_WsFiltros(context);
                        sTagName = GXSoapXMLReader.Name;
                        if ( GXSoapXMLReader.IsSimple == 0 )
                        {
                           GXSoapError = GXSoapXMLReader.Read();
                           nOutParmCount = 0;
                           while ( ( ( StringUtil.StrCmp(GXSoapXMLReader.Name, sTagName) != 0 ) || ( GXSoapXMLReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
                           {
                              readOk = 0;
                              if ( StringUtil.StrCmp2( GXSoapXMLReader.LocalName, "Sdt_wsautenticacao") )
                              {
                                 if ( AV13SDT_WSAutenticacao == null )
                                 {
                                    AV13SDT_WSAutenticacao = new SdtSDT_WSAutenticacao(context);
                                 }
                                 if ( ( GXSoapXMLReader.IsSimple == 0 ) || ( GXSoapXMLReader.AttributeCount > 0 ) )
                                 {
                                    GXSoapError = AV13SDT_WSAutenticacao.readxml(GXSoapXMLReader, "Sdt_wsautenticacao");
                                 }
                                 if ( GXSoapError > 0 )
                                 {
                                    readOk = 1;
                                 }
                                 GXSoapError = GXSoapXMLReader.Read();
                              }
                              if ( StringUtil.StrCmp2( GXSoapXMLReader.LocalName, "Sdt_wsfiltros") )
                              {
                                 if ( AV12SDT_WsFiltros == null )
                                 {
                                    AV12SDT_WsFiltros = new SdtSDT_WsFiltros(context);
                                 }
                                 if ( ( GXSoapXMLReader.IsSimple == 0 ) || ( GXSoapXMLReader.AttributeCount > 0 ) )
                                 {
                                    GXSoapError = AV12SDT_WsFiltros.readxml(GXSoapXMLReader, "Sdt_wsfiltros");
                                 }
                                 if ( GXSoapError > 0 )
                                 {
                                    readOk = 1;
                                 }
                                 GXSoapError = GXSoapXMLReader.Read();
                              }
                              nOutParmCount = (short)(nOutParmCount+1);
                              if ( readOk == 0 )
                              {
                                 context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                                 context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + GXSoapXMLReader.ReadRawXML();
                                 GXSoapError = (short)(nOutParmCount*-1);
                              }
                           }
                        }
                     }
                  }
                  else if ( StringUtil.StringSearch( currMethod+"&", "CONSULTARAUTORIZACAO&", 1) > 0 )
                  {
                     if ( currSoapErr == 0 )
                     {
                        AV13SDT_WSAutenticacao = new SdtSDT_WSAutenticacao(context);
                        sTagName = GXSoapXMLReader.Name;
                        if ( GXSoapXMLReader.IsSimple == 0 )
                        {
                           GXSoapError = GXSoapXMLReader.Read();
                           nOutParmCount = 0;
                           while ( ( ( StringUtil.StrCmp(GXSoapXMLReader.Name, sTagName) != 0 ) || ( GXSoapXMLReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
                           {
                              readOk = 0;
                              if ( StringUtil.StrCmp2( GXSoapXMLReader.LocalName, "Sdt_wsautenticacao") )
                              {
                                 if ( AV13SDT_WSAutenticacao == null )
                                 {
                                    AV13SDT_WSAutenticacao = new SdtSDT_WSAutenticacao(context);
                                 }
                                 if ( ( GXSoapXMLReader.IsSimple == 0 ) || ( GXSoapXMLReader.AttributeCount > 0 ) )
                                 {
                                    GXSoapError = AV13SDT_WSAutenticacao.readxml(GXSoapXMLReader, "Sdt_wsautenticacao");
                                 }
                                 if ( GXSoapError > 0 )
                                 {
                                    readOk = 1;
                                 }
                                 GXSoapError = GXSoapXMLReader.Read();
                              }
                              nOutParmCount = (short)(nOutParmCount+1);
                              if ( readOk == 0 )
                              {
                                 context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                                 context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + GXSoapXMLReader.ReadRawXML();
                                 GXSoapError = (short)(nOutParmCount*-1);
                              }
                           }
                        }
                     }
                  }
                  else
                  {
                     currSoapErr = (short)(-20002);
                     currSoapErrmsg = "Wrong method called. Expected method: " + "CONSULTARDEMANDAS,CONSULTARAUTORIZACAO";
                  }
               }
            }
            GXSoapXMLReader.Close();
         }
         if ( currSoapErr == 0 )
         {
            if ( GXSoapError < 0 )
            {
               currSoapErr = (short)(GXSoapError*-1);
               currSoapErrmsg = context.sSOAPErrMsg;
            }
            else
            {
               if ( GXSoapXMLReader.ErrCode > 0 )
               {
                  currSoapErr = (short)(GXSoapXMLReader.ErrCode*-1);
                  currSoapErrmsg = GXSoapXMLReader.ErrDescription;
               }
               else
               {
                  if ( GXSoapError == 0 )
                  {
                     currSoapErr = (short)(-20001);
                     currSoapErrmsg = "Malformed SOAP message.";
                  }
                  else
                  {
                     currSoapErr = 0;
                     currSoapErrmsg = "No error.";
                  }
               }
            }
         }
         if ( currSoapErr == 0 )
         {
            if ( StringUtil.StringSearch( currMethod+"&", "CONSULTARDEMANDAS&", 1) > 0 )
            {
               gxep_consultardemandas( ) ;
            }
            else if ( StringUtil.StringSearch( currMethod+"&", "CONSULTARAUTORIZACAO&", 1) > 0 )
            {
               gxep_consultarautorizacao( ) ;
            }
         }
         context.CloseConnections() ;
         GXSoapXMLWriter.OpenResponse(GXSoapHTTPResponse);
         GXSoapXMLWriter.WriteStartDocument("utf-8", 0);
         GXSoapXMLWriter.WriteStartElement("SOAP-ENV:Envelope");
         GXSoapXMLWriter.WriteAttribute("xmlns:SOAP-ENV", "http://schemas.xmlsoap.org/soap/envelope/");
         GXSoapXMLWriter.WriteAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
         GXSoapXMLWriter.WriteAttribute("xmlns:SOAP-ENC", "http://schemas.xmlsoap.org/soap/encoding/");
         GXSoapXMLWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
         if ( StringUtil.StringSearch( currMethod+"&", "CONSULTARDEMANDAS&", 1) > 0 )
         {
            GXSoapXMLWriter.WriteStartElement("SOAP-ENV:Body");
            GXSoapXMLWriter.WriteStartElement("WS_MeetrikaIntegracao.CONSULTARDEMANDASResponse");
            GXSoapXMLWriter.WriteAttribute("xmlns", "MeetrikaIntegracao");
            if ( currSoapErr == 0 )
            {
               GXSoapXMLWriter.WriteElement("Retorno", StringUtil.RTrim( AV10Retorno));
               GXSoapXMLWriter.WriteAttribute("xmlns", "MeetrikaIntegracao");
            }
            else
            {
               GXSoapXMLWriter.WriteStartElement("SOAP-ENV:Fault");
               GXSoapXMLWriter.WriteElement("faultcode", "SOAP-ENV:Client");
               GXSoapXMLWriter.WriteElement("faultstring", currSoapErrmsg);
               GXSoapXMLWriter.WriteElement("detail", StringUtil.Trim( StringUtil.Str( (decimal)(currSoapErr), 10, 0)));
               GXSoapXMLWriter.WriteEndElement();
            }
            GXSoapXMLWriter.WriteEndElement();
            GXSoapXMLWriter.WriteEndElement();
         }
         else if ( StringUtil.StringSearch( currMethod+"&", "CONSULTARAUTORIZACAO&", 1) > 0 )
         {
            GXSoapXMLWriter.WriteStartElement("SOAP-ENV:Body");
            GXSoapXMLWriter.WriteStartElement("WS_MeetrikaIntegracao.CONSULTARAUTORIZACAOResponse");
            GXSoapXMLWriter.WriteAttribute("xmlns", "MeetrikaIntegracao");
            if ( currSoapErr == 0 )
            {
               GXSoapXMLWriter.WriteElement("Retorno", StringUtil.RTrim( AV10Retorno));
               GXSoapXMLWriter.WriteAttribute("xmlns", "MeetrikaIntegracao");
            }
            else
            {
               GXSoapXMLWriter.WriteStartElement("SOAP-ENV:Fault");
               GXSoapXMLWriter.WriteElement("faultcode", "SOAP-ENV:Client");
               GXSoapXMLWriter.WriteElement("faultstring", currSoapErrmsg);
               GXSoapXMLWriter.WriteElement("detail", StringUtil.Trim( StringUtil.Str( (decimal)(currSoapErr), 10, 0)));
               GXSoapXMLWriter.WriteEndElement();
            }
            GXSoapXMLWriter.WriteEndElement();
            GXSoapXMLWriter.WriteEndElement();
         }
         GXSoapXMLWriter.WriteEndElement();
         GXSoapXMLWriter.Close();
         cleanup();
      }

      public aws_meetrikaintegracao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public aws_meetrikaintegracao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         initialize();
         executePrivate();
      }

      public void executeSubmit( )
      {
         aws_meetrikaintegracao objaws_meetrikaintegracao;
         objaws_meetrikaintegracao = new aws_meetrikaintegracao();
         objaws_meetrikaintegracao.context.SetSubmitInitialConfig(context);
         objaws_meetrikaintegracao.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objaws_meetrikaintegracao);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((aws_meetrikaintegracao)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      public void gxep_consultardemandas( )
      {
         /* ConsultarDemandas Constructor */
         /* Execute user subroutine: 'VALIDA.PARAMETROS.CONSULTARDEMANDAS' */
         S111 ();
         if ( returnInSub )
         {
            returnInSub = true;
            this.cleanup();
            if (true) return;
         }
         if ( AV16IsParametros )
         {
            GXt_boolean1 = AV15IsAutenticado;
            new prc_ws_meetrika_autenticacao(context ).execute(  AV13SDT_WSAutenticacao, out  AV10Retorno, out  GXt_boolean1) ;
            AV15IsAutenticado = GXt_boolean1;
            if ( AV15IsAutenticado )
            {
               /* Execute user subroutine: 'BUSCA.DADOS.USUARIO' */
               S161 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  this.cleanup();
                  if (true) return;
               }
               /* Execute user subroutine: 'BUSCA.DADOS.USUARIO.CONTRATANTE' */
               S181 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  this.cleanup();
                  if (true) return;
               }
               /* Execute user subroutine: 'BUSCA.DADOS.USUARIO.CONTRATADA' */
               S171 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  this.cleanup();
                  if (true) return;
               }
               if ( StringUtil.StrCmp(AV12SDT_WsFiltros.gxTpr_Status, "S") == 0 )
               {
                  /* Execute user subroutine: 'LISTA.DEMANDAS.SOLICITADAS' */
                  S211 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     this.cleanup();
                     if (true) return;
                  }
               }
               else if ( StringUtil.StrCmp(AV12SDT_WsFiltros.gxTpr_Status, "A") == 0 )
               {
                  /* Execute user subroutine: 'LISTA.DEMANDAS.EMANALISE' */
                  S221 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     this.cleanup();
                     if (true) return;
                  }
               }
               else if ( StringUtil.StrCmp(AV12SDT_WsFiltros.gxTpr_Status, "E") == 0 )
               {
                  /* Execute user subroutine: 'LISTA.DEMANDAS.EMEXECUCAO' */
                  S231 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     this.cleanup();
                     if (true) return;
                  }
               }
               else if ( StringUtil.StrCmp(AV12SDT_WsFiltros.gxTpr_Status, "B") == 0 )
               {
                  /* Execute user subroutine: 'LISTA.DEMANDAS.EMSTANDBY' */
                  S241 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     this.cleanup();
                     if (true) return;
                  }
               }
               else if ( StringUtil.StrCmp(AV12SDT_WsFiltros.gxTpr_Status, "D") == 0 )
               {
                  /* Execute user subroutine: 'LISTA.DEMANDAS.DIVERGENCIAS' */
                  S251 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     this.cleanup();
                     if (true) return;
                  }
               }
               else if ( StringUtil.StrCmp(AV12SDT_WsFiltros.gxTpr_Status, "Q") == 0 )
               {
                  /* Execute user subroutine: 'LISTA.DEMANDAS.QA' */
                  S261 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     this.cleanup();
                     if (true) return;
                  }
               }
               else if ( StringUtil.StrCmp(AV12SDT_WsFiltros.gxTpr_Status, "I") == 0 )
               {
                  /* Execute user subroutine: 'LISTA.DEMANDAS.ENVIADAS' */
                  S271 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     this.cleanup();
                     if (true) return;
                  }
               }
               else if ( StringUtil.StrCmp(AV12SDT_WsFiltros.gxTpr_Status, "H") == 0 )
               {
                  /* Execute user subroutine: 'LISTA.DEMANDAS.HOMOLOGAR' */
                  S281 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     this.cleanup();
                     if (true) return;
                  }
               }
               else
               {
                  /* Execute user subroutine: 'LISTA.DEMANDAS.SOLICITADAS' */
                  S211 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     this.cleanup();
                     if (true) return;
                  }
                  /* Execute user subroutine: 'LISTA.DEMANDAS.EMANALISE' */
                  S221 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     this.cleanup();
                     if (true) return;
                  }
                  /* Execute user subroutine: 'LISTA.DEMANDAS.EMEXECUCAO' */
                  S231 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     this.cleanup();
                     if (true) return;
                  }
                  /* Execute user subroutine: 'LISTA.DEMANDAS.EMSTANDBY' */
                  S241 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     this.cleanup();
                     if (true) return;
                  }
                  /* Execute user subroutine: 'LISTA.DEMANDAS.DIVERGENCIAS' */
                  S251 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     this.cleanup();
                     if (true) return;
                  }
                  /* Execute user subroutine: 'LISTA.DEMANDAS.QA' */
                  S261 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     this.cleanup();
                     if (true) return;
                  }
                  /* Execute user subroutine: 'LISTA.DEMANDAS.ENVIADAS' */
                  S271 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     this.cleanup();
                     if (true) return;
                  }
                  /* Execute user subroutine: 'LISTA.DEMANDAS.HOMOLOGAR' */
                  S281 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     this.cleanup();
                     if (true) return;
                  }
               }
               if ( ! (0==AV23SDT_WS_Demandas.Count) )
               {
                  AV10Retorno = AV23SDT_WS_Demandas.ToJSonString(false);
               }
               else
               {
                  AV10Retorno = "Nenhuma Demanda foi encontrada.";
               }
            }
         }
         this.cleanup();
      }

      public void gxep_consultarautorizacao( )
      {
         /* ConsultarAutorizacao Constructor */
         /* Execute user subroutine: 'VALIDA.PARAMETROS.CONSULTARAUTORIZACAO' */
         S151 ();
         if ( returnInSub )
         {
            returnInSub = true;
            this.cleanup();
            if (true) return;
         }
         if ( AV16IsParametros )
         {
            GXt_boolean1 = AV15IsAutenticado;
            new prc_ws_meetrika_autenticacao(context ).execute(  AV13SDT_WSAutenticacao, out  AV10Retorno, out  GXt_boolean1) ;
            AV15IsAutenticado = GXt_boolean1;
            if ( AV15IsAutenticado )
            {
               /* Execute user subroutine: 'BUSCA.DADOS.USUARIO' */
               S161 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  this.cleanup();
                  if (true) return;
               }
               /* Execute user subroutine: 'BUSCA.DADOS.CONTRATANTE' */
               S191 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  this.cleanup();
                  if (true) return;
               }
               /* Execute user subroutine: 'BUSCA.DADOS.CONTRATADA' */
               S201 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  this.cleanup();
                  if (true) return;
               }
               if ( ! (0==AV26SDT_WS_ConsultarAutorizacaoUsuario.gxTpr_Pessoa_codigo) )
               {
                  AV10Retorno = AV26SDT_WS_ConsultarAutorizacaoUsuario.ToJSonString(false);
               }
               else
               {
                  AV10Retorno = "Nenhuma informa��o encontrada.";
               }
            }
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'VALIDA.PARAMETROS.CONSULTARDEMANDAS' Routine */
         AV16IsParametros = true;
         /* Execute user subroutine: 'VALIDA.LOGIN' */
         S121 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'VALIDA.FILTRO.STATUS' */
         S131 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'VALIDA.AREA.TRABALHO' */
         S141 ();
         if (returnInSub) return;
      }

      protected void S151( )
      {
         /* 'VALIDA.PARAMETROS.CONSULTARAUTORIZACAO' Routine */
         AV16IsParametros = true;
         /* Execute user subroutine: 'VALIDA.LOGIN' */
         S121 ();
         if (returnInSub) return;
      }

      protected void S121( )
      {
         /* 'VALIDA.LOGIN' Routine */
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13SDT_WSAutenticacao.gxTpr_Usuario)) || String.IsNullOrEmpty(StringUtil.RTrim( AV13SDT_WSAutenticacao.gxTpr_Senha)) )
         {
            AV10Retorno = "Usu�rio/Senha n�o informados." + StringUtil.NewLine( );
            AV16IsParametros = false;
         }
      }

      protected void S161( )
      {
         /* 'BUSCA.DADOS.USUARIO' Routine */
         /* Using cursor P00YB2 */
         pr_default.execute(0, new Object[] {AV10Retorno});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A54Usuario_Ativo = P00YB2_A54Usuario_Ativo[0];
            A341Usuario_UserGamGuid = P00YB2_A341Usuario_UserGamGuid[0];
            A1Usuario_Codigo = P00YB2_A1Usuario_Codigo[0];
            A2Usuario_Nome = P00YB2_A2Usuario_Nome[0];
            n2Usuario_Nome = P00YB2_n2Usuario_Nome[0];
            A57Usuario_PessoaCod = P00YB2_A57Usuario_PessoaCod[0];
            A58Usuario_PessoaNom = P00YB2_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = P00YB2_n58Usuario_PessoaNom[0];
            A58Usuario_PessoaNom = P00YB2_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = P00YB2_n58Usuario_PessoaNom[0];
            AV31wwpContext.gxTpr_Userid = (short)(A1Usuario_Codigo);
            AV31wwpContext.gxTpr_Username = StringUtil.Trim( A2Usuario_Nome);
            AV26SDT_WS_ConsultarAutorizacaoUsuario.gxTpr_Pessoa_codigo = A57Usuario_PessoaCod;
            AV26SDT_WS_ConsultarAutorizacaoUsuario.gxTpr_Pessoa_nome = StringUtil.Trim( A58Usuario_PessoaNom);
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void S171( )
      {
         /* 'BUSCA.DADOS.USUARIO.CONTRATADA' Routine */
         AV54GXLvl175 = 0;
         /* Using cursor P00YB3 */
         pr_default.execute(1, new Object[] {AV31wwpContext.gxTpr_Userid, AV31wwpContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A69ContratadaUsuario_UsuarioCod = P00YB3_A69ContratadaUsuario_UsuarioCod[0];
            A1228ContratadaUsuario_AreaTrabalhoCod = P00YB3_A1228ContratadaUsuario_AreaTrabalhoCod[0];
            n1228ContratadaUsuario_AreaTrabalhoCod = P00YB3_n1228ContratadaUsuario_AreaTrabalhoCod[0];
            A66ContratadaUsuario_ContratadaCod = P00YB3_A66ContratadaUsuario_ContratadaCod[0];
            A1228ContratadaUsuario_AreaTrabalhoCod = P00YB3_A1228ContratadaUsuario_AreaTrabalhoCod[0];
            n1228ContratadaUsuario_AreaTrabalhoCod = P00YB3_n1228ContratadaUsuario_AreaTrabalhoCod[0];
            AV54GXLvl175 = 1;
            AV31wwpContext.gxTpr_Userehcontratada = true;
            AV31wwpContext.gxTpr_Contratada_codigo = A66ContratadaUsuario_ContratadaCod;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(1);
         }
         pr_default.close(1);
         if ( AV54GXLvl175 == 0 )
         {
            AV16IsParametros = false;
            AV10Retorno = AV10Retorno + "Usu�rio n�o configurado para a �rea de Trabalho. Verifique!" + StringUtil.NewLine( );
         }
         if ( AV31wwpContext.gxTpr_Userehcontratada )
         {
            AV49ContratadasGeridasCollection.Clear();
            /* Using cursor P00YB4 */
            pr_default.execute(2, new Object[] {AV31wwpContext.gxTpr_Userid, AV31wwpContext.gxTpr_Areatrabalho_codigo});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A1078ContratoGestor_ContratoCod = P00YB4_A1078ContratoGestor_ContratoCod[0];
               A1079ContratoGestor_UsuarioCod = P00YB4_A1079ContratoGestor_UsuarioCod[0];
               A1446ContratoGestor_ContratadaAreaCod = P00YB4_A1446ContratoGestor_ContratadaAreaCod[0];
               n1446ContratoGestor_ContratadaAreaCod = P00YB4_n1446ContratoGestor_ContratadaAreaCod[0];
               A1136ContratoGestor_ContratadaCod = P00YB4_A1136ContratoGestor_ContratadaCod[0];
               n1136ContratoGestor_ContratadaCod = P00YB4_n1136ContratoGestor_ContratadaCod[0];
               A1446ContratoGestor_ContratadaAreaCod = P00YB4_A1446ContratoGestor_ContratadaAreaCod[0];
               n1446ContratoGestor_ContratadaAreaCod = P00YB4_n1446ContratoGestor_ContratadaAreaCod[0];
               A1136ContratoGestor_ContratadaCod = P00YB4_A1136ContratoGestor_ContratadaCod[0];
               n1136ContratoGestor_ContratadaCod = P00YB4_n1136ContratoGestor_ContratadaCod[0];
               AV49ContratadasGeridasCollection.Add(A1136ContratoGestor_ContratadaCod, 0);
               pr_default.readNext(2);
            }
            pr_default.close(2);
         }
      }

      protected void S181( )
      {
         /* 'BUSCA.DADOS.USUARIO.CONTRATANTE' Routine */
         AV56GXLvl224 = 0;
         /* Using cursor P00YB6 */
         pr_default.execute(3, new Object[] {AV31wwpContext.gxTpr_Userid, AV31wwpContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(3) != 101) )
         {
            A60ContratanteUsuario_UsuarioCod = P00YB6_A60ContratanteUsuario_UsuarioCod[0];
            A63ContratanteUsuario_ContratanteCod = P00YB6_A63ContratanteUsuario_ContratanteCod[0];
            A1020ContratanteUsuario_AreaTrabalhoCod = P00YB6_A1020ContratanteUsuario_AreaTrabalhoCod[0];
            n1020ContratanteUsuario_AreaTrabalhoCod = P00YB6_n1020ContratanteUsuario_AreaTrabalhoCod[0];
            A1020ContratanteUsuario_AreaTrabalhoCod = P00YB6_A1020ContratanteUsuario_AreaTrabalhoCod[0];
            n1020ContratanteUsuario_AreaTrabalhoCod = P00YB6_n1020ContratanteUsuario_AreaTrabalhoCod[0];
            AV56GXLvl224 = 1;
            AV31wwpContext.gxTpr_Userehcontratante = true;
            AV31wwpContext.gxTpr_Contratante_codigo = A63ContratanteUsuario_ContratanteCod;
            pr_default.readNext(3);
         }
         pr_default.close(3);
         if ( AV56GXLvl224 == 0 )
         {
            AV16IsParametros = false;
            AV10Retorno = "Usu�rio n�o configurado para a �rea de Trabalho. Verifique!" + StringUtil.NewLine( );
         }
         if ( AV31wwpContext.gxTpr_Userehcontratante )
         {
            AV49ContratadasGeridasCollection.Clear();
            /* Using cursor P00YB7 */
            pr_default.execute(4, new Object[] {AV31wwpContext.gxTpr_Userid, AV31wwpContext.gxTpr_Areatrabalho_codigo});
            while ( (pr_default.getStatus(4) != 101) )
            {
               A1078ContratoGestor_ContratoCod = P00YB7_A1078ContratoGestor_ContratoCod[0];
               A1079ContratoGestor_UsuarioCod = P00YB7_A1079ContratoGestor_UsuarioCod[0];
               A1446ContratoGestor_ContratadaAreaCod = P00YB7_A1446ContratoGestor_ContratadaAreaCod[0];
               n1446ContratoGestor_ContratadaAreaCod = P00YB7_n1446ContratoGestor_ContratadaAreaCod[0];
               A1136ContratoGestor_ContratadaCod = P00YB7_A1136ContratoGestor_ContratadaCod[0];
               n1136ContratoGestor_ContratadaCod = P00YB7_n1136ContratoGestor_ContratadaCod[0];
               A1446ContratoGestor_ContratadaAreaCod = P00YB7_A1446ContratoGestor_ContratadaAreaCod[0];
               n1446ContratoGestor_ContratadaAreaCod = P00YB7_n1446ContratoGestor_ContratadaAreaCod[0];
               A1136ContratoGestor_ContratadaCod = P00YB7_A1136ContratoGestor_ContratadaCod[0];
               n1136ContratoGestor_ContratadaCod = P00YB7_n1136ContratoGestor_ContratadaCod[0];
               AV49ContratadasGeridasCollection.Add(A1136ContratoGestor_ContratadaCod, 0);
               pr_default.readNext(4);
            }
            pr_default.close(4);
         }
      }

      protected void S191( )
      {
         /* 'BUSCA.DADOS.CONTRATANTE' Routine */
         /* Using cursor P00YB8 */
         pr_default.execute(5, new Object[] {AV26SDT_WS_ConsultarAutorizacaoUsuario.gxTpr_Pessoa_codigo});
         while ( (pr_default.getStatus(5) != 101) )
         {
            A335Contratante_PessoaCod = P00YB8_A335Contratante_PessoaCod[0];
            A5AreaTrabalho_Codigo = P00YB8_A5AreaTrabalho_Codigo[0];
            A6AreaTrabalho_Descricao = P00YB8_A6AreaTrabalho_Descricao[0];
            A29Contratante_Codigo = P00YB8_A29Contratante_Codigo[0];
            n29Contratante_Codigo = P00YB8_n29Contratante_Codigo[0];
            A9Contratante_RazaoSocial = P00YB8_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = P00YB8_n9Contratante_RazaoSocial[0];
            A335Contratante_PessoaCod = P00YB8_A335Contratante_PessoaCod[0];
            A9Contratante_RazaoSocial = P00YB8_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = P00YB8_n9Contratante_RazaoSocial[0];
            AV27SDT_WS_ConsultarAutorizacaoUsuarioArea = new SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho(context);
            AV27SDT_WS_ConsultarAutorizacaoUsuarioArea.gxTpr_Areatrabalho_codigo = A5AreaTrabalho_Codigo;
            AV27SDT_WS_ConsultarAutorizacaoUsuarioArea.gxTpr_Areatrabalho_descricao = StringUtil.Trim( A6AreaTrabalho_Descricao);
            AV27SDT_WS_ConsultarAutorizacaoUsuarioArea.gxTpr_Usuario_codigo = AV29Usuario_Codigo;
            AV27SDT_WS_ConsultarAutorizacaoUsuarioArea.gxTpr_Contratante_codigo = A29Contratante_Codigo;
            AV27SDT_WS_ConsultarAutorizacaoUsuarioArea.gxTpr_Contratante_razaosocial = StringUtil.Trim( A9Contratante_RazaoSocial);
            /* Using cursor P00YB9 */
            pr_default.execute(6, new Object[] {AV27SDT_WS_ConsultarAutorizacaoUsuarioArea.gxTpr_Usuario_codigo, AV27SDT_WS_ConsultarAutorizacaoUsuarioArea.gxTpr_Areatrabalho_codigo});
            while ( (pr_default.getStatus(6) != 101) )
            {
               A7Perfil_AreaTrabalhoCod = P00YB9_A7Perfil_AreaTrabalhoCod[0];
               A1Usuario_Codigo = P00YB9_A1Usuario_Codigo[0];
               A3Perfil_Codigo = P00YB9_A3Perfil_Codigo[0];
               A4Perfil_Nome = P00YB9_A4Perfil_Nome[0];
               A7Perfil_AreaTrabalhoCod = P00YB9_A7Perfil_AreaTrabalhoCod[0];
               A4Perfil_Nome = P00YB9_A4Perfil_Nome[0];
               AV27SDT_WS_ConsultarAutorizacaoUsuarioArea.gxTpr_Perfil_codigo = A3Perfil_Codigo;
               AV27SDT_WS_ConsultarAutorizacaoUsuarioArea.gxTpr_Perfil_nome = StringUtil.Trim( A4Perfil_Nome);
               pr_default.readNext(6);
            }
            pr_default.close(6);
            /* Using cursor P00YB10 */
            pr_default.execute(7, new Object[] {AV27SDT_WS_ConsultarAutorizacaoUsuarioArea.gxTpr_Areatrabalho_codigo});
            while ( (pr_default.getStatus(7) != 101) )
            {
               A75Contrato_AreaTrabalhoCod = P00YB10_A75Contrato_AreaTrabalhoCod[0];
               A74Contrato_Codigo = P00YB10_A74Contrato_Codigo[0];
               A77Contrato_Numero = P00YB10_A77Contrato_Numero[0];
               AV28SDT_WS_ConsultarAutorizacaoUsuarioContrato = new SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato(context);
               AV28SDT_WS_ConsultarAutorizacaoUsuarioContrato.gxTpr_Contrato_codigo = A74Contrato_Codigo;
               AV28SDT_WS_ConsultarAutorizacaoUsuarioContrato.gxTpr_Contrato_numero = StringUtil.Trim( A77Contrato_Numero);
               AV27SDT_WS_ConsultarAutorizacaoUsuarioArea.gxTpr_Contratos.Add(AV28SDT_WS_ConsultarAutorizacaoUsuarioContrato, 0);
               pr_default.readNext(7);
            }
            pr_default.close(7);
            AV26SDT_WS_ConsultarAutorizacaoUsuario.gxTpr_Areatrabalho.Add(AV27SDT_WS_ConsultarAutorizacaoUsuarioArea, 0);
            pr_default.readNext(5);
         }
         pr_default.close(5);
      }

      protected void S201( )
      {
         /* 'BUSCA.DADOS.CONTRATADA' Routine */
         /* Using cursor P00YB11 */
         pr_default.execute(8, new Object[] {AV26SDT_WS_ConsultarAutorizacaoUsuario.gxTpr_Pessoa_codigo});
         while ( (pr_default.getStatus(8) != 101) )
         {
            A40Contratada_PessoaCod = P00YB11_A40Contratada_PessoaCod[0];
            A52Contratada_AreaTrabalhoCod = P00YB11_A52Contratada_AreaTrabalhoCod[0];
            A53Contratada_AreaTrabalhoDes = P00YB11_A53Contratada_AreaTrabalhoDes[0];
            n53Contratada_AreaTrabalhoDes = P00YB11_n53Contratada_AreaTrabalhoDes[0];
            A39Contratada_Codigo = P00YB11_A39Contratada_Codigo[0];
            A41Contratada_PessoaNom = P00YB11_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00YB11_n41Contratada_PessoaNom[0];
            A41Contratada_PessoaNom = P00YB11_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00YB11_n41Contratada_PessoaNom[0];
            A53Contratada_AreaTrabalhoDes = P00YB11_A53Contratada_AreaTrabalhoDes[0];
            n53Contratada_AreaTrabalhoDes = P00YB11_n53Contratada_AreaTrabalhoDes[0];
            AV27SDT_WS_ConsultarAutorizacaoUsuarioArea = new SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho(context);
            AV27SDT_WS_ConsultarAutorizacaoUsuarioArea.gxTpr_Areatrabalho_codigo = A52Contratada_AreaTrabalhoCod;
            AV27SDT_WS_ConsultarAutorizacaoUsuarioArea.gxTpr_Areatrabalho_descricao = StringUtil.Trim( A53Contratada_AreaTrabalhoDes);
            AV27SDT_WS_ConsultarAutorizacaoUsuarioArea.gxTpr_Usuario_codigo = AV29Usuario_Codigo;
            AV27SDT_WS_ConsultarAutorizacaoUsuarioArea.gxTpr_Contratada_codigo = A39Contratada_Codigo;
            AV27SDT_WS_ConsultarAutorizacaoUsuarioArea.gxTpr_Contratada_pessoanom = StringUtil.Trim( A41Contratada_PessoaNom);
            /* Using cursor P00YB12 */
            pr_default.execute(9, new Object[] {AV27SDT_WS_ConsultarAutorizacaoUsuarioArea.gxTpr_Usuario_codigo, AV27SDT_WS_ConsultarAutorizacaoUsuarioArea.gxTpr_Areatrabalho_codigo});
            while ( (pr_default.getStatus(9) != 101) )
            {
               A7Perfil_AreaTrabalhoCod = P00YB12_A7Perfil_AreaTrabalhoCod[0];
               A1Usuario_Codigo = P00YB12_A1Usuario_Codigo[0];
               A3Perfil_Codigo = P00YB12_A3Perfil_Codigo[0];
               A4Perfil_Nome = P00YB12_A4Perfil_Nome[0];
               A7Perfil_AreaTrabalhoCod = P00YB12_A7Perfil_AreaTrabalhoCod[0];
               A4Perfil_Nome = P00YB12_A4Perfil_Nome[0];
               AV27SDT_WS_ConsultarAutorizacaoUsuarioArea.gxTpr_Perfil_codigo = A3Perfil_Codigo;
               AV27SDT_WS_ConsultarAutorizacaoUsuarioArea.gxTpr_Perfil_nome = StringUtil.Trim( A4Perfil_Nome);
               pr_default.readNext(9);
            }
            pr_default.close(9);
            /* Using cursor P00YB13 */
            pr_default.execute(10, new Object[] {AV27SDT_WS_ConsultarAutorizacaoUsuarioArea.gxTpr_Areatrabalho_codigo, AV27SDT_WS_ConsultarAutorizacaoUsuarioArea.gxTpr_Contratada_codigo});
            while ( (pr_default.getStatus(10) != 101) )
            {
               A39Contratada_Codigo = P00YB13_A39Contratada_Codigo[0];
               A75Contrato_AreaTrabalhoCod = P00YB13_A75Contrato_AreaTrabalhoCod[0];
               A74Contrato_Codigo = P00YB13_A74Contrato_Codigo[0];
               A77Contrato_Numero = P00YB13_A77Contrato_Numero[0];
               AV28SDT_WS_ConsultarAutorizacaoUsuarioContrato = new SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato(context);
               AV28SDT_WS_ConsultarAutorizacaoUsuarioContrato.gxTpr_Contrato_codigo = A74Contrato_Codigo;
               AV28SDT_WS_ConsultarAutorizacaoUsuarioContrato.gxTpr_Contrato_numero = StringUtil.Trim( A77Contrato_Numero);
               AV27SDT_WS_ConsultarAutorizacaoUsuarioArea.gxTpr_Contratos.Add(AV28SDT_WS_ConsultarAutorizacaoUsuarioContrato, 0);
               pr_default.readNext(10);
            }
            pr_default.close(10);
            AV26SDT_WS_ConsultarAutorizacaoUsuario.gxTpr_Areatrabalho.Add(AV27SDT_WS_ConsultarAutorizacaoUsuarioArea, 0);
            pr_default.readNext(8);
         }
         pr_default.close(8);
      }

      protected void S131( )
      {
         /* 'VALIDA.FILTRO.STATUS' Routine */
         if ( ! ( StringUtil.StrCmp(AV12SDT_WsFiltros.gxTpr_Status, "S") == 0 ) && ! ( StringUtil.StrCmp(AV12SDT_WsFiltros.gxTpr_Status, "A") == 0 ) && ! ( StringUtil.StrCmp(AV12SDT_WsFiltros.gxTpr_Status, "E") == 0 ) && ! ( StringUtil.StrCmp(AV12SDT_WsFiltros.gxTpr_Status, "B") == 0 ) && ! ( StringUtil.StrCmp(AV12SDT_WsFiltros.gxTpr_Status, "D") == 0 ) && ! ( StringUtil.StrCmp(AV12SDT_WsFiltros.gxTpr_Status, "Q") == 0 ) && ! ( StringUtil.StrCmp(AV12SDT_WsFiltros.gxTpr_Status, "I") == 0 ) && ! ( StringUtil.StrCmp(AV12SDT_WsFiltros.gxTpr_Status, "H") == 0 ) && ! ( StringUtil.StrCmp(AV12SDT_WsFiltros.gxTpr_Status, "T") == 0 ) )
         {
            AV16IsParametros = false;
            AV10Retorno = AV10Retorno + "Status n�o identificado. Varifique!" + StringUtil.NewLine( );
            AV10Retorno = AV10Retorno + "Lista dos Status (Informar apenas a sigla):";
            AV65GXV2 = 1;
            AV64GXV1 = (IGxCollection)(gxdomainfiltrostatusdemandacaixaentrada.getValues());
            while ( AV65GXV2 <= AV64GXV1.Count )
            {
               AV33X = AV64GXV1.GetString(AV65GXV2);
               AV10Retorno = AV10Retorno + StringUtil.Format( "%1 - %2 %3", AV33X, gxdomainfiltrostatusdemandacaixaentrada.getDescription(context,AV33X), " ", "", "", "", "", "", "");
               AV65GXV2 = (int)(AV65GXV2+1);
            }
            AV10Retorno = AV10Retorno + StringUtil.NewLine( );
         }
      }

      protected void S141( )
      {
         /* 'VALIDA.AREA.TRABALHO' Routine */
         if ( (0==AV12SDT_WsFiltros.gxTpr_Areatrabalhocodigo) && String.IsNullOrEmpty(StringUtil.RTrim( StringUtil.Trim( AV12SDT_WsFiltros.gxTpr_Areatrabalhonome))) )
         {
            AV10Retorno = AV10Retorno + "�rea de Trabalho n�o informada. Informe o c�digo e/ou identifica��o da mesma." + StringUtil.NewLine( );
            AV16IsParametros = false;
         }
         else
         {
            AV66GXLvl415 = 0;
            pr_default.dynParam(11, new Object[]{ new Object[]{
                                                 AV12SDT_WsFiltros.gxTpr_Areatrabalhocodigo ,
                                                 AV12SDT_WsFiltros.gxTpr_Areatrabalhonome ,
                                                 A5AreaTrabalho_Codigo ,
                                                 A6AreaTrabalho_Descricao },
                                                 new int[] {
                                                 TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING
                                                 }
            });
            /* Using cursor P00YB14 */
            pr_default.execute(11, new Object[] {AV12SDT_WsFiltros.gxTpr_Areatrabalhocodigo, AV12SDT_WsFiltros.gxTpr_Areatrabalhonome});
            while ( (pr_default.getStatus(11) != 101) )
            {
               A6AreaTrabalho_Descricao = P00YB14_A6AreaTrabalho_Descricao[0];
               A5AreaTrabalho_Codigo = P00YB14_A5AreaTrabalho_Codigo[0];
               A29Contratante_Codigo = P00YB14_A29Contratante_Codigo[0];
               n29Contratante_Codigo = P00YB14_n29Contratante_Codigo[0];
               AV66GXLvl415 = 1;
               AV31wwpContext.gxTpr_Areatrabalho_codigo = A5AreaTrabalho_Codigo;
               AV31wwpContext.gxTpr_Contratante_codigo = A29Contratante_Codigo;
               pr_default.readNext(11);
            }
            pr_default.close(11);
            if ( AV66GXLvl415 == 0 )
            {
               AV10Retorno = AV10Retorno + "�rea de Trabalho n�o identificada. Verifique!" + StringUtil.NewLine( );
               AV16IsParametros = false;
            }
         }
      }

      protected void S211( )
      {
         /* 'LISTA.DEMANDAS.SOLICITADAS' Routine */
         AV22SDT_WS_DemandaItens = new SdtSDT_WS_Demandas(context);
         AV22SDT_WS_DemandaItens.gxTpr_Situacao = gxdomainfiltrostatusdemandacaixaentrada.getDescription(context,"S");
         pr_default.dynParam(12, new Object[]{ new Object[]{
                                              A490ContagemResultado_ContratadaCod ,
                                              AV49ContratadasGeridasCollection ,
                                              AV31wwpContext.gxTpr_Userehcontratada ,
                                              AV31wwpContext.gxTpr_Userehcontratante ,
                                              AV31wwpContext.gxTpr_Contratada_codigo ,
                                              A484ContagemResultado_StatusDmn ,
                                              A508ContagemResultado_Owner ,
                                              AV31wwpContext.gxTpr_Userid ,
                                              A890ContagemResultado_Responsavel },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.SHORT,
                                              TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor P00YB16 */
         pr_default.execute(12, new Object[] {AV31wwpContext.gxTpr_Userid, AV31wwpContext.gxTpr_Userid, AV31wwpContext.gxTpr_Userid, AV31wwpContext.gxTpr_Userid, AV31wwpContext.gxTpr_Contratada_codigo});
         while ( (pr_default.getStatus(12) != 101) )
         {
            A489ContagemResultado_SistemaCod = P00YB16_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00YB16_n489ContagemResultado_SistemaCod[0];
            A1553ContagemResultado_CntSrvCod = P00YB16_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00YB16_n1553ContagemResultado_CntSrvCod[0];
            A601ContagemResultado_Servico = P00YB16_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00YB16_n601ContagemResultado_Servico[0];
            A890ContagemResultado_Responsavel = P00YB16_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = P00YB16_n890ContagemResultado_Responsavel[0];
            A508ContagemResultado_Owner = P00YB16_A508ContagemResultado_Owner[0];
            n508ContagemResultado_Owner = P00YB16_n508ContagemResultado_Owner[0];
            A484ContagemResultado_StatusDmn = P00YB16_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00YB16_n484ContagemResultado_StatusDmn[0];
            A490ContagemResultado_ContratadaCod = P00YB16_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00YB16_n490ContagemResultado_ContratadaCod[0];
            A457ContagemResultado_Demanda = P00YB16_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00YB16_n457ContagemResultado_Demanda[0];
            A493ContagemResultado_DemandaFM = P00YB16_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00YB16_n493ContagemResultado_DemandaFM[0];
            A494ContagemResultado_Descricao = P00YB16_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = P00YB16_n494ContagemResultado_Descricao[0];
            A801ContagemResultado_ServicoSigla = P00YB16_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00YB16_n801ContagemResultado_ServicoSigla[0];
            A471ContagemResultado_DataDmn = P00YB16_A471ContagemResultado_DataDmn[0];
            n471ContagemResultado_DataDmn = P00YB16_n471ContagemResultado_DataDmn[0];
            A1351ContagemResultado_DataPrevista = P00YB16_A1351ContagemResultado_DataPrevista[0];
            n1351ContagemResultado_DataPrevista = P00YB16_n1351ContagemResultado_DataPrevista[0];
            A472ContagemResultado_DataEntrega = P00YB16_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = P00YB16_n472ContagemResultado_DataEntrega[0];
            A509ContagemrResultado_SistemaSigla = P00YB16_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00YB16_n509ContagemrResultado_SistemaSigla[0];
            A1046ContagemResultado_Agrupador = P00YB16_A1046ContagemResultado_Agrupador[0];
            n1046ContagemResultado_Agrupador = P00YB16_n1046ContagemResultado_Agrupador[0];
            A2118ContagemResultado_Owner_Identificao = P00YB16_A2118ContagemResultado_Owner_Identificao[0];
            n2118ContagemResultado_Owner_Identificao = P00YB16_n2118ContagemResultado_Owner_Identificao[0];
            A456ContagemResultado_Codigo = P00YB16_A456ContagemResultado_Codigo[0];
            A509ContagemrResultado_SistemaSigla = P00YB16_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00YB16_n509ContagemrResultado_SistemaSigla[0];
            A601ContagemResultado_Servico = P00YB16_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00YB16_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = P00YB16_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00YB16_n801ContagemResultado_ServicoSigla[0];
            A2118ContagemResultado_Owner_Identificao = P00YB16_A2118ContagemResultado_Owner_Identificao[0];
            n2118ContagemResultado_Owner_Identificao = P00YB16_n2118ContagemResultado_Owner_Identificao[0];
            AV50SDT_WS_DemandasDemandas = new SdtSDT_WS_Demandas_Demanda(context);
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_osreferencia = (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : StringUtil.Trim( A493ContagemResultado_DemandaFM))+(String.IsNullOrEmpty(StringUtil.RTrim( A457ContagemResultado_Demanda)) ? "" : "|"+StringUtil.Trim( A457ContagemResultado_Demanda));
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_descricao = StringUtil.Trim( A494ContagemResultado_Descricao);
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_servicosigla = StringUtil.Trim( A801ContagemResultado_ServicoSigla);
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_statusdmndescricao = gxdomainstatusdemanda.getDescription(context,A484ContagemResultado_StatusDmn);
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_datadmn = A471ContagemResultado_DataDmn;
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_dataprevista = A1351ContagemResultado_DataPrevista;
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_dataentrega = A472ContagemResultado_DataEntrega;
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemrresultado_sistemasigla = StringUtil.Trim( A509ContagemrResultado_SistemaSigla);
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_owner_identificao = StringUtil.Trim( A2118ContagemResultado_Owner_Identificao);
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_agrupador = StringUtil.Trim( A1046ContagemResultado_Agrupador);
            AV22SDT_WS_DemandaItens.gxTpr_Demandas.Add(AV50SDT_WS_DemandasDemandas, 0);
            pr_default.readNext(12);
         }
         pr_default.close(12);
         AV23SDT_WS_Demandas.Add(AV22SDT_WS_DemandaItens, 0);
      }

      protected void S221( )
      {
         /* 'LISTA.DEMANDAS.EMANALISE' Routine */
         AV22SDT_WS_DemandaItens = new SdtSDT_WS_Demandas(context);
         AV22SDT_WS_DemandaItens.gxTpr_Situacao = gxdomainfiltrostatusdemandacaixaentrada.getDescription(context,"A");
         pr_default.dynParam(13, new Object[]{ new Object[]{
                                              A490ContagemResultado_ContratadaCod ,
                                              AV49ContratadasGeridasCollection ,
                                              AV31wwpContext.gxTpr_Userehcontratada ,
                                              AV31wwpContext.gxTpr_Userehcontratante ,
                                              AV31wwpContext.gxTpr_Contratada_codigo ,
                                              A484ContagemResultado_StatusDmn ,
                                              A890ContagemResultado_Responsavel ,
                                              AV31wwpContext.gxTpr_Userid },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.SHORT
                                              }
         });
         /* Using cursor P00YB18 */
         pr_default.execute(13, new Object[] {AV31wwpContext.gxTpr_Userid, AV31wwpContext.gxTpr_Contratada_codigo});
         while ( (pr_default.getStatus(13) != 101) )
         {
            A489ContagemResultado_SistemaCod = P00YB18_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00YB18_n489ContagemResultado_SistemaCod[0];
            A1553ContagemResultado_CntSrvCod = P00YB18_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00YB18_n1553ContagemResultado_CntSrvCod[0];
            A601ContagemResultado_Servico = P00YB18_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00YB18_n601ContagemResultado_Servico[0];
            A508ContagemResultado_Owner = P00YB18_A508ContagemResultado_Owner[0];
            n508ContagemResultado_Owner = P00YB18_n508ContagemResultado_Owner[0];
            A890ContagemResultado_Responsavel = P00YB18_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = P00YB18_n890ContagemResultado_Responsavel[0];
            A484ContagemResultado_StatusDmn = P00YB18_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00YB18_n484ContagemResultado_StatusDmn[0];
            A490ContagemResultado_ContratadaCod = P00YB18_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00YB18_n490ContagemResultado_ContratadaCod[0];
            A457ContagemResultado_Demanda = P00YB18_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00YB18_n457ContagemResultado_Demanda[0];
            A493ContagemResultado_DemandaFM = P00YB18_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00YB18_n493ContagemResultado_DemandaFM[0];
            A494ContagemResultado_Descricao = P00YB18_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = P00YB18_n494ContagemResultado_Descricao[0];
            A801ContagemResultado_ServicoSigla = P00YB18_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00YB18_n801ContagemResultado_ServicoSigla[0];
            A471ContagemResultado_DataDmn = P00YB18_A471ContagemResultado_DataDmn[0];
            n471ContagemResultado_DataDmn = P00YB18_n471ContagemResultado_DataDmn[0];
            A1351ContagemResultado_DataPrevista = P00YB18_A1351ContagemResultado_DataPrevista[0];
            n1351ContagemResultado_DataPrevista = P00YB18_n1351ContagemResultado_DataPrevista[0];
            A472ContagemResultado_DataEntrega = P00YB18_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = P00YB18_n472ContagemResultado_DataEntrega[0];
            A509ContagemrResultado_SistemaSigla = P00YB18_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00YB18_n509ContagemrResultado_SistemaSigla[0];
            A1046ContagemResultado_Agrupador = P00YB18_A1046ContagemResultado_Agrupador[0];
            n1046ContagemResultado_Agrupador = P00YB18_n1046ContagemResultado_Agrupador[0];
            A2118ContagemResultado_Owner_Identificao = P00YB18_A2118ContagemResultado_Owner_Identificao[0];
            n2118ContagemResultado_Owner_Identificao = P00YB18_n2118ContagemResultado_Owner_Identificao[0];
            A456ContagemResultado_Codigo = P00YB18_A456ContagemResultado_Codigo[0];
            A509ContagemrResultado_SistemaSigla = P00YB18_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00YB18_n509ContagemrResultado_SistemaSigla[0];
            A601ContagemResultado_Servico = P00YB18_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00YB18_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = P00YB18_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00YB18_n801ContagemResultado_ServicoSigla[0];
            A2118ContagemResultado_Owner_Identificao = P00YB18_A2118ContagemResultado_Owner_Identificao[0];
            n2118ContagemResultado_Owner_Identificao = P00YB18_n2118ContagemResultado_Owner_Identificao[0];
            AV50SDT_WS_DemandasDemandas = new SdtSDT_WS_Demandas_Demanda(context);
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_osreferencia = (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : StringUtil.Trim( A493ContagemResultado_DemandaFM))+(String.IsNullOrEmpty(StringUtil.RTrim( A457ContagemResultado_Demanda)) ? "" : "|"+StringUtil.Trim( A457ContagemResultado_Demanda));
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_descricao = StringUtil.Trim( A494ContagemResultado_Descricao);
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_servicosigla = StringUtil.Trim( A801ContagemResultado_ServicoSigla);
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_statusdmndescricao = gxdomainstatusdemanda.getDescription(context,A484ContagemResultado_StatusDmn);
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_datadmn = A471ContagemResultado_DataDmn;
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_dataprevista = A1351ContagemResultado_DataPrevista;
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_dataentrega = A472ContagemResultado_DataEntrega;
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemrresultado_sistemasigla = StringUtil.Trim( A509ContagemrResultado_SistemaSigla);
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_owner_identificao = StringUtil.Trim( A2118ContagemResultado_Owner_Identificao);
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_agrupador = StringUtil.Trim( A1046ContagemResultado_Agrupador);
            AV22SDT_WS_DemandaItens.gxTpr_Demandas.Add(AV50SDT_WS_DemandasDemandas, 0);
            pr_default.readNext(13);
         }
         pr_default.close(13);
         AV23SDT_WS_Demandas.Add(AV22SDT_WS_DemandaItens, 0);
      }

      protected void S231( )
      {
         /* 'LISTA.DEMANDAS.EMEXECUCAO' Routine */
         AV22SDT_WS_DemandaItens = new SdtSDT_WS_Demandas(context);
         AV22SDT_WS_DemandaItens.gxTpr_Situacao = gxdomainfiltrostatusdemandacaixaentrada.getDescription(context,"E");
         pr_default.dynParam(14, new Object[]{ new Object[]{
                                              A490ContagemResultado_ContratadaCod ,
                                              AV49ContratadasGeridasCollection ,
                                              AV31wwpContext.gxTpr_Userehcontratada ,
                                              AV31wwpContext.gxTpr_Userehcontratante ,
                                              AV31wwpContext.gxTpr_Contratada_codigo ,
                                              A890ContagemResultado_Responsavel ,
                                              AV31wwpContext.gxTpr_Userid ,
                                              A531ContagemResultado_StatusUltCnt ,
                                              A1236ContagemResultado_ContratanteDoResponsavel ,
                                              AV31wwpContext.gxTpr_Contratante_codigo ,
                                              A484ContagemResultado_StatusDmn },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor P00YB21 */
         pr_default.execute(14, new Object[] {AV31wwpContext.gxTpr_Contratada_codigo, AV31wwpContext.gxTpr_Userid});
         while ( (pr_default.getStatus(14) != 101) )
         {
            A489ContagemResultado_SistemaCod = P00YB21_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00YB21_n489ContagemResultado_SistemaCod[0];
            A1553ContagemResultado_CntSrvCod = P00YB21_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00YB21_n1553ContagemResultado_CntSrvCod[0];
            A601ContagemResultado_Servico = P00YB21_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00YB21_n601ContagemResultado_Servico[0];
            A508ContagemResultado_Owner = P00YB21_A508ContagemResultado_Owner[0];
            n508ContagemResultado_Owner = P00YB21_n508ContagemResultado_Owner[0];
            A484ContagemResultado_StatusDmn = P00YB21_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00YB21_n484ContagemResultado_StatusDmn[0];
            A490ContagemResultado_ContratadaCod = P00YB21_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00YB21_n490ContagemResultado_ContratadaCod[0];
            A457ContagemResultado_Demanda = P00YB21_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00YB21_n457ContagemResultado_Demanda[0];
            A493ContagemResultado_DemandaFM = P00YB21_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00YB21_n493ContagemResultado_DemandaFM[0];
            A494ContagemResultado_Descricao = P00YB21_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = P00YB21_n494ContagemResultado_Descricao[0];
            A801ContagemResultado_ServicoSigla = P00YB21_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00YB21_n801ContagemResultado_ServicoSigla[0];
            A471ContagemResultado_DataDmn = P00YB21_A471ContagemResultado_DataDmn[0];
            n471ContagemResultado_DataDmn = P00YB21_n471ContagemResultado_DataDmn[0];
            A1351ContagemResultado_DataPrevista = P00YB21_A1351ContagemResultado_DataPrevista[0];
            n1351ContagemResultado_DataPrevista = P00YB21_n1351ContagemResultado_DataPrevista[0];
            A472ContagemResultado_DataEntrega = P00YB21_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = P00YB21_n472ContagemResultado_DataEntrega[0];
            A509ContagemrResultado_SistemaSigla = P00YB21_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00YB21_n509ContagemrResultado_SistemaSigla[0];
            A1046ContagemResultado_Agrupador = P00YB21_A1046ContagemResultado_Agrupador[0];
            n1046ContagemResultado_Agrupador = P00YB21_n1046ContagemResultado_Agrupador[0];
            A531ContagemResultado_StatusUltCnt = P00YB21_A531ContagemResultado_StatusUltCnt[0];
            n531ContagemResultado_StatusUltCnt = P00YB21_n531ContagemResultado_StatusUltCnt[0];
            A2118ContagemResultado_Owner_Identificao = P00YB21_A2118ContagemResultado_Owner_Identificao[0];
            n2118ContagemResultado_Owner_Identificao = P00YB21_n2118ContagemResultado_Owner_Identificao[0];
            A890ContagemResultado_Responsavel = P00YB21_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = P00YB21_n890ContagemResultado_Responsavel[0];
            A456ContagemResultado_Codigo = P00YB21_A456ContagemResultado_Codigo[0];
            A509ContagemrResultado_SistemaSigla = P00YB21_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00YB21_n509ContagemrResultado_SistemaSigla[0];
            A601ContagemResultado_Servico = P00YB21_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00YB21_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = P00YB21_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00YB21_n801ContagemResultado_ServicoSigla[0];
            A2118ContagemResultado_Owner_Identificao = P00YB21_A2118ContagemResultado_Owner_Identificao[0];
            n2118ContagemResultado_Owner_Identificao = P00YB21_n2118ContagemResultado_Owner_Identificao[0];
            A531ContagemResultado_StatusUltCnt = P00YB21_A531ContagemResultado_StatusUltCnt[0];
            n531ContagemResultado_StatusUltCnt = P00YB21_n531ContagemResultado_StatusUltCnt[0];
            GXt_int2 = A1236ContagemResultado_ContratanteDoResponsavel;
            new prc_contratantedousuario(context ).execute( ref  A456ContagemResultado_Codigo, ref  A890ContagemResultado_Responsavel, out  GXt_int2) ;
            A1236ContagemResultado_ContratanteDoResponsavel = GXt_int2;
            if ( ! ( AV31wwpContext.gxTpr_Userehcontratante ) || ( ( A1236ContagemResultado_ContratanteDoResponsavel == AV31wwpContext.gxTpr_Contratante_codigo ) ) )
            {
               AV50SDT_WS_DemandasDemandas = new SdtSDT_WS_Demandas_Demanda(context);
               AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_osreferencia = (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : StringUtil.Trim( A493ContagemResultado_DemandaFM))+(String.IsNullOrEmpty(StringUtil.RTrim( A457ContagemResultado_Demanda)) ? "" : "|"+StringUtil.Trim( A457ContagemResultado_Demanda));
               AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_descricao = StringUtil.Trim( A494ContagemResultado_Descricao);
               AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_servicosigla = StringUtil.Trim( A801ContagemResultado_ServicoSigla);
               AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_statusdmndescricao = gxdomainstatusdemanda.getDescription(context,A484ContagemResultado_StatusDmn);
               AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_datadmn = A471ContagemResultado_DataDmn;
               AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_dataprevista = A1351ContagemResultado_DataPrevista;
               AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_dataentrega = A472ContagemResultado_DataEntrega;
               AV50SDT_WS_DemandasDemandas.gxTpr_Contagemrresultado_sistemasigla = StringUtil.Trim( A509ContagemrResultado_SistemaSigla);
               AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_owner_identificao = StringUtil.Trim( A2118ContagemResultado_Owner_Identificao);
               AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_agrupador = StringUtil.Trim( A1046ContagemResultado_Agrupador);
               AV22SDT_WS_DemandaItens.gxTpr_Demandas.Add(AV50SDT_WS_DemandasDemandas, 0);
            }
            pr_default.readNext(14);
         }
         pr_default.close(14);
         AV23SDT_WS_Demandas.Add(AV22SDT_WS_DemandaItens, 0);
      }

      protected void S241( )
      {
         /* 'LISTA.DEMANDAS.EMSTANDBY' Routine */
         AV22SDT_WS_DemandaItens = new SdtSDT_WS_Demandas(context);
         AV22SDT_WS_DemandaItens.gxTpr_Situacao = gxdomainfiltrostatusdemandacaixaentrada.getDescription(context,"B");
         pr_default.dynParam(15, new Object[]{ new Object[]{
                                              A490ContagemResultado_ContratadaCod ,
                                              AV49ContratadasGeridasCollection ,
                                              AV31wwpContext.gxTpr_Userehcontratada ,
                                              AV31wwpContext.gxTpr_Userehcontratante ,
                                              AV31wwpContext.gxTpr_Contratada_codigo ,
                                              A484ContagemResultado_StatusDmn },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor P00YB23 */
         pr_default.execute(15, new Object[] {AV31wwpContext.gxTpr_Contratada_codigo});
         while ( (pr_default.getStatus(15) != 101) )
         {
            A489ContagemResultado_SistemaCod = P00YB23_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00YB23_n489ContagemResultado_SistemaCod[0];
            A1553ContagemResultado_CntSrvCod = P00YB23_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00YB23_n1553ContagemResultado_CntSrvCod[0];
            A601ContagemResultado_Servico = P00YB23_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00YB23_n601ContagemResultado_Servico[0];
            A508ContagemResultado_Owner = P00YB23_A508ContagemResultado_Owner[0];
            n508ContagemResultado_Owner = P00YB23_n508ContagemResultado_Owner[0];
            A484ContagemResultado_StatusDmn = P00YB23_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00YB23_n484ContagemResultado_StatusDmn[0];
            A490ContagemResultado_ContratadaCod = P00YB23_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00YB23_n490ContagemResultado_ContratadaCod[0];
            A457ContagemResultado_Demanda = P00YB23_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00YB23_n457ContagemResultado_Demanda[0];
            A493ContagemResultado_DemandaFM = P00YB23_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00YB23_n493ContagemResultado_DemandaFM[0];
            A494ContagemResultado_Descricao = P00YB23_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = P00YB23_n494ContagemResultado_Descricao[0];
            A801ContagemResultado_ServicoSigla = P00YB23_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00YB23_n801ContagemResultado_ServicoSigla[0];
            A471ContagemResultado_DataDmn = P00YB23_A471ContagemResultado_DataDmn[0];
            n471ContagemResultado_DataDmn = P00YB23_n471ContagemResultado_DataDmn[0];
            A1351ContagemResultado_DataPrevista = P00YB23_A1351ContagemResultado_DataPrevista[0];
            n1351ContagemResultado_DataPrevista = P00YB23_n1351ContagemResultado_DataPrevista[0];
            A472ContagemResultado_DataEntrega = P00YB23_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = P00YB23_n472ContagemResultado_DataEntrega[0];
            A509ContagemrResultado_SistemaSigla = P00YB23_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00YB23_n509ContagemrResultado_SistemaSigla[0];
            A1046ContagemResultado_Agrupador = P00YB23_A1046ContagemResultado_Agrupador[0];
            n1046ContagemResultado_Agrupador = P00YB23_n1046ContagemResultado_Agrupador[0];
            A890ContagemResultado_Responsavel = P00YB23_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = P00YB23_n890ContagemResultado_Responsavel[0];
            A2118ContagemResultado_Owner_Identificao = P00YB23_A2118ContagemResultado_Owner_Identificao[0];
            n2118ContagemResultado_Owner_Identificao = P00YB23_n2118ContagemResultado_Owner_Identificao[0];
            A456ContagemResultado_Codigo = P00YB23_A456ContagemResultado_Codigo[0];
            A509ContagemrResultado_SistemaSigla = P00YB23_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00YB23_n509ContagemrResultado_SistemaSigla[0];
            A601ContagemResultado_Servico = P00YB23_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00YB23_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = P00YB23_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00YB23_n801ContagemResultado_ServicoSigla[0];
            A2118ContagemResultado_Owner_Identificao = P00YB23_A2118ContagemResultado_Owner_Identificao[0];
            n2118ContagemResultado_Owner_Identificao = P00YB23_n2118ContagemResultado_Owner_Identificao[0];
            AV50SDT_WS_DemandasDemandas = new SdtSDT_WS_Demandas_Demanda(context);
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_osreferencia = (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : StringUtil.Trim( A493ContagemResultado_DemandaFM))+(String.IsNullOrEmpty(StringUtil.RTrim( A457ContagemResultado_Demanda)) ? "" : "|"+StringUtil.Trim( A457ContagemResultado_Demanda));
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_descricao = StringUtil.Trim( A494ContagemResultado_Descricao);
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_servicosigla = StringUtil.Trim( A801ContagemResultado_ServicoSigla);
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_statusdmndescricao = gxdomainstatusdemanda.getDescription(context,A484ContagemResultado_StatusDmn);
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_datadmn = A471ContagemResultado_DataDmn;
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_dataprevista = A1351ContagemResultado_DataPrevista;
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_dataentrega = A472ContagemResultado_DataEntrega;
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemrresultado_sistemasigla = StringUtil.Trim( A509ContagemrResultado_SistemaSigla);
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_owner_identificao = StringUtil.Trim( A2118ContagemResultado_Owner_Identificao);
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_agrupador = StringUtil.Trim( A1046ContagemResultado_Agrupador);
            AV22SDT_WS_DemandaItens.gxTpr_Demandas.Add(AV50SDT_WS_DemandasDemandas, 0);
            pr_default.readNext(15);
         }
         pr_default.close(15);
         AV23SDT_WS_Demandas.Add(AV22SDT_WS_DemandaItens, 0);
      }

      protected void S251( )
      {
         /* 'LISTA.DEMANDAS.DIVERGENCIAS' Routine */
         AV22SDT_WS_DemandaItens = new SdtSDT_WS_Demandas(context);
         AV22SDT_WS_DemandaItens.gxTpr_Situacao = gxdomainfiltrostatusdemandacaixaentrada.getDescription(context,"D");
         pr_default.dynParam(16, new Object[]{ new Object[]{
                                              A490ContagemResultado_ContratadaCod ,
                                              AV49ContratadasGeridasCollection ,
                                              AV31wwpContext.gxTpr_Userehcontratada ,
                                              AV31wwpContext.gxTpr_Userehcontratante ,
                                              AV31wwpContext.gxTpr_Contratada_codigo ,
                                              A531ContagemResultado_StatusUltCnt ,
                                              A484ContagemResultado_StatusDmn },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor P00YB26 */
         pr_default.execute(16, new Object[] {AV31wwpContext.gxTpr_Contratada_codigo});
         while ( (pr_default.getStatus(16) != 101) )
         {
            A489ContagemResultado_SistemaCod = P00YB26_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00YB26_n489ContagemResultado_SistemaCod[0];
            A1553ContagemResultado_CntSrvCod = P00YB26_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00YB26_n1553ContagemResultado_CntSrvCod[0];
            A601ContagemResultado_Servico = P00YB26_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00YB26_n601ContagemResultado_Servico[0];
            A456ContagemResultado_Codigo = P00YB26_A456ContagemResultado_Codigo[0];
            A508ContagemResultado_Owner = P00YB26_A508ContagemResultado_Owner[0];
            n508ContagemResultado_Owner = P00YB26_n508ContagemResultado_Owner[0];
            A484ContagemResultado_StatusDmn = P00YB26_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00YB26_n484ContagemResultado_StatusDmn[0];
            A490ContagemResultado_ContratadaCod = P00YB26_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00YB26_n490ContagemResultado_ContratadaCod[0];
            A457ContagemResultado_Demanda = P00YB26_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00YB26_n457ContagemResultado_Demanda[0];
            A493ContagemResultado_DemandaFM = P00YB26_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00YB26_n493ContagemResultado_DemandaFM[0];
            A494ContagemResultado_Descricao = P00YB26_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = P00YB26_n494ContagemResultado_Descricao[0];
            A801ContagemResultado_ServicoSigla = P00YB26_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00YB26_n801ContagemResultado_ServicoSigla[0];
            A471ContagemResultado_DataDmn = P00YB26_A471ContagemResultado_DataDmn[0];
            n471ContagemResultado_DataDmn = P00YB26_n471ContagemResultado_DataDmn[0];
            A1351ContagemResultado_DataPrevista = P00YB26_A1351ContagemResultado_DataPrevista[0];
            n1351ContagemResultado_DataPrevista = P00YB26_n1351ContagemResultado_DataPrevista[0];
            A472ContagemResultado_DataEntrega = P00YB26_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = P00YB26_n472ContagemResultado_DataEntrega[0];
            A509ContagemrResultado_SistemaSigla = P00YB26_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00YB26_n509ContagemrResultado_SistemaSigla[0];
            A1046ContagemResultado_Agrupador = P00YB26_A1046ContagemResultado_Agrupador[0];
            n1046ContagemResultado_Agrupador = P00YB26_n1046ContagemResultado_Agrupador[0];
            A531ContagemResultado_StatusUltCnt = P00YB26_A531ContagemResultado_StatusUltCnt[0];
            n531ContagemResultado_StatusUltCnt = P00YB26_n531ContagemResultado_StatusUltCnt[0];
            A2118ContagemResultado_Owner_Identificao = P00YB26_A2118ContagemResultado_Owner_Identificao[0];
            n2118ContagemResultado_Owner_Identificao = P00YB26_n2118ContagemResultado_Owner_Identificao[0];
            A509ContagemrResultado_SistemaSigla = P00YB26_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00YB26_n509ContagemrResultado_SistemaSigla[0];
            A601ContagemResultado_Servico = P00YB26_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00YB26_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = P00YB26_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00YB26_n801ContagemResultado_ServicoSigla[0];
            A531ContagemResultado_StatusUltCnt = P00YB26_A531ContagemResultado_StatusUltCnt[0];
            n531ContagemResultado_StatusUltCnt = P00YB26_n531ContagemResultado_StatusUltCnt[0];
            A2118ContagemResultado_Owner_Identificao = P00YB26_A2118ContagemResultado_Owner_Identificao[0];
            n2118ContagemResultado_Owner_Identificao = P00YB26_n2118ContagemResultado_Owner_Identificao[0];
            AV50SDT_WS_DemandasDemandas = new SdtSDT_WS_Demandas_Demanda(context);
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_osreferencia = (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : StringUtil.Trim( A493ContagemResultado_DemandaFM))+(String.IsNullOrEmpty(StringUtil.RTrim( A457ContagemResultado_Demanda)) ? "" : "|"+StringUtil.Trim( A457ContagemResultado_Demanda));
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_descricao = StringUtil.Trim( A494ContagemResultado_Descricao);
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_servicosigla = StringUtil.Trim( A801ContagemResultado_ServicoSigla);
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_statusdmndescricao = gxdomainstatusdemanda.getDescription(context,A484ContagemResultado_StatusDmn);
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_datadmn = A471ContagemResultado_DataDmn;
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_dataprevista = A1351ContagemResultado_DataPrevista;
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_dataentrega = A472ContagemResultado_DataEntrega;
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemrresultado_sistemasigla = StringUtil.Trim( A509ContagemrResultado_SistemaSigla);
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_owner_identificao = StringUtil.Trim( A2118ContagemResultado_Owner_Identificao);
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_agrupador = StringUtil.Trim( A1046ContagemResultado_Agrupador);
            AV22SDT_WS_DemandaItens.gxTpr_Demandas.Add(AV50SDT_WS_DemandasDemandas, 0);
            pr_default.readNext(16);
         }
         pr_default.close(16);
         AV23SDT_WS_Demandas.Add(AV22SDT_WS_DemandaItens, 0);
      }

      protected void S261( )
      {
         /* 'LISTA.DEMANDAS.QA' Routine */
         AV22SDT_WS_DemandaItens = new SdtSDT_WS_Demandas(context);
         AV22SDT_WS_DemandaItens.gxTpr_Situacao = gxdomainfiltrostatusdemandacaixaentrada.getDescription(context,"Q");
         pr_default.dynParam(17, new Object[]{ new Object[]{
                                              A490ContagemResultado_ContratadaCod ,
                                              AV49ContratadasGeridasCollection ,
                                              AV31wwpContext.gxTpr_Userehcontratada ,
                                              AV31wwpContext.gxTpr_Userehcontratante ,
                                              AV31wwpContext.gxTpr_Contratada_codigo ,
                                              A1992ContagemResultadoQA_ParaCod ,
                                              AV31wwpContext.gxTpr_Userid ,
                                              A1996ContagemResultadoQA_RespostaDe ,
                                              A1998ContagemResultadoQA_Resposta },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         /* Using cursor P00YB29 */
         pr_default.execute(17, new Object[] {AV31wwpContext.gxTpr_Contratada_codigo, AV31wwpContext.gxTpr_Userid});
         while ( (pr_default.getStatus(17) != 101) )
         {
            A1985ContagemResultadoQA_OSCod = P00YB29_A1985ContagemResultadoQA_OSCod[0];
            A489ContagemResultado_SistemaCod = P00YB29_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00YB29_n489ContagemResultado_SistemaCod[0];
            A1553ContagemResultado_CntSrvCod = P00YB29_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00YB29_n1553ContagemResultado_CntSrvCod[0];
            A601ContagemResultado_Servico = P00YB29_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00YB29_n601ContagemResultado_Servico[0];
            A508ContagemResultado_Owner = P00YB29_A508ContagemResultado_Owner[0];
            n508ContagemResultado_Owner = P00YB29_n508ContagemResultado_Owner[0];
            A1996ContagemResultadoQA_RespostaDe = P00YB29_A1996ContagemResultadoQA_RespostaDe[0];
            n1996ContagemResultadoQA_RespostaDe = P00YB29_n1996ContagemResultadoQA_RespostaDe[0];
            A1992ContagemResultadoQA_ParaCod = P00YB29_A1992ContagemResultadoQA_ParaCod[0];
            A490ContagemResultado_ContratadaCod = P00YB29_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00YB29_n490ContagemResultado_ContratadaCod[0];
            A457ContagemResultado_Demanda = P00YB29_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00YB29_n457ContagemResultado_Demanda[0];
            A493ContagemResultado_DemandaFM = P00YB29_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00YB29_n493ContagemResultado_DemandaFM[0];
            A494ContagemResultado_Descricao = P00YB29_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = P00YB29_n494ContagemResultado_Descricao[0];
            A801ContagemResultado_ServicoSigla = P00YB29_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00YB29_n801ContagemResultado_ServicoSigla[0];
            A484ContagemResultado_StatusDmn = P00YB29_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00YB29_n484ContagemResultado_StatusDmn[0];
            A471ContagemResultado_DataDmn = P00YB29_A471ContagemResultado_DataDmn[0];
            n471ContagemResultado_DataDmn = P00YB29_n471ContagemResultado_DataDmn[0];
            A1351ContagemResultado_DataPrevista = P00YB29_A1351ContagemResultado_DataPrevista[0];
            n1351ContagemResultado_DataPrevista = P00YB29_n1351ContagemResultado_DataPrevista[0];
            A472ContagemResultado_DataEntrega = P00YB29_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = P00YB29_n472ContagemResultado_DataEntrega[0];
            A509ContagemrResultado_SistemaSigla = P00YB29_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00YB29_n509ContagemrResultado_SistemaSigla[0];
            A1046ContagemResultado_Agrupador = P00YB29_A1046ContagemResultado_Agrupador[0];
            n1046ContagemResultado_Agrupador = P00YB29_n1046ContagemResultado_Agrupador[0];
            A2118ContagemResultado_Owner_Identificao = P00YB29_A2118ContagemResultado_Owner_Identificao[0];
            n2118ContagemResultado_Owner_Identificao = P00YB29_n2118ContagemResultado_Owner_Identificao[0];
            A1984ContagemResultadoQA_Codigo = P00YB29_A1984ContagemResultadoQA_Codigo[0];
            A489ContagemResultado_SistemaCod = P00YB29_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00YB29_n489ContagemResultado_SistemaCod[0];
            A1553ContagemResultado_CntSrvCod = P00YB29_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00YB29_n1553ContagemResultado_CntSrvCod[0];
            A508ContagemResultado_Owner = P00YB29_A508ContagemResultado_Owner[0];
            n508ContagemResultado_Owner = P00YB29_n508ContagemResultado_Owner[0];
            A490ContagemResultado_ContratadaCod = P00YB29_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00YB29_n490ContagemResultado_ContratadaCod[0];
            A457ContagemResultado_Demanda = P00YB29_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00YB29_n457ContagemResultado_Demanda[0];
            A493ContagemResultado_DemandaFM = P00YB29_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00YB29_n493ContagemResultado_DemandaFM[0];
            A494ContagemResultado_Descricao = P00YB29_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = P00YB29_n494ContagemResultado_Descricao[0];
            A484ContagemResultado_StatusDmn = P00YB29_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00YB29_n484ContagemResultado_StatusDmn[0];
            A471ContagemResultado_DataDmn = P00YB29_A471ContagemResultado_DataDmn[0];
            n471ContagemResultado_DataDmn = P00YB29_n471ContagemResultado_DataDmn[0];
            A1351ContagemResultado_DataPrevista = P00YB29_A1351ContagemResultado_DataPrevista[0];
            n1351ContagemResultado_DataPrevista = P00YB29_n1351ContagemResultado_DataPrevista[0];
            A472ContagemResultado_DataEntrega = P00YB29_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = P00YB29_n472ContagemResultado_DataEntrega[0];
            A1046ContagemResultado_Agrupador = P00YB29_A1046ContagemResultado_Agrupador[0];
            n1046ContagemResultado_Agrupador = P00YB29_n1046ContagemResultado_Agrupador[0];
            A509ContagemrResultado_SistemaSigla = P00YB29_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00YB29_n509ContagemrResultado_SistemaSigla[0];
            A601ContagemResultado_Servico = P00YB29_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00YB29_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = P00YB29_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00YB29_n801ContagemResultado_ServicoSigla[0];
            A2118ContagemResultado_Owner_Identificao = P00YB29_A2118ContagemResultado_Owner_Identificao[0];
            n2118ContagemResultado_Owner_Identificao = P00YB29_n2118ContagemResultado_Owner_Identificao[0];
            GXt_int2 = A1998ContagemResultadoQA_Resposta;
            new prc_qa_resposta(context ).execute( ref  A1984ContagemResultadoQA_Codigo, out  GXt_int2) ;
            A1998ContagemResultadoQA_Resposta = GXt_int2;
            if ( (0==A1998ContagemResultadoQA_Resposta) )
            {
               AV50SDT_WS_DemandasDemandas = new SdtSDT_WS_Demandas_Demanda(context);
               AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_osreferencia = (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : StringUtil.Trim( A493ContagemResultado_DemandaFM))+(String.IsNullOrEmpty(StringUtil.RTrim( A457ContagemResultado_Demanda)) ? "" : "|"+StringUtil.Trim( A457ContagemResultado_Demanda));
               AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_descricao = StringUtil.Trim( A494ContagemResultado_Descricao);
               AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_servicosigla = StringUtil.Trim( A801ContagemResultado_ServicoSigla);
               AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_statusdmndescricao = gxdomainstatusdemanda.getDescription(context,A484ContagemResultado_StatusDmn);
               AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_datadmn = A471ContagemResultado_DataDmn;
               AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_dataprevista = A1351ContagemResultado_DataPrevista;
               AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_dataentrega = A472ContagemResultado_DataEntrega;
               AV50SDT_WS_DemandasDemandas.gxTpr_Contagemrresultado_sistemasigla = StringUtil.Trim( A509ContagemrResultado_SistemaSigla);
               AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_owner_identificao = StringUtil.Trim( A2118ContagemResultado_Owner_Identificao);
               AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_agrupador = StringUtil.Trim( A1046ContagemResultado_Agrupador);
               AV22SDT_WS_DemandaItens.gxTpr_Demandas.Add(AV50SDT_WS_DemandasDemandas, 0);
            }
            pr_default.readNext(17);
         }
         pr_default.close(17);
         AV23SDT_WS_Demandas.Add(AV22SDT_WS_DemandaItens, 0);
      }

      protected void S271( )
      {
         /* 'LISTA.DEMANDAS.ENVIADAS' Routine */
         AV22SDT_WS_DemandaItens = new SdtSDT_WS_Demandas(context);
         AV22SDT_WS_DemandaItens.gxTpr_Situacao = gxdomainfiltrostatusdemandacaixaentrada.getDescription(context,"I");
         pr_default.dynParam(18, new Object[]{ new Object[]{
                                              A490ContagemResultado_ContratadaCod ,
                                              AV49ContratadasGeridasCollection ,
                                              AV31wwpContext.gxTpr_Userehcontratada ,
                                              AV31wwpContext.gxTpr_Userehcontratante ,
                                              AV31wwpContext.gxTpr_Contratada_codigo ,
                                              A484ContagemResultado_StatusDmn ,
                                              A508ContagemResultado_Owner ,
                                              AV31wwpContext.gxTpr_Userid ,
                                              A1603ContagemResultado_CntCod },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.SHORT,
                                              TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor P00YB31 */
         pr_default.execute(18, new Object[] {AV31wwpContext.gxTpr_Contratada_codigo});
         while ( (pr_default.getStatus(18) != 101) )
         {
            A489ContagemResultado_SistemaCod = P00YB31_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00YB31_n489ContagemResultado_SistemaCod[0];
            A1553ContagemResultado_CntSrvCod = P00YB31_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00YB31_n1553ContagemResultado_CntSrvCod[0];
            A601ContagemResultado_Servico = P00YB31_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00YB31_n601ContagemResultado_Servico[0];
            A1603ContagemResultado_CntCod = P00YB31_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P00YB31_n1603ContagemResultado_CntCod[0];
            A508ContagemResultado_Owner = P00YB31_A508ContagemResultado_Owner[0];
            n508ContagemResultado_Owner = P00YB31_n508ContagemResultado_Owner[0];
            A484ContagemResultado_StatusDmn = P00YB31_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00YB31_n484ContagemResultado_StatusDmn[0];
            A490ContagemResultado_ContratadaCod = P00YB31_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00YB31_n490ContagemResultado_ContratadaCod[0];
            A457ContagemResultado_Demanda = P00YB31_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00YB31_n457ContagemResultado_Demanda[0];
            A493ContagemResultado_DemandaFM = P00YB31_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00YB31_n493ContagemResultado_DemandaFM[0];
            A494ContagemResultado_Descricao = P00YB31_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = P00YB31_n494ContagemResultado_Descricao[0];
            A801ContagemResultado_ServicoSigla = P00YB31_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00YB31_n801ContagemResultado_ServicoSigla[0];
            A471ContagemResultado_DataDmn = P00YB31_A471ContagemResultado_DataDmn[0];
            n471ContagemResultado_DataDmn = P00YB31_n471ContagemResultado_DataDmn[0];
            A1351ContagemResultado_DataPrevista = P00YB31_A1351ContagemResultado_DataPrevista[0];
            n1351ContagemResultado_DataPrevista = P00YB31_n1351ContagemResultado_DataPrevista[0];
            A472ContagemResultado_DataEntrega = P00YB31_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = P00YB31_n472ContagemResultado_DataEntrega[0];
            A509ContagemrResultado_SistemaSigla = P00YB31_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00YB31_n509ContagemrResultado_SistemaSigla[0];
            A1046ContagemResultado_Agrupador = P00YB31_A1046ContagemResultado_Agrupador[0];
            n1046ContagemResultado_Agrupador = P00YB31_n1046ContagemResultado_Agrupador[0];
            A890ContagemResultado_Responsavel = P00YB31_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = P00YB31_n890ContagemResultado_Responsavel[0];
            A2118ContagemResultado_Owner_Identificao = P00YB31_A2118ContagemResultado_Owner_Identificao[0];
            n2118ContagemResultado_Owner_Identificao = P00YB31_n2118ContagemResultado_Owner_Identificao[0];
            A456ContagemResultado_Codigo = P00YB31_A456ContagemResultado_Codigo[0];
            A509ContagemrResultado_SistemaSigla = P00YB31_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00YB31_n509ContagemrResultado_SistemaSigla[0];
            A601ContagemResultado_Servico = P00YB31_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00YB31_n601ContagemResultado_Servico[0];
            A1603ContagemResultado_CntCod = P00YB31_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P00YB31_n1603ContagemResultado_CntCod[0];
            A801ContagemResultado_ServicoSigla = P00YB31_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00YB31_n801ContagemResultado_ServicoSigla[0];
            A2118ContagemResultado_Owner_Identificao = P00YB31_A2118ContagemResultado_Owner_Identificao[0];
            n2118ContagemResultado_Owner_Identificao = P00YB31_n2118ContagemResultado_Owner_Identificao[0];
            if ( ( A508ContagemResultado_Owner == AV31wwpContext.gxTpr_Userid ) || ( ( A508ContagemResultado_Owner != AV31wwpContext.gxTpr_Userid ) && ( new prc_contratocaixaentradaconfiguracoes(context).executeUdp(  A1603ContagemResultado_CntCod) ) ) )
            {
               AV50SDT_WS_DemandasDemandas = new SdtSDT_WS_Demandas_Demanda(context);
               AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_osreferencia = (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : StringUtil.Trim( A493ContagemResultado_DemandaFM))+(String.IsNullOrEmpty(StringUtil.RTrim( A457ContagemResultado_Demanda)) ? "" : "|"+StringUtil.Trim( A457ContagemResultado_Demanda));
               AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_descricao = StringUtil.Trim( A494ContagemResultado_Descricao);
               AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_servicosigla = StringUtil.Trim( A801ContagemResultado_ServicoSigla);
               AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_statusdmndescricao = gxdomainstatusdemanda.getDescription(context,A484ContagemResultado_StatusDmn);
               AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_datadmn = A471ContagemResultado_DataDmn;
               AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_dataprevista = A1351ContagemResultado_DataPrevista;
               AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_dataentrega = A472ContagemResultado_DataEntrega;
               AV50SDT_WS_DemandasDemandas.gxTpr_Contagemrresultado_sistemasigla = StringUtil.Trim( A509ContagemrResultado_SistemaSigla);
               AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_owner_identificao = StringUtil.Trim( A2118ContagemResultado_Owner_Identificao);
               AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_agrupador = StringUtil.Trim( A1046ContagemResultado_Agrupador);
               AV22SDT_WS_DemandaItens.gxTpr_Demandas.Add(AV50SDT_WS_DemandasDemandas, 0);
            }
            pr_default.readNext(18);
         }
         pr_default.close(18);
         AV23SDT_WS_Demandas.Add(AV22SDT_WS_DemandaItens, 0);
      }

      protected void S281( )
      {
         /* 'LISTA.DEMANDAS.HOMOLOGAR' Routine */
         AV22SDT_WS_DemandaItens = new SdtSDT_WS_Demandas(context);
         AV22SDT_WS_DemandaItens.gxTpr_Situacao = gxdomainfiltrostatusdemandacaixaentrada.getDescription(context,"H");
         pr_default.dynParam(19, new Object[]{ new Object[]{
                                              A490ContagemResultado_ContratadaCod ,
                                              AV49ContratadasGeridasCollection ,
                                              AV31wwpContext.gxTpr_Userehcontratada ,
                                              AV31wwpContext.gxTpr_Userehcontratante ,
                                              AV31wwpContext.gxTpr_Contratada_codigo ,
                                              A484ContagemResultado_StatusDmn ,
                                              A1457ContagemResultado_TemDpnHmlg },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor P00YB33 */
         pr_default.execute(19, new Object[] {AV31wwpContext.gxTpr_Contratada_codigo});
         while ( (pr_default.getStatus(19) != 101) )
         {
            A489ContagemResultado_SistemaCod = P00YB33_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00YB33_n489ContagemResultado_SistemaCod[0];
            A1553ContagemResultado_CntSrvCod = P00YB33_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00YB33_n1553ContagemResultado_CntSrvCod[0];
            A601ContagemResultado_Servico = P00YB33_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00YB33_n601ContagemResultado_Servico[0];
            A508ContagemResultado_Owner = P00YB33_A508ContagemResultado_Owner[0];
            n508ContagemResultado_Owner = P00YB33_n508ContagemResultado_Owner[0];
            A1457ContagemResultado_TemDpnHmlg = P00YB33_A1457ContagemResultado_TemDpnHmlg[0];
            n1457ContagemResultado_TemDpnHmlg = P00YB33_n1457ContagemResultado_TemDpnHmlg[0];
            A484ContagemResultado_StatusDmn = P00YB33_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00YB33_n484ContagemResultado_StatusDmn[0];
            A490ContagemResultado_ContratadaCod = P00YB33_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00YB33_n490ContagemResultado_ContratadaCod[0];
            A457ContagemResultado_Demanda = P00YB33_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00YB33_n457ContagemResultado_Demanda[0];
            A493ContagemResultado_DemandaFM = P00YB33_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00YB33_n493ContagemResultado_DemandaFM[0];
            A494ContagemResultado_Descricao = P00YB33_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = P00YB33_n494ContagemResultado_Descricao[0];
            A801ContagemResultado_ServicoSigla = P00YB33_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00YB33_n801ContagemResultado_ServicoSigla[0];
            A471ContagemResultado_DataDmn = P00YB33_A471ContagemResultado_DataDmn[0];
            n471ContagemResultado_DataDmn = P00YB33_n471ContagemResultado_DataDmn[0];
            A1351ContagemResultado_DataPrevista = P00YB33_A1351ContagemResultado_DataPrevista[0];
            n1351ContagemResultado_DataPrevista = P00YB33_n1351ContagemResultado_DataPrevista[0];
            A472ContagemResultado_DataEntrega = P00YB33_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = P00YB33_n472ContagemResultado_DataEntrega[0];
            A509ContagemrResultado_SistemaSigla = P00YB33_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00YB33_n509ContagemrResultado_SistemaSigla[0];
            A1046ContagemResultado_Agrupador = P00YB33_A1046ContagemResultado_Agrupador[0];
            n1046ContagemResultado_Agrupador = P00YB33_n1046ContagemResultado_Agrupador[0];
            A2118ContagemResultado_Owner_Identificao = P00YB33_A2118ContagemResultado_Owner_Identificao[0];
            n2118ContagemResultado_Owner_Identificao = P00YB33_n2118ContagemResultado_Owner_Identificao[0];
            A456ContagemResultado_Codigo = P00YB33_A456ContagemResultado_Codigo[0];
            A509ContagemrResultado_SistemaSigla = P00YB33_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00YB33_n509ContagemrResultado_SistemaSigla[0];
            A601ContagemResultado_Servico = P00YB33_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00YB33_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = P00YB33_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00YB33_n801ContagemResultado_ServicoSigla[0];
            A2118ContagemResultado_Owner_Identificao = P00YB33_A2118ContagemResultado_Owner_Identificao[0];
            n2118ContagemResultado_Owner_Identificao = P00YB33_n2118ContagemResultado_Owner_Identificao[0];
            AV50SDT_WS_DemandasDemandas = new SdtSDT_WS_Demandas_Demanda(context);
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_osreferencia = (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : StringUtil.Trim( A493ContagemResultado_DemandaFM))+(String.IsNullOrEmpty(StringUtil.RTrim( A457ContagemResultado_Demanda)) ? "" : "|"+StringUtil.Trim( A457ContagemResultado_Demanda));
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_descricao = StringUtil.Trim( A494ContagemResultado_Descricao);
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_servicosigla = StringUtil.Trim( A801ContagemResultado_ServicoSigla);
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_statusdmndescricao = gxdomainstatusdemanda.getDescription(context,A484ContagemResultado_StatusDmn);
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_datadmn = A471ContagemResultado_DataDmn;
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_dataprevista = A1351ContagemResultado_DataPrevista;
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_dataentrega = A472ContagemResultado_DataEntrega;
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemrresultado_sistemasigla = StringUtil.Trim( A509ContagemrResultado_SistemaSigla);
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_owner_identificao = StringUtil.Trim( A2118ContagemResultado_Owner_Identificao);
            AV50SDT_WS_DemandasDemandas.gxTpr_Contagemresultado_agrupador = StringUtil.Trim( A1046ContagemResultado_Agrupador);
            AV22SDT_WS_DemandaItens.gxTpr_Demandas.Add(AV50SDT_WS_DemandasDemandas, 0);
            pr_default.readNext(19);
         }
         pr_default.close(19);
         AV23SDT_WS_Demandas.Add(AV22SDT_WS_DemandaItens, 0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXSoapHTTPRequest = new GxHttpRequest(context) ;
         GXSoapXMLReader = new GXXMLReader(context.GetPhysicalPath());
         GXSoapHTTPResponse = new GxHttpResponse(context) ;
         GXSoapXMLWriter = new GXXMLWriter(context.GetPhysicalPath());
         currSoapErrmsg = "";
         currMethod = "";
         AV13SDT_WSAutenticacao = new SdtSDT_WSAutenticacao(context);
         AV12SDT_WsFiltros = new SdtSDT_WsFiltros(context);
         sTagName = "";
         AV10Retorno = "";
         AV23SDT_WS_Demandas = new GxObjectCollection( context, "Demanda", "Demanda", "SdtSDT_WS_Demandas", "GeneXus.Programs");
         AV26SDT_WS_ConsultarAutorizacaoUsuario = new SdtSDT_WS_ConsultarAutorizacaoUsuario(context);
         scmdbuf = "";
         P00YB2_A54Usuario_Ativo = new bool[] {false} ;
         P00YB2_A341Usuario_UserGamGuid = new String[] {""} ;
         P00YB2_A1Usuario_Codigo = new int[1] ;
         P00YB2_A2Usuario_Nome = new String[] {""} ;
         P00YB2_n2Usuario_Nome = new bool[] {false} ;
         P00YB2_A57Usuario_PessoaCod = new int[1] ;
         P00YB2_A58Usuario_PessoaNom = new String[] {""} ;
         P00YB2_n58Usuario_PessoaNom = new bool[] {false} ;
         A341Usuario_UserGamGuid = "";
         A2Usuario_Nome = "";
         A58Usuario_PessoaNom = "";
         AV31wwpContext = new wwpbaseobjects.SdtWWPContext(context);
         P00YB3_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         P00YB3_A1228ContratadaUsuario_AreaTrabalhoCod = new int[1] ;
         P00YB3_n1228ContratadaUsuario_AreaTrabalhoCod = new bool[] {false} ;
         P00YB3_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         AV49ContratadasGeridasCollection = new GxSimpleCollection();
         P00YB4_A1078ContratoGestor_ContratoCod = new int[1] ;
         P00YB4_A1079ContratoGestor_UsuarioCod = new int[1] ;
         P00YB4_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         P00YB4_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         P00YB4_A1136ContratoGestor_ContratadaCod = new int[1] ;
         P00YB4_n1136ContratoGestor_ContratadaCod = new bool[] {false} ;
         P00YB6_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         P00YB6_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         P00YB6_A1020ContratanteUsuario_AreaTrabalhoCod = new int[1] ;
         P00YB6_n1020ContratanteUsuario_AreaTrabalhoCod = new bool[] {false} ;
         P00YB7_A1078ContratoGestor_ContratoCod = new int[1] ;
         P00YB7_A1079ContratoGestor_UsuarioCod = new int[1] ;
         P00YB7_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         P00YB7_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         P00YB7_A1136ContratoGestor_ContratadaCod = new int[1] ;
         P00YB7_n1136ContratoGestor_ContratadaCod = new bool[] {false} ;
         P00YB8_A335Contratante_PessoaCod = new int[1] ;
         P00YB8_A5AreaTrabalho_Codigo = new int[1] ;
         P00YB8_A6AreaTrabalho_Descricao = new String[] {""} ;
         P00YB8_A29Contratante_Codigo = new int[1] ;
         P00YB8_n29Contratante_Codigo = new bool[] {false} ;
         P00YB8_A9Contratante_RazaoSocial = new String[] {""} ;
         P00YB8_n9Contratante_RazaoSocial = new bool[] {false} ;
         A6AreaTrabalho_Descricao = "";
         A9Contratante_RazaoSocial = "";
         AV27SDT_WS_ConsultarAutorizacaoUsuarioArea = new SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho(context);
         P00YB9_A7Perfil_AreaTrabalhoCod = new int[1] ;
         P00YB9_A1Usuario_Codigo = new int[1] ;
         P00YB9_A3Perfil_Codigo = new int[1] ;
         P00YB9_A4Perfil_Nome = new String[] {""} ;
         A4Perfil_Nome = "";
         P00YB10_A75Contrato_AreaTrabalhoCod = new int[1] ;
         P00YB10_A74Contrato_Codigo = new int[1] ;
         P00YB10_A77Contrato_Numero = new String[] {""} ;
         A77Contrato_Numero = "";
         AV28SDT_WS_ConsultarAutorizacaoUsuarioContrato = new SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato(context);
         P00YB11_A40Contratada_PessoaCod = new int[1] ;
         P00YB11_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00YB11_A53Contratada_AreaTrabalhoDes = new String[] {""} ;
         P00YB11_n53Contratada_AreaTrabalhoDes = new bool[] {false} ;
         P00YB11_A39Contratada_Codigo = new int[1] ;
         P00YB11_A41Contratada_PessoaNom = new String[] {""} ;
         P00YB11_n41Contratada_PessoaNom = new bool[] {false} ;
         A53Contratada_AreaTrabalhoDes = "";
         A41Contratada_PessoaNom = "";
         P00YB12_A7Perfil_AreaTrabalhoCod = new int[1] ;
         P00YB12_A1Usuario_Codigo = new int[1] ;
         P00YB12_A3Perfil_Codigo = new int[1] ;
         P00YB12_A4Perfil_Nome = new String[] {""} ;
         P00YB13_A39Contratada_Codigo = new int[1] ;
         P00YB13_A75Contrato_AreaTrabalhoCod = new int[1] ;
         P00YB13_A74Contrato_Codigo = new int[1] ;
         P00YB13_A77Contrato_Numero = new String[] {""} ;
         AV64GXV1 = new GxSimpleCollection();
         AV33X = "";
         P00YB14_A6AreaTrabalho_Descricao = new String[] {""} ;
         P00YB14_A5AreaTrabalho_Codigo = new int[1] ;
         P00YB14_A29Contratante_Codigo = new int[1] ;
         P00YB14_n29Contratante_Codigo = new bool[] {false} ;
         AV22SDT_WS_DemandaItens = new SdtSDT_WS_Demandas(context);
         A484ContagemResultado_StatusDmn = "";
         P00YB16_A489ContagemResultado_SistemaCod = new int[1] ;
         P00YB16_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00YB16_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00YB16_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00YB16_A601ContagemResultado_Servico = new int[1] ;
         P00YB16_n601ContagemResultado_Servico = new bool[] {false} ;
         P00YB16_A890ContagemResultado_Responsavel = new int[1] ;
         P00YB16_n890ContagemResultado_Responsavel = new bool[] {false} ;
         P00YB16_A508ContagemResultado_Owner = new int[1] ;
         P00YB16_n508ContagemResultado_Owner = new bool[] {false} ;
         P00YB16_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00YB16_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00YB16_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00YB16_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00YB16_A457ContagemResultado_Demanda = new String[] {""} ;
         P00YB16_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00YB16_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00YB16_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00YB16_A494ContagemResultado_Descricao = new String[] {""} ;
         P00YB16_n494ContagemResultado_Descricao = new bool[] {false} ;
         P00YB16_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P00YB16_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P00YB16_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00YB16_n471ContagemResultado_DataDmn = new bool[] {false} ;
         P00YB16_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P00YB16_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         P00YB16_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P00YB16_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P00YB16_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P00YB16_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P00YB16_A1046ContagemResultado_Agrupador = new String[] {""} ;
         P00YB16_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         P00YB16_A2118ContagemResultado_Owner_Identificao = new String[] {""} ;
         P00YB16_n2118ContagemResultado_Owner_Identificao = new bool[] {false} ;
         P00YB16_A456ContagemResultado_Codigo = new int[1] ;
         A457ContagemResultado_Demanda = "";
         A493ContagemResultado_DemandaFM = "";
         A494ContagemResultado_Descricao = "";
         A801ContagemResultado_ServicoSigla = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A1351ContagemResultado_DataPrevista = (DateTime)(DateTime.MinValue);
         A472ContagemResultado_DataEntrega = DateTime.MinValue;
         A509ContagemrResultado_SistemaSigla = "";
         A1046ContagemResultado_Agrupador = "";
         A2118ContagemResultado_Owner_Identificao = "";
         AV50SDT_WS_DemandasDemandas = new SdtSDT_WS_Demandas_Demanda(context);
         P00YB18_A489ContagemResultado_SistemaCod = new int[1] ;
         P00YB18_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00YB18_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00YB18_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00YB18_A601ContagemResultado_Servico = new int[1] ;
         P00YB18_n601ContagemResultado_Servico = new bool[] {false} ;
         P00YB18_A508ContagemResultado_Owner = new int[1] ;
         P00YB18_n508ContagemResultado_Owner = new bool[] {false} ;
         P00YB18_A890ContagemResultado_Responsavel = new int[1] ;
         P00YB18_n890ContagemResultado_Responsavel = new bool[] {false} ;
         P00YB18_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00YB18_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00YB18_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00YB18_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00YB18_A457ContagemResultado_Demanda = new String[] {""} ;
         P00YB18_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00YB18_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00YB18_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00YB18_A494ContagemResultado_Descricao = new String[] {""} ;
         P00YB18_n494ContagemResultado_Descricao = new bool[] {false} ;
         P00YB18_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P00YB18_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P00YB18_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00YB18_n471ContagemResultado_DataDmn = new bool[] {false} ;
         P00YB18_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P00YB18_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         P00YB18_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P00YB18_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P00YB18_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P00YB18_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P00YB18_A1046ContagemResultado_Agrupador = new String[] {""} ;
         P00YB18_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         P00YB18_A2118ContagemResultado_Owner_Identificao = new String[] {""} ;
         P00YB18_n2118ContagemResultado_Owner_Identificao = new bool[] {false} ;
         P00YB18_A456ContagemResultado_Codigo = new int[1] ;
         P00YB21_A489ContagemResultado_SistemaCod = new int[1] ;
         P00YB21_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00YB21_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00YB21_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00YB21_A601ContagemResultado_Servico = new int[1] ;
         P00YB21_n601ContagemResultado_Servico = new bool[] {false} ;
         P00YB21_A508ContagemResultado_Owner = new int[1] ;
         P00YB21_n508ContagemResultado_Owner = new bool[] {false} ;
         P00YB21_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00YB21_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00YB21_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00YB21_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00YB21_A457ContagemResultado_Demanda = new String[] {""} ;
         P00YB21_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00YB21_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00YB21_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00YB21_A494ContagemResultado_Descricao = new String[] {""} ;
         P00YB21_n494ContagemResultado_Descricao = new bool[] {false} ;
         P00YB21_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P00YB21_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P00YB21_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00YB21_n471ContagemResultado_DataDmn = new bool[] {false} ;
         P00YB21_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P00YB21_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         P00YB21_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P00YB21_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P00YB21_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P00YB21_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P00YB21_A1046ContagemResultado_Agrupador = new String[] {""} ;
         P00YB21_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         P00YB21_A531ContagemResultado_StatusUltCnt = new short[1] ;
         P00YB21_n531ContagemResultado_StatusUltCnt = new bool[] {false} ;
         P00YB21_A2118ContagemResultado_Owner_Identificao = new String[] {""} ;
         P00YB21_n2118ContagemResultado_Owner_Identificao = new bool[] {false} ;
         P00YB21_A890ContagemResultado_Responsavel = new int[1] ;
         P00YB21_n890ContagemResultado_Responsavel = new bool[] {false} ;
         P00YB21_A456ContagemResultado_Codigo = new int[1] ;
         P00YB23_A489ContagemResultado_SistemaCod = new int[1] ;
         P00YB23_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00YB23_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00YB23_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00YB23_A601ContagemResultado_Servico = new int[1] ;
         P00YB23_n601ContagemResultado_Servico = new bool[] {false} ;
         P00YB23_A508ContagemResultado_Owner = new int[1] ;
         P00YB23_n508ContagemResultado_Owner = new bool[] {false} ;
         P00YB23_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00YB23_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00YB23_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00YB23_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00YB23_A457ContagemResultado_Demanda = new String[] {""} ;
         P00YB23_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00YB23_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00YB23_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00YB23_A494ContagemResultado_Descricao = new String[] {""} ;
         P00YB23_n494ContagemResultado_Descricao = new bool[] {false} ;
         P00YB23_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P00YB23_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P00YB23_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00YB23_n471ContagemResultado_DataDmn = new bool[] {false} ;
         P00YB23_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P00YB23_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         P00YB23_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P00YB23_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P00YB23_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P00YB23_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P00YB23_A1046ContagemResultado_Agrupador = new String[] {""} ;
         P00YB23_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         P00YB23_A890ContagemResultado_Responsavel = new int[1] ;
         P00YB23_n890ContagemResultado_Responsavel = new bool[] {false} ;
         P00YB23_A2118ContagemResultado_Owner_Identificao = new String[] {""} ;
         P00YB23_n2118ContagemResultado_Owner_Identificao = new bool[] {false} ;
         P00YB23_A456ContagemResultado_Codigo = new int[1] ;
         P00YB26_A489ContagemResultado_SistemaCod = new int[1] ;
         P00YB26_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00YB26_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00YB26_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00YB26_A601ContagemResultado_Servico = new int[1] ;
         P00YB26_n601ContagemResultado_Servico = new bool[] {false} ;
         P00YB26_A456ContagemResultado_Codigo = new int[1] ;
         P00YB26_A508ContagemResultado_Owner = new int[1] ;
         P00YB26_n508ContagemResultado_Owner = new bool[] {false} ;
         P00YB26_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00YB26_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00YB26_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00YB26_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00YB26_A457ContagemResultado_Demanda = new String[] {""} ;
         P00YB26_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00YB26_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00YB26_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00YB26_A494ContagemResultado_Descricao = new String[] {""} ;
         P00YB26_n494ContagemResultado_Descricao = new bool[] {false} ;
         P00YB26_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P00YB26_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P00YB26_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00YB26_n471ContagemResultado_DataDmn = new bool[] {false} ;
         P00YB26_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P00YB26_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         P00YB26_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P00YB26_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P00YB26_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P00YB26_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P00YB26_A1046ContagemResultado_Agrupador = new String[] {""} ;
         P00YB26_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         P00YB26_A531ContagemResultado_StatusUltCnt = new short[1] ;
         P00YB26_n531ContagemResultado_StatusUltCnt = new bool[] {false} ;
         P00YB26_A2118ContagemResultado_Owner_Identificao = new String[] {""} ;
         P00YB26_n2118ContagemResultado_Owner_Identificao = new bool[] {false} ;
         P00YB29_A1985ContagemResultadoQA_OSCod = new int[1] ;
         P00YB29_A489ContagemResultado_SistemaCod = new int[1] ;
         P00YB29_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00YB29_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00YB29_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00YB29_A601ContagemResultado_Servico = new int[1] ;
         P00YB29_n601ContagemResultado_Servico = new bool[] {false} ;
         P00YB29_A508ContagemResultado_Owner = new int[1] ;
         P00YB29_n508ContagemResultado_Owner = new bool[] {false} ;
         P00YB29_A1996ContagemResultadoQA_RespostaDe = new int[1] ;
         P00YB29_n1996ContagemResultadoQA_RespostaDe = new bool[] {false} ;
         P00YB29_A1992ContagemResultadoQA_ParaCod = new int[1] ;
         P00YB29_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00YB29_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00YB29_A457ContagemResultado_Demanda = new String[] {""} ;
         P00YB29_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00YB29_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00YB29_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00YB29_A494ContagemResultado_Descricao = new String[] {""} ;
         P00YB29_n494ContagemResultado_Descricao = new bool[] {false} ;
         P00YB29_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P00YB29_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P00YB29_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00YB29_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00YB29_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00YB29_n471ContagemResultado_DataDmn = new bool[] {false} ;
         P00YB29_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P00YB29_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         P00YB29_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P00YB29_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P00YB29_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P00YB29_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P00YB29_A1046ContagemResultado_Agrupador = new String[] {""} ;
         P00YB29_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         P00YB29_A2118ContagemResultado_Owner_Identificao = new String[] {""} ;
         P00YB29_n2118ContagemResultado_Owner_Identificao = new bool[] {false} ;
         P00YB29_A1984ContagemResultadoQA_Codigo = new int[1] ;
         P00YB31_A489ContagemResultado_SistemaCod = new int[1] ;
         P00YB31_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00YB31_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00YB31_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00YB31_A601ContagemResultado_Servico = new int[1] ;
         P00YB31_n601ContagemResultado_Servico = new bool[] {false} ;
         P00YB31_A1603ContagemResultado_CntCod = new int[1] ;
         P00YB31_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P00YB31_A508ContagemResultado_Owner = new int[1] ;
         P00YB31_n508ContagemResultado_Owner = new bool[] {false} ;
         P00YB31_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00YB31_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00YB31_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00YB31_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00YB31_A457ContagemResultado_Demanda = new String[] {""} ;
         P00YB31_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00YB31_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00YB31_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00YB31_A494ContagemResultado_Descricao = new String[] {""} ;
         P00YB31_n494ContagemResultado_Descricao = new bool[] {false} ;
         P00YB31_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P00YB31_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P00YB31_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00YB31_n471ContagemResultado_DataDmn = new bool[] {false} ;
         P00YB31_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P00YB31_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         P00YB31_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P00YB31_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P00YB31_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P00YB31_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P00YB31_A1046ContagemResultado_Agrupador = new String[] {""} ;
         P00YB31_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         P00YB31_A890ContagemResultado_Responsavel = new int[1] ;
         P00YB31_n890ContagemResultado_Responsavel = new bool[] {false} ;
         P00YB31_A2118ContagemResultado_Owner_Identificao = new String[] {""} ;
         P00YB31_n2118ContagemResultado_Owner_Identificao = new bool[] {false} ;
         P00YB31_A456ContagemResultado_Codigo = new int[1] ;
         P00YB33_A489ContagemResultado_SistemaCod = new int[1] ;
         P00YB33_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00YB33_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00YB33_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00YB33_A601ContagemResultado_Servico = new int[1] ;
         P00YB33_n601ContagemResultado_Servico = new bool[] {false} ;
         P00YB33_A508ContagemResultado_Owner = new int[1] ;
         P00YB33_n508ContagemResultado_Owner = new bool[] {false} ;
         P00YB33_A1457ContagemResultado_TemDpnHmlg = new bool[] {false} ;
         P00YB33_n1457ContagemResultado_TemDpnHmlg = new bool[] {false} ;
         P00YB33_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00YB33_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00YB33_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00YB33_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00YB33_A457ContagemResultado_Demanda = new String[] {""} ;
         P00YB33_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00YB33_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00YB33_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00YB33_A494ContagemResultado_Descricao = new String[] {""} ;
         P00YB33_n494ContagemResultado_Descricao = new bool[] {false} ;
         P00YB33_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P00YB33_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P00YB33_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00YB33_n471ContagemResultado_DataDmn = new bool[] {false} ;
         P00YB33_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         P00YB33_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         P00YB33_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P00YB33_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P00YB33_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P00YB33_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P00YB33_A1046ContagemResultado_Agrupador = new String[] {""} ;
         P00YB33_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         P00YB33_A2118ContagemResultado_Owner_Identificao = new String[] {""} ;
         P00YB33_n2118ContagemResultado_Owner_Identificao = new bool[] {false} ;
         P00YB33_A456ContagemResultado_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.aws_meetrikaintegracao__default(),
            new Object[][] {
                new Object[] {
               P00YB2_A54Usuario_Ativo, P00YB2_A341Usuario_UserGamGuid, P00YB2_A1Usuario_Codigo, P00YB2_A2Usuario_Nome, P00YB2_n2Usuario_Nome, P00YB2_A57Usuario_PessoaCod, P00YB2_A58Usuario_PessoaNom, P00YB2_n58Usuario_PessoaNom
               }
               , new Object[] {
               P00YB3_A69ContratadaUsuario_UsuarioCod, P00YB3_A1228ContratadaUsuario_AreaTrabalhoCod, P00YB3_n1228ContratadaUsuario_AreaTrabalhoCod, P00YB3_A66ContratadaUsuario_ContratadaCod
               }
               , new Object[] {
               P00YB4_A1078ContratoGestor_ContratoCod, P00YB4_A1079ContratoGestor_UsuarioCod, P00YB4_A1446ContratoGestor_ContratadaAreaCod, P00YB4_n1446ContratoGestor_ContratadaAreaCod, P00YB4_A1136ContratoGestor_ContratadaCod, P00YB4_n1136ContratoGestor_ContratadaCod
               }
               , new Object[] {
               P00YB6_A60ContratanteUsuario_UsuarioCod, P00YB6_A63ContratanteUsuario_ContratanteCod, P00YB6_A1020ContratanteUsuario_AreaTrabalhoCod, P00YB6_n1020ContratanteUsuario_AreaTrabalhoCod
               }
               , new Object[] {
               P00YB7_A1078ContratoGestor_ContratoCod, P00YB7_A1079ContratoGestor_UsuarioCod, P00YB7_A1446ContratoGestor_ContratadaAreaCod, P00YB7_n1446ContratoGestor_ContratadaAreaCod, P00YB7_A1136ContratoGestor_ContratadaCod, P00YB7_n1136ContratoGestor_ContratadaCod
               }
               , new Object[] {
               P00YB8_A335Contratante_PessoaCod, P00YB8_A5AreaTrabalho_Codigo, P00YB8_A6AreaTrabalho_Descricao, P00YB8_A29Contratante_Codigo, P00YB8_n29Contratante_Codigo, P00YB8_A9Contratante_RazaoSocial, P00YB8_n9Contratante_RazaoSocial
               }
               , new Object[] {
               P00YB9_A7Perfil_AreaTrabalhoCod, P00YB9_A1Usuario_Codigo, P00YB9_A3Perfil_Codigo, P00YB9_A4Perfil_Nome
               }
               , new Object[] {
               P00YB10_A75Contrato_AreaTrabalhoCod, P00YB10_A74Contrato_Codigo, P00YB10_A77Contrato_Numero
               }
               , new Object[] {
               P00YB11_A40Contratada_PessoaCod, P00YB11_A52Contratada_AreaTrabalhoCod, P00YB11_A53Contratada_AreaTrabalhoDes, P00YB11_n53Contratada_AreaTrabalhoDes, P00YB11_A39Contratada_Codigo, P00YB11_A41Contratada_PessoaNom, P00YB11_n41Contratada_PessoaNom
               }
               , new Object[] {
               P00YB12_A7Perfil_AreaTrabalhoCod, P00YB12_A1Usuario_Codigo, P00YB12_A3Perfil_Codigo, P00YB12_A4Perfil_Nome
               }
               , new Object[] {
               P00YB13_A39Contratada_Codigo, P00YB13_A75Contrato_AreaTrabalhoCod, P00YB13_A74Contrato_Codigo, P00YB13_A77Contrato_Numero
               }
               , new Object[] {
               P00YB14_A6AreaTrabalho_Descricao, P00YB14_A5AreaTrabalho_Codigo, P00YB14_A29Contratante_Codigo, P00YB14_n29Contratante_Codigo
               }
               , new Object[] {
               P00YB16_A489ContagemResultado_SistemaCod, P00YB16_n489ContagemResultado_SistemaCod, P00YB16_A1553ContagemResultado_CntSrvCod, P00YB16_n1553ContagemResultado_CntSrvCod, P00YB16_A601ContagemResultado_Servico, P00YB16_n601ContagemResultado_Servico, P00YB16_A890ContagemResultado_Responsavel, P00YB16_n890ContagemResultado_Responsavel, P00YB16_A508ContagemResultado_Owner, P00YB16_A484ContagemResultado_StatusDmn,
               P00YB16_n484ContagemResultado_StatusDmn, P00YB16_A490ContagemResultado_ContratadaCod, P00YB16_n490ContagemResultado_ContratadaCod, P00YB16_A457ContagemResultado_Demanda, P00YB16_n457ContagemResultado_Demanda, P00YB16_A493ContagemResultado_DemandaFM, P00YB16_n493ContagemResultado_DemandaFM, P00YB16_A494ContagemResultado_Descricao, P00YB16_n494ContagemResultado_Descricao, P00YB16_A801ContagemResultado_ServicoSigla,
               P00YB16_n801ContagemResultado_ServicoSigla, P00YB16_A471ContagemResultado_DataDmn, P00YB16_A1351ContagemResultado_DataPrevista, P00YB16_n1351ContagemResultado_DataPrevista, P00YB16_A472ContagemResultado_DataEntrega, P00YB16_n472ContagemResultado_DataEntrega, P00YB16_A509ContagemrResultado_SistemaSigla, P00YB16_n509ContagemrResultado_SistemaSigla, P00YB16_A1046ContagemResultado_Agrupador, P00YB16_n1046ContagemResultado_Agrupador,
               P00YB16_A2118ContagemResultado_Owner_Identificao, P00YB16_n2118ContagemResultado_Owner_Identificao, P00YB16_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P00YB18_A489ContagemResultado_SistemaCod, P00YB18_n489ContagemResultado_SistemaCod, P00YB18_A1553ContagemResultado_CntSrvCod, P00YB18_n1553ContagemResultado_CntSrvCod, P00YB18_A601ContagemResultado_Servico, P00YB18_n601ContagemResultado_Servico, P00YB18_A508ContagemResultado_Owner, P00YB18_A890ContagemResultado_Responsavel, P00YB18_n890ContagemResultado_Responsavel, P00YB18_A484ContagemResultado_StatusDmn,
               P00YB18_n484ContagemResultado_StatusDmn, P00YB18_A490ContagemResultado_ContratadaCod, P00YB18_n490ContagemResultado_ContratadaCod, P00YB18_A457ContagemResultado_Demanda, P00YB18_n457ContagemResultado_Demanda, P00YB18_A493ContagemResultado_DemandaFM, P00YB18_n493ContagemResultado_DemandaFM, P00YB18_A494ContagemResultado_Descricao, P00YB18_n494ContagemResultado_Descricao, P00YB18_A801ContagemResultado_ServicoSigla,
               P00YB18_n801ContagemResultado_ServicoSigla, P00YB18_A471ContagemResultado_DataDmn, P00YB18_A1351ContagemResultado_DataPrevista, P00YB18_n1351ContagemResultado_DataPrevista, P00YB18_A472ContagemResultado_DataEntrega, P00YB18_n472ContagemResultado_DataEntrega, P00YB18_A509ContagemrResultado_SistemaSigla, P00YB18_n509ContagemrResultado_SistemaSigla, P00YB18_A1046ContagemResultado_Agrupador, P00YB18_n1046ContagemResultado_Agrupador,
               P00YB18_A2118ContagemResultado_Owner_Identificao, P00YB18_n2118ContagemResultado_Owner_Identificao, P00YB18_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P00YB21_A489ContagemResultado_SistemaCod, P00YB21_n489ContagemResultado_SistemaCod, P00YB21_A1553ContagemResultado_CntSrvCod, P00YB21_n1553ContagemResultado_CntSrvCod, P00YB21_A601ContagemResultado_Servico, P00YB21_n601ContagemResultado_Servico, P00YB21_A508ContagemResultado_Owner, P00YB21_A484ContagemResultado_StatusDmn, P00YB21_n484ContagemResultado_StatusDmn, P00YB21_A490ContagemResultado_ContratadaCod,
               P00YB21_n490ContagemResultado_ContratadaCod, P00YB21_A457ContagemResultado_Demanda, P00YB21_n457ContagemResultado_Demanda, P00YB21_A493ContagemResultado_DemandaFM, P00YB21_n493ContagemResultado_DemandaFM, P00YB21_A494ContagemResultado_Descricao, P00YB21_n494ContagemResultado_Descricao, P00YB21_A801ContagemResultado_ServicoSigla, P00YB21_n801ContagemResultado_ServicoSigla, P00YB21_A471ContagemResultado_DataDmn,
               P00YB21_A1351ContagemResultado_DataPrevista, P00YB21_n1351ContagemResultado_DataPrevista, P00YB21_A472ContagemResultado_DataEntrega, P00YB21_n472ContagemResultado_DataEntrega, P00YB21_A509ContagemrResultado_SistemaSigla, P00YB21_n509ContagemrResultado_SistemaSigla, P00YB21_A1046ContagemResultado_Agrupador, P00YB21_n1046ContagemResultado_Agrupador, P00YB21_A531ContagemResultado_StatusUltCnt, P00YB21_n531ContagemResultado_StatusUltCnt,
               P00YB21_A2118ContagemResultado_Owner_Identificao, P00YB21_n2118ContagemResultado_Owner_Identificao, P00YB21_A890ContagemResultado_Responsavel, P00YB21_n890ContagemResultado_Responsavel, P00YB21_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P00YB23_A489ContagemResultado_SistemaCod, P00YB23_n489ContagemResultado_SistemaCod, P00YB23_A1553ContagemResultado_CntSrvCod, P00YB23_n1553ContagemResultado_CntSrvCod, P00YB23_A601ContagemResultado_Servico, P00YB23_n601ContagemResultado_Servico, P00YB23_A508ContagemResultado_Owner, P00YB23_A484ContagemResultado_StatusDmn, P00YB23_n484ContagemResultado_StatusDmn, P00YB23_A490ContagemResultado_ContratadaCod,
               P00YB23_n490ContagemResultado_ContratadaCod, P00YB23_A457ContagemResultado_Demanda, P00YB23_n457ContagemResultado_Demanda, P00YB23_A493ContagemResultado_DemandaFM, P00YB23_n493ContagemResultado_DemandaFM, P00YB23_A494ContagemResultado_Descricao, P00YB23_n494ContagemResultado_Descricao, P00YB23_A801ContagemResultado_ServicoSigla, P00YB23_n801ContagemResultado_ServicoSigla, P00YB23_A471ContagemResultado_DataDmn,
               P00YB23_A1351ContagemResultado_DataPrevista, P00YB23_n1351ContagemResultado_DataPrevista, P00YB23_A472ContagemResultado_DataEntrega, P00YB23_n472ContagemResultado_DataEntrega, P00YB23_A509ContagemrResultado_SistemaSigla, P00YB23_n509ContagemrResultado_SistemaSigla, P00YB23_A1046ContagemResultado_Agrupador, P00YB23_n1046ContagemResultado_Agrupador, P00YB23_A890ContagemResultado_Responsavel, P00YB23_n890ContagemResultado_Responsavel,
               P00YB23_A2118ContagemResultado_Owner_Identificao, P00YB23_n2118ContagemResultado_Owner_Identificao, P00YB23_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P00YB26_A489ContagemResultado_SistemaCod, P00YB26_n489ContagemResultado_SistemaCod, P00YB26_A1553ContagemResultado_CntSrvCod, P00YB26_n1553ContagemResultado_CntSrvCod, P00YB26_A601ContagemResultado_Servico, P00YB26_n601ContagemResultado_Servico, P00YB26_A456ContagemResultado_Codigo, P00YB26_A508ContagemResultado_Owner, P00YB26_A484ContagemResultado_StatusDmn, P00YB26_n484ContagemResultado_StatusDmn,
               P00YB26_A490ContagemResultado_ContratadaCod, P00YB26_n490ContagemResultado_ContratadaCod, P00YB26_A457ContagemResultado_Demanda, P00YB26_n457ContagemResultado_Demanda, P00YB26_A493ContagemResultado_DemandaFM, P00YB26_n493ContagemResultado_DemandaFM, P00YB26_A494ContagemResultado_Descricao, P00YB26_n494ContagemResultado_Descricao, P00YB26_A801ContagemResultado_ServicoSigla, P00YB26_n801ContagemResultado_ServicoSigla,
               P00YB26_A471ContagemResultado_DataDmn, P00YB26_A1351ContagemResultado_DataPrevista, P00YB26_n1351ContagemResultado_DataPrevista, P00YB26_A472ContagemResultado_DataEntrega, P00YB26_n472ContagemResultado_DataEntrega, P00YB26_A509ContagemrResultado_SistemaSigla, P00YB26_n509ContagemrResultado_SistemaSigla, P00YB26_A1046ContagemResultado_Agrupador, P00YB26_n1046ContagemResultado_Agrupador, P00YB26_A531ContagemResultado_StatusUltCnt,
               P00YB26_n531ContagemResultado_StatusUltCnt, P00YB26_A2118ContagemResultado_Owner_Identificao, P00YB26_n2118ContagemResultado_Owner_Identificao
               }
               , new Object[] {
               P00YB29_A1985ContagemResultadoQA_OSCod, P00YB29_A489ContagemResultado_SistemaCod, P00YB29_n489ContagemResultado_SistemaCod, P00YB29_A1553ContagemResultado_CntSrvCod, P00YB29_n1553ContagemResultado_CntSrvCod, P00YB29_A601ContagemResultado_Servico, P00YB29_n601ContagemResultado_Servico, P00YB29_A508ContagemResultado_Owner, P00YB29_n508ContagemResultado_Owner, P00YB29_A1996ContagemResultadoQA_RespostaDe,
               P00YB29_n1996ContagemResultadoQA_RespostaDe, P00YB29_A1992ContagemResultadoQA_ParaCod, P00YB29_A490ContagemResultado_ContratadaCod, P00YB29_n490ContagemResultado_ContratadaCod, P00YB29_A457ContagemResultado_Demanda, P00YB29_n457ContagemResultado_Demanda, P00YB29_A493ContagemResultado_DemandaFM, P00YB29_n493ContagemResultado_DemandaFM, P00YB29_A494ContagemResultado_Descricao, P00YB29_n494ContagemResultado_Descricao,
               P00YB29_A801ContagemResultado_ServicoSigla, P00YB29_n801ContagemResultado_ServicoSigla, P00YB29_A484ContagemResultado_StatusDmn, P00YB29_n484ContagemResultado_StatusDmn, P00YB29_A471ContagemResultado_DataDmn, P00YB29_n471ContagemResultado_DataDmn, P00YB29_A1351ContagemResultado_DataPrevista, P00YB29_n1351ContagemResultado_DataPrevista, P00YB29_A472ContagemResultado_DataEntrega, P00YB29_n472ContagemResultado_DataEntrega,
               P00YB29_A509ContagemrResultado_SistemaSigla, P00YB29_n509ContagemrResultado_SistemaSigla, P00YB29_A1046ContagemResultado_Agrupador, P00YB29_n1046ContagemResultado_Agrupador, P00YB29_A2118ContagemResultado_Owner_Identificao, P00YB29_n2118ContagemResultado_Owner_Identificao, P00YB29_A1984ContagemResultadoQA_Codigo
               }
               , new Object[] {
               P00YB31_A489ContagemResultado_SistemaCod, P00YB31_n489ContagemResultado_SistemaCod, P00YB31_A1553ContagemResultado_CntSrvCod, P00YB31_n1553ContagemResultado_CntSrvCod, P00YB31_A601ContagemResultado_Servico, P00YB31_n601ContagemResultado_Servico, P00YB31_A1603ContagemResultado_CntCod, P00YB31_n1603ContagemResultado_CntCod, P00YB31_A508ContagemResultado_Owner, P00YB31_A484ContagemResultado_StatusDmn,
               P00YB31_n484ContagemResultado_StatusDmn, P00YB31_A490ContagemResultado_ContratadaCod, P00YB31_n490ContagemResultado_ContratadaCod, P00YB31_A457ContagemResultado_Demanda, P00YB31_n457ContagemResultado_Demanda, P00YB31_A493ContagemResultado_DemandaFM, P00YB31_n493ContagemResultado_DemandaFM, P00YB31_A494ContagemResultado_Descricao, P00YB31_n494ContagemResultado_Descricao, P00YB31_A801ContagemResultado_ServicoSigla,
               P00YB31_n801ContagemResultado_ServicoSigla, P00YB31_A471ContagemResultado_DataDmn, P00YB31_A1351ContagemResultado_DataPrevista, P00YB31_n1351ContagemResultado_DataPrevista, P00YB31_A472ContagemResultado_DataEntrega, P00YB31_n472ContagemResultado_DataEntrega, P00YB31_A509ContagemrResultado_SistemaSigla, P00YB31_n509ContagemrResultado_SistemaSigla, P00YB31_A1046ContagemResultado_Agrupador, P00YB31_n1046ContagemResultado_Agrupador,
               P00YB31_A890ContagemResultado_Responsavel, P00YB31_n890ContagemResultado_Responsavel, P00YB31_A2118ContagemResultado_Owner_Identificao, P00YB31_n2118ContagemResultado_Owner_Identificao, P00YB31_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P00YB33_A489ContagemResultado_SistemaCod, P00YB33_n489ContagemResultado_SistemaCod, P00YB33_A1553ContagemResultado_CntSrvCod, P00YB33_n1553ContagemResultado_CntSrvCod, P00YB33_A601ContagemResultado_Servico, P00YB33_n601ContagemResultado_Servico, P00YB33_A508ContagemResultado_Owner, P00YB33_A1457ContagemResultado_TemDpnHmlg, P00YB33_n1457ContagemResultado_TemDpnHmlg, P00YB33_A484ContagemResultado_StatusDmn,
               P00YB33_n484ContagemResultado_StatusDmn, P00YB33_A490ContagemResultado_ContratadaCod, P00YB33_n490ContagemResultado_ContratadaCod, P00YB33_A457ContagemResultado_Demanda, P00YB33_n457ContagemResultado_Demanda, P00YB33_A493ContagemResultado_DemandaFM, P00YB33_n493ContagemResultado_DemandaFM, P00YB33_A494ContagemResultado_Descricao, P00YB33_n494ContagemResultado_Descricao, P00YB33_A801ContagemResultado_ServicoSigla,
               P00YB33_n801ContagemResultado_ServicoSigla, P00YB33_A471ContagemResultado_DataDmn, P00YB33_A1351ContagemResultado_DataPrevista, P00YB33_n1351ContagemResultado_DataPrevista, P00YB33_A472ContagemResultado_DataEntrega, P00YB33_n472ContagemResultado_DataEntrega, P00YB33_A509ContagemrResultado_SistemaSigla, P00YB33_n509ContagemrResultado_SistemaSigla, P00YB33_A1046ContagemResultado_Agrupador, P00YB33_n1046ContagemResultado_Agrupador,
               P00YB33_A2118ContagemResultado_Owner_Identificao, P00YB33_n2118ContagemResultado_Owner_Identificao, P00YB33_A456ContagemResultado_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short GXSoapError ;
      private short currSoapErr ;
      private short readOk ;
      private short nOutParmCount ;
      private short AV54GXLvl175 ;
      private short AV56GXLvl224 ;
      private short AV66GXLvl415 ;
      private short AV31wwpContext_gxTpr_Userid ;
      private short A531ContagemResultado_StatusUltCnt ;
      private int A1Usuario_Codigo ;
      private int A57Usuario_PessoaCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A1228ContratadaUsuario_AreaTrabalhoCod ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A1078ContratoGestor_ContratoCod ;
      private int A1079ContratoGestor_UsuarioCod ;
      private int A1446ContratoGestor_ContratadaAreaCod ;
      private int A1136ContratoGestor_ContratadaCod ;
      private int A60ContratanteUsuario_UsuarioCod ;
      private int A63ContratanteUsuario_ContratanteCod ;
      private int A1020ContratanteUsuario_AreaTrabalhoCod ;
      private int A335Contratante_PessoaCod ;
      private int A5AreaTrabalho_Codigo ;
      private int A29Contratante_Codigo ;
      private int AV29Usuario_Codigo ;
      private int A7Perfil_AreaTrabalhoCod ;
      private int A3Perfil_Codigo ;
      private int A75Contrato_AreaTrabalhoCod ;
      private int A74Contrato_Codigo ;
      private int A40Contratada_PessoaCod ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A39Contratada_Codigo ;
      private int AV65GXV2 ;
      private int AV12SDT_WsFiltros_gxTpr_Areatrabalhocodigo ;
      private int AV31wwpContext_gxTpr_Contratada_codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A508ContagemResultado_Owner ;
      private int A890ContagemResultado_Responsavel ;
      private int A489ContagemResultado_SistemaCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A601ContagemResultado_Servico ;
      private int A456ContagemResultado_Codigo ;
      private int AV31wwpContext_gxTpr_Contratante_codigo ;
      private int A1236ContagemResultado_ContratanteDoResponsavel ;
      private int A1992ContagemResultadoQA_ParaCod ;
      private int A1996ContagemResultadoQA_RespostaDe ;
      private int A1998ContagemResultadoQA_Resposta ;
      private int A1985ContagemResultadoQA_OSCod ;
      private int A1984ContagemResultadoQA_Codigo ;
      private int GXt_int2 ;
      private int A1603ContagemResultado_CntCod ;
      private String currSoapErrmsg ;
      private String currMethod ;
      private String sTagName ;
      private String scmdbuf ;
      private String A341Usuario_UserGamGuid ;
      private String A2Usuario_Nome ;
      private String A58Usuario_PessoaNom ;
      private String A9Contratante_RazaoSocial ;
      private String A4Perfil_Nome ;
      private String A77Contrato_Numero ;
      private String A41Contratada_PessoaNom ;
      private String AV33X ;
      private String A484ContagemResultado_StatusDmn ;
      private String A801ContagemResultado_ServicoSigla ;
      private String A509ContagemrResultado_SistemaSigla ;
      private String A1046ContagemResultado_Agrupador ;
      private DateTime A1351ContagemResultado_DataPrevista ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime A472ContagemResultado_DataEntrega ;
      private bool returnInSub ;
      private bool AV16IsParametros ;
      private bool AV15IsAutenticado ;
      private bool GXt_boolean1 ;
      private bool A54Usuario_Ativo ;
      private bool n2Usuario_Nome ;
      private bool n58Usuario_PessoaNom ;
      private bool n1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool n1446ContratoGestor_ContratadaAreaCod ;
      private bool n1136ContratoGestor_ContratadaCod ;
      private bool n1020ContratanteUsuario_AreaTrabalhoCod ;
      private bool n29Contratante_Codigo ;
      private bool n9Contratante_RazaoSocial ;
      private bool n53Contratada_AreaTrabalhoDes ;
      private bool n41Contratada_PessoaNom ;
      private bool AV31wwpContext_gxTpr_Userehcontratada ;
      private bool AV31wwpContext_gxTpr_Userehcontratante ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n890ContagemResultado_Responsavel ;
      private bool n508ContagemResultado_Owner ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n457ContagemResultado_Demanda ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n494ContagemResultado_Descricao ;
      private bool n801ContagemResultado_ServicoSigla ;
      private bool n471ContagemResultado_DataDmn ;
      private bool n1351ContagemResultado_DataPrevista ;
      private bool n472ContagemResultado_DataEntrega ;
      private bool n509ContagemrResultado_SistemaSigla ;
      private bool n1046ContagemResultado_Agrupador ;
      private bool n2118ContagemResultado_Owner_Identificao ;
      private bool n531ContagemResultado_StatusUltCnt ;
      private bool n1996ContagemResultadoQA_RespostaDe ;
      private bool n1603ContagemResultado_CntCod ;
      private bool A1457ContagemResultado_TemDpnHmlg ;
      private bool n1457ContagemResultado_TemDpnHmlg ;
      private String AV10Retorno ;
      private String A6AreaTrabalho_Descricao ;
      private String A53Contratada_AreaTrabalhoDes ;
      private String AV12SDT_WsFiltros_gxTpr_Areatrabalhonome ;
      private String A457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String A494ContagemResultado_Descricao ;
      private String A2118ContagemResultado_Owner_Identificao ;
      private GXXMLReader GXSoapXMLReader ;
      private GXXMLWriter GXSoapXMLWriter ;
      private GxHttpRequest GXSoapHTTPRequest ;
      private GxHttpResponse GXSoapHTTPResponse ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private bool[] P00YB2_A54Usuario_Ativo ;
      private String[] P00YB2_A341Usuario_UserGamGuid ;
      private int[] P00YB2_A1Usuario_Codigo ;
      private String[] P00YB2_A2Usuario_Nome ;
      private bool[] P00YB2_n2Usuario_Nome ;
      private int[] P00YB2_A57Usuario_PessoaCod ;
      private String[] P00YB2_A58Usuario_PessoaNom ;
      private bool[] P00YB2_n58Usuario_PessoaNom ;
      private int[] P00YB3_A69ContratadaUsuario_UsuarioCod ;
      private int[] P00YB3_A1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] P00YB3_n1228ContratadaUsuario_AreaTrabalhoCod ;
      private int[] P00YB3_A66ContratadaUsuario_ContratadaCod ;
      private int[] P00YB4_A1078ContratoGestor_ContratoCod ;
      private int[] P00YB4_A1079ContratoGestor_UsuarioCod ;
      private int[] P00YB4_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] P00YB4_n1446ContratoGestor_ContratadaAreaCod ;
      private int[] P00YB4_A1136ContratoGestor_ContratadaCod ;
      private bool[] P00YB4_n1136ContratoGestor_ContratadaCod ;
      private int[] P00YB6_A60ContratanteUsuario_UsuarioCod ;
      private int[] P00YB6_A63ContratanteUsuario_ContratanteCod ;
      private int[] P00YB6_A1020ContratanteUsuario_AreaTrabalhoCod ;
      private bool[] P00YB6_n1020ContratanteUsuario_AreaTrabalhoCod ;
      private int[] P00YB7_A1078ContratoGestor_ContratoCod ;
      private int[] P00YB7_A1079ContratoGestor_UsuarioCod ;
      private int[] P00YB7_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] P00YB7_n1446ContratoGestor_ContratadaAreaCod ;
      private int[] P00YB7_A1136ContratoGestor_ContratadaCod ;
      private bool[] P00YB7_n1136ContratoGestor_ContratadaCod ;
      private int[] P00YB8_A335Contratante_PessoaCod ;
      private int[] P00YB8_A5AreaTrabalho_Codigo ;
      private String[] P00YB8_A6AreaTrabalho_Descricao ;
      private int[] P00YB8_A29Contratante_Codigo ;
      private bool[] P00YB8_n29Contratante_Codigo ;
      private String[] P00YB8_A9Contratante_RazaoSocial ;
      private bool[] P00YB8_n9Contratante_RazaoSocial ;
      private int[] P00YB9_A7Perfil_AreaTrabalhoCod ;
      private int[] P00YB9_A1Usuario_Codigo ;
      private int[] P00YB9_A3Perfil_Codigo ;
      private String[] P00YB9_A4Perfil_Nome ;
      private int[] P00YB10_A75Contrato_AreaTrabalhoCod ;
      private int[] P00YB10_A74Contrato_Codigo ;
      private String[] P00YB10_A77Contrato_Numero ;
      private int[] P00YB11_A40Contratada_PessoaCod ;
      private int[] P00YB11_A52Contratada_AreaTrabalhoCod ;
      private String[] P00YB11_A53Contratada_AreaTrabalhoDes ;
      private bool[] P00YB11_n53Contratada_AreaTrabalhoDes ;
      private int[] P00YB11_A39Contratada_Codigo ;
      private String[] P00YB11_A41Contratada_PessoaNom ;
      private bool[] P00YB11_n41Contratada_PessoaNom ;
      private int[] P00YB12_A7Perfil_AreaTrabalhoCod ;
      private int[] P00YB12_A1Usuario_Codigo ;
      private int[] P00YB12_A3Perfil_Codigo ;
      private String[] P00YB12_A4Perfil_Nome ;
      private int[] P00YB13_A39Contratada_Codigo ;
      private int[] P00YB13_A75Contrato_AreaTrabalhoCod ;
      private int[] P00YB13_A74Contrato_Codigo ;
      private String[] P00YB13_A77Contrato_Numero ;
      private String[] P00YB14_A6AreaTrabalho_Descricao ;
      private int[] P00YB14_A5AreaTrabalho_Codigo ;
      private int[] P00YB14_A29Contratante_Codigo ;
      private bool[] P00YB14_n29Contratante_Codigo ;
      private int[] P00YB16_A489ContagemResultado_SistemaCod ;
      private bool[] P00YB16_n489ContagemResultado_SistemaCod ;
      private int[] P00YB16_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00YB16_n1553ContagemResultado_CntSrvCod ;
      private int[] P00YB16_A601ContagemResultado_Servico ;
      private bool[] P00YB16_n601ContagemResultado_Servico ;
      private int[] P00YB16_A890ContagemResultado_Responsavel ;
      private bool[] P00YB16_n890ContagemResultado_Responsavel ;
      private int[] P00YB16_A508ContagemResultado_Owner ;
      private bool[] P00YB16_n508ContagemResultado_Owner ;
      private String[] P00YB16_A484ContagemResultado_StatusDmn ;
      private bool[] P00YB16_n484ContagemResultado_StatusDmn ;
      private int[] P00YB16_A490ContagemResultado_ContratadaCod ;
      private bool[] P00YB16_n490ContagemResultado_ContratadaCod ;
      private String[] P00YB16_A457ContagemResultado_Demanda ;
      private bool[] P00YB16_n457ContagemResultado_Demanda ;
      private String[] P00YB16_A493ContagemResultado_DemandaFM ;
      private bool[] P00YB16_n493ContagemResultado_DemandaFM ;
      private String[] P00YB16_A494ContagemResultado_Descricao ;
      private bool[] P00YB16_n494ContagemResultado_Descricao ;
      private String[] P00YB16_A801ContagemResultado_ServicoSigla ;
      private bool[] P00YB16_n801ContagemResultado_ServicoSigla ;
      private DateTime[] P00YB16_A471ContagemResultado_DataDmn ;
      private bool[] P00YB16_n471ContagemResultado_DataDmn ;
      private DateTime[] P00YB16_A1351ContagemResultado_DataPrevista ;
      private bool[] P00YB16_n1351ContagemResultado_DataPrevista ;
      private DateTime[] P00YB16_A472ContagemResultado_DataEntrega ;
      private bool[] P00YB16_n472ContagemResultado_DataEntrega ;
      private String[] P00YB16_A509ContagemrResultado_SistemaSigla ;
      private bool[] P00YB16_n509ContagemrResultado_SistemaSigla ;
      private String[] P00YB16_A1046ContagemResultado_Agrupador ;
      private bool[] P00YB16_n1046ContagemResultado_Agrupador ;
      private String[] P00YB16_A2118ContagemResultado_Owner_Identificao ;
      private bool[] P00YB16_n2118ContagemResultado_Owner_Identificao ;
      private int[] P00YB16_A456ContagemResultado_Codigo ;
      private int[] P00YB18_A489ContagemResultado_SistemaCod ;
      private bool[] P00YB18_n489ContagemResultado_SistemaCod ;
      private int[] P00YB18_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00YB18_n1553ContagemResultado_CntSrvCod ;
      private int[] P00YB18_A601ContagemResultado_Servico ;
      private bool[] P00YB18_n601ContagemResultado_Servico ;
      private int[] P00YB18_A508ContagemResultado_Owner ;
      private bool[] P00YB18_n508ContagemResultado_Owner ;
      private int[] P00YB18_A890ContagemResultado_Responsavel ;
      private bool[] P00YB18_n890ContagemResultado_Responsavel ;
      private String[] P00YB18_A484ContagemResultado_StatusDmn ;
      private bool[] P00YB18_n484ContagemResultado_StatusDmn ;
      private int[] P00YB18_A490ContagemResultado_ContratadaCod ;
      private bool[] P00YB18_n490ContagemResultado_ContratadaCod ;
      private String[] P00YB18_A457ContagemResultado_Demanda ;
      private bool[] P00YB18_n457ContagemResultado_Demanda ;
      private String[] P00YB18_A493ContagemResultado_DemandaFM ;
      private bool[] P00YB18_n493ContagemResultado_DemandaFM ;
      private String[] P00YB18_A494ContagemResultado_Descricao ;
      private bool[] P00YB18_n494ContagemResultado_Descricao ;
      private String[] P00YB18_A801ContagemResultado_ServicoSigla ;
      private bool[] P00YB18_n801ContagemResultado_ServicoSigla ;
      private DateTime[] P00YB18_A471ContagemResultado_DataDmn ;
      private bool[] P00YB18_n471ContagemResultado_DataDmn ;
      private DateTime[] P00YB18_A1351ContagemResultado_DataPrevista ;
      private bool[] P00YB18_n1351ContagemResultado_DataPrevista ;
      private DateTime[] P00YB18_A472ContagemResultado_DataEntrega ;
      private bool[] P00YB18_n472ContagemResultado_DataEntrega ;
      private String[] P00YB18_A509ContagemrResultado_SistemaSigla ;
      private bool[] P00YB18_n509ContagemrResultado_SistemaSigla ;
      private String[] P00YB18_A1046ContagemResultado_Agrupador ;
      private bool[] P00YB18_n1046ContagemResultado_Agrupador ;
      private String[] P00YB18_A2118ContagemResultado_Owner_Identificao ;
      private bool[] P00YB18_n2118ContagemResultado_Owner_Identificao ;
      private int[] P00YB18_A456ContagemResultado_Codigo ;
      private int[] P00YB21_A489ContagemResultado_SistemaCod ;
      private bool[] P00YB21_n489ContagemResultado_SistemaCod ;
      private int[] P00YB21_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00YB21_n1553ContagemResultado_CntSrvCod ;
      private int[] P00YB21_A601ContagemResultado_Servico ;
      private bool[] P00YB21_n601ContagemResultado_Servico ;
      private int[] P00YB21_A508ContagemResultado_Owner ;
      private bool[] P00YB21_n508ContagemResultado_Owner ;
      private String[] P00YB21_A484ContagemResultado_StatusDmn ;
      private bool[] P00YB21_n484ContagemResultado_StatusDmn ;
      private int[] P00YB21_A490ContagemResultado_ContratadaCod ;
      private bool[] P00YB21_n490ContagemResultado_ContratadaCod ;
      private String[] P00YB21_A457ContagemResultado_Demanda ;
      private bool[] P00YB21_n457ContagemResultado_Demanda ;
      private String[] P00YB21_A493ContagemResultado_DemandaFM ;
      private bool[] P00YB21_n493ContagemResultado_DemandaFM ;
      private String[] P00YB21_A494ContagemResultado_Descricao ;
      private bool[] P00YB21_n494ContagemResultado_Descricao ;
      private String[] P00YB21_A801ContagemResultado_ServicoSigla ;
      private bool[] P00YB21_n801ContagemResultado_ServicoSigla ;
      private DateTime[] P00YB21_A471ContagemResultado_DataDmn ;
      private bool[] P00YB21_n471ContagemResultado_DataDmn ;
      private DateTime[] P00YB21_A1351ContagemResultado_DataPrevista ;
      private bool[] P00YB21_n1351ContagemResultado_DataPrevista ;
      private DateTime[] P00YB21_A472ContagemResultado_DataEntrega ;
      private bool[] P00YB21_n472ContagemResultado_DataEntrega ;
      private String[] P00YB21_A509ContagemrResultado_SistemaSigla ;
      private bool[] P00YB21_n509ContagemrResultado_SistemaSigla ;
      private String[] P00YB21_A1046ContagemResultado_Agrupador ;
      private bool[] P00YB21_n1046ContagemResultado_Agrupador ;
      private short[] P00YB21_A531ContagemResultado_StatusUltCnt ;
      private bool[] P00YB21_n531ContagemResultado_StatusUltCnt ;
      private String[] P00YB21_A2118ContagemResultado_Owner_Identificao ;
      private bool[] P00YB21_n2118ContagemResultado_Owner_Identificao ;
      private int[] P00YB21_A890ContagemResultado_Responsavel ;
      private bool[] P00YB21_n890ContagemResultado_Responsavel ;
      private int[] P00YB21_A456ContagemResultado_Codigo ;
      private int[] P00YB23_A489ContagemResultado_SistemaCod ;
      private bool[] P00YB23_n489ContagemResultado_SistemaCod ;
      private int[] P00YB23_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00YB23_n1553ContagemResultado_CntSrvCod ;
      private int[] P00YB23_A601ContagemResultado_Servico ;
      private bool[] P00YB23_n601ContagemResultado_Servico ;
      private int[] P00YB23_A508ContagemResultado_Owner ;
      private bool[] P00YB23_n508ContagemResultado_Owner ;
      private String[] P00YB23_A484ContagemResultado_StatusDmn ;
      private bool[] P00YB23_n484ContagemResultado_StatusDmn ;
      private int[] P00YB23_A490ContagemResultado_ContratadaCod ;
      private bool[] P00YB23_n490ContagemResultado_ContratadaCod ;
      private String[] P00YB23_A457ContagemResultado_Demanda ;
      private bool[] P00YB23_n457ContagemResultado_Demanda ;
      private String[] P00YB23_A493ContagemResultado_DemandaFM ;
      private bool[] P00YB23_n493ContagemResultado_DemandaFM ;
      private String[] P00YB23_A494ContagemResultado_Descricao ;
      private bool[] P00YB23_n494ContagemResultado_Descricao ;
      private String[] P00YB23_A801ContagemResultado_ServicoSigla ;
      private bool[] P00YB23_n801ContagemResultado_ServicoSigla ;
      private DateTime[] P00YB23_A471ContagemResultado_DataDmn ;
      private bool[] P00YB23_n471ContagemResultado_DataDmn ;
      private DateTime[] P00YB23_A1351ContagemResultado_DataPrevista ;
      private bool[] P00YB23_n1351ContagemResultado_DataPrevista ;
      private DateTime[] P00YB23_A472ContagemResultado_DataEntrega ;
      private bool[] P00YB23_n472ContagemResultado_DataEntrega ;
      private String[] P00YB23_A509ContagemrResultado_SistemaSigla ;
      private bool[] P00YB23_n509ContagemrResultado_SistemaSigla ;
      private String[] P00YB23_A1046ContagemResultado_Agrupador ;
      private bool[] P00YB23_n1046ContagemResultado_Agrupador ;
      private int[] P00YB23_A890ContagemResultado_Responsavel ;
      private bool[] P00YB23_n890ContagemResultado_Responsavel ;
      private String[] P00YB23_A2118ContagemResultado_Owner_Identificao ;
      private bool[] P00YB23_n2118ContagemResultado_Owner_Identificao ;
      private int[] P00YB23_A456ContagemResultado_Codigo ;
      private int[] P00YB26_A489ContagemResultado_SistemaCod ;
      private bool[] P00YB26_n489ContagemResultado_SistemaCod ;
      private int[] P00YB26_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00YB26_n1553ContagemResultado_CntSrvCod ;
      private int[] P00YB26_A601ContagemResultado_Servico ;
      private bool[] P00YB26_n601ContagemResultado_Servico ;
      private int[] P00YB26_A456ContagemResultado_Codigo ;
      private int[] P00YB26_A508ContagemResultado_Owner ;
      private bool[] P00YB26_n508ContagemResultado_Owner ;
      private String[] P00YB26_A484ContagemResultado_StatusDmn ;
      private bool[] P00YB26_n484ContagemResultado_StatusDmn ;
      private int[] P00YB26_A490ContagemResultado_ContratadaCod ;
      private bool[] P00YB26_n490ContagemResultado_ContratadaCod ;
      private String[] P00YB26_A457ContagemResultado_Demanda ;
      private bool[] P00YB26_n457ContagemResultado_Demanda ;
      private String[] P00YB26_A493ContagemResultado_DemandaFM ;
      private bool[] P00YB26_n493ContagemResultado_DemandaFM ;
      private String[] P00YB26_A494ContagemResultado_Descricao ;
      private bool[] P00YB26_n494ContagemResultado_Descricao ;
      private String[] P00YB26_A801ContagemResultado_ServicoSigla ;
      private bool[] P00YB26_n801ContagemResultado_ServicoSigla ;
      private DateTime[] P00YB26_A471ContagemResultado_DataDmn ;
      private bool[] P00YB26_n471ContagemResultado_DataDmn ;
      private DateTime[] P00YB26_A1351ContagemResultado_DataPrevista ;
      private bool[] P00YB26_n1351ContagemResultado_DataPrevista ;
      private DateTime[] P00YB26_A472ContagemResultado_DataEntrega ;
      private bool[] P00YB26_n472ContagemResultado_DataEntrega ;
      private String[] P00YB26_A509ContagemrResultado_SistemaSigla ;
      private bool[] P00YB26_n509ContagemrResultado_SistemaSigla ;
      private String[] P00YB26_A1046ContagemResultado_Agrupador ;
      private bool[] P00YB26_n1046ContagemResultado_Agrupador ;
      private short[] P00YB26_A531ContagemResultado_StatusUltCnt ;
      private bool[] P00YB26_n531ContagemResultado_StatusUltCnt ;
      private String[] P00YB26_A2118ContagemResultado_Owner_Identificao ;
      private bool[] P00YB26_n2118ContagemResultado_Owner_Identificao ;
      private int[] P00YB29_A1985ContagemResultadoQA_OSCod ;
      private int[] P00YB29_A489ContagemResultado_SistemaCod ;
      private bool[] P00YB29_n489ContagemResultado_SistemaCod ;
      private int[] P00YB29_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00YB29_n1553ContagemResultado_CntSrvCod ;
      private int[] P00YB29_A601ContagemResultado_Servico ;
      private bool[] P00YB29_n601ContagemResultado_Servico ;
      private int[] P00YB29_A508ContagemResultado_Owner ;
      private bool[] P00YB29_n508ContagemResultado_Owner ;
      private int[] P00YB29_A1996ContagemResultadoQA_RespostaDe ;
      private bool[] P00YB29_n1996ContagemResultadoQA_RespostaDe ;
      private int[] P00YB29_A1992ContagemResultadoQA_ParaCod ;
      private int[] P00YB29_A490ContagemResultado_ContratadaCod ;
      private bool[] P00YB29_n490ContagemResultado_ContratadaCod ;
      private String[] P00YB29_A457ContagemResultado_Demanda ;
      private bool[] P00YB29_n457ContagemResultado_Demanda ;
      private String[] P00YB29_A493ContagemResultado_DemandaFM ;
      private bool[] P00YB29_n493ContagemResultado_DemandaFM ;
      private String[] P00YB29_A494ContagemResultado_Descricao ;
      private bool[] P00YB29_n494ContagemResultado_Descricao ;
      private String[] P00YB29_A801ContagemResultado_ServicoSigla ;
      private bool[] P00YB29_n801ContagemResultado_ServicoSigla ;
      private String[] P00YB29_A484ContagemResultado_StatusDmn ;
      private bool[] P00YB29_n484ContagemResultado_StatusDmn ;
      private DateTime[] P00YB29_A471ContagemResultado_DataDmn ;
      private bool[] P00YB29_n471ContagemResultado_DataDmn ;
      private DateTime[] P00YB29_A1351ContagemResultado_DataPrevista ;
      private bool[] P00YB29_n1351ContagemResultado_DataPrevista ;
      private DateTime[] P00YB29_A472ContagemResultado_DataEntrega ;
      private bool[] P00YB29_n472ContagemResultado_DataEntrega ;
      private String[] P00YB29_A509ContagemrResultado_SistemaSigla ;
      private bool[] P00YB29_n509ContagemrResultado_SistemaSigla ;
      private String[] P00YB29_A1046ContagemResultado_Agrupador ;
      private bool[] P00YB29_n1046ContagemResultado_Agrupador ;
      private String[] P00YB29_A2118ContagemResultado_Owner_Identificao ;
      private bool[] P00YB29_n2118ContagemResultado_Owner_Identificao ;
      private int[] P00YB29_A1984ContagemResultadoQA_Codigo ;
      private int[] P00YB31_A489ContagemResultado_SistemaCod ;
      private bool[] P00YB31_n489ContagemResultado_SistemaCod ;
      private int[] P00YB31_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00YB31_n1553ContagemResultado_CntSrvCod ;
      private int[] P00YB31_A601ContagemResultado_Servico ;
      private bool[] P00YB31_n601ContagemResultado_Servico ;
      private int[] P00YB31_A1603ContagemResultado_CntCod ;
      private bool[] P00YB31_n1603ContagemResultado_CntCod ;
      private int[] P00YB31_A508ContagemResultado_Owner ;
      private bool[] P00YB31_n508ContagemResultado_Owner ;
      private String[] P00YB31_A484ContagemResultado_StatusDmn ;
      private bool[] P00YB31_n484ContagemResultado_StatusDmn ;
      private int[] P00YB31_A490ContagemResultado_ContratadaCod ;
      private bool[] P00YB31_n490ContagemResultado_ContratadaCod ;
      private String[] P00YB31_A457ContagemResultado_Demanda ;
      private bool[] P00YB31_n457ContagemResultado_Demanda ;
      private String[] P00YB31_A493ContagemResultado_DemandaFM ;
      private bool[] P00YB31_n493ContagemResultado_DemandaFM ;
      private String[] P00YB31_A494ContagemResultado_Descricao ;
      private bool[] P00YB31_n494ContagemResultado_Descricao ;
      private String[] P00YB31_A801ContagemResultado_ServicoSigla ;
      private bool[] P00YB31_n801ContagemResultado_ServicoSigla ;
      private DateTime[] P00YB31_A471ContagemResultado_DataDmn ;
      private bool[] P00YB31_n471ContagemResultado_DataDmn ;
      private DateTime[] P00YB31_A1351ContagemResultado_DataPrevista ;
      private bool[] P00YB31_n1351ContagemResultado_DataPrevista ;
      private DateTime[] P00YB31_A472ContagemResultado_DataEntrega ;
      private bool[] P00YB31_n472ContagemResultado_DataEntrega ;
      private String[] P00YB31_A509ContagemrResultado_SistemaSigla ;
      private bool[] P00YB31_n509ContagemrResultado_SistemaSigla ;
      private String[] P00YB31_A1046ContagemResultado_Agrupador ;
      private bool[] P00YB31_n1046ContagemResultado_Agrupador ;
      private int[] P00YB31_A890ContagemResultado_Responsavel ;
      private bool[] P00YB31_n890ContagemResultado_Responsavel ;
      private String[] P00YB31_A2118ContagemResultado_Owner_Identificao ;
      private bool[] P00YB31_n2118ContagemResultado_Owner_Identificao ;
      private int[] P00YB31_A456ContagemResultado_Codigo ;
      private int[] P00YB33_A489ContagemResultado_SistemaCod ;
      private bool[] P00YB33_n489ContagemResultado_SistemaCod ;
      private int[] P00YB33_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00YB33_n1553ContagemResultado_CntSrvCod ;
      private int[] P00YB33_A601ContagemResultado_Servico ;
      private bool[] P00YB33_n601ContagemResultado_Servico ;
      private int[] P00YB33_A508ContagemResultado_Owner ;
      private bool[] P00YB33_n508ContagemResultado_Owner ;
      private bool[] P00YB33_A1457ContagemResultado_TemDpnHmlg ;
      private bool[] P00YB33_n1457ContagemResultado_TemDpnHmlg ;
      private String[] P00YB33_A484ContagemResultado_StatusDmn ;
      private bool[] P00YB33_n484ContagemResultado_StatusDmn ;
      private int[] P00YB33_A490ContagemResultado_ContratadaCod ;
      private bool[] P00YB33_n490ContagemResultado_ContratadaCod ;
      private String[] P00YB33_A457ContagemResultado_Demanda ;
      private bool[] P00YB33_n457ContagemResultado_Demanda ;
      private String[] P00YB33_A493ContagemResultado_DemandaFM ;
      private bool[] P00YB33_n493ContagemResultado_DemandaFM ;
      private String[] P00YB33_A494ContagemResultado_Descricao ;
      private bool[] P00YB33_n494ContagemResultado_Descricao ;
      private String[] P00YB33_A801ContagemResultado_ServicoSigla ;
      private bool[] P00YB33_n801ContagemResultado_ServicoSigla ;
      private DateTime[] P00YB33_A471ContagemResultado_DataDmn ;
      private bool[] P00YB33_n471ContagemResultado_DataDmn ;
      private DateTime[] P00YB33_A1351ContagemResultado_DataPrevista ;
      private bool[] P00YB33_n1351ContagemResultado_DataPrevista ;
      private DateTime[] P00YB33_A472ContagemResultado_DataEntrega ;
      private bool[] P00YB33_n472ContagemResultado_DataEntrega ;
      private String[] P00YB33_A509ContagemrResultado_SistemaSigla ;
      private bool[] P00YB33_n509ContagemrResultado_SistemaSigla ;
      private String[] P00YB33_A1046ContagemResultado_Agrupador ;
      private bool[] P00YB33_n1046ContagemResultado_Agrupador ;
      private String[] P00YB33_A2118ContagemResultado_Owner_Identificao ;
      private bool[] P00YB33_n2118ContagemResultado_Owner_Identificao ;
      private int[] P00YB33_A456ContagemResultado_Codigo ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV49ContratadasGeridasCollection ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV64GXV1 ;
      [ObjectCollection(ItemType=typeof( SdtSDT_WS_Demandas ))]
      private IGxCollection AV23SDT_WS_Demandas ;
      private SdtSDT_WSAutenticacao AV13SDT_WSAutenticacao ;
      private SdtSDT_WS_ConsultarAutorizacaoUsuario AV26SDT_WS_ConsultarAutorizacaoUsuario ;
      private SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho AV27SDT_WS_ConsultarAutorizacaoUsuarioArea ;
      private SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato AV28SDT_WS_ConsultarAutorizacaoUsuarioContrato ;
      private SdtSDT_WS_Demandas AV22SDT_WS_DemandaItens ;
      private SdtSDT_WS_Demandas_Demanda AV50SDT_WS_DemandasDemandas ;
      private SdtSDT_WsFiltros AV12SDT_WsFiltros ;
      private wwpbaseobjects.SdtWWPContext AV31wwpContext ;
   }

   public class aws_meetrikaintegracao__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00YB14( IGxContext context ,
                                              int AV12SDT_WsFiltros_gxTpr_Areatrabalhocodigo ,
                                              String AV12SDT_WsFiltros_gxTpr_Areatrabalhonome ,
                                              int A5AreaTrabalho_Codigo ,
                                              String A6AreaTrabalho_Descricao )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [2] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT [AreaTrabalho_Descricao], [AreaTrabalho_Codigo], [Contratante_Codigo] FROM [AreaTrabalho] WITH (NOLOCK)";
         if ( ! (0==AV12SDT_WsFiltros_gxTpr_Areatrabalhocodigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([AreaTrabalho_Codigo] = @AV12SDT__6Areatrabalhocodigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([AreaTrabalho_Codigo] = @AV12SDT__6Areatrabalhocodigo)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( (0==AV12SDT_WsFiltros_gxTpr_Areatrabalhocodigo) && ! String.IsNullOrEmpty(StringUtil.RTrim( StringUtil.Trim( AV12SDT_WsFiltros_gxTpr_Areatrabalhonome))) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([AreaTrabalho_Descricao] = RTRIM(LTRIM(@AV12SDT__7Areatrabalhonome)))";
            }
            else
            {
               sWhereString = sWhereString + " ([AreaTrabalho_Descricao] = RTRIM(LTRIM(@AV12SDT__7Areatrabalhonome)))";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [AreaTrabalho_Codigo]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00YB16( IGxContext context ,
                                              int A490ContagemResultado_ContratadaCod ,
                                              IGxCollection AV49ContratadasGeridasCollection ,
                                              bool AV31wwpContext_gxTpr_Userehcontratada ,
                                              bool AV31wwpContext_gxTpr_Userehcontratante ,
                                              int AV31wwpContext_gxTpr_Contratada_codigo ,
                                              String A484ContagemResultado_StatusDmn ,
                                              int A508ContagemResultado_Owner ,
                                              short AV31wwpContext_gxTpr_Userid ,
                                              int A890ContagemResultado_Responsavel )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [5] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T3.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_Responsavel], T1.[ContagemResultado_Owner], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_ContratadaCod], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Descricao], T4.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_DataPrevista], T1.[ContagemResultado_DataEntrega], T2.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T1.[ContagemResultado_Agrupador], COALESCE( T5.[ContagemResultado_Owner_Identificao], '') AS ContagemResultado_Owner_Identificao, T1.[ContagemResultado_Codigo] FROM (((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T4 WITH (NOLOCK) ON T4.[Servico_Codigo] = T3.[Servico_Codigo]) LEFT JOIN (SELECT T7.[Pessoa_Nome] AS ContagemResultado_Owner_Identificao, T6.[Usuario_Codigo] FROM ([Usuario] T6 WITH (NOLOCK) INNER JOIN [Pessoa] T7 WITH (NOLOCK) ON T7.[Pessoa_Codigo] = T6.[Usuario_PessoaCod]) ) T5 ON T5.[Usuario_Codigo] = T1.[ContagemResultado_Owner])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContagemResultado_StatusDmn] = 'S' or T1.[ContagemResultado_StatusDmn] = 'J' or T1.[ContagemResultado_StatusDmn] = 'T' or T1.[ContagemResultado_StatusDmn] = 'G' or T1.[ContagemResultado_StatusDmn] = 'D')";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_Owner] = @AV31wwpContext__Userid or T1.[ContagemResultado_Responsavel] = @AV31wwpContext__Userid or ( T1.[ContagemResultado_Owner] <> @AV31wwpContext__Userid and T1.[ContagemResultado_Responsavel] <> @AV31wwpContext__Userid and T1.[ContagemResultado_StatusDmn] = 'D'))";
         if ( AV31wwpContext_gxTpr_Userehcontratada )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV31wwpC_8Contratada_codigo)";
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV31wwpContext_gxTpr_Userehcontratante )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV49ContratadasGeridasCollection, "T1.[ContagemResultado_ContratadaCod] IN (", ")") + ")";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_ContratadaCod], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_Responsavel]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      protected Object[] conditional_P00YB18( IGxContext context ,
                                              int A490ContagemResultado_ContratadaCod ,
                                              IGxCollection AV49ContratadasGeridasCollection ,
                                              bool AV31wwpContext_gxTpr_Userehcontratada ,
                                              bool AV31wwpContext_gxTpr_Userehcontratante ,
                                              int AV31wwpContext_gxTpr_Contratada_codigo ,
                                              String A484ContagemResultado_StatusDmn ,
                                              int A890ContagemResultado_Responsavel ,
                                              short AV31wwpContext_gxTpr_Userid )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int7 ;
         GXv_int7 = new short [2] ;
         Object[] GXv_Object8 ;
         GXv_Object8 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T3.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_Owner], T1.[ContagemResultado_Responsavel], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_ContratadaCod], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Descricao], T4.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_DataPrevista], T1.[ContagemResultado_DataEntrega], T2.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T1.[ContagemResultado_Agrupador], COALESCE( T5.[ContagemResultado_Owner_Identificao], '') AS ContagemResultado_Owner_Identificao, T1.[ContagemResultado_Codigo] FROM (((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T4 WITH (NOLOCK) ON T4.[Servico_Codigo] = T3.[Servico_Codigo]) LEFT JOIN (SELECT T7.[Pessoa_Nome] AS ContagemResultado_Owner_Identificao, T6.[Usuario_Codigo] FROM ([Usuario] T6 WITH (NOLOCK) INNER JOIN [Pessoa] T7 WITH (NOLOCK) ON T7.[Pessoa_Codigo] = T6.[Usuario_PessoaCod]) ) T5 ON T5.[Usuario_Codigo] = T1.[ContagemResultado_Owner])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContagemResultado_StatusDmn] = 'E' or T1.[ContagemResultado_StatusDmn] = 'I')";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_Responsavel] = @AV31wwpContext__Userid)";
         if ( AV31wwpContext_gxTpr_Userehcontratada )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV31wwpC_8Contratada_codigo)";
         }
         else
         {
            GXv_int7[1] = 1;
         }
         if ( AV31wwpContext_gxTpr_Userehcontratante )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV49ContratadasGeridasCollection, "T1.[ContagemResultado_ContratadaCod] IN (", ")") + ")";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_ContratadaCod], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_Responsavel]";
         GXv_Object8[0] = scmdbuf;
         GXv_Object8[1] = GXv_int7;
         return GXv_Object8 ;
      }

      protected Object[] conditional_P00YB21( IGxContext context ,
                                              int A490ContagemResultado_ContratadaCod ,
                                              IGxCollection AV49ContratadasGeridasCollection ,
                                              bool AV31wwpContext_gxTpr_Userehcontratada ,
                                              bool AV31wwpContext_gxTpr_Userehcontratante ,
                                              int AV31wwpContext_gxTpr_Contratada_codigo ,
                                              int A890ContagemResultado_Responsavel ,
                                              short AV31wwpContext_gxTpr_Userid ,
                                              short A531ContagemResultado_StatusUltCnt ,
                                              int A1236ContagemResultado_ContratanteDoResponsavel ,
                                              int AV31wwpContext_gxTpr_Contratante_codigo ,
                                              String A484ContagemResultado_StatusDmn )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int9 ;
         GXv_int9 = new short [2] ;
         Object[] GXv_Object10 ;
         GXv_Object10 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T3.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_Owner], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_ContratadaCod], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Descricao], T4.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_DataPrevista], T1.[ContagemResultado_DataEntrega], T2.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T1.[ContagemResultado_Agrupador], COALESCE( T6.[ContagemResultado_StatusUltCnt], 0) AS ContagemResultado_StatusUltCnt, COALESCE( T5.[ContagemResultado_Owner_Identificao], '') AS ContagemResultado_Owner_Identificao, T1.[ContagemResultado_Responsavel], T1.[ContagemResultado_Codigo] FROM ((((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T4 WITH (NOLOCK) ON T4.[Servico_Codigo] = T3.[Servico_Codigo]) LEFT JOIN (SELECT T8.[Pessoa_Nome] AS ContagemResultado_Owner_Identificao, T7.[Usuario_Codigo] FROM ([Usuario] T7 WITH (NOLOCK) INNER JOIN [Pessoa] T8 WITH (NOLOCK) ON T8.[Pessoa_Codigo] = T7.[Usuario_PessoaCod]) ) T5 ON T5.[Usuario_Codigo] = T1.[ContagemResultado_Owner]) LEFT JOIN (SELECT MIN([ContagemResultado_StatusCnt]) AS ContagemResultado_StatusUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T6 ON T6.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo])";
         scmdbuf = scmdbuf + " WHERE (Not COALESCE( T6.[ContagemResultado_StatusUltCnt], 0) = 7)";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_StatusDmn] = 'A')";
         if ( AV31wwpContext_gxTpr_Userehcontratada )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV31wwpC_8Contratada_codigo)";
         }
         else
         {
            GXv_int9[0] = 1;
         }
         if ( AV31wwpContext_gxTpr_Userehcontratante )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV49ContratadasGeridasCollection, "T1.[ContagemResultado_ContratadaCod] IN (", ")") + ")";
         }
         if ( AV31wwpContext_gxTpr_Userehcontratada )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Responsavel] = @AV31wwpContext__Userid)";
         }
         else
         {
            GXv_int9[1] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_ContratadaCod], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_Responsavel]";
         GXv_Object10[0] = scmdbuf;
         GXv_Object10[1] = GXv_int9;
         return GXv_Object10 ;
      }

      protected Object[] conditional_P00YB23( IGxContext context ,
                                              int A490ContagemResultado_ContratadaCod ,
                                              IGxCollection AV49ContratadasGeridasCollection ,
                                              bool AV31wwpContext_gxTpr_Userehcontratada ,
                                              bool AV31wwpContext_gxTpr_Userehcontratante ,
                                              int AV31wwpContext_gxTpr_Contratada_codigo ,
                                              String A484ContagemResultado_StatusDmn )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int11 ;
         GXv_int11 = new short [1] ;
         Object[] GXv_Object12 ;
         GXv_Object12 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T3.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_Owner], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_ContratadaCod], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Descricao], T4.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_DataPrevista], T1.[ContagemResultado_DataEntrega], T2.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T1.[ContagemResultado_Agrupador], T1.[ContagemResultado_Responsavel], COALESCE( T5.[ContagemResultado_Owner_Identificao], '') AS ContagemResultado_Owner_Identificao, T1.[ContagemResultado_Codigo] FROM (((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T4 WITH (NOLOCK) ON T4.[Servico_Codigo] = T3.[Servico_Codigo]) LEFT JOIN (SELECT T7.[Pessoa_Nome] AS ContagemResultado_Owner_Identificao, T6.[Usuario_Codigo] FROM ([Usuario] T6 WITH (NOLOCK) INNER JOIN [Pessoa] T7 WITH (NOLOCK) ON T7.[Pessoa_Codigo] = T6.[Usuario_PessoaCod]) ) T5 ON T5.[Usuario_Codigo] = T1.[ContagemResultado_Owner])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContagemResultado_StatusDmn] = 'B')";
         if ( AV31wwpContext_gxTpr_Userehcontratada )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV31wwpC_8Contratada_codigo)";
         }
         else
         {
            GXv_int11[0] = 1;
         }
         if ( AV31wwpContext_gxTpr_Userehcontratante )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV49ContratadasGeridasCollection, "T1.[ContagemResultado_ContratadaCod] IN (", ")") + ")";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_ContratadaCod], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_Responsavel]";
         GXv_Object12[0] = scmdbuf;
         GXv_Object12[1] = GXv_int11;
         return GXv_Object12 ;
      }

      protected Object[] conditional_P00YB26( IGxContext context ,
                                              int A490ContagemResultado_ContratadaCod ,
                                              IGxCollection AV49ContratadasGeridasCollection ,
                                              bool AV31wwpContext_gxTpr_Userehcontratada ,
                                              bool AV31wwpContext_gxTpr_Userehcontratante ,
                                              int AV31wwpContext_gxTpr_Contratada_codigo ,
                                              short A531ContagemResultado_StatusUltCnt ,
                                              String A484ContagemResultado_StatusDmn )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int13 ;
         GXv_int13 = new short [1] ;
         Object[] GXv_Object14 ;
         GXv_Object14 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T3.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_Owner], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_ContratadaCod], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Descricao], T4.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_DataPrevista], T1.[ContagemResultado_DataEntrega], T2.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T1.[ContagemResultado_Agrupador], COALESCE( T5.[ContagemResultado_StatusUltCnt], 0) AS ContagemResultado_StatusUltCnt, COALESCE( T6.[ContagemResultado_Owner_Identificao], '') AS ContagemResultado_Owner_Identificao FROM ((((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T4 WITH (NOLOCK) ON T4.[Servico_Codigo] = T3.[Servico_Codigo]) LEFT JOIN (SELECT MIN([ContagemResultado_StatusCnt]) AS ContagemResultado_StatusUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T5 ON T5.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN (SELECT T8.[Pessoa_Nome] AS ContagemResultado_Owner_Identificao, T7.[Usuario_Codigo] FROM ([Usuario] T7 WITH (NOLOCK) INNER JOIN [Pessoa] T8 WITH (NOLOCK) ON T8.[Pessoa_Codigo] = T7.[Usuario_PessoaCod]) ) T6 ON T6.[Usuario_Codigo] = T1.[ContagemResultado_Owner])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContagemResultado_StatusDmn] = 'A')";
         scmdbuf = scmdbuf + " and (COALESCE( T5.[ContagemResultado_StatusUltCnt], 0) = 7)";
         if ( AV31wwpContext_gxTpr_Userehcontratada )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV31wwpC_8Contratada_codigo)";
         }
         else
         {
            GXv_int13[0] = 1;
         }
         if ( AV31wwpContext_gxTpr_Userehcontratante )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV49ContratadasGeridasCollection, "T1.[ContagemResultado_ContratadaCod] IN (", ")") + ")";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_StatusDmn]";
         GXv_Object14[0] = scmdbuf;
         GXv_Object14[1] = GXv_int13;
         return GXv_Object14 ;
      }

      protected Object[] conditional_P00YB29( IGxContext context ,
                                              int A490ContagemResultado_ContratadaCod ,
                                              IGxCollection AV49ContratadasGeridasCollection ,
                                              bool AV31wwpContext_gxTpr_Userehcontratada ,
                                              bool AV31wwpContext_gxTpr_Userehcontratante ,
                                              int AV31wwpContext_gxTpr_Contratada_codigo ,
                                              int A1992ContagemResultadoQA_ParaCod ,
                                              short AV31wwpContext_gxTpr_Userid ,
                                              int A1996ContagemResultadoQA_RespostaDe ,
                                              int A1998ContagemResultadoQA_Resposta )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int15 ;
         GXv_int15 = new short [2] ;
         Object[] GXv_Object16 ;
         GXv_Object16 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultadoQA_OSCod] AS ContagemResultadoQA_OSCod, T2.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T4.[Servico_Codigo] AS ContagemResultado_Servico, T2.[ContagemResultado_Owner], T1.[ContagemResultadoQA_RespostaDe], T1.[ContagemResultadoQA_ParaCod], T2.[ContagemResultado_ContratadaCod], T2.[ContagemResultado_Demanda], T2.[ContagemResultado_DemandaFM], T2.[ContagemResultado_Descricao], T5.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T2.[ContagemResultado_StatusDmn], T2.[ContagemResultado_DataDmn], T2.[ContagemResultado_DataPrevista], T2.[ContagemResultado_DataEntrega], T3.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T2.[ContagemResultado_Agrupador], COALESCE( T6.[ContagemResultado_Owner_Identificao], '') AS ContagemResultado_Owner_Identificao, T1.[ContagemResultadoQA_Codigo] FROM ((((([ContagemResultadoQA] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultadoQA_OSCod]) LEFT JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = T2.[ContagemResultado_SistemaCod]) LEFT JOIN [ContratoServicos] T4 WITH (NOLOCK) ON T4.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T5 WITH (NOLOCK) ON T5.[Servico_Codigo] = T4.[Servico_Codigo]) LEFT JOIN (SELECT T8.[Pessoa_Nome] AS ContagemResultado_Owner_Identificao, T7.[Usuario_Codigo] FROM ([Usuario] T7 WITH (NOLOCK) INNER JOIN [Pessoa] T8 WITH (NOLOCK) ON T8.[Pessoa_Codigo] = T7.[Usuario_PessoaCod]) ) T6 ON T6.[Usuario_Codigo] = T2.[ContagemResultado_Owner])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContagemResultadoQA_RespostaDe] IS NULL)";
         if ( AV31wwpContext_gxTpr_Userehcontratada )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaCod] = @AV31wwpC_8Contratada_codigo)";
         }
         else
         {
            GXv_int15[0] = 1;
         }
         if ( AV31wwpContext_gxTpr_Userehcontratante )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV49ContratadasGeridasCollection, "T2.[ContagemResultado_ContratadaCod] IN (", ")") + ")";
         }
         if ( ! AV31wwpContext_gxTpr_Userehcontratante )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultadoQA_ParaCod] = @AV31wwpContext__Userid)";
         }
         else
         {
            GXv_int15[1] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T2.[ContagemResultado_ContratadaCod]";
         GXv_Object16[0] = scmdbuf;
         GXv_Object16[1] = GXv_int15;
         return GXv_Object16 ;
      }

      protected Object[] conditional_P00YB31( IGxContext context ,
                                              int A490ContagemResultado_ContratadaCod ,
                                              IGxCollection AV49ContratadasGeridasCollection ,
                                              bool AV31wwpContext_gxTpr_Userehcontratada ,
                                              bool AV31wwpContext_gxTpr_Userehcontratante ,
                                              int AV31wwpContext_gxTpr_Contratada_codigo ,
                                              String A484ContagemResultado_StatusDmn ,
                                              int A508ContagemResultado_Owner ,
                                              short AV31wwpContext_gxTpr_Userid ,
                                              int A1603ContagemResultado_CntCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int17 ;
         GXv_int17 = new short [1] ;
         Object[] GXv_Object18 ;
         GXv_Object18 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T3.[Servico_Codigo] AS ContagemResultado_Servico, T3.[Contrato_Codigo] AS ContagemResultado_CntCod, T1.[ContagemResultado_Owner], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_ContratadaCod], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Descricao], T4.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_DataPrevista], T1.[ContagemResultado_DataEntrega], T2.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T1.[ContagemResultado_Agrupador], T1.[ContagemResultado_Responsavel], COALESCE( T5.[ContagemResultado_Owner_Identificao], '') AS ContagemResultado_Owner_Identificao, T1.[ContagemResultado_Codigo] FROM (((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T4 WITH (NOLOCK) ON T4.[Servico_Codigo] = T3.[Servico_Codigo]) LEFT JOIN (SELECT T7.[Pessoa_Nome] AS ContagemResultado_Owner_Identificao, T6.[Usuario_Codigo] FROM ([Usuario] T6 WITH (NOLOCK) INNER JOIN [Pessoa] T7 WITH (NOLOCK) ON T7.[Pessoa_Codigo] = T6.[Usuario_PessoaCod]) ) T5 ON T5.[Usuario_Codigo] = T1.[ContagemResultado_Owner])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContagemResultado_StatusDmn] = 'B' or T1.[ContagemResultado_StatusDmn] = 'S' or T1.[ContagemResultado_StatusDmn] = 'E' or T1.[ContagemResultado_StatusDmn] = 'I' or T1.[ContagemResultado_StatusDmn] = 'A' or T1.[ContagemResultado_StatusDmn] = 'J' or T1.[ContagemResultado_StatusDmn] = 'T' or T1.[ContagemResultado_StatusDmn] = 'Q' or T1.[ContagemResultado_StatusDmn] = 'G' or T1.[ContagemResultado_StatusDmn] = 'M' or T1.[ContagemResultado_StatusDmn] = 'D')";
         if ( AV31wwpContext_gxTpr_Userehcontratada )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV31wwpC_8Contratada_codigo)";
         }
         else
         {
            GXv_int17[0] = 1;
         }
         if ( AV31wwpContext_gxTpr_Userehcontratante )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV49ContratadasGeridasCollection, "T1.[ContagemResultado_ContratadaCod] IN (", ")") + ")";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_ContratadaCod], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_Responsavel]";
         GXv_Object18[0] = scmdbuf;
         GXv_Object18[1] = GXv_int17;
         return GXv_Object18 ;
      }

      protected Object[] conditional_P00YB33( IGxContext context ,
                                              int A490ContagemResultado_ContratadaCod ,
                                              IGxCollection AV49ContratadasGeridasCollection ,
                                              bool AV31wwpContext_gxTpr_Userehcontratada ,
                                              bool AV31wwpContext_gxTpr_Userehcontratante ,
                                              int AV31wwpContext_gxTpr_Contratada_codigo ,
                                              String A484ContagemResultado_StatusDmn ,
                                              bool A1457ContagemResultado_TemDpnHmlg )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int19 ;
         GXv_int19 = new short [1] ;
         Object[] GXv_Object20 ;
         GXv_Object20 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T3.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_Owner], T1.[ContagemResultado_TemDpnHmlg], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_ContratadaCod], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Descricao], T4.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_DataPrevista], T1.[ContagemResultado_DataEntrega], T2.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T1.[ContagemResultado_Agrupador], COALESCE( T5.[ContagemResultado_Owner_Identificao], '') AS ContagemResultado_Owner_Identificao, T1.[ContagemResultado_Codigo] FROM (((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T4 WITH (NOLOCK) ON T4.[Servico_Codigo] = T3.[Servico_Codigo]) LEFT JOIN (SELECT T7.[Pessoa_Nome] AS ContagemResultado_Owner_Identificao, T6.[Usuario_Codigo] FROM ([Usuario] T6 WITH (NOLOCK) INNER JOIN [Pessoa] T7 WITH (NOLOCK) ON T7.[Pessoa_Codigo] = T6.[Usuario_PessoaCod]) ) T5 ON T5.[Usuario_Codigo] = T1.[ContagemResultado_Owner])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContagemResultado_StatusDmn] = 'R' or T1.[ContagemResultado_StatusDmn] = 'C')";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_TemDpnHmlg] IS NULL or Not T1.[ContagemResultado_TemDpnHmlg] = 1)";
         if ( AV31wwpContext_gxTpr_Userehcontratada )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV31wwpC_8Contratada_codigo)";
         }
         else
         {
            GXv_int19[0] = 1;
         }
         if ( AV31wwpContext_gxTpr_Userehcontratante )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV49ContratadasGeridasCollection, "T1.[ContagemResultado_ContratadaCod] IN (", ")") + ")";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Codigo]";
         GXv_Object20[0] = scmdbuf;
         GXv_Object20[1] = GXv_int19;
         return GXv_Object20 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 11 :
                     return conditional_P00YB14(context, (int)dynConstraints[0] , (String)dynConstraints[1] , (int)dynConstraints[2] , (String)dynConstraints[3] );
               case 12 :
                     return conditional_P00YB16(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (bool)dynConstraints[2] , (bool)dynConstraints[3] , (int)dynConstraints[4] , (String)dynConstraints[5] , (int)dynConstraints[6] , (short)dynConstraints[7] , (int)dynConstraints[8] );
               case 13 :
                     return conditional_P00YB18(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (bool)dynConstraints[2] , (bool)dynConstraints[3] , (int)dynConstraints[4] , (String)dynConstraints[5] , (int)dynConstraints[6] , (short)dynConstraints[7] );
               case 14 :
                     return conditional_P00YB21(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (bool)dynConstraints[2] , (bool)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] , (short)dynConstraints[6] , (short)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] , (String)dynConstraints[10] );
               case 15 :
                     return conditional_P00YB23(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (bool)dynConstraints[2] , (bool)dynConstraints[3] , (int)dynConstraints[4] , (String)dynConstraints[5] );
               case 16 :
                     return conditional_P00YB26(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (bool)dynConstraints[2] , (bool)dynConstraints[3] , (int)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] );
               case 17 :
                     return conditional_P00YB29(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (bool)dynConstraints[2] , (bool)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] , (short)dynConstraints[6] , (int)dynConstraints[7] , (int)dynConstraints[8] );
               case 18 :
                     return conditional_P00YB31(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (bool)dynConstraints[2] , (bool)dynConstraints[3] , (int)dynConstraints[4] , (String)dynConstraints[5] , (int)dynConstraints[6] , (short)dynConstraints[7] , (int)dynConstraints[8] );
               case 19 :
                     return conditional_P00YB33(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (bool)dynConstraints[2] , (bool)dynConstraints[3] , (int)dynConstraints[4] , (String)dynConstraints[5] , (bool)dynConstraints[6] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00YB2 ;
          prmP00YB2 = new Object[] {
          new Object[] {"@AV10Retorno",SqlDbType.VarChar,2097152,0}
          } ;
          Object[] prmP00YB3 ;
          prmP00YB3 = new Object[] {
          new Object[] {"@AV31wwpContext__Userid",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV31wwpC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00YB4 ;
          prmP00YB4 = new Object[] {
          new Object[] {"@AV31wwpContext__Userid",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV31wwpC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00YB6 ;
          prmP00YB6 = new Object[] {
          new Object[] {"@AV31wwpContext__Userid",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV31wwpC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00YB7 ;
          prmP00YB7 = new Object[] {
          new Object[] {"@AV31wwpContext__Userid",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV31wwpC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00YB8 ;
          prmP00YB8 = new Object[] {
          new Object[] {"@AV26SDT__2Pessoa_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00YB9 ;
          prmP00YB9 = new Object[] {
          new Object[] {"@AV27SDT__3Usuario_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV27SDT__4Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00YB10 ;
          prmP00YB10 = new Object[] {
          new Object[] {"@AV27SDT__4Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00YB11 ;
          prmP00YB11 = new Object[] {
          new Object[] {"@AV26SDT__2Pessoa_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00YB12 ;
          prmP00YB12 = new Object[] {
          new Object[] {"@AV27SDT__3Usuario_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV27SDT__4Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00YB13 ;
          prmP00YB13 = new Object[] {
          new Object[] {"@AV27SDT__4Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV27SDT__5Contratada_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00YB14 ;
          prmP00YB14 = new Object[] {
          new Object[] {"@AV12SDT__6Areatrabalhocodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12SDT__7Areatrabalhonome",SqlDbType.VarChar,50,0}
          } ;
          Object[] prmP00YB16 ;
          prmP00YB16 = new Object[] {
          new Object[] {"@AV31wwpContext__Userid",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV31wwpContext__Userid",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV31wwpContext__Userid",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV31wwpContext__Userid",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV31wwpC_8Contratada_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00YB18 ;
          prmP00YB18 = new Object[] {
          new Object[] {"@AV31wwpContext__Userid",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV31wwpC_8Contratada_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00YB21 ;
          prmP00YB21 = new Object[] {
          new Object[] {"@AV31wwpC_8Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV31wwpContext__Userid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmP00YB23 ;
          prmP00YB23 = new Object[] {
          new Object[] {"@AV31wwpC_8Contratada_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00YB26 ;
          prmP00YB26 = new Object[] {
          new Object[] {"@AV31wwpC_8Contratada_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00YB29 ;
          prmP00YB29 = new Object[] {
          new Object[] {"@AV31wwpC_8Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV31wwpContext__Userid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmP00YB31 ;
          prmP00YB31 = new Object[] {
          new Object[] {"@AV31wwpC_8Contratada_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00YB33 ;
          prmP00YB33 = new Object[] {
          new Object[] {"@AV31wwpC_8Contratada_codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00YB2", "SELECT T1.[Usuario_Ativo], T1.[Usuario_UserGamGuid], T1.[Usuario_Codigo], T1.[Usuario_Nome], T1.[Usuario_PessoaCod] AS Usuario_PessoaCod, T2.[Pessoa_Nome] AS Usuario_PessoaNom FROM ([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) WHERE (T1.[Usuario_UserGamGuid] = RTRIM(LTRIM(@AV10Retorno))) AND (T1.[Usuario_Ativo] = 1) ORDER BY T1.[Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YB2,100,0,false,false )
             ,new CursorDef("P00YB3", "SELECT TOP 1 T1.[ContratadaUsuario_UsuarioCod], T2.[Contratada_AreaTrabalhoCod] AS ContratadaUsuario_AreaTrabalhoCod, T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod FROM ([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) WHERE (T1.[ContratadaUsuario_UsuarioCod] = @AV31wwpContext__Userid) AND (T2.[Contratada_AreaTrabalhoCod] = @AV31wwpC_1Areatrabalho_codigo) ORDER BY T1.[ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YB3,1,0,false,true )
             ,new CursorDef("P00YB4", "SELECT T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T1.[ContratoGestor_UsuarioCod], T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod, T2.[Contratada_Codigo] AS ContratoGestor_ContratadaCod FROM ([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) WHERE (T1.[ContratoGestor_UsuarioCod] = @AV31wwpContext__Userid) AND (T2.[Contrato_AreaTrabalhoCod] = @AV31wwpC_1Areatrabalho_codigo) ORDER BY T1.[ContratoGestor_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YB4,100,0,false,false )
             ,new CursorDef("P00YB6", "SELECT T1.[ContratanteUsuario_UsuarioCod], T1.[ContratanteUsuario_ContratanteCod], COALESCE( T2.[ContratanteUsuario_AreaTrabalhoCod], 0) AS ContratanteUsuario_AreaTrabalhoCod FROM ([ContratanteUsuario] T1 WITH (NOLOCK) LEFT JOIN (SELECT MIN(T3.[AreaTrabalho_Codigo]) AS ContratanteUsuario_AreaTrabalhoCod, T4.[ContratanteUsuario_ContratanteCod], T4.[ContratanteUsuario_UsuarioCod] FROM [AreaTrabalho] T3 WITH (NOLOCK),  [ContratanteUsuario] T4 WITH (NOLOCK) WHERE T3.[Contratante_Codigo] = T4.[ContratanteUsuario_ContratanteCod] GROUP BY T4.[ContratanteUsuario_ContratanteCod], T4.[ContratanteUsuario_UsuarioCod] ) T2 ON T2.[ContratanteUsuario_ContratanteCod] = T1.[ContratanteUsuario_ContratanteCod] AND T2.[ContratanteUsuario_UsuarioCod] = T1.[ContratanteUsuario_UsuarioCod]) WHERE (T1.[ContratanteUsuario_UsuarioCod] = @AV31wwpContext__Userid) AND (COALESCE( T2.[ContratanteUsuario_AreaTrabalhoCod], 0) = @AV31wwpC_1Areatrabalho_codigo) ORDER BY T1.[ContratanteUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YB6,100,0,false,false )
             ,new CursorDef("P00YB7", "SELECT T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T1.[ContratoGestor_UsuarioCod], T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod, T2.[Contratada_Codigo] AS ContratoGestor_ContratadaCod FROM ([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) WHERE (T1.[ContratoGestor_UsuarioCod] = @AV31wwpContext__Userid) AND (T2.[Contrato_AreaTrabalhoCod] = @AV31wwpC_1Areatrabalho_codigo) ORDER BY T1.[ContratoGestor_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YB7,100,0,false,false )
             ,new CursorDef("P00YB8", "SELECT T2.[Contratante_PessoaCod] AS Contratante_PessoaCod, T1.[AreaTrabalho_Codigo], T1.[AreaTrabalho_Descricao], T1.[Contratante_Codigo], T3.[Pessoa_Nome] AS Contratante_RazaoSocial FROM (([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[Contratante_Codigo]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratante_PessoaCod]) WHERE T2.[Contratante_PessoaCod] = @AV26SDT__2Pessoa_codigo ORDER BY T1.[AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YB8,100,0,true,false )
             ,new CursorDef("P00YB9", "SELECT T2.[Perfil_AreaTrabalhoCod], T1.[Usuario_Codigo], T1.[Perfil_Codigo], T2.[Perfil_Nome] FROM ([UsuarioPerfil] T1 WITH (NOLOCK) INNER JOIN [Perfil] T2 WITH (NOLOCK) ON T2.[Perfil_Codigo] = T1.[Perfil_Codigo]) WHERE (T1.[Usuario_Codigo] = @AV27SDT__3Usuario_codigo) AND (T2.[Perfil_AreaTrabalhoCod] = @AV27SDT__4Areatrabalho_codigo) ORDER BY T1.[Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YB9,100,0,false,false )
             ,new CursorDef("P00YB10", "SELECT [Contrato_AreaTrabalhoCod], [Contrato_Codigo], [Contrato_Numero] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_AreaTrabalhoCod] = @AV27SDT__4Areatrabalho_codigo ORDER BY [Contrato_AreaTrabalhoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YB10,100,0,false,false )
             ,new CursorDef("P00YB11", "SELECT T1.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T3.[AreaTrabalho_Descricao] AS Contratada_AreaTrabalhoDes, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom FROM (([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) INNER JOIN [AreaTrabalho] T3 WITH (NOLOCK) ON T3.[AreaTrabalho_Codigo] = T1.[Contratada_AreaTrabalhoCod]) WHERE T1.[Contratada_PessoaCod] = @AV26SDT__2Pessoa_codigo ORDER BY T1.[Contratada_PessoaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YB11,100,0,true,false )
             ,new CursorDef("P00YB12", "SELECT T2.[Perfil_AreaTrabalhoCod], T1.[Usuario_Codigo], T1.[Perfil_Codigo], T2.[Perfil_Nome] FROM ([UsuarioPerfil] T1 WITH (NOLOCK) INNER JOIN [Perfil] T2 WITH (NOLOCK) ON T2.[Perfil_Codigo] = T1.[Perfil_Codigo]) WHERE (T1.[Usuario_Codigo] = @AV27SDT__3Usuario_codigo) AND (T2.[Perfil_AreaTrabalhoCod] = @AV27SDT__4Areatrabalho_codigo) ORDER BY T1.[Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YB12,100,0,false,false )
             ,new CursorDef("P00YB13", "SELECT [Contratada_Codigo], [Contrato_AreaTrabalhoCod], [Contrato_Codigo], [Contrato_Numero] FROM [Contrato] WITH (NOLOCK) WHERE ([Contrato_AreaTrabalhoCod] = @AV27SDT__4Areatrabalho_codigo) AND ([Contratada_Codigo] = @AV27SDT__5Contratada_codigo) ORDER BY [Contrato_AreaTrabalhoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YB13,100,0,false,false )
             ,new CursorDef("P00YB14", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YB14,100,0,false,false )
             ,new CursorDef("P00YB16", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YB16,100,0,false,false )
             ,new CursorDef("P00YB18", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YB18,100,0,false,false )
             ,new CursorDef("P00YB21", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YB21,100,0,true,false )
             ,new CursorDef("P00YB23", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YB23,100,0,false,false )
             ,new CursorDef("P00YB26", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YB26,100,0,false,false )
             ,new CursorDef("P00YB29", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YB29,100,0,true,false )
             ,new CursorDef("P00YB31", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YB31,100,0,true,false )
             ,new CursorDef("P00YB33", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YB33,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 40) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 20) ;
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((String[]) buf[9])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((String[]) buf[17])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((String[]) buf[19])[0] = rslt.getString(11, 15) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[21])[0] = rslt.getGXDate(12) ;
                ((DateTime[]) buf[22])[0] = rslt.getGXDateTime(13) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((DateTime[]) buf[24])[0] = rslt.getGXDate(14) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((String[]) buf[26])[0] = rslt.getString(15, 25) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                ((String[]) buf[28])[0] = rslt.getString(16, 15) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(16);
                ((String[]) buf[30])[0] = rslt.getVarchar(17) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(17);
                ((int[]) buf[32])[0] = rslt.getInt(18) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((String[]) buf[17])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((String[]) buf[19])[0] = rslt.getString(11, 15) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[21])[0] = rslt.getGXDate(12) ;
                ((DateTime[]) buf[22])[0] = rslt.getGXDateTime(13) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((DateTime[]) buf[24])[0] = rslt.getGXDate(14) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((String[]) buf[26])[0] = rslt.getString(15, 25) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                ((String[]) buf[28])[0] = rslt.getString(16, 15) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(16);
                ((String[]) buf[30])[0] = rslt.getVarchar(17) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(17);
                ((int[]) buf[32])[0] = rslt.getInt(18) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((String[]) buf[7])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((String[]) buf[17])[0] = rslt.getString(10, 15) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((DateTime[]) buf[19])[0] = rslt.getGXDate(11) ;
                ((DateTime[]) buf[20])[0] = rslt.getGXDateTime(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((DateTime[]) buf[22])[0] = rslt.getGXDate(13) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((String[]) buf[24])[0] = rslt.getString(14, 25) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((String[]) buf[26])[0] = rslt.getString(15, 15) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                ((short[]) buf[28])[0] = rslt.getShort(16) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(16);
                ((String[]) buf[30])[0] = rslt.getVarchar(17) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(17);
                ((int[]) buf[32])[0] = rslt.getInt(18) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(18);
                ((int[]) buf[34])[0] = rslt.getInt(19) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((String[]) buf[7])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((String[]) buf[17])[0] = rslt.getString(10, 15) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((DateTime[]) buf[19])[0] = rslt.getGXDate(11) ;
                ((DateTime[]) buf[20])[0] = rslt.getGXDateTime(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((DateTime[]) buf[22])[0] = rslt.getGXDate(13) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((String[]) buf[24])[0] = rslt.getString(14, 25) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((String[]) buf[26])[0] = rslt.getString(15, 15) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                ((int[]) buf[28])[0] = rslt.getInt(16) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(16);
                ((String[]) buf[30])[0] = rslt.getVarchar(17) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(17);
                ((int[]) buf[32])[0] = rslt.getInt(18) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((String[]) buf[8])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getString(11, 15) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[20])[0] = rslt.getGXDate(12) ;
                ((DateTime[]) buf[21])[0] = rslt.getGXDateTime(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((DateTime[]) buf[23])[0] = rslt.getGXDate(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((String[]) buf[25])[0] = rslt.getString(15, 25) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((String[]) buf[27])[0] = rslt.getString(16, 15) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                ((short[]) buf[29])[0] = rslt.getShort(17) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(17);
                ((String[]) buf[31])[0] = rslt.getVarchar(18) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(18);
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getVarchar(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((String[]) buf[20])[0] = rslt.getString(12, 15) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((String[]) buf[22])[0] = rslt.getString(13, 1) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((DateTime[]) buf[24])[0] = rslt.getGXDate(14) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((DateTime[]) buf[26])[0] = rslt.getGXDateTime(15) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                ((DateTime[]) buf[28])[0] = rslt.getGXDate(16) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(16);
                ((String[]) buf[30])[0] = rslt.getString(17, 25) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(17);
                ((String[]) buf[32])[0] = rslt.getString(18, 15) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(18);
                ((String[]) buf[34])[0] = rslt.getVarchar(19) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(19);
                ((int[]) buf[36])[0] = rslt.getInt(20) ;
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((String[]) buf[9])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((String[]) buf[17])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((String[]) buf[19])[0] = rslt.getString(11, 15) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[21])[0] = rslt.getGXDate(12) ;
                ((DateTime[]) buf[22])[0] = rslt.getGXDateTime(13) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((DateTime[]) buf[24])[0] = rslt.getGXDate(14) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((String[]) buf[26])[0] = rslt.getString(15, 25) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                ((String[]) buf[28])[0] = rslt.getString(16, 15) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(16);
                ((int[]) buf[30])[0] = rslt.getInt(17) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(17);
                ((String[]) buf[32])[0] = rslt.getVarchar(18) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(18);
                ((int[]) buf[34])[0] = rslt.getInt(19) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.getBool(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((String[]) buf[17])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((String[]) buf[19])[0] = rslt.getString(11, 15) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[21])[0] = rslt.getGXDate(12) ;
                ((DateTime[]) buf[22])[0] = rslt.getGXDateTime(13) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((DateTime[]) buf[24])[0] = rslt.getGXDate(14) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((String[]) buf[26])[0] = rslt.getString(15, 25) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                ((String[]) buf[28])[0] = rslt.getString(16, 15) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(16);
                ((String[]) buf[30])[0] = rslt.getVarchar(17) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(17);
                ((int[]) buf[32])[0] = rslt.getInt(18) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 3 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 4 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 11 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[2]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[3]);
                }
                return;
             case 12 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[5]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[6]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[7]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[8]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                return;
             case 13 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[2]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[3]);
                }
                return;
             case 14 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[2]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[3]);
                }
                return;
             case 15 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[1]);
                }
                return;
             case 16 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[1]);
                }
                return;
             case 17 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[2]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[3]);
                }
                return;
             case 18 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[1]);
                }
                return;
             case 19 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[1]);
                }
                return;
       }
    }

 }

}
