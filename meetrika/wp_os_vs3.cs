/*
               File: WP_OS_Vs3
        Description: Nova Ordem de Servi�o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/27/2020 10:5:15.51
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_os_vs3 : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_os_vs3( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_os_vs3( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavSdt_wp_os_contagemresultado_servicogrupo = new GXCombobox();
         dynavSdt_wp_os_contagemresultado_servico = new GXCombobox();
         cmbavSdt_wp_os_contratoservicosprazo_complexidade = new GXCombobox();
         dynavSdt_wp_os_contagemresultado_contratadacod = new GXCombobox();
         dynavSdt_wp_os_contagemresultado_contratadaorigemcod = new GXCombobox();
         dynavSdt_wp_os_contagemresultado_contadorfscod = new GXCombobox();
         cmbavSdt_wp_os_contratoservicos_localexec = new GXCombobox();
         dynavSdt_wp_os_modulo_codigo = new GXCombobox();
         dynavSdt_wp_os_contagemresultado_fncusrcod = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"SDT_WP_OS_CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
            {
               AV47Wwpcontext = NumberUtil.Val( GetNextPar( ), ".");
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVSDT_WP_OS_CONTAGEMRESULTADO_CONTRATADACODT42( AV47Wwpcontext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"SDT_WP_OS_CONTAGEMRESULTADO_CONTRATADAORIGEMCOD") == 0 )
            {
               AV47Wwpcontext = NumberUtil.Val( GetNextPar( ), ".");
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVSDT_WP_OS_CONTAGEMRESULTADO_CONTRATADAORIGEMCODT42( AV47Wwpcontext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"SDT_WP_OS_CONTAGEMRESULTADO_CONTADORFSCOD") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVSDT_WP_OS_CONTAGEMRESULTADO_CONTADORFSCODT42( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"SDT_WP_OS_MODULO_CODIGO") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV9SDT_WP_OS);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVSDT_WP_OS_MODULO_CODIGOT42( AV9SDT_WP_OS) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"SDT_WP_OS_CONTAGEMRESULTADO_FNCUSRCOD") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV9SDT_WP_OS);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVSDT_WP_OS_CONTAGEMRESULTADO_FNCUSRCODT42( AV9SDT_WP_OS) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAT42( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTT42( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203271051555");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/TabsPanel/BootstrapTabsPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_os_vs3.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "Sdt_wp_os", AV9SDT_WP_OS);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("Sdt_wp_os", AV9SDT_WP_OS);
         }
         GxWebStd.gx_hidden_field( context, "vWWPCONTEXT", StringUtil.LTrim( StringUtil.NToC( AV47Wwpcontext, 10, 2, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_WP_OS", AV9SDT_WP_OS);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_WP_OS", AV9SDT_WP_OS);
         }
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABMAIN_Width", StringUtil.RTrim( Gxuitabspanel_tabmain_Width));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABMAIN_Cls", StringUtil.RTrim( Gxuitabspanel_tabmain_Cls));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABMAIN_Autowidth", StringUtil.BoolToStr( Gxuitabspanel_tabmain_Autowidth));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABMAIN_Autoheight", StringUtil.BoolToStr( Gxuitabspanel_tabmain_Autoheight));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABMAIN_Autoscroll", StringUtil.BoolToStr( Gxuitabspanel_tabmain_Autoscroll));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABMAIN_Designtimetabs", StringUtil.RTrim( Gxuitabspanel_tabmain_Designtimetabs));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         if ( ! ( WebComp_Wc_contagemresultadoevidencias == null ) )
         {
            WebComp_Wc_contagemresultadoevidencias.componentjscripts();
         }
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WET42( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTT42( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_os_vs3.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WP_OS_Vs3" ;
      }

      public override String GetPgmdesc( )
      {
         return "Nova Ordem de Servi�o" ;
      }

      protected void WBT40( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_T42( true) ;
         }
         else
         {
            wb_table1_2_T42( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_T42e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 210,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSdt_wp_os_contagemresultado_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9SDT_WP_OS.gxTpr_Contagemresultado_codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV9SDT_WP_OS.gxTpr_Contagemresultado_codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,210);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSdt_wp_os_contagemresultado_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavSdt_wp_os_contagemresultado_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_OS_Vs3.htm");
         }
         wbLoad = true;
      }

      protected void STARTT42( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Nova Ordem de Servi�o", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPT40( ) ;
      }

      protected void WST42( )
      {
         STARTT42( ) ;
         EVTT42( ) ;
      }

      protected void EVTT42( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11T42 */
                              E11T42 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E12T42 */
                                    E12T42 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13T42 */
                              E13T42 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14T42 */
                              E14T42 ();
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     else if ( StringUtil.StrCmp(sEvtType, "W") == 0 )
                     {
                        sEvtType = StringUtil.Left( sEvt, 4);
                        sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        nCmpId = (short)(NumberUtil.Val( sEvtType, "."));
                        if ( nCmpId == 187 )
                        {
                           OldWc_contagemresultadoevidencias = cgiGet( "W0187");
                           if ( ( StringUtil.Len( OldWc_contagemresultadoevidencias) == 0 ) || ( StringUtil.StrCmp(OldWc_contagemresultadoevidencias, WebComp_Wc_contagemresultadoevidencias_Component) != 0 ) )
                           {
                              WebComp_Wc_contagemresultadoevidencias = getWebComponent(GetType(), "GeneXus.Programs", OldWc_contagemresultadoevidencias, new Object[] {context} );
                              WebComp_Wc_contagemresultadoevidencias.ComponentInit();
                              WebComp_Wc_contagemresultadoevidencias.Name = "OldWc_contagemresultadoevidencias";
                              WebComp_Wc_contagemresultadoevidencias_Component = OldWc_contagemresultadoevidencias;
                           }
                           if ( StringUtil.Len( WebComp_Wc_contagemresultadoevidencias_Component) != 0 )
                           {
                              WebComp_Wc_contagemresultadoevidencias.componentprocess("W0187", "", sEvt);
                           }
                           WebComp_Wc_contagemresultadoevidencias_Component = OldWc_contagemresultadoevidencias;
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WET42( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAT42( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynavSdt_wp_os_contagemresultado_servicogrupo.Name = "SDT_WP_OS_CONTAGEMRESULTADO_SERVICOGRUPO";
            dynavSdt_wp_os_contagemresultado_servicogrupo.WebTags = "";
            dynavSdt_wp_os_contagemresultado_servicogrupo.removeAllItems();
            /* Using cursor H00T42 */
            pr_default.execute(0);
            while ( (pr_default.getStatus(0) != 101) )
            {
               dynavSdt_wp_os_contagemresultado_servicogrupo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(H00T42_A157ServicoGrupo_Codigo[0]), 6, 0)), H00T42_A158ServicoGrupo_Descricao[0], 0);
               pr_default.readNext(0);
            }
            pr_default.close(0);
            if ( dynavSdt_wp_os_contagemresultado_servicogrupo.ItemCount > 0 )
            {
               AV9SDT_WP_OS.gxTpr_Contagemresultado_servicogrupo = (int)(NumberUtil.Val( dynavSdt_wp_os_contagemresultado_servicogrupo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV9SDT_WP_OS.gxTpr_Contagemresultado_servicogrupo), 6, 0))), "."));
            }
            dynavSdt_wp_os_contagemresultado_servico.Name = "SDT_WP_OS_CONTAGEMRESULTADO_SERVICO";
            dynavSdt_wp_os_contagemresultado_servico.WebTags = "";
            dynavSdt_wp_os_contagemresultado_servico.removeAllItems();
            /* Using cursor H00T43 */
            pr_default.execute(1);
            while ( (pr_default.getStatus(1) != 101) )
            {
               dynavSdt_wp_os_contagemresultado_servico.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(H00T43_A155Servico_Codigo[0]), 6, 0)), H00T43_A608Servico_Nome[0], 0);
               pr_default.readNext(1);
            }
            pr_default.close(1);
            if ( dynavSdt_wp_os_contagemresultado_servico.ItemCount > 0 )
            {
               AV9SDT_WP_OS.gxTpr_Contagemresultado_servico = (int)(NumberUtil.Val( dynavSdt_wp_os_contagemresultado_servico.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV9SDT_WP_OS.gxTpr_Contagemresultado_servico), 6, 0))), "."));
            }
            cmbavSdt_wp_os_contratoservicosprazo_complexidade.Name = "SDT_WP_OS_CONTRATOSERVICOSPRAZO_COMPLEXIDADE";
            cmbavSdt_wp_os_contratoservicosprazo_complexidade.WebTags = "";
            if ( cmbavSdt_wp_os_contratoservicosprazo_complexidade.ItemCount > 0 )
            {
               AV9SDT_WP_OS.gxTpr_Contratoservicosprazo_complexidade = (short)(NumberUtil.Val( cmbavSdt_wp_os_contratoservicosprazo_complexidade.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV9SDT_WP_OS.gxTpr_Contratoservicosprazo_complexidade), 3, 0))), "."));
            }
            dynavSdt_wp_os_contagemresultado_contratadacod.Name = "SDT_WP_OS_CONTAGEMRESULTADO_CONTRATADACOD";
            dynavSdt_wp_os_contagemresultado_contratadacod.WebTags = "";
            dynavSdt_wp_os_contagemresultado_contratadaorigemcod.Name = "SDT_WP_OS_CONTAGEMRESULTADO_CONTRATADAORIGEMCOD";
            dynavSdt_wp_os_contagemresultado_contratadaorigemcod.WebTags = "";
            dynavSdt_wp_os_contagemresultado_contadorfscod.Name = "SDT_WP_OS_CONTAGEMRESULTADO_CONTADORFSCOD";
            dynavSdt_wp_os_contagemresultado_contadorfscod.WebTags = "";
            cmbavSdt_wp_os_contratoservicos_localexec.Name = "SDT_WP_OS_CONTRATOSERVICOS_LOCALEXEC";
            cmbavSdt_wp_os_contratoservicos_localexec.WebTags = "";
            cmbavSdt_wp_os_contratoservicos_localexec.addItem("A", "Contratada", 0);
            cmbavSdt_wp_os_contratoservicos_localexec.addItem("E", "Contratante", 0);
            cmbavSdt_wp_os_contratoservicos_localexec.addItem("O", "Opcional", 0);
            if ( cmbavSdt_wp_os_contratoservicos_localexec.ItemCount > 0 )
            {
               AV9SDT_WP_OS.gxTpr_Contratoservicos_localexec = cmbavSdt_wp_os_contratoservicos_localexec.getValidValue(AV9SDT_WP_OS.gxTpr_Contratoservicos_localexec);
            }
            dynavSdt_wp_os_modulo_codigo.Name = "SDT_WP_OS_MODULO_CODIGO";
            dynavSdt_wp_os_modulo_codigo.WebTags = "";
            dynavSdt_wp_os_contagemresultado_fncusrcod.Name = "SDT_WP_OS_CONTAGEMRESULTADO_FNCUSRCOD";
            dynavSdt_wp_os_contagemresultado_fncusrcod.WebTags = "";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavSdt_wp_os_contagemresultado_demandafm_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVSDT_WP_OS_CONTAGEMRESULTADO_SERVICOT41( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVSDT_WP_OS_CONTAGEMRESULTADO_SERVICO_dataT41( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVSDT_WP_OS_CONTAGEMRESULTADO_SERVICO_htmlT41( )
      {
         int gxdynajaxvalue ;
         GXDLVSDT_WP_OS_CONTAGEMRESULTADO_SERVICO_dataT41( ) ;
         gxdynajaxindex = 1;
         dynavSdt_wp_os_contagemresultado_servico.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavSdt_wp_os_contagemresultado_servico.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavSdt_wp_os_contagemresultado_servico.ItemCount > 0 )
         {
            AV9SDT_WP_OS.gxTpr_Contagemresultado_servico = (int)(NumberUtil.Val( dynavSdt_wp_os_contagemresultado_servico.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV9SDT_WP_OS.gxTpr_Contagemresultado_servico), 6, 0))), "."));
         }
      }

      protected void GXDLVSDT_WP_OS_CONTAGEMRESULTADO_SERVICO_dataT41( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor H00T44 */
         pr_default.execute(2);
         while ( (pr_default.getStatus(2) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00T44_A155Servico_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00T44_A608Servico_Nome[0]));
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      protected void GXDLVSDT_WP_OS_CONTAGEMRESULTADO_SERVICOGRUPOT41( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVSDT_WP_OS_CONTAGEMRESULTADO_SERVICOGRUPO_dataT41( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVSDT_WP_OS_CONTAGEMRESULTADO_SERVICOGRUPO_htmlT41( )
      {
         int gxdynajaxvalue ;
         GXDLVSDT_WP_OS_CONTAGEMRESULTADO_SERVICOGRUPO_dataT41( ) ;
         gxdynajaxindex = 1;
         dynavSdt_wp_os_contagemresultado_servicogrupo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavSdt_wp_os_contagemresultado_servicogrupo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavSdt_wp_os_contagemresultado_servicogrupo.ItemCount > 0 )
         {
            AV9SDT_WP_OS.gxTpr_Contagemresultado_servicogrupo = (int)(NumberUtil.Val( dynavSdt_wp_os_contagemresultado_servicogrupo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV9SDT_WP_OS.gxTpr_Contagemresultado_servicogrupo), 6, 0))), "."));
         }
      }

      protected void GXDLVSDT_WP_OS_CONTAGEMRESULTADO_SERVICOGRUPO_dataT41( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor H00T45 */
         pr_default.execute(3);
         while ( (pr_default.getStatus(3) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00T45_A157ServicoGrupo_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H00T45_A158ServicoGrupo_Descricao[0]);
            pr_default.readNext(3);
         }
         pr_default.close(3);
      }

      protected void GXDLVSDT_WP_OS_CONTAGEMRESULTADO_CONTRATADACODT42( decimal AV47Wwpcontext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVSDT_WP_OS_CONTAGEMRESULTADO_CONTRATADACOD_dataT42( AV47Wwpcontext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVSDT_WP_OS_CONTAGEMRESULTADO_CONTRATADACOD_htmlT42( decimal AV47Wwpcontext )
      {
         int gxdynajaxvalue ;
         GXDLVSDT_WP_OS_CONTAGEMRESULTADO_CONTRATADACOD_dataT42( AV47Wwpcontext) ;
         gxdynajaxindex = 1;
         dynavSdt_wp_os_contagemresultado_contratadacod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavSdt_wp_os_contagemresultado_contratadacod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavSdt_wp_os_contagemresultado_contratadacod.ItemCount > 0 )
         {
            AV9SDT_WP_OS.gxTpr_Contagemresultado_contratadacod = (int)(NumberUtil.Val( dynavSdt_wp_os_contagemresultado_contratadacod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV9SDT_WP_OS.gxTpr_Contagemresultado_contratadacod), 6, 0))), "."));
         }
      }

      protected void GXDLVSDT_WP_OS_CONTAGEMRESULTADO_CONTRATADACOD_dataT42( decimal AV47Wwpcontext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhuma)");
         /* * Property Areatrabalho_codigo not supported in */
         /* Using cursor H00T46 */
         pr_default.execute(4);
         while ( (pr_default.getStatus(4) != 101) )
         {
            /* * Property Areatrabalho_codigo not supported in */
            /* * Property Areatrabalho_codigo not supported in */
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00T46_A39Contratada_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00T46_A41Contratada_PessoaNom[0]));
            pr_default.readNext(4);
         }
         pr_default.close(4);
      }

      protected void GXDLVSDT_WP_OS_CONTAGEMRESULTADO_CONTRATADAORIGEMCODT42( decimal AV47Wwpcontext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVSDT_WP_OS_CONTAGEMRESULTADO_CONTRATADAORIGEMCOD_dataT42( AV47Wwpcontext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVSDT_WP_OS_CONTAGEMRESULTADO_CONTRATADAORIGEMCOD_htmlT42( decimal AV47Wwpcontext )
      {
         int gxdynajaxvalue ;
         GXDLVSDT_WP_OS_CONTAGEMRESULTADO_CONTRATADAORIGEMCOD_dataT42( AV47Wwpcontext) ;
         gxdynajaxindex = 1;
         dynavSdt_wp_os_contagemresultado_contratadaorigemcod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavSdt_wp_os_contagemresultado_contratadaorigemcod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavSdt_wp_os_contagemresultado_contratadaorigemcod.ItemCount > 0 )
         {
            AV9SDT_WP_OS.gxTpr_Contagemresultado_contratadaorigemcod = (int)(NumberUtil.Val( dynavSdt_wp_os_contagemresultado_contratadaorigemcod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV9SDT_WP_OS.gxTpr_Contagemresultado_contratadaorigemcod), 6, 0))), "."));
         }
      }

      protected void GXDLVSDT_WP_OS_CONTAGEMRESULTADO_CONTRATADAORIGEMCOD_dataT42( decimal AV47Wwpcontext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhuma");
         /* * Property Areatrabalho_codigo not supported in */
         /* Using cursor H00T47 */
         pr_default.execute(5);
         while ( (pr_default.getStatus(5) != 101) )
         {
            /* * Property Areatrabalho_codigo not supported in */
            /* * Property Areatrabalho_codigo not supported in */
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00T47_A39Contratada_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00T47_A41Contratada_PessoaNom[0]));
            pr_default.readNext(5);
         }
         pr_default.close(5);
      }

      protected void GXDLVSDT_WP_OS_CONTAGEMRESULTADO_CONTADORFSCODT42( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVSDT_WP_OS_CONTAGEMRESULTADO_CONTADORFSCOD_dataT42( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVSDT_WP_OS_CONTAGEMRESULTADO_CONTADORFSCOD_htmlT42( )
      {
         int gxdynajaxvalue ;
         GXDLVSDT_WP_OS_CONTAGEMRESULTADO_CONTADORFSCOD_dataT42( ) ;
         gxdynajaxindex = 1;
         dynavSdt_wp_os_contagemresultado_contadorfscod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavSdt_wp_os_contagemresultado_contadorfscod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavSdt_wp_os_contagemresultado_contadorfscod.ItemCount > 0 )
         {
            AV9SDT_WP_OS.gxTpr_Contagemresultado_contadorfscod = (int)(NumberUtil.Val( dynavSdt_wp_os_contagemresultado_contadorfscod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV9SDT_WP_OS.gxTpr_Contagemresultado_contadorfscod), 6, 0))), "."));
         }
      }

      protected void GXDLVSDT_WP_OS_CONTAGEMRESULTADO_CONTADORFSCOD_dataT42( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00T48 */
         pr_default.execute(6);
         while ( (pr_default.getStatus(6) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00T48_A1Usuario_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00T48_A58Usuario_PessoaNom[0]));
            pr_default.readNext(6);
         }
         pr_default.close(6);
      }

      protected void GXDLVSDT_WP_OS_MODULO_CODIGOT42( SdtSDT_WP_OS AV9SDT_WP_OS )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVSDT_WP_OS_MODULO_CODIGO_dataT42( AV9SDT_WP_OS) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVSDT_WP_OS_MODULO_CODIGO_htmlT42( SdtSDT_WP_OS AV9SDT_WP_OS )
      {
         int gxdynajaxvalue ;
         GXDLVSDT_WP_OS_MODULO_CODIGO_dataT42( AV9SDT_WP_OS) ;
         gxdynajaxindex = 1;
         dynavSdt_wp_os_modulo_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavSdt_wp_os_modulo_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavSdt_wp_os_modulo_codigo.ItemCount > 0 )
         {
            AV9SDT_WP_OS.gxTpr_Modulo_codigo = (int)(NumberUtil.Val( dynavSdt_wp_os_modulo_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV9SDT_WP_OS.gxTpr_Modulo_codigo), 6, 0))), "."));
         }
      }

      protected void GXDLVSDT_WP_OS_MODULO_CODIGO_dataT42( SdtSDT_WP_OS AV9SDT_WP_OS )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00T49 */
         pr_default.execute(7, new Object[] {AV9SDT_WP_OS.gxTpr_Contagemresultado_sistemacod});
         while ( (pr_default.getStatus(7) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00T49_A146Modulo_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00T49_A145Modulo_Sigla[0]));
            pr_default.readNext(7);
         }
         pr_default.close(7);
      }

      protected void GXDLVSDT_WP_OS_CONTAGEMRESULTADO_FNCUSRCODT42( SdtSDT_WP_OS AV9SDT_WP_OS )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVSDT_WP_OS_CONTAGEMRESULTADO_FNCUSRCOD_dataT42( AV9SDT_WP_OS) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVSDT_WP_OS_CONTAGEMRESULTADO_FNCUSRCOD_htmlT42( SdtSDT_WP_OS AV9SDT_WP_OS )
      {
         int gxdynajaxvalue ;
         GXDLVSDT_WP_OS_CONTAGEMRESULTADO_FNCUSRCOD_dataT42( AV9SDT_WP_OS) ;
         gxdynajaxindex = 1;
         dynavSdt_wp_os_contagemresultado_fncusrcod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavSdt_wp_os_contagemresultado_fncusrcod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavSdt_wp_os_contagemresultado_fncusrcod.ItemCount > 0 )
         {
            AV9SDT_WP_OS.gxTpr_Contagemresultado_fncusrcod = (int)(NumberUtil.Val( dynavSdt_wp_os_contagemresultado_fncusrcod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV9SDT_WP_OS.gxTpr_Contagemresultado_fncusrcod), 6, 0))), "."));
         }
      }

      protected void GXDLVSDT_WP_OS_CONTAGEMRESULTADO_FNCUSRCOD_dataT42( SdtSDT_WP_OS AV9SDT_WP_OS )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor H00T410 */
         pr_default.execute(8, new Object[] {AV9SDT_WP_OS.gxTpr_Contagemresultado_sistemacod});
         while ( (pr_default.getStatus(8) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00T410_A161FuncaoUsuario_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H00T410_A162FuncaoUsuario_Nome[0]);
            pr_default.readNext(8);
         }
         pr_default.close(8);
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavSdt_wp_os_contagemresultado_servicogrupo.ItemCount > 0 )
         {
            AV9SDT_WP_OS.gxTpr_Contagemresultado_servicogrupo = (int)(NumberUtil.Val( dynavSdt_wp_os_contagemresultado_servicogrupo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV9SDT_WP_OS.gxTpr_Contagemresultado_servicogrupo), 6, 0))), "."));
         }
         if ( dynavSdt_wp_os_contagemresultado_servico.ItemCount > 0 )
         {
            AV9SDT_WP_OS.gxTpr_Contagemresultado_servico = (int)(NumberUtil.Val( dynavSdt_wp_os_contagemresultado_servico.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV9SDT_WP_OS.gxTpr_Contagemresultado_servico), 6, 0))), "."));
         }
         if ( cmbavSdt_wp_os_contratoservicosprazo_complexidade.ItemCount > 0 )
         {
            AV9SDT_WP_OS.gxTpr_Contratoservicosprazo_complexidade = (short)(NumberUtil.Val( cmbavSdt_wp_os_contratoservicosprazo_complexidade.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV9SDT_WP_OS.gxTpr_Contratoservicosprazo_complexidade), 3, 0))), "."));
         }
         if ( dynavSdt_wp_os_contagemresultado_contratadacod.ItemCount > 0 )
         {
            AV9SDT_WP_OS.gxTpr_Contagemresultado_contratadacod = (int)(NumberUtil.Val( dynavSdt_wp_os_contagemresultado_contratadacod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV9SDT_WP_OS.gxTpr_Contagemresultado_contratadacod), 6, 0))), "."));
         }
         if ( dynavSdt_wp_os_contagemresultado_contratadaorigemcod.ItemCount > 0 )
         {
            AV9SDT_WP_OS.gxTpr_Contagemresultado_contratadaorigemcod = (int)(NumberUtil.Val( dynavSdt_wp_os_contagemresultado_contratadaorigemcod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV9SDT_WP_OS.gxTpr_Contagemresultado_contratadaorigemcod), 6, 0))), "."));
         }
         if ( dynavSdt_wp_os_contagemresultado_contadorfscod.ItemCount > 0 )
         {
            AV9SDT_WP_OS.gxTpr_Contagemresultado_contadorfscod = (int)(NumberUtil.Val( dynavSdt_wp_os_contagemresultado_contadorfscod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV9SDT_WP_OS.gxTpr_Contagemresultado_contadorfscod), 6, 0))), "."));
         }
         if ( cmbavSdt_wp_os_contratoservicos_localexec.ItemCount > 0 )
         {
            AV9SDT_WP_OS.gxTpr_Contratoservicos_localexec = cmbavSdt_wp_os_contratoservicos_localexec.getValidValue(AV9SDT_WP_OS.gxTpr_Contratoservicos_localexec);
         }
         if ( dynavSdt_wp_os_modulo_codigo.ItemCount > 0 )
         {
            AV9SDT_WP_OS.gxTpr_Modulo_codigo = (int)(NumberUtil.Val( dynavSdt_wp_os_modulo_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV9SDT_WP_OS.gxTpr_Modulo_codigo), 6, 0))), "."));
         }
         if ( dynavSdt_wp_os_contagemresultado_fncusrcod.ItemCount > 0 )
         {
            AV9SDT_WP_OS.gxTpr_Contagemresultado_fncusrcod = (int)(NumberUtil.Val( dynavSdt_wp_os_contagemresultado_fncusrcod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV9SDT_WP_OS.gxTpr_Contagemresultado_fncusrcod), 6, 0))), "."));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFT42( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavSdt_wp_os_contagemresultado_sistemagestor_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSdt_wp_os_contagemresultado_sistemagestor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSdt_wp_os_contagemresultado_sistemagestor_Enabled), 5, 0)));
      }

      protected void RFT42( )
      {
         initialize_formulas( ) ;
         /* Execute user event: E13T42 */
         E13T42 ();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            if ( 1 != 0 )
            {
               if ( StringUtil.Len( WebComp_Wc_contagemresultadoevidencias_Component) != 0 )
               {
                  WebComp_Wc_contagemresultadoevidencias.componentstart();
               }
            }
         }
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E14T42 */
            E14T42 ();
            WBT40( ) ;
         }
      }

      protected void STRUPT40( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavSdt_wp_os_contagemresultado_sistemagestor_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSdt_wp_os_contagemresultado_sistemagestor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSdt_wp_os_contagemresultado_sistemagestor_Enabled), 5, 0)));
         GXVSDT_WP_OS_CONTAGEMRESULTADO_CONTADORFSCOD_htmlT42( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11T42 */
         E11T42 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "Sdt_wp_os"), AV9SDT_WP_OS);
            /* Read variables values. */
            AV9SDT_WP_OS.gxTpr_Contagemresultado_demandafm = cgiGet( edtavSdt_wp_os_contagemresultado_demandafm_Internalname);
            AV9SDT_WP_OS.gxTpr_Usuario_cargouonom = StringUtil.Upper( cgiGet( edtavSdt_wp_os_usuario_cargouonom_Internalname));
            AV9SDT_WP_OS.gxTpr_Contagemresultado_requisitadoporcontratante = StringUtil.StrToBool( cgiGet( edtavSdt_wp_os_contagemresultado_requisitadoporcontratante_Internalname));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavSdt_wp_os_contagemresultado_owner_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavSdt_wp_os_contagemresultado_owner_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SDT_WP_OS_CONTAGEMRESULTADO_OWNER");
               GX_FocusControl = edtavSdt_wp_os_contagemresultado_owner_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV9SDT_WP_OS.gxTpr_Contagemresultado_owner = 0;
            }
            else
            {
               AV9SDT_WP_OS.gxTpr_Contagemresultado_owner = (int)(context.localUtil.CToN( cgiGet( edtavSdt_wp_os_contagemresultado_owner_Internalname), ",", "."));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavSdt_wp_os_contagemresultado_datadmn_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {""}), 1, "SDT_WP_OS_CONTAGEMRESULTADO_DATADMN");
               GX_FocusControl = edtavSdt_wp_os_contagemresultado_datadmn_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV9SDT_WP_OS.gxTpr_Contagemresultado_datadmn = (DateTime)(DateTime.MinValue);
            }
            else
            {
               AV9SDT_WP_OS.gxTpr_Contagemresultado_datadmn = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavSdt_wp_os_contagemresultado_datadmn_Internalname), 0));
            }
            AV9SDT_WP_OS.gxTpr_Contagemresultado_descricao = cgiGet( edtavSdt_wp_os_contagemresultado_descricao_Internalname);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavSdt_wp_os_contagemresultado_cntcod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavSdt_wp_os_contagemresultado_cntcod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SDT_WP_OS_CONTAGEMRESULTADO_CNTCOD");
               GX_FocusControl = edtavSdt_wp_os_contagemresultado_cntcod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV9SDT_WP_OS.gxTpr_Contagemresultado_cntcod = 0;
            }
            else
            {
               AV9SDT_WP_OS.gxTpr_Contagemresultado_cntcod = (int)(context.localUtil.CToN( cgiGet( edtavSdt_wp_os_contagemresultado_cntcod_Internalname), ",", "."));
            }
            dynavSdt_wp_os_contagemresultado_servicogrupo.CurrentValue = cgiGet( dynavSdt_wp_os_contagemresultado_servicogrupo_Internalname);
            AV9SDT_WP_OS.gxTpr_Contagemresultado_servicogrupo = (int)(NumberUtil.Val( cgiGet( dynavSdt_wp_os_contagemresultado_servicogrupo_Internalname), "."));
            dynavSdt_wp_os_contagemresultado_servico.CurrentValue = cgiGet( dynavSdt_wp_os_contagemresultado_servico_Internalname);
            AV9SDT_WP_OS.gxTpr_Contagemresultado_servico = (int)(NumberUtil.Val( cgiGet( dynavSdt_wp_os_contagemresultado_servico_Internalname), "."));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavSdt_wp_os_contagemresultado_cntsrvcod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavSdt_wp_os_contagemresultado_cntsrvcod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SDT_WP_OS_CONTAGEMRESULTADO_CNTSRVCOD");
               GX_FocusControl = edtavSdt_wp_os_contagemresultado_cntsrvcod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV9SDT_WP_OS.gxTpr_Contagemresultado_cntsrvcod = 0;
            }
            else
            {
               AV9SDT_WP_OS.gxTpr_Contagemresultado_cntsrvcod = (int)(context.localUtil.CToN( cgiGet( edtavSdt_wp_os_contagemresultado_cntsrvcod_Internalname), ",", "."));
            }
            AV9SDT_WP_OS.gxTpr_Contratoservicos_undcntnome = StringUtil.Upper( cgiGet( edtavSdt_wp_os_contratoservicos_undcntnome_Internalname));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavSdt_wp_os_contratoservicos_qntuntcns_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavSdt_wp_os_contratoservicos_qntuntcns_Internalname), ",", ".") > 9999.9999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SDT_WP_OS_CONTRATOSERVICOS_QNTUNTCNS");
               GX_FocusControl = edtavSdt_wp_os_contratoservicos_qntuntcns_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV9SDT_WP_OS.gxTpr_Contratoservicos_qntuntcns = 0;
            }
            else
            {
               AV9SDT_WP_OS.gxTpr_Contratoservicos_qntuntcns = context.localUtil.CToN( cgiGet( edtavSdt_wp_os_contratoservicos_qntuntcns_Internalname), ",", ".");
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavSdt_wp_os_contagemresultado_quantidadesolicitada_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavSdt_wp_os_contagemresultado_quantidadesolicitada_Internalname), ",", ".") > 9999.9999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SDT_WP_OS_CONTAGEMRESULTADO_QUANTIDADESOLICITADA");
               GX_FocusControl = edtavSdt_wp_os_contagemresultado_quantidadesolicitada_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV9SDT_WP_OS.gxTpr_Contagemresultado_quantidadesolicitada = 0;
            }
            else
            {
               AV9SDT_WP_OS.gxTpr_Contagemresultado_quantidadesolicitada = context.localUtil.CToN( cgiGet( edtavSdt_wp_os_contagemresultado_quantidadesolicitada_Internalname), ",", ".");
            }
            cmbavSdt_wp_os_contratoservicosprazo_complexidade.CurrentValue = cgiGet( cmbavSdt_wp_os_contratoservicosprazo_complexidade_Internalname);
            AV9SDT_WP_OS.gxTpr_Contratoservicosprazo_complexidade = (short)(NumberUtil.Val( cgiGet( cmbavSdt_wp_os_contratoservicosprazo_complexidade_Internalname), "."));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavSdt_wp_os_contratoservicosprioridade_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavSdt_wp_os_contratoservicosprioridade_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SDT_WP_OS_CONTRATOSERVICOSPRIORIDADE_CODIGO");
               GX_FocusControl = edtavSdt_wp_os_contratoservicosprioridade_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV9SDT_WP_OS.gxTpr_Contratoservicosprioridade_codigo = 0;
            }
            else
            {
               AV9SDT_WP_OS.gxTpr_Contratoservicosprioridade_codigo = (int)(context.localUtil.CToN( cgiGet( edtavSdt_wp_os_contratoservicosprioridade_codigo_Internalname), ",", "."));
            }
            dynavSdt_wp_os_contagemresultado_contratadacod.CurrentValue = cgiGet( dynavSdt_wp_os_contagemresultado_contratadacod_Internalname);
            AV9SDT_WP_OS.gxTpr_Contagemresultado_contratadacod = (int)(NumberUtil.Val( cgiGet( dynavSdt_wp_os_contagemresultado_contratadacod_Internalname), "."));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavSdt_wp_os_contagemresultado_responsavel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavSdt_wp_os_contagemresultado_responsavel_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SDT_WP_OS_CONTAGEMRESULTADO_RESPONSAVEL");
               GX_FocusControl = edtavSdt_wp_os_contagemresultado_responsavel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV9SDT_WP_OS.gxTpr_Contagemresultado_responsavel = 0;
            }
            else
            {
               AV9SDT_WP_OS.gxTpr_Contagemresultado_responsavel = (int)(context.localUtil.CToN( cgiGet( edtavSdt_wp_os_contagemresultado_responsavel_Internalname), ",", "."));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavSdt_wp_os_contagemresultado_osvinculada_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavSdt_wp_os_contagemresultado_osvinculada_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SDT_WP_OS_CONTAGEMRESULTADO_OSVINCULADA");
               GX_FocusControl = edtavSdt_wp_os_contagemresultado_osvinculada_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV9SDT_WP_OS.gxTpr_Contagemresultado_osvinculada = 0;
            }
            else
            {
               AV9SDT_WP_OS.gxTpr_Contagemresultado_osvinculada = (int)(context.localUtil.CToN( cgiGet( edtavSdt_wp_os_contagemresultado_osvinculada_Internalname), ",", "."));
            }
            AV9SDT_WP_OS.gxTpr_Contagemresultado_dmnvinculada = StringUtil.Upper( cgiGet( edtavSdt_wp_os_contagemresultado_dmnvinculada_Internalname));
            AV9SDT_WP_OS.gxTpr_Contagemresultado_dmnvinculadaref = StringUtil.Upper( cgiGet( edtavSdt_wp_os_contagemresultado_dmnvinculadaref_Internalname));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavSdt_wp_os_contagemresultado_pfbfsimp_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavSdt_wp_os_contagemresultado_pfbfsimp_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SDT_WP_OS_CONTAGEMRESULTADO_PFBFSIMP");
               GX_FocusControl = edtavSdt_wp_os_contagemresultado_pfbfsimp_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV9SDT_WP_OS.gxTpr_Contagemresultado_pfbfsimp = 0;
            }
            else
            {
               AV9SDT_WP_OS.gxTpr_Contagemresultado_pfbfsimp = context.localUtil.CToN( cgiGet( edtavSdt_wp_os_contagemresultado_pfbfsimp_Internalname), ",", ".");
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavSdt_wp_os_contagemresultado_pflfsimp_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavSdt_wp_os_contagemresultado_pflfsimp_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SDT_WP_OS_CONTAGEMRESULTADO_PFLFSIMP");
               GX_FocusControl = edtavSdt_wp_os_contagemresultado_pflfsimp_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV9SDT_WP_OS.gxTpr_Contagemresultado_pflfsimp = 0;
            }
            else
            {
               AV9SDT_WP_OS.gxTpr_Contagemresultado_pflfsimp = context.localUtil.CToN( cgiGet( edtavSdt_wp_os_contagemresultado_pflfsimp_Internalname), ",", ".");
            }
            dynavSdt_wp_os_contagemresultado_contratadaorigemcod.CurrentValue = cgiGet( dynavSdt_wp_os_contagemresultado_contratadaorigemcod_Internalname);
            AV9SDT_WP_OS.gxTpr_Contagemresultado_contratadaorigemcod = (int)(NumberUtil.Val( cgiGet( dynavSdt_wp_os_contagemresultado_contratadaorigemcod_Internalname), "."));
            dynavSdt_wp_os_contagemresultado_contadorfscod.CurrentValue = cgiGet( dynavSdt_wp_os_contagemresultado_contadorfscod_Internalname);
            AV9SDT_WP_OS.gxTpr_Contagemresultado_contadorfscod = (int)(NumberUtil.Val( cgiGet( dynavSdt_wp_os_contagemresultado_contadorfscod_Internalname), "."));
            cmbavSdt_wp_os_contratoservicos_localexec.CurrentValue = cgiGet( cmbavSdt_wp_os_contratoservicos_localexec_Internalname);
            AV9SDT_WP_OS.gxTpr_Contratoservicos_localexec = cgiGet( cmbavSdt_wp_os_contratoservicos_localexec_Internalname);
            if ( context.localUtil.VCDateTime( cgiGet( edtavSdt_wp_os_contagemresultado_prazoanalise_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {""}), 1, "SDT_WP_OS_CONTAGEMRESULTADO_PRAZOANALISE");
               GX_FocusControl = edtavSdt_wp_os_contagemresultado_prazoanalise_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV9SDT_WP_OS.gxTpr_Contagemresultado_prazoanalise = (DateTime)(DateTime.MinValue);
            }
            else
            {
               AV9SDT_WP_OS.gxTpr_Contagemresultado_prazoanalise = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavSdt_wp_os_contagemresultado_prazoanalise_Internalname), 0));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavSdt_wp_os_contagemresultado_dataentrega_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {""}), 1, "SDT_WP_OS_CONTAGEMRESULTADO_DATAENTREGA");
               GX_FocusControl = edtavSdt_wp_os_contagemresultado_dataentrega_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV9SDT_WP_OS.gxTpr_Contagemresultado_dataentrega = (DateTime)(DateTime.MinValue);
            }
            else
            {
               AV9SDT_WP_OS.gxTpr_Contagemresultado_dataentrega = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavSdt_wp_os_contagemresultado_dataentrega_Internalname), 0));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavSdt_wp_os_contagemresultado_sistemacod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavSdt_wp_os_contagemresultado_sistemacod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SDT_WP_OS_CONTAGEMRESULTADO_SISTEMACOD");
               GX_FocusControl = edtavSdt_wp_os_contagemresultado_sistemacod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV9SDT_WP_OS.gxTpr_Contagemresultado_sistemacod = 0;
            }
            else
            {
               AV9SDT_WP_OS.gxTpr_Contagemresultado_sistemacod = (int)(context.localUtil.CToN( cgiGet( edtavSdt_wp_os_contagemresultado_sistemacod_Internalname), ",", "."));
            }
            dynavSdt_wp_os_modulo_codigo.CurrentValue = cgiGet( dynavSdt_wp_os_modulo_codigo_Internalname);
            AV9SDT_WP_OS.gxTpr_Modulo_codigo = (int)(NumberUtil.Val( cgiGet( dynavSdt_wp_os_modulo_codigo_Internalname), "."));
            dynavSdt_wp_os_contagemresultado_fncusrcod.CurrentValue = cgiGet( dynavSdt_wp_os_contagemresultado_fncusrcod_Internalname);
            AV9SDT_WP_OS.gxTpr_Contagemresultado_fncusrcod = (int)(NumberUtil.Val( cgiGet( dynavSdt_wp_os_contagemresultado_fncusrcod_Internalname), "."));
            AV9SDT_WP_OS.gxTpr_Contagemresultado_sistemagestor = StringUtil.Upper( cgiGet( edtavSdt_wp_os_contagemresultado_sistemagestor_Internalname));
            AV9SDT_WP_OS.gxTpr_Contagemresultado_link = cgiGet( edtavSdt_wp_os_contagemresultado_link_Internalname);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavSdt_wp_os_contagemresultado_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavSdt_wp_os_contagemresultado_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SDT_WP_OS_CONTAGEMRESULTADO_CODIGO");
               GX_FocusControl = edtavSdt_wp_os_contagemresultado_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV9SDT_WP_OS.gxTpr_Contagemresultado_codigo = 0;
            }
            else
            {
               AV9SDT_WP_OS.gxTpr_Contagemresultado_codigo = (int)(context.localUtil.CToN( cgiGet( edtavSdt_wp_os_contagemresultado_codigo_Internalname), ",", "."));
            }
            /* Read saved values. */
            Gxuitabspanel_tabmain_Width = cgiGet( "GXUITABSPANEL_TABMAIN_Width");
            Gxuitabspanel_tabmain_Cls = cgiGet( "GXUITABSPANEL_TABMAIN_Cls");
            Gxuitabspanel_tabmain_Autowidth = StringUtil.StrToBool( cgiGet( "GXUITABSPANEL_TABMAIN_Autowidth"));
            Gxuitabspanel_tabmain_Autoheight = StringUtil.StrToBool( cgiGet( "GXUITABSPANEL_TABMAIN_Autoheight"));
            Gxuitabspanel_tabmain_Autoscroll = StringUtil.StrToBool( cgiGet( "GXUITABSPANEL_TABMAIN_Autoscroll"));
            Gxuitabspanel_tabmain_Designtimetabs = cgiGet( "GXUITABSPANEL_TABMAIN_Designtimetabs");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            GXVSDT_WP_OS_CONTAGEMRESULTADO_CONTADORFSCOD_htmlT42( ) ;
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11T42 */
         E11T42 ();
         if (returnInSub) return;
      }

      protected void E11T42( )
      {
         /* Start Routine */
         edtavSdt_wp_os_contagemresultado_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSdt_wp_os_contagemresultado_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSdt_wp_os_contagemresultado_codigo_Visible), 5, 0)));
         /* Object Property */
         if ( StringUtil.StrCmp(StringUtil.Lower( WebComp_Wc_contagemresultadoevidencias_Component), StringUtil.Lower( "WC_ContagemResultadoEvidencias")) != 0 )
         {
            WebComp_Wc_contagemresultadoevidencias = getWebComponent(GetType(), "GeneXus.Programs", "wc_contagemresultadoevidencias", new Object[] {context} );
            WebComp_Wc_contagemresultadoevidencias.ComponentInit();
            WebComp_Wc_contagemresultadoevidencias.Name = "WC_ContagemResultadoEvidencias";
            WebComp_Wc_contagemresultadoevidencias_Component = "WC_ContagemResultadoEvidencias";
         }
         if ( StringUtil.Len( WebComp_Wc_contagemresultadoevidencias_Component) != 0 )
         {
            WebComp_Wc_contagemresultadoevidencias.setjustcreated();
            WebComp_Wc_contagemresultadoevidencias.componentprepare(new Object[] {(String)"W0187",(String)""});
            WebComp_Wc_contagemresultadoevidencias.componentbind(new Object[] {});
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E12T42 */
         E12T42 ();
         if (returnInSub) return;
      }

      protected void E12T42( )
      {
         /* Enter Routine */
      }

      protected void S112( )
      {
         /* 'SHOW MESSAGES' Routine */
         AV46GXV34 = 1;
         while ( AV46GXV34 <= AV6Messages.Count )
         {
            AV5Message = ((SdtMessages_Message)AV6Messages.Item(AV46GXV34));
            GX_msglist.addItem(AV5Message.gxTpr_Description);
            AV46GXV34 = (int)(AV46GXV34+1);
         }
      }

      protected void E13T42( )
      {
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV7Context) ;
      }

      protected void nextLoad( )
      {
      }

      protected void E14T42( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_T42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktitle_Internalname, "Dados", "", "", lblTextblocktitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_T42( true) ;
         }
         else
         {
            wb_table2_8_T42( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_T42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_202_T42( true) ;
         }
         else
         {
            wb_table3_202_T42( false) ;
         }
         return  ;
      }

      protected void wb_table3_202_T42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_T42e( true) ;
         }
         else
         {
            wb_table1_2_T42e( false) ;
         }
      }

      protected void wb_table3_202_T42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 205,'',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnbntartefatos_Internalname, "", "Artefatos", bttBtnbntartefatos_Jsonclick, 7, "Artefatos", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"e15t41_client"+"'", TempTags, "", 2, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 207,'',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnenter_Internalname, "", "Solicitar", bttBtnenter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 209,'',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_202_T42e( true) ;
         }
         else
         {
            wb_table3_202_T42e( false) ;
         }
      }

      protected void wb_table2_8_T42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GXUITABSPANEL_TABMAINContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABMAINContainer"+"TitleTabSolicitar"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "Solicita��o") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABMAINContainer"+"TabSolicitar"+"\" style=\"display:none;\">") ;
            wb_table4_17_T42( true) ;
         }
         else
         {
            wb_table4_17_T42( false) ;
         }
         return  ;
      }

      protected void wb_table4_17_T42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABMAINContainer"+"TitleTabDescricao"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "Descri��o Complementar") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABMAINContainer"+"TabDescricao"+"\" style=\"display:none;\">") ;
            wb_table5_191_T42( true) ;
         }
         else
         {
            wb_table5_191_T42( false) ;
         }
         return  ;
      }

      protected void wb_table5_191_T42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABMAINContainer"+"TitleTabRequisitos"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "Requisitos") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABMAINContainer"+"TabRequisitos"+"\" style=\"display:none;\">") ;
            wb_table6_197_T42( true) ;
         }
         else
         {
            wb_table6_197_T42( false) ;
         }
         return  ;
      }

      protected void wb_table6_197_T42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_T42e( true) ;
         }
         else
         {
            wb_table2_8_T42e( false) ;
         }
      }

      protected void wb_table6_197_T42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablerequisitos_Internalname, tblTablerequisitos_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_197_T42e( true) ;
         }
         else
         {
            wb_table6_197_T42e( false) ;
         }
      }

      protected void wb_table5_191_T42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledescrica_Internalname, tblTabledescrica_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_191_T42e( true) ;
         }
         else
         {
            wb_table5_191_T42e( false) ;
         }
      }

      protected void wb_table4_17_T42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesolicita_Internalname, tblTablesolicita_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksdt_wp_os_contagemresultado_demandafm_Internalname, "N� OS", "", "", lblTextblocksdt_wp_os_contagemresultado_demandafm_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSdt_wp_os_contagemresultado_demandafm_Internalname, AV9SDT_WP_OS.gxTpr_Contagemresultado_demandafm, StringUtil.RTrim( context.localUtil.Format( AV9SDT_WP_OS.gxTpr_Contagemresultado_demandafm, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,22);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSdt_wp_os_contagemresultado_demandafm_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 80, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksdt_wp_os_usuario_cargouonom_Internalname, "Unidade Organizacional", "", "", lblTextblocksdt_wp_os_usuario_cargouonom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSdt_wp_os_usuario_cargouonom_Internalname, StringUtil.RTrim( AV9SDT_WP_OS.gxTpr_Usuario_cargouonom), StringUtil.RTrim( context.localUtil.Format( AV9SDT_WP_OS.gxTpr_Usuario_cargouonom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,27);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSdt_wp_os_usuario_cargouonom_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksdt_wp_os_contagemresultado_requisitadoporcontratante_Internalname, "Requisitado por", "", "", lblTextblocksdt_wp_os_contagemresultado_requisitadoporcontratante_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table7_32_T42( true) ;
         }
         else
         {
            wb_table7_32_T42( false) ;
         }
         return  ;
      }

      protected void wb_table7_32_T42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksdt_wp_os_contagemresultado_owner_Internalname, "Requisitante", "", "", lblTextblocksdt_wp_os_contagemresultado_owner_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSdt_wp_os_contagemresultado_owner_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9SDT_WP_OS.gxTpr_Contagemresultado_owner), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV9SDT_WP_OS.gxTpr_Contagemresultado_owner), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,42);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSdt_wp_os_contagemresultado_owner_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksdt_wp_os_contagemresultado_datadmn_Internalname, "Data da OS", "", "", lblTextblocksdt_wp_os_contagemresultado_datadmn_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavSdt_wp_os_contagemresultado_datadmn_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavSdt_wp_os_contagemresultado_datadmn_Internalname, context.localUtil.Format(AV9SDT_WP_OS.gxTpr_Contagemresultado_datadmn, "99/99/99"), context.localUtil.Format( AV9SDT_WP_OS.gxTpr_Contagemresultado_datadmn, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,47);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSdt_wp_os_contagemresultado_datadmn_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 70, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_OS_Vs3.htm");
            GxWebStd.gx_bitmap( context, edtavSdt_wp_os_contagemresultado_datadmn_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksdt_wp_os_contagemresultado_descricao_Internalname, "Titulo", "", "", lblTextblocksdt_wp_os_contagemresultado_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSdt_wp_os_contagemresultado_descricao_Internalname, AV9SDT_WP_OS.gxTpr_Contagemresultado_descricao, StringUtil.RTrim( context.localUtil.Format( AV9SDT_WP_OS.gxTpr_Contagemresultado_descricao, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,52);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSdt_wp_os_contagemresultado_descricao_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 100, "%", 1, "row", 500, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksdt_wp_os_contagemresultado_cntcod_Internalname, "Contrato", "", "", lblTextblocksdt_wp_os_contagemresultado_cntcod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSdt_wp_os_contagemresultado_cntcod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9SDT_WP_OS.gxTpr_Contagemresultado_cntcod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV9SDT_WP_OS.gxTpr_Contagemresultado_cntcod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,57);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSdt_wp_os_contagemresultado_cntcod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksdt_wp_os_contagemresultado_servicogrupo_Internalname, "Grupo de Servi�os", "", "", lblTextblocksdt_wp_os_contagemresultado_servicogrupo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavSdt_wp_os_contagemresultado_servicogrupo, dynavSdt_wp_os_contagemresultado_servicogrupo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV9SDT_WP_OS.gxTpr_Contagemresultado_servicogrupo), 6, 0)), 1, dynavSdt_wp_os_contagemresultado_servicogrupo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,62);\"", "", true, "HLP_WP_OS_Vs3.htm");
            dynavSdt_wp_os_contagemresultado_servicogrupo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9SDT_WP_OS.gxTpr_Contagemresultado_servicogrupo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSdt_wp_os_contagemresultado_servicogrupo_Internalname, "Values", (String)(dynavSdt_wp_os_contagemresultado_servicogrupo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksdt_wp_os_contagemresultado_servico_Internalname, "Servi�o", "", "", lblTextblocksdt_wp_os_contagemresultado_servico_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavSdt_wp_os_contagemresultado_servico, dynavSdt_wp_os_contagemresultado_servico_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV9SDT_WP_OS.gxTpr_Contagemresultado_servico), 6, 0)), 1, dynavSdt_wp_os_contagemresultado_servico_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,67);\"", "", true, "HLP_WP_OS_Vs3.htm");
            dynavSdt_wp_os_contagemresultado_servico.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9SDT_WP_OS.gxTpr_Contagemresultado_servico), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSdt_wp_os_contagemresultado_servico_Internalname, "Values", (String)(dynavSdt_wp_os_contagemresultado_servico.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksdt_wp_os_contagemresultado_cntsrvcod_Internalname, "Codigo no Contrato", "", "", lblTextblocksdt_wp_os_contagemresultado_cntsrvcod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSdt_wp_os_contagemresultado_cntsrvcod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9SDT_WP_OS.gxTpr_Contagemresultado_cntsrvcod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV9SDT_WP_OS.gxTpr_Contagemresultado_cntsrvcod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,72);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSdt_wp_os_contagemresultado_cntsrvcod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksdt_wp_os_contratoservicos_undcntnome_Internalname, "Unidade de Contrata��o", "", "", lblTextblocksdt_wp_os_contratoservicos_undcntnome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSdt_wp_os_contratoservicos_undcntnome_Internalname, StringUtil.RTrim( AV9SDT_WP_OS.gxTpr_Contratoservicos_undcntnome), StringUtil.RTrim( context.localUtil.Format( AV9SDT_WP_OS.gxTpr_Contratoservicos_undcntnome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,77);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSdt_wp_os_contratoservicos_undcntnome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksdt_wp_os_contratoservicos_qntuntcns_Internalname, "Qtde Unit de Consumo", "", "", lblTextblocksdt_wp_os_contratoservicos_qntuntcns_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSdt_wp_os_contratoservicos_qntuntcns_Internalname, StringUtil.LTrim( StringUtil.NToC( AV9SDT_WP_OS.gxTpr_Contratoservicos_qntuntcns, 9, 4, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV9SDT_WP_OS.gxTpr_Contratoservicos_qntuntcns, "ZZZ9.9999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','4');"+";gx.evt.onblur(this,82);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSdt_wp_os_contratoservicos_qntuntcns_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 80, "px", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksdt_wp_os_contagemresultado_quantidadesolicitada_Internalname, "Quantidade Solicitada", "", "", lblTextblocksdt_wp_os_contagemresultado_quantidadesolicitada_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 87,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSdt_wp_os_contagemresultado_quantidadesolicitada_Internalname, StringUtil.LTrim( StringUtil.NToC( AV9SDT_WP_OS.gxTpr_Contagemresultado_quantidadesolicitada, 9, 4, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV9SDT_WP_OS.gxTpr_Contagemresultado_quantidadesolicitada, "ZZZ9.9999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','4');"+";gx.evt.onblur(this,87);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSdt_wp_os_contagemresultado_quantidadesolicitada_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksdt_wp_os_contratoservicosprazo_complexidade_Internalname, "Complexidade", "", "", lblTextblocksdt_wp_os_contratoservicosprazo_complexidade_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavSdt_wp_os_contratoservicosprazo_complexidade, cmbavSdt_wp_os_contratoservicosprazo_complexidade_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV9SDT_WP_OS.gxTpr_Contratoservicosprazo_complexidade), 3, 0)), 1, cmbavSdt_wp_os_contratoservicosprazo_complexidade_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 60, "px", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,92);\"", "", true, "HLP_WP_OS_Vs3.htm");
            cmbavSdt_wp_os_contratoservicosprazo_complexidade.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9SDT_WP_OS.gxTpr_Contratoservicosprazo_complexidade), 3, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSdt_wp_os_contratoservicosprazo_complexidade_Internalname, "Values", (String)(cmbavSdt_wp_os_contratoservicosprazo_complexidade.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksdt_wp_os_contratoservicosprioridade_codigo_Internalname, "Prioridade", "", "", lblTextblocksdt_wp_os_contratoservicosprioridade_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSdt_wp_os_contratoservicosprioridade_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9SDT_WP_OS.gxTpr_Contratoservicosprioridade_codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV9SDT_WP_OS.gxTpr_Contratoservicosprioridade_codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,97);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSdt_wp_os_contratoservicosprioridade_codigo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksdt_wp_os_contagemresultado_contratadacod_Internalname, "Requisitado", "", "", lblTextblocksdt_wp_os_contagemresultado_contratadacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavSdt_wp_os_contagemresultado_contratadacod, dynavSdt_wp_os_contagemresultado_contratadacod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV9SDT_WP_OS.gxTpr_Contagemresultado_contratadacod), 6, 0)), 1, dynavSdt_wp_os_contagemresultado_contratadacod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,102);\"", "", true, "HLP_WP_OS_Vs3.htm");
            dynavSdt_wp_os_contagemresultado_contratadacod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9SDT_WP_OS.gxTpr_Contagemresultado_contratadacod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSdt_wp_os_contagemresultado_contratadacod_Internalname, "Values", (String)(dynavSdt_wp_os_contagemresultado_contratadacod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksdt_wp_os_contagemresultado_responsavel_Internalname, "Respons�vel", "", "", lblTextblocksdt_wp_os_contagemresultado_responsavel_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSdt_wp_os_contagemresultado_responsavel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9SDT_WP_OS.gxTpr_Contagemresultado_responsavel), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV9SDT_WP_OS.gxTpr_Contagemresultado_responsavel), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,107);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSdt_wp_os_contagemresultado_responsavel_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table8_110_T42( true) ;
         }
         else
         {
            wb_table8_110_T42( false) ;
         }
         return  ;
      }

      protected void wb_table8_110_T42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksdt_wp_os_contagemresultado_pfbfsimp_Internalname, "Bruto", "", "", lblTextblocksdt_wp_os_contagemresultado_pfbfsimp_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 125,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSdt_wp_os_contagemresultado_pfbfsimp_Internalname, StringUtil.LTrim( StringUtil.NToC( AV9SDT_WP_OS.gxTpr_Contagemresultado_pfbfsimp, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV9SDT_WP_OS.gxTpr_Contagemresultado_pfbfsimp, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,125);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSdt_wp_os_contagemresultado_pfbfsimp_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksdt_wp_os_contagemresultado_pflfsimp_Internalname, "L�quido", "", "", lblTextblocksdt_wp_os_contagemresultado_pflfsimp_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 130,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSdt_wp_os_contagemresultado_pflfsimp_Internalname, StringUtil.LTrim( StringUtil.NToC( AV9SDT_WP_OS.gxTpr_Contagemresultado_pflfsimp, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV9SDT_WP_OS.gxTpr_Contagemresultado_pflfsimp, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,130);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSdt_wp_os_contagemresultado_pflfsimp_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksdt_wp_os_contagemresultado_contratadaorigemcod_Internalname, "Origem da Refer�ncia", "", "", lblTextblocksdt_wp_os_contagemresultado_contratadaorigemcod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 135,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavSdt_wp_os_contagemresultado_contratadaorigemcod, dynavSdt_wp_os_contagemresultado_contratadaorigemcod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV9SDT_WP_OS.gxTpr_Contagemresultado_contratadaorigemcod), 6, 0)), 1, dynavSdt_wp_os_contagemresultado_contratadaorigemcod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,135);\"", "", true, "HLP_WP_OS_Vs3.htm");
            dynavSdt_wp_os_contagemresultado_contratadaorigemcod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9SDT_WP_OS.gxTpr_Contagemresultado_contratadaorigemcod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSdt_wp_os_contagemresultado_contratadaorigemcod_Internalname, "Values", (String)(dynavSdt_wp_os_contagemresultado_contratadaorigemcod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksdt_wp_os_contagemresultado_contadorfscod_Internalname, "Respons�vel na Origem", "", "", lblTextblocksdt_wp_os_contagemresultado_contadorfscod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 140,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavSdt_wp_os_contagemresultado_contadorfscod, dynavSdt_wp_os_contagemresultado_contadorfscod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV9SDT_WP_OS.gxTpr_Contagemresultado_contadorfscod), 6, 0)), 1, dynavSdt_wp_os_contagemresultado_contadorfscod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,140);\"", "", true, "HLP_WP_OS_Vs3.htm");
            dynavSdt_wp_os_contagemresultado_contadorfscod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9SDT_WP_OS.gxTpr_Contagemresultado_contadorfscod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSdt_wp_os_contagemresultado_contadorfscod_Internalname, "Values", (String)(dynavSdt_wp_os_contagemresultado_contadorfscod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksdt_wp_os_contratoservicos_localexec_Internalname, "Data de Entrega Real", "", "", lblTextblocksdt_wp_os_contratoservicos_localexec_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 145,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavSdt_wp_os_contratoservicos_localexec, cmbavSdt_wp_os_contratoservicos_localexec_Internalname, StringUtil.RTrim( AV9SDT_WP_OS.gxTpr_Contratoservicos_localexec), 1, cmbavSdt_wp_os_contratoservicos_localexec_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,145);\"", "", true, "HLP_WP_OS_Vs3.htm");
            cmbavSdt_wp_os_contratoservicos_localexec.CurrentValue = StringUtil.RTrim( AV9SDT_WP_OS.gxTpr_Contratoservicos_localexec);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSdt_wp_os_contratoservicos_localexec_Internalname, "Values", (String)(cmbavSdt_wp_os_contratoservicos_localexec.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksdt_wp_os_contagemresultado_prazoanalise_Internalname, "Prazo de An�lise", "", "", lblTextblocksdt_wp_os_contagemresultado_prazoanalise_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 150,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavSdt_wp_os_contagemresultado_prazoanalise_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavSdt_wp_os_contagemresultado_prazoanalise_Internalname, context.localUtil.Format(AV9SDT_WP_OS.gxTpr_Contagemresultado_prazoanalise, "99/99/99"), context.localUtil.Format( AV9SDT_WP_OS.gxTpr_Contagemresultado_prazoanalise, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,150);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSdt_wp_os_contagemresultado_prazoanalise_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_OS_Vs3.htm");
            GxWebStd.gx_bitmap( context, edtavSdt_wp_os_contagemresultado_prazoanalise_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksdt_wp_os_contagemresultado_dataentrega_Internalname, "Prazo de Entrega", "", "", lblTextblocksdt_wp_os_contagemresultado_dataentrega_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 155,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavSdt_wp_os_contagemresultado_dataentrega_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavSdt_wp_os_contagemresultado_dataentrega_Internalname, context.localUtil.Format(AV9SDT_WP_OS.gxTpr_Contagemresultado_dataentrega, "99/99/99"), context.localUtil.Format( AV9SDT_WP_OS.gxTpr_Contagemresultado_dataentrega, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,155);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSdt_wp_os_contagemresultado_dataentrega_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_OS_Vs3.htm");
            GxWebStd.gx_bitmap( context, edtavSdt_wp_os_contagemresultado_dataentrega_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksdt_wp_os_contagemresultado_sistemacod_Internalname, "Sistema", "", "", lblTextblocksdt_wp_os_contagemresultado_sistemacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 160,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSdt_wp_os_contagemresultado_sistemacod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9SDT_WP_OS.gxTpr_Contagemresultado_sistemacod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV9SDT_WP_OS.gxTpr_Contagemresultado_sistemacod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,160);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSdt_wp_os_contagemresultado_sistemacod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksdt_wp_os_modulo_codigo_Internalname, "M�dulo", "", "", lblTextblocksdt_wp_os_modulo_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 165,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavSdt_wp_os_modulo_codigo, dynavSdt_wp_os_modulo_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV9SDT_WP_OS.gxTpr_Modulo_codigo), 6, 0)), 1, dynavSdt_wp_os_modulo_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,165);\"", "", true, "HLP_WP_OS_Vs3.htm");
            dynavSdt_wp_os_modulo_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9SDT_WP_OS.gxTpr_Modulo_codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSdt_wp_os_modulo_codigo_Internalname, "Values", (String)(dynavSdt_wp_os_modulo_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksdt_wp_os_contagemresultado_fncusrcod_Internalname, "Fun��o de Usu�rio", "", "", lblTextblocksdt_wp_os_contagemresultado_fncusrcod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 170,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavSdt_wp_os_contagemresultado_fncusrcod, dynavSdt_wp_os_contagemresultado_fncusrcod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV9SDT_WP_OS.gxTpr_Contagemresultado_fncusrcod), 6, 0)), 1, dynavSdt_wp_os_contagemresultado_fncusrcod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,170);\"", "", true, "HLP_WP_OS_Vs3.htm");
            dynavSdt_wp_os_contagemresultado_fncusrcod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9SDT_WP_OS.gxTpr_Contagemresultado_fncusrcod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSdt_wp_os_contagemresultado_fncusrcod_Internalname, "Values", (String)(dynavSdt_wp_os_contagemresultado_fncusrcod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksdt_wp_os_contagemresultado_sistemagestor_Internalname, "Gestor Respons�vel pelo Sistema", "", "", lblTextblocksdt_wp_os_contagemresultado_sistemagestor_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtavSdt_wp_os_contagemresultado_sistemagestor_Internalname, StringUtil.RTrim( AV9SDT_WP_OS.gxTpr_Contagemresultado_sistemagestor), StringUtil.RTrim( context.localUtil.Format( AV9SDT_WP_OS.gxTpr_Contagemresultado_sistemagestor, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSdt_wp_os_contagemresultado_sistemagestor_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavSdt_wp_os_contagemresultado_sistemagestor_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksdt_wp_os_contagemresultado_link_Internalname, "Link", "", "", lblTextblocksdt_wp_os_contagemresultado_link_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 180,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSdt_wp_os_contagemresultado_link_Internalname, AV9SDT_WP_OS.gxTpr_Contagemresultado_link, AV9SDT_WP_OS.gxTpr_Contagemresultado_link, TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,180);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSdt_wp_os_contagemresultado_link_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 360, "px", 1, "row", 2097152, 0, 1, 0, 1, 0, -1, true, "", "left", false, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Control Group */
            GxWebStd.gx_group_start( context, grpUnnamedgroup1_Internalname, "Anexos", 1, 0, "px", 0, "px", "Group", "", "HLP_WP_OS_Vs3.htm");
            wb_table9_184_T42( true) ;
         }
         else
         {
            wb_table9_184_T42( false) ;
         }
         return  ;
      }

      protected void wb_table9_184_T42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_17_T42e( true) ;
         }
         else
         {
            wb_table4_17_T42e( false) ;
         }
      }

      protected void wb_table9_184_T42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblGroupanexos_Internalname, tblGroupanexos_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            if ( ! isFullAjaxMode( ) )
            {
               /* WebComponent */
               GxWebStd.gx_hidden_field( context, "W0187"+"", StringUtil.RTrim( WebComp_Wc_contagemresultadoevidencias_Component));
               context.WriteHtmlText( "<div") ;
               GxWebStd.ClassAttribute( context, "gxwebcomponent");
               context.WriteHtmlText( " id=\""+"gxHTMLWrpW0187"+""+"\""+"") ;
               context.WriteHtmlText( ">") ;
               if ( StringUtil.Len( WebComp_Wc_contagemresultadoevidencias_Component) != 0 )
               {
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldWc_contagemresultadoevidencias), StringUtil.Lower( WebComp_Wc_contagemresultadoevidencias_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspStartCmp("gxHTMLWrpW0187"+"");
                  }
                  WebComp_Wc_contagemresultadoevidencias.componentdraw();
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldWc_contagemresultadoevidencias), StringUtil.Lower( WebComp_Wc_contagemresultadoevidencias_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspEndCmp();
                  }
               }
               context.WriteHtmlText( "</div>") ;
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_184_T42e( true) ;
         }
         else
         {
            wb_table9_184_T42e( false) ;
         }
      }

      protected void wb_table8_110_T42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedlblvincularcom_Internalname, tblTablemergedlblvincularcom_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblvincularcom_Internalname, "Vincular com", "", "", lblLblvincularcom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSdt_wp_os_contagemresultado_osvinculada_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9SDT_WP_OS.gxTpr_Contagemresultado_osvinculada), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV9SDT_WP_OS.gxTpr_Contagemresultado_osvinculada), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,115);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSdt_wp_os_contagemresultado_osvinculada_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSdt_wp_os_contagemresultado_dmnvinculada_Internalname, AV9SDT_WP_OS.gxTpr_Contagemresultado_dmnvinculada, StringUtil.RTrim( context.localUtil.Format( AV9SDT_WP_OS.gxTpr_Contagemresultado_dmnvinculada, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,117);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "N� OS", edtavSdt_wp_os_contagemresultado_dmnvinculada_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSdt_wp_os_contagemresultado_dmnvinculadaref_Internalname, AV9SDT_WP_OS.gxTpr_Contagemresultado_dmnvinculadaref, StringUtil.RTrim( context.localUtil.Format( AV9SDT_WP_OS.gxTpr_Contagemresultado_dmnvinculadaref, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,119);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "N� OS Refer�ncia", edtavSdt_wp_os_contagemresultado_dmnvinculadaref_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_110_T42e( true) ;
         }
         else
         {
            wb_table8_110_T42e( false) ;
         }
      }

      protected void wb_table7_32_T42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedsdt_wp_os_contagemresultado_requisitadoporcontratante_Internalname, tblTablemergedsdt_wp_os_contagemresultado_requisitadoporcontratante_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSdt_wp_os_contagemresultado_requisitadoporcontratante_Internalname, StringUtil.BoolToStr( AV9SDT_WP_OS.gxTpr_Contagemresultado_requisitadoporcontratante), StringUtil.BoolToStr( AV9SDT_WP_OS.gxTpr_Contagemresultado_requisitadoporcontratante), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,35);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSdt_wp_os_contagemresultado_requisitadoporcontratante_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblSdt_wp_os_contagemresultado_requisitadoporcontratante_righttext_Internalname, "�(Contratante)", "", "", lblSdt_wp_os_contagemresultado_requisitadoporcontratante_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_OS_Vs3.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_32_T42e( true) ;
         }
         else
         {
            wb_table7_32_T42e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAT42( ) ;
         WST42( ) ;
         WET42( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         if ( ! ( WebComp_Wc_contagemresultadoevidencias == null ) )
         {
            if ( StringUtil.Len( WebComp_Wc_contagemresultadoevidencias_Component) != 0 )
            {
               WebComp_Wc_contagemresultadoevidencias.componentthemes();
            }
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203271051827");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_os_vs3.js", "?20203271051828");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/TabsPanel/BootstrapTabsPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblocktitle_Internalname = "TEXTBLOCKTITLE";
         lblTextblocksdt_wp_os_contagemresultado_demandafm_Internalname = "TEXTBLOCKSDT_WP_OS_CONTAGEMRESULTADO_DEMANDAFM";
         edtavSdt_wp_os_contagemresultado_demandafm_Internalname = "SDT_WP_OS_CONTAGEMRESULTADO_DEMANDAFM";
         lblTextblocksdt_wp_os_usuario_cargouonom_Internalname = "TEXTBLOCKSDT_WP_OS_USUARIO_CARGOUONOM";
         edtavSdt_wp_os_usuario_cargouonom_Internalname = "SDT_WP_OS_USUARIO_CARGOUONOM";
         lblTextblocksdt_wp_os_contagemresultado_requisitadoporcontratante_Internalname = "TEXTBLOCKSDT_WP_OS_CONTAGEMRESULTADO_REQUISITADOPORCONTRATANTE";
         edtavSdt_wp_os_contagemresultado_requisitadoporcontratante_Internalname = "SDT_WP_OS_CONTAGEMRESULTADO_REQUISITADOPORCONTRATANTE";
         lblSdt_wp_os_contagemresultado_requisitadoporcontratante_righttext_Internalname = "SDT_WP_OS_CONTAGEMRESULTADO_REQUISITADOPORCONTRATANTE_RIGHTTEXT";
         tblTablemergedsdt_wp_os_contagemresultado_requisitadoporcontratante_Internalname = "TABLEMERGEDSDT_WP_OS_CONTAGEMRESULTADO_REQUISITADOPORCONTRATANTE";
         lblTextblocksdt_wp_os_contagemresultado_owner_Internalname = "TEXTBLOCKSDT_WP_OS_CONTAGEMRESULTADO_OWNER";
         edtavSdt_wp_os_contagemresultado_owner_Internalname = "SDT_WP_OS_CONTAGEMRESULTADO_OWNER";
         lblTextblocksdt_wp_os_contagemresultado_datadmn_Internalname = "TEXTBLOCKSDT_WP_OS_CONTAGEMRESULTADO_DATADMN";
         edtavSdt_wp_os_contagemresultado_datadmn_Internalname = "SDT_WP_OS_CONTAGEMRESULTADO_DATADMN";
         lblTextblocksdt_wp_os_contagemresultado_descricao_Internalname = "TEXTBLOCKSDT_WP_OS_CONTAGEMRESULTADO_DESCRICAO";
         edtavSdt_wp_os_contagemresultado_descricao_Internalname = "SDT_WP_OS_CONTAGEMRESULTADO_DESCRICAO";
         lblTextblocksdt_wp_os_contagemresultado_cntcod_Internalname = "TEXTBLOCKSDT_WP_OS_CONTAGEMRESULTADO_CNTCOD";
         edtavSdt_wp_os_contagemresultado_cntcod_Internalname = "SDT_WP_OS_CONTAGEMRESULTADO_CNTCOD";
         lblTextblocksdt_wp_os_contagemresultado_servicogrupo_Internalname = "TEXTBLOCKSDT_WP_OS_CONTAGEMRESULTADO_SERVICOGRUPO";
         dynavSdt_wp_os_contagemresultado_servicogrupo_Internalname = "SDT_WP_OS_CONTAGEMRESULTADO_SERVICOGRUPO";
         lblTextblocksdt_wp_os_contagemresultado_servico_Internalname = "TEXTBLOCKSDT_WP_OS_CONTAGEMRESULTADO_SERVICO";
         dynavSdt_wp_os_contagemresultado_servico_Internalname = "SDT_WP_OS_CONTAGEMRESULTADO_SERVICO";
         lblTextblocksdt_wp_os_contagemresultado_cntsrvcod_Internalname = "TEXTBLOCKSDT_WP_OS_CONTAGEMRESULTADO_CNTSRVCOD";
         edtavSdt_wp_os_contagemresultado_cntsrvcod_Internalname = "SDT_WP_OS_CONTAGEMRESULTADO_CNTSRVCOD";
         lblTextblocksdt_wp_os_contratoservicos_undcntnome_Internalname = "TEXTBLOCKSDT_WP_OS_CONTRATOSERVICOS_UNDCNTNOME";
         edtavSdt_wp_os_contratoservicos_undcntnome_Internalname = "SDT_WP_OS_CONTRATOSERVICOS_UNDCNTNOME";
         lblTextblocksdt_wp_os_contratoservicos_qntuntcns_Internalname = "TEXTBLOCKSDT_WP_OS_CONTRATOSERVICOS_QNTUNTCNS";
         edtavSdt_wp_os_contratoservicos_qntuntcns_Internalname = "SDT_WP_OS_CONTRATOSERVICOS_QNTUNTCNS";
         lblTextblocksdt_wp_os_contagemresultado_quantidadesolicitada_Internalname = "TEXTBLOCKSDT_WP_OS_CONTAGEMRESULTADO_QUANTIDADESOLICITADA";
         edtavSdt_wp_os_contagemresultado_quantidadesolicitada_Internalname = "SDT_WP_OS_CONTAGEMRESULTADO_QUANTIDADESOLICITADA";
         lblTextblocksdt_wp_os_contratoservicosprazo_complexidade_Internalname = "TEXTBLOCKSDT_WP_OS_CONTRATOSERVICOSPRAZO_COMPLEXIDADE";
         cmbavSdt_wp_os_contratoservicosprazo_complexidade_Internalname = "SDT_WP_OS_CONTRATOSERVICOSPRAZO_COMPLEXIDADE";
         lblTextblocksdt_wp_os_contratoservicosprioridade_codigo_Internalname = "TEXTBLOCKSDT_WP_OS_CONTRATOSERVICOSPRIORIDADE_CODIGO";
         edtavSdt_wp_os_contratoservicosprioridade_codigo_Internalname = "SDT_WP_OS_CONTRATOSERVICOSPRIORIDADE_CODIGO";
         lblTextblocksdt_wp_os_contagemresultado_contratadacod_Internalname = "TEXTBLOCKSDT_WP_OS_CONTAGEMRESULTADO_CONTRATADACOD";
         dynavSdt_wp_os_contagemresultado_contratadacod_Internalname = "SDT_WP_OS_CONTAGEMRESULTADO_CONTRATADACOD";
         lblTextblocksdt_wp_os_contagemresultado_responsavel_Internalname = "TEXTBLOCKSDT_WP_OS_CONTAGEMRESULTADO_RESPONSAVEL";
         edtavSdt_wp_os_contagemresultado_responsavel_Internalname = "SDT_WP_OS_CONTAGEMRESULTADO_RESPONSAVEL";
         lblLblvincularcom_Internalname = "LBLVINCULARCOM";
         edtavSdt_wp_os_contagemresultado_osvinculada_Internalname = "SDT_WP_OS_CONTAGEMRESULTADO_OSVINCULADA";
         edtavSdt_wp_os_contagemresultado_dmnvinculada_Internalname = "SDT_WP_OS_CONTAGEMRESULTADO_DMNVINCULADA";
         edtavSdt_wp_os_contagemresultado_dmnvinculadaref_Internalname = "SDT_WP_OS_CONTAGEMRESULTADO_DMNVINCULADAREF";
         tblTablemergedlblvincularcom_Internalname = "TABLEMERGEDLBLVINCULARCOM";
         lblTextblocksdt_wp_os_contagemresultado_pfbfsimp_Internalname = "TEXTBLOCKSDT_WP_OS_CONTAGEMRESULTADO_PFBFSIMP";
         edtavSdt_wp_os_contagemresultado_pfbfsimp_Internalname = "SDT_WP_OS_CONTAGEMRESULTADO_PFBFSIMP";
         lblTextblocksdt_wp_os_contagemresultado_pflfsimp_Internalname = "TEXTBLOCKSDT_WP_OS_CONTAGEMRESULTADO_PFLFSIMP";
         edtavSdt_wp_os_contagemresultado_pflfsimp_Internalname = "SDT_WP_OS_CONTAGEMRESULTADO_PFLFSIMP";
         lblTextblocksdt_wp_os_contagemresultado_contratadaorigemcod_Internalname = "TEXTBLOCKSDT_WP_OS_CONTAGEMRESULTADO_CONTRATADAORIGEMCOD";
         dynavSdt_wp_os_contagemresultado_contratadaorigemcod_Internalname = "SDT_WP_OS_CONTAGEMRESULTADO_CONTRATADAORIGEMCOD";
         lblTextblocksdt_wp_os_contagemresultado_contadorfscod_Internalname = "TEXTBLOCKSDT_WP_OS_CONTAGEMRESULTADO_CONTADORFSCOD";
         dynavSdt_wp_os_contagemresultado_contadorfscod_Internalname = "SDT_WP_OS_CONTAGEMRESULTADO_CONTADORFSCOD";
         lblTextblocksdt_wp_os_contratoservicos_localexec_Internalname = "TEXTBLOCKSDT_WP_OS_CONTRATOSERVICOS_LOCALEXEC";
         cmbavSdt_wp_os_contratoservicos_localexec_Internalname = "SDT_WP_OS_CONTRATOSERVICOS_LOCALEXEC";
         lblTextblocksdt_wp_os_contagemresultado_prazoanalise_Internalname = "TEXTBLOCKSDT_WP_OS_CONTAGEMRESULTADO_PRAZOANALISE";
         edtavSdt_wp_os_contagemresultado_prazoanalise_Internalname = "SDT_WP_OS_CONTAGEMRESULTADO_PRAZOANALISE";
         lblTextblocksdt_wp_os_contagemresultado_dataentrega_Internalname = "TEXTBLOCKSDT_WP_OS_CONTAGEMRESULTADO_DATAENTREGA";
         edtavSdt_wp_os_contagemresultado_dataentrega_Internalname = "SDT_WP_OS_CONTAGEMRESULTADO_DATAENTREGA";
         lblTextblocksdt_wp_os_contagemresultado_sistemacod_Internalname = "TEXTBLOCKSDT_WP_OS_CONTAGEMRESULTADO_SISTEMACOD";
         edtavSdt_wp_os_contagemresultado_sistemacod_Internalname = "SDT_WP_OS_CONTAGEMRESULTADO_SISTEMACOD";
         lblTextblocksdt_wp_os_modulo_codigo_Internalname = "TEXTBLOCKSDT_WP_OS_MODULO_CODIGO";
         dynavSdt_wp_os_modulo_codigo_Internalname = "SDT_WP_OS_MODULO_CODIGO";
         lblTextblocksdt_wp_os_contagemresultado_fncusrcod_Internalname = "TEXTBLOCKSDT_WP_OS_CONTAGEMRESULTADO_FNCUSRCOD";
         dynavSdt_wp_os_contagemresultado_fncusrcod_Internalname = "SDT_WP_OS_CONTAGEMRESULTADO_FNCUSRCOD";
         lblTextblocksdt_wp_os_contagemresultado_sistemagestor_Internalname = "TEXTBLOCKSDT_WP_OS_CONTAGEMRESULTADO_SISTEMAGESTOR";
         edtavSdt_wp_os_contagemresultado_sistemagestor_Internalname = "SDT_WP_OS_CONTAGEMRESULTADO_SISTEMAGESTOR";
         lblTextblocksdt_wp_os_contagemresultado_link_Internalname = "TEXTBLOCKSDT_WP_OS_CONTAGEMRESULTADO_LINK";
         edtavSdt_wp_os_contagemresultado_link_Internalname = "SDT_WP_OS_CONTAGEMRESULTADO_LINK";
         tblGroupanexos_Internalname = "GROUPANEXOS";
         grpUnnamedgroup1_Internalname = "UNNAMEDGROUP1";
         tblTablesolicita_Internalname = "TABLESOLICITA";
         tblTabledescrica_Internalname = "TABLEDESCRICA";
         tblTablerequisitos_Internalname = "TABLEREQUISITOS";
         Gxuitabspanel_tabmain_Internalname = "GXUITABSPANEL_TABMAIN";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtnbntartefatos_Internalname = "BTNBNTARTEFATOS";
         bttBtnenter_Internalname = "BTNENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtavSdt_wp_os_contagemresultado_codigo_Internalname = "SDT_WP_OS_CONTAGEMRESULTADO_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavSdt_wp_os_contagemresultado_requisitadoporcontratante_Jsonclick = "";
         edtavSdt_wp_os_contagemresultado_dmnvinculadaref_Jsonclick = "";
         edtavSdt_wp_os_contagemresultado_dmnvinculada_Jsonclick = "";
         edtavSdt_wp_os_contagemresultado_osvinculada_Jsonclick = "";
         edtavSdt_wp_os_contagemresultado_link_Jsonclick = "";
         edtavSdt_wp_os_contagemresultado_sistemagestor_Jsonclick = "";
         edtavSdt_wp_os_contagemresultado_sistemagestor_Enabled = 0;
         dynavSdt_wp_os_contagemresultado_fncusrcod_Jsonclick = "";
         dynavSdt_wp_os_modulo_codigo_Jsonclick = "";
         edtavSdt_wp_os_contagemresultado_sistemacod_Jsonclick = "";
         edtavSdt_wp_os_contagemresultado_dataentrega_Jsonclick = "";
         edtavSdt_wp_os_contagemresultado_prazoanalise_Jsonclick = "";
         cmbavSdt_wp_os_contratoservicos_localexec_Jsonclick = "";
         dynavSdt_wp_os_contagemresultado_contadorfscod_Jsonclick = "";
         dynavSdt_wp_os_contagemresultado_contratadaorigemcod_Jsonclick = "";
         edtavSdt_wp_os_contagemresultado_pflfsimp_Jsonclick = "";
         edtavSdt_wp_os_contagemresultado_pfbfsimp_Jsonclick = "";
         edtavSdt_wp_os_contagemresultado_responsavel_Jsonclick = "";
         dynavSdt_wp_os_contagemresultado_contratadacod_Jsonclick = "";
         edtavSdt_wp_os_contratoservicosprioridade_codigo_Jsonclick = "";
         cmbavSdt_wp_os_contratoservicosprazo_complexidade_Jsonclick = "";
         edtavSdt_wp_os_contagemresultado_quantidadesolicitada_Jsonclick = "";
         edtavSdt_wp_os_contratoservicos_qntuntcns_Jsonclick = "";
         edtavSdt_wp_os_contratoservicos_undcntnome_Jsonclick = "";
         edtavSdt_wp_os_contagemresultado_cntsrvcod_Jsonclick = "";
         dynavSdt_wp_os_contagemresultado_servico_Jsonclick = "";
         dynavSdt_wp_os_contagemresultado_servicogrupo_Jsonclick = "";
         edtavSdt_wp_os_contagemresultado_cntcod_Jsonclick = "";
         edtavSdt_wp_os_contagemresultado_descricao_Jsonclick = "";
         edtavSdt_wp_os_contagemresultado_datadmn_Jsonclick = "";
         edtavSdt_wp_os_contagemresultado_owner_Jsonclick = "";
         edtavSdt_wp_os_usuario_cargouonom_Jsonclick = "";
         edtavSdt_wp_os_contagemresultado_demandafm_Jsonclick = "";
         edtavSdt_wp_os_contagemresultado_sistemagestor_Enabled = -1;
         edtavSdt_wp_os_contagemresultado_codigo_Jsonclick = "";
         edtavSdt_wp_os_contagemresultado_codigo_Visible = 1;
         Gxuitabspanel_tabmain_Designtimetabs = "[{\"id\":\"TabSolicitar\"},{\"id\":\"TabDescricao\"},{\"id\":\"TabRequisitos\"}]";
         Gxuitabspanel_tabmain_Autoscroll = Convert.ToBoolean( -1);
         Gxuitabspanel_tabmain_Autoheight = Convert.ToBoolean( -1);
         Gxuitabspanel_tabmain_Autowidth = Convert.ToBoolean( 0);
         Gxuitabspanel_tabmain_Cls = "Tabs";
         Gxuitabspanel_tabmain_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Nova Ordem de Servi�o";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOBNTARTEFATOS'","{handler:'E15T41',iparms:[],oparms:[]}");
         setEventMetadata("ENTER","{handler:'E12T42',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV9SDT_WP_OS = new SdtSDT_WP_OS(context);
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         OldWc_contagemresultadoevidencias = "";
         WebComp_Wc_contagemresultadoevidencias_Component = "";
         scmdbuf = "";
         H00T42_A157ServicoGrupo_Codigo = new int[1] ;
         H00T42_A158ServicoGrupo_Descricao = new String[] {""} ;
         H00T43_A155Servico_Codigo = new int[1] ;
         H00T43_A608Servico_Nome = new String[] {""} ;
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         H00T44_A155Servico_Codigo = new int[1] ;
         H00T44_A608Servico_Nome = new String[] {""} ;
         H00T45_A157ServicoGrupo_Codigo = new int[1] ;
         H00T45_A158ServicoGrupo_Descricao = new String[] {""} ;
         H00T46_A40Contratada_PessoaCod = new int[1] ;
         H00T46_A39Contratada_Codigo = new int[1] ;
         H00T46_A41Contratada_PessoaNom = new String[] {""} ;
         H00T46_n41Contratada_PessoaNom = new bool[] {false} ;
         H00T46_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00T47_A40Contratada_PessoaCod = new int[1] ;
         H00T47_A39Contratada_Codigo = new int[1] ;
         H00T47_A41Contratada_PessoaNom = new String[] {""} ;
         H00T47_n41Contratada_PessoaNom = new bool[] {false} ;
         H00T47_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00T48_A57Usuario_PessoaCod = new int[1] ;
         H00T48_A1Usuario_Codigo = new int[1] ;
         H00T48_A58Usuario_PessoaNom = new String[] {""} ;
         H00T48_n58Usuario_PessoaNom = new bool[] {false} ;
         H00T49_A146Modulo_Codigo = new int[1] ;
         H00T49_A145Modulo_Sigla = new String[] {""} ;
         H00T49_A127Sistema_Codigo = new int[1] ;
         H00T410_A161FuncaoUsuario_Codigo = new int[1] ;
         H00T410_A162FuncaoUsuario_Nome = new String[] {""} ;
         H00T410_A127Sistema_Codigo = new int[1] ;
         H00T410_A164FuncaoUsuario_Ativo = new bool[] {false} ;
         AV6Messages = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
         AV5Message = new SdtMessages_Message(context);
         AV7Context = new wwpbaseobjects.SdtWWPContext(context);
         sStyleString = "";
         lblTextblocktitle_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         bttBtnbntartefatos_Jsonclick = "";
         bttBtnenter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         lblTextblocksdt_wp_os_contagemresultado_demandafm_Jsonclick = "";
         lblTextblocksdt_wp_os_usuario_cargouonom_Jsonclick = "";
         lblTextblocksdt_wp_os_contagemresultado_requisitadoporcontratante_Jsonclick = "";
         lblTextblocksdt_wp_os_contagemresultado_owner_Jsonclick = "";
         lblTextblocksdt_wp_os_contagemresultado_datadmn_Jsonclick = "";
         lblTextblocksdt_wp_os_contagemresultado_descricao_Jsonclick = "";
         lblTextblocksdt_wp_os_contagemresultado_cntcod_Jsonclick = "";
         lblTextblocksdt_wp_os_contagemresultado_servicogrupo_Jsonclick = "";
         lblTextblocksdt_wp_os_contagemresultado_servico_Jsonclick = "";
         lblTextblocksdt_wp_os_contagemresultado_cntsrvcod_Jsonclick = "";
         lblTextblocksdt_wp_os_contratoservicos_undcntnome_Jsonclick = "";
         lblTextblocksdt_wp_os_contratoservicos_qntuntcns_Jsonclick = "";
         lblTextblocksdt_wp_os_contagemresultado_quantidadesolicitada_Jsonclick = "";
         lblTextblocksdt_wp_os_contratoservicosprazo_complexidade_Jsonclick = "";
         lblTextblocksdt_wp_os_contratoservicosprioridade_codigo_Jsonclick = "";
         lblTextblocksdt_wp_os_contagemresultado_contratadacod_Jsonclick = "";
         lblTextblocksdt_wp_os_contagemresultado_responsavel_Jsonclick = "";
         lblTextblocksdt_wp_os_contagemresultado_pfbfsimp_Jsonclick = "";
         lblTextblocksdt_wp_os_contagemresultado_pflfsimp_Jsonclick = "";
         lblTextblocksdt_wp_os_contagemresultado_contratadaorigemcod_Jsonclick = "";
         lblTextblocksdt_wp_os_contagemresultado_contadorfscod_Jsonclick = "";
         lblTextblocksdt_wp_os_contratoservicos_localexec_Jsonclick = "";
         lblTextblocksdt_wp_os_contagemresultado_prazoanalise_Jsonclick = "";
         lblTextblocksdt_wp_os_contagemresultado_dataentrega_Jsonclick = "";
         lblTextblocksdt_wp_os_contagemresultado_sistemacod_Jsonclick = "";
         lblTextblocksdt_wp_os_modulo_codigo_Jsonclick = "";
         lblTextblocksdt_wp_os_contagemresultado_fncusrcod_Jsonclick = "";
         lblTextblocksdt_wp_os_contagemresultado_sistemagestor_Jsonclick = "";
         lblTextblocksdt_wp_os_contagemresultado_link_Jsonclick = "";
         lblLblvincularcom_Jsonclick = "";
         lblSdt_wp_os_contagemresultado_requisitadoporcontratante_righttext_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_os_vs3__default(),
            new Object[][] {
                new Object[] {
               H00T42_A157ServicoGrupo_Codigo, H00T42_A158ServicoGrupo_Descricao
               }
               , new Object[] {
               H00T43_A155Servico_Codigo, H00T43_A608Servico_Nome
               }
               , new Object[] {
               H00T44_A155Servico_Codigo, H00T44_A608Servico_Nome
               }
               , new Object[] {
               H00T45_A157ServicoGrupo_Codigo, H00T45_A158ServicoGrupo_Descricao
               }
               , new Object[] {
               H00T46_A40Contratada_PessoaCod, H00T46_A39Contratada_Codigo, H00T46_A41Contratada_PessoaNom, H00T46_n41Contratada_PessoaNom, H00T46_A52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               H00T47_A40Contratada_PessoaCod, H00T47_A39Contratada_Codigo, H00T47_A41Contratada_PessoaNom, H00T47_n41Contratada_PessoaNom, H00T47_A52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               H00T48_A57Usuario_PessoaCod, H00T48_A1Usuario_Codigo, H00T48_A58Usuario_PessoaNom, H00T48_n58Usuario_PessoaNom
               }
               , new Object[] {
               H00T49_A146Modulo_Codigo, H00T49_A145Modulo_Sigla, H00T49_A127Sistema_Codigo
               }
               , new Object[] {
               H00T410_A161FuncaoUsuario_Codigo, H00T410_A162FuncaoUsuario_Nome, H00T410_A127Sistema_Codigo, H00T410_A164FuncaoUsuario_Ativo
               }
            }
         );
         WebComp_Wc_contagemresultadoevidencias = new GeneXus.Http.GXNullWebComponent();
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavSdt_wp_os_contagemresultado_sistemagestor_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nCmpId ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int edtavSdt_wp_os_contagemresultado_codigo_Visible ;
      private int gxdynajaxindex ;
      private int edtavSdt_wp_os_contagemresultado_sistemagestor_Enabled ;
      private int AV46GXV34 ;
      private int idxLst ;
      private decimal AV47Wwpcontext ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gxuitabspanel_tabmain_Width ;
      private String Gxuitabspanel_tabmain_Cls ;
      private String Gxuitabspanel_tabmain_Designtimetabs ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String edtavSdt_wp_os_contagemresultado_codigo_Internalname ;
      private String edtavSdt_wp_os_contagemresultado_codigo_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String OldWc_contagemresultadoevidencias ;
      private String WebComp_Wc_contagemresultadoevidencias_Component ;
      private String scmdbuf ;
      private String edtavSdt_wp_os_contagemresultado_demandafm_Internalname ;
      private String gxwrpcisep ;
      private String edtavSdt_wp_os_contagemresultado_sistemagestor_Internalname ;
      private String edtavSdt_wp_os_usuario_cargouonom_Internalname ;
      private String edtavSdt_wp_os_contagemresultado_requisitadoporcontratante_Internalname ;
      private String edtavSdt_wp_os_contagemresultado_owner_Internalname ;
      private String edtavSdt_wp_os_contagemresultado_datadmn_Internalname ;
      private String edtavSdt_wp_os_contagemresultado_descricao_Internalname ;
      private String edtavSdt_wp_os_contagemresultado_cntcod_Internalname ;
      private String dynavSdt_wp_os_contagemresultado_servicogrupo_Internalname ;
      private String dynavSdt_wp_os_contagemresultado_servico_Internalname ;
      private String edtavSdt_wp_os_contagemresultado_cntsrvcod_Internalname ;
      private String edtavSdt_wp_os_contratoservicos_undcntnome_Internalname ;
      private String edtavSdt_wp_os_contratoservicos_qntuntcns_Internalname ;
      private String edtavSdt_wp_os_contagemresultado_quantidadesolicitada_Internalname ;
      private String cmbavSdt_wp_os_contratoservicosprazo_complexidade_Internalname ;
      private String edtavSdt_wp_os_contratoservicosprioridade_codigo_Internalname ;
      private String dynavSdt_wp_os_contagemresultado_contratadacod_Internalname ;
      private String edtavSdt_wp_os_contagemresultado_responsavel_Internalname ;
      private String edtavSdt_wp_os_contagemresultado_osvinculada_Internalname ;
      private String edtavSdt_wp_os_contagemresultado_dmnvinculada_Internalname ;
      private String edtavSdt_wp_os_contagemresultado_dmnvinculadaref_Internalname ;
      private String edtavSdt_wp_os_contagemresultado_pfbfsimp_Internalname ;
      private String edtavSdt_wp_os_contagemresultado_pflfsimp_Internalname ;
      private String dynavSdt_wp_os_contagemresultado_contratadaorigemcod_Internalname ;
      private String dynavSdt_wp_os_contagemresultado_contadorfscod_Internalname ;
      private String cmbavSdt_wp_os_contratoservicos_localexec_Internalname ;
      private String edtavSdt_wp_os_contagemresultado_prazoanalise_Internalname ;
      private String edtavSdt_wp_os_contagemresultado_dataentrega_Internalname ;
      private String edtavSdt_wp_os_contagemresultado_sistemacod_Internalname ;
      private String dynavSdt_wp_os_modulo_codigo_Internalname ;
      private String dynavSdt_wp_os_contagemresultado_fncusrcod_Internalname ;
      private String edtavSdt_wp_os_contagemresultado_link_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String lblTextblocktitle_Internalname ;
      private String lblTextblocktitle_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtnbntartefatos_Internalname ;
      private String bttBtnbntartefatos_Jsonclick ;
      private String bttBtnenter_Internalname ;
      private String bttBtnenter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTablerequisitos_Internalname ;
      private String tblTabledescrica_Internalname ;
      private String tblTablesolicita_Internalname ;
      private String lblTextblocksdt_wp_os_contagemresultado_demandafm_Internalname ;
      private String lblTextblocksdt_wp_os_contagemresultado_demandafm_Jsonclick ;
      private String edtavSdt_wp_os_contagemresultado_demandafm_Jsonclick ;
      private String lblTextblocksdt_wp_os_usuario_cargouonom_Internalname ;
      private String lblTextblocksdt_wp_os_usuario_cargouonom_Jsonclick ;
      private String edtavSdt_wp_os_usuario_cargouonom_Jsonclick ;
      private String lblTextblocksdt_wp_os_contagemresultado_requisitadoporcontratante_Internalname ;
      private String lblTextblocksdt_wp_os_contagemresultado_requisitadoporcontratante_Jsonclick ;
      private String lblTextblocksdt_wp_os_contagemresultado_owner_Internalname ;
      private String lblTextblocksdt_wp_os_contagemresultado_owner_Jsonclick ;
      private String edtavSdt_wp_os_contagemresultado_owner_Jsonclick ;
      private String lblTextblocksdt_wp_os_contagemresultado_datadmn_Internalname ;
      private String lblTextblocksdt_wp_os_contagemresultado_datadmn_Jsonclick ;
      private String edtavSdt_wp_os_contagemresultado_datadmn_Jsonclick ;
      private String lblTextblocksdt_wp_os_contagemresultado_descricao_Internalname ;
      private String lblTextblocksdt_wp_os_contagemresultado_descricao_Jsonclick ;
      private String edtavSdt_wp_os_contagemresultado_descricao_Jsonclick ;
      private String lblTextblocksdt_wp_os_contagemresultado_cntcod_Internalname ;
      private String lblTextblocksdt_wp_os_contagemresultado_cntcod_Jsonclick ;
      private String edtavSdt_wp_os_contagemresultado_cntcod_Jsonclick ;
      private String lblTextblocksdt_wp_os_contagemresultado_servicogrupo_Internalname ;
      private String lblTextblocksdt_wp_os_contagemresultado_servicogrupo_Jsonclick ;
      private String dynavSdt_wp_os_contagemresultado_servicogrupo_Jsonclick ;
      private String lblTextblocksdt_wp_os_contagemresultado_servico_Internalname ;
      private String lblTextblocksdt_wp_os_contagemresultado_servico_Jsonclick ;
      private String dynavSdt_wp_os_contagemresultado_servico_Jsonclick ;
      private String lblTextblocksdt_wp_os_contagemresultado_cntsrvcod_Internalname ;
      private String lblTextblocksdt_wp_os_contagemresultado_cntsrvcod_Jsonclick ;
      private String edtavSdt_wp_os_contagemresultado_cntsrvcod_Jsonclick ;
      private String lblTextblocksdt_wp_os_contratoservicos_undcntnome_Internalname ;
      private String lblTextblocksdt_wp_os_contratoservicos_undcntnome_Jsonclick ;
      private String edtavSdt_wp_os_contratoservicos_undcntnome_Jsonclick ;
      private String lblTextblocksdt_wp_os_contratoservicos_qntuntcns_Internalname ;
      private String lblTextblocksdt_wp_os_contratoservicos_qntuntcns_Jsonclick ;
      private String edtavSdt_wp_os_contratoservicos_qntuntcns_Jsonclick ;
      private String lblTextblocksdt_wp_os_contagemresultado_quantidadesolicitada_Internalname ;
      private String lblTextblocksdt_wp_os_contagemresultado_quantidadesolicitada_Jsonclick ;
      private String edtavSdt_wp_os_contagemresultado_quantidadesolicitada_Jsonclick ;
      private String lblTextblocksdt_wp_os_contratoservicosprazo_complexidade_Internalname ;
      private String lblTextblocksdt_wp_os_contratoservicosprazo_complexidade_Jsonclick ;
      private String cmbavSdt_wp_os_contratoservicosprazo_complexidade_Jsonclick ;
      private String lblTextblocksdt_wp_os_contratoservicosprioridade_codigo_Internalname ;
      private String lblTextblocksdt_wp_os_contratoservicosprioridade_codigo_Jsonclick ;
      private String edtavSdt_wp_os_contratoservicosprioridade_codigo_Jsonclick ;
      private String lblTextblocksdt_wp_os_contagemresultado_contratadacod_Internalname ;
      private String lblTextblocksdt_wp_os_contagemresultado_contratadacod_Jsonclick ;
      private String dynavSdt_wp_os_contagemresultado_contratadacod_Jsonclick ;
      private String lblTextblocksdt_wp_os_contagemresultado_responsavel_Internalname ;
      private String lblTextblocksdt_wp_os_contagemresultado_responsavel_Jsonclick ;
      private String edtavSdt_wp_os_contagemresultado_responsavel_Jsonclick ;
      private String lblTextblocksdt_wp_os_contagemresultado_pfbfsimp_Internalname ;
      private String lblTextblocksdt_wp_os_contagemresultado_pfbfsimp_Jsonclick ;
      private String edtavSdt_wp_os_contagemresultado_pfbfsimp_Jsonclick ;
      private String lblTextblocksdt_wp_os_contagemresultado_pflfsimp_Internalname ;
      private String lblTextblocksdt_wp_os_contagemresultado_pflfsimp_Jsonclick ;
      private String edtavSdt_wp_os_contagemresultado_pflfsimp_Jsonclick ;
      private String lblTextblocksdt_wp_os_contagemresultado_contratadaorigemcod_Internalname ;
      private String lblTextblocksdt_wp_os_contagemresultado_contratadaorigemcod_Jsonclick ;
      private String dynavSdt_wp_os_contagemresultado_contratadaorigemcod_Jsonclick ;
      private String lblTextblocksdt_wp_os_contagemresultado_contadorfscod_Internalname ;
      private String lblTextblocksdt_wp_os_contagemresultado_contadorfscod_Jsonclick ;
      private String dynavSdt_wp_os_contagemresultado_contadorfscod_Jsonclick ;
      private String lblTextblocksdt_wp_os_contratoservicos_localexec_Internalname ;
      private String lblTextblocksdt_wp_os_contratoservicos_localexec_Jsonclick ;
      private String cmbavSdt_wp_os_contratoservicos_localexec_Jsonclick ;
      private String lblTextblocksdt_wp_os_contagemresultado_prazoanalise_Internalname ;
      private String lblTextblocksdt_wp_os_contagemresultado_prazoanalise_Jsonclick ;
      private String edtavSdt_wp_os_contagemresultado_prazoanalise_Jsonclick ;
      private String lblTextblocksdt_wp_os_contagemresultado_dataentrega_Internalname ;
      private String lblTextblocksdt_wp_os_contagemresultado_dataentrega_Jsonclick ;
      private String edtavSdt_wp_os_contagemresultado_dataentrega_Jsonclick ;
      private String lblTextblocksdt_wp_os_contagemresultado_sistemacod_Internalname ;
      private String lblTextblocksdt_wp_os_contagemresultado_sistemacod_Jsonclick ;
      private String edtavSdt_wp_os_contagemresultado_sistemacod_Jsonclick ;
      private String lblTextblocksdt_wp_os_modulo_codigo_Internalname ;
      private String lblTextblocksdt_wp_os_modulo_codigo_Jsonclick ;
      private String dynavSdt_wp_os_modulo_codigo_Jsonclick ;
      private String lblTextblocksdt_wp_os_contagemresultado_fncusrcod_Internalname ;
      private String lblTextblocksdt_wp_os_contagemresultado_fncusrcod_Jsonclick ;
      private String dynavSdt_wp_os_contagemresultado_fncusrcod_Jsonclick ;
      private String lblTextblocksdt_wp_os_contagemresultado_sistemagestor_Internalname ;
      private String lblTextblocksdt_wp_os_contagemresultado_sistemagestor_Jsonclick ;
      private String edtavSdt_wp_os_contagemresultado_sistemagestor_Jsonclick ;
      private String lblTextblocksdt_wp_os_contagemresultado_link_Internalname ;
      private String lblTextblocksdt_wp_os_contagemresultado_link_Jsonclick ;
      private String edtavSdt_wp_os_contagemresultado_link_Jsonclick ;
      private String grpUnnamedgroup1_Internalname ;
      private String tblGroupanexos_Internalname ;
      private String tblTablemergedlblvincularcom_Internalname ;
      private String lblLblvincularcom_Internalname ;
      private String lblLblvincularcom_Jsonclick ;
      private String edtavSdt_wp_os_contagemresultado_osvinculada_Jsonclick ;
      private String edtavSdt_wp_os_contagemresultado_dmnvinculada_Jsonclick ;
      private String edtavSdt_wp_os_contagemresultado_dmnvinculadaref_Jsonclick ;
      private String tblTablemergedsdt_wp_os_contagemresultado_requisitadoporcontratante_Internalname ;
      private String edtavSdt_wp_os_contagemresultado_requisitadoporcontratante_Jsonclick ;
      private String lblSdt_wp_os_contagemresultado_requisitadoporcontratante_righttext_Internalname ;
      private String lblSdt_wp_os_contagemresultado_requisitadoporcontratante_righttext_Jsonclick ;
      private String Gxuitabspanel_tabmain_Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool Gxuitabspanel_tabmain_Autowidth ;
      private bool Gxuitabspanel_tabmain_Autoheight ;
      private bool Gxuitabspanel_tabmain_Autoscroll ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private GXWebComponent WebComp_Wc_contagemresultadoevidencias ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynavSdt_wp_os_contagemresultado_servicogrupo ;
      private GXCombobox dynavSdt_wp_os_contagemresultado_servico ;
      private GXCombobox cmbavSdt_wp_os_contratoservicosprazo_complexidade ;
      private GXCombobox dynavSdt_wp_os_contagemresultado_contratadacod ;
      private GXCombobox dynavSdt_wp_os_contagemresultado_contratadaorigemcod ;
      private GXCombobox dynavSdt_wp_os_contagemresultado_contadorfscod ;
      private GXCombobox cmbavSdt_wp_os_contratoservicos_localexec ;
      private GXCombobox dynavSdt_wp_os_modulo_codigo ;
      private GXCombobox dynavSdt_wp_os_contagemresultado_fncusrcod ;
      private IDataStoreProvider pr_default ;
      private int[] H00T42_A157ServicoGrupo_Codigo ;
      private String[] H00T42_A158ServicoGrupo_Descricao ;
      private int[] H00T43_A155Servico_Codigo ;
      private String[] H00T43_A608Servico_Nome ;
      private int[] H00T44_A155Servico_Codigo ;
      private String[] H00T44_A608Servico_Nome ;
      private int[] H00T45_A157ServicoGrupo_Codigo ;
      private String[] H00T45_A158ServicoGrupo_Descricao ;
      private int[] H00T46_A40Contratada_PessoaCod ;
      private int[] H00T46_A39Contratada_Codigo ;
      private String[] H00T46_A41Contratada_PessoaNom ;
      private bool[] H00T46_n41Contratada_PessoaNom ;
      private int[] H00T46_A52Contratada_AreaTrabalhoCod ;
      private int[] H00T47_A40Contratada_PessoaCod ;
      private int[] H00T47_A39Contratada_Codigo ;
      private String[] H00T47_A41Contratada_PessoaNom ;
      private bool[] H00T47_n41Contratada_PessoaNom ;
      private int[] H00T47_A52Contratada_AreaTrabalhoCod ;
      private int[] H00T48_A57Usuario_PessoaCod ;
      private int[] H00T48_A1Usuario_Codigo ;
      private String[] H00T48_A58Usuario_PessoaNom ;
      private bool[] H00T48_n58Usuario_PessoaNom ;
      private int[] H00T49_A146Modulo_Codigo ;
      private String[] H00T49_A145Modulo_Sigla ;
      private int[] H00T49_A127Sistema_Codigo ;
      private int[] H00T410_A161FuncaoUsuario_Codigo ;
      private String[] H00T410_A162FuncaoUsuario_Nome ;
      private int[] H00T410_A127Sistema_Codigo ;
      private bool[] H00T410_A164FuncaoUsuario_Ativo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( SdtMessages_Message ))]
      private IGxCollection AV6Messages ;
      private GXWebForm Form ;
      private SdtMessages_Message AV5Message ;
      private wwpbaseobjects.SdtWWPContext AV7Context ;
      private SdtSDT_WP_OS AV9SDT_WP_OS ;
   }

   public class wp_os_vs3__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00T42 ;
          prmH00T42 = new Object[] {
          } ;
          Object[] prmH00T43 ;
          prmH00T43 = new Object[] {
          } ;
          Object[] prmH00T44 ;
          prmH00T44 = new Object[] {
          } ;
          Object[] prmH00T45 ;
          prmH00T45 = new Object[] {
          } ;
          Object[] prmH00T46 ;
          prmH00T46 = new Object[] {
          } ;
          Object[] prmH00T47 ;
          prmH00T47 = new Object[] {
          } ;
          Object[] prmH00T48 ;
          prmH00T48 = new Object[] {
          } ;
          Object[] prmH00T49 ;
          prmH00T49 = new Object[] {
          new Object[] {"@AV9SDT_W_1Contagemresultado_s",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00T410 ;
          prmH00T410 = new Object[] {
          new Object[] {"@AV9SDT_W_1Contagemresultado_s",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00T42", "SELECT [ServicoGrupo_Codigo], [ServicoGrupo_Descricao] FROM [ServicoGrupo] WITH (NOLOCK) ORDER BY [ServicoGrupo_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00T42,0,0,true,false )
             ,new CursorDef("H00T43", "SELECT [Servico_Codigo], [Servico_Nome] FROM [Servico] WITH (NOLOCK) ORDER BY [Servico_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00T43,0,0,true,false )
             ,new CursorDef("H00T44", "SELECT [Servico_Codigo], [Servico_Nome] FROM [Servico] WITH (NOLOCK) ORDER BY [Servico_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00T44,0,0,true,false )
             ,new CursorDef("H00T45", "SELECT [ServicoGrupo_Codigo], [ServicoGrupo_Descricao] FROM [ServicoGrupo] WITH (NOLOCK) ORDER BY [ServicoGrupo_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00T45,0,0,true,false )
             ,new CursorDef("H00T46", "SELECT T2.[Pessoa_Codigo] AS Contratada_PessoaCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contratada_AreaTrabalhoCod] FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) ORDER BY T2.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00T46,0,0,true,false )
             ,new CursorDef("H00T47", "SELECT T2.[Pessoa_Codigo] AS Contratada_PessoaCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contratada_AreaTrabalhoCod] FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) ORDER BY T2.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00T47,0,0,true,false )
             ,new CursorDef("H00T48", "SELECT T2.[Pessoa_Codigo] AS Usuario_PessoaCod, T1.[Usuario_Codigo], T2.[Pessoa_Nome] AS Usuario_PessoaNom FROM ([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) ORDER BY T2.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00T48,0,0,true,false )
             ,new CursorDef("H00T49", "SELECT [Modulo_Codigo], [Modulo_Sigla], [Sistema_Codigo] FROM [Modulo] WITH (NOLOCK) WHERE [Sistema_Codigo] = @AV9SDT_W_1Contagemresultado_s ORDER BY [Modulo_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00T49,0,0,true,false )
             ,new CursorDef("H00T410", "SELECT [FuncaoUsuario_Codigo], [FuncaoUsuario_Nome], [Sistema_Codigo], [FuncaoUsuario_Ativo] FROM [ModuloFuncoes] WITH (NOLOCK) WHERE ([FuncaoUsuario_Ativo] = 1) AND ([Sistema_Codigo] = @AV9SDT_W_1Contagemresultado_s) ORDER BY [FuncaoUsuario_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00T410,0,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
