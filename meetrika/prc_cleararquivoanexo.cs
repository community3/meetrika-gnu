/*
               File: PRC_ClearArquivoAnexo
        Description: Clear Arquivo Anexo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:7:43.54
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_cleararquivoanexo : GXProcedure
   {
      public prc_cleararquivoanexo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_cleararquivoanexo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Codigo ,
                           String aP1_Tabela )
      {
         this.AV8Codigo = aP0_Codigo;
         this.AV9Tabela = aP1_Tabela;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_Codigo ,
                                 String aP1_Tabela )
      {
         prc_cleararquivoanexo objprc_cleararquivoanexo;
         objprc_cleararquivoanexo = new prc_cleararquivoanexo();
         objprc_cleararquivoanexo.AV8Codigo = aP0_Codigo;
         objprc_cleararquivoanexo.AV9Tabela = aP1_Tabela;
         objprc_cleararquivoanexo.context.SetSubmitInitialConfig(context);
         objprc_cleararquivoanexo.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_cleararquivoanexo);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_cleararquivoanexo)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( (0==AV8Codigo) )
         {
         }
         else if ( StringUtil.StrCmp(AV9Tabela, "Contrato") == 0 )
         {
            /* Using cursor P002C2 */
            pr_default.execute(0, new Object[] {AV8Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A108ContratoArquivosAnexos_Codigo = P002C2_A108ContratoArquivosAnexos_Codigo[0];
               A112ContratoArquivosAnexos_NomeArq = P002C2_A112ContratoArquivosAnexos_NomeArq[0];
               A109ContratoArquivosAnexos_TipoArq = P002C2_A109ContratoArquivosAnexos_TipoArq[0];
               A113ContratoArquivosAnexos_Data = P002C2_A113ContratoArquivosAnexos_Data[0];
               A111ContratoArquivosAnexos_Arquivo = P002C2_A111ContratoArquivosAnexos_Arquivo[0];
               A111ContratoArquivosAnexos_Arquivo = "";
               A112ContratoArquivosAnexos_NomeArq = "";
               A109ContratoArquivosAnexos_TipoArq = "";
               A113ContratoArquivosAnexos_Data = (DateTime)(DateTime.MinValue);
               /* Using cursor P002C3 */
               A109ContratoArquivosAnexos_TipoArq = FileUtil.GetFileType( A111ContratoArquivosAnexos_Arquivo);
               A112ContratoArquivosAnexos_NomeArq = FileUtil.GetFileName( A111ContratoArquivosAnexos_Arquivo);
               pr_default.execute(1, new Object[] {A111ContratoArquivosAnexos_Arquivo, A113ContratoArquivosAnexos_Data, A109ContratoArquivosAnexos_TipoArq, A112ContratoArquivosAnexos_NomeArq, A108ContratoArquivosAnexos_Codigo});
               pr_default.close(1);
               dsDefault.SmartCacheProvider.SetUpdated("ContratoArquivosAnexos") ;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
         }
         else if ( StringUtil.StrCmp(AV9Tabela, "Evidencia") == 0 )
         {
            /* Optimized UPDATE. */
            /* Using cursor P002C4 */
            pr_default.execute(2, new Object[] {AV8Codigo});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("FuncaoAPFEvidencia") ;
            dsDefault.SmartCacheProvider.SetUpdated("FuncaoAPFEvidencia") ;
            /* End optimized UPDATE. */
         }
         else if ( StringUtil.StrCmp(AV9Tabela, "EvidenciaDmn") == 0 )
         {
            /* Optimized DELETE. */
            /* Using cursor P002C5 */
            pr_default.execute(3, new Object[] {AV8Codigo});
            pr_default.close(3);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoEvidencia") ;
            /* End optimized DELETE. */
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_ClearArquivoAnexo");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P002C2_A108ContratoArquivosAnexos_Codigo = new int[1] ;
         P002C2_A112ContratoArquivosAnexos_NomeArq = new String[] {""} ;
         P002C2_A109ContratoArquivosAnexos_TipoArq = new String[] {""} ;
         P002C2_A113ContratoArquivosAnexos_Data = new DateTime[] {DateTime.MinValue} ;
         P002C2_A111ContratoArquivosAnexos_Arquivo = new String[] {""} ;
         A112ContratoArquivosAnexos_NomeArq = "";
         A109ContratoArquivosAnexos_TipoArq = "";
         A113ContratoArquivosAnexos_Data = (DateTime)(DateTime.MinValue);
         A111ContratoArquivosAnexos_Arquivo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_cleararquivoanexo__default(),
            new Object[][] {
                new Object[] {
               P002C2_A108ContratoArquivosAnexos_Codigo, P002C2_A112ContratoArquivosAnexos_NomeArq, P002C2_A109ContratoArquivosAnexos_TipoArq, P002C2_A113ContratoArquivosAnexos_Data, P002C2_A111ContratoArquivosAnexos_Arquivo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV8Codigo ;
      private int A108ContratoArquivosAnexos_Codigo ;
      private String AV9Tabela ;
      private String scmdbuf ;
      private String A112ContratoArquivosAnexos_NomeArq ;
      private String A109ContratoArquivosAnexos_TipoArq ;
      private DateTime A113ContratoArquivosAnexos_Data ;
      private String A111ContratoArquivosAnexos_Arquivo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P002C2_A108ContratoArquivosAnexos_Codigo ;
      private String[] P002C2_A112ContratoArquivosAnexos_NomeArq ;
      private String[] P002C2_A109ContratoArquivosAnexos_TipoArq ;
      private DateTime[] P002C2_A113ContratoArquivosAnexos_Data ;
      private String[] P002C2_A111ContratoArquivosAnexos_Arquivo ;
   }

   public class prc_cleararquivoanexo__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
         ,new UpdateCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP002C2 ;
          prmP002C2 = new Object[] {
          new Object[] {"@AV8Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP002C3 ;
          prmP002C3 = new Object[] {
          new Object[] {"@ContratoArquivosAnexos_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@ContratoArquivosAnexos_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContratoArquivosAnexos_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@ContratoArquivosAnexos_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@ContratoArquivosAnexos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP002C4 ;
          prmP002C4 = new Object[] {
          new Object[] {"@AV8Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP002C5 ;
          prmP002C5 = new Object[] {
          new Object[] {"@AV8Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P002C2", "SELECT [ContratoArquivosAnexos_Codigo], [ContratoArquivosAnexos_NomeArq], [ContratoArquivosAnexos_TipoArq], [ContratoArquivosAnexos_Data], [ContratoArquivosAnexos_Arquivo] FROM [ContratoArquivosAnexos] WITH (UPDLOCK) WHERE [ContratoArquivosAnexos_Codigo] = @AV8Codigo ORDER BY [ContratoArquivosAnexos_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP002C2,1,0,true,true )
             ,new CursorDef("P002C3", "UPDATE [ContratoArquivosAnexos] SET [ContratoArquivosAnexos_Arquivo]=@ContratoArquivosAnexos_Arquivo, [ContratoArquivosAnexos_Data]=@ContratoArquivosAnexos_Data, [ContratoArquivosAnexos_TipoArq]=@ContratoArquivosAnexos_TipoArq, [ContratoArquivosAnexos_NomeArq]=@ContratoArquivosAnexos_NomeArq  WHERE [ContratoArquivosAnexos_Codigo] = @ContratoArquivosAnexos_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP002C3)
             ,new CursorDef("P002C4", "UPDATE [FuncaoAPFEvidencia] SET [FuncaoAPFEvidencia_Arquivo]=CONVERT(varbinary(1), ''), [FuncaoAPFEvidencia_NomeArq]='', [FuncaoAPFEvidencia_TipoArq]='', [FuncaoAPFEvidencia_Data]=convert( DATETIME, '17530101', 112 )  WHERE [FuncaoAPFEvidencia_Codigo] = @AV8Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP002C4)
             ,new CursorDef("P002C5", "DELETE FROM [ContagemResultadoEvidencia]  WHERE [ContagemResultadoEvidencia_Codigo] = @AV8Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP002C5)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 10) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDateTime(4) ;
                ((String[]) buf[4])[0] = rslt.getBLOBFile(5, rslt.getString(3, 10), rslt.getString(2, 50)) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameterDatetime(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                stmt.SetParameter(5, (int)parms[4]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
