/*
               File: ContagemItemParecerGeneral
        Description: Contagem Item Parecer General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:19:21.67
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagemitemparecergeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contagemitemparecergeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contagemitemparecergeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemItemParecer_Codigo )
      {
         this.A243ContagemItemParecer_Codigo = aP0_ContagemItemParecer_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A243ContagemItemParecer_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A243ContagemItemParecer_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A243ContagemItemParecer_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A243ContagemItemParecer_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA5X2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV14Pgmname = "ContagemItemParecerGeneral";
               context.Gx_err = 0;
               WS5X2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contagem Item Parecer General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117192176");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contagemitemparecergeneral.aspx") + "?" + UrlEncode("" +A243ContagemItemParecer_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA243ContagemItemParecer_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA243ContagemItemParecer_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEMPARECER_DATA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A245ContagemItemParecer_Data, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEMPARECER_COMENTARIO", GetSecureSignedToken( sPrefix, A244ContagemItemParecer_Comentario));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEMPARECER_USUARIOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A246ContagemItemParecer_UsuarioCod), "ZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = sPrefix + "hsh" + "ContagemItemParecerGeneral";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A246ContagemItemParecer_UsuarioCod), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, sPrefix+"hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contagemitemparecergeneral:[SendSecurityCheck value for]"+"ContagemItemParecer_UsuarioCod:"+context.localUtil.Format( (decimal)(A246ContagemItemParecer_UsuarioCod), "ZZZZZ9"));
      }

      protected void RenderHtmlCloseForm5X2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contagemitemparecergeneral.js", "?20203117192179");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContagemItemParecerGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contagem Item Parecer General" ;
      }

      protected void WB5X0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contagemitemparecergeneral.aspx");
            }
            wb_table1_2_5X2( true) ;
         }
         else
         {
            wb_table1_2_5X2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_5X2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItemParecer_UsuarioCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A246ContagemItemParecer_UsuarioCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A246ContagemItemParecer_UsuarioCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItemParecer_UsuarioCod_Jsonclick, 0, "Attribute", "", "", "", edtContagemItemParecer_UsuarioCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemItemParecerGeneral.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItemParecer_UsuarioPessoaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A247ContagemItemParecer_UsuarioPessoaCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A247ContagemItemParecer_UsuarioPessoaCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItemParecer_UsuarioPessoaCod_Jsonclick, 0, "Attribute", "", "", "", edtContagemItemParecer_UsuarioPessoaCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemItemParecerGeneral.htm");
         }
         wbLoad = true;
      }

      protected void START5X2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contagem Item Parecer General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP5X0( ) ;
            }
         }
      }

      protected void WS5X2( )
      {
         START5X2( ) ;
         EVT5X2( ) ;
      }

      protected void EVT5X2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP5X0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP5X0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E115X2 */
                                    E115X2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP5X0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E125X2 */
                                    E125X2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP5X0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E135X2 */
                                    E135X2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP5X0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E145X2 */
                                    E145X2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP5X0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP5X0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE5X2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm5X2( ) ;
            }
         }
      }

      protected void PA5X2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF5X2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV14Pgmname = "ContagemItemParecerGeneral";
         context.Gx_err = 0;
      }

      protected void RF5X2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H005X2 */
            pr_default.execute(0, new Object[] {A243ContagemItemParecer_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A247ContagemItemParecer_UsuarioPessoaCod = H005X2_A247ContagemItemParecer_UsuarioPessoaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A247ContagemItemParecer_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A247ContagemItemParecer_UsuarioPessoaCod), 6, 0)));
               n247ContagemItemParecer_UsuarioPessoaCod = H005X2_n247ContagemItemParecer_UsuarioPessoaCod[0];
               A246ContagemItemParecer_UsuarioCod = H005X2_A246ContagemItemParecer_UsuarioCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A246ContagemItemParecer_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A246ContagemItemParecer_UsuarioCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEMPARECER_USUARIOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A246ContagemItemParecer_UsuarioCod), "ZZZZZ9")));
               A248ContagemItemParecer_UsuarioPessoaNom = H005X2_A248ContagemItemParecer_UsuarioPessoaNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A248ContagemItemParecer_UsuarioPessoaNom", A248ContagemItemParecer_UsuarioPessoaNom);
               n248ContagemItemParecer_UsuarioPessoaNom = H005X2_n248ContagemItemParecer_UsuarioPessoaNom[0];
               A244ContagemItemParecer_Comentario = H005X2_A244ContagemItemParecer_Comentario[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A244ContagemItemParecer_Comentario", A244ContagemItemParecer_Comentario);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEMPARECER_COMENTARIO", GetSecureSignedToken( sPrefix, A244ContagemItemParecer_Comentario));
               A245ContagemItemParecer_Data = H005X2_A245ContagemItemParecer_Data[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A245ContagemItemParecer_Data", context.localUtil.TToC( A245ContagemItemParecer_Data, 8, 5, 0, 3, "/", ":", " "));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEMPARECER_DATA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A245ContagemItemParecer_Data, "99/99/99 99:99")));
               A224ContagemItem_Lancamento = H005X2_A224ContagemItem_Lancamento[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
               A247ContagemItemParecer_UsuarioPessoaCod = H005X2_A247ContagemItemParecer_UsuarioPessoaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A247ContagemItemParecer_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A247ContagemItemParecer_UsuarioPessoaCod), 6, 0)));
               n247ContagemItemParecer_UsuarioPessoaCod = H005X2_n247ContagemItemParecer_UsuarioPessoaCod[0];
               A248ContagemItemParecer_UsuarioPessoaNom = H005X2_A248ContagemItemParecer_UsuarioPessoaNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A248ContagemItemParecer_UsuarioPessoaNom", A248ContagemItemParecer_UsuarioPessoaNom);
               n248ContagemItemParecer_UsuarioPessoaNom = H005X2_n248ContagemItemParecer_UsuarioPessoaNom[0];
               /* Execute user event: E125X2 */
               E125X2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WB5X0( ) ;
         }
      }

      protected void STRUP5X0( )
      {
         /* Before Start, stand alone formulas. */
         AV14Pgmname = "ContagemItemParecerGeneral";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E115X2 */
         E115X2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A224ContagemItem_Lancamento = (int)(context.localUtil.CToN( cgiGet( edtContagemItem_Lancamento_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
            A245ContagemItemParecer_Data = context.localUtil.CToT( cgiGet( edtContagemItemParecer_Data_Internalname), 0);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A245ContagemItemParecer_Data", context.localUtil.TToC( A245ContagemItemParecer_Data, 8, 5, 0, 3, "/", ":", " "));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEMPARECER_DATA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A245ContagemItemParecer_Data, "99/99/99 99:99")));
            A244ContagemItemParecer_Comentario = cgiGet( edtContagemItemParecer_Comentario_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A244ContagemItemParecer_Comentario", A244ContagemItemParecer_Comentario);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEMPARECER_COMENTARIO", GetSecureSignedToken( sPrefix, A244ContagemItemParecer_Comentario));
            A248ContagemItemParecer_UsuarioPessoaNom = StringUtil.Upper( cgiGet( edtContagemItemParecer_UsuarioPessoaNom_Internalname));
            n248ContagemItemParecer_UsuarioPessoaNom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A248ContagemItemParecer_UsuarioPessoaNom", A248ContagemItemParecer_UsuarioPessoaNom);
            A246ContagemItemParecer_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( edtContagemItemParecer_UsuarioCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A246ContagemItemParecer_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A246ContagemItemParecer_UsuarioCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEMPARECER_USUARIOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A246ContagemItemParecer_UsuarioCod), "ZZZZZ9")));
            A247ContagemItemParecer_UsuarioPessoaCod = (int)(context.localUtil.CToN( cgiGet( edtContagemItemParecer_UsuarioPessoaCod_Internalname), ",", "."));
            n247ContagemItemParecer_UsuarioPessoaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A247ContagemItemParecer_UsuarioPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A247ContagemItemParecer_UsuarioPessoaCod), 6, 0)));
            /* Read saved values. */
            wcpOA243ContagemItemParecer_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA243ContagemItemParecer_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = sPrefix + "hsh" + "ContagemItemParecerGeneral";
            A246ContagemItemParecer_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( edtContagemItemParecer_UsuarioCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A246ContagemItemParecer_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A246ContagemItemParecer_UsuarioCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEMPARECER_USUARIOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A246ContagemItemParecer_UsuarioCod), "ZZZZZ9")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A246ContagemItemParecer_UsuarioCod), "ZZZZZ9");
            hsh = cgiGet( sPrefix+"hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("contagemitemparecergeneral:[SecurityCheckFailed value for]"+"ContagemItemParecer_UsuarioCod:"+context.localUtil.Format( (decimal)(A246ContagemItemParecer_UsuarioCod), "ZZZZZ9"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E115X2 */
         E115X2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E115X2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E125X2( )
      {
         /* Load Routine */
         edtContagemItemParecer_UsuarioPessoaNom_Link = formatLink("viewusuario.aspx") + "?" + UrlEncode("" +A246ContagemItemParecer_UsuarioCod) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemItemParecer_UsuarioPessoaNom_Internalname, "Link", edtContagemItemParecer_UsuarioPessoaNom_Link);
         edtContagemItemParecer_UsuarioCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemItemParecer_UsuarioCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItemParecer_UsuarioCod_Visible), 5, 0)));
         edtContagemItemParecer_UsuarioPessoaCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemItemParecer_UsuarioPessoaCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItemParecer_UsuarioPessoaCod_Visible), 5, 0)));
         if ( ! ( ( AV6WWPContext.gxTpr_Update ) ) )
         {
            bttBtnupdate_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Enabled), 5, 0)));
         }
         if ( ! ( ( AV6WWPContext.gxTpr_Delete ) ) )
         {
            bttBtndelete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Enabled), 5, 0)));
         }
      }

      protected void E135X2( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("contagemitemparecer.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A243ContagemItemParecer_Codigo) + "," + UrlEncode("" +A224ContagemItem_Lancamento);
         context.wjLocDisableFrm = 1;
      }

      protected void E145X2( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("contagemitemparecer.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A243ContagemItemParecer_Codigo) + "," + UrlEncode("" +A224ContagemItem_Lancamento);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV14Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ContagemItemParecer";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "ContagemItemParecer_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7ContagemItemParecer_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_5X2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_5X2( true) ;
         }
         else
         {
            wb_table2_8_5X2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_5X2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_36_5X2( true) ;
         }
         else
         {
            wb_table3_36_5X2( false) ;
         }
         return  ;
      }

      protected void wb_table3_36_5X2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_5X2e( true) ;
         }
         else
         {
            wb_table1_2_5X2e( false) ;
         }
      }

      protected void wb_table3_36_5X2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, 1, bttBtnupdate_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemItemParecerGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, 1, bttBtndelete_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemItemParecerGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_36_5X2e( true) ;
         }
         else
         {
            wb_table3_36_5X2e( false) ;
         }
      }

      protected void wb_table2_8_5X2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitemparecer_codigo_Internalname, "Sequencial", "", "", lblTextblockcontagemitemparecer_codigo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemParecerGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItemParecer_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A243ContagemItemParecer_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A243ContagemItemParecer_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItemParecer_Codigo_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemItemParecerGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_lancamento_Internalname, "Lan�amento", "", "", lblTextblockcontagemitem_lancamento_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemParecerGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItem_Lancamento_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A224ContagemItem_Lancamento), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A224ContagemItem_Lancamento), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_Lancamento_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemItemParecerGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitemparecer_data_Internalname, "Data", "", "", lblTextblockcontagemitemparecer_data_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemParecerGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtContagemItemParecer_Data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContagemItemParecer_Data_Internalname, context.localUtil.TToC( A245ContagemItemParecer_Data, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A245ContagemItemParecer_Data, "99/99/99 99:99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItemParecer_Data_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "DataHora", "right", false, "HLP_ContagemItemParecerGeneral.htm");
            GxWebStd.gx_bitmap( context, edtContagemItemParecer_Data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContagemItemParecerGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitemparecer_comentario_Internalname, "Coment�rio", "", "", lblTextblockcontagemitemparecer_comentario_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemParecerGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContagemItemParecer_Comentario_Internalname, A244ContagemItemParecer_Comentario, "", "", 0, 1, 0, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "5000", -1, "", "", -1, true, "Comentario", "HLP_ContagemItemParecerGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitemparecer_usuariopessoanom_Internalname, "Usu�rio", "", "", lblTextblockcontagemitemparecer_usuariopessoanom_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemParecerGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItemParecer_UsuarioPessoaNom_Internalname, StringUtil.RTrim( A248ContagemItemParecer_UsuarioPessoaNom), StringUtil.RTrim( context.localUtil.Format( A248ContagemItemParecer_UsuarioPessoaNom, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtContagemItemParecer_UsuarioPessoaNom_Link, "", "", "", edtContagemItemParecer_UsuarioPessoaNom_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_ContagemItemParecerGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_5X2e( true) ;
         }
         else
         {
            wb_table2_8_5X2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A243ContagemItemParecer_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A243ContagemItemParecer_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A243ContagemItemParecer_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA5X2( ) ;
         WS5X2( ) ;
         WE5X2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA243ContagemItemParecer_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA5X2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contagemitemparecergeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA5X2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A243ContagemItemParecer_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A243ContagemItemParecer_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A243ContagemItemParecer_Codigo), 6, 0)));
         }
         wcpOA243ContagemItemParecer_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA243ContagemItemParecer_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A243ContagemItemParecer_Codigo != wcpOA243ContagemItemParecer_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA243ContagemItemParecer_Codigo = A243ContagemItemParecer_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA243ContagemItemParecer_Codigo = cgiGet( sPrefix+"A243ContagemItemParecer_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA243ContagemItemParecer_Codigo) > 0 )
         {
            A243ContagemItemParecer_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA243ContagemItemParecer_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A243ContagemItemParecer_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A243ContagemItemParecer_Codigo), 6, 0)));
         }
         else
         {
            A243ContagemItemParecer_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A243ContagemItemParecer_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA5X2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS5X2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS5X2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A243ContagemItemParecer_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A243ContagemItemParecer_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA243ContagemItemParecer_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A243ContagemItemParecer_Codigo_CTRL", StringUtil.RTrim( sCtrlA243ContagemItemParecer_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE5X2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117192223");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("contagemitemparecergeneral.js", "?20203117192223");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontagemitemparecer_codigo_Internalname = sPrefix+"TEXTBLOCKCONTAGEMITEMPARECER_CODIGO";
         edtContagemItemParecer_Codigo_Internalname = sPrefix+"CONTAGEMITEMPARECER_CODIGO";
         lblTextblockcontagemitem_lancamento_Internalname = sPrefix+"TEXTBLOCKCONTAGEMITEM_LANCAMENTO";
         edtContagemItem_Lancamento_Internalname = sPrefix+"CONTAGEMITEM_LANCAMENTO";
         lblTextblockcontagemitemparecer_data_Internalname = sPrefix+"TEXTBLOCKCONTAGEMITEMPARECER_DATA";
         edtContagemItemParecer_Data_Internalname = sPrefix+"CONTAGEMITEMPARECER_DATA";
         lblTextblockcontagemitemparecer_comentario_Internalname = sPrefix+"TEXTBLOCKCONTAGEMITEMPARECER_COMENTARIO";
         edtContagemItemParecer_Comentario_Internalname = sPrefix+"CONTAGEMITEMPARECER_COMENTARIO";
         lblTextblockcontagemitemparecer_usuariopessoanom_Internalname = sPrefix+"TEXTBLOCKCONTAGEMITEMPARECER_USUARIOPESSOANOM";
         edtContagemItemParecer_UsuarioPessoaNom_Internalname = sPrefix+"CONTAGEMITEMPARECER_USUARIOPESSOANOM";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         edtContagemItemParecer_UsuarioCod_Internalname = sPrefix+"CONTAGEMITEMPARECER_USUARIOCOD";
         edtContagemItemParecer_UsuarioPessoaCod_Internalname = sPrefix+"CONTAGEMITEMPARECER_USUARIOPESSOACOD";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtContagemItemParecer_UsuarioPessoaNom_Jsonclick = "";
         edtContagemItemParecer_Data_Jsonclick = "";
         edtContagemItem_Lancamento_Jsonclick = "";
         edtContagemItemParecer_Codigo_Jsonclick = "";
         bttBtndelete_Enabled = 1;
         bttBtnupdate_Enabled = 1;
         edtContagemItemParecer_UsuarioPessoaNom_Link = "";
         edtContagemItemParecer_UsuarioPessoaCod_Jsonclick = "";
         edtContagemItemParecer_UsuarioPessoaCod_Visible = 1;
         edtContagemItemParecer_UsuarioCod_Jsonclick = "";
         edtContagemItemParecer_UsuarioCod_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E135X2',iparms:[{av:'A243ContagemItemParecer_Codigo',fld:'CONTAGEMITEMPARECER_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DODELETE'","{handler:'E145X2',iparms:[{av:'A243ContagemItemParecer_Codigo',fld:'CONTAGEMITEMPARECER_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV14Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A245ContagemItemParecer_Data = (DateTime)(DateTime.MinValue);
         A244ContagemItemParecer_Comentario = "";
         GXKey = "";
         forbiddenHiddens = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H005X2_A243ContagemItemParecer_Codigo = new int[1] ;
         H005X2_A247ContagemItemParecer_UsuarioPessoaCod = new int[1] ;
         H005X2_n247ContagemItemParecer_UsuarioPessoaCod = new bool[] {false} ;
         H005X2_A246ContagemItemParecer_UsuarioCod = new int[1] ;
         H005X2_A248ContagemItemParecer_UsuarioPessoaNom = new String[] {""} ;
         H005X2_n248ContagemItemParecer_UsuarioPessoaNom = new bool[] {false} ;
         H005X2_A244ContagemItemParecer_Comentario = new String[] {""} ;
         H005X2_A245ContagemItemParecer_Data = new DateTime[] {DateTime.MinValue} ;
         H005X2_A224ContagemItem_Lancamento = new int[1] ;
         A248ContagemItemParecer_UsuarioPessoaNom = "";
         hsh = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         lblTextblockcontagemitemparecer_codigo_Jsonclick = "";
         lblTextblockcontagemitem_lancamento_Jsonclick = "";
         lblTextblockcontagemitemparecer_data_Jsonclick = "";
         lblTextblockcontagemitemparecer_comentario_Jsonclick = "";
         lblTextblockcontagemitemparecer_usuariopessoanom_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA243ContagemItemParecer_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemitemparecergeneral__default(),
            new Object[][] {
                new Object[] {
               H005X2_A243ContagemItemParecer_Codigo, H005X2_A247ContagemItemParecer_UsuarioPessoaCod, H005X2_n247ContagemItemParecer_UsuarioPessoaCod, H005X2_A246ContagemItemParecer_UsuarioCod, H005X2_A248ContagemItemParecer_UsuarioPessoaNom, H005X2_n248ContagemItemParecer_UsuarioPessoaNom, H005X2_A244ContagemItemParecer_Comentario, H005X2_A245ContagemItemParecer_Data, H005X2_A224ContagemItem_Lancamento
               }
            }
         );
         AV14Pgmname = "ContagemItemParecerGeneral";
         /* GeneXus formulas. */
         AV14Pgmname = "ContagemItemParecerGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A243ContagemItemParecer_Codigo ;
      private int wcpOA243ContagemItemParecer_Codigo ;
      private int A246ContagemItemParecer_UsuarioCod ;
      private int edtContagemItemParecer_UsuarioCod_Visible ;
      private int A247ContagemItemParecer_UsuarioPessoaCod ;
      private int edtContagemItemParecer_UsuarioPessoaCod_Visible ;
      private int A224ContagemItem_Lancamento ;
      private int bttBtnupdate_Enabled ;
      private int bttBtndelete_Enabled ;
      private int AV7ContagemItemParecer_Codigo ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV14Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String GX_FocusControl ;
      private String edtContagemItemParecer_UsuarioCod_Internalname ;
      private String edtContagemItemParecer_UsuarioCod_Jsonclick ;
      private String edtContagemItemParecer_UsuarioPessoaCod_Internalname ;
      private String edtContagemItemParecer_UsuarioPessoaCod_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String scmdbuf ;
      private String A248ContagemItemParecer_UsuarioPessoaNom ;
      private String edtContagemItem_Lancamento_Internalname ;
      private String edtContagemItemParecer_Data_Internalname ;
      private String edtContagemItemParecer_Comentario_Internalname ;
      private String edtContagemItemParecer_UsuarioPessoaNom_Internalname ;
      private String hsh ;
      private String edtContagemItemParecer_UsuarioPessoaNom_Link ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockcontagemitemparecer_codigo_Internalname ;
      private String lblTextblockcontagemitemparecer_codigo_Jsonclick ;
      private String edtContagemItemParecer_Codigo_Internalname ;
      private String edtContagemItemParecer_Codigo_Jsonclick ;
      private String lblTextblockcontagemitem_lancamento_Internalname ;
      private String lblTextblockcontagemitem_lancamento_Jsonclick ;
      private String edtContagemItem_Lancamento_Jsonclick ;
      private String lblTextblockcontagemitemparecer_data_Internalname ;
      private String lblTextblockcontagemitemparecer_data_Jsonclick ;
      private String edtContagemItemParecer_Data_Jsonclick ;
      private String lblTextblockcontagemitemparecer_comentario_Internalname ;
      private String lblTextblockcontagemitemparecer_comentario_Jsonclick ;
      private String lblTextblockcontagemitemparecer_usuariopessoanom_Internalname ;
      private String lblTextblockcontagemitemparecer_usuariopessoanom_Jsonclick ;
      private String edtContagemItemParecer_UsuarioPessoaNom_Jsonclick ;
      private String sCtrlA243ContagemItemParecer_Codigo ;
      private DateTime A245ContagemItemParecer_Data ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n247ContagemItemParecer_UsuarioPessoaCod ;
      private bool n248ContagemItemParecer_UsuarioPessoaNom ;
      private bool returnInSub ;
      private String A244ContagemItemParecer_Comentario ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H005X2_A243ContagemItemParecer_Codigo ;
      private int[] H005X2_A247ContagemItemParecer_UsuarioPessoaCod ;
      private bool[] H005X2_n247ContagemItemParecer_UsuarioPessoaCod ;
      private int[] H005X2_A246ContagemItemParecer_UsuarioCod ;
      private String[] H005X2_A248ContagemItemParecer_UsuarioPessoaNom ;
      private bool[] H005X2_n248ContagemItemParecer_UsuarioPessoaNom ;
      private String[] H005X2_A244ContagemItemParecer_Comentario ;
      private DateTime[] H005X2_A245ContagemItemParecer_Data ;
      private int[] H005X2_A224ContagemItem_Lancamento ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class contagemitemparecergeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH005X2 ;
          prmH005X2 = new Object[] {
          new Object[] {"@ContagemItemParecer_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H005X2", "SELECT T1.[ContagemItemParecer_Codigo], T2.[Usuario_PessoaCod] AS ContagemItemParecer_UsuarioPessoaCod, T1.[ContagemItemParecer_UsuarioCod] AS ContagemItemParecer_UsuarioCod, T3.[Pessoa_Nome] AS ContagemItemParecer_UsuarioPessoaNom, T1.[ContagemItemParecer_Comentario], T1.[ContagemItemParecer_Data], T1.[ContagemItem_Lancamento] FROM (([ContagemItemParecer] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContagemItemParecer_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) WHERE T1.[ContagemItemParecer_Codigo] = @ContagemItemParecer_Codigo ORDER BY T1.[ContagemItemParecer_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH005X2,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getLongVarchar(5) ;
                ((DateTime[]) buf[7])[0] = rslt.getGXDateTime(6) ;
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
