/*
               File: type_SdtSDTSolicitacao_SDTSolicitacaoItem
        Description: SDTSolicitacao
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:36:58.20
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDTSolicitacao.SDTSolicitacaoItem" )]
   [XmlType(TypeName =  "SDTSolicitacao.SDTSolicitacaoItem" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtSDTSolicitacao_SDTSolicitacaoItem : GxUserType
   {
      public SdtSDTSolicitacao_SDTSolicitacaoItem( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDTSolicitacao_SDTSolicitacaoItem_Solicitacoesitens_descricao = "";
         gxTv_SdtSDTSolicitacao_SDTSolicitacaoItem_Solicitacoesitens_arquivo = "";
         gxTv_SdtSDTSolicitacao_SDTSolicitacaoItem_Funcaousuario_nome = "";
      }

      public SdtSDTSolicitacao_SDTSolicitacaoItem( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDTSolicitacao_SDTSolicitacaoItem deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDTSolicitacao_SDTSolicitacaoItem)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDTSolicitacao_SDTSolicitacaoItem obj ;
         obj = this;
         obj.gxTpr_Solicitacoesitens_descricao = deserialized.gxTpr_Solicitacoesitens_descricao;
         obj.gxTpr_Solicitacoesitens_arquivo = deserialized.gxTpr_Solicitacoesitens_arquivo;
         obj.gxTpr_Funcaousuario_codigo = deserialized.gxTpr_Funcaousuario_codigo;
         obj.gxTpr_Funcaousuario_nome = deserialized.gxTpr_Funcaousuario_nome;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "SolicitacoesItens_Descricao") )
               {
                  gxTv_SdtSDTSolicitacao_SDTSolicitacaoItem_Solicitacoesitens_descricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SolicitacoesItens_Arquivo") )
               {
                  gxTv_SdtSDTSolicitacao_SDTSolicitacaoItem_Solicitacoesitens_arquivo=context.FileFromBase64( oReader.Value) ;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncaoUsuario_Codigo") )
               {
                  gxTv_SdtSDTSolicitacao_SDTSolicitacaoItem_Funcaousuario_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "FuncaoUsuario_Nome") )
               {
                  gxTv_SdtSDTSolicitacao_SDTSolicitacaoItem_Funcaousuario_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDTSolicitacao.SDTSolicitacaoItem";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("SolicitacoesItens_Descricao", StringUtil.RTrim( gxTv_SdtSDTSolicitacao_SDTSolicitacaoItem_Solicitacoesitens_descricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("SolicitacoesItens_Arquivo", context.FileToBase64( gxTv_SdtSDTSolicitacao_SDTSolicitacaoItem_Solicitacoesitens_arquivo));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("FuncaoUsuario_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDTSolicitacao_SDTSolicitacaoItem_Funcaousuario_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("FuncaoUsuario_Nome", StringUtil.RTrim( gxTv_SdtSDTSolicitacao_SDTSolicitacaoItem_Funcaousuario_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("SolicitacoesItens_Descricao", gxTv_SdtSDTSolicitacao_SDTSolicitacaoItem_Solicitacoesitens_descricao, false);
         AddObjectProperty("SolicitacoesItens_Arquivo", gxTv_SdtSDTSolicitacao_SDTSolicitacaoItem_Solicitacoesitens_arquivo, false);
         AddObjectProperty("FuncaoUsuario_Codigo", gxTv_SdtSDTSolicitacao_SDTSolicitacaoItem_Funcaousuario_codigo, false);
         AddObjectProperty("FuncaoUsuario_Nome", gxTv_SdtSDTSolicitacao_SDTSolicitacaoItem_Funcaousuario_nome, false);
         return  ;
      }

      [  SoapElement( ElementName = "SolicitacoesItens_Descricao" )]
      [  XmlElement( ElementName = "SolicitacoesItens_Descricao"   )]
      public String gxTpr_Solicitacoesitens_descricao
      {
         get {
            return gxTv_SdtSDTSolicitacao_SDTSolicitacaoItem_Solicitacoesitens_descricao ;
         }

         set {
            gxTv_SdtSDTSolicitacao_SDTSolicitacaoItem_Solicitacoesitens_descricao = (String)(value);
         }

      }

      [  SoapElement( ElementName = "SolicitacoesItens_Arquivo" )]
      [  XmlElement( ElementName = "SolicitacoesItens_Arquivo"   )]
      [GxUpload()]
      public byte[] gxTpr_Solicitacoesitens_arquivo_Blob
      {
         get {
            IGxContext context = this.context == null ? new GxContext() : this.context;
            return context.FileToByteArray( gxTv_SdtSDTSolicitacao_SDTSolicitacaoItem_Solicitacoesitens_arquivo) ;
         }

         set {
            IGxContext context = this.context == null ? new GxContext() : this.context;
            gxTv_SdtSDTSolicitacao_SDTSolicitacaoItem_Solicitacoesitens_arquivo=context.FileFromByteArray( value) ;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      [GxUpload()]
      public String gxTpr_Solicitacoesitens_arquivo
      {
         get {
            return gxTv_SdtSDTSolicitacao_SDTSolicitacaoItem_Solicitacoesitens_arquivo ;
         }

         set {
            gxTv_SdtSDTSolicitacao_SDTSolicitacaoItem_Solicitacoesitens_arquivo = value;
         }

      }

      [  SoapElement( ElementName = "FuncaoUsuario_Codigo" )]
      [  XmlElement( ElementName = "FuncaoUsuario_Codigo"   )]
      public int gxTpr_Funcaousuario_codigo
      {
         get {
            return gxTv_SdtSDTSolicitacao_SDTSolicitacaoItem_Funcaousuario_codigo ;
         }

         set {
            gxTv_SdtSDTSolicitacao_SDTSolicitacaoItem_Funcaousuario_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "FuncaoUsuario_Nome" )]
      [  XmlElement( ElementName = "FuncaoUsuario_Nome"   )]
      public String gxTpr_Funcaousuario_nome
      {
         get {
            return gxTv_SdtSDTSolicitacao_SDTSolicitacaoItem_Funcaousuario_nome ;
         }

         set {
            gxTv_SdtSDTSolicitacao_SDTSolicitacaoItem_Funcaousuario_nome = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtSDTSolicitacao_SDTSolicitacaoItem_Solicitacoesitens_descricao = "";
         gxTv_SdtSDTSolicitacao_SDTSolicitacaoItem_Solicitacoesitens_arquivo = "";
         gxTv_SdtSDTSolicitacao_SDTSolicitacaoItem_Funcaousuario_nome = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected int gxTv_SdtSDTSolicitacao_SDTSolicitacaoItem_Funcaousuario_codigo ;
      protected String sTagName ;
      protected String gxTv_SdtSDTSolicitacao_SDTSolicitacaoItem_Solicitacoesitens_descricao ;
      protected String gxTv_SdtSDTSolicitacao_SDTSolicitacaoItem_Funcaousuario_nome ;
      protected String gxTv_SdtSDTSolicitacao_SDTSolicitacaoItem_Solicitacoesitens_arquivo ;
   }

   [DataContract(Name = @"SDTSolicitacao.SDTSolicitacaoItem", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtSDTSolicitacao_SDTSolicitacaoItem_RESTInterface : GxGenericCollectionItem<SdtSDTSolicitacao_SDTSolicitacaoItem>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDTSolicitacao_SDTSolicitacaoItem_RESTInterface( ) : base()
      {
      }

      public SdtSDTSolicitacao_SDTSolicitacaoItem_RESTInterface( SdtSDTSolicitacao_SDTSolicitacaoItem psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "SolicitacoesItens_Descricao" , Order = 0 )]
      public String gxTpr_Solicitacoesitens_descricao
      {
         get {
            return sdt.gxTpr_Solicitacoesitens_descricao ;
         }

         set {
            sdt.gxTpr_Solicitacoesitens_descricao = (String)(value);
         }

      }

      [DataMember( Name = "SolicitacoesItens_Arquivo" , Order = 1 )]
      [GxUpload()]
      public String gxTpr_Solicitacoesitens_arquivo
      {
         get {
            return PathUtil.RelativePath( sdt.gxTpr_Solicitacoesitens_arquivo) ;
         }

         set {
            sdt.gxTpr_Solicitacoesitens_arquivo = value;
         }

      }

      [DataMember( Name = "FuncaoUsuario_Codigo" , Order = 2 )]
      public Nullable<int> gxTpr_Funcaousuario_codigo
      {
         get {
            return sdt.gxTpr_Funcaousuario_codigo ;
         }

         set {
            sdt.gxTpr_Funcaousuario_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "FuncaoUsuario_Nome" , Order = 3 )]
      public String gxTpr_Funcaousuario_nome
      {
         get {
            return sdt.gxTpr_Funcaousuario_nome ;
         }

         set {
            sdt.gxTpr_Funcaousuario_nome = (String)(value);
         }

      }

      public SdtSDTSolicitacao_SDTSolicitacaoItem sdt
      {
         get {
            return (SdtSDTSolicitacao_SDTSolicitacaoItem)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDTSolicitacao_SDTSolicitacaoItem() ;
         }
      }

   }

}
