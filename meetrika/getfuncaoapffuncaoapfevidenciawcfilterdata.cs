/*
               File: GetFuncaoAPFFuncaoAPFEvidenciaWCFilterData
        Description: Get Funcao APFFuncao APFEvidencia WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:12:30.67
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getfuncaoapffuncaoapfevidenciawcfilterdata : GXProcedure
   {
      public getfuncaoapffuncaoapfevidenciawcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getfuncaoapffuncaoapfevidenciawcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
         return AV27OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getfuncaoapffuncaoapfevidenciawcfilterdata objgetfuncaoapffuncaoapfevidenciawcfilterdata;
         objgetfuncaoapffuncaoapfevidenciawcfilterdata = new getfuncaoapffuncaoapfevidenciawcfilterdata();
         objgetfuncaoapffuncaoapfevidenciawcfilterdata.AV18DDOName = aP0_DDOName;
         objgetfuncaoapffuncaoapfevidenciawcfilterdata.AV16SearchTxt = aP1_SearchTxt;
         objgetfuncaoapffuncaoapfevidenciawcfilterdata.AV17SearchTxtTo = aP2_SearchTxtTo;
         objgetfuncaoapffuncaoapfevidenciawcfilterdata.AV22OptionsJson = "" ;
         objgetfuncaoapffuncaoapfevidenciawcfilterdata.AV25OptionsDescJson = "" ;
         objgetfuncaoapffuncaoapfevidenciawcfilterdata.AV27OptionIndexesJson = "" ;
         objgetfuncaoapffuncaoapfevidenciawcfilterdata.context.SetSubmitInitialConfig(context);
         objgetfuncaoapffuncaoapfevidenciawcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetfuncaoapffuncaoapfevidenciawcfilterdata);
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getfuncaoapffuncaoapfevidenciawcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV21Options = (IGxCollection)(new GxSimpleCollection());
         AV24OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV26OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_FUNCAOAPFEVIDENCIA_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADFUNCAOAPFEVIDENCIA_DESCRICAOOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_FUNCAOAPFEVIDENCIA_NOMEARQ") == 0 )
         {
            /* Execute user subroutine: 'LOADFUNCAOAPFEVIDENCIA_NOMEARQOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_FUNCAOAPFEVIDENCIA_TIPOARQ") == 0 )
         {
            /* Execute user subroutine: 'LOADFUNCAOAPFEVIDENCIA_TIPOARQOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV22OptionsJson = AV21Options.ToJSonString(false);
         AV25OptionsDescJson = AV24OptionsDesc.ToJSonString(false);
         AV27OptionIndexesJson = AV26OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV29Session.Get("FuncaoAPFFuncaoAPFEvidenciaWCGridState"), "") == 0 )
         {
            AV31GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "FuncaoAPFFuncaoAPFEvidenciaWCGridState"), "");
         }
         else
         {
            AV31GridState.FromXml(AV29Session.Get("FuncaoAPFFuncaoAPFEvidenciaWCGridState"), "");
         }
         AV38GXV1 = 1;
         while ( AV38GXV1 <= AV31GridState.gxTpr_Filtervalues.Count )
         {
            AV32GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV31GridState.gxTpr_Filtervalues.Item(AV38GXV1));
            if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFEVIDENCIA_DESCRICAO") == 0 )
            {
               AV10TFFuncaoAPFEvidencia_Descricao = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFEVIDENCIA_DESCRICAO_SEL") == 0 )
            {
               AV11TFFuncaoAPFEvidencia_Descricao_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFEVIDENCIA_NOMEARQ") == 0 )
            {
               AV12TFFuncaoAPFEvidencia_NomeArq = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFEVIDENCIA_NOMEARQ_SEL") == 0 )
            {
               AV13TFFuncaoAPFEvidencia_NomeArq_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFEVIDENCIA_TIPOARQ") == 0 )
            {
               AV14TFFuncaoAPFEvidencia_TipoArq = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFEVIDENCIA_TIPOARQ_SEL") == 0 )
            {
               AV15TFFuncaoAPFEvidencia_TipoArq_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "PARM_&FUNCAOAPF_CODIGO") == 0 )
            {
               AV34FuncaoAPF_Codigo = (int)(NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "PARM_&FUNCAOAPF_SISTEMACOD") == 0 )
            {
               AV35FuncaoAPF_SistemaCod = (int)(NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Value, "."));
            }
            AV38GXV1 = (int)(AV38GXV1+1);
         }
      }

      protected void S121( )
      {
         /* 'LOADFUNCAOAPFEVIDENCIA_DESCRICAOOPTIONS' Routine */
         AV10TFFuncaoAPFEvidencia_Descricao = AV16SearchTxt;
         AV11TFFuncaoAPFEvidencia_Descricao_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV11TFFuncaoAPFEvidencia_Descricao_Sel ,
                                              AV10TFFuncaoAPFEvidencia_Descricao ,
                                              AV13TFFuncaoAPFEvidencia_NomeArq_Sel ,
                                              AV12TFFuncaoAPFEvidencia_NomeArq ,
                                              AV15TFFuncaoAPFEvidencia_TipoArq_Sel ,
                                              AV14TFFuncaoAPFEvidencia_TipoArq ,
                                              A407FuncaoAPFEvidencia_Descricao ,
                                              A409FuncaoAPFEvidencia_NomeArq ,
                                              A410FuncaoAPFEvidencia_TipoArq ,
                                              A750FuncaoAPFEvidencia_Ativo ,
                                              AV34FuncaoAPF_Codigo ,
                                              A165FuncaoAPF_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV10TFFuncaoAPFEvidencia_Descricao = StringUtil.Concat( StringUtil.RTrim( AV10TFFuncaoAPFEvidencia_Descricao), "%", "");
         lV12TFFuncaoAPFEvidencia_NomeArq = StringUtil.PadR( StringUtil.RTrim( AV12TFFuncaoAPFEvidencia_NomeArq), 50, "%");
         lV14TFFuncaoAPFEvidencia_TipoArq = StringUtil.PadR( StringUtil.RTrim( AV14TFFuncaoAPFEvidencia_TipoArq), 10, "%");
         /* Using cursor P00UA2 */
         pr_default.execute(0, new Object[] {AV34FuncaoAPF_Codigo, lV10TFFuncaoAPFEvidencia_Descricao, AV11TFFuncaoAPFEvidencia_Descricao_Sel, lV12TFFuncaoAPFEvidencia_NomeArq, AV13TFFuncaoAPFEvidencia_NomeArq_Sel, lV14TFFuncaoAPFEvidencia_TipoArq, AV15TFFuncaoAPFEvidencia_TipoArq_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKUA2 = false;
            A165FuncaoAPF_Codigo = P00UA2_A165FuncaoAPF_Codigo[0];
            A750FuncaoAPFEvidencia_Ativo = P00UA2_A750FuncaoAPFEvidencia_Ativo[0];
            n750FuncaoAPFEvidencia_Ativo = P00UA2_n750FuncaoAPFEvidencia_Ativo[0];
            A407FuncaoAPFEvidencia_Descricao = P00UA2_A407FuncaoAPFEvidencia_Descricao[0];
            n407FuncaoAPFEvidencia_Descricao = P00UA2_n407FuncaoAPFEvidencia_Descricao[0];
            A410FuncaoAPFEvidencia_TipoArq = P00UA2_A410FuncaoAPFEvidencia_TipoArq[0];
            n410FuncaoAPFEvidencia_TipoArq = P00UA2_n410FuncaoAPFEvidencia_TipoArq[0];
            A409FuncaoAPFEvidencia_NomeArq = P00UA2_A409FuncaoAPFEvidencia_NomeArq[0];
            n409FuncaoAPFEvidencia_NomeArq = P00UA2_n409FuncaoAPFEvidencia_NomeArq[0];
            A406FuncaoAPFEvidencia_Codigo = P00UA2_A406FuncaoAPFEvidencia_Codigo[0];
            AV28count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00UA2_A165FuncaoAPF_Codigo[0] == A165FuncaoAPF_Codigo ) && ( StringUtil.StrCmp(P00UA2_A407FuncaoAPFEvidencia_Descricao[0], A407FuncaoAPFEvidencia_Descricao) == 0 ) )
            {
               BRKUA2 = false;
               A406FuncaoAPFEvidencia_Codigo = P00UA2_A406FuncaoAPFEvidencia_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKUA2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A407FuncaoAPFEvidencia_Descricao)) )
            {
               AV20Option = A407FuncaoAPFEvidencia_Descricao;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUA2 )
            {
               BRKUA2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADFUNCAOAPFEVIDENCIA_NOMEARQOPTIONS' Routine */
         AV12TFFuncaoAPFEvidencia_NomeArq = AV16SearchTxt;
         AV13TFFuncaoAPFEvidencia_NomeArq_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV11TFFuncaoAPFEvidencia_Descricao_Sel ,
                                              AV10TFFuncaoAPFEvidencia_Descricao ,
                                              AV13TFFuncaoAPFEvidencia_NomeArq_Sel ,
                                              AV12TFFuncaoAPFEvidencia_NomeArq ,
                                              AV15TFFuncaoAPFEvidencia_TipoArq_Sel ,
                                              AV14TFFuncaoAPFEvidencia_TipoArq ,
                                              A407FuncaoAPFEvidencia_Descricao ,
                                              A409FuncaoAPFEvidencia_NomeArq ,
                                              A410FuncaoAPFEvidencia_TipoArq ,
                                              A750FuncaoAPFEvidencia_Ativo ,
                                              AV34FuncaoAPF_Codigo ,
                                              A165FuncaoAPF_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV10TFFuncaoAPFEvidencia_Descricao = StringUtil.Concat( StringUtil.RTrim( AV10TFFuncaoAPFEvidencia_Descricao), "%", "");
         lV12TFFuncaoAPFEvidencia_NomeArq = StringUtil.PadR( StringUtil.RTrim( AV12TFFuncaoAPFEvidencia_NomeArq), 50, "%");
         lV14TFFuncaoAPFEvidencia_TipoArq = StringUtil.PadR( StringUtil.RTrim( AV14TFFuncaoAPFEvidencia_TipoArq), 10, "%");
         /* Using cursor P00UA3 */
         pr_default.execute(1, new Object[] {AV34FuncaoAPF_Codigo, lV10TFFuncaoAPFEvidencia_Descricao, AV11TFFuncaoAPFEvidencia_Descricao_Sel, lV12TFFuncaoAPFEvidencia_NomeArq, AV13TFFuncaoAPFEvidencia_NomeArq_Sel, lV14TFFuncaoAPFEvidencia_TipoArq, AV15TFFuncaoAPFEvidencia_TipoArq_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKUA4 = false;
            A165FuncaoAPF_Codigo = P00UA3_A165FuncaoAPF_Codigo[0];
            A750FuncaoAPFEvidencia_Ativo = P00UA3_A750FuncaoAPFEvidencia_Ativo[0];
            n750FuncaoAPFEvidencia_Ativo = P00UA3_n750FuncaoAPFEvidencia_Ativo[0];
            A409FuncaoAPFEvidencia_NomeArq = P00UA3_A409FuncaoAPFEvidencia_NomeArq[0];
            n409FuncaoAPFEvidencia_NomeArq = P00UA3_n409FuncaoAPFEvidencia_NomeArq[0];
            A410FuncaoAPFEvidencia_TipoArq = P00UA3_A410FuncaoAPFEvidencia_TipoArq[0];
            n410FuncaoAPFEvidencia_TipoArq = P00UA3_n410FuncaoAPFEvidencia_TipoArq[0];
            A407FuncaoAPFEvidencia_Descricao = P00UA3_A407FuncaoAPFEvidencia_Descricao[0];
            n407FuncaoAPFEvidencia_Descricao = P00UA3_n407FuncaoAPFEvidencia_Descricao[0];
            A406FuncaoAPFEvidencia_Codigo = P00UA3_A406FuncaoAPFEvidencia_Codigo[0];
            AV28count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( P00UA3_A165FuncaoAPF_Codigo[0] == A165FuncaoAPF_Codigo ) && ( StringUtil.StrCmp(P00UA3_A409FuncaoAPFEvidencia_NomeArq[0], A409FuncaoAPFEvidencia_NomeArq) == 0 ) )
            {
               BRKUA4 = false;
               A406FuncaoAPFEvidencia_Codigo = P00UA3_A406FuncaoAPFEvidencia_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKUA4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A409FuncaoAPFEvidencia_NomeArq)) )
            {
               AV20Option = A409FuncaoAPFEvidencia_NomeArq;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUA4 )
            {
               BRKUA4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADFUNCAOAPFEVIDENCIA_TIPOARQOPTIONS' Routine */
         AV14TFFuncaoAPFEvidencia_TipoArq = AV16SearchTxt;
         AV15TFFuncaoAPFEvidencia_TipoArq_Sel = "";
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV11TFFuncaoAPFEvidencia_Descricao_Sel ,
                                              AV10TFFuncaoAPFEvidencia_Descricao ,
                                              AV13TFFuncaoAPFEvidencia_NomeArq_Sel ,
                                              AV12TFFuncaoAPFEvidencia_NomeArq ,
                                              AV15TFFuncaoAPFEvidencia_TipoArq_Sel ,
                                              AV14TFFuncaoAPFEvidencia_TipoArq ,
                                              A407FuncaoAPFEvidencia_Descricao ,
                                              A409FuncaoAPFEvidencia_NomeArq ,
                                              A410FuncaoAPFEvidencia_TipoArq ,
                                              A750FuncaoAPFEvidencia_Ativo ,
                                              AV34FuncaoAPF_Codigo ,
                                              A165FuncaoAPF_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV10TFFuncaoAPFEvidencia_Descricao = StringUtil.Concat( StringUtil.RTrim( AV10TFFuncaoAPFEvidencia_Descricao), "%", "");
         lV12TFFuncaoAPFEvidencia_NomeArq = StringUtil.PadR( StringUtil.RTrim( AV12TFFuncaoAPFEvidencia_NomeArq), 50, "%");
         lV14TFFuncaoAPFEvidencia_TipoArq = StringUtil.PadR( StringUtil.RTrim( AV14TFFuncaoAPFEvidencia_TipoArq), 10, "%");
         /* Using cursor P00UA4 */
         pr_default.execute(2, new Object[] {AV34FuncaoAPF_Codigo, lV10TFFuncaoAPFEvidencia_Descricao, AV11TFFuncaoAPFEvidencia_Descricao_Sel, lV12TFFuncaoAPFEvidencia_NomeArq, AV13TFFuncaoAPFEvidencia_NomeArq_Sel, lV14TFFuncaoAPFEvidencia_TipoArq, AV15TFFuncaoAPFEvidencia_TipoArq_Sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKUA6 = false;
            A165FuncaoAPF_Codigo = P00UA4_A165FuncaoAPF_Codigo[0];
            A750FuncaoAPFEvidencia_Ativo = P00UA4_A750FuncaoAPFEvidencia_Ativo[0];
            n750FuncaoAPFEvidencia_Ativo = P00UA4_n750FuncaoAPFEvidencia_Ativo[0];
            A410FuncaoAPFEvidencia_TipoArq = P00UA4_A410FuncaoAPFEvidencia_TipoArq[0];
            n410FuncaoAPFEvidencia_TipoArq = P00UA4_n410FuncaoAPFEvidencia_TipoArq[0];
            A409FuncaoAPFEvidencia_NomeArq = P00UA4_A409FuncaoAPFEvidencia_NomeArq[0];
            n409FuncaoAPFEvidencia_NomeArq = P00UA4_n409FuncaoAPFEvidencia_NomeArq[0];
            A407FuncaoAPFEvidencia_Descricao = P00UA4_A407FuncaoAPFEvidencia_Descricao[0];
            n407FuncaoAPFEvidencia_Descricao = P00UA4_n407FuncaoAPFEvidencia_Descricao[0];
            A406FuncaoAPFEvidencia_Codigo = P00UA4_A406FuncaoAPFEvidencia_Codigo[0];
            AV28count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( P00UA4_A165FuncaoAPF_Codigo[0] == A165FuncaoAPF_Codigo ) && ( StringUtil.StrCmp(P00UA4_A410FuncaoAPFEvidencia_TipoArq[0], A410FuncaoAPFEvidencia_TipoArq) == 0 ) )
            {
               BRKUA6 = false;
               A406FuncaoAPFEvidencia_Codigo = P00UA4_A406FuncaoAPFEvidencia_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKUA6 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A410FuncaoAPFEvidencia_TipoArq)) )
            {
               AV20Option = A410FuncaoAPFEvidencia_TipoArq;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUA6 )
            {
               BRKUA6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV21Options = new GxSimpleCollection();
         AV24OptionsDesc = new GxSimpleCollection();
         AV26OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV29Session = context.GetSession();
         AV31GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV32GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFFuncaoAPFEvidencia_Descricao = "";
         AV11TFFuncaoAPFEvidencia_Descricao_Sel = "";
         AV12TFFuncaoAPFEvidencia_NomeArq = "";
         AV13TFFuncaoAPFEvidencia_NomeArq_Sel = "";
         AV14TFFuncaoAPFEvidencia_TipoArq = "";
         AV15TFFuncaoAPFEvidencia_TipoArq_Sel = "";
         scmdbuf = "";
         lV10TFFuncaoAPFEvidencia_Descricao = "";
         lV12TFFuncaoAPFEvidencia_NomeArq = "";
         lV14TFFuncaoAPFEvidencia_TipoArq = "";
         A407FuncaoAPFEvidencia_Descricao = "";
         A409FuncaoAPFEvidencia_NomeArq = "";
         A410FuncaoAPFEvidencia_TipoArq = "";
         P00UA2_A165FuncaoAPF_Codigo = new int[1] ;
         P00UA2_A750FuncaoAPFEvidencia_Ativo = new bool[] {false} ;
         P00UA2_n750FuncaoAPFEvidencia_Ativo = new bool[] {false} ;
         P00UA2_A407FuncaoAPFEvidencia_Descricao = new String[] {""} ;
         P00UA2_n407FuncaoAPFEvidencia_Descricao = new bool[] {false} ;
         P00UA2_A410FuncaoAPFEvidencia_TipoArq = new String[] {""} ;
         P00UA2_n410FuncaoAPFEvidencia_TipoArq = new bool[] {false} ;
         P00UA2_A409FuncaoAPFEvidencia_NomeArq = new String[] {""} ;
         P00UA2_n409FuncaoAPFEvidencia_NomeArq = new bool[] {false} ;
         P00UA2_A406FuncaoAPFEvidencia_Codigo = new int[1] ;
         AV20Option = "";
         P00UA3_A165FuncaoAPF_Codigo = new int[1] ;
         P00UA3_A750FuncaoAPFEvidencia_Ativo = new bool[] {false} ;
         P00UA3_n750FuncaoAPFEvidencia_Ativo = new bool[] {false} ;
         P00UA3_A409FuncaoAPFEvidencia_NomeArq = new String[] {""} ;
         P00UA3_n409FuncaoAPFEvidencia_NomeArq = new bool[] {false} ;
         P00UA3_A410FuncaoAPFEvidencia_TipoArq = new String[] {""} ;
         P00UA3_n410FuncaoAPFEvidencia_TipoArq = new bool[] {false} ;
         P00UA3_A407FuncaoAPFEvidencia_Descricao = new String[] {""} ;
         P00UA3_n407FuncaoAPFEvidencia_Descricao = new bool[] {false} ;
         P00UA3_A406FuncaoAPFEvidencia_Codigo = new int[1] ;
         P00UA4_A165FuncaoAPF_Codigo = new int[1] ;
         P00UA4_A750FuncaoAPFEvidencia_Ativo = new bool[] {false} ;
         P00UA4_n750FuncaoAPFEvidencia_Ativo = new bool[] {false} ;
         P00UA4_A410FuncaoAPFEvidencia_TipoArq = new String[] {""} ;
         P00UA4_n410FuncaoAPFEvidencia_TipoArq = new bool[] {false} ;
         P00UA4_A409FuncaoAPFEvidencia_NomeArq = new String[] {""} ;
         P00UA4_n409FuncaoAPFEvidencia_NomeArq = new bool[] {false} ;
         P00UA4_A407FuncaoAPFEvidencia_Descricao = new String[] {""} ;
         P00UA4_n407FuncaoAPFEvidencia_Descricao = new bool[] {false} ;
         P00UA4_A406FuncaoAPFEvidencia_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getfuncaoapffuncaoapfevidenciawcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00UA2_A165FuncaoAPF_Codigo, P00UA2_A750FuncaoAPFEvidencia_Ativo, P00UA2_n750FuncaoAPFEvidencia_Ativo, P00UA2_A407FuncaoAPFEvidencia_Descricao, P00UA2_n407FuncaoAPFEvidencia_Descricao, P00UA2_A410FuncaoAPFEvidencia_TipoArq, P00UA2_n410FuncaoAPFEvidencia_TipoArq, P00UA2_A409FuncaoAPFEvidencia_NomeArq, P00UA2_n409FuncaoAPFEvidencia_NomeArq, P00UA2_A406FuncaoAPFEvidencia_Codigo
               }
               , new Object[] {
               P00UA3_A165FuncaoAPF_Codigo, P00UA3_A750FuncaoAPFEvidencia_Ativo, P00UA3_n750FuncaoAPFEvidencia_Ativo, P00UA3_A409FuncaoAPFEvidencia_NomeArq, P00UA3_n409FuncaoAPFEvidencia_NomeArq, P00UA3_A410FuncaoAPFEvidencia_TipoArq, P00UA3_n410FuncaoAPFEvidencia_TipoArq, P00UA3_A407FuncaoAPFEvidencia_Descricao, P00UA3_n407FuncaoAPFEvidencia_Descricao, P00UA3_A406FuncaoAPFEvidencia_Codigo
               }
               , new Object[] {
               P00UA4_A165FuncaoAPF_Codigo, P00UA4_A750FuncaoAPFEvidencia_Ativo, P00UA4_n750FuncaoAPFEvidencia_Ativo, P00UA4_A410FuncaoAPFEvidencia_TipoArq, P00UA4_n410FuncaoAPFEvidencia_TipoArq, P00UA4_A409FuncaoAPFEvidencia_NomeArq, P00UA4_n409FuncaoAPFEvidencia_NomeArq, P00UA4_A407FuncaoAPFEvidencia_Descricao, P00UA4_n407FuncaoAPFEvidencia_Descricao, P00UA4_A406FuncaoAPFEvidencia_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV38GXV1 ;
      private int AV34FuncaoAPF_Codigo ;
      private int AV35FuncaoAPF_SistemaCod ;
      private int A165FuncaoAPF_Codigo ;
      private int A406FuncaoAPFEvidencia_Codigo ;
      private long AV28count ;
      private String AV12TFFuncaoAPFEvidencia_NomeArq ;
      private String AV13TFFuncaoAPFEvidencia_NomeArq_Sel ;
      private String AV14TFFuncaoAPFEvidencia_TipoArq ;
      private String AV15TFFuncaoAPFEvidencia_TipoArq_Sel ;
      private String scmdbuf ;
      private String lV12TFFuncaoAPFEvidencia_NomeArq ;
      private String lV14TFFuncaoAPFEvidencia_TipoArq ;
      private String A409FuncaoAPFEvidencia_NomeArq ;
      private String A410FuncaoAPFEvidencia_TipoArq ;
      private bool returnInSub ;
      private bool A750FuncaoAPFEvidencia_Ativo ;
      private bool BRKUA2 ;
      private bool n750FuncaoAPFEvidencia_Ativo ;
      private bool n407FuncaoAPFEvidencia_Descricao ;
      private bool n410FuncaoAPFEvidencia_TipoArq ;
      private bool n409FuncaoAPFEvidencia_NomeArq ;
      private bool BRKUA4 ;
      private bool BRKUA6 ;
      private String AV27OptionIndexesJson ;
      private String AV22OptionsJson ;
      private String AV25OptionsDescJson ;
      private String A407FuncaoAPFEvidencia_Descricao ;
      private String AV18DDOName ;
      private String AV16SearchTxt ;
      private String AV17SearchTxtTo ;
      private String AV10TFFuncaoAPFEvidencia_Descricao ;
      private String AV11TFFuncaoAPFEvidencia_Descricao_Sel ;
      private String lV10TFFuncaoAPFEvidencia_Descricao ;
      private String AV20Option ;
      private IGxSession AV29Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00UA2_A165FuncaoAPF_Codigo ;
      private bool[] P00UA2_A750FuncaoAPFEvidencia_Ativo ;
      private bool[] P00UA2_n750FuncaoAPFEvidencia_Ativo ;
      private String[] P00UA2_A407FuncaoAPFEvidencia_Descricao ;
      private bool[] P00UA2_n407FuncaoAPFEvidencia_Descricao ;
      private String[] P00UA2_A410FuncaoAPFEvidencia_TipoArq ;
      private bool[] P00UA2_n410FuncaoAPFEvidencia_TipoArq ;
      private String[] P00UA2_A409FuncaoAPFEvidencia_NomeArq ;
      private bool[] P00UA2_n409FuncaoAPFEvidencia_NomeArq ;
      private int[] P00UA2_A406FuncaoAPFEvidencia_Codigo ;
      private int[] P00UA3_A165FuncaoAPF_Codigo ;
      private bool[] P00UA3_A750FuncaoAPFEvidencia_Ativo ;
      private bool[] P00UA3_n750FuncaoAPFEvidencia_Ativo ;
      private String[] P00UA3_A409FuncaoAPFEvidencia_NomeArq ;
      private bool[] P00UA3_n409FuncaoAPFEvidencia_NomeArq ;
      private String[] P00UA3_A410FuncaoAPFEvidencia_TipoArq ;
      private bool[] P00UA3_n410FuncaoAPFEvidencia_TipoArq ;
      private String[] P00UA3_A407FuncaoAPFEvidencia_Descricao ;
      private bool[] P00UA3_n407FuncaoAPFEvidencia_Descricao ;
      private int[] P00UA3_A406FuncaoAPFEvidencia_Codigo ;
      private int[] P00UA4_A165FuncaoAPF_Codigo ;
      private bool[] P00UA4_A750FuncaoAPFEvidencia_Ativo ;
      private bool[] P00UA4_n750FuncaoAPFEvidencia_Ativo ;
      private String[] P00UA4_A410FuncaoAPFEvidencia_TipoArq ;
      private bool[] P00UA4_n410FuncaoAPFEvidencia_TipoArq ;
      private String[] P00UA4_A409FuncaoAPFEvidencia_NomeArq ;
      private bool[] P00UA4_n409FuncaoAPFEvidencia_NomeArq ;
      private String[] P00UA4_A407FuncaoAPFEvidencia_Descricao ;
      private bool[] P00UA4_n407FuncaoAPFEvidencia_Descricao ;
      private int[] P00UA4_A406FuncaoAPFEvidencia_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV31GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV32GridStateFilterValue ;
   }

   public class getfuncaoapffuncaoapfevidenciawcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00UA2( IGxContext context ,
                                             String AV11TFFuncaoAPFEvidencia_Descricao_Sel ,
                                             String AV10TFFuncaoAPFEvidencia_Descricao ,
                                             String AV13TFFuncaoAPFEvidencia_NomeArq_Sel ,
                                             String AV12TFFuncaoAPFEvidencia_NomeArq ,
                                             String AV15TFFuncaoAPFEvidencia_TipoArq_Sel ,
                                             String AV14TFFuncaoAPFEvidencia_TipoArq ,
                                             String A407FuncaoAPFEvidencia_Descricao ,
                                             String A409FuncaoAPFEvidencia_NomeArq ,
                                             String A410FuncaoAPFEvidencia_TipoArq ,
                                             bool A750FuncaoAPFEvidencia_Ativo ,
                                             int AV34FuncaoAPF_Codigo ,
                                             int A165FuncaoAPF_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [7] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [FuncaoAPF_Codigo], [FuncaoAPFEvidencia_Ativo], [FuncaoAPFEvidencia_Descricao], [FuncaoAPFEvidencia_TipoArq], [FuncaoAPFEvidencia_NomeArq], [FuncaoAPFEvidencia_Codigo] FROM [FuncaoAPFEvidencia] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([FuncaoAPF_Codigo] = @AV34FuncaoAPF_Codigo)";
         scmdbuf = scmdbuf + " and ([FuncaoAPFEvidencia_Ativo] = 1)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFFuncaoAPFEvidencia_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFFuncaoAPFEvidencia_Descricao)) ) )
         {
            sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_Descricao] like @lV10TFFuncaoAPFEvidencia_Descricao)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFFuncaoAPFEvidencia_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_Descricao] = @AV11TFFuncaoAPFEvidencia_Descricao_Sel)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFFuncaoAPFEvidencia_NomeArq_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFFuncaoAPFEvidencia_NomeArq)) ) )
         {
            sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_NomeArq] like @lV12TFFuncaoAPFEvidencia_NomeArq)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFFuncaoAPFEvidencia_NomeArq_Sel)) )
         {
            sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_NomeArq] = @AV13TFFuncaoAPFEvidencia_NomeArq_Sel)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFFuncaoAPFEvidencia_TipoArq_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFFuncaoAPFEvidencia_TipoArq)) ) )
         {
            sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_TipoArq] like @lV14TFFuncaoAPFEvidencia_TipoArq)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFFuncaoAPFEvidencia_TipoArq_Sel)) )
         {
            sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_TipoArq] = @AV15TFFuncaoAPFEvidencia_TipoArq_Sel)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [FuncaoAPF_Codigo], [FuncaoAPFEvidencia_Descricao]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00UA3( IGxContext context ,
                                             String AV11TFFuncaoAPFEvidencia_Descricao_Sel ,
                                             String AV10TFFuncaoAPFEvidencia_Descricao ,
                                             String AV13TFFuncaoAPFEvidencia_NomeArq_Sel ,
                                             String AV12TFFuncaoAPFEvidencia_NomeArq ,
                                             String AV15TFFuncaoAPFEvidencia_TipoArq_Sel ,
                                             String AV14TFFuncaoAPFEvidencia_TipoArq ,
                                             String A407FuncaoAPFEvidencia_Descricao ,
                                             String A409FuncaoAPFEvidencia_NomeArq ,
                                             String A410FuncaoAPFEvidencia_TipoArq ,
                                             bool A750FuncaoAPFEvidencia_Ativo ,
                                             int AV34FuncaoAPF_Codigo ,
                                             int A165FuncaoAPF_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [7] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT [FuncaoAPF_Codigo], [FuncaoAPFEvidencia_Ativo], [FuncaoAPFEvidencia_NomeArq], [FuncaoAPFEvidencia_TipoArq], [FuncaoAPFEvidencia_Descricao], [FuncaoAPFEvidencia_Codigo] FROM [FuncaoAPFEvidencia] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([FuncaoAPF_Codigo] = @AV34FuncaoAPF_Codigo)";
         scmdbuf = scmdbuf + " and ([FuncaoAPFEvidencia_Ativo] = 1)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFFuncaoAPFEvidencia_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFFuncaoAPFEvidencia_Descricao)) ) )
         {
            sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_Descricao] like @lV10TFFuncaoAPFEvidencia_Descricao)";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFFuncaoAPFEvidencia_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_Descricao] = @AV11TFFuncaoAPFEvidencia_Descricao_Sel)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFFuncaoAPFEvidencia_NomeArq_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFFuncaoAPFEvidencia_NomeArq)) ) )
         {
            sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_NomeArq] like @lV12TFFuncaoAPFEvidencia_NomeArq)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFFuncaoAPFEvidencia_NomeArq_Sel)) )
         {
            sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_NomeArq] = @AV13TFFuncaoAPFEvidencia_NomeArq_Sel)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFFuncaoAPFEvidencia_TipoArq_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFFuncaoAPFEvidencia_TipoArq)) ) )
         {
            sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_TipoArq] like @lV14TFFuncaoAPFEvidencia_TipoArq)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFFuncaoAPFEvidencia_TipoArq_Sel)) )
         {
            sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_TipoArq] = @AV15TFFuncaoAPFEvidencia_TipoArq_Sel)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [FuncaoAPF_Codigo], [FuncaoAPFEvidencia_NomeArq]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00UA4( IGxContext context ,
                                             String AV11TFFuncaoAPFEvidencia_Descricao_Sel ,
                                             String AV10TFFuncaoAPFEvidencia_Descricao ,
                                             String AV13TFFuncaoAPFEvidencia_NomeArq_Sel ,
                                             String AV12TFFuncaoAPFEvidencia_NomeArq ,
                                             String AV15TFFuncaoAPFEvidencia_TipoArq_Sel ,
                                             String AV14TFFuncaoAPFEvidencia_TipoArq ,
                                             String A407FuncaoAPFEvidencia_Descricao ,
                                             String A409FuncaoAPFEvidencia_NomeArq ,
                                             String A410FuncaoAPFEvidencia_TipoArq ,
                                             bool A750FuncaoAPFEvidencia_Ativo ,
                                             int AV34FuncaoAPF_Codigo ,
                                             int A165FuncaoAPF_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [7] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT [FuncaoAPF_Codigo], [FuncaoAPFEvidencia_Ativo], [FuncaoAPFEvidencia_TipoArq], [FuncaoAPFEvidencia_NomeArq], [FuncaoAPFEvidencia_Descricao], [FuncaoAPFEvidencia_Codigo] FROM [FuncaoAPFEvidencia] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([FuncaoAPF_Codigo] = @AV34FuncaoAPF_Codigo)";
         scmdbuf = scmdbuf + " and ([FuncaoAPFEvidencia_Ativo] = 1)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFFuncaoAPFEvidencia_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFFuncaoAPFEvidencia_Descricao)) ) )
         {
            sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_Descricao] like @lV10TFFuncaoAPFEvidencia_Descricao)";
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFFuncaoAPFEvidencia_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_Descricao] = @AV11TFFuncaoAPFEvidencia_Descricao_Sel)";
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFFuncaoAPFEvidencia_NomeArq_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFFuncaoAPFEvidencia_NomeArq)) ) )
         {
            sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_NomeArq] like @lV12TFFuncaoAPFEvidencia_NomeArq)";
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFFuncaoAPFEvidencia_NomeArq_Sel)) )
         {
            sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_NomeArq] = @AV13TFFuncaoAPFEvidencia_NomeArq_Sel)";
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFFuncaoAPFEvidencia_TipoArq_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFFuncaoAPFEvidencia_TipoArq)) ) )
         {
            sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_TipoArq] like @lV14TFFuncaoAPFEvidencia_TipoArq)";
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFFuncaoAPFEvidencia_TipoArq_Sel)) )
         {
            sWhereString = sWhereString + " and ([FuncaoAPFEvidencia_TipoArq] = @AV15TFFuncaoAPFEvidencia_TipoArq_Sel)";
         }
         else
         {
            GXv_int5[6] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [FuncaoAPF_Codigo], [FuncaoAPFEvidencia_TipoArq]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00UA2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] );
               case 1 :
                     return conditional_P00UA3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] );
               case 2 :
                     return conditional_P00UA4(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00UA2 ;
          prmP00UA2 = new Object[] {
          new Object[] {"@AV34FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV10TFFuncaoAPFEvidencia_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV11TFFuncaoAPFEvidencia_Descricao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV12TFFuncaoAPFEvidencia_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@AV13TFFuncaoAPFEvidencia_NomeArq_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV14TFFuncaoAPFEvidencia_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@AV15TFFuncaoAPFEvidencia_TipoArq_Sel",SqlDbType.Char,10,0}
          } ;
          Object[] prmP00UA3 ;
          prmP00UA3 = new Object[] {
          new Object[] {"@AV34FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV10TFFuncaoAPFEvidencia_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV11TFFuncaoAPFEvidencia_Descricao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV12TFFuncaoAPFEvidencia_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@AV13TFFuncaoAPFEvidencia_NomeArq_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV14TFFuncaoAPFEvidencia_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@AV15TFFuncaoAPFEvidencia_TipoArq_Sel",SqlDbType.Char,10,0}
          } ;
          Object[] prmP00UA4 ;
          prmP00UA4 = new Object[] {
          new Object[] {"@AV34FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV10TFFuncaoAPFEvidencia_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV11TFFuncaoAPFEvidencia_Descricao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV12TFFuncaoAPFEvidencia_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@AV13TFFuncaoAPFEvidencia_NomeArq_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV14TFFuncaoAPFEvidencia_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@AV15TFFuncaoAPFEvidencia_TipoArq_Sel",SqlDbType.Char,10,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00UA2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00UA2,100,0,true,false )
             ,new CursorDef("P00UA3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00UA3,100,0,true,false )
             ,new CursorDef("P00UA4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00UA4,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 10) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 10) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getfuncaoapffuncaoapfevidenciawcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getfuncaoapffuncaoapfevidenciawcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getfuncaoapffuncaoapfevidenciawcfilterdata") )
          {
             return  ;
          }
          getfuncaoapffuncaoapfevidenciawcfilterdata worker = new getfuncaoapffuncaoapfevidenciawcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
