/*
               File: WWAutorizacaoConsumo
        Description:  Autoriza��o de Consumo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:23:40.63
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwautorizacaoconsumo : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwautorizacaoconsumo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwautorizacaoconsumo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_94 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_94_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_94_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16AutorizacaoConsumo_VigenciaInicio1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16AutorizacaoConsumo_VigenciaInicio1", context.localUtil.Format(AV16AutorizacaoConsumo_VigenciaInicio1, "99/99/99"));
               AV17AutorizacaoConsumo_VigenciaInicio_To1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17AutorizacaoConsumo_VigenciaInicio_To1", context.localUtil.Format(AV17AutorizacaoConsumo_VigenciaInicio_To1, "99/99/99"));
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV20AutorizacaoConsumo_VigenciaInicio2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20AutorizacaoConsumo_VigenciaInicio2", context.localUtil.Format(AV20AutorizacaoConsumo_VigenciaInicio2, "99/99/99"));
               AV21AutorizacaoConsumo_VigenciaInicio_To2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21AutorizacaoConsumo_VigenciaInicio_To2", context.localUtil.Format(AV21AutorizacaoConsumo_VigenciaInicio_To2, "99/99/99"));
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV24AutorizacaoConsumo_VigenciaInicio3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24AutorizacaoConsumo_VigenciaInicio3", context.localUtil.Format(AV24AutorizacaoConsumo_VigenciaInicio3, "99/99/99"));
               AV25AutorizacaoConsumo_VigenciaInicio_To3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25AutorizacaoConsumo_VigenciaInicio_To3", context.localUtil.Format(AV25AutorizacaoConsumo_VigenciaInicio_To3, "99/99/99"));
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV34TFAutorizacaoConsumo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFAutorizacaoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34TFAutorizacaoConsumo_Codigo), 6, 0)));
               AV35TFAutorizacaoConsumo_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFAutorizacaoConsumo_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFAutorizacaoConsumo_Codigo_To), 6, 0)));
               AV38TFContrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFContrato_Codigo), 6, 0)));
               AV39TFContrato_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFContrato_Codigo_To), 6, 0)));
               AV42TFAutorizacaoConsumo_VigenciaInicio = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFAutorizacaoConsumo_VigenciaInicio", context.localUtil.Format(AV42TFAutorizacaoConsumo_VigenciaInicio, "99/99/99"));
               AV43TFAutorizacaoConsumo_VigenciaInicio_To = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFAutorizacaoConsumo_VigenciaInicio_To", context.localUtil.Format(AV43TFAutorizacaoConsumo_VigenciaInicio_To, "99/99/99"));
               AV48TFAutorizacaoConsumo_VigenciaFim = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFAutorizacaoConsumo_VigenciaFim", context.localUtil.Format(AV48TFAutorizacaoConsumo_VigenciaFim, "99/99/99"));
               AV49TFAutorizacaoConsumo_VigenciaFim_To = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFAutorizacaoConsumo_VigenciaFim_To", context.localUtil.Format(AV49TFAutorizacaoConsumo_VigenciaFim_To, "99/99/99"));
               AV54TFAutorizacaoConsumo_UnidadeMedicaoNom = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFAutorizacaoConsumo_UnidadeMedicaoNom", AV54TFAutorizacaoConsumo_UnidadeMedicaoNom);
               AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel", AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel);
               AV58TFAutorizacaoConsumo_Quantidade = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFAutorizacaoConsumo_Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58TFAutorizacaoConsumo_Quantidade), 3, 0)));
               AV59TFAutorizacaoConsumo_Quantidade_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFAutorizacaoConsumo_Quantidade_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59TFAutorizacaoConsumo_Quantidade_To), 3, 0)));
               AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace", AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace);
               AV40ddo_Contrato_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ddo_Contrato_CodigoTitleControlIdToReplace", AV40ddo_Contrato_CodigoTitleControlIdToReplace);
               AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace", AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace);
               AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace", AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace);
               AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace", AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace);
               AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace", AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace);
               AV90Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16AutorizacaoConsumo_VigenciaInicio1, AV17AutorizacaoConsumo_VigenciaInicio_To1, AV19DynamicFiltersSelector2, AV20AutorizacaoConsumo_VigenciaInicio2, AV21AutorizacaoConsumo_VigenciaInicio_To2, AV23DynamicFiltersSelector3, AV24AutorizacaoConsumo_VigenciaInicio3, AV25AutorizacaoConsumo_VigenciaInicio_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFAutorizacaoConsumo_Codigo, AV35TFAutorizacaoConsumo_Codigo_To, AV38TFContrato_Codigo, AV39TFContrato_Codigo_To, AV42TFAutorizacaoConsumo_VigenciaInicio, AV43TFAutorizacaoConsumo_VigenciaInicio_To, AV48TFAutorizacaoConsumo_VigenciaFim, AV49TFAutorizacaoConsumo_VigenciaFim_To, AV54TFAutorizacaoConsumo_UnidadeMedicaoNom, AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel, AV58TFAutorizacaoConsumo_Quantidade, AV59TFAutorizacaoConsumo_Quantidade_To, AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace, AV40ddo_Contrato_CodigoTitleControlIdToReplace, AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace, AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace, AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace, AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace, AV90Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PANA2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTNA2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202031221234116");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwautorizacaoconsumo.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vAUTORIZACAOCONSUMO_VIGENCIAINICIO1", context.localUtil.Format(AV16AutorizacaoConsumo_VigenciaInicio1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO1", context.localUtil.Format(AV17AutorizacaoConsumo_VigenciaInicio_To1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vAUTORIZACAOCONSUMO_VIGENCIAINICIO2", context.localUtil.Format(AV20AutorizacaoConsumo_VigenciaInicio2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO2", context.localUtil.Format(AV21AutorizacaoConsumo_VigenciaInicio_To2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vAUTORIZACAOCONSUMO_VIGENCIAINICIO3", context.localUtil.Format(AV24AutorizacaoConsumo_VigenciaInicio3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO3", context.localUtil.Format(AV25AutorizacaoConsumo_VigenciaInicio_To3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFAUTORIZACAOCONSUMO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV34TFAutorizacaoConsumo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFAUTORIZACAOCONSUMO_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35TFAutorizacaoConsumo_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38TFContrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATO_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV39TFContrato_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO", context.localUtil.Format(AV42TFAutorizacaoConsumo_VigenciaInicio, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO", context.localUtil.Format(AV43TFAutorizacaoConsumo_VigenciaInicio_To, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFAUTORIZACAOCONSUMO_VIGENCIAFIM", context.localUtil.Format(AV48TFAutorizacaoConsumo_VigenciaFim, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO", context.localUtil.Format(AV49TFAutorizacaoConsumo_VigenciaFim_To, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM", StringUtil.RTrim( AV54TFAutorizacaoConsumo_UnidadeMedicaoNom));
         GxWebStd.gx_hidden_field( context, "GXH_vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL", StringUtil.RTrim( AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFAUTORIZACAOCONSUMO_QUANTIDADE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV58TFAutorizacaoConsumo_Quantidade), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV59TFAutorizacaoConsumo_Quantidade_To), 3, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_94", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_94), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV63GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV64GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV61DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV61DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAUTORIZACAOCONSUMO_CODIGOTITLEFILTERDATA", AV33AutorizacaoConsumo_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAUTORIZACAOCONSUMO_CODIGOTITLEFILTERDATA", AV33AutorizacaoConsumo_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATO_CODIGOTITLEFILTERDATA", AV37Contrato_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATO_CODIGOTITLEFILTERDATA", AV37Contrato_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAUTORIZACAOCONSUMO_VIGENCIAINICIOTITLEFILTERDATA", AV41AutorizacaoConsumo_VigenciaInicioTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAUTORIZACAOCONSUMO_VIGENCIAINICIOTITLEFILTERDATA", AV41AutorizacaoConsumo_VigenciaInicioTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAUTORIZACAOCONSUMO_VIGENCIAFIMTITLEFILTERDATA", AV47AutorizacaoConsumo_VigenciaFimTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAUTORIZACAOCONSUMO_VIGENCIAFIMTITLEFILTERDATA", AV47AutorizacaoConsumo_VigenciaFimTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLEFILTERDATA", AV53AutorizacaoConsumo_UnidadeMedicaoNomTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLEFILTERDATA", AV53AutorizacaoConsumo_UnidadeMedicaoNomTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAUTORIZACAOCONSUMO_QUANTIDADETITLEFILTERDATA", AV57AutorizacaoConsumo_QuantidadeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAUTORIZACAOCONSUMO_QUANTIDADETITLEFILTERDATA", AV57AutorizacaoConsumo_QuantidadeTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV90Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_CODIGO_Caption", StringUtil.RTrim( Ddo_autorizacaoconsumo_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_CODIGO_Tooltip", StringUtil.RTrim( Ddo_autorizacaoconsumo_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_CODIGO_Cls", StringUtil.RTrim( Ddo_autorizacaoconsumo_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_autorizacaoconsumo_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_autorizacaoconsumo_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_autorizacaoconsumo_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_autorizacaoconsumo_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_autorizacaoconsumo_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_CODIGO_Filtertype", StringUtil.RTrim( Ddo_autorizacaoconsumo_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_CODIGO_Sortasc", StringUtil.RTrim( Ddo_autorizacaoconsumo_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_autorizacaoconsumo_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_autorizacaoconsumo_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_autorizacaoconsumo_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_autorizacaoconsumo_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_autorizacaoconsumo_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Caption", StringUtil.RTrim( Ddo_contrato_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Tooltip", StringUtil.RTrim( Ddo_contrato_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Cls", StringUtil.RTrim( Ddo_contrato_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_contrato_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_contrato_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contrato_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contrato_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_contrato_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_contrato_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_contrato_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_contrato_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Filtertype", StringUtil.RTrim( Ddo_contrato_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_contrato_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_contrato_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Sortasc", StringUtil.RTrim( Ddo_contrato_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_contrato_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_contrato_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_contrato_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_contrato_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_contrato_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Caption", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciainicio_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Tooltip", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciainicio_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Cls", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciainicio_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Filteredtext_set", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciainicio_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Filteredtextto_set", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciainicio_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Dropdownoptionstype", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciainicio_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciainicio_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Includesortasc", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_vigenciainicio_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Includesortdsc", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_vigenciainicio_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Sortedstatus", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciainicio_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Includefilter", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_vigenciainicio_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Filtertype", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciainicio_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Filterisrange", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_vigenciainicio_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Includedatalist", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_vigenciainicio_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Sortasc", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciainicio_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Sortdsc", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciainicio_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Cleanfilter", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciainicio_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Rangefilterfrom", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciainicio_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Rangefilterto", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciainicio_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Searchbuttontext", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciainicio_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Caption", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciafim_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Tooltip", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciafim_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Cls", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciafim_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Filteredtext_set", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciafim_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Filteredtextto_set", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciafim_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Dropdownoptionstype", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciafim_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciafim_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Includesortasc", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_vigenciafim_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Includesortdsc", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_vigenciafim_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Sortedstatus", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciafim_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Includefilter", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_vigenciafim_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Filtertype", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciafim_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Filterisrange", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_vigenciafim_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Includedatalist", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_vigenciafim_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Sortasc", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciafim_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Sortdsc", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciafim_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Cleanfilter", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciafim_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Rangefilterfrom", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciafim_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Rangefilterto", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciafim_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Searchbuttontext", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciafim_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Caption", StringUtil.RTrim( Ddo_autorizacaoconsumo_unidademedicaonom_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Tooltip", StringUtil.RTrim( Ddo_autorizacaoconsumo_unidademedicaonom_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Cls", StringUtil.RTrim( Ddo_autorizacaoconsumo_unidademedicaonom_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Filteredtext_set", StringUtil.RTrim( Ddo_autorizacaoconsumo_unidademedicaonom_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Selectedvalue_set", StringUtil.RTrim( Ddo_autorizacaoconsumo_unidademedicaonom_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Dropdownoptionstype", StringUtil.RTrim( Ddo_autorizacaoconsumo_unidademedicaonom_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_autorizacaoconsumo_unidademedicaonom_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Includesortasc", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_unidademedicaonom_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Includesortdsc", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_unidademedicaonom_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Sortedstatus", StringUtil.RTrim( Ddo_autorizacaoconsumo_unidademedicaonom_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Includefilter", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_unidademedicaonom_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Filtertype", StringUtil.RTrim( Ddo_autorizacaoconsumo_unidademedicaonom_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Filterisrange", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_unidademedicaonom_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Includedatalist", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_unidademedicaonom_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Datalisttype", StringUtil.RTrim( Ddo_autorizacaoconsumo_unidademedicaonom_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Datalistproc", StringUtil.RTrim( Ddo_autorizacaoconsumo_unidademedicaonom_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_autorizacaoconsumo_unidademedicaonom_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Sortasc", StringUtil.RTrim( Ddo_autorizacaoconsumo_unidademedicaonom_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Sortdsc", StringUtil.RTrim( Ddo_autorizacaoconsumo_unidademedicaonom_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Loadingdata", StringUtil.RTrim( Ddo_autorizacaoconsumo_unidademedicaonom_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Cleanfilter", StringUtil.RTrim( Ddo_autorizacaoconsumo_unidademedicaonom_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Noresultsfound", StringUtil.RTrim( Ddo_autorizacaoconsumo_unidademedicaonom_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Searchbuttontext", StringUtil.RTrim( Ddo_autorizacaoconsumo_unidademedicaonom_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Caption", StringUtil.RTrim( Ddo_autorizacaoconsumo_quantidade_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Tooltip", StringUtil.RTrim( Ddo_autorizacaoconsumo_quantidade_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Cls", StringUtil.RTrim( Ddo_autorizacaoconsumo_quantidade_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Filteredtext_set", StringUtil.RTrim( Ddo_autorizacaoconsumo_quantidade_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Filteredtextto_set", StringUtil.RTrim( Ddo_autorizacaoconsumo_quantidade_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Dropdownoptionstype", StringUtil.RTrim( Ddo_autorizacaoconsumo_quantidade_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_autorizacaoconsumo_quantidade_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Includesortasc", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_quantidade_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Includesortdsc", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_quantidade_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Sortedstatus", StringUtil.RTrim( Ddo_autorizacaoconsumo_quantidade_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Includefilter", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_quantidade_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Filtertype", StringUtil.RTrim( Ddo_autorizacaoconsumo_quantidade_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Filterisrange", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_quantidade_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Includedatalist", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_quantidade_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Sortasc", StringUtil.RTrim( Ddo_autorizacaoconsumo_quantidade_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Sortdsc", StringUtil.RTrim( Ddo_autorizacaoconsumo_quantidade_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Cleanfilter", StringUtil.RTrim( Ddo_autorizacaoconsumo_quantidade_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Rangefilterfrom", StringUtil.RTrim( Ddo_autorizacaoconsumo_quantidade_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Rangefilterto", StringUtil.RTrim( Ddo_autorizacaoconsumo_quantidade_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Searchbuttontext", StringUtil.RTrim( Ddo_autorizacaoconsumo_quantidade_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_autorizacaoconsumo_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_autorizacaoconsumo_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_autorizacaoconsumo_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_contrato_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_contrato_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_contrato_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Activeeventkey", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciainicio_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Filteredtext_get", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciainicio_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Filteredtextto_get", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciainicio_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Activeeventkey", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciafim_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Filteredtext_get", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciafim_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Filteredtextto_get", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciafim_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Activeeventkey", StringUtil.RTrim( Ddo_autorizacaoconsumo_unidademedicaonom_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Filteredtext_get", StringUtil.RTrim( Ddo_autorizacaoconsumo_unidademedicaonom_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Selectedvalue_get", StringUtil.RTrim( Ddo_autorizacaoconsumo_unidademedicaonom_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Activeeventkey", StringUtil.RTrim( Ddo_autorizacaoconsumo_quantidade_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Filteredtext_get", StringUtil.RTrim( Ddo_autorizacaoconsumo_quantidade_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Filteredtextto_get", StringUtil.RTrim( Ddo_autorizacaoconsumo_quantidade_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WENA2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTNA2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwautorizacaoconsumo.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWAutorizacaoConsumo" ;
      }

      public override String GetPgmdesc( )
      {
         return " Autoriza��o de Consumo" ;
      }

      protected void WBNA0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_NA2( true) ;
         }
         else
         {
            wb_table1_2_NA2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_NA2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(108, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,108);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(109, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,109);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfautorizacaoconsumo_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV34TFAutorizacaoConsumo_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV34TFAutorizacaoConsumo_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,110);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfautorizacaoconsumo_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfautorizacaoconsumo_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAutorizacaoConsumo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfautorizacaoconsumo_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35TFAutorizacaoConsumo_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV35TFAutorizacaoConsumo_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,111);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfautorizacaoconsumo_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfautorizacaoconsumo_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAutorizacaoConsumo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontrato_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38TFContrato_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV38TFContrato_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,112);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontrato_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontrato_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAutorizacaoConsumo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontrato_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV39TFContrato_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV39TFContrato_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,113);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontrato_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontrato_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAutorizacaoConsumo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'" + sGXsfl_94_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfautorizacaoconsumo_vigenciainicio_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfautorizacaoconsumo_vigenciainicio_Internalname, context.localUtil.Format(AV42TFAutorizacaoConsumo_VigenciaInicio, "99/99/99"), context.localUtil.Format( AV42TFAutorizacaoConsumo_VigenciaInicio, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,114);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfautorizacaoconsumo_vigenciainicio_Jsonclick, 0, "Attribute", "", "", "", edtavTfautorizacaoconsumo_vigenciainicio_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAutorizacaoConsumo.htm");
            GxWebStd.gx_bitmap( context, edtavTfautorizacaoconsumo_vigenciainicio_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfautorizacaoconsumo_vigenciainicio_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWAutorizacaoConsumo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'" + sGXsfl_94_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfautorizacaoconsumo_vigenciainicio_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfautorizacaoconsumo_vigenciainicio_to_Internalname, context.localUtil.Format(AV43TFAutorizacaoConsumo_VigenciaInicio_To, "99/99/99"), context.localUtil.Format( AV43TFAutorizacaoConsumo_VigenciaInicio_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,115);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfautorizacaoconsumo_vigenciainicio_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfautorizacaoconsumo_vigenciainicio_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAutorizacaoConsumo.htm");
            GxWebStd.gx_bitmap( context, edtavTfautorizacaoconsumo_vigenciainicio_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfautorizacaoconsumo_vigenciainicio_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWAutorizacaoConsumo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_autorizacaoconsumo_vigenciainicioauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'',false,'" + sGXsfl_94_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_autorizacaoconsumo_vigenciainicioauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_autorizacaoconsumo_vigenciainicioauxdate_Internalname, context.localUtil.Format(AV44DDO_AutorizacaoConsumo_VigenciaInicioAuxDate, "99/99/99"), context.localUtil.Format( AV44DDO_AutorizacaoConsumo_VigenciaInicioAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,117);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_autorizacaoconsumo_vigenciainicioauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAutorizacaoConsumo.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_autorizacaoconsumo_vigenciainicioauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWAutorizacaoConsumo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'',false,'" + sGXsfl_94_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_autorizacaoconsumo_vigenciainicioauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_autorizacaoconsumo_vigenciainicioauxdateto_Internalname, context.localUtil.Format(AV45DDO_AutorizacaoConsumo_VigenciaInicioAuxDateTo, "99/99/99"), context.localUtil.Format( AV45DDO_AutorizacaoConsumo_VigenciaInicioAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,118);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_autorizacaoconsumo_vigenciainicioauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAutorizacaoConsumo.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_autorizacaoconsumo_vigenciainicioauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWAutorizacaoConsumo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'',false,'" + sGXsfl_94_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfautorizacaoconsumo_vigenciafim_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfautorizacaoconsumo_vigenciafim_Internalname, context.localUtil.Format(AV48TFAutorizacaoConsumo_VigenciaFim, "99/99/99"), context.localUtil.Format( AV48TFAutorizacaoConsumo_VigenciaFim, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,119);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfautorizacaoconsumo_vigenciafim_Jsonclick, 0, "Attribute", "", "", "", edtavTfautorizacaoconsumo_vigenciafim_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAutorizacaoConsumo.htm");
            GxWebStd.gx_bitmap( context, edtavTfautorizacaoconsumo_vigenciafim_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfautorizacaoconsumo_vigenciafim_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWAutorizacaoConsumo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 120,'',false,'" + sGXsfl_94_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfautorizacaoconsumo_vigenciafim_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfautorizacaoconsumo_vigenciafim_to_Internalname, context.localUtil.Format(AV49TFAutorizacaoConsumo_VigenciaFim_To, "99/99/99"), context.localUtil.Format( AV49TFAutorizacaoConsumo_VigenciaFim_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,120);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfautorizacaoconsumo_vigenciafim_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfautorizacaoconsumo_vigenciafim_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAutorizacaoConsumo.htm");
            GxWebStd.gx_bitmap( context, edtavTfautorizacaoconsumo_vigenciafim_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfautorizacaoconsumo_vigenciafim_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWAutorizacaoConsumo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_autorizacaoconsumo_vigenciafimauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 122,'',false,'" + sGXsfl_94_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_autorizacaoconsumo_vigenciafimauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_autorizacaoconsumo_vigenciafimauxdate_Internalname, context.localUtil.Format(AV50DDO_AutorizacaoConsumo_VigenciaFimAuxDate, "99/99/99"), context.localUtil.Format( AV50DDO_AutorizacaoConsumo_VigenciaFimAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,122);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_autorizacaoconsumo_vigenciafimauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAutorizacaoConsumo.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_autorizacaoconsumo_vigenciafimauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWAutorizacaoConsumo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 123,'',false,'" + sGXsfl_94_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_autorizacaoconsumo_vigenciafimauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_autorizacaoconsumo_vigenciafimauxdateto_Internalname, context.localUtil.Format(AV51DDO_AutorizacaoConsumo_VigenciaFimAuxDateTo, "99/99/99"), context.localUtil.Format( AV51DDO_AutorizacaoConsumo_VigenciaFimAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,123);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_autorizacaoconsumo_vigenciafimauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAutorizacaoConsumo.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_autorizacaoconsumo_vigenciafimauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWAutorizacaoConsumo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 124,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfautorizacaoconsumo_unidademedicaonom_Internalname, StringUtil.RTrim( AV54TFAutorizacaoConsumo_UnidadeMedicaoNom), StringUtil.RTrim( context.localUtil.Format( AV54TFAutorizacaoConsumo_UnidadeMedicaoNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,124);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfautorizacaoconsumo_unidademedicaonom_Jsonclick, 0, "Attribute", "", "", "", edtavTfautorizacaoconsumo_unidademedicaonom_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAutorizacaoConsumo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 125,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfautorizacaoconsumo_unidademedicaonom_sel_Internalname, StringUtil.RTrim( AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel), StringUtil.RTrim( context.localUtil.Format( AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,125);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfautorizacaoconsumo_unidademedicaonom_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfautorizacaoconsumo_unidademedicaonom_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAutorizacaoConsumo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 126,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfautorizacaoconsumo_quantidade_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV58TFAutorizacaoConsumo_Quantidade), 3, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV58TFAutorizacaoConsumo_Quantidade), "ZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,126);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfautorizacaoconsumo_quantidade_Jsonclick, 0, "Attribute", "", "", "", edtavTfautorizacaoconsumo_quantidade_Visible, 1, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAutorizacaoConsumo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 127,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfautorizacaoconsumo_quantidade_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV59TFAutorizacaoConsumo_Quantidade_To), 3, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV59TFAutorizacaoConsumo_Quantidade_To), "ZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,127);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfautorizacaoconsumo_quantidade_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfautorizacaoconsumo_quantidade_to_Visible, 1, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAutorizacaoConsumo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_AUTORIZACAOCONSUMO_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 129,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_autorizacaoconsumo_codigotitlecontrolidtoreplace_Internalname, AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,129);\"", 0, edtavDdo_autorizacaoconsumo_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWAutorizacaoConsumo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATO_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 131,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contrato_codigotitlecontrolidtoreplace_Internalname, AV40ddo_Contrato_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,131);\"", 0, edtavDdo_contrato_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWAutorizacaoConsumo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 133,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_autorizacaoconsumo_vigenciainiciotitlecontrolidtoreplace_Internalname, AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,133);\"", 0, edtavDdo_autorizacaoconsumo_vigenciainiciotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWAutorizacaoConsumo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 135,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_autorizacaoconsumo_vigenciafimtitlecontrolidtoreplace_Internalname, AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,135);\"", 0, edtavDdo_autorizacaoconsumo_vigenciafimtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWAutorizacaoConsumo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 137,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_autorizacaoconsumo_unidademedicaonomtitlecontrolidtoreplace_Internalname, AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,137);\"", 0, edtavDdo_autorizacaoconsumo_unidademedicaonomtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWAutorizacaoConsumo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 139,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_autorizacaoconsumo_quantidadetitlecontrolidtoreplace_Internalname, AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,139);\"", 0, edtavDdo_autorizacaoconsumo_quantidadetitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWAutorizacaoConsumo.htm");
         }
         wbLoad = true;
      }

      protected void STARTNA2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Autoriza��o de Consumo", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPNA0( ) ;
      }

      protected void WSNA2( )
      {
         STARTNA2( ) ;
         EVTNA2( ) ;
      }

      protected void EVTNA2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11NA2 */
                              E11NA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_AUTORIZACAOCONSUMO_CODIGO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12NA2 */
                              E12NA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATO_CODIGO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13NA2 */
                              E13NA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14NA2 */
                              E14NA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15NA2 */
                              E15NA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16NA2 */
                              E16NA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_AUTORIZACAOCONSUMO_QUANTIDADE.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17NA2 */
                              E17NA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18NA2 */
                              E18NA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19NA2 */
                              E19NA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20NA2 */
                              E20NA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21NA2 */
                              E21NA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22NA2 */
                              E22NA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E23NA2 */
                              E23NA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E24NA2 */
                              E24NA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E25NA2 */
                              E25NA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E26NA2 */
                              E26NA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E27NA2 */
                              E27NA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E28NA2 */
                              E28NA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_94_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_94_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_94_idx), 4, 0)), 4, "0");
                              SubsflControlProps_942( ) ;
                              A1774AutorizacaoConsumo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtAutorizacaoConsumo_Codigo_Internalname), ",", "."));
                              A74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContrato_Codigo_Internalname), ",", "."));
                              A83Contrato_DataVigenciaTermino = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContrato_DataVigenciaTermino_Internalname), 0));
                              A82Contrato_DataVigenciaInicio = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContrato_DataVigenciaInicio_Internalname), 0));
                              A1775AutorizacaoConsumo_VigenciaInicio = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtAutorizacaoConsumo_VigenciaInicio_Internalname), 0));
                              A1776AutorizacaoConsumo_VigenciaFim = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtAutorizacaoConsumo_VigenciaFim_Internalname), 0));
                              A1777AutorizacaoConsumo_UnidadeMedicaoCod = (int)(context.localUtil.CToN( cgiGet( edtAutorizacaoConsumo_UnidadeMedicaoCod_Internalname), ",", "."));
                              A1778AutorizacaoConsumo_UnidadeMedicaoNom = StringUtil.Upper( cgiGet( edtAutorizacaoConsumo_UnidadeMedicaoNom_Internalname));
                              n1778AutorizacaoConsumo_UnidadeMedicaoNom = false;
                              A1779AutorizacaoConsumo_Quantidade = (short)(context.localUtil.CToN( cgiGet( edtAutorizacaoConsumo_Quantidade_Internalname), ",", "."));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E29NA2 */
                                    E29NA2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E30NA2 */
                                    E30NA2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E31NA2 */
                                    E31NA2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Autorizacaoconsumo_vigenciainicio1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vAUTORIZACAOCONSUMO_VIGENCIAINICIO1"), 0) != AV16AutorizacaoConsumo_VigenciaInicio1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Autorizacaoconsumo_vigenciainicio_to1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO1"), 0) != AV17AutorizacaoConsumo_VigenciaInicio_To1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Autorizacaoconsumo_vigenciainicio2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vAUTORIZACAOCONSUMO_VIGENCIAINICIO2"), 0) != AV20AutorizacaoConsumo_VigenciaInicio2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Autorizacaoconsumo_vigenciainicio_to2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO2"), 0) != AV21AutorizacaoConsumo_VigenciaInicio_To2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Autorizacaoconsumo_vigenciainicio3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vAUTORIZACAOCONSUMO_VIGENCIAINICIO3"), 0) != AV24AutorizacaoConsumo_VigenciaInicio3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Autorizacaoconsumo_vigenciainicio_to3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO3"), 0) != AV25AutorizacaoConsumo_VigenciaInicio_To3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfautorizacaoconsumo_codigo Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFAUTORIZACAOCONSUMO_CODIGO"), ",", ".") != Convert.ToDecimal( AV34TFAutorizacaoConsumo_Codigo )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfautorizacaoconsumo_codigo_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFAUTORIZACAOCONSUMO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV35TFAutorizacaoConsumo_Codigo_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontrato_codigo Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATO_CODIGO"), ",", ".") != Convert.ToDecimal( AV38TFContrato_Codigo )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontrato_codigo_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV39TFContrato_Codigo_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfautorizacaoconsumo_vigenciainicio Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO"), 0) != AV42TFAutorizacaoConsumo_VigenciaInicio )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfautorizacaoconsumo_vigenciainicio_to Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO"), 0) != AV43TFAutorizacaoConsumo_VigenciaInicio_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfautorizacaoconsumo_vigenciafim Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFAUTORIZACAOCONSUMO_VIGENCIAFIM"), 0) != AV48TFAutorizacaoConsumo_VigenciaFim )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfautorizacaoconsumo_vigenciafim_to Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO"), 0) != AV49TFAutorizacaoConsumo_VigenciaFim_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfautorizacaoconsumo_unidademedicaonom Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM"), AV54TFAutorizacaoConsumo_UnidadeMedicaoNom) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfautorizacaoconsumo_unidademedicaonom_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL"), AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfautorizacaoconsumo_quantidade Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFAUTORIZACAOCONSUMO_QUANTIDADE"), ",", ".") != Convert.ToDecimal( AV58TFAutorizacaoConsumo_Quantidade )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfautorizacaoconsumo_quantidade_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO"), ",", ".") != Convert.ToDecimal( AV59TFAutorizacaoConsumo_Quantidade_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WENA2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PANA2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("AUTORIZACAOCONSUMO_VIGENCIAINICIO", "Consumo_Vigencia Inicio", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("AUTORIZACAOCONSUMO_VIGENCIAINICIO", "Consumo_Vigencia Inicio", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("AUTORIZACAOCONSUMO_VIGENCIAINICIO", "Consumo_Vigencia Inicio", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_942( ) ;
         while ( nGXsfl_94_idx <= nRC_GXsfl_94 )
         {
            sendrow_942( ) ;
            nGXsfl_94_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_94_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_94_idx+1));
            sGXsfl_94_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_94_idx), 4, 0)), 4, "0");
            SubsflControlProps_942( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       DateTime AV16AutorizacaoConsumo_VigenciaInicio1 ,
                                       DateTime AV17AutorizacaoConsumo_VigenciaInicio_To1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       DateTime AV20AutorizacaoConsumo_VigenciaInicio2 ,
                                       DateTime AV21AutorizacaoConsumo_VigenciaInicio_To2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       DateTime AV24AutorizacaoConsumo_VigenciaInicio3 ,
                                       DateTime AV25AutorizacaoConsumo_VigenciaInicio_To3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       int AV34TFAutorizacaoConsumo_Codigo ,
                                       int AV35TFAutorizacaoConsumo_Codigo_To ,
                                       int AV38TFContrato_Codigo ,
                                       int AV39TFContrato_Codigo_To ,
                                       DateTime AV42TFAutorizacaoConsumo_VigenciaInicio ,
                                       DateTime AV43TFAutorizacaoConsumo_VigenciaInicio_To ,
                                       DateTime AV48TFAutorizacaoConsumo_VigenciaFim ,
                                       DateTime AV49TFAutorizacaoConsumo_VigenciaFim_To ,
                                       String AV54TFAutorizacaoConsumo_UnidadeMedicaoNom ,
                                       String AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel ,
                                       short AV58TFAutorizacaoConsumo_Quantidade ,
                                       short AV59TFAutorizacaoConsumo_Quantidade_To ,
                                       String AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace ,
                                       String AV40ddo_Contrato_CodigoTitleControlIdToReplace ,
                                       String AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace ,
                                       String AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace ,
                                       String AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace ,
                                       String AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace ,
                                       String AV90Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFNA2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_AUTORIZACAOCONSUMO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1774AutorizacaoConsumo_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "AUTORIZACAOCONSUMO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1774AutorizacaoConsumo_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_AUTORIZACAOCONSUMO_VIGENCIAINICIO", GetSecureSignedToken( "", A1775AutorizacaoConsumo_VigenciaInicio));
         GxWebStd.gx_hidden_field( context, "AUTORIZACAOCONSUMO_VIGENCIAINICIO", context.localUtil.Format(A1775AutorizacaoConsumo_VigenciaInicio, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "gxhash_AUTORIZACAOCONSUMO_VIGENCIAFIM", GetSecureSignedToken( "", A1776AutorizacaoConsumo_VigenciaFim));
         GxWebStd.gx_hidden_field( context, "AUTORIZACAOCONSUMO_VIGENCIAFIM", context.localUtil.Format(A1776AutorizacaoConsumo_VigenciaFim, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "gxhash_AUTORIZACAOCONSUMO_UNIDADEMEDICAOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1777AutorizacaoConsumo_UnidadeMedicaoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "AUTORIZACAOCONSUMO_UNIDADEMEDICAOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1777AutorizacaoConsumo_UnidadeMedicaoCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_AUTORIZACAOCONSUMO_QUANTIDADE", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1779AutorizacaoConsumo_Quantidade), "ZZ9")));
         GxWebStd.gx_hidden_field( context, "AUTORIZACAOCONSUMO_QUANTIDADE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1779AutorizacaoConsumo_Quantidade), 3, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFNA2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV90Pgmname = "WWAutorizacaoConsumo";
         context.Gx_err = 0;
      }

      protected void RFNA2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 94;
         /* Execute user event: E30NA2 */
         E30NA2 ();
         nGXsfl_94_idx = 1;
         sGXsfl_94_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_94_idx), 4, 0)), 4, "0");
         SubsflControlProps_942( ) ;
         nGXsfl_94_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_942( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV67WWAutorizacaoConsumoDS_1_Dynamicfiltersselector1 ,
                                                 AV68WWAutorizacaoConsumoDS_2_Autorizacaoconsumo_vigenciainicio1 ,
                                                 AV69WWAutorizacaoConsumoDS_3_Autorizacaoconsumo_vigenciainicio_to1 ,
                                                 AV70WWAutorizacaoConsumoDS_4_Dynamicfiltersenabled2 ,
                                                 AV71WWAutorizacaoConsumoDS_5_Dynamicfiltersselector2 ,
                                                 AV72WWAutorizacaoConsumoDS_6_Autorizacaoconsumo_vigenciainicio2 ,
                                                 AV73WWAutorizacaoConsumoDS_7_Autorizacaoconsumo_vigenciainicio_to2 ,
                                                 AV74WWAutorizacaoConsumoDS_8_Dynamicfiltersenabled3 ,
                                                 AV75WWAutorizacaoConsumoDS_9_Dynamicfiltersselector3 ,
                                                 AV76WWAutorizacaoConsumoDS_10_Autorizacaoconsumo_vigenciainicio3 ,
                                                 AV77WWAutorizacaoConsumoDS_11_Autorizacaoconsumo_vigenciainicio_to3 ,
                                                 AV78WWAutorizacaoConsumoDS_12_Tfautorizacaoconsumo_codigo ,
                                                 AV79WWAutorizacaoConsumoDS_13_Tfautorizacaoconsumo_codigo_to ,
                                                 AV80WWAutorizacaoConsumoDS_14_Tfcontrato_codigo ,
                                                 AV81WWAutorizacaoConsumoDS_15_Tfcontrato_codigo_to ,
                                                 AV82WWAutorizacaoConsumoDS_16_Tfautorizacaoconsumo_vigenciainicio ,
                                                 AV83WWAutorizacaoConsumoDS_17_Tfautorizacaoconsumo_vigenciainicio_to ,
                                                 AV84WWAutorizacaoConsumoDS_18_Tfautorizacaoconsumo_vigenciafim ,
                                                 AV85WWAutorizacaoConsumoDS_19_Tfautorizacaoconsumo_vigenciafim_to ,
                                                 AV87WWAutorizacaoConsumoDS_21_Tfautorizacaoconsumo_unidademedicaonom_sel ,
                                                 AV86WWAutorizacaoConsumoDS_20_Tfautorizacaoconsumo_unidademedicaonom ,
                                                 AV88WWAutorizacaoConsumoDS_22_Tfautorizacaoconsumo_quantidade ,
                                                 AV89WWAutorizacaoConsumoDS_23_Tfautorizacaoconsumo_quantidade_to ,
                                                 A1775AutorizacaoConsumo_VigenciaInicio ,
                                                 A1774AutorizacaoConsumo_Codigo ,
                                                 A74Contrato_Codigo ,
                                                 A1776AutorizacaoConsumo_VigenciaFim ,
                                                 A1778AutorizacaoConsumo_UnidadeMedicaoNom ,
                                                 A1779AutorizacaoConsumo_Quantidade ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE,
                                                 TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT,
                                                 TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV86WWAutorizacaoConsumoDS_20_Tfautorizacaoconsumo_unidademedicaonom = StringUtil.PadR( StringUtil.RTrim( AV86WWAutorizacaoConsumoDS_20_Tfautorizacaoconsumo_unidademedicaonom), 50, "%");
            /* Using cursor H00NA2 */
            pr_default.execute(0, new Object[] {AV68WWAutorizacaoConsumoDS_2_Autorizacaoconsumo_vigenciainicio1, AV69WWAutorizacaoConsumoDS_3_Autorizacaoconsumo_vigenciainicio_to1, AV72WWAutorizacaoConsumoDS_6_Autorizacaoconsumo_vigenciainicio2, AV73WWAutorizacaoConsumoDS_7_Autorizacaoconsumo_vigenciainicio_to2, AV76WWAutorizacaoConsumoDS_10_Autorizacaoconsumo_vigenciainicio3, AV77WWAutorizacaoConsumoDS_11_Autorizacaoconsumo_vigenciainicio_to3, AV78WWAutorizacaoConsumoDS_12_Tfautorizacaoconsumo_codigo, AV79WWAutorizacaoConsumoDS_13_Tfautorizacaoconsumo_codigo_to, AV80WWAutorizacaoConsumoDS_14_Tfcontrato_codigo, AV81WWAutorizacaoConsumoDS_15_Tfcontrato_codigo_to, AV82WWAutorizacaoConsumoDS_16_Tfautorizacaoconsumo_vigenciainicio, AV83WWAutorizacaoConsumoDS_17_Tfautorizacaoconsumo_vigenciainicio_to, AV84WWAutorizacaoConsumoDS_18_Tfautorizacaoconsumo_vigenciafim, AV85WWAutorizacaoConsumoDS_19_Tfautorizacaoconsumo_vigenciafim_to, lV86WWAutorizacaoConsumoDS_20_Tfautorizacaoconsumo_unidademedicaonom, AV87WWAutorizacaoConsumoDS_21_Tfautorizacaoconsumo_unidademedicaonom_sel, AV88WWAutorizacaoConsumoDS_22_Tfautorizacaoconsumo_quantidade, AV89WWAutorizacaoConsumoDS_23_Tfautorizacaoconsumo_quantidade_to, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_94_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1779AutorizacaoConsumo_Quantidade = H00NA2_A1779AutorizacaoConsumo_Quantidade[0];
               A1778AutorizacaoConsumo_UnidadeMedicaoNom = H00NA2_A1778AutorizacaoConsumo_UnidadeMedicaoNom[0];
               n1778AutorizacaoConsumo_UnidadeMedicaoNom = H00NA2_n1778AutorizacaoConsumo_UnidadeMedicaoNom[0];
               A1777AutorizacaoConsumo_UnidadeMedicaoCod = H00NA2_A1777AutorizacaoConsumo_UnidadeMedicaoCod[0];
               A1776AutorizacaoConsumo_VigenciaFim = H00NA2_A1776AutorizacaoConsumo_VigenciaFim[0];
               A1775AutorizacaoConsumo_VigenciaInicio = H00NA2_A1775AutorizacaoConsumo_VigenciaInicio[0];
               A82Contrato_DataVigenciaInicio = H00NA2_A82Contrato_DataVigenciaInicio[0];
               A83Contrato_DataVigenciaTermino = H00NA2_A83Contrato_DataVigenciaTermino[0];
               A74Contrato_Codigo = H00NA2_A74Contrato_Codigo[0];
               A1774AutorizacaoConsumo_Codigo = H00NA2_A1774AutorizacaoConsumo_Codigo[0];
               A1778AutorizacaoConsumo_UnidadeMedicaoNom = H00NA2_A1778AutorizacaoConsumo_UnidadeMedicaoNom[0];
               n1778AutorizacaoConsumo_UnidadeMedicaoNom = H00NA2_n1778AutorizacaoConsumo_UnidadeMedicaoNom[0];
               A82Contrato_DataVigenciaInicio = H00NA2_A82Contrato_DataVigenciaInicio[0];
               A83Contrato_DataVigenciaTermino = H00NA2_A83Contrato_DataVigenciaTermino[0];
               /* Execute user event: E31NA2 */
               E31NA2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 94;
            WBNA0( ) ;
         }
         nGXsfl_94_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV67WWAutorizacaoConsumoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV68WWAutorizacaoConsumoDS_2_Autorizacaoconsumo_vigenciainicio1 = AV16AutorizacaoConsumo_VigenciaInicio1;
         AV69WWAutorizacaoConsumoDS_3_Autorizacaoconsumo_vigenciainicio_to1 = AV17AutorizacaoConsumo_VigenciaInicio_To1;
         AV70WWAutorizacaoConsumoDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV71WWAutorizacaoConsumoDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV72WWAutorizacaoConsumoDS_6_Autorizacaoconsumo_vigenciainicio2 = AV20AutorizacaoConsumo_VigenciaInicio2;
         AV73WWAutorizacaoConsumoDS_7_Autorizacaoconsumo_vigenciainicio_to2 = AV21AutorizacaoConsumo_VigenciaInicio_To2;
         AV74WWAutorizacaoConsumoDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV75WWAutorizacaoConsumoDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV76WWAutorizacaoConsumoDS_10_Autorizacaoconsumo_vigenciainicio3 = AV24AutorizacaoConsumo_VigenciaInicio3;
         AV77WWAutorizacaoConsumoDS_11_Autorizacaoconsumo_vigenciainicio_to3 = AV25AutorizacaoConsumo_VigenciaInicio_To3;
         AV78WWAutorizacaoConsumoDS_12_Tfautorizacaoconsumo_codigo = AV34TFAutorizacaoConsumo_Codigo;
         AV79WWAutorizacaoConsumoDS_13_Tfautorizacaoconsumo_codigo_to = AV35TFAutorizacaoConsumo_Codigo_To;
         AV80WWAutorizacaoConsumoDS_14_Tfcontrato_codigo = AV38TFContrato_Codigo;
         AV81WWAutorizacaoConsumoDS_15_Tfcontrato_codigo_to = AV39TFContrato_Codigo_To;
         AV82WWAutorizacaoConsumoDS_16_Tfautorizacaoconsumo_vigenciainicio = AV42TFAutorizacaoConsumo_VigenciaInicio;
         AV83WWAutorizacaoConsumoDS_17_Tfautorizacaoconsumo_vigenciainicio_to = AV43TFAutorizacaoConsumo_VigenciaInicio_To;
         AV84WWAutorizacaoConsumoDS_18_Tfautorizacaoconsumo_vigenciafim = AV48TFAutorizacaoConsumo_VigenciaFim;
         AV85WWAutorizacaoConsumoDS_19_Tfautorizacaoconsumo_vigenciafim_to = AV49TFAutorizacaoConsumo_VigenciaFim_To;
         AV86WWAutorizacaoConsumoDS_20_Tfautorizacaoconsumo_unidademedicaonom = AV54TFAutorizacaoConsumo_UnidadeMedicaoNom;
         AV87WWAutorizacaoConsumoDS_21_Tfautorizacaoconsumo_unidademedicaonom_sel = AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel;
         AV88WWAutorizacaoConsumoDS_22_Tfautorizacaoconsumo_quantidade = AV58TFAutorizacaoConsumo_Quantidade;
         AV89WWAutorizacaoConsumoDS_23_Tfautorizacaoconsumo_quantidade_to = AV59TFAutorizacaoConsumo_Quantidade_To;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV67WWAutorizacaoConsumoDS_1_Dynamicfiltersselector1 ,
                                              AV68WWAutorizacaoConsumoDS_2_Autorizacaoconsumo_vigenciainicio1 ,
                                              AV69WWAutorizacaoConsumoDS_3_Autorizacaoconsumo_vigenciainicio_to1 ,
                                              AV70WWAutorizacaoConsumoDS_4_Dynamicfiltersenabled2 ,
                                              AV71WWAutorizacaoConsumoDS_5_Dynamicfiltersselector2 ,
                                              AV72WWAutorizacaoConsumoDS_6_Autorizacaoconsumo_vigenciainicio2 ,
                                              AV73WWAutorizacaoConsumoDS_7_Autorizacaoconsumo_vigenciainicio_to2 ,
                                              AV74WWAutorizacaoConsumoDS_8_Dynamicfiltersenabled3 ,
                                              AV75WWAutorizacaoConsumoDS_9_Dynamicfiltersselector3 ,
                                              AV76WWAutorizacaoConsumoDS_10_Autorizacaoconsumo_vigenciainicio3 ,
                                              AV77WWAutorizacaoConsumoDS_11_Autorizacaoconsumo_vigenciainicio_to3 ,
                                              AV78WWAutorizacaoConsumoDS_12_Tfautorizacaoconsumo_codigo ,
                                              AV79WWAutorizacaoConsumoDS_13_Tfautorizacaoconsumo_codigo_to ,
                                              AV80WWAutorizacaoConsumoDS_14_Tfcontrato_codigo ,
                                              AV81WWAutorizacaoConsumoDS_15_Tfcontrato_codigo_to ,
                                              AV82WWAutorizacaoConsumoDS_16_Tfautorizacaoconsumo_vigenciainicio ,
                                              AV83WWAutorizacaoConsumoDS_17_Tfautorizacaoconsumo_vigenciainicio_to ,
                                              AV84WWAutorizacaoConsumoDS_18_Tfautorizacaoconsumo_vigenciafim ,
                                              AV85WWAutorizacaoConsumoDS_19_Tfautorizacaoconsumo_vigenciafim_to ,
                                              AV87WWAutorizacaoConsumoDS_21_Tfautorizacaoconsumo_unidademedicaonom_sel ,
                                              AV86WWAutorizacaoConsumoDS_20_Tfautorizacaoconsumo_unidademedicaonom ,
                                              AV88WWAutorizacaoConsumoDS_22_Tfautorizacaoconsumo_quantidade ,
                                              AV89WWAutorizacaoConsumoDS_23_Tfautorizacaoconsumo_quantidade_to ,
                                              A1775AutorizacaoConsumo_VigenciaInicio ,
                                              A1774AutorizacaoConsumo_Codigo ,
                                              A74Contrato_Codigo ,
                                              A1776AutorizacaoConsumo_VigenciaFim ,
                                              A1778AutorizacaoConsumo_UnidadeMedicaoNom ,
                                              A1779AutorizacaoConsumo_Quantidade ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT,
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV86WWAutorizacaoConsumoDS_20_Tfautorizacaoconsumo_unidademedicaonom = StringUtil.PadR( StringUtil.RTrim( AV86WWAutorizacaoConsumoDS_20_Tfautorizacaoconsumo_unidademedicaonom), 50, "%");
         /* Using cursor H00NA3 */
         pr_default.execute(1, new Object[] {AV68WWAutorizacaoConsumoDS_2_Autorizacaoconsumo_vigenciainicio1, AV69WWAutorizacaoConsumoDS_3_Autorizacaoconsumo_vigenciainicio_to1, AV72WWAutorizacaoConsumoDS_6_Autorizacaoconsumo_vigenciainicio2, AV73WWAutorizacaoConsumoDS_7_Autorizacaoconsumo_vigenciainicio_to2, AV76WWAutorizacaoConsumoDS_10_Autorizacaoconsumo_vigenciainicio3, AV77WWAutorizacaoConsumoDS_11_Autorizacaoconsumo_vigenciainicio_to3, AV78WWAutorizacaoConsumoDS_12_Tfautorizacaoconsumo_codigo, AV79WWAutorizacaoConsumoDS_13_Tfautorizacaoconsumo_codigo_to, AV80WWAutorizacaoConsumoDS_14_Tfcontrato_codigo, AV81WWAutorizacaoConsumoDS_15_Tfcontrato_codigo_to, AV82WWAutorizacaoConsumoDS_16_Tfautorizacaoconsumo_vigenciainicio, AV83WWAutorizacaoConsumoDS_17_Tfautorizacaoconsumo_vigenciainicio_to, AV84WWAutorizacaoConsumoDS_18_Tfautorizacaoconsumo_vigenciafim, AV85WWAutorizacaoConsumoDS_19_Tfautorizacaoconsumo_vigenciafim_to, lV86WWAutorizacaoConsumoDS_20_Tfautorizacaoconsumo_unidademedicaonom, AV87WWAutorizacaoConsumoDS_21_Tfautorizacaoconsumo_unidademedicaonom_sel, AV88WWAutorizacaoConsumoDS_22_Tfautorizacaoconsumo_quantidade, AV89WWAutorizacaoConsumoDS_23_Tfautorizacaoconsumo_quantidade_to});
         GRID_nRecordCount = H00NA3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV67WWAutorizacaoConsumoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV68WWAutorizacaoConsumoDS_2_Autorizacaoconsumo_vigenciainicio1 = AV16AutorizacaoConsumo_VigenciaInicio1;
         AV69WWAutorizacaoConsumoDS_3_Autorizacaoconsumo_vigenciainicio_to1 = AV17AutorizacaoConsumo_VigenciaInicio_To1;
         AV70WWAutorizacaoConsumoDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV71WWAutorizacaoConsumoDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV72WWAutorizacaoConsumoDS_6_Autorizacaoconsumo_vigenciainicio2 = AV20AutorizacaoConsumo_VigenciaInicio2;
         AV73WWAutorizacaoConsumoDS_7_Autorizacaoconsumo_vigenciainicio_to2 = AV21AutorizacaoConsumo_VigenciaInicio_To2;
         AV74WWAutorizacaoConsumoDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV75WWAutorizacaoConsumoDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV76WWAutorizacaoConsumoDS_10_Autorizacaoconsumo_vigenciainicio3 = AV24AutorizacaoConsumo_VigenciaInicio3;
         AV77WWAutorizacaoConsumoDS_11_Autorizacaoconsumo_vigenciainicio_to3 = AV25AutorizacaoConsumo_VigenciaInicio_To3;
         AV78WWAutorizacaoConsumoDS_12_Tfautorizacaoconsumo_codigo = AV34TFAutorizacaoConsumo_Codigo;
         AV79WWAutorizacaoConsumoDS_13_Tfautorizacaoconsumo_codigo_to = AV35TFAutorizacaoConsumo_Codigo_To;
         AV80WWAutorizacaoConsumoDS_14_Tfcontrato_codigo = AV38TFContrato_Codigo;
         AV81WWAutorizacaoConsumoDS_15_Tfcontrato_codigo_to = AV39TFContrato_Codigo_To;
         AV82WWAutorizacaoConsumoDS_16_Tfautorizacaoconsumo_vigenciainicio = AV42TFAutorizacaoConsumo_VigenciaInicio;
         AV83WWAutorizacaoConsumoDS_17_Tfautorizacaoconsumo_vigenciainicio_to = AV43TFAutorizacaoConsumo_VigenciaInicio_To;
         AV84WWAutorizacaoConsumoDS_18_Tfautorizacaoconsumo_vigenciafim = AV48TFAutorizacaoConsumo_VigenciaFim;
         AV85WWAutorizacaoConsumoDS_19_Tfautorizacaoconsumo_vigenciafim_to = AV49TFAutorizacaoConsumo_VigenciaFim_To;
         AV86WWAutorizacaoConsumoDS_20_Tfautorizacaoconsumo_unidademedicaonom = AV54TFAutorizacaoConsumo_UnidadeMedicaoNom;
         AV87WWAutorizacaoConsumoDS_21_Tfautorizacaoconsumo_unidademedicaonom_sel = AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel;
         AV88WWAutorizacaoConsumoDS_22_Tfautorizacaoconsumo_quantidade = AV58TFAutorizacaoConsumo_Quantidade;
         AV89WWAutorizacaoConsumoDS_23_Tfautorizacaoconsumo_quantidade_to = AV59TFAutorizacaoConsumo_Quantidade_To;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16AutorizacaoConsumo_VigenciaInicio1, AV17AutorizacaoConsumo_VigenciaInicio_To1, AV19DynamicFiltersSelector2, AV20AutorizacaoConsumo_VigenciaInicio2, AV21AutorizacaoConsumo_VigenciaInicio_To2, AV23DynamicFiltersSelector3, AV24AutorizacaoConsumo_VigenciaInicio3, AV25AutorizacaoConsumo_VigenciaInicio_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFAutorizacaoConsumo_Codigo, AV35TFAutorizacaoConsumo_Codigo_To, AV38TFContrato_Codigo, AV39TFContrato_Codigo_To, AV42TFAutorizacaoConsumo_VigenciaInicio, AV43TFAutorizacaoConsumo_VigenciaInicio_To, AV48TFAutorizacaoConsumo_VigenciaFim, AV49TFAutorizacaoConsumo_VigenciaFim_To, AV54TFAutorizacaoConsumo_UnidadeMedicaoNom, AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel, AV58TFAutorizacaoConsumo_Quantidade, AV59TFAutorizacaoConsumo_Quantidade_To, AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace, AV40ddo_Contrato_CodigoTitleControlIdToReplace, AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace, AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace, AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace, AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace, AV90Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV67WWAutorizacaoConsumoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV68WWAutorizacaoConsumoDS_2_Autorizacaoconsumo_vigenciainicio1 = AV16AutorizacaoConsumo_VigenciaInicio1;
         AV69WWAutorizacaoConsumoDS_3_Autorizacaoconsumo_vigenciainicio_to1 = AV17AutorizacaoConsumo_VigenciaInicio_To1;
         AV70WWAutorizacaoConsumoDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV71WWAutorizacaoConsumoDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV72WWAutorizacaoConsumoDS_6_Autorizacaoconsumo_vigenciainicio2 = AV20AutorizacaoConsumo_VigenciaInicio2;
         AV73WWAutorizacaoConsumoDS_7_Autorizacaoconsumo_vigenciainicio_to2 = AV21AutorizacaoConsumo_VigenciaInicio_To2;
         AV74WWAutorizacaoConsumoDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV75WWAutorizacaoConsumoDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV76WWAutorizacaoConsumoDS_10_Autorizacaoconsumo_vigenciainicio3 = AV24AutorizacaoConsumo_VigenciaInicio3;
         AV77WWAutorizacaoConsumoDS_11_Autorizacaoconsumo_vigenciainicio_to3 = AV25AutorizacaoConsumo_VigenciaInicio_To3;
         AV78WWAutorizacaoConsumoDS_12_Tfautorizacaoconsumo_codigo = AV34TFAutorizacaoConsumo_Codigo;
         AV79WWAutorizacaoConsumoDS_13_Tfautorizacaoconsumo_codigo_to = AV35TFAutorizacaoConsumo_Codigo_To;
         AV80WWAutorizacaoConsumoDS_14_Tfcontrato_codigo = AV38TFContrato_Codigo;
         AV81WWAutorizacaoConsumoDS_15_Tfcontrato_codigo_to = AV39TFContrato_Codigo_To;
         AV82WWAutorizacaoConsumoDS_16_Tfautorizacaoconsumo_vigenciainicio = AV42TFAutorizacaoConsumo_VigenciaInicio;
         AV83WWAutorizacaoConsumoDS_17_Tfautorizacaoconsumo_vigenciainicio_to = AV43TFAutorizacaoConsumo_VigenciaInicio_To;
         AV84WWAutorizacaoConsumoDS_18_Tfautorizacaoconsumo_vigenciafim = AV48TFAutorizacaoConsumo_VigenciaFim;
         AV85WWAutorizacaoConsumoDS_19_Tfautorizacaoconsumo_vigenciafim_to = AV49TFAutorizacaoConsumo_VigenciaFim_To;
         AV86WWAutorizacaoConsumoDS_20_Tfautorizacaoconsumo_unidademedicaonom = AV54TFAutorizacaoConsumo_UnidadeMedicaoNom;
         AV87WWAutorizacaoConsumoDS_21_Tfautorizacaoconsumo_unidademedicaonom_sel = AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel;
         AV88WWAutorizacaoConsumoDS_22_Tfautorizacaoconsumo_quantidade = AV58TFAutorizacaoConsumo_Quantidade;
         AV89WWAutorizacaoConsumoDS_23_Tfautorizacaoconsumo_quantidade_to = AV59TFAutorizacaoConsumo_Quantidade_To;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16AutorizacaoConsumo_VigenciaInicio1, AV17AutorizacaoConsumo_VigenciaInicio_To1, AV19DynamicFiltersSelector2, AV20AutorizacaoConsumo_VigenciaInicio2, AV21AutorizacaoConsumo_VigenciaInicio_To2, AV23DynamicFiltersSelector3, AV24AutorizacaoConsumo_VigenciaInicio3, AV25AutorizacaoConsumo_VigenciaInicio_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFAutorizacaoConsumo_Codigo, AV35TFAutorizacaoConsumo_Codigo_To, AV38TFContrato_Codigo, AV39TFContrato_Codigo_To, AV42TFAutorizacaoConsumo_VigenciaInicio, AV43TFAutorizacaoConsumo_VigenciaInicio_To, AV48TFAutorizacaoConsumo_VigenciaFim, AV49TFAutorizacaoConsumo_VigenciaFim_To, AV54TFAutorizacaoConsumo_UnidadeMedicaoNom, AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel, AV58TFAutorizacaoConsumo_Quantidade, AV59TFAutorizacaoConsumo_Quantidade_To, AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace, AV40ddo_Contrato_CodigoTitleControlIdToReplace, AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace, AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace, AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace, AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace, AV90Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV67WWAutorizacaoConsumoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV68WWAutorizacaoConsumoDS_2_Autorizacaoconsumo_vigenciainicio1 = AV16AutorizacaoConsumo_VigenciaInicio1;
         AV69WWAutorizacaoConsumoDS_3_Autorizacaoconsumo_vigenciainicio_to1 = AV17AutorizacaoConsumo_VigenciaInicio_To1;
         AV70WWAutorizacaoConsumoDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV71WWAutorizacaoConsumoDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV72WWAutorizacaoConsumoDS_6_Autorizacaoconsumo_vigenciainicio2 = AV20AutorizacaoConsumo_VigenciaInicio2;
         AV73WWAutorizacaoConsumoDS_7_Autorizacaoconsumo_vigenciainicio_to2 = AV21AutorizacaoConsumo_VigenciaInicio_To2;
         AV74WWAutorizacaoConsumoDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV75WWAutorizacaoConsumoDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV76WWAutorizacaoConsumoDS_10_Autorizacaoconsumo_vigenciainicio3 = AV24AutorizacaoConsumo_VigenciaInicio3;
         AV77WWAutorizacaoConsumoDS_11_Autorizacaoconsumo_vigenciainicio_to3 = AV25AutorizacaoConsumo_VigenciaInicio_To3;
         AV78WWAutorizacaoConsumoDS_12_Tfautorizacaoconsumo_codigo = AV34TFAutorizacaoConsumo_Codigo;
         AV79WWAutorizacaoConsumoDS_13_Tfautorizacaoconsumo_codigo_to = AV35TFAutorizacaoConsumo_Codigo_To;
         AV80WWAutorizacaoConsumoDS_14_Tfcontrato_codigo = AV38TFContrato_Codigo;
         AV81WWAutorizacaoConsumoDS_15_Tfcontrato_codigo_to = AV39TFContrato_Codigo_To;
         AV82WWAutorizacaoConsumoDS_16_Tfautorizacaoconsumo_vigenciainicio = AV42TFAutorizacaoConsumo_VigenciaInicio;
         AV83WWAutorizacaoConsumoDS_17_Tfautorizacaoconsumo_vigenciainicio_to = AV43TFAutorizacaoConsumo_VigenciaInicio_To;
         AV84WWAutorizacaoConsumoDS_18_Tfautorizacaoconsumo_vigenciafim = AV48TFAutorizacaoConsumo_VigenciaFim;
         AV85WWAutorizacaoConsumoDS_19_Tfautorizacaoconsumo_vigenciafim_to = AV49TFAutorizacaoConsumo_VigenciaFim_To;
         AV86WWAutorizacaoConsumoDS_20_Tfautorizacaoconsumo_unidademedicaonom = AV54TFAutorizacaoConsumo_UnidadeMedicaoNom;
         AV87WWAutorizacaoConsumoDS_21_Tfautorizacaoconsumo_unidademedicaonom_sel = AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel;
         AV88WWAutorizacaoConsumoDS_22_Tfautorizacaoconsumo_quantidade = AV58TFAutorizacaoConsumo_Quantidade;
         AV89WWAutorizacaoConsumoDS_23_Tfautorizacaoconsumo_quantidade_to = AV59TFAutorizacaoConsumo_Quantidade_To;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16AutorizacaoConsumo_VigenciaInicio1, AV17AutorizacaoConsumo_VigenciaInicio_To1, AV19DynamicFiltersSelector2, AV20AutorizacaoConsumo_VigenciaInicio2, AV21AutorizacaoConsumo_VigenciaInicio_To2, AV23DynamicFiltersSelector3, AV24AutorizacaoConsumo_VigenciaInicio3, AV25AutorizacaoConsumo_VigenciaInicio_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFAutorizacaoConsumo_Codigo, AV35TFAutorizacaoConsumo_Codigo_To, AV38TFContrato_Codigo, AV39TFContrato_Codigo_To, AV42TFAutorizacaoConsumo_VigenciaInicio, AV43TFAutorizacaoConsumo_VigenciaInicio_To, AV48TFAutorizacaoConsumo_VigenciaFim, AV49TFAutorizacaoConsumo_VigenciaFim_To, AV54TFAutorizacaoConsumo_UnidadeMedicaoNom, AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel, AV58TFAutorizacaoConsumo_Quantidade, AV59TFAutorizacaoConsumo_Quantidade_To, AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace, AV40ddo_Contrato_CodigoTitleControlIdToReplace, AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace, AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace, AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace, AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace, AV90Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV67WWAutorizacaoConsumoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV68WWAutorizacaoConsumoDS_2_Autorizacaoconsumo_vigenciainicio1 = AV16AutorizacaoConsumo_VigenciaInicio1;
         AV69WWAutorizacaoConsumoDS_3_Autorizacaoconsumo_vigenciainicio_to1 = AV17AutorizacaoConsumo_VigenciaInicio_To1;
         AV70WWAutorizacaoConsumoDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV71WWAutorizacaoConsumoDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV72WWAutorizacaoConsumoDS_6_Autorizacaoconsumo_vigenciainicio2 = AV20AutorizacaoConsumo_VigenciaInicio2;
         AV73WWAutorizacaoConsumoDS_7_Autorizacaoconsumo_vigenciainicio_to2 = AV21AutorizacaoConsumo_VigenciaInicio_To2;
         AV74WWAutorizacaoConsumoDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV75WWAutorizacaoConsumoDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV76WWAutorizacaoConsumoDS_10_Autorizacaoconsumo_vigenciainicio3 = AV24AutorizacaoConsumo_VigenciaInicio3;
         AV77WWAutorizacaoConsumoDS_11_Autorizacaoconsumo_vigenciainicio_to3 = AV25AutorizacaoConsumo_VigenciaInicio_To3;
         AV78WWAutorizacaoConsumoDS_12_Tfautorizacaoconsumo_codigo = AV34TFAutorizacaoConsumo_Codigo;
         AV79WWAutorizacaoConsumoDS_13_Tfautorizacaoconsumo_codigo_to = AV35TFAutorizacaoConsumo_Codigo_To;
         AV80WWAutorizacaoConsumoDS_14_Tfcontrato_codigo = AV38TFContrato_Codigo;
         AV81WWAutorizacaoConsumoDS_15_Tfcontrato_codigo_to = AV39TFContrato_Codigo_To;
         AV82WWAutorizacaoConsumoDS_16_Tfautorizacaoconsumo_vigenciainicio = AV42TFAutorizacaoConsumo_VigenciaInicio;
         AV83WWAutorizacaoConsumoDS_17_Tfautorizacaoconsumo_vigenciainicio_to = AV43TFAutorizacaoConsumo_VigenciaInicio_To;
         AV84WWAutorizacaoConsumoDS_18_Tfautorizacaoconsumo_vigenciafim = AV48TFAutorizacaoConsumo_VigenciaFim;
         AV85WWAutorizacaoConsumoDS_19_Tfautorizacaoconsumo_vigenciafim_to = AV49TFAutorizacaoConsumo_VigenciaFim_To;
         AV86WWAutorizacaoConsumoDS_20_Tfautorizacaoconsumo_unidademedicaonom = AV54TFAutorizacaoConsumo_UnidadeMedicaoNom;
         AV87WWAutorizacaoConsumoDS_21_Tfautorizacaoconsumo_unidademedicaonom_sel = AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel;
         AV88WWAutorizacaoConsumoDS_22_Tfautorizacaoconsumo_quantidade = AV58TFAutorizacaoConsumo_Quantidade;
         AV89WWAutorizacaoConsumoDS_23_Tfautorizacaoconsumo_quantidade_to = AV59TFAutorizacaoConsumo_Quantidade_To;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16AutorizacaoConsumo_VigenciaInicio1, AV17AutorizacaoConsumo_VigenciaInicio_To1, AV19DynamicFiltersSelector2, AV20AutorizacaoConsumo_VigenciaInicio2, AV21AutorizacaoConsumo_VigenciaInicio_To2, AV23DynamicFiltersSelector3, AV24AutorizacaoConsumo_VigenciaInicio3, AV25AutorizacaoConsumo_VigenciaInicio_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFAutorizacaoConsumo_Codigo, AV35TFAutorizacaoConsumo_Codigo_To, AV38TFContrato_Codigo, AV39TFContrato_Codigo_To, AV42TFAutorizacaoConsumo_VigenciaInicio, AV43TFAutorizacaoConsumo_VigenciaInicio_To, AV48TFAutorizacaoConsumo_VigenciaFim, AV49TFAutorizacaoConsumo_VigenciaFim_To, AV54TFAutorizacaoConsumo_UnidadeMedicaoNom, AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel, AV58TFAutorizacaoConsumo_Quantidade, AV59TFAutorizacaoConsumo_Quantidade_To, AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace, AV40ddo_Contrato_CodigoTitleControlIdToReplace, AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace, AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace, AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace, AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace, AV90Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV67WWAutorizacaoConsumoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV68WWAutorizacaoConsumoDS_2_Autorizacaoconsumo_vigenciainicio1 = AV16AutorizacaoConsumo_VigenciaInicio1;
         AV69WWAutorizacaoConsumoDS_3_Autorizacaoconsumo_vigenciainicio_to1 = AV17AutorizacaoConsumo_VigenciaInicio_To1;
         AV70WWAutorizacaoConsumoDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV71WWAutorizacaoConsumoDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV72WWAutorizacaoConsumoDS_6_Autorizacaoconsumo_vigenciainicio2 = AV20AutorizacaoConsumo_VigenciaInicio2;
         AV73WWAutorizacaoConsumoDS_7_Autorizacaoconsumo_vigenciainicio_to2 = AV21AutorizacaoConsumo_VigenciaInicio_To2;
         AV74WWAutorizacaoConsumoDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV75WWAutorizacaoConsumoDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV76WWAutorizacaoConsumoDS_10_Autorizacaoconsumo_vigenciainicio3 = AV24AutorizacaoConsumo_VigenciaInicio3;
         AV77WWAutorizacaoConsumoDS_11_Autorizacaoconsumo_vigenciainicio_to3 = AV25AutorizacaoConsumo_VigenciaInicio_To3;
         AV78WWAutorizacaoConsumoDS_12_Tfautorizacaoconsumo_codigo = AV34TFAutorizacaoConsumo_Codigo;
         AV79WWAutorizacaoConsumoDS_13_Tfautorizacaoconsumo_codigo_to = AV35TFAutorizacaoConsumo_Codigo_To;
         AV80WWAutorizacaoConsumoDS_14_Tfcontrato_codigo = AV38TFContrato_Codigo;
         AV81WWAutorizacaoConsumoDS_15_Tfcontrato_codigo_to = AV39TFContrato_Codigo_To;
         AV82WWAutorizacaoConsumoDS_16_Tfautorizacaoconsumo_vigenciainicio = AV42TFAutorizacaoConsumo_VigenciaInicio;
         AV83WWAutorizacaoConsumoDS_17_Tfautorizacaoconsumo_vigenciainicio_to = AV43TFAutorizacaoConsumo_VigenciaInicio_To;
         AV84WWAutorizacaoConsumoDS_18_Tfautorizacaoconsumo_vigenciafim = AV48TFAutorizacaoConsumo_VigenciaFim;
         AV85WWAutorizacaoConsumoDS_19_Tfautorizacaoconsumo_vigenciafim_to = AV49TFAutorizacaoConsumo_VigenciaFim_To;
         AV86WWAutorizacaoConsumoDS_20_Tfautorizacaoconsumo_unidademedicaonom = AV54TFAutorizacaoConsumo_UnidadeMedicaoNom;
         AV87WWAutorizacaoConsumoDS_21_Tfautorizacaoconsumo_unidademedicaonom_sel = AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel;
         AV88WWAutorizacaoConsumoDS_22_Tfautorizacaoconsumo_quantidade = AV58TFAutorizacaoConsumo_Quantidade;
         AV89WWAutorizacaoConsumoDS_23_Tfautorizacaoconsumo_quantidade_to = AV59TFAutorizacaoConsumo_Quantidade_To;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16AutorizacaoConsumo_VigenciaInicio1, AV17AutorizacaoConsumo_VigenciaInicio_To1, AV19DynamicFiltersSelector2, AV20AutorizacaoConsumo_VigenciaInicio2, AV21AutorizacaoConsumo_VigenciaInicio_To2, AV23DynamicFiltersSelector3, AV24AutorizacaoConsumo_VigenciaInicio3, AV25AutorizacaoConsumo_VigenciaInicio_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFAutorizacaoConsumo_Codigo, AV35TFAutorizacaoConsumo_Codigo_To, AV38TFContrato_Codigo, AV39TFContrato_Codigo_To, AV42TFAutorizacaoConsumo_VigenciaInicio, AV43TFAutorizacaoConsumo_VigenciaInicio_To, AV48TFAutorizacaoConsumo_VigenciaFim, AV49TFAutorizacaoConsumo_VigenciaFim_To, AV54TFAutorizacaoConsumo_UnidadeMedicaoNom, AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel, AV58TFAutorizacaoConsumo_Quantidade, AV59TFAutorizacaoConsumo_Quantidade_To, AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace, AV40ddo_Contrato_CodigoTitleControlIdToReplace, AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace, AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace, AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace, AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace, AV90Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return (int)(0) ;
      }

      protected void STRUPNA0( )
      {
         /* Before Start, stand alone formulas. */
         AV90Pgmname = "WWAutorizacaoConsumo";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E29NA2 */
         E29NA2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV61DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vAUTORIZACAOCONSUMO_CODIGOTITLEFILTERDATA"), AV33AutorizacaoConsumo_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATO_CODIGOTITLEFILTERDATA"), AV37Contrato_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vAUTORIZACAOCONSUMO_VIGENCIAINICIOTITLEFILTERDATA"), AV41AutorizacaoConsumo_VigenciaInicioTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vAUTORIZACAOCONSUMO_VIGENCIAFIMTITLEFILTERDATA"), AV47AutorizacaoConsumo_VigenciaFimTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vAUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLEFILTERDATA"), AV53AutorizacaoConsumo_UnidadeMedicaoNomTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vAUTORIZACAOCONSUMO_QUANTIDADETITLEFILTERDATA"), AV57AutorizacaoConsumo_QuantidadeTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( context.localUtil.VCDateTime( cgiGet( edtavAutorizacaoconsumo_vigenciainicio1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Autorizacao Consumo_Vigencia Inicio1"}), 1, "vAUTORIZACAOCONSUMO_VIGENCIAINICIO1");
               GX_FocusControl = edtavAutorizacaoconsumo_vigenciainicio1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV16AutorizacaoConsumo_VigenciaInicio1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16AutorizacaoConsumo_VigenciaInicio1", context.localUtil.Format(AV16AutorizacaoConsumo_VigenciaInicio1, "99/99/99"));
            }
            else
            {
               AV16AutorizacaoConsumo_VigenciaInicio1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavAutorizacaoconsumo_vigenciainicio1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16AutorizacaoConsumo_VigenciaInicio1", context.localUtil.Format(AV16AutorizacaoConsumo_VigenciaInicio1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavAutorizacaoconsumo_vigenciainicio_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Autorizacao Consumo_Vigencia Inicio_To1"}), 1, "vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO1");
               GX_FocusControl = edtavAutorizacaoconsumo_vigenciainicio_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV17AutorizacaoConsumo_VigenciaInicio_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17AutorizacaoConsumo_VigenciaInicio_To1", context.localUtil.Format(AV17AutorizacaoConsumo_VigenciaInicio_To1, "99/99/99"));
            }
            else
            {
               AV17AutorizacaoConsumo_VigenciaInicio_To1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavAutorizacaoconsumo_vigenciainicio_to1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17AutorizacaoConsumo_VigenciaInicio_To1", context.localUtil.Format(AV17AutorizacaoConsumo_VigenciaInicio_To1, "99/99/99"));
            }
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            if ( context.localUtil.VCDateTime( cgiGet( edtavAutorizacaoconsumo_vigenciainicio2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Autorizacao Consumo_Vigencia Inicio2"}), 1, "vAUTORIZACAOCONSUMO_VIGENCIAINICIO2");
               GX_FocusControl = edtavAutorizacaoconsumo_vigenciainicio2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV20AutorizacaoConsumo_VigenciaInicio2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20AutorizacaoConsumo_VigenciaInicio2", context.localUtil.Format(AV20AutorizacaoConsumo_VigenciaInicio2, "99/99/99"));
            }
            else
            {
               AV20AutorizacaoConsumo_VigenciaInicio2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavAutorizacaoconsumo_vigenciainicio2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20AutorizacaoConsumo_VigenciaInicio2", context.localUtil.Format(AV20AutorizacaoConsumo_VigenciaInicio2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavAutorizacaoconsumo_vigenciainicio_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Autorizacao Consumo_Vigencia Inicio_To2"}), 1, "vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO2");
               GX_FocusControl = edtavAutorizacaoconsumo_vigenciainicio_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV21AutorizacaoConsumo_VigenciaInicio_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21AutorizacaoConsumo_VigenciaInicio_To2", context.localUtil.Format(AV21AutorizacaoConsumo_VigenciaInicio_To2, "99/99/99"));
            }
            else
            {
               AV21AutorizacaoConsumo_VigenciaInicio_To2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavAutorizacaoconsumo_vigenciainicio_to2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21AutorizacaoConsumo_VigenciaInicio_To2", context.localUtil.Format(AV21AutorizacaoConsumo_VigenciaInicio_To2, "99/99/99"));
            }
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            if ( context.localUtil.VCDateTime( cgiGet( edtavAutorizacaoconsumo_vigenciainicio3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Autorizacao Consumo_Vigencia Inicio3"}), 1, "vAUTORIZACAOCONSUMO_VIGENCIAINICIO3");
               GX_FocusControl = edtavAutorizacaoconsumo_vigenciainicio3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV24AutorizacaoConsumo_VigenciaInicio3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24AutorizacaoConsumo_VigenciaInicio3", context.localUtil.Format(AV24AutorizacaoConsumo_VigenciaInicio3, "99/99/99"));
            }
            else
            {
               AV24AutorizacaoConsumo_VigenciaInicio3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavAutorizacaoconsumo_vigenciainicio3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24AutorizacaoConsumo_VigenciaInicio3", context.localUtil.Format(AV24AutorizacaoConsumo_VigenciaInicio3, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavAutorizacaoconsumo_vigenciainicio_to3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Autorizacao Consumo_Vigencia Inicio_To3"}), 1, "vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO3");
               GX_FocusControl = edtavAutorizacaoconsumo_vigenciainicio_to3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV25AutorizacaoConsumo_VigenciaInicio_To3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25AutorizacaoConsumo_VigenciaInicio_To3", context.localUtil.Format(AV25AutorizacaoConsumo_VigenciaInicio_To3, "99/99/99"));
            }
            else
            {
               AV25AutorizacaoConsumo_VigenciaInicio_To3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavAutorizacaoconsumo_vigenciainicio_to3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25AutorizacaoConsumo_VigenciaInicio_To3", context.localUtil.Format(AV25AutorizacaoConsumo_VigenciaInicio_To3, "99/99/99"));
            }
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfautorizacaoconsumo_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfautorizacaoconsumo_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFAUTORIZACAOCONSUMO_CODIGO");
               GX_FocusControl = edtavTfautorizacaoconsumo_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV34TFAutorizacaoConsumo_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFAutorizacaoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34TFAutorizacaoConsumo_Codigo), 6, 0)));
            }
            else
            {
               AV34TFAutorizacaoConsumo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfautorizacaoconsumo_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFAutorizacaoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34TFAutorizacaoConsumo_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfautorizacaoconsumo_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfautorizacaoconsumo_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFAUTORIZACAOCONSUMO_CODIGO_TO");
               GX_FocusControl = edtavTfautorizacaoconsumo_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV35TFAutorizacaoConsumo_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFAutorizacaoConsumo_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFAutorizacaoConsumo_Codigo_To), 6, 0)));
            }
            else
            {
               AV35TFAutorizacaoConsumo_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfautorizacaoconsumo_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFAutorizacaoConsumo_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFAutorizacaoConsumo_Codigo_To), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontrato_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontrato_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATO_CODIGO");
               GX_FocusControl = edtavTfcontrato_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV38TFContrato_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFContrato_Codigo), 6, 0)));
            }
            else
            {
               AV38TFContrato_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfcontrato_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFContrato_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontrato_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontrato_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATO_CODIGO_TO");
               GX_FocusControl = edtavTfcontrato_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV39TFContrato_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFContrato_Codigo_To), 6, 0)));
            }
            else
            {
               AV39TFContrato_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontrato_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFContrato_Codigo_To), 6, 0)));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfautorizacaoconsumo_vigenciainicio_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFAutorizacao Consumo_Vigencia Inicio"}), 1, "vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO");
               GX_FocusControl = edtavTfautorizacaoconsumo_vigenciainicio_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV42TFAutorizacaoConsumo_VigenciaInicio = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFAutorizacaoConsumo_VigenciaInicio", context.localUtil.Format(AV42TFAutorizacaoConsumo_VigenciaInicio, "99/99/99"));
            }
            else
            {
               AV42TFAutorizacaoConsumo_VigenciaInicio = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfautorizacaoconsumo_vigenciainicio_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFAutorizacaoConsumo_VigenciaInicio", context.localUtil.Format(AV42TFAutorizacaoConsumo_VigenciaInicio, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfautorizacaoconsumo_vigenciainicio_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFAutorizacao Consumo_Vigencia Inicio_To"}), 1, "vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO");
               GX_FocusControl = edtavTfautorizacaoconsumo_vigenciainicio_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV43TFAutorizacaoConsumo_VigenciaInicio_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFAutorizacaoConsumo_VigenciaInicio_To", context.localUtil.Format(AV43TFAutorizacaoConsumo_VigenciaInicio_To, "99/99/99"));
            }
            else
            {
               AV43TFAutorizacaoConsumo_VigenciaInicio_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfautorizacaoconsumo_vigenciainicio_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFAutorizacaoConsumo_VigenciaInicio_To", context.localUtil.Format(AV43TFAutorizacaoConsumo_VigenciaInicio_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_autorizacaoconsumo_vigenciainicioauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Autorizacao Consumo_Vigencia Inicio Aux Date"}), 1, "vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOAUXDATE");
               GX_FocusControl = edtavDdo_autorizacaoconsumo_vigenciainicioauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV44DDO_AutorizacaoConsumo_VigenciaInicioAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44DDO_AutorizacaoConsumo_VigenciaInicioAuxDate", context.localUtil.Format(AV44DDO_AutorizacaoConsumo_VigenciaInicioAuxDate, "99/99/99"));
            }
            else
            {
               AV44DDO_AutorizacaoConsumo_VigenciaInicioAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_autorizacaoconsumo_vigenciainicioauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44DDO_AutorizacaoConsumo_VigenciaInicioAuxDate", context.localUtil.Format(AV44DDO_AutorizacaoConsumo_VigenciaInicioAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_autorizacaoconsumo_vigenciainicioauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Autorizacao Consumo_Vigencia Inicio Aux Date To"}), 1, "vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOAUXDATETO");
               GX_FocusControl = edtavDdo_autorizacaoconsumo_vigenciainicioauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV45DDO_AutorizacaoConsumo_VigenciaInicioAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45DDO_AutorizacaoConsumo_VigenciaInicioAuxDateTo", context.localUtil.Format(AV45DDO_AutorizacaoConsumo_VigenciaInicioAuxDateTo, "99/99/99"));
            }
            else
            {
               AV45DDO_AutorizacaoConsumo_VigenciaInicioAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_autorizacaoconsumo_vigenciainicioauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45DDO_AutorizacaoConsumo_VigenciaInicioAuxDateTo", context.localUtil.Format(AV45DDO_AutorizacaoConsumo_VigenciaInicioAuxDateTo, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfautorizacaoconsumo_vigenciafim_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFAutorizacao Consumo_Vigencia Fim"}), 1, "vTFAUTORIZACAOCONSUMO_VIGENCIAFIM");
               GX_FocusControl = edtavTfautorizacaoconsumo_vigenciafim_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV48TFAutorizacaoConsumo_VigenciaFim = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFAutorizacaoConsumo_VigenciaFim", context.localUtil.Format(AV48TFAutorizacaoConsumo_VigenciaFim, "99/99/99"));
            }
            else
            {
               AV48TFAutorizacaoConsumo_VigenciaFim = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfautorizacaoconsumo_vigenciafim_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFAutorizacaoConsumo_VigenciaFim", context.localUtil.Format(AV48TFAutorizacaoConsumo_VigenciaFim, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfautorizacaoconsumo_vigenciafim_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFAutorizacao Consumo_Vigencia Fim_To"}), 1, "vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO");
               GX_FocusControl = edtavTfautorizacaoconsumo_vigenciafim_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV49TFAutorizacaoConsumo_VigenciaFim_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFAutorizacaoConsumo_VigenciaFim_To", context.localUtil.Format(AV49TFAutorizacaoConsumo_VigenciaFim_To, "99/99/99"));
            }
            else
            {
               AV49TFAutorizacaoConsumo_VigenciaFim_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfautorizacaoconsumo_vigenciafim_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFAutorizacaoConsumo_VigenciaFim_To", context.localUtil.Format(AV49TFAutorizacaoConsumo_VigenciaFim_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_autorizacaoconsumo_vigenciafimauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Autorizacao Consumo_Vigencia Fim Aux Date"}), 1, "vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMAUXDATE");
               GX_FocusControl = edtavDdo_autorizacaoconsumo_vigenciafimauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV50DDO_AutorizacaoConsumo_VigenciaFimAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50DDO_AutorizacaoConsumo_VigenciaFimAuxDate", context.localUtil.Format(AV50DDO_AutorizacaoConsumo_VigenciaFimAuxDate, "99/99/99"));
            }
            else
            {
               AV50DDO_AutorizacaoConsumo_VigenciaFimAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_autorizacaoconsumo_vigenciafimauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50DDO_AutorizacaoConsumo_VigenciaFimAuxDate", context.localUtil.Format(AV50DDO_AutorizacaoConsumo_VigenciaFimAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_autorizacaoconsumo_vigenciafimauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Autorizacao Consumo_Vigencia Fim Aux Date To"}), 1, "vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMAUXDATETO");
               GX_FocusControl = edtavDdo_autorizacaoconsumo_vigenciafimauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV51DDO_AutorizacaoConsumo_VigenciaFimAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51DDO_AutorizacaoConsumo_VigenciaFimAuxDateTo", context.localUtil.Format(AV51DDO_AutorizacaoConsumo_VigenciaFimAuxDateTo, "99/99/99"));
            }
            else
            {
               AV51DDO_AutorizacaoConsumo_VigenciaFimAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_autorizacaoconsumo_vigenciafimauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51DDO_AutorizacaoConsumo_VigenciaFimAuxDateTo", context.localUtil.Format(AV51DDO_AutorizacaoConsumo_VigenciaFimAuxDateTo, "99/99/99"));
            }
            AV54TFAutorizacaoConsumo_UnidadeMedicaoNom = StringUtil.Upper( cgiGet( edtavTfautorizacaoconsumo_unidademedicaonom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFAutorizacaoConsumo_UnidadeMedicaoNom", AV54TFAutorizacaoConsumo_UnidadeMedicaoNom);
            AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel = StringUtil.Upper( cgiGet( edtavTfautorizacaoconsumo_unidademedicaonom_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel", AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfautorizacaoconsumo_quantidade_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfautorizacaoconsumo_quantidade_Internalname), ",", ".") > Convert.ToDecimal( 999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFAUTORIZACAOCONSUMO_QUANTIDADE");
               GX_FocusControl = edtavTfautorizacaoconsumo_quantidade_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV58TFAutorizacaoConsumo_Quantidade = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFAutorizacaoConsumo_Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58TFAutorizacaoConsumo_Quantidade), 3, 0)));
            }
            else
            {
               AV58TFAutorizacaoConsumo_Quantidade = (short)(context.localUtil.CToN( cgiGet( edtavTfautorizacaoconsumo_quantidade_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFAutorizacaoConsumo_Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58TFAutorizacaoConsumo_Quantidade), 3, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfautorizacaoconsumo_quantidade_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfautorizacaoconsumo_quantidade_to_Internalname), ",", ".") > Convert.ToDecimal( 999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO");
               GX_FocusControl = edtavTfautorizacaoconsumo_quantidade_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV59TFAutorizacaoConsumo_Quantidade_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFAutorizacaoConsumo_Quantidade_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59TFAutorizacaoConsumo_Quantidade_To), 3, 0)));
            }
            else
            {
               AV59TFAutorizacaoConsumo_Quantidade_To = (short)(context.localUtil.CToN( cgiGet( edtavTfautorizacaoconsumo_quantidade_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFAutorizacaoConsumo_Quantidade_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59TFAutorizacaoConsumo_Quantidade_To), 3, 0)));
            }
            AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_autorizacaoconsumo_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace", AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace);
            AV40ddo_Contrato_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_contrato_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ddo_Contrato_CodigoTitleControlIdToReplace", AV40ddo_Contrato_CodigoTitleControlIdToReplace);
            AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace = cgiGet( edtavDdo_autorizacaoconsumo_vigenciainiciotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace", AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace);
            AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace = cgiGet( edtavDdo_autorizacaoconsumo_vigenciafimtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace", AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace);
            AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace = cgiGet( edtavDdo_autorizacaoconsumo_unidademedicaonomtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace", AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace);
            AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace = cgiGet( edtavDdo_autorizacaoconsumo_quantidadetitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace", AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_94 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_94"), ",", "."));
            AV63GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV64GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_autorizacaoconsumo_codigo_Caption = cgiGet( "DDO_AUTORIZACAOCONSUMO_CODIGO_Caption");
            Ddo_autorizacaoconsumo_codigo_Tooltip = cgiGet( "DDO_AUTORIZACAOCONSUMO_CODIGO_Tooltip");
            Ddo_autorizacaoconsumo_codigo_Cls = cgiGet( "DDO_AUTORIZACAOCONSUMO_CODIGO_Cls");
            Ddo_autorizacaoconsumo_codigo_Filteredtext_set = cgiGet( "DDO_AUTORIZACAOCONSUMO_CODIGO_Filteredtext_set");
            Ddo_autorizacaoconsumo_codigo_Filteredtextto_set = cgiGet( "DDO_AUTORIZACAOCONSUMO_CODIGO_Filteredtextto_set");
            Ddo_autorizacaoconsumo_codigo_Dropdownoptionstype = cgiGet( "DDO_AUTORIZACAOCONSUMO_CODIGO_Dropdownoptionstype");
            Ddo_autorizacaoconsumo_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_AUTORIZACAOCONSUMO_CODIGO_Titlecontrolidtoreplace");
            Ddo_autorizacaoconsumo_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_AUTORIZACAOCONSUMO_CODIGO_Includesortasc"));
            Ddo_autorizacaoconsumo_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_AUTORIZACAOCONSUMO_CODIGO_Includesortdsc"));
            Ddo_autorizacaoconsumo_codigo_Sortedstatus = cgiGet( "DDO_AUTORIZACAOCONSUMO_CODIGO_Sortedstatus");
            Ddo_autorizacaoconsumo_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_AUTORIZACAOCONSUMO_CODIGO_Includefilter"));
            Ddo_autorizacaoconsumo_codigo_Filtertype = cgiGet( "DDO_AUTORIZACAOCONSUMO_CODIGO_Filtertype");
            Ddo_autorizacaoconsumo_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_AUTORIZACAOCONSUMO_CODIGO_Filterisrange"));
            Ddo_autorizacaoconsumo_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_AUTORIZACAOCONSUMO_CODIGO_Includedatalist"));
            Ddo_autorizacaoconsumo_codigo_Sortasc = cgiGet( "DDO_AUTORIZACAOCONSUMO_CODIGO_Sortasc");
            Ddo_autorizacaoconsumo_codigo_Sortdsc = cgiGet( "DDO_AUTORIZACAOCONSUMO_CODIGO_Sortdsc");
            Ddo_autorizacaoconsumo_codigo_Cleanfilter = cgiGet( "DDO_AUTORIZACAOCONSUMO_CODIGO_Cleanfilter");
            Ddo_autorizacaoconsumo_codigo_Rangefilterfrom = cgiGet( "DDO_AUTORIZACAOCONSUMO_CODIGO_Rangefilterfrom");
            Ddo_autorizacaoconsumo_codigo_Rangefilterto = cgiGet( "DDO_AUTORIZACAOCONSUMO_CODIGO_Rangefilterto");
            Ddo_autorizacaoconsumo_codigo_Searchbuttontext = cgiGet( "DDO_AUTORIZACAOCONSUMO_CODIGO_Searchbuttontext");
            Ddo_contrato_codigo_Caption = cgiGet( "DDO_CONTRATO_CODIGO_Caption");
            Ddo_contrato_codigo_Tooltip = cgiGet( "DDO_CONTRATO_CODIGO_Tooltip");
            Ddo_contrato_codigo_Cls = cgiGet( "DDO_CONTRATO_CODIGO_Cls");
            Ddo_contrato_codigo_Filteredtext_set = cgiGet( "DDO_CONTRATO_CODIGO_Filteredtext_set");
            Ddo_contrato_codigo_Filteredtextto_set = cgiGet( "DDO_CONTRATO_CODIGO_Filteredtextto_set");
            Ddo_contrato_codigo_Dropdownoptionstype = cgiGet( "DDO_CONTRATO_CODIGO_Dropdownoptionstype");
            Ddo_contrato_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATO_CODIGO_Titlecontrolidtoreplace");
            Ddo_contrato_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_CODIGO_Includesortasc"));
            Ddo_contrato_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_CODIGO_Includesortdsc"));
            Ddo_contrato_codigo_Sortedstatus = cgiGet( "DDO_CONTRATO_CODIGO_Sortedstatus");
            Ddo_contrato_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_CODIGO_Includefilter"));
            Ddo_contrato_codigo_Filtertype = cgiGet( "DDO_CONTRATO_CODIGO_Filtertype");
            Ddo_contrato_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_CODIGO_Filterisrange"));
            Ddo_contrato_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_CODIGO_Includedatalist"));
            Ddo_contrato_codigo_Sortasc = cgiGet( "DDO_CONTRATO_CODIGO_Sortasc");
            Ddo_contrato_codigo_Sortdsc = cgiGet( "DDO_CONTRATO_CODIGO_Sortdsc");
            Ddo_contrato_codigo_Cleanfilter = cgiGet( "DDO_CONTRATO_CODIGO_Cleanfilter");
            Ddo_contrato_codigo_Rangefilterfrom = cgiGet( "DDO_CONTRATO_CODIGO_Rangefilterfrom");
            Ddo_contrato_codigo_Rangefilterto = cgiGet( "DDO_CONTRATO_CODIGO_Rangefilterto");
            Ddo_contrato_codigo_Searchbuttontext = cgiGet( "DDO_CONTRATO_CODIGO_Searchbuttontext");
            Ddo_autorizacaoconsumo_vigenciainicio_Caption = cgiGet( "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Caption");
            Ddo_autorizacaoconsumo_vigenciainicio_Tooltip = cgiGet( "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Tooltip");
            Ddo_autorizacaoconsumo_vigenciainicio_Cls = cgiGet( "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Cls");
            Ddo_autorizacaoconsumo_vigenciainicio_Filteredtext_set = cgiGet( "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Filteredtext_set");
            Ddo_autorizacaoconsumo_vigenciainicio_Filteredtextto_set = cgiGet( "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Filteredtextto_set");
            Ddo_autorizacaoconsumo_vigenciainicio_Dropdownoptionstype = cgiGet( "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Dropdownoptionstype");
            Ddo_autorizacaoconsumo_vigenciainicio_Titlecontrolidtoreplace = cgiGet( "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Titlecontrolidtoreplace");
            Ddo_autorizacaoconsumo_vigenciainicio_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Includesortasc"));
            Ddo_autorizacaoconsumo_vigenciainicio_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Includesortdsc"));
            Ddo_autorizacaoconsumo_vigenciainicio_Sortedstatus = cgiGet( "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Sortedstatus");
            Ddo_autorizacaoconsumo_vigenciainicio_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Includefilter"));
            Ddo_autorizacaoconsumo_vigenciainicio_Filtertype = cgiGet( "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Filtertype");
            Ddo_autorizacaoconsumo_vigenciainicio_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Filterisrange"));
            Ddo_autorizacaoconsumo_vigenciainicio_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Includedatalist"));
            Ddo_autorizacaoconsumo_vigenciainicio_Sortasc = cgiGet( "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Sortasc");
            Ddo_autorizacaoconsumo_vigenciainicio_Sortdsc = cgiGet( "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Sortdsc");
            Ddo_autorizacaoconsumo_vigenciainicio_Cleanfilter = cgiGet( "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Cleanfilter");
            Ddo_autorizacaoconsumo_vigenciainicio_Rangefilterfrom = cgiGet( "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Rangefilterfrom");
            Ddo_autorizacaoconsumo_vigenciainicio_Rangefilterto = cgiGet( "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Rangefilterto");
            Ddo_autorizacaoconsumo_vigenciainicio_Searchbuttontext = cgiGet( "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Searchbuttontext");
            Ddo_autorizacaoconsumo_vigenciafim_Caption = cgiGet( "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Caption");
            Ddo_autorizacaoconsumo_vigenciafim_Tooltip = cgiGet( "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Tooltip");
            Ddo_autorizacaoconsumo_vigenciafim_Cls = cgiGet( "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Cls");
            Ddo_autorizacaoconsumo_vigenciafim_Filteredtext_set = cgiGet( "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Filteredtext_set");
            Ddo_autorizacaoconsumo_vigenciafim_Filteredtextto_set = cgiGet( "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Filteredtextto_set");
            Ddo_autorizacaoconsumo_vigenciafim_Dropdownoptionstype = cgiGet( "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Dropdownoptionstype");
            Ddo_autorizacaoconsumo_vigenciafim_Titlecontrolidtoreplace = cgiGet( "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Titlecontrolidtoreplace");
            Ddo_autorizacaoconsumo_vigenciafim_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Includesortasc"));
            Ddo_autorizacaoconsumo_vigenciafim_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Includesortdsc"));
            Ddo_autorizacaoconsumo_vigenciafim_Sortedstatus = cgiGet( "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Sortedstatus");
            Ddo_autorizacaoconsumo_vigenciafim_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Includefilter"));
            Ddo_autorizacaoconsumo_vigenciafim_Filtertype = cgiGet( "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Filtertype");
            Ddo_autorizacaoconsumo_vigenciafim_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Filterisrange"));
            Ddo_autorizacaoconsumo_vigenciafim_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Includedatalist"));
            Ddo_autorizacaoconsumo_vigenciafim_Sortasc = cgiGet( "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Sortasc");
            Ddo_autorizacaoconsumo_vigenciafim_Sortdsc = cgiGet( "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Sortdsc");
            Ddo_autorizacaoconsumo_vigenciafim_Cleanfilter = cgiGet( "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Cleanfilter");
            Ddo_autorizacaoconsumo_vigenciafim_Rangefilterfrom = cgiGet( "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Rangefilterfrom");
            Ddo_autorizacaoconsumo_vigenciafim_Rangefilterto = cgiGet( "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Rangefilterto");
            Ddo_autorizacaoconsumo_vigenciafim_Searchbuttontext = cgiGet( "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Searchbuttontext");
            Ddo_autorizacaoconsumo_unidademedicaonom_Caption = cgiGet( "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Caption");
            Ddo_autorizacaoconsumo_unidademedicaonom_Tooltip = cgiGet( "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Tooltip");
            Ddo_autorizacaoconsumo_unidademedicaonom_Cls = cgiGet( "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Cls");
            Ddo_autorizacaoconsumo_unidademedicaonom_Filteredtext_set = cgiGet( "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Filteredtext_set");
            Ddo_autorizacaoconsumo_unidademedicaonom_Selectedvalue_set = cgiGet( "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Selectedvalue_set");
            Ddo_autorizacaoconsumo_unidademedicaonom_Dropdownoptionstype = cgiGet( "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Dropdownoptionstype");
            Ddo_autorizacaoconsumo_unidademedicaonom_Titlecontrolidtoreplace = cgiGet( "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Titlecontrolidtoreplace");
            Ddo_autorizacaoconsumo_unidademedicaonom_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Includesortasc"));
            Ddo_autorizacaoconsumo_unidademedicaonom_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Includesortdsc"));
            Ddo_autorizacaoconsumo_unidademedicaonom_Sortedstatus = cgiGet( "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Sortedstatus");
            Ddo_autorizacaoconsumo_unidademedicaonom_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Includefilter"));
            Ddo_autorizacaoconsumo_unidademedicaonom_Filtertype = cgiGet( "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Filtertype");
            Ddo_autorizacaoconsumo_unidademedicaonom_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Filterisrange"));
            Ddo_autorizacaoconsumo_unidademedicaonom_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Includedatalist"));
            Ddo_autorizacaoconsumo_unidademedicaonom_Datalisttype = cgiGet( "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Datalisttype");
            Ddo_autorizacaoconsumo_unidademedicaonom_Datalistproc = cgiGet( "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Datalistproc");
            Ddo_autorizacaoconsumo_unidademedicaonom_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_autorizacaoconsumo_unidademedicaonom_Sortasc = cgiGet( "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Sortasc");
            Ddo_autorizacaoconsumo_unidademedicaonom_Sortdsc = cgiGet( "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Sortdsc");
            Ddo_autorizacaoconsumo_unidademedicaonom_Loadingdata = cgiGet( "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Loadingdata");
            Ddo_autorizacaoconsumo_unidademedicaonom_Cleanfilter = cgiGet( "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Cleanfilter");
            Ddo_autorizacaoconsumo_unidademedicaonom_Noresultsfound = cgiGet( "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Noresultsfound");
            Ddo_autorizacaoconsumo_unidademedicaonom_Searchbuttontext = cgiGet( "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Searchbuttontext");
            Ddo_autorizacaoconsumo_quantidade_Caption = cgiGet( "DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Caption");
            Ddo_autorizacaoconsumo_quantidade_Tooltip = cgiGet( "DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Tooltip");
            Ddo_autorizacaoconsumo_quantidade_Cls = cgiGet( "DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Cls");
            Ddo_autorizacaoconsumo_quantidade_Filteredtext_set = cgiGet( "DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Filteredtext_set");
            Ddo_autorizacaoconsumo_quantidade_Filteredtextto_set = cgiGet( "DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Filteredtextto_set");
            Ddo_autorizacaoconsumo_quantidade_Dropdownoptionstype = cgiGet( "DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Dropdownoptionstype");
            Ddo_autorizacaoconsumo_quantidade_Titlecontrolidtoreplace = cgiGet( "DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Titlecontrolidtoreplace");
            Ddo_autorizacaoconsumo_quantidade_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Includesortasc"));
            Ddo_autorizacaoconsumo_quantidade_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Includesortdsc"));
            Ddo_autorizacaoconsumo_quantidade_Sortedstatus = cgiGet( "DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Sortedstatus");
            Ddo_autorizacaoconsumo_quantidade_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Includefilter"));
            Ddo_autorizacaoconsumo_quantidade_Filtertype = cgiGet( "DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Filtertype");
            Ddo_autorizacaoconsumo_quantidade_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Filterisrange"));
            Ddo_autorizacaoconsumo_quantidade_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Includedatalist"));
            Ddo_autorizacaoconsumo_quantidade_Sortasc = cgiGet( "DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Sortasc");
            Ddo_autorizacaoconsumo_quantidade_Sortdsc = cgiGet( "DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Sortdsc");
            Ddo_autorizacaoconsumo_quantidade_Cleanfilter = cgiGet( "DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Cleanfilter");
            Ddo_autorizacaoconsumo_quantidade_Rangefilterfrom = cgiGet( "DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Rangefilterfrom");
            Ddo_autorizacaoconsumo_quantidade_Rangefilterto = cgiGet( "DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Rangefilterto");
            Ddo_autorizacaoconsumo_quantidade_Searchbuttontext = cgiGet( "DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_autorizacaoconsumo_codigo_Activeeventkey = cgiGet( "DDO_AUTORIZACAOCONSUMO_CODIGO_Activeeventkey");
            Ddo_autorizacaoconsumo_codigo_Filteredtext_get = cgiGet( "DDO_AUTORIZACAOCONSUMO_CODIGO_Filteredtext_get");
            Ddo_autorizacaoconsumo_codigo_Filteredtextto_get = cgiGet( "DDO_AUTORIZACAOCONSUMO_CODIGO_Filteredtextto_get");
            Ddo_contrato_codigo_Activeeventkey = cgiGet( "DDO_CONTRATO_CODIGO_Activeeventkey");
            Ddo_contrato_codigo_Filteredtext_get = cgiGet( "DDO_CONTRATO_CODIGO_Filteredtext_get");
            Ddo_contrato_codigo_Filteredtextto_get = cgiGet( "DDO_CONTRATO_CODIGO_Filteredtextto_get");
            Ddo_autorizacaoconsumo_vigenciainicio_Activeeventkey = cgiGet( "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Activeeventkey");
            Ddo_autorizacaoconsumo_vigenciainicio_Filteredtext_get = cgiGet( "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Filteredtext_get");
            Ddo_autorizacaoconsumo_vigenciainicio_Filteredtextto_get = cgiGet( "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Filteredtextto_get");
            Ddo_autorizacaoconsumo_vigenciafim_Activeeventkey = cgiGet( "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Activeeventkey");
            Ddo_autorizacaoconsumo_vigenciafim_Filteredtext_get = cgiGet( "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Filteredtext_get");
            Ddo_autorizacaoconsumo_vigenciafim_Filteredtextto_get = cgiGet( "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Filteredtextto_get");
            Ddo_autorizacaoconsumo_unidademedicaonom_Activeeventkey = cgiGet( "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Activeeventkey");
            Ddo_autorizacaoconsumo_unidademedicaonom_Filteredtext_get = cgiGet( "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Filteredtext_get");
            Ddo_autorizacaoconsumo_unidademedicaonom_Selectedvalue_get = cgiGet( "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Selectedvalue_get");
            Ddo_autorizacaoconsumo_quantidade_Activeeventkey = cgiGet( "DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Activeeventkey");
            Ddo_autorizacaoconsumo_quantidade_Filteredtext_get = cgiGet( "DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Filteredtext_get");
            Ddo_autorizacaoconsumo_quantidade_Filteredtextto_get = cgiGet( "DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vAUTORIZACAOCONSUMO_VIGENCIAINICIO1"), 0) != AV16AutorizacaoConsumo_VigenciaInicio1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO1"), 0) != AV17AutorizacaoConsumo_VigenciaInicio_To1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vAUTORIZACAOCONSUMO_VIGENCIAINICIO2"), 0) != AV20AutorizacaoConsumo_VigenciaInicio2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO2"), 0) != AV21AutorizacaoConsumo_VigenciaInicio_To2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vAUTORIZACAOCONSUMO_VIGENCIAINICIO3"), 0) != AV24AutorizacaoConsumo_VigenciaInicio3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO3"), 0) != AV25AutorizacaoConsumo_VigenciaInicio_To3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFAUTORIZACAOCONSUMO_CODIGO"), ",", ".") != Convert.ToDecimal( AV34TFAutorizacaoConsumo_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFAUTORIZACAOCONSUMO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV35TFAutorizacaoConsumo_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATO_CODIGO"), ",", ".") != Convert.ToDecimal( AV38TFContrato_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV39TFContrato_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO"), 0) != AV42TFAutorizacaoConsumo_VigenciaInicio )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO"), 0) != AV43TFAutorizacaoConsumo_VigenciaInicio_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFAUTORIZACAOCONSUMO_VIGENCIAFIM"), 0) != AV48TFAutorizacaoConsumo_VigenciaFim )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO"), 0) != AV49TFAutorizacaoConsumo_VigenciaFim_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM"), AV54TFAutorizacaoConsumo_UnidadeMedicaoNom) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL"), AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFAUTORIZACAOCONSUMO_QUANTIDADE"), ",", ".") != Convert.ToDecimal( AV58TFAutorizacaoConsumo_Quantidade )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO"), ",", ".") != Convert.ToDecimal( AV59TFAutorizacaoConsumo_Quantidade_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E29NA2 */
         E29NA2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E29NA2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "AUTORIZACAOCONSUMO_VIGENCIAINICIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "AUTORIZACAOCONSUMO_VIGENCIAINICIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector3 = "AUTORIZACAOCONSUMO_VIGENCIAINICIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfautorizacaoconsumo_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfautorizacaoconsumo_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfautorizacaoconsumo_codigo_Visible), 5, 0)));
         edtavTfautorizacaoconsumo_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfautorizacaoconsumo_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfautorizacaoconsumo_codigo_to_Visible), 5, 0)));
         edtavTfcontrato_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontrato_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontrato_codigo_Visible), 5, 0)));
         edtavTfcontrato_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontrato_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontrato_codigo_to_Visible), 5, 0)));
         edtavTfautorizacaoconsumo_vigenciainicio_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfautorizacaoconsumo_vigenciainicio_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfautorizacaoconsumo_vigenciainicio_Visible), 5, 0)));
         edtavTfautorizacaoconsumo_vigenciainicio_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfautorizacaoconsumo_vigenciainicio_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfautorizacaoconsumo_vigenciainicio_to_Visible), 5, 0)));
         edtavTfautorizacaoconsumo_vigenciafim_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfautorizacaoconsumo_vigenciafim_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfautorizacaoconsumo_vigenciafim_Visible), 5, 0)));
         edtavTfautorizacaoconsumo_vigenciafim_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfautorizacaoconsumo_vigenciafim_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfautorizacaoconsumo_vigenciafim_to_Visible), 5, 0)));
         edtavTfautorizacaoconsumo_unidademedicaonom_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfautorizacaoconsumo_unidademedicaonom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfautorizacaoconsumo_unidademedicaonom_Visible), 5, 0)));
         edtavTfautorizacaoconsumo_unidademedicaonom_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfautorizacaoconsumo_unidademedicaonom_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfautorizacaoconsumo_unidademedicaonom_sel_Visible), 5, 0)));
         edtavTfautorizacaoconsumo_quantidade_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfautorizacaoconsumo_quantidade_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfautorizacaoconsumo_quantidade_Visible), 5, 0)));
         edtavTfautorizacaoconsumo_quantidade_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfautorizacaoconsumo_quantidade_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfautorizacaoconsumo_quantidade_to_Visible), 5, 0)));
         Ddo_autorizacaoconsumo_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_AutorizacaoConsumo_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_autorizacaoconsumo_codigo_Internalname, "TitleControlIdToReplace", Ddo_autorizacaoconsumo_codigo_Titlecontrolidtoreplace);
         AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace = Ddo_autorizacaoconsumo_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace", AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace);
         edtavDdo_autorizacaoconsumo_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_autorizacaoconsumo_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_autorizacaoconsumo_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contrato_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_Contrato_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_codigo_Internalname, "TitleControlIdToReplace", Ddo_contrato_codigo_Titlecontrolidtoreplace);
         AV40ddo_Contrato_CodigoTitleControlIdToReplace = Ddo_contrato_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ddo_Contrato_CodigoTitleControlIdToReplace", AV40ddo_Contrato_CodigoTitleControlIdToReplace);
         edtavDdo_contrato_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contrato_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contrato_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_autorizacaoconsumo_vigenciainicio_Titlecontrolidtoreplace = subGrid_Internalname+"_AutorizacaoConsumo_VigenciaInicio";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_autorizacaoconsumo_vigenciainicio_Internalname, "TitleControlIdToReplace", Ddo_autorizacaoconsumo_vigenciainicio_Titlecontrolidtoreplace);
         AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace = Ddo_autorizacaoconsumo_vigenciainicio_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace", AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace);
         edtavDdo_autorizacaoconsumo_vigenciainiciotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_autorizacaoconsumo_vigenciainiciotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_autorizacaoconsumo_vigenciainiciotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_autorizacaoconsumo_vigenciafim_Titlecontrolidtoreplace = subGrid_Internalname+"_AutorizacaoConsumo_VigenciaFim";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_autorizacaoconsumo_vigenciafim_Internalname, "TitleControlIdToReplace", Ddo_autorizacaoconsumo_vigenciafim_Titlecontrolidtoreplace);
         AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace = Ddo_autorizacaoconsumo_vigenciafim_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace", AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace);
         edtavDdo_autorizacaoconsumo_vigenciafimtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_autorizacaoconsumo_vigenciafimtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_autorizacaoconsumo_vigenciafimtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_autorizacaoconsumo_unidademedicaonom_Titlecontrolidtoreplace = subGrid_Internalname+"_AutorizacaoConsumo_UnidadeMedicaoNom";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_autorizacaoconsumo_unidademedicaonom_Internalname, "TitleControlIdToReplace", Ddo_autorizacaoconsumo_unidademedicaonom_Titlecontrolidtoreplace);
         AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace = Ddo_autorizacaoconsumo_unidademedicaonom_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace", AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace);
         edtavDdo_autorizacaoconsumo_unidademedicaonomtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_autorizacaoconsumo_unidademedicaonomtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_autorizacaoconsumo_unidademedicaonomtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_autorizacaoconsumo_quantidade_Titlecontrolidtoreplace = subGrid_Internalname+"_AutorizacaoConsumo_Quantidade";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_autorizacaoconsumo_quantidade_Internalname, "TitleControlIdToReplace", Ddo_autorizacaoconsumo_quantidade_Titlecontrolidtoreplace);
         AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace = Ddo_autorizacaoconsumo_quantidade_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace", AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace);
         edtavDdo_autorizacaoconsumo_quantidadetitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_autorizacaoconsumo_quantidadetitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_autorizacaoconsumo_quantidadetitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Autoriza��o de Consumo";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Consumo_Vigencia Inicio", 0);
         cmbavOrderedby.addItem("2", "C�digo", 0);
         cmbavOrderedby.addItem("3", "Contrato", 0);
         cmbavOrderedby.addItem("4", "D. Vigencia T�rmino", 0);
         cmbavOrderedby.addItem("5", "Unidade Medi��o", 0);
         cmbavOrderedby.addItem("6", "Quantidade", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV61DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV61DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E30NA2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV33AutorizacaoConsumo_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV37Contrato_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV41AutorizacaoConsumo_VigenciaInicioTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV47AutorizacaoConsumo_VigenciaFimTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV53AutorizacaoConsumo_UnidadeMedicaoNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV57AutorizacaoConsumo_QuantidadeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtAutorizacaoConsumo_Codigo_Titleformat = 2;
         edtAutorizacaoConsumo_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "C�digo", AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAutorizacaoConsumo_Codigo_Internalname, "Title", edtAutorizacaoConsumo_Codigo_Title);
         edtContrato_Codigo_Titleformat = 2;
         edtContrato_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Contrato", AV40ddo_Contrato_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Codigo_Internalname, "Title", edtContrato_Codigo_Title);
         edtAutorizacaoConsumo_VigenciaInicio_Titleformat = 2;
         edtAutorizacaoConsumo_VigenciaInicio_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "D. Vig�ncia Inicio", AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAutorizacaoConsumo_VigenciaInicio_Internalname, "Title", edtAutorizacaoConsumo_VigenciaInicio_Title);
         edtAutorizacaoConsumo_VigenciaFim_Titleformat = 2;
         edtAutorizacaoConsumo_VigenciaFim_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "D. Vigencia T�rmino", AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAutorizacaoConsumo_VigenciaFim_Internalname, "Title", edtAutorizacaoConsumo_VigenciaFim_Title);
         edtAutorizacaoConsumo_UnidadeMedicaoNom_Titleformat = 2;
         edtAutorizacaoConsumo_UnidadeMedicaoNom_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Unidade Medi��o", AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAutorizacaoConsumo_UnidadeMedicaoNom_Internalname, "Title", edtAutorizacaoConsumo_UnidadeMedicaoNom_Title);
         edtAutorizacaoConsumo_Quantidade_Titleformat = 2;
         edtAutorizacaoConsumo_Quantidade_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Quantidade", AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAutorizacaoConsumo_Quantidade_Internalname, "Title", edtAutorizacaoConsumo_Quantidade_Title);
         AV63GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV63GridCurrentPage), 10, 0)));
         AV64GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV64GridPageCount), 10, 0)));
         AV67WWAutorizacaoConsumoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV68WWAutorizacaoConsumoDS_2_Autorizacaoconsumo_vigenciainicio1 = AV16AutorizacaoConsumo_VigenciaInicio1;
         AV69WWAutorizacaoConsumoDS_3_Autorizacaoconsumo_vigenciainicio_to1 = AV17AutorizacaoConsumo_VigenciaInicio_To1;
         AV70WWAutorizacaoConsumoDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV71WWAutorizacaoConsumoDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV72WWAutorizacaoConsumoDS_6_Autorizacaoconsumo_vigenciainicio2 = AV20AutorizacaoConsumo_VigenciaInicio2;
         AV73WWAutorizacaoConsumoDS_7_Autorizacaoconsumo_vigenciainicio_to2 = AV21AutorizacaoConsumo_VigenciaInicio_To2;
         AV74WWAutorizacaoConsumoDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV75WWAutorizacaoConsumoDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV76WWAutorizacaoConsumoDS_10_Autorizacaoconsumo_vigenciainicio3 = AV24AutorizacaoConsumo_VigenciaInicio3;
         AV77WWAutorizacaoConsumoDS_11_Autorizacaoconsumo_vigenciainicio_to3 = AV25AutorizacaoConsumo_VigenciaInicio_To3;
         AV78WWAutorizacaoConsumoDS_12_Tfautorizacaoconsumo_codigo = AV34TFAutorizacaoConsumo_Codigo;
         AV79WWAutorizacaoConsumoDS_13_Tfautorizacaoconsumo_codigo_to = AV35TFAutorizacaoConsumo_Codigo_To;
         AV80WWAutorizacaoConsumoDS_14_Tfcontrato_codigo = AV38TFContrato_Codigo;
         AV81WWAutorizacaoConsumoDS_15_Tfcontrato_codigo_to = AV39TFContrato_Codigo_To;
         AV82WWAutorizacaoConsumoDS_16_Tfautorizacaoconsumo_vigenciainicio = AV42TFAutorizacaoConsumo_VigenciaInicio;
         AV83WWAutorizacaoConsumoDS_17_Tfautorizacaoconsumo_vigenciainicio_to = AV43TFAutorizacaoConsumo_VigenciaInicio_To;
         AV84WWAutorizacaoConsumoDS_18_Tfautorizacaoconsumo_vigenciafim = AV48TFAutorizacaoConsumo_VigenciaFim;
         AV85WWAutorizacaoConsumoDS_19_Tfautorizacaoconsumo_vigenciafim_to = AV49TFAutorizacaoConsumo_VigenciaFim_To;
         AV86WWAutorizacaoConsumoDS_20_Tfautorizacaoconsumo_unidademedicaonom = AV54TFAutorizacaoConsumo_UnidadeMedicaoNom;
         AV87WWAutorizacaoConsumoDS_21_Tfautorizacaoconsumo_unidademedicaonom_sel = AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel;
         AV88WWAutorizacaoConsumoDS_22_Tfautorizacaoconsumo_quantidade = AV58TFAutorizacaoConsumo_Quantidade;
         AV89WWAutorizacaoConsumoDS_23_Tfautorizacaoconsumo_quantidade_to = AV59TFAutorizacaoConsumo_Quantidade_To;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV33AutorizacaoConsumo_CodigoTitleFilterData", AV33AutorizacaoConsumo_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV37Contrato_CodigoTitleFilterData", AV37Contrato_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV41AutorizacaoConsumo_VigenciaInicioTitleFilterData", AV41AutorizacaoConsumo_VigenciaInicioTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV47AutorizacaoConsumo_VigenciaFimTitleFilterData", AV47AutorizacaoConsumo_VigenciaFimTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV53AutorizacaoConsumo_UnidadeMedicaoNomTitleFilterData", AV53AutorizacaoConsumo_UnidadeMedicaoNomTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV57AutorizacaoConsumo_QuantidadeTitleFilterData", AV57AutorizacaoConsumo_QuantidadeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11NA2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV62PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV62PageToGo) ;
         }
      }

      protected void E12NA2( )
      {
         /* Ddo_autorizacaoconsumo_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_autorizacaoconsumo_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_autorizacaoconsumo_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_autorizacaoconsumo_codigo_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_autorizacaoconsumo_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_autorizacaoconsumo_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_autorizacaoconsumo_codigo_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_autorizacaoconsumo_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV34TFAutorizacaoConsumo_Codigo = (int)(NumberUtil.Val( Ddo_autorizacaoconsumo_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFAutorizacaoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34TFAutorizacaoConsumo_Codigo), 6, 0)));
            AV35TFAutorizacaoConsumo_Codigo_To = (int)(NumberUtil.Val( Ddo_autorizacaoconsumo_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFAutorizacaoConsumo_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFAutorizacaoConsumo_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13NA2( )
      {
         /* Ddo_contrato_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contrato_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contrato_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_codigo_Internalname, "SortedStatus", Ddo_contrato_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contrato_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contrato_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_codigo_Internalname, "SortedStatus", Ddo_contrato_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contrato_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV38TFContrato_Codigo = (int)(NumberUtil.Val( Ddo_contrato_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFContrato_Codigo), 6, 0)));
            AV39TFContrato_Codigo_To = (int)(NumberUtil.Val( Ddo_contrato_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFContrato_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14NA2( )
      {
         /* Ddo_autorizacaoconsumo_vigenciainicio_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_autorizacaoconsumo_vigenciainicio_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_autorizacaoconsumo_vigenciainicio_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_autorizacaoconsumo_vigenciainicio_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_vigenciainicio_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_autorizacaoconsumo_vigenciainicio_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_autorizacaoconsumo_vigenciainicio_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_autorizacaoconsumo_vigenciainicio_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_vigenciainicio_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_autorizacaoconsumo_vigenciainicio_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV42TFAutorizacaoConsumo_VigenciaInicio = context.localUtil.CToD( Ddo_autorizacaoconsumo_vigenciainicio_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFAutorizacaoConsumo_VigenciaInicio", context.localUtil.Format(AV42TFAutorizacaoConsumo_VigenciaInicio, "99/99/99"));
            AV43TFAutorizacaoConsumo_VigenciaInicio_To = context.localUtil.CToD( Ddo_autorizacaoconsumo_vigenciainicio_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFAutorizacaoConsumo_VigenciaInicio_To", context.localUtil.Format(AV43TFAutorizacaoConsumo_VigenciaInicio_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E15NA2( )
      {
         /* Ddo_autorizacaoconsumo_vigenciafim_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_autorizacaoconsumo_vigenciafim_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_autorizacaoconsumo_vigenciafim_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_autorizacaoconsumo_vigenciafim_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_vigenciafim_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_autorizacaoconsumo_vigenciafim_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_autorizacaoconsumo_vigenciafim_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_autorizacaoconsumo_vigenciafim_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_vigenciafim_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_autorizacaoconsumo_vigenciafim_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV48TFAutorizacaoConsumo_VigenciaFim = context.localUtil.CToD( Ddo_autorizacaoconsumo_vigenciafim_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFAutorizacaoConsumo_VigenciaFim", context.localUtil.Format(AV48TFAutorizacaoConsumo_VigenciaFim, "99/99/99"));
            AV49TFAutorizacaoConsumo_VigenciaFim_To = context.localUtil.CToD( Ddo_autorizacaoconsumo_vigenciafim_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFAutorizacaoConsumo_VigenciaFim_To", context.localUtil.Format(AV49TFAutorizacaoConsumo_VigenciaFim_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E16NA2( )
      {
         /* Ddo_autorizacaoconsumo_unidademedicaonom_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_autorizacaoconsumo_unidademedicaonom_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_autorizacaoconsumo_unidademedicaonom_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_autorizacaoconsumo_unidademedicaonom_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_unidademedicaonom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_autorizacaoconsumo_unidademedicaonom_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_autorizacaoconsumo_unidademedicaonom_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_autorizacaoconsumo_unidademedicaonom_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_unidademedicaonom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_autorizacaoconsumo_unidademedicaonom_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV54TFAutorizacaoConsumo_UnidadeMedicaoNom = Ddo_autorizacaoconsumo_unidademedicaonom_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFAutorizacaoConsumo_UnidadeMedicaoNom", AV54TFAutorizacaoConsumo_UnidadeMedicaoNom);
            AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel = Ddo_autorizacaoconsumo_unidademedicaonom_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel", AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E17NA2( )
      {
         /* Ddo_autorizacaoconsumo_quantidade_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_autorizacaoconsumo_quantidade_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_autorizacaoconsumo_quantidade_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_autorizacaoconsumo_quantidade_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_quantidade_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_autorizacaoconsumo_quantidade_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_autorizacaoconsumo_quantidade_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_autorizacaoconsumo_quantidade_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_quantidade_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_autorizacaoconsumo_quantidade_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV58TFAutorizacaoConsumo_Quantidade = (short)(NumberUtil.Val( Ddo_autorizacaoconsumo_quantidade_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFAutorizacaoConsumo_Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58TFAutorizacaoConsumo_Quantidade), 3, 0)));
            AV59TFAutorizacaoConsumo_Quantidade_To = (short)(NumberUtil.Val( Ddo_autorizacaoconsumo_quantidade_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFAutorizacaoConsumo_Quantidade_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59TFAutorizacaoConsumo_Quantidade_To), 3, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E31NA2( )
      {
         /* Grid_Load Routine */
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 94;
         }
         sendrow_942( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_94_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(94, GridRow);
         }
      }

      protected void E18NA2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E24NA2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E19NA2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16AutorizacaoConsumo_VigenciaInicio1, AV17AutorizacaoConsumo_VigenciaInicio_To1, AV19DynamicFiltersSelector2, AV20AutorizacaoConsumo_VigenciaInicio2, AV21AutorizacaoConsumo_VigenciaInicio_To2, AV23DynamicFiltersSelector3, AV24AutorizacaoConsumo_VigenciaInicio3, AV25AutorizacaoConsumo_VigenciaInicio_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFAutorizacaoConsumo_Codigo, AV35TFAutorizacaoConsumo_Codigo_To, AV38TFContrato_Codigo, AV39TFContrato_Codigo_To, AV42TFAutorizacaoConsumo_VigenciaInicio, AV43TFAutorizacaoConsumo_VigenciaInicio_To, AV48TFAutorizacaoConsumo_VigenciaFim, AV49TFAutorizacaoConsumo_VigenciaFim_To, AV54TFAutorizacaoConsumo_UnidadeMedicaoNom, AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel, AV58TFAutorizacaoConsumo_Quantidade, AV59TFAutorizacaoConsumo_Quantidade_To, AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace, AV40ddo_Contrato_CodigoTitleControlIdToReplace, AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace, AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace, AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace, AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace, AV90Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E25NA2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E26NA2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E20NA2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16AutorizacaoConsumo_VigenciaInicio1, AV17AutorizacaoConsumo_VigenciaInicio_To1, AV19DynamicFiltersSelector2, AV20AutorizacaoConsumo_VigenciaInicio2, AV21AutorizacaoConsumo_VigenciaInicio_To2, AV23DynamicFiltersSelector3, AV24AutorizacaoConsumo_VigenciaInicio3, AV25AutorizacaoConsumo_VigenciaInicio_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFAutorizacaoConsumo_Codigo, AV35TFAutorizacaoConsumo_Codigo_To, AV38TFContrato_Codigo, AV39TFContrato_Codigo_To, AV42TFAutorizacaoConsumo_VigenciaInicio, AV43TFAutorizacaoConsumo_VigenciaInicio_To, AV48TFAutorizacaoConsumo_VigenciaFim, AV49TFAutorizacaoConsumo_VigenciaFim_To, AV54TFAutorizacaoConsumo_UnidadeMedicaoNom, AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel, AV58TFAutorizacaoConsumo_Quantidade, AV59TFAutorizacaoConsumo_Quantidade_To, AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace, AV40ddo_Contrato_CodigoTitleControlIdToReplace, AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace, AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace, AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace, AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace, AV90Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E27NA2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E21NA2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16AutorizacaoConsumo_VigenciaInicio1, AV17AutorizacaoConsumo_VigenciaInicio_To1, AV19DynamicFiltersSelector2, AV20AutorizacaoConsumo_VigenciaInicio2, AV21AutorizacaoConsumo_VigenciaInicio_To2, AV23DynamicFiltersSelector3, AV24AutorizacaoConsumo_VigenciaInicio3, AV25AutorizacaoConsumo_VigenciaInicio_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV34TFAutorizacaoConsumo_Codigo, AV35TFAutorizacaoConsumo_Codigo_To, AV38TFContrato_Codigo, AV39TFContrato_Codigo_To, AV42TFAutorizacaoConsumo_VigenciaInicio, AV43TFAutorizacaoConsumo_VigenciaInicio_To, AV48TFAutorizacaoConsumo_VigenciaFim, AV49TFAutorizacaoConsumo_VigenciaFim_To, AV54TFAutorizacaoConsumo_UnidadeMedicaoNom, AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel, AV58TFAutorizacaoConsumo_Quantidade, AV59TFAutorizacaoConsumo_Quantidade_To, AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace, AV40ddo_Contrato_CodigoTitleControlIdToReplace, AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace, AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace, AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace, AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace, AV90Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E28NA2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E22NA2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
      }

      protected void E23NA2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("autorizacaoconsumo.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S182( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_autorizacaoconsumo_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_autorizacaoconsumo_codigo_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_codigo_Sortedstatus);
         Ddo_contrato_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_codigo_Internalname, "SortedStatus", Ddo_contrato_codigo_Sortedstatus);
         Ddo_autorizacaoconsumo_vigenciainicio_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_autorizacaoconsumo_vigenciainicio_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_vigenciainicio_Sortedstatus);
         Ddo_autorizacaoconsumo_vigenciafim_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_autorizacaoconsumo_vigenciafim_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_vigenciafim_Sortedstatus);
         Ddo_autorizacaoconsumo_unidademedicaonom_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_autorizacaoconsumo_unidademedicaonom_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_unidademedicaonom_Sortedstatus);
         Ddo_autorizacaoconsumo_quantidade_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_autorizacaoconsumo_quantidade_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_quantidade_Sortedstatus);
      }

      protected void S162( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_autorizacaoconsumo_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_autorizacaoconsumo_codigo_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_contrato_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_codigo_Internalname, "SortedStatus", Ddo_contrato_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 1 )
         {
            Ddo_autorizacaoconsumo_vigenciainicio_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_autorizacaoconsumo_vigenciainicio_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_vigenciainicio_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_autorizacaoconsumo_vigenciafim_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_autorizacaoconsumo_vigenciafim_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_vigenciafim_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_autorizacaoconsumo_unidademedicaonom_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_autorizacaoconsumo_unidademedicaonom_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_unidademedicaonom_Sortedstatus);
         }
         else if ( AV13OrderedBy == 6 )
         {
            Ddo_autorizacaoconsumo_quantidade_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_autorizacaoconsumo_quantidade_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_quantidade_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "AUTORIZACAOCONSUMO_VIGENCIAINICIO") == 0 )
         {
            tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio2_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "AUTORIZACAOCONSUMO_VIGENCIAINICIO") == 0 )
         {
            tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio2_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio3_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "AUTORIZACAOCONSUMO_VIGENCIAINICIO") == 0 )
         {
            tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio3_Visible), 5, 0)));
         }
      }

      protected void S202( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "AUTORIZACAOCONSUMO_VIGENCIAINICIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV20AutorizacaoConsumo_VigenciaInicio2 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20AutorizacaoConsumo_VigenciaInicio2", context.localUtil.Format(AV20AutorizacaoConsumo_VigenciaInicio2, "99/99/99"));
         AV21AutorizacaoConsumo_VigenciaInicio_To2 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21AutorizacaoConsumo_VigenciaInicio_To2", context.localUtil.Format(AV21AutorizacaoConsumo_VigenciaInicio_To2, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "AUTORIZACAOCONSUMO_VIGENCIAINICIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV24AutorizacaoConsumo_VigenciaInicio3 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24AutorizacaoConsumo_VigenciaInicio3", context.localUtil.Format(AV24AutorizacaoConsumo_VigenciaInicio3, "99/99/99"));
         AV25AutorizacaoConsumo_VigenciaInicio_To3 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25AutorizacaoConsumo_VigenciaInicio_To3", context.localUtil.Format(AV25AutorizacaoConsumo_VigenciaInicio_To3, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S222( )
      {
         /* 'CLEANFILTERS' Routine */
         AV34TFAutorizacaoConsumo_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFAutorizacaoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34TFAutorizacaoConsumo_Codigo), 6, 0)));
         Ddo_autorizacaoconsumo_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_autorizacaoconsumo_codigo_Internalname, "FilteredText_set", Ddo_autorizacaoconsumo_codigo_Filteredtext_set);
         AV35TFAutorizacaoConsumo_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFAutorizacaoConsumo_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFAutorizacaoConsumo_Codigo_To), 6, 0)));
         Ddo_autorizacaoconsumo_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_autorizacaoconsumo_codigo_Internalname, "FilteredTextTo_set", Ddo_autorizacaoconsumo_codigo_Filteredtextto_set);
         AV38TFContrato_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFContrato_Codigo), 6, 0)));
         Ddo_contrato_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_codigo_Internalname, "FilteredText_set", Ddo_contrato_codigo_Filteredtext_set);
         AV39TFContrato_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFContrato_Codigo_To), 6, 0)));
         Ddo_contrato_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_codigo_Internalname, "FilteredTextTo_set", Ddo_contrato_codigo_Filteredtextto_set);
         AV42TFAutorizacaoConsumo_VigenciaInicio = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFAutorizacaoConsumo_VigenciaInicio", context.localUtil.Format(AV42TFAutorizacaoConsumo_VigenciaInicio, "99/99/99"));
         Ddo_autorizacaoconsumo_vigenciainicio_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_autorizacaoconsumo_vigenciainicio_Internalname, "FilteredText_set", Ddo_autorizacaoconsumo_vigenciainicio_Filteredtext_set);
         AV43TFAutorizacaoConsumo_VigenciaInicio_To = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFAutorizacaoConsumo_VigenciaInicio_To", context.localUtil.Format(AV43TFAutorizacaoConsumo_VigenciaInicio_To, "99/99/99"));
         Ddo_autorizacaoconsumo_vigenciainicio_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_autorizacaoconsumo_vigenciainicio_Internalname, "FilteredTextTo_set", Ddo_autorizacaoconsumo_vigenciainicio_Filteredtextto_set);
         AV48TFAutorizacaoConsumo_VigenciaFim = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFAutorizacaoConsumo_VigenciaFim", context.localUtil.Format(AV48TFAutorizacaoConsumo_VigenciaFim, "99/99/99"));
         Ddo_autorizacaoconsumo_vigenciafim_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_autorizacaoconsumo_vigenciafim_Internalname, "FilteredText_set", Ddo_autorizacaoconsumo_vigenciafim_Filteredtext_set);
         AV49TFAutorizacaoConsumo_VigenciaFim_To = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFAutorizacaoConsumo_VigenciaFim_To", context.localUtil.Format(AV49TFAutorizacaoConsumo_VigenciaFim_To, "99/99/99"));
         Ddo_autorizacaoconsumo_vigenciafim_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_autorizacaoconsumo_vigenciafim_Internalname, "FilteredTextTo_set", Ddo_autorizacaoconsumo_vigenciafim_Filteredtextto_set);
         AV54TFAutorizacaoConsumo_UnidadeMedicaoNom = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFAutorizacaoConsumo_UnidadeMedicaoNom", AV54TFAutorizacaoConsumo_UnidadeMedicaoNom);
         Ddo_autorizacaoconsumo_unidademedicaonom_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_autorizacaoconsumo_unidademedicaonom_Internalname, "FilteredText_set", Ddo_autorizacaoconsumo_unidademedicaonom_Filteredtext_set);
         AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel", AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel);
         Ddo_autorizacaoconsumo_unidademedicaonom_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_autorizacaoconsumo_unidademedicaonom_Internalname, "SelectedValue_set", Ddo_autorizacaoconsumo_unidademedicaonom_Selectedvalue_set);
         AV58TFAutorizacaoConsumo_Quantidade = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFAutorizacaoConsumo_Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58TFAutorizacaoConsumo_Quantidade), 3, 0)));
         Ddo_autorizacaoconsumo_quantidade_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_autorizacaoconsumo_quantidade_Internalname, "FilteredText_set", Ddo_autorizacaoconsumo_quantidade_Filteredtext_set);
         AV59TFAutorizacaoConsumo_Quantidade_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFAutorizacaoConsumo_Quantidade_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59TFAutorizacaoConsumo_Quantidade_To), 3, 0)));
         Ddo_autorizacaoconsumo_quantidade_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_autorizacaoconsumo_quantidade_Internalname, "FilteredTextTo_set", Ddo_autorizacaoconsumo_quantidade_Filteredtextto_set);
         AV15DynamicFiltersSelector1 = "AUTORIZACAOCONSUMO_VIGENCIAINICIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16AutorizacaoConsumo_VigenciaInicio1 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16AutorizacaoConsumo_VigenciaInicio1", context.localUtil.Format(AV16AutorizacaoConsumo_VigenciaInicio1, "99/99/99"));
         AV17AutorizacaoConsumo_VigenciaInicio_To1 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17AutorizacaoConsumo_VigenciaInicio_To1", context.localUtil.Format(AV17AutorizacaoConsumo_VigenciaInicio_To1, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV30Session.Get(AV90Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV90Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV30Session.Get(AV90Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S232( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV91GXV1 = 1;
         while ( AV91GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV91GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFAUTORIZACAOCONSUMO_CODIGO") == 0 )
            {
               AV34TFAutorizacaoConsumo_Codigo = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TFAutorizacaoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34TFAutorizacaoConsumo_Codigo), 6, 0)));
               AV35TFAutorizacaoConsumo_Codigo_To = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFAutorizacaoConsumo_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFAutorizacaoConsumo_Codigo_To), 6, 0)));
               if ( ! (0==AV34TFAutorizacaoConsumo_Codigo) )
               {
                  Ddo_autorizacaoconsumo_codigo_Filteredtext_set = StringUtil.Str( (decimal)(AV34TFAutorizacaoConsumo_Codigo), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_autorizacaoconsumo_codigo_Internalname, "FilteredText_set", Ddo_autorizacaoconsumo_codigo_Filteredtext_set);
               }
               if ( ! (0==AV35TFAutorizacaoConsumo_Codigo_To) )
               {
                  Ddo_autorizacaoconsumo_codigo_Filteredtextto_set = StringUtil.Str( (decimal)(AV35TFAutorizacaoConsumo_Codigo_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_autorizacaoconsumo_codigo_Internalname, "FilteredTextTo_set", Ddo_autorizacaoconsumo_codigo_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATO_CODIGO") == 0 )
            {
               AV38TFContrato_Codigo = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38TFContrato_Codigo), 6, 0)));
               AV39TFContrato_Codigo_To = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContrato_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFContrato_Codigo_To), 6, 0)));
               if ( ! (0==AV38TFContrato_Codigo) )
               {
                  Ddo_contrato_codigo_Filteredtext_set = StringUtil.Str( (decimal)(AV38TFContrato_Codigo), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_codigo_Internalname, "FilteredText_set", Ddo_contrato_codigo_Filteredtext_set);
               }
               if ( ! (0==AV39TFContrato_Codigo_To) )
               {
                  Ddo_contrato_codigo_Filteredtextto_set = StringUtil.Str( (decimal)(AV39TFContrato_Codigo_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_codigo_Internalname, "FilteredTextTo_set", Ddo_contrato_codigo_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFAUTORIZACAOCONSUMO_VIGENCIAINICIO") == 0 )
            {
               AV42TFAutorizacaoConsumo_VigenciaInicio = context.localUtil.CToD( AV11GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFAutorizacaoConsumo_VigenciaInicio", context.localUtil.Format(AV42TFAutorizacaoConsumo_VigenciaInicio, "99/99/99"));
               AV43TFAutorizacaoConsumo_VigenciaInicio_To = context.localUtil.CToD( AV11GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFAutorizacaoConsumo_VigenciaInicio_To", context.localUtil.Format(AV43TFAutorizacaoConsumo_VigenciaInicio_To, "99/99/99"));
               if ( ! (DateTime.MinValue==AV42TFAutorizacaoConsumo_VigenciaInicio) )
               {
                  Ddo_autorizacaoconsumo_vigenciainicio_Filteredtext_set = context.localUtil.DToC( AV42TFAutorizacaoConsumo_VigenciaInicio, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_autorizacaoconsumo_vigenciainicio_Internalname, "FilteredText_set", Ddo_autorizacaoconsumo_vigenciainicio_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV43TFAutorizacaoConsumo_VigenciaInicio_To) )
               {
                  Ddo_autorizacaoconsumo_vigenciainicio_Filteredtextto_set = context.localUtil.DToC( AV43TFAutorizacaoConsumo_VigenciaInicio_To, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_autorizacaoconsumo_vigenciainicio_Internalname, "FilteredTextTo_set", Ddo_autorizacaoconsumo_vigenciainicio_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFAUTORIZACAOCONSUMO_VIGENCIAFIM") == 0 )
            {
               AV48TFAutorizacaoConsumo_VigenciaFim = context.localUtil.CToD( AV11GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFAutorizacaoConsumo_VigenciaFim", context.localUtil.Format(AV48TFAutorizacaoConsumo_VigenciaFim, "99/99/99"));
               AV49TFAutorizacaoConsumo_VigenciaFim_To = context.localUtil.CToD( AV11GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFAutorizacaoConsumo_VigenciaFim_To", context.localUtil.Format(AV49TFAutorizacaoConsumo_VigenciaFim_To, "99/99/99"));
               if ( ! (DateTime.MinValue==AV48TFAutorizacaoConsumo_VigenciaFim) )
               {
                  Ddo_autorizacaoconsumo_vigenciafim_Filteredtext_set = context.localUtil.DToC( AV48TFAutorizacaoConsumo_VigenciaFim, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_autorizacaoconsumo_vigenciafim_Internalname, "FilteredText_set", Ddo_autorizacaoconsumo_vigenciafim_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV49TFAutorizacaoConsumo_VigenciaFim_To) )
               {
                  Ddo_autorizacaoconsumo_vigenciafim_Filteredtextto_set = context.localUtil.DToC( AV49TFAutorizacaoConsumo_VigenciaFim_To, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_autorizacaoconsumo_vigenciafim_Internalname, "FilteredTextTo_set", Ddo_autorizacaoconsumo_vigenciafim_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM") == 0 )
            {
               AV54TFAutorizacaoConsumo_UnidadeMedicaoNom = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFAutorizacaoConsumo_UnidadeMedicaoNom", AV54TFAutorizacaoConsumo_UnidadeMedicaoNom);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54TFAutorizacaoConsumo_UnidadeMedicaoNom)) )
               {
                  Ddo_autorizacaoconsumo_unidademedicaonom_Filteredtext_set = AV54TFAutorizacaoConsumo_UnidadeMedicaoNom;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_autorizacaoconsumo_unidademedicaonom_Internalname, "FilteredText_set", Ddo_autorizacaoconsumo_unidademedicaonom_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL") == 0 )
            {
               AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel", AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel)) )
               {
                  Ddo_autorizacaoconsumo_unidademedicaonom_Selectedvalue_set = AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_autorizacaoconsumo_unidademedicaonom_Internalname, "SelectedValue_set", Ddo_autorizacaoconsumo_unidademedicaonom_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFAUTORIZACAOCONSUMO_QUANTIDADE") == 0 )
            {
               AV58TFAutorizacaoConsumo_Quantidade = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFAutorizacaoConsumo_Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58TFAutorizacaoConsumo_Quantidade), 3, 0)));
               AV59TFAutorizacaoConsumo_Quantidade_To = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFAutorizacaoConsumo_Quantidade_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59TFAutorizacaoConsumo_Quantidade_To), 3, 0)));
               if ( ! (0==AV58TFAutorizacaoConsumo_Quantidade) )
               {
                  Ddo_autorizacaoconsumo_quantidade_Filteredtext_set = StringUtil.Str( (decimal)(AV58TFAutorizacaoConsumo_Quantidade), 3, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_autorizacaoconsumo_quantidade_Internalname, "FilteredText_set", Ddo_autorizacaoconsumo_quantidade_Filteredtext_set);
               }
               if ( ! (0==AV59TFAutorizacaoConsumo_Quantidade_To) )
               {
                  Ddo_autorizacaoconsumo_quantidade_Filteredtextto_set = StringUtil.Str( (decimal)(AV59TFAutorizacaoConsumo_Quantidade_To), 3, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_autorizacaoconsumo_quantidade_Internalname, "FilteredTextTo_set", Ddo_autorizacaoconsumo_quantidade_Filteredtextto_set);
               }
            }
            AV91GXV1 = (int)(AV91GXV1+1);
         }
      }

      protected void S212( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "AUTORIZACAOCONSUMO_VIGENCIAINICIO") == 0 )
            {
               AV16AutorizacaoConsumo_VigenciaInicio1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16AutorizacaoConsumo_VigenciaInicio1", context.localUtil.Format(AV16AutorizacaoConsumo_VigenciaInicio1, "99/99/99"));
               AV17AutorizacaoConsumo_VigenciaInicio_To1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17AutorizacaoConsumo_VigenciaInicio_To1", context.localUtil.Format(AV17AutorizacaoConsumo_VigenciaInicio_To1, "99/99/99"));
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "AUTORIZACAOCONSUMO_VIGENCIAINICIO") == 0 )
               {
                  AV20AutorizacaoConsumo_VigenciaInicio2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20AutorizacaoConsumo_VigenciaInicio2", context.localUtil.Format(AV20AutorizacaoConsumo_VigenciaInicio2, "99/99/99"));
                  AV21AutorizacaoConsumo_VigenciaInicio_To2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21AutorizacaoConsumo_VigenciaInicio_To2", context.localUtil.Format(AV21AutorizacaoConsumo_VigenciaInicio_To2, "99/99/99"));
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "AUTORIZACAOCONSUMO_VIGENCIAINICIO") == 0 )
                  {
                     AV24AutorizacaoConsumo_VigenciaInicio3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24AutorizacaoConsumo_VigenciaInicio3", context.localUtil.Format(AV24AutorizacaoConsumo_VigenciaInicio3, "99/99/99"));
                     AV25AutorizacaoConsumo_VigenciaInicio_To3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25AutorizacaoConsumo_VigenciaInicio_To3", context.localUtil.Format(AV25AutorizacaoConsumo_VigenciaInicio_To3, "99/99/99"));
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S172( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV30Session.Get(AV90Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (0==AV34TFAutorizacaoConsumo_Codigo) && (0==AV35TFAutorizacaoConsumo_Codigo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFAUTORIZACAOCONSUMO_CODIGO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV34TFAutorizacaoConsumo_Codigo), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV35TFAutorizacaoConsumo_Codigo_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV38TFContrato_Codigo) && (0==AV39TFContrato_Codigo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATO_CODIGO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV38TFContrato_Codigo), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV39TFContrato_Codigo_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV42TFAutorizacaoConsumo_VigenciaInicio) && (DateTime.MinValue==AV43TFAutorizacaoConsumo_VigenciaInicio_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFAUTORIZACAOCONSUMO_VIGENCIAINICIO";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV42TFAutorizacaoConsumo_VigenciaInicio, 2, "/");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV43TFAutorizacaoConsumo_VigenciaInicio_To, 2, "/");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV48TFAutorizacaoConsumo_VigenciaFim) && (DateTime.MinValue==AV49TFAutorizacaoConsumo_VigenciaFim_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFAUTORIZACAOCONSUMO_VIGENCIAFIM";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV48TFAutorizacaoConsumo_VigenciaFim, 2, "/");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV49TFAutorizacaoConsumo_VigenciaFim_To, 2, "/");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54TFAutorizacaoConsumo_UnidadeMedicaoNom)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM";
            AV11GridStateFilterValue.gxTpr_Value = AV54TFAutorizacaoConsumo_UnidadeMedicaoNom;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV58TFAutorizacaoConsumo_Quantidade) && (0==AV59TFAutorizacaoConsumo_Quantidade_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFAUTORIZACAOCONSUMO_QUANTIDADE";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV58TFAutorizacaoConsumo_Quantidade), 3, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV59TFAutorizacaoConsumo_Quantidade_To), 3, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV90Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S192( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "AUTORIZACAOCONSUMO_VIGENCIAINICIO") == 0 ) && ! ( (DateTime.MinValue==AV16AutorizacaoConsumo_VigenciaInicio1) && (DateTime.MinValue==AV17AutorizacaoConsumo_VigenciaInicio_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV16AutorizacaoConsumo_VigenciaInicio1, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV17AutorizacaoConsumo_VigenciaInicio_To1, 2, "/");
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "AUTORIZACAOCONSUMO_VIGENCIAINICIO") == 0 ) && ! ( (DateTime.MinValue==AV20AutorizacaoConsumo_VigenciaInicio2) && (DateTime.MinValue==AV21AutorizacaoConsumo_VigenciaInicio_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV20AutorizacaoConsumo_VigenciaInicio2, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV21AutorizacaoConsumo_VigenciaInicio_To2, 2, "/");
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "AUTORIZACAOCONSUMO_VIGENCIAINICIO") == 0 ) && ! ( (DateTime.MinValue==AV24AutorizacaoConsumo_VigenciaInicio3) && (DateTime.MinValue==AV25AutorizacaoConsumo_VigenciaInicio_To3) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV24AutorizacaoConsumo_VigenciaInicio3, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV25AutorizacaoConsumo_VigenciaInicio_To3, 2, "/");
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV90Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "AutorizacaoConsumo";
         AV30Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_NA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_NA2( true) ;
         }
         else
         {
            wb_table2_8_NA2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_NA2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_88_NA2( true) ;
         }
         else
         {
            wb_table3_88_NA2( false) ;
         }
         return  ;
      }

      protected void wb_table3_88_NA2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_NA2e( true) ;
         }
         else
         {
            wb_table1_2_NA2e( false) ;
         }
      }

      protected void wb_table3_88_NA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_91_NA2( true) ;
         }
         else
         {
            wb_table4_91_NA2( false) ;
         }
         return  ;
      }

      protected void wb_table4_91_NA2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_88_NA2e( true) ;
         }
         else
         {
            wb_table3_88_NA2e( false) ;
         }
      }

      protected void wb_table4_91_NA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"94\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAutorizacaoConsumo_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtAutorizacaoConsumo_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAutorizacaoConsumo_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContrato_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtContrato_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContrato_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Vig�ncia T�rmino") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Vig�ncia Inicio") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAutorizacaoConsumo_VigenciaInicio_Titleformat == 0 )
               {
                  context.SendWebValue( edtAutorizacaoConsumo_VigenciaInicio_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAutorizacaoConsumo_VigenciaInicio_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAutorizacaoConsumo_VigenciaFim_Titleformat == 0 )
               {
                  context.SendWebValue( edtAutorizacaoConsumo_VigenciaFim_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAutorizacaoConsumo_VigenciaFim_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "de Medi��o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAutorizacaoConsumo_UnidadeMedicaoNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtAutorizacaoConsumo_UnidadeMedicaoNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAutorizacaoConsumo_UnidadeMedicaoNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAutorizacaoConsumo_Quantidade_Titleformat == 0 )
               {
                  context.SendWebValue( edtAutorizacaoConsumo_Quantidade_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAutorizacaoConsumo_Quantidade_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1774AutorizacaoConsumo_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAutorizacaoConsumo_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAutorizacaoConsumo_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContrato_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContrato_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A83Contrato_DataVigenciaTermino, "99/99/99"));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A82Contrato_DataVigenciaInicio, "99/99/99"));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A1775AutorizacaoConsumo_VigenciaInicio, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAutorizacaoConsumo_VigenciaInicio_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAutorizacaoConsumo_VigenciaInicio_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A1776AutorizacaoConsumo_VigenciaFim, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAutorizacaoConsumo_VigenciaFim_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAutorizacaoConsumo_VigenciaFim_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1777AutorizacaoConsumo_UnidadeMedicaoCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1778AutorizacaoConsumo_UnidadeMedicaoNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAutorizacaoConsumo_UnidadeMedicaoNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAutorizacaoConsumo_UnidadeMedicaoNom_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1779AutorizacaoConsumo_Quantidade), 3, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAutorizacaoConsumo_Quantidade_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAutorizacaoConsumo_Quantidade_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 94 )
         {
            wbEnd = 0;
            nRC_GXsfl_94 = (short)(nGXsfl_94_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_91_NA2e( true) ;
         }
         else
         {
            wb_table4_91_NA2e( false) ;
         }
      }

      protected void wb_table2_8_NA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblAutorizacaoconsumotitle_Internalname, "Autoriza��o de Consumo", "", "", lblAutorizacaoconsumotitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWAutorizacaoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_13_NA2( true) ;
         }
         else
         {
            wb_table5_13_NA2( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_NA2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWAutorizacaoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_94_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWAutorizacaoConsumo.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWAutorizacaoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_23_NA2( true) ;
         }
         else
         {
            wb_table6_23_NA2( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_NA2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_NA2e( true) ;
         }
         else
         {
            wb_table2_8_NA2e( false) ;
         }
      }

      protected void wb_table6_23_NA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWAutorizacaoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_NA2( true) ;
         }
         else
         {
            wb_table7_28_NA2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_NA2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWAutorizacaoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_NA2e( true) ;
         }
         else
         {
            wb_table6_23_NA2e( false) ;
         }
      }

      protected void wb_table7_28_NA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAutorizacaoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_94_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_WWAutorizacaoConsumo.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAutorizacaoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_37_NA2( true) ;
         }
         else
         {
            wb_table8_37_NA2( false) ;
         }
         return  ;
      }

      protected void wb_table8_37_NA2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWAutorizacaoConsumo.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWAutorizacaoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAutorizacaoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'" + sGXsfl_94_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,52);\"", "", true, "HLP_WWAutorizacaoConsumo.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAutorizacaoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_56_NA2( true) ;
         }
         else
         {
            wb_table9_56_NA2( false) ;
         }
         return  ;
      }

      protected void wb_table9_56_NA2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWAutorizacaoConsumo.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWAutorizacaoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAutorizacaoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'',false,'" + sGXsfl_94_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,71);\"", "", true, "HLP_WWAutorizacaoConsumo.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAutorizacaoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_75_NA2( true) ;
         }
         else
         {
            wb_table10_75_NA2( false) ;
         }
         return  ;
      }

      protected void wb_table10_75_NA2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWAutorizacaoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_NA2e( true) ;
         }
         else
         {
            wb_table7_28_NA2e( false) ;
         }
      }

      protected void wb_table10_75_NA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio3_Internalname, tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'" + sGXsfl_94_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavAutorizacaoconsumo_vigenciainicio3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavAutorizacaoconsumo_vigenciainicio3_Internalname, context.localUtil.Format(AV24AutorizacaoConsumo_VigenciaInicio3, "99/99/99"), context.localUtil.Format( AV24AutorizacaoConsumo_VigenciaInicio3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,78);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAutorizacaoconsumo_vigenciainicio3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAutorizacaoConsumo.htm");
            GxWebStd.gx_bitmap( context, edtavAutorizacaoconsumo_vigenciainicio3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWAutorizacaoConsumo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersautorizacaoconsumo_vigenciainicio_rangemiddletext3_Internalname, "at�", "", "", lblDynamicfiltersautorizacaoconsumo_vigenciainicio_rangemiddletext3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAutorizacaoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'',false,'" + sGXsfl_94_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavAutorizacaoconsumo_vigenciainicio_to3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavAutorizacaoconsumo_vigenciainicio_to3_Internalname, context.localUtil.Format(AV25AutorizacaoConsumo_VigenciaInicio_To3, "99/99/99"), context.localUtil.Format( AV25AutorizacaoConsumo_VigenciaInicio_To3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,82);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAutorizacaoconsumo_vigenciainicio_to3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAutorizacaoConsumo.htm");
            GxWebStd.gx_bitmap( context, edtavAutorizacaoconsumo_vigenciainicio_to3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWAutorizacaoConsumo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_75_NA2e( true) ;
         }
         else
         {
            wb_table10_75_NA2e( false) ;
         }
      }

      protected void wb_table9_56_NA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio2_Internalname, tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'" + sGXsfl_94_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavAutorizacaoconsumo_vigenciainicio2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavAutorizacaoconsumo_vigenciainicio2_Internalname, context.localUtil.Format(AV20AutorizacaoConsumo_VigenciaInicio2, "99/99/99"), context.localUtil.Format( AV20AutorizacaoConsumo_VigenciaInicio2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAutorizacaoconsumo_vigenciainicio2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAutorizacaoConsumo.htm");
            GxWebStd.gx_bitmap( context, edtavAutorizacaoconsumo_vigenciainicio2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWAutorizacaoConsumo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersautorizacaoconsumo_vigenciainicio_rangemiddletext2_Internalname, "at�", "", "", lblDynamicfiltersautorizacaoconsumo_vigenciainicio_rangemiddletext2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAutorizacaoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'" + sGXsfl_94_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavAutorizacaoconsumo_vigenciainicio_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavAutorizacaoconsumo_vigenciainicio_to2_Internalname, context.localUtil.Format(AV21AutorizacaoConsumo_VigenciaInicio_To2, "99/99/99"), context.localUtil.Format( AV21AutorizacaoConsumo_VigenciaInicio_To2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,63);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAutorizacaoconsumo_vigenciainicio_to2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAutorizacaoConsumo.htm");
            GxWebStd.gx_bitmap( context, edtavAutorizacaoconsumo_vigenciainicio_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWAutorizacaoConsumo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_56_NA2e( true) ;
         }
         else
         {
            wb_table9_56_NA2e( false) ;
         }
      }

      protected void wb_table8_37_NA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio1_Internalname, tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_94_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavAutorizacaoconsumo_vigenciainicio1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavAutorizacaoconsumo_vigenciainicio1_Internalname, context.localUtil.Format(AV16AutorizacaoConsumo_VigenciaInicio1, "99/99/99"), context.localUtil.Format( AV16AutorizacaoConsumo_VigenciaInicio1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,40);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAutorizacaoconsumo_vigenciainicio1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAutorizacaoConsumo.htm");
            GxWebStd.gx_bitmap( context, edtavAutorizacaoconsumo_vigenciainicio1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWAutorizacaoConsumo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersautorizacaoconsumo_vigenciainicio_rangemiddletext1_Internalname, "at�", "", "", lblDynamicfiltersautorizacaoconsumo_vigenciainicio_rangemiddletext1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAutorizacaoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'" + sGXsfl_94_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavAutorizacaoconsumo_vigenciainicio_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavAutorizacaoconsumo_vigenciainicio_to1_Internalname, context.localUtil.Format(AV17AutorizacaoConsumo_VigenciaInicio_To1, "99/99/99"), context.localUtil.Format( AV17AutorizacaoConsumo_VigenciaInicio_To1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAutorizacaoconsumo_vigenciainicio_to1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAutorizacaoConsumo.htm");
            GxWebStd.gx_bitmap( context, edtavAutorizacaoconsumo_vigenciainicio_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWAutorizacaoConsumo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_37_NA2e( true) ;
         }
         else
         {
            wb_table8_37_NA2e( false) ;
         }
      }

      protected void wb_table5_13_NA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWAutorizacaoConsumo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_NA2e( true) ;
         }
         else
         {
            wb_table5_13_NA2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PANA2( ) ;
         WSNA2( ) ;
         WENA2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202031221234986");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwautorizacaoconsumo.js", "?202031221234986");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_942( )
      {
         edtAutorizacaoConsumo_Codigo_Internalname = "AUTORIZACAOCONSUMO_CODIGO_"+sGXsfl_94_idx;
         edtContrato_Codigo_Internalname = "CONTRATO_CODIGO_"+sGXsfl_94_idx;
         edtContrato_DataVigenciaTermino_Internalname = "CONTRATO_DATAVIGENCIATERMINO_"+sGXsfl_94_idx;
         edtContrato_DataVigenciaInicio_Internalname = "CONTRATO_DATAVIGENCIAINICIO_"+sGXsfl_94_idx;
         edtAutorizacaoConsumo_VigenciaInicio_Internalname = "AUTORIZACAOCONSUMO_VIGENCIAINICIO_"+sGXsfl_94_idx;
         edtAutorizacaoConsumo_VigenciaFim_Internalname = "AUTORIZACAOCONSUMO_VIGENCIAFIM_"+sGXsfl_94_idx;
         edtAutorizacaoConsumo_UnidadeMedicaoCod_Internalname = "AUTORIZACAOCONSUMO_UNIDADEMEDICAOCOD_"+sGXsfl_94_idx;
         edtAutorizacaoConsumo_UnidadeMedicaoNom_Internalname = "AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_"+sGXsfl_94_idx;
         edtAutorizacaoConsumo_Quantidade_Internalname = "AUTORIZACAOCONSUMO_QUANTIDADE_"+sGXsfl_94_idx;
      }

      protected void SubsflControlProps_fel_942( )
      {
         edtAutorizacaoConsumo_Codigo_Internalname = "AUTORIZACAOCONSUMO_CODIGO_"+sGXsfl_94_fel_idx;
         edtContrato_Codigo_Internalname = "CONTRATO_CODIGO_"+sGXsfl_94_fel_idx;
         edtContrato_DataVigenciaTermino_Internalname = "CONTRATO_DATAVIGENCIATERMINO_"+sGXsfl_94_fel_idx;
         edtContrato_DataVigenciaInicio_Internalname = "CONTRATO_DATAVIGENCIAINICIO_"+sGXsfl_94_fel_idx;
         edtAutorizacaoConsumo_VigenciaInicio_Internalname = "AUTORIZACAOCONSUMO_VIGENCIAINICIO_"+sGXsfl_94_fel_idx;
         edtAutorizacaoConsumo_VigenciaFim_Internalname = "AUTORIZACAOCONSUMO_VIGENCIAFIM_"+sGXsfl_94_fel_idx;
         edtAutorizacaoConsumo_UnidadeMedicaoCod_Internalname = "AUTORIZACAOCONSUMO_UNIDADEMEDICAOCOD_"+sGXsfl_94_fel_idx;
         edtAutorizacaoConsumo_UnidadeMedicaoNom_Internalname = "AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_"+sGXsfl_94_fel_idx;
         edtAutorizacaoConsumo_Quantidade_Internalname = "AUTORIZACAOCONSUMO_QUANTIDADE_"+sGXsfl_94_fel_idx;
      }

      protected void sendrow_942( )
      {
         SubsflControlProps_942( ) ;
         WBNA0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_94_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_94_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_94_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAutorizacaoConsumo_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1774AutorizacaoConsumo_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1774AutorizacaoConsumo_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAutorizacaoConsumo_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContrato_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContrato_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContrato_DataVigenciaTermino_Internalname,context.localUtil.Format(A83Contrato_DataVigenciaTermino, "99/99/99"),context.localUtil.Format( A83Contrato_DataVigenciaTermino, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContrato_DataVigenciaTermino_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)0,(bool)true,(String)"Data",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContrato_DataVigenciaInicio_Internalname,context.localUtil.Format(A82Contrato_DataVigenciaInicio, "99/99/99"),context.localUtil.Format( A82Contrato_DataVigenciaInicio, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContrato_DataVigenciaInicio_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)0,(bool)true,(String)"Data",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAutorizacaoConsumo_VigenciaInicio_Internalname,context.localUtil.Format(A1775AutorizacaoConsumo_VigenciaInicio, "99/99/99"),context.localUtil.Format( A1775AutorizacaoConsumo_VigenciaInicio, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAutorizacaoConsumo_VigenciaInicio_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAutorizacaoConsumo_VigenciaFim_Internalname,context.localUtil.Format(A1776AutorizacaoConsumo_VigenciaFim, "99/99/99"),context.localUtil.Format( A1776AutorizacaoConsumo_VigenciaFim, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAutorizacaoConsumo_VigenciaFim_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAutorizacaoConsumo_UnidadeMedicaoCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1777AutorizacaoConsumo_UnidadeMedicaoCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1777AutorizacaoConsumo_UnidadeMedicaoCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAutorizacaoConsumo_UnidadeMedicaoCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAutorizacaoConsumo_UnidadeMedicaoNom_Internalname,StringUtil.RTrim( A1778AutorizacaoConsumo_UnidadeMedicaoNom),StringUtil.RTrim( context.localUtil.Format( A1778AutorizacaoConsumo_UnidadeMedicaoNom, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAutorizacaoConsumo_UnidadeMedicaoNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAutorizacaoConsumo_Quantidade_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1779AutorizacaoConsumo_Quantidade), 3, 0, ",", "")),context.localUtil.Format( (decimal)(A1779AutorizacaoConsumo_Quantidade), "ZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAutorizacaoConsumo_Quantidade_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)3,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)0,(bool)true,(String)"Quantidade",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_AUTORIZACAOCONSUMO_CODIGO"+"_"+sGXsfl_94_idx, GetSecureSignedToken( sGXsfl_94_idx, context.localUtil.Format( (decimal)(A1774AutorizacaoConsumo_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATO_CODIGO"+"_"+sGXsfl_94_idx, GetSecureSignedToken( sGXsfl_94_idx, context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_AUTORIZACAOCONSUMO_VIGENCIAINICIO"+"_"+sGXsfl_94_idx, GetSecureSignedToken( sGXsfl_94_idx, A1775AutorizacaoConsumo_VigenciaInicio));
            GxWebStd.gx_hidden_field( context, "gxhash_AUTORIZACAOCONSUMO_VIGENCIAFIM"+"_"+sGXsfl_94_idx, GetSecureSignedToken( sGXsfl_94_idx, A1776AutorizacaoConsumo_VigenciaFim));
            GxWebStd.gx_hidden_field( context, "gxhash_AUTORIZACAOCONSUMO_UNIDADEMEDICAOCOD"+"_"+sGXsfl_94_idx, GetSecureSignedToken( sGXsfl_94_idx, context.localUtil.Format( (decimal)(A1777AutorizacaoConsumo_UnidadeMedicaoCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_AUTORIZACAOCONSUMO_QUANTIDADE"+"_"+sGXsfl_94_idx, GetSecureSignedToken( sGXsfl_94_idx, context.localUtil.Format( (decimal)(A1779AutorizacaoConsumo_Quantidade), "ZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_94_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_94_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_94_idx+1));
            sGXsfl_94_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_94_idx), 4, 0)), 4, "0");
            SubsflControlProps_942( ) ;
         }
         /* End function sendrow_942 */
      }

      protected void init_default_properties( )
      {
         lblAutorizacaoconsumotitle_Internalname = "AUTORIZACAOCONSUMOTITLE";
         imgInsert_Internalname = "INSERT";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavAutorizacaoconsumo_vigenciainicio1_Internalname = "vAUTORIZACAOCONSUMO_VIGENCIAINICIO1";
         lblDynamicfiltersautorizacaoconsumo_vigenciainicio_rangemiddletext1_Internalname = "DYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO_RANGEMIDDLETEXT1";
         edtavAutorizacaoconsumo_vigenciainicio_to1_Internalname = "vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO1";
         tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio1_Internalname = "TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavAutorizacaoconsumo_vigenciainicio2_Internalname = "vAUTORIZACAOCONSUMO_VIGENCIAINICIO2";
         lblDynamicfiltersautorizacaoconsumo_vigenciainicio_rangemiddletext2_Internalname = "DYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO_RANGEMIDDLETEXT2";
         edtavAutorizacaoconsumo_vigenciainicio_to2_Internalname = "vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO2";
         tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio2_Internalname = "TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         edtavAutorizacaoconsumo_vigenciainicio3_Internalname = "vAUTORIZACAOCONSUMO_VIGENCIAINICIO3";
         lblDynamicfiltersautorizacaoconsumo_vigenciainicio_rangemiddletext3_Internalname = "DYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO_RANGEMIDDLETEXT3";
         edtavAutorizacaoconsumo_vigenciainicio_to3_Internalname = "vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO3";
         tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio3_Internalname = "TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtAutorizacaoConsumo_Codigo_Internalname = "AUTORIZACAOCONSUMO_CODIGO";
         edtContrato_Codigo_Internalname = "CONTRATO_CODIGO";
         edtContrato_DataVigenciaTermino_Internalname = "CONTRATO_DATAVIGENCIATERMINO";
         edtContrato_DataVigenciaInicio_Internalname = "CONTRATO_DATAVIGENCIAINICIO";
         edtAutorizacaoConsumo_VigenciaInicio_Internalname = "AUTORIZACAOCONSUMO_VIGENCIAINICIO";
         edtAutorizacaoConsumo_VigenciaFim_Internalname = "AUTORIZACAOCONSUMO_VIGENCIAFIM";
         edtAutorizacaoConsumo_UnidadeMedicaoCod_Internalname = "AUTORIZACAOCONSUMO_UNIDADEMEDICAOCOD";
         edtAutorizacaoConsumo_UnidadeMedicaoNom_Internalname = "AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM";
         edtAutorizacaoConsumo_Quantidade_Internalname = "AUTORIZACAOCONSUMO_QUANTIDADE";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfautorizacaoconsumo_codigo_Internalname = "vTFAUTORIZACAOCONSUMO_CODIGO";
         edtavTfautorizacaoconsumo_codigo_to_Internalname = "vTFAUTORIZACAOCONSUMO_CODIGO_TO";
         edtavTfcontrato_codigo_Internalname = "vTFCONTRATO_CODIGO";
         edtavTfcontrato_codigo_to_Internalname = "vTFCONTRATO_CODIGO_TO";
         edtavTfautorizacaoconsumo_vigenciainicio_Internalname = "vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO";
         edtavTfautorizacaoconsumo_vigenciainicio_to_Internalname = "vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO";
         edtavDdo_autorizacaoconsumo_vigenciainicioauxdate_Internalname = "vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOAUXDATE";
         edtavDdo_autorizacaoconsumo_vigenciainicioauxdateto_Internalname = "vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOAUXDATETO";
         divDdo_autorizacaoconsumo_vigenciainicioauxdates_Internalname = "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOAUXDATES";
         edtavTfautorizacaoconsumo_vigenciafim_Internalname = "vTFAUTORIZACAOCONSUMO_VIGENCIAFIM";
         edtavTfautorizacaoconsumo_vigenciafim_to_Internalname = "vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO";
         edtavDdo_autorizacaoconsumo_vigenciafimauxdate_Internalname = "vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMAUXDATE";
         edtavDdo_autorizacaoconsumo_vigenciafimauxdateto_Internalname = "vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMAUXDATETO";
         divDdo_autorizacaoconsumo_vigenciafimauxdates_Internalname = "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMAUXDATES";
         edtavTfautorizacaoconsumo_unidademedicaonom_Internalname = "vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM";
         edtavTfautorizacaoconsumo_unidademedicaonom_sel_Internalname = "vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL";
         edtavTfautorizacaoconsumo_quantidade_Internalname = "vTFAUTORIZACAOCONSUMO_QUANTIDADE";
         edtavTfautorizacaoconsumo_quantidade_to_Internalname = "vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO";
         Ddo_autorizacaoconsumo_codigo_Internalname = "DDO_AUTORIZACAOCONSUMO_CODIGO";
         edtavDdo_autorizacaoconsumo_codigotitlecontrolidtoreplace_Internalname = "vDDO_AUTORIZACAOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_contrato_codigo_Internalname = "DDO_CONTRATO_CODIGO";
         edtavDdo_contrato_codigotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_autorizacaoconsumo_vigenciainicio_Internalname = "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO";
         edtavDdo_autorizacaoconsumo_vigenciainiciotitlecontrolidtoreplace_Internalname = "vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE";
         Ddo_autorizacaoconsumo_vigenciafim_Internalname = "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM";
         edtavDdo_autorizacaoconsumo_vigenciafimtitlecontrolidtoreplace_Internalname = "vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMTITLECONTROLIDTOREPLACE";
         Ddo_autorizacaoconsumo_unidademedicaonom_Internalname = "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM";
         edtavDdo_autorizacaoconsumo_unidademedicaonomtitlecontrolidtoreplace_Internalname = "vDDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLECONTROLIDTOREPLACE";
         Ddo_autorizacaoconsumo_quantidade_Internalname = "DDO_AUTORIZACAOCONSUMO_QUANTIDADE";
         edtavDdo_autorizacaoconsumo_quantidadetitlecontrolidtoreplace_Internalname = "vDDO_AUTORIZACAOCONSUMO_QUANTIDADETITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtAutorizacaoConsumo_Quantidade_Jsonclick = "";
         edtAutorizacaoConsumo_UnidadeMedicaoNom_Jsonclick = "";
         edtAutorizacaoConsumo_UnidadeMedicaoCod_Jsonclick = "";
         edtAutorizacaoConsumo_VigenciaFim_Jsonclick = "";
         edtAutorizacaoConsumo_VigenciaInicio_Jsonclick = "";
         edtContrato_DataVigenciaInicio_Jsonclick = "";
         edtContrato_DataVigenciaTermino_Jsonclick = "";
         edtContrato_Codigo_Jsonclick = "";
         edtAutorizacaoConsumo_Codigo_Jsonclick = "";
         edtavAutorizacaoconsumo_vigenciainicio_to1_Jsonclick = "";
         edtavAutorizacaoconsumo_vigenciainicio1_Jsonclick = "";
         edtavAutorizacaoconsumo_vigenciainicio_to2_Jsonclick = "";
         edtavAutorizacaoconsumo_vigenciainicio2_Jsonclick = "";
         edtavAutorizacaoconsumo_vigenciainicio_to3_Jsonclick = "";
         edtavAutorizacaoconsumo_vigenciainicio3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtAutorizacaoConsumo_Quantidade_Titleformat = 0;
         edtAutorizacaoConsumo_UnidadeMedicaoNom_Titleformat = 0;
         edtAutorizacaoConsumo_VigenciaFim_Titleformat = 0;
         edtAutorizacaoConsumo_VigenciaInicio_Titleformat = 0;
         edtContrato_Codigo_Titleformat = 0;
         edtAutorizacaoConsumo_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio3_Visible = 1;
         tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio2_Visible = 1;
         tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio1_Visible = 1;
         edtAutorizacaoConsumo_Quantidade_Title = "Quantidade";
         edtAutorizacaoConsumo_UnidadeMedicaoNom_Title = "Unidade Medi��o";
         edtAutorizacaoConsumo_VigenciaFim_Title = "D. Vigencia T�rmino";
         edtAutorizacaoConsumo_VigenciaInicio_Title = "D. Vig�ncia Inicio";
         edtContrato_Codigo_Title = "Contrato";
         edtAutorizacaoConsumo_Codigo_Title = "C�digo";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_autorizacaoconsumo_quantidadetitlecontrolidtoreplace_Visible = 1;
         edtavDdo_autorizacaoconsumo_unidademedicaonomtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_autorizacaoconsumo_vigenciafimtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_autorizacaoconsumo_vigenciainiciotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contrato_codigotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_autorizacaoconsumo_codigotitlecontrolidtoreplace_Visible = 1;
         edtavTfautorizacaoconsumo_quantidade_to_Jsonclick = "";
         edtavTfautorizacaoconsumo_quantidade_to_Visible = 1;
         edtavTfautorizacaoconsumo_quantidade_Jsonclick = "";
         edtavTfautorizacaoconsumo_quantidade_Visible = 1;
         edtavTfautorizacaoconsumo_unidademedicaonom_sel_Jsonclick = "";
         edtavTfautorizacaoconsumo_unidademedicaonom_sel_Visible = 1;
         edtavTfautorizacaoconsumo_unidademedicaonom_Jsonclick = "";
         edtavTfautorizacaoconsumo_unidademedicaonom_Visible = 1;
         edtavDdo_autorizacaoconsumo_vigenciafimauxdateto_Jsonclick = "";
         edtavDdo_autorizacaoconsumo_vigenciafimauxdate_Jsonclick = "";
         edtavTfautorizacaoconsumo_vigenciafim_to_Jsonclick = "";
         edtavTfautorizacaoconsumo_vigenciafim_to_Visible = 1;
         edtavTfautorizacaoconsumo_vigenciafim_Jsonclick = "";
         edtavTfautorizacaoconsumo_vigenciafim_Visible = 1;
         edtavDdo_autorizacaoconsumo_vigenciainicioauxdateto_Jsonclick = "";
         edtavDdo_autorizacaoconsumo_vigenciainicioauxdate_Jsonclick = "";
         edtavTfautorizacaoconsumo_vigenciainicio_to_Jsonclick = "";
         edtavTfautorizacaoconsumo_vigenciainicio_to_Visible = 1;
         edtavTfautorizacaoconsumo_vigenciainicio_Jsonclick = "";
         edtavTfautorizacaoconsumo_vigenciainicio_Visible = 1;
         edtavTfcontrato_codigo_to_Jsonclick = "";
         edtavTfcontrato_codigo_to_Visible = 1;
         edtavTfcontrato_codigo_Jsonclick = "";
         edtavTfcontrato_codigo_Visible = 1;
         edtavTfautorizacaoconsumo_codigo_to_Jsonclick = "";
         edtavTfautorizacaoconsumo_codigo_to_Visible = 1;
         edtavTfautorizacaoconsumo_codigo_Jsonclick = "";
         edtavTfautorizacaoconsumo_codigo_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_autorizacaoconsumo_quantidade_Searchbuttontext = "Pesquisar";
         Ddo_autorizacaoconsumo_quantidade_Rangefilterto = "At�";
         Ddo_autorizacaoconsumo_quantidade_Rangefilterfrom = "Desde";
         Ddo_autorizacaoconsumo_quantidade_Cleanfilter = "Limpar pesquisa";
         Ddo_autorizacaoconsumo_quantidade_Sortdsc = "Ordenar de Z � A";
         Ddo_autorizacaoconsumo_quantidade_Sortasc = "Ordenar de A � Z";
         Ddo_autorizacaoconsumo_quantidade_Includedatalist = Convert.ToBoolean( 0);
         Ddo_autorizacaoconsumo_quantidade_Filterisrange = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_quantidade_Filtertype = "Numeric";
         Ddo_autorizacaoconsumo_quantidade_Includefilter = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_quantidade_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_quantidade_Includesortasc = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_quantidade_Titlecontrolidtoreplace = "";
         Ddo_autorizacaoconsumo_quantidade_Dropdownoptionstype = "GridTitleSettings";
         Ddo_autorizacaoconsumo_quantidade_Cls = "ColumnSettings";
         Ddo_autorizacaoconsumo_quantidade_Tooltip = "Op��es";
         Ddo_autorizacaoconsumo_quantidade_Caption = "";
         Ddo_autorizacaoconsumo_unidademedicaonom_Searchbuttontext = "Pesquisar";
         Ddo_autorizacaoconsumo_unidademedicaonom_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_autorizacaoconsumo_unidademedicaonom_Cleanfilter = "Limpar pesquisa";
         Ddo_autorizacaoconsumo_unidademedicaonom_Loadingdata = "Carregando dados...";
         Ddo_autorizacaoconsumo_unidademedicaonom_Sortdsc = "Ordenar de Z � A";
         Ddo_autorizacaoconsumo_unidademedicaonom_Sortasc = "Ordenar de A � Z";
         Ddo_autorizacaoconsumo_unidademedicaonom_Datalistupdateminimumcharacters = 0;
         Ddo_autorizacaoconsumo_unidademedicaonom_Datalistproc = "GetWWAutorizacaoConsumoFilterData";
         Ddo_autorizacaoconsumo_unidademedicaonom_Datalisttype = "Dynamic";
         Ddo_autorizacaoconsumo_unidademedicaonom_Includedatalist = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_unidademedicaonom_Filterisrange = Convert.ToBoolean( 0);
         Ddo_autorizacaoconsumo_unidademedicaonom_Filtertype = "Character";
         Ddo_autorizacaoconsumo_unidademedicaonom_Includefilter = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_unidademedicaonom_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_unidademedicaonom_Includesortasc = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_unidademedicaonom_Titlecontrolidtoreplace = "";
         Ddo_autorizacaoconsumo_unidademedicaonom_Dropdownoptionstype = "GridTitleSettings";
         Ddo_autorizacaoconsumo_unidademedicaonom_Cls = "ColumnSettings";
         Ddo_autorizacaoconsumo_unidademedicaonom_Tooltip = "Op��es";
         Ddo_autorizacaoconsumo_unidademedicaonom_Caption = "";
         Ddo_autorizacaoconsumo_vigenciafim_Searchbuttontext = "Pesquisar";
         Ddo_autorizacaoconsumo_vigenciafim_Rangefilterto = "At�";
         Ddo_autorizacaoconsumo_vigenciafim_Rangefilterfrom = "Desde";
         Ddo_autorizacaoconsumo_vigenciafim_Cleanfilter = "Limpar pesquisa";
         Ddo_autorizacaoconsumo_vigenciafim_Sortdsc = "Ordenar de Z � A";
         Ddo_autorizacaoconsumo_vigenciafim_Sortasc = "Ordenar de A � Z";
         Ddo_autorizacaoconsumo_vigenciafim_Includedatalist = Convert.ToBoolean( 0);
         Ddo_autorizacaoconsumo_vigenciafim_Filterisrange = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_vigenciafim_Filtertype = "Date";
         Ddo_autorizacaoconsumo_vigenciafim_Includefilter = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_vigenciafim_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_vigenciafim_Includesortasc = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_vigenciafim_Titlecontrolidtoreplace = "";
         Ddo_autorizacaoconsumo_vigenciafim_Dropdownoptionstype = "GridTitleSettings";
         Ddo_autorizacaoconsumo_vigenciafim_Cls = "ColumnSettings";
         Ddo_autorizacaoconsumo_vigenciafim_Tooltip = "Op��es";
         Ddo_autorizacaoconsumo_vigenciafim_Caption = "";
         Ddo_autorizacaoconsumo_vigenciainicio_Searchbuttontext = "Pesquisar";
         Ddo_autorizacaoconsumo_vigenciainicio_Rangefilterto = "At�";
         Ddo_autorizacaoconsumo_vigenciainicio_Rangefilterfrom = "Desde";
         Ddo_autorizacaoconsumo_vigenciainicio_Cleanfilter = "Limpar pesquisa";
         Ddo_autorizacaoconsumo_vigenciainicio_Sortdsc = "Ordenar de Z � A";
         Ddo_autorizacaoconsumo_vigenciainicio_Sortasc = "Ordenar de A � Z";
         Ddo_autorizacaoconsumo_vigenciainicio_Includedatalist = Convert.ToBoolean( 0);
         Ddo_autorizacaoconsumo_vigenciainicio_Filterisrange = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_vigenciainicio_Filtertype = "Date";
         Ddo_autorizacaoconsumo_vigenciainicio_Includefilter = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_vigenciainicio_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_vigenciainicio_Includesortasc = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_vigenciainicio_Titlecontrolidtoreplace = "";
         Ddo_autorizacaoconsumo_vigenciainicio_Dropdownoptionstype = "GridTitleSettings";
         Ddo_autorizacaoconsumo_vigenciainicio_Cls = "ColumnSettings";
         Ddo_autorizacaoconsumo_vigenciainicio_Tooltip = "Op��es";
         Ddo_autorizacaoconsumo_vigenciainicio_Caption = "";
         Ddo_contrato_codigo_Searchbuttontext = "Pesquisar";
         Ddo_contrato_codigo_Rangefilterto = "At�";
         Ddo_contrato_codigo_Rangefilterfrom = "Desde";
         Ddo_contrato_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_contrato_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_contrato_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_contrato_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contrato_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contrato_codigo_Filtertype = "Numeric";
         Ddo_contrato_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_contrato_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contrato_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contrato_codigo_Titlecontrolidtoreplace = "";
         Ddo_contrato_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contrato_codigo_Cls = "ColumnSettings";
         Ddo_contrato_codigo_Tooltip = "Op��es";
         Ddo_contrato_codigo_Caption = "";
         Ddo_autorizacaoconsumo_codigo_Searchbuttontext = "Pesquisar";
         Ddo_autorizacaoconsumo_codigo_Rangefilterto = "At�";
         Ddo_autorizacaoconsumo_codigo_Rangefilterfrom = "Desde";
         Ddo_autorizacaoconsumo_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_autorizacaoconsumo_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_autorizacaoconsumo_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_autorizacaoconsumo_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_autorizacaoconsumo_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_codigo_Filtertype = "Numeric";
         Ddo_autorizacaoconsumo_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_codigo_Titlecontrolidtoreplace = "";
         Ddo_autorizacaoconsumo_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_autorizacaoconsumo_codigo_Cls = "ColumnSettings";
         Ddo_autorizacaoconsumo_codigo_Tooltip = "Op��es";
         Ddo_autorizacaoconsumo_codigo_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Autoriza��o de Consumo";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_QUANTIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AutorizacaoConsumo_VigenciaInicio1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17AutorizacaoConsumo_VigenciaInicio_To1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AutorizacaoConsumo_VigenciaInicio2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21AutorizacaoConsumo_VigenciaInicio_To2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AutorizacaoConsumo_VigenciaInicio3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25AutorizacaoConsumo_VigenciaInicio_To3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV34TFAutorizacaoConsumo_Codigo',fld:'vTFAUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFAutorizacaoConsumo_Codigo_To',fld:'vTFAUTORIZACAOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFAutorizacaoConsumo_VigenciaInicio',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFAutorizacaoConsumo_VigenciaInicio_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFAutorizacaoConsumo_VigenciaFim',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFAutorizacaoConsumo_VigenciaFim_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFAutorizacaoConsumo_UnidadeMedicaoNom',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',pic:'@!',nv:''},{av:'AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL',pic:'@!',nv:''},{av:'AV58TFAutorizacaoConsumo_Quantidade',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE',pic:'ZZ9',nv:0},{av:'AV59TFAutorizacaoConsumo_Quantidade_To',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO',pic:'ZZ9',nv:0},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV33AutorizacaoConsumo_CodigoTitleFilterData',fld:'vAUTORIZACAOCONSUMO_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV37Contrato_CodigoTitleFilterData',fld:'vCONTRATO_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV41AutorizacaoConsumo_VigenciaInicioTitleFilterData',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIOTITLEFILTERDATA',pic:'',nv:null},{av:'AV47AutorizacaoConsumo_VigenciaFimTitleFilterData',fld:'vAUTORIZACAOCONSUMO_VIGENCIAFIMTITLEFILTERDATA',pic:'',nv:null},{av:'AV53AutorizacaoConsumo_UnidadeMedicaoNomTitleFilterData',fld:'vAUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV57AutorizacaoConsumo_QuantidadeTitleFilterData',fld:'vAUTORIZACAOCONSUMO_QUANTIDADETITLEFILTERDATA',pic:'',nv:null},{av:'edtAutorizacaoConsumo_Codigo_Titleformat',ctrl:'AUTORIZACAOCONSUMO_CODIGO',prop:'Titleformat'},{av:'edtAutorizacaoConsumo_Codigo_Title',ctrl:'AUTORIZACAOCONSUMO_CODIGO',prop:'Title'},{av:'edtContrato_Codigo_Titleformat',ctrl:'CONTRATO_CODIGO',prop:'Titleformat'},{av:'edtContrato_Codigo_Title',ctrl:'CONTRATO_CODIGO',prop:'Title'},{av:'edtAutorizacaoConsumo_VigenciaInicio_Titleformat',ctrl:'AUTORIZACAOCONSUMO_VIGENCIAINICIO',prop:'Titleformat'},{av:'edtAutorizacaoConsumo_VigenciaInicio_Title',ctrl:'AUTORIZACAOCONSUMO_VIGENCIAINICIO',prop:'Title'},{av:'edtAutorizacaoConsumo_VigenciaFim_Titleformat',ctrl:'AUTORIZACAOCONSUMO_VIGENCIAFIM',prop:'Titleformat'},{av:'edtAutorizacaoConsumo_VigenciaFim_Title',ctrl:'AUTORIZACAOCONSUMO_VIGENCIAFIM',prop:'Title'},{av:'edtAutorizacaoConsumo_UnidadeMedicaoNom_Titleformat',ctrl:'AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',prop:'Titleformat'},{av:'edtAutorizacaoConsumo_UnidadeMedicaoNom_Title',ctrl:'AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',prop:'Title'},{av:'edtAutorizacaoConsumo_Quantidade_Titleformat',ctrl:'AUTORIZACAOCONSUMO_QUANTIDADE',prop:'Titleformat'},{av:'edtAutorizacaoConsumo_Quantidade_Title',ctrl:'AUTORIZACAOCONSUMO_QUANTIDADE',prop:'Title'},{av:'AV63GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV64GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11NA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AutorizacaoConsumo_VigenciaInicio1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17AutorizacaoConsumo_VigenciaInicio_To1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AutorizacaoConsumo_VigenciaInicio2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21AutorizacaoConsumo_VigenciaInicio_To2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AutorizacaoConsumo_VigenciaInicio3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25AutorizacaoConsumo_VigenciaInicio_To3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFAutorizacaoConsumo_Codigo',fld:'vTFAUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFAutorizacaoConsumo_Codigo_To',fld:'vTFAUTORIZACAOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFAutorizacaoConsumo_VigenciaInicio',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFAutorizacaoConsumo_VigenciaInicio_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFAutorizacaoConsumo_VigenciaFim',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFAutorizacaoConsumo_VigenciaFim_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFAutorizacaoConsumo_UnidadeMedicaoNom',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',pic:'@!',nv:''},{av:'AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL',pic:'@!',nv:''},{av:'AV58TFAutorizacaoConsumo_Quantidade',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE',pic:'ZZ9',nv:0},{av:'AV59TFAutorizacaoConsumo_Quantidade_To',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO',pic:'ZZ9',nv:0},{av:'AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_QUANTIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_AUTORIZACAOCONSUMO_CODIGO.ONOPTIONCLICKED","{handler:'E12NA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AutorizacaoConsumo_VigenciaInicio1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17AutorizacaoConsumo_VigenciaInicio_To1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AutorizacaoConsumo_VigenciaInicio2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21AutorizacaoConsumo_VigenciaInicio_To2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AutorizacaoConsumo_VigenciaInicio3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25AutorizacaoConsumo_VigenciaInicio_To3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFAutorizacaoConsumo_Codigo',fld:'vTFAUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFAutorizacaoConsumo_Codigo_To',fld:'vTFAUTORIZACAOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFAutorizacaoConsumo_VigenciaInicio',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFAutorizacaoConsumo_VigenciaInicio_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFAutorizacaoConsumo_VigenciaFim',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFAutorizacaoConsumo_VigenciaFim_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFAutorizacaoConsumo_UnidadeMedicaoNom',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',pic:'@!',nv:''},{av:'AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL',pic:'@!',nv:''},{av:'AV58TFAutorizacaoConsumo_Quantidade',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE',pic:'ZZ9',nv:0},{av:'AV59TFAutorizacaoConsumo_Quantidade_To',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO',pic:'ZZ9',nv:0},{av:'AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_QUANTIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_autorizacaoconsumo_codigo_Activeeventkey',ctrl:'DDO_AUTORIZACAOCONSUMO_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_autorizacaoconsumo_codigo_Filteredtext_get',ctrl:'DDO_AUTORIZACAOCONSUMO_CODIGO',prop:'FilteredText_get'},{av:'Ddo_autorizacaoconsumo_codigo_Filteredtextto_get',ctrl:'DDO_AUTORIZACAOCONSUMO_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_autorizacaoconsumo_codigo_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_CODIGO',prop:'SortedStatus'},{av:'AV34TFAutorizacaoConsumo_Codigo',fld:'vTFAUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFAutorizacaoConsumo_Codigo_To',fld:'vTFAUTORIZACAOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_vigenciainicio_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_vigenciafim_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_unidademedicaonom_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_quantidade_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_QUANTIDADE',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATO_CODIGO.ONOPTIONCLICKED","{handler:'E13NA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AutorizacaoConsumo_VigenciaInicio1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17AutorizacaoConsumo_VigenciaInicio_To1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AutorizacaoConsumo_VigenciaInicio2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21AutorizacaoConsumo_VigenciaInicio_To2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AutorizacaoConsumo_VigenciaInicio3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25AutorizacaoConsumo_VigenciaInicio_To3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFAutorizacaoConsumo_Codigo',fld:'vTFAUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFAutorizacaoConsumo_Codigo_To',fld:'vTFAUTORIZACAOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFAutorizacaoConsumo_VigenciaInicio',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFAutorizacaoConsumo_VigenciaInicio_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFAutorizacaoConsumo_VigenciaFim',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFAutorizacaoConsumo_VigenciaFim_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFAutorizacaoConsumo_UnidadeMedicaoNom',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',pic:'@!',nv:''},{av:'AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL',pic:'@!',nv:''},{av:'AV58TFAutorizacaoConsumo_Quantidade',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE',pic:'ZZ9',nv:0},{av:'AV59TFAutorizacaoConsumo_Quantidade_To',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO',pic:'ZZ9',nv:0},{av:'AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_QUANTIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contrato_codigo_Activeeventkey',ctrl:'DDO_CONTRATO_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_contrato_codigo_Filteredtext_get',ctrl:'DDO_CONTRATO_CODIGO',prop:'FilteredText_get'},{av:'Ddo_contrato_codigo_Filteredtextto_get',ctrl:'DDO_CONTRATO_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_autorizacaoconsumo_codigo_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_CODIGO',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_vigenciainicio_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_vigenciafim_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_unidademedicaonom_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_quantidade_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_QUANTIDADE',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO.ONOPTIONCLICKED","{handler:'E14NA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AutorizacaoConsumo_VigenciaInicio1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17AutorizacaoConsumo_VigenciaInicio_To1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AutorizacaoConsumo_VigenciaInicio2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21AutorizacaoConsumo_VigenciaInicio_To2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AutorizacaoConsumo_VigenciaInicio3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25AutorizacaoConsumo_VigenciaInicio_To3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFAutorizacaoConsumo_Codigo',fld:'vTFAUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFAutorizacaoConsumo_Codigo_To',fld:'vTFAUTORIZACAOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFAutorizacaoConsumo_VigenciaInicio',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFAutorizacaoConsumo_VigenciaInicio_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFAutorizacaoConsumo_VigenciaFim',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFAutorizacaoConsumo_VigenciaFim_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFAutorizacaoConsumo_UnidadeMedicaoNom',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',pic:'@!',nv:''},{av:'AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL',pic:'@!',nv:''},{av:'AV58TFAutorizacaoConsumo_Quantidade',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE',pic:'ZZ9',nv:0},{av:'AV59TFAutorizacaoConsumo_Quantidade_To',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO',pic:'ZZ9',nv:0},{av:'AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_QUANTIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_autorizacaoconsumo_vigenciainicio_Activeeventkey',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO',prop:'ActiveEventKey'},{av:'Ddo_autorizacaoconsumo_vigenciainicio_Filteredtext_get',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO',prop:'FilteredText_get'},{av:'Ddo_autorizacaoconsumo_vigenciainicio_Filteredtextto_get',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_autorizacaoconsumo_vigenciainicio_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO',prop:'SortedStatus'},{av:'AV42TFAutorizacaoConsumo_VigenciaInicio',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFAutorizacaoConsumo_VigenciaInicio_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'Ddo_autorizacaoconsumo_codigo_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_vigenciafim_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_unidademedicaonom_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_quantidade_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_QUANTIDADE',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM.ONOPTIONCLICKED","{handler:'E15NA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AutorizacaoConsumo_VigenciaInicio1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17AutorizacaoConsumo_VigenciaInicio_To1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AutorizacaoConsumo_VigenciaInicio2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21AutorizacaoConsumo_VigenciaInicio_To2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AutorizacaoConsumo_VigenciaInicio3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25AutorizacaoConsumo_VigenciaInicio_To3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFAutorizacaoConsumo_Codigo',fld:'vTFAUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFAutorizacaoConsumo_Codigo_To',fld:'vTFAUTORIZACAOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFAutorizacaoConsumo_VigenciaInicio',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFAutorizacaoConsumo_VigenciaInicio_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFAutorizacaoConsumo_VigenciaFim',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFAutorizacaoConsumo_VigenciaFim_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFAutorizacaoConsumo_UnidadeMedicaoNom',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',pic:'@!',nv:''},{av:'AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL',pic:'@!',nv:''},{av:'AV58TFAutorizacaoConsumo_Quantidade',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE',pic:'ZZ9',nv:0},{av:'AV59TFAutorizacaoConsumo_Quantidade_To',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO',pic:'ZZ9',nv:0},{av:'AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_QUANTIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_autorizacaoconsumo_vigenciafim_Activeeventkey',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM',prop:'ActiveEventKey'},{av:'Ddo_autorizacaoconsumo_vigenciafim_Filteredtext_get',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM',prop:'FilteredText_get'},{av:'Ddo_autorizacaoconsumo_vigenciafim_Filteredtextto_get',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_autorizacaoconsumo_vigenciafim_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM',prop:'SortedStatus'},{av:'AV48TFAutorizacaoConsumo_VigenciaFim',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFAutorizacaoConsumo_VigenciaFim_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'Ddo_autorizacaoconsumo_codigo_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_vigenciainicio_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_unidademedicaonom_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_quantidade_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_QUANTIDADE',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM.ONOPTIONCLICKED","{handler:'E16NA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AutorizacaoConsumo_VigenciaInicio1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17AutorizacaoConsumo_VigenciaInicio_To1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AutorizacaoConsumo_VigenciaInicio2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21AutorizacaoConsumo_VigenciaInicio_To2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AutorizacaoConsumo_VigenciaInicio3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25AutorizacaoConsumo_VigenciaInicio_To3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFAutorizacaoConsumo_Codigo',fld:'vTFAUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFAutorizacaoConsumo_Codigo_To',fld:'vTFAUTORIZACAOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFAutorizacaoConsumo_VigenciaInicio',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFAutorizacaoConsumo_VigenciaInicio_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFAutorizacaoConsumo_VigenciaFim',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFAutorizacaoConsumo_VigenciaFim_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFAutorizacaoConsumo_UnidadeMedicaoNom',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',pic:'@!',nv:''},{av:'AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL',pic:'@!',nv:''},{av:'AV58TFAutorizacaoConsumo_Quantidade',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE',pic:'ZZ9',nv:0},{av:'AV59TFAutorizacaoConsumo_Quantidade_To',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO',pic:'ZZ9',nv:0},{av:'AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_QUANTIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_autorizacaoconsumo_unidademedicaonom_Activeeventkey',ctrl:'DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',prop:'ActiveEventKey'},{av:'Ddo_autorizacaoconsumo_unidademedicaonom_Filteredtext_get',ctrl:'DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',prop:'FilteredText_get'},{av:'Ddo_autorizacaoconsumo_unidademedicaonom_Selectedvalue_get',ctrl:'DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_autorizacaoconsumo_unidademedicaonom_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',prop:'SortedStatus'},{av:'AV54TFAutorizacaoConsumo_UnidadeMedicaoNom',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',pic:'@!',nv:''},{av:'AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL',pic:'@!',nv:''},{av:'Ddo_autorizacaoconsumo_codigo_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_vigenciainicio_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_vigenciafim_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_quantidade_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_QUANTIDADE',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_AUTORIZACAOCONSUMO_QUANTIDADE.ONOPTIONCLICKED","{handler:'E17NA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AutorizacaoConsumo_VigenciaInicio1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17AutorizacaoConsumo_VigenciaInicio_To1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AutorizacaoConsumo_VigenciaInicio2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21AutorizacaoConsumo_VigenciaInicio_To2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AutorizacaoConsumo_VigenciaInicio3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25AutorizacaoConsumo_VigenciaInicio_To3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFAutorizacaoConsumo_Codigo',fld:'vTFAUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFAutorizacaoConsumo_Codigo_To',fld:'vTFAUTORIZACAOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFAutorizacaoConsumo_VigenciaInicio',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFAutorizacaoConsumo_VigenciaInicio_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFAutorizacaoConsumo_VigenciaFim',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFAutorizacaoConsumo_VigenciaFim_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFAutorizacaoConsumo_UnidadeMedicaoNom',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',pic:'@!',nv:''},{av:'AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL',pic:'@!',nv:''},{av:'AV58TFAutorizacaoConsumo_Quantidade',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE',pic:'ZZ9',nv:0},{av:'AV59TFAutorizacaoConsumo_Quantidade_To',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO',pic:'ZZ9',nv:0},{av:'AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_QUANTIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_autorizacaoconsumo_quantidade_Activeeventkey',ctrl:'DDO_AUTORIZACAOCONSUMO_QUANTIDADE',prop:'ActiveEventKey'},{av:'Ddo_autorizacaoconsumo_quantidade_Filteredtext_get',ctrl:'DDO_AUTORIZACAOCONSUMO_QUANTIDADE',prop:'FilteredText_get'},{av:'Ddo_autorizacaoconsumo_quantidade_Filteredtextto_get',ctrl:'DDO_AUTORIZACAOCONSUMO_QUANTIDADE',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_autorizacaoconsumo_quantidade_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_QUANTIDADE',prop:'SortedStatus'},{av:'AV58TFAutorizacaoConsumo_Quantidade',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE',pic:'ZZ9',nv:0},{av:'AV59TFAutorizacaoConsumo_Quantidade_To',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO',pic:'ZZ9',nv:0},{av:'Ddo_autorizacaoconsumo_codigo_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contrato_codigo_Sortedstatus',ctrl:'DDO_CONTRATO_CODIGO',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_vigenciainicio_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_vigenciafim_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_unidademedicaonom_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E31NA2',iparms:[],oparms:[]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E18NA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AutorizacaoConsumo_VigenciaInicio1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17AutorizacaoConsumo_VigenciaInicio_To1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AutorizacaoConsumo_VigenciaInicio2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21AutorizacaoConsumo_VigenciaInicio_To2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AutorizacaoConsumo_VigenciaInicio3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25AutorizacaoConsumo_VigenciaInicio_To3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFAutorizacaoConsumo_Codigo',fld:'vTFAUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFAutorizacaoConsumo_Codigo_To',fld:'vTFAUTORIZACAOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFAutorizacaoConsumo_VigenciaInicio',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFAutorizacaoConsumo_VigenciaInicio_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFAutorizacaoConsumo_VigenciaFim',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFAutorizacaoConsumo_VigenciaFim_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFAutorizacaoConsumo_UnidadeMedicaoNom',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',pic:'@!',nv:''},{av:'AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL',pic:'@!',nv:''},{av:'AV58TFAutorizacaoConsumo_Quantidade',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE',pic:'ZZ9',nv:0},{av:'AV59TFAutorizacaoConsumo_Quantidade_To',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO',pic:'ZZ9',nv:0},{av:'AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_QUANTIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E24NA2',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E19NA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AutorizacaoConsumo_VigenciaInicio1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17AutorizacaoConsumo_VigenciaInicio_To1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AutorizacaoConsumo_VigenciaInicio2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21AutorizacaoConsumo_VigenciaInicio_To2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AutorizacaoConsumo_VigenciaInicio3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25AutorizacaoConsumo_VigenciaInicio_To3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFAutorizacaoConsumo_Codigo',fld:'vTFAUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFAutorizacaoConsumo_Codigo_To',fld:'vTFAUTORIZACAOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFAutorizacaoConsumo_VigenciaInicio',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFAutorizacaoConsumo_VigenciaInicio_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFAutorizacaoConsumo_VigenciaFim',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFAutorizacaoConsumo_VigenciaFim_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFAutorizacaoConsumo_UnidadeMedicaoNom',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',pic:'@!',nv:''},{av:'AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL',pic:'@!',nv:''},{av:'AV58TFAutorizacaoConsumo_Quantidade',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE',pic:'ZZ9',nv:0},{av:'AV59TFAutorizacaoConsumo_Quantidade_To',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO',pic:'ZZ9',nv:0},{av:'AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_QUANTIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AutorizacaoConsumo_VigenciaInicio2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21AutorizacaoConsumo_VigenciaInicio_To2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AutorizacaoConsumo_VigenciaInicio3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25AutorizacaoConsumo_VigenciaInicio_To3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AutorizacaoConsumo_VigenciaInicio1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17AutorizacaoConsumo_VigenciaInicio_To1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO2',prop:'Visible'},{av:'tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO3',prop:'Visible'},{av:'tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E25NA2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO1',prop:'Visible'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E26NA2',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E20NA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AutorizacaoConsumo_VigenciaInicio1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17AutorizacaoConsumo_VigenciaInicio_To1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AutorizacaoConsumo_VigenciaInicio2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21AutorizacaoConsumo_VigenciaInicio_To2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AutorizacaoConsumo_VigenciaInicio3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25AutorizacaoConsumo_VigenciaInicio_To3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFAutorizacaoConsumo_Codigo',fld:'vTFAUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFAutorizacaoConsumo_Codigo_To',fld:'vTFAUTORIZACAOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFAutorizacaoConsumo_VigenciaInicio',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFAutorizacaoConsumo_VigenciaInicio_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFAutorizacaoConsumo_VigenciaFim',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFAutorizacaoConsumo_VigenciaFim_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFAutorizacaoConsumo_UnidadeMedicaoNom',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',pic:'@!',nv:''},{av:'AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL',pic:'@!',nv:''},{av:'AV58TFAutorizacaoConsumo_Quantidade',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE',pic:'ZZ9',nv:0},{av:'AV59TFAutorizacaoConsumo_Quantidade_To',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO',pic:'ZZ9',nv:0},{av:'AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_QUANTIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AutorizacaoConsumo_VigenciaInicio2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21AutorizacaoConsumo_VigenciaInicio_To2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AutorizacaoConsumo_VigenciaInicio3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25AutorizacaoConsumo_VigenciaInicio_To3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AutorizacaoConsumo_VigenciaInicio1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17AutorizacaoConsumo_VigenciaInicio_To1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO2',prop:'Visible'},{av:'tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO3',prop:'Visible'},{av:'tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E27NA2',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E21NA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AutorizacaoConsumo_VigenciaInicio1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17AutorizacaoConsumo_VigenciaInicio_To1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AutorizacaoConsumo_VigenciaInicio2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21AutorizacaoConsumo_VigenciaInicio_To2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AutorizacaoConsumo_VigenciaInicio3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25AutorizacaoConsumo_VigenciaInicio_To3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFAutorizacaoConsumo_Codigo',fld:'vTFAUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFAutorizacaoConsumo_Codigo_To',fld:'vTFAUTORIZACAOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFAutorizacaoConsumo_VigenciaInicio',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFAutorizacaoConsumo_VigenciaInicio_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFAutorizacaoConsumo_VigenciaFim',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFAutorizacaoConsumo_VigenciaFim_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFAutorizacaoConsumo_UnidadeMedicaoNom',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',pic:'@!',nv:''},{av:'AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL',pic:'@!',nv:''},{av:'AV58TFAutorizacaoConsumo_Quantidade',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE',pic:'ZZ9',nv:0},{av:'AV59TFAutorizacaoConsumo_Quantidade_To',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO',pic:'ZZ9',nv:0},{av:'AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_QUANTIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AutorizacaoConsumo_VigenciaInicio2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21AutorizacaoConsumo_VigenciaInicio_To2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AutorizacaoConsumo_VigenciaInicio3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25AutorizacaoConsumo_VigenciaInicio_To3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AutorizacaoConsumo_VigenciaInicio1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17AutorizacaoConsumo_VigenciaInicio_To1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO2',prop:'Visible'},{av:'tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO3',prop:'Visible'},{av:'tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E28NA2',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO3',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E22NA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AutorizacaoConsumo_VigenciaInicio1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17AutorizacaoConsumo_VigenciaInicio_To1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AutorizacaoConsumo_VigenciaInicio2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21AutorizacaoConsumo_VigenciaInicio_To2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AutorizacaoConsumo_VigenciaInicio3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25AutorizacaoConsumo_VigenciaInicio_To3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV34TFAutorizacaoConsumo_Codigo',fld:'vTFAUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35TFAutorizacaoConsumo_Codigo_To',fld:'vTFAUTORIZACAOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV42TFAutorizacaoConsumo_VigenciaInicio',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV43TFAutorizacaoConsumo_VigenciaInicio_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV48TFAutorizacaoConsumo_VigenciaFim',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM',pic:'',nv:''},{av:'AV49TFAutorizacaoConsumo_VigenciaFim_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV54TFAutorizacaoConsumo_UnidadeMedicaoNom',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',pic:'@!',nv:''},{av:'AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL',pic:'@!',nv:''},{av:'AV58TFAutorizacaoConsumo_Quantidade',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE',pic:'ZZ9',nv:0},{av:'AV59TFAutorizacaoConsumo_Quantidade_To',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO',pic:'ZZ9',nv:0},{av:'AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40ddo_Contrato_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_QUANTIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV34TFAutorizacaoConsumo_Codigo',fld:'vTFAUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_autorizacaoconsumo_codigo_Filteredtext_set',ctrl:'DDO_AUTORIZACAOCONSUMO_CODIGO',prop:'FilteredText_set'},{av:'AV35TFAutorizacaoConsumo_Codigo_To',fld:'vTFAUTORIZACAOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_autorizacaoconsumo_codigo_Filteredtextto_set',ctrl:'DDO_AUTORIZACAOCONSUMO_CODIGO',prop:'FilteredTextTo_set'},{av:'AV38TFContrato_Codigo',fld:'vTFCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contrato_codigo_Filteredtext_set',ctrl:'DDO_CONTRATO_CODIGO',prop:'FilteredText_set'},{av:'AV39TFContrato_Codigo_To',fld:'vTFCONTRATO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contrato_codigo_Filteredtextto_set',ctrl:'DDO_CONTRATO_CODIGO',prop:'FilteredTextTo_set'},{av:'AV42TFAutorizacaoConsumo_VigenciaInicio',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO',pic:'',nv:''},{av:'Ddo_autorizacaoconsumo_vigenciainicio_Filteredtext_set',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO',prop:'FilteredText_set'},{av:'AV43TFAutorizacaoConsumo_VigenciaInicio_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'Ddo_autorizacaoconsumo_vigenciainicio_Filteredtextto_set',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO',prop:'FilteredTextTo_set'},{av:'AV48TFAutorizacaoConsumo_VigenciaFim',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM',pic:'',nv:''},{av:'Ddo_autorizacaoconsumo_vigenciafim_Filteredtext_set',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM',prop:'FilteredText_set'},{av:'AV49TFAutorizacaoConsumo_VigenciaFim_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'Ddo_autorizacaoconsumo_vigenciafim_Filteredtextto_set',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM',prop:'FilteredTextTo_set'},{av:'AV54TFAutorizacaoConsumo_UnidadeMedicaoNom',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',pic:'@!',nv:''},{av:'Ddo_autorizacaoconsumo_unidademedicaonom_Filteredtext_set',ctrl:'DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',prop:'FilteredText_set'},{av:'AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL',pic:'@!',nv:''},{av:'Ddo_autorizacaoconsumo_unidademedicaonom_Selectedvalue_set',ctrl:'DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',prop:'SelectedValue_set'},{av:'AV58TFAutorizacaoConsumo_Quantidade',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE',pic:'ZZ9',nv:0},{av:'Ddo_autorizacaoconsumo_quantidade_Filteredtext_set',ctrl:'DDO_AUTORIZACAOCONSUMO_QUANTIDADE',prop:'FilteredText_set'},{av:'AV59TFAutorizacaoConsumo_Quantidade_To',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO',pic:'ZZ9',nv:0},{av:'Ddo_autorizacaoconsumo_quantidade_Filteredtextto_set',ctrl:'DDO_AUTORIZACAOCONSUMO_QUANTIDADE',prop:'FilteredTextTo_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16AutorizacaoConsumo_VigenciaInicio1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO1',pic:'',nv:''},{av:'AV17AutorizacaoConsumo_VigenciaInicio_To1',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO1',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO1',prop:'Visible'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20AutorizacaoConsumo_VigenciaInicio2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO2',pic:'',nv:''},{av:'AV21AutorizacaoConsumo_VigenciaInicio_To2',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24AutorizacaoConsumo_VigenciaInicio3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO3',pic:'',nv:''},{av:'AV25AutorizacaoConsumo_VigenciaInicio_To3',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO2',prop:'Visible'},{av:'tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSAUTORIZACAOCONSUMO_VIGENCIAINICIO3',prop:'Visible'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E23NA2',iparms:[{av:'A1774AutorizacaoConsumo_Codigo',fld:'AUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_autorizacaoconsumo_codigo_Activeeventkey = "";
         Ddo_autorizacaoconsumo_codigo_Filteredtext_get = "";
         Ddo_autorizacaoconsumo_codigo_Filteredtextto_get = "";
         Ddo_contrato_codigo_Activeeventkey = "";
         Ddo_contrato_codigo_Filteredtext_get = "";
         Ddo_contrato_codigo_Filteredtextto_get = "";
         Ddo_autorizacaoconsumo_vigenciainicio_Activeeventkey = "";
         Ddo_autorizacaoconsumo_vigenciainicio_Filteredtext_get = "";
         Ddo_autorizacaoconsumo_vigenciainicio_Filteredtextto_get = "";
         Ddo_autorizacaoconsumo_vigenciafim_Activeeventkey = "";
         Ddo_autorizacaoconsumo_vigenciafim_Filteredtext_get = "";
         Ddo_autorizacaoconsumo_vigenciafim_Filteredtextto_get = "";
         Ddo_autorizacaoconsumo_unidademedicaonom_Activeeventkey = "";
         Ddo_autorizacaoconsumo_unidademedicaonom_Filteredtext_get = "";
         Ddo_autorizacaoconsumo_unidademedicaonom_Selectedvalue_get = "";
         Ddo_autorizacaoconsumo_quantidade_Activeeventkey = "";
         Ddo_autorizacaoconsumo_quantidade_Filteredtext_get = "";
         Ddo_autorizacaoconsumo_quantidade_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV16AutorizacaoConsumo_VigenciaInicio1 = DateTime.MinValue;
         AV17AutorizacaoConsumo_VigenciaInicio_To1 = DateTime.MinValue;
         AV19DynamicFiltersSelector2 = "";
         AV20AutorizacaoConsumo_VigenciaInicio2 = DateTime.MinValue;
         AV21AutorizacaoConsumo_VigenciaInicio_To2 = DateTime.MinValue;
         AV23DynamicFiltersSelector3 = "";
         AV24AutorizacaoConsumo_VigenciaInicio3 = DateTime.MinValue;
         AV25AutorizacaoConsumo_VigenciaInicio_To3 = DateTime.MinValue;
         AV42TFAutorizacaoConsumo_VigenciaInicio = DateTime.MinValue;
         AV43TFAutorizacaoConsumo_VigenciaInicio_To = DateTime.MinValue;
         AV48TFAutorizacaoConsumo_VigenciaFim = DateTime.MinValue;
         AV49TFAutorizacaoConsumo_VigenciaFim_To = DateTime.MinValue;
         AV54TFAutorizacaoConsumo_UnidadeMedicaoNom = "";
         AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel = "";
         AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace = "";
         AV40ddo_Contrato_CodigoTitleControlIdToReplace = "";
         AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace = "";
         AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace = "";
         AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace = "";
         AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace = "";
         AV90Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV61DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV33AutorizacaoConsumo_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV37Contrato_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV41AutorizacaoConsumo_VigenciaInicioTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV47AutorizacaoConsumo_VigenciaFimTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV53AutorizacaoConsumo_UnidadeMedicaoNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV57AutorizacaoConsumo_QuantidadeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_autorizacaoconsumo_codigo_Filteredtext_set = "";
         Ddo_autorizacaoconsumo_codigo_Filteredtextto_set = "";
         Ddo_autorizacaoconsumo_codigo_Sortedstatus = "";
         Ddo_contrato_codigo_Filteredtext_set = "";
         Ddo_contrato_codigo_Filteredtextto_set = "";
         Ddo_contrato_codigo_Sortedstatus = "";
         Ddo_autorizacaoconsumo_vigenciainicio_Filteredtext_set = "";
         Ddo_autorizacaoconsumo_vigenciainicio_Filteredtextto_set = "";
         Ddo_autorizacaoconsumo_vigenciainicio_Sortedstatus = "";
         Ddo_autorizacaoconsumo_vigenciafim_Filteredtext_set = "";
         Ddo_autorizacaoconsumo_vigenciafim_Filteredtextto_set = "";
         Ddo_autorizacaoconsumo_vigenciafim_Sortedstatus = "";
         Ddo_autorizacaoconsumo_unidademedicaonom_Filteredtext_set = "";
         Ddo_autorizacaoconsumo_unidademedicaonom_Selectedvalue_set = "";
         Ddo_autorizacaoconsumo_unidademedicaonom_Sortedstatus = "";
         Ddo_autorizacaoconsumo_quantidade_Filteredtext_set = "";
         Ddo_autorizacaoconsumo_quantidade_Filteredtextto_set = "";
         Ddo_autorizacaoconsumo_quantidade_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV44DDO_AutorizacaoConsumo_VigenciaInicioAuxDate = DateTime.MinValue;
         AV45DDO_AutorizacaoConsumo_VigenciaInicioAuxDateTo = DateTime.MinValue;
         AV50DDO_AutorizacaoConsumo_VigenciaFimAuxDate = DateTime.MinValue;
         AV51DDO_AutorizacaoConsumo_VigenciaFimAuxDateTo = DateTime.MinValue;
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         A83Contrato_DataVigenciaTermino = DateTime.MinValue;
         A82Contrato_DataVigenciaInicio = DateTime.MinValue;
         A1775AutorizacaoConsumo_VigenciaInicio = DateTime.MinValue;
         A1776AutorizacaoConsumo_VigenciaFim = DateTime.MinValue;
         A1778AutorizacaoConsumo_UnidadeMedicaoNom = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV86WWAutorizacaoConsumoDS_20_Tfautorizacaoconsumo_unidademedicaonom = "";
         AV67WWAutorizacaoConsumoDS_1_Dynamicfiltersselector1 = "";
         AV68WWAutorizacaoConsumoDS_2_Autorizacaoconsumo_vigenciainicio1 = DateTime.MinValue;
         AV69WWAutorizacaoConsumoDS_3_Autorizacaoconsumo_vigenciainicio_to1 = DateTime.MinValue;
         AV71WWAutorizacaoConsumoDS_5_Dynamicfiltersselector2 = "";
         AV72WWAutorizacaoConsumoDS_6_Autorizacaoconsumo_vigenciainicio2 = DateTime.MinValue;
         AV73WWAutorizacaoConsumoDS_7_Autorizacaoconsumo_vigenciainicio_to2 = DateTime.MinValue;
         AV75WWAutorizacaoConsumoDS_9_Dynamicfiltersselector3 = "";
         AV76WWAutorizacaoConsumoDS_10_Autorizacaoconsumo_vigenciainicio3 = DateTime.MinValue;
         AV77WWAutorizacaoConsumoDS_11_Autorizacaoconsumo_vigenciainicio_to3 = DateTime.MinValue;
         AV82WWAutorizacaoConsumoDS_16_Tfautorizacaoconsumo_vigenciainicio = DateTime.MinValue;
         AV83WWAutorizacaoConsumoDS_17_Tfautorizacaoconsumo_vigenciainicio_to = DateTime.MinValue;
         AV84WWAutorizacaoConsumoDS_18_Tfautorizacaoconsumo_vigenciafim = DateTime.MinValue;
         AV85WWAutorizacaoConsumoDS_19_Tfautorizacaoconsumo_vigenciafim_to = DateTime.MinValue;
         AV87WWAutorizacaoConsumoDS_21_Tfautorizacaoconsumo_unidademedicaonom_sel = "";
         AV86WWAutorizacaoConsumoDS_20_Tfautorizacaoconsumo_unidademedicaonom = "";
         H00NA2_A1779AutorizacaoConsumo_Quantidade = new short[1] ;
         H00NA2_A1778AutorizacaoConsumo_UnidadeMedicaoNom = new String[] {""} ;
         H00NA2_n1778AutorizacaoConsumo_UnidadeMedicaoNom = new bool[] {false} ;
         H00NA2_A1777AutorizacaoConsumo_UnidadeMedicaoCod = new int[1] ;
         H00NA2_A1776AutorizacaoConsumo_VigenciaFim = new DateTime[] {DateTime.MinValue} ;
         H00NA2_A1775AutorizacaoConsumo_VigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         H00NA2_A82Contrato_DataVigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         H00NA2_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         H00NA2_A74Contrato_Codigo = new int[1] ;
         H00NA2_A1774AutorizacaoConsumo_Codigo = new int[1] ;
         H00NA3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV30Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblAutorizacaoconsumotitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblDynamicfiltersautorizacaoconsumo_vigenciainicio_rangemiddletext3_Jsonclick = "";
         lblDynamicfiltersautorizacaoconsumo_vigenciainicio_rangemiddletext2_Jsonclick = "";
         lblDynamicfiltersautorizacaoconsumo_vigenciainicio_rangemiddletext1_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwautorizacaoconsumo__default(),
            new Object[][] {
                new Object[] {
               H00NA2_A1779AutorizacaoConsumo_Quantidade, H00NA2_A1778AutorizacaoConsumo_UnidadeMedicaoNom, H00NA2_n1778AutorizacaoConsumo_UnidadeMedicaoNom, H00NA2_A1777AutorizacaoConsumo_UnidadeMedicaoCod, H00NA2_A1776AutorizacaoConsumo_VigenciaFim, H00NA2_A1775AutorizacaoConsumo_VigenciaInicio, H00NA2_A82Contrato_DataVigenciaInicio, H00NA2_A83Contrato_DataVigenciaTermino, H00NA2_A74Contrato_Codigo, H00NA2_A1774AutorizacaoConsumo_Codigo
               }
               , new Object[] {
               H00NA3_AGRID_nRecordCount
               }
            }
         );
         AV90Pgmname = "WWAutorizacaoConsumo";
         /* GeneXus formulas. */
         AV90Pgmname = "WWAutorizacaoConsumo";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_94 ;
      private short nGXsfl_94_idx=1 ;
      private short AV13OrderedBy ;
      private short AV58TFAutorizacaoConsumo_Quantidade ;
      private short AV59TFAutorizacaoConsumo_Quantidade_To ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short A1779AutorizacaoConsumo_Quantidade ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_94_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV88WWAutorizacaoConsumoDS_22_Tfautorizacaoconsumo_quantidade ;
      private short AV89WWAutorizacaoConsumoDS_23_Tfautorizacaoconsumo_quantidade_to ;
      private short edtAutorizacaoConsumo_Codigo_Titleformat ;
      private short edtContrato_Codigo_Titleformat ;
      private short edtAutorizacaoConsumo_VigenciaInicio_Titleformat ;
      private short edtAutorizacaoConsumo_VigenciaFim_Titleformat ;
      private short edtAutorizacaoConsumo_UnidadeMedicaoNom_Titleformat ;
      private short edtAutorizacaoConsumo_Quantidade_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV34TFAutorizacaoConsumo_Codigo ;
      private int AV35TFAutorizacaoConsumo_Codigo_To ;
      private int AV38TFContrato_Codigo ;
      private int AV39TFContrato_Codigo_To ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_autorizacaoconsumo_unidademedicaonom_Datalistupdateminimumcharacters ;
      private int edtavTfautorizacaoconsumo_codigo_Visible ;
      private int edtavTfautorizacaoconsumo_codigo_to_Visible ;
      private int edtavTfcontrato_codigo_Visible ;
      private int edtavTfcontrato_codigo_to_Visible ;
      private int edtavTfautorizacaoconsumo_vigenciainicio_Visible ;
      private int edtavTfautorizacaoconsumo_vigenciainicio_to_Visible ;
      private int edtavTfautorizacaoconsumo_vigenciafim_Visible ;
      private int edtavTfautorizacaoconsumo_vigenciafim_to_Visible ;
      private int edtavTfautorizacaoconsumo_unidademedicaonom_Visible ;
      private int edtavTfautorizacaoconsumo_unidademedicaonom_sel_Visible ;
      private int edtavTfautorizacaoconsumo_quantidade_Visible ;
      private int edtavTfautorizacaoconsumo_quantidade_to_Visible ;
      private int edtavDdo_autorizacaoconsumo_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contrato_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_autorizacaoconsumo_vigenciainiciotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_autorizacaoconsumo_vigenciafimtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_autorizacaoconsumo_unidademedicaonomtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_autorizacaoconsumo_quantidadetitlecontrolidtoreplace_Visible ;
      private int A1774AutorizacaoConsumo_Codigo ;
      private int A74Contrato_Codigo ;
      private int A1777AutorizacaoConsumo_UnidadeMedicaoCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV78WWAutorizacaoConsumoDS_12_Tfautorizacaoconsumo_codigo ;
      private int AV79WWAutorizacaoConsumoDS_13_Tfautorizacaoconsumo_codigo_to ;
      private int AV80WWAutorizacaoConsumoDS_14_Tfcontrato_codigo ;
      private int AV81WWAutorizacaoConsumoDS_15_Tfcontrato_codigo_to ;
      private int edtavOrdereddsc_Visible ;
      private int AV62PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio1_Visible ;
      private int tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio2_Visible ;
      private int tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio3_Visible ;
      private int AV91GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV63GridCurrentPage ;
      private long AV64GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_autorizacaoconsumo_codigo_Activeeventkey ;
      private String Ddo_autorizacaoconsumo_codigo_Filteredtext_get ;
      private String Ddo_autorizacaoconsumo_codigo_Filteredtextto_get ;
      private String Ddo_contrato_codigo_Activeeventkey ;
      private String Ddo_contrato_codigo_Filteredtext_get ;
      private String Ddo_contrato_codigo_Filteredtextto_get ;
      private String Ddo_autorizacaoconsumo_vigenciainicio_Activeeventkey ;
      private String Ddo_autorizacaoconsumo_vigenciainicio_Filteredtext_get ;
      private String Ddo_autorizacaoconsumo_vigenciainicio_Filteredtextto_get ;
      private String Ddo_autorizacaoconsumo_vigenciafim_Activeeventkey ;
      private String Ddo_autorizacaoconsumo_vigenciafim_Filteredtext_get ;
      private String Ddo_autorizacaoconsumo_vigenciafim_Filteredtextto_get ;
      private String Ddo_autorizacaoconsumo_unidademedicaonom_Activeeventkey ;
      private String Ddo_autorizacaoconsumo_unidademedicaonom_Filteredtext_get ;
      private String Ddo_autorizacaoconsumo_unidademedicaonom_Selectedvalue_get ;
      private String Ddo_autorizacaoconsumo_quantidade_Activeeventkey ;
      private String Ddo_autorizacaoconsumo_quantidade_Filteredtext_get ;
      private String Ddo_autorizacaoconsumo_quantidade_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_94_idx="0001" ;
      private String AV54TFAutorizacaoConsumo_UnidadeMedicaoNom ;
      private String AV55TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel ;
      private String AV90Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_autorizacaoconsumo_codigo_Caption ;
      private String Ddo_autorizacaoconsumo_codigo_Tooltip ;
      private String Ddo_autorizacaoconsumo_codigo_Cls ;
      private String Ddo_autorizacaoconsumo_codigo_Filteredtext_set ;
      private String Ddo_autorizacaoconsumo_codigo_Filteredtextto_set ;
      private String Ddo_autorizacaoconsumo_codigo_Dropdownoptionstype ;
      private String Ddo_autorizacaoconsumo_codigo_Titlecontrolidtoreplace ;
      private String Ddo_autorizacaoconsumo_codigo_Sortedstatus ;
      private String Ddo_autorizacaoconsumo_codigo_Filtertype ;
      private String Ddo_autorizacaoconsumo_codigo_Sortasc ;
      private String Ddo_autorizacaoconsumo_codigo_Sortdsc ;
      private String Ddo_autorizacaoconsumo_codigo_Cleanfilter ;
      private String Ddo_autorizacaoconsumo_codigo_Rangefilterfrom ;
      private String Ddo_autorizacaoconsumo_codigo_Rangefilterto ;
      private String Ddo_autorizacaoconsumo_codigo_Searchbuttontext ;
      private String Ddo_contrato_codigo_Caption ;
      private String Ddo_contrato_codigo_Tooltip ;
      private String Ddo_contrato_codigo_Cls ;
      private String Ddo_contrato_codigo_Filteredtext_set ;
      private String Ddo_contrato_codigo_Filteredtextto_set ;
      private String Ddo_contrato_codigo_Dropdownoptionstype ;
      private String Ddo_contrato_codigo_Titlecontrolidtoreplace ;
      private String Ddo_contrato_codigo_Sortedstatus ;
      private String Ddo_contrato_codigo_Filtertype ;
      private String Ddo_contrato_codigo_Sortasc ;
      private String Ddo_contrato_codigo_Sortdsc ;
      private String Ddo_contrato_codigo_Cleanfilter ;
      private String Ddo_contrato_codigo_Rangefilterfrom ;
      private String Ddo_contrato_codigo_Rangefilterto ;
      private String Ddo_contrato_codigo_Searchbuttontext ;
      private String Ddo_autorizacaoconsumo_vigenciainicio_Caption ;
      private String Ddo_autorizacaoconsumo_vigenciainicio_Tooltip ;
      private String Ddo_autorizacaoconsumo_vigenciainicio_Cls ;
      private String Ddo_autorizacaoconsumo_vigenciainicio_Filteredtext_set ;
      private String Ddo_autorizacaoconsumo_vigenciainicio_Filteredtextto_set ;
      private String Ddo_autorizacaoconsumo_vigenciainicio_Dropdownoptionstype ;
      private String Ddo_autorizacaoconsumo_vigenciainicio_Titlecontrolidtoreplace ;
      private String Ddo_autorizacaoconsumo_vigenciainicio_Sortedstatus ;
      private String Ddo_autorizacaoconsumo_vigenciainicio_Filtertype ;
      private String Ddo_autorizacaoconsumo_vigenciainicio_Sortasc ;
      private String Ddo_autorizacaoconsumo_vigenciainicio_Sortdsc ;
      private String Ddo_autorizacaoconsumo_vigenciainicio_Cleanfilter ;
      private String Ddo_autorizacaoconsumo_vigenciainicio_Rangefilterfrom ;
      private String Ddo_autorizacaoconsumo_vigenciainicio_Rangefilterto ;
      private String Ddo_autorizacaoconsumo_vigenciainicio_Searchbuttontext ;
      private String Ddo_autorizacaoconsumo_vigenciafim_Caption ;
      private String Ddo_autorizacaoconsumo_vigenciafim_Tooltip ;
      private String Ddo_autorizacaoconsumo_vigenciafim_Cls ;
      private String Ddo_autorizacaoconsumo_vigenciafim_Filteredtext_set ;
      private String Ddo_autorizacaoconsumo_vigenciafim_Filteredtextto_set ;
      private String Ddo_autorizacaoconsumo_vigenciafim_Dropdownoptionstype ;
      private String Ddo_autorizacaoconsumo_vigenciafim_Titlecontrolidtoreplace ;
      private String Ddo_autorizacaoconsumo_vigenciafim_Sortedstatus ;
      private String Ddo_autorizacaoconsumo_vigenciafim_Filtertype ;
      private String Ddo_autorizacaoconsumo_vigenciafim_Sortasc ;
      private String Ddo_autorizacaoconsumo_vigenciafim_Sortdsc ;
      private String Ddo_autorizacaoconsumo_vigenciafim_Cleanfilter ;
      private String Ddo_autorizacaoconsumo_vigenciafim_Rangefilterfrom ;
      private String Ddo_autorizacaoconsumo_vigenciafim_Rangefilterto ;
      private String Ddo_autorizacaoconsumo_vigenciafim_Searchbuttontext ;
      private String Ddo_autorizacaoconsumo_unidademedicaonom_Caption ;
      private String Ddo_autorizacaoconsumo_unidademedicaonom_Tooltip ;
      private String Ddo_autorizacaoconsumo_unidademedicaonom_Cls ;
      private String Ddo_autorizacaoconsumo_unidademedicaonom_Filteredtext_set ;
      private String Ddo_autorizacaoconsumo_unidademedicaonom_Selectedvalue_set ;
      private String Ddo_autorizacaoconsumo_unidademedicaonom_Dropdownoptionstype ;
      private String Ddo_autorizacaoconsumo_unidademedicaonom_Titlecontrolidtoreplace ;
      private String Ddo_autorizacaoconsumo_unidademedicaonom_Sortedstatus ;
      private String Ddo_autorizacaoconsumo_unidademedicaonom_Filtertype ;
      private String Ddo_autorizacaoconsumo_unidademedicaonom_Datalisttype ;
      private String Ddo_autorizacaoconsumo_unidademedicaonom_Datalistproc ;
      private String Ddo_autorizacaoconsumo_unidademedicaonom_Sortasc ;
      private String Ddo_autorizacaoconsumo_unidademedicaonom_Sortdsc ;
      private String Ddo_autorizacaoconsumo_unidademedicaonom_Loadingdata ;
      private String Ddo_autorizacaoconsumo_unidademedicaonom_Cleanfilter ;
      private String Ddo_autorizacaoconsumo_unidademedicaonom_Noresultsfound ;
      private String Ddo_autorizacaoconsumo_unidademedicaonom_Searchbuttontext ;
      private String Ddo_autorizacaoconsumo_quantidade_Caption ;
      private String Ddo_autorizacaoconsumo_quantidade_Tooltip ;
      private String Ddo_autorizacaoconsumo_quantidade_Cls ;
      private String Ddo_autorizacaoconsumo_quantidade_Filteredtext_set ;
      private String Ddo_autorizacaoconsumo_quantidade_Filteredtextto_set ;
      private String Ddo_autorizacaoconsumo_quantidade_Dropdownoptionstype ;
      private String Ddo_autorizacaoconsumo_quantidade_Titlecontrolidtoreplace ;
      private String Ddo_autorizacaoconsumo_quantidade_Sortedstatus ;
      private String Ddo_autorizacaoconsumo_quantidade_Filtertype ;
      private String Ddo_autorizacaoconsumo_quantidade_Sortasc ;
      private String Ddo_autorizacaoconsumo_quantidade_Sortdsc ;
      private String Ddo_autorizacaoconsumo_quantidade_Cleanfilter ;
      private String Ddo_autorizacaoconsumo_quantidade_Rangefilterfrom ;
      private String Ddo_autorizacaoconsumo_quantidade_Rangefilterto ;
      private String Ddo_autorizacaoconsumo_quantidade_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfautorizacaoconsumo_codigo_Internalname ;
      private String edtavTfautorizacaoconsumo_codigo_Jsonclick ;
      private String edtavTfautorizacaoconsumo_codigo_to_Internalname ;
      private String edtavTfautorizacaoconsumo_codigo_to_Jsonclick ;
      private String edtavTfcontrato_codigo_Internalname ;
      private String edtavTfcontrato_codigo_Jsonclick ;
      private String edtavTfcontrato_codigo_to_Internalname ;
      private String edtavTfcontrato_codigo_to_Jsonclick ;
      private String edtavTfautorizacaoconsumo_vigenciainicio_Internalname ;
      private String edtavTfautorizacaoconsumo_vigenciainicio_Jsonclick ;
      private String edtavTfautorizacaoconsumo_vigenciainicio_to_Internalname ;
      private String edtavTfautorizacaoconsumo_vigenciainicio_to_Jsonclick ;
      private String divDdo_autorizacaoconsumo_vigenciainicioauxdates_Internalname ;
      private String edtavDdo_autorizacaoconsumo_vigenciainicioauxdate_Internalname ;
      private String edtavDdo_autorizacaoconsumo_vigenciainicioauxdate_Jsonclick ;
      private String edtavDdo_autorizacaoconsumo_vigenciainicioauxdateto_Internalname ;
      private String edtavDdo_autorizacaoconsumo_vigenciainicioauxdateto_Jsonclick ;
      private String edtavTfautorizacaoconsumo_vigenciafim_Internalname ;
      private String edtavTfautorizacaoconsumo_vigenciafim_Jsonclick ;
      private String edtavTfautorizacaoconsumo_vigenciafim_to_Internalname ;
      private String edtavTfautorizacaoconsumo_vigenciafim_to_Jsonclick ;
      private String divDdo_autorizacaoconsumo_vigenciafimauxdates_Internalname ;
      private String edtavDdo_autorizacaoconsumo_vigenciafimauxdate_Internalname ;
      private String edtavDdo_autorizacaoconsumo_vigenciafimauxdate_Jsonclick ;
      private String edtavDdo_autorizacaoconsumo_vigenciafimauxdateto_Internalname ;
      private String edtavDdo_autorizacaoconsumo_vigenciafimauxdateto_Jsonclick ;
      private String edtavTfautorizacaoconsumo_unidademedicaonom_Internalname ;
      private String edtavTfautorizacaoconsumo_unidademedicaonom_Jsonclick ;
      private String edtavTfautorizacaoconsumo_unidademedicaonom_sel_Internalname ;
      private String edtavTfautorizacaoconsumo_unidademedicaonom_sel_Jsonclick ;
      private String edtavTfautorizacaoconsumo_quantidade_Internalname ;
      private String edtavTfautorizacaoconsumo_quantidade_Jsonclick ;
      private String edtavTfautorizacaoconsumo_quantidade_to_Internalname ;
      private String edtavTfautorizacaoconsumo_quantidade_to_Jsonclick ;
      private String edtavDdo_autorizacaoconsumo_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contrato_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_autorizacaoconsumo_vigenciainiciotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_autorizacaoconsumo_vigenciafimtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_autorizacaoconsumo_unidademedicaonomtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_autorizacaoconsumo_quantidadetitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtAutorizacaoConsumo_Codigo_Internalname ;
      private String edtContrato_Codigo_Internalname ;
      private String edtContrato_DataVigenciaTermino_Internalname ;
      private String edtContrato_DataVigenciaInicio_Internalname ;
      private String edtAutorizacaoConsumo_VigenciaInicio_Internalname ;
      private String edtAutorizacaoConsumo_VigenciaFim_Internalname ;
      private String edtAutorizacaoConsumo_UnidadeMedicaoCod_Internalname ;
      private String A1778AutorizacaoConsumo_UnidadeMedicaoNom ;
      private String edtAutorizacaoConsumo_UnidadeMedicaoNom_Internalname ;
      private String edtAutorizacaoConsumo_Quantidade_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV86WWAutorizacaoConsumoDS_20_Tfautorizacaoconsumo_unidademedicaonom ;
      private String AV87WWAutorizacaoConsumoDS_21_Tfautorizacaoconsumo_unidademedicaonom_sel ;
      private String AV86WWAutorizacaoConsumoDS_20_Tfautorizacaoconsumo_unidademedicaonom ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavAutorizacaoconsumo_vigenciainicio1_Internalname ;
      private String edtavAutorizacaoconsumo_vigenciainicio_to1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavAutorizacaoconsumo_vigenciainicio2_Internalname ;
      private String edtavAutorizacaoconsumo_vigenciainicio_to2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavAutorizacaoconsumo_vigenciainicio3_Internalname ;
      private String edtavAutorizacaoconsumo_vigenciainicio_to3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_autorizacaoconsumo_codigo_Internalname ;
      private String Ddo_contrato_codigo_Internalname ;
      private String Ddo_autorizacaoconsumo_vigenciainicio_Internalname ;
      private String Ddo_autorizacaoconsumo_vigenciafim_Internalname ;
      private String Ddo_autorizacaoconsumo_unidademedicaonom_Internalname ;
      private String Ddo_autorizacaoconsumo_quantidade_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtAutorizacaoConsumo_Codigo_Title ;
      private String edtContrato_Codigo_Title ;
      private String edtAutorizacaoConsumo_VigenciaInicio_Title ;
      private String edtAutorizacaoConsumo_VigenciaFim_Title ;
      private String edtAutorizacaoConsumo_UnidadeMedicaoNom_Title ;
      private String edtAutorizacaoConsumo_Quantidade_Title ;
      private String tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio1_Internalname ;
      private String tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio2_Internalname ;
      private String tblTablemergeddynamicfiltersautorizacaoconsumo_vigenciainicio3_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblAutorizacaoconsumotitle_Internalname ;
      private String lblAutorizacaoconsumotitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavAutorizacaoconsumo_vigenciainicio3_Jsonclick ;
      private String lblDynamicfiltersautorizacaoconsumo_vigenciainicio_rangemiddletext3_Internalname ;
      private String lblDynamicfiltersautorizacaoconsumo_vigenciainicio_rangemiddletext3_Jsonclick ;
      private String edtavAutorizacaoconsumo_vigenciainicio_to3_Jsonclick ;
      private String edtavAutorizacaoconsumo_vigenciainicio2_Jsonclick ;
      private String lblDynamicfiltersautorizacaoconsumo_vigenciainicio_rangemiddletext2_Internalname ;
      private String lblDynamicfiltersautorizacaoconsumo_vigenciainicio_rangemiddletext2_Jsonclick ;
      private String edtavAutorizacaoconsumo_vigenciainicio_to2_Jsonclick ;
      private String edtavAutorizacaoconsumo_vigenciainicio1_Jsonclick ;
      private String lblDynamicfiltersautorizacaoconsumo_vigenciainicio_rangemiddletext1_Internalname ;
      private String lblDynamicfiltersautorizacaoconsumo_vigenciainicio_rangemiddletext1_Jsonclick ;
      private String edtavAutorizacaoconsumo_vigenciainicio_to1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_94_fel_idx="0001" ;
      private String ROClassString ;
      private String edtAutorizacaoConsumo_Codigo_Jsonclick ;
      private String edtContrato_Codigo_Jsonclick ;
      private String edtContrato_DataVigenciaTermino_Jsonclick ;
      private String edtContrato_DataVigenciaInicio_Jsonclick ;
      private String edtAutorizacaoConsumo_VigenciaInicio_Jsonclick ;
      private String edtAutorizacaoConsumo_VigenciaFim_Jsonclick ;
      private String edtAutorizacaoConsumo_UnidadeMedicaoCod_Jsonclick ;
      private String edtAutorizacaoConsumo_UnidadeMedicaoNom_Jsonclick ;
      private String edtAutorizacaoConsumo_Quantidade_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV16AutorizacaoConsumo_VigenciaInicio1 ;
      private DateTime AV17AutorizacaoConsumo_VigenciaInicio_To1 ;
      private DateTime AV20AutorizacaoConsumo_VigenciaInicio2 ;
      private DateTime AV21AutorizacaoConsumo_VigenciaInicio_To2 ;
      private DateTime AV24AutorizacaoConsumo_VigenciaInicio3 ;
      private DateTime AV25AutorizacaoConsumo_VigenciaInicio_To3 ;
      private DateTime AV42TFAutorizacaoConsumo_VigenciaInicio ;
      private DateTime AV43TFAutorizacaoConsumo_VigenciaInicio_To ;
      private DateTime AV48TFAutorizacaoConsumo_VigenciaFim ;
      private DateTime AV49TFAutorizacaoConsumo_VigenciaFim_To ;
      private DateTime AV44DDO_AutorizacaoConsumo_VigenciaInicioAuxDate ;
      private DateTime AV45DDO_AutorizacaoConsumo_VigenciaInicioAuxDateTo ;
      private DateTime AV50DDO_AutorizacaoConsumo_VigenciaFimAuxDate ;
      private DateTime AV51DDO_AutorizacaoConsumo_VigenciaFimAuxDateTo ;
      private DateTime A83Contrato_DataVigenciaTermino ;
      private DateTime A82Contrato_DataVigenciaInicio ;
      private DateTime A1775AutorizacaoConsumo_VigenciaInicio ;
      private DateTime A1776AutorizacaoConsumo_VigenciaFim ;
      private DateTime AV68WWAutorizacaoConsumoDS_2_Autorizacaoconsumo_vigenciainicio1 ;
      private DateTime AV69WWAutorizacaoConsumoDS_3_Autorizacaoconsumo_vigenciainicio_to1 ;
      private DateTime AV72WWAutorizacaoConsumoDS_6_Autorizacaoconsumo_vigenciainicio2 ;
      private DateTime AV73WWAutorizacaoConsumoDS_7_Autorizacaoconsumo_vigenciainicio_to2 ;
      private DateTime AV76WWAutorizacaoConsumoDS_10_Autorizacaoconsumo_vigenciainicio3 ;
      private DateTime AV77WWAutorizacaoConsumoDS_11_Autorizacaoconsumo_vigenciainicio_to3 ;
      private DateTime AV82WWAutorizacaoConsumoDS_16_Tfautorizacaoconsumo_vigenciainicio ;
      private DateTime AV83WWAutorizacaoConsumoDS_17_Tfautorizacaoconsumo_vigenciainicio_to ;
      private DateTime AV84WWAutorizacaoConsumoDS_18_Tfautorizacaoconsumo_vigenciafim ;
      private DateTime AV85WWAutorizacaoConsumoDS_19_Tfautorizacaoconsumo_vigenciafim_to ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_autorizacaoconsumo_codigo_Includesortasc ;
      private bool Ddo_autorizacaoconsumo_codigo_Includesortdsc ;
      private bool Ddo_autorizacaoconsumo_codigo_Includefilter ;
      private bool Ddo_autorizacaoconsumo_codigo_Filterisrange ;
      private bool Ddo_autorizacaoconsumo_codigo_Includedatalist ;
      private bool Ddo_contrato_codigo_Includesortasc ;
      private bool Ddo_contrato_codigo_Includesortdsc ;
      private bool Ddo_contrato_codigo_Includefilter ;
      private bool Ddo_contrato_codigo_Filterisrange ;
      private bool Ddo_contrato_codigo_Includedatalist ;
      private bool Ddo_autorizacaoconsumo_vigenciainicio_Includesortasc ;
      private bool Ddo_autorizacaoconsumo_vigenciainicio_Includesortdsc ;
      private bool Ddo_autorizacaoconsumo_vigenciainicio_Includefilter ;
      private bool Ddo_autorizacaoconsumo_vigenciainicio_Filterisrange ;
      private bool Ddo_autorizacaoconsumo_vigenciainicio_Includedatalist ;
      private bool Ddo_autorizacaoconsumo_vigenciafim_Includesortasc ;
      private bool Ddo_autorizacaoconsumo_vigenciafim_Includesortdsc ;
      private bool Ddo_autorizacaoconsumo_vigenciafim_Includefilter ;
      private bool Ddo_autorizacaoconsumo_vigenciafim_Filterisrange ;
      private bool Ddo_autorizacaoconsumo_vigenciafim_Includedatalist ;
      private bool Ddo_autorizacaoconsumo_unidademedicaonom_Includesortasc ;
      private bool Ddo_autorizacaoconsumo_unidademedicaonom_Includesortdsc ;
      private bool Ddo_autorizacaoconsumo_unidademedicaonom_Includefilter ;
      private bool Ddo_autorizacaoconsumo_unidademedicaonom_Filterisrange ;
      private bool Ddo_autorizacaoconsumo_unidademedicaonom_Includedatalist ;
      private bool Ddo_autorizacaoconsumo_quantidade_Includesortasc ;
      private bool Ddo_autorizacaoconsumo_quantidade_Includesortdsc ;
      private bool Ddo_autorizacaoconsumo_quantidade_Includefilter ;
      private bool Ddo_autorizacaoconsumo_quantidade_Filterisrange ;
      private bool Ddo_autorizacaoconsumo_quantidade_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1778AutorizacaoConsumo_UnidadeMedicaoNom ;
      private bool AV70WWAutorizacaoConsumoDS_4_Dynamicfiltersenabled2 ;
      private bool AV74WWAutorizacaoConsumoDS_8_Dynamicfiltersenabled3 ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV36ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace ;
      private String AV40ddo_Contrato_CodigoTitleControlIdToReplace ;
      private String AV46ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace ;
      private String AV52ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace ;
      private String AV56ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace ;
      private String AV60ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace ;
      private String AV67WWAutorizacaoConsumoDS_1_Dynamicfiltersselector1 ;
      private String AV71WWAutorizacaoConsumoDS_5_Dynamicfiltersselector2 ;
      private String AV75WWAutorizacaoConsumoDS_9_Dynamicfiltersselector3 ;
      private IGxSession AV30Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private short[] H00NA2_A1779AutorizacaoConsumo_Quantidade ;
      private String[] H00NA2_A1778AutorizacaoConsumo_UnidadeMedicaoNom ;
      private bool[] H00NA2_n1778AutorizacaoConsumo_UnidadeMedicaoNom ;
      private int[] H00NA2_A1777AutorizacaoConsumo_UnidadeMedicaoCod ;
      private DateTime[] H00NA2_A1776AutorizacaoConsumo_VigenciaFim ;
      private DateTime[] H00NA2_A1775AutorizacaoConsumo_VigenciaInicio ;
      private DateTime[] H00NA2_A82Contrato_DataVigenciaInicio ;
      private DateTime[] H00NA2_A83Contrato_DataVigenciaTermino ;
      private int[] H00NA2_A74Contrato_Codigo ;
      private int[] H00NA2_A1774AutorizacaoConsumo_Codigo ;
      private long[] H00NA3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV33AutorizacaoConsumo_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV37Contrato_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV41AutorizacaoConsumo_VigenciaInicioTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV47AutorizacaoConsumo_VigenciaFimTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV53AutorizacaoConsumo_UnidadeMedicaoNomTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV57AutorizacaoConsumo_QuantidadeTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV61DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwautorizacaoconsumo__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00NA2( IGxContext context ,
                                             String AV67WWAutorizacaoConsumoDS_1_Dynamicfiltersselector1 ,
                                             DateTime AV68WWAutorizacaoConsumoDS_2_Autorizacaoconsumo_vigenciainicio1 ,
                                             DateTime AV69WWAutorizacaoConsumoDS_3_Autorizacaoconsumo_vigenciainicio_to1 ,
                                             bool AV70WWAutorizacaoConsumoDS_4_Dynamicfiltersenabled2 ,
                                             String AV71WWAutorizacaoConsumoDS_5_Dynamicfiltersselector2 ,
                                             DateTime AV72WWAutorizacaoConsumoDS_6_Autorizacaoconsumo_vigenciainicio2 ,
                                             DateTime AV73WWAutorizacaoConsumoDS_7_Autorizacaoconsumo_vigenciainicio_to2 ,
                                             bool AV74WWAutorizacaoConsumoDS_8_Dynamicfiltersenabled3 ,
                                             String AV75WWAutorizacaoConsumoDS_9_Dynamicfiltersselector3 ,
                                             DateTime AV76WWAutorizacaoConsumoDS_10_Autorizacaoconsumo_vigenciainicio3 ,
                                             DateTime AV77WWAutorizacaoConsumoDS_11_Autorizacaoconsumo_vigenciainicio_to3 ,
                                             int AV78WWAutorizacaoConsumoDS_12_Tfautorizacaoconsumo_codigo ,
                                             int AV79WWAutorizacaoConsumoDS_13_Tfautorizacaoconsumo_codigo_to ,
                                             int AV80WWAutorizacaoConsumoDS_14_Tfcontrato_codigo ,
                                             int AV81WWAutorizacaoConsumoDS_15_Tfcontrato_codigo_to ,
                                             DateTime AV82WWAutorizacaoConsumoDS_16_Tfautorizacaoconsumo_vigenciainicio ,
                                             DateTime AV83WWAutorizacaoConsumoDS_17_Tfautorizacaoconsumo_vigenciainicio_to ,
                                             DateTime AV84WWAutorizacaoConsumoDS_18_Tfautorizacaoconsumo_vigenciafim ,
                                             DateTime AV85WWAutorizacaoConsumoDS_19_Tfautorizacaoconsumo_vigenciafim_to ,
                                             String AV87WWAutorizacaoConsumoDS_21_Tfautorizacaoconsumo_unidademedicaonom_sel ,
                                             String AV86WWAutorizacaoConsumoDS_20_Tfautorizacaoconsumo_unidademedicaonom ,
                                             short AV88WWAutorizacaoConsumoDS_22_Tfautorizacaoconsumo_quantidade ,
                                             short AV89WWAutorizacaoConsumoDS_23_Tfautorizacaoconsumo_quantidade_to ,
                                             DateTime A1775AutorizacaoConsumo_VigenciaInicio ,
                                             int A1774AutorizacaoConsumo_Codigo ,
                                             int A74Contrato_Codigo ,
                                             DateTime A1776AutorizacaoConsumo_VigenciaFim ,
                                             String A1778AutorizacaoConsumo_UnidadeMedicaoNom ,
                                             short A1779AutorizacaoConsumo_Quantidade ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [23] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[AutorizacaoConsumo_Quantidade], T2.[UnidadeMedicao_Nome] AS AutorizacaoConsumo_UnidadeMedicaoNom, T1.[AutorizacaoConsumo_UnidadeMedicaoCod] AS AutorizacaoConsumo_UnidadeMedicaoCod, T1.[AutorizacaoConsumo_VigenciaFim], T1.[AutorizacaoConsumo_VigenciaInicio], T3.[Contrato_DataVigenciaInicio], T3.[Contrato_DataVigenciaTermino], T1.[Contrato_Codigo], T1.[AutorizacaoConsumo_Codigo]";
         sFromString = " FROM (([AutorizacaoConsumo] T1 WITH (NOLOCK) INNER JOIN [UnidadeMedicao] T2 WITH (NOLOCK) ON T2.[UnidadeMedicao_Codigo] = T1.[AutorizacaoConsumo_UnidadeMedicaoCod]) INNER JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T1.[Contrato_Codigo])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV67WWAutorizacaoConsumoDS_1_Dynamicfiltersselector1, "AUTORIZACAOCONSUMO_VIGENCIAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV68WWAutorizacaoConsumoDS_2_Autorizacaoconsumo_vigenciainicio1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaInicio] >= @AV68WWAutorizacaoConsumoDS_2_Autorizacaoconsumo_vigenciainicio1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_VigenciaInicio] >= @AV68WWAutorizacaoConsumoDS_2_Autorizacaoconsumo_vigenciainicio1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV67WWAutorizacaoConsumoDS_1_Dynamicfiltersselector1, "AUTORIZACAOCONSUMO_VIGENCIAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV69WWAutorizacaoConsumoDS_3_Autorizacaoconsumo_vigenciainicio_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaInicio] <= @AV69WWAutorizacaoConsumoDS_3_Autorizacaoconsumo_vigenciainicio_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_VigenciaInicio] <= @AV69WWAutorizacaoConsumoDS_3_Autorizacaoconsumo_vigenciainicio_to1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( AV70WWAutorizacaoConsumoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV71WWAutorizacaoConsumoDS_5_Dynamicfiltersselector2, "AUTORIZACAOCONSUMO_VIGENCIAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV72WWAutorizacaoConsumoDS_6_Autorizacaoconsumo_vigenciainicio2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaInicio] >= @AV72WWAutorizacaoConsumoDS_6_Autorizacaoconsumo_vigenciainicio2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_VigenciaInicio] >= @AV72WWAutorizacaoConsumoDS_6_Autorizacaoconsumo_vigenciainicio2)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV70WWAutorizacaoConsumoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV71WWAutorizacaoConsumoDS_5_Dynamicfiltersselector2, "AUTORIZACAOCONSUMO_VIGENCIAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV73WWAutorizacaoConsumoDS_7_Autorizacaoconsumo_vigenciainicio_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaInicio] <= @AV73WWAutorizacaoConsumoDS_7_Autorizacaoconsumo_vigenciainicio_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_VigenciaInicio] <= @AV73WWAutorizacaoConsumoDS_7_Autorizacaoconsumo_vigenciainicio_to2)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV74WWAutorizacaoConsumoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV75WWAutorizacaoConsumoDS_9_Dynamicfiltersselector3, "AUTORIZACAOCONSUMO_VIGENCIAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV76WWAutorizacaoConsumoDS_10_Autorizacaoconsumo_vigenciainicio3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaInicio] >= @AV76WWAutorizacaoConsumoDS_10_Autorizacaoconsumo_vigenciainicio3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_VigenciaInicio] >= @AV76WWAutorizacaoConsumoDS_10_Autorizacaoconsumo_vigenciainicio3)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV74WWAutorizacaoConsumoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV75WWAutorizacaoConsumoDS_9_Dynamicfiltersselector3, "AUTORIZACAOCONSUMO_VIGENCIAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV77WWAutorizacaoConsumoDS_11_Autorizacaoconsumo_vigenciainicio_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaInicio] <= @AV77WWAutorizacaoConsumoDS_11_Autorizacaoconsumo_vigenciainicio_to3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_VigenciaInicio] <= @AV77WWAutorizacaoConsumoDS_11_Autorizacaoconsumo_vigenciainicio_to3)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! (0==AV78WWAutorizacaoConsumoDS_12_Tfautorizacaoconsumo_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_Codigo] >= @AV78WWAutorizacaoConsumoDS_12_Tfautorizacaoconsumo_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_Codigo] >= @AV78WWAutorizacaoConsumoDS_12_Tfautorizacaoconsumo_codigo)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ! (0==AV79WWAutorizacaoConsumoDS_13_Tfautorizacaoconsumo_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_Codigo] <= @AV79WWAutorizacaoConsumoDS_13_Tfautorizacaoconsumo_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_Codigo] <= @AV79WWAutorizacaoConsumoDS_13_Tfautorizacaoconsumo_codigo_to)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ! (0==AV80WWAutorizacaoConsumoDS_14_Tfcontrato_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] >= @AV80WWAutorizacaoConsumoDS_14_Tfcontrato_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] >= @AV80WWAutorizacaoConsumoDS_14_Tfcontrato_codigo)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! (0==AV81WWAutorizacaoConsumoDS_15_Tfcontrato_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] <= @AV81WWAutorizacaoConsumoDS_15_Tfcontrato_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] <= @AV81WWAutorizacaoConsumoDS_15_Tfcontrato_codigo_to)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV82WWAutorizacaoConsumoDS_16_Tfautorizacaoconsumo_vigenciainicio) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaInicio] >= @AV82WWAutorizacaoConsumoDS_16_Tfautorizacaoconsumo_vigenciainicio)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_VigenciaInicio] >= @AV82WWAutorizacaoConsumoDS_16_Tfautorizacaoconsumo_vigenciainicio)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ! (DateTime.MinValue==AV83WWAutorizacaoConsumoDS_17_Tfautorizacaoconsumo_vigenciainicio_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaInicio] <= @AV83WWAutorizacaoConsumoDS_17_Tfautorizacaoconsumo_vigenciainicio_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_VigenciaInicio] <= @AV83WWAutorizacaoConsumoDS_17_Tfautorizacaoconsumo_vigenciainicio_to)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( ! (DateTime.MinValue==AV84WWAutorizacaoConsumoDS_18_Tfautorizacaoconsumo_vigenciafim) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaFim] >= @AV84WWAutorizacaoConsumoDS_18_Tfautorizacaoconsumo_vigenciafim)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_VigenciaFim] >= @AV84WWAutorizacaoConsumoDS_18_Tfautorizacaoconsumo_vigenciafim)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( ! (DateTime.MinValue==AV85WWAutorizacaoConsumoDS_19_Tfautorizacaoconsumo_vigenciafim_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaFim] <= @AV85WWAutorizacaoConsumoDS_19_Tfautorizacaoconsumo_vigenciafim_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_VigenciaFim] <= @AV85WWAutorizacaoConsumoDS_19_Tfautorizacaoconsumo_vigenciafim_to)";
            }
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV87WWAutorizacaoConsumoDS_21_Tfautorizacaoconsumo_unidademedicaonom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWAutorizacaoConsumoDS_20_Tfautorizacaoconsumo_unidademedicaonom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] like @lV86WWAutorizacaoConsumoDS_20_Tfautorizacaoconsumo_unidademedicaonom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeMedicao_Nome] like @lV86WWAutorizacaoConsumoDS_20_Tfautorizacaoconsumo_unidademedicaonom)";
            }
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWAutorizacaoConsumoDS_21_Tfautorizacaoconsumo_unidademedicaonom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] = @AV87WWAutorizacaoConsumoDS_21_Tfautorizacaoconsumo_unidademedicaonom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeMedicao_Nome] = @AV87WWAutorizacaoConsumoDS_21_Tfautorizacaoconsumo_unidademedicaonom_sel)";
            }
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( ! (0==AV88WWAutorizacaoConsumoDS_22_Tfautorizacaoconsumo_quantidade) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_Quantidade] >= @AV88WWAutorizacaoConsumoDS_22_Tfautorizacaoconsumo_quantidade)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_Quantidade] >= @AV88WWAutorizacaoConsumoDS_22_Tfautorizacaoconsumo_quantidade)";
            }
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( ! (0==AV89WWAutorizacaoConsumoDS_23_Tfautorizacaoconsumo_quantidade_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_Quantidade] <= @AV89WWAutorizacaoConsumoDS_23_Tfautorizacaoconsumo_quantidade_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_Quantidade] <= @AV89WWAutorizacaoConsumoDS_23_Tfautorizacaoconsumo_quantidade_to)";
            }
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AutorizacaoConsumo_VigenciaInicio]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AutorizacaoConsumo_VigenciaInicio] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AutorizacaoConsumo_Codigo]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AutorizacaoConsumo_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Codigo]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AutorizacaoConsumo_VigenciaFim]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AutorizacaoConsumo_VigenciaFim] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[UnidadeMedicao_Nome]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[UnidadeMedicao_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AutorizacaoConsumo_Quantidade]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AutorizacaoConsumo_Quantidade] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AutorizacaoConsumo_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00NA3( IGxContext context ,
                                             String AV67WWAutorizacaoConsumoDS_1_Dynamicfiltersselector1 ,
                                             DateTime AV68WWAutorizacaoConsumoDS_2_Autorizacaoconsumo_vigenciainicio1 ,
                                             DateTime AV69WWAutorizacaoConsumoDS_3_Autorizacaoconsumo_vigenciainicio_to1 ,
                                             bool AV70WWAutorizacaoConsumoDS_4_Dynamicfiltersenabled2 ,
                                             String AV71WWAutorizacaoConsumoDS_5_Dynamicfiltersselector2 ,
                                             DateTime AV72WWAutorizacaoConsumoDS_6_Autorizacaoconsumo_vigenciainicio2 ,
                                             DateTime AV73WWAutorizacaoConsumoDS_7_Autorizacaoconsumo_vigenciainicio_to2 ,
                                             bool AV74WWAutorizacaoConsumoDS_8_Dynamicfiltersenabled3 ,
                                             String AV75WWAutorizacaoConsumoDS_9_Dynamicfiltersselector3 ,
                                             DateTime AV76WWAutorizacaoConsumoDS_10_Autorizacaoconsumo_vigenciainicio3 ,
                                             DateTime AV77WWAutorizacaoConsumoDS_11_Autorizacaoconsumo_vigenciainicio_to3 ,
                                             int AV78WWAutorizacaoConsumoDS_12_Tfautorizacaoconsumo_codigo ,
                                             int AV79WWAutorizacaoConsumoDS_13_Tfautorizacaoconsumo_codigo_to ,
                                             int AV80WWAutorizacaoConsumoDS_14_Tfcontrato_codigo ,
                                             int AV81WWAutorizacaoConsumoDS_15_Tfcontrato_codigo_to ,
                                             DateTime AV82WWAutorizacaoConsumoDS_16_Tfautorizacaoconsumo_vigenciainicio ,
                                             DateTime AV83WWAutorizacaoConsumoDS_17_Tfautorizacaoconsumo_vigenciainicio_to ,
                                             DateTime AV84WWAutorizacaoConsumoDS_18_Tfautorizacaoconsumo_vigenciafim ,
                                             DateTime AV85WWAutorizacaoConsumoDS_19_Tfautorizacaoconsumo_vigenciafim_to ,
                                             String AV87WWAutorizacaoConsumoDS_21_Tfautorizacaoconsumo_unidademedicaonom_sel ,
                                             String AV86WWAutorizacaoConsumoDS_20_Tfautorizacaoconsumo_unidademedicaonom ,
                                             short AV88WWAutorizacaoConsumoDS_22_Tfautorizacaoconsumo_quantidade ,
                                             short AV89WWAutorizacaoConsumoDS_23_Tfautorizacaoconsumo_quantidade_to ,
                                             DateTime A1775AutorizacaoConsumo_VigenciaInicio ,
                                             int A1774AutorizacaoConsumo_Codigo ,
                                             int A74Contrato_Codigo ,
                                             DateTime A1776AutorizacaoConsumo_VigenciaFim ,
                                             String A1778AutorizacaoConsumo_UnidadeMedicaoNom ,
                                             short A1779AutorizacaoConsumo_Quantidade ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [18] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM (([AutorizacaoConsumo] T1 WITH (NOLOCK) INNER JOIN [UnidadeMedicao] T3 WITH (NOLOCK) ON T3.[UnidadeMedicao_Codigo] = T1.[AutorizacaoConsumo_UnidadeMedicaoCod]) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo])";
         if ( ( StringUtil.StrCmp(AV67WWAutorizacaoConsumoDS_1_Dynamicfiltersselector1, "AUTORIZACAOCONSUMO_VIGENCIAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV68WWAutorizacaoConsumoDS_2_Autorizacaoconsumo_vigenciainicio1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaInicio] >= @AV68WWAutorizacaoConsumoDS_2_Autorizacaoconsumo_vigenciainicio1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_VigenciaInicio] >= @AV68WWAutorizacaoConsumoDS_2_Autorizacaoconsumo_vigenciainicio1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV67WWAutorizacaoConsumoDS_1_Dynamicfiltersselector1, "AUTORIZACAOCONSUMO_VIGENCIAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV69WWAutorizacaoConsumoDS_3_Autorizacaoconsumo_vigenciainicio_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaInicio] <= @AV69WWAutorizacaoConsumoDS_3_Autorizacaoconsumo_vigenciainicio_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_VigenciaInicio] <= @AV69WWAutorizacaoConsumoDS_3_Autorizacaoconsumo_vigenciainicio_to1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( AV70WWAutorizacaoConsumoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV71WWAutorizacaoConsumoDS_5_Dynamicfiltersselector2, "AUTORIZACAOCONSUMO_VIGENCIAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV72WWAutorizacaoConsumoDS_6_Autorizacaoconsumo_vigenciainicio2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaInicio] >= @AV72WWAutorizacaoConsumoDS_6_Autorizacaoconsumo_vigenciainicio2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_VigenciaInicio] >= @AV72WWAutorizacaoConsumoDS_6_Autorizacaoconsumo_vigenciainicio2)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV70WWAutorizacaoConsumoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV71WWAutorizacaoConsumoDS_5_Dynamicfiltersselector2, "AUTORIZACAOCONSUMO_VIGENCIAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV73WWAutorizacaoConsumoDS_7_Autorizacaoconsumo_vigenciainicio_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaInicio] <= @AV73WWAutorizacaoConsumoDS_7_Autorizacaoconsumo_vigenciainicio_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_VigenciaInicio] <= @AV73WWAutorizacaoConsumoDS_7_Autorizacaoconsumo_vigenciainicio_to2)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV74WWAutorizacaoConsumoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV75WWAutorizacaoConsumoDS_9_Dynamicfiltersselector3, "AUTORIZACAOCONSUMO_VIGENCIAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV76WWAutorizacaoConsumoDS_10_Autorizacaoconsumo_vigenciainicio3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaInicio] >= @AV76WWAutorizacaoConsumoDS_10_Autorizacaoconsumo_vigenciainicio3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_VigenciaInicio] >= @AV76WWAutorizacaoConsumoDS_10_Autorizacaoconsumo_vigenciainicio3)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV74WWAutorizacaoConsumoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV75WWAutorizacaoConsumoDS_9_Dynamicfiltersselector3, "AUTORIZACAOCONSUMO_VIGENCIAINICIO") == 0 ) && ( ! (DateTime.MinValue==AV77WWAutorizacaoConsumoDS_11_Autorizacaoconsumo_vigenciainicio_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaInicio] <= @AV77WWAutorizacaoConsumoDS_11_Autorizacaoconsumo_vigenciainicio_to3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_VigenciaInicio] <= @AV77WWAutorizacaoConsumoDS_11_Autorizacaoconsumo_vigenciainicio_to3)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! (0==AV78WWAutorizacaoConsumoDS_12_Tfautorizacaoconsumo_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_Codigo] >= @AV78WWAutorizacaoConsumoDS_12_Tfautorizacaoconsumo_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_Codigo] >= @AV78WWAutorizacaoConsumoDS_12_Tfautorizacaoconsumo_codigo)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ! (0==AV79WWAutorizacaoConsumoDS_13_Tfautorizacaoconsumo_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_Codigo] <= @AV79WWAutorizacaoConsumoDS_13_Tfautorizacaoconsumo_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_Codigo] <= @AV79WWAutorizacaoConsumoDS_13_Tfautorizacaoconsumo_codigo_to)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ! (0==AV80WWAutorizacaoConsumoDS_14_Tfcontrato_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] >= @AV80WWAutorizacaoConsumoDS_14_Tfcontrato_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] >= @AV80WWAutorizacaoConsumoDS_14_Tfcontrato_codigo)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! (0==AV81WWAutorizacaoConsumoDS_15_Tfcontrato_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] <= @AV81WWAutorizacaoConsumoDS_15_Tfcontrato_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] <= @AV81WWAutorizacaoConsumoDS_15_Tfcontrato_codigo_to)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV82WWAutorizacaoConsumoDS_16_Tfautorizacaoconsumo_vigenciainicio) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaInicio] >= @AV82WWAutorizacaoConsumoDS_16_Tfautorizacaoconsumo_vigenciainicio)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_VigenciaInicio] >= @AV82WWAutorizacaoConsumoDS_16_Tfautorizacaoconsumo_vigenciainicio)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! (DateTime.MinValue==AV83WWAutorizacaoConsumoDS_17_Tfautorizacaoconsumo_vigenciainicio_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaInicio] <= @AV83WWAutorizacaoConsumoDS_17_Tfautorizacaoconsumo_vigenciainicio_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_VigenciaInicio] <= @AV83WWAutorizacaoConsumoDS_17_Tfautorizacaoconsumo_vigenciainicio_to)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( ! (DateTime.MinValue==AV84WWAutorizacaoConsumoDS_18_Tfautorizacaoconsumo_vigenciafim) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaFim] >= @AV84WWAutorizacaoConsumoDS_18_Tfautorizacaoconsumo_vigenciafim)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_VigenciaFim] >= @AV84WWAutorizacaoConsumoDS_18_Tfautorizacaoconsumo_vigenciafim)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( ! (DateTime.MinValue==AV85WWAutorizacaoConsumoDS_19_Tfautorizacaoconsumo_vigenciafim_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaFim] <= @AV85WWAutorizacaoConsumoDS_19_Tfautorizacaoconsumo_vigenciafim_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_VigenciaFim] <= @AV85WWAutorizacaoConsumoDS_19_Tfautorizacaoconsumo_vigenciafim_to)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV87WWAutorizacaoConsumoDS_21_Tfautorizacaoconsumo_unidademedicaonom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWAutorizacaoConsumoDS_20_Tfautorizacaoconsumo_unidademedicaonom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[UnidadeMedicao_Nome] like @lV86WWAutorizacaoConsumoDS_20_Tfautorizacaoconsumo_unidademedicaonom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[UnidadeMedicao_Nome] like @lV86WWAutorizacaoConsumoDS_20_Tfautorizacaoconsumo_unidademedicaonom)";
            }
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWAutorizacaoConsumoDS_21_Tfautorizacaoconsumo_unidademedicaonom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[UnidadeMedicao_Nome] = @AV87WWAutorizacaoConsumoDS_21_Tfautorizacaoconsumo_unidademedicaonom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[UnidadeMedicao_Nome] = @AV87WWAutorizacaoConsumoDS_21_Tfautorizacaoconsumo_unidademedicaonom_sel)";
            }
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( ! (0==AV88WWAutorizacaoConsumoDS_22_Tfautorizacaoconsumo_quantidade) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_Quantidade] >= @AV88WWAutorizacaoConsumoDS_22_Tfautorizacaoconsumo_quantidade)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_Quantidade] >= @AV88WWAutorizacaoConsumoDS_22_Tfautorizacaoconsumo_quantidade)";
            }
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( ! (0==AV89WWAutorizacaoConsumoDS_23_Tfautorizacaoconsumo_quantidade_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_Quantidade] <= @AV89WWAutorizacaoConsumoDS_23_Tfautorizacaoconsumo_quantidade_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[AutorizacaoConsumo_Quantidade] <= @AV89WWAutorizacaoConsumoDS_23_Tfautorizacaoconsumo_quantidade_to)";
            }
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00NA2(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (short)dynConstraints[21] , (short)dynConstraints[22] , (DateTime)dynConstraints[23] , (int)dynConstraints[24] , (int)dynConstraints[25] , (DateTime)dynConstraints[26] , (String)dynConstraints[27] , (short)dynConstraints[28] , (short)dynConstraints[29] , (bool)dynConstraints[30] );
               case 1 :
                     return conditional_H00NA3(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (short)dynConstraints[21] , (short)dynConstraints[22] , (DateTime)dynConstraints[23] , (int)dynConstraints[24] , (int)dynConstraints[25] , (DateTime)dynConstraints[26] , (String)dynConstraints[27] , (short)dynConstraints[28] , (short)dynConstraints[29] , (bool)dynConstraints[30] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00NA2 ;
          prmH00NA2 = new Object[] {
          new Object[] {"@AV68WWAutorizacaoConsumoDS_2_Autorizacaoconsumo_vigenciainicio1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV69WWAutorizacaoConsumoDS_3_Autorizacaoconsumo_vigenciainicio_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV72WWAutorizacaoConsumoDS_6_Autorizacaoconsumo_vigenciainicio2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV73WWAutorizacaoConsumoDS_7_Autorizacaoconsumo_vigenciainicio_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV76WWAutorizacaoConsumoDS_10_Autorizacaoconsumo_vigenciainicio3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV77WWAutorizacaoConsumoDS_11_Autorizacaoconsumo_vigenciainicio_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV78WWAutorizacaoConsumoDS_12_Tfautorizacaoconsumo_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV79WWAutorizacaoConsumoDS_13_Tfautorizacaoconsumo_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV80WWAutorizacaoConsumoDS_14_Tfcontrato_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV81WWAutorizacaoConsumoDS_15_Tfcontrato_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV82WWAutorizacaoConsumoDS_16_Tfautorizacaoconsumo_vigenciainicio",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV83WWAutorizacaoConsumoDS_17_Tfautorizacaoconsumo_vigenciainicio_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV84WWAutorizacaoConsumoDS_18_Tfautorizacaoconsumo_vigenciafim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV85WWAutorizacaoConsumoDS_19_Tfautorizacaoconsumo_vigenciafim_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV86WWAutorizacaoConsumoDS_20_Tfautorizacaoconsumo_unidademedicaonom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV87WWAutorizacaoConsumoDS_21_Tfautorizacaoconsumo_unidademedicaonom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV88WWAutorizacaoConsumoDS_22_Tfautorizacaoconsumo_quantidade",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV89WWAutorizacaoConsumoDS_23_Tfautorizacaoconsumo_quantidade_to",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00NA3 ;
          prmH00NA3 = new Object[] {
          new Object[] {"@AV68WWAutorizacaoConsumoDS_2_Autorizacaoconsumo_vigenciainicio1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV69WWAutorizacaoConsumoDS_3_Autorizacaoconsumo_vigenciainicio_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV72WWAutorizacaoConsumoDS_6_Autorizacaoconsumo_vigenciainicio2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV73WWAutorizacaoConsumoDS_7_Autorizacaoconsumo_vigenciainicio_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV76WWAutorizacaoConsumoDS_10_Autorizacaoconsumo_vigenciainicio3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV77WWAutorizacaoConsumoDS_11_Autorizacaoconsumo_vigenciainicio_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV78WWAutorizacaoConsumoDS_12_Tfautorizacaoconsumo_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV79WWAutorizacaoConsumoDS_13_Tfautorizacaoconsumo_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV80WWAutorizacaoConsumoDS_14_Tfcontrato_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV81WWAutorizacaoConsumoDS_15_Tfcontrato_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV82WWAutorizacaoConsumoDS_16_Tfautorizacaoconsumo_vigenciainicio",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV83WWAutorizacaoConsumoDS_17_Tfautorizacaoconsumo_vigenciainicio_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV84WWAutorizacaoConsumoDS_18_Tfautorizacaoconsumo_vigenciafim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV85WWAutorizacaoConsumoDS_19_Tfautorizacaoconsumo_vigenciafim_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV86WWAutorizacaoConsumoDS_20_Tfautorizacaoconsumo_unidademedicaonom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV87WWAutorizacaoConsumoDS_21_Tfautorizacaoconsumo_unidademedicaonom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV88WWAutorizacaoConsumoDS_22_Tfautorizacaoconsumo_quantidade",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV89WWAutorizacaoConsumoDS_23_Tfautorizacaoconsumo_quantidade_to",SqlDbType.SmallInt,3,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00NA2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00NA2,11,0,true,false )
             ,new CursorDef("H00NA3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00NA3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(4) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(5) ;
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(6) ;
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(7) ;
                ((int[]) buf[8])[0] = rslt.getInt(8) ;
                ((int[]) buf[9])[0] = rslt.getInt(9) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[23]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[24]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[25]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[26]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[27]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[28]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[33]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[34]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[35]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[36]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[39]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[40]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[42]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[18]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[19]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[20]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[21]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[22]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[23]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[28]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[29]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[30]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[31]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[34]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[35]);
                }
                return;
       }
    }

 }

}
