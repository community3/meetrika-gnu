/*
               File: WWTecnologia
        Description:  Tecnologia
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:34:15.79
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwtecnologia : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwtecnologia( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwtecnologia( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavTecnologia_tipotecnologia1 = new GXCombobox();
         cmbTecnologia_TipoTecnologia = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_48 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_48_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_48_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV17Tecnologia_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Tecnologia_Nome1", AV17Tecnologia_Nome1);
               AV62Tecnologia_TipoTecnologia1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62Tecnologia_TipoTecnologia1", AV62Tecnologia_TipoTecnologia1);
               AV67TFTecnologia_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFTecnologia_Nome", AV67TFTecnologia_Nome);
               AV68TFTecnologia_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFTecnologia_Nome_Sel", AV68TFTecnologia_Nome_Sel);
               AV69ddo_Tecnologia_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ddo_Tecnologia_NomeTitleControlIdToReplace", AV69ddo_Tecnologia_NomeTitleControlIdToReplace);
               AV73ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace", AV73ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV72TFTecnologia_TipoTecnologia_Sels);
               AV96Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               A131Tecnologia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17Tecnologia_Nome1, AV62Tecnologia_TipoTecnologia1, AV67TFTecnologia_Nome, AV68TFTecnologia_Nome_Sel, AV69ddo_Tecnologia_NomeTitleControlIdToReplace, AV73ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace, AV72TFTecnologia_TipoTecnologia_Sels, AV96Pgmname, AV10GridState, A131Tecnologia_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA4N2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START4N2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117341599");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwtecnologia.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vTECNOLOGIA_NOME1", StringUtil.RTrim( AV17Tecnologia_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vTECNOLOGIA_TIPOTECNOLOGIA1", StringUtil.RTrim( AV62Tecnologia_TipoTecnologia1));
         GxWebStd.gx_hidden_field( context, "GXH_vTFTECNOLOGIA_NOME", StringUtil.RTrim( AV67TFTecnologia_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFTECNOLOGIA_NOME_SEL", StringUtil.RTrim( AV68TFTecnologia_Nome_Sel));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_48", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_48), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV76GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV77GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV74DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV74DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTECNOLOGIA_NOMETITLEFILTERDATA", AV66Tecnologia_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTECNOLOGIA_NOMETITLEFILTERDATA", AV66Tecnologia_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTECNOLOGIA_TIPOTECNOLOGIATITLEFILTERDATA", AV70Tecnologia_TipoTecnologiaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTECNOLOGIA_TIPOTECNOLOGIATITLEFILTERDATA", AV70Tecnologia_TipoTecnologiaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFTECNOLOGIA_TIPOTECNOLOGIA_SELS", AV72TFTecnologia_TipoTecnologia_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFTECNOLOGIA_TIPOTECNOLOGIA_SELS", AV72TFTecnologia_TipoTecnologia_Sels);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV96Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Caption", StringUtil.RTrim( Ddo_tecnologia_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Tooltip", StringUtil.RTrim( Ddo_tecnologia_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Cls", StringUtil.RTrim( Ddo_tecnologia_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_tecnologia_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_tecnologia_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_tecnologia_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_tecnologia_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_tecnologia_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_tecnologia_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Sortedstatus", StringUtil.RTrim( Ddo_tecnologia_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Includefilter", StringUtil.BoolToStr( Ddo_tecnologia_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Filtertype", StringUtil.RTrim( Ddo_tecnologia_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_tecnologia_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_tecnologia_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Datalisttype", StringUtil.RTrim( Ddo_tecnologia_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Datalistproc", StringUtil.RTrim( Ddo_tecnologia_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_tecnologia_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Sortasc", StringUtil.RTrim( Ddo_tecnologia_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Sortdsc", StringUtil.RTrim( Ddo_tecnologia_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Loadingdata", StringUtil.RTrim( Ddo_tecnologia_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Cleanfilter", StringUtil.RTrim( Ddo_tecnologia_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Noresultsfound", StringUtil.RTrim( Ddo_tecnologia_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_tecnologia_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Caption", StringUtil.RTrim( Ddo_tecnologia_tipotecnologia_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Tooltip", StringUtil.RTrim( Ddo_tecnologia_tipotecnologia_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Cls", StringUtil.RTrim( Ddo_tecnologia_tipotecnologia_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Selectedvalue_set", StringUtil.RTrim( Ddo_tecnologia_tipotecnologia_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Dropdownoptionstype", StringUtil.RTrim( Ddo_tecnologia_tipotecnologia_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_tecnologia_tipotecnologia_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Includesortasc", StringUtil.BoolToStr( Ddo_tecnologia_tipotecnologia_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Includesortdsc", StringUtil.BoolToStr( Ddo_tecnologia_tipotecnologia_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Sortedstatus", StringUtil.RTrim( Ddo_tecnologia_tipotecnologia_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Includefilter", StringUtil.BoolToStr( Ddo_tecnologia_tipotecnologia_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Includedatalist", StringUtil.BoolToStr( Ddo_tecnologia_tipotecnologia_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Datalisttype", StringUtil.RTrim( Ddo_tecnologia_tipotecnologia_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Allowmultipleselection", StringUtil.BoolToStr( Ddo_tecnologia_tipotecnologia_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Datalistfixedvalues", StringUtil.RTrim( Ddo_tecnologia_tipotecnologia_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Sortasc", StringUtil.RTrim( Ddo_tecnologia_tipotecnologia_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Sortdsc", StringUtil.RTrim( Ddo_tecnologia_tipotecnologia_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Cleanfilter", StringUtil.RTrim( Ddo_tecnologia_tipotecnologia_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Searchbuttontext", StringUtil.RTrim( Ddo_tecnologia_tipotecnologia_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Activeeventkey", StringUtil.RTrim( Ddo_tecnologia_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_tecnologia_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_tecnologia_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Activeeventkey", StringUtil.RTrim( Ddo_tecnologia_tipotecnologia_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Selectedvalue_get", StringUtil.RTrim( Ddo_tecnologia_tipotecnologia_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE4N2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT4N2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwtecnologia.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWTecnologia" ;
      }

      public override String GetPgmdesc( )
      {
         return " Tecnologia" ;
      }

      protected void WB4N0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_4N2( true) ;
         }
         else
         {
            wb_table1_2_4N2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_4N2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'" + sGXsfl_48_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftecnologia_nome_Internalname, StringUtil.RTrim( AV67TFTecnologia_Nome), StringUtil.RTrim( context.localUtil.Format( AV67TFTecnologia_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftecnologia_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTftecnologia_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWTecnologia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',false,'" + sGXsfl_48_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftecnologia_nome_sel_Internalname, StringUtil.RTrim( AV68TFTecnologia_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV68TFTecnologia_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,60);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftecnologia_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTftecnologia_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWTecnologia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_TECNOLOGIA_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'" + sGXsfl_48_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_tecnologia_nometitlecontrolidtoreplace_Internalname, AV69ddo_Tecnologia_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,62);\"", 0, edtavDdo_tecnologia_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWTecnologia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_TECNOLOGIA_TIPOTECNOLOGIAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'" + sGXsfl_48_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_tecnologia_tipotecnologiatitlecontrolidtoreplace_Internalname, AV73ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,64);\"", 0, edtavDdo_tecnologia_tipotecnologiatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWTecnologia.htm");
         }
         wbLoad = true;
      }

      protected void START4N2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Tecnologia", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP4N0( ) ;
      }

      protected void WS4N2( )
      {
         START4N2( ) ;
         EVT4N2( ) ;
      }

      protected void EVT4N2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E114N2 */
                              E114N2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_TECNOLOGIA_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E124N2 */
                              E124N2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_TECNOLOGIA_TIPOTECNOLOGIA.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E134N2 */
                              E134N2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E144N2 */
                              E144N2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E154N2 */
                              E154N2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E164N2 */
                              E164N2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E174N2 */
                              E174N2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 18), "'DOBTNASSOCIATION'") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 18), "'DOBTNASSOCIATION'") == 0 ) )
                           {
                              nGXsfl_48_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_48_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_48_idx), 4, 0)), 4, "0");
                              SubsflControlProps_482( ) ;
                              AV31Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV31Update)) ? AV93Update_GXI : context.convertURL( context.PathToRelativeUrl( AV31Update))));
                              AV32Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)) ? AV94Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV32Delete))));
                              A131Tecnologia_Codigo = (int)(context.localUtil.CToN( cgiGet( edtTecnologia_Codigo_Internalname), ",", "."));
                              A132Tecnologia_Nome = StringUtil.Upper( cgiGet( edtTecnologia_Nome_Internalname));
                              cmbTecnologia_TipoTecnologia.Name = cmbTecnologia_TipoTecnologia_Internalname;
                              cmbTecnologia_TipoTecnologia.CurrentValue = cgiGet( cmbTecnologia_TipoTecnologia_Internalname);
                              A355Tecnologia_TipoTecnologia = cgiGet( cmbTecnologia_TipoTecnologia_Internalname);
                              n355Tecnologia_TipoTecnologia = false;
                              AV63btnAssociation = cgiGet( edtavBtnassociation_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBtnassociation_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV63btnAssociation)) ? AV95Btnassociation_GXI : context.convertURL( context.PathToRelativeUrl( AV63btnAssociation))));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E184N2 */
                                    E184N2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E194N2 */
                                    E194N2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E204N2 */
                                    E204N2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "'DOBTNASSOCIATION'") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E214N2 */
                                    E214N2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tecnologia_nome1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTECNOLOGIA_NOME1"), AV17Tecnologia_Nome1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tecnologia_tipotecnologia1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTECNOLOGIA_TIPOTECNOLOGIA1"), AV62Tecnologia_TipoTecnologia1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tftecnologia_nome Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTECNOLOGIA_NOME"), AV67TFTecnologia_Nome) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tftecnologia_nome_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTECNOLOGIA_NOME_SEL"), AV68TFTecnologia_Nome_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE4N2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA4N2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("TECNOLOGIA_NOME", "Nome", 0);
            cmbavDynamicfiltersselector1.addItem("TECNOLOGIA_TIPOTECNOLOGIA", "Tipo", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavTecnologia_tipotecnologia1.Name = "vTECNOLOGIA_TIPOTECNOLOGIA1";
            cmbavTecnologia_tipotecnologia1.WebTags = "";
            cmbavTecnologia_tipotecnologia1.addItem("", "Todos", 0);
            cmbavTecnologia_tipotecnologia1.addItem("OS", "Sistema Operacional", 0);
            cmbavTecnologia_tipotecnologia1.addItem("LNG", "Linguagem", 0);
            cmbavTecnologia_tipotecnologia1.addItem("DBM", "Banco de Dados", 0);
            cmbavTecnologia_tipotecnologia1.addItem("SFT", "Software", 0);
            cmbavTecnologia_tipotecnologia1.addItem("SRV", "Servidor", 0);
            cmbavTecnologia_tipotecnologia1.addItem("DSK", "Desktop", 0);
            cmbavTecnologia_tipotecnologia1.addItem("NTB", "Notebook", 0);
            cmbavTecnologia_tipotecnologia1.addItem("PRN", "Impresora", 0);
            cmbavTecnologia_tipotecnologia1.addItem("HRD", "Hardware", 0);
            if ( cmbavTecnologia_tipotecnologia1.ItemCount > 0 )
            {
               AV62Tecnologia_TipoTecnologia1 = cmbavTecnologia_tipotecnologia1.getValidValue(AV62Tecnologia_TipoTecnologia1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62Tecnologia_TipoTecnologia1", AV62Tecnologia_TipoTecnologia1);
            }
            GXCCtl = "TECNOLOGIA_TIPOTECNOLOGIA_" + sGXsfl_48_idx;
            cmbTecnologia_TipoTecnologia.Name = GXCCtl;
            cmbTecnologia_TipoTecnologia.WebTags = "";
            cmbTecnologia_TipoTecnologia.addItem("", "(Nenhum)", 0);
            cmbTecnologia_TipoTecnologia.addItem("OS", "Sistema Operacional", 0);
            cmbTecnologia_TipoTecnologia.addItem("LNG", "Linguagem", 0);
            cmbTecnologia_TipoTecnologia.addItem("DBM", "Banco de Dados", 0);
            cmbTecnologia_TipoTecnologia.addItem("SFT", "Software", 0);
            cmbTecnologia_TipoTecnologia.addItem("SRV", "Servidor", 0);
            cmbTecnologia_TipoTecnologia.addItem("DSK", "Desktop", 0);
            cmbTecnologia_TipoTecnologia.addItem("NTB", "Notebook", 0);
            cmbTecnologia_TipoTecnologia.addItem("PRN", "Impresora", 0);
            cmbTecnologia_TipoTecnologia.addItem("HRD", "Hardware", 0);
            if ( cmbTecnologia_TipoTecnologia.ItemCount > 0 )
            {
               A355Tecnologia_TipoTecnologia = cmbTecnologia_TipoTecnologia.getValidValue(A355Tecnologia_TipoTecnologia);
               n355Tecnologia_TipoTecnologia = false;
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_482( ) ;
         while ( nGXsfl_48_idx <= nRC_GXsfl_48 )
         {
            sendrow_482( ) ;
            nGXsfl_48_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_48_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_48_idx+1));
            sGXsfl_48_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_48_idx), 4, 0)), 4, "0");
            SubsflControlProps_482( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       String AV17Tecnologia_Nome1 ,
                                       String AV62Tecnologia_TipoTecnologia1 ,
                                       String AV67TFTecnologia_Nome ,
                                       String AV68TFTecnologia_Nome_Sel ,
                                       String AV69ddo_Tecnologia_NomeTitleControlIdToReplace ,
                                       String AV73ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace ,
                                       IGxCollection AV72TFTecnologia_TipoTecnologia_Sels ,
                                       String AV96Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       int A131Tecnologia_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF4N2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_TECNOLOGIA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A131Tecnologia_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "TECNOLOGIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A131Tecnologia_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_TECNOLOGIA_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A132Tecnologia_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "TECNOLOGIA_NOME", StringUtil.RTrim( A132Tecnologia_Nome));
         GxWebStd.gx_hidden_field( context, "gxhash_TECNOLOGIA_TIPOTECNOLOGIA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A355Tecnologia_TipoTecnologia, ""))));
         GxWebStd.gx_hidden_field( context, "TECNOLOGIA_TIPOTECNOLOGIA", StringUtil.RTrim( A355Tecnologia_TipoTecnologia));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavTecnologia_tipotecnologia1.ItemCount > 0 )
         {
            AV62Tecnologia_TipoTecnologia1 = cmbavTecnologia_tipotecnologia1.getValidValue(AV62Tecnologia_TipoTecnologia1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62Tecnologia_TipoTecnologia1", AV62Tecnologia_TipoTecnologia1);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF4N2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV96Pgmname = "WWTecnologia";
         context.Gx_err = 0;
      }

      protected void RF4N2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 48;
         /* Execute user event: E194N2 */
         E194N2 ();
         nGXsfl_48_idx = 1;
         sGXsfl_48_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_48_idx), 4, 0)), 4, "0");
         SubsflControlProps_482( ) ;
         nGXsfl_48_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_482( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 A355Tecnologia_TipoTecnologia ,
                                                 AV92WWTecnologiaDS_6_Tftecnologia_tipotecnologia_sels ,
                                                 AV87WWTecnologiaDS_1_Dynamicfiltersselector1 ,
                                                 AV88WWTecnologiaDS_2_Tecnologia_nome1 ,
                                                 AV89WWTecnologiaDS_3_Tecnologia_tipotecnologia1 ,
                                                 AV91WWTecnologiaDS_5_Tftecnologia_nome_sel ,
                                                 AV90WWTecnologiaDS_4_Tftecnologia_nome ,
                                                 AV92WWTecnologiaDS_6_Tftecnologia_tipotecnologia_sels.Count ,
                                                 A132Tecnologia_Nome ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT,
                                                 TypeConstants.BOOLEAN
                                                 }
            });
            lV88WWTecnologiaDS_2_Tecnologia_nome1 = StringUtil.PadR( StringUtil.RTrim( AV88WWTecnologiaDS_2_Tecnologia_nome1), 50, "%");
            lV90WWTecnologiaDS_4_Tftecnologia_nome = StringUtil.PadR( StringUtil.RTrim( AV90WWTecnologiaDS_4_Tftecnologia_nome), 50, "%");
            /* Using cursor H004N2 */
            pr_default.execute(0, new Object[] {lV88WWTecnologiaDS_2_Tecnologia_nome1, AV89WWTecnologiaDS_3_Tecnologia_tipotecnologia1, lV90WWTecnologiaDS_4_Tftecnologia_nome, AV91WWTecnologiaDS_5_Tftecnologia_nome_sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_48_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A355Tecnologia_TipoTecnologia = H004N2_A355Tecnologia_TipoTecnologia[0];
               n355Tecnologia_TipoTecnologia = H004N2_n355Tecnologia_TipoTecnologia[0];
               A132Tecnologia_Nome = H004N2_A132Tecnologia_Nome[0];
               A131Tecnologia_Codigo = H004N2_A131Tecnologia_Codigo[0];
               /* Execute user event: E204N2 */
               E204N2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 48;
            WB4N0( ) ;
         }
         nGXsfl_48_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV87WWTecnologiaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV88WWTecnologiaDS_2_Tecnologia_nome1 = AV17Tecnologia_Nome1;
         AV89WWTecnologiaDS_3_Tecnologia_tipotecnologia1 = AV62Tecnologia_TipoTecnologia1;
         AV90WWTecnologiaDS_4_Tftecnologia_nome = AV67TFTecnologia_Nome;
         AV91WWTecnologiaDS_5_Tftecnologia_nome_sel = AV68TFTecnologia_Nome_Sel;
         AV92WWTecnologiaDS_6_Tftecnologia_tipotecnologia_sels = AV72TFTecnologia_TipoTecnologia_Sels;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A355Tecnologia_TipoTecnologia ,
                                              AV92WWTecnologiaDS_6_Tftecnologia_tipotecnologia_sels ,
                                              AV87WWTecnologiaDS_1_Dynamicfiltersselector1 ,
                                              AV88WWTecnologiaDS_2_Tecnologia_nome1 ,
                                              AV89WWTecnologiaDS_3_Tecnologia_tipotecnologia1 ,
                                              AV91WWTecnologiaDS_5_Tftecnologia_nome_sel ,
                                              AV90WWTecnologiaDS_4_Tftecnologia_nome ,
                                              AV92WWTecnologiaDS_6_Tftecnologia_tipotecnologia_sels.Count ,
                                              A132Tecnologia_Nome ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.BOOLEAN
                                              }
         });
         lV88WWTecnologiaDS_2_Tecnologia_nome1 = StringUtil.PadR( StringUtil.RTrim( AV88WWTecnologiaDS_2_Tecnologia_nome1), 50, "%");
         lV90WWTecnologiaDS_4_Tftecnologia_nome = StringUtil.PadR( StringUtil.RTrim( AV90WWTecnologiaDS_4_Tftecnologia_nome), 50, "%");
         /* Using cursor H004N3 */
         pr_default.execute(1, new Object[] {lV88WWTecnologiaDS_2_Tecnologia_nome1, AV89WWTecnologiaDS_3_Tecnologia_tipotecnologia1, lV90WWTecnologiaDS_4_Tftecnologia_nome, AV91WWTecnologiaDS_5_Tftecnologia_nome_sel});
         GRID_nRecordCount = H004N3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV87WWTecnologiaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV88WWTecnologiaDS_2_Tecnologia_nome1 = AV17Tecnologia_Nome1;
         AV89WWTecnologiaDS_3_Tecnologia_tipotecnologia1 = AV62Tecnologia_TipoTecnologia1;
         AV90WWTecnologiaDS_4_Tftecnologia_nome = AV67TFTecnologia_Nome;
         AV91WWTecnologiaDS_5_Tftecnologia_nome_sel = AV68TFTecnologia_Nome_Sel;
         AV92WWTecnologiaDS_6_Tftecnologia_tipotecnologia_sels = AV72TFTecnologia_TipoTecnologia_Sels;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17Tecnologia_Nome1, AV62Tecnologia_TipoTecnologia1, AV67TFTecnologia_Nome, AV68TFTecnologia_Nome_Sel, AV69ddo_Tecnologia_NomeTitleControlIdToReplace, AV73ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace, AV72TFTecnologia_TipoTecnologia_Sels, AV96Pgmname, AV10GridState, A131Tecnologia_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV87WWTecnologiaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV88WWTecnologiaDS_2_Tecnologia_nome1 = AV17Tecnologia_Nome1;
         AV89WWTecnologiaDS_3_Tecnologia_tipotecnologia1 = AV62Tecnologia_TipoTecnologia1;
         AV90WWTecnologiaDS_4_Tftecnologia_nome = AV67TFTecnologia_Nome;
         AV91WWTecnologiaDS_5_Tftecnologia_nome_sel = AV68TFTecnologia_Nome_Sel;
         AV92WWTecnologiaDS_6_Tftecnologia_tipotecnologia_sels = AV72TFTecnologia_TipoTecnologia_Sels;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17Tecnologia_Nome1, AV62Tecnologia_TipoTecnologia1, AV67TFTecnologia_Nome, AV68TFTecnologia_Nome_Sel, AV69ddo_Tecnologia_NomeTitleControlIdToReplace, AV73ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace, AV72TFTecnologia_TipoTecnologia_Sels, AV96Pgmname, AV10GridState, A131Tecnologia_Codigo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV87WWTecnologiaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV88WWTecnologiaDS_2_Tecnologia_nome1 = AV17Tecnologia_Nome1;
         AV89WWTecnologiaDS_3_Tecnologia_tipotecnologia1 = AV62Tecnologia_TipoTecnologia1;
         AV90WWTecnologiaDS_4_Tftecnologia_nome = AV67TFTecnologia_Nome;
         AV91WWTecnologiaDS_5_Tftecnologia_nome_sel = AV68TFTecnologia_Nome_Sel;
         AV92WWTecnologiaDS_6_Tftecnologia_tipotecnologia_sels = AV72TFTecnologia_TipoTecnologia_Sels;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17Tecnologia_Nome1, AV62Tecnologia_TipoTecnologia1, AV67TFTecnologia_Nome, AV68TFTecnologia_Nome_Sel, AV69ddo_Tecnologia_NomeTitleControlIdToReplace, AV73ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace, AV72TFTecnologia_TipoTecnologia_Sels, AV96Pgmname, AV10GridState, A131Tecnologia_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV87WWTecnologiaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV88WWTecnologiaDS_2_Tecnologia_nome1 = AV17Tecnologia_Nome1;
         AV89WWTecnologiaDS_3_Tecnologia_tipotecnologia1 = AV62Tecnologia_TipoTecnologia1;
         AV90WWTecnologiaDS_4_Tftecnologia_nome = AV67TFTecnologia_Nome;
         AV91WWTecnologiaDS_5_Tftecnologia_nome_sel = AV68TFTecnologia_Nome_Sel;
         AV92WWTecnologiaDS_6_Tftecnologia_tipotecnologia_sels = AV72TFTecnologia_TipoTecnologia_Sels;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17Tecnologia_Nome1, AV62Tecnologia_TipoTecnologia1, AV67TFTecnologia_Nome, AV68TFTecnologia_Nome_Sel, AV69ddo_Tecnologia_NomeTitleControlIdToReplace, AV73ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace, AV72TFTecnologia_TipoTecnologia_Sels, AV96Pgmname, AV10GridState, A131Tecnologia_Codigo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV87WWTecnologiaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV88WWTecnologiaDS_2_Tecnologia_nome1 = AV17Tecnologia_Nome1;
         AV89WWTecnologiaDS_3_Tecnologia_tipotecnologia1 = AV62Tecnologia_TipoTecnologia1;
         AV90WWTecnologiaDS_4_Tftecnologia_nome = AV67TFTecnologia_Nome;
         AV91WWTecnologiaDS_5_Tftecnologia_nome_sel = AV68TFTecnologia_Nome_Sel;
         AV92WWTecnologiaDS_6_Tftecnologia_tipotecnologia_sels = AV72TFTecnologia_TipoTecnologia_Sels;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17Tecnologia_Nome1, AV62Tecnologia_TipoTecnologia1, AV67TFTecnologia_Nome, AV68TFTecnologia_Nome_Sel, AV69ddo_Tecnologia_NomeTitleControlIdToReplace, AV73ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace, AV72TFTecnologia_TipoTecnologia_Sels, AV96Pgmname, AV10GridState, A131Tecnologia_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUP4N0( )
      {
         /* Before Start, stand alone formulas. */
         AV96Pgmname = "WWTecnologia";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E184N2 */
         E184N2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV74DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vTECNOLOGIA_NOMETITLEFILTERDATA"), AV66Tecnologia_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vTECNOLOGIA_TIPOTECNOLOGIATITLEFILTERDATA"), AV70Tecnologia_TipoTecnologiaTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            AV17Tecnologia_Nome1 = StringUtil.Upper( cgiGet( edtavTecnologia_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Tecnologia_Nome1", AV17Tecnologia_Nome1);
            cmbavTecnologia_tipotecnologia1.Name = cmbavTecnologia_tipotecnologia1_Internalname;
            cmbavTecnologia_tipotecnologia1.CurrentValue = cgiGet( cmbavTecnologia_tipotecnologia1_Internalname);
            AV62Tecnologia_TipoTecnologia1 = cgiGet( cmbavTecnologia_tipotecnologia1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62Tecnologia_TipoTecnologia1", AV62Tecnologia_TipoTecnologia1);
            AV67TFTecnologia_Nome = StringUtil.Upper( cgiGet( edtavTftecnologia_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFTecnologia_Nome", AV67TFTecnologia_Nome);
            AV68TFTecnologia_Nome_Sel = StringUtil.Upper( cgiGet( edtavTftecnologia_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFTecnologia_Nome_Sel", AV68TFTecnologia_Nome_Sel);
            AV69ddo_Tecnologia_NomeTitleControlIdToReplace = cgiGet( edtavDdo_tecnologia_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ddo_Tecnologia_NomeTitleControlIdToReplace", AV69ddo_Tecnologia_NomeTitleControlIdToReplace);
            AV73ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace = cgiGet( edtavDdo_tecnologia_tipotecnologiatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace", AV73ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_48 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_48"), ",", "."));
            AV76GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV77GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_tecnologia_nome_Caption = cgiGet( "DDO_TECNOLOGIA_NOME_Caption");
            Ddo_tecnologia_nome_Tooltip = cgiGet( "DDO_TECNOLOGIA_NOME_Tooltip");
            Ddo_tecnologia_nome_Cls = cgiGet( "DDO_TECNOLOGIA_NOME_Cls");
            Ddo_tecnologia_nome_Filteredtext_set = cgiGet( "DDO_TECNOLOGIA_NOME_Filteredtext_set");
            Ddo_tecnologia_nome_Selectedvalue_set = cgiGet( "DDO_TECNOLOGIA_NOME_Selectedvalue_set");
            Ddo_tecnologia_nome_Dropdownoptionstype = cgiGet( "DDO_TECNOLOGIA_NOME_Dropdownoptionstype");
            Ddo_tecnologia_nome_Titlecontrolidtoreplace = cgiGet( "DDO_TECNOLOGIA_NOME_Titlecontrolidtoreplace");
            Ddo_tecnologia_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_TECNOLOGIA_NOME_Includesortasc"));
            Ddo_tecnologia_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_TECNOLOGIA_NOME_Includesortdsc"));
            Ddo_tecnologia_nome_Sortedstatus = cgiGet( "DDO_TECNOLOGIA_NOME_Sortedstatus");
            Ddo_tecnologia_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_TECNOLOGIA_NOME_Includefilter"));
            Ddo_tecnologia_nome_Filtertype = cgiGet( "DDO_TECNOLOGIA_NOME_Filtertype");
            Ddo_tecnologia_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_TECNOLOGIA_NOME_Filterisrange"));
            Ddo_tecnologia_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_TECNOLOGIA_NOME_Includedatalist"));
            Ddo_tecnologia_nome_Datalisttype = cgiGet( "DDO_TECNOLOGIA_NOME_Datalisttype");
            Ddo_tecnologia_nome_Datalistproc = cgiGet( "DDO_TECNOLOGIA_NOME_Datalistproc");
            Ddo_tecnologia_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_TECNOLOGIA_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_tecnologia_nome_Sortasc = cgiGet( "DDO_TECNOLOGIA_NOME_Sortasc");
            Ddo_tecnologia_nome_Sortdsc = cgiGet( "DDO_TECNOLOGIA_NOME_Sortdsc");
            Ddo_tecnologia_nome_Loadingdata = cgiGet( "DDO_TECNOLOGIA_NOME_Loadingdata");
            Ddo_tecnologia_nome_Cleanfilter = cgiGet( "DDO_TECNOLOGIA_NOME_Cleanfilter");
            Ddo_tecnologia_nome_Noresultsfound = cgiGet( "DDO_TECNOLOGIA_NOME_Noresultsfound");
            Ddo_tecnologia_nome_Searchbuttontext = cgiGet( "DDO_TECNOLOGIA_NOME_Searchbuttontext");
            Ddo_tecnologia_tipotecnologia_Caption = cgiGet( "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Caption");
            Ddo_tecnologia_tipotecnologia_Tooltip = cgiGet( "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Tooltip");
            Ddo_tecnologia_tipotecnologia_Cls = cgiGet( "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Cls");
            Ddo_tecnologia_tipotecnologia_Selectedvalue_set = cgiGet( "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Selectedvalue_set");
            Ddo_tecnologia_tipotecnologia_Dropdownoptionstype = cgiGet( "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Dropdownoptionstype");
            Ddo_tecnologia_tipotecnologia_Titlecontrolidtoreplace = cgiGet( "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Titlecontrolidtoreplace");
            Ddo_tecnologia_tipotecnologia_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Includesortasc"));
            Ddo_tecnologia_tipotecnologia_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Includesortdsc"));
            Ddo_tecnologia_tipotecnologia_Sortedstatus = cgiGet( "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Sortedstatus");
            Ddo_tecnologia_tipotecnologia_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Includefilter"));
            Ddo_tecnologia_tipotecnologia_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Includedatalist"));
            Ddo_tecnologia_tipotecnologia_Datalisttype = cgiGet( "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Datalisttype");
            Ddo_tecnologia_tipotecnologia_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Allowmultipleselection"));
            Ddo_tecnologia_tipotecnologia_Datalistfixedvalues = cgiGet( "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Datalistfixedvalues");
            Ddo_tecnologia_tipotecnologia_Sortasc = cgiGet( "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Sortasc");
            Ddo_tecnologia_tipotecnologia_Sortdsc = cgiGet( "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Sortdsc");
            Ddo_tecnologia_tipotecnologia_Cleanfilter = cgiGet( "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Cleanfilter");
            Ddo_tecnologia_tipotecnologia_Searchbuttontext = cgiGet( "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_tecnologia_nome_Activeeventkey = cgiGet( "DDO_TECNOLOGIA_NOME_Activeeventkey");
            Ddo_tecnologia_nome_Filteredtext_get = cgiGet( "DDO_TECNOLOGIA_NOME_Filteredtext_get");
            Ddo_tecnologia_nome_Selectedvalue_get = cgiGet( "DDO_TECNOLOGIA_NOME_Selectedvalue_get");
            Ddo_tecnologia_tipotecnologia_Activeeventkey = cgiGet( "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Activeeventkey");
            Ddo_tecnologia_tipotecnologia_Selectedvalue_get = cgiGet( "DDO_TECNOLOGIA_TIPOTECNOLOGIA_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTECNOLOGIA_NOME1"), AV17Tecnologia_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTECNOLOGIA_TIPOTECNOLOGIA1"), AV62Tecnologia_TipoTecnologia1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTECNOLOGIA_NOME"), AV67TFTecnologia_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTECNOLOGIA_NOME_SEL"), AV68TFTecnologia_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E184N2 */
         E184N2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E184N2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         AV62Tecnologia_TipoTecnologia1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62Tecnologia_TipoTecnologia1", AV62Tecnologia_TipoTecnologia1);
         AV15DynamicFiltersSelector1 = "TECNOLOGIA_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavTftecnologia_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTftecnologia_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftecnologia_nome_Visible), 5, 0)));
         edtavTftecnologia_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTftecnologia_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftecnologia_nome_sel_Visible), 5, 0)));
         Ddo_tecnologia_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_Tecnologia_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tecnologia_nome_Internalname, "TitleControlIdToReplace", Ddo_tecnologia_nome_Titlecontrolidtoreplace);
         AV69ddo_Tecnologia_NomeTitleControlIdToReplace = Ddo_tecnologia_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ddo_Tecnologia_NomeTitleControlIdToReplace", AV69ddo_Tecnologia_NomeTitleControlIdToReplace);
         edtavDdo_tecnologia_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_tecnologia_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_tecnologia_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_tecnologia_tipotecnologia_Titlecontrolidtoreplace = subGrid_Internalname+"_Tecnologia_TipoTecnologia";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tecnologia_tipotecnologia_Internalname, "TitleControlIdToReplace", Ddo_tecnologia_tipotecnologia_Titlecontrolidtoreplace);
         AV73ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace = Ddo_tecnologia_tipotecnologia_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace", AV73ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace);
         edtavDdo_tecnologia_tipotecnologiatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_tecnologia_tipotecnologiatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_tecnologia_tipotecnologiatitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Tecnologia";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Tecnologia", 0);
         cmbavOrderedby.addItem("2", "Tipo", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S142 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV74DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV74DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E194N2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV66Tecnologia_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV70Tecnologia_TipoTecnologiaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtTecnologia_Nome_Titleformat = 2;
         edtTecnologia_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV69ddo_Tecnologia_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTecnologia_Nome_Internalname, "Title", edtTecnologia_Nome_Title);
         cmbTecnologia_TipoTecnologia_Titleformat = 2;
         cmbTecnologia_TipoTecnologia.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Tipo", AV73ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbTecnologia_TipoTecnologia_Internalname, "Title", cmbTecnologia_TipoTecnologia.Title.Text);
         AV76GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV76GridCurrentPage), 10, 0)));
         AV77GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV77GridPageCount), 10, 0)));
         imgInsert_Visible = (AV6WWPContext.gxTpr_Userehadministradorgam||AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Visible), 5, 0)));
         edtavUpdate_Visible = (AV6WWPContext.gxTpr_Userehadministradorgam||AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUpdate_Visible), 5, 0)));
         edtavDelete_Visible = (AV6WWPContext.gxTpr_Userehadministradorgam||AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDelete_Visible), 5, 0)));
         AV87WWTecnologiaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV88WWTecnologiaDS_2_Tecnologia_nome1 = AV17Tecnologia_Nome1;
         AV89WWTecnologiaDS_3_Tecnologia_tipotecnologia1 = AV62Tecnologia_TipoTecnologia1;
         AV90WWTecnologiaDS_4_Tftecnologia_nome = AV67TFTecnologia_Nome;
         AV91WWTecnologiaDS_5_Tftecnologia_nome_sel = AV68TFTecnologia_Nome_Sel;
         AV92WWTecnologiaDS_6_Tftecnologia_tipotecnologia_sels = AV72TFTecnologia_TipoTecnologia_Sels;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV66Tecnologia_NomeTitleFilterData", AV66Tecnologia_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV70Tecnologia_TipoTecnologiaTitleFilterData", AV70Tecnologia_TipoTecnologiaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E114N2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV75PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV75PageToGo) ;
         }
      }

      protected void E124N2( )
      {
         /* Ddo_tecnologia_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_tecnologia_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tecnologia_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tecnologia_nome_Internalname, "SortedStatus", Ddo_tecnologia_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tecnologia_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tecnologia_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tecnologia_nome_Internalname, "SortedStatus", Ddo_tecnologia_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tecnologia_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV67TFTecnologia_Nome = Ddo_tecnologia_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFTecnologia_Nome", AV67TFTecnologia_Nome);
            AV68TFTecnologia_Nome_Sel = Ddo_tecnologia_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFTecnologia_Nome_Sel", AV68TFTecnologia_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E134N2( )
      {
         /* Ddo_tecnologia_tipotecnologia_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_tecnologia_tipotecnologia_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tecnologia_tipotecnologia_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tecnologia_tipotecnologia_Internalname, "SortedStatus", Ddo_tecnologia_tipotecnologia_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tecnologia_tipotecnologia_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_tecnologia_tipotecnologia_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tecnologia_tipotecnologia_Internalname, "SortedStatus", Ddo_tecnologia_tipotecnologia_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tecnologia_tipotecnologia_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV71TFTecnologia_TipoTecnologia_SelsJson = Ddo_tecnologia_tipotecnologia_Selectedvalue_get;
            AV72TFTecnologia_TipoTecnologia_Sels.FromJSonString(AV71TFTecnologia_TipoTecnologia_SelsJson);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV72TFTecnologia_TipoTecnologia_Sels", AV72TFTecnologia_TipoTecnologia_Sels);
      }

      private void E204N2( )
      {
         /* Grid_Load Routine */
         AV31Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV31Update);
         AV93Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("tecnologia.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A131Tecnologia_Codigo);
         AV32Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV32Delete);
         AV94Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("tecnologia.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A131Tecnologia_Codigo);
         AV63btnAssociation = context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavBtnassociation_Internalname, AV63btnAssociation);
         AV95Btnassociation_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( )));
         edtavBtnassociation_Tooltiptext = "Clique para associar os Ambientes Tecnol�gicos desta Tecnolog�a";
         edtTecnologia_Nome_Link = formatLink("viewtecnologia.aspx") + "?" + UrlEncode("" +A131Tecnologia_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         AV31Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV31Update);
         AV93Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         AV32Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV32Delete);
         AV94Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 48;
         }
         sendrow_482( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_48_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(48, GridRow);
         }
      }

      protected void E144N2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E174N2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E154N2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV72TFTecnologia_TipoTecnologia_Sels", AV72TFTecnologia_TipoTecnologia_Sels);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavTecnologia_tipotecnologia1.CurrentValue = StringUtil.RTrim( AV62Tecnologia_TipoTecnologia1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTecnologia_tipotecnologia1_Internalname, "Values", cmbavTecnologia_tipotecnologia1.ToJavascriptSource());
      }

      protected void E214N2( )
      {
         /* 'DobtnAssociation' Routine */
         context.PopUp(formatLink("wp_associationtecnologiaambientetecnologico.aspx") + "?" + UrlEncode("" +A131Tecnologia_Codigo), new Object[] {});
         context.DoAjaxRefresh();
      }

      protected void E164N2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("tecnologia.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S162( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_tecnologia_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tecnologia_nome_Internalname, "SortedStatus", Ddo_tecnologia_nome_Sortedstatus);
         Ddo_tecnologia_tipotecnologia_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tecnologia_tipotecnologia_Internalname, "SortedStatus", Ddo_tecnologia_tipotecnologia_Sortedstatus);
      }

      protected void S142( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_tecnologia_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tecnologia_nome_Internalname, "SortedStatus", Ddo_tecnologia_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_tecnologia_tipotecnologia_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tecnologia_tipotecnologia_Internalname, "SortedStatus", Ddo_tecnologia_tipotecnologia_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavTecnologia_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTecnologia_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTecnologia_nome1_Visible), 5, 0)));
         cmbavTecnologia_tipotecnologia1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTecnologia_tipotecnologia1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavTecnologia_tipotecnologia1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TECNOLOGIA_NOME") == 0 )
         {
            edtavTecnologia_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTecnologia_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTecnologia_nome1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TECNOLOGIA_TIPOTECNOLOGIA") == 0 )
         {
            cmbavTecnologia_tipotecnologia1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTecnologia_tipotecnologia1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavTecnologia_tipotecnologia1.Visible), 5, 0)));
         }
      }

      protected void S172( )
      {
         /* 'CLEANFILTERS' Routine */
         AV67TFTecnologia_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFTecnologia_Nome", AV67TFTecnologia_Nome);
         Ddo_tecnologia_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tecnologia_nome_Internalname, "FilteredText_set", Ddo_tecnologia_nome_Filteredtext_set);
         AV68TFTecnologia_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFTecnologia_Nome_Sel", AV68TFTecnologia_Nome_Sel);
         Ddo_tecnologia_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tecnologia_nome_Internalname, "SelectedValue_set", Ddo_tecnologia_nome_Selectedvalue_set);
         AV72TFTecnologia_TipoTecnologia_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_tecnologia_tipotecnologia_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tecnologia_tipotecnologia_Internalname, "SelectedValue_set", Ddo_tecnologia_tipotecnologia_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "TECNOLOGIA_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV17Tecnologia_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Tecnologia_Nome1", AV17Tecnologia_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.DoAjaxRefresh();
      }

      protected void S132( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV33Session.Get(AV96Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV96Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV33Session.Get(AV96Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S202( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV97GXV1 = 1;
         while ( AV97GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV97GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFTECNOLOGIA_NOME") == 0 )
            {
               AV67TFTecnologia_Nome = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFTecnologia_Nome", AV67TFTecnologia_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67TFTecnologia_Nome)) )
               {
                  Ddo_tecnologia_nome_Filteredtext_set = AV67TFTecnologia_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tecnologia_nome_Internalname, "FilteredText_set", Ddo_tecnologia_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFTECNOLOGIA_NOME_SEL") == 0 )
            {
               AV68TFTecnologia_Nome_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFTecnologia_Nome_Sel", AV68TFTecnologia_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68TFTecnologia_Nome_Sel)) )
               {
                  Ddo_tecnologia_nome_Selectedvalue_set = AV68TFTecnologia_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tecnologia_nome_Internalname, "SelectedValue_set", Ddo_tecnologia_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFTECNOLOGIA_TIPOTECNOLOGIA_SEL") == 0 )
            {
               AV71TFTecnologia_TipoTecnologia_SelsJson = AV11GridStateFilterValue.gxTpr_Value;
               AV72TFTecnologia_TipoTecnologia_Sels.FromJSonString(AV71TFTecnologia_TipoTecnologia_SelsJson);
               if ( ! ( AV72TFTecnologia_TipoTecnologia_Sels.Count == 0 ) )
               {
                  Ddo_tecnologia_tipotecnologia_Selectedvalue_set = AV71TFTecnologia_TipoTecnologia_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tecnologia_tipotecnologia_Internalname, "SelectedValue_set", Ddo_tecnologia_tipotecnologia_Selectedvalue_set);
               }
            }
            AV97GXV1 = (int)(AV97GXV1+1);
         }
      }

      protected void S192( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TECNOLOGIA_NOME") == 0 )
            {
               AV17Tecnologia_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Tecnologia_Nome1", AV17Tecnologia_Nome1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TECNOLOGIA_TIPOTECNOLOGIA") == 0 )
            {
               AV62Tecnologia_TipoTecnologia1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62Tecnologia_TipoTecnologia1", AV62Tecnologia_TipoTecnologia1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
      }

      protected void S152( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV33Session.Get(AV96Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67TFTecnologia_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFTECNOLOGIA_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV67TFTecnologia_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68TFTecnologia_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFTECNOLOGIA_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV68TFTecnologia_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( AV72TFTecnologia_TipoTecnologia_Sels.Count == 0 ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFTECNOLOGIA_TIPOTECNOLOGIA_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV72TFTecnologia_TipoTecnologia_Sels.ToJSonString(false);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV96Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S212( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TECNOLOGIA_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Tecnologia_Nome1)) )
         {
            AV12GridStateDynamicFilter.gxTpr_Value = AV17Tecnologia_Nome1;
         }
         else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "TECNOLOGIA_TIPOTECNOLOGIA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV62Tecnologia_TipoTecnologia1)) )
         {
            AV12GridStateDynamicFilter.gxTpr_Value = AV62Tecnologia_TipoTecnologia1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
         {
            AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
         }
      }

      protected void S122( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV96Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Tecnologia";
         AV33Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void S182( )
      {
         /* 'RESETDYNFILTERS' Routine */
      }

      protected void wb_table1_2_4N2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_4N2( true) ;
         }
         else
         {
            wb_table2_8_4N2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_4N2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_42_4N2( true) ;
         }
         else
         {
            wb_table3_42_4N2( false) ;
         }
         return  ;
      }

      protected void wb_table3_42_4N2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_4N2e( true) ;
         }
         else
         {
            wb_table1_2_4N2e( false) ;
         }
      }

      protected void wb_table3_42_4N2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table4_45_4N2( true) ;
         }
         else
         {
            wb_table4_45_4N2( false) ;
         }
         return  ;
      }

      protected void wb_table4_45_4N2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_42_4N2e( true) ;
         }
         else
         {
            wb_table3_42_4N2e( false) ;
         }
      }

      protected void wb_table4_45_4N2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"48\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtTecnologia_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtTecnologia_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtTecnologia_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbTecnologia_TipoTecnologia_Titleformat == 0 )
               {
                  context.SendWebValue( cmbTecnologia_TipoTecnologia.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbTecnologia_TipoTecnologia.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV31Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV32Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A131Tecnologia_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A132Tecnologia_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtTecnologia_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTecnologia_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtTecnologia_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A355Tecnologia_TipoTecnologia));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbTecnologia_TipoTecnologia.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbTecnologia_TipoTecnologia_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV63btnAssociation));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavBtnassociation_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 48 )
         {
            wbEnd = 0;
            nRC_GXsfl_48 = (short)(nGXsfl_48_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_45_4N2e( true) ;
         }
         else
         {
            wb_table4_45_4N2e( false) ;
         }
      }

      protected void wb_table2_8_4N2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTecnologiatitle_Internalname, "Tecnologias", "", "", lblTecnologiatitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWTecnologia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table5_13_4N2( true) ;
         }
         else
         {
            wb_table5_13_4N2( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_4N2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWTecnologia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_48_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWTecnologia.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_48_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWTecnologia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_23_4N2( true) ;
         }
         else
         {
            wb_table6_23_4N2( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_4N2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_4N2e( true) ;
         }
         else
         {
            wb_table2_8_4N2e( false) ;
         }
      }

      protected void wb_table6_23_4N2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWTecnologia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table7_28_4N2( true) ;
         }
         else
         {
            wb_table7_28_4N2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_4N2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_4N2e( true) ;
         }
         else
         {
            wb_table6_23_4N2e( false) ;
         }
      }

      protected void wb_table7_28_4N2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWTecnologia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_48_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_WWTecnologia.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWTecnologia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'" + sGXsfl_48_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTecnologia_nome1_Internalname, StringUtil.RTrim( AV17Tecnologia_Nome1), StringUtil.RTrim( context.localUtil.Format( AV17Tecnologia_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,37);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTecnologia_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavTecnologia_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWTecnologia.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'" + sGXsfl_48_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavTecnologia_tipotecnologia1, cmbavTecnologia_tipotecnologia1_Internalname, StringUtil.RTrim( AV62Tecnologia_TipoTecnologia1), 1, cmbavTecnologia_tipotecnologia1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavTecnologia_tipotecnologia1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,38);\"", "", true, "HLP_WWTecnologia.htm");
            cmbavTecnologia_tipotecnologia1.CurrentValue = StringUtil.RTrim( AV62Tecnologia_TipoTecnologia1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTecnologia_tipotecnologia1_Internalname, "Values", (String)(cmbavTecnologia_tipotecnologia1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_4N2e( true) ;
         }
         else
         {
            wb_table7_28_4N2e( false) ;
         }
      }

      protected void wb_table5_13_4N2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgInsert_Visible, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWTecnologia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_4N2e( true) ;
         }
         else
         {
            wb_table5_13_4N2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA4N2( ) ;
         WS4N2( ) ;
         WE4N2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117341895");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwtecnologia.js", "?20203117341895");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_482( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_48_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_48_idx;
         edtTecnologia_Codigo_Internalname = "TECNOLOGIA_CODIGO_"+sGXsfl_48_idx;
         edtTecnologia_Nome_Internalname = "TECNOLOGIA_NOME_"+sGXsfl_48_idx;
         cmbTecnologia_TipoTecnologia_Internalname = "TECNOLOGIA_TIPOTECNOLOGIA_"+sGXsfl_48_idx;
         edtavBtnassociation_Internalname = "vBTNASSOCIATION_"+sGXsfl_48_idx;
      }

      protected void SubsflControlProps_fel_482( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_48_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_48_fel_idx;
         edtTecnologia_Codigo_Internalname = "TECNOLOGIA_CODIGO_"+sGXsfl_48_fel_idx;
         edtTecnologia_Nome_Internalname = "TECNOLOGIA_NOME_"+sGXsfl_48_fel_idx;
         cmbTecnologia_TipoTecnologia_Internalname = "TECNOLOGIA_TIPOTECNOLOGIA_"+sGXsfl_48_fel_idx;
         edtavBtnassociation_Internalname = "vBTNASSOCIATION_"+sGXsfl_48_fel_idx;
      }

      protected void sendrow_482( )
      {
         SubsflControlProps_482( ) ;
         WB4N0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_48_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_48_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_48_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV31Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV31Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV93Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV31Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV31Update)) ? AV93Update_GXI : context.PathToRelativeUrl( AV31Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavUpdate_Visible,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV31Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV32Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV94Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)) ? AV94Delete_GXI : context.PathToRelativeUrl( AV32Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavDelete_Visible,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV32Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTecnologia_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A131Tecnologia_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A131Tecnologia_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTecnologia_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)48,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTecnologia_Nome_Internalname,StringUtil.RTrim( A132Tecnologia_Nome),StringUtil.RTrim( context.localUtil.Format( A132Tecnologia_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtTecnologia_Nome_Link,(String)"",(String)"",(String)"",(String)edtTecnologia_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)48,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_48_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "TECNOLOGIA_TIPOTECNOLOGIA_" + sGXsfl_48_idx;
               cmbTecnologia_TipoTecnologia.Name = GXCCtl;
               cmbTecnologia_TipoTecnologia.WebTags = "";
               cmbTecnologia_TipoTecnologia.addItem("", "(Nenhum)", 0);
               cmbTecnologia_TipoTecnologia.addItem("OS", "Sistema Operacional", 0);
               cmbTecnologia_TipoTecnologia.addItem("LNG", "Linguagem", 0);
               cmbTecnologia_TipoTecnologia.addItem("DBM", "Banco de Dados", 0);
               cmbTecnologia_TipoTecnologia.addItem("SFT", "Software", 0);
               cmbTecnologia_TipoTecnologia.addItem("SRV", "Servidor", 0);
               cmbTecnologia_TipoTecnologia.addItem("DSK", "Desktop", 0);
               cmbTecnologia_TipoTecnologia.addItem("NTB", "Notebook", 0);
               cmbTecnologia_TipoTecnologia.addItem("PRN", "Impresora", 0);
               cmbTecnologia_TipoTecnologia.addItem("HRD", "Hardware", 0);
               if ( cmbTecnologia_TipoTecnologia.ItemCount > 0 )
               {
                  A355Tecnologia_TipoTecnologia = cmbTecnologia_TipoTecnologia.getValidValue(A355Tecnologia_TipoTecnologia);
                  n355Tecnologia_TipoTecnologia = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbTecnologia_TipoTecnologia,(String)cmbTecnologia_TipoTecnologia_Internalname,StringUtil.RTrim( A355Tecnologia_TipoTecnologia),(short)1,(String)cmbTecnologia_TipoTecnologia_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbTecnologia_TipoTecnologia.CurrentValue = StringUtil.RTrim( A355Tecnologia_TipoTecnologia);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbTecnologia_TipoTecnologia_Internalname, "Values", (String)(cmbTecnologia_TipoTecnologia.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavBtnassociation_Enabled!=0)&&(edtavBtnassociation_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 54,'',false,'',48)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV63btnAssociation_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV63btnAssociation))&&String.IsNullOrEmpty(StringUtil.RTrim( AV95Btnassociation_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV63btnAssociation)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavBtnassociation_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV63btnAssociation)) ? AV95Btnassociation_GXI : context.PathToRelativeUrl( AV63btnAssociation)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavBtnassociation_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavBtnassociation_Jsonclick,"'"+""+"'"+",false,"+"'"+"E\\'DOBTNASSOCIATION\\'."+sGXsfl_48_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV63btnAssociation_IsBlob,(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_TECNOLOGIA_CODIGO"+"_"+sGXsfl_48_idx, GetSecureSignedToken( sGXsfl_48_idx, context.localUtil.Format( (decimal)(A131Tecnologia_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_TECNOLOGIA_NOME"+"_"+sGXsfl_48_idx, GetSecureSignedToken( sGXsfl_48_idx, StringUtil.RTrim( context.localUtil.Format( A132Tecnologia_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_TECNOLOGIA_TIPOTECNOLOGIA"+"_"+sGXsfl_48_idx, GetSecureSignedToken( sGXsfl_48_idx, StringUtil.RTrim( context.localUtil.Format( A355Tecnologia_TipoTecnologia, ""))));
            GridContainer.AddRow(GridRow);
            nGXsfl_48_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_48_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_48_idx+1));
            sGXsfl_48_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_48_idx), 4, 0)), 4, "0");
            SubsflControlProps_482( ) ;
         }
         /* End function sendrow_482 */
      }

      protected void init_default_properties( )
      {
         lblTecnologiatitle_Internalname = "TECNOLOGIATITLE";
         imgInsert_Internalname = "INSERT";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavTecnologia_nome1_Internalname = "vTECNOLOGIA_NOME1";
         cmbavTecnologia_tipotecnologia1_Internalname = "vTECNOLOGIA_TIPOTECNOLOGIA1";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtTecnologia_Codigo_Internalname = "TECNOLOGIA_CODIGO";
         edtTecnologia_Nome_Internalname = "TECNOLOGIA_NOME";
         cmbTecnologia_TipoTecnologia_Internalname = "TECNOLOGIA_TIPOTECNOLOGIA";
         edtavBtnassociation_Internalname = "vBTNASSOCIATION";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         edtavTftecnologia_nome_Internalname = "vTFTECNOLOGIA_NOME";
         edtavTftecnologia_nome_sel_Internalname = "vTFTECNOLOGIA_NOME_SEL";
         Ddo_tecnologia_nome_Internalname = "DDO_TECNOLOGIA_NOME";
         edtavDdo_tecnologia_nometitlecontrolidtoreplace_Internalname = "vDDO_TECNOLOGIA_NOMETITLECONTROLIDTOREPLACE";
         Ddo_tecnologia_tipotecnologia_Internalname = "DDO_TECNOLOGIA_TIPOTECNOLOGIA";
         edtavDdo_tecnologia_tipotecnologiatitlecontrolidtoreplace_Internalname = "vDDO_TECNOLOGIA_TIPOTECNOLOGIATITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavBtnassociation_Jsonclick = "";
         edtavBtnassociation_Visible = -1;
         edtavBtnassociation_Enabled = 1;
         cmbTecnologia_TipoTecnologia_Jsonclick = "";
         edtTecnologia_Nome_Jsonclick = "";
         edtTecnologia_Codigo_Jsonclick = "";
         imgInsert_Visible = 1;
         cmbavTecnologia_tipotecnologia1_Jsonclick = "";
         edtavTecnologia_nome1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavBtnassociation_Tooltiptext = "Clique para associar os Ambientes Tecnol�gicos desta Tecnolog�a";
         edtTecnologia_Nome_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         cmbTecnologia_TipoTecnologia_Titleformat = 0;
         edtTecnologia_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavTecnologia_tipotecnologia1.Visible = 1;
         edtavTecnologia_nome1_Visible = 1;
         edtavDelete_Visible = -1;
         edtavUpdate_Visible = -1;
         cmbTecnologia_TipoTecnologia.Title.Text = "Tipo";
         edtTecnologia_Nome_Title = "Nome";
         edtavOrdereddsc_Visible = 1;
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_tecnologia_tipotecnologiatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_tecnologia_nometitlecontrolidtoreplace_Visible = 1;
         edtavTftecnologia_nome_sel_Jsonclick = "";
         edtavTftecnologia_nome_sel_Visible = 1;
         edtavTftecnologia_nome_Jsonclick = "";
         edtavTftecnologia_nome_Visible = 1;
         Ddo_tecnologia_tipotecnologia_Searchbuttontext = "Filtrar Selecionados";
         Ddo_tecnologia_tipotecnologia_Cleanfilter = "Limpar pesquisa";
         Ddo_tecnologia_tipotecnologia_Sortdsc = "Ordenar de Z � A";
         Ddo_tecnologia_tipotecnologia_Sortasc = "Ordenar de A � Z";
         Ddo_tecnologia_tipotecnologia_Datalistfixedvalues = "OS:Sistema Operacional,LNG:Linguagem,DBM:Banco de Dados,SFT:Software,SRV:Servidor,DSK:Desktop,NTB:Notebook,PRN:Impresora,HRD:Hardware";
         Ddo_tecnologia_tipotecnologia_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_tecnologia_tipotecnologia_Datalisttype = "FixedValues";
         Ddo_tecnologia_tipotecnologia_Includedatalist = Convert.ToBoolean( -1);
         Ddo_tecnologia_tipotecnologia_Includefilter = Convert.ToBoolean( 0);
         Ddo_tecnologia_tipotecnologia_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_tecnologia_tipotecnologia_Includesortasc = Convert.ToBoolean( -1);
         Ddo_tecnologia_tipotecnologia_Titlecontrolidtoreplace = "";
         Ddo_tecnologia_tipotecnologia_Dropdownoptionstype = "GridTitleSettings";
         Ddo_tecnologia_tipotecnologia_Cls = "ColumnSettings";
         Ddo_tecnologia_tipotecnologia_Tooltip = "Op��es";
         Ddo_tecnologia_tipotecnologia_Caption = "";
         Ddo_tecnologia_nome_Searchbuttontext = "Pesquisar";
         Ddo_tecnologia_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_tecnologia_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_tecnologia_nome_Loadingdata = "Carregando dados...";
         Ddo_tecnologia_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_tecnologia_nome_Sortasc = "Ordenar de A � Z";
         Ddo_tecnologia_nome_Datalistupdateminimumcharacters = 0;
         Ddo_tecnologia_nome_Datalistproc = "GetWWTecnologiaFilterData";
         Ddo_tecnologia_nome_Datalisttype = "Dynamic";
         Ddo_tecnologia_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_tecnologia_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_tecnologia_nome_Filtertype = "Character";
         Ddo_tecnologia_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_tecnologia_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_tecnologia_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_tecnologia_nome_Titlecontrolidtoreplace = "";
         Ddo_tecnologia_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_tecnologia_nome_Cls = "ColumnSettings";
         Ddo_tecnologia_nome_Tooltip = "Op��es";
         Ddo_tecnologia_nome_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Tecnologia";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A131Tecnologia_Codigo',fld:'TECNOLOGIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV69ddo_Tecnologia_NomeTitleControlIdToReplace',fld:'vDDO_TECNOLOGIA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace',fld:'vDDO_TECNOLOGIA_TIPOTECNOLOGIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Tecnologia_Nome1',fld:'vTECNOLOGIA_NOME1',pic:'@!',nv:''},{av:'AV62Tecnologia_TipoTecnologia1',fld:'vTECNOLOGIA_TIPOTECNOLOGIA1',pic:'',nv:''},{av:'AV67TFTecnologia_Nome',fld:'vTFTECNOLOGIA_NOME',pic:'@!',nv:''},{av:'AV68TFTecnologia_Nome_Sel',fld:'vTFTECNOLOGIA_NOME_SEL',pic:'@!',nv:''},{av:'AV72TFTecnologia_TipoTecnologia_Sels',fld:'vTFTECNOLOGIA_TIPOTECNOLOGIA_SELS',pic:'',nv:null},{av:'AV96Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}],oparms:[{av:'AV66Tecnologia_NomeTitleFilterData',fld:'vTECNOLOGIA_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV70Tecnologia_TipoTecnologiaTitleFilterData',fld:'vTECNOLOGIA_TIPOTECNOLOGIATITLEFILTERDATA',pic:'',nv:null},{av:'edtTecnologia_Nome_Titleformat',ctrl:'TECNOLOGIA_NOME',prop:'Titleformat'},{av:'edtTecnologia_Nome_Title',ctrl:'TECNOLOGIA_NOME',prop:'Title'},{av:'cmbTecnologia_TipoTecnologia'},{av:'AV76GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV77GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'imgInsert_Visible',ctrl:'INSERT',prop:'Visible'},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E114N2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Tecnologia_Nome1',fld:'vTECNOLOGIA_NOME1',pic:'@!',nv:''},{av:'AV62Tecnologia_TipoTecnologia1',fld:'vTECNOLOGIA_TIPOTECNOLOGIA1',pic:'',nv:''},{av:'AV67TFTecnologia_Nome',fld:'vTFTECNOLOGIA_NOME',pic:'@!',nv:''},{av:'AV68TFTecnologia_Nome_Sel',fld:'vTFTECNOLOGIA_NOME_SEL',pic:'@!',nv:''},{av:'AV69ddo_Tecnologia_NomeTitleControlIdToReplace',fld:'vDDO_TECNOLOGIA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace',fld:'vDDO_TECNOLOGIA_TIPOTECNOLOGIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72TFTecnologia_TipoTecnologia_Sels',fld:'vTFTECNOLOGIA_TIPOTECNOLOGIA_SELS',pic:'',nv:null},{av:'AV96Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A131Tecnologia_Codigo',fld:'TECNOLOGIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_TECNOLOGIA_NOME.ONOPTIONCLICKED","{handler:'E124N2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Tecnologia_Nome1',fld:'vTECNOLOGIA_NOME1',pic:'@!',nv:''},{av:'AV62Tecnologia_TipoTecnologia1',fld:'vTECNOLOGIA_TIPOTECNOLOGIA1',pic:'',nv:''},{av:'AV67TFTecnologia_Nome',fld:'vTFTECNOLOGIA_NOME',pic:'@!',nv:''},{av:'AV68TFTecnologia_Nome_Sel',fld:'vTFTECNOLOGIA_NOME_SEL',pic:'@!',nv:''},{av:'AV69ddo_Tecnologia_NomeTitleControlIdToReplace',fld:'vDDO_TECNOLOGIA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace',fld:'vDDO_TECNOLOGIA_TIPOTECNOLOGIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72TFTecnologia_TipoTecnologia_Sels',fld:'vTFTECNOLOGIA_TIPOTECNOLOGIA_SELS',pic:'',nv:null},{av:'AV96Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A131Tecnologia_Codigo',fld:'TECNOLOGIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_tecnologia_nome_Activeeventkey',ctrl:'DDO_TECNOLOGIA_NOME',prop:'ActiveEventKey'},{av:'Ddo_tecnologia_nome_Filteredtext_get',ctrl:'DDO_TECNOLOGIA_NOME',prop:'FilteredText_get'},{av:'Ddo_tecnologia_nome_Selectedvalue_get',ctrl:'DDO_TECNOLOGIA_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_tecnologia_nome_Sortedstatus',ctrl:'DDO_TECNOLOGIA_NOME',prop:'SortedStatus'},{av:'AV67TFTecnologia_Nome',fld:'vTFTECNOLOGIA_NOME',pic:'@!',nv:''},{av:'AV68TFTecnologia_Nome_Sel',fld:'vTFTECNOLOGIA_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_tecnologia_tipotecnologia_Sortedstatus',ctrl:'DDO_TECNOLOGIA_TIPOTECNOLOGIA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_TECNOLOGIA_TIPOTECNOLOGIA.ONOPTIONCLICKED","{handler:'E134N2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Tecnologia_Nome1',fld:'vTECNOLOGIA_NOME1',pic:'@!',nv:''},{av:'AV62Tecnologia_TipoTecnologia1',fld:'vTECNOLOGIA_TIPOTECNOLOGIA1',pic:'',nv:''},{av:'AV67TFTecnologia_Nome',fld:'vTFTECNOLOGIA_NOME',pic:'@!',nv:''},{av:'AV68TFTecnologia_Nome_Sel',fld:'vTFTECNOLOGIA_NOME_SEL',pic:'@!',nv:''},{av:'AV69ddo_Tecnologia_NomeTitleControlIdToReplace',fld:'vDDO_TECNOLOGIA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace',fld:'vDDO_TECNOLOGIA_TIPOTECNOLOGIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72TFTecnologia_TipoTecnologia_Sels',fld:'vTFTECNOLOGIA_TIPOTECNOLOGIA_SELS',pic:'',nv:null},{av:'AV96Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A131Tecnologia_Codigo',fld:'TECNOLOGIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_tecnologia_tipotecnologia_Activeeventkey',ctrl:'DDO_TECNOLOGIA_TIPOTECNOLOGIA',prop:'ActiveEventKey'},{av:'Ddo_tecnologia_tipotecnologia_Selectedvalue_get',ctrl:'DDO_TECNOLOGIA_TIPOTECNOLOGIA',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_tecnologia_tipotecnologia_Sortedstatus',ctrl:'DDO_TECNOLOGIA_TIPOTECNOLOGIA',prop:'SortedStatus'},{av:'AV72TFTecnologia_TipoTecnologia_Sels',fld:'vTFTECNOLOGIA_TIPOTECNOLOGIA_SELS',pic:'',nv:null},{av:'Ddo_tecnologia_nome_Sortedstatus',ctrl:'DDO_TECNOLOGIA_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E204N2',iparms:[{av:'A131Tecnologia_Codigo',fld:'TECNOLOGIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV31Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV32Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV63btnAssociation',fld:'vBTNASSOCIATION',pic:'',nv:''},{av:'edtavBtnassociation_Tooltiptext',ctrl:'vBTNASSOCIATION',prop:'Tooltiptext'},{av:'edtTecnologia_Nome_Link',ctrl:'TECNOLOGIA_NOME',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E144N2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Tecnologia_Nome1',fld:'vTECNOLOGIA_NOME1',pic:'@!',nv:''},{av:'AV62Tecnologia_TipoTecnologia1',fld:'vTECNOLOGIA_TIPOTECNOLOGIA1',pic:'',nv:''},{av:'AV67TFTecnologia_Nome',fld:'vTFTECNOLOGIA_NOME',pic:'@!',nv:''},{av:'AV68TFTecnologia_Nome_Sel',fld:'vTFTECNOLOGIA_NOME_SEL',pic:'@!',nv:''},{av:'AV69ddo_Tecnologia_NomeTitleControlIdToReplace',fld:'vDDO_TECNOLOGIA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace',fld:'vDDO_TECNOLOGIA_TIPOTECNOLOGIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72TFTecnologia_TipoTecnologia_Sels',fld:'vTFTECNOLOGIA_TIPOTECNOLOGIA_SELS',pic:'',nv:null},{av:'AV96Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A131Tecnologia_Codigo',fld:'TECNOLOGIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E174N2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavTecnologia_nome1_Visible',ctrl:'vTECNOLOGIA_NOME1',prop:'Visible'},{av:'cmbavTecnologia_tipotecnologia1'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E154N2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Tecnologia_Nome1',fld:'vTECNOLOGIA_NOME1',pic:'@!',nv:''},{av:'AV62Tecnologia_TipoTecnologia1',fld:'vTECNOLOGIA_TIPOTECNOLOGIA1',pic:'',nv:''},{av:'AV67TFTecnologia_Nome',fld:'vTFTECNOLOGIA_NOME',pic:'@!',nv:''},{av:'AV68TFTecnologia_Nome_Sel',fld:'vTFTECNOLOGIA_NOME_SEL',pic:'@!',nv:''},{av:'AV69ddo_Tecnologia_NomeTitleControlIdToReplace',fld:'vDDO_TECNOLOGIA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace',fld:'vDDO_TECNOLOGIA_TIPOTECNOLOGIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72TFTecnologia_TipoTecnologia_Sels',fld:'vTFTECNOLOGIA_TIPOTECNOLOGIA_SELS',pic:'',nv:null},{av:'AV96Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A131Tecnologia_Codigo',fld:'TECNOLOGIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV67TFTecnologia_Nome',fld:'vTFTECNOLOGIA_NOME',pic:'@!',nv:''},{av:'Ddo_tecnologia_nome_Filteredtext_set',ctrl:'DDO_TECNOLOGIA_NOME',prop:'FilteredText_set'},{av:'AV68TFTecnologia_Nome_Sel',fld:'vTFTECNOLOGIA_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_tecnologia_nome_Selectedvalue_set',ctrl:'DDO_TECNOLOGIA_NOME',prop:'SelectedValue_set'},{av:'AV72TFTecnologia_TipoTecnologia_Sels',fld:'vTFTECNOLOGIA_TIPOTECNOLOGIA_SELS',pic:'',nv:null},{av:'Ddo_tecnologia_tipotecnologia_Selectedvalue_set',ctrl:'DDO_TECNOLOGIA_TIPOTECNOLOGIA',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Tecnologia_Nome1',fld:'vTECNOLOGIA_NOME1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavTecnologia_nome1_Visible',ctrl:'vTECNOLOGIA_NOME1',prop:'Visible'},{av:'cmbavTecnologia_tipotecnologia1'},{av:'AV62Tecnologia_TipoTecnologia1',fld:'vTECNOLOGIA_TIPOTECNOLOGIA1',pic:'',nv:''}]}");
         setEventMetadata("'DOBTNASSOCIATION'","{handler:'E214N2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Tecnologia_Nome1',fld:'vTECNOLOGIA_NOME1',pic:'@!',nv:''},{av:'AV62Tecnologia_TipoTecnologia1',fld:'vTECNOLOGIA_TIPOTECNOLOGIA1',pic:'',nv:''},{av:'AV67TFTecnologia_Nome',fld:'vTFTECNOLOGIA_NOME',pic:'@!',nv:''},{av:'AV68TFTecnologia_Nome_Sel',fld:'vTFTECNOLOGIA_NOME_SEL',pic:'@!',nv:''},{av:'AV69ddo_Tecnologia_NomeTitleControlIdToReplace',fld:'vDDO_TECNOLOGIA_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace',fld:'vDDO_TECNOLOGIA_TIPOTECNOLOGIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72TFTecnologia_TipoTecnologia_Sels',fld:'vTFTECNOLOGIA_TIPOTECNOLOGIA_SELS',pic:'',nv:null},{av:'AV96Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A131Tecnologia_Codigo',fld:'TECNOLOGIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'DOINSERT'","{handler:'E164N2',iparms:[{av:'A131Tecnologia_Codigo',fld:'TECNOLOGIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_tecnologia_nome_Activeeventkey = "";
         Ddo_tecnologia_nome_Filteredtext_get = "";
         Ddo_tecnologia_nome_Selectedvalue_get = "";
         Ddo_tecnologia_tipotecnologia_Activeeventkey = "";
         Ddo_tecnologia_tipotecnologia_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17Tecnologia_Nome1 = "";
         AV62Tecnologia_TipoTecnologia1 = "";
         AV67TFTecnologia_Nome = "";
         AV68TFTecnologia_Nome_Sel = "";
         AV69ddo_Tecnologia_NomeTitleControlIdToReplace = "";
         AV73ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace = "";
         AV72TFTecnologia_TipoTecnologia_Sels = new GxSimpleCollection();
         AV96Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV74DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV66Tecnologia_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV70Tecnologia_TipoTecnologiaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_tecnologia_nome_Filteredtext_set = "";
         Ddo_tecnologia_nome_Selectedvalue_set = "";
         Ddo_tecnologia_nome_Sortedstatus = "";
         Ddo_tecnologia_tipotecnologia_Selectedvalue_set = "";
         Ddo_tecnologia_tipotecnologia_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV31Update = "";
         AV93Update_GXI = "";
         AV32Delete = "";
         AV94Delete_GXI = "";
         A132Tecnologia_Nome = "";
         A355Tecnologia_TipoTecnologia = "";
         AV63btnAssociation = "";
         AV95Btnassociation_GXI = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         AV92WWTecnologiaDS_6_Tftecnologia_tipotecnologia_sels = new GxSimpleCollection();
         scmdbuf = "";
         lV88WWTecnologiaDS_2_Tecnologia_nome1 = "";
         lV90WWTecnologiaDS_4_Tftecnologia_nome = "";
         AV87WWTecnologiaDS_1_Dynamicfiltersselector1 = "";
         AV88WWTecnologiaDS_2_Tecnologia_nome1 = "";
         AV89WWTecnologiaDS_3_Tecnologia_tipotecnologia1 = "";
         AV91WWTecnologiaDS_5_Tftecnologia_nome_sel = "";
         AV90WWTecnologiaDS_4_Tftecnologia_nome = "";
         H004N2_A355Tecnologia_TipoTecnologia = new String[] {""} ;
         H004N2_n355Tecnologia_TipoTecnologia = new bool[] {false} ;
         H004N2_A132Tecnologia_Nome = new String[] {""} ;
         H004N2_A131Tecnologia_Codigo = new int[1] ;
         H004N3_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV71TFTecnologia_TipoTecnologia_SelsJson = "";
         GridRow = new GXWebRow();
         AV33Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblTecnologiatitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwtecnologia__default(),
            new Object[][] {
                new Object[] {
               H004N2_A355Tecnologia_TipoTecnologia, H004N2_n355Tecnologia_TipoTecnologia, H004N2_A132Tecnologia_Nome, H004N2_A131Tecnologia_Codigo
               }
               , new Object[] {
               H004N3_AGRID_nRecordCount
               }
            }
         );
         AV96Pgmname = "WWTecnologia";
         /* GeneXus formulas. */
         AV96Pgmname = "WWTecnologia";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_48 ;
      private short nGXsfl_48_idx=1 ;
      private short AV13OrderedBy ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_48_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtTecnologia_Nome_Titleformat ;
      private short cmbTecnologia_TipoTecnologia_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int A131Tecnologia_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_tecnologia_nome_Datalistupdateminimumcharacters ;
      private int edtavTftecnologia_nome_Visible ;
      private int edtavTftecnologia_nome_sel_Visible ;
      private int edtavDdo_tecnologia_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_tecnologia_tipotecnologiatitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV92WWTecnologiaDS_6_Tftecnologia_tipotecnologia_sels_Count ;
      private int edtavOrdereddsc_Visible ;
      private int imgInsert_Visible ;
      private int edtavUpdate_Visible ;
      private int edtavDelete_Visible ;
      private int AV75PageToGo ;
      private int edtavTecnologia_nome1_Visible ;
      private int AV97GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavBtnassociation_Enabled ;
      private int edtavBtnassociation_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV76GridCurrentPage ;
      private long AV77GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_tecnologia_nome_Activeeventkey ;
      private String Ddo_tecnologia_nome_Filteredtext_get ;
      private String Ddo_tecnologia_nome_Selectedvalue_get ;
      private String Ddo_tecnologia_tipotecnologia_Activeeventkey ;
      private String Ddo_tecnologia_tipotecnologia_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_48_idx="0001" ;
      private String AV17Tecnologia_Nome1 ;
      private String AV62Tecnologia_TipoTecnologia1 ;
      private String AV67TFTecnologia_Nome ;
      private String AV68TFTecnologia_Nome_Sel ;
      private String AV96Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_tecnologia_nome_Caption ;
      private String Ddo_tecnologia_nome_Tooltip ;
      private String Ddo_tecnologia_nome_Cls ;
      private String Ddo_tecnologia_nome_Filteredtext_set ;
      private String Ddo_tecnologia_nome_Selectedvalue_set ;
      private String Ddo_tecnologia_nome_Dropdownoptionstype ;
      private String Ddo_tecnologia_nome_Titlecontrolidtoreplace ;
      private String Ddo_tecnologia_nome_Sortedstatus ;
      private String Ddo_tecnologia_nome_Filtertype ;
      private String Ddo_tecnologia_nome_Datalisttype ;
      private String Ddo_tecnologia_nome_Datalistproc ;
      private String Ddo_tecnologia_nome_Sortasc ;
      private String Ddo_tecnologia_nome_Sortdsc ;
      private String Ddo_tecnologia_nome_Loadingdata ;
      private String Ddo_tecnologia_nome_Cleanfilter ;
      private String Ddo_tecnologia_nome_Noresultsfound ;
      private String Ddo_tecnologia_nome_Searchbuttontext ;
      private String Ddo_tecnologia_tipotecnologia_Caption ;
      private String Ddo_tecnologia_tipotecnologia_Tooltip ;
      private String Ddo_tecnologia_tipotecnologia_Cls ;
      private String Ddo_tecnologia_tipotecnologia_Selectedvalue_set ;
      private String Ddo_tecnologia_tipotecnologia_Dropdownoptionstype ;
      private String Ddo_tecnologia_tipotecnologia_Titlecontrolidtoreplace ;
      private String Ddo_tecnologia_tipotecnologia_Sortedstatus ;
      private String Ddo_tecnologia_tipotecnologia_Datalisttype ;
      private String Ddo_tecnologia_tipotecnologia_Datalistfixedvalues ;
      private String Ddo_tecnologia_tipotecnologia_Sortasc ;
      private String Ddo_tecnologia_tipotecnologia_Sortdsc ;
      private String Ddo_tecnologia_tipotecnologia_Cleanfilter ;
      private String Ddo_tecnologia_tipotecnologia_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String edtavTftecnologia_nome_Internalname ;
      private String edtavTftecnologia_nome_Jsonclick ;
      private String edtavTftecnologia_nome_sel_Internalname ;
      private String edtavTftecnologia_nome_sel_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavDdo_tecnologia_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_tecnologia_tipotecnologiatitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtTecnologia_Codigo_Internalname ;
      private String A132Tecnologia_Nome ;
      private String edtTecnologia_Nome_Internalname ;
      private String cmbTecnologia_TipoTecnologia_Internalname ;
      private String A355Tecnologia_TipoTecnologia ;
      private String edtavBtnassociation_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV88WWTecnologiaDS_2_Tecnologia_nome1 ;
      private String lV90WWTecnologiaDS_4_Tftecnologia_nome ;
      private String AV88WWTecnologiaDS_2_Tecnologia_nome1 ;
      private String AV89WWTecnologiaDS_3_Tecnologia_tipotecnologia1 ;
      private String AV91WWTecnologiaDS_5_Tftecnologia_nome_sel ;
      private String AV90WWTecnologiaDS_4_Tftecnologia_nome ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavTecnologia_nome1_Internalname ;
      private String cmbavTecnologia_tipotecnologia1_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_tecnologia_nome_Internalname ;
      private String Ddo_tecnologia_tipotecnologia_Internalname ;
      private String edtTecnologia_Nome_Title ;
      private String imgInsert_Internalname ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavBtnassociation_Tooltiptext ;
      private String edtTecnologia_Nome_Link ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblTecnologiatitle_Internalname ;
      private String lblTecnologiatitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String imgCleanfilters_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavTecnologia_nome1_Jsonclick ;
      private String cmbavTecnologia_tipotecnologia1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_48_fel_idx="0001" ;
      private String ROClassString ;
      private String edtTecnologia_Codigo_Jsonclick ;
      private String edtTecnologia_Nome_Jsonclick ;
      private String cmbTecnologia_TipoTecnologia_Jsonclick ;
      private String edtavBtnassociation_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_tecnologia_nome_Includesortasc ;
      private bool Ddo_tecnologia_nome_Includesortdsc ;
      private bool Ddo_tecnologia_nome_Includefilter ;
      private bool Ddo_tecnologia_nome_Filterisrange ;
      private bool Ddo_tecnologia_nome_Includedatalist ;
      private bool Ddo_tecnologia_tipotecnologia_Includesortasc ;
      private bool Ddo_tecnologia_tipotecnologia_Includesortdsc ;
      private bool Ddo_tecnologia_tipotecnologia_Includefilter ;
      private bool Ddo_tecnologia_tipotecnologia_Includedatalist ;
      private bool Ddo_tecnologia_tipotecnologia_Allowmultipleselection ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n355Tecnologia_TipoTecnologia ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV31Update_IsBlob ;
      private bool AV32Delete_IsBlob ;
      private bool AV63btnAssociation_IsBlob ;
      private String AV71TFTecnologia_TipoTecnologia_SelsJson ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV69ddo_Tecnologia_NomeTitleControlIdToReplace ;
      private String AV73ddo_Tecnologia_TipoTecnologiaTitleControlIdToReplace ;
      private String AV93Update_GXI ;
      private String AV94Delete_GXI ;
      private String AV95Btnassociation_GXI ;
      private String AV87WWTecnologiaDS_1_Dynamicfiltersselector1 ;
      private String AV31Update ;
      private String AV32Delete ;
      private String AV63btnAssociation ;
      private IGxSession AV33Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavTecnologia_tipotecnologia1 ;
      private GXCombobox cmbTecnologia_TipoTecnologia ;
      private IDataStoreProvider pr_default ;
      private String[] H004N2_A355Tecnologia_TipoTecnologia ;
      private bool[] H004N2_n355Tecnologia_TipoTecnologia ;
      private String[] H004N2_A132Tecnologia_Nome ;
      private int[] H004N2_A131Tecnologia_Codigo ;
      private long[] H004N3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV72TFTecnologia_TipoTecnologia_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV92WWTecnologiaDS_6_Tftecnologia_tipotecnologia_sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV66Tecnologia_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV70Tecnologia_TipoTecnologiaTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV74DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwtecnologia__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H004N2( IGxContext context ,
                                             String A355Tecnologia_TipoTecnologia ,
                                             IGxCollection AV92WWTecnologiaDS_6_Tftecnologia_tipotecnologia_sels ,
                                             String AV87WWTecnologiaDS_1_Dynamicfiltersselector1 ,
                                             String AV88WWTecnologiaDS_2_Tecnologia_nome1 ,
                                             String AV89WWTecnologiaDS_3_Tecnologia_tipotecnologia1 ,
                                             String AV91WWTecnologiaDS_5_Tftecnologia_nome_sel ,
                                             String AV90WWTecnologiaDS_4_Tftecnologia_nome ,
                                             int AV92WWTecnologiaDS_6_Tftecnologia_tipotecnologia_sels_Count ,
                                             String A132Tecnologia_Nome ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [9] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [Tecnologia_TipoTecnologia], [Tecnologia_Nome], [Tecnologia_Codigo]";
         sFromString = " FROM [Tecnologia] WITH (NOLOCK)";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV87WWTecnologiaDS_1_Dynamicfiltersselector1, "TECNOLOGIA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWTecnologiaDS_2_Tecnologia_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Tecnologia_Nome] like '%' + @lV88WWTecnologiaDS_2_Tecnologia_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Tecnologia_Nome] like '%' + @lV88WWTecnologiaDS_2_Tecnologia_nome1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV87WWTecnologiaDS_1_Dynamicfiltersselector1, "TECNOLOGIA_TIPOTECNOLOGIA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWTecnologiaDS_3_Tecnologia_tipotecnologia1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Tecnologia_TipoTecnologia] = @AV89WWTecnologiaDS_3_Tecnologia_tipotecnologia1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Tecnologia_TipoTecnologia] = @AV89WWTecnologiaDS_3_Tecnologia_tipotecnologia1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV91WWTecnologiaDS_5_Tftecnologia_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWTecnologiaDS_4_Tftecnologia_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Tecnologia_Nome] like @lV90WWTecnologiaDS_4_Tftecnologia_nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([Tecnologia_Nome] like @lV90WWTecnologiaDS_4_Tftecnologia_nome)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWTecnologiaDS_5_Tftecnologia_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Tecnologia_Nome] = @AV91WWTecnologiaDS_5_Tftecnologia_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Tecnologia_Nome] = @AV91WWTecnologiaDS_5_Tftecnologia_nome_sel)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV92WWTecnologiaDS_6_Tftecnologia_tipotecnologia_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV92WWTecnologiaDS_6_Tftecnologia_tipotecnologia_sels, "[Tecnologia_TipoTecnologia] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV92WWTecnologiaDS_6_Tftecnologia_tipotecnologia_sels, "[Tecnologia_TipoTecnologia] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Tecnologia_Nome]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Tecnologia_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Tecnologia_TipoTecnologia]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Tecnologia_TipoTecnologia] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [Tecnologia_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H004N3( IGxContext context ,
                                             String A355Tecnologia_TipoTecnologia ,
                                             IGxCollection AV92WWTecnologiaDS_6_Tftecnologia_tipotecnologia_sels ,
                                             String AV87WWTecnologiaDS_1_Dynamicfiltersselector1 ,
                                             String AV88WWTecnologiaDS_2_Tecnologia_nome1 ,
                                             String AV89WWTecnologiaDS_3_Tecnologia_tipotecnologia1 ,
                                             String AV91WWTecnologiaDS_5_Tftecnologia_nome_sel ,
                                             String AV90WWTecnologiaDS_4_Tftecnologia_nome ,
                                             int AV92WWTecnologiaDS_6_Tftecnologia_tipotecnologia_sels_Count ,
                                             String A132Tecnologia_Nome ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [4] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [Tecnologia] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV87WWTecnologiaDS_1_Dynamicfiltersselector1, "TECNOLOGIA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWTecnologiaDS_2_Tecnologia_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Tecnologia_Nome] like '%' + @lV88WWTecnologiaDS_2_Tecnologia_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Tecnologia_Nome] like '%' + @lV88WWTecnologiaDS_2_Tecnologia_nome1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV87WWTecnologiaDS_1_Dynamicfiltersselector1, "TECNOLOGIA_TIPOTECNOLOGIA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWTecnologiaDS_3_Tecnologia_tipotecnologia1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Tecnologia_TipoTecnologia] = @AV89WWTecnologiaDS_3_Tecnologia_tipotecnologia1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Tecnologia_TipoTecnologia] = @AV89WWTecnologiaDS_3_Tecnologia_tipotecnologia1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV91WWTecnologiaDS_5_Tftecnologia_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWTecnologiaDS_4_Tftecnologia_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Tecnologia_Nome] like @lV90WWTecnologiaDS_4_Tftecnologia_nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([Tecnologia_Nome] like @lV90WWTecnologiaDS_4_Tftecnologia_nome)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWTecnologiaDS_5_Tftecnologia_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Tecnologia_Nome] = @AV91WWTecnologiaDS_5_Tftecnologia_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Tecnologia_Nome] = @AV91WWTecnologiaDS_5_Tftecnologia_nome_sel)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV92WWTecnologiaDS_6_Tftecnologia_tipotecnologia_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV92WWTecnologiaDS_6_Tftecnologia_tipotecnologia_sels, "[Tecnologia_TipoTecnologia] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV92WWTecnologiaDS_6_Tftecnologia_tipotecnologia_sels, "[Tecnologia_TipoTecnologia] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H004N2(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (int)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (bool)dynConstraints[10] );
               case 1 :
                     return conditional_H004N3(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (int)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (bool)dynConstraints[10] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH004N2 ;
          prmH004N2 = new Object[] {
          new Object[] {"@lV88WWTecnologiaDS_2_Tecnologia_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV89WWTecnologiaDS_3_Tecnologia_tipotecnologia1",SqlDbType.Char,3,0} ,
          new Object[] {"@lV90WWTecnologiaDS_4_Tftecnologia_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV91WWTecnologiaDS_5_Tftecnologia_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH004N3 ;
          prmH004N3 = new Object[] {
          new Object[] {"@lV88WWTecnologiaDS_2_Tecnologia_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV89WWTecnologiaDS_3_Tecnologia_tipotecnologia1",SqlDbType.Char,3,0} ,
          new Object[] {"@lV90WWTecnologiaDS_4_Tftecnologia_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV91WWTecnologiaDS_5_Tftecnologia_nome_sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H004N2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH004N2,11,0,true,false )
             ,new CursorDef("H004N3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH004N3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 3) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 50) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[14]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[4]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[5]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[6]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                return;
       }
    }

 }

}
