/*
               File: GetWWModuloFilterData
        Description: Get WWModulo Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:10:27.55
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwmodulofilterdata : GXProcedure
   {
      public getwwmodulofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwmodulofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
         return AV27OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwmodulofilterdata objgetwwmodulofilterdata;
         objgetwwmodulofilterdata = new getwwmodulofilterdata();
         objgetwwmodulofilterdata.AV18DDOName = aP0_DDOName;
         objgetwwmodulofilterdata.AV16SearchTxt = aP1_SearchTxt;
         objgetwwmodulofilterdata.AV17SearchTxtTo = aP2_SearchTxtTo;
         objgetwwmodulofilterdata.AV22OptionsJson = "" ;
         objgetwwmodulofilterdata.AV25OptionsDescJson = "" ;
         objgetwwmodulofilterdata.AV27OptionIndexesJson = "" ;
         objgetwwmodulofilterdata.context.SetSubmitInitialConfig(context);
         objgetwwmodulofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwmodulofilterdata);
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwmodulofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV21Options = (IGxCollection)(new GxSimpleCollection());
         AV24OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV26OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_MODULO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADMODULO_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_MODULO_SIGLA") == 0 )
         {
            /* Execute user subroutine: 'LOADMODULO_SIGLAOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_SISTEMA_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADSISTEMA_DESCRICAOOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV22OptionsJson = AV21Options.ToJSonString(false);
         AV25OptionsDescJson = AV24OptionsDesc.ToJSonString(false);
         AV27OptionIndexesJson = AV26OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV29Session.Get("WWModuloGridState"), "") == 0 )
         {
            AV31GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWModuloGridState"), "");
         }
         else
         {
            AV31GridState.FromXml(AV29Session.Get("WWModuloGridState"), "");
         }
         AV43GXV1 = 1;
         while ( AV43GXV1 <= AV31GridState.gxTpr_Filtervalues.Count )
         {
            AV32GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV31GridState.gxTpr_Filtervalues.Item(AV43GXV1));
            if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "SISTEMA_AREATRABALHOCOD") == 0 )
            {
               AV34Sistema_AreaTrabalhoCod = (int)(NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFMODULO_NOME") == 0 )
            {
               AV10TFModulo_Nome = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFMODULO_NOME_SEL") == 0 )
            {
               AV11TFModulo_Nome_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFMODULO_SIGLA") == 0 )
            {
               AV12TFModulo_Sigla = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFMODULO_SIGLA_SEL") == 0 )
            {
               AV13TFModulo_Sigla_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFSISTEMA_DESCRICAO") == 0 )
            {
               AV14TFSistema_Descricao = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFSISTEMA_DESCRICAO_SEL") == 0 )
            {
               AV15TFSistema_Descricao_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            AV43GXV1 = (int)(AV43GXV1+1);
         }
         if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(1));
            AV35DynamicFiltersSelector1 = AV33GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "MODULO_NOME") == 0 )
            {
               AV36Modulo_Nome1 = AV33GridStateDynamicFilter.gxTpr_Value;
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADMODULO_NOMEOPTIONS' Routine */
         AV10TFModulo_Nome = AV16SearchTxt;
         AV11TFModulo_Nome_Sel = "";
         AV45WWModuloDS_1_Sistema_areatrabalhocod = AV34Sistema_AreaTrabalhoCod;
         AV46WWModuloDS_2_Dynamicfiltersselector1 = AV35DynamicFiltersSelector1;
         AV47WWModuloDS_3_Modulo_nome1 = AV36Modulo_Nome1;
         AV48WWModuloDS_4_Tfmodulo_nome = AV10TFModulo_Nome;
         AV49WWModuloDS_5_Tfmodulo_nome_sel = AV11TFModulo_Nome_Sel;
         AV50WWModuloDS_6_Tfmodulo_sigla = AV12TFModulo_Sigla;
         AV51WWModuloDS_7_Tfmodulo_sigla_sel = AV13TFModulo_Sigla_Sel;
         AV52WWModuloDS_8_Tfsistema_descricao = AV14TFSistema_Descricao;
         AV53WWModuloDS_9_Tfsistema_descricao_sel = AV15TFSistema_Descricao_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV45WWModuloDS_1_Sistema_areatrabalhocod ,
                                              AV46WWModuloDS_2_Dynamicfiltersselector1 ,
                                              AV47WWModuloDS_3_Modulo_nome1 ,
                                              AV49WWModuloDS_5_Tfmodulo_nome_sel ,
                                              AV48WWModuloDS_4_Tfmodulo_nome ,
                                              AV51WWModuloDS_7_Tfmodulo_sigla_sel ,
                                              AV50WWModuloDS_6_Tfmodulo_sigla ,
                                              AV53WWModuloDS_9_Tfsistema_descricao_sel ,
                                              AV52WWModuloDS_8_Tfsistema_descricao ,
                                              A135Sistema_AreaTrabalhoCod ,
                                              A143Modulo_Nome ,
                                              A145Modulo_Sigla ,
                                              A128Sistema_Descricao },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV47WWModuloDS_3_Modulo_nome1 = StringUtil.PadR( StringUtil.RTrim( AV47WWModuloDS_3_Modulo_nome1), 50, "%");
         lV48WWModuloDS_4_Tfmodulo_nome = StringUtil.PadR( StringUtil.RTrim( AV48WWModuloDS_4_Tfmodulo_nome), 50, "%");
         lV50WWModuloDS_6_Tfmodulo_sigla = StringUtil.PadR( StringUtil.RTrim( AV50WWModuloDS_6_Tfmodulo_sigla), 15, "%");
         lV52WWModuloDS_8_Tfsistema_descricao = StringUtil.Concat( StringUtil.RTrim( AV52WWModuloDS_8_Tfsistema_descricao), "%", "");
         /* Using cursor P00G52 */
         pr_default.execute(0, new Object[] {AV45WWModuloDS_1_Sistema_areatrabalhocod, lV47WWModuloDS_3_Modulo_nome1, lV48WWModuloDS_4_Tfmodulo_nome, AV49WWModuloDS_5_Tfmodulo_nome_sel, lV50WWModuloDS_6_Tfmodulo_sigla, AV51WWModuloDS_7_Tfmodulo_sigla_sel, lV52WWModuloDS_8_Tfsistema_descricao, AV53WWModuloDS_9_Tfsistema_descricao_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKG52 = false;
            A127Sistema_Codigo = P00G52_A127Sistema_Codigo[0];
            A143Modulo_Nome = P00G52_A143Modulo_Nome[0];
            A128Sistema_Descricao = P00G52_A128Sistema_Descricao[0];
            n128Sistema_Descricao = P00G52_n128Sistema_Descricao[0];
            A145Modulo_Sigla = P00G52_A145Modulo_Sigla[0];
            A135Sistema_AreaTrabalhoCod = P00G52_A135Sistema_AreaTrabalhoCod[0];
            A146Modulo_Codigo = P00G52_A146Modulo_Codigo[0];
            A128Sistema_Descricao = P00G52_A128Sistema_Descricao[0];
            n128Sistema_Descricao = P00G52_n128Sistema_Descricao[0];
            A135Sistema_AreaTrabalhoCod = P00G52_A135Sistema_AreaTrabalhoCod[0];
            AV28count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00G52_A143Modulo_Nome[0], A143Modulo_Nome) == 0 ) )
            {
               BRKG52 = false;
               A146Modulo_Codigo = P00G52_A146Modulo_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKG52 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A143Modulo_Nome)) )
            {
               AV20Option = A143Modulo_Nome;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKG52 )
            {
               BRKG52 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADMODULO_SIGLAOPTIONS' Routine */
         AV12TFModulo_Sigla = AV16SearchTxt;
         AV13TFModulo_Sigla_Sel = "";
         AV45WWModuloDS_1_Sistema_areatrabalhocod = AV34Sistema_AreaTrabalhoCod;
         AV46WWModuloDS_2_Dynamicfiltersselector1 = AV35DynamicFiltersSelector1;
         AV47WWModuloDS_3_Modulo_nome1 = AV36Modulo_Nome1;
         AV48WWModuloDS_4_Tfmodulo_nome = AV10TFModulo_Nome;
         AV49WWModuloDS_5_Tfmodulo_nome_sel = AV11TFModulo_Nome_Sel;
         AV50WWModuloDS_6_Tfmodulo_sigla = AV12TFModulo_Sigla;
         AV51WWModuloDS_7_Tfmodulo_sigla_sel = AV13TFModulo_Sigla_Sel;
         AV52WWModuloDS_8_Tfsistema_descricao = AV14TFSistema_Descricao;
         AV53WWModuloDS_9_Tfsistema_descricao_sel = AV15TFSistema_Descricao_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV45WWModuloDS_1_Sistema_areatrabalhocod ,
                                              AV46WWModuloDS_2_Dynamicfiltersselector1 ,
                                              AV47WWModuloDS_3_Modulo_nome1 ,
                                              AV49WWModuloDS_5_Tfmodulo_nome_sel ,
                                              AV48WWModuloDS_4_Tfmodulo_nome ,
                                              AV51WWModuloDS_7_Tfmodulo_sigla_sel ,
                                              AV50WWModuloDS_6_Tfmodulo_sigla ,
                                              AV53WWModuloDS_9_Tfsistema_descricao_sel ,
                                              AV52WWModuloDS_8_Tfsistema_descricao ,
                                              A135Sistema_AreaTrabalhoCod ,
                                              A143Modulo_Nome ,
                                              A145Modulo_Sigla ,
                                              A128Sistema_Descricao },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV47WWModuloDS_3_Modulo_nome1 = StringUtil.PadR( StringUtil.RTrim( AV47WWModuloDS_3_Modulo_nome1), 50, "%");
         lV48WWModuloDS_4_Tfmodulo_nome = StringUtil.PadR( StringUtil.RTrim( AV48WWModuloDS_4_Tfmodulo_nome), 50, "%");
         lV50WWModuloDS_6_Tfmodulo_sigla = StringUtil.PadR( StringUtil.RTrim( AV50WWModuloDS_6_Tfmodulo_sigla), 15, "%");
         lV52WWModuloDS_8_Tfsistema_descricao = StringUtil.Concat( StringUtil.RTrim( AV52WWModuloDS_8_Tfsistema_descricao), "%", "");
         /* Using cursor P00G53 */
         pr_default.execute(1, new Object[] {AV45WWModuloDS_1_Sistema_areatrabalhocod, lV47WWModuloDS_3_Modulo_nome1, lV48WWModuloDS_4_Tfmodulo_nome, AV49WWModuloDS_5_Tfmodulo_nome_sel, lV50WWModuloDS_6_Tfmodulo_sigla, AV51WWModuloDS_7_Tfmodulo_sigla_sel, lV52WWModuloDS_8_Tfsistema_descricao, AV53WWModuloDS_9_Tfsistema_descricao_sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKG54 = false;
            A127Sistema_Codigo = P00G53_A127Sistema_Codigo[0];
            A145Modulo_Sigla = P00G53_A145Modulo_Sigla[0];
            A128Sistema_Descricao = P00G53_A128Sistema_Descricao[0];
            n128Sistema_Descricao = P00G53_n128Sistema_Descricao[0];
            A143Modulo_Nome = P00G53_A143Modulo_Nome[0];
            A135Sistema_AreaTrabalhoCod = P00G53_A135Sistema_AreaTrabalhoCod[0];
            A146Modulo_Codigo = P00G53_A146Modulo_Codigo[0];
            A128Sistema_Descricao = P00G53_A128Sistema_Descricao[0];
            n128Sistema_Descricao = P00G53_n128Sistema_Descricao[0];
            A135Sistema_AreaTrabalhoCod = P00G53_A135Sistema_AreaTrabalhoCod[0];
            AV28count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00G53_A145Modulo_Sigla[0], A145Modulo_Sigla) == 0 ) )
            {
               BRKG54 = false;
               A146Modulo_Codigo = P00G53_A146Modulo_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKG54 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A145Modulo_Sigla)) )
            {
               AV20Option = A145Modulo_Sigla;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKG54 )
            {
               BRKG54 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADSISTEMA_DESCRICAOOPTIONS' Routine */
         AV14TFSistema_Descricao = AV16SearchTxt;
         AV15TFSistema_Descricao_Sel = "";
         AV45WWModuloDS_1_Sistema_areatrabalhocod = AV34Sistema_AreaTrabalhoCod;
         AV46WWModuloDS_2_Dynamicfiltersselector1 = AV35DynamicFiltersSelector1;
         AV47WWModuloDS_3_Modulo_nome1 = AV36Modulo_Nome1;
         AV48WWModuloDS_4_Tfmodulo_nome = AV10TFModulo_Nome;
         AV49WWModuloDS_5_Tfmodulo_nome_sel = AV11TFModulo_Nome_Sel;
         AV50WWModuloDS_6_Tfmodulo_sigla = AV12TFModulo_Sigla;
         AV51WWModuloDS_7_Tfmodulo_sigla_sel = AV13TFModulo_Sigla_Sel;
         AV52WWModuloDS_8_Tfsistema_descricao = AV14TFSistema_Descricao;
         AV53WWModuloDS_9_Tfsistema_descricao_sel = AV15TFSistema_Descricao_Sel;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV45WWModuloDS_1_Sistema_areatrabalhocod ,
                                              AV46WWModuloDS_2_Dynamicfiltersselector1 ,
                                              AV47WWModuloDS_3_Modulo_nome1 ,
                                              AV49WWModuloDS_5_Tfmodulo_nome_sel ,
                                              AV48WWModuloDS_4_Tfmodulo_nome ,
                                              AV51WWModuloDS_7_Tfmodulo_sigla_sel ,
                                              AV50WWModuloDS_6_Tfmodulo_sigla ,
                                              AV53WWModuloDS_9_Tfsistema_descricao_sel ,
                                              AV52WWModuloDS_8_Tfsistema_descricao ,
                                              A135Sistema_AreaTrabalhoCod ,
                                              A143Modulo_Nome ,
                                              A145Modulo_Sigla ,
                                              A128Sistema_Descricao },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV47WWModuloDS_3_Modulo_nome1 = StringUtil.PadR( StringUtil.RTrim( AV47WWModuloDS_3_Modulo_nome1), 50, "%");
         lV48WWModuloDS_4_Tfmodulo_nome = StringUtil.PadR( StringUtil.RTrim( AV48WWModuloDS_4_Tfmodulo_nome), 50, "%");
         lV50WWModuloDS_6_Tfmodulo_sigla = StringUtil.PadR( StringUtil.RTrim( AV50WWModuloDS_6_Tfmodulo_sigla), 15, "%");
         lV52WWModuloDS_8_Tfsistema_descricao = StringUtil.Concat( StringUtil.RTrim( AV52WWModuloDS_8_Tfsistema_descricao), "%", "");
         /* Using cursor P00G54 */
         pr_default.execute(2, new Object[] {AV45WWModuloDS_1_Sistema_areatrabalhocod, lV47WWModuloDS_3_Modulo_nome1, lV48WWModuloDS_4_Tfmodulo_nome, AV49WWModuloDS_5_Tfmodulo_nome_sel, lV50WWModuloDS_6_Tfmodulo_sigla, AV51WWModuloDS_7_Tfmodulo_sigla_sel, lV52WWModuloDS_8_Tfsistema_descricao, AV53WWModuloDS_9_Tfsistema_descricao_sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKG56 = false;
            A127Sistema_Codigo = P00G54_A127Sistema_Codigo[0];
            A128Sistema_Descricao = P00G54_A128Sistema_Descricao[0];
            n128Sistema_Descricao = P00G54_n128Sistema_Descricao[0];
            A145Modulo_Sigla = P00G54_A145Modulo_Sigla[0];
            A143Modulo_Nome = P00G54_A143Modulo_Nome[0];
            A135Sistema_AreaTrabalhoCod = P00G54_A135Sistema_AreaTrabalhoCod[0];
            A146Modulo_Codigo = P00G54_A146Modulo_Codigo[0];
            A128Sistema_Descricao = P00G54_A128Sistema_Descricao[0];
            n128Sistema_Descricao = P00G54_n128Sistema_Descricao[0];
            A135Sistema_AreaTrabalhoCod = P00G54_A135Sistema_AreaTrabalhoCod[0];
            AV28count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00G54_A128Sistema_Descricao[0], A128Sistema_Descricao) == 0 ) )
            {
               BRKG56 = false;
               A127Sistema_Codigo = P00G54_A127Sistema_Codigo[0];
               A146Modulo_Codigo = P00G54_A146Modulo_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKG56 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A128Sistema_Descricao)) )
            {
               AV20Option = A128Sistema_Descricao;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKG56 )
            {
               BRKG56 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV21Options = new GxSimpleCollection();
         AV24OptionsDesc = new GxSimpleCollection();
         AV26OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV29Session = context.GetSession();
         AV31GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV32GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFModulo_Nome = "";
         AV11TFModulo_Nome_Sel = "";
         AV12TFModulo_Sigla = "";
         AV13TFModulo_Sigla_Sel = "";
         AV14TFSistema_Descricao = "";
         AV15TFSistema_Descricao_Sel = "";
         AV33GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV35DynamicFiltersSelector1 = "";
         AV36Modulo_Nome1 = "";
         AV46WWModuloDS_2_Dynamicfiltersselector1 = "";
         AV47WWModuloDS_3_Modulo_nome1 = "";
         AV48WWModuloDS_4_Tfmodulo_nome = "";
         AV49WWModuloDS_5_Tfmodulo_nome_sel = "";
         AV50WWModuloDS_6_Tfmodulo_sigla = "";
         AV51WWModuloDS_7_Tfmodulo_sigla_sel = "";
         AV52WWModuloDS_8_Tfsistema_descricao = "";
         AV53WWModuloDS_9_Tfsistema_descricao_sel = "";
         scmdbuf = "";
         lV47WWModuloDS_3_Modulo_nome1 = "";
         lV48WWModuloDS_4_Tfmodulo_nome = "";
         lV50WWModuloDS_6_Tfmodulo_sigla = "";
         lV52WWModuloDS_8_Tfsistema_descricao = "";
         A143Modulo_Nome = "";
         A145Modulo_Sigla = "";
         A128Sistema_Descricao = "";
         P00G52_A127Sistema_Codigo = new int[1] ;
         P00G52_A143Modulo_Nome = new String[] {""} ;
         P00G52_A128Sistema_Descricao = new String[] {""} ;
         P00G52_n128Sistema_Descricao = new bool[] {false} ;
         P00G52_A145Modulo_Sigla = new String[] {""} ;
         P00G52_A135Sistema_AreaTrabalhoCod = new int[1] ;
         P00G52_A146Modulo_Codigo = new int[1] ;
         AV20Option = "";
         P00G53_A127Sistema_Codigo = new int[1] ;
         P00G53_A145Modulo_Sigla = new String[] {""} ;
         P00G53_A128Sistema_Descricao = new String[] {""} ;
         P00G53_n128Sistema_Descricao = new bool[] {false} ;
         P00G53_A143Modulo_Nome = new String[] {""} ;
         P00G53_A135Sistema_AreaTrabalhoCod = new int[1] ;
         P00G53_A146Modulo_Codigo = new int[1] ;
         P00G54_A127Sistema_Codigo = new int[1] ;
         P00G54_A128Sistema_Descricao = new String[] {""} ;
         P00G54_n128Sistema_Descricao = new bool[] {false} ;
         P00G54_A145Modulo_Sigla = new String[] {""} ;
         P00G54_A143Modulo_Nome = new String[] {""} ;
         P00G54_A135Sistema_AreaTrabalhoCod = new int[1] ;
         P00G54_A146Modulo_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwmodulofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00G52_A127Sistema_Codigo, P00G52_A143Modulo_Nome, P00G52_A128Sistema_Descricao, P00G52_n128Sistema_Descricao, P00G52_A145Modulo_Sigla, P00G52_A135Sistema_AreaTrabalhoCod, P00G52_A146Modulo_Codigo
               }
               , new Object[] {
               P00G53_A127Sistema_Codigo, P00G53_A145Modulo_Sigla, P00G53_A128Sistema_Descricao, P00G53_n128Sistema_Descricao, P00G53_A143Modulo_Nome, P00G53_A135Sistema_AreaTrabalhoCod, P00G53_A146Modulo_Codigo
               }
               , new Object[] {
               P00G54_A127Sistema_Codigo, P00G54_A128Sistema_Descricao, P00G54_n128Sistema_Descricao, P00G54_A145Modulo_Sigla, P00G54_A143Modulo_Nome, P00G54_A135Sistema_AreaTrabalhoCod, P00G54_A146Modulo_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV43GXV1 ;
      private int AV34Sistema_AreaTrabalhoCod ;
      private int AV45WWModuloDS_1_Sistema_areatrabalhocod ;
      private int A135Sistema_AreaTrabalhoCod ;
      private int A127Sistema_Codigo ;
      private int A146Modulo_Codigo ;
      private long AV28count ;
      private String AV10TFModulo_Nome ;
      private String AV11TFModulo_Nome_Sel ;
      private String AV12TFModulo_Sigla ;
      private String AV13TFModulo_Sigla_Sel ;
      private String AV36Modulo_Nome1 ;
      private String AV47WWModuloDS_3_Modulo_nome1 ;
      private String AV48WWModuloDS_4_Tfmodulo_nome ;
      private String AV49WWModuloDS_5_Tfmodulo_nome_sel ;
      private String AV50WWModuloDS_6_Tfmodulo_sigla ;
      private String AV51WWModuloDS_7_Tfmodulo_sigla_sel ;
      private String scmdbuf ;
      private String lV47WWModuloDS_3_Modulo_nome1 ;
      private String lV48WWModuloDS_4_Tfmodulo_nome ;
      private String lV50WWModuloDS_6_Tfmodulo_sigla ;
      private String A143Modulo_Nome ;
      private String A145Modulo_Sigla ;
      private bool returnInSub ;
      private bool BRKG52 ;
      private bool n128Sistema_Descricao ;
      private bool BRKG54 ;
      private bool BRKG56 ;
      private String AV27OptionIndexesJson ;
      private String AV22OptionsJson ;
      private String AV25OptionsDescJson ;
      private String A128Sistema_Descricao ;
      private String AV18DDOName ;
      private String AV16SearchTxt ;
      private String AV17SearchTxtTo ;
      private String AV14TFSistema_Descricao ;
      private String AV15TFSistema_Descricao_Sel ;
      private String AV35DynamicFiltersSelector1 ;
      private String AV46WWModuloDS_2_Dynamicfiltersselector1 ;
      private String AV52WWModuloDS_8_Tfsistema_descricao ;
      private String AV53WWModuloDS_9_Tfsistema_descricao_sel ;
      private String lV52WWModuloDS_8_Tfsistema_descricao ;
      private String AV20Option ;
      private IGxSession AV29Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00G52_A127Sistema_Codigo ;
      private String[] P00G52_A143Modulo_Nome ;
      private String[] P00G52_A128Sistema_Descricao ;
      private bool[] P00G52_n128Sistema_Descricao ;
      private String[] P00G52_A145Modulo_Sigla ;
      private int[] P00G52_A135Sistema_AreaTrabalhoCod ;
      private int[] P00G52_A146Modulo_Codigo ;
      private int[] P00G53_A127Sistema_Codigo ;
      private String[] P00G53_A145Modulo_Sigla ;
      private String[] P00G53_A128Sistema_Descricao ;
      private bool[] P00G53_n128Sistema_Descricao ;
      private String[] P00G53_A143Modulo_Nome ;
      private int[] P00G53_A135Sistema_AreaTrabalhoCod ;
      private int[] P00G53_A146Modulo_Codigo ;
      private int[] P00G54_A127Sistema_Codigo ;
      private String[] P00G54_A128Sistema_Descricao ;
      private bool[] P00G54_n128Sistema_Descricao ;
      private String[] P00G54_A145Modulo_Sigla ;
      private String[] P00G54_A143Modulo_Nome ;
      private int[] P00G54_A135Sistema_AreaTrabalhoCod ;
      private int[] P00G54_A146Modulo_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV31GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV32GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV33GridStateDynamicFilter ;
   }

   public class getwwmodulofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00G52( IGxContext context ,
                                             int AV45WWModuloDS_1_Sistema_areatrabalhocod ,
                                             String AV46WWModuloDS_2_Dynamicfiltersselector1 ,
                                             String AV47WWModuloDS_3_Modulo_nome1 ,
                                             String AV49WWModuloDS_5_Tfmodulo_nome_sel ,
                                             String AV48WWModuloDS_4_Tfmodulo_nome ,
                                             String AV51WWModuloDS_7_Tfmodulo_sigla_sel ,
                                             String AV50WWModuloDS_6_Tfmodulo_sigla ,
                                             String AV53WWModuloDS_9_Tfsistema_descricao_sel ,
                                             String AV52WWModuloDS_8_Tfsistema_descricao ,
                                             int A135Sistema_AreaTrabalhoCod ,
                                             String A143Modulo_Nome ,
                                             String A145Modulo_Sigla ,
                                             String A128Sistema_Descricao )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [8] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Sistema_Codigo], T1.[Modulo_Nome], T2.[Sistema_Descricao], T1.[Modulo_Sigla], T2.[Sistema_AreaTrabalhoCod], T1.[Modulo_Codigo] FROM ([Modulo] T1 WITH (NOLOCK) INNER JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[Sistema_Codigo])";
         if ( ! (0==AV45WWModuloDS_1_Sistema_areatrabalhocod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_AreaTrabalhoCod] = @AV45WWModuloDS_1_Sistema_areatrabalhocod)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_AreaTrabalhoCod] = @AV45WWModuloDS_1_Sistema_areatrabalhocod)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV46WWModuloDS_2_Dynamicfiltersselector1, "MODULO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47WWModuloDS_3_Modulo_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Modulo_Nome] like '%' + @lV47WWModuloDS_3_Modulo_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Modulo_Nome] like '%' + @lV47WWModuloDS_3_Modulo_nome1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV49WWModuloDS_5_Tfmodulo_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48WWModuloDS_4_Tfmodulo_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Modulo_Nome] like @lV48WWModuloDS_4_Tfmodulo_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Modulo_Nome] like @lV48WWModuloDS_4_Tfmodulo_nome)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49WWModuloDS_5_Tfmodulo_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Modulo_Nome] = @AV49WWModuloDS_5_Tfmodulo_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Modulo_Nome] = @AV49WWModuloDS_5_Tfmodulo_nome_sel)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV51WWModuloDS_7_Tfmodulo_sigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50WWModuloDS_6_Tfmodulo_sigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Modulo_Sigla] like @lV50WWModuloDS_6_Tfmodulo_sigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Modulo_Sigla] like @lV50WWModuloDS_6_Tfmodulo_sigla)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51WWModuloDS_7_Tfmodulo_sigla_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Modulo_Sigla] = @AV51WWModuloDS_7_Tfmodulo_sigla_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Modulo_Sigla] = @AV51WWModuloDS_7_Tfmodulo_sigla_sel)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV53WWModuloDS_9_Tfsistema_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52WWModuloDS_8_Tfsistema_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Descricao] like @lV52WWModuloDS_8_Tfsistema_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Descricao] like @lV52WWModuloDS_8_Tfsistema_descricao)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWModuloDS_9_Tfsistema_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Descricao] = @AV53WWModuloDS_9_Tfsistema_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Descricao] = @AV53WWModuloDS_9_Tfsistema_descricao_sel)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Modulo_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00G53( IGxContext context ,
                                             int AV45WWModuloDS_1_Sistema_areatrabalhocod ,
                                             String AV46WWModuloDS_2_Dynamicfiltersselector1 ,
                                             String AV47WWModuloDS_3_Modulo_nome1 ,
                                             String AV49WWModuloDS_5_Tfmodulo_nome_sel ,
                                             String AV48WWModuloDS_4_Tfmodulo_nome ,
                                             String AV51WWModuloDS_7_Tfmodulo_sigla_sel ,
                                             String AV50WWModuloDS_6_Tfmodulo_sigla ,
                                             String AV53WWModuloDS_9_Tfsistema_descricao_sel ,
                                             String AV52WWModuloDS_8_Tfsistema_descricao ,
                                             int A135Sistema_AreaTrabalhoCod ,
                                             String A143Modulo_Nome ,
                                             String A145Modulo_Sigla ,
                                             String A128Sistema_Descricao )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [8] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Sistema_Codigo], T1.[Modulo_Sigla], T2.[Sistema_Descricao], T1.[Modulo_Nome], T2.[Sistema_AreaTrabalhoCod], T1.[Modulo_Codigo] FROM ([Modulo] T1 WITH (NOLOCK) INNER JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[Sistema_Codigo])";
         if ( ! (0==AV45WWModuloDS_1_Sistema_areatrabalhocod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_AreaTrabalhoCod] = @AV45WWModuloDS_1_Sistema_areatrabalhocod)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_AreaTrabalhoCod] = @AV45WWModuloDS_1_Sistema_areatrabalhocod)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV46WWModuloDS_2_Dynamicfiltersselector1, "MODULO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47WWModuloDS_3_Modulo_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Modulo_Nome] like '%' + @lV47WWModuloDS_3_Modulo_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Modulo_Nome] like '%' + @lV47WWModuloDS_3_Modulo_nome1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV49WWModuloDS_5_Tfmodulo_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48WWModuloDS_4_Tfmodulo_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Modulo_Nome] like @lV48WWModuloDS_4_Tfmodulo_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Modulo_Nome] like @lV48WWModuloDS_4_Tfmodulo_nome)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49WWModuloDS_5_Tfmodulo_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Modulo_Nome] = @AV49WWModuloDS_5_Tfmodulo_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Modulo_Nome] = @AV49WWModuloDS_5_Tfmodulo_nome_sel)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV51WWModuloDS_7_Tfmodulo_sigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50WWModuloDS_6_Tfmodulo_sigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Modulo_Sigla] like @lV50WWModuloDS_6_Tfmodulo_sigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Modulo_Sigla] like @lV50WWModuloDS_6_Tfmodulo_sigla)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51WWModuloDS_7_Tfmodulo_sigla_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Modulo_Sigla] = @AV51WWModuloDS_7_Tfmodulo_sigla_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Modulo_Sigla] = @AV51WWModuloDS_7_Tfmodulo_sigla_sel)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV53WWModuloDS_9_Tfsistema_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52WWModuloDS_8_Tfsistema_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Descricao] like @lV52WWModuloDS_8_Tfsistema_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Descricao] like @lV52WWModuloDS_8_Tfsistema_descricao)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWModuloDS_9_Tfsistema_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Descricao] = @AV53WWModuloDS_9_Tfsistema_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Descricao] = @AV53WWModuloDS_9_Tfsistema_descricao_sel)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Modulo_Sigla]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00G54( IGxContext context ,
                                             int AV45WWModuloDS_1_Sistema_areatrabalhocod ,
                                             String AV46WWModuloDS_2_Dynamicfiltersselector1 ,
                                             String AV47WWModuloDS_3_Modulo_nome1 ,
                                             String AV49WWModuloDS_5_Tfmodulo_nome_sel ,
                                             String AV48WWModuloDS_4_Tfmodulo_nome ,
                                             String AV51WWModuloDS_7_Tfmodulo_sigla_sel ,
                                             String AV50WWModuloDS_6_Tfmodulo_sigla ,
                                             String AV53WWModuloDS_9_Tfsistema_descricao_sel ,
                                             String AV52WWModuloDS_8_Tfsistema_descricao ,
                                             int A135Sistema_AreaTrabalhoCod ,
                                             String A143Modulo_Nome ,
                                             String A145Modulo_Sigla ,
                                             String A128Sistema_Descricao )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [8] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[Sistema_Codigo], T2.[Sistema_Descricao], T1.[Modulo_Sigla], T1.[Modulo_Nome], T2.[Sistema_AreaTrabalhoCod], T1.[Modulo_Codigo] FROM ([Modulo] T1 WITH (NOLOCK) INNER JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[Sistema_Codigo])";
         if ( ! (0==AV45WWModuloDS_1_Sistema_areatrabalhocod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_AreaTrabalhoCod] = @AV45WWModuloDS_1_Sistema_areatrabalhocod)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_AreaTrabalhoCod] = @AV45WWModuloDS_1_Sistema_areatrabalhocod)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV46WWModuloDS_2_Dynamicfiltersselector1, "MODULO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47WWModuloDS_3_Modulo_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Modulo_Nome] like '%' + @lV47WWModuloDS_3_Modulo_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Modulo_Nome] like '%' + @lV47WWModuloDS_3_Modulo_nome1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV49WWModuloDS_5_Tfmodulo_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48WWModuloDS_4_Tfmodulo_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Modulo_Nome] like @lV48WWModuloDS_4_Tfmodulo_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Modulo_Nome] like @lV48WWModuloDS_4_Tfmodulo_nome)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49WWModuloDS_5_Tfmodulo_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Modulo_Nome] = @AV49WWModuloDS_5_Tfmodulo_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Modulo_Nome] = @AV49WWModuloDS_5_Tfmodulo_nome_sel)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV51WWModuloDS_7_Tfmodulo_sigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50WWModuloDS_6_Tfmodulo_sigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Modulo_Sigla] like @lV50WWModuloDS_6_Tfmodulo_sigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Modulo_Sigla] like @lV50WWModuloDS_6_Tfmodulo_sigla)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51WWModuloDS_7_Tfmodulo_sigla_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Modulo_Sigla] = @AV51WWModuloDS_7_Tfmodulo_sigla_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Modulo_Sigla] = @AV51WWModuloDS_7_Tfmodulo_sigla_sel)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV53WWModuloDS_9_Tfsistema_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52WWModuloDS_8_Tfsistema_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Descricao] like @lV52WWModuloDS_8_Tfsistema_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Descricao] like @lV52WWModuloDS_8_Tfsistema_descricao)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWModuloDS_9_Tfsistema_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Descricao] = @AV53WWModuloDS_9_Tfsistema_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Descricao] = @AV53WWModuloDS_9_Tfsistema_descricao_sel)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T2.[Sistema_Descricao]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00G52(context, (int)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (int)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] );
               case 1 :
                     return conditional_P00G53(context, (int)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (int)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] );
               case 2 :
                     return conditional_P00G54(context, (int)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (int)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00G52 ;
          prmP00G52 = new Object[] {
          new Object[] {"@AV45WWModuloDS_1_Sistema_areatrabalhocod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV47WWModuloDS_3_Modulo_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV48WWModuloDS_4_Tfmodulo_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV49WWModuloDS_5_Tfmodulo_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV50WWModuloDS_6_Tfmodulo_sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV51WWModuloDS_7_Tfmodulo_sigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV52WWModuloDS_8_Tfsistema_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV53WWModuloDS_9_Tfsistema_descricao_sel",SqlDbType.VarChar,200,0}
          } ;
          Object[] prmP00G53 ;
          prmP00G53 = new Object[] {
          new Object[] {"@AV45WWModuloDS_1_Sistema_areatrabalhocod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV47WWModuloDS_3_Modulo_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV48WWModuloDS_4_Tfmodulo_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV49WWModuloDS_5_Tfmodulo_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV50WWModuloDS_6_Tfmodulo_sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV51WWModuloDS_7_Tfmodulo_sigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV52WWModuloDS_8_Tfsistema_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV53WWModuloDS_9_Tfsistema_descricao_sel",SqlDbType.VarChar,200,0}
          } ;
          Object[] prmP00G54 ;
          prmP00G54 = new Object[] {
          new Object[] {"@AV45WWModuloDS_1_Sistema_areatrabalhocod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV47WWModuloDS_3_Modulo_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV48WWModuloDS_4_Tfmodulo_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV49WWModuloDS_5_Tfmodulo_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV50WWModuloDS_6_Tfmodulo_sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV51WWModuloDS_7_Tfmodulo_sigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV52WWModuloDS_8_Tfsistema_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV53WWModuloDS_9_Tfsistema_descricao_sel",SqlDbType.VarChar,200,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00G52", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00G52,100,0,true,false )
             ,new CursorDef("P00G53", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00G53,100,0,true,false )
             ,new CursorDef("P00G54", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00G54,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 15) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 50) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 15) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 50) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[8]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[8]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[8]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwmodulofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwmodulofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwmodulofilterdata") )
          {
             return  ;
          }
          getwwmodulofilterdata worker = new getwwmodulofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
