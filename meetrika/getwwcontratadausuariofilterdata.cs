/*
               File: GetWWContratadaUsuarioFilterData
        Description: Get WWContratada Usuario Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:10:55.39
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwcontratadausuariofilterdata : GXProcedure
   {
      public getwwcontratadausuariofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwcontratadausuariofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
         return AV27OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwcontratadausuariofilterdata objgetwwcontratadausuariofilterdata;
         objgetwwcontratadausuariofilterdata = new getwwcontratadausuariofilterdata();
         objgetwwcontratadausuariofilterdata.AV18DDOName = aP0_DDOName;
         objgetwwcontratadausuariofilterdata.AV16SearchTxt = aP1_SearchTxt;
         objgetwwcontratadausuariofilterdata.AV17SearchTxtTo = aP2_SearchTxtTo;
         objgetwwcontratadausuariofilterdata.AV22OptionsJson = "" ;
         objgetwwcontratadausuariofilterdata.AV25OptionsDescJson = "" ;
         objgetwwcontratadausuariofilterdata.AV27OptionIndexesJson = "" ;
         objgetwwcontratadausuariofilterdata.context.SetSubmitInitialConfig(context);
         objgetwwcontratadausuariofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwcontratadausuariofilterdata);
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwcontratadausuariofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV21Options = (IGxCollection)(new GxSimpleCollection());
         AV24OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV26OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_USUARIO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADUSUARIO_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATADAUSUARIO_USUARIOPESSOANOMOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_CONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATADAUSUARIO_CONTRATADAPESSOANOMOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV22OptionsJson = AV21Options.ToJSonString(false);
         AV25OptionsDescJson = AV24OptionsDesc.ToJSonString(false);
         AV27OptionIndexesJson = AV26OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV29Session.Get("WWContratadaUsuarioGridState"), "") == 0 )
         {
            AV31GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWContratadaUsuarioGridState"), "");
         }
         else
         {
            AV31GridState.FromXml(AV29Session.Get("WWContratadaUsuarioGridState"), "");
         }
         AV45GXV1 = 1;
         while ( AV45GXV1 <= AV31GridState.gxTpr_Filtervalues.Count )
         {
            AV32GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV31GridState.gxTpr_Filtervalues.Item(AV45GXV1));
            if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFUSUARIO_NOME") == 0 )
            {
               AV10TFUsuario_Nome = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFUSUARIO_NOME_SEL") == 0 )
            {
               AV11TFUsuario_Nome_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 )
            {
               AV12TFContratadaUsuario_UsuarioPessoaNom = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATADAUSUARIO_USUARIOPESSOANOM_SEL") == 0 )
            {
               AV13TFContratadaUsuario_UsuarioPessoaNom_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATADAUSUARIO_CONTRATADAPESSOANOM") == 0 )
            {
               AV14TFContratadaUsuario_ContratadaPessoaNom = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFCONTRATADAUSUARIO_CONTRATADAPESSOANOM_SEL") == 0 )
            {
               AV15TFContratadaUsuario_ContratadaPessoaNom_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            AV45GXV1 = (int)(AV45GXV1+1);
         }
         if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(1));
            AV34DynamicFiltersSelector1 = AV33GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "USUARIO_NOME") == 0 )
            {
               AV35DynamicFiltersOperator1 = AV33GridStateDynamicFilter.gxTpr_Operator;
               AV36Usuario_Nome1 = AV33GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 )
            {
               AV35DynamicFiltersOperator1 = AV33GridStateDynamicFilter.gxTpr_Operator;
               AV37ContratadaUsuario_UsuarioPessoaNom1 = AV33GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV38DynamicFiltersEnabled2 = true;
               AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(2));
               AV39DynamicFiltersSelector2 = AV33GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "USUARIO_NOME") == 0 )
               {
                  AV40DynamicFiltersOperator2 = AV33GridStateDynamicFilter.gxTpr_Operator;
                  AV41Usuario_Nome2 = AV33GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 )
               {
                  AV40DynamicFiltersOperator2 = AV33GridStateDynamicFilter.gxTpr_Operator;
                  AV42ContratadaUsuario_UsuarioPessoaNom2 = AV33GridStateDynamicFilter.gxTpr_Value;
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADUSUARIO_NOMEOPTIONS' Routine */
         AV10TFUsuario_Nome = AV16SearchTxt;
         AV11TFUsuario_Nome_Sel = "";
         AV47WWContratadaUsuarioDS_1_Dynamicfiltersselector1 = AV34DynamicFiltersSelector1;
         AV48WWContratadaUsuarioDS_2_Dynamicfiltersoperator1 = AV35DynamicFiltersOperator1;
         AV49WWContratadaUsuarioDS_3_Usuario_nome1 = AV36Usuario_Nome1;
         AV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1 = AV37ContratadaUsuario_UsuarioPessoaNom1;
         AV51WWContratadaUsuarioDS_5_Dynamicfiltersenabled2 = AV38DynamicFiltersEnabled2;
         AV52WWContratadaUsuarioDS_6_Dynamicfiltersselector2 = AV39DynamicFiltersSelector2;
         AV53WWContratadaUsuarioDS_7_Dynamicfiltersoperator2 = AV40DynamicFiltersOperator2;
         AV54WWContratadaUsuarioDS_8_Usuario_nome2 = AV41Usuario_Nome2;
         AV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2 = AV42ContratadaUsuario_UsuarioPessoaNom2;
         AV56WWContratadaUsuarioDS_10_Tfusuario_nome = AV10TFUsuario_Nome;
         AV57WWContratadaUsuarioDS_11_Tfusuario_nome_sel = AV11TFUsuario_Nome_Sel;
         AV58WWContratadaUsuarioDS_12_Tfcontratadausuario_usuariopessoanom = AV12TFContratadaUsuario_UsuarioPessoaNom;
         AV59WWContratadaUsuarioDS_13_Tfcontratadausuario_usuariopessoanom_sel = AV13TFContratadaUsuario_UsuarioPessoaNom_Sel;
         AV60WWContratadaUsuarioDS_14_Tfcontratadausuario_contratadapessoanom = AV14TFContratadaUsuario_ContratadaPessoaNom;
         AV61WWContratadaUsuarioDS_15_Tfcontratadausuario_contratadapessoanom_sel = AV15TFContratadaUsuario_ContratadaPessoaNom_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV47WWContratadaUsuarioDS_1_Dynamicfiltersselector1 ,
                                              AV48WWContratadaUsuarioDS_2_Dynamicfiltersoperator1 ,
                                              AV49WWContratadaUsuarioDS_3_Usuario_nome1 ,
                                              AV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1 ,
                                              AV51WWContratadaUsuarioDS_5_Dynamicfiltersenabled2 ,
                                              AV52WWContratadaUsuarioDS_6_Dynamicfiltersselector2 ,
                                              AV53WWContratadaUsuarioDS_7_Dynamicfiltersoperator2 ,
                                              AV54WWContratadaUsuarioDS_8_Usuario_nome2 ,
                                              AV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2 ,
                                              AV57WWContratadaUsuarioDS_11_Tfusuario_nome_sel ,
                                              AV56WWContratadaUsuarioDS_10_Tfusuario_nome ,
                                              AV59WWContratadaUsuarioDS_13_Tfcontratadausuario_usuariopessoanom_sel ,
                                              AV58WWContratadaUsuarioDS_12_Tfcontratadausuario_usuariopessoanom ,
                                              AV61WWContratadaUsuarioDS_15_Tfcontratadausuario_contratadapessoanom_sel ,
                                              AV60WWContratadaUsuarioDS_14_Tfcontratadausuario_contratadapessoanom ,
                                              A2Usuario_Nome ,
                                              A71ContratadaUsuario_UsuarioPessoaNom ,
                                              A68ContratadaUsuario_ContratadaPessoaNom },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN
                                              }
         });
         lV49WWContratadaUsuarioDS_3_Usuario_nome1 = StringUtil.PadR( StringUtil.RTrim( AV49WWContratadaUsuarioDS_3_Usuario_nome1), 50, "%");
         lV49WWContratadaUsuarioDS_3_Usuario_nome1 = StringUtil.PadR( StringUtil.RTrim( AV49WWContratadaUsuarioDS_3_Usuario_nome1), 50, "%");
         lV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1), 100, "%");
         lV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1), 100, "%");
         lV54WWContratadaUsuarioDS_8_Usuario_nome2 = StringUtil.PadR( StringUtil.RTrim( AV54WWContratadaUsuarioDS_8_Usuario_nome2), 50, "%");
         lV54WWContratadaUsuarioDS_8_Usuario_nome2 = StringUtil.PadR( StringUtil.RTrim( AV54WWContratadaUsuarioDS_8_Usuario_nome2), 50, "%");
         lV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2), 100, "%");
         lV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2), 100, "%");
         lV56WWContratadaUsuarioDS_10_Tfusuario_nome = StringUtil.PadR( StringUtil.RTrim( AV56WWContratadaUsuarioDS_10_Tfusuario_nome), 50, "%");
         lV58WWContratadaUsuarioDS_12_Tfcontratadausuario_usuariopessoanom = StringUtil.PadR( StringUtil.RTrim( AV58WWContratadaUsuarioDS_12_Tfcontratadausuario_usuariopessoanom), 100, "%");
         lV60WWContratadaUsuarioDS_14_Tfcontratadausuario_contratadapessoanom = StringUtil.PadR( StringUtil.RTrim( AV60WWContratadaUsuarioDS_14_Tfcontratadausuario_contratadapessoanom), 100, "%");
         /* Using cursor P00LB2 */
         pr_default.execute(0, new Object[] {lV49WWContratadaUsuarioDS_3_Usuario_nome1, lV49WWContratadaUsuarioDS_3_Usuario_nome1, lV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1, lV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1, lV54WWContratadaUsuarioDS_8_Usuario_nome2, lV54WWContratadaUsuarioDS_8_Usuario_nome2, lV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2, lV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2, lV56WWContratadaUsuarioDS_10_Tfusuario_nome, AV57WWContratadaUsuarioDS_11_Tfusuario_nome_sel, lV58WWContratadaUsuarioDS_12_Tfcontratadausuario_usuariopessoanom, AV59WWContratadaUsuarioDS_13_Tfcontratadausuario_usuariopessoanom_sel, lV60WWContratadaUsuarioDS_14_Tfcontratadausuario_contratadapessoanom, AV61WWContratadaUsuarioDS_15_Tfcontratadausuario_contratadapessoanom_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKLB2 = false;
            A66ContratadaUsuario_ContratadaCod = P00LB2_A66ContratadaUsuario_ContratadaCod[0];
            A67ContratadaUsuario_ContratadaPessoaCod = P00LB2_A67ContratadaUsuario_ContratadaPessoaCod[0];
            n67ContratadaUsuario_ContratadaPessoaCod = P00LB2_n67ContratadaUsuario_ContratadaPessoaCod[0];
            A69ContratadaUsuario_UsuarioCod = P00LB2_A69ContratadaUsuario_UsuarioCod[0];
            A70ContratadaUsuario_UsuarioPessoaCod = P00LB2_A70ContratadaUsuario_UsuarioPessoaCod[0];
            n70ContratadaUsuario_UsuarioPessoaCod = P00LB2_n70ContratadaUsuario_UsuarioPessoaCod[0];
            A2Usuario_Nome = P00LB2_A2Usuario_Nome[0];
            n2Usuario_Nome = P00LB2_n2Usuario_Nome[0];
            A68ContratadaUsuario_ContratadaPessoaNom = P00LB2_A68ContratadaUsuario_ContratadaPessoaNom[0];
            n68ContratadaUsuario_ContratadaPessoaNom = P00LB2_n68ContratadaUsuario_ContratadaPessoaNom[0];
            A71ContratadaUsuario_UsuarioPessoaNom = P00LB2_A71ContratadaUsuario_UsuarioPessoaNom[0];
            n71ContratadaUsuario_UsuarioPessoaNom = P00LB2_n71ContratadaUsuario_UsuarioPessoaNom[0];
            A67ContratadaUsuario_ContratadaPessoaCod = P00LB2_A67ContratadaUsuario_ContratadaPessoaCod[0];
            n67ContratadaUsuario_ContratadaPessoaCod = P00LB2_n67ContratadaUsuario_ContratadaPessoaCod[0];
            A68ContratadaUsuario_ContratadaPessoaNom = P00LB2_A68ContratadaUsuario_ContratadaPessoaNom[0];
            n68ContratadaUsuario_ContratadaPessoaNom = P00LB2_n68ContratadaUsuario_ContratadaPessoaNom[0];
            A70ContratadaUsuario_UsuarioPessoaCod = P00LB2_A70ContratadaUsuario_UsuarioPessoaCod[0];
            n70ContratadaUsuario_UsuarioPessoaCod = P00LB2_n70ContratadaUsuario_UsuarioPessoaCod[0];
            A2Usuario_Nome = P00LB2_A2Usuario_Nome[0];
            n2Usuario_Nome = P00LB2_n2Usuario_Nome[0];
            A71ContratadaUsuario_UsuarioPessoaNom = P00LB2_A71ContratadaUsuario_UsuarioPessoaNom[0];
            n71ContratadaUsuario_UsuarioPessoaNom = P00LB2_n71ContratadaUsuario_UsuarioPessoaNom[0];
            AV28count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00LB2_A2Usuario_Nome[0], A2Usuario_Nome) == 0 ) )
            {
               BRKLB2 = false;
               A66ContratadaUsuario_ContratadaCod = P00LB2_A66ContratadaUsuario_ContratadaCod[0];
               A69ContratadaUsuario_UsuarioCod = P00LB2_A69ContratadaUsuario_UsuarioCod[0];
               AV28count = (long)(AV28count+1);
               BRKLB2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A2Usuario_Nome)) )
            {
               AV20Option = A2Usuario_Nome;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKLB2 )
            {
               BRKLB2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTRATADAUSUARIO_USUARIOPESSOANOMOPTIONS' Routine */
         AV12TFContratadaUsuario_UsuarioPessoaNom = AV16SearchTxt;
         AV13TFContratadaUsuario_UsuarioPessoaNom_Sel = "";
         AV47WWContratadaUsuarioDS_1_Dynamicfiltersselector1 = AV34DynamicFiltersSelector1;
         AV48WWContratadaUsuarioDS_2_Dynamicfiltersoperator1 = AV35DynamicFiltersOperator1;
         AV49WWContratadaUsuarioDS_3_Usuario_nome1 = AV36Usuario_Nome1;
         AV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1 = AV37ContratadaUsuario_UsuarioPessoaNom1;
         AV51WWContratadaUsuarioDS_5_Dynamicfiltersenabled2 = AV38DynamicFiltersEnabled2;
         AV52WWContratadaUsuarioDS_6_Dynamicfiltersselector2 = AV39DynamicFiltersSelector2;
         AV53WWContratadaUsuarioDS_7_Dynamicfiltersoperator2 = AV40DynamicFiltersOperator2;
         AV54WWContratadaUsuarioDS_8_Usuario_nome2 = AV41Usuario_Nome2;
         AV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2 = AV42ContratadaUsuario_UsuarioPessoaNom2;
         AV56WWContratadaUsuarioDS_10_Tfusuario_nome = AV10TFUsuario_Nome;
         AV57WWContratadaUsuarioDS_11_Tfusuario_nome_sel = AV11TFUsuario_Nome_Sel;
         AV58WWContratadaUsuarioDS_12_Tfcontratadausuario_usuariopessoanom = AV12TFContratadaUsuario_UsuarioPessoaNom;
         AV59WWContratadaUsuarioDS_13_Tfcontratadausuario_usuariopessoanom_sel = AV13TFContratadaUsuario_UsuarioPessoaNom_Sel;
         AV60WWContratadaUsuarioDS_14_Tfcontratadausuario_contratadapessoanom = AV14TFContratadaUsuario_ContratadaPessoaNom;
         AV61WWContratadaUsuarioDS_15_Tfcontratadausuario_contratadapessoanom_sel = AV15TFContratadaUsuario_ContratadaPessoaNom_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV47WWContratadaUsuarioDS_1_Dynamicfiltersselector1 ,
                                              AV48WWContratadaUsuarioDS_2_Dynamicfiltersoperator1 ,
                                              AV49WWContratadaUsuarioDS_3_Usuario_nome1 ,
                                              AV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1 ,
                                              AV51WWContratadaUsuarioDS_5_Dynamicfiltersenabled2 ,
                                              AV52WWContratadaUsuarioDS_6_Dynamicfiltersselector2 ,
                                              AV53WWContratadaUsuarioDS_7_Dynamicfiltersoperator2 ,
                                              AV54WWContratadaUsuarioDS_8_Usuario_nome2 ,
                                              AV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2 ,
                                              AV57WWContratadaUsuarioDS_11_Tfusuario_nome_sel ,
                                              AV56WWContratadaUsuarioDS_10_Tfusuario_nome ,
                                              AV59WWContratadaUsuarioDS_13_Tfcontratadausuario_usuariopessoanom_sel ,
                                              AV58WWContratadaUsuarioDS_12_Tfcontratadausuario_usuariopessoanom ,
                                              AV61WWContratadaUsuarioDS_15_Tfcontratadausuario_contratadapessoanom_sel ,
                                              AV60WWContratadaUsuarioDS_14_Tfcontratadausuario_contratadapessoanom ,
                                              A2Usuario_Nome ,
                                              A71ContratadaUsuario_UsuarioPessoaNom ,
                                              A68ContratadaUsuario_ContratadaPessoaNom },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN
                                              }
         });
         lV49WWContratadaUsuarioDS_3_Usuario_nome1 = StringUtil.PadR( StringUtil.RTrim( AV49WWContratadaUsuarioDS_3_Usuario_nome1), 50, "%");
         lV49WWContratadaUsuarioDS_3_Usuario_nome1 = StringUtil.PadR( StringUtil.RTrim( AV49WWContratadaUsuarioDS_3_Usuario_nome1), 50, "%");
         lV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1), 100, "%");
         lV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1), 100, "%");
         lV54WWContratadaUsuarioDS_8_Usuario_nome2 = StringUtil.PadR( StringUtil.RTrim( AV54WWContratadaUsuarioDS_8_Usuario_nome2), 50, "%");
         lV54WWContratadaUsuarioDS_8_Usuario_nome2 = StringUtil.PadR( StringUtil.RTrim( AV54WWContratadaUsuarioDS_8_Usuario_nome2), 50, "%");
         lV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2), 100, "%");
         lV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2), 100, "%");
         lV56WWContratadaUsuarioDS_10_Tfusuario_nome = StringUtil.PadR( StringUtil.RTrim( AV56WWContratadaUsuarioDS_10_Tfusuario_nome), 50, "%");
         lV58WWContratadaUsuarioDS_12_Tfcontratadausuario_usuariopessoanom = StringUtil.PadR( StringUtil.RTrim( AV58WWContratadaUsuarioDS_12_Tfcontratadausuario_usuariopessoanom), 100, "%");
         lV60WWContratadaUsuarioDS_14_Tfcontratadausuario_contratadapessoanom = StringUtil.PadR( StringUtil.RTrim( AV60WWContratadaUsuarioDS_14_Tfcontratadausuario_contratadapessoanom), 100, "%");
         /* Using cursor P00LB3 */
         pr_default.execute(1, new Object[] {lV49WWContratadaUsuarioDS_3_Usuario_nome1, lV49WWContratadaUsuarioDS_3_Usuario_nome1, lV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1, lV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1, lV54WWContratadaUsuarioDS_8_Usuario_nome2, lV54WWContratadaUsuarioDS_8_Usuario_nome2, lV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2, lV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2, lV56WWContratadaUsuarioDS_10_Tfusuario_nome, AV57WWContratadaUsuarioDS_11_Tfusuario_nome_sel, lV58WWContratadaUsuarioDS_12_Tfcontratadausuario_usuariopessoanom, AV59WWContratadaUsuarioDS_13_Tfcontratadausuario_usuariopessoanom_sel, lV60WWContratadaUsuarioDS_14_Tfcontratadausuario_contratadapessoanom, AV61WWContratadaUsuarioDS_15_Tfcontratadausuario_contratadapessoanom_sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKLB4 = false;
            A66ContratadaUsuario_ContratadaCod = P00LB3_A66ContratadaUsuario_ContratadaCod[0];
            A67ContratadaUsuario_ContratadaPessoaCod = P00LB3_A67ContratadaUsuario_ContratadaPessoaCod[0];
            n67ContratadaUsuario_ContratadaPessoaCod = P00LB3_n67ContratadaUsuario_ContratadaPessoaCod[0];
            A69ContratadaUsuario_UsuarioCod = P00LB3_A69ContratadaUsuario_UsuarioCod[0];
            A70ContratadaUsuario_UsuarioPessoaCod = P00LB3_A70ContratadaUsuario_UsuarioPessoaCod[0];
            n70ContratadaUsuario_UsuarioPessoaCod = P00LB3_n70ContratadaUsuario_UsuarioPessoaCod[0];
            A71ContratadaUsuario_UsuarioPessoaNom = P00LB3_A71ContratadaUsuario_UsuarioPessoaNom[0];
            n71ContratadaUsuario_UsuarioPessoaNom = P00LB3_n71ContratadaUsuario_UsuarioPessoaNom[0];
            A68ContratadaUsuario_ContratadaPessoaNom = P00LB3_A68ContratadaUsuario_ContratadaPessoaNom[0];
            n68ContratadaUsuario_ContratadaPessoaNom = P00LB3_n68ContratadaUsuario_ContratadaPessoaNom[0];
            A2Usuario_Nome = P00LB3_A2Usuario_Nome[0];
            n2Usuario_Nome = P00LB3_n2Usuario_Nome[0];
            A67ContratadaUsuario_ContratadaPessoaCod = P00LB3_A67ContratadaUsuario_ContratadaPessoaCod[0];
            n67ContratadaUsuario_ContratadaPessoaCod = P00LB3_n67ContratadaUsuario_ContratadaPessoaCod[0];
            A68ContratadaUsuario_ContratadaPessoaNom = P00LB3_A68ContratadaUsuario_ContratadaPessoaNom[0];
            n68ContratadaUsuario_ContratadaPessoaNom = P00LB3_n68ContratadaUsuario_ContratadaPessoaNom[0];
            A70ContratadaUsuario_UsuarioPessoaCod = P00LB3_A70ContratadaUsuario_UsuarioPessoaCod[0];
            n70ContratadaUsuario_UsuarioPessoaCod = P00LB3_n70ContratadaUsuario_UsuarioPessoaCod[0];
            A2Usuario_Nome = P00LB3_A2Usuario_Nome[0];
            n2Usuario_Nome = P00LB3_n2Usuario_Nome[0];
            A71ContratadaUsuario_UsuarioPessoaNom = P00LB3_A71ContratadaUsuario_UsuarioPessoaNom[0];
            n71ContratadaUsuario_UsuarioPessoaNom = P00LB3_n71ContratadaUsuario_UsuarioPessoaNom[0];
            AV28count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00LB3_A71ContratadaUsuario_UsuarioPessoaNom[0], A71ContratadaUsuario_UsuarioPessoaNom) == 0 ) )
            {
               BRKLB4 = false;
               A66ContratadaUsuario_ContratadaCod = P00LB3_A66ContratadaUsuario_ContratadaCod[0];
               A69ContratadaUsuario_UsuarioCod = P00LB3_A69ContratadaUsuario_UsuarioCod[0];
               A70ContratadaUsuario_UsuarioPessoaCod = P00LB3_A70ContratadaUsuario_UsuarioPessoaCod[0];
               n70ContratadaUsuario_UsuarioPessoaCod = P00LB3_n70ContratadaUsuario_UsuarioPessoaCod[0];
               A70ContratadaUsuario_UsuarioPessoaCod = P00LB3_A70ContratadaUsuario_UsuarioPessoaCod[0];
               n70ContratadaUsuario_UsuarioPessoaCod = P00LB3_n70ContratadaUsuario_UsuarioPessoaCod[0];
               AV28count = (long)(AV28count+1);
               BRKLB4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A71ContratadaUsuario_UsuarioPessoaNom)) )
            {
               AV20Option = A71ContratadaUsuario_UsuarioPessoaNom;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKLB4 )
            {
               BRKLB4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADCONTRATADAUSUARIO_CONTRATADAPESSOANOMOPTIONS' Routine */
         AV14TFContratadaUsuario_ContratadaPessoaNom = AV16SearchTxt;
         AV15TFContratadaUsuario_ContratadaPessoaNom_Sel = "";
         AV47WWContratadaUsuarioDS_1_Dynamicfiltersselector1 = AV34DynamicFiltersSelector1;
         AV48WWContratadaUsuarioDS_2_Dynamicfiltersoperator1 = AV35DynamicFiltersOperator1;
         AV49WWContratadaUsuarioDS_3_Usuario_nome1 = AV36Usuario_Nome1;
         AV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1 = AV37ContratadaUsuario_UsuarioPessoaNom1;
         AV51WWContratadaUsuarioDS_5_Dynamicfiltersenabled2 = AV38DynamicFiltersEnabled2;
         AV52WWContratadaUsuarioDS_6_Dynamicfiltersselector2 = AV39DynamicFiltersSelector2;
         AV53WWContratadaUsuarioDS_7_Dynamicfiltersoperator2 = AV40DynamicFiltersOperator2;
         AV54WWContratadaUsuarioDS_8_Usuario_nome2 = AV41Usuario_Nome2;
         AV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2 = AV42ContratadaUsuario_UsuarioPessoaNom2;
         AV56WWContratadaUsuarioDS_10_Tfusuario_nome = AV10TFUsuario_Nome;
         AV57WWContratadaUsuarioDS_11_Tfusuario_nome_sel = AV11TFUsuario_Nome_Sel;
         AV58WWContratadaUsuarioDS_12_Tfcontratadausuario_usuariopessoanom = AV12TFContratadaUsuario_UsuarioPessoaNom;
         AV59WWContratadaUsuarioDS_13_Tfcontratadausuario_usuariopessoanom_sel = AV13TFContratadaUsuario_UsuarioPessoaNom_Sel;
         AV60WWContratadaUsuarioDS_14_Tfcontratadausuario_contratadapessoanom = AV14TFContratadaUsuario_ContratadaPessoaNom;
         AV61WWContratadaUsuarioDS_15_Tfcontratadausuario_contratadapessoanom_sel = AV15TFContratadaUsuario_ContratadaPessoaNom_Sel;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV47WWContratadaUsuarioDS_1_Dynamicfiltersselector1 ,
                                              AV48WWContratadaUsuarioDS_2_Dynamicfiltersoperator1 ,
                                              AV49WWContratadaUsuarioDS_3_Usuario_nome1 ,
                                              AV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1 ,
                                              AV51WWContratadaUsuarioDS_5_Dynamicfiltersenabled2 ,
                                              AV52WWContratadaUsuarioDS_6_Dynamicfiltersselector2 ,
                                              AV53WWContratadaUsuarioDS_7_Dynamicfiltersoperator2 ,
                                              AV54WWContratadaUsuarioDS_8_Usuario_nome2 ,
                                              AV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2 ,
                                              AV57WWContratadaUsuarioDS_11_Tfusuario_nome_sel ,
                                              AV56WWContratadaUsuarioDS_10_Tfusuario_nome ,
                                              AV59WWContratadaUsuarioDS_13_Tfcontratadausuario_usuariopessoanom_sel ,
                                              AV58WWContratadaUsuarioDS_12_Tfcontratadausuario_usuariopessoanom ,
                                              AV61WWContratadaUsuarioDS_15_Tfcontratadausuario_contratadapessoanom_sel ,
                                              AV60WWContratadaUsuarioDS_14_Tfcontratadausuario_contratadapessoanom ,
                                              A2Usuario_Nome ,
                                              A71ContratadaUsuario_UsuarioPessoaNom ,
                                              A68ContratadaUsuario_ContratadaPessoaNom },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN
                                              }
         });
         lV49WWContratadaUsuarioDS_3_Usuario_nome1 = StringUtil.PadR( StringUtil.RTrim( AV49WWContratadaUsuarioDS_3_Usuario_nome1), 50, "%");
         lV49WWContratadaUsuarioDS_3_Usuario_nome1 = StringUtil.PadR( StringUtil.RTrim( AV49WWContratadaUsuarioDS_3_Usuario_nome1), 50, "%");
         lV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1), 100, "%");
         lV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1), 100, "%");
         lV54WWContratadaUsuarioDS_8_Usuario_nome2 = StringUtil.PadR( StringUtil.RTrim( AV54WWContratadaUsuarioDS_8_Usuario_nome2), 50, "%");
         lV54WWContratadaUsuarioDS_8_Usuario_nome2 = StringUtil.PadR( StringUtil.RTrim( AV54WWContratadaUsuarioDS_8_Usuario_nome2), 50, "%");
         lV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2), 100, "%");
         lV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2), 100, "%");
         lV56WWContratadaUsuarioDS_10_Tfusuario_nome = StringUtil.PadR( StringUtil.RTrim( AV56WWContratadaUsuarioDS_10_Tfusuario_nome), 50, "%");
         lV58WWContratadaUsuarioDS_12_Tfcontratadausuario_usuariopessoanom = StringUtil.PadR( StringUtil.RTrim( AV58WWContratadaUsuarioDS_12_Tfcontratadausuario_usuariopessoanom), 100, "%");
         lV60WWContratadaUsuarioDS_14_Tfcontratadausuario_contratadapessoanom = StringUtil.PadR( StringUtil.RTrim( AV60WWContratadaUsuarioDS_14_Tfcontratadausuario_contratadapessoanom), 100, "%");
         /* Using cursor P00LB4 */
         pr_default.execute(2, new Object[] {lV49WWContratadaUsuarioDS_3_Usuario_nome1, lV49WWContratadaUsuarioDS_3_Usuario_nome1, lV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1, lV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1, lV54WWContratadaUsuarioDS_8_Usuario_nome2, lV54WWContratadaUsuarioDS_8_Usuario_nome2, lV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2, lV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2, lV56WWContratadaUsuarioDS_10_Tfusuario_nome, AV57WWContratadaUsuarioDS_11_Tfusuario_nome_sel, lV58WWContratadaUsuarioDS_12_Tfcontratadausuario_usuariopessoanom, AV59WWContratadaUsuarioDS_13_Tfcontratadausuario_usuariopessoanom_sel, lV60WWContratadaUsuarioDS_14_Tfcontratadausuario_contratadapessoanom, AV61WWContratadaUsuarioDS_15_Tfcontratadausuario_contratadapessoanom_sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKLB6 = false;
            A66ContratadaUsuario_ContratadaCod = P00LB4_A66ContratadaUsuario_ContratadaCod[0];
            A67ContratadaUsuario_ContratadaPessoaCod = P00LB4_A67ContratadaUsuario_ContratadaPessoaCod[0];
            n67ContratadaUsuario_ContratadaPessoaCod = P00LB4_n67ContratadaUsuario_ContratadaPessoaCod[0];
            A69ContratadaUsuario_UsuarioCod = P00LB4_A69ContratadaUsuario_UsuarioCod[0];
            A70ContratadaUsuario_UsuarioPessoaCod = P00LB4_A70ContratadaUsuario_UsuarioPessoaCod[0];
            n70ContratadaUsuario_UsuarioPessoaCod = P00LB4_n70ContratadaUsuario_UsuarioPessoaCod[0];
            A68ContratadaUsuario_ContratadaPessoaNom = P00LB4_A68ContratadaUsuario_ContratadaPessoaNom[0];
            n68ContratadaUsuario_ContratadaPessoaNom = P00LB4_n68ContratadaUsuario_ContratadaPessoaNom[0];
            A71ContratadaUsuario_UsuarioPessoaNom = P00LB4_A71ContratadaUsuario_UsuarioPessoaNom[0];
            n71ContratadaUsuario_UsuarioPessoaNom = P00LB4_n71ContratadaUsuario_UsuarioPessoaNom[0];
            A2Usuario_Nome = P00LB4_A2Usuario_Nome[0];
            n2Usuario_Nome = P00LB4_n2Usuario_Nome[0];
            A67ContratadaUsuario_ContratadaPessoaCod = P00LB4_A67ContratadaUsuario_ContratadaPessoaCod[0];
            n67ContratadaUsuario_ContratadaPessoaCod = P00LB4_n67ContratadaUsuario_ContratadaPessoaCod[0];
            A68ContratadaUsuario_ContratadaPessoaNom = P00LB4_A68ContratadaUsuario_ContratadaPessoaNom[0];
            n68ContratadaUsuario_ContratadaPessoaNom = P00LB4_n68ContratadaUsuario_ContratadaPessoaNom[0];
            A70ContratadaUsuario_UsuarioPessoaCod = P00LB4_A70ContratadaUsuario_UsuarioPessoaCod[0];
            n70ContratadaUsuario_UsuarioPessoaCod = P00LB4_n70ContratadaUsuario_UsuarioPessoaCod[0];
            A2Usuario_Nome = P00LB4_A2Usuario_Nome[0];
            n2Usuario_Nome = P00LB4_n2Usuario_Nome[0];
            A71ContratadaUsuario_UsuarioPessoaNom = P00LB4_A71ContratadaUsuario_UsuarioPessoaNom[0];
            n71ContratadaUsuario_UsuarioPessoaNom = P00LB4_n71ContratadaUsuario_UsuarioPessoaNom[0];
            AV28count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00LB4_A68ContratadaUsuario_ContratadaPessoaNom[0], A68ContratadaUsuario_ContratadaPessoaNom) == 0 ) )
            {
               BRKLB6 = false;
               A66ContratadaUsuario_ContratadaCod = P00LB4_A66ContratadaUsuario_ContratadaCod[0];
               A67ContratadaUsuario_ContratadaPessoaCod = P00LB4_A67ContratadaUsuario_ContratadaPessoaCod[0];
               n67ContratadaUsuario_ContratadaPessoaCod = P00LB4_n67ContratadaUsuario_ContratadaPessoaCod[0];
               A69ContratadaUsuario_UsuarioCod = P00LB4_A69ContratadaUsuario_UsuarioCod[0];
               A67ContratadaUsuario_ContratadaPessoaCod = P00LB4_A67ContratadaUsuario_ContratadaPessoaCod[0];
               n67ContratadaUsuario_ContratadaPessoaCod = P00LB4_n67ContratadaUsuario_ContratadaPessoaCod[0];
               AV28count = (long)(AV28count+1);
               BRKLB6 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A68ContratadaUsuario_ContratadaPessoaNom)) )
            {
               AV20Option = A68ContratadaUsuario_ContratadaPessoaNom;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKLB6 )
            {
               BRKLB6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV21Options = new GxSimpleCollection();
         AV24OptionsDesc = new GxSimpleCollection();
         AV26OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV29Session = context.GetSession();
         AV31GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV32GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFUsuario_Nome = "";
         AV11TFUsuario_Nome_Sel = "";
         AV12TFContratadaUsuario_UsuarioPessoaNom = "";
         AV13TFContratadaUsuario_UsuarioPessoaNom_Sel = "";
         AV14TFContratadaUsuario_ContratadaPessoaNom = "";
         AV15TFContratadaUsuario_ContratadaPessoaNom_Sel = "";
         AV33GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV34DynamicFiltersSelector1 = "";
         AV36Usuario_Nome1 = "";
         AV37ContratadaUsuario_UsuarioPessoaNom1 = "";
         AV39DynamicFiltersSelector2 = "";
         AV41Usuario_Nome2 = "";
         AV42ContratadaUsuario_UsuarioPessoaNom2 = "";
         AV47WWContratadaUsuarioDS_1_Dynamicfiltersselector1 = "";
         AV49WWContratadaUsuarioDS_3_Usuario_nome1 = "";
         AV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1 = "";
         AV52WWContratadaUsuarioDS_6_Dynamicfiltersselector2 = "";
         AV54WWContratadaUsuarioDS_8_Usuario_nome2 = "";
         AV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2 = "";
         AV56WWContratadaUsuarioDS_10_Tfusuario_nome = "";
         AV57WWContratadaUsuarioDS_11_Tfusuario_nome_sel = "";
         AV58WWContratadaUsuarioDS_12_Tfcontratadausuario_usuariopessoanom = "";
         AV59WWContratadaUsuarioDS_13_Tfcontratadausuario_usuariopessoanom_sel = "";
         AV60WWContratadaUsuarioDS_14_Tfcontratadausuario_contratadapessoanom = "";
         AV61WWContratadaUsuarioDS_15_Tfcontratadausuario_contratadapessoanom_sel = "";
         scmdbuf = "";
         lV49WWContratadaUsuarioDS_3_Usuario_nome1 = "";
         lV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1 = "";
         lV54WWContratadaUsuarioDS_8_Usuario_nome2 = "";
         lV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2 = "";
         lV56WWContratadaUsuarioDS_10_Tfusuario_nome = "";
         lV58WWContratadaUsuarioDS_12_Tfcontratadausuario_usuariopessoanom = "";
         lV60WWContratadaUsuarioDS_14_Tfcontratadausuario_contratadapessoanom = "";
         A2Usuario_Nome = "";
         A71ContratadaUsuario_UsuarioPessoaNom = "";
         A68ContratadaUsuario_ContratadaPessoaNom = "";
         P00LB2_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         P00LB2_A67ContratadaUsuario_ContratadaPessoaCod = new int[1] ;
         P00LB2_n67ContratadaUsuario_ContratadaPessoaCod = new bool[] {false} ;
         P00LB2_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         P00LB2_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         P00LB2_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         P00LB2_A2Usuario_Nome = new String[] {""} ;
         P00LB2_n2Usuario_Nome = new bool[] {false} ;
         P00LB2_A68ContratadaUsuario_ContratadaPessoaNom = new String[] {""} ;
         P00LB2_n68ContratadaUsuario_ContratadaPessoaNom = new bool[] {false} ;
         P00LB2_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         P00LB2_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         AV20Option = "";
         P00LB3_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         P00LB3_A67ContratadaUsuario_ContratadaPessoaCod = new int[1] ;
         P00LB3_n67ContratadaUsuario_ContratadaPessoaCod = new bool[] {false} ;
         P00LB3_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         P00LB3_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         P00LB3_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         P00LB3_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         P00LB3_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         P00LB3_A68ContratadaUsuario_ContratadaPessoaNom = new String[] {""} ;
         P00LB3_n68ContratadaUsuario_ContratadaPessoaNom = new bool[] {false} ;
         P00LB3_A2Usuario_Nome = new String[] {""} ;
         P00LB3_n2Usuario_Nome = new bool[] {false} ;
         P00LB4_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         P00LB4_A67ContratadaUsuario_ContratadaPessoaCod = new int[1] ;
         P00LB4_n67ContratadaUsuario_ContratadaPessoaCod = new bool[] {false} ;
         P00LB4_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         P00LB4_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         P00LB4_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         P00LB4_A68ContratadaUsuario_ContratadaPessoaNom = new String[] {""} ;
         P00LB4_n68ContratadaUsuario_ContratadaPessoaNom = new bool[] {false} ;
         P00LB4_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         P00LB4_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         P00LB4_A2Usuario_Nome = new String[] {""} ;
         P00LB4_n2Usuario_Nome = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwcontratadausuariofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00LB2_A66ContratadaUsuario_ContratadaCod, P00LB2_A67ContratadaUsuario_ContratadaPessoaCod, P00LB2_n67ContratadaUsuario_ContratadaPessoaCod, P00LB2_A69ContratadaUsuario_UsuarioCod, P00LB2_A70ContratadaUsuario_UsuarioPessoaCod, P00LB2_n70ContratadaUsuario_UsuarioPessoaCod, P00LB2_A2Usuario_Nome, P00LB2_n2Usuario_Nome, P00LB2_A68ContratadaUsuario_ContratadaPessoaNom, P00LB2_n68ContratadaUsuario_ContratadaPessoaNom,
               P00LB2_A71ContratadaUsuario_UsuarioPessoaNom, P00LB2_n71ContratadaUsuario_UsuarioPessoaNom
               }
               , new Object[] {
               P00LB3_A66ContratadaUsuario_ContratadaCod, P00LB3_A67ContratadaUsuario_ContratadaPessoaCod, P00LB3_n67ContratadaUsuario_ContratadaPessoaCod, P00LB3_A69ContratadaUsuario_UsuarioCod, P00LB3_A70ContratadaUsuario_UsuarioPessoaCod, P00LB3_n70ContratadaUsuario_UsuarioPessoaCod, P00LB3_A71ContratadaUsuario_UsuarioPessoaNom, P00LB3_n71ContratadaUsuario_UsuarioPessoaNom, P00LB3_A68ContratadaUsuario_ContratadaPessoaNom, P00LB3_n68ContratadaUsuario_ContratadaPessoaNom,
               P00LB3_A2Usuario_Nome, P00LB3_n2Usuario_Nome
               }
               , new Object[] {
               P00LB4_A66ContratadaUsuario_ContratadaCod, P00LB4_A67ContratadaUsuario_ContratadaPessoaCod, P00LB4_n67ContratadaUsuario_ContratadaPessoaCod, P00LB4_A69ContratadaUsuario_UsuarioCod, P00LB4_A70ContratadaUsuario_UsuarioPessoaCod, P00LB4_n70ContratadaUsuario_UsuarioPessoaCod, P00LB4_A68ContratadaUsuario_ContratadaPessoaNom, P00LB4_n68ContratadaUsuario_ContratadaPessoaNom, P00LB4_A71ContratadaUsuario_UsuarioPessoaNom, P00LB4_n71ContratadaUsuario_UsuarioPessoaNom,
               P00LB4_A2Usuario_Nome, P00LB4_n2Usuario_Nome
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV35DynamicFiltersOperator1 ;
      private short AV40DynamicFiltersOperator2 ;
      private short AV48WWContratadaUsuarioDS_2_Dynamicfiltersoperator1 ;
      private short AV53WWContratadaUsuarioDS_7_Dynamicfiltersoperator2 ;
      private int AV45GXV1 ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A67ContratadaUsuario_ContratadaPessoaCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A70ContratadaUsuario_UsuarioPessoaCod ;
      private long AV28count ;
      private String AV10TFUsuario_Nome ;
      private String AV11TFUsuario_Nome_Sel ;
      private String AV12TFContratadaUsuario_UsuarioPessoaNom ;
      private String AV13TFContratadaUsuario_UsuarioPessoaNom_Sel ;
      private String AV14TFContratadaUsuario_ContratadaPessoaNom ;
      private String AV15TFContratadaUsuario_ContratadaPessoaNom_Sel ;
      private String AV36Usuario_Nome1 ;
      private String AV37ContratadaUsuario_UsuarioPessoaNom1 ;
      private String AV41Usuario_Nome2 ;
      private String AV42ContratadaUsuario_UsuarioPessoaNom2 ;
      private String AV49WWContratadaUsuarioDS_3_Usuario_nome1 ;
      private String AV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1 ;
      private String AV54WWContratadaUsuarioDS_8_Usuario_nome2 ;
      private String AV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2 ;
      private String AV56WWContratadaUsuarioDS_10_Tfusuario_nome ;
      private String AV57WWContratadaUsuarioDS_11_Tfusuario_nome_sel ;
      private String AV58WWContratadaUsuarioDS_12_Tfcontratadausuario_usuariopessoanom ;
      private String AV59WWContratadaUsuarioDS_13_Tfcontratadausuario_usuariopessoanom_sel ;
      private String AV60WWContratadaUsuarioDS_14_Tfcontratadausuario_contratadapessoanom ;
      private String AV61WWContratadaUsuarioDS_15_Tfcontratadausuario_contratadapessoanom_sel ;
      private String scmdbuf ;
      private String lV49WWContratadaUsuarioDS_3_Usuario_nome1 ;
      private String lV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1 ;
      private String lV54WWContratadaUsuarioDS_8_Usuario_nome2 ;
      private String lV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2 ;
      private String lV56WWContratadaUsuarioDS_10_Tfusuario_nome ;
      private String lV58WWContratadaUsuarioDS_12_Tfcontratadausuario_usuariopessoanom ;
      private String lV60WWContratadaUsuarioDS_14_Tfcontratadausuario_contratadapessoanom ;
      private String A2Usuario_Nome ;
      private String A71ContratadaUsuario_UsuarioPessoaNom ;
      private String A68ContratadaUsuario_ContratadaPessoaNom ;
      private bool returnInSub ;
      private bool AV38DynamicFiltersEnabled2 ;
      private bool AV51WWContratadaUsuarioDS_5_Dynamicfiltersenabled2 ;
      private bool BRKLB2 ;
      private bool n67ContratadaUsuario_ContratadaPessoaCod ;
      private bool n70ContratadaUsuario_UsuarioPessoaCod ;
      private bool n2Usuario_Nome ;
      private bool n68ContratadaUsuario_ContratadaPessoaNom ;
      private bool n71ContratadaUsuario_UsuarioPessoaNom ;
      private bool BRKLB4 ;
      private bool BRKLB6 ;
      private String AV27OptionIndexesJson ;
      private String AV22OptionsJson ;
      private String AV25OptionsDescJson ;
      private String AV18DDOName ;
      private String AV16SearchTxt ;
      private String AV17SearchTxtTo ;
      private String AV34DynamicFiltersSelector1 ;
      private String AV39DynamicFiltersSelector2 ;
      private String AV47WWContratadaUsuarioDS_1_Dynamicfiltersselector1 ;
      private String AV52WWContratadaUsuarioDS_6_Dynamicfiltersselector2 ;
      private String AV20Option ;
      private IGxSession AV29Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00LB2_A66ContratadaUsuario_ContratadaCod ;
      private int[] P00LB2_A67ContratadaUsuario_ContratadaPessoaCod ;
      private bool[] P00LB2_n67ContratadaUsuario_ContratadaPessoaCod ;
      private int[] P00LB2_A69ContratadaUsuario_UsuarioCod ;
      private int[] P00LB2_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] P00LB2_n70ContratadaUsuario_UsuarioPessoaCod ;
      private String[] P00LB2_A2Usuario_Nome ;
      private bool[] P00LB2_n2Usuario_Nome ;
      private String[] P00LB2_A68ContratadaUsuario_ContratadaPessoaNom ;
      private bool[] P00LB2_n68ContratadaUsuario_ContratadaPessoaNom ;
      private String[] P00LB2_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] P00LB2_n71ContratadaUsuario_UsuarioPessoaNom ;
      private int[] P00LB3_A66ContratadaUsuario_ContratadaCod ;
      private int[] P00LB3_A67ContratadaUsuario_ContratadaPessoaCod ;
      private bool[] P00LB3_n67ContratadaUsuario_ContratadaPessoaCod ;
      private int[] P00LB3_A69ContratadaUsuario_UsuarioCod ;
      private int[] P00LB3_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] P00LB3_n70ContratadaUsuario_UsuarioPessoaCod ;
      private String[] P00LB3_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] P00LB3_n71ContratadaUsuario_UsuarioPessoaNom ;
      private String[] P00LB3_A68ContratadaUsuario_ContratadaPessoaNom ;
      private bool[] P00LB3_n68ContratadaUsuario_ContratadaPessoaNom ;
      private String[] P00LB3_A2Usuario_Nome ;
      private bool[] P00LB3_n2Usuario_Nome ;
      private int[] P00LB4_A66ContratadaUsuario_ContratadaCod ;
      private int[] P00LB4_A67ContratadaUsuario_ContratadaPessoaCod ;
      private bool[] P00LB4_n67ContratadaUsuario_ContratadaPessoaCod ;
      private int[] P00LB4_A69ContratadaUsuario_UsuarioCod ;
      private int[] P00LB4_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] P00LB4_n70ContratadaUsuario_UsuarioPessoaCod ;
      private String[] P00LB4_A68ContratadaUsuario_ContratadaPessoaNom ;
      private bool[] P00LB4_n68ContratadaUsuario_ContratadaPessoaNom ;
      private String[] P00LB4_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] P00LB4_n71ContratadaUsuario_UsuarioPessoaNom ;
      private String[] P00LB4_A2Usuario_Nome ;
      private bool[] P00LB4_n2Usuario_Nome ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV31GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV32GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV33GridStateDynamicFilter ;
   }

   public class getwwcontratadausuariofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00LB2( IGxContext context ,
                                             String AV47WWContratadaUsuarioDS_1_Dynamicfiltersselector1 ,
                                             short AV48WWContratadaUsuarioDS_2_Dynamicfiltersoperator1 ,
                                             String AV49WWContratadaUsuarioDS_3_Usuario_nome1 ,
                                             String AV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1 ,
                                             bool AV51WWContratadaUsuarioDS_5_Dynamicfiltersenabled2 ,
                                             String AV52WWContratadaUsuarioDS_6_Dynamicfiltersselector2 ,
                                             short AV53WWContratadaUsuarioDS_7_Dynamicfiltersoperator2 ,
                                             String AV54WWContratadaUsuarioDS_8_Usuario_nome2 ,
                                             String AV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2 ,
                                             String AV57WWContratadaUsuarioDS_11_Tfusuario_nome_sel ,
                                             String AV56WWContratadaUsuarioDS_10_Tfusuario_nome ,
                                             String AV59WWContratadaUsuarioDS_13_Tfcontratadausuario_usuariopessoanom_sel ,
                                             String AV58WWContratadaUsuarioDS_12_Tfcontratadausuario_usuariopessoanom ,
                                             String AV61WWContratadaUsuarioDS_15_Tfcontratadausuario_contratadapessoanom_sel ,
                                             String AV60WWContratadaUsuarioDS_14_Tfcontratadausuario_contratadapessoanom ,
                                             String A2Usuario_Nome ,
                                             String A71ContratadaUsuario_UsuarioPessoaNom ,
                                             String A68ContratadaUsuario_ContratadaPessoaNom )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [14] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T2.[Contratada_PessoaCod] AS ContratadaUsuario_ContratadaPessoaCod, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T4.[Usuario_PessoaCod] AS ContratadaUsuario_UsuarioPessoaCod, T4.[Usuario_Nome], T3.[Pessoa_Nome] AS ContratadaUsuario_ContratadaPessoaNom, T5.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPesso FROM (((([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratada_PessoaCod]) INNER JOIN [Usuario] T4 WITH (NOLOCK) ON T4.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Usuario_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV47WWContratadaUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_NOME") == 0 ) && ( AV48WWContratadaUsuarioDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49WWContratadaUsuarioDS_3_Usuario_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like @lV49WWContratadaUsuarioDS_3_Usuario_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like @lV49WWContratadaUsuarioDS_3_Usuario_nome1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV47WWContratadaUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_NOME") == 0 ) && ( AV48WWContratadaUsuarioDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49WWContratadaUsuarioDS_3_Usuario_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like '%' + @lV49WWContratadaUsuarioDS_3_Usuario_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like '%' + @lV49WWContratadaUsuarioDS_3_Usuario_nome1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV47WWContratadaUsuarioDS_1_Dynamicfiltersselector1, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV48WWContratadaUsuarioDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV47WWContratadaUsuarioDS_1_Dynamicfiltersselector1, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV48WWContratadaUsuarioDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like '%' + @lV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV51WWContratadaUsuarioDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV52WWContratadaUsuarioDS_6_Dynamicfiltersselector2, "USUARIO_NOME") == 0 ) && ( AV53WWContratadaUsuarioDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWContratadaUsuarioDS_8_Usuario_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like @lV54WWContratadaUsuarioDS_8_Usuario_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like @lV54WWContratadaUsuarioDS_8_Usuario_nome2)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV51WWContratadaUsuarioDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV52WWContratadaUsuarioDS_6_Dynamicfiltersselector2, "USUARIO_NOME") == 0 ) && ( AV53WWContratadaUsuarioDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWContratadaUsuarioDS_8_Usuario_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like '%' + @lV54WWContratadaUsuarioDS_8_Usuario_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like '%' + @lV54WWContratadaUsuarioDS_8_Usuario_nome2)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV51WWContratadaUsuarioDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV52WWContratadaUsuarioDS_6_Dynamicfiltersselector2, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV53WWContratadaUsuarioDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV51WWContratadaUsuarioDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV52WWContratadaUsuarioDS_6_Dynamicfiltersselector2, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV53WWContratadaUsuarioDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like '%' + @lV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV57WWContratadaUsuarioDS_11_Tfusuario_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWContratadaUsuarioDS_10_Tfusuario_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like @lV56WWContratadaUsuarioDS_10_Tfusuario_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like @lV56WWContratadaUsuarioDS_10_Tfusuario_nome)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWContratadaUsuarioDS_11_Tfusuario_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] = @AV57WWContratadaUsuarioDS_11_Tfusuario_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] = @AV57WWContratadaUsuarioDS_11_Tfusuario_nome_sel)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV59WWContratadaUsuarioDS_13_Tfcontratadausuario_usuariopessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWContratadaUsuarioDS_12_Tfcontratadausuario_usuariopessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV58WWContratadaUsuarioDS_12_Tfcontratadausuario_usuariopessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV58WWContratadaUsuarioDS_12_Tfcontratadausuario_usuariopessoanom)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWContratadaUsuarioDS_13_Tfcontratadausuario_usuariopessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] = @AV59WWContratadaUsuarioDS_13_Tfcontratadausuario_usuariopessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] = @AV59WWContratadaUsuarioDS_13_Tfcontratadausuario_usuariopessoanom_sel)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV61WWContratadaUsuarioDS_15_Tfcontratadausuario_contratadapessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWContratadaUsuarioDS_14_Tfcontratadausuario_contratadapessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV60WWContratadaUsuarioDS_14_Tfcontratadausuario_contratadapessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV60WWContratadaUsuarioDS_14_Tfcontratadausuario_contratadapessoanom)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWContratadaUsuarioDS_15_Tfcontratadausuario_contratadapessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV61WWContratadaUsuarioDS_15_Tfcontratadausuario_contratadapessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] = @AV61WWContratadaUsuarioDS_15_Tfcontratadausuario_contratadapessoanom_sel)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T4.[Usuario_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00LB3( IGxContext context ,
                                             String AV47WWContratadaUsuarioDS_1_Dynamicfiltersselector1 ,
                                             short AV48WWContratadaUsuarioDS_2_Dynamicfiltersoperator1 ,
                                             String AV49WWContratadaUsuarioDS_3_Usuario_nome1 ,
                                             String AV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1 ,
                                             bool AV51WWContratadaUsuarioDS_5_Dynamicfiltersenabled2 ,
                                             String AV52WWContratadaUsuarioDS_6_Dynamicfiltersselector2 ,
                                             short AV53WWContratadaUsuarioDS_7_Dynamicfiltersoperator2 ,
                                             String AV54WWContratadaUsuarioDS_8_Usuario_nome2 ,
                                             String AV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2 ,
                                             String AV57WWContratadaUsuarioDS_11_Tfusuario_nome_sel ,
                                             String AV56WWContratadaUsuarioDS_10_Tfusuario_nome ,
                                             String AV59WWContratadaUsuarioDS_13_Tfcontratadausuario_usuariopessoanom_sel ,
                                             String AV58WWContratadaUsuarioDS_12_Tfcontratadausuario_usuariopessoanom ,
                                             String AV61WWContratadaUsuarioDS_15_Tfcontratadausuario_contratadapessoanom_sel ,
                                             String AV60WWContratadaUsuarioDS_14_Tfcontratadausuario_contratadapessoanom ,
                                             String A2Usuario_Nome ,
                                             String A71ContratadaUsuario_UsuarioPessoaNom ,
                                             String A68ContratadaUsuario_ContratadaPessoaNom )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [14] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T2.[Contratada_PessoaCod] AS ContratadaUsuario_ContratadaPessoaCod, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T4.[Usuario_PessoaCod] AS ContratadaUsuario_UsuarioPessoaCod, T5.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPesso, T3.[Pessoa_Nome] AS ContratadaUsuario_ContratadaPessoaNom, T4.[Usuario_Nome] FROM (((([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratada_PessoaCod]) INNER JOIN [Usuario] T4 WITH (NOLOCK) ON T4.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Usuario_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV47WWContratadaUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_NOME") == 0 ) && ( AV48WWContratadaUsuarioDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49WWContratadaUsuarioDS_3_Usuario_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like @lV49WWContratadaUsuarioDS_3_Usuario_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like @lV49WWContratadaUsuarioDS_3_Usuario_nome1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV47WWContratadaUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_NOME") == 0 ) && ( AV48WWContratadaUsuarioDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49WWContratadaUsuarioDS_3_Usuario_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like '%' + @lV49WWContratadaUsuarioDS_3_Usuario_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like '%' + @lV49WWContratadaUsuarioDS_3_Usuario_nome1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV47WWContratadaUsuarioDS_1_Dynamicfiltersselector1, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV48WWContratadaUsuarioDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV47WWContratadaUsuarioDS_1_Dynamicfiltersselector1, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV48WWContratadaUsuarioDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like '%' + @lV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV51WWContratadaUsuarioDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV52WWContratadaUsuarioDS_6_Dynamicfiltersselector2, "USUARIO_NOME") == 0 ) && ( AV53WWContratadaUsuarioDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWContratadaUsuarioDS_8_Usuario_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like @lV54WWContratadaUsuarioDS_8_Usuario_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like @lV54WWContratadaUsuarioDS_8_Usuario_nome2)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV51WWContratadaUsuarioDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV52WWContratadaUsuarioDS_6_Dynamicfiltersselector2, "USUARIO_NOME") == 0 ) && ( AV53WWContratadaUsuarioDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWContratadaUsuarioDS_8_Usuario_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like '%' + @lV54WWContratadaUsuarioDS_8_Usuario_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like '%' + @lV54WWContratadaUsuarioDS_8_Usuario_nome2)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV51WWContratadaUsuarioDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV52WWContratadaUsuarioDS_6_Dynamicfiltersselector2, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV53WWContratadaUsuarioDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV51WWContratadaUsuarioDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV52WWContratadaUsuarioDS_6_Dynamicfiltersselector2, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV53WWContratadaUsuarioDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like '%' + @lV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV57WWContratadaUsuarioDS_11_Tfusuario_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWContratadaUsuarioDS_10_Tfusuario_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like @lV56WWContratadaUsuarioDS_10_Tfusuario_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like @lV56WWContratadaUsuarioDS_10_Tfusuario_nome)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWContratadaUsuarioDS_11_Tfusuario_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] = @AV57WWContratadaUsuarioDS_11_Tfusuario_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] = @AV57WWContratadaUsuarioDS_11_Tfusuario_nome_sel)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV59WWContratadaUsuarioDS_13_Tfcontratadausuario_usuariopessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWContratadaUsuarioDS_12_Tfcontratadausuario_usuariopessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV58WWContratadaUsuarioDS_12_Tfcontratadausuario_usuariopessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV58WWContratadaUsuarioDS_12_Tfcontratadausuario_usuariopessoanom)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWContratadaUsuarioDS_13_Tfcontratadausuario_usuariopessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] = @AV59WWContratadaUsuarioDS_13_Tfcontratadausuario_usuariopessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] = @AV59WWContratadaUsuarioDS_13_Tfcontratadausuario_usuariopessoanom_sel)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV61WWContratadaUsuarioDS_15_Tfcontratadausuario_contratadapessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWContratadaUsuarioDS_14_Tfcontratadausuario_contratadapessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV60WWContratadaUsuarioDS_14_Tfcontratadausuario_contratadapessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV60WWContratadaUsuarioDS_14_Tfcontratadausuario_contratadapessoanom)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWContratadaUsuarioDS_15_Tfcontratadausuario_contratadapessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV61WWContratadaUsuarioDS_15_Tfcontratadausuario_contratadapessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] = @AV61WWContratadaUsuarioDS_15_Tfcontratadausuario_contratadapessoanom_sel)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T5.[Pessoa_Nome]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00LB4( IGxContext context ,
                                             String AV47WWContratadaUsuarioDS_1_Dynamicfiltersselector1 ,
                                             short AV48WWContratadaUsuarioDS_2_Dynamicfiltersoperator1 ,
                                             String AV49WWContratadaUsuarioDS_3_Usuario_nome1 ,
                                             String AV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1 ,
                                             bool AV51WWContratadaUsuarioDS_5_Dynamicfiltersenabled2 ,
                                             String AV52WWContratadaUsuarioDS_6_Dynamicfiltersselector2 ,
                                             short AV53WWContratadaUsuarioDS_7_Dynamicfiltersoperator2 ,
                                             String AV54WWContratadaUsuarioDS_8_Usuario_nome2 ,
                                             String AV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2 ,
                                             String AV57WWContratadaUsuarioDS_11_Tfusuario_nome_sel ,
                                             String AV56WWContratadaUsuarioDS_10_Tfusuario_nome ,
                                             String AV59WWContratadaUsuarioDS_13_Tfcontratadausuario_usuariopessoanom_sel ,
                                             String AV58WWContratadaUsuarioDS_12_Tfcontratadausuario_usuariopessoanom ,
                                             String AV61WWContratadaUsuarioDS_15_Tfcontratadausuario_contratadapessoanom_sel ,
                                             String AV60WWContratadaUsuarioDS_14_Tfcontratadausuario_contratadapessoanom ,
                                             String A2Usuario_Nome ,
                                             String A71ContratadaUsuario_UsuarioPessoaNom ,
                                             String A68ContratadaUsuario_ContratadaPessoaNom )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [14] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T2.[Contratada_PessoaCod] AS ContratadaUsuario_ContratadaPessoaCod, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T4.[Usuario_PessoaCod] AS ContratadaUsuario_UsuarioPessoaCod, T3.[Pessoa_Nome] AS ContratadaUsuario_ContratadaPessoaNom, T5.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPesso, T4.[Usuario_Nome] FROM (((([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratada_PessoaCod]) INNER JOIN [Usuario] T4 WITH (NOLOCK) ON T4.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Usuario_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV47WWContratadaUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_NOME") == 0 ) && ( AV48WWContratadaUsuarioDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49WWContratadaUsuarioDS_3_Usuario_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like @lV49WWContratadaUsuarioDS_3_Usuario_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like @lV49WWContratadaUsuarioDS_3_Usuario_nome1)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV47WWContratadaUsuarioDS_1_Dynamicfiltersselector1, "USUARIO_NOME") == 0 ) && ( AV48WWContratadaUsuarioDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49WWContratadaUsuarioDS_3_Usuario_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like '%' + @lV49WWContratadaUsuarioDS_3_Usuario_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like '%' + @lV49WWContratadaUsuarioDS_3_Usuario_nome1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV47WWContratadaUsuarioDS_1_Dynamicfiltersselector1, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV48WWContratadaUsuarioDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV47WWContratadaUsuarioDS_1_Dynamicfiltersselector1, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV48WWContratadaUsuarioDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like '%' + @lV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( AV51WWContratadaUsuarioDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV52WWContratadaUsuarioDS_6_Dynamicfiltersselector2, "USUARIO_NOME") == 0 ) && ( AV53WWContratadaUsuarioDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWContratadaUsuarioDS_8_Usuario_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like @lV54WWContratadaUsuarioDS_8_Usuario_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like @lV54WWContratadaUsuarioDS_8_Usuario_nome2)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV51WWContratadaUsuarioDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV52WWContratadaUsuarioDS_6_Dynamicfiltersselector2, "USUARIO_NOME") == 0 ) && ( AV53WWContratadaUsuarioDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWContratadaUsuarioDS_8_Usuario_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like '%' + @lV54WWContratadaUsuarioDS_8_Usuario_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like '%' + @lV54WWContratadaUsuarioDS_8_Usuario_nome2)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( AV51WWContratadaUsuarioDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV52WWContratadaUsuarioDS_6_Dynamicfiltersselector2, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV53WWContratadaUsuarioDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( AV51WWContratadaUsuarioDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV52WWContratadaUsuarioDS_6_Dynamicfiltersselector2, "CONTRATADAUSUARIO_USUARIOPESSOANOM") == 0 ) && ( AV53WWContratadaUsuarioDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like '%' + @lV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV57WWContratadaUsuarioDS_11_Tfusuario_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWContratadaUsuarioDS_10_Tfusuario_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] like @lV56WWContratadaUsuarioDS_10_Tfusuario_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] like @lV56WWContratadaUsuarioDS_10_Tfusuario_nome)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWContratadaUsuarioDS_11_Tfusuario_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_Nome] = @AV57WWContratadaUsuarioDS_11_Tfusuario_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_Nome] = @AV57WWContratadaUsuarioDS_11_Tfusuario_nome_sel)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV59WWContratadaUsuarioDS_13_Tfcontratadausuario_usuariopessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWContratadaUsuarioDS_12_Tfcontratadausuario_usuariopessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV58WWContratadaUsuarioDS_12_Tfcontratadausuario_usuariopessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV58WWContratadaUsuarioDS_12_Tfcontratadausuario_usuariopessoanom)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWContratadaUsuarioDS_13_Tfcontratadausuario_usuariopessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] = @AV59WWContratadaUsuarioDS_13_Tfcontratadausuario_usuariopessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] = @AV59WWContratadaUsuarioDS_13_Tfcontratadausuario_usuariopessoanom_sel)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV61WWContratadaUsuarioDS_15_Tfcontratadausuario_contratadapessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWContratadaUsuarioDS_14_Tfcontratadausuario_contratadapessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV60WWContratadaUsuarioDS_14_Tfcontratadausuario_contratadapessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV60WWContratadaUsuarioDS_14_Tfcontratadausuario_contratadapessoanom)";
            }
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWContratadaUsuarioDS_15_Tfcontratadausuario_contratadapessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV61WWContratadaUsuarioDS_15_Tfcontratadausuario_contratadapessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] = @AV61WWContratadaUsuarioDS_15_Tfcontratadausuario_contratadapessoanom_sel)";
            }
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T3.[Pessoa_Nome]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00LB2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] );
               case 1 :
                     return conditional_P00LB3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] );
               case 2 :
                     return conditional_P00LB4(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00LB2 ;
          prmP00LB2 = new Object[] {
          new Object[] {"@lV49WWContratadaUsuarioDS_3_Usuario_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV49WWContratadaUsuarioDS_3_Usuario_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV54WWContratadaUsuarioDS_8_Usuario_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV54WWContratadaUsuarioDS_8_Usuario_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV56WWContratadaUsuarioDS_10_Tfusuario_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV57WWContratadaUsuarioDS_11_Tfusuario_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV58WWContratadaUsuarioDS_12_Tfcontratadausuario_usuariopessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV59WWContratadaUsuarioDS_13_Tfcontratadausuario_usuariopessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV60WWContratadaUsuarioDS_14_Tfcontratadausuario_contratadapessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV61WWContratadaUsuarioDS_15_Tfcontratadausuario_contratadapessoanom_sel",SqlDbType.Char,100,0}
          } ;
          Object[] prmP00LB3 ;
          prmP00LB3 = new Object[] {
          new Object[] {"@lV49WWContratadaUsuarioDS_3_Usuario_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV49WWContratadaUsuarioDS_3_Usuario_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV54WWContratadaUsuarioDS_8_Usuario_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV54WWContratadaUsuarioDS_8_Usuario_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV56WWContratadaUsuarioDS_10_Tfusuario_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV57WWContratadaUsuarioDS_11_Tfusuario_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV58WWContratadaUsuarioDS_12_Tfcontratadausuario_usuariopessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV59WWContratadaUsuarioDS_13_Tfcontratadausuario_usuariopessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV60WWContratadaUsuarioDS_14_Tfcontratadausuario_contratadapessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV61WWContratadaUsuarioDS_15_Tfcontratadausuario_contratadapessoanom_sel",SqlDbType.Char,100,0}
          } ;
          Object[] prmP00LB4 ;
          prmP00LB4 = new Object[] {
          new Object[] {"@lV49WWContratadaUsuarioDS_3_Usuario_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV49WWContratadaUsuarioDS_3_Usuario_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV50WWContratadaUsuarioDS_4_Contratadausuario_usuariopessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV54WWContratadaUsuarioDS_8_Usuario_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV54WWContratadaUsuarioDS_8_Usuario_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV55WWContratadaUsuarioDS_9_Contratadausuario_usuariopessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV56WWContratadaUsuarioDS_10_Tfusuario_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV57WWContratadaUsuarioDS_11_Tfusuario_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV58WWContratadaUsuarioDS_12_Tfcontratadausuario_usuariopessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV59WWContratadaUsuarioDS_13_Tfcontratadausuario_usuariopessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV60WWContratadaUsuarioDS_14_Tfcontratadausuario_contratadapessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV61WWContratadaUsuarioDS_15_Tfcontratadausuario_contratadapessoanom_sel",SqlDbType.Char,100,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00LB2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00LB2,100,0,true,false )
             ,new CursorDef("P00LB3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00LB3,100,0,true,false )
             ,new CursorDef("P00LB4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00LB4,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 50) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 50) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwcontratadausuariofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwcontratadausuariofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwcontratadausuariofilterdata") )
          {
             return  ;
          }
          getwwcontratadausuariofilterdata worker = new getwwcontratadausuariofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
