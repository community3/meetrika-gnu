/**@preserve  GeneXus C# 10_3_14-114418 on 3/1/2020 17:20:3.58
*/
gx.evt.autoSkip = false;
gx.define('areatrabalhogeneral', true, function (CmpContext) {
   this.ServerClass =  "areatrabalhogeneral" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("web");
   this.setCmpContext(CmpContext);
   this.ReadonlyForm = true;
   this.hasEnterEvent = false;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.ajaxSecurityToken =  true ;
   this.SetStandaloneVars=function()
   {
   };
   this.Valid_Areatrabalho_organizacaocod=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("AREATRABALHO_ORGANIZACAOCOD");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Valid_Contratante_codigo=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("CONTRATANTE_CODIGO");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Valid_Contratante_pessoacod=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("CONTRATANTE_PESSOACOD");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Valid_Estado_uf=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("ESTADO_UF");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Valid_Municipio_codigo=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("MUNICIPIO_CODIGO");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Valid_Areatrabalho_codigo=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("AREATRABALHO_CODIGO");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.e130v2_client=function()
   {
      this.executeServerEvent("'DOUPDATE'", false, null, false, false);
   };
   this.e140v2_client=function()
   {
      this.executeServerEvent("'DODELETE'", false, null, false, false);
   };
   this.e150v2_client=function()
   {
      this.executeServerEvent("ENTER", true, null, false, false);
   };
   this.e160v2_client=function()
   {
      this.executeServerEvent("CANCEL", true, null, false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,8,11,13,15,17,19,21,24,27,29,36,38,40,42,44,46,49,51,53,55,57,59,64,67,69,71,73,75,77,80,82,84,86,88,90,93,95,97,99,101,103,106,108,110,112,117,120,122,125,131,132,133,134,135,136];
   this.GXLastCtrlId =136;
   this.DVPANEL_CONTRATANTEContainer = gx.uc.getNew(this, 62, 13, "BootstrapPanel", this.CmpContext + "DVPANEL_CONTRATANTEContainer", "Dvpanel_contratante");
   var DVPANEL_CONTRATANTEContainer = this.DVPANEL_CONTRATANTEContainer;
   DVPANEL_CONTRATANTEContainer.setProp("Width", "Width", "100%", "str");
   DVPANEL_CONTRATANTEContainer.setProp("Height", "Height", "100", "str");
   DVPANEL_CONTRATANTEContainer.setProp("AutoWidth", "Autowidth", false, "bool");
   DVPANEL_CONTRATANTEContainer.setProp("AutoHeight", "Autoheight", true, "bool");
   DVPANEL_CONTRATANTEContainer.setProp("Cls", "Cls", "GXUI-DVelop-Panel", "str");
   DVPANEL_CONTRATANTEContainer.setProp("ShowHeader", "Showheader", true, "bool");
   DVPANEL_CONTRATANTEContainer.setProp("Title", "Title", "Contratante", "str");
   DVPANEL_CONTRATANTEContainer.setProp("Collapsible", "Collapsible", false, "bool");
   DVPANEL_CONTRATANTEContainer.setProp("Collapsed", "Collapsed", false, "bool");
   DVPANEL_CONTRATANTEContainer.setProp("ShowCollapseIcon", "Showcollapseicon", false, "bool");
   DVPANEL_CONTRATANTEContainer.setProp("IconPosition", "Iconposition", "left", "str");
   DVPANEL_CONTRATANTEContainer.setProp("AutoScroll", "Autoscroll", false, "bool");
   DVPANEL_CONTRATANTEContainer.setProp("Visible", "Visible", true, "bool");
   DVPANEL_CONTRATANTEContainer.setProp("Enabled", "Enabled", true, "boolean");
   DVPANEL_CONTRATANTEContainer.setProp("Class", "Class", "", "char");
   DVPANEL_CONTRATANTEContainer.setC2ShowFunction(function(UC) { UC.show(); });
   this.setUserControl(DVPANEL_CONTRATANTEContainer);
   GXValidFnc[2]={fld:"TABLECONTENT",grid:0};
   GXValidFnc[8]={fld:"TABLEATTRIBUTES",grid:0};
   GXValidFnc[11]={fld:"TEXTBLOCKORGANIZACAO_NOME", format:0,grid:0};
   GXValidFnc[13]={lvl:0,type:"char",len:50,dec:0,sign:false,pic:"@!",ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"ORGANIZACAO_NOME",gxz:"Z1214Organizacao_Nome",gxold:"O1214Organizacao_Nome",gxvar:"A1214Organizacao_Nome",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A1214Organizacao_Nome=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z1214Organizacao_Nome=Value},v2c:function(){gx.fn.setControlValue("ORGANIZACAO_NOME",gx.O.A1214Organizacao_Nome,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1214Organizacao_Nome=this.val()},val:function(){return gx.fn.getControlValue("ORGANIZACAO_NOME")},nac:gx.falseFn};
   this.declareDomainHdlr( 13 , function() {
   });
   GXValidFnc[15]={fld:"TEXTBLOCKAREATRABALHO_TIPOPLANILHA", format:0,grid:0};
   GXValidFnc[17]={lvl:0,type:"int",len:2,dec:0,sign:false,pic:"Z9",ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"AREATRABALHO_TIPOPLANILHA",gxz:"Z1154AreaTrabalho_TipoPlanilha",gxold:"O1154AreaTrabalho_TipoPlanilha",gxvar:"A1154AreaTrabalho_TipoPlanilha",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A1154AreaTrabalho_TipoPlanilha=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1154AreaTrabalho_TipoPlanilha=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("AREATRABALHO_TIPOPLANILHA",gx.O.A1154AreaTrabalho_TipoPlanilha,0)},c2v:function(){if(this.val()!==undefined)gx.O.A1154AreaTrabalho_TipoPlanilha=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("AREATRABALHO_TIPOPLANILHA",'.')},nac:gx.falseFn};
   GXValidFnc[19]={fld:"TEXTBLOCKAREATRABALHO_SS_CODIGO", format:0,grid:0};
   GXValidFnc[21]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"AREATRABALHO_SS_CODIGO",gxz:"Z1588AreaTrabalho_SS_Codigo",gxold:"O1588AreaTrabalho_SS_Codigo",gxvar:"A1588AreaTrabalho_SS_Codigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A1588AreaTrabalho_SS_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1588AreaTrabalho_SS_Codigo=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("AREATRABALHO_SS_CODIGO",gx.O.A1588AreaTrabalho_SS_Codigo,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1588AreaTrabalho_SS_Codigo=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("AREATRABALHO_SS_CODIGO",'.')},nac:gx.falseFn};
   this.declareDomainHdlr( 21 , function() {
   });
   GXValidFnc[24]={fld:"UNNAMEDTABLE2",grid:0};
   GXValidFnc[27]={fld:"TEXTBLOCKAREATRABALHO_DESCRICAO", format:0,grid:0};
   GXValidFnc[29]={lvl:0,type:"svchar",len:50,dec:0,sign:false,pic:"@!",ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"AREATRABALHO_DESCRICAO",gxz:"Z6AreaTrabalho_Descricao",gxold:"O6AreaTrabalho_Descricao",gxvar:"A6AreaTrabalho_Descricao",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A6AreaTrabalho_Descricao=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z6AreaTrabalho_Descricao=Value},v2c:function(){gx.fn.setControlValue("AREATRABALHO_DESCRICAO",gx.O.A6AreaTrabalho_Descricao,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A6AreaTrabalho_Descricao=this.val()},val:function(){return gx.fn.getControlValue("AREATRABALHO_DESCRICAO")},nac:gx.falseFn};
   this.declareDomainHdlr( 29 , function() {
   });
   GXValidFnc[36]={fld:"TEXTBLOCKAREATRABALHO_CALCULOPFINAL", format:0,grid:0};
   GXValidFnc[38]={lvl:0,type:"char",len:2,dec:0,sign:false,ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"AREATRABALHO_CALCULOPFINAL",gxz:"Z642AreaTrabalho_CalculoPFinal",gxold:"O642AreaTrabalho_CalculoPFinal",gxvar:"A642AreaTrabalho_CalculoPFinal",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.A642AreaTrabalho_CalculoPFinal=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z642AreaTrabalho_CalculoPFinal=Value},v2c:function(){gx.fn.setComboBoxValue("AREATRABALHO_CALCULOPFINAL",gx.O.A642AreaTrabalho_CalculoPFinal);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A642AreaTrabalho_CalculoPFinal=this.val()},val:function(){return gx.fn.getControlValue("AREATRABALHO_CALCULOPFINAL")},nac:gx.falseFn};
   this.declareDomainHdlr( 38 , function() {
   });
   GXValidFnc[40]={fld:"TEXTBLOCKAREATRABALHO_SERVICOPADRAO", format:0,grid:0};
   GXValidFnc[42]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"AREATRABALHO_SERVICOPADRAO",gxz:"Z830AreaTrabalho_ServicoPadrao",gxold:"O830AreaTrabalho_ServicoPadrao",gxvar:"A830AreaTrabalho_ServicoPadrao",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A830AreaTrabalho_ServicoPadrao=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z830AreaTrabalho_ServicoPadrao=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("AREATRABALHO_SERVICOPADRAO",gx.O.A830AreaTrabalho_ServicoPadrao,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A830AreaTrabalho_ServicoPadrao=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("AREATRABALHO_SERVICOPADRAO",'.')},nac:gx.falseFn};
   this.declareDomainHdlr( 42 , function() {
   });
   GXValidFnc[44]={fld:"TEXTBLOCKAREATRABALHO_VALIDAOSFM", format:0,grid:0};
   GXValidFnc[46]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"AREATRABALHO_VALIDAOSFM",gxz:"Z834AreaTrabalho_ValidaOSFM",gxold:"O834AreaTrabalho_ValidaOSFM",gxvar:"A834AreaTrabalho_ValidaOSFM",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.A834AreaTrabalho_ValidaOSFM=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z834AreaTrabalho_ValidaOSFM=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setComboBoxValue("AREATRABALHO_VALIDAOSFM",gx.O.A834AreaTrabalho_ValidaOSFM)},c2v:function(){if(this.val()!==undefined)gx.O.A834AreaTrabalho_ValidaOSFM=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("AREATRABALHO_VALIDAOSFM")},nac:gx.falseFn};
   GXValidFnc[49]={fld:"TEXTBLOCKAREATRABALHO_DIASPARAPAGAR", format:0,grid:0};
   GXValidFnc[51]={lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"AREATRABALHO_DIASPARAPAGAR",gxz:"Z855AreaTrabalho_DiasParaPagar",gxold:"O855AreaTrabalho_DiasParaPagar",gxvar:"A855AreaTrabalho_DiasParaPagar",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A855AreaTrabalho_DiasParaPagar=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z855AreaTrabalho_DiasParaPagar=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("AREATRABALHO_DIASPARAPAGAR",gx.O.A855AreaTrabalho_DiasParaPagar,0)},c2v:function(){if(this.val()!==undefined)gx.O.A855AreaTrabalho_DiasParaPagar=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("AREATRABALHO_DIASPARAPAGAR",'.')},nac:gx.falseFn};
   GXValidFnc[53]={fld:"TEXTBLOCKAREATRABALHO_CONTRATADAUPDBSLCOD", format:0,grid:0};
   GXValidFnc[55]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"AREATRABALHO_CONTRATADAUPDBSLCOD",gxz:"Z987AreaTrabalho_ContratadaUpdBslCod",gxold:"O987AreaTrabalho_ContratadaUpdBslCod",gxvar:"A987AreaTrabalho_ContratadaUpdBslCod",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A987AreaTrabalho_ContratadaUpdBslCod=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z987AreaTrabalho_ContratadaUpdBslCod=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("AREATRABALHO_CONTRATADAUPDBSLCOD",gx.O.A987AreaTrabalho_ContratadaUpdBslCod,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A987AreaTrabalho_ContratadaUpdBslCod=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("AREATRABALHO_CONTRATADAUPDBSLCOD",'.')},nac:gx.falseFn};
   this.declareDomainHdlr( 55 , function() {
   });
   GXValidFnc[57]={fld:"TEXTBLOCKAREATRABALHO_VERTA", format:0,grid:0};
   GXValidFnc[59]={lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"AREATRABALHO_VERTA",gxz:"Z2081AreaTrabalho_VerTA",gxold:"O2081AreaTrabalho_VerTA",gxvar:"A2081AreaTrabalho_VerTA",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.A2081AreaTrabalho_VerTA=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z2081AreaTrabalho_VerTA=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("AREATRABALHO_VERTA",gx.O.A2081AreaTrabalho_VerTA)},c2v:function(){if(this.val()!==undefined)gx.O.A2081AreaTrabalho_VerTA=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("AREATRABALHO_VERTA",'.')},nac:gx.falseFn};
   GXValidFnc[64]={fld:"CONTRATANTE",grid:0};
   GXValidFnc[67]={fld:"TEXTBLOCKCONTRATANTE_CNPJ", format:0,grid:0};
   GXValidFnc[69]={lvl:0,type:"svchar",len:15,dec:0,sign:false,ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATANTE_CNPJ",gxz:"Z12Contratante_CNPJ",gxold:"O12Contratante_CNPJ",gxvar:"A12Contratante_CNPJ",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A12Contratante_CNPJ=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z12Contratante_CNPJ=Value},v2c:function(){gx.fn.setControlValue("CONTRATANTE_CNPJ",gx.O.A12Contratante_CNPJ,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A12Contratante_CNPJ=this.val()},val:function(){return gx.fn.getControlValue("CONTRATANTE_CNPJ")},nac:gx.falseFn};
   this.declareDomainHdlr( 69 , function() {
   });
   GXValidFnc[71]={fld:"TEXTBLOCKCONTRATANTE_RAZAOSOCIAL", format:0,grid:0};
   GXValidFnc[73]={lvl:0,type:"char",len:100,dec:0,sign:false,pic:"@!",ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATANTE_RAZAOSOCIAL",gxz:"Z9Contratante_RazaoSocial",gxold:"O9Contratante_RazaoSocial",gxvar:"A9Contratante_RazaoSocial",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A9Contratante_RazaoSocial=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z9Contratante_RazaoSocial=Value},v2c:function(){gx.fn.setControlValue("CONTRATANTE_RAZAOSOCIAL",gx.O.A9Contratante_RazaoSocial,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A9Contratante_RazaoSocial=this.val()},val:function(){return gx.fn.getControlValue("CONTRATANTE_RAZAOSOCIAL")},nac:gx.falseFn};
   this.declareDomainHdlr( 73 , function() {
   });
   GXValidFnc[75]={fld:"TEXTBLOCKCONTRATANTE_NOMEFANTASIA", format:0,grid:0};
   GXValidFnc[77]={lvl:0,type:"char",len:100,dec:0,sign:false,pic:"@!",ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATANTE_NOMEFANTASIA",gxz:"Z10Contratante_NomeFantasia",gxold:"O10Contratante_NomeFantasia",gxvar:"A10Contratante_NomeFantasia",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A10Contratante_NomeFantasia=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z10Contratante_NomeFantasia=Value},v2c:function(){gx.fn.setControlValue("CONTRATANTE_NOMEFANTASIA",gx.O.A10Contratante_NomeFantasia,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A10Contratante_NomeFantasia=this.val()},val:function(){return gx.fn.getControlValue("CONTRATANTE_NOMEFANTASIA")},nac:gx.falseFn};
   this.declareDomainHdlr( 77 , function() {
   });
   GXValidFnc[80]={fld:"TEXTBLOCKCONTRATANTE_IE", format:0,grid:0};
   GXValidFnc[82]={lvl:0,type:"char",len:15,dec:0,sign:false,ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATANTE_IE",gxz:"Z11Contratante_IE",gxold:"O11Contratante_IE",gxvar:"A11Contratante_IE",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A11Contratante_IE=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z11Contratante_IE=Value},v2c:function(){gx.fn.setControlValue("CONTRATANTE_IE",gx.O.A11Contratante_IE,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A11Contratante_IE=this.val()},val:function(){return gx.fn.getControlValue("CONTRATANTE_IE")},nac:gx.falseFn};
   this.declareDomainHdlr( 82 , function() {
   });
   GXValidFnc[84]={fld:"TEXTBLOCKCONTRATANTE_WEBSITE", format:0,grid:0};
   GXValidFnc[86]={lvl:0,type:"svchar",len:100,dec:0,sign:false,ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATANTE_WEBSITE",gxz:"Z13Contratante_WebSite",gxold:"O13Contratante_WebSite",gxvar:"A13Contratante_WebSite",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A13Contratante_WebSite=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z13Contratante_WebSite=Value},v2c:function(){gx.fn.setControlValue("CONTRATANTE_WEBSITE",gx.O.A13Contratante_WebSite,0)},c2v:function(){if(this.val()!==undefined)gx.O.A13Contratante_WebSite=this.val()},val:function(){return gx.fn.getControlValue("CONTRATANTE_WEBSITE")},nac:gx.falseFn};
   GXValidFnc[88]={fld:"TEXTBLOCKCONTRATANTE_EMAIL", format:0,grid:0};
   GXValidFnc[90]={lvl:0,type:"svchar",len:100,dec:0,sign:false,ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATANTE_EMAIL",gxz:"Z14Contratante_Email",gxold:"O14Contratante_Email",gxvar:"A14Contratante_Email",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A14Contratante_Email=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z14Contratante_Email=Value},v2c:function(){gx.fn.setControlValue("CONTRATANTE_EMAIL",gx.O.A14Contratante_Email,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A14Contratante_Email=this.val()},val:function(){return gx.fn.getControlValue("CONTRATANTE_EMAIL")},nac:gx.falseFn};
   this.declareDomainHdlr( 90 , function() {
      gx.fn.setCtrlProperty("CONTRATANTE_EMAIL","Link", (!gx.fn.getCtrlProperty("CONTRATANTE_EMAIL","Enabled") ?  : "") );
   });
   GXValidFnc[93]={fld:"TEXTBLOCKCONTRATANTE_TELEFONE", format:0,grid:0};
   GXValidFnc[95]={lvl:0,type:"char",len:20,dec:0,sign:false,ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATANTE_TELEFONE",gxz:"Z31Contratante_Telefone",gxold:"O31Contratante_Telefone",gxvar:"A31Contratante_Telefone",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A31Contratante_Telefone=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z31Contratante_Telefone=Value},v2c:function(){gx.fn.setControlValue("CONTRATANTE_TELEFONE",gx.O.A31Contratante_Telefone,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A31Contratante_Telefone=this.val()},val:function(){return gx.fn.getControlValue("CONTRATANTE_TELEFONE")},nac:gx.falseFn};
   this.declareDomainHdlr( 95 , function() {
   });
   GXValidFnc[97]={fld:"TEXTBLOCKCONTRATANTE_RAMAL", format:0,grid:0};
   GXValidFnc[99]={lvl:0,type:"char",len:10,dec:0,sign:false,ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATANTE_RAMAL",gxz:"Z32Contratante_Ramal",gxold:"O32Contratante_Ramal",gxvar:"A32Contratante_Ramal",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A32Contratante_Ramal=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z32Contratante_Ramal=Value},v2c:function(){gx.fn.setControlValue("CONTRATANTE_RAMAL",gx.O.A32Contratante_Ramal,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A32Contratante_Ramal=this.val()},val:function(){return gx.fn.getControlValue("CONTRATANTE_RAMAL")},nac:gx.falseFn};
   this.declareDomainHdlr( 99 , function() {
   });
   GXValidFnc[101]={fld:"TEXTBLOCKCONTRATANTE_FAX", format:0,grid:0};
   GXValidFnc[103]={lvl:0,type:"char",len:20,dec:0,sign:false,ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATANTE_FAX",gxz:"Z33Contratante_Fax",gxold:"O33Contratante_Fax",gxvar:"A33Contratante_Fax",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A33Contratante_Fax=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z33Contratante_Fax=Value},v2c:function(){gx.fn.setControlValue("CONTRATANTE_FAX",gx.O.A33Contratante_Fax,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A33Contratante_Fax=this.val()},val:function(){return gx.fn.getControlValue("CONTRATANTE_FAX")},nac:gx.falseFn};
   this.declareDomainHdlr( 103 , function() {
   });
   GXValidFnc[106]={fld:"TEXTBLOCKESTADO_NOME", format:0,grid:0};
   GXValidFnc[108]={lvl:0,type:"char",len:50,dec:0,sign:false,pic:"@!",ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"ESTADO_NOME",gxz:"Z24Estado_Nome",gxold:"O24Estado_Nome",gxvar:"A24Estado_Nome",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A24Estado_Nome=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z24Estado_Nome=Value},v2c:function(){gx.fn.setControlValue("ESTADO_NOME",gx.O.A24Estado_Nome,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A24Estado_Nome=this.val()},val:function(){return gx.fn.getControlValue("ESTADO_NOME")},nac:gx.falseFn};
   this.declareDomainHdlr( 108 , function() {
   });
   GXValidFnc[110]={fld:"TEXTBLOCKMUNICIPIO_NOME", format:0,grid:0};
   GXValidFnc[112]={lvl:0,type:"char",len:50,dec:0,sign:false,pic:"@!",ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"MUNICIPIO_NOME",gxz:"Z26Municipio_Nome",gxold:"O26Municipio_Nome",gxvar:"A26Municipio_Nome",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A26Municipio_Nome=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z26Municipio_Nome=Value},v2c:function(){gx.fn.setControlValue("MUNICIPIO_NOME",gx.O.A26Municipio_Nome,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A26Municipio_Nome=this.val()},val:function(){return gx.fn.getControlValue("MUNICIPIO_NOME")},nac:gx.falseFn};
   this.declareDomainHdlr( 112 , function() {
   });
   GXValidFnc[117]={fld:"UNNAMEDTABLE1",grid:0};
   GXValidFnc[120]={fld:"TEXTBLOCKAREATRABALHO_ATIVO", format:0,grid:0};
   GXValidFnc[122]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"AREATRABALHO_ATIVO",gxz:"Z72AreaTrabalho_Ativo",gxold:"O72AreaTrabalho_Ativo",gxvar:"A72AreaTrabalho_Ativo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"checkbox",v2v:function(Value){if(Value!==undefined)gx.O.A72AreaTrabalho_Ativo=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z72AreaTrabalho_Ativo=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setCheckBoxValue("AREATRABALHO_ATIVO",gx.O.A72AreaTrabalho_Ativo,true);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A72AreaTrabalho_Ativo=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("AREATRABALHO_ATIVO")},nac:gx.falseFn,values:['true','false']};
   this.declareDomainHdlr( 122 , function() {
   });
   GXValidFnc[125]={fld:"TABLEACTIONS",grid:0};
   GXValidFnc[131]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,grid:0,gxgrid:null,fnc:this.Valid_Areatrabalho_organizacaocod,isvalid:null,rgrid:[],fld:"AREATRABALHO_ORGANIZACAOCOD",gxz:"Z1216AreaTrabalho_OrganizacaoCod",gxold:"O1216AreaTrabalho_OrganizacaoCod",gxvar:"A1216AreaTrabalho_OrganizacaoCod",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A1216AreaTrabalho_OrganizacaoCod=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1216AreaTrabalho_OrganizacaoCod=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("AREATRABALHO_ORGANIZACAOCOD",gx.O.A1216AreaTrabalho_OrganizacaoCod,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1216AreaTrabalho_OrganizacaoCod=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("AREATRABALHO_ORGANIZACAOCOD",'.')},nac:gx.falseFn};
   this.declareDomainHdlr( 131 , function() {
   });
   GXValidFnc[132]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,grid:0,gxgrid:null,fnc:this.Valid_Contratante_codigo,isvalid:null,rgrid:[],fld:"CONTRATANTE_CODIGO",gxz:"Z29Contratante_Codigo",gxold:"O29Contratante_Codigo",gxvar:"A29Contratante_Codigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A29Contratante_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z29Contratante_Codigo=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("CONTRATANTE_CODIGO",gx.O.A29Contratante_Codigo,0)},c2v:function(){if(this.val()!==undefined)gx.O.A29Contratante_Codigo=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("CONTRATANTE_CODIGO",'.')},nac:gx.falseFn};
   GXValidFnc[133]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,grid:0,gxgrid:null,fnc:this.Valid_Contratante_pessoacod,isvalid:null,rgrid:[],fld:"CONTRATANTE_PESSOACOD",gxz:"Z335Contratante_PessoaCod",gxold:"O335Contratante_PessoaCod",gxvar:"A335Contratante_PessoaCod",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A335Contratante_PessoaCod=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z335Contratante_PessoaCod=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("CONTRATANTE_PESSOACOD",gx.O.A335Contratante_PessoaCod,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A335Contratante_PessoaCod=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("CONTRATANTE_PESSOACOD",'.')},nac:gx.falseFn};
   this.declareDomainHdlr( 133 , function() {
   });
   GXValidFnc[134]={lvl:0,type:"char",len:2,dec:0,sign:false,pic:"@!",ro:1,grid:0,gxgrid:null,fnc:this.Valid_Estado_uf,isvalid:null,rgrid:[],fld:"ESTADO_UF",gxz:"Z23Estado_UF",gxold:"O23Estado_UF",gxvar:"A23Estado_UF",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A23Estado_UF=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z23Estado_UF=Value},v2c:function(){gx.fn.setControlValue("ESTADO_UF",gx.O.A23Estado_UF,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A23Estado_UF=this.val()},val:function(){return gx.fn.getControlValue("ESTADO_UF")},nac:gx.falseFn};
   this.declareDomainHdlr( 134 , function() {
   });
   GXValidFnc[135]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,grid:0,gxgrid:null,fnc:this.Valid_Municipio_codigo,isvalid:null,rgrid:[],fld:"MUNICIPIO_CODIGO",gxz:"Z25Municipio_Codigo",gxold:"O25Municipio_Codigo",gxvar:"A25Municipio_Codigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A25Municipio_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z25Municipio_Codigo=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("MUNICIPIO_CODIGO",gx.O.A25Municipio_Codigo,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A25Municipio_Codigo=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("MUNICIPIO_CODIGO",'.')},nac:gx.falseFn};
   this.declareDomainHdlr( 135 , function() {
   });
   GXValidFnc[136]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,grid:0,gxgrid:null,fnc:this.Valid_Areatrabalho_codigo,isvalid:null,rgrid:[],fld:"AREATRABALHO_CODIGO",gxz:"Z5AreaTrabalho_Codigo",gxold:"O5AreaTrabalho_Codigo",gxvar:"A5AreaTrabalho_Codigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A5AreaTrabalho_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z5AreaTrabalho_Codigo=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("AREATRABALHO_CODIGO",gx.O.A5AreaTrabalho_Codigo,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A5AreaTrabalho_Codigo=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("AREATRABALHO_CODIGO",'.')},nac:gx.falseFn};
   this.declareDomainHdlr( 136 , function() {
   });
   this.A1214Organizacao_Nome = "" ;
   this.Z1214Organizacao_Nome = "" ;
   this.O1214Organizacao_Nome = "" ;
   this.A1154AreaTrabalho_TipoPlanilha = 0 ;
   this.Z1154AreaTrabalho_TipoPlanilha = 0 ;
   this.O1154AreaTrabalho_TipoPlanilha = 0 ;
   this.A1588AreaTrabalho_SS_Codigo = 0 ;
   this.Z1588AreaTrabalho_SS_Codigo = 0 ;
   this.O1588AreaTrabalho_SS_Codigo = 0 ;
   this.A6AreaTrabalho_Descricao = "" ;
   this.Z6AreaTrabalho_Descricao = "" ;
   this.O6AreaTrabalho_Descricao = "" ;
   this.A642AreaTrabalho_CalculoPFinal = "" ;
   this.Z642AreaTrabalho_CalculoPFinal = "" ;
   this.O642AreaTrabalho_CalculoPFinal = "" ;
   this.A830AreaTrabalho_ServicoPadrao = 0 ;
   this.Z830AreaTrabalho_ServicoPadrao = 0 ;
   this.O830AreaTrabalho_ServicoPadrao = 0 ;
   this.A834AreaTrabalho_ValidaOSFM = false ;
   this.Z834AreaTrabalho_ValidaOSFM = false ;
   this.O834AreaTrabalho_ValidaOSFM = false ;
   this.A855AreaTrabalho_DiasParaPagar = 0 ;
   this.Z855AreaTrabalho_DiasParaPagar = 0 ;
   this.O855AreaTrabalho_DiasParaPagar = 0 ;
   this.A987AreaTrabalho_ContratadaUpdBslCod = 0 ;
   this.Z987AreaTrabalho_ContratadaUpdBslCod = 0 ;
   this.O987AreaTrabalho_ContratadaUpdBslCod = 0 ;
   this.A2081AreaTrabalho_VerTA = 0 ;
   this.Z2081AreaTrabalho_VerTA = 0 ;
   this.O2081AreaTrabalho_VerTA = 0 ;
   this.A12Contratante_CNPJ = "" ;
   this.Z12Contratante_CNPJ = "" ;
   this.O12Contratante_CNPJ = "" ;
   this.A9Contratante_RazaoSocial = "" ;
   this.Z9Contratante_RazaoSocial = "" ;
   this.O9Contratante_RazaoSocial = "" ;
   this.A10Contratante_NomeFantasia = "" ;
   this.Z10Contratante_NomeFantasia = "" ;
   this.O10Contratante_NomeFantasia = "" ;
   this.A11Contratante_IE = "" ;
   this.Z11Contratante_IE = "" ;
   this.O11Contratante_IE = "" ;
   this.A13Contratante_WebSite = "" ;
   this.Z13Contratante_WebSite = "" ;
   this.O13Contratante_WebSite = "" ;
   this.A14Contratante_Email = "" ;
   this.Z14Contratante_Email = "" ;
   this.O14Contratante_Email = "" ;
   this.A31Contratante_Telefone = "" ;
   this.Z31Contratante_Telefone = "" ;
   this.O31Contratante_Telefone = "" ;
   this.A32Contratante_Ramal = "" ;
   this.Z32Contratante_Ramal = "" ;
   this.O32Contratante_Ramal = "" ;
   this.A33Contratante_Fax = "" ;
   this.Z33Contratante_Fax = "" ;
   this.O33Contratante_Fax = "" ;
   this.A24Estado_Nome = "" ;
   this.Z24Estado_Nome = "" ;
   this.O24Estado_Nome = "" ;
   this.A26Municipio_Nome = "" ;
   this.Z26Municipio_Nome = "" ;
   this.O26Municipio_Nome = "" ;
   this.A72AreaTrabalho_Ativo = false ;
   this.Z72AreaTrabalho_Ativo = false ;
   this.O72AreaTrabalho_Ativo = false ;
   this.A1216AreaTrabalho_OrganizacaoCod = 0 ;
   this.Z1216AreaTrabalho_OrganizacaoCod = 0 ;
   this.O1216AreaTrabalho_OrganizacaoCod = 0 ;
   this.A29Contratante_Codigo = 0 ;
   this.Z29Contratante_Codigo = 0 ;
   this.O29Contratante_Codigo = 0 ;
   this.A335Contratante_PessoaCod = 0 ;
   this.Z335Contratante_PessoaCod = 0 ;
   this.O335Contratante_PessoaCod = 0 ;
   this.A23Estado_UF = "" ;
   this.Z23Estado_UF = "" ;
   this.O23Estado_UF = "" ;
   this.A25Municipio_Codigo = 0 ;
   this.Z25Municipio_Codigo = 0 ;
   this.O25Municipio_Codigo = 0 ;
   this.A5AreaTrabalho_Codigo = 0 ;
   this.Z5AreaTrabalho_Codigo = 0 ;
   this.O5AreaTrabalho_Codigo = 0 ;
   this.A1214Organizacao_Nome = "" ;
   this.A1154AreaTrabalho_TipoPlanilha = 0 ;
   this.A1588AreaTrabalho_SS_Codigo = 0 ;
   this.A6AreaTrabalho_Descricao = "" ;
   this.A642AreaTrabalho_CalculoPFinal = "" ;
   this.A830AreaTrabalho_ServicoPadrao = 0 ;
   this.A834AreaTrabalho_ValidaOSFM = false ;
   this.A855AreaTrabalho_DiasParaPagar = 0 ;
   this.A987AreaTrabalho_ContratadaUpdBslCod = 0 ;
   this.A2081AreaTrabalho_VerTA = 0 ;
   this.A12Contratante_CNPJ = "" ;
   this.A9Contratante_RazaoSocial = "" ;
   this.A10Contratante_NomeFantasia = "" ;
   this.A11Contratante_IE = "" ;
   this.A13Contratante_WebSite = "" ;
   this.A14Contratante_Email = "" ;
   this.A31Contratante_Telefone = "" ;
   this.A32Contratante_Ramal = "" ;
   this.A33Contratante_Fax = "" ;
   this.A24Estado_Nome = "" ;
   this.A26Municipio_Nome = "" ;
   this.A72AreaTrabalho_Ativo = false ;
   this.A1216AreaTrabalho_OrganizacaoCod = 0 ;
   this.A29Contratante_Codigo = 0 ;
   this.A335Contratante_PessoaCod = 0 ;
   this.A23Estado_UF = "" ;
   this.A25Municipio_Codigo = 0 ;
   this.A5AreaTrabalho_Codigo = 0 ;
   this.Events = {"e130v2_client": ["'DOUPDATE'", true] ,"e140v2_client": ["'DODELETE'", true] ,"e150v2_client": ["ENTER", true] ,"e160v2_client": ["CANCEL", true]};
   this.EvtParms["REFRESH"] = [[],[]];
   this.EvtParms["LOAD"] = [[{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A14Contratante_Email',fld:'CONTRATANTE_EMAIL',pic:'',nv:''},{av:'A23Estado_UF',fld:'ESTADO_UF',pic:'@!',nv:''},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',nv:0}],[{av:'gx.fn.getCtrlProperty("CONTRATANTE_CNPJ","Link")',ctrl:'CONTRATANTE_CNPJ',prop:'Link'},{av:'gx.fn.getCtrlProperty("CONTRATANTE_EMAIL","Link")',ctrl:'CONTRATANTE_EMAIL',prop:'Link'},{av:'gx.fn.getCtrlProperty("ESTADO_NOME","Link")',ctrl:'ESTADO_NOME',prop:'Link'},{av:'gx.fn.getCtrlProperty("MUNICIPIO_NOME","Link")',ctrl:'MUNICIPIO_NOME',prop:'Link'},{av:'gx.fn.getCtrlProperty("AREATRABALHO_ORGANIZACAOCOD","Visible")',ctrl:'AREATRABALHO_ORGANIZACAOCOD',prop:'Visible'},{av:'gx.fn.getCtrlProperty("CONTRATANTE_CODIGO","Visible")',ctrl:'CONTRATANTE_CODIGO',prop:'Visible'},{av:'gx.fn.getCtrlProperty("CONTRATANTE_PESSOACOD","Visible")',ctrl:'CONTRATANTE_PESSOACOD',prop:'Visible'},{av:'gx.fn.getCtrlProperty("ESTADO_UF","Visible")',ctrl:'ESTADO_UF',prop:'Visible'},{av:'gx.fn.getCtrlProperty("MUNICIPIO_CODIGO","Visible")',ctrl:'MUNICIPIO_CODIGO',prop:'Visible'},{av:'gx.fn.getCtrlProperty("AREATRABALHO_CODIGO","Visible")',ctrl:'AREATRABALHO_CODIGO',prop:'Visible'}]];
   this.EvtParms["'DOUPDATE'"] = [[{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0}],[]];
   this.EvtParms["'DODELETE'"] = [[{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0}],[]];
   this.InitStandaloneVars( );
});
