/*
               File: PRC_ClonarContrato
        Description: Clonar
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:16:56.19
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_clonarcontrato : GXProcedure
   {
      public prc_clonarcontrato( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_clonarcontrato( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contrato_Codigo )
      {
         this.AV8Contrato_Codigo = aP0_Contrato_Codigo;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_Contrato_Codigo )
      {
         prc_clonarcontrato objprc_clonarcontrato;
         objprc_clonarcontrato = new prc_clonarcontrato();
         objprc_clonarcontrato.AV8Contrato_Codigo = aP0_Contrato_Codigo;
         objprc_clonarcontrato.context.SetSubmitInitialConfig(context);
         objprc_clonarcontrato.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_clonarcontrato);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_clonarcontrato)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00TS2 */
         pr_default.execute(0, new Object[] {AV8Contrato_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1013Contrato_PrepostoCod = P00TS2_A1013Contrato_PrepostoCod[0];
            n1013Contrato_PrepostoCod = P00TS2_n1013Contrato_PrepostoCod[0];
            A80Contrato_Objeto = P00TS2_A80Contrato_Objeto[0];
            A79Contrato_Ano = P00TS2_A79Contrato_Ano[0];
            A78Contrato_NumeroAta = P00TS2_A78Contrato_NumeroAta[0];
            n78Contrato_NumeroAta = P00TS2_n78Contrato_NumeroAta[0];
            A77Contrato_Numero = P00TS2_A77Contrato_Numero[0];
            A2086Contrato_LmtFtr = P00TS2_A2086Contrato_LmtFtr[0];
            n2086Contrato_LmtFtr = P00TS2_n2086Contrato_LmtFtr[0];
            A1358Contrato_PrdFtrFim = P00TS2_A1358Contrato_PrdFtrFim[0];
            n1358Contrato_PrdFtrFim = P00TS2_n1358Contrato_PrdFtrFim[0];
            A1357Contrato_PrdFtrIni = P00TS2_A1357Contrato_PrdFtrIni[0];
            n1357Contrato_PrdFtrIni = P00TS2_n1357Contrato_PrdFtrIni[0];
            A1354Contrato_PrdFtrCada = P00TS2_A1354Contrato_PrdFtrCada[0];
            n1354Contrato_PrdFtrCada = P00TS2_n1354Contrato_PrdFtrCada[0];
            A1150Contrato_AceitaPFFS = P00TS2_A1150Contrato_AceitaPFFS[0];
            n1150Contrato_AceitaPFFS = P00TS2_n1150Contrato_AceitaPFFS[0];
            A453Contrato_IndiceDivergencia = P00TS2_A453Contrato_IndiceDivergencia[0];
            A452Contrato_CalculoDivergencia = P00TS2_A452Contrato_CalculoDivergencia[0];
            A116Contrato_ValorUnidadeContratacao = P00TS2_A116Contrato_ValorUnidadeContratacao[0];
            A115Contrato_UnidadeContratacao = P00TS2_A115Contrato_UnidadeContratacao[0];
            A92Contrato_Ativo = P00TS2_A92Contrato_Ativo[0];
            A91Contrato_DiasPagto = P00TS2_A91Contrato_DiasPagto[0];
            A90Contrato_RegrasPagto = P00TS2_A90Contrato_RegrasPagto[0];
            A39Contratada_Codigo = P00TS2_A39Contratada_Codigo[0];
            A89Contrato_Valor = P00TS2_A89Contrato_Valor[0];
            A88Contrato_DataFimAdaptacao = P00TS2_A88Contrato_DataFimAdaptacao[0];
            A87Contrato_DataTerminoAta = P00TS2_A87Contrato_DataTerminoAta[0];
            A86Contrato_DataPedidoReajuste = P00TS2_A86Contrato_DataPedidoReajuste[0];
            A85Contrato_DataAssinatura = P00TS2_A85Contrato_DataAssinatura[0];
            A84Contrato_DataPublicacaoDOU = P00TS2_A84Contrato_DataPublicacaoDOU[0];
            A83Contrato_DataVigenciaTermino = P00TS2_A83Contrato_DataVigenciaTermino[0];
            A82Contrato_DataVigenciaInicio = P00TS2_A82Contrato_DataVigenciaInicio[0];
            A81Contrato_Quantidade = P00TS2_A81Contrato_Quantidade[0];
            A75Contrato_AreaTrabalhoCod = P00TS2_A75Contrato_AreaTrabalhoCod[0];
            A74Contrato_Codigo = P00TS2_A74Contrato_Codigo[0];
            AV10Contratada_Codigo = A39Contratada_Codigo;
            /*
               INSERT RECORD ON TABLE Contrato

            */
            W77Contrato_Numero = A77Contrato_Numero;
            W78Contrato_NumeroAta = A78Contrato_NumeroAta;
            n78Contrato_NumeroAta = false;
            W80Contrato_Objeto = A80Contrato_Objeto;
            W1013Contrato_PrepostoCod = A1013Contrato_PrepostoCod;
            n1013Contrato_PrepostoCod = false;
            W79Contrato_Ano = A79Contrato_Ano;
            A77Contrato_Numero = "Clonado";
            A78Contrato_NumeroAta = "";
            n78Contrato_NumeroAta = false;
            A80Contrato_Objeto = "";
            A1013Contrato_PrepostoCod = 0;
            n1013Contrato_PrepostoCod = false;
            n1013Contrato_PrepostoCod = true;
            A79Contrato_Ano = (short)(DateTimeUtil.Year( DateTimeUtil.ServerDate( context, "DEFAULT")));
            /* Using cursor P00TS3 */
            pr_default.execute(1, new Object[] {A75Contrato_AreaTrabalhoCod, A77Contrato_Numero, n78Contrato_NumeroAta, A78Contrato_NumeroAta, A79Contrato_Ano, A80Contrato_Objeto, A81Contrato_Quantidade, A82Contrato_DataVigenciaInicio, A83Contrato_DataVigenciaTermino, A84Contrato_DataPublicacaoDOU, A85Contrato_DataAssinatura, A86Contrato_DataPedidoReajuste, A87Contrato_DataTerminoAta, A88Contrato_DataFimAdaptacao, A89Contrato_Valor, A39Contratada_Codigo, A90Contrato_RegrasPagto, A91Contrato_DiasPagto, A92Contrato_Ativo, A115Contrato_UnidadeContratacao, A116Contrato_ValorUnidadeContratacao, A452Contrato_CalculoDivergencia, A453Contrato_IndiceDivergencia, n1013Contrato_PrepostoCod, A1013Contrato_PrepostoCod, n1150Contrato_AceitaPFFS, A1150Contrato_AceitaPFFS, n1354Contrato_PrdFtrCada, A1354Contrato_PrdFtrCada, n1357Contrato_PrdFtrIni, A1357Contrato_PrdFtrIni, n1358Contrato_PrdFtrFim, A1358Contrato_PrdFtrFim, n2086Contrato_LmtFtr, A2086Contrato_LmtFtr});
            A74Contrato_Codigo = P00TS3_A74Contrato_Codigo[0];
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("Contrato") ;
            if ( (pr_default.getStatus(1) == 1) )
            {
               context.Gx_err = 1;
               Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
            }
            else
            {
               context.Gx_err = 0;
               Gx_emsg = "";
            }
            A77Contrato_Numero = W77Contrato_Numero;
            A78Contrato_NumeroAta = W78Contrato_NumeroAta;
            n78Contrato_NumeroAta = false;
            A80Contrato_Objeto = W80Contrato_Objeto;
            A1013Contrato_PrepostoCod = W1013Contrato_PrepostoCod;
            n1013Contrato_PrepostoCod = false;
            A79Contrato_Ano = W79Contrato_Ano;
            /* End Insert */
            AV9NewContrato = A74Contrato_Codigo;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         /* Using cursor P00TS4 */
         pr_default.execute(2, new Object[] {AV8Contrato_Codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A74Contrato_Codigo = P00TS4_A74Contrato_Codigo[0];
            W74Contrato_Codigo = A74Contrato_Codigo;
            /* Using cursor P00TS5 */
            pr_default.execute(3, new Object[] {A74Contrato_Codigo});
            while ( (pr_default.getStatus(3) != 101) )
            {
               A153ContratoClausulas_Item = P00TS5_A153ContratoClausulas_Item[0];
               A154ContratoClausulas_Descricao = P00TS5_A154ContratoClausulas_Descricao[0];
               A152ContratoClausulas_Codigo = P00TS5_A152ContratoClausulas_Codigo[0];
               W74Contrato_Codigo = A74Contrato_Codigo;
               AV11ContratoClausulas_Item = A153ContratoClausulas_Item;
               /*
                  INSERT RECORD ON TABLE ContratoClausulas

               */
               W74Contrato_Codigo = A74Contrato_Codigo;
               W153ContratoClausulas_Item = A153ContratoClausulas_Item;
               A74Contrato_Codigo = AV9NewContrato;
               A153ContratoClausulas_Item = AV11ContratoClausulas_Item;
               /* Using cursor P00TS6 */
               pr_default.execute(4, new Object[] {A74Contrato_Codigo, A153ContratoClausulas_Item, A154ContratoClausulas_Descricao});
               A152ContratoClausulas_Codigo = P00TS6_A152ContratoClausulas_Codigo[0];
               pr_default.close(4);
               dsDefault.SmartCacheProvider.SetUpdated("ContratoClausulas") ;
               if ( (pr_default.getStatus(4) == 1) )
               {
                  context.Gx_err = 1;
                  Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
               }
               else
               {
                  context.Gx_err = 0;
                  Gx_emsg = "";
               }
               A74Contrato_Codigo = W74Contrato_Codigo;
               A153ContratoClausulas_Item = W153ContratoClausulas_Item;
               /* End Insert */
               A74Contrato_Codigo = W74Contrato_Codigo;
               pr_default.readNext(3);
            }
            pr_default.close(3);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            A74Contrato_Codigo = W74Contrato_Codigo;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(2);
         /* Using cursor P00TS7 */
         pr_default.execute(5, new Object[] {AV8Contrato_Codigo});
         while ( (pr_default.getStatus(5) != 101) )
         {
            A74Contrato_Codigo = P00TS7_A74Contrato_Codigo[0];
            /* Using cursor P00TS8 */
            pr_default.execute(6, new Object[] {A74Contrato_Codigo});
            while ( (pr_default.getStatus(6) != 101) )
            {
               A1207ContratoUnidades_ContratoCod = P00TS8_A1207ContratoUnidades_ContratoCod[0];
               A1208ContratoUnidades_Produtividade = P00TS8_A1208ContratoUnidades_Produtividade[0];
               n1208ContratoUnidades_Produtividade = P00TS8_n1208ContratoUnidades_Produtividade[0];
               A1204ContratoUnidades_UndMedCod = P00TS8_A1204ContratoUnidades_UndMedCod[0];
               W1207ContratoUnidades_ContratoCod = A1207ContratoUnidades_ContratoCod;
               /*
                  INSERT RECORD ON TABLE ContratoUnidades

               */
               W1207ContratoUnidades_ContratoCod = A1207ContratoUnidades_ContratoCod;
               A1207ContratoUnidades_ContratoCod = AV9NewContrato;
               /* Using cursor P00TS9 */
               pr_default.execute(7, new Object[] {A1207ContratoUnidades_ContratoCod, A1204ContratoUnidades_UndMedCod, n1208ContratoUnidades_Produtividade, A1208ContratoUnidades_Produtividade});
               pr_default.close(7);
               dsDefault.SmartCacheProvider.SetUpdated("ContratoUnidades") ;
               if ( (pr_default.getStatus(7) == 1) )
               {
                  context.Gx_err = 1;
                  Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
               }
               else
               {
                  context.Gx_err = 0;
                  Gx_emsg = "";
               }
               A1207ContratoUnidades_ContratoCod = W1207ContratoUnidades_ContratoCod;
               /* End Insert */
               A1207ContratoUnidades_ContratoCod = W1207ContratoUnidades_ContratoCod;
               pr_default.readNext(6);
            }
            pr_default.close(6);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(5);
         /* Using cursor P00TS10 */
         pr_default.execute(8, new Object[] {AV8Contrato_Codigo});
         while ( (pr_default.getStatus(8) != 101) )
         {
            A74Contrato_Codigo = P00TS10_A74Contrato_Codigo[0];
            W74Contrato_Codigo = A74Contrato_Codigo;
            /* Using cursor P00TS11 */
            pr_default.execute(9, new Object[] {A74Contrato_Codigo});
            while ( (pr_default.getStatus(9) != 101) )
            {
               A1858ContratoServicos_Alias = P00TS11_A1858ContratoServicos_Alias[0];
               n1858ContratoServicos_Alias = P00TS11_n1858ContratoServicos_Alias[0];
               A2094ContratoServicos_SolicitaGestorSistema = P00TS11_A2094ContratoServicos_SolicitaGestorSistema[0];
               A2074ContratoServicos_CalculoRmn = P00TS11_A2074ContratoServicos_CalculoRmn[0];
               n2074ContratoServicos_CalculoRmn = P00TS11_n2074ContratoServicos_CalculoRmn[0];
               A1538ContratoServicos_PercPgm = P00TS11_A1538ContratoServicos_PercPgm[0];
               n1538ContratoServicos_PercPgm = P00TS11_n1538ContratoServicos_PercPgm[0];
               A1799ContratoServicos_LimiteProposta = P00TS11_A1799ContratoServicos_LimiteProposta[0];
               n1799ContratoServicos_LimiteProposta = P00TS11_n1799ContratoServicos_LimiteProposta[0];
               A1723ContratoServicos_CodigoFiscal = P00TS11_A1723ContratoServicos_CodigoFiscal[0];
               n1723ContratoServicos_CodigoFiscal = P00TS11_n1723ContratoServicos_CodigoFiscal[0];
               A1649ContratoServicos_PrazoInicio = P00TS11_A1649ContratoServicos_PrazoInicio[0];
               n1649ContratoServicos_PrazoInicio = P00TS11_n1649ContratoServicos_PrazoInicio[0];
               A1539ContratoServicos_PercCnc = P00TS11_A1539ContratoServicos_PercCnc[0];
               n1539ContratoServicos_PercCnc = P00TS11_n1539ContratoServicos_PercCnc[0];
               A1537ContratoServicos_PercTmp = P00TS11_A1537ContratoServicos_PercTmp[0];
               n1537ContratoServicos_PercTmp = P00TS11_n1537ContratoServicos_PercTmp[0];
               A1531ContratoServicos_TipoHierarquia = P00TS11_A1531ContratoServicos_TipoHierarquia[0];
               n1531ContratoServicos_TipoHierarquia = P00TS11_n1531ContratoServicos_TipoHierarquia[0];
               A1516ContratoServicos_TmpEstAnl = P00TS11_A1516ContratoServicos_TmpEstAnl[0];
               n1516ContratoServicos_TmpEstAnl = P00TS11_n1516ContratoServicos_TmpEstAnl[0];
               A1502ContratoServicos_TmpEstCrr = P00TS11_A1502ContratoServicos_TmpEstCrr[0];
               n1502ContratoServicos_TmpEstCrr = P00TS11_n1502ContratoServicos_TmpEstCrr[0];
               A1501ContratoServicos_TmpEstExc = P00TS11_A1501ContratoServicos_TmpEstExc[0];
               n1501ContratoServicos_TmpEstExc = P00TS11_n1501ContratoServicos_TmpEstExc[0];
               A1455ContratoServicos_IndiceDivergencia = P00TS11_A1455ContratoServicos_IndiceDivergencia[0];
               n1455ContratoServicos_IndiceDivergencia = P00TS11_n1455ContratoServicos_IndiceDivergencia[0];
               A1454ContratoServicos_PrazoTpDias = P00TS11_A1454ContratoServicos_PrazoTpDias[0];
               n1454ContratoServicos_PrazoTpDias = P00TS11_n1454ContratoServicos_PrazoTpDias[0];
               A1397ContratoServicos_NaoRequerAtr = P00TS11_A1397ContratoServicos_NaoRequerAtr[0];
               n1397ContratoServicos_NaoRequerAtr = P00TS11_n1397ContratoServicos_NaoRequerAtr[0];
               A1341ContratoServicos_FatorCnvUndCnt = P00TS11_A1341ContratoServicos_FatorCnvUndCnt[0];
               n1341ContratoServicos_FatorCnvUndCnt = P00TS11_n1341ContratoServicos_FatorCnvUndCnt[0];
               A1340ContratoServicos_QntUntCns = P00TS11_A1340ContratoServicos_QntUntCns[0];
               n1340ContratoServicos_QntUntCns = P00TS11_n1340ContratoServicos_QntUntCns[0];
               A1325ContratoServicos_StatusPagFnc = P00TS11_A1325ContratoServicos_StatusPagFnc[0];
               n1325ContratoServicos_StatusPagFnc = P00TS11_n1325ContratoServicos_StatusPagFnc[0];
               A1266ContratoServicos_Momento = P00TS11_A1266ContratoServicos_Momento[0];
               n1266ContratoServicos_Momento = P00TS11_n1266ContratoServicos_Momento[0];
               A1225ContratoServicos_PrazoCorrecaoTipo = P00TS11_A1225ContratoServicos_PrazoCorrecaoTipo[0];
               n1225ContratoServicos_PrazoCorrecaoTipo = P00TS11_n1225ContratoServicos_PrazoCorrecaoTipo[0];
               A1224ContratoServicos_PrazoCorrecao = P00TS11_A1224ContratoServicos_PrazoCorrecao[0];
               n1224ContratoServicos_PrazoCorrecao = P00TS11_n1224ContratoServicos_PrazoCorrecao[0];
               A1217ContratoServicos_EspelhaAceite = P00TS11_A1217ContratoServicos_EspelhaAceite[0];
               n1217ContratoServicos_EspelhaAceite = P00TS11_n1217ContratoServicos_EspelhaAceite[0];
               A1212ContratoServicos_UnidadeContratada = P00TS11_A1212ContratoServicos_UnidadeContratada[0];
               A1191ContratoServicos_Produtividade = P00TS11_A1191ContratoServicos_Produtividade[0];
               n1191ContratoServicos_Produtividade = P00TS11_n1191ContratoServicos_Produtividade[0];
               A1190ContratoServicos_PrazoImediato = P00TS11_A1190ContratoServicos_PrazoImediato[0];
               n1190ContratoServicos_PrazoImediato = P00TS11_n1190ContratoServicos_PrazoImediato[0];
               A1182ContratoServicos_PrazoAtendeGarantia = P00TS11_A1182ContratoServicos_PrazoAtendeGarantia[0];
               n1182ContratoServicos_PrazoAtendeGarantia = P00TS11_n1182ContratoServicos_PrazoAtendeGarantia[0];
               A1181ContratoServicos_PrazoGarantia = P00TS11_A1181ContratoServicos_PrazoGarantia[0];
               n1181ContratoServicos_PrazoGarantia = P00TS11_n1181ContratoServicos_PrazoGarantia[0];
               A1153ContratoServicos_PrazoResposta = P00TS11_A1153ContratoServicos_PrazoResposta[0];
               n1153ContratoServicos_PrazoResposta = P00TS11_n1153ContratoServicos_PrazoResposta[0];
               A1152ContratoServicos_PrazoAnalise = P00TS11_A1152ContratoServicos_PrazoAnalise[0];
               n1152ContratoServicos_PrazoAnalise = P00TS11_n1152ContratoServicos_PrazoAnalise[0];
               A888ContratoServicos_HmlSemCnf = P00TS11_A888ContratoServicos_HmlSemCnf[0];
               n888ContratoServicos_HmlSemCnf = P00TS11_n888ContratoServicos_HmlSemCnf[0];
               A868ContratoServicos_TipoVnc = P00TS11_A868ContratoServicos_TipoVnc[0];
               n868ContratoServicos_TipoVnc = P00TS11_n868ContratoServicos_TipoVnc[0];
               A639ContratoServicos_LocalExec = P00TS11_A639ContratoServicos_LocalExec[0];
               A638ContratoServicos_Ativo = P00TS11_A638ContratoServicos_Ativo[0];
               A607ServicoContrato_Faturamento = P00TS11_A607ServicoContrato_Faturamento[0];
               A558Servico_Percentual = P00TS11_A558Servico_Percentual[0];
               n558Servico_Percentual = P00TS11_n558Servico_Percentual[0];
               A557Servico_VlrUnidadeContratada = P00TS11_A557Servico_VlrUnidadeContratada[0];
               A555Servico_QtdContratada = P00TS11_A555Servico_QtdContratada[0];
               n555Servico_QtdContratada = P00TS11_n555Servico_QtdContratada[0];
               A155Servico_Codigo = P00TS11_A155Servico_Codigo[0];
               A160ContratoServicos_Codigo = P00TS11_A160ContratoServicos_Codigo[0];
               W74Contrato_Codigo = A74Contrato_Codigo;
               AV12ContratoServicos_Alias = A1858ContratoServicos_Alias;
               /*
                  INSERT RECORD ON TABLE ContratoServicos

               */
               W74Contrato_Codigo = A74Contrato_Codigo;
               W1858ContratoServicos_Alias = A1858ContratoServicos_Alias;
               n1858ContratoServicos_Alias = false;
               A74Contrato_Codigo = AV9NewContrato;
               A1858ContratoServicos_Alias = AV12ContratoServicos_Alias;
               n1858ContratoServicos_Alias = false;
               /* Using cursor P00TS12 */
               pr_default.execute(10, new Object[] {A74Contrato_Codigo, A155Servico_Codigo, n555Servico_QtdContratada, A555Servico_QtdContratada, A557Servico_VlrUnidadeContratada, n558Servico_Percentual, A558Servico_Percentual, A607ServicoContrato_Faturamento, A638ContratoServicos_Ativo, A639ContratoServicos_LocalExec, n868ContratoServicos_TipoVnc, A868ContratoServicos_TipoVnc, n888ContratoServicos_HmlSemCnf, A888ContratoServicos_HmlSemCnf, n1152ContratoServicos_PrazoAnalise, A1152ContratoServicos_PrazoAnalise, n1153ContratoServicos_PrazoResposta, A1153ContratoServicos_PrazoResposta, n1181ContratoServicos_PrazoGarantia, A1181ContratoServicos_PrazoGarantia, n1182ContratoServicos_PrazoAtendeGarantia, A1182ContratoServicos_PrazoAtendeGarantia, n1190ContratoServicos_PrazoImediato, A1190ContratoServicos_PrazoImediato, n1191ContratoServicos_Produtividade, A1191ContratoServicos_Produtividade, A1212ContratoServicos_UnidadeContratada, n1217ContratoServicos_EspelhaAceite, A1217ContratoServicos_EspelhaAceite, n1224ContratoServicos_PrazoCorrecao, A1224ContratoServicos_PrazoCorrecao, n1225ContratoServicos_PrazoCorrecaoTipo, A1225ContratoServicos_PrazoCorrecaoTipo, n1266ContratoServicos_Momento, A1266ContratoServicos_Momento, n1325ContratoServicos_StatusPagFnc, A1325ContratoServicos_StatusPagFnc, n1340ContratoServicos_QntUntCns, A1340ContratoServicos_QntUntCns, n1341ContratoServicos_FatorCnvUndCnt, A1341ContratoServicos_FatorCnvUndCnt, n1397ContratoServicos_NaoRequerAtr, A1397ContratoServicos_NaoRequerAtr, n1454ContratoServicos_PrazoTpDias, A1454ContratoServicos_PrazoTpDias, n1455ContratoServicos_IndiceDivergencia, A1455ContratoServicos_IndiceDivergencia, n1501ContratoServicos_TmpEstExc, A1501ContratoServicos_TmpEstExc, n1502ContratoServicos_TmpEstCrr, A1502ContratoServicos_TmpEstCrr, n1516ContratoServicos_TmpEstAnl, A1516ContratoServicos_TmpEstAnl, n1531ContratoServicos_TipoHierarquia, A1531ContratoServicos_TipoHierarquia, n1537ContratoServicos_PercTmp, A1537ContratoServicos_PercTmp, n1539ContratoServicos_PercCnc, A1539ContratoServicos_PercCnc, n1649ContratoServicos_PrazoInicio, A1649ContratoServicos_PrazoInicio, n1723ContratoServicos_CodigoFiscal, A1723ContratoServicos_CodigoFiscal, n1799ContratoServicos_LimiteProposta, A1799ContratoServicos_LimiteProposta, n1538ContratoServicos_PercPgm, A1538ContratoServicos_PercPgm, n1858ContratoServicos_Alias, A1858ContratoServicos_Alias, n2074ContratoServicos_CalculoRmn, A2074ContratoServicos_CalculoRmn, A2094ContratoServicos_SolicitaGestorSistema});
               A160ContratoServicos_Codigo = P00TS12_A160ContratoServicos_Codigo[0];
               pr_default.close(10);
               dsDefault.SmartCacheProvider.SetUpdated("ContratoServicos") ;
               if ( (pr_default.getStatus(10) == 1) )
               {
                  context.Gx_err = 1;
                  Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
               }
               else
               {
                  context.Gx_err = 0;
                  Gx_emsg = "";
               }
               A74Contrato_Codigo = W74Contrato_Codigo;
               A1858ContratoServicos_Alias = W1858ContratoServicos_Alias;
               n1858ContratoServicos_Alias = false;
               /* End Insert */
               A74Contrato_Codigo = W74Contrato_Codigo;
               pr_default.readNext(9);
            }
            pr_default.close(9);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            A74Contrato_Codigo = W74Contrato_Codigo;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(8);
         /* Using cursor P00TS13 */
         pr_default.execute(11, new Object[] {AV8Contrato_Codigo});
         while ( (pr_default.getStatus(11) != 101) )
         {
            A74Contrato_Codigo = P00TS13_A74Contrato_Codigo[0];
            W74Contrato_Codigo = A74Contrato_Codigo;
            /* Using cursor P00TS14 */
            pr_default.execute(12, new Object[] {A74Contrato_Codigo});
            while ( (pr_default.getStatus(12) != 101) )
            {
               A103ContratoGarantia_Percentual = P00TS14_A103ContratoGarantia_Percentual[0];
               A107ContratoGarantia_ValorEncerramento = P00TS14_A107ContratoGarantia_ValorEncerramento[0];
               A106ContratoGarantia_DataEncerramento = P00TS14_A106ContratoGarantia_DataEncerramento[0];
               A105ContratoGarantia_ValorGarantia = P00TS14_A105ContratoGarantia_ValorGarantia[0];
               A104ContratoGarantia_DataCaucao = P00TS14_A104ContratoGarantia_DataCaucao[0];
               A102ContratoGarantia_DataPagtoGarantia = P00TS14_A102ContratoGarantia_DataPagtoGarantia[0];
               A101ContratoGarantia_Codigo = P00TS14_A101ContratoGarantia_Codigo[0];
               W74Contrato_Codigo = A74Contrato_Codigo;
               AV13ContratoGarantia_Percentual = A103ContratoGarantia_Percentual;
               /*
                  INSERT RECORD ON TABLE ContratoGarantia

               */
               W74Contrato_Codigo = A74Contrato_Codigo;
               W103ContratoGarantia_Percentual = A103ContratoGarantia_Percentual;
               A74Contrato_Codigo = AV9NewContrato;
               A103ContratoGarantia_Percentual = AV13ContratoGarantia_Percentual;
               /* Using cursor P00TS15 */
               pr_default.execute(13, new Object[] {A74Contrato_Codigo, A102ContratoGarantia_DataPagtoGarantia, A103ContratoGarantia_Percentual, A104ContratoGarantia_DataCaucao, A105ContratoGarantia_ValorGarantia, A106ContratoGarantia_DataEncerramento, A107ContratoGarantia_ValorEncerramento});
               A101ContratoGarantia_Codigo = P00TS15_A101ContratoGarantia_Codigo[0];
               pr_default.close(13);
               dsDefault.SmartCacheProvider.SetUpdated("ContratoGarantia") ;
               if ( (pr_default.getStatus(13) == 1) )
               {
                  context.Gx_err = 1;
                  Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
               }
               else
               {
                  context.Gx_err = 0;
                  Gx_emsg = "";
               }
               A74Contrato_Codigo = W74Contrato_Codigo;
               A103ContratoGarantia_Percentual = W103ContratoGarantia_Percentual;
               /* End Insert */
               A74Contrato_Codigo = W74Contrato_Codigo;
               pr_default.readNext(12);
            }
            pr_default.close(12);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            A74Contrato_Codigo = W74Contrato_Codigo;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(11);
         /* Using cursor P00TS16 */
         pr_default.execute(14, new Object[] {AV8Contrato_Codigo});
         while ( (pr_default.getStatus(14) != 101) )
         {
            A74Contrato_Codigo = P00TS16_A74Contrato_Codigo[0];
            W74Contrato_Codigo = A74Contrato_Codigo;
            /* Using cursor P00TS17 */
            pr_default.execute(15, new Object[] {A74Contrato_Codigo});
            while ( (pr_default.getStatus(15) != 101) )
            {
               A311ContratoDadosCertame_Data = P00TS17_A311ContratoDadosCertame_Data[0];
               A313ContratoDadosCertame_DataAdjudicacao = P00TS17_A313ContratoDadosCertame_DataAdjudicacao[0];
               n313ContratoDadosCertame_DataAdjudicacao = P00TS17_n313ContratoDadosCertame_DataAdjudicacao[0];
               A312ContratoDadosCertame_DataHomologacao = P00TS17_A312ContratoDadosCertame_DataHomologacao[0];
               n312ContratoDadosCertame_DataHomologacao = P00TS17_n312ContratoDadosCertame_DataHomologacao[0];
               A310ContratoDadosCertame_Uasg = P00TS17_A310ContratoDadosCertame_Uasg[0];
               n310ContratoDadosCertame_Uasg = P00TS17_n310ContratoDadosCertame_Uasg[0];
               A309ContratoDadosCertame_Site = P00TS17_A309ContratoDadosCertame_Site[0];
               n309ContratoDadosCertame_Site = P00TS17_n309ContratoDadosCertame_Site[0];
               A308ContratoDadosCertame_Numero = P00TS17_A308ContratoDadosCertame_Numero[0];
               n308ContratoDadosCertame_Numero = P00TS17_n308ContratoDadosCertame_Numero[0];
               A307ContratoDadosCertame_Modalidade = P00TS17_A307ContratoDadosCertame_Modalidade[0];
               A314ContratoDadosCertame_Codigo = P00TS17_A314ContratoDadosCertame_Codigo[0];
               W74Contrato_Codigo = A74Contrato_Codigo;
               AV14ContratoDadosCertame_Data = A311ContratoDadosCertame_Data;
               /*
                  INSERT RECORD ON TABLE ContratoDadosCertame

               */
               W74Contrato_Codigo = A74Contrato_Codigo;
               W311ContratoDadosCertame_Data = A311ContratoDadosCertame_Data;
               A74Contrato_Codigo = AV9NewContrato;
               A311ContratoDadosCertame_Data = AV14ContratoDadosCertame_Data;
               /* Using cursor P00TS18 */
               pr_default.execute(16, new Object[] {A74Contrato_Codigo, A307ContratoDadosCertame_Modalidade, n308ContratoDadosCertame_Numero, A308ContratoDadosCertame_Numero, n309ContratoDadosCertame_Site, A309ContratoDadosCertame_Site, n310ContratoDadosCertame_Uasg, A310ContratoDadosCertame_Uasg, A311ContratoDadosCertame_Data, n312ContratoDadosCertame_DataHomologacao, A312ContratoDadosCertame_DataHomologacao, n313ContratoDadosCertame_DataAdjudicacao, A313ContratoDadosCertame_DataAdjudicacao});
               A314ContratoDadosCertame_Codigo = P00TS18_A314ContratoDadosCertame_Codigo[0];
               pr_default.close(16);
               dsDefault.SmartCacheProvider.SetUpdated("ContratoDadosCertame") ;
               if ( (pr_default.getStatus(16) == 1) )
               {
                  context.Gx_err = 1;
                  Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
               }
               else
               {
                  context.Gx_err = 0;
                  Gx_emsg = "";
               }
               A74Contrato_Codigo = W74Contrato_Codigo;
               A311ContratoDadosCertame_Data = W311ContratoDadosCertame_Data;
               /* End Insert */
               A74Contrato_Codigo = W74Contrato_Codigo;
               pr_default.readNext(15);
            }
            pr_default.close(15);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            A74Contrato_Codigo = W74Contrato_Codigo;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(14);
         /* Using cursor P00TS19 */
         pr_default.execute(17, new Object[] {AV8Contrato_Codigo});
         while ( (pr_default.getStatus(17) != 101) )
         {
            A74Contrato_Codigo = P00TS19_A74Contrato_Codigo[0];
            W74Contrato_Codigo = A74Contrato_Codigo;
            /* Using cursor P00TS20 */
            pr_default.execute(18, new Object[] {A74Contrato_Codigo});
            while ( (pr_default.getStatus(18) != 101) )
            {
               A322ContratoObrigacao_Item = P00TS20_A322ContratoObrigacao_Item[0];
               A1424ContratoObrigacao_DataMaxima = P00TS20_A1424ContratoObrigacao_DataMaxima[0];
               n1424ContratoObrigacao_DataMaxima = P00TS20_n1424ContratoObrigacao_DataMaxima[0];
               A1425ContratoObrigacao_SubItem = P00TS20_A1425ContratoObrigacao_SubItem[0];
               n1425ContratoObrigacao_SubItem = P00TS20_n1425ContratoObrigacao_SubItem[0];
               A323ContratoObrigacao_Descricao = P00TS20_A323ContratoObrigacao_Descricao[0];
               A321ContratoObrigacao_Codigo = P00TS20_A321ContratoObrigacao_Codigo[0];
               W74Contrato_Codigo = A74Contrato_Codigo;
               AV17ContratoObrigacao_Item = A322ContratoObrigacao_Item;
               /*
                  INSERT RECORD ON TABLE ContratoObrigacao

               */
               W74Contrato_Codigo = A74Contrato_Codigo;
               W322ContratoObrigacao_Item = A322ContratoObrigacao_Item;
               A74Contrato_Codigo = AV9NewContrato;
               A322ContratoObrigacao_Item = AV17ContratoObrigacao_Item;
               /* Using cursor P00TS21 */
               pr_default.execute(19, new Object[] {A74Contrato_Codigo, A322ContratoObrigacao_Item, A323ContratoObrigacao_Descricao, n1425ContratoObrigacao_SubItem, A1425ContratoObrigacao_SubItem, n1424ContratoObrigacao_DataMaxima, A1424ContratoObrigacao_DataMaxima});
               A321ContratoObrigacao_Codigo = P00TS21_A321ContratoObrigacao_Codigo[0];
               pr_default.close(19);
               dsDefault.SmartCacheProvider.SetUpdated("ContratoObrigacao") ;
               if ( (pr_default.getStatus(19) == 1) )
               {
                  context.Gx_err = 1;
                  Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
               }
               else
               {
                  context.Gx_err = 0;
                  Gx_emsg = "";
               }
               A74Contrato_Codigo = W74Contrato_Codigo;
               A322ContratoObrigacao_Item = W322ContratoObrigacao_Item;
               /* End Insert */
               A74Contrato_Codigo = W74Contrato_Codigo;
               pr_default.readNext(18);
            }
            pr_default.close(18);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            A74Contrato_Codigo = W74Contrato_Codigo;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(17);
         context.wjLoc = "contrato.aspx"+ "?" + GXUtil.UrlEncode(StringUtil.RTrim("UPD")) + "," + GXUtil.UrlEncode("" +AV9NewContrato) + "," + GXUtil.UrlEncode("" +AV10Contratada_Codigo);
         context.wjLocDisableFrm = 1;
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_ClonarContrato");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00TS2_A1013Contrato_PrepostoCod = new int[1] ;
         P00TS2_n1013Contrato_PrepostoCod = new bool[] {false} ;
         P00TS2_A80Contrato_Objeto = new String[] {""} ;
         P00TS2_A79Contrato_Ano = new short[1] ;
         P00TS2_A78Contrato_NumeroAta = new String[] {""} ;
         P00TS2_n78Contrato_NumeroAta = new bool[] {false} ;
         P00TS2_A77Contrato_Numero = new String[] {""} ;
         P00TS2_A2086Contrato_LmtFtr = new decimal[1] ;
         P00TS2_n2086Contrato_LmtFtr = new bool[] {false} ;
         P00TS2_A1358Contrato_PrdFtrFim = new short[1] ;
         P00TS2_n1358Contrato_PrdFtrFim = new bool[] {false} ;
         P00TS2_A1357Contrato_PrdFtrIni = new short[1] ;
         P00TS2_n1357Contrato_PrdFtrIni = new bool[] {false} ;
         P00TS2_A1354Contrato_PrdFtrCada = new String[] {""} ;
         P00TS2_n1354Contrato_PrdFtrCada = new bool[] {false} ;
         P00TS2_A1150Contrato_AceitaPFFS = new bool[] {false} ;
         P00TS2_n1150Contrato_AceitaPFFS = new bool[] {false} ;
         P00TS2_A453Contrato_IndiceDivergencia = new decimal[1] ;
         P00TS2_A452Contrato_CalculoDivergencia = new String[] {""} ;
         P00TS2_A116Contrato_ValorUnidadeContratacao = new decimal[1] ;
         P00TS2_A115Contrato_UnidadeContratacao = new short[1] ;
         P00TS2_A92Contrato_Ativo = new bool[] {false} ;
         P00TS2_A91Contrato_DiasPagto = new short[1] ;
         P00TS2_A90Contrato_RegrasPagto = new String[] {""} ;
         P00TS2_A39Contratada_Codigo = new int[1] ;
         P00TS2_A89Contrato_Valor = new decimal[1] ;
         P00TS2_A88Contrato_DataFimAdaptacao = new DateTime[] {DateTime.MinValue} ;
         P00TS2_A87Contrato_DataTerminoAta = new DateTime[] {DateTime.MinValue} ;
         P00TS2_A86Contrato_DataPedidoReajuste = new DateTime[] {DateTime.MinValue} ;
         P00TS2_A85Contrato_DataAssinatura = new DateTime[] {DateTime.MinValue} ;
         P00TS2_A84Contrato_DataPublicacaoDOU = new DateTime[] {DateTime.MinValue} ;
         P00TS2_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         P00TS2_A82Contrato_DataVigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         P00TS2_A81Contrato_Quantidade = new int[1] ;
         P00TS2_A75Contrato_AreaTrabalhoCod = new int[1] ;
         P00TS2_A74Contrato_Codigo = new int[1] ;
         A80Contrato_Objeto = "";
         A78Contrato_NumeroAta = "";
         A77Contrato_Numero = "";
         A1354Contrato_PrdFtrCada = "";
         A452Contrato_CalculoDivergencia = "";
         A90Contrato_RegrasPagto = "";
         A88Contrato_DataFimAdaptacao = DateTime.MinValue;
         A87Contrato_DataTerminoAta = DateTime.MinValue;
         A86Contrato_DataPedidoReajuste = DateTime.MinValue;
         A85Contrato_DataAssinatura = DateTime.MinValue;
         A84Contrato_DataPublicacaoDOU = DateTime.MinValue;
         A83Contrato_DataVigenciaTermino = DateTime.MinValue;
         A82Contrato_DataVigenciaInicio = DateTime.MinValue;
         W77Contrato_Numero = "";
         W78Contrato_NumeroAta = "";
         W80Contrato_Objeto = "";
         P00TS3_A74Contrato_Codigo = new int[1] ;
         Gx_emsg = "";
         P00TS4_A74Contrato_Codigo = new int[1] ;
         P00TS5_A74Contrato_Codigo = new int[1] ;
         P00TS5_A153ContratoClausulas_Item = new String[] {""} ;
         P00TS5_A154ContratoClausulas_Descricao = new String[] {""} ;
         P00TS5_A152ContratoClausulas_Codigo = new int[1] ;
         A153ContratoClausulas_Item = "";
         A154ContratoClausulas_Descricao = "";
         AV11ContratoClausulas_Item = "";
         W153ContratoClausulas_Item = "";
         P00TS6_A152ContratoClausulas_Codigo = new int[1] ;
         P00TS7_A74Contrato_Codigo = new int[1] ;
         P00TS8_A1207ContratoUnidades_ContratoCod = new int[1] ;
         P00TS8_A1208ContratoUnidades_Produtividade = new decimal[1] ;
         P00TS8_n1208ContratoUnidades_Produtividade = new bool[] {false} ;
         P00TS8_A1204ContratoUnidades_UndMedCod = new int[1] ;
         P00TS10_A74Contrato_Codigo = new int[1] ;
         P00TS11_A74Contrato_Codigo = new int[1] ;
         P00TS11_A1858ContratoServicos_Alias = new String[] {""} ;
         P00TS11_n1858ContratoServicos_Alias = new bool[] {false} ;
         P00TS11_A2094ContratoServicos_SolicitaGestorSistema = new bool[] {false} ;
         P00TS11_A2074ContratoServicos_CalculoRmn = new short[1] ;
         P00TS11_n2074ContratoServicos_CalculoRmn = new bool[] {false} ;
         P00TS11_A1538ContratoServicos_PercPgm = new short[1] ;
         P00TS11_n1538ContratoServicos_PercPgm = new bool[] {false} ;
         P00TS11_A1799ContratoServicos_LimiteProposta = new decimal[1] ;
         P00TS11_n1799ContratoServicos_LimiteProposta = new bool[] {false} ;
         P00TS11_A1723ContratoServicos_CodigoFiscal = new String[] {""} ;
         P00TS11_n1723ContratoServicos_CodigoFiscal = new bool[] {false} ;
         P00TS11_A1649ContratoServicos_PrazoInicio = new short[1] ;
         P00TS11_n1649ContratoServicos_PrazoInicio = new bool[] {false} ;
         P00TS11_A1539ContratoServicos_PercCnc = new short[1] ;
         P00TS11_n1539ContratoServicos_PercCnc = new bool[] {false} ;
         P00TS11_A1537ContratoServicos_PercTmp = new short[1] ;
         P00TS11_n1537ContratoServicos_PercTmp = new bool[] {false} ;
         P00TS11_A1531ContratoServicos_TipoHierarquia = new short[1] ;
         P00TS11_n1531ContratoServicos_TipoHierarquia = new bool[] {false} ;
         P00TS11_A1516ContratoServicos_TmpEstAnl = new int[1] ;
         P00TS11_n1516ContratoServicos_TmpEstAnl = new bool[] {false} ;
         P00TS11_A1502ContratoServicos_TmpEstCrr = new int[1] ;
         P00TS11_n1502ContratoServicos_TmpEstCrr = new bool[] {false} ;
         P00TS11_A1501ContratoServicos_TmpEstExc = new int[1] ;
         P00TS11_n1501ContratoServicos_TmpEstExc = new bool[] {false} ;
         P00TS11_A1455ContratoServicos_IndiceDivergencia = new decimal[1] ;
         P00TS11_n1455ContratoServicos_IndiceDivergencia = new bool[] {false} ;
         P00TS11_A1454ContratoServicos_PrazoTpDias = new String[] {""} ;
         P00TS11_n1454ContratoServicos_PrazoTpDias = new bool[] {false} ;
         P00TS11_A1397ContratoServicos_NaoRequerAtr = new bool[] {false} ;
         P00TS11_n1397ContratoServicos_NaoRequerAtr = new bool[] {false} ;
         P00TS11_A1341ContratoServicos_FatorCnvUndCnt = new decimal[1] ;
         P00TS11_n1341ContratoServicos_FatorCnvUndCnt = new bool[] {false} ;
         P00TS11_A1340ContratoServicos_QntUntCns = new decimal[1] ;
         P00TS11_n1340ContratoServicos_QntUntCns = new bool[] {false} ;
         P00TS11_A1325ContratoServicos_StatusPagFnc = new String[] {""} ;
         P00TS11_n1325ContratoServicos_StatusPagFnc = new bool[] {false} ;
         P00TS11_A1266ContratoServicos_Momento = new String[] {""} ;
         P00TS11_n1266ContratoServicos_Momento = new bool[] {false} ;
         P00TS11_A1225ContratoServicos_PrazoCorrecaoTipo = new String[] {""} ;
         P00TS11_n1225ContratoServicos_PrazoCorrecaoTipo = new bool[] {false} ;
         P00TS11_A1224ContratoServicos_PrazoCorrecao = new short[1] ;
         P00TS11_n1224ContratoServicos_PrazoCorrecao = new bool[] {false} ;
         P00TS11_A1217ContratoServicos_EspelhaAceite = new bool[] {false} ;
         P00TS11_n1217ContratoServicos_EspelhaAceite = new bool[] {false} ;
         P00TS11_A1212ContratoServicos_UnidadeContratada = new int[1] ;
         P00TS11_A1191ContratoServicos_Produtividade = new decimal[1] ;
         P00TS11_n1191ContratoServicos_Produtividade = new bool[] {false} ;
         P00TS11_A1190ContratoServicos_PrazoImediato = new bool[] {false} ;
         P00TS11_n1190ContratoServicos_PrazoImediato = new bool[] {false} ;
         P00TS11_A1182ContratoServicos_PrazoAtendeGarantia = new short[1] ;
         P00TS11_n1182ContratoServicos_PrazoAtendeGarantia = new bool[] {false} ;
         P00TS11_A1181ContratoServicos_PrazoGarantia = new short[1] ;
         P00TS11_n1181ContratoServicos_PrazoGarantia = new bool[] {false} ;
         P00TS11_A1153ContratoServicos_PrazoResposta = new short[1] ;
         P00TS11_n1153ContratoServicos_PrazoResposta = new bool[] {false} ;
         P00TS11_A1152ContratoServicos_PrazoAnalise = new short[1] ;
         P00TS11_n1152ContratoServicos_PrazoAnalise = new bool[] {false} ;
         P00TS11_A888ContratoServicos_HmlSemCnf = new bool[] {false} ;
         P00TS11_n888ContratoServicos_HmlSemCnf = new bool[] {false} ;
         P00TS11_A868ContratoServicos_TipoVnc = new String[] {""} ;
         P00TS11_n868ContratoServicos_TipoVnc = new bool[] {false} ;
         P00TS11_A639ContratoServicos_LocalExec = new String[] {""} ;
         P00TS11_A638ContratoServicos_Ativo = new bool[] {false} ;
         P00TS11_A607ServicoContrato_Faturamento = new String[] {""} ;
         P00TS11_A558Servico_Percentual = new decimal[1] ;
         P00TS11_n558Servico_Percentual = new bool[] {false} ;
         P00TS11_A557Servico_VlrUnidadeContratada = new decimal[1] ;
         P00TS11_A555Servico_QtdContratada = new long[1] ;
         P00TS11_n555Servico_QtdContratada = new bool[] {false} ;
         P00TS11_A155Servico_Codigo = new int[1] ;
         P00TS11_A160ContratoServicos_Codigo = new int[1] ;
         A1858ContratoServicos_Alias = "";
         A1723ContratoServicos_CodigoFiscal = "";
         A1454ContratoServicos_PrazoTpDias = "";
         A1325ContratoServicos_StatusPagFnc = "";
         A1266ContratoServicos_Momento = "";
         A1225ContratoServicos_PrazoCorrecaoTipo = "";
         A868ContratoServicos_TipoVnc = "";
         A639ContratoServicos_LocalExec = "";
         A607ServicoContrato_Faturamento = "";
         AV12ContratoServicos_Alias = "";
         W1858ContratoServicos_Alias = "";
         P00TS12_A160ContratoServicos_Codigo = new int[1] ;
         P00TS13_A74Contrato_Codigo = new int[1] ;
         P00TS14_A74Contrato_Codigo = new int[1] ;
         P00TS14_A103ContratoGarantia_Percentual = new decimal[1] ;
         P00TS14_A107ContratoGarantia_ValorEncerramento = new decimal[1] ;
         P00TS14_A106ContratoGarantia_DataEncerramento = new DateTime[] {DateTime.MinValue} ;
         P00TS14_A105ContratoGarantia_ValorGarantia = new decimal[1] ;
         P00TS14_A104ContratoGarantia_DataCaucao = new DateTime[] {DateTime.MinValue} ;
         P00TS14_A102ContratoGarantia_DataPagtoGarantia = new DateTime[] {DateTime.MinValue} ;
         P00TS14_A101ContratoGarantia_Codigo = new int[1] ;
         A106ContratoGarantia_DataEncerramento = DateTime.MinValue;
         A104ContratoGarantia_DataCaucao = DateTime.MinValue;
         A102ContratoGarantia_DataPagtoGarantia = DateTime.MinValue;
         P00TS15_A101ContratoGarantia_Codigo = new int[1] ;
         P00TS16_A74Contrato_Codigo = new int[1] ;
         P00TS17_A74Contrato_Codigo = new int[1] ;
         P00TS17_A311ContratoDadosCertame_Data = new DateTime[] {DateTime.MinValue} ;
         P00TS17_A313ContratoDadosCertame_DataAdjudicacao = new DateTime[] {DateTime.MinValue} ;
         P00TS17_n313ContratoDadosCertame_DataAdjudicacao = new bool[] {false} ;
         P00TS17_A312ContratoDadosCertame_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         P00TS17_n312ContratoDadosCertame_DataHomologacao = new bool[] {false} ;
         P00TS17_A310ContratoDadosCertame_Uasg = new short[1] ;
         P00TS17_n310ContratoDadosCertame_Uasg = new bool[] {false} ;
         P00TS17_A309ContratoDadosCertame_Site = new String[] {""} ;
         P00TS17_n309ContratoDadosCertame_Site = new bool[] {false} ;
         P00TS17_A308ContratoDadosCertame_Numero = new long[1] ;
         P00TS17_n308ContratoDadosCertame_Numero = new bool[] {false} ;
         P00TS17_A307ContratoDadosCertame_Modalidade = new String[] {""} ;
         P00TS17_A314ContratoDadosCertame_Codigo = new int[1] ;
         A311ContratoDadosCertame_Data = DateTime.MinValue;
         A313ContratoDadosCertame_DataAdjudicacao = DateTime.MinValue;
         A312ContratoDadosCertame_DataHomologacao = DateTime.MinValue;
         A309ContratoDadosCertame_Site = "";
         A307ContratoDadosCertame_Modalidade = "";
         AV14ContratoDadosCertame_Data = DateTime.MinValue;
         W311ContratoDadosCertame_Data = DateTime.MinValue;
         P00TS18_A314ContratoDadosCertame_Codigo = new int[1] ;
         P00TS19_A74Contrato_Codigo = new int[1] ;
         P00TS20_A74Contrato_Codigo = new int[1] ;
         P00TS20_A322ContratoObrigacao_Item = new String[] {""} ;
         P00TS20_A1424ContratoObrigacao_DataMaxima = new DateTime[] {DateTime.MinValue} ;
         P00TS20_n1424ContratoObrigacao_DataMaxima = new bool[] {false} ;
         P00TS20_A1425ContratoObrigacao_SubItem = new String[] {""} ;
         P00TS20_n1425ContratoObrigacao_SubItem = new bool[] {false} ;
         P00TS20_A323ContratoObrigacao_Descricao = new String[] {""} ;
         P00TS20_A321ContratoObrigacao_Codigo = new int[1] ;
         A322ContratoObrigacao_Item = "";
         A1424ContratoObrigacao_DataMaxima = DateTime.MinValue;
         A1425ContratoObrigacao_SubItem = "";
         A323ContratoObrigacao_Descricao = "";
         AV17ContratoObrigacao_Item = "";
         W322ContratoObrigacao_Item = "";
         P00TS21_A321ContratoObrigacao_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_clonarcontrato__default(),
            new Object[][] {
                new Object[] {
               P00TS2_A1013Contrato_PrepostoCod, P00TS2_n1013Contrato_PrepostoCod, P00TS2_A80Contrato_Objeto, P00TS2_A79Contrato_Ano, P00TS2_A78Contrato_NumeroAta, P00TS2_n78Contrato_NumeroAta, P00TS2_A77Contrato_Numero, P00TS2_A2086Contrato_LmtFtr, P00TS2_n2086Contrato_LmtFtr, P00TS2_A1358Contrato_PrdFtrFim,
               P00TS2_n1358Contrato_PrdFtrFim, P00TS2_A1357Contrato_PrdFtrIni, P00TS2_n1357Contrato_PrdFtrIni, P00TS2_A1354Contrato_PrdFtrCada, P00TS2_n1354Contrato_PrdFtrCada, P00TS2_A1150Contrato_AceitaPFFS, P00TS2_n1150Contrato_AceitaPFFS, P00TS2_A453Contrato_IndiceDivergencia, P00TS2_A452Contrato_CalculoDivergencia, P00TS2_A116Contrato_ValorUnidadeContratacao,
               P00TS2_A115Contrato_UnidadeContratacao, P00TS2_A92Contrato_Ativo, P00TS2_A91Contrato_DiasPagto, P00TS2_A90Contrato_RegrasPagto, P00TS2_A39Contratada_Codigo, P00TS2_A89Contrato_Valor, P00TS2_A88Contrato_DataFimAdaptacao, P00TS2_A87Contrato_DataTerminoAta, P00TS2_A86Contrato_DataPedidoReajuste, P00TS2_A85Contrato_DataAssinatura,
               P00TS2_A84Contrato_DataPublicacaoDOU, P00TS2_A83Contrato_DataVigenciaTermino, P00TS2_A82Contrato_DataVigenciaInicio, P00TS2_A81Contrato_Quantidade, P00TS2_A75Contrato_AreaTrabalhoCod, P00TS2_A74Contrato_Codigo
               }
               , new Object[] {
               P00TS3_A74Contrato_Codigo
               }
               , new Object[] {
               P00TS4_A74Contrato_Codigo
               }
               , new Object[] {
               P00TS5_A74Contrato_Codigo, P00TS5_A153ContratoClausulas_Item, P00TS5_A154ContratoClausulas_Descricao, P00TS5_A152ContratoClausulas_Codigo
               }
               , new Object[] {
               P00TS6_A152ContratoClausulas_Codigo
               }
               , new Object[] {
               P00TS7_A74Contrato_Codigo
               }
               , new Object[] {
               P00TS8_A1207ContratoUnidades_ContratoCod, P00TS8_A1208ContratoUnidades_Produtividade, P00TS8_n1208ContratoUnidades_Produtividade, P00TS8_A1204ContratoUnidades_UndMedCod
               }
               , new Object[] {
               }
               , new Object[] {
               P00TS10_A74Contrato_Codigo
               }
               , new Object[] {
               P00TS11_A74Contrato_Codigo, P00TS11_A1858ContratoServicos_Alias, P00TS11_n1858ContratoServicos_Alias, P00TS11_A2094ContratoServicos_SolicitaGestorSistema, P00TS11_A2074ContratoServicos_CalculoRmn, P00TS11_n2074ContratoServicos_CalculoRmn, P00TS11_A1538ContratoServicos_PercPgm, P00TS11_n1538ContratoServicos_PercPgm, P00TS11_A1799ContratoServicos_LimiteProposta, P00TS11_n1799ContratoServicos_LimiteProposta,
               P00TS11_A1723ContratoServicos_CodigoFiscal, P00TS11_n1723ContratoServicos_CodigoFiscal, P00TS11_A1649ContratoServicos_PrazoInicio, P00TS11_n1649ContratoServicos_PrazoInicio, P00TS11_A1539ContratoServicos_PercCnc, P00TS11_n1539ContratoServicos_PercCnc, P00TS11_A1537ContratoServicos_PercTmp, P00TS11_n1537ContratoServicos_PercTmp, P00TS11_A1531ContratoServicos_TipoHierarquia, P00TS11_n1531ContratoServicos_TipoHierarquia,
               P00TS11_A1516ContratoServicos_TmpEstAnl, P00TS11_n1516ContratoServicos_TmpEstAnl, P00TS11_A1502ContratoServicos_TmpEstCrr, P00TS11_n1502ContratoServicos_TmpEstCrr, P00TS11_A1501ContratoServicos_TmpEstExc, P00TS11_n1501ContratoServicos_TmpEstExc, P00TS11_A1455ContratoServicos_IndiceDivergencia, P00TS11_n1455ContratoServicos_IndiceDivergencia, P00TS11_A1454ContratoServicos_PrazoTpDias, P00TS11_n1454ContratoServicos_PrazoTpDias,
               P00TS11_A1397ContratoServicos_NaoRequerAtr, P00TS11_n1397ContratoServicos_NaoRequerAtr, P00TS11_A1341ContratoServicos_FatorCnvUndCnt, P00TS11_n1341ContratoServicos_FatorCnvUndCnt, P00TS11_A1340ContratoServicos_QntUntCns, P00TS11_n1340ContratoServicos_QntUntCns, P00TS11_A1325ContratoServicos_StatusPagFnc, P00TS11_n1325ContratoServicos_StatusPagFnc, P00TS11_A1266ContratoServicos_Momento, P00TS11_n1266ContratoServicos_Momento,
               P00TS11_A1225ContratoServicos_PrazoCorrecaoTipo, P00TS11_n1225ContratoServicos_PrazoCorrecaoTipo, P00TS11_A1224ContratoServicos_PrazoCorrecao, P00TS11_n1224ContratoServicos_PrazoCorrecao, P00TS11_A1217ContratoServicos_EspelhaAceite, P00TS11_n1217ContratoServicos_EspelhaAceite, P00TS11_A1212ContratoServicos_UnidadeContratada, P00TS11_A1191ContratoServicos_Produtividade, P00TS11_n1191ContratoServicos_Produtividade, P00TS11_A1190ContratoServicos_PrazoImediato,
               P00TS11_n1190ContratoServicos_PrazoImediato, P00TS11_A1182ContratoServicos_PrazoAtendeGarantia, P00TS11_n1182ContratoServicos_PrazoAtendeGarantia, P00TS11_A1181ContratoServicos_PrazoGarantia, P00TS11_n1181ContratoServicos_PrazoGarantia, P00TS11_A1153ContratoServicos_PrazoResposta, P00TS11_n1153ContratoServicos_PrazoResposta, P00TS11_A1152ContratoServicos_PrazoAnalise, P00TS11_n1152ContratoServicos_PrazoAnalise, P00TS11_A888ContratoServicos_HmlSemCnf,
               P00TS11_n888ContratoServicos_HmlSemCnf, P00TS11_A868ContratoServicos_TipoVnc, P00TS11_n868ContratoServicos_TipoVnc, P00TS11_A639ContratoServicos_LocalExec, P00TS11_A638ContratoServicos_Ativo, P00TS11_A607ServicoContrato_Faturamento, P00TS11_A558Servico_Percentual, P00TS11_n558Servico_Percentual, P00TS11_A557Servico_VlrUnidadeContratada, P00TS11_A555Servico_QtdContratada,
               P00TS11_n555Servico_QtdContratada, P00TS11_A155Servico_Codigo, P00TS11_A160ContratoServicos_Codigo
               }
               , new Object[] {
               P00TS12_A160ContratoServicos_Codigo
               }
               , new Object[] {
               P00TS13_A74Contrato_Codigo
               }
               , new Object[] {
               P00TS14_A74Contrato_Codigo, P00TS14_A103ContratoGarantia_Percentual, P00TS14_A107ContratoGarantia_ValorEncerramento, P00TS14_A106ContratoGarantia_DataEncerramento, P00TS14_A105ContratoGarantia_ValorGarantia, P00TS14_A104ContratoGarantia_DataCaucao, P00TS14_A102ContratoGarantia_DataPagtoGarantia, P00TS14_A101ContratoGarantia_Codigo
               }
               , new Object[] {
               P00TS15_A101ContratoGarantia_Codigo
               }
               , new Object[] {
               P00TS16_A74Contrato_Codigo
               }
               , new Object[] {
               P00TS17_A74Contrato_Codigo, P00TS17_A311ContratoDadosCertame_Data, P00TS17_A313ContratoDadosCertame_DataAdjudicacao, P00TS17_n313ContratoDadosCertame_DataAdjudicacao, P00TS17_A312ContratoDadosCertame_DataHomologacao, P00TS17_n312ContratoDadosCertame_DataHomologacao, P00TS17_A310ContratoDadosCertame_Uasg, P00TS17_n310ContratoDadosCertame_Uasg, P00TS17_A309ContratoDadosCertame_Site, P00TS17_n309ContratoDadosCertame_Site,
               P00TS17_A308ContratoDadosCertame_Numero, P00TS17_n308ContratoDadosCertame_Numero, P00TS17_A307ContratoDadosCertame_Modalidade, P00TS17_A314ContratoDadosCertame_Codigo
               }
               , new Object[] {
               P00TS18_A314ContratoDadosCertame_Codigo
               }
               , new Object[] {
               P00TS19_A74Contrato_Codigo
               }
               , new Object[] {
               P00TS20_A74Contrato_Codigo, P00TS20_A322ContratoObrigacao_Item, P00TS20_A1424ContratoObrigacao_DataMaxima, P00TS20_n1424ContratoObrigacao_DataMaxima, P00TS20_A1425ContratoObrigacao_SubItem, P00TS20_n1425ContratoObrigacao_SubItem, P00TS20_A323ContratoObrigacao_Descricao, P00TS20_A321ContratoObrigacao_Codigo
               }
               , new Object[] {
               P00TS21_A321ContratoObrigacao_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A79Contrato_Ano ;
      private short A1358Contrato_PrdFtrFim ;
      private short A1357Contrato_PrdFtrIni ;
      private short A115Contrato_UnidadeContratacao ;
      private short A91Contrato_DiasPagto ;
      private short W79Contrato_Ano ;
      private short A2074ContratoServicos_CalculoRmn ;
      private short A1538ContratoServicos_PercPgm ;
      private short A1649ContratoServicos_PrazoInicio ;
      private short A1539ContratoServicos_PercCnc ;
      private short A1537ContratoServicos_PercTmp ;
      private short A1531ContratoServicos_TipoHierarquia ;
      private short A1224ContratoServicos_PrazoCorrecao ;
      private short A1182ContratoServicos_PrazoAtendeGarantia ;
      private short A1181ContratoServicos_PrazoGarantia ;
      private short A1153ContratoServicos_PrazoResposta ;
      private short A1152ContratoServicos_PrazoAnalise ;
      private short A310ContratoDadosCertame_Uasg ;
      private int AV8Contrato_Codigo ;
      private int A1013Contrato_PrepostoCod ;
      private int A39Contratada_Codigo ;
      private int A81Contrato_Quantidade ;
      private int A75Contrato_AreaTrabalhoCod ;
      private int A74Contrato_Codigo ;
      private int AV10Contratada_Codigo ;
      private int GX_INS17 ;
      private int W1013Contrato_PrepostoCod ;
      private int AV9NewContrato ;
      private int W74Contrato_Codigo ;
      private int A152ContratoClausulas_Codigo ;
      private int GX_INS31 ;
      private int A1207ContratoUnidades_ContratoCod ;
      private int A1204ContratoUnidades_UndMedCod ;
      private int W1207ContratoUnidades_ContratoCod ;
      private int GX_INS143 ;
      private int A1516ContratoServicos_TmpEstAnl ;
      private int A1502ContratoServicos_TmpEstCrr ;
      private int A1501ContratoServicos_TmpEstExc ;
      private int A1212ContratoServicos_UnidadeContratada ;
      private int A155Servico_Codigo ;
      private int A160ContratoServicos_Codigo ;
      private int GX_INS34 ;
      private int A101ContratoGarantia_Codigo ;
      private int GX_INS21 ;
      private int A314ContratoDadosCertame_Codigo ;
      private int GX_INS52 ;
      private int A321ContratoObrigacao_Codigo ;
      private int GX_INS55 ;
      private long A555Servico_QtdContratada ;
      private long A308ContratoDadosCertame_Numero ;
      private decimal A2086Contrato_LmtFtr ;
      private decimal A453Contrato_IndiceDivergencia ;
      private decimal A116Contrato_ValorUnidadeContratacao ;
      private decimal A89Contrato_Valor ;
      private decimal A1208ContratoUnidades_Produtividade ;
      private decimal A1799ContratoServicos_LimiteProposta ;
      private decimal A1455ContratoServicos_IndiceDivergencia ;
      private decimal A1341ContratoServicos_FatorCnvUndCnt ;
      private decimal A1340ContratoServicos_QntUntCns ;
      private decimal A1191ContratoServicos_Produtividade ;
      private decimal A558Servico_Percentual ;
      private decimal A557Servico_VlrUnidadeContratada ;
      private decimal A103ContratoGarantia_Percentual ;
      private decimal A107ContratoGarantia_ValorEncerramento ;
      private decimal A105ContratoGarantia_ValorGarantia ;
      private decimal AV13ContratoGarantia_Percentual ;
      private decimal W103ContratoGarantia_Percentual ;
      private String scmdbuf ;
      private String A78Contrato_NumeroAta ;
      private String A77Contrato_Numero ;
      private String A1354Contrato_PrdFtrCada ;
      private String A452Contrato_CalculoDivergencia ;
      private String W77Contrato_Numero ;
      private String W78Contrato_NumeroAta ;
      private String Gx_emsg ;
      private String A153ContratoClausulas_Item ;
      private String AV11ContratoClausulas_Item ;
      private String W153ContratoClausulas_Item ;
      private String A1858ContratoServicos_Alias ;
      private String A1723ContratoServicos_CodigoFiscal ;
      private String A1454ContratoServicos_PrazoTpDias ;
      private String A1325ContratoServicos_StatusPagFnc ;
      private String A1266ContratoServicos_Momento ;
      private String A1225ContratoServicos_PrazoCorrecaoTipo ;
      private String A868ContratoServicos_TipoVnc ;
      private String A639ContratoServicos_LocalExec ;
      private String AV12ContratoServicos_Alias ;
      private String W1858ContratoServicos_Alias ;
      private DateTime A88Contrato_DataFimAdaptacao ;
      private DateTime A87Contrato_DataTerminoAta ;
      private DateTime A86Contrato_DataPedidoReajuste ;
      private DateTime A85Contrato_DataAssinatura ;
      private DateTime A84Contrato_DataPublicacaoDOU ;
      private DateTime A83Contrato_DataVigenciaTermino ;
      private DateTime A82Contrato_DataVigenciaInicio ;
      private DateTime A106ContratoGarantia_DataEncerramento ;
      private DateTime A104ContratoGarantia_DataCaucao ;
      private DateTime A102ContratoGarantia_DataPagtoGarantia ;
      private DateTime A311ContratoDadosCertame_Data ;
      private DateTime A313ContratoDadosCertame_DataAdjudicacao ;
      private DateTime A312ContratoDadosCertame_DataHomologacao ;
      private DateTime AV14ContratoDadosCertame_Data ;
      private DateTime W311ContratoDadosCertame_Data ;
      private DateTime A1424ContratoObrigacao_DataMaxima ;
      private bool n1013Contrato_PrepostoCod ;
      private bool n78Contrato_NumeroAta ;
      private bool n2086Contrato_LmtFtr ;
      private bool n1358Contrato_PrdFtrFim ;
      private bool n1357Contrato_PrdFtrIni ;
      private bool n1354Contrato_PrdFtrCada ;
      private bool A1150Contrato_AceitaPFFS ;
      private bool n1150Contrato_AceitaPFFS ;
      private bool A92Contrato_Ativo ;
      private bool n1208ContratoUnidades_Produtividade ;
      private bool n1858ContratoServicos_Alias ;
      private bool A2094ContratoServicos_SolicitaGestorSistema ;
      private bool n2074ContratoServicos_CalculoRmn ;
      private bool n1538ContratoServicos_PercPgm ;
      private bool n1799ContratoServicos_LimiteProposta ;
      private bool n1723ContratoServicos_CodigoFiscal ;
      private bool n1649ContratoServicos_PrazoInicio ;
      private bool n1539ContratoServicos_PercCnc ;
      private bool n1537ContratoServicos_PercTmp ;
      private bool n1531ContratoServicos_TipoHierarquia ;
      private bool n1516ContratoServicos_TmpEstAnl ;
      private bool n1502ContratoServicos_TmpEstCrr ;
      private bool n1501ContratoServicos_TmpEstExc ;
      private bool n1455ContratoServicos_IndiceDivergencia ;
      private bool n1454ContratoServicos_PrazoTpDias ;
      private bool A1397ContratoServicos_NaoRequerAtr ;
      private bool n1397ContratoServicos_NaoRequerAtr ;
      private bool n1341ContratoServicos_FatorCnvUndCnt ;
      private bool n1340ContratoServicos_QntUntCns ;
      private bool n1325ContratoServicos_StatusPagFnc ;
      private bool n1266ContratoServicos_Momento ;
      private bool n1225ContratoServicos_PrazoCorrecaoTipo ;
      private bool n1224ContratoServicos_PrazoCorrecao ;
      private bool A1217ContratoServicos_EspelhaAceite ;
      private bool n1217ContratoServicos_EspelhaAceite ;
      private bool n1191ContratoServicos_Produtividade ;
      private bool A1190ContratoServicos_PrazoImediato ;
      private bool n1190ContratoServicos_PrazoImediato ;
      private bool n1182ContratoServicos_PrazoAtendeGarantia ;
      private bool n1181ContratoServicos_PrazoGarantia ;
      private bool n1153ContratoServicos_PrazoResposta ;
      private bool n1152ContratoServicos_PrazoAnalise ;
      private bool A888ContratoServicos_HmlSemCnf ;
      private bool n888ContratoServicos_HmlSemCnf ;
      private bool n868ContratoServicos_TipoVnc ;
      private bool A638ContratoServicos_Ativo ;
      private bool n558Servico_Percentual ;
      private bool n555Servico_QtdContratada ;
      private bool n313ContratoDadosCertame_DataAdjudicacao ;
      private bool n312ContratoDadosCertame_DataHomologacao ;
      private bool n310ContratoDadosCertame_Uasg ;
      private bool n309ContratoDadosCertame_Site ;
      private bool n308ContratoDadosCertame_Numero ;
      private bool n1424ContratoObrigacao_DataMaxima ;
      private bool n1425ContratoObrigacao_SubItem ;
      private String A80Contrato_Objeto ;
      private String A90Contrato_RegrasPagto ;
      private String W80Contrato_Objeto ;
      private String A154ContratoClausulas_Descricao ;
      private String A323ContratoObrigacao_Descricao ;
      private String A607ServicoContrato_Faturamento ;
      private String A309ContratoDadosCertame_Site ;
      private String A307ContratoDadosCertame_Modalidade ;
      private String A322ContratoObrigacao_Item ;
      private String A1425ContratoObrigacao_SubItem ;
      private String AV17ContratoObrigacao_Item ;
      private String W322ContratoObrigacao_Item ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00TS2_A1013Contrato_PrepostoCod ;
      private bool[] P00TS2_n1013Contrato_PrepostoCod ;
      private String[] P00TS2_A80Contrato_Objeto ;
      private short[] P00TS2_A79Contrato_Ano ;
      private String[] P00TS2_A78Contrato_NumeroAta ;
      private bool[] P00TS2_n78Contrato_NumeroAta ;
      private String[] P00TS2_A77Contrato_Numero ;
      private decimal[] P00TS2_A2086Contrato_LmtFtr ;
      private bool[] P00TS2_n2086Contrato_LmtFtr ;
      private short[] P00TS2_A1358Contrato_PrdFtrFim ;
      private bool[] P00TS2_n1358Contrato_PrdFtrFim ;
      private short[] P00TS2_A1357Contrato_PrdFtrIni ;
      private bool[] P00TS2_n1357Contrato_PrdFtrIni ;
      private String[] P00TS2_A1354Contrato_PrdFtrCada ;
      private bool[] P00TS2_n1354Contrato_PrdFtrCada ;
      private bool[] P00TS2_A1150Contrato_AceitaPFFS ;
      private bool[] P00TS2_n1150Contrato_AceitaPFFS ;
      private decimal[] P00TS2_A453Contrato_IndiceDivergencia ;
      private String[] P00TS2_A452Contrato_CalculoDivergencia ;
      private decimal[] P00TS2_A116Contrato_ValorUnidadeContratacao ;
      private short[] P00TS2_A115Contrato_UnidadeContratacao ;
      private bool[] P00TS2_A92Contrato_Ativo ;
      private short[] P00TS2_A91Contrato_DiasPagto ;
      private String[] P00TS2_A90Contrato_RegrasPagto ;
      private int[] P00TS2_A39Contratada_Codigo ;
      private decimal[] P00TS2_A89Contrato_Valor ;
      private DateTime[] P00TS2_A88Contrato_DataFimAdaptacao ;
      private DateTime[] P00TS2_A87Contrato_DataTerminoAta ;
      private DateTime[] P00TS2_A86Contrato_DataPedidoReajuste ;
      private DateTime[] P00TS2_A85Contrato_DataAssinatura ;
      private DateTime[] P00TS2_A84Contrato_DataPublicacaoDOU ;
      private DateTime[] P00TS2_A83Contrato_DataVigenciaTermino ;
      private DateTime[] P00TS2_A82Contrato_DataVigenciaInicio ;
      private int[] P00TS2_A81Contrato_Quantidade ;
      private int[] P00TS2_A75Contrato_AreaTrabalhoCod ;
      private int[] P00TS2_A74Contrato_Codigo ;
      private int[] P00TS3_A74Contrato_Codigo ;
      private int[] P00TS4_A74Contrato_Codigo ;
      private int[] P00TS5_A74Contrato_Codigo ;
      private String[] P00TS5_A153ContratoClausulas_Item ;
      private String[] P00TS5_A154ContratoClausulas_Descricao ;
      private int[] P00TS5_A152ContratoClausulas_Codigo ;
      private int[] P00TS6_A152ContratoClausulas_Codigo ;
      private int[] P00TS7_A74Contrato_Codigo ;
      private int[] P00TS8_A1207ContratoUnidades_ContratoCod ;
      private decimal[] P00TS8_A1208ContratoUnidades_Produtividade ;
      private bool[] P00TS8_n1208ContratoUnidades_Produtividade ;
      private int[] P00TS8_A1204ContratoUnidades_UndMedCod ;
      private int[] P00TS10_A74Contrato_Codigo ;
      private int[] P00TS11_A74Contrato_Codigo ;
      private String[] P00TS11_A1858ContratoServicos_Alias ;
      private bool[] P00TS11_n1858ContratoServicos_Alias ;
      private bool[] P00TS11_A2094ContratoServicos_SolicitaGestorSistema ;
      private short[] P00TS11_A2074ContratoServicos_CalculoRmn ;
      private bool[] P00TS11_n2074ContratoServicos_CalculoRmn ;
      private short[] P00TS11_A1538ContratoServicos_PercPgm ;
      private bool[] P00TS11_n1538ContratoServicos_PercPgm ;
      private decimal[] P00TS11_A1799ContratoServicos_LimiteProposta ;
      private bool[] P00TS11_n1799ContratoServicos_LimiteProposta ;
      private String[] P00TS11_A1723ContratoServicos_CodigoFiscal ;
      private bool[] P00TS11_n1723ContratoServicos_CodigoFiscal ;
      private short[] P00TS11_A1649ContratoServicos_PrazoInicio ;
      private bool[] P00TS11_n1649ContratoServicos_PrazoInicio ;
      private short[] P00TS11_A1539ContratoServicos_PercCnc ;
      private bool[] P00TS11_n1539ContratoServicos_PercCnc ;
      private short[] P00TS11_A1537ContratoServicos_PercTmp ;
      private bool[] P00TS11_n1537ContratoServicos_PercTmp ;
      private short[] P00TS11_A1531ContratoServicos_TipoHierarquia ;
      private bool[] P00TS11_n1531ContratoServicos_TipoHierarquia ;
      private int[] P00TS11_A1516ContratoServicos_TmpEstAnl ;
      private bool[] P00TS11_n1516ContratoServicos_TmpEstAnl ;
      private int[] P00TS11_A1502ContratoServicos_TmpEstCrr ;
      private bool[] P00TS11_n1502ContratoServicos_TmpEstCrr ;
      private int[] P00TS11_A1501ContratoServicos_TmpEstExc ;
      private bool[] P00TS11_n1501ContratoServicos_TmpEstExc ;
      private decimal[] P00TS11_A1455ContratoServicos_IndiceDivergencia ;
      private bool[] P00TS11_n1455ContratoServicos_IndiceDivergencia ;
      private String[] P00TS11_A1454ContratoServicos_PrazoTpDias ;
      private bool[] P00TS11_n1454ContratoServicos_PrazoTpDias ;
      private bool[] P00TS11_A1397ContratoServicos_NaoRequerAtr ;
      private bool[] P00TS11_n1397ContratoServicos_NaoRequerAtr ;
      private decimal[] P00TS11_A1341ContratoServicos_FatorCnvUndCnt ;
      private bool[] P00TS11_n1341ContratoServicos_FatorCnvUndCnt ;
      private decimal[] P00TS11_A1340ContratoServicos_QntUntCns ;
      private bool[] P00TS11_n1340ContratoServicos_QntUntCns ;
      private String[] P00TS11_A1325ContratoServicos_StatusPagFnc ;
      private bool[] P00TS11_n1325ContratoServicos_StatusPagFnc ;
      private String[] P00TS11_A1266ContratoServicos_Momento ;
      private bool[] P00TS11_n1266ContratoServicos_Momento ;
      private String[] P00TS11_A1225ContratoServicos_PrazoCorrecaoTipo ;
      private bool[] P00TS11_n1225ContratoServicos_PrazoCorrecaoTipo ;
      private short[] P00TS11_A1224ContratoServicos_PrazoCorrecao ;
      private bool[] P00TS11_n1224ContratoServicos_PrazoCorrecao ;
      private bool[] P00TS11_A1217ContratoServicos_EspelhaAceite ;
      private bool[] P00TS11_n1217ContratoServicos_EspelhaAceite ;
      private int[] P00TS11_A1212ContratoServicos_UnidadeContratada ;
      private decimal[] P00TS11_A1191ContratoServicos_Produtividade ;
      private bool[] P00TS11_n1191ContratoServicos_Produtividade ;
      private bool[] P00TS11_A1190ContratoServicos_PrazoImediato ;
      private bool[] P00TS11_n1190ContratoServicos_PrazoImediato ;
      private short[] P00TS11_A1182ContratoServicos_PrazoAtendeGarantia ;
      private bool[] P00TS11_n1182ContratoServicos_PrazoAtendeGarantia ;
      private short[] P00TS11_A1181ContratoServicos_PrazoGarantia ;
      private bool[] P00TS11_n1181ContratoServicos_PrazoGarantia ;
      private short[] P00TS11_A1153ContratoServicos_PrazoResposta ;
      private bool[] P00TS11_n1153ContratoServicos_PrazoResposta ;
      private short[] P00TS11_A1152ContratoServicos_PrazoAnalise ;
      private bool[] P00TS11_n1152ContratoServicos_PrazoAnalise ;
      private bool[] P00TS11_A888ContratoServicos_HmlSemCnf ;
      private bool[] P00TS11_n888ContratoServicos_HmlSemCnf ;
      private String[] P00TS11_A868ContratoServicos_TipoVnc ;
      private bool[] P00TS11_n868ContratoServicos_TipoVnc ;
      private String[] P00TS11_A639ContratoServicos_LocalExec ;
      private bool[] P00TS11_A638ContratoServicos_Ativo ;
      private String[] P00TS11_A607ServicoContrato_Faturamento ;
      private decimal[] P00TS11_A558Servico_Percentual ;
      private bool[] P00TS11_n558Servico_Percentual ;
      private decimal[] P00TS11_A557Servico_VlrUnidadeContratada ;
      private long[] P00TS11_A555Servico_QtdContratada ;
      private bool[] P00TS11_n555Servico_QtdContratada ;
      private int[] P00TS11_A155Servico_Codigo ;
      private int[] P00TS11_A160ContratoServicos_Codigo ;
      private int[] P00TS12_A160ContratoServicos_Codigo ;
      private int[] P00TS13_A74Contrato_Codigo ;
      private int[] P00TS14_A74Contrato_Codigo ;
      private decimal[] P00TS14_A103ContratoGarantia_Percentual ;
      private decimal[] P00TS14_A107ContratoGarantia_ValorEncerramento ;
      private DateTime[] P00TS14_A106ContratoGarantia_DataEncerramento ;
      private decimal[] P00TS14_A105ContratoGarantia_ValorGarantia ;
      private DateTime[] P00TS14_A104ContratoGarantia_DataCaucao ;
      private DateTime[] P00TS14_A102ContratoGarantia_DataPagtoGarantia ;
      private int[] P00TS14_A101ContratoGarantia_Codigo ;
      private int[] P00TS15_A101ContratoGarantia_Codigo ;
      private int[] P00TS16_A74Contrato_Codigo ;
      private int[] P00TS17_A74Contrato_Codigo ;
      private DateTime[] P00TS17_A311ContratoDadosCertame_Data ;
      private DateTime[] P00TS17_A313ContratoDadosCertame_DataAdjudicacao ;
      private bool[] P00TS17_n313ContratoDadosCertame_DataAdjudicacao ;
      private DateTime[] P00TS17_A312ContratoDadosCertame_DataHomologacao ;
      private bool[] P00TS17_n312ContratoDadosCertame_DataHomologacao ;
      private short[] P00TS17_A310ContratoDadosCertame_Uasg ;
      private bool[] P00TS17_n310ContratoDadosCertame_Uasg ;
      private String[] P00TS17_A309ContratoDadosCertame_Site ;
      private bool[] P00TS17_n309ContratoDadosCertame_Site ;
      private long[] P00TS17_A308ContratoDadosCertame_Numero ;
      private bool[] P00TS17_n308ContratoDadosCertame_Numero ;
      private String[] P00TS17_A307ContratoDadosCertame_Modalidade ;
      private int[] P00TS17_A314ContratoDadosCertame_Codigo ;
      private int[] P00TS18_A314ContratoDadosCertame_Codigo ;
      private int[] P00TS19_A74Contrato_Codigo ;
      private int[] P00TS20_A74Contrato_Codigo ;
      private String[] P00TS20_A322ContratoObrigacao_Item ;
      private DateTime[] P00TS20_A1424ContratoObrigacao_DataMaxima ;
      private bool[] P00TS20_n1424ContratoObrigacao_DataMaxima ;
      private String[] P00TS20_A1425ContratoObrigacao_SubItem ;
      private bool[] P00TS20_n1425ContratoObrigacao_SubItem ;
      private String[] P00TS20_A323ContratoObrigacao_Descricao ;
      private int[] P00TS20_A321ContratoObrigacao_Codigo ;
      private int[] P00TS21_A321ContratoObrigacao_Codigo ;
   }

   public class prc_clonarcontrato__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00TS2 ;
          prmP00TS2 = new Object[] {
          new Object[] {"@AV8Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00TS3 ;
          prmP00TS3 = new Object[] {
          new Object[] {"@Contrato_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contrato_Numero",SqlDbType.Char,20,0} ,
          new Object[] {"@Contrato_NumeroAta",SqlDbType.Char,10,0} ,
          new Object[] {"@Contrato_Ano",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contrato_Objeto",SqlDbType.VarChar,10000,0} ,
          new Object[] {"@Contrato_Quantidade",SqlDbType.Int,9,0} ,
          new Object[] {"@Contrato_DataVigenciaInicio",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Contrato_DataVigenciaTermino",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Contrato_DataPublicacaoDOU",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Contrato_DataAssinatura",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Contrato_DataPedidoReajuste",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Contrato_DataTerminoAta",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Contrato_DataFimAdaptacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Contrato_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Contratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Contrato_RegrasPagto",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Contrato_DiasPagto",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contrato_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contrato_UnidadeContratacao",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@Contrato_ValorUnidadeContratacao",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Contrato_CalculoDivergencia",SqlDbType.Char,1,0} ,
          new Object[] {"@Contrato_IndiceDivergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Contrato_PrepostoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Contrato_AceitaPFFS",SqlDbType.Bit,4,0} ,
          new Object[] {"@Contrato_PrdFtrCada",SqlDbType.Char,1,0} ,
          new Object[] {"@Contrato_PrdFtrIni",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contrato_PrdFtrFim",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Contrato_LmtFtr",SqlDbType.Decimal,14,5}
          } ;
          Object[] prmP00TS4 ;
          prmP00TS4 = new Object[] {
          new Object[] {"@AV8Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00TS5 ;
          prmP00TS5 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00TS6 ;
          prmP00TS6 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoClausulas_Item",SqlDbType.Char,10,0} ,
          new Object[] {"@ContratoClausulas_Descricao",SqlDbType.VarChar,500,0}
          } ;
          Object[] prmP00TS7 ;
          prmP00TS7 = new Object[] {
          new Object[] {"@AV8Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00TS8 ;
          prmP00TS8 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00TS9 ;
          prmP00TS9 = new Object[] {
          new Object[] {"@ContratoUnidades_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoUnidades_UndMedCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoUnidades_Produtividade",SqlDbType.Decimal,14,5}
          } ;
          Object[] prmP00TS10 ;
          prmP00TS10 = new Object[] {
          new Object[] {"@AV8Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00TS11 ;
          prmP00TS11 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00TS12 ;
          prmP00TS12 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Servico_QtdContratada",SqlDbType.Decimal,10,0} ,
          new Object[] {"@Servico_VlrUnidadeContratada",SqlDbType.Decimal,18,5} ,
          new Object[] {"@Servico_Percentual",SqlDbType.Decimal,7,3} ,
          new Object[] {"@ServicoContrato_Faturamento",SqlDbType.VarChar,5,0} ,
          new Object[] {"@ContratoServicos_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoServicos_LocalExec",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicos_TipoVnc",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicos_HmlSemCnf",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoServicos_PrazoAnalise",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PrazoResposta",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PrazoGarantia",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PrazoAtendeGarantia",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PrazoImediato",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoServicos_Produtividade",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContratoServicos_UnidadeContratada",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicos_EspelhaAceite",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoServicos_PrazoCorrecao",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PrazoCorrecaoTipo",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicos_Momento",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicos_StatusPagFnc",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicos_QntUntCns",SqlDbType.Decimal,9,4} ,
          new Object[] {"@ContratoServicos_FatorCnvUndCnt",SqlDbType.Decimal,9,4} ,
          new Object[] {"@ContratoServicos_NaoRequerAtr",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratoServicos_PrazoTpDias",SqlDbType.Char,1,0} ,
          new Object[] {"@ContratoServicos_IndiceDivergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContratoServicos_TmpEstExc",SqlDbType.Int,8,0} ,
          new Object[] {"@ContratoServicos_TmpEstCrr",SqlDbType.Int,8,0} ,
          new Object[] {"@ContratoServicos_TmpEstAnl",SqlDbType.Int,8,0} ,
          new Object[] {"@ContratoServicos_TipoHierarquia",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PercTmp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PercCnc",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_PrazoInicio",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_CodigoFiscal",SqlDbType.Char,5,0} ,
          new Object[] {"@ContratoServicos_LimiteProposta",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContratoServicos_PercPgm",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_Alias",SqlDbType.Char,15,0} ,
          new Object[] {"@ContratoServicos_CalculoRmn",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicos_SolicitaGestorSistema",SqlDbType.Bit,4,0}
          } ;
          String cmdBufferP00TS12 ;
          cmdBufferP00TS12=" INSERT INTO [ContratoServicos]([Contrato_Codigo], [Servico_Codigo], [Servico_QtdContratada], [Servico_VlrUnidadeContratada], [Servico_Percentual], [ServicoContrato_Faturamento], [ContratoServicos_Ativo], [ContratoServicos_LocalExec], [ContratoServicos_TipoVnc], [ContratoServicos_HmlSemCnf], [ContratoServicos_PrazoAnalise], [ContratoServicos_PrazoResposta], [ContratoServicos_PrazoGarantia], [ContratoServicos_PrazoAtendeGarantia], [ContratoServicos_PrazoImediato], [ContratoServicos_Produtividade], [ContratoServicos_UnidadeContratada], [ContratoServicos_EspelhaAceite], [ContratoServicos_PrazoCorrecao], [ContratoServicos_PrazoCorrecaoTipo], [ContratoServicos_Momento], [ContratoServicos_StatusPagFnc], [ContratoServicos_QntUntCns], [ContratoServicos_FatorCnvUndCnt], [ContratoServicos_NaoRequerAtr], [ContratoServicos_PrazoTpDias], [ContratoServicos_IndiceDivergencia], [ContratoServicos_TmpEstExc], [ContratoServicos_TmpEstCrr], [ContratoServicos_TmpEstAnl], [ContratoServicos_TipoHierarquia], [ContratoServicos_PercTmp], [ContratoServicos_PercCnc], [ContratoServicos_PrazoInicio], [ContratoServicos_CodigoFiscal], [ContratoServicos_LimiteProposta], [ContratoServicos_PercPgm], [ContratoServicos_Alias], [ContratoServicos_CalculoRmn], [ContratoServicos_SolicitaGestorSistema]) VALUES(@Contrato_Codigo, @Servico_Codigo, @Servico_QtdContratada, @Servico_VlrUnidadeContratada, @Servico_Percentual, @ServicoContrato_Faturamento, @ContratoServicos_Ativo, @ContratoServicos_LocalExec, @ContratoServicos_TipoVnc, @ContratoServicos_HmlSemCnf, @ContratoServicos_PrazoAnalise, @ContratoServicos_PrazoResposta, @ContratoServicos_PrazoGarantia, @ContratoServicos_PrazoAtendeGarantia, @ContratoServicos_PrazoImediato, @ContratoServicos_Produtividade, @ContratoServicos_UnidadeContratada, @ContratoServicos_EspelhaAceite, "
          + " @ContratoServicos_PrazoCorrecao, @ContratoServicos_PrazoCorrecaoTipo, @ContratoServicos_Momento, @ContratoServicos_StatusPagFnc, @ContratoServicos_QntUntCns, @ContratoServicos_FatorCnvUndCnt, @ContratoServicos_NaoRequerAtr, @ContratoServicos_PrazoTpDias, @ContratoServicos_IndiceDivergencia, @ContratoServicos_TmpEstExc, @ContratoServicos_TmpEstCrr, @ContratoServicos_TmpEstAnl, @ContratoServicos_TipoHierarquia, @ContratoServicos_PercTmp, @ContratoServicos_PercCnc, @ContratoServicos_PrazoInicio, @ContratoServicos_CodigoFiscal, @ContratoServicos_LimiteProposta, @ContratoServicos_PercPgm, @ContratoServicos_Alias, @ContratoServicos_CalculoRmn, @ContratoServicos_SolicitaGestorSistema); SELECT SCOPE_IDENTITY()" ;
          Object[] prmP00TS13 ;
          prmP00TS13 = new Object[] {
          new Object[] {"@AV8Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00TS14 ;
          prmP00TS14 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00TS15 ;
          prmP00TS15 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoGarantia_DataPagtoGarantia",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContratoGarantia_Percentual",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContratoGarantia_DataCaucao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContratoGarantia_ValorGarantia",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContratoGarantia_DataEncerramento",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContratoGarantia_ValorEncerramento",SqlDbType.Decimal,18,5}
          } ;
          Object[] prmP00TS16 ;
          prmP00TS16 = new Object[] {
          new Object[] {"@AV8Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00TS17 ;
          prmP00TS17 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00TS18 ;
          prmP00TS18 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoDadosCertame_Modalidade",SqlDbType.VarChar,40,0} ,
          new Object[] {"@ContratoDadosCertame_Numero",SqlDbType.Decimal,10,0} ,
          new Object[] {"@ContratoDadosCertame_Site",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@ContratoDadosCertame_Uasg",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoDadosCertame_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContratoDadosCertame_DataHomologacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContratoDadosCertame_DataAdjudicacao",SqlDbType.DateTime,8,0}
          } ;
          Object[] prmP00TS19 ;
          prmP00TS19 = new Object[] {
          new Object[] {"@AV8Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00TS20 ;
          prmP00TS20 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00TS21 ;
          prmP00TS21 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoObrigacao_Item",SqlDbType.VarChar,40,0} ,
          new Object[] {"@ContratoObrigacao_Descricao",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@ContratoObrigacao_SubItem",SqlDbType.VarChar,40,0} ,
          new Object[] {"@ContratoObrigacao_DataMaxima",SqlDbType.DateTime,8,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00TS2", "SELECT TOP 1 [Contrato_PrepostoCod], [Contrato_Objeto], [Contrato_Ano], [Contrato_NumeroAta], [Contrato_Numero], [Contrato_LmtFtr], [Contrato_PrdFtrFim], [Contrato_PrdFtrIni], [Contrato_PrdFtrCada], [Contrato_AceitaPFFS], [Contrato_IndiceDivergencia], [Contrato_CalculoDivergencia], [Contrato_ValorUnidadeContratacao], [Contrato_UnidadeContratacao], [Contrato_Ativo], [Contrato_DiasPagto], [Contrato_RegrasPagto], [Contratada_Codigo], [Contrato_Valor], [Contrato_DataFimAdaptacao], [Contrato_DataTerminoAta], [Contrato_DataPedidoReajuste], [Contrato_DataAssinatura], [Contrato_DataPublicacaoDOU], [Contrato_DataVigenciaTermino], [Contrato_DataVigenciaInicio], [Contrato_Quantidade], [Contrato_AreaTrabalhoCod], [Contrato_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @AV8Contrato_Codigo ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00TS2,1,0,true,true )
             ,new CursorDef("P00TS3", "INSERT INTO [Contrato]([Contrato_AreaTrabalhoCod], [Contrato_Numero], [Contrato_NumeroAta], [Contrato_Ano], [Contrato_Objeto], [Contrato_Quantidade], [Contrato_DataVigenciaInicio], [Contrato_DataVigenciaTermino], [Contrato_DataPublicacaoDOU], [Contrato_DataAssinatura], [Contrato_DataPedidoReajuste], [Contrato_DataTerminoAta], [Contrato_DataFimAdaptacao], [Contrato_Valor], [Contratada_Codigo], [Contrato_RegrasPagto], [Contrato_DiasPagto], [Contrato_Ativo], [Contrato_UnidadeContratacao], [Contrato_ValorUnidadeContratacao], [Contrato_CalculoDivergencia], [Contrato_IndiceDivergencia], [Contrato_PrepostoCod], [Contrato_AceitaPFFS], [Contrato_PrdFtrCada], [Contrato_PrdFtrIni], [Contrato_PrdFtrFim], [Contrato_LmtFtr]) VALUES(@Contrato_AreaTrabalhoCod, @Contrato_Numero, @Contrato_NumeroAta, @Contrato_Ano, @Contrato_Objeto, @Contrato_Quantidade, @Contrato_DataVigenciaInicio, @Contrato_DataVigenciaTermino, @Contrato_DataPublicacaoDOU, @Contrato_DataAssinatura, @Contrato_DataPedidoReajuste, @Contrato_DataTerminoAta, @Contrato_DataFimAdaptacao, @Contrato_Valor, @Contratada_Codigo, @Contrato_RegrasPagto, @Contrato_DiasPagto, @Contrato_Ativo, @Contrato_UnidadeContratacao, @Contrato_ValorUnidadeContratacao, @Contrato_CalculoDivergencia, @Contrato_IndiceDivergencia, @Contrato_PrepostoCod, @Contrato_AceitaPFFS, @Contrato_PrdFtrCada, @Contrato_PrdFtrIni, @Contrato_PrdFtrFim, @Contrato_LmtFtr); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmP00TS3)
             ,new CursorDef("P00TS4", "SELECT [Contrato_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @AV8Contrato_Codigo ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00TS4,1,0,true,true )
             ,new CursorDef("P00TS5", "SELECT [Contrato_Codigo], [ContratoClausulas_Item], [ContratoClausulas_Descricao], [ContratoClausulas_Codigo] FROM [ContratoClausulas] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00TS5,100,0,true,false )
             ,new CursorDef("P00TS6", "INSERT INTO [ContratoClausulas]([Contrato_Codigo], [ContratoClausulas_Item], [ContratoClausulas_Descricao]) VALUES(@Contrato_Codigo, @ContratoClausulas_Item, @ContratoClausulas_Descricao); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmP00TS6)
             ,new CursorDef("P00TS7", "SELECT [Contrato_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @AV8Contrato_Codigo ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00TS7,1,0,true,true )
             ,new CursorDef("P00TS8", "SELECT [ContratoUnidades_ContratoCod], [ContratoUnidades_Produtividade], [ContratoUnidades_UndMedCod] FROM [ContratoUnidades] WITH (NOLOCK) WHERE [ContratoUnidades_ContratoCod] = @Contrato_Codigo ORDER BY [ContratoUnidades_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00TS8,100,0,true,false )
             ,new CursorDef("P00TS9", "INSERT INTO [ContratoUnidades]([ContratoUnidades_ContratoCod], [ContratoUnidades_UndMedCod], [ContratoUnidades_Produtividade]) VALUES(@ContratoUnidades_ContratoCod, @ContratoUnidades_UndMedCod, @ContratoUnidades_Produtividade)", GxErrorMask.GX_NOMASK,prmP00TS9)
             ,new CursorDef("P00TS10", "SELECT [Contrato_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @AV8Contrato_Codigo ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00TS10,1,0,true,true )
             ,new CursorDef("P00TS11", "SELECT [Contrato_Codigo], [ContratoServicos_Alias], [ContratoServicos_SolicitaGestorSistema], [ContratoServicos_CalculoRmn], [ContratoServicos_PercPgm], [ContratoServicos_LimiteProposta], [ContratoServicos_CodigoFiscal], [ContratoServicos_PrazoInicio], [ContratoServicos_PercCnc], [ContratoServicos_PercTmp], [ContratoServicos_TipoHierarquia], [ContratoServicos_TmpEstAnl], [ContratoServicos_TmpEstCrr], [ContratoServicos_TmpEstExc], [ContratoServicos_IndiceDivergencia], [ContratoServicos_PrazoTpDias], [ContratoServicos_NaoRequerAtr], [ContratoServicos_FatorCnvUndCnt], [ContratoServicos_QntUntCns], [ContratoServicos_StatusPagFnc], [ContratoServicos_Momento], [ContratoServicos_PrazoCorrecaoTipo], [ContratoServicos_PrazoCorrecao], [ContratoServicos_EspelhaAceite], [ContratoServicos_UnidadeContratada], [ContratoServicos_Produtividade], [ContratoServicos_PrazoImediato], [ContratoServicos_PrazoAtendeGarantia], [ContratoServicos_PrazoGarantia], [ContratoServicos_PrazoResposta], [ContratoServicos_PrazoAnalise], [ContratoServicos_HmlSemCnf], [ContratoServicos_TipoVnc], [ContratoServicos_LocalExec], [ContratoServicos_Ativo], [ServicoContrato_Faturamento], [Servico_Percentual], [Servico_VlrUnidadeContratada], [Servico_QtdContratada], [Servico_Codigo], [ContratoServicos_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00TS11,100,0,true,false )
             ,new CursorDef("P00TS12", cmdBufferP00TS12, GxErrorMask.GX_NOMASK,prmP00TS12)
             ,new CursorDef("P00TS13", "SELECT [Contrato_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @AV8Contrato_Codigo ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00TS13,1,0,true,true )
             ,new CursorDef("P00TS14", "SELECT [Contrato_Codigo], [ContratoGarantia_Percentual], [ContratoGarantia_ValorEncerramento], [ContratoGarantia_DataEncerramento], [ContratoGarantia_ValorGarantia], [ContratoGarantia_DataCaucao], [ContratoGarantia_DataPagtoGarantia], [ContratoGarantia_Codigo] FROM [ContratoGarantia] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00TS14,100,0,true,false )
             ,new CursorDef("P00TS15", "INSERT INTO [ContratoGarantia]([Contrato_Codigo], [ContratoGarantia_DataPagtoGarantia], [ContratoGarantia_Percentual], [ContratoGarantia_DataCaucao], [ContratoGarantia_ValorGarantia], [ContratoGarantia_DataEncerramento], [ContratoGarantia_ValorEncerramento]) VALUES(@Contrato_Codigo, @ContratoGarantia_DataPagtoGarantia, @ContratoGarantia_Percentual, @ContratoGarantia_DataCaucao, @ContratoGarantia_ValorGarantia, @ContratoGarantia_DataEncerramento, @ContratoGarantia_ValorEncerramento); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmP00TS15)
             ,new CursorDef("P00TS16", "SELECT [Contrato_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @AV8Contrato_Codigo ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00TS16,1,0,true,true )
             ,new CursorDef("P00TS17", "SELECT [Contrato_Codigo], [ContratoDadosCertame_Data], [ContratoDadosCertame_DataAdjudicacao], [ContratoDadosCertame_DataHomologacao], [ContratoDadosCertame_Uasg], [ContratoDadosCertame_Site], [ContratoDadosCertame_Numero], [ContratoDadosCertame_Modalidade], [ContratoDadosCertame_Codigo] FROM [ContratoDadosCertame] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00TS17,100,0,true,false )
             ,new CursorDef("P00TS18", "INSERT INTO [ContratoDadosCertame]([Contrato_Codigo], [ContratoDadosCertame_Modalidade], [ContratoDadosCertame_Numero], [ContratoDadosCertame_Site], [ContratoDadosCertame_Uasg], [ContratoDadosCertame_Data], [ContratoDadosCertame_DataHomologacao], [ContratoDadosCertame_DataAdjudicacao]) VALUES(@Contrato_Codigo, @ContratoDadosCertame_Modalidade, @ContratoDadosCertame_Numero, @ContratoDadosCertame_Site, @ContratoDadosCertame_Uasg, @ContratoDadosCertame_Data, @ContratoDadosCertame_DataHomologacao, @ContratoDadosCertame_DataAdjudicacao); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmP00TS18)
             ,new CursorDef("P00TS19", "SELECT [Contrato_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @AV8Contrato_Codigo ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00TS19,1,0,true,true )
             ,new CursorDef("P00TS20", "SELECT [Contrato_Codigo], [ContratoObrigacao_Item], [ContratoObrigacao_DataMaxima], [ContratoObrigacao_SubItem], [ContratoObrigacao_Descricao], [ContratoObrigacao_Codigo] FROM [ContratoObrigacao] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00TS20,100,0,true,false )
             ,new CursorDef("P00TS21", "INSERT INTO [ContratoObrigacao]([Contrato_Codigo], [ContratoObrigacao_Item], [ContratoObrigacao_Descricao], [ContratoObrigacao_SubItem], [ContratoObrigacao_DataMaxima]) VALUES(@Contrato_Codigo, @ContratoObrigacao_Item, @ContratoObrigacao_Descricao, @ContratoObrigacao_SubItem, @ContratoObrigacao_DataMaxima); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmP00TS21)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getLongVarchar(2) ;
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 10) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 20) ;
                ((decimal[]) buf[7])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((short[]) buf[9])[0] = rslt.getShort(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((short[]) buf[11])[0] = rslt.getShort(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getString(9, 1) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((bool[]) buf[15])[0] = rslt.getBool(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((decimal[]) buf[17])[0] = rslt.getDecimal(11) ;
                ((String[]) buf[18])[0] = rslt.getString(12, 1) ;
                ((decimal[]) buf[19])[0] = rslt.getDecimal(13) ;
                ((short[]) buf[20])[0] = rslt.getShort(14) ;
                ((bool[]) buf[21])[0] = rslt.getBool(15) ;
                ((short[]) buf[22])[0] = rslt.getShort(16) ;
                ((String[]) buf[23])[0] = rslt.getLongVarchar(17) ;
                ((int[]) buf[24])[0] = rslt.getInt(18) ;
                ((decimal[]) buf[25])[0] = rslt.getDecimal(19) ;
                ((DateTime[]) buf[26])[0] = rslt.getGXDate(20) ;
                ((DateTime[]) buf[27])[0] = rslt.getGXDate(21) ;
                ((DateTime[]) buf[28])[0] = rslt.getGXDate(22) ;
                ((DateTime[]) buf[29])[0] = rslt.getGXDate(23) ;
                ((DateTime[]) buf[30])[0] = rslt.getGXDate(24) ;
                ((DateTime[]) buf[31])[0] = rslt.getGXDate(25) ;
                ((DateTime[]) buf[32])[0] = rslt.getGXDate(26) ;
                ((int[]) buf[33])[0] = rslt.getInt(27) ;
                ((int[]) buf[34])[0] = rslt.getInt(28) ;
                ((int[]) buf[35])[0] = rslt.getInt(29) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 10) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((short[]) buf[6])[0] = rslt.getShort(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 5) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((short[]) buf[12])[0] = rslt.getShort(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((short[]) buf[14])[0] = rslt.getShort(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((short[]) buf[16])[0] = rslt.getShort(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((short[]) buf[18])[0] = rslt.getShort(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((int[]) buf[20])[0] = rslt.getInt(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((int[]) buf[22])[0] = rslt.getInt(13) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((int[]) buf[24])[0] = rslt.getInt(14) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((decimal[]) buf[26])[0] = rslt.getDecimal(15) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                ((String[]) buf[28])[0] = rslt.getString(16, 1) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(16);
                ((bool[]) buf[30])[0] = rslt.getBool(17) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(17);
                ((decimal[]) buf[32])[0] = rslt.getDecimal(18) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(18);
                ((decimal[]) buf[34])[0] = rslt.getDecimal(19) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(19);
                ((String[]) buf[36])[0] = rslt.getString(20, 1) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(20);
                ((String[]) buf[38])[0] = rslt.getString(21, 1) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(21);
                ((String[]) buf[40])[0] = rslt.getString(22, 1) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(22);
                ((short[]) buf[42])[0] = rslt.getShort(23) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(23);
                ((bool[]) buf[44])[0] = rslt.getBool(24) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(24);
                ((int[]) buf[46])[0] = rslt.getInt(25) ;
                ((decimal[]) buf[47])[0] = rslt.getDecimal(26) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(26);
                ((bool[]) buf[49])[0] = rslt.getBool(27) ;
                ((bool[]) buf[50])[0] = rslt.wasNull(27);
                ((short[]) buf[51])[0] = rslt.getShort(28) ;
                ((bool[]) buf[52])[0] = rslt.wasNull(28);
                ((short[]) buf[53])[0] = rslt.getShort(29) ;
                ((bool[]) buf[54])[0] = rslt.wasNull(29);
                ((short[]) buf[55])[0] = rslt.getShort(30) ;
                ((bool[]) buf[56])[0] = rslt.wasNull(30);
                ((short[]) buf[57])[0] = rslt.getShort(31) ;
                ((bool[]) buf[58])[0] = rslt.wasNull(31);
                ((bool[]) buf[59])[0] = rslt.getBool(32) ;
                ((bool[]) buf[60])[0] = rslt.wasNull(32);
                ((String[]) buf[61])[0] = rslt.getString(33, 1) ;
                ((bool[]) buf[62])[0] = rslt.wasNull(33);
                ((String[]) buf[63])[0] = rslt.getString(34, 1) ;
                ((bool[]) buf[64])[0] = rslt.getBool(35) ;
                ((String[]) buf[65])[0] = rslt.getVarchar(36) ;
                ((decimal[]) buf[66])[0] = rslt.getDecimal(37) ;
                ((bool[]) buf[67])[0] = rslt.wasNull(37);
                ((decimal[]) buf[68])[0] = rslt.getDecimal(38) ;
                ((long[]) buf[69])[0] = rslt.getLong(39) ;
                ((bool[]) buf[70])[0] = rslt.wasNull(39);
                ((int[]) buf[71])[0] = rslt.getInt(40) ;
                ((int[]) buf[72])[0] = rslt.getInt(41) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(6) ;
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(7) ;
                ((int[]) buf[7])[0] = rslt.getInt(8) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((short[]) buf[6])[0] = rslt.getShort(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((long[]) buf[10])[0] = rslt.getLong(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getVarchar(8) ;
                ((int[]) buf[13])[0] = rslt.getInt(9) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getLongVarchar(5) ;
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[3]);
                }
                stmt.SetParameter(4, (short)parms[4]);
                stmt.SetParameter(5, (String)parms[5]);
                stmt.SetParameter(6, (int)parms[6]);
                stmt.SetParameter(7, (DateTime)parms[7]);
                stmt.SetParameter(8, (DateTime)parms[8]);
                stmt.SetParameter(9, (DateTime)parms[9]);
                stmt.SetParameter(10, (DateTime)parms[10]);
                stmt.SetParameter(11, (DateTime)parms[11]);
                stmt.SetParameter(12, (DateTime)parms[12]);
                stmt.SetParameter(13, (DateTime)parms[13]);
                stmt.SetParameter(14, (decimal)parms[14]);
                stmt.SetParameter(15, (int)parms[15]);
                stmt.SetParameter(16, (String)parms[16]);
                stmt.SetParameter(17, (short)parms[17]);
                stmt.SetParameter(18, (bool)parms[18]);
                stmt.SetParameter(19, (short)parms[19]);
                stmt.SetParameter(20, (decimal)parms[20]);
                stmt.SetParameter(21, (String)parms[21]);
                stmt.SetParameter(22, (decimal)parms[22]);
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 23 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(23, (int)parms[24]);
                }
                if ( (bool)parms[25] )
                {
                   stmt.setNull( 24 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(24, (bool)parms[26]);
                }
                if ( (bool)parms[27] )
                {
                   stmt.setNull( 25 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(25, (String)parms[28]);
                }
                if ( (bool)parms[29] )
                {
                   stmt.setNull( 26 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(26, (short)parms[30]);
                }
                if ( (bool)parms[31] )
                {
                   stmt.setNull( 27 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(27, (short)parms[32]);
                }
                if ( (bool)parms[33] )
                {
                   stmt.setNull( 28 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(28, (decimal)parms[34]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(3, (decimal)parms[3]);
                }
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(3, (long)parms[3]);
                }
                stmt.SetParameter(4, (decimal)parms[4]);
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[6]);
                }
                stmt.SetParameter(6, (String)parms[7]);
                stmt.SetParameter(7, (bool)parms[8]);
                stmt.SetParameter(8, (String)parms[9]);
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 9 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(9, (String)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 10 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(10, (bool)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 11 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(11, (short)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 12 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(12, (short)parms[17]);
                }
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 13 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(13, (short)parms[19]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 14 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(14, (short)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 15 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(15, (bool)parms[23]);
                }
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 16 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(16, (decimal)parms[25]);
                }
                stmt.SetParameter(17, (int)parms[26]);
                if ( (bool)parms[27] )
                {
                   stmt.setNull( 18 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(18, (bool)parms[28]);
                }
                if ( (bool)parms[29] )
                {
                   stmt.setNull( 19 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(19, (short)parms[30]);
                }
                if ( (bool)parms[31] )
                {
                   stmt.setNull( 20 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(20, (String)parms[32]);
                }
                if ( (bool)parms[33] )
                {
                   stmt.setNull( 21 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(21, (String)parms[34]);
                }
                if ( (bool)parms[35] )
                {
                   stmt.setNull( 22 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(22, (String)parms[36]);
                }
                if ( (bool)parms[37] )
                {
                   stmt.setNull( 23 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(23, (decimal)parms[38]);
                }
                if ( (bool)parms[39] )
                {
                   stmt.setNull( 24 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(24, (decimal)parms[40]);
                }
                if ( (bool)parms[41] )
                {
                   stmt.setNull( 25 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(25, (bool)parms[42]);
                }
                if ( (bool)parms[43] )
                {
                   stmt.setNull( 26 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(26, (String)parms[44]);
                }
                if ( (bool)parms[45] )
                {
                   stmt.setNull( 27 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(27, (decimal)parms[46]);
                }
                if ( (bool)parms[47] )
                {
                   stmt.setNull( 28 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(28, (int)parms[48]);
                }
                if ( (bool)parms[49] )
                {
                   stmt.setNull( 29 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(29, (int)parms[50]);
                }
                if ( (bool)parms[51] )
                {
                   stmt.setNull( 30 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(30, (int)parms[52]);
                }
                if ( (bool)parms[53] )
                {
                   stmt.setNull( 31 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(31, (short)parms[54]);
                }
                if ( (bool)parms[55] )
                {
                   stmt.setNull( 32 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(32, (short)parms[56]);
                }
                if ( (bool)parms[57] )
                {
                   stmt.setNull( 33 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(33, (short)parms[58]);
                }
                if ( (bool)parms[59] )
                {
                   stmt.setNull( 34 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(34, (short)parms[60]);
                }
                if ( (bool)parms[61] )
                {
                   stmt.setNull( 35 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(35, (String)parms[62]);
                }
                if ( (bool)parms[63] )
                {
                   stmt.setNull( 36 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(36, (decimal)parms[64]);
                }
                if ( (bool)parms[65] )
                {
                   stmt.setNull( 37 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(37, (short)parms[66]);
                }
                if ( (bool)parms[67] )
                {
                   stmt.setNull( 38 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(38, (String)parms[68]);
                }
                if ( (bool)parms[69] )
                {
                   stmt.setNull( 39 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(39, (short)parms[70]);
                }
                stmt.SetParameter(40, (bool)parms[71]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (decimal)parms[2]);
                stmt.SetParameter(4, (DateTime)parms[3]);
                stmt.SetParameter(5, (decimal)parms[4]);
                stmt.SetParameter(6, (DateTime)parms[5]);
                stmt.SetParameter(7, (decimal)parms[6]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(3, (long)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(5, (short)parms[7]);
                }
                stmt.SetParameter(6, (DateTime)parms[8]);
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(7, (DateTime)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 8 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(8, (DateTime)parms[12]);
                }
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(5, (DateTime)parms[6]);
                }
                return;
       }
    }

 }

}
