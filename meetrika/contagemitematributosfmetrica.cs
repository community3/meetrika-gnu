/*
               File: ContagemItemAtributosFMetrica
        Description: Contagem Item Atributos FMetrica
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:19:35.11
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagemitematributosfmetrica : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_2") == 0 )
         {
            A261ContagemItemAtributosFMetrica_ItemContagemLan = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A261ContagemItemAtributosFMetrica_ItemContagemLan", StringUtil.LTrim( StringUtil.Str( (decimal)(A261ContagemItemAtributosFMetrica_ItemContagemLan), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_2( A261ContagemItemAtributosFMetrica_ItemContagemLan) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_3") == 0 )
         {
            A249ContagemItem_FMetricasAtributosCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A249ContagemItem_FMetricasAtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A249ContagemItem_FMetricasAtributosCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_3( A249ContagemItem_FMetricasAtributosCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_4") == 0 )
         {
            A253ContagemItem_FMetricasAtributosTabCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n253ContagemItem_FMetricasAtributosTabCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A253ContagemItem_FMetricasAtributosTabCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A253ContagemItem_FMetricasAtributosTabCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_4( A253ContagemItem_FMetricasAtributosTabCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Contagem Item Atributos FMetrica", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtContagemItemAtributosFMetrica_ItemContagemLan_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contagemitematributosfmetrica( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contagemitematributosfmetrica( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_1A47( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_1A47e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_1A47( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_1A47( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_1A47e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroupdata_Internalname, "Contagem Item Atributos FMetrica", 1, 0, "px", 0, "px", "Group", " "+"style=\"-moz-border-radius: 3pt;;\""+" ", "HLP_ContagemItemAtributosFMetrica.htm");
            wb_table3_28_1A47( true) ;
         }
         return  ;
      }

      protected void wb_table3_28_1A47e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_1A47e( true) ;
         }
         else
         {
            wb_table1_2_1A47e( false) ;
         }
      }

      protected void wb_table3_28_1A47( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_34_1A47( true) ;
         }
         return  ;
      }

      protected void wb_table4_34_1A47e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemItemAtributosFMetrica.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemItemAtributosFMetrica.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemItemAtributosFMetrica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_1A47e( true) ;
         }
         else
         {
            wb_table3_28_1A47e( false) ;
         }
      }

      protected void wb_table4_34_1A47( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Container", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitematributosfmetrica_itemcontagemlan_Internalname, "Contagem Lan", "", "", lblTextblockcontagemitematributosfmetrica_itemcontagemlan_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemItemAtributosFMetrica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemItemAtributosFMetrica_ItemContagemLan_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A261ContagemItemAtributosFMetrica_ItemContagemLan), 6, 0, ",", "")), ((edtContagemItemAtributosFMetrica_ItemContagemLan_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A261ContagemItemAtributosFMetrica_ItemContagemLan), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A261ContagemItemAtributosFMetrica_ItemContagemLan), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItemAtributosFMetrica_ItemContagemLan_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemItemAtributosFMetrica_ItemContagemLan_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemItemAtributosFMetrica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_fmetricasatributoscod_Internalname, "Atributo", "", "", lblTextblockcontagemitem_fmetricasatributoscod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemItemAtributosFMetrica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemItem_FMetricasAtributosCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A249ContagemItem_FMetricasAtributosCod), 6, 0, ",", "")), ((edtContagemItem_FMetricasAtributosCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A249ContagemItem_FMetricasAtributosCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A249ContagemItem_FMetricasAtributosCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_FMetricasAtributosCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemItem_FMetricasAtributosCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemItemAtributosFMetrica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_fmetricasatributosnom_Internalname, "Atributo", "", "", lblTextblockcontagemitem_fmetricasatributosnom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemItemAtributosFMetrica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItem_FMetricasAtributosNom_Internalname, StringUtil.RTrim( A250ContagemItem_FMetricasAtributosNom), StringUtil.RTrim( context.localUtil.Format( A250ContagemItem_FMetricasAtributosNom, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_FMetricasAtributosNom_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemItem_FMetricasAtributosNom_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ContagemItemAtributosFMetrica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_fmetricasatributostabcod_Internalname, "Tab Cod", "", "", lblTextblockcontagemitem_fmetricasatributostabcod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemItemAtributosFMetrica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItem_FMetricasAtributosTabCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A253ContagemItem_FMetricasAtributosTabCod), 6, 0, ",", "")), ((edtContagemItem_FMetricasAtributosTabCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A253ContagemItem_FMetricasAtributosTabCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A253ContagemItem_FMetricasAtributosTabCod), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_FMetricasAtributosTabCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemItem_FMetricasAtributosTabCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemItemAtributosFMetrica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_fmetricasatributostabnom_Internalname, "Tab Nom", "", "", lblTextblockcontagemitem_fmetricasatributostabnom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemItemAtributosFMetrica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItem_FMetricasAtributosTabNom_Internalname, StringUtil.RTrim( A254ContagemItem_FMetricasAtributosTabNom), StringUtil.RTrim( context.localUtil.Format( A254ContagemItem_FMetricasAtributosTabNom, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_FMetricasAtributosTabNom_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemItem_FMetricasAtributosTabNom_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ContagemItemAtributosFMetrica.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_1A47e( true) ;
         }
         else
         {
            wb_table4_34_1A47e( false) ;
         }
      }

      protected void wb_table2_5_1A47( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSectiontoolbar_Internalname, 1, 0, "px", 0, "px", "ToolbarMain", "left", "top", "", "WHITE-SPACE: nowrap;", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_Internalname, context.GetImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_Visible, 1, "", "Primeiro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemItemAtributosFMetrica.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemItemAtributosFMetrica.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_Internalname, context.GetImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemItemAtributosFMetrica.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemItemAtributosFMetrica.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_Internalname, context.GetImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_Visible, 1, "", "Pr�ximo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemItemAtributosFMetrica.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemItemAtributosFMetrica.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_Internalname, context.GetImagePath( "29211874-e613-48e5-9011-8017d984217e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_Visible, 1, "", "�ltimo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemItemAtributosFMetrica.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemItemAtributosFMetrica.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_Internalname, context.GetImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_Visible, 1, "", "Selecionar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemItemAtributosFMetrica.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemItemAtributosFMetrica.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_Internalname, context.GetImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemItemAtributosFMetrica.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemItemAtributosFMetrica.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_Internalname, context.GetImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_Visible, 1, "", "Fechar", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemItemAtributosFMetrica.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemItemAtributosFMetrica.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_Internalname, context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemItemAtributosFMetrica.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemItemAtributosFMetrica.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_1A47e( true) ;
         }
         else
         {
            wb_table2_5_1A47e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemItemAtributosFMetrica_ItemContagemLan_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemItemAtributosFMetrica_ItemContagemLan_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMITEMATRIBUTOSFMETRICA_ITEMCONTAGEMLAN");
                  AnyError = 1;
                  GX_FocusControl = edtContagemItemAtributosFMetrica_ItemContagemLan_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A261ContagemItemAtributosFMetrica_ItemContagemLan = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A261ContagemItemAtributosFMetrica_ItemContagemLan", StringUtil.LTrim( StringUtil.Str( (decimal)(A261ContagemItemAtributosFMetrica_ItemContagemLan), 6, 0)));
               }
               else
               {
                  A261ContagemItemAtributosFMetrica_ItemContagemLan = (int)(context.localUtil.CToN( cgiGet( edtContagemItemAtributosFMetrica_ItemContagemLan_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A261ContagemItemAtributosFMetrica_ItemContagemLan", StringUtil.LTrim( StringUtil.Str( (decimal)(A261ContagemItemAtributosFMetrica_ItemContagemLan), 6, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemItem_FMetricasAtributosCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemItem_FMetricasAtributosCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMITEM_FMETRICASATRIBUTOSCOD");
                  AnyError = 1;
                  GX_FocusControl = edtContagemItem_FMetricasAtributosCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A249ContagemItem_FMetricasAtributosCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A249ContagemItem_FMetricasAtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A249ContagemItem_FMetricasAtributosCod), 6, 0)));
               }
               else
               {
                  A249ContagemItem_FMetricasAtributosCod = (int)(context.localUtil.CToN( cgiGet( edtContagemItem_FMetricasAtributosCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A249ContagemItem_FMetricasAtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A249ContagemItem_FMetricasAtributosCod), 6, 0)));
               }
               A250ContagemItem_FMetricasAtributosNom = StringUtil.Upper( cgiGet( edtContagemItem_FMetricasAtributosNom_Internalname));
               n250ContagemItem_FMetricasAtributosNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A250ContagemItem_FMetricasAtributosNom", A250ContagemItem_FMetricasAtributosNom);
               A253ContagemItem_FMetricasAtributosTabCod = (int)(context.localUtil.CToN( cgiGet( edtContagemItem_FMetricasAtributosTabCod_Internalname), ",", "."));
               n253ContagemItem_FMetricasAtributosTabCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A253ContagemItem_FMetricasAtributosTabCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A253ContagemItem_FMetricasAtributosTabCod), 6, 0)));
               A254ContagemItem_FMetricasAtributosTabNom = StringUtil.Upper( cgiGet( edtContagemItem_FMetricasAtributosTabNom_Internalname));
               n254ContagemItem_FMetricasAtributosTabNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A254ContagemItem_FMetricasAtributosTabNom", A254ContagemItem_FMetricasAtributosTabNom);
               /* Read saved values. */
               Z261ContagemItemAtributosFMetrica_ItemContagemLan = (int)(context.localUtil.CToN( cgiGet( "Z261ContagemItemAtributosFMetrica_ItemContagemLan"), ",", "."));
               Z249ContagemItem_FMetricasAtributosCod = (int)(context.localUtil.CToN( cgiGet( "Z249ContagemItem_FMetricasAtributosCod"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A261ContagemItemAtributosFMetrica_ItemContagemLan = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A261ContagemItemAtributosFMetrica_ItemContagemLan", StringUtil.LTrim( StringUtil.Str( (decimal)(A261ContagemItemAtributosFMetrica_ItemContagemLan), 6, 0)));
                  A249ContagemItem_FMetricasAtributosCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A249ContagemItem_FMetricasAtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A249ContagemItem_FMetricasAtributosCod), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll1A47( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         imgBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_Visible), 5, 0)));
         imgBtn_first_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_separator_Visible), 5, 0)));
         imgBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_Visible), 5, 0)));
         imgBtn_previous_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_separator_Visible), 5, 0)));
         imgBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_Visible), 5, 0)));
         imgBtn_next_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_separator_Visible), 5, 0)));
         imgBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_Visible), 5, 0)));
         imgBtn_last_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_separator_Visible), 5, 0)));
         imgBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_Visible), 5, 0)));
         imgBtn_select_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_separator_Visible), 5, 0)));
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Visible), 5, 0)));
            imgBtn_enter2_separator_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_separator_Visible), 5, 0)));
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)));
         }
         DisableAttributes1A47( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption1A0( )
      {
      }

      protected void ZM1A47( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
            }
            else
            {
            }
         }
         if ( GX_JID == -1 )
         {
            Z261ContagemItemAtributosFMetrica_ItemContagemLan = A261ContagemItemAtributosFMetrica_ItemContagemLan;
            Z249ContagemItem_FMetricasAtributosCod = A249ContagemItem_FMetricasAtributosCod;
            Z250ContagemItem_FMetricasAtributosNom = A250ContagemItem_FMetricasAtributosNom;
            Z253ContagemItem_FMetricasAtributosTabCod = A253ContagemItem_FMetricasAtributosTabCod;
            Z254ContagemItem_FMetricasAtributosTabNom = A254ContagemItem_FMetricasAtributosTabNom;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_delete2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_enter2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
      }

      protected void Load1A47( )
      {
         /* Using cursor T001A7 */
         pr_default.execute(5, new Object[] {A261ContagemItemAtributosFMetrica_ItemContagemLan, A249ContagemItem_FMetricasAtributosCod});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound47 = 1;
            A250ContagemItem_FMetricasAtributosNom = T001A7_A250ContagemItem_FMetricasAtributosNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A250ContagemItem_FMetricasAtributosNom", A250ContagemItem_FMetricasAtributosNom);
            n250ContagemItem_FMetricasAtributosNom = T001A7_n250ContagemItem_FMetricasAtributosNom[0];
            A254ContagemItem_FMetricasAtributosTabNom = T001A7_A254ContagemItem_FMetricasAtributosTabNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A254ContagemItem_FMetricasAtributosTabNom", A254ContagemItem_FMetricasAtributosTabNom);
            n254ContagemItem_FMetricasAtributosTabNom = T001A7_n254ContagemItem_FMetricasAtributosTabNom[0];
            A253ContagemItem_FMetricasAtributosTabCod = T001A7_A253ContagemItem_FMetricasAtributosTabCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A253ContagemItem_FMetricasAtributosTabCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A253ContagemItem_FMetricasAtributosTabCod), 6, 0)));
            n253ContagemItem_FMetricasAtributosTabCod = T001A7_n253ContagemItem_FMetricasAtributosTabCod[0];
            ZM1A47( -1) ;
         }
         pr_default.close(5);
         OnLoadActions1A47( ) ;
      }

      protected void OnLoadActions1A47( )
      {
      }

      protected void CheckExtendedTable1A47( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T001A4 */
         pr_default.execute(2, new Object[] {A261ContagemItemAtributosFMetrica_ItemContagemLan});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contagem Item Atributos FMetrica_Item Contagem'.", "ForeignKeyNotFound", 1, "CONTAGEMITEMATRIBUTOSFMETRICA_ITEMCONTAGEMLAN");
            AnyError = 1;
            GX_FocusControl = edtContagemItemAtributosFMetrica_ItemContagemLan_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(2);
         /* Using cursor T001A5 */
         pr_default.execute(3, new Object[] {A249ContagemItem_FMetricasAtributosCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contagem Item_FMetricas Atributos'.", "ForeignKeyNotFound", 1, "CONTAGEMITEM_FMETRICASATRIBUTOSCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemItem_FMetricasAtributosCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A250ContagemItem_FMetricasAtributosNom = T001A5_A250ContagemItem_FMetricasAtributosNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A250ContagemItem_FMetricasAtributosNom", A250ContagemItem_FMetricasAtributosNom);
         n250ContagemItem_FMetricasAtributosNom = T001A5_n250ContagemItem_FMetricasAtributosNom[0];
         A253ContagemItem_FMetricasAtributosTabCod = T001A5_A253ContagemItem_FMetricasAtributosTabCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A253ContagemItem_FMetricasAtributosTabCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A253ContagemItem_FMetricasAtributosTabCod), 6, 0)));
         n253ContagemItem_FMetricasAtributosTabCod = T001A5_n253ContagemItem_FMetricasAtributosTabCod[0];
         pr_default.close(3);
         /* Using cursor T001A6 */
         pr_default.execute(4, new Object[] {n253ContagemItem_FMetricasAtributosTabCod, A253ContagemItem_FMetricasAtributosTabCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Tabela'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A254ContagemItem_FMetricasAtributosTabNom = T001A6_A254ContagemItem_FMetricasAtributosTabNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A254ContagemItem_FMetricasAtributosTabNom", A254ContagemItem_FMetricasAtributosTabNom);
         n254ContagemItem_FMetricasAtributosTabNom = T001A6_n254ContagemItem_FMetricasAtributosTabNom[0];
         pr_default.close(4);
      }

      protected void CloseExtendedTableCursors1A47( )
      {
         pr_default.close(2);
         pr_default.close(3);
         pr_default.close(4);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_2( int A261ContagemItemAtributosFMetrica_ItemContagemLan )
      {
         /* Using cursor T001A8 */
         pr_default.execute(6, new Object[] {A261ContagemItemAtributosFMetrica_ItemContagemLan});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contagem Item Atributos FMetrica_Item Contagem'.", "ForeignKeyNotFound", 1, "CONTAGEMITEMATRIBUTOSFMETRICA_ITEMCONTAGEMLAN");
            AnyError = 1;
            GX_FocusControl = edtContagemItemAtributosFMetrica_ItemContagemLan_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void gxLoad_3( int A249ContagemItem_FMetricasAtributosCod )
      {
         /* Using cursor T001A9 */
         pr_default.execute(7, new Object[] {A249ContagemItem_FMetricasAtributosCod});
         if ( (pr_default.getStatus(7) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contagem Item_FMetricas Atributos'.", "ForeignKeyNotFound", 1, "CONTAGEMITEM_FMETRICASATRIBUTOSCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemItem_FMetricasAtributosCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A250ContagemItem_FMetricasAtributosNom = T001A9_A250ContagemItem_FMetricasAtributosNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A250ContagemItem_FMetricasAtributosNom", A250ContagemItem_FMetricasAtributosNom);
         n250ContagemItem_FMetricasAtributosNom = T001A9_n250ContagemItem_FMetricasAtributosNom[0];
         A253ContagemItem_FMetricasAtributosTabCod = T001A9_A253ContagemItem_FMetricasAtributosTabCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A253ContagemItem_FMetricasAtributosTabCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A253ContagemItem_FMetricasAtributosTabCod), 6, 0)));
         n253ContagemItem_FMetricasAtributosTabCod = T001A9_n253ContagemItem_FMetricasAtributosTabCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A250ContagemItem_FMetricasAtributosNom))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A253ContagemItem_FMetricasAtributosTabCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(7) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(7);
      }

      protected void gxLoad_4( int A253ContagemItem_FMetricasAtributosTabCod )
      {
         /* Using cursor T001A10 */
         pr_default.execute(8, new Object[] {n253ContagemItem_FMetricasAtributosTabCod, A253ContagemItem_FMetricasAtributosTabCod});
         if ( (pr_default.getStatus(8) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Tabela'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A254ContagemItem_FMetricasAtributosTabNom = T001A10_A254ContagemItem_FMetricasAtributosTabNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A254ContagemItem_FMetricasAtributosTabNom", A254ContagemItem_FMetricasAtributosTabNom);
         n254ContagemItem_FMetricasAtributosTabNom = T001A10_n254ContagemItem_FMetricasAtributosTabNom[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A254ContagemItem_FMetricasAtributosTabNom))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(8) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(8);
      }

      protected void GetKey1A47( )
      {
         /* Using cursor T001A11 */
         pr_default.execute(9, new Object[] {A261ContagemItemAtributosFMetrica_ItemContagemLan, A249ContagemItem_FMetricasAtributosCod});
         if ( (pr_default.getStatus(9) != 101) )
         {
            RcdFound47 = 1;
         }
         else
         {
            RcdFound47 = 0;
         }
         pr_default.close(9);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T001A3 */
         pr_default.execute(1, new Object[] {A261ContagemItemAtributosFMetrica_ItemContagemLan, A249ContagemItem_FMetricasAtributosCod});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM1A47( 1) ;
            RcdFound47 = 1;
            A261ContagemItemAtributosFMetrica_ItemContagemLan = T001A3_A261ContagemItemAtributosFMetrica_ItemContagemLan[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A261ContagemItemAtributosFMetrica_ItemContagemLan", StringUtil.LTrim( StringUtil.Str( (decimal)(A261ContagemItemAtributosFMetrica_ItemContagemLan), 6, 0)));
            A249ContagemItem_FMetricasAtributosCod = T001A3_A249ContagemItem_FMetricasAtributosCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A249ContagemItem_FMetricasAtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A249ContagemItem_FMetricasAtributosCod), 6, 0)));
            Z261ContagemItemAtributosFMetrica_ItemContagemLan = A261ContagemItemAtributosFMetrica_ItemContagemLan;
            Z249ContagemItem_FMetricasAtributosCod = A249ContagemItem_FMetricasAtributosCod;
            sMode47 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load1A47( ) ;
            if ( AnyError == 1 )
            {
               RcdFound47 = 0;
               InitializeNonKey1A47( ) ;
            }
            Gx_mode = sMode47;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound47 = 0;
            InitializeNonKey1A47( ) ;
            sMode47 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode47;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey1A47( ) ;
         if ( RcdFound47 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound47 = 0;
         /* Using cursor T001A12 */
         pr_default.execute(10, new Object[] {A261ContagemItemAtributosFMetrica_ItemContagemLan, A249ContagemItem_FMetricasAtributosCod});
         if ( (pr_default.getStatus(10) != 101) )
         {
            while ( (pr_default.getStatus(10) != 101) && ( ( T001A12_A261ContagemItemAtributosFMetrica_ItemContagemLan[0] < A261ContagemItemAtributosFMetrica_ItemContagemLan ) || ( T001A12_A261ContagemItemAtributosFMetrica_ItemContagemLan[0] == A261ContagemItemAtributosFMetrica_ItemContagemLan ) && ( T001A12_A249ContagemItem_FMetricasAtributosCod[0] < A249ContagemItem_FMetricasAtributosCod ) ) )
            {
               pr_default.readNext(10);
            }
            if ( (pr_default.getStatus(10) != 101) && ( ( T001A12_A261ContagemItemAtributosFMetrica_ItemContagemLan[0] > A261ContagemItemAtributosFMetrica_ItemContagemLan ) || ( T001A12_A261ContagemItemAtributosFMetrica_ItemContagemLan[0] == A261ContagemItemAtributosFMetrica_ItemContagemLan ) && ( T001A12_A249ContagemItem_FMetricasAtributosCod[0] > A249ContagemItem_FMetricasAtributosCod ) ) )
            {
               A261ContagemItemAtributosFMetrica_ItemContagemLan = T001A12_A261ContagemItemAtributosFMetrica_ItemContagemLan[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A261ContagemItemAtributosFMetrica_ItemContagemLan", StringUtil.LTrim( StringUtil.Str( (decimal)(A261ContagemItemAtributosFMetrica_ItemContagemLan), 6, 0)));
               A249ContagemItem_FMetricasAtributosCod = T001A12_A249ContagemItem_FMetricasAtributosCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A249ContagemItem_FMetricasAtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A249ContagemItem_FMetricasAtributosCod), 6, 0)));
               RcdFound47 = 1;
            }
         }
         pr_default.close(10);
      }

      protected void move_previous( )
      {
         RcdFound47 = 0;
         /* Using cursor T001A13 */
         pr_default.execute(11, new Object[] {A261ContagemItemAtributosFMetrica_ItemContagemLan, A249ContagemItem_FMetricasAtributosCod});
         if ( (pr_default.getStatus(11) != 101) )
         {
            while ( (pr_default.getStatus(11) != 101) && ( ( T001A13_A261ContagemItemAtributosFMetrica_ItemContagemLan[0] > A261ContagemItemAtributosFMetrica_ItemContagemLan ) || ( T001A13_A261ContagemItemAtributosFMetrica_ItemContagemLan[0] == A261ContagemItemAtributosFMetrica_ItemContagemLan ) && ( T001A13_A249ContagemItem_FMetricasAtributosCod[0] > A249ContagemItem_FMetricasAtributosCod ) ) )
            {
               pr_default.readNext(11);
            }
            if ( (pr_default.getStatus(11) != 101) && ( ( T001A13_A261ContagemItemAtributosFMetrica_ItemContagemLan[0] < A261ContagemItemAtributosFMetrica_ItemContagemLan ) || ( T001A13_A261ContagemItemAtributosFMetrica_ItemContagemLan[0] == A261ContagemItemAtributosFMetrica_ItemContagemLan ) && ( T001A13_A249ContagemItem_FMetricasAtributosCod[0] < A249ContagemItem_FMetricasAtributosCod ) ) )
            {
               A261ContagemItemAtributosFMetrica_ItemContagemLan = T001A13_A261ContagemItemAtributosFMetrica_ItemContagemLan[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A261ContagemItemAtributosFMetrica_ItemContagemLan", StringUtil.LTrim( StringUtil.Str( (decimal)(A261ContagemItemAtributosFMetrica_ItemContagemLan), 6, 0)));
               A249ContagemItem_FMetricasAtributosCod = T001A13_A249ContagemItem_FMetricasAtributosCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A249ContagemItem_FMetricasAtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A249ContagemItem_FMetricasAtributosCod), 6, 0)));
               RcdFound47 = 1;
            }
         }
         pr_default.close(11);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey1A47( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtContagemItemAtributosFMetrica_ItemContagemLan_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert1A47( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound47 == 1 )
            {
               if ( ( A261ContagemItemAtributosFMetrica_ItemContagemLan != Z261ContagemItemAtributosFMetrica_ItemContagemLan ) || ( A249ContagemItem_FMetricasAtributosCod != Z249ContagemItem_FMetricasAtributosCod ) )
               {
                  A261ContagemItemAtributosFMetrica_ItemContagemLan = Z261ContagemItemAtributosFMetrica_ItemContagemLan;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A261ContagemItemAtributosFMetrica_ItemContagemLan", StringUtil.LTrim( StringUtil.Str( (decimal)(A261ContagemItemAtributosFMetrica_ItemContagemLan), 6, 0)));
                  A249ContagemItem_FMetricasAtributosCod = Z249ContagemItem_FMetricasAtributosCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A249ContagemItem_FMetricasAtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A249ContagemItem_FMetricasAtributosCod), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CONTAGEMITEMATRIBUTOSFMETRICA_ITEMCONTAGEMLAN");
                  AnyError = 1;
                  GX_FocusControl = edtContagemItemAtributosFMetrica_ItemContagemLan_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtContagemItemAtributosFMetrica_ItemContagemLan_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update1A47( ) ;
                  GX_FocusControl = edtContagemItemAtributosFMetrica_ItemContagemLan_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( ( A261ContagemItemAtributosFMetrica_ItemContagemLan != Z261ContagemItemAtributosFMetrica_ItemContagemLan ) || ( A249ContagemItem_FMetricasAtributosCod != Z249ContagemItem_FMetricasAtributosCod ) )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtContagemItemAtributosFMetrica_ItemContagemLan_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert1A47( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CONTAGEMITEMATRIBUTOSFMETRICA_ITEMCONTAGEMLAN");
                     AnyError = 1;
                     GX_FocusControl = edtContagemItemAtributosFMetrica_ItemContagemLan_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtContagemItemAtributosFMetrica_ItemContagemLan_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert1A47( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( ( A261ContagemItemAtributosFMetrica_ItemContagemLan != Z261ContagemItemAtributosFMetrica_ItemContagemLan ) || ( A249ContagemItem_FMetricasAtributosCod != Z249ContagemItem_FMetricasAtributosCod ) )
         {
            A261ContagemItemAtributosFMetrica_ItemContagemLan = Z261ContagemItemAtributosFMetrica_ItemContagemLan;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A261ContagemItemAtributosFMetrica_ItemContagemLan", StringUtil.LTrim( StringUtil.Str( (decimal)(A261ContagemItemAtributosFMetrica_ItemContagemLan), 6, 0)));
            A249ContagemItem_FMetricasAtributosCod = Z249ContagemItem_FMetricasAtributosCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A249ContagemItem_FMetricasAtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A249ContagemItem_FMetricasAtributosCod), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CONTAGEMITEMATRIBUTOSFMETRICA_ITEMCONTAGEMLAN");
            AnyError = 1;
            GX_FocusControl = edtContagemItemAtributosFMetrica_ItemContagemLan_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtContagemItemAtributosFMetrica_ItemContagemLan_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound47 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "CONTAGEMITEMATRIBUTOSFMETRICA_ITEMCONTAGEMLAN");
            AnyError = 1;
            GX_FocusControl = edtContagemItemAtributosFMetrica_ItemContagemLan_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart1A47( ) ;
         if ( RcdFound47 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         ScanEnd1A47( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound47 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound47 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart1A47( ) ;
         if ( RcdFound47 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound47 != 0 )
            {
               ScanNext1A47( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         ScanEnd1A47( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency1A47( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T001A2 */
            pr_default.execute(0, new Object[] {A261ContagemItemAtributosFMetrica_ItemContagemLan, A249ContagemItem_FMetricasAtributosCod});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemItemAtributosFMetrica"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContagemItemAtributosFMetrica"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert1A47( )
      {
         BeforeValidate1A47( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1A47( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM1A47( 0) ;
            CheckOptimisticConcurrency1A47( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1A47( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert1A47( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001A14 */
                     pr_default.execute(12, new Object[] {A261ContagemItemAtributosFMetrica_ItemContagemLan, A249ContagemItem_FMetricasAtributosCod});
                     pr_default.close(12);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemItemAtributosFMetrica") ;
                     if ( (pr_default.getStatus(12) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption1A0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load1A47( ) ;
            }
            EndLevel1A47( ) ;
         }
         CloseExtendedTableCursors1A47( ) ;
      }

      protected void Update1A47( )
      {
         BeforeValidate1A47( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1A47( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1A47( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1A47( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate1A47( ) ;
                  if ( AnyError == 0 )
                  {
                     /* No attributes to update on table [ContagemItemAtributosFMetrica] */
                     DeferredUpdate1A47( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption1A0( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel1A47( ) ;
         }
         CloseExtendedTableCursors1A47( ) ;
      }

      protected void DeferredUpdate1A47( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate1A47( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1A47( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls1A47( ) ;
            AfterConfirm1A47( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete1A47( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T001A15 */
                  pr_default.execute(13, new Object[] {A261ContagemItemAtributosFMetrica_ItemContagemLan, A249ContagemItem_FMetricasAtributosCod});
                  pr_default.close(13);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemItemAtributosFMetrica") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound47 == 0 )
                        {
                           InitAll1A47( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption1A0( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode47 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel1A47( ) ;
         Gx_mode = sMode47;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls1A47( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T001A16 */
            pr_default.execute(14, new Object[] {A249ContagemItem_FMetricasAtributosCod});
            A250ContagemItem_FMetricasAtributosNom = T001A16_A250ContagemItem_FMetricasAtributosNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A250ContagemItem_FMetricasAtributosNom", A250ContagemItem_FMetricasAtributosNom);
            n250ContagemItem_FMetricasAtributosNom = T001A16_n250ContagemItem_FMetricasAtributosNom[0];
            A253ContagemItem_FMetricasAtributosTabCod = T001A16_A253ContagemItem_FMetricasAtributosTabCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A253ContagemItem_FMetricasAtributosTabCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A253ContagemItem_FMetricasAtributosTabCod), 6, 0)));
            n253ContagemItem_FMetricasAtributosTabCod = T001A16_n253ContagemItem_FMetricasAtributosTabCod[0];
            pr_default.close(14);
            /* Using cursor T001A17 */
            pr_default.execute(15, new Object[] {n253ContagemItem_FMetricasAtributosTabCod, A253ContagemItem_FMetricasAtributosTabCod});
            A254ContagemItem_FMetricasAtributosTabNom = T001A17_A254ContagemItem_FMetricasAtributosTabNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A254ContagemItem_FMetricasAtributosTabNom", A254ContagemItem_FMetricasAtributosTabNom);
            n254ContagemItem_FMetricasAtributosTabNom = T001A17_n254ContagemItem_FMetricasAtributosTabNom[0];
            pr_default.close(15);
         }
      }

      protected void EndLevel1A47( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete1A47( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(14);
            pr_default.close(15);
            context.CommitDataStores( "ContagemItemAtributosFMetrica");
            if ( AnyError == 0 )
            {
               ConfirmValues1A0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(14);
            pr_default.close(15);
            context.RollbackDataStores( "ContagemItemAtributosFMetrica");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart1A47( )
      {
         /* Using cursor T001A18 */
         pr_default.execute(16);
         RcdFound47 = 0;
         if ( (pr_default.getStatus(16) != 101) )
         {
            RcdFound47 = 1;
            A261ContagemItemAtributosFMetrica_ItemContagemLan = T001A18_A261ContagemItemAtributosFMetrica_ItemContagemLan[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A261ContagemItemAtributosFMetrica_ItemContagemLan", StringUtil.LTrim( StringUtil.Str( (decimal)(A261ContagemItemAtributosFMetrica_ItemContagemLan), 6, 0)));
            A249ContagemItem_FMetricasAtributosCod = T001A18_A249ContagemItem_FMetricasAtributosCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A249ContagemItem_FMetricasAtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A249ContagemItem_FMetricasAtributosCod), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext1A47( )
      {
         /* Scan next routine */
         pr_default.readNext(16);
         RcdFound47 = 0;
         if ( (pr_default.getStatus(16) != 101) )
         {
            RcdFound47 = 1;
            A261ContagemItemAtributosFMetrica_ItemContagemLan = T001A18_A261ContagemItemAtributosFMetrica_ItemContagemLan[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A261ContagemItemAtributosFMetrica_ItemContagemLan", StringUtil.LTrim( StringUtil.Str( (decimal)(A261ContagemItemAtributosFMetrica_ItemContagemLan), 6, 0)));
            A249ContagemItem_FMetricasAtributosCod = T001A18_A249ContagemItem_FMetricasAtributosCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A249ContagemItem_FMetricasAtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A249ContagemItem_FMetricasAtributosCod), 6, 0)));
         }
      }

      protected void ScanEnd1A47( )
      {
         pr_default.close(16);
      }

      protected void AfterConfirm1A47( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert1A47( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate1A47( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete1A47( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete1A47( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate1A47( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes1A47( )
      {
         edtContagemItemAtributosFMetrica_ItemContagemLan_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItemAtributosFMetrica_ItemContagemLan_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItemAtributosFMetrica_ItemContagemLan_Enabled), 5, 0)));
         edtContagemItem_FMetricasAtributosCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FMetricasAtributosCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FMetricasAtributosCod_Enabled), 5, 0)));
         edtContagemItem_FMetricasAtributosNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FMetricasAtributosNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FMetricasAtributosNom_Enabled), 5, 0)));
         edtContagemItem_FMetricasAtributosTabCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FMetricasAtributosTabCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FMetricasAtributosTabCod_Enabled), 5, 0)));
         edtContagemItem_FMetricasAtributosTabNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_FMetricasAtributosTabNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FMetricasAtributosTabNom_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues1A0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117193644");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contagemitematributosfmetrica.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z261ContagemItemAtributosFMetrica_ItemContagemLan", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z261ContagemItemAtributosFMetrica_ItemContagemLan), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z249ContagemItem_FMetricasAtributosCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z249ContagemItem_FMetricasAtributosCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contagemitematributosfmetrica.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "ContagemItemAtributosFMetrica" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contagem Item Atributos FMetrica" ;
      }

      protected void InitializeNonKey1A47( )
      {
         A250ContagemItem_FMetricasAtributosNom = "";
         n250ContagemItem_FMetricasAtributosNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A250ContagemItem_FMetricasAtributosNom", A250ContagemItem_FMetricasAtributosNom);
         A253ContagemItem_FMetricasAtributosTabCod = 0;
         n253ContagemItem_FMetricasAtributosTabCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A253ContagemItem_FMetricasAtributosTabCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A253ContagemItem_FMetricasAtributosTabCod), 6, 0)));
         A254ContagemItem_FMetricasAtributosTabNom = "";
         n254ContagemItem_FMetricasAtributosTabNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A254ContagemItem_FMetricasAtributosTabNom", A254ContagemItem_FMetricasAtributosTabNom);
      }

      protected void InitAll1A47( )
      {
         A261ContagemItemAtributosFMetrica_ItemContagemLan = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A261ContagemItemAtributosFMetrica_ItemContagemLan", StringUtil.LTrim( StringUtil.Str( (decimal)(A261ContagemItemAtributosFMetrica_ItemContagemLan), 6, 0)));
         A249ContagemItem_FMetricasAtributosCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A249ContagemItem_FMetricasAtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A249ContagemItem_FMetricasAtributosCod), 6, 0)));
         InitializeNonKey1A47( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117193650");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contagemitematributosfmetrica.js", "?20203117193650");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         imgBtn_first_Internalname = "BTN_FIRST";
         imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR";
         imgBtn_previous_Internalname = "BTN_PREVIOUS";
         imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR";
         imgBtn_next_Internalname = "BTN_NEXT";
         imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR";
         imgBtn_last_Internalname = "BTN_LAST";
         imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR";
         imgBtn_select_Internalname = "BTN_SELECT";
         imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR";
         imgBtn_enter2_Internalname = "BTN_ENTER2";
         imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR";
         imgBtn_cancel2_Internalname = "BTN_CANCEL2";
         imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR";
         imgBtn_delete2_Internalname = "BTN_DELETE2";
         imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR";
         divSectiontoolbar_Internalname = "SECTIONTOOLBAR";
         tblTabletoolbar_Internalname = "TABLETOOLBAR";
         lblTextblockcontagemitematributosfmetrica_itemcontagemlan_Internalname = "TEXTBLOCKCONTAGEMITEMATRIBUTOSFMETRICA_ITEMCONTAGEMLAN";
         edtContagemItemAtributosFMetrica_ItemContagemLan_Internalname = "CONTAGEMITEMATRIBUTOSFMETRICA_ITEMCONTAGEMLAN";
         lblTextblockcontagemitem_fmetricasatributoscod_Internalname = "TEXTBLOCKCONTAGEMITEM_FMETRICASATRIBUTOSCOD";
         edtContagemItem_FMetricasAtributosCod_Internalname = "CONTAGEMITEM_FMETRICASATRIBUTOSCOD";
         lblTextblockcontagemitem_fmetricasatributosnom_Internalname = "TEXTBLOCKCONTAGEMITEM_FMETRICASATRIBUTOSNOM";
         edtContagemItem_FMetricasAtributosNom_Internalname = "CONTAGEMITEM_FMETRICASATRIBUTOSNOM";
         lblTextblockcontagemitem_fmetricasatributostabcod_Internalname = "TEXTBLOCKCONTAGEMITEM_FMETRICASATRIBUTOSTABCOD";
         edtContagemItem_FMetricasAtributosTabCod_Internalname = "CONTAGEMITEM_FMETRICASATRIBUTOSTABCOD";
         lblTextblockcontagemitem_fmetricasatributostabnom_Internalname = "TEXTBLOCKCONTAGEMITEM_FMETRICASATRIBUTOSTABNOM";
         edtContagemItem_FMetricasAtributosTabNom_Internalname = "CONTAGEMITEM_FMETRICASATRIBUTOSTABNOM";
         tblTable2_Internalname = "TABLE2";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         tblTable1_Internalname = "TABLE1";
         grpGroupdata_Internalname = "GROUPDATA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Contagem Item Atributos FMetrica";
         imgBtn_delete2_separator_Visible = 1;
         imgBtn_delete2_Enabled = 1;
         imgBtn_delete2_Visible = 1;
         imgBtn_cancel2_separator_Visible = 1;
         imgBtn_cancel2_Visible = 1;
         imgBtn_enter2_separator_Visible = 1;
         imgBtn_enter2_Enabled = 1;
         imgBtn_enter2_Visible = 1;
         imgBtn_select_separator_Visible = 1;
         imgBtn_select_Visible = 1;
         imgBtn_last_separator_Visible = 1;
         imgBtn_last_Visible = 1;
         imgBtn_next_separator_Visible = 1;
         imgBtn_next_Visible = 1;
         imgBtn_previous_separator_Visible = 1;
         imgBtn_previous_Visible = 1;
         imgBtn_first_separator_Visible = 1;
         imgBtn_first_Visible = 1;
         edtContagemItem_FMetricasAtributosTabNom_Jsonclick = "";
         edtContagemItem_FMetricasAtributosTabNom_Enabled = 0;
         edtContagemItem_FMetricasAtributosTabCod_Jsonclick = "";
         edtContagemItem_FMetricasAtributosTabCod_Enabled = 0;
         edtContagemItem_FMetricasAtributosNom_Jsonclick = "";
         edtContagemItem_FMetricasAtributosNom_Enabled = 0;
         edtContagemItem_FMetricasAtributosCod_Jsonclick = "";
         edtContagemItem_FMetricasAtributosCod_Enabled = 1;
         edtContagemItemAtributosFMetrica_ItemContagemLan_Jsonclick = "";
         edtContagemItemAtributosFMetrica_ItemContagemLan_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         /* Using cursor T001A19 */
         pr_default.execute(17, new Object[] {A261ContagemItemAtributosFMetrica_ItemContagemLan});
         if ( (pr_default.getStatus(17) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contagem Item Atributos FMetrica_Item Contagem'.", "ForeignKeyNotFound", 1, "CONTAGEMITEMATRIBUTOSFMETRICA_ITEMCONTAGEMLAN");
            AnyError = 1;
            GX_FocusControl = edtContagemItemAtributosFMetrica_ItemContagemLan_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(17);
         /* Using cursor T001A16 */
         pr_default.execute(14, new Object[] {A249ContagemItem_FMetricasAtributosCod});
         if ( (pr_default.getStatus(14) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contagem Item_FMetricas Atributos'.", "ForeignKeyNotFound", 1, "CONTAGEMITEM_FMETRICASATRIBUTOSCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemItem_FMetricasAtributosCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A250ContagemItem_FMetricasAtributosNom = T001A16_A250ContagemItem_FMetricasAtributosNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A250ContagemItem_FMetricasAtributosNom", A250ContagemItem_FMetricasAtributosNom);
         n250ContagemItem_FMetricasAtributosNom = T001A16_n250ContagemItem_FMetricasAtributosNom[0];
         A253ContagemItem_FMetricasAtributosTabCod = T001A16_A253ContagemItem_FMetricasAtributosTabCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A253ContagemItem_FMetricasAtributosTabCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A253ContagemItem_FMetricasAtributosTabCod), 6, 0)));
         n253ContagemItem_FMetricasAtributosTabCod = T001A16_n253ContagemItem_FMetricasAtributosTabCod[0];
         pr_default.close(14);
         /* Using cursor T001A17 */
         pr_default.execute(15, new Object[] {n253ContagemItem_FMetricasAtributosTabCod, A253ContagemItem_FMetricasAtributosTabCod});
         if ( (pr_default.getStatus(15) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Tabela'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A254ContagemItem_FMetricasAtributosTabNom = T001A17_A254ContagemItem_FMetricasAtributosTabNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A254ContagemItem_FMetricasAtributosTabNom", A254ContagemItem_FMetricasAtributosTabNom);
         n254ContagemItem_FMetricasAtributosTabNom = T001A17_n254ContagemItem_FMetricasAtributosTabNom[0];
         pr_default.close(15);
         if ( AnyError == 0 )
         {
            GX_FocusControl = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Contagemitematributosfmetrica_itemcontagemlan( int GX_Parm1 )
      {
         A261ContagemItemAtributosFMetrica_ItemContagemLan = GX_Parm1;
         /* Using cursor T001A19 */
         pr_default.execute(17, new Object[] {A261ContagemItemAtributosFMetrica_ItemContagemLan});
         if ( (pr_default.getStatus(17) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contagem Item Atributos FMetrica_Item Contagem'.", "ForeignKeyNotFound", 1, "CONTAGEMITEMATRIBUTOSFMETRICA_ITEMCONTAGEMLAN");
            AnyError = 1;
            GX_FocusControl = edtContagemItemAtributosFMetrica_ItemContagemLan_Internalname;
         }
         pr_default.close(17);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagemitem_fmetricasatributoscod( int GX_Parm1 ,
                                                            int GX_Parm2 ,
                                                            int GX_Parm3 ,
                                                            String GX_Parm4 ,
                                                            String GX_Parm5 )
      {
         A261ContagemItemAtributosFMetrica_ItemContagemLan = GX_Parm1;
         A249ContagemItem_FMetricasAtributosCod = GX_Parm2;
         A253ContagemItem_FMetricasAtributosTabCod = GX_Parm3;
         n253ContagemItem_FMetricasAtributosTabCod = false;
         A250ContagemItem_FMetricasAtributosNom = GX_Parm4;
         n250ContagemItem_FMetricasAtributosNom = false;
         A254ContagemItem_FMetricasAtributosTabNom = GX_Parm5;
         n254ContagemItem_FMetricasAtributosTabNom = false;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         /* Using cursor T001A16 */
         pr_default.execute(14, new Object[] {A249ContagemItem_FMetricasAtributosCod});
         if ( (pr_default.getStatus(14) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Contagem Item_FMetricas Atributos'.", "ForeignKeyNotFound", 1, "CONTAGEMITEM_FMETRICASATRIBUTOSCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemItem_FMetricasAtributosCod_Internalname;
         }
         A250ContagemItem_FMetricasAtributosNom = T001A16_A250ContagemItem_FMetricasAtributosNom[0];
         n250ContagemItem_FMetricasAtributosNom = T001A16_n250ContagemItem_FMetricasAtributosNom[0];
         A253ContagemItem_FMetricasAtributosTabCod = T001A16_A253ContagemItem_FMetricasAtributosTabCod[0];
         n253ContagemItem_FMetricasAtributosTabCod = T001A16_n253ContagemItem_FMetricasAtributosTabCod[0];
         pr_default.close(14);
         /* Using cursor T001A17 */
         pr_default.execute(15, new Object[] {n253ContagemItem_FMetricasAtributosTabCod, A253ContagemItem_FMetricasAtributosTabCod});
         if ( (pr_default.getStatus(15) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Tabela'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A254ContagemItem_FMetricasAtributosTabNom = T001A17_A254ContagemItem_FMetricasAtributosTabNom[0];
         n254ContagemItem_FMetricasAtributosTabNom = T001A17_n254ContagemItem_FMetricasAtributosTabNom[0];
         pr_default.close(15);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A250ContagemItem_FMetricasAtributosNom = "";
            n250ContagemItem_FMetricasAtributosNom = false;
            A253ContagemItem_FMetricasAtributosTabCod = 0;
            n253ContagemItem_FMetricasAtributosTabCod = false;
            A254ContagemItem_FMetricasAtributosTabNom = "";
            n254ContagemItem_FMetricasAtributosTabNom = false;
         }
         isValidOutput.Add(StringUtil.RTrim( A250ContagemItem_FMetricasAtributosNom));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A253ContagemItem_FMetricasAtributosTabCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A254ContagemItem_FMetricasAtributosTabNom));
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z261ContagemItemAtributosFMetrica_ItemContagemLan), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z249ContagemItem_FMetricasAtributosCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.RTrim( Z250ContagemItem_FMetricasAtributosNom));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z253ContagemItem_FMetricasAtributosTabCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( Z254ContagemItem_FMetricasAtributosTabNom));
         isValidOutput.Add(imgBtn_delete2_Enabled);
         isValidOutput.Add(imgBtn_enter2_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(17);
         pr_default.close(14);
         pr_default.close(15);
      }

      public override void initialize( )
      {
         sPrefix = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         lblTextblockcontagemitematributosfmetrica_itemcontagemlan_Jsonclick = "";
         lblTextblockcontagemitem_fmetricasatributoscod_Jsonclick = "";
         lblTextblockcontagemitem_fmetricasatributosnom_Jsonclick = "";
         A250ContagemItem_FMetricasAtributosNom = "";
         lblTextblockcontagemitem_fmetricasatributostabcod_Jsonclick = "";
         lblTextblockcontagemitem_fmetricasatributostabnom_Jsonclick = "";
         A254ContagemItem_FMetricasAtributosTabNom = "";
         imgBtn_first_Jsonclick = "";
         imgBtn_first_separator_Jsonclick = "";
         imgBtn_previous_Jsonclick = "";
         imgBtn_previous_separator_Jsonclick = "";
         imgBtn_next_Jsonclick = "";
         imgBtn_next_separator_Jsonclick = "";
         imgBtn_last_Jsonclick = "";
         imgBtn_last_separator_Jsonclick = "";
         imgBtn_select_Jsonclick = "";
         imgBtn_select_separator_Jsonclick = "";
         imgBtn_enter2_Jsonclick = "";
         imgBtn_enter2_separator_Jsonclick = "";
         imgBtn_cancel2_Jsonclick = "";
         imgBtn_cancel2_separator_Jsonclick = "";
         imgBtn_delete2_Jsonclick = "";
         imgBtn_delete2_separator_Jsonclick = "";
         Gx_mode = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         Z250ContagemItem_FMetricasAtributosNom = "";
         Z254ContagemItem_FMetricasAtributosTabNom = "";
         T001A7_A250ContagemItem_FMetricasAtributosNom = new String[] {""} ;
         T001A7_n250ContagemItem_FMetricasAtributosNom = new bool[] {false} ;
         T001A7_A254ContagemItem_FMetricasAtributosTabNom = new String[] {""} ;
         T001A7_n254ContagemItem_FMetricasAtributosTabNom = new bool[] {false} ;
         T001A7_A261ContagemItemAtributosFMetrica_ItemContagemLan = new int[1] ;
         T001A7_A249ContagemItem_FMetricasAtributosCod = new int[1] ;
         T001A7_A253ContagemItem_FMetricasAtributosTabCod = new int[1] ;
         T001A7_n253ContagemItem_FMetricasAtributosTabCod = new bool[] {false} ;
         T001A4_A261ContagemItemAtributosFMetrica_ItemContagemLan = new int[1] ;
         T001A5_A250ContagemItem_FMetricasAtributosNom = new String[] {""} ;
         T001A5_n250ContagemItem_FMetricasAtributosNom = new bool[] {false} ;
         T001A5_A253ContagemItem_FMetricasAtributosTabCod = new int[1] ;
         T001A5_n253ContagemItem_FMetricasAtributosTabCod = new bool[] {false} ;
         T001A6_A254ContagemItem_FMetricasAtributosTabNom = new String[] {""} ;
         T001A6_n254ContagemItem_FMetricasAtributosTabNom = new bool[] {false} ;
         T001A8_A261ContagemItemAtributosFMetrica_ItemContagemLan = new int[1] ;
         T001A9_A250ContagemItem_FMetricasAtributosNom = new String[] {""} ;
         T001A9_n250ContagemItem_FMetricasAtributosNom = new bool[] {false} ;
         T001A9_A253ContagemItem_FMetricasAtributosTabCod = new int[1] ;
         T001A9_n253ContagemItem_FMetricasAtributosTabCod = new bool[] {false} ;
         T001A10_A254ContagemItem_FMetricasAtributosTabNom = new String[] {""} ;
         T001A10_n254ContagemItem_FMetricasAtributosTabNom = new bool[] {false} ;
         T001A11_A261ContagemItemAtributosFMetrica_ItemContagemLan = new int[1] ;
         T001A11_A249ContagemItem_FMetricasAtributosCod = new int[1] ;
         T001A3_A261ContagemItemAtributosFMetrica_ItemContagemLan = new int[1] ;
         T001A3_A249ContagemItem_FMetricasAtributosCod = new int[1] ;
         sMode47 = "";
         T001A12_A261ContagemItemAtributosFMetrica_ItemContagemLan = new int[1] ;
         T001A12_A249ContagemItem_FMetricasAtributosCod = new int[1] ;
         T001A13_A261ContagemItemAtributosFMetrica_ItemContagemLan = new int[1] ;
         T001A13_A249ContagemItem_FMetricasAtributosCod = new int[1] ;
         T001A2_A261ContagemItemAtributosFMetrica_ItemContagemLan = new int[1] ;
         T001A2_A249ContagemItem_FMetricasAtributosCod = new int[1] ;
         T001A16_A250ContagemItem_FMetricasAtributosNom = new String[] {""} ;
         T001A16_n250ContagemItem_FMetricasAtributosNom = new bool[] {false} ;
         T001A16_A253ContagemItem_FMetricasAtributosTabCod = new int[1] ;
         T001A16_n253ContagemItem_FMetricasAtributosTabCod = new bool[] {false} ;
         T001A17_A254ContagemItem_FMetricasAtributosTabNom = new String[] {""} ;
         T001A17_n254ContagemItem_FMetricasAtributosTabNom = new bool[] {false} ;
         T001A18_A261ContagemItemAtributosFMetrica_ItemContagemLan = new int[1] ;
         T001A18_A249ContagemItem_FMetricasAtributosCod = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         T001A19_A261ContagemItemAtributosFMetrica_ItemContagemLan = new int[1] ;
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemitematributosfmetrica__default(),
            new Object[][] {
                new Object[] {
               T001A2_A261ContagemItemAtributosFMetrica_ItemContagemLan, T001A2_A249ContagemItem_FMetricasAtributosCod
               }
               , new Object[] {
               T001A3_A261ContagemItemAtributosFMetrica_ItemContagemLan, T001A3_A249ContagemItem_FMetricasAtributosCod
               }
               , new Object[] {
               T001A4_A261ContagemItemAtributosFMetrica_ItemContagemLan
               }
               , new Object[] {
               T001A5_A250ContagemItem_FMetricasAtributosNom, T001A5_n250ContagemItem_FMetricasAtributosNom, T001A5_A253ContagemItem_FMetricasAtributosTabCod, T001A5_n253ContagemItem_FMetricasAtributosTabCod
               }
               , new Object[] {
               T001A6_A254ContagemItem_FMetricasAtributosTabNom, T001A6_n254ContagemItem_FMetricasAtributosTabNom
               }
               , new Object[] {
               T001A7_A250ContagemItem_FMetricasAtributosNom, T001A7_n250ContagemItem_FMetricasAtributosNom, T001A7_A254ContagemItem_FMetricasAtributosTabNom, T001A7_n254ContagemItem_FMetricasAtributosTabNom, T001A7_A261ContagemItemAtributosFMetrica_ItemContagemLan, T001A7_A249ContagemItem_FMetricasAtributosCod, T001A7_A253ContagemItem_FMetricasAtributosTabCod, T001A7_n253ContagemItem_FMetricasAtributosTabCod
               }
               , new Object[] {
               T001A8_A261ContagemItemAtributosFMetrica_ItemContagemLan
               }
               , new Object[] {
               T001A9_A250ContagemItem_FMetricasAtributosNom, T001A9_n250ContagemItem_FMetricasAtributosNom, T001A9_A253ContagemItem_FMetricasAtributosTabCod, T001A9_n253ContagemItem_FMetricasAtributosTabCod
               }
               , new Object[] {
               T001A10_A254ContagemItem_FMetricasAtributosTabNom, T001A10_n254ContagemItem_FMetricasAtributosTabNom
               }
               , new Object[] {
               T001A11_A261ContagemItemAtributosFMetrica_ItemContagemLan, T001A11_A249ContagemItem_FMetricasAtributosCod
               }
               , new Object[] {
               T001A12_A261ContagemItemAtributosFMetrica_ItemContagemLan, T001A12_A249ContagemItem_FMetricasAtributosCod
               }
               , new Object[] {
               T001A13_A261ContagemItemAtributosFMetrica_ItemContagemLan, T001A13_A249ContagemItem_FMetricasAtributosCod
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T001A16_A250ContagemItem_FMetricasAtributosNom, T001A16_n250ContagemItem_FMetricasAtributosNom, T001A16_A253ContagemItem_FMetricasAtributosTabCod, T001A16_n253ContagemItem_FMetricasAtributosTabCod
               }
               , new Object[] {
               T001A17_A254ContagemItem_FMetricasAtributosTabNom, T001A17_n254ContagemItem_FMetricasAtributosTabNom
               }
               , new Object[] {
               T001A18_A261ContagemItemAtributosFMetrica_ItemContagemLan, T001A18_A249ContagemItem_FMetricasAtributosCod
               }
               , new Object[] {
               T001A19_A261ContagemItemAtributosFMetrica_ItemContagemLan
               }
            }
         );
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short GX_JID ;
      private short RcdFound47 ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z261ContagemItemAtributosFMetrica_ItemContagemLan ;
      private int Z249ContagemItem_FMetricasAtributosCod ;
      private int A261ContagemItemAtributosFMetrica_ItemContagemLan ;
      private int A249ContagemItem_FMetricasAtributosCod ;
      private int A253ContagemItem_FMetricasAtributosTabCod ;
      private int trnEnded ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int edtContagemItemAtributosFMetrica_ItemContagemLan_Enabled ;
      private int edtContagemItem_FMetricasAtributosCod_Enabled ;
      private int edtContagemItem_FMetricasAtributosNom_Enabled ;
      private int edtContagemItem_FMetricasAtributosTabCod_Enabled ;
      private int edtContagemItem_FMetricasAtributosTabNom_Enabled ;
      private int imgBtn_first_Visible ;
      private int imgBtn_first_separator_Visible ;
      private int imgBtn_previous_Visible ;
      private int imgBtn_previous_separator_Visible ;
      private int imgBtn_next_Visible ;
      private int imgBtn_next_separator_Visible ;
      private int imgBtn_last_Visible ;
      private int imgBtn_last_separator_Visible ;
      private int imgBtn_select_Visible ;
      private int imgBtn_select_separator_Visible ;
      private int imgBtn_enter2_Visible ;
      private int imgBtn_enter2_Enabled ;
      private int imgBtn_enter2_separator_Visible ;
      private int imgBtn_cancel2_Visible ;
      private int imgBtn_cancel2_separator_Visible ;
      private int imgBtn_delete2_Visible ;
      private int imgBtn_delete2_Enabled ;
      private int imgBtn_delete2_separator_Visible ;
      private int Z253ContagemItem_FMetricasAtributosTabCod ;
      private int idxLst ;
      private String sPrefix ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtContagemItemAtributosFMetrica_ItemContagemLan_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String grpGroupdata_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblockcontagemitematributosfmetrica_itemcontagemlan_Internalname ;
      private String lblTextblockcontagemitematributosfmetrica_itemcontagemlan_Jsonclick ;
      private String edtContagemItemAtributosFMetrica_ItemContagemLan_Jsonclick ;
      private String lblTextblockcontagemitem_fmetricasatributoscod_Internalname ;
      private String lblTextblockcontagemitem_fmetricasatributoscod_Jsonclick ;
      private String edtContagemItem_FMetricasAtributosCod_Internalname ;
      private String edtContagemItem_FMetricasAtributosCod_Jsonclick ;
      private String lblTextblockcontagemitem_fmetricasatributosnom_Internalname ;
      private String lblTextblockcontagemitem_fmetricasatributosnom_Jsonclick ;
      private String edtContagemItem_FMetricasAtributosNom_Internalname ;
      private String A250ContagemItem_FMetricasAtributosNom ;
      private String edtContagemItem_FMetricasAtributosNom_Jsonclick ;
      private String lblTextblockcontagemitem_fmetricasatributostabcod_Internalname ;
      private String lblTextblockcontagemitem_fmetricasatributostabcod_Jsonclick ;
      private String edtContagemItem_FMetricasAtributosTabCod_Internalname ;
      private String edtContagemItem_FMetricasAtributosTabCod_Jsonclick ;
      private String lblTextblockcontagemitem_fmetricasatributostabnom_Internalname ;
      private String lblTextblockcontagemitem_fmetricasatributostabnom_Jsonclick ;
      private String edtContagemItem_FMetricasAtributosTabNom_Internalname ;
      private String A254ContagemItem_FMetricasAtributosTabNom ;
      private String edtContagemItem_FMetricasAtributosTabNom_Jsonclick ;
      private String tblTabletoolbar_Internalname ;
      private String divSectiontoolbar_Internalname ;
      private String imgBtn_first_Internalname ;
      private String imgBtn_first_Jsonclick ;
      private String imgBtn_first_separator_Internalname ;
      private String imgBtn_first_separator_Jsonclick ;
      private String imgBtn_previous_Internalname ;
      private String imgBtn_previous_Jsonclick ;
      private String imgBtn_previous_separator_Internalname ;
      private String imgBtn_previous_separator_Jsonclick ;
      private String imgBtn_next_Internalname ;
      private String imgBtn_next_Jsonclick ;
      private String imgBtn_next_separator_Internalname ;
      private String imgBtn_next_separator_Jsonclick ;
      private String imgBtn_last_Internalname ;
      private String imgBtn_last_Jsonclick ;
      private String imgBtn_last_separator_Internalname ;
      private String imgBtn_last_separator_Jsonclick ;
      private String imgBtn_select_Internalname ;
      private String imgBtn_select_Jsonclick ;
      private String imgBtn_select_separator_Internalname ;
      private String imgBtn_select_separator_Jsonclick ;
      private String imgBtn_enter2_Internalname ;
      private String imgBtn_enter2_Jsonclick ;
      private String imgBtn_enter2_separator_Internalname ;
      private String imgBtn_enter2_separator_Jsonclick ;
      private String imgBtn_cancel2_Internalname ;
      private String imgBtn_cancel2_Jsonclick ;
      private String imgBtn_cancel2_separator_Internalname ;
      private String imgBtn_cancel2_separator_Jsonclick ;
      private String imgBtn_delete2_Internalname ;
      private String imgBtn_delete2_Jsonclick ;
      private String imgBtn_delete2_separator_Internalname ;
      private String imgBtn_delete2_separator_Jsonclick ;
      private String Gx_mode ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z250ContagemItem_FMetricasAtributosNom ;
      private String Z254ContagemItem_FMetricasAtributosTabNom ;
      private String sMode47 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private bool entryPointCalled ;
      private bool n253ContagemItem_FMetricasAtributosTabCod ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n250ContagemItem_FMetricasAtributosNom ;
      private bool n254ContagemItem_FMetricasAtributosTabNom ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] T001A7_A250ContagemItem_FMetricasAtributosNom ;
      private bool[] T001A7_n250ContagemItem_FMetricasAtributosNom ;
      private String[] T001A7_A254ContagemItem_FMetricasAtributosTabNom ;
      private bool[] T001A7_n254ContagemItem_FMetricasAtributosTabNom ;
      private int[] T001A7_A261ContagemItemAtributosFMetrica_ItemContagemLan ;
      private int[] T001A7_A249ContagemItem_FMetricasAtributosCod ;
      private int[] T001A7_A253ContagemItem_FMetricasAtributosTabCod ;
      private bool[] T001A7_n253ContagemItem_FMetricasAtributosTabCod ;
      private int[] T001A4_A261ContagemItemAtributosFMetrica_ItemContagemLan ;
      private String[] T001A5_A250ContagemItem_FMetricasAtributosNom ;
      private bool[] T001A5_n250ContagemItem_FMetricasAtributosNom ;
      private int[] T001A5_A253ContagemItem_FMetricasAtributosTabCod ;
      private bool[] T001A5_n253ContagemItem_FMetricasAtributosTabCod ;
      private String[] T001A6_A254ContagemItem_FMetricasAtributosTabNom ;
      private bool[] T001A6_n254ContagemItem_FMetricasAtributosTabNom ;
      private int[] T001A8_A261ContagemItemAtributosFMetrica_ItemContagemLan ;
      private String[] T001A9_A250ContagemItem_FMetricasAtributosNom ;
      private bool[] T001A9_n250ContagemItem_FMetricasAtributosNom ;
      private int[] T001A9_A253ContagemItem_FMetricasAtributosTabCod ;
      private bool[] T001A9_n253ContagemItem_FMetricasAtributosTabCod ;
      private String[] T001A10_A254ContagemItem_FMetricasAtributosTabNom ;
      private bool[] T001A10_n254ContagemItem_FMetricasAtributosTabNom ;
      private int[] T001A11_A261ContagemItemAtributosFMetrica_ItemContagemLan ;
      private int[] T001A11_A249ContagemItem_FMetricasAtributosCod ;
      private int[] T001A3_A261ContagemItemAtributosFMetrica_ItemContagemLan ;
      private int[] T001A3_A249ContagemItem_FMetricasAtributosCod ;
      private int[] T001A12_A261ContagemItemAtributosFMetrica_ItemContagemLan ;
      private int[] T001A12_A249ContagemItem_FMetricasAtributosCod ;
      private int[] T001A13_A261ContagemItemAtributosFMetrica_ItemContagemLan ;
      private int[] T001A13_A249ContagemItem_FMetricasAtributosCod ;
      private int[] T001A2_A261ContagemItemAtributosFMetrica_ItemContagemLan ;
      private int[] T001A2_A249ContagemItem_FMetricasAtributosCod ;
      private String[] T001A16_A250ContagemItem_FMetricasAtributosNom ;
      private bool[] T001A16_n250ContagemItem_FMetricasAtributosNom ;
      private int[] T001A16_A253ContagemItem_FMetricasAtributosTabCod ;
      private bool[] T001A16_n253ContagemItem_FMetricasAtributosTabCod ;
      private String[] T001A17_A254ContagemItem_FMetricasAtributosTabNom ;
      private bool[] T001A17_n254ContagemItem_FMetricasAtributosTabNom ;
      private int[] T001A18_A261ContagemItemAtributosFMetrica_ItemContagemLan ;
      private int[] T001A18_A249ContagemItem_FMetricasAtributosCod ;
      private int[] T001A19_A261ContagemItemAtributosFMetrica_ItemContagemLan ;
      private GXWebForm Form ;
   }

   public class contagemitematributosfmetrica__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new UpdateCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT001A7 ;
          prmT001A7 = new Object[] {
          new Object[] {"@ContagemItemAtributosFMetrica_ItemContagemLan",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemItem_FMetricasAtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001A4 ;
          prmT001A4 = new Object[] {
          new Object[] {"@ContagemItemAtributosFMetrica_ItemContagemLan",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001A5 ;
          prmT001A5 = new Object[] {
          new Object[] {"@ContagemItem_FMetricasAtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001A6 ;
          prmT001A6 = new Object[] {
          new Object[] {"@ContagemItem_FMetricasAtributosTabCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001A8 ;
          prmT001A8 = new Object[] {
          new Object[] {"@ContagemItemAtributosFMetrica_ItemContagemLan",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001A9 ;
          prmT001A9 = new Object[] {
          new Object[] {"@ContagemItem_FMetricasAtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001A10 ;
          prmT001A10 = new Object[] {
          new Object[] {"@ContagemItem_FMetricasAtributosTabCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001A11 ;
          prmT001A11 = new Object[] {
          new Object[] {"@ContagemItemAtributosFMetrica_ItemContagemLan",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemItem_FMetricasAtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001A3 ;
          prmT001A3 = new Object[] {
          new Object[] {"@ContagemItemAtributosFMetrica_ItemContagemLan",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemItem_FMetricasAtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001A12 ;
          prmT001A12 = new Object[] {
          new Object[] {"@ContagemItemAtributosFMetrica_ItemContagemLan",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemItem_FMetricasAtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001A13 ;
          prmT001A13 = new Object[] {
          new Object[] {"@ContagemItemAtributosFMetrica_ItemContagemLan",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemItem_FMetricasAtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001A2 ;
          prmT001A2 = new Object[] {
          new Object[] {"@ContagemItemAtributosFMetrica_ItemContagemLan",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemItem_FMetricasAtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001A14 ;
          prmT001A14 = new Object[] {
          new Object[] {"@ContagemItemAtributosFMetrica_ItemContagemLan",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemItem_FMetricasAtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001A15 ;
          prmT001A15 = new Object[] {
          new Object[] {"@ContagemItemAtributosFMetrica_ItemContagemLan",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemItem_FMetricasAtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001A18 ;
          prmT001A18 = new Object[] {
          } ;
          Object[] prmT001A19 ;
          prmT001A19 = new Object[] {
          new Object[] {"@ContagemItemAtributosFMetrica_ItemContagemLan",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001A16 ;
          prmT001A16 = new Object[] {
          new Object[] {"@ContagemItem_FMetricasAtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001A17 ;
          prmT001A17 = new Object[] {
          new Object[] {"@ContagemItem_FMetricasAtributosTabCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T001A2", "SELECT [ContagemItemAtributosFMetrica_ItemContagemLan] AS ContagemItemAtributosFMetrica_ItemContagemLan, [ContagemItem_FMetricasAtributosCod] AS ContagemItem_FMetricasAtributosCod FROM [ContagemItemAtributosFMetrica] WITH (UPDLOCK) WHERE [ContagemItemAtributosFMetrica_ItemContagemLan] = @ContagemItemAtributosFMetrica_ItemContagemLan AND [ContagemItem_FMetricasAtributosCod] = @ContagemItem_FMetricasAtributosCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001A2,1,0,true,false )
             ,new CursorDef("T001A3", "SELECT [ContagemItemAtributosFMetrica_ItemContagemLan] AS ContagemItemAtributosFMetrica_ItemContagemLan, [ContagemItem_FMetricasAtributosCod] AS ContagemItem_FMetricasAtributosCod FROM [ContagemItemAtributosFMetrica] WITH (NOLOCK) WHERE [ContagemItemAtributosFMetrica_ItemContagemLan] = @ContagemItemAtributosFMetrica_ItemContagemLan AND [ContagemItem_FMetricasAtributosCod] = @ContagemItem_FMetricasAtributosCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001A3,1,0,true,false )
             ,new CursorDef("T001A4", "SELECT [ContagemItem_Lancamento] AS ContagemItemAtributosFMetrica_ItemContagemLan FROM [ContagemItem] WITH (NOLOCK) WHERE [ContagemItem_Lancamento] = @ContagemItemAtributosFMetrica_ItemContagemLan ",true, GxErrorMask.GX_NOMASK, false, this,prmT001A4,1,0,true,false )
             ,new CursorDef("T001A5", "SELECT [Atributos_Nome] AS ContagemItem_FMetricasAtributosNom, [Atributos_TabelaCod] AS ContagemItem_FMetricasAtributosTabCod FROM [Atributos] WITH (NOLOCK) WHERE [Atributos_Codigo] = @ContagemItem_FMetricasAtributosCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001A5,1,0,true,false )
             ,new CursorDef("T001A6", "SELECT [Tabela_Nome] AS ContagemItem_FMetricasAtributosTabNom FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_Codigo] = @ContagemItem_FMetricasAtributosTabCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001A6,1,0,true,false )
             ,new CursorDef("T001A7", "SELECT T2.[Atributos_Nome] AS ContagemItem_FMetricasAtributosNom, T3.[Tabela_Nome] AS ContagemItem_FMetricasAtributosTabNom, TM1.[ContagemItemAtributosFMetrica_ItemContagemLan] AS ContagemItemAtributosFMetrica_ItemContagemLan, TM1.[ContagemItem_FMetricasAtributosCod] AS ContagemItem_FMetricasAtributosCod, T2.[Atributos_TabelaCod] AS ContagemItem_FMetricasAtributosTabCod FROM (([ContagemItemAtributosFMetrica] TM1 WITH (NOLOCK) INNER JOIN [Atributos] T2 WITH (NOLOCK) ON T2.[Atributos_Codigo] = TM1.[ContagemItem_FMetricasAtributosCod]) LEFT JOIN [Tabela] T3 WITH (NOLOCK) ON T3.[Tabela_Codigo] = T2.[Atributos_TabelaCod]) WHERE TM1.[ContagemItemAtributosFMetrica_ItemContagemLan] = @ContagemItemAtributosFMetrica_ItemContagemLan and TM1.[ContagemItem_FMetricasAtributosCod] = @ContagemItem_FMetricasAtributosCod ORDER BY TM1.[ContagemItemAtributosFMetrica_ItemContagemLan], TM1.[ContagemItem_FMetricasAtributosCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001A7,100,0,true,false )
             ,new CursorDef("T001A8", "SELECT [ContagemItem_Lancamento] AS ContagemItemAtributosFMetrica_ItemContagemLan FROM [ContagemItem] WITH (NOLOCK) WHERE [ContagemItem_Lancamento] = @ContagemItemAtributosFMetrica_ItemContagemLan ",true, GxErrorMask.GX_NOMASK, false, this,prmT001A8,1,0,true,false )
             ,new CursorDef("T001A9", "SELECT [Atributos_Nome] AS ContagemItem_FMetricasAtributosNom, [Atributos_TabelaCod] AS ContagemItem_FMetricasAtributosTabCod FROM [Atributos] WITH (NOLOCK) WHERE [Atributos_Codigo] = @ContagemItem_FMetricasAtributosCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001A9,1,0,true,false )
             ,new CursorDef("T001A10", "SELECT [Tabela_Nome] AS ContagemItem_FMetricasAtributosTabNom FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_Codigo] = @ContagemItem_FMetricasAtributosTabCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001A10,1,0,true,false )
             ,new CursorDef("T001A11", "SELECT [ContagemItemAtributosFMetrica_ItemContagemLan] AS ContagemItemAtributosFMetrica_ItemContagemLan, [ContagemItem_FMetricasAtributosCod] AS ContagemItem_FMetricasAtributosCod FROM [ContagemItemAtributosFMetrica] WITH (NOLOCK) WHERE [ContagemItemAtributosFMetrica_ItemContagemLan] = @ContagemItemAtributosFMetrica_ItemContagemLan AND [ContagemItem_FMetricasAtributosCod] = @ContagemItem_FMetricasAtributosCod  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001A11,1,0,true,false )
             ,new CursorDef("T001A12", "SELECT TOP 1 [ContagemItemAtributosFMetrica_ItemContagemLan] AS ContagemItemAtributosFMetrica_ItemContagemLan, [ContagemItem_FMetricasAtributosCod] AS ContagemItem_FMetricasAtributosCod FROM [ContagemItemAtributosFMetrica] WITH (NOLOCK) WHERE ( [ContagemItemAtributosFMetrica_ItemContagemLan] > @ContagemItemAtributosFMetrica_ItemContagemLan or [ContagemItemAtributosFMetrica_ItemContagemLan] = @ContagemItemAtributosFMetrica_ItemContagemLan and [ContagemItem_FMetricasAtributosCod] > @ContagemItem_FMetricasAtributosCod) ORDER BY [ContagemItemAtributosFMetrica_ItemContagemLan], [ContagemItem_FMetricasAtributosCod]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001A12,1,0,true,true )
             ,new CursorDef("T001A13", "SELECT TOP 1 [ContagemItemAtributosFMetrica_ItemContagemLan] AS ContagemItemAtributosFMetrica_ItemContagemLan, [ContagemItem_FMetricasAtributosCod] AS ContagemItem_FMetricasAtributosCod FROM [ContagemItemAtributosFMetrica] WITH (NOLOCK) WHERE ( [ContagemItemAtributosFMetrica_ItemContagemLan] < @ContagemItemAtributosFMetrica_ItemContagemLan or [ContagemItemAtributosFMetrica_ItemContagemLan] = @ContagemItemAtributosFMetrica_ItemContagemLan and [ContagemItem_FMetricasAtributosCod] < @ContagemItem_FMetricasAtributosCod) ORDER BY [ContagemItemAtributosFMetrica_ItemContagemLan] DESC, [ContagemItem_FMetricasAtributosCod] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001A13,1,0,true,true )
             ,new CursorDef("T001A14", "INSERT INTO [ContagemItemAtributosFMetrica]([ContagemItemAtributosFMetrica_ItemContagemLan], [ContagemItem_FMetricasAtributosCod]) VALUES(@ContagemItemAtributosFMetrica_ItemContagemLan, @ContagemItem_FMetricasAtributosCod)", GxErrorMask.GX_NOMASK,prmT001A14)
             ,new CursorDef("T001A15", "DELETE FROM [ContagemItemAtributosFMetrica]  WHERE [ContagemItemAtributosFMetrica_ItemContagemLan] = @ContagemItemAtributosFMetrica_ItemContagemLan AND [ContagemItem_FMetricasAtributosCod] = @ContagemItem_FMetricasAtributosCod", GxErrorMask.GX_NOMASK,prmT001A15)
             ,new CursorDef("T001A16", "SELECT [Atributos_Nome] AS ContagemItem_FMetricasAtributosNom, [Atributos_TabelaCod] AS ContagemItem_FMetricasAtributosTabCod FROM [Atributos] WITH (NOLOCK) WHERE [Atributos_Codigo] = @ContagemItem_FMetricasAtributosCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001A16,1,0,true,false )
             ,new CursorDef("T001A17", "SELECT [Tabela_Nome] AS ContagemItem_FMetricasAtributosTabNom FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_Codigo] = @ContagemItem_FMetricasAtributosTabCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001A17,1,0,true,false )
             ,new CursorDef("T001A18", "SELECT [ContagemItemAtributosFMetrica_ItemContagemLan] AS ContagemItemAtributosFMetrica_ItemContagemLan, [ContagemItem_FMetricasAtributosCod] AS ContagemItem_FMetricasAtributosCod FROM [ContagemItemAtributosFMetrica] WITH (NOLOCK) ORDER BY [ContagemItemAtributosFMetrica_ItemContagemLan], [ContagemItem_FMetricasAtributosCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001A18,100,0,true,false )
             ,new CursorDef("T001A19", "SELECT [ContagemItem_Lancamento] AS ContagemItemAtributosFMetrica_ItemContagemLan FROM [ContagemItem] WITH (NOLOCK) WHERE [ContagemItem_Lancamento] = @ContagemItemAtributosFMetrica_ItemContagemLan ",true, GxErrorMask.GX_NOMASK, false, this,prmT001A19,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 8 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 14 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 15 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
