/*
               File: PRC_ContagemResultadoAlteraStatus
        Description: Contagem Resultado Retorno
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/24/2020 22:57:44.29
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_contagemresultadoalterastatus : GXProcedure
   {
      public prc_contagemresultadoalterastatus( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_contagemresultadoalterastatus( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultado_Codigo ,
                           String aP1_Status ,
                           int aP2_ContagemResultado_NaoCnfDmnCod ,
                           String aP3_ContagemResultado_Observacao ,
                           ref bool aP4_Confirmado )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV8Status = aP1_Status;
         this.AV9ContagemResultado_NaoCnfDmnCod = aP2_ContagemResultado_NaoCnfDmnCod;
         this.AV10ContagemResultado_Observacao = aP3_ContagemResultado_Observacao;
         this.AV19Confirmado = aP4_Confirmado;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP4_Confirmado=this.AV19Confirmado;
      }

      public bool executeUdp( ref int aP0_ContagemResultado_Codigo ,
                              String aP1_Status ,
                              int aP2_ContagemResultado_NaoCnfDmnCod ,
                              String aP3_ContagemResultado_Observacao )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV8Status = aP1_Status;
         this.AV9ContagemResultado_NaoCnfDmnCod = aP2_ContagemResultado_NaoCnfDmnCod;
         this.AV10ContagemResultado_Observacao = aP3_ContagemResultado_Observacao;
         this.AV19Confirmado = aP4_Confirmado;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP4_Confirmado=this.AV19Confirmado;
         return AV19Confirmado ;
      }

      public void executeSubmit( ref int aP0_ContagemResultado_Codigo ,
                                 String aP1_Status ,
                                 int aP2_ContagemResultado_NaoCnfDmnCod ,
                                 String aP3_ContagemResultado_Observacao ,
                                 ref bool aP4_Confirmado )
      {
         prc_contagemresultadoalterastatus objprc_contagemresultadoalterastatus;
         objprc_contagemresultadoalterastatus = new prc_contagemresultadoalterastatus();
         objprc_contagemresultadoalterastatus.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_contagemresultadoalterastatus.AV8Status = aP1_Status;
         objprc_contagemresultadoalterastatus.AV9ContagemResultado_NaoCnfDmnCod = aP2_ContagemResultado_NaoCnfDmnCod;
         objprc_contagemresultadoalterastatus.AV10ContagemResultado_Observacao = aP3_ContagemResultado_Observacao;
         objprc_contagemresultadoalterastatus.AV19Confirmado = aP4_Confirmado;
         objprc_contagemresultadoalterastatus.context.SetSubmitInitialConfig(context);
         objprc_contagemresultadoalterastatus.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_contagemresultadoalterastatus);
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP4_Confirmado=this.AV19Confirmado;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_contagemresultadoalterastatus)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV18WWPContext) ;
         AV40ServerNow = DateTimeUtil.ServerNow( context, "DEFAULT");
         /* Using cursor P00302 */
         pr_default.execute(0, new Object[] {A456ContagemResultado_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A472ContagemResultado_DataEntrega = P00302_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = P00302_n472ContagemResultado_DataEntrega[0];
            A912ContagemResultado_HoraEntrega = P00302_A912ContagemResultado_HoraEntrega[0];
            n912ContagemResultado_HoraEntrega = P00302_n912ContagemResultado_HoraEntrega[0];
            A484ContagemResultado_StatusDmn = P00302_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00302_n484ContagemResultado_StatusDmn[0];
            A490ContagemResultado_ContratadaCod = P00302_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00302_n490ContagemResultado_ContratadaCod[0];
            A1553ContagemResultado_CntSrvCod = P00302_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00302_n1553ContagemResultado_CntSrvCod[0];
            A468ContagemResultado_NaoCnfDmnCod = P00302_A468ContagemResultado_NaoCnfDmnCod[0];
            n468ContagemResultado_NaoCnfDmnCod = P00302_n468ContagemResultado_NaoCnfDmnCod[0];
            A1349ContagemResultado_DataExecucao = P00302_A1349ContagemResultado_DataExecucao[0];
            n1349ContagemResultado_DataExecucao = P00302_n1349ContagemResultado_DataExecucao[0];
            A1237ContagemResultado_PrazoMaisDias = P00302_A1237ContagemResultado_PrazoMaisDias[0];
            n1237ContagemResultado_PrazoMaisDias = P00302_n1237ContagemResultado_PrazoMaisDias[0];
            A1227ContagemResultado_PrazoInicialDias = P00302_A1227ContagemResultado_PrazoInicialDias[0];
            n1227ContagemResultado_PrazoInicialDias = P00302_n1227ContagemResultado_PrazoInicialDias[0];
            A1611ContagemResultado_PrzTpDias = P00302_A1611ContagemResultado_PrzTpDias[0];
            n1611ContagemResultado_PrzTpDias = P00302_n1611ContagemResultado_PrzTpDias[0];
            A1043ContagemResultado_LiqLogCod = P00302_A1043ContagemResultado_LiqLogCod[0];
            n1043ContagemResultado_LiqLogCod = P00302_n1043ContagemResultado_LiqLogCod[0];
            A890ContagemResultado_Responsavel = P00302_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = P00302_n890ContagemResultado_Responsavel[0];
            A457ContagemResultado_Demanda = P00302_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00302_n457ContagemResultado_Demanda[0];
            A1348ContagemResultado_DataHomologacao = P00302_A1348ContagemResultado_DataHomologacao[0];
            n1348ContagemResultado_DataHomologacao = P00302_n1348ContagemResultado_DataHomologacao[0];
            A514ContagemResultado_Observacao = P00302_A514ContagemResultado_Observacao[0];
            n514ContagemResultado_Observacao = P00302_n514ContagemResultado_Observacao[0];
            A1603ContagemResultado_CntCod = P00302_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P00302_n1603ContagemResultado_CntCod[0];
            A512ContagemResultado_ValorPF = P00302_A512ContagemResultado_ValorPF[0];
            n512ContagemResultado_ValorPF = P00302_n512ContagemResultado_ValorPF[0];
            A1611ContagemResultado_PrzTpDias = P00302_A1611ContagemResultado_PrzTpDias[0];
            n1611ContagemResultado_PrzTpDias = P00302_n1611ContagemResultado_PrzTpDias[0];
            A1603ContagemResultado_CntCod = P00302_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P00302_n1603ContagemResultado_CntCod[0];
            GXt_decimal1 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
            A574ContagemResultado_PFFinal = GXt_decimal1;
            A606ContagemResultado_ValorFinal = (decimal)(A574ContagemResultado_PFFinal*A512ContagemResultado_ValorPF);
            AV20PrazoEntrega = DateTimeUtil.ResetTime( A472ContagemResultado_DataEntrega ) ;
            AV20PrazoEntrega = DateTimeUtil.TAdd( AV20PrazoEntrega, 3600*(DateTimeUtil.Hour( A912ContagemResultado_HoraEntrega)));
            AV20PrazoEntrega = DateTimeUtil.TAdd( AV20PrazoEntrega, 60*(DateTimeUtil.Minute( A912ContagemResultado_HoraEntrega)));
            AV21StatusAnterior = A484ContagemResultado_StatusDmn;
            AV43Contratada_Codigo = A490ContagemResultado_ContratadaCod;
            AV42ContratoServicos_Codigo = A1553ContagemResultado_CntSrvCod;
            new prc_alterastatusdmn(context ).execute( ref  A456ContagemResultado_Codigo,  AV8Status,  0) ;
            AV22Acao = AV8Status;
            if ( StringUtil.StrCmp(AV8Status, "A") == 0 )
            {
               AV22Acao = "D";
            }
            else if ( StringUtil.StrCmp(AV8Status, "R") == 0 )
            {
               A468ContagemResultado_NaoCnfDmnCod = 0;
               n468ContagemResultado_NaoCnfDmnCod = false;
               n468ContagemResultado_NaoCnfDmnCod = true;
               A1349ContagemResultado_DataExecucao = DateTimeUtil.ServerNow( context, "DEFAULT");
               n1349ContagemResultado_DataExecucao = false;
               AV22Acao = "V";
            }
            else if ( StringUtil.StrCmp(AV8Status, "H") == 0 )
            {
               A468ContagemResultado_NaoCnfDmnCod = 0;
               n468ContagemResultado_NaoCnfDmnCod = false;
               n468ContagemResultado_NaoCnfDmnCod = true;
               AV50Codigo = A456ContagemResultado_Codigo;
               /* Execute user subroutine: 'DLTNAOCNFCONTESTADA' */
               S131 ();
               if ( returnInSub )
               {
                  pr_default.close(0);
                  this.cleanup();
                  if (true) return;
               }
            }
            else if ( StringUtil.StrCmp(AV8Status, "D") == 0 )
            {
               AV50Codigo = A456ContagemResultado_Codigo;
               /* Execute user subroutine: 'DLTNAOCNFCONTESTADA' */
               S131 ();
               if ( returnInSub )
               {
                  pr_default.close(0);
                  this.cleanup();
                  if (true) return;
               }
               /* Execute user subroutine: 'QUEMRESOLVEU' */
               S111 ();
               if ( returnInSub )
               {
                  pr_default.close(0);
                  this.cleanup();
                  if (true) return;
               }
               AV20PrazoEntrega = DateTimeUtil.ResetTime( DateTimeUtil.ServerDate( context, "DEFAULT") ) ;
               AV35PrazoBase = (short)(A1227ContagemResultado_PrazoInicialDias+A1237ContagemResultado_PrazoMaisDias);
               AV48TipoDias = A1611ContagemResultado_PrzTpDias;
               GXt_int2 = 0;
               GXt_dtime3 = DateTimeUtil.ResetTime( DateTime.MinValue ) ;
               new prc_prazodecorrecaodata(context ).execute(  A1553ContagemResultado_CntSrvCod,  "",  0, ref  GXt_int2,  AV35PrazoBase,  0,  GXt_dtime3, ref  AV20PrazoEntrega) ;
               A472ContagemResultado_DataEntrega = DateTimeUtil.ResetTime(AV20PrazoEntrega);
               n472ContagemResultado_DataEntrega = false;
               A912ContagemResultado_HoraEntrega = DateTimeUtil.ResetDate(AV20PrazoEntrega);
               n912ContagemResultado_HoraEntrega = false;
               if ( AV9ContagemResultado_NaoCnfDmnCod > 0 )
               {
                  A468ContagemResultado_NaoCnfDmnCod = AV9ContagemResultado_NaoCnfDmnCod;
                  n468ContagemResultado_NaoCnfDmnCod = false;
                  /* Execute user subroutine: 'GETGLOSAVEL' */
                  S121 ();
                  if ( returnInSub )
                  {
                     pr_default.close(0);
                     this.cleanup();
                     if (true) return;
                  }
               }
               else
               {
                  A468ContagemResultado_NaoCnfDmnCod = 0;
                  n468ContagemResultado_NaoCnfDmnCod = false;
                  n468ContagemResultado_NaoCnfDmnCod = true;
               }
               A1043ContagemResultado_LiqLogCod = 0;
               n1043ContagemResultado_LiqLogCod = false;
               n1043ContagemResultado_LiqLogCod = true;
               if ( (0==AV36Usuario_Codigo) )
               {
                  A890ContagemResultado_Responsavel = 0;
                  n890ContagemResultado_Responsavel = false;
                  n890ContagemResultado_Responsavel = true;
               }
               else
               {
                  A890ContagemResultado_Responsavel = AV36Usuario_Codigo;
                  n890ContagemResultado_Responsavel = false;
               }
               AV22Acao = "R";
               AV26Prestadora = A490ContagemResultado_ContratadaCod;
               AV31Demanda = A457ContagemResultado_Demanda;
            }
            else if ( StringUtil.StrCmp(AV8Status, "H") == 0 )
            {
               A1348ContagemResultado_DataHomologacao = AV40ServerNow;
               n1348ContagemResultado_DataHomologacao = false;
            }
            else if ( StringUtil.StrCmp(AV8Status, "X") == 0 )
            {
               A468ContagemResultado_NaoCnfDmnCod = AV9ContagemResultado_NaoCnfDmnCod;
               n468ContagemResultado_NaoCnfDmnCod = false;
            }
            if ( String.IsNullOrEmpty(StringUtil.RTrim( A514ContagemResultado_Observacao)) )
            {
               A514ContagemResultado_Observacao = AV10ContagemResultado_Observacao;
               n514ContagemResultado_Observacao = false;
            }
            else
            {
               A514ContagemResultado_Observacao = StringUtil.Trim( A514ContagemResultado_Observacao) + StringUtil.NewLine( ) + StringUtil.Trim( AV10ContagemResultado_Observacao);
               n514ContagemResultado_Observacao = false;
            }
            AV44Contrato_Codigo = A1603ContagemResultado_CntCod;
            AV45Valor_Debito = A606ContagemResultado_ValorFinal;
            AV19Confirmado = true;
            if ( StringUtil.StrCmp(AV22Acao, "D") != 0 )
            {
               new prc_inslogresponsavel(context ).execute( ref  A456ContagemResultado_Codigo,  AV36Usuario_Codigo,  AV22Acao,  "D",  AV18WWPContext.gxTpr_Userid,  0,  AV21StatusAnterior,  AV8Status,  AV10ContagemResultado_Observacao,  AV20PrazoEntrega,  true) ;
               if ( ( StringUtil.StrCmp(AV22Acao, "R") == 0 ) && ( ( StringUtil.StrCmp(AV21StatusAnterior, "R") == 0 ) || ( StringUtil.StrCmp(AV21StatusAnterior, "H") == 0 ) ) )
               {
                  AV46LogResponsavel_Codigo = (long)(NumberUtil.Val( AV30WebSession.Get("LogRspCodigo"), "."));
                  AV30WebSession.Remove("LogRspCodigo");
                  GXt_boolean4 = true;
                  new prc_newosnaocnf(context ).execute( ref  A456ContagemResultado_Codigo, ref  AV9ContagemResultado_NaoCnfDmnCod, ref  GXt_boolean4,  AV46LogResponsavel_Codigo) ;
                  new prc_novocicloexecucao(context ).execute(  A456ContagemResultado_Codigo,  DateTimeUtil.ServerNow( context, "DEFAULT"),  AV20PrazoEntrega,  AV48TipoDias,  AV49Glosavel) ;
               }
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P00303 */
            pr_default.execute(1, new Object[] {n472ContagemResultado_DataEntrega, A472ContagemResultado_DataEntrega, n912ContagemResultado_HoraEntrega, A912ContagemResultado_HoraEntrega, n468ContagemResultado_NaoCnfDmnCod, A468ContagemResultado_NaoCnfDmnCod, n1349ContagemResultado_DataExecucao, A1349ContagemResultado_DataExecucao, n1043ContagemResultado_LiqLogCod, A1043ContagemResultado_LiqLogCod, n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, n1348ContagemResultado_DataHomologacao, A1348ContagemResultado_DataHomologacao, n514ContagemResultado_Observacao, A514ContagemResultado_Observacao, A456ContagemResultado_Codigo});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            if (true) break;
            /* Using cursor P00304 */
            pr_default.execute(2, new Object[] {n472ContagemResultado_DataEntrega, A472ContagemResultado_DataEntrega, n912ContagemResultado_HoraEntrega, A912ContagemResultado_HoraEntrega, n468ContagemResultado_NaoCnfDmnCod, A468ContagemResultado_NaoCnfDmnCod, n1349ContagemResultado_DataExecucao, A1349ContagemResultado_DataExecucao, n1043ContagemResultado_LiqLogCod, A1043ContagemResultado_LiqLogCod, n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, n1348ContagemResultado_DataHomologacao, A1348ContagemResultado_DataHomologacao, n514ContagemResultado_Observacao, A514ContagemResultado_Observacao, A456ContagemResultado_Codigo});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         if ( StringUtil.StrCmp(AV8Status, "D") == 0 )
         {
            AV27Usuarios.Add(AV36Usuario_Codigo, 0);
            /* Using cursor P00305 */
            pr_default.execute(3, new Object[] {AV42ContratoServicos_Codigo});
            while ( (pr_default.getStatus(3) != 101) )
            {
               A74Contrato_Codigo = P00305_A74Contrato_Codigo[0];
               A160ContratoServicos_Codigo = P00305_A160ContratoServicos_Codigo[0];
               /* Using cursor P00306 */
               pr_default.execute(4, new Object[] {A74Contrato_Codigo});
               while ( (pr_default.getStatus(4) != 101) )
               {
                  A1078ContratoGestor_ContratoCod = P00306_A1078ContratoGestor_ContratoCod[0];
                  A1079ContratoGestor_UsuarioCod = P00306_A1079ContratoGestor_UsuarioCod[0];
                  A1446ContratoGestor_ContratadaAreaCod = P00306_A1446ContratoGestor_ContratadaAreaCod[0];
                  n1446ContratoGestor_ContratadaAreaCod = P00306_n1446ContratoGestor_ContratadaAreaCod[0];
                  A1446ContratoGestor_ContratadaAreaCod = P00306_A1446ContratoGestor_ContratadaAreaCod[0];
                  n1446ContratoGestor_ContratadaAreaCod = P00306_n1446ContratoGestor_ContratadaAreaCod[0];
                  GXt_boolean4 = A1135ContratoGestor_UsuarioEhContratante;
                  new prc_usuarioehcontratantedaarea(context ).execute( ref  A1446ContratoGestor_ContratadaAreaCod, ref  A1079ContratoGestor_UsuarioCod, out  GXt_boolean4) ;
                  A1135ContratoGestor_UsuarioEhContratante = GXt_boolean4;
                  if ( ! A1135ContratoGestor_UsuarioEhContratante )
                  {
                     AV27Usuarios.Add(A1079ContratoGestor_UsuarioCod, 0);
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                  }
                  pr_default.readNext(4);
               }
               pr_default.close(4);
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(3);
            if ( AV27Usuarios.Count > 0 )
            {
               /* Execute user subroutine: 'BUSCA.URL.SISTEMA' */
               S141 ();
               if ( returnInSub )
               {
                  this.cleanup();
                  if (true) return;
               }
               AV34Subject = StringUtil.Trim( AV18WWPContext.gxTpr_Areatrabalho_descricao) + " Retorno de demanda (No reply)";
               AV34Subject = StringUtil.StringReplace( AV34Subject, "  ", " ");
               AV28EmailText = AV28EmailText + StringUtil.NewLine( ) + "A demanda " + AV31Demanda + " foi retornada." + StringUtil.NewLine( ) + StringUtil.NewLine( );
               /* Using cursor P00307 */
               pr_default.execute(5, new Object[] {AV9ContagemResultado_NaoCnfDmnCod});
               while ( (pr_default.getStatus(5) != 101) )
               {
                  A426NaoConformidade_Codigo = P00307_A426NaoConformidade_Codigo[0];
                  A427NaoConformidade_Nome = P00307_A427NaoConformidade_Nome[0];
                  AV28EmailText = AV28EmailText + "N�o conformidade: " + A427NaoConformidade_Nome + StringUtil.NewLine( );
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(5);
               AV28EmailText = AV28EmailText + "Observa��o:       " + AV10ContagemResultado_Observacao + StringUtil.NewLine( ) + StringUtil.NewLine( );
               AV28EmailText = AV28EmailText + "Prazo de corre��o: " + context.localUtil.TToC( AV20PrazoEntrega, 8, 5, 0, 3, "/", ":", " ") + StringUtil.NewLine( ) + StringUtil.NewLine( );
               AV28EmailText = AV28EmailText + "Para atendimento da demanda acesse o sistema de gest�o de solicita��es <" + StringUtil.Trim( AV51ParametrosSistema_URLApp) + ">." + StringUtil.NewLine( ) + StringUtil.NewLine( );
               AV28EmailText = AV28EmailText + StringUtil.Trim( AV18WWPContext.gxTpr_Areatrabalho_descricao) + StringUtil.NewLine( ) + StringUtil.Trim( AV18WWPContext.gxTpr_Username) + StringUtil.NewLine( );
               AV41ContagemResultado_Codigos.Add(A456ContagemResultado_Codigo, 0);
               AV30WebSession.Set("DemandaCodigo", AV41ContagemResultado_Codigos.ToXml(false, true, "Collection", ""));
               new prc_enviaremail(context ).execute(  AV18WWPContext.gxTpr_Areatrabalho_codigo,  AV27Usuarios,  AV34Subject,  AV28EmailText,  AV32Attachments, ref  AV33Resultado) ;
               AV30WebSession.Remove("DemandaCodigo");
            }
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'QUEMRESOLVEU' Routine */
         AV59GXLvl130 = 0;
         /* Using cursor P00308 */
         pr_default.execute(6, new Object[] {A456ContagemResultado_Codigo});
         while ( (pr_default.getStatus(6) != 101) )
         {
            A892LogResponsavel_DemandaCod = P00308_A892LogResponsavel_DemandaCod[0];
            n892LogResponsavel_DemandaCod = P00308_n892LogResponsavel_DemandaCod[0];
            A896LogResponsavel_Owner = P00308_A896LogResponsavel_Owner[0];
            A490ContagemResultado_ContratadaCod = P00308_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00308_n490ContagemResultado_ContratadaCod[0];
            A894LogResponsavel_Acao = P00308_A894LogResponsavel_Acao[0];
            A1797LogResponsavel_Codigo = P00308_A1797LogResponsavel_Codigo[0];
            A490ContagemResultado_ContratadaCod = P00308_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00308_n490ContagemResultado_ContratadaCod[0];
            AV59GXLvl130 = 1;
            /* Using cursor P00309 */
            pr_default.execute(7, new Object[] {n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod, A896LogResponsavel_Owner});
            while ( (pr_default.getStatus(7) != 101) )
            {
               A66ContratadaUsuario_ContratadaCod = P00309_A66ContratadaUsuario_ContratadaCod[0];
               A69ContratadaUsuario_UsuarioCod = P00309_A69ContratadaUsuario_UsuarioCod[0];
               AV36Usuario_Codigo = A896LogResponsavel_Owner;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(7);
            if ( AV36Usuario_Codigo > 0 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            pr_default.readNext(6);
         }
         pr_default.close(6);
         if ( AV59GXLvl130 == 0 )
         {
            /* Using cursor P003010 */
            pr_default.execute(8, new Object[] {AV42ContratoServicos_Codigo});
            while ( (pr_default.getStatus(8) != 101) )
            {
               A74Contrato_Codigo = P003010_A74Contrato_Codigo[0];
               A160ContratoServicos_Codigo = P003010_A160ContratoServicos_Codigo[0];
               A1013Contrato_PrepostoCod = P003010_A1013Contrato_PrepostoCod[0];
               n1013Contrato_PrepostoCod = P003010_n1013Contrato_PrepostoCod[0];
               A1013Contrato_PrepostoCod = P003010_A1013Contrato_PrepostoCod[0];
               n1013Contrato_PrepostoCod = P003010_n1013Contrato_PrepostoCod[0];
               if ( A1013Contrato_PrepostoCod > 0 )
               {
                  AV36Usuario_Codigo = A1013Contrato_PrepostoCod;
               }
               else
               {
                  /* Using cursor P003011 */
                  pr_default.execute(9, new Object[] {A74Contrato_Codigo, AV43Contratada_Codigo});
                  while ( (pr_default.getStatus(9) != 101) )
                  {
                     A1078ContratoGestor_ContratoCod = P003011_A1078ContratoGestor_ContratoCod[0];
                     A1136ContratoGestor_ContratadaCod = P003011_A1136ContratoGestor_ContratadaCod[0];
                     n1136ContratoGestor_ContratadaCod = P003011_n1136ContratoGestor_ContratadaCod[0];
                     A1079ContratoGestor_UsuarioCod = P003011_A1079ContratoGestor_UsuarioCod[0];
                     A1446ContratoGestor_ContratadaAreaCod = P003011_A1446ContratoGestor_ContratadaAreaCod[0];
                     n1446ContratoGestor_ContratadaAreaCod = P003011_n1446ContratoGestor_ContratadaAreaCod[0];
                     A1136ContratoGestor_ContratadaCod = P003011_A1136ContratoGestor_ContratadaCod[0];
                     n1136ContratoGestor_ContratadaCod = P003011_n1136ContratoGestor_ContratadaCod[0];
                     A1446ContratoGestor_ContratadaAreaCod = P003011_A1446ContratoGestor_ContratadaAreaCod[0];
                     n1446ContratoGestor_ContratadaAreaCod = P003011_n1446ContratoGestor_ContratadaAreaCod[0];
                     GXt_boolean4 = A1135ContratoGestor_UsuarioEhContratante;
                     new prc_usuarioehcontratantedaarea(context ).execute( ref  A1446ContratoGestor_ContratadaAreaCod, ref  A1079ContratoGestor_UsuarioCod, out  GXt_boolean4) ;
                     A1135ContratoGestor_UsuarioEhContratante = GXt_boolean4;
                     if ( ! A1135ContratoGestor_UsuarioEhContratante )
                     {
                        AV36Usuario_Codigo = A1079ContratoGestor_UsuarioCod;
                        /* Exit For each command. Update data (if necessary), close cursors & exit. */
                        if (true) break;
                     }
                     pr_default.readNext(9);
                  }
                  pr_default.close(9);
               }
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(8);
         }
      }

      protected void S121( )
      {
         /* 'GETGLOSAVEL' Routine */
         /* Using cursor P003012 */
         pr_default.execute(10, new Object[] {AV9ContagemResultado_NaoCnfDmnCod});
         while ( (pr_default.getStatus(10) != 101) )
         {
            A426NaoConformidade_Codigo = P003012_A426NaoConformidade_Codigo[0];
            A2029NaoConformidade_Glosavel = P003012_A2029NaoConformidade_Glosavel[0];
            n2029NaoConformidade_Glosavel = P003012_n2029NaoConformidade_Glosavel[0];
            AV49Glosavel = A2029NaoConformidade_Glosavel;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(10);
      }

      protected void S131( )
      {
         /* 'DLTNAOCNFCONTESTADA' Routine */
         /* Using cursor P003014 */
         pr_default.execute(11, new Object[] {AV50Codigo});
         if ( (pr_default.getStatus(11) != 101) )
         {
            A40000GXC1 = P003014_A40000GXC1[0];
         }
         else
         {
            A40000GXC1 = 0;
         }
         pr_default.close(11);
         AV50Codigo = A40000GXC1;
         if ( AV50Codigo > 0 )
         {
            new prc_dltnaocnf(context ).execute( ref  AV50Codigo) ;
         }
      }

      protected void S141( )
      {
         /* 'BUSCA.URL.SISTEMA' Routine */
         AV51ParametrosSistema_URLApp = "";
         /* Using cursor P003015 */
         pr_default.execute(12);
         while ( (pr_default.getStatus(12) != 101) )
         {
            A1679ParametrosSistema_URLApp = P003015_A1679ParametrosSistema_URLApp[0];
            n1679ParametrosSistema_URLApp = P003015_n1679ParametrosSistema_URLApp[0];
            A330ParametrosSistema_Codigo = P003015_A330ParametrosSistema_Codigo[0];
            AV51ParametrosSistema_URLApp = A1679ParametrosSistema_URLApp;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(12);
         }
         pr_default.close(12);
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_ContagemResultadoAlteraStatus");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV18WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV40ServerNow = (DateTime)(DateTime.MinValue);
         scmdbuf = "";
         P00302_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P00302_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P00302_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         P00302_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         P00302_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00302_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00302_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00302_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00302_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00302_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00302_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         P00302_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         P00302_A1349ContagemResultado_DataExecucao = new DateTime[] {DateTime.MinValue} ;
         P00302_n1349ContagemResultado_DataExecucao = new bool[] {false} ;
         P00302_A1237ContagemResultado_PrazoMaisDias = new short[1] ;
         P00302_n1237ContagemResultado_PrazoMaisDias = new bool[] {false} ;
         P00302_A1227ContagemResultado_PrazoInicialDias = new short[1] ;
         P00302_n1227ContagemResultado_PrazoInicialDias = new bool[] {false} ;
         P00302_A1611ContagemResultado_PrzTpDias = new String[] {""} ;
         P00302_n1611ContagemResultado_PrzTpDias = new bool[] {false} ;
         P00302_A1043ContagemResultado_LiqLogCod = new int[1] ;
         P00302_n1043ContagemResultado_LiqLogCod = new bool[] {false} ;
         P00302_A890ContagemResultado_Responsavel = new int[1] ;
         P00302_n890ContagemResultado_Responsavel = new bool[] {false} ;
         P00302_A457ContagemResultado_Demanda = new String[] {""} ;
         P00302_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00302_A1348ContagemResultado_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         P00302_n1348ContagemResultado_DataHomologacao = new bool[] {false} ;
         P00302_A514ContagemResultado_Observacao = new String[] {""} ;
         P00302_n514ContagemResultado_Observacao = new bool[] {false} ;
         P00302_A1603ContagemResultado_CntCod = new int[1] ;
         P00302_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P00302_A456ContagemResultado_Codigo = new int[1] ;
         P00302_A512ContagemResultado_ValorPF = new decimal[1] ;
         P00302_n512ContagemResultado_ValorPF = new bool[] {false} ;
         A472ContagemResultado_DataEntrega = DateTime.MinValue;
         A912ContagemResultado_HoraEntrega = (DateTime)(DateTime.MinValue);
         A484ContagemResultado_StatusDmn = "";
         A1349ContagemResultado_DataExecucao = (DateTime)(DateTime.MinValue);
         A1611ContagemResultado_PrzTpDias = "";
         A457ContagemResultado_Demanda = "";
         A1348ContagemResultado_DataHomologacao = (DateTime)(DateTime.MinValue);
         A514ContagemResultado_Observacao = "";
         AV20PrazoEntrega = (DateTime)(DateTime.MinValue);
         AV21StatusAnterior = "";
         AV22Acao = "";
         AV48TipoDias = "";
         Gx_date = DateTime.MinValue;
         GXt_dtime3 = (DateTime)(DateTime.MinValue);
         AV31Demanda = "";
         AV30WebSession = context.GetSession();
         AV27Usuarios = new GxSimpleCollection();
         P00305_A74Contrato_Codigo = new int[1] ;
         P00305_A160ContratoServicos_Codigo = new int[1] ;
         P00306_A1078ContratoGestor_ContratoCod = new int[1] ;
         P00306_A1079ContratoGestor_UsuarioCod = new int[1] ;
         P00306_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         P00306_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         AV34Subject = "";
         AV28EmailText = "";
         P00307_A426NaoConformidade_Codigo = new int[1] ;
         P00307_A427NaoConformidade_Nome = new String[] {""} ;
         A427NaoConformidade_Nome = "";
         AV51ParametrosSistema_URLApp = "";
         AV41ContagemResultado_Codigos = new GxSimpleCollection();
         AV32Attachments = new GxSimpleCollection();
         AV33Resultado = "";
         P00308_A892LogResponsavel_DemandaCod = new int[1] ;
         P00308_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         P00308_A896LogResponsavel_Owner = new int[1] ;
         P00308_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00308_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00308_A894LogResponsavel_Acao = new String[] {""} ;
         P00308_A1797LogResponsavel_Codigo = new long[1] ;
         A894LogResponsavel_Acao = "";
         P00309_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         P00309_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         P003010_A74Contrato_Codigo = new int[1] ;
         P003010_A160ContratoServicos_Codigo = new int[1] ;
         P003010_A1013Contrato_PrepostoCod = new int[1] ;
         P003010_n1013Contrato_PrepostoCod = new bool[] {false} ;
         P003011_A1078ContratoGestor_ContratoCod = new int[1] ;
         P003011_A1136ContratoGestor_ContratadaCod = new int[1] ;
         P003011_n1136ContratoGestor_ContratadaCod = new bool[] {false} ;
         P003011_A1079ContratoGestor_UsuarioCod = new int[1] ;
         P003011_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         P003011_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         P003012_A426NaoConformidade_Codigo = new int[1] ;
         P003012_A2029NaoConformidade_Glosavel = new bool[] {false} ;
         P003012_n2029NaoConformidade_Glosavel = new bool[] {false} ;
         P003014_A40000GXC1 = new int[1] ;
         P003015_A1679ParametrosSistema_URLApp = new String[] {""} ;
         P003015_n1679ParametrosSistema_URLApp = new bool[] {false} ;
         P003015_A330ParametrosSistema_Codigo = new int[1] ;
         A1679ParametrosSistema_URLApp = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_contagemresultadoalterastatus__default(),
            new Object[][] {
                new Object[] {
               P00302_A472ContagemResultado_DataEntrega, P00302_n472ContagemResultado_DataEntrega, P00302_A912ContagemResultado_HoraEntrega, P00302_n912ContagemResultado_HoraEntrega, P00302_A484ContagemResultado_StatusDmn, P00302_n484ContagemResultado_StatusDmn, P00302_A490ContagemResultado_ContratadaCod, P00302_n490ContagemResultado_ContratadaCod, P00302_A1553ContagemResultado_CntSrvCod, P00302_n1553ContagemResultado_CntSrvCod,
               P00302_A468ContagemResultado_NaoCnfDmnCod, P00302_n468ContagemResultado_NaoCnfDmnCod, P00302_A1349ContagemResultado_DataExecucao, P00302_n1349ContagemResultado_DataExecucao, P00302_A1237ContagemResultado_PrazoMaisDias, P00302_n1237ContagemResultado_PrazoMaisDias, P00302_A1227ContagemResultado_PrazoInicialDias, P00302_n1227ContagemResultado_PrazoInicialDias, P00302_A1611ContagemResultado_PrzTpDias, P00302_n1611ContagemResultado_PrzTpDias,
               P00302_A1043ContagemResultado_LiqLogCod, P00302_n1043ContagemResultado_LiqLogCod, P00302_A890ContagemResultado_Responsavel, P00302_n890ContagemResultado_Responsavel, P00302_A457ContagemResultado_Demanda, P00302_n457ContagemResultado_Demanda, P00302_A1348ContagemResultado_DataHomologacao, P00302_n1348ContagemResultado_DataHomologacao, P00302_A514ContagemResultado_Observacao, P00302_n514ContagemResultado_Observacao,
               P00302_A1603ContagemResultado_CntCod, P00302_n1603ContagemResultado_CntCod, P00302_A456ContagemResultado_Codigo, P00302_A512ContagemResultado_ValorPF, P00302_n512ContagemResultado_ValorPF
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P00305_A74Contrato_Codigo, P00305_A160ContratoServicos_Codigo
               }
               , new Object[] {
               P00306_A1078ContratoGestor_ContratoCod, P00306_A1079ContratoGestor_UsuarioCod, P00306_A1446ContratoGestor_ContratadaAreaCod, P00306_n1446ContratoGestor_ContratadaAreaCod
               }
               , new Object[] {
               P00307_A426NaoConformidade_Codigo, P00307_A427NaoConformidade_Nome
               }
               , new Object[] {
               P00308_A892LogResponsavel_DemandaCod, P00308_n892LogResponsavel_DemandaCod, P00308_A896LogResponsavel_Owner, P00308_A490ContagemResultado_ContratadaCod, P00308_n490ContagemResultado_ContratadaCod, P00308_A894LogResponsavel_Acao, P00308_A1797LogResponsavel_Codigo
               }
               , new Object[] {
               P00309_A66ContratadaUsuario_ContratadaCod, P00309_A69ContratadaUsuario_UsuarioCod
               }
               , new Object[] {
               P003010_A74Contrato_Codigo, P003010_A160ContratoServicos_Codigo, P003010_A1013Contrato_PrepostoCod, P003010_n1013Contrato_PrepostoCod
               }
               , new Object[] {
               P003011_A1078ContratoGestor_ContratoCod, P003011_A1136ContratoGestor_ContratadaCod, P003011_n1136ContratoGestor_ContratadaCod, P003011_A1079ContratoGestor_UsuarioCod, P003011_A1446ContratoGestor_ContratadaAreaCod, P003011_n1446ContratoGestor_ContratadaAreaCod
               }
               , new Object[] {
               P003012_A426NaoConformidade_Codigo, P003012_A2029NaoConformidade_Glosavel, P003012_n2029NaoConformidade_Glosavel
               }
               , new Object[] {
               P003014_A40000GXC1
               }
               , new Object[] {
               P003015_A1679ParametrosSistema_URLApp, P003015_n1679ParametrosSistema_URLApp, P003015_A330ParametrosSistema_Codigo
               }
            }
         );
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
      }

      private short A1237ContagemResultado_PrazoMaisDias ;
      private short A1227ContagemResultado_PrazoInicialDias ;
      private short AV35PrazoBase ;
      private short GXt_int2 ;
      private short AV59GXLvl130 ;
      private int A456ContagemResultado_Codigo ;
      private int AV9ContagemResultado_NaoCnfDmnCod ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A468ContagemResultado_NaoCnfDmnCod ;
      private int A1043ContagemResultado_LiqLogCod ;
      private int A890ContagemResultado_Responsavel ;
      private int A1603ContagemResultado_CntCod ;
      private int AV43Contratada_Codigo ;
      private int AV42ContratoServicos_Codigo ;
      private int AV50Codigo ;
      private int AV36Usuario_Codigo ;
      private int AV26Prestadora ;
      private int AV44Contrato_Codigo ;
      private int A74Contrato_Codigo ;
      private int A160ContratoServicos_Codigo ;
      private int A1078ContratoGestor_ContratoCod ;
      private int A1079ContratoGestor_UsuarioCod ;
      private int A1446ContratoGestor_ContratadaAreaCod ;
      private int A426NaoConformidade_Codigo ;
      private int A892LogResponsavel_DemandaCod ;
      private int A896LogResponsavel_Owner ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A1013Contrato_PrepostoCod ;
      private int A1136ContratoGestor_ContratadaCod ;
      private int A40000GXC1 ;
      private int A330ParametrosSistema_Codigo ;
      private long AV46LogResponsavel_Codigo ;
      private long A1797LogResponsavel_Codigo ;
      private decimal A512ContagemResultado_ValorPF ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal GXt_decimal1 ;
      private decimal A606ContagemResultado_ValorFinal ;
      private decimal AV45Valor_Debito ;
      private String AV8Status ;
      private String scmdbuf ;
      private String A484ContagemResultado_StatusDmn ;
      private String A1611ContagemResultado_PrzTpDias ;
      private String AV21StatusAnterior ;
      private String AV22Acao ;
      private String AV48TipoDias ;
      private String AV34Subject ;
      private String AV28EmailText ;
      private String A427NaoConformidade_Nome ;
      private String AV33Resultado ;
      private String A894LogResponsavel_Acao ;
      private DateTime AV40ServerNow ;
      private DateTime A912ContagemResultado_HoraEntrega ;
      private DateTime A1349ContagemResultado_DataExecucao ;
      private DateTime A1348ContagemResultado_DataHomologacao ;
      private DateTime AV20PrazoEntrega ;
      private DateTime GXt_dtime3 ;
      private DateTime A472ContagemResultado_DataEntrega ;
      private DateTime Gx_date ;
      private bool AV19Confirmado ;
      private bool n472ContagemResultado_DataEntrega ;
      private bool n912ContagemResultado_HoraEntrega ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n468ContagemResultado_NaoCnfDmnCod ;
      private bool n1349ContagemResultado_DataExecucao ;
      private bool n1237ContagemResultado_PrazoMaisDias ;
      private bool n1227ContagemResultado_PrazoInicialDias ;
      private bool n1611ContagemResultado_PrzTpDias ;
      private bool n1043ContagemResultado_LiqLogCod ;
      private bool n890ContagemResultado_Responsavel ;
      private bool n457ContagemResultado_Demanda ;
      private bool n1348ContagemResultado_DataHomologacao ;
      private bool n514ContagemResultado_Observacao ;
      private bool n1603ContagemResultado_CntCod ;
      private bool n512ContagemResultado_ValorPF ;
      private bool returnInSub ;
      private bool AV49Glosavel ;
      private bool n1446ContratoGestor_ContratadaAreaCod ;
      private bool A1135ContratoGestor_UsuarioEhContratante ;
      private bool n892LogResponsavel_DemandaCod ;
      private bool n1013Contrato_PrepostoCod ;
      private bool n1136ContratoGestor_ContratadaCod ;
      private bool GXt_boolean4 ;
      private bool A2029NaoConformidade_Glosavel ;
      private bool n2029NaoConformidade_Glosavel ;
      private bool n1679ParametrosSistema_URLApp ;
      private String AV10ContagemResultado_Observacao ;
      private String A514ContagemResultado_Observacao ;
      private String A457ContagemResultado_Demanda ;
      private String AV31Demanda ;
      private String AV51ParametrosSistema_URLApp ;
      private String A1679ParametrosSistema_URLApp ;
      private IGxSession AV30WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultado_Codigo ;
      private bool aP4_Confirmado ;
      private IDataStoreProvider pr_default ;
      private DateTime[] P00302_A472ContagemResultado_DataEntrega ;
      private bool[] P00302_n472ContagemResultado_DataEntrega ;
      private DateTime[] P00302_A912ContagemResultado_HoraEntrega ;
      private bool[] P00302_n912ContagemResultado_HoraEntrega ;
      private String[] P00302_A484ContagemResultado_StatusDmn ;
      private bool[] P00302_n484ContagemResultado_StatusDmn ;
      private int[] P00302_A490ContagemResultado_ContratadaCod ;
      private bool[] P00302_n490ContagemResultado_ContratadaCod ;
      private int[] P00302_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00302_n1553ContagemResultado_CntSrvCod ;
      private int[] P00302_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] P00302_n468ContagemResultado_NaoCnfDmnCod ;
      private DateTime[] P00302_A1349ContagemResultado_DataExecucao ;
      private bool[] P00302_n1349ContagemResultado_DataExecucao ;
      private short[] P00302_A1237ContagemResultado_PrazoMaisDias ;
      private bool[] P00302_n1237ContagemResultado_PrazoMaisDias ;
      private short[] P00302_A1227ContagemResultado_PrazoInicialDias ;
      private bool[] P00302_n1227ContagemResultado_PrazoInicialDias ;
      private String[] P00302_A1611ContagemResultado_PrzTpDias ;
      private bool[] P00302_n1611ContagemResultado_PrzTpDias ;
      private int[] P00302_A1043ContagemResultado_LiqLogCod ;
      private bool[] P00302_n1043ContagemResultado_LiqLogCod ;
      private int[] P00302_A890ContagemResultado_Responsavel ;
      private bool[] P00302_n890ContagemResultado_Responsavel ;
      private String[] P00302_A457ContagemResultado_Demanda ;
      private bool[] P00302_n457ContagemResultado_Demanda ;
      private DateTime[] P00302_A1348ContagemResultado_DataHomologacao ;
      private bool[] P00302_n1348ContagemResultado_DataHomologacao ;
      private String[] P00302_A514ContagemResultado_Observacao ;
      private bool[] P00302_n514ContagemResultado_Observacao ;
      private int[] P00302_A1603ContagemResultado_CntCod ;
      private bool[] P00302_n1603ContagemResultado_CntCod ;
      private int[] P00302_A456ContagemResultado_Codigo ;
      private decimal[] P00302_A512ContagemResultado_ValorPF ;
      private bool[] P00302_n512ContagemResultado_ValorPF ;
      private int[] P00305_A74Contrato_Codigo ;
      private int[] P00305_A160ContratoServicos_Codigo ;
      private int[] P00306_A1078ContratoGestor_ContratoCod ;
      private int[] P00306_A1079ContratoGestor_UsuarioCod ;
      private int[] P00306_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] P00306_n1446ContratoGestor_ContratadaAreaCod ;
      private int[] P00307_A426NaoConformidade_Codigo ;
      private String[] P00307_A427NaoConformidade_Nome ;
      private int[] P00308_A892LogResponsavel_DemandaCod ;
      private bool[] P00308_n892LogResponsavel_DemandaCod ;
      private int[] P00308_A896LogResponsavel_Owner ;
      private int[] P00308_A490ContagemResultado_ContratadaCod ;
      private bool[] P00308_n490ContagemResultado_ContratadaCod ;
      private String[] P00308_A894LogResponsavel_Acao ;
      private long[] P00308_A1797LogResponsavel_Codigo ;
      private int[] P00309_A66ContratadaUsuario_ContratadaCod ;
      private int[] P00309_A69ContratadaUsuario_UsuarioCod ;
      private int[] P003010_A74Contrato_Codigo ;
      private int[] P003010_A160ContratoServicos_Codigo ;
      private int[] P003010_A1013Contrato_PrepostoCod ;
      private bool[] P003010_n1013Contrato_PrepostoCod ;
      private int[] P003011_A1078ContratoGestor_ContratoCod ;
      private int[] P003011_A1136ContratoGestor_ContratadaCod ;
      private bool[] P003011_n1136ContratoGestor_ContratadaCod ;
      private int[] P003011_A1079ContratoGestor_UsuarioCod ;
      private int[] P003011_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] P003011_n1446ContratoGestor_ContratadaAreaCod ;
      private int[] P003012_A426NaoConformidade_Codigo ;
      private bool[] P003012_A2029NaoConformidade_Glosavel ;
      private bool[] P003012_n2029NaoConformidade_Glosavel ;
      private int[] P003014_A40000GXC1 ;
      private String[] P003015_A1679ParametrosSistema_URLApp ;
      private bool[] P003015_n1679ParametrosSistema_URLApp ;
      private int[] P003015_A330ParametrosSistema_Codigo ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV27Usuarios ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV41ContagemResultado_Codigos ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV32Attachments ;
      private wwpbaseobjects.SdtWWPContext AV18WWPContext ;
   }

   public class prc_contagemresultadoalterastatus__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00302 ;
          prmP00302 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00303 ;
          prmP00303 = new Object[] {
          new Object[] {"@ContagemResultado_DataEntrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraEntrega",SqlDbType.DateTime,0,5} ,
          new Object[] {"@ContagemResultado_NaoCnfDmnCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataExecucao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_LiqLogCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataHomologacao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_Observacao",SqlDbType.VarChar,20000,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00304 ;
          prmP00304 = new Object[] {
          new Object[] {"@ContagemResultado_DataEntrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraEntrega",SqlDbType.DateTime,0,5} ,
          new Object[] {"@ContagemResultado_NaoCnfDmnCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataExecucao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_LiqLogCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataHomologacao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_Observacao",SqlDbType.VarChar,20000,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00305 ;
          prmP00305 = new Object[] {
          new Object[] {"@AV42ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00306 ;
          prmP00306 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00307 ;
          prmP00307 = new Object[] {
          new Object[] {"@AV9ContagemResultado_NaoCnfDmnCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00308 ;
          prmP00308 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00309 ;
          prmP00309 = new Object[] {
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@LogResponsavel_Owner",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003010 ;
          prmP003010 = new Object[] {
          new Object[] {"@AV42ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003011 ;
          prmP003011 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV43Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003012 ;
          prmP003012 = new Object[] {
          new Object[] {"@AV9ContagemResultado_NaoCnfDmnCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003014 ;
          prmP003014 = new Object[] {
          new Object[] {"@AV50Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003015 ;
          prmP003015 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("P00302", "SELECT TOP 1 T1.[ContagemResultado_DataEntrega], T1.[ContagemResultado_HoraEntrega], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_ContratadaCod], T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_NaoCnfDmnCod], T1.[ContagemResultado_DataExecucao], T1.[ContagemResultado_PrazoMaisDias], T1.[ContagemResultado_PrazoInicialDias], T2.[ContratoServicos_PrazoTpDias] AS ContagemResultado_PrzTpDias, T1.[ContagemResultado_LiqLogCod], T1.[ContagemResultado_Responsavel], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_DataHomologacao], T1.[ContagemResultado_Observacao], T2.[Contrato_Codigo] AS ContagemResultado_CntCod, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_ValorPF] FROM ([ContagemResultado] T1 WITH (UPDLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) WHERE T1.[ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00302,1,0,true,true )
             ,new CursorDef("P00303", "UPDATE [ContagemResultado] SET [ContagemResultado_DataEntrega]=@ContagemResultado_DataEntrega, [ContagemResultado_HoraEntrega]=@ContagemResultado_HoraEntrega, [ContagemResultado_NaoCnfDmnCod]=@ContagemResultado_NaoCnfDmnCod, [ContagemResultado_DataExecucao]=@ContagemResultado_DataExecucao, [ContagemResultado_LiqLogCod]=@ContagemResultado_LiqLogCod, [ContagemResultado_Responsavel]=@ContagemResultado_Responsavel, [ContagemResultado_DataHomologacao]=@ContagemResultado_DataHomologacao, [ContagemResultado_Observacao]=@ContagemResultado_Observacao  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00303)
             ,new CursorDef("P00304", "UPDATE [ContagemResultado] SET [ContagemResultado_DataEntrega]=@ContagemResultado_DataEntrega, [ContagemResultado_HoraEntrega]=@ContagemResultado_HoraEntrega, [ContagemResultado_NaoCnfDmnCod]=@ContagemResultado_NaoCnfDmnCod, [ContagemResultado_DataExecucao]=@ContagemResultado_DataExecucao, [ContagemResultado_LiqLogCod]=@ContagemResultado_LiqLogCod, [ContagemResultado_Responsavel]=@ContagemResultado_Responsavel, [ContagemResultado_DataHomologacao]=@ContagemResultado_DataHomologacao, [ContagemResultado_Observacao]=@ContagemResultado_Observacao  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00304)
             ,new CursorDef("P00305", "SELECT TOP 1 [Contrato_Codigo], [ContratoServicos_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @AV42ContratoServicos_Codigo ORDER BY [ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00305,1,0,true,true )
             ,new CursorDef("P00306", "SELECT T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T1.[ContratoGestor_UsuarioCod], T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod FROM ([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) WHERE T1.[ContratoGestor_ContratoCod] = @Contrato_Codigo ORDER BY T1.[ContratoGestor_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00306,100,0,true,false )
             ,new CursorDef("P00307", "SELECT [NaoConformidade_Codigo], [NaoConformidade_Nome] FROM [NaoConformidade] WITH (NOLOCK) WHERE [NaoConformidade_Codigo] = @AV9ContagemResultado_NaoCnfDmnCod ORDER BY [NaoConformidade_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00307,1,0,false,true )
             ,new CursorDef("P00308", "SELECT T1.[LogResponsavel_DemandaCod] AS LogResponsavel_DemandaCod, T1.[LogResponsavel_Owner], T2.[ContagemResultado_ContratadaCod], T1.[LogResponsavel_Acao], T1.[LogResponsavel_Codigo] FROM ([LogResponsavel] T1 WITH (NOLOCK) LEFT JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[LogResponsavel_DemandaCod]) WHERE (T1.[LogResponsavel_DemandaCod] = @ContagemResultado_Codigo) AND (T1.[LogResponsavel_Acao] = 'V') ORDER BY T1.[LogResponsavel_DemandaCod], T1.[LogResponsavel_Codigo] DESC ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00308,100,0,true,false )
             ,new CursorDef("P00309", "SELECT TOP 1 [ContratadaUsuario_ContratadaCod], [ContratadaUsuario_UsuarioCod] FROM [ContratadaUsuario] WITH (NOLOCK) WHERE [ContratadaUsuario_ContratadaCod] = @ContagemResultado_ContratadaCod and [ContratadaUsuario_UsuarioCod] = @LogResponsavel_Owner ORDER BY [ContratadaUsuario_ContratadaCod], [ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00309,1,0,false,true )
             ,new CursorDef("P003010", "SELECT TOP 1 T1.[Contrato_Codigo], T1.[ContratoServicos_Codigo], T2.[Contrato_PrepostoCod] FROM ([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE T1.[ContratoServicos_Codigo] = @AV42ContratoServicos_Codigo ORDER BY T1.[ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003010,1,0,true,true )
             ,new CursorDef("P003011", "SELECT T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T2.[Contratada_Codigo] AS ContratoGestor_ContratadaCod, T1.[ContratoGestor_UsuarioCod], T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod FROM ([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) WHERE (T1.[ContratoGestor_ContratoCod] = @Contrato_Codigo) AND (T2.[Contratada_Codigo] = @AV43Contratada_Codigo) ORDER BY T1.[ContratoGestor_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003011,100,0,true,false )
             ,new CursorDef("P003012", "SELECT TOP 1 [NaoConformidade_Codigo], [NaoConformidade_Glosavel] FROM [NaoConformidade] WITH (NOLOCK) WHERE [NaoConformidade_Codigo] = @AV9ContagemResultado_NaoCnfDmnCod ORDER BY [NaoConformidade_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003012,1,0,false,true )
             ,new CursorDef("P003014", "SELECT COALESCE( T1.[GXC1], 0) AS GXC1 FROM (SELECT MAX([ContagemResultadoNaoCnf_Codigo]) AS GXC1 FROM [ContagemResultadoNaoCnf] WITH (NOLOCK) WHERE ([ContagemResultadoNaoCnf_Contestada] = 1) AND ([ContagemResultadoNaoCnf_OSCod] = @AV50Codigo) ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003014,1,0,true,true )
             ,new CursorDef("P003015", "SELECT TOP 1 [ParametrosSistema_URLApp], [ParametrosSistema_Codigo] FROM [ParametrosSistema] WITH (NOLOCK) ORDER BY [ParametrosSistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003015,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[12])[0] = rslt.getGXDateTime(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((short[]) buf[14])[0] = rslt.getShort(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((short[]) buf[16])[0] = rslt.getShort(9) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((String[]) buf[18])[0] = rslt.getString(10, 1) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((int[]) buf[20])[0] = rslt.getInt(11) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(11);
                ((int[]) buf[22])[0] = rslt.getInt(12) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(12);
                ((String[]) buf[24])[0] = rslt.getVarchar(13) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(13);
                ((DateTime[]) buf[26])[0] = rslt.getGXDateTime(14) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(14);
                ((String[]) buf[28])[0] = rslt.getLongVarchar(15) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(15);
                ((int[]) buf[30])[0] = rslt.getInt(16) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(16);
                ((int[]) buf[32])[0] = rslt.getInt(17) ;
                ((decimal[]) buf[33])[0] = rslt.getDecimal(18) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(18);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 20) ;
                ((long[]) buf[6])[0] = rslt.getLong(5) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(2, (DateTime)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(4, (DateTime)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 7 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(7, (DateTime)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 8 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[15]);
                }
                stmt.SetParameter(9, (int)parms[16]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(1, (DateTime)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(2, (DateTime)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(4, (DateTime)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 7 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(7, (DateTime)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 8 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[15]);
                }
                stmt.SetParameter(9, (int)parms[16]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
