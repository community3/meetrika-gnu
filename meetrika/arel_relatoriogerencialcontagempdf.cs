/*
               File: REL_RelatorioGerencialContagemPDF
        Description: REL_Relatorio Gerencial Contagem PDF
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:18:25.87
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.Printer;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class arel_relatoriogerencialcontagempdf : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      public override void webExecute( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            if ( ! entryPointCalled )
            {
               AV48ContagemResultado_DataDmnIn = context.localUtil.ParseDateParm( gxfirstwebparm);
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV47ContagemResultado_DataDmnFim = context.localUtil.ParseDateParm( GetNextPar( ));
                  AV17ContagemResultado_ContratadaOrigemCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV14ContagemResultado_CntSrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV13ContagemResultado_StatusDmn = GetNextPar( );
               }
            }
         }
         if ( GxWebError == 0 )
         {
            executePrivate();
         }
         cleanup();
      }

      public arel_relatoriogerencialcontagempdf( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public arel_relatoriogerencialcontagempdf( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( DateTime aP0_ContagemResultado_DataDmnIn ,
                           DateTime aP1_ContagemResultado_DataDmnFim ,
                           int aP2_ContagemResultado_ContratadaOrigemCod ,
                           int aP3_ContagemResultado_CntSrvCod ,
                           String aP4_ContagemResultado_StatusDmn )
      {
         this.AV48ContagemResultado_DataDmnIn = aP0_ContagemResultado_DataDmnIn;
         this.AV47ContagemResultado_DataDmnFim = aP1_ContagemResultado_DataDmnFim;
         this.AV17ContagemResultado_ContratadaOrigemCod = aP2_ContagemResultado_ContratadaOrigemCod;
         this.AV14ContagemResultado_CntSrvCod = aP3_ContagemResultado_CntSrvCod;
         this.AV13ContagemResultado_StatusDmn = aP4_ContagemResultado_StatusDmn;
         initialize();
         executePrivate();
      }

      public void executeSubmit( DateTime aP0_ContagemResultado_DataDmnIn ,
                                 DateTime aP1_ContagemResultado_DataDmnFim ,
                                 int aP2_ContagemResultado_ContratadaOrigemCod ,
                                 int aP3_ContagemResultado_CntSrvCod ,
                                 String aP4_ContagemResultado_StatusDmn )
      {
         arel_relatoriogerencialcontagempdf objarel_relatoriogerencialcontagempdf;
         objarel_relatoriogerencialcontagempdf = new arel_relatoriogerencialcontagempdf();
         objarel_relatoriogerencialcontagempdf.AV48ContagemResultado_DataDmnIn = aP0_ContagemResultado_DataDmnIn;
         objarel_relatoriogerencialcontagempdf.AV47ContagemResultado_DataDmnFim = aP1_ContagemResultado_DataDmnFim;
         objarel_relatoriogerencialcontagempdf.AV17ContagemResultado_ContratadaOrigemCod = aP2_ContagemResultado_ContratadaOrigemCod;
         objarel_relatoriogerencialcontagempdf.AV14ContagemResultado_CntSrvCod = aP3_ContagemResultado_CntSrvCod;
         objarel_relatoriogerencialcontagempdf.AV13ContagemResultado_StatusDmn = aP4_ContagemResultado_StatusDmn;
         objarel_relatoriogerencialcontagempdf.context.SetSubmitInitialConfig(context);
         objarel_relatoriogerencialcontagempdf.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objarel_relatoriogerencialcontagempdf);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((arel_relatoriogerencialcontagempdf)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         M_top = 0;
         M_bot = 6;
         P_lines = (int)(66-M_bot);
         getPrinter().GxClearAttris() ;
         add_metrics( ) ;
         lineHeight = 15;
         PrtOffset = 0;
         gxXPage = 100;
         gxYPage = 100;
         getPrinter().GxSetDocName("") ;
         try
         {
            Gx_out = "FIL" ;
            if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 2, 256, 11909, 22262, 0, 1, 1, 0, 1, 1) )
            {
               cleanup();
               return;
            }
            getPrinter().setModal(false) ;
            P_lines = (int)(gxYPage-(lineHeight*6));
            Gx_line = (int)(P_lines+1);
            getPrinter().setPageLines(P_lines);
            getPrinter().setLineHeight(lineHeight);
            getPrinter().setM_top(M_top);
            getPrinter().setM_bot(M_bot);
            new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV15WWPContext) ;
            AV25Contrato_AreaTrabalhoCod = AV15WWPContext.gxTpr_Areatrabalho_codigo;
            GXt_char1 = AV8TituloDataHora;
            new prc_datahoraporextenso(context ).execute(  2, out  GXt_char1) ;
            AV8TituloDataHora = GXt_char1;
            GXt_char1 = AV8TituloDataHora;
            new prc_usuarionome(context ).execute( out  GXt_char1) ;
            AV8TituloDataHora = AV8TituloDataHora + " Usu�rio: " + GXt_char1;
            AV32IsPrintOrigemReferencia = false;
            AV33IsPrintRotulos = false;
            GXt_objcol_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem2 = AV26SDT_RelatorioGerencial;
            new prc_gerarelatoriogerencialcontagem(context ).execute(  AV25Contrato_AreaTrabalhoCod,  AV48ContagemResultado_DataDmnIn,  AV47ContagemResultado_DataDmnFim,  AV17ContagemResultado_ContratadaOrigemCod,  AV14ContagemResultado_CntSrvCod,  AV13ContagemResultado_StatusDmn, out  GXt_objcol_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem2) ;
            AV26SDT_RelatorioGerencial = GXt_objcol_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem2;
            if ( ! (0==AV26SDT_RelatorioGerencial.Count) )
            {
               AV62GXV1 = 1;
               while ( AV62GXV1 <= AV26SDT_RelatorioGerencial.Count )
               {
                  AV27SDT_RelatorioGerencialContagem = ((SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem)AV26SDT_RelatorioGerencial.Item(AV62GXV1));
                  AV9ContagemResultado_ContratadaOrigemPesNom = AV27SDT_RelatorioGerencialContagem.gxTpr_Contagemresultado_contratadaorigempesnom;
                  HXR0( false, 39) ;
                  getPrinter().GxAttris("Calibri", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Origem da Refer�ncia:", 0, Gx_line+17, 158, Gx_line+36, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV9ContagemResultado_ContratadaOrigemPesNom, "@!")), 158, Gx_line+17, 883, Gx_line+36, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+39);
                  AV32IsPrintOrigemReferencia = true;
                  HXR0( false, 18) ;
                  getPrinter().GxDrawLine(0, Gx_line+17, 1525, Gx_line+17, 1, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Calibri", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("PFL FS Inicial", 992, Gx_line+0, 1084, Gx_line+18, 1, 0, 0, 0) ;
                  getPrinter().GxDrawText("Servi�o", 672, Gx_line+0, 861, Gx_line+18, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText("Status", 575, Gx_line+0, 667, Gx_line+18, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText("Diverg�ncia", 1318, Gx_line+0, 1393, Gx_line+18, 1, 0, 0, 0) ;
                  getPrinter().GxDrawText("PFL FS - Final ", 1215, Gx_line+0, 1307, Gx_line+18, 1, 0, 0, 0) ;
                  getPrinter().GxDrawText("Data", 0, Gx_line+0, 58, Gx_line+18, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText("Data da Contagem", 874, Gx_line+0, 979, Gx_line+18, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("Descri��o", 200, Gx_line+0, 455, Gx_line+18, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText("N�| N�Ref.", 67, Gx_line+0, 192, Gx_line+18, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText("Data da Contagem", 1097, Gx_line+0, 1202, Gx_line+18, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("Total PF", 1403, Gx_line+0, 1527, Gx_line+18, 1, 0, 0, 0) ;
                  getPrinter().GxDrawText("OS Refer�ncia", 458, Gx_line+0, 566, Gx_line+18, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+18);
                  AV33IsPrintRotulos = true;
                  AV63GXV2 = 1;
                  while ( AV63GXV2 <= AV27SDT_RelatorioGerencialContagem.gxTpr_Os.Count )
                  {
                     AV28SDT_RelatorioGerencialContagemOS = ((SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS)AV27SDT_RelatorioGerencialContagem.gxTpr_Os.Item(AV63GXV2));
                     /* Execute user subroutine: 'RESET.VARIAVEIS' */
                     S121 ();
                     if ( returnInSub )
                     {
                        this.cleanup();
                        if (true) return;
                     }
                     AV37ContagemResultado_Codigo = AV28SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_codigo;
                     AV12ContagemResultado_DataDmn = AV28SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_datadmn;
                     AV30ContagemResultado_Descricao = AV28SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_descricao;
                     AV11NumeroOSFM_Referencia = AV28SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_nnref;
                     AV10Status = AV28SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_statusdescricao;
                     AV31ContagemResultado_ServicoSigla = AV28SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_servicosigla;
                     AV19ContagemResultado_DataCntInicial = AV28SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_datacontageminicial;
                     AV22ContagemResultado_PFLFS_Inicial = AV28SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pflfs_inicial;
                     AV20ContagemResultado_DataCntFinal = AV28SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_datacontagemfinal;
                     AV23ContagemResultado_PFLFS_Final = AV28SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pflfs_final;
                     AV42ContagemResultado_PF = AV28SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pf;
                     HXR0( false, 15) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV30ContagemResultado_Descricao, "")), 200, Gx_line+0, 455, Gx_line+15, 0, 0, 0, 0) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV31ContagemResultado_ServicoSigla, "@!")), 672, Gx_line+0, 861, Gx_line+15, 0, 0, 0, 0) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV10Status, "")), 575, Gx_line+0, 667, Gx_line+15, 0, 0, 0, 0) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV11NumeroOSFM_Referencia, "")), 67, Gx_line+0, 192, Gx_line+15, 0, 0, 0, 0) ;
                     getPrinter().GxDrawText(context.localUtil.Format( AV12ContagemResultado_DataDmn, "99/99/9999"), 0, Gx_line+0, 58, Gx_line+15, 0, 0, 0, 0) ;
                     getPrinter().GxDrawText(context.localUtil.Format( AV19ContagemResultado_DataCntInicial, "99/99/9999"), 874, Gx_line+0, 979, Gx_line+15, 1, 0, 0, 0) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV22ContagemResultado_PFLFS_Inicial, "ZZ,ZZZ,ZZ9.999")), 992, Gx_line+0, 1084, Gx_line+15, 1, 0, 0, 0) ;
                     getPrinter().GxDrawText(context.localUtil.Format( AV20ContagemResultado_DataCntFinal, "99/99/9999"), 1097, Gx_line+0, 1202, Gx_line+15, 1, 0, 0, 0) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV23ContagemResultado_PFLFS_Final, "ZZ,ZZZ,ZZ9.999")), 1215, Gx_line+0, 1307, Gx_line+15, 1, 0, 0, 0) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV24ContagemResultado_PFLFS_Diferenca, "ZZ,ZZZ,ZZ9.999")), 1318, Gx_line+0, 1394, Gx_line+15, 1, 0, 0, 0) ;
                     getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV42ContagemResultado_PF, "ZZ,ZZZ,ZZ9.999")), 1403, Gx_line+0, 1527, Gx_line+15, 1, 0, 0, 0) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV34NumeroOSFM_Vinvulada, "")), 458, Gx_line+0, 566, Gx_line+15, 0, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+15);
                     AV36IsPrintTotal1 = false;
                     AV64GXV3 = 1;
                     while ( AV64GXV3 <= AV28SDT_RelatorioGerencialContagemOS.gxTpr_Osvinculada.Count )
                     {
                        AV29SDT_RelatorioGerencialContagemOSVinculada = ((SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_OSVinculada)AV28SDT_RelatorioGerencialContagemOS.gxTpr_Osvinculada.Item(AV64GXV3));
                        /* Execute user subroutine: 'RESET.VARIAVEIS' */
                        S121 ();
                        if ( returnInSub )
                        {
                           this.cleanup();
                           if (true) return;
                        }
                        AV37ContagemResultado_Codigo = AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_codigo;
                        AV34NumeroOSFM_Vinvulada = AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_nnrefvinculada;
                        AV12ContagemResultado_DataDmn = AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_datadmn;
                        AV30ContagemResultado_Descricao = AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_descricao;
                        AV11NumeroOSFM_Referencia = AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_nnref;
                        AV10Status = AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_statusdescricao;
                        AV31ContagemResultado_ServicoSigla = AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_servicosigla;
                        AV19ContagemResultado_DataCntInicial = AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_datacontageminicial;
                        AV22ContagemResultado_PFLFS_Inicial = AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_pflfs_inicial;
                        AV20ContagemResultado_DataCntFinal = AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_datacontagemfinal;
                        AV23ContagemResultado_PFLFS_Final = AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_pflfs_final;
                        AV42ContagemResultado_PF = AV29SDT_RelatorioGerencialContagemOSVinculada.gxTpr_Contagemresultado_pf;
                        HXR0( false, 15) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV30ContagemResultado_Descricao, "")), 200, Gx_line+0, 455, Gx_line+15, 0, 0, 0, 0) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV31ContagemResultado_ServicoSigla, "@!")), 672, Gx_line+0, 861, Gx_line+15, 0, 0, 0, 0) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV10Status, "")), 575, Gx_line+0, 667, Gx_line+15, 0, 0, 0, 0) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV11NumeroOSFM_Referencia, "")), 67, Gx_line+0, 192, Gx_line+15, 0, 0, 0, 0) ;
                        getPrinter().GxDrawText(context.localUtil.Format( AV12ContagemResultado_DataDmn, "99/99/9999"), 0, Gx_line+0, 58, Gx_line+15, 0, 0, 0, 0) ;
                        getPrinter().GxDrawText(context.localUtil.Format( AV19ContagemResultado_DataCntInicial, "99/99/9999"), 874, Gx_line+0, 979, Gx_line+15, 1, 0, 0, 0) ;
                        getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV22ContagemResultado_PFLFS_Inicial, "ZZ,ZZZ,ZZ9.999")), 992, Gx_line+0, 1084, Gx_line+15, 1, 0, 0, 0) ;
                        getPrinter().GxDrawText(context.localUtil.Format( AV20ContagemResultado_DataCntFinal, "99/99/9999"), 1097, Gx_line+0, 1202, Gx_line+15, 1, 0, 0, 0) ;
                        getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV23ContagemResultado_PFLFS_Final, "ZZ,ZZZ,ZZ9.999")), 1215, Gx_line+0, 1307, Gx_line+15, 1, 0, 0, 0) ;
                        getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV24ContagemResultado_PFLFS_Diferenca, "ZZ,ZZZ,ZZ9.999")), 1318, Gx_line+0, 1394, Gx_line+15, 1, 0, 0, 0) ;
                        getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV42ContagemResultado_PF, "ZZ,ZZZ,ZZ9.999")), 1403, Gx_line+0, 1527, Gx_line+15, 1, 0, 0, 0) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV34NumeroOSFM_Vinvulada, "")), 458, Gx_line+0, 566, Gx_line+15, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+15);
                        AV36IsPrintTotal1 = true;
                        AV64GXV3 = (int)(AV64GXV3+1);
                     }
                     if ( AV36IsPrintTotal1 )
                     {
                        AV38ContagemResultado_PFLFS_Inicial_Total = AV28SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pflfs_inicial_total;
                        AV39ContagemResultado_PFLFS_Final_Total = AV28SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pflfs_final_total;
                        AV40ContagemResultado_PFLFS_Diferenca_Total = AV28SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pflfs_diferenca_total;
                        AV41ContagemResultado_PF_Total = AV28SDT_RelatorioGerencialContagemOS.gxTpr_Contagemresultado_pf_total;
                        HXR0( false, 33) ;
                        getPrinter().GxDrawLine(875, Gx_line+0, 1525, Gx_line+0, 1, 0, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV38ContagemResultado_PFLFS_Inicial_Total, "ZZ,ZZZ,ZZ9.999")), 992, Gx_line+0, 1084, Gx_line+15, 1, 0, 0, 0) ;
                        getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV39ContagemResultado_PFLFS_Final_Total, "ZZ,ZZZ,ZZ9.999")), 1215, Gx_line+0, 1307, Gx_line+15, 1, 0, 0, 0) ;
                        getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV40ContagemResultado_PFLFS_Diferenca_Total, "ZZ,ZZZ,ZZ9.999")), 1318, Gx_line+0, 1394, Gx_line+15, 1, 0, 0, 0) ;
                        getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV41ContagemResultado_PF_Total, "ZZ,ZZZ,ZZ9.999")), 1403, Gx_line+0, 1527, Gx_line+15, 1, 0, 0, 0) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText("Diverg�ncias:", 874, Gx_line+0, 979, Gx_line+14, 0, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+33);
                     }
                     HXR0( false, 14) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+14);
                     AV63GXV2 = (int)(AV63GXV2+1);
                  }
                  AV58TotalOS = (short)(AV58TotalOS+(AV27SDT_RelatorioGerencialContagem.gxTpr_Totalos));
                  AV59TotalOSDivergentes = (short)(AV59TotalOSDivergentes+(AV27SDT_RelatorioGerencialContagem.gxTpr_Totalosdivergentes));
                  AV62GXV1 = (int)(AV62GXV1+1);
               }
               if ( ! (0==AV59TotalOSDivergentes) )
               {
                  AV56PercentualOSDivergentes = (decimal)((AV58TotalOS*AV59TotalOSDivergentes)/ (decimal)(100));
               }
               else
               {
                  AV56PercentualOSDivergentes = 0;
               }
               HXR0( false, 120) ;
               getPrinter().GxDrawLine(0, Gx_line+34, 1525, Gx_line+34, 1, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV59TotalOSDivergentes), "ZZZ9")), 201, Gx_line+66, 334, Gx_line+83, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Total de OS:", 25, Gx_line+50, 192, Gx_line+68, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV58TotalOS), "ZZZ9")), 201, Gx_line+50, 334, Gx_line+67, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( AV56PercentualOSDivergentes, "ZZ9.99")), 201, Gx_line+84, 334, Gx_line+101, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Total de OS com Diverg�ncia:", 25, Gx_line+67, 192, Gx_line+85, 0, 0, 0, 0) ;
               getPrinter().GxDrawText("Diverg�ncias (%):", 25, Gx_line+83, 192, Gx_line+101, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Percentual de Diverg�ncias:", 0, Gx_line+16, 192, Gx_line+35, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+120);
               HXR0( false, 33) ;
               getPrinter().GxDrawLine(0, Gx_line+17, 1525, Gx_line+17, 1, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Pontos de Fun��o Divergentes:", 0, Gx_line+0, 208, Gx_line+19, 0, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+33);
               AV65GXV4 = 1;
               while ( AV65GXV4 <= AV26SDT_RelatorioGerencial.Count )
               {
                  AV27SDT_RelatorioGerencialContagem = ((SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem)AV26SDT_RelatorioGerencial.Item(AV65GXV4));
                  AV9ContagemResultado_ContratadaOrigemPesNom = AV27SDT_RelatorioGerencialContagem.gxTpr_Contagemresultado_contratadaorigempesnom;
                  HXR0( false, 19) ;
                  getPrinter().GxAttris("Calibri", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV9ContagemResultado_ContratadaOrigemPesNom, "@!")), 201, Gx_line+0, 527, Gx_line+19, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText("Origem da Refer�ncia:", 25, Gx_line+0, 192, Gx_line+18, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText("Pontos de Fun��o:", 533, Gx_line+0, 642, Gx_line+18, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV27SDT_RelatorioGerencialContagem.gxTpr_Totalpfdivergentes, "ZZ,ZZZ,ZZ9.999"), 642, Gx_line+0, 745, Gx_line+18, 2, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+19);
                  AV65GXV4 = (int)(AV65GXV4+1);
               }
            }
            else
            {
               HXR0( false, 50) ;
               getPrinter().GxAttris("Calibri", 14, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("N�o Foram encotrados dados para os filtros informados.", 0, Gx_line+17, 1525, Gx_line+50, 1, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+50);
            }
            /* Print footer for last page */
            ToSkip = (int)(P_lines+1);
            HXR0( true, 0) ;
         }
         catch ( GeneXus.Printer.ProcessInterruptedException e )
         {
         }
         finally
         {
            /* Close printer file */
            try
            {
               getPrinter().GxEndPage() ;
               getPrinter().GxEndDocument() ;
            }
            catch ( GeneXus.Printer.ProcessInterruptedException e )
            {
            }
            endPrinter();
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      protected void S112( )
      {
         /* 'EXIBE.FILTROS.APLICADOS' Routine */
         /* Using cursor P00XR3 */
         pr_default.execute(0, new Object[] {AV17ContagemResultado_ContratadaOrigemCod});
         if ( (pr_default.getStatus(0) != 101) )
         {
            A40000Contratada_PessoaNom = P00XR3_A40000Contratada_PessoaNom[0];
         }
         else
         {
            A40000Contratada_PessoaNom = "";
         }
         pr_default.close(0);
         /* Using cursor P00XR5 */
         pr_default.execute(1, new Object[] {AV14ContagemResultado_CntSrvCod, AV25Contrato_AreaTrabalhoCod});
         if ( (pr_default.getStatus(1) != 101) )
         {
            A40001ContratoServicos_Alias = P00XR5_A40001ContratoServicos_Alias[0];
         }
         else
         {
            A40001ContratoServicos_Alias = "";
         }
         pr_default.close(1);
         AV16Filtro = "";
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15WWPContext.gxTpr_Areatrabalho_descricao)) )
         {
            AV16Filtro = StringUtil.Format( "�rea de Trabalho: %1", AV15WWPContext.gxTpr_Areatrabalho_descricao, "", "", "", "", "", "", "", "");
            HXR0( false, 20) ;
            getPrinter().GxAttris("Calibri", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV16Filtro, "")), 0, Gx_line+0, 750, Gx_line+20, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! (DateTime.MinValue==AV48ContagemResultado_DataDmnIn) && (DateTime.MinValue==AV47ContagemResultado_DataDmnFim) )
         {
            AV16Filtro = StringUtil.Format( "Data da Demanda: %1 � %2", context.localUtil.DToC( AV48ContagemResultado_DataDmnIn, 0, "-"), context.localUtil.DToC( AV47ContagemResultado_DataDmnFim, 0, "-"), "", "", "", "", "", "", "");
            HXR0( false, 20) ;
            getPrinter().GxAttris("Calibri", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV16Filtro, "")), 0, Gx_line+0, 750, Gx_line+20, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! (0==AV17ContagemResultado_ContratadaOrigemCod) )
         {
            AV54Contratada_PessoaNom = A40000Contratada_PessoaNom;
            AV16Filtro = StringUtil.Format( "Origem da Refer�ncia: %1", AV54Contratada_PessoaNom, "", "", "", "", "", "", "", "");
            HXR0( false, 20) ;
            getPrinter().GxAttris("Calibri", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV16Filtro, "")), 0, Gx_line+0, 750, Gx_line+20, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! (0==AV14ContagemResultado_CntSrvCod) )
         {
            AV55ContratoServicos_Alias = A40001ContratoServicos_Alias;
            AV16Filtro = StringUtil.Format( "Servi�o: %1", AV55ContratoServicos_Alias, "", "", "", "", "", "", "", "");
            HXR0( false, 20) ;
            getPrinter().GxAttris("Calibri", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV16Filtro, "")), 0, Gx_line+0, 750, Gx_line+20, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13ContagemResultado_StatusDmn)) )
         {
            AV16Filtro = StringUtil.Format( "Status: %1", gxdomainstatusdemanda.getDescription(context,AV13ContagemResultado_StatusDmn), "", "", "", "", "", "", "", "");
            HXR0( false, 20) ;
            getPrinter().GxAttris("Calibri", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV16Filtro, "")), 0, Gx_line+0, 750, Gx_line+20, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+20);
         }
      }

      protected void S121( )
      {
         /* 'RESET.VARIAVEIS' Routine */
         AV34NumeroOSFM_Vinvulada = "";
         AV12ContagemResultado_DataDmn = DateTime.MinValue;
         AV30ContagemResultado_Descricao = "";
         AV11NumeroOSFM_Referencia = "";
         AV10Status = "";
         AV31ContagemResultado_ServicoSigla = "";
         AV19ContagemResultado_DataCntInicial = DateTime.MinValue;
         AV22ContagemResultado_PFLFS_Inicial = 0;
         AV20ContagemResultado_DataCntFinal = DateTime.MinValue;
         AV23ContagemResultado_PFLFS_Final = 0;
         AV42ContagemResultado_PF = 0;
      }

      protected void HXR0( bool bFoot ,
                           int Inc )
      {
         /* Skip the required number of lines */
         while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
         {
            if ( Gx_line + Inc >= P_lines )
            {
               if ( Gx_page > 0 )
               {
                  /* Print footers */
                  Gx_line = P_lines;
                  getPrinter().GxDrawLine(0, Gx_line+0, 1525, Gx_line+0, 1, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 9, false, false, false, false, 0, 105, 105, 105, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV8TituloDataHora, "")), 0, Gx_line+0, 593, Gx_line+15, 0, 0, 0, 1) ;
                  getPrinter().GxDrawText("{{pages}}", 1495, Gx_line+0, 1523, Gx_line+15, 0, 0, 0, 1) ;
                  getPrinter().GxDrawText("de", 1477, Gx_line+0, 1496, Gx_line+15, 0, 0, 0, 1) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(Gx_page), "ZZZZZ9")), 1453, Gx_line+0, 1475, Gx_line+15, 0, 0, 0, 1) ;
                  getPrinter().GxDrawText("P�gina:", 1403, Gx_line+0, 1451, Gx_line+15, 0, 0, 0, 1) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+15);
                  getPrinter().GxEndPage() ;
                  if ( bFoot )
                  {
                     return  ;
                  }
               }
               ToSkip = 0;
               Gx_line = 0;
               Gx_page = (int)(Gx_page+1);
               /* Skip Margin Top Lines */
               Gx_line = (int)(Gx_line+(M_top*lineHeight));
               /* Print headers */
               getPrinter().GxStartPage() ;
               getPrinter().GxAttris("Calibri", 16, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Relat�rio Gerencial", 0, Gx_line+0, 1525, Gx_line+33, 1, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+99);
               /* Execute user subroutine: 'EXIBE.FILTROS.APLICADOS' */
               S112 ();
               if ( returnInSub )
               {
                  this.cleanup();
                  if (true) return;
               }
               if ( AV32IsPrintOrigemReferencia )
               {
                  getPrinter().GxAttris("Calibri", 11, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("Origem da Refer�ncia:", 0, Gx_line+17, 158, Gx_line+36, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV9ContagemResultado_ContratadaOrigemPesNom, "@!")), 158, Gx_line+17, 883, Gx_line+36, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+39);
               }
               if ( AV33IsPrintRotulos )
               {
                  getPrinter().GxDrawLine(0, Gx_line+17, 1525, Gx_line+17, 1, 0, 0, 0, 0) ;
                  getPrinter().GxAttris("Calibri", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("PFL FS Inicial", 992, Gx_line+0, 1084, Gx_line+18, 1, 0, 0, 0) ;
                  getPrinter().GxDrawText("Servi�o", 672, Gx_line+0, 861, Gx_line+18, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText("Status", 575, Gx_line+0, 667, Gx_line+18, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText("Diverg�ncia", 1318, Gx_line+0, 1393, Gx_line+18, 1, 0, 0, 0) ;
                  getPrinter().GxDrawText("PFL FS - Final ", 1215, Gx_line+0, 1307, Gx_line+18, 1, 0, 0, 0) ;
                  getPrinter().GxDrawText("Data", 0, Gx_line+0, 58, Gx_line+18, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText("Data da Contagem", 874, Gx_line+0, 979, Gx_line+18, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("Descri��o", 200, Gx_line+0, 455, Gx_line+18, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText("N�| N�Ref.", 67, Gx_line+0, 192, Gx_line+18, 0, 0, 0, 0) ;
                  getPrinter().GxDrawText("Data da Contagem", 1097, Gx_line+0, 1202, Gx_line+18, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("Total PF", 1403, Gx_line+0, 1527, Gx_line+18, 1, 0, 0, 0) ;
                  getPrinter().GxDrawText("OS Refer�ncia", 458, Gx_line+0, 566, Gx_line+18, 0, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+18);
               }
               if (true) break;
            }
            else
            {
               PrtOffset = 0;
               Gx_line = (int)(Gx_line+1);
            }
            ToSkip = (int)(ToSkip-1);
         }
         getPrinter().setPage(Gx_page);
      }

      protected void add_metrics( )
      {
         add_metrics0( ) ;
         add_metrics1( ) ;
         add_metrics2( ) ;
      }

      protected void add_metrics0( )
      {
         getPrinter().setMetrics("Calibri", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      protected void add_metrics1( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      protected void add_metrics2( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if (IsMain)	waitPrinterEnd();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXKey = "";
         gxfirstwebparm = "";
         AV15WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TituloDataHora = "";
         GXt_char1 = "";
         AV26SDT_RelatorioGerencial = new GxObjectCollection( context, "SDT_RelatorioGerencialContagem.SDT_RelatorioGerencialContagem", "GxEv3Up14_Meetrika", "SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem", "GeneXus.Programs");
         GXt_objcol_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem2 = new GxObjectCollection( context, "SDT_RelatorioGerencialContagem.SDT_RelatorioGerencialContagem", "GxEv3Up14_Meetrika", "SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem", "GeneXus.Programs");
         AV27SDT_RelatorioGerencialContagem = new SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem(context);
         AV9ContagemResultado_ContratadaOrigemPesNom = "";
         AV28SDT_RelatorioGerencialContagemOS = new SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS(context);
         AV12ContagemResultado_DataDmn = DateTime.MinValue;
         AV30ContagemResultado_Descricao = "";
         AV11NumeroOSFM_Referencia = "";
         AV10Status = "";
         AV31ContagemResultado_ServicoSigla = "";
         AV19ContagemResultado_DataCntInicial = DateTime.MinValue;
         AV20ContagemResultado_DataCntFinal = DateTime.MinValue;
         AV34NumeroOSFM_Vinvulada = "";
         AV29SDT_RelatorioGerencialContagemOSVinculada = new SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_OSVinculada(context);
         scmdbuf = "";
         P00XR3_A40000Contratada_PessoaNom = new String[] {""} ;
         A40000Contratada_PessoaNom = "";
         P00XR5_A40001ContratoServicos_Alias = new String[] {""} ;
         A40001ContratoServicos_Alias = "";
         AV16Filtro = "";
         AV54Contratada_PessoaNom = "";
         AV55ContratoServicos_Alias = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.arel_relatoriogerencialcontagempdf__default(),
            new Object[][] {
                new Object[] {
               P00XR3_A40000Contratada_PessoaNom
               }
               , new Object[] {
               P00XR5_A40001ContratoServicos_Alias
               }
            }
         );
         /* GeneXus formulas. */
         Gx_line = 0;
         context.Gx_err = 0;
      }

      private short gxcookieaux ;
      private short nGotPars ;
      private short GxWebError ;
      private short AV58TotalOS ;
      private short AV59TotalOSDivergentes ;
      private int AV17ContagemResultado_ContratadaOrigemCod ;
      private int AV14ContagemResultado_CntSrvCod ;
      private int M_top ;
      private int M_bot ;
      private int Line ;
      private int ToSkip ;
      private int PrtOffset ;
      private int AV25Contrato_AreaTrabalhoCod ;
      private int AV62GXV1 ;
      private int Gx_OldLine ;
      private int AV63GXV2 ;
      private int AV37ContagemResultado_Codigo ;
      private int AV64GXV3 ;
      private int AV65GXV4 ;
      private decimal AV22ContagemResultado_PFLFS_Inicial ;
      private decimal AV23ContagemResultado_PFLFS_Final ;
      private decimal AV42ContagemResultado_PF ;
      private decimal AV24ContagemResultado_PFLFS_Diferenca ;
      private decimal AV38ContagemResultado_PFLFS_Inicial_Total ;
      private decimal AV39ContagemResultado_PFLFS_Final_Total ;
      private decimal AV40ContagemResultado_PFLFS_Diferenca_Total ;
      private decimal AV41ContagemResultado_PF_Total ;
      private decimal AV56PercentualOSDivergentes ;
      private String GXKey ;
      private String gxfirstwebparm ;
      private String AV13ContagemResultado_StatusDmn ;
      private String GXt_char1 ;
      private String AV9ContagemResultado_ContratadaOrigemPesNom ;
      private String AV31ContagemResultado_ServicoSigla ;
      private String scmdbuf ;
      private String A40000Contratada_PessoaNom ;
      private String A40001ContratoServicos_Alias ;
      private String AV54Contratada_PessoaNom ;
      private String AV55ContratoServicos_Alias ;
      private DateTime AV48ContagemResultado_DataDmnIn ;
      private DateTime AV47ContagemResultado_DataDmnFim ;
      private DateTime AV12ContagemResultado_DataDmn ;
      private DateTime AV19ContagemResultado_DataCntInicial ;
      private DateTime AV20ContagemResultado_DataCntFinal ;
      private bool entryPointCalled ;
      private bool AV32IsPrintOrigemReferencia ;
      private bool AV33IsPrintRotulos ;
      private bool returnInSub ;
      private bool AV36IsPrintTotal1 ;
      private String AV8TituloDataHora ;
      private String AV30ContagemResultado_Descricao ;
      private String AV11NumeroOSFM_Referencia ;
      private String AV10Status ;
      private String AV34NumeroOSFM_Vinvulada ;
      private String AV16Filtro ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00XR3_A40000Contratada_PessoaNom ;
      private String[] P00XR5_A40001ContratoServicos_Alias ;
      [ObjectCollection(ItemType=typeof( SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem ))]
      private IGxCollection AV26SDT_RelatorioGerencial ;
      [ObjectCollection(ItemType=typeof( SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem ))]
      private IGxCollection GXt_objcol_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem2 ;
      private SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem AV27SDT_RelatorioGerencialContagem ;
      private SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS AV28SDT_RelatorioGerencialContagemOS ;
      private SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_OSVinculada AV29SDT_RelatorioGerencialContagemOSVinculada ;
      private wwpbaseobjects.SdtWWPContext AV15WWPContext ;
   }

   public class arel_relatoriogerencialcontagempdf__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00XR3 ;
          prmP00XR3 = new Object[] {
          new Object[] {"@AV17ContagemResultado_ContratadaOrigemCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00XR5 ;
          prmP00XR5 = new Object[] {
          new Object[] {"@AV14ContagemResultado_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV25Contrato_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00XR3", "SELECT COALESCE( T1.[Contratada_PessoaNom], '') AS Contratada_PessoaNom FROM (SELECT MIN(T3.[Pessoa_Nome]) AS Contratada_PessoaNom FROM ([Contratada] T2 WITH (NOLOCK) INNER JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratada_PessoaCod]) WHERE T2.[Contratada_Codigo] = @AV17ContagemResultado_ContratadaOrigemCod ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00XR3,1,0,true,false )
             ,new CursorDef("P00XR5", "SELECT COALESCE( T1.[ContratoServicos_Alias], '') AS ContratoServicos_Alias FROM (SELECT MIN(T2.[ContratoServicos_Alias]) AS ContratoServicos_Alias FROM ([ContratoServicos] T2 WITH (NOLOCK) INNER JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) WHERE (T2.[Servico_Codigo] = @AV14ContagemResultado_CntSrvCod) AND (T3.[Contrato_AreaTrabalhoCod] = @AV25Contrato_AreaTrabalhoCod) ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00XR5,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
