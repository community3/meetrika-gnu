/*
               File: type_SdtContagemResultadoRequisito
        Description: Contagem Resultado Requisito
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:12:18.50
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "ContagemResultadoRequisito" )]
   [XmlType(TypeName =  "ContagemResultadoRequisito" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtContagemResultadoRequisito : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContagemResultadoRequisito( )
      {
         /* Constructor for serialization */
         gxTv_SdtContagemResultadoRequisito_Mode = "";
      }

      public SdtContagemResultadoRequisito( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV2005ContagemResultadoRequisito_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV2005ContagemResultadoRequisito_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"ContagemResultadoRequisito_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "ContagemResultadoRequisito");
         metadata.Set("BT", "ContagemResultadoRequisito");
         metadata.Set("PK", "[ \"ContagemResultadoRequisito_Codigo\" ]");
         metadata.Set("PKAssigned", "[ \"ContagemResultadoRequisito_Codigo\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"ContagemResultado_Codigo\" ],\"FKMap\":[ \"ContagemResultadoRequisito_OSCod-ContagemResultado_Codigo\" ] },{ \"FK\":[ \"Requisito_Codigo\" ],\"FKMap\":[ \"ContagemResultadoRequisito_ReqCod-Requisito_Codigo\" ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadorequisito_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadorequisito_oscod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadorequisito_reqcod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadorequisito_owner_Z" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtContagemResultadoRequisito deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtContagemResultadoRequisito)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtContagemResultadoRequisito obj ;
         obj = this;
         obj.gxTpr_Contagemresultadorequisito_codigo = deserialized.gxTpr_Contagemresultadorequisito_codigo;
         obj.gxTpr_Contagemresultadorequisito_oscod = deserialized.gxTpr_Contagemresultadorequisito_oscod;
         obj.gxTpr_Contagemresultadorequisito_reqcod = deserialized.gxTpr_Contagemresultadorequisito_reqcod;
         obj.gxTpr_Contagemresultadorequisito_owner = deserialized.gxTpr_Contagemresultadorequisito_owner;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Contagemresultadorequisito_codigo_Z = deserialized.gxTpr_Contagemresultadorequisito_codigo_Z;
         obj.gxTpr_Contagemresultadorequisito_oscod_Z = deserialized.gxTpr_Contagemresultadorequisito_oscod_Z;
         obj.gxTpr_Contagemresultadorequisito_reqcod_Z = deserialized.gxTpr_Contagemresultadorequisito_reqcod_Z;
         obj.gxTpr_Contagemresultadorequisito_owner_Z = deserialized.gxTpr_Contagemresultadorequisito_owner_Z;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoRequisito_Codigo") )
               {
                  gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoRequisito_OSCod") )
               {
                  gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_oscod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoRequisito_ReqCod") )
               {
                  gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_reqcod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoRequisito_Owner") )
               {
                  gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_owner = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtContagemResultadoRequisito_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtContagemResultadoRequisito_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoRequisito_Codigo_Z") )
               {
                  gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoRequisito_OSCod_Z") )
               {
                  gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_oscod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoRequisito_ReqCod_Z") )
               {
                  gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_reqcod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoRequisito_Owner_Z") )
               {
                  gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_owner_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "ContagemResultadoRequisito";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("ContagemResultadoRequisito_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultadoRequisito_OSCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_oscod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultadoRequisito_ReqCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_reqcod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultadoRequisito_Owner", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_owner)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtContagemResultadoRequisito_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoRequisito_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoRequisito_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoRequisito_OSCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_oscod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoRequisito_ReqCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_reqcod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoRequisito_Owner_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_owner_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("ContagemResultadoRequisito_Codigo", gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_codigo, false);
         AddObjectProperty("ContagemResultadoRequisito_OSCod", gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_oscod, false);
         AddObjectProperty("ContagemResultadoRequisito_ReqCod", gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_reqcod, false);
         AddObjectProperty("ContagemResultadoRequisito_Owner", gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_owner, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtContagemResultadoRequisito_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtContagemResultadoRequisito_Initialized, false);
            AddObjectProperty("ContagemResultadoRequisito_Codigo_Z", gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_codigo_Z, false);
            AddObjectProperty("ContagemResultadoRequisito_OSCod_Z", gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_oscod_Z, false);
            AddObjectProperty("ContagemResultadoRequisito_ReqCod_Z", gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_reqcod_Z, false);
            AddObjectProperty("ContagemResultadoRequisito_Owner_Z", gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_owner_Z, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "ContagemResultadoRequisito_Codigo" )]
      [  XmlElement( ElementName = "ContagemResultadoRequisito_Codigo"   )]
      public int gxTpr_Contagemresultadorequisito_codigo
      {
         get {
            return gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_codigo ;
         }

         set {
            if ( gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_codigo != value )
            {
               gxTv_SdtContagemResultadoRequisito_Mode = "INS";
               this.gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_codigo_Z_SetNull( );
               this.gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_oscod_Z_SetNull( );
               this.gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_reqcod_Z_SetNull( );
               this.gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_owner_Z_SetNull( );
            }
            gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultadoRequisito_OSCod" )]
      [  XmlElement( ElementName = "ContagemResultadoRequisito_OSCod"   )]
      public int gxTpr_Contagemresultadorequisito_oscod
      {
         get {
            return gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_oscod ;
         }

         set {
            gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_oscod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultadoRequisito_ReqCod" )]
      [  XmlElement( ElementName = "ContagemResultadoRequisito_ReqCod"   )]
      public int gxTpr_Contagemresultadorequisito_reqcod
      {
         get {
            return gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_reqcod ;
         }

         set {
            gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_reqcod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultadoRequisito_Owner" )]
      [  XmlElement( ElementName = "ContagemResultadoRequisito_Owner"   )]
      public bool gxTpr_Contagemresultadorequisito_owner
      {
         get {
            return gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_owner ;
         }

         set {
            gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_owner = value;
         }

      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtContagemResultadoRequisito_Mode ;
         }

         set {
            gxTv_SdtContagemResultadoRequisito_Mode = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoRequisito_Mode_SetNull( )
      {
         gxTv_SdtContagemResultadoRequisito_Mode = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoRequisito_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtContagemResultadoRequisito_Initialized ;
         }

         set {
            gxTv_SdtContagemResultadoRequisito_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoRequisito_Initialized_SetNull( )
      {
         gxTv_SdtContagemResultadoRequisito_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoRequisito_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoRequisito_Codigo_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoRequisito_Codigo_Z"   )]
      public int gxTpr_Contagemresultadorequisito_codigo_Z
      {
         get {
            return gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_codigo_Z ;
         }

         set {
            gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_codigo_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoRequisito_OSCod_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoRequisito_OSCod_Z"   )]
      public int gxTpr_Contagemresultadorequisito_oscod_Z
      {
         get {
            return gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_oscod_Z ;
         }

         set {
            gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_oscod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_oscod_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_oscod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_oscod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoRequisito_ReqCod_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoRequisito_ReqCod_Z"   )]
      public int gxTpr_Contagemresultadorequisito_reqcod_Z
      {
         get {
            return gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_reqcod_Z ;
         }

         set {
            gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_reqcod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_reqcod_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_reqcod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_reqcod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoRequisito_Owner_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoRequisito_Owner_Z"   )]
      public bool gxTpr_Contagemresultadorequisito_owner_Z
      {
         get {
            return gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_owner_Z ;
         }

         set {
            gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_owner_Z = value;
         }

      }

      public void gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_owner_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_owner_Z = false;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_owner_Z_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtContagemResultadoRequisito_Mode = "";
         gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_owner = false;
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "contagemresultadorequisito", "GeneXus.Programs.contagemresultadorequisito_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtContagemResultadoRequisito_Initialized ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_codigo ;
      private int gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_oscod ;
      private int gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_reqcod ;
      private int gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_codigo_Z ;
      private int gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_oscod_Z ;
      private int gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_reqcod_Z ;
      private String gxTv_SdtContagemResultadoRequisito_Mode ;
      private String sTagName ;
      private bool gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_owner ;
      private bool gxTv_SdtContagemResultadoRequisito_Contagemresultadorequisito_owner_Z ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"ContagemResultadoRequisito", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtContagemResultadoRequisito_RESTInterface : GxGenericCollectionItem<SdtContagemResultadoRequisito>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContagemResultadoRequisito_RESTInterface( ) : base()
      {
      }

      public SdtContagemResultadoRequisito_RESTInterface( SdtContagemResultadoRequisito psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "ContagemResultadoRequisito_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultadorequisito_codigo
      {
         get {
            return sdt.gxTpr_Contagemresultadorequisito_codigo ;
         }

         set {
            sdt.gxTpr_Contagemresultadorequisito_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultadoRequisito_OSCod" , Order = 1 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultadorequisito_oscod
      {
         get {
            return sdt.gxTpr_Contagemresultadorequisito_oscod ;
         }

         set {
            sdt.gxTpr_Contagemresultadorequisito_oscod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultadoRequisito_ReqCod" , Order = 2 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultadorequisito_reqcod
      {
         get {
            return sdt.gxTpr_Contagemresultadorequisito_reqcod ;
         }

         set {
            sdt.gxTpr_Contagemresultadorequisito_reqcod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultadoRequisito_Owner" , Order = 3 )]
      [GxSeudo()]
      public bool gxTpr_Contagemresultadorequisito_owner
      {
         get {
            return sdt.gxTpr_Contagemresultadorequisito_owner ;
         }

         set {
            sdt.gxTpr_Contagemresultadorequisito_owner = value;
         }

      }

      public SdtContagemResultadoRequisito sdt
      {
         get {
            return (SdtContagemResultadoRequisito)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtContagemResultadoRequisito() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 10 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
